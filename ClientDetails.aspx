﻿<%@ Page Title="Informations client" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ClientDetails.aspx.cs" Inherits="ClientDetails" %>

<%@ Register Src="~/API/AmlAlert.ascx" TagPrefix="asp" TagName="AmlAlert" %>
<%@ Register Src="~/API/AmlHideAlert.ascx" TagPrefix="asp" TagName="AmlHideAlert" %>
<%@ Register Src="~/API/BlockedTransfer.ascx" TagPrefix="asp" TagName="BlockedTransfer" %>
<%@ Register Src="~/API/sms-gage.ascx" TagPrefix="asp" TagName="SmsGage" %>
<%@ Register Src="~/API/GarnishmentList.ascx" TagPrefix="asp" TagName="Garnishment" %>
<%@ Register Src="~/API/AmlSurveillance.ascx" TagPrefix="asp" TagName="AmlSurveillance" %>
<%@ Register Src="~/API/IPConnectionList.ascx" TagPrefix="asp" TagName="IpConnectionList" %>
<%@ Register Src="~/API/PersonalizedCardOrder.ascx" TagPrefix="asp" TagName="PersonalizedCardOrderList" %>
<%@ Register Src="~/API/LightLock.ascx" TagPrefix="asp" TagName="LightLock" %>
<%@ Register Src="~/API/ReturnFundsList.ascx" TagPrefix="asp" TagName="ReturnFundsList" %>
<%@ Register Src="~/API/ClientFileManager.ascx" TagPrefix="asp" TagName="ClientFileManager" %>
<%@ Register Src="~/API/ClientFileUpload.ascx" TagPrefix="asp" TagName="ClientFileUpload" %>
<%@ Register Src="~/API/ReqDComList.ascx" TagPrefix="asp" TagName="ReqDComList" %>
<%@ Register Src="~/API/ProNotificationCounter.ascx" TagName="ProNotificationCounter" TagPrefix="asp" %>
<%@ Register Src="~/API/Scoring.ascx" TagPrefix="asp" TagName="Scoring" %>
<%@ Register Src="~/API/Activity.ascx" TagPrefix="asp" TagName="Activity" %>
<%@ Register Src="~/API/DeathStatus.ascx" TagPrefix="asp" TagName="DeathStatus" %>
<%@ Register Src="~/API/SearchTracfinStatement.ascx" TagPrefix="asp" TagName="SearchTracfinStatement" %>
<%@ Register Src="~/API/CAPAddress.ascx" TagPrefix="asp" TagName="CAPAddress" %>
<%@ Register Src="~/API/CAPEmail.ascx" TagPrefix="asp" TagName="CAPEmail" %>
<%@ Register Src="~/API/Balance.ascx" TagPrefix="asp" TagName="Balance" %>
<%@ Register Src="~/API/ChurnResult.ascx" TagPrefix="asp" TagName="ChurnResult" %>
<%@ Register Src="~/API/ClientRelation.ascx" TagPrefix="asp" TagName="ClientRelation" %>
<%@ Register Src="~/API/SABCobaltOpeComparison.ascx" TagName="SABCobaltOpeComparison" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script type="text/javascript" src="Scripts/sms-gage.js"></script>
    <script src="Scripts/jquery.stickytableheaders.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="Scripts/toggles.js"></script>
    <script type="text/javascript" src="Scripts/ui.tabs.paging.js"></script>
    <%--<script type="text/javascript" src="Scripts/LightLock.js"></script>--%>
    <script type="text/javascript" src="Scripts/Scoring.js"></script>

    <script type="text/javascript" src="Scripts/jquery.sticky.js"></script>
    <link rel="Stylesheet" href="Styles/ReturnFundsList.css" />

    <link rel="Stylesheet" href="Styles/ClientFileUpload.css" />

    <link type="text/css" rel="Stylesheet" href="Styles/ProNotificationCounter.css" />
    <link type="text/css" rel="Stylesheet" href="Styles/Scoring.css?v=20180905" />
    <link type="text/css" rel="Stylesheet" href="Styles/Activity.css" />
    <link type="text/css" rel="Stylesheet" href="Styles/DeathStatus.css" />

    <script type="text/javascript" src="Scripts/CAPAddress.js?v=20171128"></script>
    <script type="text/javascript" src="Scripts/CAPEmail.js"></script>
    <link rel="Stylesheet" href="Styles/CAPAddress.css" />
    <link rel="Stylesheet" href="Styles/CAPEmail.css" />
    <link rel="Stylesheet" href="Styles/ChurnResult.css" />

    <script type="text/javascript" src="Scripts/ClientRelation.js"></script>
    <script type="text/javascript" src="Scripts/tableHeadFixer.js"></script>
    <link rel="Stylesheet" href="Styles/ClientRelation.css" />
    <link type="text/css" rel="Stylesheet" href="Styles/SABCobaltOpeComparison.css" />

    <script type="text/javascript">

        $(document).ready(function () {
            InitPage();
            CheckMessages();
            checkDriStatus();
            checkHeaderBand(showTab);
        });

        function checkDriStatus() {
            var driClass = $('#<%=hdnDriStatus.ClientID%>').val();

            if(driClass.trim().length > 0)
                $('body').removeClass(driClass).addClass(driClass);
            else
                $('body').removeClass(driClass);
        }

        function showTab() {
            var tabToShow = "";

            var forceTabId = $('#<%=hdnForceShowTab.ClientID%>').val();

            if(forceTabId.trim().length > 0 && $('#'+forceTabId) != null) {
                try {
                    tabToShow = forceTabId;
                    $('.ClientDetails a[href="#'+forceTabId+'"]').click();
                } catch(ex){console.log("showTab ex :"+ex.toString());}
            }
            else if ($('#<%=hdnActiveTab.ClientID%>').val().trim().length > 0) {
                $(".ClientDetails").tabs("option", "active", $('#<%=hdnActiveTab.ClientID%>').val().trim());
                tabToShow = $('#<%=hdnActiveTab.ClientID%>').val().trim();
            }

            if ('<%=panelAML.Visible.ToString().ToLower()%>' == 'true' &&
                $('#<%=hdnNbAlert.ClientID%>').val().trim().length > 0 &&
                parseInt($('#<%=hdnNbAlert.ClientID%>').val().trim()) > 0) {
                try {
                    if (tabToShow == "") {
                        $('.ClientDetails a[href="#<%=panelAML.ClientID%>"]').click();
                    }
                    showNbAlert($('#<%=hdnNbAlert.ClientID%>').val().trim(), "lblNbAlert", "divNbAlert", "<%=panelAML.ClientID%>");
                }
                catch (ex) { console.log(ex); }
            }

			if ('<%=panelDocs.Visible.ToString().ToLower()%>' == 'true' &&
				$('#<%=hdnDocsAlert.ClientID%>').val().trim().length > 0 &&
                parseInt($('#<%=hdnDocsAlert.ClientID%>').val().trim()) > 0) {
				try {
					if (tabToShow == "") {
						$('.ClientDetails a[href="#<%=panelDocs.ClientID%>"]').click();
					}
					showNbAlert($('#<%=hdnDocsAlert.ClientID%>').val().trim(), "lblDocsAlert", "divDocsAlert", "<%=panelDocs.ClientID%>");
				}
                catch (ex) { console.log(ex); }
			}
        }

        function InitPage() {
            InitControls();
            initTooltip();

            $(".ClientDetails").tabs({
                activate: function (event, ui) {
                    $('#<%=hdnActiveTab.ClientID%>').val($(".ClientDetails").tabs("option", "active"));

                        //CARDS
                        if ($('#<%=panelCards.ClientID%>') != null && $('#<%=panelCards.ClientID%>').is(':visible')) {
                            //InitLimitsBar();
                            ShowCardActionLoading();
                            $('#<%=btnLoadHisto.ClientID%>').click();
                        }
                        //AML
                        if ($("#<%= panelAML.ClientID %>").is(':visible')) {
                            $("#divNbAlert").removeClass("semiTransparent");
                            InitAMLSurveillance();
                        }
                        else { $("#divNbAlert").addClass("semiTransparent"); }

                        //DOCUMENTS
                        if ($('#<%=panelDocs.ClientID%>') != null && $('#<%=panelDocs.ClientID%>').is(':visible')) {
                            $("#divDocsAlert").removeClass("semiTransparent");
                            $('#<%=btnLoadOtherDocs.ClientID%>').click();
                        }
                        else { $("#divDocsAlert").addClass("semiTransparent"); }

                        //MESSAGES
                        if ($("#<%= panelMessageExchange.ClientID %>").is(':visible')) {
                            $('#<%= btnRefreshWebMessageHistory.ClientID %>').click();
                        }

                        //OPERATIONS
                        if ($("#<%= panelBalanceOperations.ClientID %>").is(':visible')) {
                            InitDetailsSlider();
                        }

                        //IDENTIFIANTS
                        if ($("#<%= panelWeb.ClientID %>").is(':visible')) {
                            $('#<%= btnBindIPLocation.ClientID %>').click();
                        }
                    }
                });

                $(".ClientDetails").tabs('paging', { cycle: true, follow: true, followOnActive: true, activeOnAdd: true });

                $('#PersonalInfoPanel').show();
                $('#PersonalInfoArrow').attr('src', './Styles/Img/downArrow.png');

                $('#popup-PersonalInfo').dialog({
                    autoOpen: false,
                    resizable: false,
                    draggable: false,
                    modal: true,
                    minWidth: 500,
                    zIndex: 99997,
                    title: "Enregistrer les informations personnelles",
                    buttons: [{
                        text: 'OK', click: function () {
                            if (checkTaxResidence()) {
                                //console.log('checkTaxResidence OK');
                                showLoading("0.6", "Enregistrement en cours");
                                $("#<%=btnSavePersonalInfo.ClientID %>").click();
                                $(this).dialog('close');
                                $('#PersonalInfoPanel .loading').show();
                            }
                            else {
                                $(this).dialog('close');
                            }
                        }
                    }, { text: 'Annuler', click: function () { $(this).dialog('close'); } }]
                });
                $("#popup-PersonalInfo").parent().appendTo(jQuery("form:first"));
                $('#popup-DocsInfo').dialog({
                    autoOpen: false,
                    resizable: false,
                    draggable: false,
                    modal: true,
                    minWidth: 500,
                    zIndex: 99997,
                    title: "Enregistrer les documents",
                    buttons: [{ text: 'OK', click: function () { $("#<%=btnSaveDocsInfo.ClientID %>").click(); $(this).dialog('close'); $('#DocsPanel .loading').show(); } }, { text: 'Annuler', click: function () { $(this).dialog('close'); } }]
                });
                $("#popup-DocsInfo").parent().appendTo(jQuery("form:first"));
                $('#popup-Limits').dialog({
                    autoOpen: false,
                    resizable: false,
                    draggable: false,
                    modal: true,
                    minWidth: 400,
                    zIndex: 99997,
                    buttons: [{ text: 'OK', click: function () { $("#<%=btnAcceptRefuseLimitRequest.ClientID %>").click(); $(this).dialog('close'); $('#LimitsPanel .loading').show(); } }, { text: 'Annuler', click: function () { $(this).dialog('close'); } }]
                });
                $("#popup-Limits").parent().appendTo(jQuery("form:first"));
                $('#popup-resetWEBPassword').dialog({
                    autoOpen: false,
                    resizable: false,
                    draggable: false,
                    modal: true,
                    minWidth: 310,
                    zIndex: 99997,
                    title: "Réinitialiser mot de passe WEB",
                    buttons: [{ text: 'OK', click: function () { $("#<%=btnResetWEBPassword.ClientID %>").click(); $(this).dialog('close'); $('#WEBPasswordPanel .loading').show(); } }, { text: 'Annuler', click: function () { $(this).dialog('close'); } }]
                });
                $("#popup-resetWEBPassword").parent().appendTo(jQuery("form:first"));
                $('#popup-sendWEBIDByMail').dialog({
                    autoOpen: false,
                    resizable: false,
                    draggable: false,
                    modal: true,
                    minWidth: 310,
                    zIndex: 99997,
                    title: "Envoyer identifiant WEB",
                    buttons: [{ text: 'OK', click: function () { $("#<%=btnSendWEBID.ClientID %>").click(); $(this).dialog('close'); $('#WEBIDPanel .loading').show(); } }, { text: 'Annuler', click: function () { $(this).dialog('close'); } }]
                });
                $("#popup-sendWEBIDByMail").parent().appendTo(jQuery("form:first"));
                $('#popup-PINBySMS').dialog({
                    autoOpen: false,
                    resizable: false,
                    draggable: false,
                    modal: true,
                    minWidth: 300,
                    zIndex: 99997,
                    title: "Renvoyer le code PIN",
                    buttons: [{ text: 'OK', click: function () { $("#<%=btnSendPIN.ClientID %>").click(); $(this).dialog('close'); $('#PINBySMSPanel .loading').show(); } }, { text: 'Annuler', click: function () { $(this).dialog('close'); } }]
                });
                $("#popup-PINBySMS").parent().appendTo(jQuery("form:first"));

                $('#divCheckAddressPopup').dialog({
                    autoOpen: false,
                    resizable: false,
                    draggable: false,
                    modal: true,
                    zIndex: 99997,
                    title: "Détails vérification adresse",
                    width: '500px'
                });
                if ($('#<%=hdnAllowCourrierActions.ClientID%>').val() == '1') {

                    if ('<%=btnForceAddressConfirmation.Visible.ToString().ToLower()%>' == "true") {
                        $('#divCheckAddressPopup').dialog({
                            buttons: [
                                { text: 'Forcer confirmation', click: function () { $('#popup-ForceAddressConfirmation').dialog('open'); }, css: { float: 'left' } },
                                { text: 'Renvoi', click: function () { $('#popup-SendCourrier').dialog('open'); }, css: { float: 'left' } },
                                { text: 'Rappel', click: function () { $('#popup-NotifyCourrier').dialog('open'); } }]
                        });
                    }
                    else {
                        $('#divCheckAddressPopup').dialog({
                            buttons: [
                                { text: 'Renvoi', click: function () { $('#popup-SendCourrier').dialog('open'); }, css: { float: 'left' } },
                                { text: 'Rappel', click: function () { $('#popup-NotifyCourrier').dialog('open'); } }]
                        });
                    }
                }

                $('#popup-SendCourrier').dialog({
                    autoOpen: false,
                    resizable: false,
                    draggable: false,
                    modal: true,
                    minWidth: 350,
                    zIndex: 99997,
                    title: "Renvoi du courrier de vérification",
                    buttons: [{ text: 'Renvoyer', click: function () { $("#<%=btnSendCourrier.ClientID %>").click(); $(this).dialog('close'); $('#divCheckAddressPopup').dialog('close'); $('#PersonalInfoPanel .loading').show(); } }]
                });
                $("#popup-SendCourrier").parent().appendTo(jQuery("form:first"));

                $('#popup-ForceAddressConfirmation').dialog({
                    autoOpen: false,
                    resizable: false,
                    draggable: false,
                    modal: true,
                    minWidth: 360,
                    zIndex: 99997,
                    title: "Forcer confirmation adresse du client",
                    buttons: [{ text: 'Forcer', click: function () { $("#<%=btnForceAddressConfirmation.ClientID %>").click(); $(this).dialog('close'); $('#divCheckAddressPopup').dialog('close'); $('#PersonalInfoPanel .loading').show(); } }]
                });
                $("#popup-ForceAddressConfirmation").parent().appendTo(jQuery("form:first"));

                $('#popup-NotifyCourrier').dialog({
                    autoOpen: false,
                    resizable: false,
                    draggable: false,
                    modal: true,
                    minWidth: 350,
                    zIndex: 99997,
                    title: "Rappel de vérification",
                    buttons: [{ text: 'Rappeler', click: function () { $("#<%=btnNotifyCourrier.ClientID %>").click(); $(this).dialog('close'); $('#divCheckAddressPopup').dialog('close'); $('#PersonalInfoPanel .loading').show(); } }]
                });
                $("#popup-NotifyCourrier").parent().appendTo(jQuery("form:first"));

                $('#popup-cardLockConfirm').dialog({
                    autoOpen: false,
                    resizable: false,
                    draggable: false,
                    modal: true,
                    minWidth: 300,
                    zIndex: 99997,
                    title: "Opposer la carte du client",
                    buttons: [{
                        text: 'Oui', click: function () {
                            $("#<%=btnCardLock.ClientID %>").click(); $(this).dialog('close');
                            ShowCardActionLoading();
                            //$('#panelCardLock .loading').show();
                        }
                    }, { text: 'Non', click: function () { $(this).dialog('close'); } }]
                });
                $("#popup-cardLockConfirm").parent().appendTo(jQuery("form:first"));

                $("#popup-sendRibByMail").dialog({
                    autoOpen: false,
                    resizable: false,
                    draggable: false,
                    modal: true,
                    minWidth: 550,
                    zIndex: 99997,
                    title: "Envoyer RIB par email",
                    buttons:
                        [{
                            text: 'OK',
                            click: function () {
                                $('#req_RibEmail').css('visibility', 'hidden');
                                if (!$('#<%=cbChangeRibEmail.ClientID%>').is(':checked') || ($('#<%=cbChangeRibEmail.ClientID%>').is(':checked') && checkEmail($('#<%=txtRibMail.ClientID%>').val()))) {
                                    $("#<%=btnRibSendMail.ClientID %>").click();
                                    $(this).dialog('close');
                                    $('#RibPanel .loading').show();
                                }
                                else {
                                    $('#req_RibEmail').css('visibility', 'visible');
                                    $('#<%=txtRibMail.ClientID%>').focus();
                                }
                            }
                        },
                    {
                        text: 'Annuler',
                        click: function () { $(this).dialog('close'); }
                    }]
                });
                    $("#popup-sendRibByMail").parent().appendTo(jQuery("form:first"));

                    $('#popup-delete-webmessage').dialog({
                        autoOpen: false,
                        resizable: false,
                        draggable: false,
                        modal: true,
                        minWidth: 300,
                        zIndex: 99997,
                        title: "Suppression message(s)",
                        buttons: [{ text: 'OK', click: function () { $("#<%=btnDeleteWebMessage.ClientID %>").click(); $(this).dialog('close'); $('#<%=panelDeleteWebMessage.ClientID%> .loading').show(); } }, { text: 'Annuler', click: function () { $(this).dialog('close'); } }]
                    });
                    $("#popup-delete-webmessage").parent().appendTo(jQuery("form:first"));

                    var sTitle = "Bloquer le compte du client";
                    $('#lblLockUnlockConfirm').html("Voulez-vous vraiment bloquer le compte du client ?");
                    $('#panelLockReason').show();
                    $('#<%=txtLockReason.ClientID%>').val('');
                if ($('#<%=hfLockUnlockStatus.ClientID%>').val() == "true") {
                    sTitle = "Débloquer le compte client";
                    $('#lblLockUnlockConfirm').html("Voulez-vous vraiment débloquer le compte du client ?");
                    $('#panelLockReason').hide();
                }

                $('#popup-lockUnlock').dialog({
                    autoOpen: false,
                    resizable: false,
                    draggable: false,
                    modal: true,
                    minWidth: 550,
                    zIndex: 99997,
                    title: sTitle,
                    buttons:
                        [{
                            text: 'OK',
                            click: function () {
                                $("#<%=btnLockUnlock.ClientID %>").click();
                                $(this).dialog('close');
                                $('#LockUnlockPanel .loading').show();
                            }
                        },
                    {
                        text: 'Annuler',
                        click: function () { $(this).dialog('close'); }
                    }]
                });
                $('#popup-lockUnlock').parent().appendTo(jQuery("form:first"));

                $("#popup-iFrameOperationDiv").dialog({
                    autoOpen: false,
                    resizable: false,
                    draggable: false,
                    modal: true,
                    minWidth: 1000,
                    zIndex: 99997,
                    title: "Operations",
                    beforeClose: function (event, ui) {
                        $('#iFrameDiv').hide();
                        var iFrame = document.getElementById("iFrameClientBalanceOperations");
                        iFrame.src = "";
                    }
                });

                $('#popup-AskKycUpdate').dialog({
                    autoOpen: false,
                    resizable: false,
                    draggable: false,
                    modal: true,
                    minWidth: 300,
                    zIndex: 99997,
                    title: "Demander mise à jour KYC",
                    buttons: [{ text: 'OK', click: function () { $("#<%=btnAskKycUpdate.ClientID %>").click(); $(this).dialog('close'); } }, { text: 'Annuler', click: function () { $(this).dialog('close'); } }]
                });
                $("#popup-AskKycUpdate").parent().appendTo(jQuery("form:first"));
            }

            function CheckChallengeReceived(bTimeout) {
                if(bTimeout == null)
                    bTimeout = true;

                if(bTimeout)
                    setTimeout(function () {
                        ShowChallengeReceivedDialog();
                    }, 2000);
                else ShowChallengeReceivedDialog();
            }
            function ShowChallengeReceivedDialog() {
                $('.check-sms').show();
                $('.send-to-email').hide();
                $('.change-email').hide();
                $('.verify-email').hide();
                $('#dialog-check-challenge-received').dialog({
                    autoOpen: true,
                    resizable: false,
                    draggable: false,
                    modal: true,
                    title: "Code de remplacement",
                    width: 350,
                    //buttons: [{ text: 'OK', click: function () { $(this).dialog('close'); } }]
                });
                $("#dialog-check-challenge-received").parent().appendTo(jQuery("form:first"));
            }
            function HideChallengeReceivedDialog() {
                if ($('#dialog-check-challenge-received').parent().hasClass('ui-dialog'))
                    $('#dialog-check-challenge-received').dialog('destroy');
            }
            function ChangeChallengeStep(step) {
                $('.check-sms').hide();
                $('.send-to-email').hide();
                $('.change-email').hide();
                $('.verify-email').hide();
                $('.' + step).show();
            }

            function ShowCardActionLoading() {
                $('.cards-actions-loading').show();
                $('.cards-actions-loading').width($('#<%=panelCardsHisto.ClientID%>').outerWidth());
                $('.cards-actions-loading').height($('#<%=panelCardsHisto.ClientID%>').outerHeight());

                $('.cards-actions-loading').position({
                    my: 'center',
                    at: 'center',
                    of: $('#<%=panelCardsHisto.ClientID%>')
            });
        }

        function checkEmail(email) {
            var email_regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return email_regex.test(email);
        }

        function InitControls() {

            $("#letter-histo").stickyTableHeaders({ scrollableArea: $(".letter-histo-scrollable-area")[0] });

            $('.RadioButtonList').buttonset();

            $('#<%=lblCheckAddressLink.ClientID%>').bind('click', function () {
                $('#divCheckAddressPopup').dialog('open');
            });

            $("#<%=txtBirthDate.ClientID %>").datepicker({
                defaultDate: "0",
                dateFormat: "dd/mm/yy",
                dayNamesMin: ["Di", "Lu", "Ma", "Me", "Je", "Ve", "Sa"],
                monthNames: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"],
                firstDay: 1,
                //changeMonth: true,
                numberOfMonths: 1
            });

            $("#<%=txtDeliveryDateID.ClientID %>").datepicker({
                defaultDate: "-1w",
                dateFormat: "dd/mm/yy",
                dayNamesMin: ["Di", "Lu", "Ma", "Me", "Je", "Ve", "Sa"],
                monthNames: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"],
                firstDay: 1,
                //changeMonth: true,
                numberOfMonths: 1,
                onClose: function (selectedDate) {
                    $("#<%=txtExpireDateID.ClientID %>").datepicker("option", "minDate", selectedDate);
                },
                /*beforeShow: function (input, inst) {
                    setTimeout(function () {
                        inst.dpDiv.position({ my: 'right top', at: 'right bottom', of: input });
                    }, 0);
                }*/
            });
                $("#<%=txtExpireDateID.ClientID %>").datepicker({
                defaultDate: "0",
                dateFormat: "dd/mm/yy",
                dayNamesMin: ["Di", "Lu", "Ma", "Me", "Je", "Ve", "Sa"],
                monthNames: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"],
                firstDay: 1,
                //changeMonth: true,
                numberOfMonths: 1,
                onClose: function (selectedDate) {
                    $("#<%=txtDeliveryDateID.ClientID %>").datepicker("option", "maxDate", selectedDate);
                }
                });

                $("#<%=txtDeliveryDateHO.ClientID %>").datepicker({
                defaultDate: "-1w",
                dateFormat: "dd/mm/yy",
                dayNamesMin: ["Di", "Lu", "Ma", "Me", "Je", "Ve", "Sa"],
                monthNames: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"],
                firstDay: 1,
                //changeMonth: true,
                numberOfMonths: 1,
                onClose: function (selectedDate) {
                    $("#<%=txtExpireDateHO.ClientID %>").datepicker("option", "minDate", selectedDate);
                },
                    /*beforeShow: function (input, inst) {
                        setTimeout(function () {
                            inst.dpDiv.position({ my: 'right top', at: 'right bottom', of: input });
                        }, 0);
                    }*/
                });
                $("#<%=txtExpireDateHO.ClientID %>").datepicker({
                defaultDate: "0",
                dateFormat: "dd/mm/yy",
                dayNamesMin: ["Di", "Lu", "Ma", "Me", "Je", "Ve", "Sa"],
                monthNames: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"],
                firstDay: 1,
                //changeMonth: true,
                numberOfMonths: 1,
                onClose: function (selectedDate) {
                    $("#<%=txtDeliveryDateHO.ClientID %>").datepicker("option", "maxDate", selectedDate);
                }
                });

                checkMaidenName();
                $('#<%=rblCivility.ClientID%>').change(function () {
                    checkMaidenName();
                });

                InitCardPreferenceToggles();
            }

            function checkMaidenName(){
                if ($('#<%=rblCivility.ClientID%> input:checked').val() == "MME")
                    $('#divMaidenName').show();
                else
                    $('#divMaidenName').hide();
            }

            function CheckMessages() {
                setTimeout(function () {

                    if ($('#<%=hdnMessage.ClientID%>').val().length > 0) {
                        $('#message').empty().append($('#<%=hdnMessage.ClientID%>').val());
                        $('#popup-message').dialog({
                            resizable: false,
                            draggable: false,
                            modal: true,
                            title: "Message",
                            width: "auto",
                            zIndex: 99997,
                            buttons: [{ text: 'Fermer', click: function () { $(this).dialog('close'); $(this).dialog('destroy'); } }]
                        });
                        $('#<%=hdnMessage.ClientID%>').val('');
                    }

                    $('.loading').hide();
                },500);
        }

        function ConfirmAction(action, values) {
            switch (action) {
                case 'PersonalInfo':
                    $('#popup-PersonalInfo').dialog('open');
                    break;
                case 'DocsInfo':
                    $("#popup-DocsInfo").dialog('open');
                    break;
                case 'sendWEBID':
                    if ($('#<%=txtMail.ClientID%>').val().trim().length > 0) {
                        $("#popup-sendWEBIDByMail").dialog('open');
                    }
                    else { AlertMessage("Envoyer identifiant WEB","Le mail du client n'est pas renseigné !"); }
                    break;
                case 'resetWEBPassword':
                    $('#popup-resetWEBPassword').dialog('open');
                    break;
                case 'PINBySMS':
                    $('#popup-PINBySMS').dialog('open');
                    break;
                case 'cardReplacement':
                    /*$('#popup-cardReplacement').dialog('open');*/
                    $('#<%=btnCardReplacement.ClientID%>').click();
                    break;
                case 'cardLock':
                    showCardLockConfirm(values);
                    break;
                case 'sendRibByMail':
                    if ($('#<%=txtMail.ClientID%>').val().trim().length == 0)
                        $('#<%=cbChangeRibEmail.ClientID%>').attr("checked", "checked");

                    if ($('#<%=cbChangeRibEmail.ClientID%>').is(':checked'))
                        $('#divRibMailReplacement').css('visibility', 'visible');
                    else
                        $('#divRibMailReplacement').css('visibility', 'hidden');
                    $('#popup-sendRibByMail').dialog('open');
                    break;
                case 'lockUnlock':
                    $('#popup-lockUnlock').dialog('open');
                    break;
                case 'deleteWebMessage':
                    if($('#WebMessagePanel input[type=checkbox]:checked').length > 0)
                        $('#popup-delete-webmessage').dialog('open');
                    break;
                case 'AskKycUpdate':
                    $('#popup-AskKycUpdate').dialog('open');
                    break;
                case 'KycUpdate':
                    $('#popup-KycUpdate').dialog('open');
                    break;
            }
        }

        function showCardLockConfirm(CardNumber){
            if(CardNumber != null && CardNumber.trim().length > 0){
                $('#<%=hfCardLock_CardNumber.ClientID%>').val(CardNumber);
                $('#popup-cardLockConfirm').dialog('open');
            }
            else {
                AlertMessage("Une erreur est survenue.");
            }
        }

        function GoToRegistration(regID) {
            document.location.href = "RegistrationDetails.aspx?No=" + regID;
        }

        function InitLimitRequestAction(ref, action) {
            $('#<%=hdnRefLimitRequest.ClientID%>').val(ref);
            $('#<%=hdnLimitRequestAction.ClientID%>').val(action);

            //console.log(ref + "-" + action);
            //console.log($('#<%=hdnRefLimitRequest.ClientID%>').val() + "-" + $('#<%=hdnLimitRequestAction.ClientID%>').val());

            var title = '';
            var content = '';
            switch(action)
            {
                case "1":
                    title = "Accepter la demande de plafond";
                    content = "Voulez-vous vraiment accepter la demande de changement de plafond ?";
                    break;
                case "2":
                    title = "Refuser la demande de plafond";
                    content = "Voulez-vous vraiment refuser la demande de changement de plafond ?";
                    break;
            }
            $('#popup-Limits-label').empty().append(content);
            $('#popup-Limits').dialog({ title: title });
            $("#popup-Limits").parent().appendTo(jQuery("form:first"));
            $('#popup-Limits').dialog('open');
        }

        function InitLimitsBar() {
            // Card payment progress bar
            var sMax = $('#<%=hdnCardPaymentLimit.ClientID%>').val();
            var sVal = $('#<%=hdnCardPaymentForNow.ClientID%>').val();
            InitProgressBar('CardPaymentProgress', sMax, sVal);

            // Withdraw progress bar
            var sMax = $('#<%=hdnWithdrawLimit.ClientID%>').val();
            var sVal = $('#<%=hdnWithdrawForNow.ClientID%>').val();
            InitProgressBar('WithdrawProgress', sMax, sVal);

            // Cash deposit progress bar
            var sMax = $('#<%=hdnCashDepositLimit.ClientID%>').val();
            var sVal = $('#<%=hdnCashDepositForNow.ClientID%>').val();
            InitProgressBar('CashDepositProgress', sMax, sVal);
        }
        function InitProgressBar(id, max, val) {
            //console.log(val + "/" + max);
            var pVal = val.replace(',00', '');
            $('#' + id).progressbar({
                max: parseInt(max),
                value: parseInt(pVal)
            });

            lblProgressValue = $('<span/>').addClass('progressLabel').append(val + ' &euro;');

            var pBarWidth = $('#' + id + ' .ui-progressbar-value').outerWidth();
            //console.log(pBarWidth);

            $('#' + id + ' .ui-progressbar-value').empty().append(lblProgressValue).show();
            if (pVal == '0') {
                $('#' + id + ' .ui-progressbar-value').css('border', '0');
            }

            var lblWidth = $('#' + id + ' .progressLabel').outerWidth() + 5;
            //console.log(lblWidth);

            if (lblWidth >= pBarWidth) {
                $('#' + id + ' .progressLabel').position({ my: 'left', at: 'right+5', of: $('#' + id + ' .ui-progressbar-value') });
                $('#' + id + ' .progressLabel').addClass('font-orange');
            }
            else {
                $('#' + id + ' .progressLabel').position({ my: 'right', at: 'right-5', of: $('#' + id + ' .ui-progressbar-value') });
                $('#' + id + ' .progressLabel').addClass('font-white');
            }
        }

        function InitLockUnlock() {
            var sTitle = "Bloquer le compte du client";
            $('#lblLockUnlockConfirm').html("Voulez-vous vraiment bloquer le compte du client ?");
            $('#panelLockReason').show();
            $('#<%=txtLockReason.ClientID%>').val('');
            if ($('#<%=hfLockUnlockStatus.ClientID%>').val() == "true") {
                sTitle = "Débloquer le compte client";
                $('#lblLockUnlockConfirm').html("Voulez-vous vraiment débloquer le compte du client ?");
                $('#panelLockReason').hide();
            }
            $('#popup-lockUnlock').dialog({
                autoOpen: false,
                resizable: false,
                draggable: false,
                modal: true,
                minWidth: 550,
                zIndex: 99997,
                title: sTitle,
                buttons:
                    [{
                        text: 'OK',
                        click: function () {
                            $("#<%=btnLockUnlock.ClientID %>").click();
                            $(this).dialog('close');
                            $('#LockUnlockPanel .loading').show();
                        }
                    },
                {
                    text: 'Annuler',
                    click: function () { $(this).dialog('close'); }
                }]
            });
                $('#popup-lockUnlock').parent().appendTo(jQuery("form:first"));

            <%--$('#<%=lblLockUnlockTitle.ClientID%>').html("Bloquer");
            if ($('#<%=hfLockUnlockStatus.ClientID%>').val().trim() == "true") {
                $('#<%=lblLockUnlockTitle.ClientID%>').html("Débloquer");
            }--%>
        }

        function changeRibEmail() {
            if ($('#<%=cbChangeRibEmail.ClientID%>').is(':checked')) {
                $('#divRibMailReplacement').css('visibility', 'visible');
                $('#<%=txtRibMail.ClientID%>').val('').focus();
            }
            else {
                $('#req_RibEmail').css('visibility', 'hidden');
                $('#divRibMailReplacement').css('visibility', 'hidden');
            }
        }

        function printRib(refCustomer, accountNumber) {
            window.open("rib.aspx?ref="+refCustomer+"&accNb="+accountNumber, '_blank');
        }

        function RibRefresh() {
            //alert('rib refresh');
            $('#<%=btnRibRefresh.ClientID%>').click();
        }

        function balanceOperation() {
            //document.location.href = "ClientBalanceOperations.aspx?ref=<%=hdnRefCustomer.Value%>";
            var iFrame = document.getElementById("iFrameClientBalanceOperations");
            iFrame.src = "ClientBalanceOperations_Frame.aspx?ref=<%=hdnRefCustomer.Value%>&accountno=<%=lblClientAccountNumber.Text%>&isCobalt=<%=hdnIsCobalt.Value%>&iban=<%=hdnIBAN.Value%>";
            document.getElementById("iFrameDiv").style.display = "block";
            $('#popup-iFrameOperationDiv').dialog('open').dialog({ title: "Solde / Opérations" });

        }
        function authorizations() {
            var iFrame = document.getElementById("iFrameClientBalanceOperations");
            iFrame.src = "ClientAuthorizations_Frame.aspx?ref=<%=lblClientAccountNumber.Text%>";
            document.getElementById("iFrameDiv").style.display = "block";
            $('#popup-iFrameOperationDiv').dialog('open').dialog({ title: "Autorisations / Refus Monext" });
        }
        function rejectsSAB() {
            var iFrame = document.getElementById("iFrameClientBalanceOperations");
            iFrame.src = "ClientRejectsSAB_Frame.aspx?ref=<%=lblClientAccountNumber.Text%>";
            document.getElementById("iFrameDiv").style.display = "block";
            $('#popup-iFrameOperationDiv').dialog('open').dialog({ title: "Refus SAB" });
        }
        function transfers() {
            var iFrame = document.getElementById("iFrameClientBalanceOperations");
            iFrame.src = "ClientTransfers_Frame.aspx?ref=<%=lblClientAccountNumber.Text%>";
            document.getElementById("iFrameDiv").style.display = "block";
            $('#popup-iFrameOperationDiv').dialog('open').dialog({ title: "Virements entrants" });
        }
        function debits() {
            var iFrame = document.getElementById("iFrameClientBalanceOperations");
            iFrame.src = "ClientDebits_Frame.aspx?ref=<%=hdnRefCustomer.Value%>&account=<%=lblClientAccountNumber.Text%>&isCobalt=<%=hdnIsCobalt.Value%>&iban=<%=hdnIBAN.Value%>";
            document.getElementById("iFrameDiv").style.display = "block";
            $('#popup-iFrameOperationDiv').dialog('open').dialog({ title: "Prélèvements" });
        }
        function releves() {
            var iFrame = document.getElementById("iFrameClientBalanceOperations");
            iFrame.src = "vos-releves_Frame.aspx?ref=<%=hdnRefCustomer.Value%>&card=<%=lblClientCardNumber.Text%>";
            document.getElementById("iFrameDiv").style.display = "block";
            $('#popup-iFrameOperationDiv').dialog('open').dialog({ title: "Relevés" });
        }
        function mandats() {
            var iFrame = document.getElementById("iFrameClientBalanceOperations");
            iFrame.src = "ClientMandats_Frame.aspx?ref=<%=hdnRefCustomer.Value%>&account=<%=lblClientAccountNumber.Text%>&isCobalt=<%=hdnIsCobalt.Value%>&iban=<%=hdnIBAN.Value%>";
            document.getElementById("iFrameDiv").style.display = "block";
            $('#popup-iFrameOperationDiv').dialog('open').dialog({ title: "Mandats" });
        }

        function showRedMailDetails() {
            $("#divRedMail-popup").dialog({
                resizable: false,
                draggable: false,
                width: 700,
                modal: true,
                zIndex: 99997,
                title: "Liste des inscriptions à la même adresse"
            });
        }

        function showClientDetails(sRegistrationCode) {
            if (sRegistrationCode != null && sRegistrationCode.trim().length > 0) {
                window.open("ClientDetails.aspx?No=" + sRegistrationCode, '_blank')
            }
        }

        function GoToSurveillance() {
            var index = 0;
            for (i = 0; i < $('.ui-tabs-nav li').length; i++) {
                if ($('.ui-tabs-nav li:eq(' + i + ')').attr('aria-controls') == '<%=panelAML.ClientID%>') {
                    index = i;
                    break;
                }
            }
            $(".ClientDetails").tabs("option", "active", index);
            window.scrollTo(0, document.body.scrollHeight);
        }

        function ShowAnnualFeeStatus() {
            $('#popup-force-annual-fee').dialog({
                autoOpen: true,
                resizable: false,
                draggable: false,
                modal: true,
                minWidth: 500,
                zIndex: 99997,
                title: "Statut renouvellement annuel",
            });
            $('#popup-force-annual-fee').parent().appendTo(jQuery("form:first"));
        }
        function HideAnnualFeeStatus() {
            $('#popup-force-annual-fee').dialog('close');
        }
        function ShowAnnualFeeLoading() {
            $('#div-force-annual-fee .loading').show();
        }

        function refreshAmlClientNote() {
            //console.log('refreshAmlClientNote');
            $('#<%=btnRefreshClientNote.ClientID%>').click();
        }

        function InitCardPreferenceToggles(){
            $('#card-payment-preference input[type=checkbox]').unbind('change');

            $('#toggleBlockAllPayment').toggles({
                text: { on: 'oui', off: 'non' },
                drag: false,
                on: $('#<%=ckbBlockAllPayment.ClientID%>').is(':checked'),
                checkbox: $('#<%=ckbBlockAllPayment.ClientID%>'),
                width: 125,
                height: 20
            });
            $('#toggleBlockOutFrancePayment').toggles({
                text: { on: 'oui', off: 'non' },
                drag: false,
                on: $('#<%=ckbBlockOutFrancePayment.ClientID%>').is(':checked'),
                checkbox: $('#<%=ckbBlockOutFrancePayment.ClientID%>'),
                width: 125,
                height: 20
            });
            $('#toggleBlockInternetPayment').toggles({
                text: { on: 'oui', off: 'non' },
                drag: false,
                on: $('#<%=ckbBlockInternetPayment.ClientID%>').is(':checked'),
                checkbox: $('#<%=ckbBlockInternetPayment.ClientID%>'),
                width: 125,
                height: 20
            });

            $('#card-payment-preference input[type=checkbox]').change(function(){
                $(this).closest('td').find('input[type=submit]')[0].click();
            });
        }
    </script>
    <script type="text/javascript">
        function ShowPanelLoading(panelID) {
            var panel = $('#'+panelID);
            if(panelID.trim().length > 0 && panel != null){

                $('.panel-loading').show();
                $('.panel-loading').width(panel.outerWidth());
                $('.panel-loading').height(panel.outerHeight());

                $('.panel-loading').position({
                    my: 'center',
                    at: 'center',
                    of: panel
                });
            }
        }
        function HidePanelLoading(){
            $('.panel-loading').hide();
        }
    </script>
    <script type="text/javascript">
        var iCpt = 4;
        var interval = null;
        function OpenZendeskTab(id) {
            OpenZendeskInterval(id);
            interval = setInterval(function(){OpenZendeskInterval(id)}, 1000);
        }
        function OpenZendeskInterval(id){
            iCpt--;
            $('.zendesk-interval').html(iCpt);
            if(iCpt == 0){
                $('#zendesk-countdown').hide();
                clearInterval(interval);
                var newwin = window.open('https://comptenickel.zendesk.com/agent/tickets/new/1?requester_id=' + id);
                if(!newwin)
                    AlertMessage("Erreur", "Veuillez autoriser l'ouverture de popup puis recharger la page.", null, null);
            }
        }
        function CheckSVIheadbandPosition(){
            // check svi headband positioning
            if($('#panelHeadband').is(':visible'))
                $('.svi-headband').addClass('svi-offset');
        }

        function ShowSabStatusDialog() {
            var bankName = "SAB";
            if ($('#<%=hdnIsCobalt.ClientID%>').val() == "1") { bankName = "Cobalt";}
            $('#popup-SabStatus').dialog({
                autoOpen: true,
                resizable: false,
                draggable: false,
                modal: true,
                minWidth: 600,
                //zIndex: 99997,
                title: "&Eacute;tat " + bankName,
                buttons: [{ text: 'Fermer', click: function () { $(this).dialog('destroy'); } }]
            });
            $("#popup-SabStatus").parent().appendTo(jQuery("form:first"));
        }
        function OnChangeSabStatus(){
            $('#MainContent_btnSaveSabStatus').prop('disabled', false);
            if($('#<%=ddlSabStatus.ClientID%>').val() != '0') {
                $('#<%=ddlLockSIReason.ClientID%>').show();
                $('#<%=ddlSabStatus.ClientID%>').prop('style', 'width:40%;box-sizing:border-box');
            }
            else {
                $('#<%=ddlLockSIReason.ClientID%>').hide();
                $('#<%=ddlSabStatus.ClientID%>').prop('style', 'width:75%;box-sizing:border-box');
            }
        }

        function ShowChangeLimitDialog() {
            $('#dialog-change-limit').dialog({
                autoOpen: true,
                resizable: false,
                draggable: false,
                modal: true,
                minWidth: 300,
                //zIndex: 99997,
                title: "Modifier les limites",
                buttons: [{ text: 'Annuler', click: function () { $(this).dialog('destroy'); } }, { text: 'Confirmer', click: function () { $('#<%=btnChangeLimit.ClientID%>').click(); } }]
            });
            $("#dialog-change-limit").parent().appendTo(jQuery("form:first"));
        }
        function HideChangeLimitDialog() {
            $('#dialog-change-limit').dialog('destroy');
        }
    </script>
    <script type="text/javascript">
        function initNickelChromeData() {
            <%--$('#<%=btnInitNickelChromeData.ClientID%>').click();
            $('#<%=btnInitNickelChromeData.ClientID%>').click(function (e) { e.stopImmediatePropagation(); });--%>
            __doPostBack('<%= btnInitNickelChromeData.UniqueID %>', '');
        }
    </script>
    <script type="text/javascript">
        function ShowSABCobaltOpeComparisonPopup() {
            $("#popup-sab-cobalt-ope-comparison").dialog({
                resizable: false,
                draggable: false,
                width: 1800,
                height: 700,
                modal: true,
                zIndex: 99997,
                title: "Comparer les opérations Cobalt/SAB"
            });
        }
    </script>
    <style type="text/css">
        .svi-headband {
            position: fixed;
            z-index: 99995;
            top: 0;
            left: 0;
            width: 100%;
            text-align: center;
            padding: 10px;
            text-transform: uppercase;
            font-weight: bold;
            color: #fff;
            box-shadow: 1px 1px 3px 0px #999;
        }

        .svi-headband.svi-not-auth {
            background-color: #f00;
        }

        .svi-headband.svi-auth {
            background-color: #0b0;
        }

        .svi-headband.svi-offset {
            top: 93px;
        }

        #zendesk-countdown {
            display: inline-block;
        }

        #dialog-check-challenge-received {
            /*position:relative;*/
            padding: 10px 10px 0 10px;
        }

            #dialog-check-challenge-received .button-container {
                text-align: right;
                padding: 10px 0 0 0;
                border-top: 1px solid #f57527;
                margin-top: 10px;
                /*position:absolute;
            bottom:0;
            right:10px;*/
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">

    <asp:Panel ID="panelSVIAuthenticated" runat="server" Visible="false" CssClass="svi-headband">
        <asp:Literal ID="litSVIauth" runat="server"></asp:Literal>
        <div id="zendesk-countdown">&nbsp;(Zendesk s'ouvrira dans <span class="zendesk-interval"></span>s)</div>
    </asp:Panel>

    <div style="position: relative;">
        <asp:Button ID="btnPrev" runat="server" Text="Recherche client" PostBackUrl="~/ClientSearch.aspx" CssClass="button" />
        <div style="position: absolute; right: 0; top: 0; text-align: right; max-width: 200px">
            <asp:UpdatePanel ID="upReopenAccount" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Button ID="btnReopenAccount" runat="server" Text="Rouvrir le compte" CssClass="button" OnClick="btnReopenAccount_Click" Visible="false"/>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnReopenAccount" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
            <asp:Button ID="btnCloseAccount" runat="server" Text="Clôturer le compte" CssClass="button" /><br />
            <asp:Label ID="litCloseAccountStatus" runat="server" Visible="false" Style="font-size: 0.8em; text-transform: uppercase; font-weight: bold; position: relative; top: -6px; right: 5px;"></asp:Label>
        </div>
    </div>

    <div class="table" style="width: 100%">
        <div class="row">
            <div class="cell" style="vertical-align: middle">
                <h2 style="color: #344b56; text-transform: uppercase; margin-bottom: 10px; margin-top: 10px; white-space: nowrap; position: relative; padding-left: 58px;">
                    <asp:Image ID="imgAccountType" runat="server" />
                    <asp:Label ID="lblAccountType" runat="server"></asp:Label>
                </h2>
            </div>
            <asp:Panel ID="panelDeathStatus" runat="server" CssClass="cell" Style="vertical-align: middle">
                <asp:DeathStatus ID="deathStatus1" runat="server" />
            </asp:Panel>
            <asp:Panel ID="panelScoring" runat="server" CssClass="cell" Style="vertical-align: middle; padding-left: 10px;">
                <asp:Scoring ID="scoring1" runat="server" Type="client" />
            </asp:Panel>
            <asp:Panel ID="panelDriStatus" runat="server" CssClass="cell" Style="vertical-align: middle; padding-left: 10px;">
                <asp:UpdatePanel ID="upDriStatus" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Image ID="imgDriStatus" runat="server" ImageUrl="" CssClass="image-tooltip" ToolTip="DRI" Height="60px" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </asp:Panel>
            <asp:Panel ID="panelClientSurendette" runat="server" CssClass="cell" Style="vertical-align: middle; padding-left: 10px;">
                <asp:UpdatePanel ID="upClientSurendette" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Image ID="imgClientSurendette" runat="server" ImageUrl="" CssClass="image-tooltip" ToolTip="Client surendetté" Height="60px" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </asp:Panel>
            <asp:Panel ID="panelAccountLockLight" runat="server" CssClass="cell" Style="padding-left: 10px; vertical-align: middle">
                <asp:LightLock ID="LightLock1" runat="server" ActionAllowed="true" />
            </asp:Panel>
            <asp:Panel ID="panelBankStatus" runat="server" CssClass="cell" Style="padding-left: 10px; vertical-align: middle">
                <asp:UpdatePanel ID="upBankStatusImage" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Image ID="imgBankStatus" runat="server" ImageUrl="~/Styles/Img/sab-logo.png" CssClass="image-tooltip" ToolTip="Statut SAB" Height="40px" Style="cursor: pointer" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </asp:Panel>
            <asp:Panel ID="panelActivity" runat="server" CssClass="cell" Style="vertical-align: middle">
                <asp:Activity ID="activity1" runat="server" />
            </asp:Panel>
            <asp:Panel ID="panelAnnualFeeStatus" runat="server" CssClass="cell" Style="vertical-align: middle">
                <asp:UpdatePanel ID="upAnnualFeeStatus" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Image ID="imgAnnualFeeStatus" runat="server" ToolTip="Statut renouvellement" Visible="false" CssClass="image-tooltip" Height="40px" Style="margin-left: 10px;" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </asp:Panel>
            <asp:Panel ID="panelKYCUpdateStatus" runat="server" CssClass="cell" Style="vertical-align: middle; padding-left: 10px;">
                <asp:UpdatePanel ID="upKYCUpdateStatus" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Image ID="imgKYCUpdateStatus" runat="server" ImageUrl="" CssClass="image-tooltip" ToolTip="Statut KYC" Height="45px" Visible="true" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </asp:Panel>
            <asp:Panel ID="panelSurveillanceIcon" runat="server" CssClass="cell" Style="vertical-align: middle">
                <asp:UpdatePanel ID="upSurveillanceIcon" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Image ID="imgSurveillanceIcon" runat="server" ImageUrl="~/Styles/Img/surveillance.png" CssClass="image-tooltip" ToolTip="Sous surveillance" Height="40px" Style="cursor: pointer; margin-left: 10px;" onclick="GoToSurveillance();" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </asp:Panel>
            <asp:Panel ID="panelActiveAlert" runat="server" CssClass="cell" Visible="false" Style="vertical-align: middle; padding-left: 10px">
                <asp:Image ID="imgActiveAlert" runat="server" ImageUrl="~/Styles/Img/alert_active.png" CssClass="image-tooltip" Height="40" />
            </asp:Panel>
            <div class="cell" style="padding-left: 10px; vertical-align: middle">
                <asp:ProNotificationCounter ID="proNotificationCounter1" runat="server" />
            </div>
			<asp:Panel ID="panelScoreChurn" runat="server" CssClass="cell" style="padding-left: 10px; vertical-align: middle">
                <asp:ChurnResult ID="churnResult1" runat="server" />
            </asp:Panel>
            <asp:Panel ID="panelRelation" runat="server" CssClass="cell" style="padding-left: 10px; vertical-align: middle">
                <asp:ClientRelation ID="clientRelation1" runat="server" IsPopup="true" />
            </asp:Panel>
            <div class="cell" style="vertical-align: middle; width: 100%">
                <asp:Panel ID="panelRedMail" CssClass="redMail" runat="server" Visible="false" Style="padding-left: 10px;">
                    <img alt="" src="./Styles/Img/red-mail.png" style="height: 30px;" onclick="showRedMailDetails();" />

                    <div id="divRedMail-popup" style="display: none">
                        <div style="max-height: 300px; overflow: auto">
                            <asp:Repeater ID="rptRedMail" runat="server">
                                <HeaderTemplate>
                                    <table class="tableOrange">
                                        <tr>
                                            <th>N&deg; inscription</th>
                                            <th>Nom</th>
                                            <th>Prénom</th>
                                            <th>Date création</th>
                                            <th>Profil AML</th>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr class='<%# Container.ItemIndex % 2 == 0 ? "aml-table-row" : "aml-table-alternate-row" %>' onclick='showClientDetails("<%# Eval("RegistrationCode")%>")'>
                                        <td><%# Eval("RegistrationCode")%></td>
                                        <td><%# Eval("LastName")%></td>
                                        <td><%# Eval("FirstName")%></td>
                                        <td><%# Eval("CreationDate")%></td>
                                        <td style="text-align: center"><%# Eval("AMLProfile")%></td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </asp:Panel>
            </div>
        </div>
    </div>
    <div class="table" style="margin: 10px 0 20px 0; width: 100%;">
        <div class="row">
            <div class="cell" style="vertical-align: top; width: 250px">
                <div style="font-weight: bold; color: #f57527">Titulaire principal</div>
                <div>
                    <asp:Label ID="lblClientName" runat="server"></asp:Label>
                </div>
            </div>
            <div class="cell" style="vertical-align: top; padding-left: 20px; width: 250px">
                <div class="table">
                    <div class="row">
                        <div class="cell" style="vertical-align:top">
                            <div style="font-weight: bold; color: #f57527">N&deg; Carte</div>
                            <div>
                                <asp:Label ID="lblClientCardNumber" runat="server"></asp:Label>
                            </div>
                        </div>
                        <asp:Panel ID="panelNickelChrome" runat="server" CssClass="cell nickel-chrome-panel" style="padding-left:5px;" onclick="initNickelChromeData();">
                            <script type="text/javascript">

                                function showNickelChrome() {
                                    $('#popup-NickelChrome').dialog({
                                        autoOpen: true,
                                        resizable: false,
                                        draggable: false,
                                        modal: true,
                                        width: 'auto',
                                        //zIndex: 99997,
                                        title: "Client Nickel Chrome",
                                        //buttons: [{ text: 'Fermer', click: function () { $(this).dialog('destroy'); } }]
                                    });
                                    $("#popup-NickelChrome").parent().appendTo(jQuery("form:first"));
                                }
                                function NickelChromeAddressValid() {
                                    $('#nickel-chrome-check-phone').slideDown();
                                }
                                function NickelChromeModifyAddress() {
                                    $('#popup-NickelChrome').dialog('close');
                                    $('#popup-NickelChrome').dialog('destroy');
                                    $('html,body').animate({
                                    scrollTop: $("#divAddress").offset().top},
                                    'slow');
                                }
                                function NickelChromePhoneValid() {
                                    $('#nickel-chrome-reorder').slideDown();
                                }
                                function NickelChromeModifyPhone() {
                                    $('#popup-NickelChrome').dialog('close');
                                    $('#popup-NickelChrome').dialog('destroy');
                                    $('html,body').animate({
                                        scrollTop: $("#divContact").offset().top},
                                        'slow');
                                }
                                function NickelChromeOrderDone() {
                                    $('#popup-NickelChrome').dialog('close');
                                    $('#popup-NickelChrome').dialog('destroy');
                                    
                                }
                            </script>
                            <style type="text/css">
                                .nickel-chrome-panel:hover{
                                    cursor:pointer;
                                }

                                .btnNickelChromeStepModify, .btnNickelChromeStepValid {
                                    margin:0 3px 10px 0;
                                    font-size:14px;
                                    font-weight:bold;
                                    font-family:Arial, Verdana;
                                    padding:10px;
                                    display:inline-block;
                                    background-color:#fff;
                                    text-align:center;
                                    color:#f57527;
                                    border: 2px #f57527 solid;
                                    text-transform:uppercase;
                                    text-decoration:none;
                                    -moz-border-radius:8px;
                                    border-radius:8px;
                                    box-shadow: 0 0 5px #a9a9a9;
                                }
                                .btnNickelChromeStepModify:hover, .btnNickelChromeStepValid:hover {
                                    cursor:pointer;
                                }
                            </style>
                            <div style="position:relative; width:80px;">
                                <div style="position:absolute; top:-5px;">
                                    <img alt="nickel-chrome" src="Styles/Img/nickel-chrome-logo.png" height="50" class="image-tooltip" title="N&deg; Assistance : 01 41 85 93 18<br/>N&deg; Assurance : 01 41 85 93 31" />
                                    <asp:UpdatePanel ID="upNickelChromeStatus" runat="server">
                                        <ContentTemplate>
                                            <asp:Button ID="btnInitNickelChromeData" runat="server" style="display:none" OnClick="btnInitNickelChromeData_Click" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                            <div style="display:none" id="popup-NickelChrome">
                                
                                <asp:UpdatePanel ID="upNickelChromeInformations" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:Panel ID="panelNickelChromeInformation" runat="server" CssClass="table" style="width:100%;">
                                            <div class="table" style="">
                                                <div class="row">
                                                    <div class="cell">
                                                        <b class="font-orange">N&deg; Assistance : </b>
                                                    </div>
                                                    <div class="cell" style="padding-left:10px">
                                                        01 41 85 93 18
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="cell">
                                                        <b class="font-orange">N&deg; Assurance : </b>
                                                    </div>
                                                    <div class="cell" style="padding-left:10px">
                                                        01 41 85 93 31
                                                    </div>
                                                </div>
                                            </div>
                                            <div style="margin-top:20px;">
                                                <b class="font-orange">Carte commandée le <asp:Label ID="lblNickelChromeOrderDate" runat="server">??/??/????</asp:Label></b>
                                            </div>
                                            <asp:Panel ID="panelNickelChromeCancelPossible" runat="server" Visible="true" style="margin-top:5px;text-align:center">
                                                <asp:Button ID="btnNickelChromeCancel" runat="server" CssClass="MiniButton" Text="Annuler et procéder à la demande de remboursement" OnClick="btnNickelChromeCancel_Click" />
                                                <asp:Label ID="lblNickelChromeRefundStatus" runat="server" Visible="false" style="margin-top:10px;"></asp:Label>
                                            </asp:Panel>
                                            <div style="width:100%; margin-top:20px;">
                                                <div style="">
                                                    <b class="font-orange">Modèle choisi</b>
                                                </div>
                                                <div style="text-align:center; margin-top:10px">
                                                    <asp:Image ID="imgClientNickelChromeCard" runat="server" ImageUrl="~/Styles/Img/nickel-chrome/Nickel-Chrome-orange.png" Width="200" />
                                                </div>
                                            </div>
                                            <asp:Panel ID="panelNickelChromeReorderPossible" runat="server" Visible="false" style="margin-top:20px;text-align:center">
                                                <asp:Button ID="btnNickelChromeShowReorder" runat="server" CssClass="button" Text="Demander un renvoi automatique" OnClick="btnNickelChromeShowReorder_Click" />
                                            </asp:Panel>
                                        </asp:Panel>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="btnNickelChromeShowReorder" EventName="Click" />
                                        <asp:AsyncPostBackTrigger ControlID="btnNickelChromeCancel" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                                <asp:UpdatePanel ID="upNickelChromeReorder" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:Panel ID="panelNickelChromeReorder" runat="server" Visible="false">
                                            <div id="nickel-chrome-check-address">
                                                <div style="margin-top:20px;">
                                                    <b class="font-orange">Je vérifie son adresse</b>
                                                </div>
                                                <div style="text-align: center;">
                                                    <asp:Label ID="lblNickelChromeClientAddress" runat="server"></asp:Label>
                                                    <br /><br />
                                                    <div class="btnNickelChromeStepValid" onclick="NickelChromeAddressValid()">Je valide<br />
                                                        son
                                                        adresse</div>
                                                    <div class="btnNickelChromeStepModify" onclick="NickelChromeModifyAddress();">
                                                        Le client a <br />
                                                        changé d'adresse</div>
                                                </div>
                                            </div>
                                            <div id="nickel-chrome-check-phone" style="display:none">
                                                <div class="step-title" style="margin-top: 20px">
                                                    <b class="font-orange">Je vérifie son numéro de téléphone</b>
                                                </div>
                                                <div style="text-align: center;">
                                                    <asp:Label ID="lblNickelChromeClientPhone" runat="server" Style="font-size: 20px"></asp:Label>
                                                    <br /><br />
                                                    <div class="btnNickelChromeStepValid" onclick="NickelChromePhoneValid()">Je valide<br />
                                                        son
                                                        téléphone</div>
                                                    <div class="btnNickelChromeStepModify" onclick="NickelChromeModifyPhone();">
                                                        Le client a <br />
                                                        changé de numéro</div>
                                                </div>
                                            </div>
                                            <div id="nickel-chrome-reorder" style="display:none; margin-top:20px;">
                                                <asp:Button ID="btnNickelChromeReorder" runat="server" Text="Commander" CssClass="button" OnClick="btnNickelChromeReorder_Click" style="height:auto; width:100%" />
                                            </div>
                                        </asp:Panel>
                                        <asp:Panel ID="panelNickelChromeOrderResult" runat="server" Visible="false">
                                            <div style="text-align:center; margin-top:10px; font-weight:bold">
                                                <asp:Label ID="lblNickelChromeOrderMessage" runat="server">Commande effectuée</asp:Label>
                                            </div>
                                            <div style="margin-top:20px;">
                                                <input type="button" class="button" style="height:auto;width:100%" value="Terminer" onclick="NickelChromeOrderDone();" />
                                                <asp:Button ID="btnNickelChromeReorderDone" runat="server" Text="Terminer" CssClass="button" OnClick="btnNickelChromeReorderDone_Click" style="display:none;" />
                                            </div>
                                        </asp:Panel>
                                        
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="btnNickelChromeReorderDone" EventName="Click" />
                                        <asp:AsyncPostBackTrigger ControlID="btnNickelChromeReorder" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                        </asp:Panel>
                    </div>
                </div>
            </div>
            <div class="cell" style="padding-left: 20px; vertical-align: top;">
                <div style="min-width:100px;font-weight: bold; color: #f57527">N&deg; Compte</div>
                <div>
                    <asp:Label ID="lblClientAccountNumber" runat="server"></asp:Label>
                </div>
            </div>
            <div class="cell" style="padding-left:20px; vertical-align: top; width: 250px">
                <div style="font-weight: bold; color: #f57527">Inscrit le</div>
                <div>
                    <asp:Label ID="lblRegistrationDate" runat="server"></asp:Label>
                </div>
            </div>
            <div class="cell" style="padding-left: 20px; vertical-align: top; visibility: hidden">
                <div style="font-weight: bold; color: #f57527">N&deg; Inscription</div>
                <div>
                    <asp:Label ID="lblClientRegistrationNumber" runat="server" style="display:none"></asp:Label>
                </div>
            </div>
            <div class="cell" style="text-align: right">
                <input id="btnRegistration" runat="server" type="button" class="MiniButton" value="accéder au dossier d'inscription" style="width: 200px; position: relative; right: 0; top: 10px;" />
            </div>
        </div>
    </div>

    <asp:Repeater ID="rptParentAccounts" runat="server">
        <HeaderTemplate>
            <div class="table" style="margin-bottom: 20px; width: 100%">
                <div class="row">
                    <div class="cell" style="width: 250px">
                        <div style="font-weight: bold; color: #f57527">Représentant légal</div>
                    </div>
                    <div class="cell" style="vertical-align: top; padding-left: 20px; width: 110px">
                        <div style="font-weight: bold; color: #f57527">N&deg; Carte</div>
                    </div>
                    <div class="cell" style="vertical-align: top; padding-left: 20px;">
                        <div style="font-weight: bold; color: #f57527">N&deg; Compte</div>
                    </div>
                    <div class="cell" style="padding-left: 20px; vertical-align: top; visibility: hidden">
                        <div style="font-weight: bold; color: #f57527">N&deg; Inscription</div>
                        <div>
                            <asp:Label ID="lblClientRegistrationNumberParent" runat="server" style="display:none"></asp:Label>
                        </div>
                    </div>
                    <div class="cell"></div>
                    <div class="cell"></div>
                </div>
        </HeaderTemplate>
        <ItemTemplate>
            <div class="row">
                <div class="cell" style="vertical-align: top;">
                    <%#Eval("name") %>
                </div>
                <div class="cell" style="vertical-align: top; padding-left: 20px;">
                    <%#Eval("cardnumber") %>
                </div>
                <div class="cell" style="vertical-align: top; padding-left: 20px;">
                    <%#Eval("accountnumber") %>
                </div>
                <div class="cell">
                </div>
                <div class="cell"></div>
                <div class="cell" style="text-align: right; padding-bottom: 4px">
                    <input type="button" value="accéder à la fiche du client" onclick="document.location.href='ClientDetails.aspx?ref=<%#Eval("ref") %>    '" class="MiniButton" style="width: 200px; position: relative; right: 0; top: -5px;" />
                </div>
            </div>
        </ItemTemplate>
        <FooterTemplate>
            </div>
        </FooterTemplate>
    </asp:Repeater>

    <asp:Repeater ID="rptAccounts1218" runat="server">
        <HeaderTemplate>
            <div class="table" style="margin-bottom: 20px; width: 100%">
                <div class="row">
                    <div class="cell" style="width: 250px">
                        <div style="font-weight: bold; color: #f57527">Mineurs associés</div>
                    </div>
                    <div class="cell" style="vertical-align: top; padding-left: 20px; width: 110px">
                        <div style="font-weight: bold; color: #f57527">N&deg; Carte</div>
                    </div>
                    <div class="cell" style="vertical-align: top; padding-left: 20px;">
                        <div style="font-weight: bold; color: #f57527">N&deg; Compte</div>
                    </div>
                    <div class="cell" style="padding-left: 20px; vertical-align: top; visibility: hidden">
                        <div style="font-weight: bold; color: #f57527">N&deg; Inscription</div>
                        <div>
                            <asp:Label ID="lblClientRegistrationNumber1218" runat="server" style="display:none"></asp:Label>
                        </div>
                    </div>
                    <div class="cell"></div>
                    <div class="cell"></div>
                </div>
        </HeaderTemplate>
        <ItemTemplate>
            <div class="row">
                <div class="cell" style="vertical-align: top;">
                    <%#Eval("name") %>
                </div>
                <div class="cell" style="vertical-align: top; padding-left: 20px;">
                    <%#Eval("cardnumber") %>
                </div>
                <div class="cell" style="vertical-align: top; padding-left: 20px;">
                    <%#Eval("accountnumber") %>
                </div>
                <div class="cell">
                </div>
                <div class="cell"></div>
                <div class="cell" style="text-align: right; padding-bottom: 4px">
                    <input type="button" value="accéder à la fiche du client" onclick="document.location.href='ClientDetails.aspx?ref=<%#Eval("ref") %>    '" class="MiniButton" style="width: 200px; position: relative; right: 0; top: -5px;" />
                </div>
            </div>
        </ItemTemplate>
        <FooterTemplate>
            </div>
        </FooterTemplate>
    </asp:Repeater>

    <div id="divNbAlert" class="alertPastille" style="display: none; position: absolute; top: -15px; right: -15px">
        <div style="width: 30px; height: 30px; text-align: center; line-height: 30px; background: url(Styles/Img/red_circle.png) no-repeat center">
            <%--<img id="imgNbAlert" alt="" src="Styles/Img/red_circle.png" style="position:absolute; top:0; left:0; z-index:1;width:30px; height:30px;" />--%>
            <%--<div style="position:absolute; top:0; left:0; z-index:2; display:inline; width:30px; height:30px; vertical-align:middle; text-align:center; padding-top:6px">--%>
            <label id="lblNbAlert" style="font-size: 12px; font-weight: bold; color: #fff;"></label>
            <%--</div>--%>
        </div>
    </div>

	<div id="divDocsAlert" class="alertPastille" style="display:none;position:absolute;top:-15px;right:-15px">
        <div style="width:30px;height:30px;text-align:center;line-height:30px;background:url(Styles/Img/red_circle.png) no-repeat center">
            <label id="lblDocsAlert" style="font-size:12px; font-weight:bold; color:#fff;">!</label>
        </div>
    </div>

    <div style="border: 3px solid #344b56; border-radius: 8px; width: 100%; margin: auto;margin-bottom:20px; text-transform:uppercase" class="ClientDetails">
		<asp:Repeater ID="rptTabs" runat="server">
            <HeaderTemplate>
                <ul>
            </HeaderTemplate>
            <ItemTemplate>
                <%# Eval("tab") %>
            </ItemTemplate>
            <FooterTemplate>
                </ul>
            </FooterTemplate>
        </asp:Repeater>

        <asp:Panel ID="panelPersonalInfo" runat="server">
            <h4 id="PersonalInfoTitle" class="AccordionTitle" style="display: none">
                <img id="PersonalInfoArrow" alt="" src="" />
                Informations personnelles
            </h4>

            <div id="PersonalInfoPanel" style="padding: 10px; margin-bottom: 10px; display: none">

                <asp:UpdatePanel ID="upPersonalInfo" runat="server">
                    <ContentTemplate>
                        <div class="table" style="width: 100%; border-collapse: collapse">
                            <div class="row">
                                <div class="cell" style="width: 33%">
                                    <div class="label">
                                        <asp:Label ID="lblCivility" runat="server" AssociatedControlID="rblCivility">Civilité</asp:Label>
                                    </div>
                                    <div>
                                        <asp:RadioButtonList ID="rblCivility" runat="server" RepeatDirection="Horizontal" CssClass="RadioButtonList">
                                            <asp:ListItem Text="M." Value="MR"></asp:ListItem>
                                            <asp:ListItem Text="Mme" Value="MME"></asp:ListItem>
                                            <asp:ListItem Text="Mlle" Value="MLLE"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                                <div class="cell" style="width: 33%">
                                </div>
                                <div class="cell" style="width: 33%">
                                </div>
                            </div>
                            <div class="row">
                                <div class="cell" style="width: 33%">
                                    <div class="label">
                                        <asp:Label ID="lblFirstName" runat="server" AssociatedControlID="txtFirstName">Prénom</asp:Label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtFirstName" runat="server" Style="width: 90%; text-transform: uppercase"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="cell" style="width: 33%">
                                    <div class="label">
                                        <asp:Label ID="lblLastName" runat="server" AssociatedControlID="txtLastName">Nom</asp:Label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtLastName" runat="server" Style="width: 90%; text-transform: uppercase"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="cell">
                                    <div class="label">
                                        <asp:Label ID="lblNewLastName" runat="server" AssociatedControlID="txtNewLastName">Nom d'usage</asp:Label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtNewLastName" runat="server" Style="width: 90%; text-transform: uppercase"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="cell">
                                    <div class="label">
                                        <asp:Label ID="lblMarriedLastName" runat="server" AssociatedControlID="txtMarriedLastName">Nom marital</asp:Label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtMarriedLastName" runat="server" Style="width: 90%; text-transform: uppercase"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="cell">
                                    <div id="divMaidenName" style="display: none">
                                        <div class="label">
                                            <asp:Label ID="lblMaidenName" runat="server" AssociatedControlID="txtMaidenName">Nom de jeune fille</asp:Label>
                                        </div>
                                        <div>
                                            <asp:TextBox ID="txtMaidenName" runat="server" Style="width: 90%; text-transform: uppercase"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="cell" style="width: 33%; vertical-align: top">
                                </div>
                            </div>
                            <div class="row">
                                <div class="cell" style="width: 33%; vertical-align: top">
                                    <div class="label">
                                        <asp:Label ID="lblBirthDate" runat="server" AssociatedControlID="txtBirthDate">Date de naissance</asp:Label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtBirthDate" runat="server" Style="width: 90%"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="cell">
                                </div>
                                <div class="cell">
                                </div>
                            </div>

                        </div>
                        <div class="label">
                            <asp:Label ID="lblBirthPlace" runat="server" AssociatedControlID="txtBirthDate">Lieu de naissance</asp:Label>
                            <script type="text/javascript">
                                $(document).ready(function(){
                                    ManageBirthPlace();
                                });
                                function ManageBirthPlace() {
                                    var BirthCountry = $('#<%=ddlBirthCountry.ClientID%>').val();
                                    $('.birthDepartment').hide();
                                    $('.birthCity').hide();

                                    //console.log(BirthCountry);
                                    //GP 971 Guadeloupe
                                    //MQ 972 Martinique
                                    //GF 973 Guyane
                                    //RE 974 La Réunion
                                    //PM 975 St-Pierre-et-Miquelon
                                    //YT 976 Mayotte
                                    switch(BirthCountry) {
                                        case "FR":
                                            InitFrDepartmentAndCitiesAutocomplete();
                                            break;
                                        case "GP":
                                            InitFrDepartmentAndCitiesAutocomplete('971');
                                            break
                                        case "MQ":
                                            InitFrDepartmentAndCitiesAutocomplete('972');
                                            break;
                                        case "GF":
                                            InitFrDepartmentAndCitiesAutocomplete('973');
                                            break;
                                        case "RE":
                                            InitFrDepartmentAndCitiesAutocomplete('974');
                                            break;
                                        case "PM":
                                            InitFrDepartmentAndCitiesAutocomplete('975');
                                            break;
                                        case "YT":
                                            InitFrDepartmentAndCitiesAutocomplete('976');
                                            break;
                                        default:
                                            $('.birthCity').show();
                                            break;
                                    }
                                }
                                function InitFrDepartmentAndCitiesAutocomplete(dep){
                                    $('.birthDepartment').show();
                                    $('.birthCity').hide();

                                    if(dep != null && dep.length > 0){
                                        $('.birthDepartment').hide();
                                        $('#<%=txtBirthDepartment.ClientID%>').val(dep);
                                        $('.birthCity').show();
                                    }
                                    else {
                                        InitFrDepartmentAutocomplete();
                                        if($('#<%=txtBirthDepartment.ClientID%>').val().trim().length >= 2)
                                            $('.birthCity').show();
                                    }
                                }

                                var arDepartmentList = new Array();
                                function InitFrDepartmentAutocomplete() {
                                    $('#<%=txtBirthDepartment.ClientID%>').autocomplete({
                                        source: function (request, response) {
                                            $.ajax({
                                                url: "./ws_tools.asmx/GetDepartmentList",
                                                data: '{ "prefix": "'+request.term+'"}' , //, culture: 'fr-FR', order: 'ASC'
                                                dataType: "json",
                                                type: "POST",
                                                contentType: "application/json; charset=utf-8",
                                                dataFilter: function (data) { return data; },
                                                success: function (data) {
                                                    arDepartmentList = new Array();

                                                    response(

                                                        $.map(data.d, function (item) {
                                                            //console.log(item);
                                                            arDepartmentList.push(item.substring(0, item.indexOf('(')).trim());
                                                            return { value: item }
                                                        })
                                                    )
                                                },
                                                error: function (XMLHttpRequest, textStatus, errorThrown) {
                                                    alert(textStatus + ", " + errorThrown);
                                                }
                                            });
                                        },
                                        minLength: 0,
                                        close: function () {
                                        },
                                        select: function( event, ui ){
                                            setTimeout(function(){
                                                var sThirdChar = ui.item.value.charAt(2);
                                                if($.isNumeric(sThirdChar))
                                                    $('#<%=txtBirthDepartment.ClientID%>').val(ui.item.value.substring(0,3));
                                                else
                                                    $('#<%=txtBirthDepartment.ClientID%>').val(ui.item.value.substring(0,2));

                                                $('.birthCity').show();
                                                $('#<%=txtBirthCity.ClientID%>').val("").focus();
                                            },100);

                                        }
                                    });
                                }

                                var arCityList = new Array();
                                function InitFrCitiesAutocomplete() {
                                    var dep = "";
                                    dep = $('#<%=txtBirthDepartment.ClientID%>').val();
                                        var BirthCountry = $('#<%=ddlBirthCountry.ClientID%>').val();
                                        $('#<%=txtBirthCity.ClientID%>').autocomplete({disabled: true});
                                        if(dep != null && dep.length >= 2){
                                            switch(BirthCountry){
                                                case "FR":
                                                case "GP":
                                                case "MQ":
                                                case "GF":
                                                case "RE":
                                                case "PM":
                                                case "YT":
                                                    $('#<%=txtBirthCity.ClientID%>').autocomplete({
                                                        disabled: false,
                                                        source: function (request, response) {
                                                            $.ajax({
                                                                url: "./ws_tools.asmx/GetCityList",
                                                                data: '{ "prefix": "'+request.term+'", "dep": "'+dep+'"}' , //, culture: 'fr-FR', order: 'ASC'
                                                                dataType: "json",
                                                                type: "POST",
                                                                contentType: "application/json; charset=utf-8",
                                                                dataFilter: function (data) { return data; },
                                                                success: function (data) {
                                                                    arCityList = new Array();
                                                                    response(
                                                                        $.map(data.d, function (item) {
                                                                            arCityList.push(item.substring(0,item.indexOf('(')).trim());
                                                                            //console.log(item);
                                                                            return {
                                                                                value: item
                                                                            }
                                                                        })
                                                                    )
                                                                },
                                                                error: function (XMLHttpRequest, textStatus, errorThrown) {
                                                                    alert(textStatus + ", " + errorThrown);
                                                                }
                                                            });
                                                        },
                                                        minLength: 1,
                                                        select: function (event, ui) {
                                                            setTimeout(function(){
                                                                $('#<%=txtBirthCity.ClientID%>').val(ui.item.value.substring(0,ui.item.value.indexOf('(')).trim());
                                                            $('#<%=hfBirthZipcode.ClientID%>').val(ui.item.value.substring(ui.item.value.indexOf('(')+1,ui.item.value.indexOf(')')).trim());
                                                            //console.log($('#<%=hfBirthZipcode.ClientID%>').val());
                                                        },100);
                                                    },
                                                        open: function () {
                                                        },
                                                        close: function () {
                                                        }
                                                    });
                                                break;
                                        }
                                    }
                                }
                            </script>
                        </div>
                        <div style="background-color: #ECECEC; padding: 10px">
                            <div class="table" style="width: 100%; border-collapse: collapse;">
                                <div class="row">
                                    <div class="cell" style="width: 33%">
                                        <div class="label">
                                            <asp:Label ID="lblBirthCountry" runat="server" AssociatedControlID="ddlBirthCountry">Pays</asp:Label>
                                        </div>
                                        <div>
                                            <asp:DropDownList ID="ddlBirthCountry" runat="server" DataTextField="CountryName" DataValueField="ISO2" Style="width: 250px" onchange="ManageBirthPlace();" AppendDataBoundItems="true">
                                                <asp:ListItem Value=""></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="cell birthDepartment" style="display: none">
                                        <div class="label">
                                            <asp:Label ID="lblBirthDepartment" runat="server" AssociatedControlID="txtBirthDepartment">Département</asp:Label>
                                        </div>
                                        <div>
                                            <asp:TextBox ID="txtBirthDepartment" runat="server" Style="width: 50px;" MaxLength="3"></asp:TextBox>
                                            <asp:HiddenField ID="hfBirthFrDepartment" runat="server" />
                                        </div>
                                    </div>
                                    <div class="cell birthCity">
                                        <div class="label">
                                            <asp:Label ID="lblBirthCity" runat="server" AssociatedControlID="txtBirthCity">Ville</asp:Label>
                                        </div>
                                        <div>

                                            <asp:TextBox ID="txtBirthCity" runat="server" Style="width: 90%; text-transform: uppercase;" onfocus="InitFrCitiesAutocomplete();"></asp:TextBox>
                                            <asp:HiddenField ID="hfBirthFrCity" runat="server" />
                                            <asp:HiddenField ID="hfBirthZipcode" runat="server" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="table" style="width: 100%; color: #344b56; font-weight: bold; text-transform: uppercase; border-bottom: 1px solid #344b56; margin-top: 30px">
                            <div class="table-row">
                                <div class="table-cell" style="vertical-align: bottom; width: 12%;">
                                    KYC
                                    <span id="lblCheckKYCLink" runat="server" class="image-tooltip" style="cursor: default; text-transform: none; font-weight: normal; font-size: 0.9em">[Etat :
                                        <asp:Image ID="imgCheckKYCStatus" runat="server" ImageUrl="~/Styles/Img/gray_status.png" Style="position: relative; top: 2px" />]</span>
                                </div>
                                <div class="table-cell" style="padding: 0 10px">
                                    <asp:Panel ID="panelAskKYCUpdate" runat="server" Visible="false">
                                        <asp:UpdatePanel ID="upButtonAskKycUpdate" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:Panel ID="panelAskKYCUpdateButton" runat="server">
                                                    <input type="button" class="MiniButton" value="demander mise à jour KYC" onclick="ConfirmAction('AskKycUpdate')" style="font-size: 13px; padding: 0 10px;" />
                                                </asp:Panel>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </asp:Panel>
                                </div>
                                <div class="table-cell" style="width: 50%; text-align: right; padding-bottom: 5px;">
                                    <asp:Panel ID="panelKYCUpdate" runat="server" Visible="true">
                                        <asp:UpdatePanel ID="upButtonKycUpdate" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:Panel ID="panelKYCUpdateButton" runat="server">
                                                    <asp:Button ID="btnKycUpdate" runat="server" CssClass="MiniButton" Text="mettre à jour (par téléphone)" OnClick="btnKycUpdate_Click" style="font-size:13px;padding:0 10px;" />
                                                </asp:Panel>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>
                        <div id="divKYC">
                            <div style="width: 100%; text-align: left; padding: 10px;">
                            </div>
                            <style type="text/css">
                                input[readonly], input[disabled], select[disabled] {
                                    background-color: #fff;
                                    color: #808080;
                                }
                                .KycConditions {
                                        text-align: justify;
                                        text-transform: none;
                                        line-height: normal;
                                        font-size: 0.8em;
                                    }
                            </style>
                            <div style="width: 50%;">
                                <div class="label">
                                    <asp:Label ID="lblKYCnationality" runat="server" AssociatedControlID="ddlKYCnationality">Nationalité</asp:Label>
                                </div>
                                <div>
                                    <asp:DropDownList ID="ddlKYCnationality" runat="server" DataTextField="Label" DataValueField="Value" Style="width: 90%">
                                        <asp:ListItem Value=""></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div style="width: 50%">
                                <div class="label">
                                    <asp:Label ID="lblKYCmat" runat="server" AssociatedControlID="ddlKYCmat">Situation conjugale</asp:Label>
                                </div>
                                <div>
                                    <asp:DropDownList ID="ddlKYCmat" runat="server" DataTextField="Label" DataValueField="Value" Style="width: 90%">
                                        <asp:ListItem Value=""></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div style="background-color: #ECECEC; padding: 10px; margin-top: 5px;">
                                <div class="table" style="width: 100%; border-collapse: collapse; text-transform: uppercase">
                                    <div class="row">
                                        <div class="cell" style="width: 50%">
                                            <div class="label">
                                                <asp:Label ID="lblKYCpro" runat="server" AssociatedControlID="ddlKYCpro">Situation professionnelle</asp:Label>
                                            </div>
                                            <div>
                                                <asp:DropDownList ID="ddlKYCpro" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlKYCpro_SelectedIndexChanged"
                                                    DataTextField="Label" DataValueField="Value" Style="width: 90%">
                                                    <asp:ListItem Value=""></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="cell" style="width: 50%">
                                            <asp:Panel ID="panelKYCincome" runat="server" Visible="true">
                                                <div class="label">
                                                    <asp:Label ID="lblKYCincome" runat="server" AssociatedControlID="ddlKYCincome">Revenus mensuels</asp:Label>
                                                </div>
                                                <div>
                                                    <asp:DropDownList ID="ddlKYCincome" runat="server" DataTextField="Label" DataValueField="Value" Style="width: 90%">
                                                        <asp:ListItem Value=""></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </div>
                                <div class="table" style="width: 100%; border-collapse: collapse; text-transform: uppercase">
                                    <div class="row">
                                        <div class="cell" style="width: 50%">
                                            <asp:Panel ID="panelKYCproPlus" runat="server" Visible="false">
                                                <div class="label">
                                                    <asp:Label ID="lblKYCproPlus" runat="server" AssociatedControlID="ddlKYCproPlus">Secteur professionnel</asp:Label>
                                                </div>
                                                <div>
                                                    <asp:DropDownList ID="ddlKYCproPlus" runat="server" DataTextField="Label" DataValueField="Value" Style="width: 90%">
                                                        <asp:ListItem Value=""></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                        <div class="cell" style="width: 50%">
                                            <asp:Panel ID="panelKYCjob" runat="server" Visible="false">
                                                <div class="label">
                                                    <asp:Label ID="lblKYCjob" runat="server" AssociatedControlID="txtKYCjob">Profession</asp:Label>
                                                </div>
                                                <div>
                                                    <asp:TextBox ID="txtKYCjob" runat="server" Style="width: 90%"></asp:TextBox>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="background-color: #ECECEC; padding: 10px; margin-top: 5px;">
                                <div class="table" style="width: 100%; border-collapse: collapse; text-transform: uppercase">
                                    <div class="row">
                                        <div class="cell" style="width: 50%">
                                            <div class="label">
                                                <asp:Label ID="lblKYChab" runat="server" AssociatedControlID="ddlKYChab">Situation patrimoniale</asp:Label>
                                            </div>
                                            <div>
                                                <asp:DropDownList ID="ddlKYChab" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlKYChab_SelectedIndexChanged"
                                                    DataTextField="Label" DataValueField="Value" Style="width: 90%">
                                                    <asp:ListItem Value=""></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="cell" style="width: 50%">
                                            <asp:Panel ID="panelKYCestate" runat="server" Visible="true">
                                                <div class="label">
                                                    <asp:Label ID="lblKYCestate" runat="server" AssociatedControlID="ddlKYCestate">Valeur patrimoniale</asp:Label>
                                                </div>
                                                <div>
                                                    <asp:DropDownList ID="ddlKYCestate" runat="server" DataTextField="Label" DataValueField="Value" Style="width: 90%">
                                                        <asp:ListItem Value=""></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </div>
                                <asp:Panel ID="panelKYCPropertyStatusPlus" runat="server" Visible="false" Style="width: 50%">
                                    <div class="label">
                                        <asp:Label ID="lblKYCPropertyStatusPlus" runat="server" AssociatedControlID="txtKYCPropertyStatusPlus">Chez Mr ou Mme</asp:Label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtKYCPropertyStatusPlus" runat="server" Style="width: 90%"></asp:TextBox>
                                    </div>
                                </asp:Panel>
                            </div>
                            <div style="background-color: #ECECEC; padding: 10px; margin-top: 5px;">
                                <div>
                                    <div class="label">
                                        <asp:Label ID="lblKYCaccountUsageTag" runat="server" AssociatedControlID="cblKYCaccountUsageTag">Usage du compte</asp:Label>
                                    </div>
                                    <div>
                                        <asp:CheckBoxList ID="cblKYCaccountUsageTag" runat="server" DataTextField="Label" DataValueField="Value" Style="width: 90%">
                                        </asp:CheckBoxList>
                                    </div>
                                </div>
                                <div style="margin-top: 10px; width: 50%">
                                    <div class="label">
                                        <asp:Label ID="lblKYCCNusage" runat="server" AssociatedControlID="ddlKYCCNusage">Mon Compte-Nickel</asp:Label>
                                    </div>
                                    <div>
                                        <asp:DropDownList ID="ddlKYCCNusage" runat="server" DataTextField="Label" DataValueField="Value" Style="width: 90%">
                                            <asp:ListItem Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
							<asp:Panel ID="panelKycTaxResidenceOld" runat="server" Visible="false" style="background-color:#ECECEC; padding:10px; margin-top:5px;">
                                <div style="width:50%">
									<div class="label">
                                        <asp:Label ID="lblKYCTaxResidence" runat="server" AssociatedControlID="ddlKYCTaxResidence">Résidence fiscale</asp:Label>
                                    </div>
                                    <div>
                                        <asp:DropDownList ID="ddlKYCTaxResidence" runat="server" DataTextField="Label" DataValueField="Value" Style="width: 90%">
                                            <asp:ListItem Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </asp:Panel>
                            <div style="margin-top:5px;">
                                <div class="table">
                                    <div class="row">
                                        <div class="cell">
                                            <input type="checkbox" id="cbKYCCertificate" runat="server" />
                                        </div>
                                        <div class="cell">
                                            <label for="<%=cbKYCCertificate.ClientID %>" style="font-size: 1em; line-height: normal;">
                                                <p class="KycConditions">
                                                    J'atteste que les informations figurant sont exactes et complètes.<br />
                                                    Je m'engage à informer Compte-Nickel de tout changement de situation fiscale dans les 90 jours suivant le changement.
                                                </p>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div style="margin-top:10px; width:100%; text-align:right">
                                <asp:Panel ID="panelUpdateFatcaAeoi" runat="server">
                                    <label class="MiniButton" for="fuFatcaAutoCertification" style="font-size:0.8em; text-transform:none; padding:0 5px;">Mettre à jour manuellement les données fiscales FATCA/AEOI</label>
                                    
                                    <div style="height:15px;">
                                        <label id="lblFatcaAutoCertificationToUpload" style="font-size:0.8em;text-transform:none"></label>
                                    </div>
                                    <asp:Button ID="btnEditTaxResidence_Hidden" runat="server" style="display:none" OnClick="btnEditTaxResidence_Click" />
                                </asp:Panel>
                                <asp:Panel ID="panelCancelFatcaAeoi" runat="server" Visible="false">
                                    <asp:Button ID="btnEditTaxResidence" runat="server" Text="Annuler la mise à jour de la résidence fiscale" class="MiniButton" OnClick="btnEditTaxResidence_Click" />
                                </asp:Panel>
                                <div style="margin-top:10px;text-align:justify;text-transform:none; font-size:0.8em; color:red">
                                    Mettre à jour manuellement l'auto-certification nécessite obligatoirement l'upload du scan du formulaire papier envoyé par le client signé et accompagné des pièces justificatives, à minima copie de pièce d'identité
                                </div>
                                <asp:HiddenField ID="hfFatcaAutoCertificationFilename" runat="server" />
                                <input id="fuFatcaAutoCertification" type="file" class="input-file" onchange="checkFileToUpload('fuFatcaAutoCertification', 'FatcaAutoCertification')" accept="image/jpg,image/jpeg,application/pdf" />

                                <script type="text/javascript">
                                    function checkFileToUpload(fileUploadID, type) {
                                        try {
                                            var isExtOK = false;

                                            if (fileUploadID != null && fileUploadID.trim().length > 0 && $('#' + fileUploadID) != null &&
                                                type != null && type.trim().length > 0) {

                                                var file = $('#' + fileUploadID)[0].files[0];
                                                var fileType = file.type;
                                                //console.log(fileType);
                                                switch (fileType) {
                                                    case 'image/jpg':
                                                    case 'image/jpeg':
                                                    case 'application/pdf':
                                                        isExtOK = true;
                                                        break;
                                                    default:
                                                        AlertMessage("Erreur", "Type de document non pris en charge.<br/><br/> Veuillez nous envoyer un document de type JPEG (.jpg ou .jpeg) ou PDF.");
                                                        this.value = '';
                                                }

                                                if (isExtOK && checkFileSize($('#' + fileUploadID), 2, type)) {
                                                    upload(type, null, null);
                                                }
                                            }
                                        }
                                        catch (ex) {
                                            AlertMessage("Erreur", "Une erreur est survenue.<br/><br/> Veuillez réessayer.");
                                            console.log('checkFileToUpload exception : ' + ex.toString());
                                        }
                                    }

                                    function checkFileSize(element, maxSize, type) {
                                        var isOK = true;

                                        if (type == null) { type = ""; }

                                        try {
                                            var fileSize = ($(element)[0].files[0].size / 1024 / 1024); //size in MB
                                            //console.log(fileSize);
                                            if (fileSize > maxSize) {
                                                isOK = false;
                                                AlertMessage("Erreur", "Document trop volumineux.<br/><br/> Veuillez nous envoyer un document de 2Mo maximum.");
                                            }
                                        }
                                        catch (ex) {
                                            isOK = false;
                                            console.log('checkFileSize exception : ' + ex.toString());
                                        }

                                        return isOK;
                                    }

                                    function upload(type, scriptSuccess, scriptFailed) {
                                        //console.log("upload :" + type)
                                        try {
                                            if (type != null && type.trim().length > 0) {

                                                switch (type.toLowerCase().trim()) {
                                                        case "w9":
                                                            $('#<%=hfW9Filename.ClientID%>').val('');
                                                            break;
                                                        case "w8":
                                                            $('#<%=hfW8Filename.ClientID%>').val('');
                                                            break;
                                                        case "fatcaautocertification":
                                                            $('#<%=hfFatcaAutoCertificationFilename.ClientID%>').val('');
                                                            break;
                                                        default:
                                                            throw new Error('document type unknown');
                                                            break;
                                                    }

                                                    var data = new FormData();
                                                    var filename = "";
                                                    //console.log(data);

                                                    //console.log($("#fu" + type));
                                                    var files = $("#fu" + type).get(0).files;
                                                    //console.log(files);

                                                    // Add the uploaded image content to the form data collection
                                                    if (files.length > 0) {
                                                        data.append("UploadedImage", files[0]);
                                                        console.log(files[0]);
                                                        filename = files[0].name;
                                                        //$('#lbl' + type + 'ToUpload').html('');
                                                    }

                                                    var typeToUpload = type;
                                                    if (type.toLowerCase().trim() == "w9" && $('#<%=rblTaxResidenceFATCA_NO.ClientID%>').is(':checked')) {
                                                        typeToUpload = "LossUsNationalityCertificate";
                                                    }

                                                    // Make Ajax request with the contentType = false, and procesDate = false
                                                    var ajaxRequest = $.ajax({
                                                        type: "POST",
                                                        url: "./Tools/Upload_FATCA_AEOI.aspx?type=" + typeToUpload + "&ref=" + $('#<%=hdnRefCustomer.ClientID%>').val(),
                                                        contentType: false,
                                                        processData: false,
                                                        data: data,
                                                        xhr: function () {
                                                            var xhr = new window.XMLHttpRequest();
                                                            //Upload progress
                                                            xhr.upload.addEventListener("progress", function (evt) {
                                                                try {
                                                                    ShowPanelLoading("PersonalInfoPanel");
                                                                    if (evt.lengthComputable) {
                                                                        var percentComplete = evt.loaded / evt.total;
                                                                        //Do something with upload progress
                                                                        //console.log(percentComplete);
                                                                        console.log(evt.loaded + "/" + evt.total);
                                                                    }
                                                                }
                                                                catch (ex) { console.log(ex); }
                                                            }, false);
                                                            //Download progress
                                                            xhr.addEventListener("progress", function (evt) {
                                                                try {
                                                                    if (evt.lengthComputable) {
                                                                        var percentComplete = evt.loaded / evt.total;
                                                                        //Do something with download progress
                                                                        //console.log(evt.loaded + "/" + evt.total);
                                                                        //console.log("uploading : " + percentComplete);

                                                                    }
                                                                }
                                                                catch (ex) { console.log(ex); }
                                                            }, false);
                                                            return xhr;
                                                        },
                                                        success: function (resp) {
                                                            $('#ar-loading-page').fadeOut();

                                                            //console.log(resp);
                                                            //alert("upload done !");
                                                            try {
                                                                var W8FileName = resp.W8FileName;
                                                                var W9FileName = resp.W9FileName;
                                                                var FatcaAutoCertificationFilename = resp.FatcaAutoCertificationFilename;
                                                                var LossUSNationalityCertificate = resp.LossUSNationalityCertificate;

                                                                $('#<%=hfW8Filename.ClientID%>').val(W8FileName);
                                                                $('#<%=hfW9Filename.ClientID%>').val(W9FileName);
                                                                if (LossUSNationalityCertificate.trim().length > 0) { $('#<%=hfW9Filename.ClientID%>').val(LossUSNationalityCertificate); } 
                                                                $('#<%=hfFatcaAutoCertificationFilename.ClientID%>').val(FatcaAutoCertificationFilename);

                                                                if (scriptSuccess != null) { scriptSuccess(); }
                                                                else {
                                                                    $('#lbl' + type + 'ToUpload').css('color', 'green').html(filename + ' a été envoyé');
                                                                    HidePanelLoading();

                                                                    if (type.toLowerCase().trim() == "fatcaautocertification") { $('#<%=btnEditTaxResidence_Hidden.ClientID%>').click(); }
                                                                    AlertMessage("Envoi formulaire", "<span style=\"color:green\">Document \"<b>" + filename + "</b>\" envoyé !</span>");
                                                                }
                                                            }
                                                            catch (ex) {
                                                                console.log(ex);
                                                                HidePanelLoading();
                                                                AlertMessage("Envoi formulaire", "Une erreur est survenue.");
                                                            }
                                                        },
                                                        error: function () {
                                                            $('#ar-loading-page').fadeOut();
                                                            if (scriptFailed != null) { scriptFailed(); }
                                                            else {
                                                                $('#lbl' + type + 'ToUpload').css('color', 'red').html(filename + ' n&apos;a pas été envoyé');
                                                                $('#fu' + type).val('');
                                                                HidePanelLoading();
                                                                AlertMessage("Envoi formulaire", "Une erreur est survenue lors de l'envoi.<br/>Veuillez réessayer");
                                                            }
                                                        }
                                                    });
                                                }
                                            }
                                            catch (ex) { console.log(ex); }
                                        }
                                </script>
                            </div>
                            <asp:Panel ID="panelKycTaxResidence_FATCA_AEOI" runat="server" Visible="true" Enabled="false">
                                <script type="text/javascript">
                                    var CountryList = "";
                                    function GetCountryList() {
                                        CountryList = "";
                                        CountryList = $('#<%=ddlKYCCountryList.ClientID%>').children('option').map(function (i, e) {
                                            return e.innerText; }).get();

                                        initTaxResidenceList();
                                    }

                                    var TaxResidenceCountryIndex;
                                    function initTaxResidenceList() {
                                        TaxResidenceCountryIndex = 0;
                                        $('#tTaxResidenceList').empty();
                                        $('#tTaxResidenceList').append('<tr><th style="min-width:250px;">Pays de résidence fiscale (hors US)</th> <th>Numéro d&apos;identification fiscale</th> <th></th> <th class="WithoutBorder"></th></tr>');

                                        if (!GetTaxResidenceCountryList()) {
                                            addTaxResidenceCountry("FRANCE");
                                            addTaxResidenceCountry();
                                            setTimeout(function () { CheckTaxResidenceListDisabled(); }, 500);
                                        }
                                        else {
                                            setTimeout(function () { CheckTaxResidenceListDisabled(); }, 500);
                                        }

                                        initCombobox();
                                    }

                                    function CheckTaxResidenceListDisabled() {
                                        if ($('#<%=panelTaxResidenceListDisabled.ClientID%>') != null) {
                                            try {
                                                var panelDisabled = $('#<%=panelTaxResidenceListDisabled.ClientID%>');
                                                var panelToDisable = $('#<%=panelTaxResidenceList.ClientID%>');

                                                panelDisabled.width(panelToDisable.outerWidth() + 20);
                                                panelDisabled.height(panelToDisable.outerHeight() - 5);
                                                panelDisabled.css('top', (panelToDisable.position().top + 25) + "px");
                                                panelDisabled.css('left', (panelToDisable.position().left - 10) + "px");
                                                panelDisabled.show();
                                            }
                                            catch (ex) { console.log(ex); }

                                        }
                                    }
                                    function CheckFatcaDisabled() {
                                        if ($('#<%=panelFatcaDisabled.ClientID%>') != null) {
                                            try {
                                                var panelDisabled = $('#<%=panelFatcaDisabled.ClientID%>');
                                                var panelToDisable = $('#<%=panelFATCA.ClientID%>');

                                                panelDisabled.width(panelToDisable.outerWidth() + 20);
                                                panelDisabled.height(panelToDisable.outerHeight() - 5);
                                                panelDisabled.css('top', (panelToDisable.position().top + 25) + "px");
                                                panelDisabled.css('left', (panelToDisable.position().left - 10) + "px");
                                                panelDisabled.show();
                                            }
                                            catch (ex) { console.log(ex); }

                                        }
                                    }

                                    function GetTaxResidenceCountryList() {
                                        var isThereTaxResidenceCountry = true;
                                        //console.log($('#<%=hfTaxResidenceClientValues.ClientID%>').val());
                                        if ($('#<%=hfTaxResidenceClientValues.ClientID%>').val().trim().length > 0) {
                                            try {
                                                var tResidenceList = $('#<%=hfTaxResidenceClientValues.ClientID%>').val().split('|');
                                                //$('#<%=hfTaxResidenceClientValues.ClientID%>').val('');
                                                //console.log(tResidenceList);

                                                for (var i = 0; i < tResidenceList.length; i++) {
                                                    var country = tResidenceList[i].split(';')[0];
                                                    var nifValue = tResidenceList[i].split(';')[1];
                                                    var index = tResidenceList[i].split(';')[2];

                                                    if (country.trim().length > 0 && index.trim().length > 0) {
                                                        if (country =="FRANCE" || nifValue.trim().length > 0) {
                                                            addTaxResidenceCountry(country, nifValue);
                                                        }
                                                        else if (nifValue.trim().length == 0 && CheckNoNifReasonExist(index) != null) {
                                                            addTaxResidenceCountry(country);
                                                            InitNoNifReason(index, country);
                                                        }
                                                        else { isThereTaxResidenceCountry = false; }
                                                    }
                                                    else { isThereTaxResidenceCountry = false; }

                                                }
                                            }
                                            catch (ex) {
                                                isThereTaxResidenceCountry = false;
                                                console.log("GetTaxResidenceCountryList ex :" + ex.toString());
                                            }
                                        }
                                        else { isThereTaxResidenceCountry = false; }

                                        return isThereTaxResidenceCountry;
                                    }

                                    function AddCountryListToSelect(id) {
                                        //console.log($('#' + id));
                                        if (id != null && id.trim().length > 0 && $('#' + id) != null) {
                                            $('#' + id).empty();
                                            $('#' + id).append('<option value=""></option>');
                                            $(CountryList).each(function (i) {
                                                thisOption = '<option value="' + CountryList[i] + '">' + CountryList[i] + '</option>';
                                                $('#' + id).append(thisOption);
                                            });
                                        }
                                    }

                                    function addTaxResidenceCountry(defaultCountryValue, defaultNifValue) {
                                        //console.log(defaultCountryValue);
                                        //console.log(defaultNifValue);

                                        if (TaxResidenceCountryIndex < 10) {
                                            TaxResidenceCountryIndex++;
                                            var NifValue = "";
                                            if (defaultNifValue != null && defaultNifValue.trim().length > 0) {
                                                NifValue = defaultNifValue;
                                            }

                                            var newIndex = 0;
                                            if ($('#tTaxResidenceList tr:last').attr('id') != null) {
                                                var trID = ($('#tTaxResidenceList tr:last').attr('id'));
                                                newIndex = trID.split('_')[1];
                                            }
                                            newIndex++;

                                            var colNoNif = '<td style=\"width:80px\"><a id="NoNif_' + newIndex + '" onclick="NoNifReason(' + newIndex + ');">Je n&apos;ai pas de code NIF</a><input type="hidden"id="hfNotNifReason_' + newIndex + '" /></td>';
                                            var defaultCountry = "";
                                            if (defaultCountryValue != null && defaultCountryValue.trim().length > 0) {
                                                defaultCountry = defaultCountryValue;
                                                if (defaultCountryValue.toLowerCase() == "france" || checkAEOI(defaultCountryValue)) {
                                                    //console.log("nonif hidden : " + defaultCountryValue);
                                                    colNoNif = '<td style=\"width:80px\"><a id="NoNif_' + newIndex + '" onclick="NoNifReason(' + newIndex + '); " style="display:none;">Je n&apos;ai pas de code NIF</a><input type="hidden"id="hfNotNifReason_' + newIndex + '" /></td>';
                                                }
                                            }

                                            var trID = 'rowTaxResidenceCountry_' + newIndex;
                                            var ddlID = 'ddlTaxResidenceCountry_' + newIndex;
                                            var txtID = 'txtTaxResidenceID_' + newIndex;

                                            var NewRow = '<tr id="' + trID + '"><td><div class="ui-widget"> <select id="' + ddlID + '" class="TaxResidenceCountryCombobox"></select></div></td>';
                                            NewRow += '<td><input type="text" id="' + txtID + '" style="padding: 5px 10px;" maxlength="25" class="txtNIF" value="' + NifValue+'" /></td>';
                                            NewRow += colNoNif;
                                            NewRow += '<td class="WithoutBorder"><div class="delete-button"><img height="25" src= "Styles/Img/delete.png" alt= "Supprimer" onclick="removeTaxResidenceCountry(\'' + trID + '\');" /></div></td>';
                                            NewRow += '</tr>';

                                            //console.log(NewRow);
                                            $('#tTaxResidenceList').append(NewRow);

                                            AddCountryListToSelect('ddlTaxResidenceCountry_' + newIndex);

                                            if (defaultCountry.trim().length > 0) { initCombobox(newIndex, defaultCountry); }
                                            else { initCombobox(); }

                                            //console.log('TaxResidenceCountryIndex++ : ' + TaxResidenceCountryIndex);
                                        }

                                        if (TaxResidenceCountryIndex == 10) { $('#divAddTaxResidenceCountry').fadeOut(); }
                                    }

                                    function removeTaxResidenceCountry(rowID) {
                                        //console.log("removeTaxResidenceCountry");
                                        if (rowID != null && rowID.trim().length > 0 && $('#' + rowID) != null) {
                                            if ($('#tTaxResidenceList tr').length > 2) {
                                                $('#' + rowID).remove();
                                                TaxResidenceCountryIndex--;
                                                $('#divAddTaxResidenceCountry').fadeIn();
                                                //console.log('TaxResidenceCountryIndex-- : ' + TaxResidenceCountryIndex);
                                            }
                                            else { AlertMessage('Pays de résidence fiscal', '<span style="color:black;">Veuillez déclarer au moins un pays de résidence fiscale autre que France.<br/>Si vous êtes résident fiscal en France uniquement <label class="linkOrange" onclick="TaxResidenceOnlyFr();">cliquez-ici</label></span>'); }
                                        }
                                    }

                                    function initCombobox(index, selectedValue) {
                                        $('.TaxResidenceCountryCombobox').each(function () {
                                            var TaxResidenceCountryComboboxID = $(this).attr("id");
                                            $("#" + TaxResidenceCountryComboboxID).combobox();
                                        });

                                        if (index != null && index > 0 && $('#ddlTaxResidenceCountry_' + index) != null &&
                                            selectedValue != null && selectedValue.trim().length > 0) {
                                            $('#ddlTaxResidenceCountry_' + index + ' option[value="' + selectedValue + '"]').attr('selected', 'selected');
                                            $('#rowTaxResidenceCountry_' + index).find('input.custom-combobox-input').val(selectedValue);
                                            if (selectedValue.trim() == "FRANCE") {
                                                //console.log('init france');
                                                $('#txtTaxResidenceID_' + index).attr("disabled", "disabled");
                                            }
                                        }
                                    }

                                    function countryOccurence(countryName, countryList) {
                                        var count = 0;
                                        var pos = countryList.indexOf(countryName);
                                        while (pos > -1) {
                                            ++count;
                                            pos = countryList.indexOf(countryName, ++pos);
                                        }

                                        return count;
                                    }

                                    function CheckTaxResidenceClientList() {
                                        var isOK = false;
                                        var isTaxResidenceClientListOK = true;
                                        var listValues = "";
                                        var message = "";

                                        try {
                                            if ($('#tTaxResidenceList tr').length > 0) {
                                                var NbCountryNotFR = 0;

                                                $('#tTaxResidenceList tr').each(function () {
                                                    if ($(this).attr('id') != null) {
                                                        var index = $(this).attr('id').split('_')[1];
                                                        var ddlID = 'ddlTaxResidenceCountry_' + index;
                                                        var txtID = 'txtTaxResidenceID_' + index;

                                                        if (!($('#' + txtID).val().trim().length > 0 || $('#' + txtID).attr('disabled'))) {
                                                            //message = 'Liste de pays de résidence fiscale et/ou numéro d&apos;identification fiscale non valide. <br/>Veuillez vérifier les informations saisies.';
                                                            //isTaxResidenceClientListOK = false;
                                                            if ($('#' + ddlID).val().trim().length > 0) {
                                                                message = 'Liste de pays de résidence fiscale et/ou numéro d&apos;identification fiscale non valide. <br/>Veuillez vérifier les informations saisies.';
                                                                isTaxResidenceClientListOK = false;
                                                            }
                                                        }
                                                        else {
                                                            //console.log($('#' + ddlID).val().trim());
                                                            //console.log($('#' + txtID).val().trim());
                                                            if (countryOccurence($('#' + ddlID).val().trim(), listValues) < 1) {
                                                                listValues += $('#' + ddlID).val() + ';' + $('#' + txtID).val() + ';' + index + '|';

                                                                if ($('#' + ddlID).val().trim().toLowerCase() != 'france') { NbCountryNotFR++; }
                                                            }
                                                            else {
                                                                message = 'Pays de résidence fiscale en double. <br/>Veuillez vérifier les informations saisies.';
                                                                isTaxResidenceClientListOK = false;
                                                            }
                                                        }

                                                        //console.log(index + ";" + $('#' + ddlID).val() + ";" + $('#' + txtID).val());
                                                    }

                                                });

                                                if (isTaxResidenceClientListOK) {
                                                    if (NbCountryNotFR < 1) {
                                                        AlertMessage('Pays de résidence fiscal', '<span style="color:black;">Veuillez déclarer au moins un pays de résidence fiscale autre que France.<br/>Si vous êtes résident(e) fiscal(e) en France uniquement <label class="linkOrange" onclick="TaxResidenceOnlyFr();">cliquez-ici</a></span>');
                                                    }
                                                    else if (listValues.trim().length > 0) {
                                                        $('#<%=hfTaxResidenceClientValues.ClientID%>').val(listValues.substring(0, listValues.length - 1));
					                                    //console.log($('#<%=hfTaxResidenceClientValues.ClientID%>').val());

                                                                        //isOK = checkClientCertificate();
                                                                        isOK = true;
                                                    }
                                                    else {
                                                        AlertMessage('Pays de résidence fiscal', '<span style="color:black;">Une erreur est survenue. <br/>Veuillez réessayer.</span>');
                                                    }
                                                }
                                                else if ($('#<%=rblTaxResidenceFATCA_YES.ClientID%>').is(':checked')) {
                                                    isOK = true;
                                                }
                                                else {
                                                    message = (message.trim().length > 0) ? message : "Une erreur est survenue. <br/>Veuillez réessayer.";
                                                    AlertMessage('Pays de résidence fiscal', '<span style="color:black;">'+message+'</span>');
                                                }
                                            }
                                            else {
                                                AlertMessage('Pays de résidence fiscal', '<span style="color:black;">Veuillez déclarer au moins un pays de résidence fiscale autre que France.<br/>Si vous êtes résident(e) fiscal(e) en France uniquement <label class="linkOrange" onclick="TaxResidenceOnlyFr();">cliquez-ici</label></span>');
                                            }
                                        }
                                        catch (ex) {
                                            isTaxResidenceClientListOK = false;
                                            isOK = false;
                                        }

                                        return isOK;
                                    }

                                    function checkAEOI(countryName) {
                                        var isAEOI = false;
                                        try {
                                            var countryList = $('#<%=hfAEOIList.ClientID%>').val().split(';');

                                        for (var i = 0; i < countryList.length; i++) {
                                            if (countryList[i].trim().toLowerCase() == countryName.trim().toLowerCase()) {
                                                isAEOI = true;
                                            }
                                        }
                                    }
                                    catch (ex) {
                                        console.log(ex);
                                    }

                                    return isAEOI;
                                    }

                                    function checkTaxResidence() {
                                        var isOK = true;

                                        if (!$('#<%=panelKycTaxResidence_FATCA_AEOI.ClientID%>').hasClass('aspNetDisabled')) {
                                            if ($('#<%=panelTaxResidenceList.ClientID%>') != null && $('#<%=panelTaxResidenceList.ClientID%>').is(':visible')) {
                                                isOK = CheckTaxResidenceClientList();
                                            }

                                            <%--if (isOK && $('#<%=panelFATCA.ClientID%>') != null && $('#<%=panelFATCA.ClientID%>').is(':visible')) {
                                                isOK = CheckFATCA_W9();
                                            }--%>

                                            if (isOK && checkClientCertificate()) {
                                                isOK = true;
                                            }
                                            else {
                                                isOK = false;
                                            }
                                        }

                                        return isOK;
                                    }

                                    function checkClientCertificate() {
                                        var bCertification = false;
                                        var message = "Cochez la case pour auto-certifier l’exactitude des informations renseignées.";

                                        if ($('#<%=cbTaxResidenceCertification.ClientID%>' || ($('#<%=panelFatcaDisabled.ClientID%>') != null && $('#<%=panelFatcaDisabled.ClientID%>'))).is(':visible'))
                                        { bCertification = true; } //else { message += "<li>Certification</li>"; }

                                        if (bCertification) { 
                                            return true;
                                        }
                                        else {
                                            AlertMessage('Erreur', message);
                                        }

                                        return false;
                                    }

                                    function TaxResidenceOnlyFr() {
                                        $("#<%=ddlKYCFrenchTaxOnly.ClientID%>").val("1").change();
                                        CloseAlertMessage();
                                    }

                                    function NoNifReason(index) {
                                        //console.log(index);
                                        //console.log($('#txtTaxResidenceID_' + index));
                                        var country = $('#ddlTaxResidenceCountry_' + index).val();
                                        if (country.trim().length > 0) {
                                            InitNoNifReason(index, country);
                                        
                                            $('#dialog-NoNifReason').dialog({
                                                autoOpen: true,
                                                resizable: false,
                                                draggable: false,
                                                modal: true,
                                                title: "Numéro d'identification fiscale (NIF)",
                                                width: 400,
                                                buttons: [
                                                    {
                                                        text: 'Annuler', class: 'dialog-button', click: function () { $(this).dialog('destroy'); }
                                                    },
                                                    {
                                                        text: 'Valider', class: 'dialog-button', click: function () { if (AddNoNifReason(index, country)) { $(this).dialog('destroy'); } }
                                                    }]
                                            });
                                            $("#dialog-NoNifReason").parent().appendTo(jQuery("form:first"));
                                        }
                                        else { AlertMessage("Je n'ai pas de code NIF","Veuillez renseigner le pays de résidence fiscale"); }
                                    }

                                    function AddNoNifReason(index, country) {
                                        var isOK = false;
                                        try {
                                            //console.log("AddNoNifReason(" + index + ", " + country + ")");
                                            if ($('#txtNoNIFReason').val().trim().length > 0) {

                                                var NoNifReasonValues = $('#<%=hfNoNifReasonValues.ClientID%>').val();
                                                //console.log('NoNifReasonList :' + NoNifReasonList);
                                                var NoNifReasonList = NoNifReasonValues.split('|');

                                                //console.log('CheckNoNifReasonExist : ' + CheckNoNifReasonExist(index));

                                                if (CheckNoNifReasonExist(index) == null) {
                                                    //console.log('add no nif reason (not exist)');

                                                    $('#<%=hfNoNifReasonValues.ClientID%>').val(NoNifReasonValues + index + ';' + $('#txtNoNIFReason').val().replace(';',',') + '|');
                                                    isOK = true;
                                                }
                                                else {
                                                    //console.log('add no nif reason (exist)');
                                                    var NewNoNifReasonValues = "";
                                                    for (var i = 0; i < NoNifReasonList.length; i++) {
                                                        var CurrentIndex = NoNifReasonList[i].split(';')[0];
                                                        var CurrentReason = NoNifReasonList[i].split(';')[1];

                                                        if (index == CurrentIndex) { NewNoNifReasonValues += CurrentIndex + ';' + $('#txtNoNIFReason').val() + '|'; }
                                                        else { NewNoNifReasonValues += CurrentIndex + ';' + CurrentReason + '|'; }
                                                    }
                                                    if (NewNoNifReasonValues.trim().length > 0) {
                                                        NewNoNifReasonValues = NewNoNifReasonValues.substring(0, (NewNoNifReasonValues.length - 1));
                                                    }

                                                    $('#<%=hfNoNifReasonValues.ClientID%>').val(NewNoNifReasonValues);
                                                    isOK = true;
                                                }

                                                InitNoNifReason(index, country);
                                            }
                                            else {
                                                isOK = false;
                                                AlertMessage("", "Veuillez indiquez pourquoi vous n'avez pas de NIF pour ce pays.");
                                            }
                                        }
                                        catch (ex) {
                                            isOK = false;
                                            console.log('AddNoNifReason exception: ' + ex);
                                        }

                                        return isOK;
                                    }

                                    function CheckNoNifReasonExist(index) {
                                        //console.log('CheckNoNifReasonExist' + index);
                                        try {
                                            var NoNifReasonValues = $('#<%=hfNoNifReasonValues.ClientID%>').val();
                                            var NoNifReasonList = NoNifReasonValues.split('|');
                                            var CurrentIndex = "";
                                            var CurrentReason = "";

                                            for (var i = 0; i < NoNifReasonList.length; i++) {
                                                CurrentIndex = NoNifReasonList[i].split(';')[0];
                                                CurrentReason = NoNifReasonList[i].split(';')[1];

                                                if (index == CurrentIndex) {
                                                    //console.log('exist no nif : ' + index);
                                                    return CurrentReason;
                                                }
                                            }
                                        }
                                        catch (ex) {
                                            console.log('CheckNoNifReasonExist exception: ' + ex);
                                            return null;
                                        }

                                        return null;
                                    }

                                    function InitNoNifReason(index, country) {
                                        try {
                                            var NoNifReasonValue = CheckNoNifReasonExist(index);
                                            $('#txtNoNIFReason').val('');

                                            console.log('NoNifReasonValue : ' + NoNifReasonValue);
                                            if (NoNifReasonValue != null) {
                                                $('#txtNoNIFReason').val(NoNifReasonValue);
                                            }

                                            var ThisCountryValue = "";
                                            var NoNifReasonList = $('#<%=hfNoNifReasonValues.ClientID%>').val().split('|');
                                            $('.txtNIF').each(function (index) {
                                                ThisCountryValue = $('#ddlTaxResidenceCountry_' + $(this).attr('id').split('_')[1]).val().trim();
                                                //console.log(ThisCountryValue + "/" + country);
                                                if (ThisCountryValue.toLowerCase() != 'france') {
                                                    var NoNifReasonValueThisCountry = CheckNoNifReasonExist($(this).attr('id').split('_')[1]);
                                                    if (ThisCountryValue.toLocaleLowerCase() != country.toLowerCase().trim()) {
                                                        if (NoNifReasonValueThisCountry == null) {
                                                            console.log("remove nonif : " + ThisCountryValue);
                                                            $(this).removeAttr("disabled");
                                                        }
                                                    }
                                                    else if (NoNifReasonValue.trim().length > 0) {
                                                        $('#txtTaxResidenceID_' + $(this).attr('id').split('_')[1]).val('').attr('disabled', 'disabled');
                                                        //$('#NoNif_' + $(this).attr('id').split('_')[1]).hide();
                                                    }
                                                    else if (checkAEOI(valueLowerCase)) {
                                                        $('#NoNif_' + $(this).attr('id').split('_')[1]).hide();
                                                    }
                                                }
                                            });

                                            /*console.log(NoNifReasonList);
                                            for (var i = 0; i < NoNifReasonList.length; i++) {
                                                var ReasonIndex = NoNifReasonList[i].split(';')[0];
                                                var ReasonValue = NoNifReasonList[i].split(';')[1];
                                                //var ReasonIndex = GetIndexFromTaxCountry(ThisCountryValue);

                                                $('#txtTaxResidenceID_' + ReasonIndex).val('').attr('disabled', 'disabled');
                                                console.log('disable txtTaxResidenceID_' + ReasonIndex);
                                            }*/
                                        }
                                        catch (ex) {
                                            console.log('InitNoNifReason exception : ' + ex);
                                        }
                                    }

                                    function GetIndexFromTaxCountry(index) {
                                    }
                                </script>
                                <script type="text/javascript">
                                    (function ($) {
                                        $.widget("custom.combobox", {
                                            _create: function () {
                                                this.wrapper = $("<span>")
                                                    .addClass("custom-combobox")
                                                    .insertAfter(this.element);

                                                this.element.hide();
                                                this._createAutocomplete();
                                                this._createShowAllButton();
                                            },

                                            _createAutocomplete: function () {
                                                //var inputID = this.element[0].id;
                                                //console.log(inputID);
                                                //console.log(inputID);

                                                var selected = this.element.children(":selected"),
                                                    value = selected.val() ? selected.text() : "";

                                                this.input = $("<input>")
                                                    .appendTo(this.wrapper)
                                                    .val(value)
                                                    .attr("title", "")
                                                    .on('change', function () {
                                                        //console.log($(this).parent().parent().parent().parent());
                                                        try {
                                                            $(this).val($(this).val().toUpperCase());
                                                            $('#txtTaxResidenceID_' + $(this).parent().parent().parent().parent().attr('id').split('_')[1]).removeAttr("disabled");

                                                            if ($(this).val().toLowerCase() == "france") {
                                                                $('#txtTaxResidenceID_' + $(this).parent().parent().parent().parent().attr('id').split('_')[1]).attr("disabled", "disabled");
                                                            }
                                                        }
                                                        catch (ex) {
                                                            console.log("_createAutoComplete ex :" + ex.toString());
                                                        }
                                                    })
                                                    .addClass("custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left")
                                                    .autocomplete({
                                                        delay: 0,
                                                        minLength: 0,
                                                        source: $.proxy(this, "_source")
                                                    })
                                                    .tooltip({
                                                        classes: {
                                                            "ui-tooltip": "ui-state-highlight"
                                                        }
                                                    });

                                                this._on(this.input, {
                                                    autocompleteselect: function (event, ui) {
                                                        try {

                                                            ui.item.option.selected = true;
                                                            this._trigger("select", event, {
                                                                item: ui.item.option
                                                            });
                                                            //console.log('autocompleteselect : ' + this.input.val());
                                                            //if (checkCountry(ui.item.value)) {
                                                            //console.log($(this));
                                                            var selectElement = $(this).attr('element')[0];
                                                            $('#txtTaxResidenceID_' + $(selectElement).attr('id').split('_')[1]).removeAttr('disabled');
                                                            $('#NoNif_' + $(selectElement).attr('id').split('_')[1]).show();

                                                            if (ui.item.value.toLowerCase() == 'france') {

                                                                $('#txtTaxResidenceID_' + $(selectElement).attr('id').split('_')[1]).attr('disabled', 'disabled');
                                                                $('#txtTaxResidenceID_' + $(selectElement).attr('id').split('_')[1]).attr('disabled', 'disabled');
                                                                $('#NoNif_' + $(selectElement).attr('id').split('_')[1]).hide();
                                                            }
                                                            else if (checkAEOI(ui.item.value)) {
                                                                $('#NoNif_' + $(selectElement).attr('id').split('_')[1]).hide();
                                                            }
                                                            //}

                                                        }
                                                        catch (ex) { console.log("_createautocomplete _on ex :"+ex.toString()); }

                                                    },

                                                    autocompletechange: "_removeIfInvalid"
                                                });
                                            },

                                            _createShowAllButton: function () {
                                                var input = this.input,
                                                    wasOpen = false;

                                                $("<a>")
                                                    .attr("tabIndex", -1)
                                                    .attr("title", "Afficher la liste")
                                                    .tooltip()
                                                    .appendTo(this.wrapper)
                                                    .button({
                                                        icons: {
                                                            primary: "ui-icon-triangle-1-s"
                                                        },
                                                        text: false
                                                    })
                                                    .removeClass("ui-corner-all")
                                                    .addClass("custom-combobox-toggle ui-corner-right")
                                                    .on("mousedown", function () {
                                                        wasOpen = input.autocomplete("widget").is(":visible");
                                                    })
                                                    .on("click", function () {
                                                        input.trigger("focus");

                                                        // Close if already visible
                                                        if (wasOpen) {
                                                            return;
                                                        }

                                                        // Pass empty string as value to search for, displaying all results
                                                        input.autocomplete("search", "");
                                                    });
                                            },

                                            _source: function (request, response) {
                                                var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
                                                response(this.element.children("option").map(function () {
                                                    var text = $(this).text();

                                                    if (this.value && (!request.term || matcher.test(text)))
                                                        return {
                                                            label: text,
                                                            value: text,
                                                            option: this
                                                        };
                                                }));
                                            },

                                            _removeIfInvalid: function (event, ui) {
                                                try {
                                                    //console.log('_removeIfInvalid');
                                                    // Selected an item, nothing to do
                                                    if (ui.item) {
                                                        //console.log(ui.item);
                                                        //console.log(ui.item.value);
                                                        //if (checkCountry(ui.item.value))
                                                        return;
                                                        //else {
                                                        //    this.input.val("");
                                                        //    this.element.val("");
                                                        //    //this.input.autocomplete("instance").term = "";
                                                        //    return;
                                                        //}
                                                    }

                                                    // Search for a match (case-insensitive)
                                                    var value = this.input.val(),
                                                        valueLowerCase = value.toLowerCase(),
                                                        valid = false;
                                                    this.element.children("option").each(function () {
                                                        if ($(this).text().toLowerCase() === valueLowerCase) {
                                                            this.selected = valid = true;
                                                            return false;
                                                        }
                                                    });
                                                    
                                                    // Found a match, nothing to do
                                                    if (valid) {
                                                        try {
                                                            var selectObj = $(this).attr('element')[0];
                                                            var objIndex = $(selectObj).attr('id').split('_')[1];

                                                            $('#txtTaxResidenceID_' + objIndex).removeAttr('disabled');
                                                            $('#NoNif_' + objIndex).show();

                                                            if (valueLowerCase == 'france') {
                                                                $('#txtTaxResidenceID_' + objIndex).attr('disabled', 'disabled');
                                                                $('#NoNif_' + objIndex).hide();
                                                            }
                                                            else if (checkAEOI(valueLowerCase)) {
                                                                $('#NoNif_' + objIndex).hide();
                                                            }
                                                        }
                                                        catch (ex) { console.log("autocomplete _removeIfInvalid : " + ex.toString()); }

                                                        return;
                                                    }

                                                    // Remove invalid value
                                                    this.input
                                                        .val("")
                                                        .attr("title", '<span style="font-weight:bold">' + value + '</span> non valide')
                                                        .tooltip("open");
                                                    this.element.val("");
                                                    this._delay(function () {
                                                        this.input.tooltip("close").attr("title", "");
                                                    }, 2500);
                                                    this.input.autocomplete("instance").term = "";
                                                }
                                                catch (ex2) {
                                                    console.log(ex2);
                                                }
                                            },

                                            _destroy: function () {
                                                this.wrapper.remove();
                                                this.element.show();
                                            }
                                        });
                                    })(jQuery);
                                </script>

                                <style type="text/css">
                                    .label-file {
                                        cursor: pointer;
                                        color: black;
                                        font-weight: bold;
                                        text-transform:none;
                                        text-decoration:underline;
                                    }
                                    .label-file:hover {
                                        color: #f57527;
                                    }
                                    .input-file {
                                        display: none;
                                    }

                                    .TaxResidenceTable {
                                        border-collapse:collapse; 
                                        border-spacing:0; 
                                        width:100%;
                                    }
                                    .TaxResidenceTable td, .TaxResidenceTable th  {
                                        border:1px solid #000;
                                        padding: 10px;
                                        text-transform:none;
                                        font-size: 0.9em;
                                        background-color:#fff;
                                    }

                                    .TaxResidenceTable td.WithoutBorder, .TaxResidenceTable th.WithoutBorder {
                                        border:0;
                                        background-color:transparent;
                                    }

                                    .TaxResidenceTable td input[type=radio]{
                                        margin:0;
                                    }

                                    .TaxResidenceTable input[type=text] {
                                        width:90%;
                                    }

                                    .linkOrange {
                                        text-transform:none;
                                        color:#f57527;
                                        text-decoration:underline;
                                    }
                                    .linkOrange:hover{
                                        color:#f57527;
                                        cursor:pointer;
                                    }

                                    a.linkBlack{
                                        text-transform:none;
                                        font-size:0.9em;
                                        color:black;
                                    }
                                    a.linkBlack:hover{
                                        color:#f57527;
                                        cursor:pointer;
                                    }

                                    input:disabled{
                                        background-color:#ddd;
                                    }
                                </style>
                                <style type="text/css">
                                    .custom-combobox {
                                        position: relative;
                                        display: inline-block;
                                    }
                                    .custom-combobox-toggle {
                                        position: absolute;
                                        top: 0;
                                     bottom: 0;
                                        margin-left: -1px;
                                        padding: 0;
                                    }
                                    .custom-combobox-input {
                                        margin: 0;
                                        padding: 5px 10px;
                                    }
                                </style>
                                <div style="background-color:#ECECEC; padding:10px; margin-top:5px;">
                                    <div class="table" style="width:100%">
                                        <div class="row">
                                            <div class="cell" style="width:50%;vertical-align:bottom">
                                                <div class="label">
                                                    <asp:Label ID="lblKYCFrenchTaxOnly" runat="server" AssociatedControlID="ddlKYCFrenchTaxOnly">Résident fiscal français uniquement ?</asp:Label>
                                                </div>
                                                <div>
                                                    <asp:DropDownList ID="ddlKYCFrenchTaxOnly" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlKYCFrenchTaxOnly_SelectedIndexChanged" DataTextField="Label" DataValueField="Value" style="width:90%">
                                                        <asp:ListItem Value="" Selected="True"></asp:ListItem>
                                                        <asp:ListItem Value="1">Oui</asp:ListItem>
                                                        <asp:ListItem Value="0">Non</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="cell" style="width:50%;vertical-align:bottom">
                                                <div class="label">
                                                    <asp:Label ID="lblKYCUsResidentOrBorn" runat="server" AssociatedControlID="ddlKYCUsResidentOrBorn">Citoyen(ne) américain(e), résident(e) américain(e) ou né(e) aux Etats-Unis ?</asp:Label>
                                                </div>
                                                <div>
                                                    <asp:DropDownList ID="ddlKYCUsResidentOrBorn" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlKYCUsResidentOrBorn_SelectedIndexChanged" DataTextField="Label" DataValueField="Value" style="width:90%">
                                                        <asp:ListItem Value="" Selected="True"></asp:ListItem>
                                                        <asp:ListItem Value="1">Oui</asp:ListItem>
                                                        <asp:ListItem Value="0">Non</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                    <asp:Panel ID="panelTaxResidenceListDisabled" runat="server" Visible="true" style="z-index:10000; position:absolute; background-color:white; opacity:0.3;"></asp:Panel>
                                    <asp:Panel ID="panelTaxResidenceList" runat="server" Visible="false" style="margin-top:10px; width:100%">
                                        <div class="label">
                                            <asp:Label ID="lblKYCTaxResidenceList" runat="server">Pays de résidence fiscale autre que Etats-Unis :</asp:Label>
                                        </div>
                                        <div>
                                            <table class="TaxResidenceTable" id="tTaxResidenceList">
                                            </table>
                                        </div>
                                        <div id="divAddTaxResidenceCountry" style="margin-top:5px;">
                                            <a class="MiniButton" onclick="addTaxResidenceCountry();" style="text-transform:none; padding:2px 20px">Ajouter un autre pays de résidence fiscale</a>
                                        </div>
                                        <asp:DropDownList ID="ddlKYCCountryList" runat="server" DataTextField="CountryName" DataValueField="ISO2" style="display:none"></asp:DropDownList>
                                        
                                        <asp:HiddenField ID="hfAEOIList" runat="server" />
                                        <div id="dialog-NoNifReason" style="display:none">
                                            <div>Veuillez indiquer N/A si le pays de résidence fiscale n’émet pas de NIF OU préciser la raison de non-obtention de NIF alors que le pays de votre résidence fiscale émet des NIF</div>
                                            <div style="margin-top:10px;">
                                                <input id="txtNoNIFReason" type="text" maxlength="500" style="width:90%" />
                                            </div>
                                        </div>
                                    </asp:Panel>
                                    <asp:Panel ID="panelFATCA_W8" runat="server" Visible="false" style="margin-top:10px; width:100%">
                                        <div class="label">
                                            "US Person"
                                        </div>
                                        <div style="text-transform:none">
                                            Si vous déclarez ne pas être citoyen(ne) ou résident(e) américain(e), ni être né(e) aux États-Unis, la législation nous impose de vous demander de remplir un <a href="https://www.irs.gov/pub/irs-pdf/fw8ben.pdf" target="_blank">formulaire W-8BEN</a>.
                                        </div>
                                        <div style="margin-top:10px">
                                            <label class="label-file" for="fuW8">Envoyer votre formulaire W-8BEN</label>
                                            <div><label id="lblW8ToUpload"></label></div>
                                            <input id="fuW8" type="file" class="input-file" onchange="checkFileToUpload('fuW8', 'W8')" accept="image/jpg,image/jpeg,application/pdf" />
                                            <input id="btnUploadFileW8" type="button" value="Envoyer" onclick="upload('W8', )" class="orange-big-button" style="display:none" />
                                        </div>
                                    </asp:Panel>
                                    <asp:Panel ID="panelFatcaDisabled" runat="server" Visible="true" style="z-index:10000; position:absolute; background-color:white; opacity:0.3;"></asp:Panel>
                                    <asp:Panel ID="panelFATCA" runat="server" Visible="false" style="margin-top:10px; width:100%">
                                        <div class="label">
                                            <asp:Label ID="lblKYCUSPerson" runat="server">US Person</asp:Label>
                                        </div>
                                        <div>
                                            <table class="TaxResidenceTable" style="">
                                                <tr>
                                                    <td>
                                                        Je déclare <span style="text-transform:uppercase; font-weight:bold">être</span> citoyen(ne) ou résident(e) américain(e) au sens de la réglementation FATCA, et je fournis <a class="" target="_blank" href="../files/fw9.pdf">le formulaire W-9 (ici)</a> de l'administration fiscale américaine, <b>complété, signé et scanné</b>.
                                                        <div style="margin-top:10px">
                                                            <div><span class="bold">TIN</span> (si renseigné)</div>
                                                            <asp:TextBox ID="txtUsTIN" runat="server" style="width:250px"></asp:TextBox>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <input type="radio" id="rblTaxResidenceFATCA_YES" runat="server" name="rblTaxResidenceFATCA" class="cssradiobutton" />
                                                        <label for="<%=rblTaxResidenceFATCA_YES.ClientID%>" class="cssradiobutton-label" style="margin-bottom:2px;"></label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Je suis né(e) aux Etats-Unis et je déclare <span style="text-transform:uppercase; font-weight:bold">ne pas être</span> citoyen(ne) américain(e) ou résident(e) américain(e) au sens de la réglementation FATCA.<br />
                                                        Je fournis <b>un certificat de perte de nationalité américaine</b> ou, en cas de non obtention de la nationalité américaine, <b>tout autre justificatif</b>.
                                                    </td>
                                                    <td>
                                                        <input type="radio" id="rblTaxResidenceFATCA_NO" runat="server" name="rblTaxResidenceFATCA" class="cssradiobutton" />
                                                        <label for="<%=rblTaxResidenceFATCA_NO.ClientID%>" class="cssradiobutton-label" style="margin-bottom:2px;"></label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div style="margin-top:10px">
                                            <label class="label-file" for="fuW9">Envoyer votre formulaire W-9 ou tout autre justificatif</label>
                                            <div><label id="lblW9ToUpload"></label></div>
                                            <input id="fuW9" type="file" class="input-file" onchange="checkFileToUpload('fuW9', 'W9')" accept="image/jpg,image/jpeg,application/pdf" />
                                            <input id="btnUploadFileW9" type="button" value="Envoyer" onclick="upload('W9')" class="orange-big-button" style="display:none" />
                                            <%--<asp:FileUpload ID="fuW9" runat="server" CssClass="fileUploadClass" />--%>
                                            <%--<a onclick="uploadW9('<%btnUploadW9.ClientID%>');">Envoyer votre formulaire W-9 ou tout autre justificatif</a>--%>
                                            <asp:HiddenField ID="hfW9Filename" runat="server" />
                                            <asp:HiddenField ID="hfW8Filename" runat="server" />
                                        </div>
                                    </asp:Panel>

                                </div>
                                <div>
                                    <div class="label">
                                        <label id="clientPrivacyTitle">
                                            Vie privée et confidentialité
                                        </label>
                                    </div>
                                    <div class="table">
                                        <div class="row">
                                            <div class="cell" style="width:30px;">&nbsp;</div>
                                            <div class="cell">
                                                <p class="KycConditions">
                                                    En vue de satisfaire à ses obligations légales et réglementaires, Financière des Paiements Électroniques est tenu de collecter, traiter et communiquer certaines des données à caractère personnel et informations relatives à vos comptes financiers et et valeurs de ces comptes aux autorités fiscales nationales. Conformément au droit local et aux conventions internationales d’échange d’informations à des fins fiscales, ces informations pourront par ailleurs être transmises aux autorités des pays dans le(s)quel(s) vous êtes imposable.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="label">
                                        <label id="clientCertificationTitle">
                                            Certification
                                        </label>
                                    </div>
                                    <div class="table">
                                        <div class="row">
                                            <div class="cell" style="width:30px;">
                                                <input id="cbTaxResidenceCertification" runat="server" type="checkbox" disabled="disabled" />
                                            </div>
                                            <div class="cell">
                                                <label for="<%=cbTaxResidenceCertification.ClientID%>">
                                                    <p class="KycConditions">
                                                        Je déclare que les informations figurant dans le présent formulaire sont, exactes et exhaustives. Je donne également mon accord à la collecte, au traitement et à la communication de mes données personnelles, y compris les NIFs émis par des pays non reportables à la date des présentes et aux informations relatives à mes comptes financiers et valeurs de ces comptes pour les objectifs visés à la section III ci-dessous. Je m’engage par ailleurs à informer Financière des Paiements Electroniques – Compte Nickel sans délai de tout changement de circonstances rendant les informations contenues dans le présent formulaire incorrectes et à fournir un formulaire d’auto certification dûment mis à jour dans les 30 jours suivant le changement de circonstances.
                                                    </p>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="label">
                                        <label id="clientDataUseTitle">
                                        Utilisation des données
                                        </label>
                                    </div>
                                    <div class="table">
                                        <div class="row">
                                            <div class="cell" style="width:30px;">
                                                <input id="cbTaxResidenceCertificateDataUse" runat="server" type="checkbox" disabled="disabled" />
                                            </div>
                                            <div class="cell">
                                                <label for="<%=cbTaxResidenceCertificateDataUse.ClientID %>">
                                                    <p class="KycConditions">
                                                        Les informations recueillies dans les présent formulaire et les activités enregistrées sur mon Compte-Nickel sont traitées par Financière des Paiements Électroniques (FPE) dans un fichier informatisé et sont conservées pendant 10 ans. J’accepte qu’elles puissent être exploitées par FPE pour communiquer avec moi, par email ou à travers mon espace client, de façon ciblée ou non, échanger des informations sur son offre ou celle de ses partenaires, pour réaliser des statistiques et mesurer l’efficacité de sa stratégie d’acquisition client ou encore pour l’intégrer à ses outils publicitaires afin d’améliorer la pertinence des publicités qui pourront m’être présentées. Conformément à la loi « informatique et libertés », vous pouvez exercer votre droit d'accès aux données vous concernant, les faire rectifier ou retirer votre accord à ce que FPE les conserve et les exploite,en contactant FPE à l'adresse <a href="mailto:cnil@compte-nickel.fr" style="white-space:nowrap">cnil@compte-nickel.fr</a>.
                                                    </p>
                                                </label>
                                            </div>
										</div>
									</div>
								</div>
							</asp:Panel>
                            
                        </div>

                        <div id="divAddress" style="color: #344b56; font-weight: bold; text-transform: uppercase; border-bottom: 1px solid #344b56; margin-top: 30px">
                            Adresse
                            <span id="lblCheckAddressLink" runat="server" class="image-tooltip" style="cursor: pointer; text-transform: none; font-weight: normal; font-size: 0.9em">[Etat courrier :
                                <asp:Image ID="imgCheckAddressStatus" runat="server" ImageUrl="~/Styles/Img/gray_status.png" Style="position: relative; top: 2px" />]</span>
                            <span id="lblCapAdresseStatus" runat="server" class="image-tooltip" style="text-transform: none; font-weight: normal; font-size: 0.9em">[Etat CAP Adresse :
                                <asp:Image ID="imgCapAdresseStatus" runat="server" ImageUrl="~/Styles/Img/gray_status.png" Style="position: relative; top: 2px" />]</span>
                        </div>
                        <asp:UpdatePanel ID="upAddress" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div id="div-address" class="table" style="width: 100%; border-collapse: collapse">
                                    <div class="row">
                                        <div class="cell" style="width: 50%">
                                            <div class="label">
                                                <asp:Label ID="lblAddress1" runat="server" AssociatedControlID="txtAddress1">Adresse 1</asp:Label>
                                            </div>
                                            <div>
                                                <asp:TextBox ID="txtAddress1" runat="server" ReadOnly="true" CssClass="readonly" Style="width: 90%; text-transform: uppercase"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="cell" style="width: 50%">
                                            <div class="label">
                                                <asp:Label ID="lblAddress2" runat="server" AssociatedControlID="txtAddress2">Adresse 2</asp:Label>
                                            </div>
                                            <div>
                                                <asp:TextBox ID="txtAddress2" runat="server" ReadOnly="true" CssClass="readonly" Style="width: 92%; text-transform: uppercase"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="cell" style="width: 50%">
                                            <div class="label">
                                                <asp:Label ID="lblAddress3" runat="server" AssociatedControlID="txtAddress3">Adresse 3</asp:Label>
                                            </div>
                                            <div>
                                                <asp:TextBox ID="txtAddress3" runat="server" ReadOnly="true" CssClass="readonly" Style="width: 90%; text-transform: uppercase"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="cell" style="width: 50%">
                                            <div class="label">
                                                <asp:Label ID="lblAddress4" runat="server" AssociatedControlID="txtAddress4">Adresse 4</asp:Label>
                                            </div>
                                            <div>
                                                <asp:TextBox ID="txtAddress4" runat="server" ReadOnly="true" CssClass="readonly" Style="width: 92%; text-transform: uppercase"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="cell" style="width: 50%">
                                            <div class="label">
                                                <asp:Label ID="lblZipcode" runat="server" AssociatedControlID="txtZipcode">Code postal</asp:Label>
                                            </div>
                                            <div>
                                                <asp:TextBox ID="txtZipcode" runat="server" ReadOnly="true" CssClass="readonly" Style="width: 70px" MaxLength="5"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="cell" style="width: 50%">
                                            <div class="label">
                                                <asp:Label ID="lblCity" runat="server" AssociatedControlID="txtCity">Ville</asp:Label>
                                            </div>
                                            <div>
                                                <asp:TextBox ID="txtCity" runat="server" ReadOnly="true" CssClass="readonly" Style="width: 92%; text-transform: uppercase"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style="margin-top: 15px; text-align: center">
                                    <asp:CAPAddress ID="capAddress1" runat="server" />
                                    <asp:HiddenField ID="hdnComplCity" runat="server" />
                                    <asp:HiddenField ID="hdnStreetNumber" runat="server" />
                                    <asp:HiddenField ID="hdnStreetName" runat="server" />
                                    <asp:HiddenField ID="hdnAddressQualityCode" runat="server" />
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>

                        <div id="divContact" style="color: #344b56; font-weight: bold; text-transform: uppercase; border-bottom: 1px solid #344b56; margin-top: 30px">
                            Contact
                        </div>
                        <div class="table" style="width: 100%; border-collapse: collapse;">
                            <div class="row">
                                <div class="cell" style="width: 50%">
                                    <div class="label">
                                        <asp:Label ID="lblPhoneNumber" runat="server" AssociatedControlID="txtPhoneNumber">N° de mobile</asp:Label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtPhoneNumber" runat="server" Style="width: 150px" MaxLength="20"></asp:TextBox>
                                    </div>
                                </div>
                                <div id="div-email" class="cell" style="width: 50%">
                                    <div>
                                        <asp:Label ID="lblMail" runat="server" CssClass="label" AssociatedControlID="txtMail">Adresse Email</asp:Label>
                                        <span id="lblCapEmailStatus" runat="server" class="image-tooltip" style="text-transform: none; font-weight: normal; font-size: 0.9em">[Etat CAP Email :
                                            <asp:Image ID="imgCapEmailStatus" runat="server" ImageUrl="~/Styles/Img/gray_status.png" Style="position: relative; top: 2px" />]</span>
                                    </div>
                                    <div>
                                        <asp:UpdatePanel ID="upMail" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtMail" runat="server" ReadOnly="true" CssClass="readonly" Style="width: 92%"></asp:TextBox>
                                                <asp:HiddenField ID="hdnMail" runat="server" />
                                                <asp:HiddenField ID="hdnMailQualityCode" runat="server" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                        <asp:CAPEmail ID="capEmail1" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <asp:Panel ID="panelSavePersonalInfo" runat="server" Style="margin-top: 30px; text-align: center; position: relative">
                            <input type="button" value="Enregistrer" onclick="ConfirmAction('PersonalInfo');" class="button" style="width: 100%;" />
                            <img class="loading" src="Styles/Img/loading.gif" alt="loading" width="25px" height="25px" style="display: none; position: absolute; top: 1px; left: 57%" />
                        </asp:Panel>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ddlKYCpro" EventName="SelectedIndexChanged" />
                        <asp:AsyncPostBackTrigger ControlID="ddlKYChab" EventName="SelectedIndexChanged" />
                        <asp:AsyncPostBackTrigger ControlID="ddlKYCFrenchTaxOnly" EventName="SelectedIndexChanged" />
                        <asp:AsyncPostBackTrigger ControlID="ddlKYCUsResidentOrBorn" EventName="SelectedIndexChanged" />
                        <asp:AsyncPostBackTrigger ControlID="btnSavePersonalInfo" EventName="click" />
                        <asp:AsyncPostBackTrigger ControlID="btnSendCourrier" EventName="click" />
                        <asp:AsyncPostBackTrigger ControlID="btnNotifyCourrier" EventName="click" />
                        <asp:AsyncPostBackTrigger ControlID="btnForceAddressConfirmation" EventName="click" />
                        <asp:AsyncPostBackTrigger ControlID="btnEditTaxResidence" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="btnEditTaxResidence_Hidden" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>

                <asp:UpdatePanel ID="upTaxResidenceList" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:HiddenField ID="hfTaxResidenceClientValues" runat="server" />
                        <asp:HiddenField ID="hfNoNifReasonValues" runat="server" />
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:UpdatePanel ID="upFatca" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:HiddenField ID="hfFatcaAlert" runat="server" />
                        <asp:HiddenField ID="hfFatcaRequired" runat="server" />
                    </ContentTemplate>
                </asp:UpdatePanel>

                <div id="divCheckAddressPopup" style="display: none">
                    <span class="ui-helper-hidden-accessible">
                        <input type="text" /></span>
                    <div class="label">
                        Etat
                    </div>
                    <div>
                        <asp:Label ID="lblCheckAddressStatus" runat="server"></asp:Label>
                    </div>
                    <div style="width: 50%; float: left">
                        <div class="label">
                            Date demande
                        </div>
                        <div>
                            <asp:Label ID="lblCheckAddressAskDate" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div style="width: 50%; float: right">
                        <div class="label">
                            Date vérification
                        </div>
                        <div>
                            <asp:Label ID="lblCheckAddressCheckDate" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div style="clear: both"></div>
                    <asp:Panel ID="panelHistoCheckAddress" runat="server" Style="margin-top: 20px;">
                        <div class="label">
                            Historique
                        </div>
                        <div class="letter-histo-scrollable-area" style="max-height: 300px; overflow: hidden; overflow-y: auto;">
                            <asp:Repeater ID="rptHistoCheckAddress" runat="server">
                                <HeaderTemplate>
                                    <table id="letter-histo" class="defaultTable" style="width: 100%; border-collapse: collapse">
                                        <thead>
                                            <tr>
                                                <th>Adresse</th>
                                                <th>Date d'envoi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblAddress" runat="server" Text='<%#Eval("Address").ToString() %>'></asp:Label>
                                        </td>
                                        <td style="width: 1px; text-align: right">
                                            <asp:Label ID="lblSentDate" runat="server" Text='<%#Eval("SentDate").ToString() %>'></asp:Label>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </tbody>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                    </asp:Panel>
                    <asp:HiddenField ID="hdnAllowCourrierActions" runat="server" />
                </div>

                <div id="popup-SendCourrier" style="display: none">
                    <span class="ui-helper-hidden-accessible">
                        <input type="text" /></span>
                    Voulez-vous renvoyer le courrier de vérification d'adresse ?
                    <asp:Button ID="btnSendCourrier" runat="server" OnClick="btnSendCourrier_Click" Style="display: none" />
                </div>
                <div id="popup-NotifyCourrier" style="display: none">
                    <span class="ui-helper-hidden-accessible">
                        <input type="text" /></span>
                    <div>Comment voulez-vous envoyer le rappel ?</div>
                    <div style="margin-top: 5px; text-align: right">
                        <asp:RadioButtonList ID="rblNotifyMethod" runat="server" RepeatDirection="Horizontal" CssClass="RadioButtonList">
                            <asp:ListItem Text="Email" Value="Mail" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="SMS" Value="SMS"></asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                    <asp:Button ID="btnNotifyCourrier" runat="server" OnClick="btnNotifyCourrier_Click" Style="display: none" />
                </div>

                <div id="popup-ForceAddressConfirmation" style="display: none">
                    <span class="ui-helper-hidden-accessible">
                        <input type="text" /></span>
                    Voulez-vous forcer la confirmation d'adresse ?
                    <asp:Button ID="btnForceAddressConfirmation" runat="server" OnClick="btnForceAddressConfirmation_Click" Style="display: none" />
                </div>

            </div>
        </asp:Panel>
        <asp:Panel ID="panelCards" runat="server">
            <asp:Panel ID="panelCardsHisto" runat="server" Visible="false" Style="margin-bottom: 20px;">
                <div id="divCardsHisto">
                    <div class="table defaultTable2" style="width: 100%; border-collapse: collapse">
                        <div class="table-row head" style="font-weight: bold">
                            <div class="table-cell" style="width: 70px; vertical-align: middle">
                            </div>
                            <div class="table-cell" style="width: 140px; vertical-align: middle">
                                N° carte
                            </div>
                            <div class="table-cell" style="width: 95px; vertical-align: middle">
                                Code barre
                            </div>
                            <div class="table-cell" style="width: 90px; vertical-align: middle">
                                Statut
                            </div>
                            <div class="table-cell" style="width: 100px; vertical-align: middle">
                                Date
                            </div>
                            <div class="table-cell" style="vertical-align: middle">
                                Limites SAB
                            </div>
                            <div class="table-cell" style="width: 145px; vertical-align: middle">
                                &nbsp;
                            </div>
                        </div>
                    </div>
                    <asp:UpdatePanel ID="upCardsHisto" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div id="div-card-histo" style="max-height: 200px; min-height: 50px; overflow-y: auto;">
                                <asp:Button ID="btnLoadHisto" runat="server" Style="display: none" OnClick="btnLoadHisto_Click" />
                                <div class="cards-actions-loading"></div>
                                <asp:Repeater ID="rptCardsHisto" runat="server">
                                    <ItemTemplate>
                                        <div class="table" style="width: 100%; border-collapse: collapse">
                                            <div class="table-row <%# Container.ItemIndex % 2 == 0 ? "default-table-row-style" : "table-alternate-row-style" %>">
                                                <div class="table-cell" style="vertical-align: middle; width: 80px">
                                                    <img src='./Styles/Img/CustomCards/<%# Eval("CardModelLink") %>' height="40" style="vertical-align: middle" />
                                                </div>
                                                <div class="table-cell" style="width: 150px">
                                                    <asp:Label ID="lblCardNumber" runat="server" Text='<%#Eval("CardNumber") %>'></asp:Label>
                                                </div>
                                                <div class="table-cell" style="width: 105px; text-align: center">
                                                    <asp:Label ID="lblBarcode" runat="server" Text='<%#Eval("Barcode") %>'></asp:Label>
                                                </div>
                                                <div class="table-cell" style="width: 100px; text-align: center">
                                                    <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("Status") %>'></asp:Label>
                                                    <asp:Image ID="imgCardStatus" runat="server" ImageUrl='<%# (Eval("Status").ToString().ToLower() == "active" || Eval("Status").ToString().ToLower() == "modifiee") ? "~/Styles/Img/green_status.png" : "~/Styles/Img/red_status.png" %>' CssClass="image-tooltip" ToolTip='<%#(Eval("Status").ToString().ToLower() == "active" || Eval("Status").ToString().ToLower() == "modifiee") ? "En service" : "Hors service" %>' style="position:relative;top:2px;" />
                                                </div>
                                                <div class="table-cell" style="width: 110px;line-height:15px;">
                                                    <div style="position:relative;top:-3px">
                                                        <span style="font-size:0.8em">Remise :</span><br />
                                                        <asp:Label ID="lblDateRemise" runat="server" Text='<%#Eval("DateRemise") %>'></asp:Label><br />
                                                        <span style="font-size:0.8em">Expiration :</span><br />
                                                        <asp:Label ID="lblExpirationDate" runat="server" Text='<%#Eval("ExpirationDate") %>'></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="table-cell">
                                                    <span style="display: inline-block; width: 80px; font-weight: bold">Achat :</span><asp:Label ID="lblPaymentLimit" runat="server" Text='<%#Eval("PaymentLimit") %>'></asp:Label>&euro; sur
                                                    <asp:Label ID="lblPaymentPeriod" runat="server" Text='<%#Eval("PaymentPeriod") %>'></asp:Label>j<br />
                                                    <span style="display: inline-block; width: 80px; font-weight: bold">Retrait :</span><asp:Label ID="lblWithdrawMoneyLimit" runat="server" Text='<%#Eval("WithdrawMoneyLimit") %>'></asp:Label>&euro; sur
                                                    <asp:Label ID="lblWithdrawMoneyPeriod" runat="server" Text='<%#Eval("WithdrawMoneyPeriod") %>'></asp:Label>j
                                                </div>
                                                <div class="table-cell" style="width: 150px; text-align: center; line-height: 20px; vertical-align: middle">
                                                    <asp:Panel ID="panelCardLockButton" runat="server" Visible='<%#bool.Parse(Eval("ShowCardLock").ToString()) %>'>
                                                        <%--<asp:Panel ID="panelCardLockButton" runat="server">--%>
                                                        <input type="button" value="Opposer" class="button" style="width: 155px; margin: 2px 0; height: 25px; line-height: 17px;" onclick="ConfirmAction('cardLock', '<%#Eval("CardNumber")%>    ');" />
                                                        <asp:Button ID="btnMonextActivate" runat="server" Text="Activer Monext" CssClass="button" CommandArgument='<%#Eval("Barcode") %>' OnClick="btnMonextActivate_Click" Style="width: 155px; margin: 2px 0; height: 25px; line-height: 17px;" />
                                                    </asp:Panel>
                                                    <asp:Panel ID="panelCardReplacementButton" runat="server" Visible='<%#bool.Parse(Eval("ShowCardReplacement").ToString()) %>'>
                                                        <input type="button" value="Remplacer" class="button" style="width: 155px; margin: 2px 0" onclick="ConfirmAction('cardReplacement');" />
                                                    </asp:Panel>
                                                </div>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <asp:HiddenField ID="hfCardLock_CardNumber" runat="server" />
                            </div>

                            <asp:Panel ID="panelCardReplacementFix" runat="server">
                                <div class="orange_subtitle">
                                    Pb remplacement carte
                                </div>
                                <div>
                                    <asp:UpdatePanel ID="upCardReplacementFix" runat="server">
                                        <ContentTemplate>
                                            <asp:Button ID="btnCardReplacementFix" runat="server" CssClass="button" Text="Rechercher correctif" OnClick="btnCardReplacementFix_Click" OnClientClick="ShowPanelLoading('divCardsHisto');" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </asp:Panel>

                            <asp:Panel ID="panelOppositionHisto" runat="server" Visible="false" Style="margin-top: 20px;">
                                <div class="orange_subtitle">
                                    Historique des oppositions
                                </div>
                                <asp:Repeater ID="rptOppositionHisto" runat="server">
                                    <HeaderTemplate>
                                        <div class="table defaultTable2" style="width: 100%; border-collapse: collapse">
                                            <div class="table-row head" style="font-weight: bold">
                                                <div class="table-cell" style="width: 170px; vertical-align: middle">
                                                    Date
                                                </div>
                                                <div class="table-cell" style="width: 100px; vertical-align: middle">
                                                    Voie
                                                </div>
                                                <%--<div class="table-cell" style="width:100px;vertical-align:middle">
                                                    Opposée
                                                </div>--%>
                                                <div class="table-cell" style="vertical-align: middle">
                                                    Origine
                                                </div>
                                                <%--<div class="table-cell" style="vertical-align:middle">
                                                    Par
                                                </div>--%>
                                            </div>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <div class="table-row <%# Container.ItemIndex % 2 == 0 ? "default-table-row-style" : "table-alternate-row-style" %>">
                                            <div class="table-cell" style="text-align: center">
                                                <%#Eval("Date") %>
                                            </div>
                                            <div class="table-cell" style="text-align: center; text-transform: uppercase">
                                                <%#Eval("Channel") %>
                                            </div>
                                            <%--<div class="table-cell" style="text-align:center; text-transform:uppercase">
                                                    <%#Eval("Opposed") %>
                                                </div>--%>
                                            <div class="table-cell" style="text-align: center;">
                                                <%# getOrigineAndBoUserLabel(Eval("Origine").ToString(),Eval("BoUser").ToString()) %>
                                            </div>
                                            <%--<div class="table-cell" style="text-align:center">
                                                    <%#Eval("BoUser") %>
                                                </div>--%>
                                        </div>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </div>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </asp:Panel>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnCardLock" EventName="click" />
                            <asp:AsyncPostBackTrigger ControlID="btnCardReplacement" EventName="click" />
                            <asp:AsyncPostBackTrigger ControlID="btnLoadHisto" EventName="click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </asp:Panel>

            <asp:Panel ID="panelPINBySMS" runat="server" Visible="false">
                <div class="orange_subtitle">
                    PIN by SMS
                </div>
                <div style="padding: 5px; margin-bottom: 10px;">
                    <asp:UpdatePanel ID="upPINBySMS" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div id="LimitsPanel" style="width: 49%; float: right">
                                <asp:Panel ID="panelNewPINBySMS" runat="server">
                                    <div style="color: #344b56; font-weight: bold; text-transform: uppercase; border-bottom: 1px solid #344b56">
                                        Nouvel envoi
                                    </div>
                                    <div class="label">
                                        <asp:Label ID="lblSendPINReason" runat="server" AssociatedControlID="ddlSendPINReason">Raison</asp:Label>
                                    </div>
                                    <div>
                                        <asp:DropDownList ID="ddlSendPINReason" runat="server">
                                            <asp:ListItem Value="1">Code Oublié et SMS effacé</asp:ListItem>
                                            <asp:ListItem Value="2">SMS jamais reçu</asp:ListItem>
                                            <asp:ListItem Value="3">Autre</asp:ListItem>
                                        </asp:DropDownList>
                                        <input type="button" value="Renvoyer" onclick="ConfirmAction('PINBySMS');" class="button" />
                                        <span style="position: relative">
                                            <img class="loading" src="Styles/Img/loading.gif" alt="loading" width="25px" height="25px" style="display: none; position: absolute; top: -3px; left: 5px" />
                                        </span>
                                    </div>
                                </asp:Panel>
                            </div>
                            <div style="width: 49%; float: left">
                                <asp:UpdatePanel ID="upPINBySMSHistory" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div style="color: #344b56; font-weight: bold; text-transform: uppercase; border-bottom: 1px solid #344b56">
                                            Historique d'envoi
                                        <asp:ImageButton ID="btnRefreshPINBySMSHistory" runat="server" Text="Refresh" OnClick="btnRefreshPINBySMSHistory_Click" ImageUrl="~/Styles/Img/refresh.png" Width="15px" Style="float: right; position: relative; right: 5px" />
                                        </div>
                                        <div>
                                            <asp:Panel ID="panelNoPINBySMSSent" runat="server" Visible="false">
                                                Aucun code PIN envoyé.
                                            </asp:Panel>
                                            <asp:Repeater ID="rptPINBySMSHistory" runat="server">
                                                <HeaderTemplate>
                                                    <div style="max-height: 90px; overflow: auto; margin-top: 5px;">
                                                        <table style="width: 100%; border-collapse: collapse;">
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td>
                                                            <span class="tooltip" title='<%#Eval("Desc") %>'><%#Eval("Label") %></span>
                                                        </td>
                                                        <td style="text-align: center">
                                                            <%#Eval("Date") %>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </table>
                                                </div>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="btnRefreshPINBySMSHistory" EventName="click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                            <div style="clear: both"></div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnSendPIN" EventName="click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </asp:Panel>

            <asp:Panel ID="panelCardPreferences" runat="server" Visible="false">
                <div class="orange_subtitle">
                    Préférences carte
                </div>
                <asp:UpdatePanel ID="upCardPreference" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table id="card-payment-preference" class="defaultTable2" style="margin-bottom: 20px">
                            <tr class="row-odd">
                                <td style="vertical-align: middle">Interdire tous les paiements
                                </td>
                                <td style="padding: 5px 0 11px 0;">
                                    <div id="toggleBlockAllPayment" class="toggle-soft"></div>
                                    <asp:CheckBox ID="ckbBlockAllPayment" runat="server" Style="display: none" />
                                    <asp:Button ID="btnUpdateBlockAllPayment" runat="server" OnClick="btnUpdateCardPreference_Click" Style="display: none" />
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: middle">Interdire les paiements hors France métropolitaine
                                </td>
                                <td style="padding: 5px 0 11px 0;">
                                    <div id="toggleBlockOutFrancePayment" class="toggle-soft"></div>
                                    <asp:CheckBox ID="ckbBlockOutFrancePayment" runat="server" Style="display: none" />
                                    <asp:Button ID="btnUpdateBlockOutFrancePayment" runat="server" OnClick="btnUpdateCardPreference_Click" Style="display: none" />
                                </td>
                            </tr>
                            <tr class="row-odd">
                                <td style="vertical-align: middle">Interdire les paiements VAD et VADS (internet)
                                </td>
                                <td style="padding: 5px 0 11px 0;">
                                    <div id="toggleBlockInternetPayment" class="toggle-soft"></div>
                                    <asp:CheckBox ID="ckbBlockInternetPayment" runat="server" Style="display: none" />
                                    <asp:Button ID="btnUpdateBlockInternetPayment" runat="server" OnClick="btnUpdateCardPreference_Click" Style="display: none" />
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnUpdateBlockAllPayment" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="btnUpdateBlockOutFrancePayment" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="btnUpdateBlockInternetPayment" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </asp:Panel>

            <asp:Panel ID="panelLimitsContainer" runat="server" Visible="false">
                <div class="orange_subtitle">
                    Limites
                </div>
                <asp:UpdatePanel ID="upShowLimits" runat="server">
                    <ContentTemplate>
                        <div class="table">
                            <div class="row">
                                <div class="cell" style="vertical-align: middle">
                                    <asp:Button ID="btnLoadLimits" runat="server" CssClass="button" Text="Afficher les limites" OnClick="btnLoadLimits_Click" OnClientClick="$('#imgLimitsLoad').show();" />
                                </div>
                                <div class="cell" style="padding-left: 5px; vertical-align: middle">
                                    <img id="imgLimitsLoad" src="Styles/Img/loading.gif" alt="loading..." style="display: none; height: 20px;" />
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnLoadLimits" EventName="click" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:Panel ID="panelLimits" runat="server" Style="padding: 0 5px;">
                    <asp:UpdatePanel ID="upLimits" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Panel ID="panelLimitsToLoad" runat="server" Visible="false" Style="margin-top: 5px;">
                                <div id="current-limits" style="width: 52%; float: left;">
                                    <div style="color: #344b56; font-weight: bold; text-transform: uppercase; border-bottom: 1px solid #344b56;">
                                        en cours
                                    </div>
                                    <div style="margin-top: 10px">
                                        <asp:Panel ID="panelCardPayment" runat="server">
                                            <div class="uppercase font-bold font-orange" style="font-size: 1.2em">
                                                <div style="float: left; width: 80%">
                                                    Votre limite de paiement en carte /<asp:Label ID="lblPaymentLimitPeriod" runat="server"></asp:Label>
                                                    jours :
                                                </div>
                                                <div style="float: right; width: 20%; text-align: right">
                                                    <asp:HiddenField ID="hdnCardPaymentLimit" runat="server" />
                                                    <asp:Label ID="lblCardPaymentLimit" runat="server" CssClass="font-bold" ForeColor="Black"></asp:Label>
                                                </div>
                                                <div style="clear: both"></div>
                                            </div>
                                            <asp:Panel ID="panelCardPaymentSituation" runat="server" Visible="false" Style="margin-top: 5px">
                                                <asp:HiddenField ID="hdnCardPaymentForNow" runat="server" />
                                                <div style="margin-bottom: 10px">
                                                    Vous avez dépensé :
                                                </div>
                                                <div id="CardPaymentProgress"></div>
                                                <div style="text-align: right; margin-top: 10px">
                                                    Vous pouvez encore dépenser :
                                                    <asp:Label ID="lblCardPaymentAvailable" runat="server" CssClass="font-bold"></asp:Label>
                                                    <span class="font-bold">&euro;</span>
                                                </div>
                                                <div style="text-align: right">
                                                    <span style="font-size: 0.6em">(sous réserve de provisions disponibles)</span>
                                                </div>
                                            </asp:Panel>
                                        </asp:Panel>

                                        <asp:Panel ID="panelCashWithdraw" runat="server" Style="margin-top: 20px">
                                            <div class="uppercase font-bold font-orange" style="font-size: 1.2em">
                                                <div style="float: left; width: 80%">
                                                    Votre limite de retrait /<asp:Label ID="lblWithdrawLimitPeriod" runat="server"></asp:Label>
                                                    jours :
                                                </div>
                                                <div style="float: right; width: 20%; text-align: right">
                                                    <asp:HiddenField ID="hdnWithdrawLimit" runat="server" />
                                                    <asp:Label ID="lblWithdrawLimit" runat="server" CssClass="font-bold" ForeColor="Black"></asp:Label>
                                                </div>
                                                <div style="clear: both"></div>
                                            </div>
                                            <asp:Panel ID="panelWithdrawSituation" runat="server" Visible="false" Style="margin-top: 5px">
                                                <asp:HiddenField ID="hdnWithdrawForNow" runat="server" />
                                                <div style="margin-bottom: 10px">
                                                    Vous avez retiré :
                                                </div>
                                                <div id="WithdrawProgress"></div>
                                                <div style="text-align: right; margin-top: 10px">
                                                    Vous pouvez encore retirer :
                                                    <asp:Label ID="lblWithdrawAvailable" runat="server" CssClass="font-bold"></asp:Label>
                                                    <span class="font-bold">&euro;</span>
                                                </div>
                                                <div style="text-align: right">
                                                    <span style="font-size: 0.6em">(sous réserve de provisions disponibles)</span>
                                                </div>
                                            </asp:Panel>
                                        </asp:Panel>

                                        <asp:Panel ID="panelCashDeposit" runat="server" Style="margin-top: 20px">
                                            <div class="uppercase font-bold font-orange" style="font-size: 1.2em">
                                                <div style="float: left; width: 80%">
                                                    Votre limite de dépôt /<asp:Label ID="lblCashDepositLimitPeriod" runat="server"></asp:Label>
                                                    jours :
                                                </div>
                                                <div style="float: right; width: 20%; text-align: right">
                                                    <asp:HiddenField ID="hdnCashDepositLimit" runat="server" />
                                                    <asp:Label ID="lblCashDepositLimit" runat="server" CssClass="font-bold" ForeColor="Black"></asp:Label>
                                                </div>
                                                <div style="clear: both"></div>
                                            </div>
                                            <asp:Panel ID="panelCashDepositSituation" runat="server" Visible="false" Style="margin-top: 5px">
                                                <asp:HiddenField ID="hdnCashDepositForNow" runat="server" />
                                                <div style="margin-bottom: 10px">
                                                    Vous avez déposé :
                                                </div>
                                                <div id="CashDepositProgress"></div>
                                                <div style="text-align: right; margin-top: 10px">
                                                    Vous pouvez encore déposer :
                                                    <asp:Label ID="lblCashDepositAvailable" runat="server" CssClass="font-bold"></asp:Label>
                                                    <span class="font-bold">&euro;</span>
                                                </div>
                                                <%--                                            <div style="text-align:right">
                                                    <span style="font-size:0.6em">(sous réserve de provisions disponibles)</span>
                                                </div>--%>
                                            </asp:Panel>
                                        </asp:Panel>
                                    </div>
                                    <asp:Panel ID="panelChangeLimit" runat="server" Visible="false" style="text-align:center;margin-top:15px">
                                        <input type="button" value="Modifier les limites" onclick="ShowChangeLimitDialog();" class="button" />
                                    </asp:Panel>

                                </div>

                                <div style="width: 46%; float: right;">
                                    <asp:HiddenField ID="hdnRefLimitRequest" runat="server" />
                                    <asp:HiddenField ID="hdnLimitRequestAction" runat="server" />

                                    <div style="color: #344b56; font-weight: bold; text-transform: uppercase; border-bottom: 1px solid #344b56;">
                                        Demandes en attente
                                    </div>
                                    <asp:Panel ID="panelNoLimitRequest" runat="server" Visible="false" Style="margin-top: 10px">
                                        Aucune demande en attente.
                                    </asp:Panel>
                                    <asp:Repeater ID="rptLimitRequests" runat="server">
                                        <HeaderTemplate>
                                            <table style="width: 100%; border-collapse: collapse; margin-top: 10px">
                                                <tr>
                                                    <th>Type</th>
                                                    <th>Montant</th>
                                                    <th>Date</th>
                                                    <th style="width: 1px; min-width: 150px">Actions</th>
                                                </tr>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <%#Eval("Type") %>
                                                </td>
                                                <td>
                                                    <%#Eval("Amount") %> &euro;
                                                </td>
                                                <td style="text-align: center">
                                                    <%#Eval("Date") %>
                                                </td>
                                                <td style="text-align: center; white-space: nowrap">
                                                    <asp:Panel ID="panelLimitRequestsActions" runat="server">
                                                        <%# CreateAcceptRefuseLimitRequestBtn(Eval("Ref").ToString())%>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                            </asp:Panel>
                            <div style="clear: both"></div>

                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnAcceptRefuseLimitRequest" EventName="click" />
                        </Triggers>
                    </asp:UpdatePanel>

                    <div id="dialog-change-limit" style="display:none">
                        <asp:UpdatePanel ID="upChangeLimit" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div style="display:flex;justify-content:space-between">
                                    <div>
                                        <asp:Label ID="lblPaymentLimits" runat="server" AssociatedControlID="ddlCardPaymentLimits">
                                            de paiement carte :
                                        </asp:Label>
                                    </div>
                                    <div>
                                        <asp:DropDownList ID="ddlCardPaymentLimits" runat="server" DataValueField="Value" DataTextField="Text" style="width:100px"></asp:DropDownList>
                                    </div>
                                </div>              
                                <div style="margin-top:5px;display:flex;justify-content:space-between">
                                    <div>
                                        <asp:Label ID="lblWithdrawLimits" runat="server" AssociatedControlID="ddlWithdrawLimits">
                                            de retrait carte :
                                        </asp:Label>
                                    </div>
                                    <div>
                                        <asp:DropDownList ID="ddlWithdrawLimits" runat="server" DataValueField="Value" DataTextField="Text" style="width:100px"></asp:DropDownList>
                                    </div>
                                </div>
                                <asp:Button ID="btnChangeLimit" runat="server" OnClick="btnChangeLimit_Click" style="display:none" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>

                </asp:Panel>
            </asp:Panel>

            <asp:Panel ID="panelPersonalizedCardOrderList" runat="server" Visible="false">
                <div class="orange_subtitle" style="margin-top: 10px">
                    Carte personnalisée
                </div>
                <asp:PersonalizedCardOrderList ID="PersonalizedCardOrderList1" runat="server" />
            </asp:Panel>
            <%--<asp:Panel ID="panelCardLock" runat="server" style="margin-bottom: 10px;">
                <div style="font-size:1.2em;color:#f57527;font-weight:bold;text-transform:uppercase;border-bottom:1px solid #f57527; margin-top:20px; margin-bottom:5px;">
                    Opposition
                </div>
                <asp:UpdatePanel ID="upCardLock" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Panel ID="panel2" runat="server" style="margin-top:5px;">
                            <input type="button" value="Opposer la carte du client" class="button" onclick="ConfirmAction('cardLock');" style="position:relative" />
                            <span style="position:relative"><img class="loading" src="Styles/Img/loading.gif" alt="loading" width="25px" height="25px" style="display:none;position:absolute;top:-3px;left:5px" /></span>
                        </asp:Panel>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnCardLock" EventName="click" />
                    </Triggers>
                </asp:UpdatePanel>
            </asp:Panel>

            <asp:Panel ID="panelCardReplacement" runat="server" style="margin-bottom: 10px;">
                <div style="font-size:1.2em;color:#f57527;font-weight:bold;text-transform:uppercase;border-bottom:1px solid #f57527">
                    Remplacement Carte
                </div>
                <asp:UpdatePanel ID="upCardReplacement" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Panel ID="panel1" runat="server" style="margin-top:5px;">
                            <input type="button" value="Remplacer" class="button" onclick="ConfirmAction('cardReplacement');" style="position:relative" />
                            <span style="position:relative"><img class="loading" src="Styles/Img/loading.gif" alt="loading" width="25px" height="25px" style="display:none;position:absolute;top:-3px;left:5px" /></span>
                        </asp:Panel>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnCardReplacement" EventName="click" />
                    </Triggers>
                </asp:UpdatePanel>
            </asp:Panel>--%>
        </asp:Panel>
        <asp:Panel ID="panelWeb" runat="server">
            <asp:Panel ID="panelIDWebHisto" runat="server" Visible="false">
                <div style="color: #344b56; font-weight: bold; text-transform: uppercase; border-bottom: 1px solid #344b56; margin: 10px 0 10px 0">
                    Historique de modification
                </div>
                <div style="max-height: 300px; overflow: auto">
                    <asp:Panel ID="panelHistoryChangeID" runat="server">
                        <asp:Repeater ID="rptHistoryChangeID" runat="server">
                            <HeaderTemplate>
                                <table class="defaultTable2">
                                    <tr>
                                        <th style="width: 1px">Date</th>
                                        <th>Type</th>
                                        <th>Ancienne valeur</th>
                                        <th>Nouvelle valeur</th>
                                        <th>Source</th>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr class="<%# Container.ItemIndex % 2 == 0 ? "" : "alternate-row" %>">
                                    <td style="text-align: center">
                                        <asp:Literal ID="litDate" runat="server" Text='<%#Eval("Date") %>'></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="litKind" runat="server" Text='<%#Eval("Kind") %>'></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="litOldValue" runat="server" Text='<%#Eval("OldValue") %>'></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="litNewValue" runat="server" Text='<%#Eval("NewValue") %>'></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="litChannel" runat="server" Text='<%#Eval("Channel") %>'></asp:Literal>
                                        <asp:Literal ID="litBOUser" runat="server" Text='<%#(Eval("BOUser").ToString().Length > 0) ? "("+Eval("BOUser").ToString() + ")" : "" %>'></asp:Literal>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </asp:Panel>
                </div>
            </asp:Panel>
            <asp:Panel ID="panelIpConnectionList" runat="server" Visible="false">
                <asp:UpdatePanel ID="upIpConnectionList" runat="server">
                    <ContentTemplate>
                        <div id="divIdConnectionList">
                            <div style="display: table; width: 100%; color: #344b56; font-weight: bold; text-transform: uppercase; border-bottom: 1px solid #344b56; margin: 30px 0 10px 0">
                                <div class="row">
                                    <div class="cell" style="width: 100%">Liste des IP</div>
                                    <div class="cell">
                                        <asp:RadioButtonList ID="rblIpConnectionListType" runat="server" RepeatDirection="Horizontal" CssClass="RadioButtonList" AutoPostBack="true" OnSelectedIndexChanged="rblIpConnectionListType_SelectedIndexChanged" onchange="ShowPanelLoading('divIdConnectionList');">
                                            <asp:ListItem Value="FIRSTLAST" Selected="True">Première/Dernière</asp:ListItem>
                                            <asp:ListItem Value="ALL">Toutes</asp:ListItem>
                                            <asp:ListItem Value="SUMMARY">Résumé</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                            </div>
                            <div style="max-height: 300px; overflow: auto">
                                <asp:Button ID="btnBindIPLocation" runat="server" OnClick="btnBindIPLocation_Click" style="display:none" />
                                <asp:IpConnectionList ID="IpConnectionList1" runat="server" />
                            </div>
                            <div style="margin-top: 10px; width: 100%; text-align: right;">
                                <asp:Button ID="btnIpListExcelExport" runat="server" CssClass="button excel-button" Text="Export" OnClick="btnIpListExcelExport_Click" />
                            </div>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnIpListExcelExport" />
                    </Triggers>
                </asp:UpdatePanel>
            </asp:Panel>
            
            <asp:Panel ID="panelWebAccess" runat="server">
                <div style="color: #344b56; font-weight: bold; text-transform: uppercase; border-bottom: 1px solid #344b56; margin: 30px 0 10px 0">
                    Accès web
                </div>
                <asp:Panel ID="panelWEBID" runat="server">
                    <div id="WEBIDPanel" style="padding: 10px; margin-bottom: 10px;">
                        <asp:UpdatePanel ID="upWEBID" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:Panel ID="panelSendWebIdByEmail" runat="server">
                                    <input type="button" value="Envoyer l'identifiant par email" onclick="ConfirmAction('sendWEBID');" class="button" style="position: relative" />
                                    <span style="position: relative">
                                        <img class="loading" src="Styles/Img/loading.gif" alt="loading" width="25px" height="25px" style="display: none; position: absolute; top: -3px; left: 5px" />
                                    </span>
                                </asp:Panel>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnSendWEBID" EventName="click" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </asp:Panel>
                <asp:Panel ID="panelWEBPassword" runat="server">
                    <div style="padding: 10px; margin-bottom: 10px;">
                        <asp:UpdatePanel ID="upWEBPassword" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:Panel ID="panelResetWebPassword" runat="server">
                                    <input type="button" value="Réinitialiser le mot de passe" class="button" onclick="ConfirmAction('resetWEBPassword');" style="position: relative" />
                                    <span style="position: relative">
                                        <img class="loading" src="Styles/Img/loading.gif" alt="loading" width="25px" height="25px" style="display: none; position: absolute; top: -3px; left: 5px" />
                                    </span>
                                </asp:Panel>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnResetWEBPassword" EventName="click" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </asp:Panel>
                <asp:Panel ID="panelLockUnlock" runat="server" Style="padding: 10px; margin-bottom: 10px;">
                    <asp:UpdatePanel ID="upLock" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="panelLockUnlockAction" runat="server">
                                <div class="table">
                                    <div class="row">
                                        <div class="cell" style="vertical-align: middle;">
                                            <asp:Button ID="btnShowLockUnlockDialog" runat="server" Text="Bloquer" CssClass="button"
                                                OnClientClick="ConfirmAction('lockUnlock'); return false;" Style="display: inline-block" />
                                            <asp:HiddenField ID="hfLockUnlockStatus" runat="server" />
                                        </div>
                                        <div class="cell" style="vertical-align: middle">
                                            <img class="loading" src="Styles/Img/loading.gif" alt="loading" width="25px" height="25px"
                                                style="display: none;" />
                                        </div>
                                        <div id="divLockInfo" runat="server" class="cell" style="vertical-align: middle; padding-left: 50px;">
                                            <div class="table" style="border: 1px solid #344b56;">
                                                <div class="row">
                                                    <div class="cell" style="background-color: #344b56; color: #fff; padding: 5px 10px;">Statut</div>
                                                    <div class="cell" style="background-color: #344b56; color: #fff; padding: 5px 10px; width: 450px;">Raison</div>
                                                </div>
                                                <div class="row">
                                                    <div class="cell" style="padding: 5px 10px; border: 1px solid #344b56">
                                                        <asp:Label ID="lblLockedDate" runat="server"></asp:Label>
                                                    </div>
                                                    <div class="cell" style="padding: 5px 10px; border: 1px solid #344b56">
                                                        <asp:Label ID="lblLockReason" runat="server"></asp:Label>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <span style="position: relative"></span>
                            </asp:Panel>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnLockUnlock" EventName="click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </asp:Panel>
            </asp:Panel>
        </asp:Panel>
        <asp:Panel ID="panelDocs" runat="server">
            <h4 id="DocsTitle" class="AccordionTitle" style="display: none">
                <img id="DocsArrow" alt="" src="" />
                Documents
            </h4>
            <div id="DocsPanel" style="padding: 10px; margin-bottom: 10px;">
                <asp:UpdatePanel ID="upDocs" runat="server">
                    <ContentTemplate>
                        <div style="color: #344b56; font-weight: bold; text-transform: uppercase; border-bottom: 1px solid #344b56">
                            Pièce d'identité
                        </div>
                        <asp:HiddenField ID="hdnRefDocID" runat="server" />
                        <div class="table" style="width: 100%; border-collapse: collapse">
                            <div class="row">
                                <div class="cell" style="width: 33%">
                                    <div class="label">
                                        <asp:Label ID="lblNumberID" runat="server" AssociatedControlID="txtNumberID">N° document</asp:Label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtNumberID" runat="server" Style="width: 90%"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="cell"></div>
                            </div>
                            <div class="row">
                                <div class="cell" style="width: 50%">
                                    <div class="label">
                                        <asp:Label ID="lblDeliveryDateID" runat="server" AssociatedControlID="txtDeliveryDateID">Date d'émission</asp:Label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtDeliveryDateID" runat="server" Style="width: 90%"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="cell" style="width: 50%">
                                    <div class="label">
                                        <asp:Label ID="lblExpireDateID" runat="server" AssociatedControlID="txtExpireDateID">Date d'expiration</asp:Label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtExpireDateID" runat="server" Style="width: 90%"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div style="color: #344b56; font-weight: bold; text-transform: uppercase; border-bottom: 1px solid #344b56; margin-top: 30px">
                            Justificatif de domicile
                        </div>
                        <asp:HiddenField ID="hdnRefDocHO" runat="server" />
                        <div class="table" style="width: 100%; border-collapse: collapse">
                            <div class="row">
                                <div class="cell" style="width: 50%">
                                    <div class="label">
                                        <asp:Label ID="lblDeliveryDateHO" runat="server" AssociatedControlID="txtDeliveryDateHO">Date d'émission</asp:Label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtDeliveryDateHO" runat="server" Style="width: 90%"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="cell" style="width: 50%">
                                    <div class="label">
                                        <asp:Label ID="lblExpireDateHO" runat="server" AssociatedControlID="txtExpireDateHO">Date d'expiration</asp:Label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtExpireDateHO" runat="server" Style="width: 90%"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <asp:Panel ID="panelSaveDocs" runat="server" Style="margin-top: 30px; text-align: center; position: relative">
                            <input type="button" value="Enregistrer" onclick="ConfirmAction('DocsInfo');" class="button" />
                            <img class="loading" src="Styles/Img/loading.gif" alt="loading" width="25px" height="25px" style="display: none; position: absolute; top: 1px; left: 57%" />
                        </asp:Panel>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSaveDocsInfo" EventName="click" />
                    </Triggers>
                </asp:UpdatePanel>

                <div id="div-other-docs">
                    <asp:UpdatePanel ID="upOtherDocs" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                        <ContentTemplate>
                            <div style="color: #344b56; font-weight: bold; text-transform: uppercase; border-bottom: 1px solid #344b56; margin-top: 30px">
                                Tous les fichiers
                            </div>
                            <asp:ClientFileManager ID="clientFileManager1" runat="server" AllowCheck="true" />
                            <div style="margin-top: 10px; padding: 10px 10px 0 10px; border: 1px solid #ddd;">
                                <asp:ClientFileUpload ID="clientFileUpload1" runat="server" />
                            </div>
                            <asp:Button ID="btnLoadOtherDocs" runat="server" OnClick="btnLoadOtherDocs_Click" Style="display: none" />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnLoadOtherDocs" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>

            </div>
        </asp:Panel>
        <asp:Panel ID="panelBalanceOperations" runat="server">
            <asp:Panel ID="panelBalanceOnly" runat="server" Visible="false">
                <asp:Balance ID="Balance1" runat="server" />
            </asp:Panel>
            <asp:Panel ID="panelBalanceOperationsLink" runat="server" Visible="false">
                <div><a onclick="balanceOperation();" class="linkNoStyle">Afficher le solde/opérations</a></div>
                <asp:Panel ID="panelSABCobaltOpeComparison" runat="server" Visible="false" style="margin-top: 5px;">
                    <asp:UpdatePanel ID="upLnkSABCobaltOpeComparison" runat="server">
                        <ContentTemplate>
                            <asp:LinkButton ID="lnkSABCobaltOpeComparison" runat="server" OnClick="lnkSABCobaltOpeComparison_Click" CssClass="sab-cobalt-comparison-link">Comparer les opérations Cobalt/SAB</asp:LinkButton>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:Panel>
                <div style="margin-top: 5px;"><a onclick="authorizations();" class="linkNoStyle">Afficher les autorisations/refus Monext</a></div>
                <div style="margin-top: 5px;"><a onclick="rejectsSAB();" class="linkNoStyle">Afficher les refus SAB</a></div>
                <div style="margin-top: 5px;"><a onclick="transfers();" class="linkNoStyle">Afficher les virements entrants</a></div>
                <div style="margin-top: 5px;"><a onclick="debits();" class="linkNoStyle">Afficher les prélèvements</a></div>
            </asp:Panel>
            <asp:Panel ID="panelBankStatement" runat="server" Visible="false" Style="margin-top: 5px;">
                <a onclick="releves();" class="linkNoStyle">Afficher les relevés</a>
            </asp:Panel>
            <asp:Panel ID="pGestionMandat" runat="server" Visible="false" Style="margin-top: 5px;">
                <a onclick="mandats();" class="linkNoStyle">Afficher les mandats</a>
            </asp:Panel>
            <asp:Panel ID="panelReturnFunds" runat="server" Visible="false">
                <asp:ReturnFundsList ID="returnFundsList1" runat="server" ShowLabel="true" />
                <asp:LinkButton ID="btnReturnFunds" runat="server" CssClass="MiniButton" Visible="false" style="text-transform:lowercase;width:160px;display:inline-block;">Effectuer un virement</asp:LinkButton>
            </asp:Panel>
        </asp:Panel>
        <asp:Panel ID="panelMessageExchange" runat="server">
            <asp:Panel ID="panelSMS" runat="server" Style="padding: 10px; margin-bottom: 10px;">

                <div style="color: #344b56; font-weight: bold; text-transform: uppercase; border-bottom: 1px solid #344b56; margin-bottom: 10px">
                    SMS
                </div>

                <div style="width: 50%">
                    <asp:SmsGage ID="SmsGage1" runat="server" />
                </div>

                <asp:UpdatePanel ID="upSMSHistory" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Panel ID="panelNoSMSHistory" runat="server" Visible="false">
                            Aucun SMS reçu / envoyé
                            <asp:ImageButton ID="btnRefreshSMSHistory" runat="server" Text="Refresh" OnClick="btnRefreshSMSHistory_Click" ImageUrl="~/Styles/Img/refresh.png" Width="15px" Style="position: relative; right: 0; top: 2px" />
                        </asp:Panel>
                        <asp:Repeater ID="rptSMSHistory" runat="server">
                            <HeaderTemplate>
                                <div id="SMSPanel" style="max-height: 300px; overflow: auto">
                                    <table class="defaultTable" style="width: 99%; border-collapse: collapse;">
                                        <tr>
                                            <th style="height: 30px">Contenu</th>
                                            <th>Statut</th>
                                            <th>Date</th>
                                            <th>
                                                <asp:ImageButton ID="btnRefreshSMSHistory" runat="server" Text="Refresh" OnClick="btnRefreshSMSHistory_Click" ImageUrl="~/Styles/Img/refresh.png" Width="15px" Style="position: relative; right: 5px; top: 2px" />
                                            </th>
                                        </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <%#Eval("Content") %>
                                    </td>
                                    <td style="width: 20%">
                                        <%#Eval("Type") %>
                                    </td>
                                    <td style="text-align: center; width: 30%">
                                        <%#Eval("Date") %>
                                    </td>
                                    <td style="width: 1px;"></td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                                </div>
                            </FooterTemplate>
                        </asp:Repeater>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnRefreshSMSHistory" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </asp:Panel>

            <asp:Panel ID="panelWebMessage" runat="server" Visible="false" Style="padding: 10px; margin-bottom: 10px;">

                <div style="color: #344b56; font-weight: bold; text-transform: uppercase; border-bottom: 1px solid #344b56; margin: 10px 0">
                    Mon Compte-Nickel
                </div>
                <asp:UpdatePanel ID="upWebMessage" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>

                        <asp:Panel ID="panelNoWebMessage" runat="server" Visible="true">
                            Aucun message web envoyé
                            <asp:ImageButton ID="btnRefreshWebMessageHistory" runat="server" Text="Refresh" OnClick="btnRefreshWebMessageHistory_Click" ImageUrl="~/Styles/Img/refresh.png" Style="position: relative; right: 0; top: 2px" Width="15px" />
                        </asp:Panel>

                        <asp:Repeater ID="rptWebMessage" runat="server">
                            <HeaderTemplate>
                                <div id="WebMessagePanel" style="max-height: 300px; overflow: auto; text-transform: none">
                                    <table class="defaultTable" style="width: 99%; border-collapse: collapse;">
                                        <tr>
                                            <th>Titre</th>
                                            <th>Contenu</th>
                                            <th>Statut</th>
                                            <th>Date</th>
                                            <th>
                                                <asp:ImageButton ID="btnRefreshWebMessageHistory" runat="server" Text="Refresh" OnClick="btnRefreshWebMessageHistory_Click" ImageUrl="~/Styles/Img/refresh.png" Width="15px" Style="position: relative; right: 5px; top: 2px" />
                                            </th>
                                        </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <%#Eval("Title") %>
                                    </td>
                                    <td>
                                        <%#Eval("Content") %>
                                    </td>
                                    <td style="width: 20%; text-align: center">
                                        <%#Eval("Status") %>
                                    </td>
                                    <td style="width: 1px; text-align: center">
                                        <%#Eval("Date") %>
                                    </td>
                                    <td style="width: 1px;">
                                        <asp:CheckBox ID="ckbWebMessage" runat="server" />
                                        <asp:HiddenField ID="hdnRefMessage" runat="server" Value='<%#Eval("RefMessage") %>' />
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                                </div>
                            </FooterTemplate>
                        </asp:Repeater>

                        <asp:Panel ID="panelDeleteWebMessage" runat="server" Style="text-align: right; margin-top: 10px">
                            <span style="position: relative">
                                <img class="loading" src="Styles/Img/loading.gif" alt="loading" width="25px" height="25px" style="display: none; position: absolute; top: -3px; right: 5px" />
                            </span>
                            <input type="button" value="Supprimer" class="button" onclick="ConfirmAction('deleteWebMessage');" />
                        </asp:Panel>

                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnRefreshWebMessageHistory" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="btnDeleteWebMessage" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>

            </asp:Panel>
        </asp:Panel>
        <asp:Panel ID="panelRib" runat="server">
            <div id="RibPanel" style="padding: 10px 10px 10px 0; margin-bottom: 10px;">
                <div style="height: 40px; margin-left: 50px;" class="table">
                    <div class="row">
                        <div class="cell" style="vertical-align: middle">
                            <input type="button" onclick="RibRefresh();" class="button" value="Rafraichir" />
                        </div>
                        <div class="cell" style="padding-left: 10px; vertical-align: middle">
                            <asp:UpdateProgress ID="updateProgressRib" runat="server" AssociatedUpdatePanelID="upRib">
                                <ProgressTemplate>
                                    <img src="Styles/Img/loading.gif" alt="loading" width="25px" height="25px" />
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                        </div>
                    </div>
                </div>

                <asp:UpdatePanel ID="upRib" runat="server" UpdateMode="Conditional" style="padding: 0;">
                    <ContentTemplate>
                        <asp:Panel ID="panelPrintRib" runat="server" Style="width: 90%; margin: auto;">
                            <div class="rib">
                                <div style="display: table; width: 100%; margin-top: 20px; background-color: #fff">
                                    <div style="display: table-row">
                                        <div style="display: table-cell; width: 150px">
                                            <img alt="" src="./Styles/Img/logo-nickel.png" style="width: 120px" />
                                        </div>
                                        <div class="ribTitle" style="display: table-cell; vertical-align: middle; text-align: center; padding-right: 20px">
                                            <span style="position: relative; left: -60px">Relevé d'identité bancaire</span>
                                        </div>
                                    </div>
                                </div>
                                <div style="display: table; width: 90%; margin: 20px auto;">
                                    <div style="display: table-row">
                                        <div style="display: table-cell; width: 50%;">
                                            <div class="rib-box" style="padding-right: 10px">
                                                <div class="rib-box-title">Domiciliation</div>
                                                <div class="rib-box-content">
                                                    <div>
                                                        <asp:Label ID="lblRIBAgencyName" runat="server"></asp:Label>
                                                    </div>
                                                    <div>
                                                        <asp:Label ID="lblRIBAgencyAddress" runat="server"></asp:Label>
                                                    </div>
                                                    <div>
                                                        <asp:Label ID="lblRIBAgencyZipcode" runat="server"></asp:Label>
                                                        <asp:Label ID="lblRIBAgencyCity" runat="server"></asp:Label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="display: table-cell; width: 50%">
                                            <div class="rib-box" style="padding-left: 10px">
                                                <div class="rib-box-title">Titulaire</div>
                                                <div class="rib-box-content">
                                                    <div>
                                                        <asp:Label ID="lblRIBClientCivility" runat="server"></asp:Label>
                                                        <asp:Label ID="lblRIBClientLastName" runat="server"></asp:Label>
                                                        <asp:Label ID="lblRIBClientFirstName" runat="server"></asp:Label>
                                                    </div>
                                                    <div>
                                                        <asp:Label ID="lblRIBClientAddress" runat="server"></asp:Label>
                                                    </div>
                                                    <div>
                                                        <asp:Label ID="lblRIBClientZipcode" runat="server"></asp:Label>
                                                        <asp:Label ID="lblRIBClientCity" runat="server"></asp:Label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style="width: 90%; margin: 20px auto">
                                    <div style="display: table; width: 100%">
                                        <div style="display: table-row" class="rib-row-title">
                                            <div style="display: table-cell; width: 27%">
                                                Code banque
                                            </div>
                                            <div style="display: table-cell; width: 27%">
                                                Code guichet
                                            </div>
                                            <div style="display: table-cell; width: 36%">
                                                N° de compte
                                            </div>
                                            <div style="display: table-cell; width: 10%">
                                                Clé
                                            </div>
                                        </div>
                                        <div style="display: table-row" class="rib-row-content">
                                            <div style="display: table-cell; width: 27%; background-color: #ECECEC;">
                                                <asp:Label ID="lblRIBBankCode" runat="server"></asp:Label>
                                            </div>
                                            <div style="display: table-cell; width: 27%; background-color: #ECECEC; border-left: 2px solid #ffffff">
                                                <asp:Label ID="lblRIBBranchCode" runat="server"></asp:Label>
                                            </div>
                                            <div style="display: table-cell; width: 36%; background-color: #ECECEC; border-left: 2px solid #ffffff">
                                                <asp:Label ID="lblRIBAccountNumber" runat="server"></asp:Label>
                                            </div>
                                            <div style="display: table-cell; width: 10%; background-color: #ECECEC; border-left: 2px solid #ffffff">
                                                <asp:Label ID="lblRIBKey" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="display: table; width: 100%; margin-top: 5px">
                                        <div style="display: table-row" class="rib-row-title">
                                            <div style="display: table-cell; width: 100%; text-align: left; padding-left: 15px;">
                                                B.I.C./SWIFT
                                            </div>
                                        </div>
                                        <div style="display: table-row" class="rib-row-content">
                                            <div style="display: table-cell; width: 100%; background-color: #ECECEC">
                                                <asp:Label ID="lblRIBBic" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="display: table; width: 100%; margin-top: 5px">
                                        <div style="display: table-row" class="rib-row-title">
                                            <div style="display: table-cell; width: 100%; text-align: left; padding-left: 15px">
                                                IBAN (Internationnal Bank Account Number)
                                            </div>
                                        </div>
                                        <div style="display: table-row" class="rib-row-content">
                                            <div style="display: table-cell; width: 100%; background-color: #ECECEC">
                                                <asp:Label ID="lblRIBIban" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="rib-footer">
                                    <div>FINANCIERE DES PAIEMENTS ELECTRONIQUES S.A.S. au capital de 468 211,00 euros,</div>
                                    <div>RCS Créteil B 753 886 092, TVA intracommunautaire FR80753886092</div>
                                    <div>Siège social 18 avenue Winston Churchill, 94220 Charenton-le-Pont</div>
                                </div>
                            </div>
                            <div>

                            </div>
                            <asp:Panel ID="panelRibButtons" runat="server" Style="height: 50px;">
                                <div style="margin-top: 20px; position: relative; right: -10px; float: right;">
                                    <div class="table">
                                        <div class="row">
                                            <div class="cell" style="vertical-align: middle">
                                                <img class="loading" src="Styles/Img/loading.gif" alt="loading" width="25px" height="25px" style="display: none;" />
                                            </div>
                                            <div class="cell" style="padding-left: 10px; vertical-align: middle">
                                                <input type="button" class="button" value="Envoyer par mail" onclick="ConfirmAction('sendRibByMail');" />
                                            </div>
                                            <div class="cell" style="padding-left: 10px; vertical-align: middle">
                                                <asp:Button ID="btnPrintRib" runat="server" CssClass="button" Text="Imprimer" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="panelRibSendMailResult" runat="server" Visible="false">
                                <asp:Label ID="lblRibSendMailResult" runat="server"></asp:Label>
                            </asp:Panel>
                        </asp:Panel>
                        <asp:Button ID="btnRibRefresh" runat="server" OnClick="btnRibRefresh_Click" CssClass="button" Text="Rafraichir" Style="display: none" />
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnRibSendMail" EventName="click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </asp:Panel>
        <asp:Panel ID="panelAML" runat="server">
            <script type="text/javascript">
                $(function () {
                    InitAML();
                });
                function showAddClientSpecificities() {
                    if ($('#divAddClientSpecificities').is(':visible')) {
                        $('#divAddClientSpecificities').slideUp(function () {
                        });

                    }
                    else {
                        $('#divAddClientSpecificities').slideDown();
                    }
                }
                function showAddClientNote() {
                    //console.log($('#divAddClientNote').is(':visible'));
                    if ($('#divAddClientNote').is(':visible')) {
                        $('#divAddClientNote').slideUp(function () {
                        });
                    }
                    else {
                        $('#divAddClientNote').slideDown();
                        $('#<%=txtClientNote.ClientID%>').focus();
                    }
                }

                function checkClientSpecificity() {
                    var isOK = false;

                    if ($('#<%=ddlSpecificityList.ClientID%>').val().trim().length > 0)
                        isOK = true;
                    else {
                        showAlertError('Veuillez sélectionner une spécificité.');
                    }

                    return isOK;
                }

                function confirmDelClientSpecificities() {
                    var showConfirm = false;

                    $(".checkbox_specificities input").each(function () {
                        if ($(this).attr("checked") == "checked")
                            showConfirm = true;
                    })

                    if (showConfirm)
                        $('#popup-delClientSpecificities').dialog('open');
                    else {
                        showAlertError('Veuillez sélectionner une spécificité à supprimer.');
                    }
                }

                function InitAML() {

                    $('#<%=btnOpenClientWatch.ClientID%>').show();
                    if ($('#<%=hfWatchStatus.ClientID%>').val().trim() == "-1")
                        $('#<%=btnOpenClientWatch.ClientID%>').hide();
                    else if ($('#<%=hfWatchStatus.ClientID%>').val().trim() == "1")
                        $('#<%=btnOpenClientWatch.ClientID%>').val('NE PLUS SURVEILLER');
                    else
                        $('#<%=btnOpenClientWatch.ClientID%>').val('SURVEILLER');

                $('#divClientWatch-popup').dialog({
                    autoOpen: false,
                    resizable: false,
                    draggable: false,
                    modal: true,
                    minWidth: 700,
                    zIndex: 99997,
                    title: "Surveiller ce client"
                });
                $('#divClientWatch-popup').parent().appendTo(jQuery("form:first"));
                $('#divGetSummarySheet-popup').dialog({
                    autoOpen: false,
                    resizable: false,
                    draggable: false,
                    modal: true,
                    minWidth: 700,
                    zIndex: 99997,
                    title: "&Eacute;diter la fiche synthèse du client"
                })
                $('#divGetSummarySheet-popup').parent().appendTo(jQuery("form:first"));
                $('#popup-delClientSpecificities').dialog({
                    autoOpen: false,
                    resizable: false,
                    draggable: false,
                    modal: true,
                    minWidth: 550,
                    zIndex: 99997,
                    title: "Confirmer supression spécificité(s) du client",
                    buttons:
                    [{
                        text: 'OK',
                        click: function () {
                            $("#<%=btnDelClientSpecificities.ClientID %>").click();
                            $(this).dialog('close');
                            //$('#LockUnlockPanel .loading').show();
                        }
                    },
                            {
                                text: 'Annuler',
                                click: function () { $(this).dialog('close'); }
                            }]
                });
                $('#popup-delClientSpecificities').parent().appendTo(jQuery("form:first"));

                $('#popup-modifyClientProfile').dialog({
                    autoOpen: false,
                    resizable: false,
                    draggable: false,
                    modal: true,
                    minWidth: 550,
                    zIndex: 99997,
                    title: "Modifier le profil client"
                    //,buttons: [{ text: 'Annuler', click: function () { $(this).dialog('close'); } }]
                });
                $('#popup-modifyClientProfile').parent().appendTo(jQuery("form:first"));

                $('#divConfirmManualAlert-popup').dialog({
                    autoOpen: false,
                    resizable: false,
                    draggable: false,
                    modal: true,
                    minWidth: 500,
                    zIndex: 99997,
                    title: "Créer une alerte manuelle"
                    ,buttons: [{ text: 'Annuler', click: function () { $(this).dialog('close'); } }, { text: 'Confirmer', click: function(){ $('#<%=btnCreateManualAlert.ClientID%>').click(); $(this).dialog('close'); } }]
                });
                $('#divConfirmManualAlert-popup').parent().appendTo(jQuery("form:first"));

                InitAMLToggles();
                InitAMLSurveillance();
            }

            function showModifyProfile() {
                $('#divClientProfileTAG_Modify').show();
                $('#divClientProfileReason_Modify').hide();
                $('#divClientProfileConfirm_Modify').hide();
                $('#<%= hdnClientProfileSelected.ClientID %>').val('');
                    $('#popup-modifyClientProfile').dialog('open');
                }

                function modifyClientProfile(step, settings) {
                    $('#divClientProfileTAG_Modify').hide();
                    $('#divClientProfileReason_Modify').hide();
                    $('#divClientProfileConfirm_Modify').hide();

                    switch (step) {
                        case "select":
                            $('#divClientProfileTAG_Modify').show();
                            if (settings.trim() != $('#<%=lblAmlClientProfile_TAG.ClientID%>').html().trim()) {
                                $('#lblClientProfilTAG').html(settings);
                                $('#<%=hdnClientProfileSelected.ClientID%>').val(settings);
                                modifyClientProfile('reason');
                            }
                            else
                                showAlertError('Ce client possède déjà ce profil.');
                            break;
                        case "reason":
                            $('#<%=txtProfileReason.ClientID%>').val('').focus();
                            $('#divClientProfileReason_Modify').show();
                            break;
                        case "confirm":
                            $('#lblClientProfileReason').html($('#<%=txtProfileReason.ClientID%>').val());

                            if ($('#<%=hdnClientProfileSelected.ClientID%>').val().trim().length > 0) {
                                $('#divClientProfileConfirm_Modify').show();
                                if($('#<%=txtProfileReason.ClientID%>').val().trim().length > 0)
                                    $('#divClientProfileConfirmReason').show();
                                else
                                    $('#divClientProfileConfirmReason').hide();
                            }
                            else {
                                $('#popup-modifyClientProfile').dialog('close');
                                showAlertError('Valeurs de profil non valides !');
                            }
                            break;
                        case "close":
                            $('#popup-modifyClientProfile').dialog('close');
                            break;
                    }
                }

                function openClientWatch() {
                    $('#<%=btnConfirmClientUnwatched.ClientID%>').hide();
                    $('#<%=btnConfirmClientWatched.ClientID%>').show();
                    if ($('#<%= hfWatchStatus.ClientID%>').val() == "1") {
                        $('#divClientWatch-popup').dialog({
                            autoOpen: false,
                            resizable: false,
                            draggable: false,
                            modal: true,
                            minWidth: 700,
                            zIndex: 99997,
                            title: "Ne plus surveiller ce client"
                        });
                        $('#<%=btnConfirmClientUnwatched.ClientID%>').show();
                        $('#<%=btnConfirmClientWatched.ClientID%>').hide();
                    }
                    $('#divClientWatch-popup').dialog('open');
                }
                function closeClientWatch() {
                    $('#divClientWatch-popup').dialog('close');
                }

                function openGetSummarySheet() {
                    $('#<%=btnConfirmClientUnwatched.ClientID%>').hide();
                    $('#<%=btnConfirmClientWatched.ClientID%>').show();
                    if ($('#<%= hfWatchStatus.ClientID%>').val() == "1") {
                        $('#divGetSummarySheet-popup').dialog({
                            autoOpen: false,
                            resizable: false,
                            draggable: false,
                            modal: true,
                            minWidth: 700,
                            zIndex: 99997,
                            title: "Editer la fiche de synthèse"
                        });
                    }
                    $('#divGetSummarySheet-popup').dialog('open');
                }
                function closeGetSummarySheet() {
                    $('#divGetSummarySheet-popup').dialog('close');
                }

                function WatchComment_maxLength(text, type, maxlength) {
                    //asp.net textarea maxlength doesnt work; do it by hand
                    //var maxlength = 2000; //set your value here (or add a parm and pass it in)
                    if (maxlength == null) {
                        if (type == "watch")
                            maxlength = $('#<%=lblNbMaxWatchComment.ClientID%>').html();
                    }

                    var object = document.getElementById(text.id)  //get your object
                    if (object.value.length > maxlength) {
                        object.focus(); //set focus to prevent jumping
                        object.value = text.value.substring(0, maxlength); //truncate the value
                        object.scrollTop = object.scrollHeight; //scroll to the end to prevent jumping
                        $('#lblNbChar_' + type).css('color', '#FF0000');
                        return false;
                    }
                    else if (object.value.length == maxlength) {
                        $('#lblNbChar_' + type).css('color', '#FF0000');
                    }
                    else {
                        $('#lblNbChar_' + type).css('color', '#344B56');
                    }

                    $('#lblNbChar_' + type).html(object.value.length);
                    return true;
                }

                function WatchComment_clearComment(type) {
                    $('#<%=txtWatchedComment.ClientID%>').val("");
                    $('#lblNbChar_' + type).html("0");
                }
                function checkConfirmClientWatched() {
                    var isCommentOK = false;

                    $('#reqWatchComment').css("visibility", "hidden");
                    if ($('#<%=txtWatchedComment.ClientID%>').val().trim().length > 0)
                        isCommentOK = true;
                    else
                        $('#reqWatchComment').css("visibility", "visible");

                    if (isCommentOK)
                        closeClientWatch();

                    return isCommentOK;
                }

                function ConfirmManualAlert(){
                    $('#divConfirmManualAlert-popup').dialog('open');
                }

            </script>

            <div id="divClientWatch-popup" style="display: none">
                <asp:UpdatePanel ID="upClientWatch" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="table" style="margin: 5px 0 0 0; width: 100%; padding: 0;">
                            <div class="row">
                                <div class="cell" style="width: 100%; vertical-align: middle">
                                    Commentaire(s)
                                    <label id="lblNbChar_watch">0</label>/
                                    <asp:Label ID="lblNbMaxWatchComment" runat="server" Text="400"></asp:Label>
                                    :
                                    <span id="reqWatchComment" class="reqStarStyle" style="visibility: hidden">*</span>
                                </div>
                                <div class="cell" style="text-align: right; vertical-align: middle">
                                    <input type="button" class="button" value="effacer" onclick="WatchComment_clearComment('watch');" />
                                </div>
                            </div>
                        </div>
                        <div>
                            <asp:TextBox ID="txtWatchedComment" MaxLength="400" runat="server" TextMode="MultiLine" autocomplete="off"
                                onKeyUp="javascript:WatchComment_maxLength(this, 'watch');" onChange="javascript:WatchComment_maxLength(this, 'watch');"
                                Style="max-width: 100%; min-width: 100%; min-height: 100px; max-height: 100px"></asp:TextBox>
                        </div>
                        <div style="width: 100%; text-align: right; margin-top: 10px;">
                            <input type="button" value="Annuler" class="button" onclick="closeClientWatch();" />
                            <asp:Button ID="btnConfirmClientWatched" runat="server" Text="Valider" CssClass="button" OnClientClick="return checkConfirmClientWatched();" OnClick="btnConfirmClientWatched_Click" />
                            <asp:Button ID="btnConfirmClientUnwatched" runat="server" Text="Valider" CssClass="button" Style="display: none" OnClientClick="return checkConfirmClientWatched();" OnClick="btnConfirmClientUnwatched_Click" />
                        </div>

                        <asp:HiddenField ID="hfWatchStatus" runat="server" />
                        <%--<asp:Label ID="lblWatchStatus" runat="server" Text="unknown"></asp:Label>--%>

                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnConfirmClientWatched" EventName="click" />
                        <asp:AsyncPostBackTrigger ControlID="btnConfirmClientUnwatched" EventName="click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
            <div id="divGetSummarySheet-popup" style="display: none">
                <asp:UpdatePanel ID="upGetSummarySheet" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div>
                            Voulez-vous vraiment envoyer la fiche synthèse du client sur votre e-mail ?
                        </div>
                        <div style="font-size: 0.9em; font-style: italic">
                            La fiche sera envoyée sur
                            <asp:Label ID="lblEmail_getSummarySheet" runat="server" Style="font-weight: bolder"></asp:Label>
                        </div>
                        <div style="width: 100%; text-align: right; margin-top: 10px;">
                            <input type="button" value="Annuler" class="button" onclick="closeGetSummarySheet();" />
                            <asp:Button ID="btnGetSummarySheet" runat="server" OnClientClick="closeGetSummarySheet();" OnClick="btnGetSummarySheet_Click" CssClass="button" Text="Envoyer" />
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnGetSummarySheet" EventName="click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>

            <div id="divConfirmManualAlert-popup">
                Etes-vous sûr de vouloir créer une alerte manuelle pour ce client ?<br />
                <br />
                <b>Information :</b> Cette nouvelle alerte vous sera automatiquement attribuée.
                <asp:UpdatePanel ID="upCreateManualAlert" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Button ID="btnCreateManualAlert" runat="server" OnClick="btnCreateManualAlert_Click" Style="display: none" />
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnCreateManualAlert" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>

            <div style="width: 50%; float: left; text-align: left">
                <asp:Button ID="btnFastAnalysisSheet" runat="server" Text="Fiche d'analyse rapide" Visible="false" CssClass="button" OnClick="btnFastAnalysisSheet_Click" />
            </div>
            <div style="width: 50%; float: right; text-align: right">
                <asp:Button ID="btnOpenGetSummarySheet" runat="server" CssClass="button" OnClientClick="openGetSummarySheet(); return false;" Text="&Eacute;diter FS" />
                <asp:Button ID="btnOpenClientWatch" runat="server" CssClass="button" OnClientClick="openClientWatch(); return false;" />
            </div>
            <div style="clear: both"></div>

            <asp:Panel ID="panelAmlClientBlockedTransfer" runat="server" Visible="false">
                <asp:UpdatePanel ID="upAmlClientBlockedTransfer" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="tabSubtitle">
                            Liste des virements non approuvés
                        </div>
                        <div style="margin: -5px 0 20px 0; font-size: 0.9em;">
                            <asp:BlockedTransfer ID="BlockedTransfer1" runat="server"></asp:BlockedTransfer>
                        </div>
                        <asp:Button ID="btnLoadBlockedTransfer" runat="server" Style="display: none" />
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnLoadBlockedTransfer" EventName="click" />
                    </Triggers>
                </asp:UpdatePanel>
            </asp:Panel>
            <div id="aml-alerts">
                <asp:UpdatePanel ID="upAmlClientAlert" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="tabSubtitle">
                            Liste des alertes
                        </div>
                        <asp:AmlAlert ID="AmlAlert1" runat="server" />
                    </ContentTemplate>
                    <Triggers>
                        <%--<asp:AsyncPostBackTrigger ControlID="btnClientWatched" EventName="click" />--%>
                    </Triggers>
                </asp:UpdatePanel>
                <asp:Panel ID="panelCreateManualAlert" runat="server" Visible="false" Style="text-align: right">
                    <input type="button" value="Créer une alerte manuelle" class="button" onclick="ConfirmManualAlert();" style="margin-top: 10px" />
                </asp:Panel>
            </div>
            <asp:AmlHideAlert ID="AmlHideAlert1" runat="server" />

            <asp:UpdatePanel ID="upAmlClientProfile" runat="server">
                <ContentTemplate>
                    <div style="color: #344b56; font-weight: bold; text-transform: uppercase; border-bottom: 1px solid #344b56; margin-top: 20px;">
                        Profil
                    </div>
                    <%--<h3 style="margin-top:20px;text-transform:uppercase;">Profil</h3>--%>
                    <asp:Panel ID="panelClientProfile_Current" runat="server" Style="margin-top: 10px; padding: 0 5px">
                        <div class="table">
                            <div class="row" style="background-color: #344b56; color: #fff">
                                <div class="cell" style="padding-left: 5px; font-weight: bold; text-align: center">Type</div>
                                <div class="cell" style="padding-left: 20px; font-weight: bold; text-align: center">Date</div>
                                <div class="cell" style="padding-left: 20px; font-weight: bold; text-align: center">Par</div>
                                <div class="cell" style="padding-left: 20px; font-weight: bold; text-align: center">Raison</div>
                                <div class="cell" style="padding-left: 20px; text-align: center; background-color: #fff"></div>
                            </div>
                            <div class="row">
                                <div class="cell" style="padding-left: 5px; vertical-align: middle">
                                    <asp:Label ID="lblAmlClientProfile_TAG" runat="server" Style="font-size: 1.5em"></asp:Label>
                                </div>
                                <div class="cell" style="padding-left: 20px; vertical-align: middle; text-align: center; width: 150px;">
                                    <asp:Label ID="lblAmlClientProfile_Date" runat="server"></asp:Label>
                                </div>
                                <div class="cell" style="padding-left: 20px; vertical-align: middle; text-align: center; width: 200px">
                                    <asp:Label ID="lblAmlClientProfile_ByUser" runat="server"></asp:Label>
                                </div>
                                <div class="cell" style="padding-left: 20px; vertical-align: middle; width: 210px; text-align: center">
                                    <asp:Label ID="lblAmlClientProfile_Reason" runat="server"></asp:Label>
                                </div>
                                <div class="cell" style="padding-left: 20px; vertical-align: middle; text-align: center; padding-top: 5px">
                                    <asp:Panel ID="panelShowModifyClientProfile" runat="server">
                                        <input id="btnShowModifyClientProfile" type="button" class="button" value="Modifier" onclick="showModifyProfile();" />
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="panelClientProfile_Empty" runat="server" Visible="false">Profil non déclaré</asp:Panel>
                    <asp:Panel ID="panelClientProfile_History" runat="server" Visible="false" Style="padding: 0 10px;">
                        <div style="margin-top: 10px; color: #f57527; font-weight: bold">Historique</div>
                        <div id="AmlProfilePanel" style="">
                            <asp:Repeater ID="rptAML_ClientProfile_History" runat="server">
                                <HeaderTemplate>
                                    <div style="max-height: 300px; width: 52%; overflow: auto; border: 2px solid #f57527">
                                        <table style="width: 100%; border-collapse: collapse;" class="tableHistory">
                                            <tr>
                                                <th>Type</th>
                                                <th>Date</th>
                                                <th>Par</th>
                                                <th>
                                                    <asp:ImageButton ID="btnRefreshProfileHistory" runat="server" Text="Refresh" OnClick="btnRefreshProfileHistory_Click" ImageUrl="~/Styles/Img/refresh.png" Width="15px" Style="position: relative; right: 5px; top: 2px" />
                                                </th>
                                            </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Eval("TAG") %></td>
                                        <td><%# tools.getFormattedDate(Eval("Date").ToString()) %></td>
                                        <td><%# Eval("ByUser") %></td>
                                        <td></td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                    </div>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                    </asp:Panel>

                    <div id="popup-modifyClientProfile" style="display: none">
                        <div id="divClientProfileTAG_Modify" style="padding-bottom: 10px;">
                            <asp:Repeater ID="rptProfileList" runat="server">
                                <HeaderTemplate>
                                    <table cellspacing="0">
                                        <tr>
                                            <th>Type</th>
                                            <th>Risque</th>
                                            <th>Description</th>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr onclick="modifyClientProfile('select','<%# Eval("Profil") %>');"
                                        onmouseover="this.style.cursor = 'pointer';this.style.backgroundColor = '#ff9966'; this.style.color = '#ffffff';"
                                        onmouseout="this.style.cursor = 'auto';this.style.backgroundColor = '#fff'; this.style.color = '#344b56';">
                                        <td style="padding-left: 10px; vertical-align: middle; padding-top: 5px;">
                                            <%# Eval("Profil") %>
                                        </td>
                                        <td style="padding-left: 10px; vertical-align: middle; padding-top: 5px;">
                                            <%# Eval("NiveauRisque") %>
                                        </td>
                                        <td style="padding-left: 10px; vertical-align: middle">
                                            <div class="trimmer" style="width: 400px;">
                                                <asp:Label ID="lblProfileDescription" runat="server" ToolTip='<%# Eval("Description") %>' CssClass="tooltip"><%# Eval("Description") %></asp:Label>
                                            </div>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                        <div id="divClientProfileReason_Modify" style="display: none;">
                            <div style="font-weight: bold">Raison <span style="font-weight: normal; font-style: italic;">(non requise)</span></div>
                            <div style="margin-top: 10px; font-weight: bold">
                                <label id="lblNbCharClientProfileReason">0</label>
                                /
                                <asp:Label ID="lblNbMaxClientProfileReason" runat="server">400</asp:Label>
                                caractères
                            </div>
                            <div>
                                <asp:TextBox ID="txtProfileReason" runat="server" TextMode="MultiLine" MaxLength="400"
                                    Style="min-width: 500px; max-width: 500px; min-height: 80px; max-height: 80px;">
                                </asp:TextBox>
                                <div style="margin-top: 20px; text-align: right">
                                    <input type="button" class="button" value="Annuler" onclick="modifyClientProfile('close');" />
                                    <input type="button" class="button" value="Valider" onclick="modifyClientProfile('confirm');" />
                                </div>
                            </div>
                        </div>
                        <div id="divClientProfileConfirm_Modify" style="display: none;">
                            <div>
                                Confirmez-vous le changement du profil client ?
                            </div>
                            <div style="margin-top: 10px; max-width: 500px;" class="table">
                                <div class="row">
                                    <div class="cell">
                                        <div style="font-weight: bold;">Profil</div>
                                        <div style="margin-top: 5px;">
                                            <label id="lblClientProfilTAG"></label>
                                        </div>
                                    </div>
                                    <div id="divClientProfileConfirmReason" class="cell" style="padding-left: 10px;">
                                        <div style="font-weight: bold">Raison</div>
                                        <div style="margin-top: 5px">
                                            <label id="lblClientProfileReason"></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="margin-top: 20px; text-align: right">
                                <input type="button" class="button" value="Annuler" onclick="modifyClientProfile('close');" />
                                <asp:Button ID="btnClientProfileChange" runat="server" Text="Confirmer" OnClientClick="modifyClientProfile('close');" OnClick="btnClientProfileChange_Click" CssClass="button" />
                            </div>
                        </div>
                        <asp:HiddenField ID="hdnClientProfileSelected" runat="server" Value=""></asp:HiddenField>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnClientProfileChange" EventName="click" />
                </Triggers>
            </asp:UpdatePanel>
            <asp:UpdatePanel ID="upAML" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Panel ID="panelAMLSpecificities" runat="server" Visible="false">
                        <div class="tabSubtitle" style="margin-top: 20px;">
                            Spécificité(s)
                        </div>
                        <%--<h3 style="margin-top:20px;text-transform:uppercase;">Spécificité(s)</h3>--%>
                        <asp:Panel ID="panel_ClientSpecificities" runat="server" Style="margin-top: 5px; padding: 0 5px;">
                            <asp:Repeater ID="rptAML_ClientSpecificities" runat="server" OnItemDataBound="rptAML_ClientSpecificities_itemDataBound">
                                <HeaderTemplate>
                                    <table style="width: 99%; border-collapse: collapse;" class="tableHistory">
                                        <tr>
                                            <th style="text-align: left; padding-left: 5px; min-width: 250px; max-width: 300px;">Spécificité</th>
                                            <th style="text-align: left; padding-left: 10px;">Date</th>
                                            <th style="text-align: left; padding-left: 10px;">Par</th>
                                            <th style="padding-left: 10px; text-align: center;"></th>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr class='<%# GetSpecificityStatus(Eval("IsDeleted").ToString()) %>'>
                                        <td style="padding-left: 5px;">
                                            <%#Eval("SpecificityLabel") %>
                                        </td>
                                        <td style="padding-left: 10px; text-align: left">
                                            <%# tools.getFormattedDate(Eval("SpecificityDate").ToString()) %>
                                        </td>
                                        <td style="padding-left: 10px">
                                            <%#Eval("ByUser") %>
                                        </td>
                                        <td style="padding-left: 10px; text-align: center">
                                            <asp:CheckBox ID="cbClientSpecificity" runat="server" CssClass="checkbox_specificities" Visible='<%# !IsSpecificityDeleted(Eval("IsDeleted").ToString())%>' />
                                            <asp:Label ID="lblClientRefSpecificity" runat="server" Text='<%# Eval("RefSpecificity") %>' Style="display: none"></asp:Label>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <tr>
                                        <td style="padding-top: 5px; border-bottom: 0;">
                                            <asp:Button ID="btnShowAddClientSpecificity1" runat="server" Text="Ajouter une spécificité" CssClass="button" OnClientClick="showAddClientSpecificities(); return false;" />
                                        </td>
                                        <td style="padding-top: 5px; border-bottom: 0;"></td>
                                        <td style="padding-top: 5px; border-bottom: 0;"></td>
                                        <td style="padding-left: 10px; border-bottom: 0; padding-top: 5px; text-align: center">
                                            <asp:Panel ID="panelShowDelClientSpecificities" runat="server">
                                                <input id="btnShowDelClientSpecificities" type="button" class="button" value="Supprimer" onclick="confirmDelClientSpecificities();" />
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                            <div id="popup-delClientSpecificities" style="display: none">
                                <label id="lblDelClientSpecificities">Voulez-vous vraiment supprimer ce(s) spécificité(s) ?</label>
                                <asp:Button ID="btnDelClientSpecificities" runat="server" CssClass="button" Text="Supprimer" OnClick="btnDelClientSpecificities_Click" Style="display: none" />
                            </div>
                            <asp:Panel ID="panel_ClientSpecificitiesNoResult" runat="server" Visible="false" Style="min-height: 80px;">
                                Aucune spécificité
                            <div style="margin-top: 5px;">
                                <asp:Button ID="btnShowAddClientSpecificity2" runat="server" Text="Ajouter une spécificité" CssClass="button" OnClientClick="showAddClientSpecificities(); return false;" />
                            </div>
                            </asp:Panel>
                            <asp:Panel ID="panelClientSpecificitiesAdd" runat="server" Style="position: relative">
                                <div id="divAddClientSpecificities" style="display: none; border: 2px solid #f57527; border-radius: 8px; padding: 5px 10px; margin-top: 5px">
                                    <h3 style="margin: 5px 0">Ajouter une spécificité</h3>
                                    <div class="table">
                                        <div class="row">
                                            <div class="cell">
                                                <asp:DropDownList ID="ddlSpecificityList" runat="server" AppendDataBoundItems="true" Style="width: 300px"
                                                    DataTextField="SpecificityLabel" DataValueField="RefSpecificity">
                                                    <asp:ListItem Value=""></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="cell" style="padding-left: 20px;">
                                                <asp:Button ID="btnAddClientSpecificities" runat="server" CssClass="button" Text="Ajouter" OnClick="btnAddClientSpecificities_Click" />
                                            </div>
                                            <div class="cell" style="padding-left: 20px;">
                                                <input type="button" value="Annuler" class="button" onclick="showAddClientSpecificities();" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                        </asp:Panel>
                    </asp:Panel>

                    <asp:Panel ID="panelAMLCommentary" runat="server">
                        <div style="color: #344b56; font-weight: bold; text-transform: uppercase; border-bottom: 1px solid #344b56; margin-top: 10px;">
                            Commentaire(s)
                        </div>
                        <%--<h3 style="margin-top:20px;text-transform:uppercase;">Commentaire(s)</h3>--%>
                        <asp:Panel ID="panel_ClientNote" runat="server" Style="position: relative; margin-top: 5px; padding: 0 5px;">
                            <asp:Repeater ID="rptAML_ClientNote" runat="server">
                                <HeaderTemplate>
                                    <table style="width: 99%; border-collapse: collapse; position: relative; margin-bottom: 10px;" class="tableHistory">
                                        <tr>
                                            <th style="text-align: left; padding-left: 5px; padding-bottom: 5px; min-width: 250px; max-width: 300px;">Commentaire</th>
                                            <th style="text-align: left; padding-left: 10px; padding-bottom: 5px;">Date</th>
                                            <th style="text-align: left; padding-left: 10px; padding-bottom: 5px;">Par</th>
                                            <th style="padding-left: 10px; text-align: center; padding-bottom: 5px;"></th>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td style="padding-left: 5px">
                                            <%#Eval("Note") %>
                                        </td>
                                        <td style="padding-left: 10px; text-align: left">
                                            <%#Eval("NoteDate") %>
                                        </td>
                                        <td style="padding-left: 10px">
                                            <%#Eval("ByUser") %>
                                        </td>
                                        <td style="padding-left: 10px; text-align: center"></td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>

                            <asp:Panel ID="panel_ClientNoteNoResult" runat="server" Visible="false" Style="min-height: 30px;">Aucun commentaire</asp:Panel>
                            <asp:Panel ID="panelClientNoteAdd" runat="server" Style="position: relative">
                                <div>
                                    <asp:Button ID="btnShowAddClientNote" runat="server" CssClass="button" OnClientClick="showAddClientNote(); return false;" Text="Ajouter un commentaire" />
                                </div>
                                <div id="divAddClientNote" style="display: none; border: 2px solid #f57527; border-radius: 8px; padding: 5px 10px; margin-top: 5px">
                                    <h3 style="margin: 5px 0">Ajouter un commentaire</h3>
                                    <div style="margin-top: 10px; font-weight: bold">
                                        <label id="lblNbCharClientNote">0</label>
                                        /
                                        <asp:Label ID="lblNbMaxClientNote" runat="server">400</asp:Label>
                                        caractères
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtClientNote" runat="server" TextMode="MultiLine" MaxLength="400"
                                            Style="min-width: 830px; max-width: 830px; min-height: 80px; max-height: 80px;">
                                        </asp:TextBox>
                                    </div>
                                    <div style="margin-top: 10px; text-align: right">
                                        <asp:Button ID="btnAddClientNote" runat="server" CssClass="button" Text="Ajouter" OnClientClick="return checkClientNote();" OnClick="btnAddClientNote_Click" />
                                        <input type="button" value="Annuler" class="button" onclick="showAddClientNote();" />
                                    </div>
                                </div>
                            </asp:Panel>
                        </asp:Panel>
                    </asp:Panel>
                    <asp:Panel ID="panelAmlHidden" runat="server" Style="display: none">
                        <asp:Button ID="btnRefreshClientNote" runat="server" OnClick="refreshClientNote" Style="display: none" />
                    </asp:Panel>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnAddClientSpecificities" EventName="click" />
                    <asp:AsyncPostBackTrigger ControlID="btnAddClientNote" EventName="click" />
                    <asp:AsyncPostBackTrigger ControlID="btnDelClientSpecificities" EventName="click" />
                    <asp:AsyncPostBackTrigger ControlID="btnRefreshClientNote" EventName="click" />
                </Triggers>
            </asp:UpdatePanel>
            <asp:Panel ID="panelSurveillance" runat="server" Visible="false">
                <asp:AmlSurveillance ID="AmlSurveillance1" runat="server" />
			</asp:Panel>
            <asp:Panel ID="panelReq" runat="server">
                <asp:ReqDComList ID="Req1" runat="server" Filter="REQ" ShowLabel="true" />
            </asp:Panel>
            <asp:Panel ID="panelTracfinStatement" runat="server" Visible="false">
                <asp:SearchTracfinStatement ID="searchTracfin1" runat="server" ShowSubtitle="true" HideFilters="true" />
            </asp:Panel>
        </asp:Panel>

        <asp:Panel ID="panelBeneficiaries" runat="server">
            <asp:Panel ID="panelBeneficiaryList" runat="server">
                <asp:Repeater ID="rBeneficiaryList" runat="server">
                    <HeaderTemplate>
                        <table id="tBeneficiaryList" cellpadding="0" cellspacing="0" style="width: 100%" class="defaultTable2">
                            <thead>
                                <tr>
                                    <th>Statut</th>
                                    <th>Nom</th>
                                    <th>Banque</th>
                                    <th>IBAN</th>
                                    <th>BIC</th>
                                    <th>Confirmé le</th>
                                    <!--<th></th>
                                    <th></th>-->
                                </tr>
                            </thead>
                            <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr class='<%# Container.ItemIndex % 2 == 0 ? "beneficiary-table-row" : "beneficiary-table-alternate-row" %>'>
                            <td style="white-space:nowrap;">
                                <div  style="display:inline-block">
                                    <img src='<%# Eval("UrlImageStatut1") %>'  Title='<%# Eval("StatutDescription") %>' width="25px" height ="25px" <%# Eval("VisibilityImageStatut1") %>/> 
                                    <img src='<%# Eval("UrlImageStatut2") %>'  Title='<%# Eval("StatutDescription") %>' width="25px" height ="25px" <%# Eval("VisibilityImageStatut2") %> /> 
                                </div>
                            </td>
                            <td <%# ((bool)Eval("IsBeneficiaryValidate")) ? "" : "style='text-decoration: line-through;color:red;'" %> >
                                <%# Eval("Name") %>
                                <span id="refBenef" style="display: none"><%# Eval("Reference") %></span>
                            </td>
                            <td <%# ((bool)Eval("IsBeneficiaryValidate")) ? "" : "style='text-decoration: line-through;color:red;'" %> >
                                <%# Eval("BankName") %>
                            </td>
                            <td <%# ((bool)Eval("IsBeneficiaryValidate")) ? "" : "style='text-decoration: line-through;color:red;'" %> >
                                <%# Eval("Iban") %>
                            </td>
                            <td <%# ((bool)Eval("IsBeneficiaryValidate")) ? "" : "style='text-decoration: line-through;color:red;'" %> >
                                <%# Eval("Bic") %>
                            </td>
                            
                            <asp:Image ID="imgAccountType" runat="server" />
                            <td <%# ((bool)Eval("IsBeneficiaryValidate")) ? "style='text-align: center'" : "style='text-align: center;text-decoration: line-through;color:red;'" %> >
                                <%# Eval("ConfirmeLe") %>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </tbody>
                    </table>
                    </FooterTemplate>
                </asp:Repeater>
            </asp:Panel>
            <asp:Panel ID="panelBeneficiaryList_Empty" runat="server" Visible="false" Style="margin-top: 10px">Aucun bénéficiaire</asp:Panel>
        </asp:Panel>

        <asp:Panel ID="panelGarnishment" runat="server">
            <asp:Garnishment ID="garnishment1" runat="server" />
        </asp:Panel>
    </div>

    <div id="popup-list" style="display: none; z-index: 99997">
        <div id="popup-PersonalInfo">
            <span class="ui-helper-hidden-accessible">
                <input type="text" /></span>
            <strong>Attention</strong> : les données saisies remplaceront les données précédemment enregistrées.
            <asp:Button ID="btnSavePersonalInfo" runat="server" Text="Enregistrer" CssClass="button" OnClick="btnSavePersonalInfo_Click" Style="display: none" />
        </div>
        <div id="popup-DocsInfo">
            <span class="ui-helper-hidden-accessible">
                <input type="text" /></span>
            <strong>Attention</strong> : les données saisies remplaceront les données précédemment enregistrées.
            <asp:Button ID="btnSaveDocsInfo" runat="server" Text="Enregistrer" CssClass="button" OnClick="btnSaveDocsInfo_Click" Style="display: none" />
        </div>
        <div id="popup-Limits">
            <span class="ui-helper-hidden-accessible">
                <input type="text" /></span>
            <span id="popup-Limits-label"></span>
            <asp:Button ID="btnAcceptRefuseLimitRequest" runat="server" Text="" CssClass="button" OnClick="btnAcceptRefuseLimitRequest_Click" Style="display: none" />
        </div>
        <div id="popup-PINBySMS">
            <span class="ui-helper-hidden-accessible">
                <input type="text" /></span>
            Voulez-vous vraiment renvoyer le code PIN à ce client ?
            <asp:Button ID="btnSendPIN" runat="server" Text="Renvoyer" CssClass="button" OnClick="btnSendPIN_Click" Style="display: none" />
        </div>
        <div id="popup-resetWEBPassword">
            <span class="ui-helper-hidden-accessible">
                <input type="text" /></span>
            Voulez-vous vraiment réinitialiser le mot de passe WEB de ce client ?
            <asp:Button ID="btnResetWEBPassword" runat="server" Text="Réinitialiser" CssClass="button" OnClick="btnResetWEBPassword_Click" Style="display: none" />
        </div>
        <div id="popup-sendWEBIDByMail">
            <span class="ui-helper-hidden-accessible">
                <input type="text" /></span>
            Voulez-vous vraiment envoyer l'identifiant WEB de ce client sur son adresse mail ?
            <asp:Button ID="btnSendWEBID" runat="server" Text="Envoyer par mail" CssClass="button" OnClick="btnSendWEBID_Click" Style="display: none" />
        </div>
        <div id="popup-sendRibByMail">
            <span class="ui-helper-hidden-accessible">
                <input type="text" /></span>
            <div>Par défaut, le RIB est envoyé à l'adresse mail du client.</div>
            <div style="margin-top: 5px">
                <asp:CheckBox ID="cbChangeRibEmail" runat="server" onchange="return changeRibEmail();" />
                <label for="<%= cbChangeRibEmail.ClientID%>">Cochez cette case, si vous voulez l'envoyer sur une autre adresse mail.</label>
            </div>
            <div id="divRibMailReplacement" style="text-align: center; width: 100%; margin-top: 10px; visibility: hidden">
                <span style="font-weight: bold">Adresse mail</span>
                <span id="req_RibEmail" class="reqStarStyle" style="visibility: hidden">*</span>
                <asp:TextBox ID="txtRibMail" runat="server" autocomplete="off"></asp:TextBox>
            </div>
            <asp:Button ID="btnRibSendMail" runat="server" Text="Envoyer par mail" CssClass="button" OnClick="btnRibSendMail_Click" Style="display: none" />
        </div>
        <div id="popup-lockUnlock">
            <label id="lblLockUnlockConfirm">Voulez-vous vraiment bloquer le compte du client ?</label>
            <div id="panelLockReason" style="margin-top: 10px;">
                <label for="<%= txtLockReason.ClientID %>">Raison : </label>
                <asp:TextBox ID="txtLockReason" runat="server" MaxLength="200" Style="width: 400px;" autocomplete="off"></asp:TextBox>
            </div>
            <asp:Button ID="btnLockUnlock" runat="server" Text="Bloquer" CssClass="button" OnClick="btnLockUnlock_Click" Style="display: none" />
        </div>
        <div id="popup-iFrameOperationDiv">
            <div id="iFrameDiv" style="display: none; width: 980px; height: 700px; margin: auto; background: url(Styles/Img/loading.gif) 50% 50% no-repeat; border: 0; z-index: 100">
                <iframe id="iFrameClientBalanceOperations" width="968px" height="694px" style="z-index: 100; border: 0">Votre navigateur ne supporte pas les iFrames.</iframe>
            </div>
        </div>
        <div id="popup-sab-cobalt-ope-comparison" style="display:none">
            <asp:UpdatePanel ID="upSABCobaltOpeComparison" runat="server" UpdateMode="Conditional" style="height:100%">
                <ContentTemplate>
                    <asp:SABCobaltOpeComparison ID="SABCobaltOpeComparison1" runat="server" Visible="false" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div id="popup-delete-webmessage">
            <span class="ui-helper-hidden-accessible">
                <input type="text" /></span>
            Voulez-vous vraiment supprimer le(s) message(s) selectionné(s) ?
            <asp:Button ID="btnDeleteWebMessage" runat="server" Text="Supprimer" CssClass="button" OnClick="btnDeleteWebMessage_Click" Style="display: none" />
        </div>
        <div id="popup-cardLockConfirm">
            <span class="ui-helper-hidden-accessible">
                <input type="text" /></span>
            Voulez-vous vraiment mettre en opposition la carte du client ?
            <asp:Button ID="btnCardLock" runat="server" OnClick="btnCardLock_Click" Style="display: none" />
            <asp:Button ID="btnCardReplacement" runat="server" OnClick="btnCardReplacement_Click" Style="display: none" />
        </div>
        <div id="dialog-check-challenge-received" style="display: none">
            <div class="check-sms">
                Un code de remplacement vient d'être envoyé au
                <asp:Label ID="lblCurrentPhone2" runat="server" Font-Bold="true"></asp:Label>&nbsp; le client l'a-t-il réceptionné ?
                <div class="button-container">
                    <input type="button" value="Oui" class="button" onclick="HideChallengeReceivedDialog();" />
                    <input type="button" value="Non" class="button" onclick="ChangeChallengeStep('verify-email');" />
                </div>
            </div>
            <div class="verify-email">
                L'adresse email du client est-elle
                <asp:Label ID="lblCurrentEmail" runat="server" Font-Bold="true"></asp:Label>
                ?
                <div class="button-container">
                    <input type="button" value="Oui" class="button" onclick="ChangeChallengeStep('send-to-email');" />
                    <input type="button" value="Non" class="button" onclick="ChangeChallengeStep('change-email');" />
                </div>
            </div>
            <div class="change-email">
                Invitez le client a modifier son adresse email sur son espace client ou à se rendre chez un buraliste pour procéder au remplacement de la carte - sur la borne.
                <div class="button-container">
                    <input type="button" value="Fermer" class="button" onclick="HideChallengeReceivedDialog();" />
                </div>
            </div>
            <div class="send-to-email" id="div-send-to-email">
                <asp:UpdatePanel ID="upCheckChallenge" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        Voulez-vous envoyer le code de remplacement à cette adresse e-mail 
                        <asp:Label ID="lblCurrentEmail2" runat="server" Font-Bold="true"></asp:Label>
                        ?
                        <div class="button-container">
                            <asp:Button ID="btnSendReplaceCardChallengeToEmail" runat="server" Text="Oui" CssClass="button" OnClick="btnSendReplaceCardChallengeToEmail_Click" />
                            <input type="button" value="Non" class="button" onclick="ChangeChallengeStep('change-email');" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
        <div id="popup-force-annual-fee" style="display: none">
            <span class="ui-helper-hidden-accessible">
                <input type="text" /></span>
            <div style="text-align: center">
                <div style="margin-top: 10px">
                    <asp:Label ID="lblAnnualFeeStatus" runat="server" Font-Bold="true"></asp:Label>
                </div>
                <asp:Panel ID="panelForceAnnualFee" runat="server" Visible="false" Style="margin-top: 15px;">
                    <div id="div-force-annual-fee" style="display: inline-block; position: relative">
                        <asp:UpdatePanel ID="upForceAnnualFee" runat="server">
                            <ContentTemplate>
                                <asp:Button ID="btnForceAnnualFee" runat="server" Text="Forcer le renouvellement" CssClass="button" OnClientClick="ShowAnnualFeeLoading();" OnClick="btnForceAnnualFee_Click" />
                            </ContentTemplate>
                        </asp:UpdatePanel>

                        <img class="loading" src="Styles/Img/loading.gif" alt="loading" width="25px" height="25px" style="display: none; position: absolute; top: 1px; left: 102%" />
                    </div>
                </asp:Panel>
            </div>
        </div>

        <div id="popup-AskKycUpdate">
            <span class="ui-helper-hidden-accessible">
                <input type="text" /></span>
            Voulez-vous vraiment demander au client une mise à jour de ses KYC ?
            <asp:UpdatePanel ID="upAskUpdateKyc" runat="server">
                <ContentTemplate>
                    <asp:Button ID="btnAskKycUpdate" runat="server" OnClick="btnAskKycUpdate_Click" Style="display: none" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>

        <div id="popup-SabStatus">
            <asp:UpdatePanel ID="upSabStatus" runat="server">
                <ContentTemplate>
                    <asp:Panel ID="panelChangeSabStatus" runat="server" DefaultButton="btnSaveSabStatus">
                        <asp:DropDownList ID="ddlSabStatus" runat="server" onchange="OnChangeSabStatus();" Style="width: 40%; box-sizing: border-box">
                            <asp:ListItem Text="0 - Normal" Value="0"></asp:ListItem>
                            <asp:ListItem Text="1 - Bloqué débit" Value="1"></asp:ListItem>
                            <asp:ListItem Text="2 - Bloqué crédit" Value="2"></asp:ListItem>
                            <asp:ListItem Text="3 - Bloqué débit/crédit" Value="3"></asp:ListItem>
                            <asp:ListItem Text="4 - Clôturé" Value="4"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:DropDownList ID="ddlLockSIReason" runat="server" DataTextField="LockSIReason" DataValueField="RefLockSIReason" Style="width: 34%; box-sizing: border-box"></asp:DropDownList>
                        <asp:Button ID="btnSaveSabStatus" runat="server" Text="Modifier" Enabled="false" CssClass="button" OnClick="btnSaveSabStatus_Click" Style="width: 24%; box-sizing: border-box; margin: 0 0 4px 0" />
                    </asp:Panel>
                    <div style="max-height: 200px; overflow-y: auto">
                        <asp:Repeater ID="rptSabStatusHistory" runat="server">
                            <HeaderTemplate>
                                <table class="defaultTable2">
                                    <tr>
                                        <th>Date</th>
                                        <th>Nouveau</th>
                                        <th>Ancien</th>
                                        <th>Par</th>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr class='<%# Container.ItemIndex % 2 == 0 ? "default-table-row-style" : "table-alternate-row-style" %>'>
                                    <td style="text-align: center"><%#Eval("Date") %></td>
                                    <td style="text-align: center"><%#Eval("New") %></td>
                                    <td style="text-align: center"><%#Eval("Old") %></td>
                                    <td style="text-align: right"><%#Eval("BOUser") %></td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>

    <asp:UpdatePanel ID="upMessage" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:HiddenField ID="hdnMessage" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="popup-message">
        <span id="message"></span>
    </div>
    <div class="panel-loading" style="z-index: 100000"></div>

    <asp:HiddenField ID="hdnAccountNumber" runat="server" Value="" />
    <asp:HiddenField ID="hdnRefCustomer" runat="server" Value="" />
    <asp:HiddenField ID="hdnRegistrationCode" runat="server" Value="" />
    <asp:HiddenField ID="hdnActiveTab" runat="server" Value="" />
    <asp:HiddenField ID="hdnNbAlert" runat="server" Value="" />
    <asp:HiddenField ID="hdnDocsAlert" runat="server" Value="" />
    <asp:HiddenField ID="hdnForceShowTab" runat="server" Value="" />
    <asp:HiddenField ID="hdnDriStatus" runat="server" Value="" />
    <asp:HiddenField ID="hdnIBAN" runat="server" Value="" />
    <asp:HiddenField ID="hdnIsCobalt" runat="server" Value="" />

    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginClientDetailsRequestHandler);
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndClientDetailsRequestHandler);
        function BeginClientDetailsRequestHandler(sender, args) {
            try{
                pbControl = args.get_postBackElement();
                //console.log(pbControl.id);
                var containerID = '';
                if (pbControl != null) {
                    if (pbControl.id.indexOf('btnSaveForm') != -1)
                        containerID = $('#<%=panelIpConnectionList.ClientID%>');
                    if (pbControl.id.indexOf('btnCreateManualAlert') != -1)
                        containerID = 'aml-alerts';
                    if (pbControl.id.indexOf('btnLoadOtherDocs') != -1)
                        containerID = 'div-other-docs';
                    if (pbControl.id.indexOf('btnMonextActivate') != -1 ||
                        pbControl.id.indexOf('btnCardReplacement') != -1)
                        containerID = 'div-card-histo';
                    if(pbControl.id.indexOf('btnAskKycUpdate') != -1)
                        containerID = 'PersonalInfoPanel';
                    if (pbControl.id.indexOf('btnKycUpdate') != -1) {
                        //console.log($('#<%=hfTaxResidenceClientValues.ClientID%>').val());
                        containerID = 'PersonalInfoPanel';
                    }
                    if(pbControl.id.indexOf('btnUpdateBlockAllPayment') != -1 || pbControl.id.indexOf('btnUpdateBlockOutFrancePayment') != -1 || pbControl.id.indexOf('btnUpdateBlockInternetPayment') != -1)
                        containerID = 'card-payment-preference';
                    if(pbControl.id.indexOf('btnValidateAddress') != -1)
                        containerID = 'div-address';
                    if(pbControl.id.indexOf('btnValidateEmail') != -1)
                        containerID = 'div-email';
                    if(pbControl.id.indexOf('btnSaveSabStatus') != -1)
                        containerID = 'popup-SabStatus';
                    if (pbControl.id.indexOf('btnSendReplaceCardChallengeToEmail') != -1)
                        containerID = 'div-send-to-email';
                    if (pbControl.id.indexOf('btnChangeLimit') != -1)
                        containerID = 'dialog-change-limit';
                    if (pbControl.id.indexOf('btnInitNickelChromeData') != -1) {
                        containerID = 'divMainPage';
                    }
                    if (pbControl.id.indexOf('btnReopenAccount') != -1) {
                        containerID = 'divMainPage';
                    }
                }
                else {
                    if($('#PersonalInfoPanel').is(':visible'))
                        containerID = 'PersonalInfoPanel';
                }

                if(containerID.length > 0)
                    ShowPanelLoading(containerID);
            }
            catch(e)
            {

            }
        }
        function EndClientDetailsRequestHandler(sender, args) {
            HidePanelLoading();
            if ($('#<%=panelTaxResidenceList.ClientID %>') != null && $('#<%=panelTaxResidenceList.ClientID %>').is(':visible')) {
                GetCountryList();
            }

            if ($('#<%=panelFATCA.ClientID%>') != null && $('#<%=panelFATCA.ClientID%>').is(':visible')) {
                CheckFatcaDisabled();
            }
        }
    </script>

    
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">
    <link rel="Stylesheet" href="Styles/sms-gage.css" type="text/css" media="all" />
    <link rel="Stylesheet" href="Styles/client-details.css" type="text/css" media="all" />
</asp:Content>
