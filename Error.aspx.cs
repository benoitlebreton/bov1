﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class erreur : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Exception ex = Server.GetLastError();

            string sServerName = ConfigurationManager.AppSettings["ServerName"].ToString();

            if (ex != null)
            {
                if (ex.GetBaseException() != null)
                    ex = ex.GetBaseException();

                try
                {
                    HttpException httpEx = (HttpException)ex;

                    if (httpEx != null && httpEx.GetHttpCode() == 404)
                    {
                        lblErrorTitle.Text = "404";

                        string sPath = HttpContext.Current.Request.Url.AbsolutePath;
                        string[] ar = sPath.Split('/');
                        string sPageName = ar[ar.Length - 1];

                        lblErrorMessage.Text = "La page " + sPageName + " n'existe pas.";
                    }
                    else throw new Exception("", ex);
                }
                catch (Exception ex2)
                {
                    if (ex is InvalidCastException)
                    {
                        Session.RemoveAll();
                        Response.Redirect("authentication.aspx");
                    }
                    else
                    {

                        lblErrorTitle.Text = "Oups ! Une erreur s'est produite.";
                        //lblErrorMessage.Text = "Une erreur s'est produite.<br/><br/>Notre équipe technique a été informée du problème et se charge de le régler au plus vite.";

                        System.Text.StringBuilder sb = new System.Text.StringBuilder();

                        if (!string.IsNullOrEmpty(sServerName.Trim()))
                            sb.AppendLine("Serveur : " + sServerName);
                        sb.AppendLine("Exception : " + ex.GetType().Name);
                        sb.AppendLine("Message : " + ex.Message);
                        //sb.AppendLine("Source : " + ex.Source);
                        sb.AppendLine("Stack Trace : " + ex.StackTrace);
                        lblErrorMessage.Text = sb.ToString().Replace(Environment.NewLine, "<br/>");
                    }
                }
            }
            else
                Response.Redirect("Default.aspx");
        }
    }
}