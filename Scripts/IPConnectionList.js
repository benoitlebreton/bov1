﻿function SearchIPAddressLocation() {
    
    $('.ip-address-to-lookup').each(function () {
        var that = $(this);
        var ip = $(that).html();
        $.ajax({
            type: "POST",
            url: 'ws_tools.asmx/getIPAddressLocation',
            data: '{ "sIPAddress": "' + ip + '" }',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {
                var json = JSON.parse(msg.d);
                //console.log(json);
                if (json.Country != null && Object.keys(json.Country).length > 0)
                    $(that).html(ip + ' (' + json.Country + ')');
            },
            error: function (e) {

            }
        });
    });
}