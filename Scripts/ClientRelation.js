﻿function ShowPanelRelation() {
    $('.relation-container').dialog({
        autoOpen: true,
        resizable: false,
        draggable: false,
        modal: true,
        minWidth: 800,
        //zIndex: 99997,
        title: "Liste des relations"
    });
    $('.relation-container').parent().appendTo(jQuery("form:first"));
}

function InitRelationControls(nbCol) {
    $('.radioButtonList').buttonset();
    $('.relation-container table').tableHeadFixer({
        "head": true,
        "left":nbCol
    });
}