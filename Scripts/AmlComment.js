﻿function BindCommentLengthCheck(inputID, labelID) {
    $('#' + labelID).html($('#' + inputID).val().length);
    $('#' + inputID).on('input', function () {
        $('#' + labelID).html($('#' + inputID).val().length);
    });
}

function InitAmlMultiComment() {
    $('#show-previous-aml-comment').unbind('click').bind('click', function () {
        if ($('#previous-aml-comment').is(':visible')) {
            $('#previous-aml-comment').slideUp();
            $('#show-previous-aml-comment').html('Afficher les commentaires précédents');
        }
        else {
            $('#previous-aml-comment').slideDown(function () {
                var scrollHeight = document.getElementById('previous-aml-comment').scrollHeight;
                $('#previous-aml-comment').animate({ scrollTop: scrollHeight }, 'normal');
            });
            $('#show-previous-aml-comment').html('Masquer les commentaires précédents');
        }
    });
}