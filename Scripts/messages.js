var MessageNo = 0;
var NbOfMessage = 0;
var Message;

var MessageHtmlNo = 0;
var NbOfMessageHtml = 0;
var MessageHtml;

var MessageListValues = "";
var HtmlMessageListValues = "";

function showMessageList(msgList, htmlMsgList) {
    MessageListValues = msgList;
    HtmlMessageListValues = htmlMsgList;

    MessageManagement();
}

function MessageManagement() {
    showAllButtonsActivated();
    $('#divMessageContainer').empty();

    if (MessageListValues.trim().length > 0) {
        var MessageList = MessageListValues.split(';');
        NbOfMessage = MessageList.length;

        if (MessageList.length > 0) {
            if (MessageNo < NbOfMessage) {
                Message = MessageList[MessageNo];
                MessageNo++;
                showMessage();
            }
            else {
                MessageHtmlManagement();
            }
        }
    }
    else {
        MessageHtmlManagement();
    }
}

function showMessage() {
    $('#divMessageContainer').append('<div id="divMessage" style="display:none">' + Message + '</div>');

    $("#divMessage").dialog("destroy");
    $("#divMessage").dialog({
        autoOpen: true,
        resizable: false,
        height: 300,
        width: 800,
        modal: true,
        title: "Message",
        beforeClose: function () { MessageManagement(); },
        buttons: {
            "Message lu": function () { $(this).dialog("close"); }
        }
    });
}

function MessageHtmlManagement() {
    $('#divMessageContainer').empty();

    MessageListHtml = HtmlMessageListValues.split(';');
    NbOfMessageHtml = MessageListHtml.length;

    if (HtmlMessageListValues.trim().length > 0) {

        if (MessageHtmlNo < NbOfMessageHtml) {
            MessageHtml = MessageListHtml[MessageHtmlNo];
            MessageHtmlNo++;
            showMessageHtml();
        }
    }
}

function showMessageHtml() {
    $('#divMessageContainer').append('<div id="divMessageHtml" style="display:none"><object id="objHtmlMessage" type="text/html" data="' + MessageHtml + "?data=" + new Date().toString() + '" width="880" height="490"></object></div>');

    $("#divMessageHtml").dialog("destroy");
    $("#divMessageHtml").dialog({
        resizable: false,
        height: 600,
        width: 900,
        title: "Message",
        modal: true,
        beforeClose: function () { MessageHtmlManagement(); },
        buttons: {
            "Message lu": function () { $(this).dialog("close"); }
        }
    });
}

function hideAllButtonsActivated() {
    $('input[type="submit"]:visible,input[type="button"]:visible').css('visibility', 'hidden').addClass('hiddenTmp');
}

function showAllButtonsActivated() {
    $('.hiddenTmp').css('visibility', 'visible').removeClass('hiddenTmp');
}