﻿var profileDialog = null;
function ShowProfileSelector() {
    profileDialog = $('#popup-profile-selector').dialog({
        autoOpen: true,
        resizable: false,
        draggable: false,
        modal: true,
        minWidth: 550,
        zIndex: 99997,
        title: "Choisir un profil"
    });
    //$('#popup-profile-selector').parent().appendTo(jQuery("form:first"));
}
function SelectClientProfile(profile, lbl, hf) {
    //console.log(hf);
    $(profileDialog).dialog('close');
    $(profileDialog).dialog('destroy');
    $('#' + lbl).html(profile);
    $('#' + hf).val(profile);
    $('#' + hf).change();
}