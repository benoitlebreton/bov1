﻿var ctrlMode = false;
var beneficiaryOldValue = "";
function beneficiaryClearValue(txt) {
    beneficiaryOldValue = $(txt).val();
    $(txt).val('');
}

function beneficiaryDefaultVal(txt) {
    if ($(txt).val().length == 0 && beneficiaryOldValue.trim().length > 0) {
        $(txt).val(beneficiaryOldValue);
    }
}

function beneficiaryInit() {
    $('.iban_1').blur(function () {
        $('.iban_1').val($('.iban_1').val().toUpperCase());
        beneficiaryDefaultVal($('.iban_1'));
    }).bind('paste', function (e) {
        var text = e.originalEvent.clipboardData.getData('Text');
        setIban(text);
        setTimeout(function () { $('.bic').focus(); }, 500);
    })

    $('#IBAN_1').blur(function () {
        $(this).val($(this).val().toUpperCase());
        beneficiaryDefaultVal($(this));
    }).bind('paste', function (e) {
        var text = e.originalEvent.clipboardData.getData('Text');
        setIban(text);
        setTimeout(function () { $('.bic').focus(); }, 500);
    });

    initBicAutoComplete();
};

function initAddBeneficiaryPanel(btnAddId) {
    var IbanLeftOffset = $('#divIban').offset().left.toFixed();
    var IbanTopOffset = $('#divIban').offset().top.toFixed();
    var IbanInfoLeftWidth = $('#divCellLeft').width();
}

function showIbanRib() {
    selectedValue = "";
    selectedValue = $('.radioAddChoice:checked').val();

    $('#divRib').hide();
    $('#divIban').hide();
    $('#divBic').hide();
    $('#divMail').hide();
    if (selectedValue != null && selectedValue.trim().length > 0) {
        if (selectedValue == 'iban') {
            $('#divRib').hide();
            $('#divIban').fadeIn();
            $('#divBic').fadeIn();
            $('#divMail').fadeIn();
        }
        else if (selectedValue == 'rib') {
            $('#divIban').hide();
            $('#divBic').hide();
            $('#divRib').fadeIn();
            $('#divMail').fadeIn();
        }
    }
}

function setIban(iban) {
    iban = iban.replace(/ /g, '');
    $('.iban_1').val(iban.substr(0, 2));
    $('.iban_2').val(iban.substr(2, 2));
    $('.iban_3').val(iban.substr(4, iban.length - 4));

    $('#IBAN_1').val(iban.substr(0, 2));
    $('#IBAN_2').val(iban.substr(2, 2));
    var from = 4;
    for (var i = 3; i < 11; i++) {
        $('#IBAN_' + i.toString()).val(iban.substr(from, 4));
        from = from + 4;
    }
    //console.log(iban);
}

function InputIBANValue() {
    //IBAN
    $('.iban_1').val($('#IBAN_1').val().trim());
    $('.iban_2').val($('#IBAN_2').val().trim());
    $('.iban_3').val('');
    for (var i = 3; i < 11; i++) {
        $('.iban_3').val($('.iban_3').val() + $('#IBAN_' + i.toString()).val().trim());
    }
}

function InitIBANinputs() {
    $('.input-iban input').on('input', function () {
        //console.log('input');
        InputIBANValue();
    });
}

function checkBeneficiary(ReturnID) {
    var isOK = true;
    var error = "";

    $('.iban_1').val('');
    $('.iban_2').val('');
    $('.iban_3').val('');
    $('.rib_1').val('');
    $('.rib_2').val('');
    $('.rib_3').val('');
    $('.rib_4').val('');

    if ($('#divIban').is(':visible')) {
        InputIBANValue();

        var IbanValue = $('.iban_1').val().trim() + $('.iban_2').val().trim() + $('.iban_3').val().trim()
        if ($('.iban_1').val().trim().length < 2 ||
            $('.iban_2').val().trim().length < 2 ||
        $('.iban_3').val().trim().length == 0 ||
        !validateIBAN(IbanValue)) {
            error += "<li>IBAN</li>";
            isOK = false;
        }

        //BIC
        var bic = $('.bic').val().trim();
        if ($('.bic').val().trim().length == 0) {
            error += "<li>BIC</li>";
            isOK = false;
        }
    }
    else {
        //RIB
        $('.rib_1').val($('#RIB_1').val().trim());
        $('.rib_2').val($('#RIB_2').val().trim());
        $('.rib_3').val($('#RIB_3').val().trim());
        $('.rib_4').val($('#RIB_4').val().trim());
        if (!validateRIB($('.rib_1').val(), $('.rib_2').val(), $('.rib_3').val(), $('.rib_4').val())) {
            error += "<li>RIB</li>";
            isOK = false;
        }
    }

    //NAME
    var name = $('.beneficiaryName').val().trim();
    if ($('.beneficiaryName').val().trim().length == 0) {
        isOK = false;
        error += "<li>Nom du bénéficiaire</li>";
    }
    reg = /^[0-9a-zA-Z /\-\?:()\.\,'\+]*$/g;
    if (!reg.test($('.beneficiaryName').val())) {
        isOK = false;
        error += '<li>Nom du bénéficiaire : seuls les caractères numériques, alphabétiques (sans accent) et / - ? : ( ) . , \' + sont autorisés.</li>';
    }

    //EMAIL
    var email = $('.beneficiaryMail').val();
    if (email.trim().length > 0) {
        emailRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (!emailRegex.test(email)) {
            isOK = false;
            error += "<li>EMAIL du bénéficiaire</li>";
        }
    }

    if (!isOK)
        showAlertBeneficiary("ERREUR", "Les champs suivants ne sont pas valides : <ul>" + error + "</ul>");
    else if (!checkSepaCountry($('.iban_1').val().trim())) {
        //PAYS
        error += showAlertBeneficiary("ERREUR", "<div>Le virement vers ce compte est impossible car il ne se situe pas dans la liste des pays autorisés : </div><div style=\"margin-top:10px\">" + countryList + "</div>");
        isOK = false;
    }

    //alert(isOK);

    if (ReturnID != null)
        ReturnID.val(name.replace(/|/g, '') + '|' + IbanValue + '|' + bic + '|' + email);

    return isOK;
}

var countryList = "";
function checkSepaCountry(iban_1) {
    isOK = false;
    countryList = "";
    $('.ddlIban_1 option').each(function () {
        countryList += $(this).text().trim() + ", ";
        if ($(this).val().trim() == iban_1.trim())
            isOK = true;
    });

    countryList = countryList.substr(0, countryList.length - 2).trim();

    return isOK;
}

function initBeneficiaryFields(name, iban, bic, email) {
    try {
        $('.beneficiaryName').val(name);
        $('.bic').val(bic);
        $('.custom-combobox input').val(bic);
        $('.beneficiaryMail').val(email);

        setIban(iban);
        setTimeout(function () { $('.beneficiaryName').focus(); }, 500);
    }
    catch(ex)
    {
        console.log("initBeneficiaryFields error : " + ex.toString());
    }
}

function showAlertBeneficiary(titleMessage, message) {
    $('.beneficiaryAlert').html(message);

    $('#dialog-beneficiary-alert').dialog({
        title: titleMessage,
        width: 500,
        resizable: false,
        modal: true,
        closeOnEscape: false,
        draggable: false,
        dialogClass: "no-close",
        title: titleMessage,
        buttons: {
            Fermer: function () {
                $(this).dialog("close");
            }
        }
    });
}

var arBicList;
function initBicAutoComplete() {
    $('.bic').autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "./ws_tools.asmx/GetBicList",
                data: '{ "prefix": "' + request.term + '"}', //, culture: 'fr-FR', order: 'ASC' 
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    arBicList = new Array();
                    response(
                        $.map(data.d, function (item) {
                            var bic = item.split('|')[0].trim();
                            var bank = item.split('|')[1].trim();
                            arBicList.push(item);
                            //console.log(item);
                            //var bic = item.toString().split("")[0].trim();

                            return {
                                value: bic + ' (' + bank + ')'
                            }
                        })
                    )
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert('error');
                }
            });
        },
        _renderItem: function (ul, item) {
            return $("<li>")
            .attr("data-value", item.value)
            .append($("<a>").text(item.label))
            .appendTo(ul);
        },
        position: { of: '.bic', my: 'left top', at: 'left bottom' },
        minLength: 1,
        select: function (event, ui) {
            var bicValue = "";
            var bankName = "";
            var bankCity = "";
            var bankCountry = "";
            var isBankDetailsOK = false;
            $('#divBankInformation').hide();
            if (ui.item != null && ui.item.value.length > 0) {
                bicValue = ui.item.value.split('(')[0].trim();
                for (var i = 0; i < arBicList.length; i++) {
                    if (arBicList[i].split('|')[0].trim() == bicValue) {
                        isBankDetailsOK = true;
                        bankName = arBicList[i].split('|')[1].trim();
                        bankCity = arBicList[i].split('|')[2].trim();
                        bankCountry = arBicList[i].split('|')[3].trim();
                        //$('#divBankInformation').css("visibility", "visible");
                    }
                }
            }
            setTimeout(function () {
                $('.bic').val(bicValue);
                //console.log(isBankDetailsOK + "-" + bankName + "-" + bankCity + "-" + bankCountry);
                if (isBankDetailsOK) {
                    $('#lblBankName').html(bankName);
                    $('#lblBankCity').html(bankCity)
                    $('#lblBankCountry').html(bankCountry);
                    $('#divBankInformation').fadeIn();
                }
            }, 10);
        },
        open: function () {
            //var sKeyboardWidth = $('.ui-keyboard:visible').width()+4;
            //$(this).autocomplete('widget').css('z-index', 16000).css('width', sKeyboardWidth);
            //$(this).autocomplete('widget').css('z-index', 16000);
            //$(this).autocomplete('widget').css('left', 0);
            //$('.ui-autocomplete:visible li').css('padding-left', '90px');
            //$('.ui-autocomplete:visible li').css('width', $('.ui-autocomplete:visible li').width() - 180);
            return false;
        },
        close: function () {
            //$(this).removeClass("ui-corner-top").addClass("ui-corner-all");
        }
    });
}

function onBicChange() {
    //alert('bic changed');
    var bankName = "";
    var bankCity = "";
    var bankCountry = "";
    var bankBIC = "";
    $('#divBankInformation').hide();

    if (arBicList != null && arBicList.length > 0) {

        for (var i = 0; i < arBicList.length; i++) {
            if (arBicList[i].split('|')[0].trim() == $('.bic').val().split('(')[0].trim().replace('XXX', '')) {
                bankName = arBicList[i].split('|')[1].trim();
                bankCity = arBicList[i].split('|')[2].trim();
                bankCountry = arBicList[i].split('|')[3].trim();
                //$('#divBankInformation').css("visibility", "visible");
                $('#lblBankName').html(bankName);
                $('#lblBankCity').html(bankCity)
                $('#lblBankCountry').html(bankCountry);
                $('.bic').val(arBicList[i].split('|')[0].trim());

                setTimeout(function () {
                    if (!$('#divBankInformation').is(':visible'))
                        $('#divBankInformation').show();
                }, 20);
            }
            
        }
    }
}

function beneficiaryToggleHelp(type) {
    sType = "iban"
    if (type != null && type.length > 0)
        sType = type.trim().toLowerCase();

    if ($('#help-'+sType).is(':visible'))
        beneficiaryHideHelp(sType);
    else beneficiaryShowHelp(sType);
}
function beneficiaryShowHelp(type) {
    $('#help-'+type).fadeIn();
    $('#help-'+type).position({ my: 'left top', at: 'right+5px top+5px', of: "#link-"+type+"-Astuce" });
    setTimeout(function () { $(window).bind('click', function () { beneficiaryHideHelp(); $(window).unbind('click'); }); }, 200);
}
function beneficiaryHideHelp(type) {
    $('#help-'+type).fadeOut();
}

function validateIBAN($v) { //This function check if the checksum if correct
    $v = $v.replace(/^(.{4})(.*)$/, "$2$1"); //Move the first 4 chars from left to the right
    $v = $v.replace(/[A-Z]/g, function ($e) { return $e.charCodeAt(0) - 'A'.charCodeAt(0) + 10 }); //Convert A-Z to 10-25
    var $sum = 0;
    var $ei = 1; //First exponent 
    for (var $i = $v.length - 1; $i >= 0; $i--) {
        $sum += $ei * parseInt($v.charAt($i), 10); //multiply the digit by it's exponent 
        $ei = ($ei * 10) % 97; //compute next base 10 exponent  in modulus 97
    };
    return $sum % 97 == 1;
}

function validateBBAN($v) { //This function check if the checksum if correct
    $v = $v.replace(/[A-Z]/g, function ($e) { return $e.charCodeAt(0) - 'A'.charCodeAt(0) + 10 }); //Convert A-Z to 10-25
    var $sum = 0;
    var $ei = 1; //First exponent 
    for (var $i = $v.length - 1; $i >= 0; $i--) {
        $sum += $ei * parseInt($v.charAt($i), 10); //multiply the digit by it's exponent 
        $ei = ($ei * 10) % 97; //compute next base 10 exponent  in modulus 97
    };
    return $sum % 97 == 1;
}