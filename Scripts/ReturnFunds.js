﻿function InitIBANinput() {
    $('.iban-input').mask('SSAA AAAA AAAA AAAA AAAA AAAA AAAA AAAA AAAA', {});
}

function SearchBIC() {
    console.log($('.iban-input').val());
    $('.bic-input').attr('disabled', true);
    $.ajax({
        type: "POST",
        url: 'ws_tools.asmx/GetBicFromIban',
        data: "{\"iban\":\"" + $('.iban-input').val() + "\"}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        complete: function (xhr, msg) {
            $('.bic-input').attr('disabled', false);
        },
        success: function (msg) {
            console.log(msg);
            $('.bic-input').val(msg.d);
            return msg.d;
        },
        error: function (e) {
        }
    });
}