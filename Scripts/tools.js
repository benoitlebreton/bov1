﻿function checkMaxLength(text, maxlength, idCurrentChar, idMaxChar) {
    //console.log("***checkMaxLengh***");
    //console.log(text);
    //console.log(maxlength);
    //console.log(idCurrentChar);
    //console.log(idMaxChar);
    //console.log("*******************");
    //asp.net textarea maxlength doesnt work; do it by hand
    //var maxlength = 2000; //set your value here (or add a parm and pass it in)
    if (maxlength == null) {
        if (idMaxChar != null && $('#' + idMaxChar) != null)
            maxlength = $('#' + idMaxChar).html();//$('#<%=lblNbMaxClientNote.ClientID%>').html();
        else
            maxlength = 200;
    }

    var object = document.getElementById(text.id)  //get your object
    if (object.value.length > maxlength) {
        object.focus(); //set focus to prevent jumping
        object.value = text.value.substring(0, maxlength); //truncate the value
        object.scrollTop = object.scrollHeight; //scroll to the end to prevent jumping
        if (idCurrentChar != null && $('#' + idCurrentChar) != null)
            $('#' + idCurrentChar).css('color', '#FF0000');
        return false;
    }
    else if (object.value.length == maxlength) {
        if (idCurrentChar != null && $('#' + idCurrentChar) != null)
            $('#' + idCurrentChar).css('color', '#FF0000');
    }
    else {
        if (idCurrentChar != null && $('#' + idCurrentChar) != null)
            $('#' + idCurrentChar).css('color', '#344B56');
    }

    if (idCurrentChar != null && $('#' + idCurrentChar) != null)
        $('#' + idCurrentChar).html(object.value.length);
    return true;
}

function showAlertError(error) {
    if (error.trim().length > 0) {
        $('#error').html(error);

        $('#popup-error').dialog({
            autoOpen: true,
            resizable: false,
            draggable: false,
            modal: true,
            title: "Erreur",
            buttons: [{ text: 'Fermer', click: function () { $(this).dialog('close'); $(this).dialog('destroy'); } }]
        });
    }
}

function checkClientNote(noteValue) {
    //console.log("checkClientNote|" + noteValue);
    var isOK = false;

    if (noteValue.trim().length > 0)
        isOK = true;
    else { showAlertError('Veuillez saisir un commentaire.'); }

    return isOK;
}


function getInputValue(id) {
    //console.log("getInputValue|" + id);
    if ($('#' + id) != null)
        return $('#' + id).val();
    else
        return "";
}

function are_cookies_enabled() {
    var cookieEnabled = (navigator.cookieEnabled) ? true : false;

    if (typeof navigator.cookieEnabled == "undefined" && !cookieEnabled) {
        document.cookie = "testcookie";
        cookieEnabled = (document.cookie.indexOf("testcookie") != -1) ? true : false;
    }
    return (cookieEnabled);
}

function checkHeaderBand(functionToCall) {
    setTimeout(function () {
        if ($('.headband').is(':visible')) {
            var headbandHeight = $('.headband').height()-20;
            if (headbandHeight < 60) { headbandHeight = 80; }

            if (functionToCall != null)
                $('.header').animate({ marginTop: headbandHeight + 'px' }, 1000, functionToCall);
            else
                $('.header').animate({ marginTop: headbandHeight + 'px' }, 1000);
        }
        else
            functionToCall();
    }, 1000);
}

function showNbAlert(nbAlert, lblNbAlertId, divNbAlertId, divParentId) {
    try {
        $('#' + lblNbAlertId).html(nbAlert);
        //var tabLeftOffset = $('.ClientDetails a[href="#' + divParentId + '"]').offset().left.toFixed();
        //var tabTopOffset = $('.ClientDetails a[href="#' + divParentId + '"]').offset().top.toFixed();
        //var tabWidth = $('.ClientDetails a[href="#' + divParentId + '"]').width().toFixed();

        $("#" + divNbAlertId).detach().appendTo($("li[role='tab'][aria-controls='" + divParentId + "']")).show();

        //$("#" + divNbAlertId).css("top", (parseInt(tabTopOffset) - 15).toString() + "px").css("left", (parseInt(tabLeftOffset) + parseInt(tabWidth) + 10).toString() + "px").show();
    }
    catch (ex) { console.log(ex.toString()); }
}

function initTooltip() {
    $('.tooltip').tooltip({
        position: {
            my: "center bottom-10",
            at: "center top",
            using: function (position, feedback) {
                $(this).css(position);
                $("<div>")
                    .addClass("arrow")
                    .addClass(feedback.vertical)
                    .addClass(feedback.horizontal)
                    .appendTo(this);
            }
        }
    }).css('cursor', 'Help');

    $('.image-tooltip').tooltip({
        position: {
            my: "center bottom-10",
            at: "center top",
            using: function (position, feedback) {
                $(this).css(position);
                $("<div>")
                    .addClass("arrow")
                    .addClass(feedback.vertical)
                    .addClass(feedback.horizontal)
                    .appendTo(this);
            }
        }
    });
}