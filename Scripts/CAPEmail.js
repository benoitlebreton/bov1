﻿var wsEmailURL = 'https://ws.capadresse.com:8684';
//var wsEmailURL = 'https://dev.capadresse.com:8684';

function ShowCAPEmailDialog() {
    //InitCAPEmail();
    $('#dialog-cap-email').dialog({
        title: 'Modifier l\'email',
        autoOpen: true,
        modal: true,
        draggable: false,
        resizable: false
    });
    $("#dialog-cap-email").parent().appendTo(jQuery("form:first"));
}

//function InitCAPEmail() {
//    $('.txtEmail').autocomplete({
//        source: function (request, response) {
//            if(request.term.indexOf('@') != -1)
//                RequestCAPEmail(request.term, function (data) {
//                    response(FormatToComboItem(data));
//                });
//        },
//        minLength: 3
//    });
//}

//function RequestCAPEmail(searchValue, cb) {
//    var requestURL = wsEmailURL + '?request=SearchMailCivWS&sInput=' + searchValue + '&sPays=FRA';

//    //console.log(requestURL);

//    $.getJSON(requestURL)
//    .done(function (data) {
//        //console.log(data);
//        cb(data);
//    });
//}

//function FormatToComboItem(data) {
//    //console.log(data);
//    var arEmail = [];
//    if (data.iRet == 0 && data.Mail != null && data.Mail.length > 0) {
//        $.each(data.Mail, function (i, email) {
//            var item = new Object();
//            item.value = email.sMail;
//            item.label = email.sMail;
//            arEmail.push(item);
//        });
//    }

//    return arEmail;
//}

function ValidateEmail() {
    var email = $('.txtEmail').val();
    var requestURL = wsEmailURL + '?request=CheckMailProp&sMail=' + email;
    $.getJSON(requestURL)
    .done(function (data) {
        console.log(data);
        $('.txtEmailQualityCode').prop('value', data.sCodeTraitement[0]);
        if (data.iRet == 0) {
            ForceValidateEmail();
        }
        else if (data.iRet == 1) {
            if (data.sCodeTraitement == '4004X')
                AlertMessage('Erreur ', 'Les adresses emails jetables ne sont pas acceptées.');
            else ShowConfirmEmail(data);
        }
        else if (data.iRet == 2) {
            if (data.Mail != null && data.Mail.length > 1) {
                ShowEmailMultiSelect(data);
            }
            else ShowConfirmEmail(data);
        }
    })
    .fail(function (jqxhr, textStatus, error) {
        var err = "Une erreur est survenue";
        if (error.length > 0)
            err += " (" + error + ")";
        AlertMessage('Erreur ', err);
        ForceValidateEmail();
    });
}

function ShowEmailMultiSelect(emails) {
    $('#dialog-cap-multi-address #select-list').empty();
    $.each(emails.Mail, function (i, email) {
        $('#dialog-cap-multi-email #select-list').append(CreateEmailSelectItem(email));
    });

    $('#dialog-cap-multi-email').dialog({
        title: 'Votre saisie retourne plusieurs résultats',
        autoOpen: true,
        modal: true,
        draggable: false,
        resizable: false,
        minWidth: 400
    });
    $("#dialog-cap-multi-email").parent().appendTo(jQuery("form:first"));
}
function CreateEmailSelectItem(email)
{
    var item = $('<div/>').click({ param: email }, UseSelectedEmail);
    item.append($('<pre/>').html(email.sMail.trim()));
    return item;
}
function UseSelectedEmail(event) {
    var email = event.data.param;
    //console.log(address);
    $('.txtEmail').val(email.sMail);
    $('.txtEmailQualityCode').prop('value', '0');
    ForceValidateEmail();
}
function ForceValidateEmail(force) {
    //if (force != null && force == true)
    //    $('.txtEmailQualityCode').prop('value', '1');
    if ($('#dialog-cap-force-email').is(':visible'))
        $('#dialog-cap-force-email').dialog('destroy');
    if ($('#dialog-cap-email').is(':visible'))
        $('#dialog-cap-email').dialog('destroy');
    if ($('#dialog-cap-multi-email').is(':visible'))
        $('#dialog-cap-multi-email').dialog('destroy');
    $('.btnValidateEmail').click();
}

function ShowConfirmEmail(data) {
    $('.txtEmail').val(data.Mail[0].sMail);
    $('#dialog-cap-force-email .message').empty().append(data.sMessage);
    $('#dialog-cap-force-email').dialog({
        title: 'Attention',
        autoOpen: true,
        modal: true,
        draggable: false,
        resizable: false,
        minWidth: 400
    });
    $("#dialog-cap-force-email").parent().appendTo(jQuery("form:first"));
}