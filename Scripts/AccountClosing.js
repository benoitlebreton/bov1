﻿function checkKeyDown(evt, txtID) {
    var isOK = false;

    if (evt.ctrlKey)
        ctrlMode = true;
    else
        ctrlMode = false;

    if (!ctrlMode) {
        evt = evt || window.event;
        var charCode = evt.which || evt.keyCode;
        var charStr = String.fromCharCode(charCode);

        var idRoot = txtID.substr(0, txtID.lastIndexOf('_'));
        var index = parseInt(txtID.split('_')[txtID.split('_').length - 1]);
        var lastID = idRoot + '_' + (index - 1);

        if (charCode == 8 && index != 1 && $('#' + txtID).val().length == 0) {
            //$('#' + lastID).focus();
            setTimeout(function () { $('#' + lastID).focus(); }, 50);
            oldValue = "";
        }

        isOK = true;
    }
    else
        isOK = true;

    return isOK;
}
function checkNext(event, txtID) {

    if (!ctrlMode) {
        var keyCode = ('which' in event) ? event.which : event.keyCode;
        var idRoot = txtID.substr(0, txtID.lastIndexOf('_'));
        var index = parseInt(txtID.split('_')[txtID.split('_').length - 1]);
        var nextID = idRoot + '_' + (index + 1);
        var lastID = idRoot + '_' + (index - 1);
        maxIndex = 10;
        //console.log(txtID+'-'+lastID + '-' + nextID + '-' + keyCode);

        if (keyCode != 8) {
            if (($('#' + txtID).val().length) == parseInt($('#' + txtID).attr('maxlength'))) {
                if (index < maxIndex) {
                    $('#' + txtID).val($('#' + txtID).val().toUpperCase());
                    $('#' + nextID).focus();
                }
                else {
                    var keyCode = ('which' in event) ? event.which : event.keyCode;
                }
            }
        }
    }
}

function date_picker() {
    $('.datePicker').mask("99/99/9999");
    $.datepicker.setDefaults($.datepicker.regional["fr"]);

    $('.datePicker').each(function () {
        $(this).datepicker($.datepicker.regional['fr'] = {
            closeText: 'Fermer',
            prevText: '&#x3c;Préc',
            nextText: 'Suiv&#x3e;',
            currentText: 'Courant',
            monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
            monthNamesShort: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
            dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
            dayNamesShort: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],
            dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
            weekHeader: 'Sm',
            dateFormat: 'dd/mm/yy',
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: '',
            changeMonth: false,
            changeYear: false,
            defaultDate: 0,
            minDate: 0,
            maxDate: "1y"
        });
    });


}

function getMinDate(obj) {
    var dtDate = new Date(2013, 1 - 1, 1);
    if (obj != null && $(obj).attr('minDate') != null) {
        dtDate = $(obj).attr('minDate');
    }
    return dtDate;
}
function getMaxDate(obj) {
    var dtDate = new Date(2013, 1 - 1, 1);
    if (obj != null && $(obj).attr('maxDate') != null) {
        dtDate = $(obj).attr('maxDate');
    }
    return dtDate;
}