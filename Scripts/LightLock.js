﻿function initTooltip() {
    $('.image-tooltip').tooltip({
        position: {
            my: "center bottom-10",
            at: "center top",
            using: function (position, feedback) {
                $(this).css(position);
                $("<div>")
                    .addClass("arrow")
                    .addClass(feedback.vertical)
                    .addClass(feedback.horizontal)
                    .appendTo(this);
            }
        }
    });
}

function showDebitStatusDetails() {
    $("#divDebitStatus-popup").dialog({
        resizable: false,
        draggable: false,
        width: 750,
        modal: true,
        zIndex: 99997,
        title: "Bloquer/débloquer débit client",
        buttons:
            [{
                text: 'Fermer',
                click: function () { $(this).dialog('close'); $(this).dialog('destroy'); }
            }],
        class: 'OnTopDialog'
    });
    $("#divDebitStatus-popup").parent().appendTo(jQuery("form:first"));
}
function clientDebitRefReasonChange(obj) {
    $('#hfClientDebitLockRefReason_selected').val($('#' + obj.id).val());
}
function selectAllClientDebitLockReason() {
    if ($('.cbAllClientDebitLockReason  input[type="checkbox"]').is(':checked'))
        $('.cbClientDebitLockReason input[type="checkbox"]').removeAttr('checked').attr('checked', 'checked');
    else
        $('.cbClientDebitLockReason input[type="checkbox"]').removeAttr('checked');
}
function checkClientDebitAddLock() {
    var isOK = false;
    $('#ddlClientDebitLockReason_req').css('visibility', 'visible');
    if ($('#hfClientDebitLockRefReason_selected').val().trim().length > 0) {
        $('#divClientDebitStatus img:first-of-type').attr('src', './Styles/Img/loading.gif');
        $('#divDebitStatus-popup').dialog('close');
        $('#divDebitStatus-popup').dialog('destroy');
        isOK = true;
    }
    else
        $('#ddlClientDebitLockReason_req').css('visibility', 'visible');

    return isOK;
}
function ClientDebitLockUnlockHistory_DisplayManagement() {
    if ($('#divClientDebitLockUnlockListHistory').is(':visible'))
        $('#divClientDebitLockUnlockListHistory').slideUp(function () {
            $('#linkClientDebitLockUnlockHistory_DisplayManagement').html("Afficher historique");
        });
    else
        $('#divClientDebitLockUnlockListHistory').slideDown(function () {
            $('#linkClientDebitLockUnlockHistory_DisplayManagement').html("Masquer historique");
        });
}

function LightLockShowPanelLoading(panelID) {
    var panel = $('#' + panelID);
    if (panelID.trim().length > 0 && $(panel).length) {

        $('.lightlock-panel-loading').show();
        $('.lightlock-panel-loading').width(panel.outerWidth());
        $('.lightlock-panel-loading').height(panel.outerHeight());

        $('.lightlock-panel-loading').position({
            my: 'center',
            at: 'center',
            of: panel
        });
    }
}
function LightLockHidePanelLoading() {
    $('.lightlock-panel-loading').hide();
}