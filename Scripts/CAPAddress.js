﻿var wsURL = 'https://ws.capadresse.com:8683';
//var wsURL = 'https://dev.capadresse.com:8683';

var searchedAddress;
function InitCAPAddress() {

    ClearInputs();

    searchedAddress = new Object();
    $('.txtZipcode').autocomplete({
        source: function (request, response) {
            RequestCAPAddress(request.term, null, 'SearchLocality', function (data) { 
                response(FormatToComboItem(data, "Zipcode"));
            });
        },
        minLength: 3,
        select: function (event, ui) {
            $('.txtCity').val(ui.item.locality);
            $('.txtComplCity').val(ui.item.localitySynonym);
            searchedAddress.localityId = ui.item.localityId;
            $('.txtStreet').focus();
        }
    });
    $('.txtCity').autocomplete({
        source: function (request, response) {
            RequestCAPAddress(request.term, null, 'SearchLocality', function (data) {
                response(FormatToComboItem(data, "City"));
            });
        },
        //minLength: 3,
        select: function (event, ui) {
            $('.txtZipcode').val(ui.item.desc);
            $('.txtComplCity').val(ui.item.localitySynonym);
            searchedAddress.localityId = ui.item.localityId;
            $('.txtStreet').focus();
        }
    });
    $('.txtStreet').autocomplete({
        source: function (request, response) {
            RequestCAPAddress(request.term, '&localityId=' + searchedAddress.localityId, 'SearchStreet', function (data) {
                response(FormatToComboItem(data, "Street"));
            });
        },
        //minLength: 3,
        select: function (event, ui) {
            searchedAddress.streetId = ui.item.streetId;
            $('.txtStreetNumber').val(ui.item.streetNumber);
            searchedAddress.streetNumberId = ui.item.streetNumberId;

            $('.txtZipcode').val(ui.item.postalCode);
            searchedAddress.localityId = ui.item.localityId;
            $('.txtStreetNumber').focus();
        }
    });
    $('.txtStreetNumber').autocomplete({
        source: function (request, response) {
            RequestCAPAddress(request.term, '&localityId=' + searchedAddress.localityId + '&streetId=' + searchedAddress.streetId, 'SearchStreetNumber', function (data) {
                response(FormatToComboItem(data, "StreetNumber"));
            });
        },
        select: function (event, ui) {
            searchedAddress.streetNumberId = ui.item.streetNumberId;
        }
    });
    $('.txtCompl1,.txtCompl2').autocomplete({
        source: function (request, response) {
            RequestCAPAddress(request.term, '&localityId=' + searchedAddress.localityId + '&streetId=' + searchedAddress.streetId + '&streetNumberId=' + searchedAddress.streetNumberId, 'SearchBuilding', function (data) {
                response(FormatToComboItem(data, "Building"));
            });
        },
        select: function (event, ui) {
            searchedAddress.buildingId = ui.item.buildingId;
        }
    });
}

function ClearInputs() {
    $('.txtZipcode').val('');
    $('.txtCity').val('');
    $('.txtComplCity').val('');
    $('.txtStreet').val('');
    $('.txtStreetNumber').val('');
    $('.txtCompl1').val('');
    $('.txtCompl2').val('');
    $('.txtAddressQualityCode').prop('value', '');
}

function FormatToComboItem(data, request)
{
    //console.log(data);
    var arAddress = [];
    if (data.addresses != null && data.addresses.address.length > 0) {
        $.each(data.addresses.address, function (i, addr) {
            if (addr.step !== 'Forced') {
                var item = new Object();

                switch (request) {
                    case "Zipcode":
                        item.value = addr.postalCode;
                        item.label = addr.inputOutput;
                        item.locality = addr.locality;
                        item.localityId = addr.localityId;
                        item.localitySynonym = addr.localitySynonym;
                        break;
                    case "City":
                        item.value = addr.locality;
                        item.label = addr.locality;
                        item.postalCode = addr.postalCode;
                        item.localityId = addr.localityId;
                        item.localitySynonym = addr.localitySynonym;
                        break;
                    case "Street":
                        item.value = addr.streetName;
                        item.label = addr.streetName;
                        item.streetName = addr.streetName;
                        item.streetId = addr.streetId;
                        item.streetNumber = addr.streetNumber;
                        item.streetNumberId = addr.streetNumberId;
                        item.postalCode = addr.postalCode;
                        item.localityId = addr.localityId;
                        break;
                    case "StreetNumber":
                        var strCompl = addr.streetNumber;
                        if (addr.streetNumberExt != null && addr.streetNumberExt.length > 0)
                            strCompl += " " + addr.streetNumberExt;
                        item.value = strCompl;
                        item.label = strCompl;
                        item.streetNumber = addr.streetNumber;
                        item.streetNumberId = addr.streetNumberId;
                        item.postalCode = addr.postalCode;
                        item.localityId = addr.localityId;
                        item.streetNumberExt = addr.streetNumberExt;
                        break;
                    case "Building":
                        item.value = addr.buildingName;
                        item.label = addr.buildingName;
                        item.buildingName = addr.buildingName;
                        item.buildingId = addr.buildingId;
                        break;
                }
                arAddress.push(item);
            }
        });
    }

    return arAddress;
}

function RequestCAPAddress(searchValue, otherParam, step, cb)
{
    var requestURL = wsURL + '?request=SearchAddress&inputOutput=' + searchValue + '&step=' + step + '&countryCode=FRA&languageCode=fr';
    if (otherParam != null)
        requestURL = requestURL + otherParam;

    //console.log(requestURL);

    $.getJSON(requestURL)
    .done(function (data) {
        //console.log(data);
        cb(data);
    });
}

function ShowCAPDialog() {
    InitCAPAddress();
    $('#dialog-cap').dialog({
        title:'Modifier l\'adresse',
        autoOpen: true,
        modal: true,
        draggable: false,
        resizable:false
    });
    $("#dialog-cap").parent().appendTo(jQuery("form:first"));
}

function ValidateAddress() {
    //$('#dialog-cap').dialog('destroy');

    var requestURL = wsURL + '?request=VerifyAddress&step=&countryCode=FRA&languageCode=fr'
        + '&postalCode=' + $('.txtZipcode').val()
        + '&locality=' + $('.txtCity').val()
        + '&streetName=' + $('.txtStreet').val()
        + '&streetNumber=' + $('.txtStreetNumber').val();
    $.getJSON(requestURL)
    .done(function (data) {
        //console.log(data);
        if(data.returnValue == 0)
        {
            if(data.addresses != null && data.addresses.address.length > 0)
            {
                if (data.addresses.address.length == 1)
                {
                    $('.txtAddressQualityCode').prop('value', data.addresses.address[0].qualityCode);
                    if (data.addresses.address[0].qualityCode == '00') {
                        $('.txtZipcode').val(data.addresses.address[0].postalCode);
                        $('.txtCity').val(data.addresses.address[0].locality);
                        if (data.addresses.address[0].localitySynonym != null && data.addresses.address[0].localitySynonym.length > 0)
                            $('.txtComplCity').val(data.addresses.address[0].localitySynonym);
                        $('.txtStreet').val(data.addresses.address[0].streetName);
                        var strCompl = data.addresses.address[0].streetNumber;
                        if (data.addresses.address[0].streetNumberExt != null && data.addresses.address[0].streetNumberExt.length > 0)
                            strCompl += " " + data.addresses.address[0].streetNumberExt;
                        $('.txtStreetNumber').val(strCompl);
                        if (data.addresses.address[0].buildingName != null && data.addresses.address[0].buildingName.length > 0)
                            $('.txtCompl1').val(data.addresses.address[0].buildingName);
                        ForceValidateAddress();
                    }
                    else ShowForceSaveDialog(data.addresses.address[0].qualityCode);
                }
                else {
                    $('.txtAddressQualityCode').prop('value', '10'); // Adresse litigieuse si aucune sélection
                    ShowAddressMultiSelect(data.addresses);
                }
            }
            else {

            }
        }
        else {
            ShowForceSaveDialog();
        }
    });
}

function ShowAddressMultiSelect(addresses) {
    $('#dialog-cap-multi-address #select-list').empty();
    $.each(addresses.address, function (i, addr) {
        $('#dialog-cap-multi-address #select-list').append(CreateAddressSelectItem(addr));
    });

    $('#dialog-cap-multi-address').dialog({
        title: 'Votre saisie retourne plusieurs résultats',
        autoOpen: true,
        modal: true,
        draggable: false,
        resizable: false,
        minWidth: 400
    });
    $("#dialog-cap-multi-address").parent().appendTo(jQuery("form:first"));
}
function CreateAddressSelectItem(address)
{
    var item = $('<div/>').click({ param: address }, UseSelectedAddress);
    item.append($('<pre/>').html(address.formattedAddress.trim()));
    return item;
}
function UseSelectedAddress(event) {
    var address = event.data.param;
    //console.log(address);
    $('.txtZipcode').val(address.postalCode);
    $('.txtCity').val(address.locality);
    $('.txtComplCity').val(address.localitySynonym);
    $('.txtStreet').val(address.streetName);
    var strCompl = address.streetNumber;
    if (address.streetNumberExt != null && address.streetNumberExt.length > 0)
        strCompl += " " + address.streetNumberExt;
    $('.txtStreetNumber').val(strCompl);
    if (address.buildingName != null && address.buildingName.length > 0)
        $('.txtCompl1').val(address.buildingName);
    $('.txtAddressQualityCode').prop('value', '00');
    ForceValidateAddress();
}

function ShowForceSaveDialog(qualityCode) {
    if (qualityCode != null) {
        var message = '';
        switch(qualityCode){
            case '04':
                message = 'Entreprise forcée possédant un cedex.';
                break;
            case '10':
                message = 'Adresse litigieuse.';
                break;
            case '20':
                message = 'Adresse rejetée.';
                break;
            case '41':
                message = 'CP/LOC forcé.';
                break;
            case '42':
                message = 'Voie forcée.';
                break;
            case '53':
                message = 'Numéro de voie forcé et adresse valide.';
                break;
            case '63':
                message = 'Numéro de voie forcé dans une voie bornée.';
                break;
            case '70':
                message = 'Adresse étrangère non traitée.';
                break;
            case '80':
                message = 'Adresse tronquée par rapport à la norme du pays en cours.';
                break;
        }
        $('#dialog-cap-force-address .message').empty().append(message);
    }

    $('#dialog-cap-force-address').dialog({
        title: 'ATTENTION',
        autoOpen: true,
        modal: true,
        draggable: false,
        resizable: false
    });
    $("#dialog-cap-force-address").parent().appendTo(jQuery("form:first"));
}
function ForceValidateAddress() {
    if ($('#dialog-cap-multi-address').is(':visible'))
        $('#dialog-cap-multi-address').dialog('destroy');
    if ($('#dialog-cap-force-address').is(':visible'))
        $('#dialog-cap-force-address').dialog('destroy');
    if ($('#dialog-cap').is(':visible'))
        $('#dialog-cap').dialog('destroy');
    $('.btnValidateAddress').click();
}