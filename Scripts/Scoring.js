﻿function ShowLoadingScore() {
    var panel = $('.scoring-details-inner');
    $('.scoring-details-loading').show();
    $('.scoring-details-loading').width(panel.outerWidth());
    $('.scoring-details-loading').height(panel.outerHeight());

    $('.scoring-details-loading').position({
        my: 'center',
        at: 'center',
        of: '.scoring-details-inner'
    });
}

function ShowCategoryDetails(index)
{
    $('.scoring-details-category:visible>div').removeClass('active');
    $('.scoring-details-category:visible>div:eq(' + index + ')').addClass('active');

    $('.scoring-details-inner:visible>div').removeClass('active');
    $('.scoring-details-inner:visible>div:eq(' + index + ')').addClass('active');
}

function ToggleScoringDetails(score)
{
    //console.log($(score).parent().find('.scoring-details')[0]);
    $(score).parent().find('.scoring-details').eq(0).toggleClass('active');
}