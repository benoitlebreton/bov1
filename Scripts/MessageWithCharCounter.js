﻿function BindMessageLengthCheck(inputID, labelID) {
    $('#' + labelID).html($('#' + inputID).val().length);
    $('#' + inputID).unbind('keyup propertychange paste').bind('keyup propertychange paste', function () {
        $('#' + labelID).html($('#' + inputID).val().length);
    });
}

function BindSplittedMessageLengthCheck(inputID, nbCharCounterID, splittedCounterID, splittedLength, splittedKindMessageID, plural, splittedKindMessagePluralID) {
    SplitCounterManage(inputID, nbCharCounterID, splittedCounterID, splittedLength, splittedKindMessageID, plural, splittedKindMessagePluralID);

    $('#' + inputID).unbind('keyup propertychange paste').bind('keyup propertychange paste', function () {
        SplitCounterManage(inputID, nbCharCounterID, splittedCounterID, splittedLength, splittedKindMessageID, plural, splittedKindMessagePluralID);
    });
}

function SplitCounterManage(inputID, nbCharCounterID, splittedCounterID, splittedLength, splittedKindMessageID, plural, splittedKindMessagePluralID) {
    var NbChar = $('#' + inputID).val().length;

    if (NbChar > 1) { $('#' + nbCharCounterID).html(NbChar+ " caractères"); }
    else { $('#' + nbCharCounterID).html(NbChar + " caractère"); }

    var NbSplitted = 1;
    if (splittedLength != null && splittedLength > 0) {
        NbSplitted = Math.ceil(NbChar / splittedLength);

        $('#' + splittedKindMessagePluralID).html('');
        if (plural != null && plural && NbSplitted > 1) { $('#' + splittedKindMessagePluralID).html('s'); }
    }
    
    $('#' + splittedCounterID).html(NbSplitted);
}


function InitMultiMessage() {
    $('#show-previous-messageWithCharCounter').unbind('click').bind('click', function () {
        if ($('#previous-messageWithCharCounter').is(':visible')) {
            $('#previous-messageWithCharCounter').slideUp();
            $('#show-previous-messageWithCharCounter').html('Afficher les messages précédents');
        }
        else {
            $('#previous-messageWithCharCounter').slideDown(function () {
                var scrollHeight = document.getElementById('previous-messageWithCharCounter').scrollHeight;
                $('#previous-messageWithCharCounter').animate({ scrollTop: scrollHeight }, 'normal');
            });
            $('#show-previous-messageWithCharCounter').html('Masquer les messages précédents');
        }
    });
}