﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="GarnishmentDetails.aspx.cs" Inherits="GarnishmentDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">

    <link rel="Stylesheet" href="Styles/pdf-editor.css" />
    <style type="text/css">
        .divZoom {
            cursor: move;
            margin-bottom: 0;
            overflow: hidden; /*keep map contents from spilling over if JS is disabled*/
        }

        .divZoom, .divFrameSmall {
            border: 2px solid #f57527;
        }

            .divZoom, .divFrameSmall, .divFrameSmall iframe {
                width: 450px;
                height: 295px;
            }

        .blur {
            -webkit-filter: blur(2px);
            filter: blur(2px);
        }

            .blur input[type=button]:hover, .blur img:hover {
                cursor: default;
            }

        .form-button {
            text-align: center;
            margin-top: 20px;
            position: absolute;
            width: 100%;
            z-index: 100;
        }

        #garn-form .row {
            display: block;
            margin-bottom: 10px;
            position: relative;
            z-index: 90;
            background-color: #fff;
        }

            #garn-form .row.blur {
                z-index: 0;
            }

        #garn-form .label {
            position: relative;
        }

        .garn-form-bg {
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            background-color: rgba(255,255,255,0.4);
            z-index: 50;
        }

        .zoom-icon {
            position: absolute;
            right: 18px;
            top: -6px;
            height: 25px;
            cursor: pointer;
        }

        #garn-form select, #garn-form input[type=text] {
            border: 1px solid #808080;
            height: 20px;
            width: 96%;
            box-sizing: border-box;
        }

        #garn-form input[type=text] {
            padding: 0 5px;
        }

        .AccordionTitle {
            margin-top: 20px;
        }

            .AccordionTitle:hover {
                cursor: default;
                background-color: #f57527;
            }

        .response-method {
            display: inline-block;
            text-align: center;
            margin: 10px 40px;
        }

            .response-method img {
                display: block;
                margin: 0 auto 10px auto;
            }

        iframe {
            border: 1px solid #ccc;
        }
        /*.ui-draggable-dragging {
            display:none;
        }*/

        input[type=text][disabled], select[disabled] {
            background-color: #fff;
            color: #000;
        }

        .search-item {
            cursor: pointer;
            padding: 0 5px;
        }

            .search-item:hover {
                background-color: #ff9966;
                color: #fff;
            }
    </style>

    <%--<script type="text/javascript" src="Scripts/jquery-ui-ATD.min.js"></script>--%>
    <script type="text/javascript" src="Scripts/pdf-editor-fix.min.js"></script>
    <script type="text/javascript" src="Scripts/jquery.ui.rotatable.min.js"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            initSendMethod();
        });

        function InitDocReader(infos) {
            //console.log(infos);
            var mapid = 'GarnImg1';
            mapObject(mapid, infos.template);
            //$('#garn-form .active .zoom-icon').each(function (index) {
            //    if (infos.zooms.length >= index + 1)
            //        $(this).click(function () {
            //            //console.log(infos.zooms[index]);
            //            $('#' + mapid).mapbox("zoomTo", infos.zooms[index].level);
            //            $('#' + mapid).mapbox("center", { x: infos.zooms[index].x, y: infos.zooms[index].y });
            //        });
            //});
            //console.log(infos.zooms);
            $(infos.zooms).each(function (index) {
                index = index + 1;
                indexTmp = index;
                $('#zoom-icon' + index).click(function (i) {
                    return function () {
                        //console.log(i);
                        //console.log(infos.zooms[i - 1]);
                        $('#' + mapid).mapbox("zoomTo", infos.zooms[i - 1].level);
                        $('#' + mapid).mapbox("center", { x: infos.zooms[i - 1].x, y: infos.zooms[i - 1].y });
                    }
                }(index));
            });
            //console.log(infos.zoomIndex);
            setTimeout(function () {
                //$('#MainContent_panelForm' + (infos.zoomIndex + 1) + ' .zoom-icon').click();
                $('#zoom-icon' + infos.zoomIndex).click();
            }, 100);

            //$('.divZoom').css('height', $('#<%=panelForm.ClientID%>').outerHeight());
        }
        function InitBlur() {
            if ($('#<%=panelFormButton.ClientID%>') != null && $('#<%=hdnStatus.ClientID%>').val() != 'treated') {
                // SET BLUR
                $('.row.active:last').nextAll().addClass('blur');
                $('.blur select, .blur input[type=text], .blur input[type=button]').attr('disabled', true);
                //console.log($('.row.active:last').position().top + " + " + $('.row.active:last').outerHeight() + " = " + ($('.row.active:last').position().top + $('.row.active:last').outerHeight()));
                try {
                    $('#<%=panelFormButton.ClientID%>').css('top', ($('.row.active:last').position().top + $('.row.active:last').outerHeight()));
                }
                catch (ex) { }
            }
        }

        function mapObject(id, path) {
            if (path != null && path.trim().length == 0) {
                $('#div' + id).hide();
            }
            else {
                $("#img" + id).attr("src", '.' + path);

                $("#img" + id + "-min").one("load", function () {
                    HideImgLoading("#img" + id + "-min");
                }).attr("src", '.' + path);

                var map = $("#" + id).mapbox({
                    mousewheel: false,
                    doubleClickZoom: false,
                    defaultLayer: 0,
                    layerSplit: 5,
                    mapContent: "#Zoom" + id,
                    pan: true,
                    zoom: true
                });

                $('#up' + id).click(function () { map.mapbox("up", 80); return false; });
                $('#left' + id).click(function () { map.mapbox("left", 80); return false; });
                $('#right' + id).click(function () { map.mapbox("right", 80); return false; });
                $('#down' + id).click(function () { map.mapbox("down", 80); return false; });
                $('#zoom' + id).click(function () { map.mapbox("zoom"); return false; });
                $('#back' + id).click(function () { map.mapbox("back"); return false; });

                $('#div' + id).mouseover(function () {
                    $('#' + id + 'ImgPanel').show();
                });
                $('#div' + id).mouseout(function () {
                    $('#' + id + 'ImgPanel').hide();
                });
            }
        }
        function HideImgLoading(id) {
            $(id + '-loading').hide();
            $(id).show();
        }

        // PDF EDITOR
        function InitAssets(infos) {
            $('.template').attr('src', infos.template)
            $(infos.assets).each(function () {
                var asset = $(this)[0];

                var color = asset.color;
                var assetDrag = $('<div/>');
                assetDrag.addClass('asset draggable');
                assetDrag.css('top', asset.top + '%');
                assetDrag.css('left', asset.left + '%');
                var assetRotate = $('<div/>');
                assetRotate.addClass('rotatable');

                assetRotate.css('transform', 'rotate(' + asset.deg + 'deg)');
                if (asset.color.trim().length > 0) {
                    assetRotate.css('color', asset.color);
                }
                if (asset.type == 'text')
                    assetRotate.append(asset.content);
                else if (asset.type == 'img') {
                    var img = $('<img/>');
                    img.attr('src', asset.content);
                    assetRotate.append(img);
                }

                assetDrag.append(assetRotate);
                $('.doc-container').append(assetDrag);
            });

            $('.doc-container .draggable').draggable({
                containment: 'parent'
            });
            $('.doc-container .rotatable').rotatable({
                wheelRotate: false,
                snap: true,
                step: 2
            });
        }

        function SaveAssets() {
            var assets = new Array();
            $('.asset').each(function () {
                var asset = new Object();
                asset.top = Math.round(($(this).position().top * 100 / $('.doc-container').innerHeight()) * 100) / 100;
                asset.left = Math.round(($(this).position().left * 100 / $('.doc-container').innerWidth()) * 100) / 100;
                asset.deg = GetRotationDegrees($(this).find('.rotatable'));
                if ($(this).find('img').length > 0) {
                    asset.content = $(this).find('img').attr('src');
                    asset.type = 'img';
                }
                else {
                    asset.content = $(this).text();
                    asset.type = 'text';
                }
                assets.push(asset);
            });

            //var infos = new Object();
            //infos.template = $('.template').attr('src');
            //infos.assets = assets;
            //console.log(infos);

            //console.log(JSON.stringify(assets));

            var JSONdata = new Object();
            JSONdata.assets = assets;

            $('#<%=hfAssets.ClientID%>').val(JSON.stringify(JSONdata));
            $('#<%=btnGenerateResponse.ClientID%>').click();
        }

        function GetRotationDegrees(obj) {
            var matrix = obj.css("-webkit-transform") ||
            obj.css("-moz-transform") ||
            obj.css("-ms-transform") ||
            obj.css("-o-transform") ||
            obj.css("transform");
            if (matrix !== 'none') {
                var values = matrix.split('(')[1].split(')')[0].split(',');
                var a = values[0];
                var b = values[1];
                var angle = Math.round(Math.atan2(b, a) * (180 / Math.PI));
            } else { var angle = 0; }
            return (angle < 0) ? angle + 360 : angle;
        }

        function initSendMethod() {
            $('.RadioButtonList').buttonset();
            $('.RadioButtonList input[type=radio]').bind('change', function () {
                SetSendMethod();
            });

            SetSendMethod();
        }

        function SetSendMethod(){
            $('#<%=panelEmail.ClientID%>').hide();
            $('#<%=panelPostalMail.ClientID%>').hide();
            if($('#<%=rblResponseMethod.ClientID%> input[type=radio]:checked').val() == 'email')
                $('#<%=panelEmail.ClientID%>').show();
            else if($('#<%=rblResponseMethod.ClientID%> input[type=radio]:checked').val() == 'mail')
                $('#<%=panelPostalMail.ClientID%>').show();

            $('#<%=panelForm7.ClientID%> .zoom-icon:visible').click();

            InitBlur();
        }

        function ShowDialogCheckResponse(file) {
            $('#frame-response').attr('src', file + '#zoom=150');
            $('#dialog-check-response').dialog({
                draggable: false,
                resizable: false,
                width: 940,
                //dialogClass: "no-close",
                modal: true,
                title: 'Vérification avant envoi de la réponse'
            });
            $('#dialog-check-response').parent().appendTo(jQuery("form:first"));
            initSendMethod();
            
        }
        function HideDialogCheckResponse() {
            $('#dialog-check-response').dialog('close');
        }

        function ShowDialogResponseMethod() {
            $('#ResponsePDFPanel').slideUp(function () {
                RefreshArrow('ResponsePDF');
                $('#ResponseMethodPanel').slideDown(function () {
                    RefreshArrow('ResponseMethod');
                });
            });
        }

        function RefreshArrow(id) {
            if ($('#' + id + 'Panel').is(':visible'))
                $('#' + id + 'Arrow').removeAttr("src").attr('src', './Styles/Img/downArrow.png');
            else
                $('#' + id + 'Arrow').removeAttr("src").attr('src', './Styles/Img/rightArrow.png');
        }

        function ShowRejectDialog() {
            $('#dialog-reject select').bind('change', function () { if ($(this).val().length == 0) $('#div-reason').show(); else $('#div-reason').hide(); });
            $('#dialog-reject').dialog({
                draggable: false,
                resizable: false,
                width: 500,
                //dialogClass: "no-close",
                modal: true,
                title: 'Sélectionnez la raison du rejet',
                buttons: {
                    Enregistrer: function () { $(this).dialog('close'); $('#<%=btnReject.ClientID%>').click(); }
                }
            });
            $('#dialog-reject').parent().appendTo(jQuery("form:first"));
        }

        function ShowRetreatDialog() {
            $('#dialog-retreat').dialog({
                draggable: false,
                resizable: false,
                width: 500,
                //dialogClass: "no-close",
                modal: true,
                title: 'Retraiter l&apos;avis',
                buttons: {
                    Annuler: function () { $(this).dialog('close'); $(this).dialog('destroy'); },
                    Confirmer: function () { $(this).dialog('close'); $(this).dialog('destroy'); $('#<%=btnRetreat.ClientID%>').click(); }
                }
            });
            $('#dialog-retreat').parent().appendTo(jQuery("form:first"));
        }

        function ShowSearchResult() {
            $('#dialog-search-result').dialog({
                draggable: true,
                resizable: false,
                width: 400,
                dialogClass: "no-close",
                modal: false,
                title: 'Votre recherche retourne plusieurs résultats',
                buttons: {
                    Annuler: function () { $(this).dialog('close'); }
                }
            });
            $('#dialog-search-result').parent().appendTo(jQuery("form:first"));
        }
        function SelectSearchResult(name, account) {
            //console.log(name + ' ' + account);
            $('#<%=txtAccountNumber.ClientID%>').val(account);
            $('#<%=lblName.ClientID%>').empty().append(name);
            $('#dialog-search-result').dialog('destroy');
        }

        function confirmCancelRejection() {
            $('#dialog-confirm-cancel-rejection').dialog({
                draggable: true,
                resizable: false,
                width: 400,
                dialogClass: "no-close",
                modal: false,
                title: 'Annulation rejet',
                buttons: {
                    Valider: function () { $('#<%=btnCancelRejectionConfirmation.ClientID%>').click(); showLoading();$(this).dialog('close'); $(this).dialog('destroy'); },
                    Annuler: function () { $(this).dialog('close'); $(this).dialog('destroy'); }
                }
            });
            $('#dialog-confirm-cancel-rejection').parent().appendTo(jQuery("form:first"));

            return false;
        }

        function confirmRejectFinal() {
            $('#dialog-confirm-reject-final').dialog({
                draggable: true,
                resizable: false,
                width: 400,
                dialogClass: "no-close",
                modal: false,
                title: 'Rejeter définitivement',
                buttons: {
                    Valider: function () { $('#<%=btnRejectFinalConfirmation.ClientID%>').click(); showLoading();$(this).dialog('close'); $(this).dialog('destroy'); },
                    Annuler: function () { $(this).dialog('close'); $(this).dialog('destroy'); }
                }
            });
            $('#dialog-confirm-reject-final').parent().appendTo(jQuery("form:first"));

            return false;
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">

    <div id="garnishment-page" style="padding-bottom: 55px;">

        <h2 style="color: #344b56; margin-bottom: 20px; text-transform: uppercase; position: relative; text-align: center">Détail avis
            <asp:Button ID="btnPrev" runat="server" Text="Recherche Avis" PostBackUrl="~/GarnishmentSearch.aspx" CssClass="button" Style="position: absolute; left: 0;" />
            <asp:Label ID="lblDate" runat="server" Style="position: absolute; right: 6px; top: 5px; font-size: 16px; font-weight: normal;"></asp:Label>
        </h2>

        <asp:Panel ID="panelForm" runat="server" DefaultButton="btnSaveForm">

            <div id="garn-form" style="float: left; width: 48%; position: relative;">
                <asp:Panel ID="panelStatus" runat="server" Visible="false" Style="text-align: center">
                    <asp:Label ID="lblStatus" runat="server" Font-Bold="true" Style="font-size: 1.2em"></asp:Label>
                </asp:Panel>
                <asp:UpdatePanel ID="upForm" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="panelFormBG" runat="server" class="garn-form-bg"></asp:Panel>
                        <asp:Panel ID="panelFormButton" runat="server" CssClass="form-button">
                            <asp:Button ID="btnSaveForm" runat="server" Text="Valider" CssClass="button" OnClick="btnSaveForm_Click" CommandArgument="1" />
                        </asp:Panel>
                        <asp:Panel ID="panelAllForm" runat="server">
                            <asp:Panel ID="panelForm1" runat="server" CssClass="row active">
                                <div class="label">
                                    Type
                                    <img id="zoom-icon1" class="zoom-icon" src="Styles/Img/shop_search_min.png" />
                                </div>
                                <div>
                                    <asp:DropDownList ID="ddlGarnishmentType" runat="server">
                                        <asp:ListItem Value="" Text="Indéterminé"></asp:ListItem>
                                        <asp:ListItem Value="ATD" Text="Avis à tiers détenteur"></asp:ListItem>
                                        <asp:ListItem Value="OA1" Text="Opposition administrative - format 1 (titre en haut gauche)"></asp:ListItem>
                                        <asp:ListItem Value="OA2" Text="Opposition administrative - format 2 (titre en haut centré)"></asp:ListItem>
                                        <asp:ListItem Value="AO" Text="Avis d'opposition à tiers"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="panelForm2" runat="server" CssClass="row">
                                <div class="label">
                                    Numéro compte
                                    <img id="zoom-icon2" class="zoom-icon" src="Styles/Img/shop_search_min.png" />
                                </div>
                                <div>
                                    <asp:TextBox ID="txtAccountNumber" runat="server"></asp:TextBox>
                                </div>
                                <asp:Panel ID="panelClientDetails" runat="server" DefaultButton="btnSearchClient" Style="background-color: #ECECEC; padding: 10px; width: 96%; box-sizing: border-box;">
                                    <asp:Panel ID="panelClientInfos" runat="server" Visible="false" Style="margin-bottom: 10px">
                                        <asp:Label ID="lblName" runat="server" Style="position: relative; top: 2px;"></asp:Label>
                                        <asp:Label ID="lblBirthDate" runat="server" style="position:relative; top:2px;"></asp:Label>
                                    </asp:Panel>
                                    <asp:Panel ID="panelClientSearch" runat="server">
                                        <asp:TextBox ID="txtName" runat="server" placeholder="nom, prénom" Style="width: 255px; border: 1px solid silver; position: relative; top: -1px;"></asp:TextBox>
                                        <%--<input id="btnShowClient" runat="server" type="button" value="accéder à la fiche du client" class="MiniButton" style="text-align:center; width:200px;float:right" />--%>
                                        <asp:Button ID="btnSearchClient" runat="server" Text="rechercher" OnClick="btnSearchClient_Click" CssClass="MiniButton" Style="text-align: center; width: 140px; float: right" />
                                        <div style="clear: both"></div>
                                    </asp:Panel>
                                </asp:Panel>
                            </asp:Panel>
                            <asp:Panel ID="panelForm3" runat="server" CssClass="row">
                                <div class="label">
                                    Référence externe
                                    <img id="zoom-icon3" class="zoom-icon" src="Styles/Img/shop_search_min.png" />
                                </div>
                                <div>
                                    <asp:TextBox ID="txtExternalRef" runat="server"></asp:TextBox>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="panelForm4" runat="server" CssClass="row">
                                <div class="label">
                                    Montant
                                    <img id="zoom-icon4" class="zoom-icon" src="Styles/Img/shop_search_min.png" />
                                </div>
                                <div>
                                    <asp:TextBox ID="txtAmount" runat="server"></asp:TextBox>
                                </div>
                                <asp:Panel ID="panelSBIStatus" runat="server" Visible="false" style="margin-top: 10px;background-color: #ECECEC;padding: 10px;text-align: center;width: 96%;box-sizing: border-box;">
                                    <asp:Label ID="lblSBIstatus" runat="server" Style="font-weight: bold; font-size: 1.2em;"></asp:Label>
                                </asp:Panel>
                            </asp:Panel>
                            <asp:Panel ID="panelForm5" runat="server" CssClass="row" Visible="false">
                                <div class="label">
                                    IBAN trésorerie
                                    <img id="zoom-icon5" class="zoom-icon" src="Styles/Img/shop_search_min.png" />
                                </div>
                                <div>
                                    <asp:TextBox ID="txtIBAN" runat="server"></asp:TextBox>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="panelForm6" runat="server" CssClass="row" Visible="false">
                                <div class="row">
                                    <div class="label">
                                        Montant à débiter
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtTransferAmount" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="panelForm7" runat="server" CssClass="row" ><%--style="min-height:190px;"--%>
                                <div>
                                    <div class="label">
                                        Méthode d'envoi
                                    </div>
                                    <div>
                                        <asp:RadioButtonList ID="rblResponseMethod" runat="server" CssClass="RadioButtonList" RepeatDirection="Horizontal">
                                            <asp:ListItem Value="email" Text="Email" />
                                            <asp:ListItem Value="mail" Text="Courrier postal" />
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                                <asp:Panel ID="panelEmail" runat="server" Style="display:none; width:100%">
                                    <div class="label">
                                        Courriel
                                        <img id="zoom-icon6" class="zoom-icon" src="Styles/Img/shop_search_min.png" />
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                                    </div>
                                </asp:Panel>
                                <asp:Panel ID="panelPostalMail" runat="server" Style="display:none; width: 100%">
                                    <div class="label">
                                        Centre des impôts
                                        <img id="zoom-icon7" class="zoom-icon" src="Styles/Img/shop_search_min.png" />
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtAddress_Recipient" runat="server"></asp:TextBox>
                                    </div>
                                    <div class="label">
                                        Adresse
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtAddress_Street" runat="server"></asp:TextBox>
                                    </div>
                                    <div class="table">
                                        <div class="row">
                                            <div class="cell">
                                                <div class="label">
                                                    Code postal
                                                </div>
                                                <div>
                                                    <asp:TextBox ID="txtAddress_Zipcode" runat="server"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="cell">
                                                <div class="label">
                                                    Ville
                                                </div>
                                                <div>
                                                    <asp:TextBox ID="txtAddress_City" runat="server"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </asp:Panel>
                        </asp:Panel>

                        <div id="dialog-search-result" style="display: none">
                            <asp:Repeater ID="rptSearchResult" runat="server">
                                <ItemTemplate>
                                    <div class="search-item" onclick='<%#String.Format("SelectSearchResult(\"{0}\",\"{1}\");", Eval("LastName") + " " + Eval("FirstName"), Eval("AccountNumber")) %>'>
                                        <asp:Label ID="lblLastName" runat="server"><%# Eval("LastName") %></asp:Label>
                                        <asp:Label ID="lblFirstName" runat="server"><%# Eval("FirstName") %></asp:Label>
                                        <asp:Label ID="lblBirthDate" runat="server"><%# Eval("BirthDate") %></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div style="float: right; width: 50%">
                <asp:UpdatePanel ID="upFile1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Panel ID="panelCroppedImg" runat="server">
                            <div style="margin: 0; padding: 0; width: 100%" id="divGarnImg1">
                                <div style="position: relative;">
                                    <div id="GarnImg1" class="divZoom" style="margin-bottom: 10px">
                                        <div style="width: 100%;">
                                            <img id="imgGarnImg1-min-loading" src="Styles/Img/loading.gif" alt="loading..." style="position: absolute; top: 130px; left: 220px" />
                                            <img id="imgGarnImg1-min" alt="" style="width: 450px" />
                                        </div>
                                        <div style="width: 900px; height: 1272px">
                                            <img id="imgGarnImg1" src="" alt="" />
                                            <div id="ZoomGarnImg1">
                                            </div>
                                        </div>
                                    </div>
                                    <div style="position: absolute; top: -5px; right: -77px; line-height: 1.4em; padding: 5px; z-index: 99; width: 80px; height: 200px;">
                                        <div id="GarnImg1ImgPanel" style="display: none; background-color: white; border: 2px solid #e8571d">
                                            <div style="margin: auto; display: table;">
                                                <div style="display: table-row">
                                                    <div style="display: table-cell; padding: 5px">
                                                        <div id="zoomGarnImg1" class="ImageButton zoom" style="">
                                                        </div>
                                                    </div>
                                                    <div style="display: table-cell; padding: 5px">
                                                        <div id="backGarnImg1" class="ImageButton back">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div style="width: 100%; margin-top: 10px">
                                                <div id="upGarnImg1" class="ImageButton up">
                                                </div>
                                            </div>
                                            <div style="margin: auto; display: table;">
                                                <div style="display: table-row">
                                                    <div style="display: table-cell; padding-right: 20px;">
                                                        <div id="leftGarnImg1" class="ImageButton left-arrow">
                                                        </div>
                                                    </div>
                                                    <div style="display: table-cell">
                                                        <div id="rightGarnImg1" class="ImageButton right">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div style="width: 100%; margin-bottom: 10px">
                                                <div id="downGarnImg1" class="ImageButton down">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                        <asp:Panel ID="panelOriginalFile" runat="server" Visible="false">
                            <div class="divFrameSmall">
                                <iframe id="frameOriginalSmall" runat="server" />
                            </div>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div style="clear: both"></div>

            <asp:Panel ID="panelSaveModificationPostTreatment" runat="server" Visible="false" style="text-align:center;margin-top:20px;z-index:200;position:relative;">
                <asp:UpdatePanel ID="upSaveModificationPostTreatment" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="btnSaveModificationPostTreatment" runat="server" Text="Enregistrer les modifications" CssClass="button" OnClick="btnSaveModificationPostTreatment_Click" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </asp:Panel>

        </asp:Panel>

        <asp:UpdatePanel ID="upResponse" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="panelResponse" runat="server" Visible="false">
                    <h4 class="AccordionTitle">
                        <span>Réponse</span>
                    </h4>

                    <asp:Panel ID="panelPDFEditor" runat="server">
                        <div id="div-response">
                            <div class="doc-container">
                                <img class="template" src="" />
                            </div>

                            <asp:UpdatePanel ID="upSendResponse" runat="server">
                                <ContentTemplate>
                                    <div style="text-align: center; margin-top: 20px">
                                        <input type="button" value="Générer le document de réponse" class="button" onclick="SaveAssets();" />
                                        <asp:HiddenField ID="hfAssets" runat="server" />
                                        <asp:Button ID="btnGenerateResponse" runat="server" OnClick="btnGenerateResponse_Click" Style="display: none" />
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="panelPDFResponse" runat="server" Visible="false">
                        <div style="width: 900px; margin: auto">
                            <iframe id="frameResponse" runat="server" width="900" height="1272" />
                        </div>
                    </asp:Panel>
                </asp:Panel>

            </ContentTemplate>
        </asp:UpdatePanel>

        <asp:UpdatePanel ID="upRejectBtn" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="panelReject" runat="server" Visible="false" Style="text-align: right">
                    <input type="button" class="button" value="Rejeter l'avis" onclick="ShowRejectDialog();" />

                    <div id="dialog-reject" style="display: none">
                        <asp:DropDownList ID="ddlRejectReason" runat="server" Style="width: 100%">
                            <asp:ListItem Value="Document non correspondant" Text="Document non correspondant" Selected="True" />
                            <asp:ListItem Value="Document illisible" Text="Document illisible" />
                            <asp:ListItem Value="Pages inversées" Text="Pages inversées" />
                            <asp:ListItem Value="N° compte incomplet" Text="N° compte incomplet" />
                            <asp:ListItem Value="IBAN absent/ incomplet" Text="IBAN absent/ incomplet" />
                            <asp:ListItem Value="Date de naissance différente" Text="Date de naissance différente" />
                            <asp:ListItem Value="Nom ou prénom différent" Text="Nom ou prénom différent" />
                            <asp:ListItem Value="Autre document à la place de l’accusé" Text="Autre document à la place de l’accusé" />
                            <asp:ListItem Value="" Text="Autre" />
                        </asp:DropDownList>
                        <div id="div-reason" style="display: none">
                            <div class="label">
                                Raison
                            </div>
                            <div>
                                <asp:TextBox ID="txtReason" runat="server" Style="width: 100%"></asp:TextBox>
                            </div>
                        </div>
                        <asp:UpdatePanel ID="upReject" runat="server">
                            <ContentTemplate>
                                <asp:Button ID="btnReject" runat="server" Text="Enregistrer" OnClick="btnReject_Click" Style="display: none" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>

        <div id="dialog-check-response" style="display: none">
            <h4 id="ResponsePDFTitle" class="AccordionTitle" style="font-size: 0.95em">
                <img id="ResponsePDFArrow" alt="" src="Styles/Img/downArrow.png" />
                Document
            </h4>
            <div id="ResponsePDFPanel" style="margin-bottom: 10px">
                <asp:UpdatePanel ID="upResponseMethod" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div style="width: 900px; text-align: center; margin: auto">
                            <iframe id="frame-response" width="899" height="600"></iframe>
                        </div>
                        <div style="width: 55%; margin: 20px auto 0 auto">
                            <b style="position: relative; top: 5px">Le document de réponse est-il correct ?</b>
                            <div style="float: right">
                                <asp:Button ID="Button1" runat="server" Text="Oui" CssClass="button" Style="font-size: 14px;" OnClick="btnTerminate_Click" />
                                <%--<input type="button" class="button" value="Oui" onclick="ShowDialogResponseMethod();" style="font-size:14px;" />--%>
                                <input type="button" class="button" value="Non, modifier" onclick="HideDialogCheckResponse();" style="font-size: 14px;" />
                            </div>
                            <div style="clear: both"></div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>

            <%--<div id="ResponseMethodPanel" style="display:none;padding:10px 10px 0 10px;">
                <asp:UpdatePanel ID="upResponseMethod" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div style="width:30%;float:left;padding-top:7px">
                            <asp:RadioButtonList ID="rblResponseMethod" runat="server" CssClass="RadioButtonList" RepeatDirection="Horizontal">
                                <asp:ListItem Value="email" Text="Email" />
                                <asp:ListItem Value="mail" Text="Courrier postal" />
                            </asp:RadioButtonList>
                        </div>
                        <asp:Panel ID="panelEmail" runat="server" style="display:none;float:right;width:70%">
                            <div class="label">
                                Adresse
                            </div>
                            <div>
                                <asp:TextBox ID="txtEmail" runat="server" style="width:100%"></asp:TextBox>
                            </div>
                        </asp:Panel>
                        <div style="clear:both"></div>
                        <div style="text-align:center;margin-top:20px">
                            <asp:Button ID="btnTerminate" runat="server" Text="Enregistrer" CssClass="button" OnClick="btnTerminate_Click" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>--%>
        </div>

        <div id="dialog-confirm-cancel-rejection" style="display:none">
            Voulez-vous vraiment annuler le refus ?
        </div>
        <div id="dialog-confirm-reject-final" style="display:none">
            Voulez-vous vraiment rejeter définitivement l'avis ?
        </div>

        <asp:Panel ID="panelRejectDetails" runat="server" Visible="false">

            <div class="table" style="width:100%">
                <asp:Panel ID="panelRejectFinalDetails" runat="server" CssClass="row" Visible="false">
                    <div class="cell">
                        <div class="label">
                            Rejeté définitivement par
                        </div>
                        <div>
                            <asp:Label ID="lblRejectedFinalBy" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="cell"></div>
                    <div class="cell"></div>
                </asp:Panel>
                <div class="row">
                    <div class="cell" style="padding-right:30px;">
                        <div class="label">
                            Rejeté par
                        </div>
                        <div>
                            <asp:Label ID="lblRejectedBy" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="cell">
                        <div class="label">
                            Raison
                        </div>
                        <div>
                            <asp:Label ID="lblRejectedReason" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="cell" style="text-align:right;">
                        <asp:Panel ID="panelRejectFinal" runat="server" Visible="false" style="display:inline-block">
                            <asp:Button ID="btnRejectFinal" runat="server" CssClass="button" Text="Rejeter définitivement" OnClientClick="return confirmRejectFinal();" />
                            <asp:Button ID="btnRejectFinalConfirmation" runat="server" CssClass="button" Text="Rejeter définitivement" OnClick="btnRejectFinal_Click" style="display:none" />
                        </asp:Panel>
                        <asp:Panel ID="panelCancelRejection" runat="server" Visible="false" style="display:inline-block">
                            <asp:Button ID="btnCancelRejection" runat="server" CssClass="button" Text="Annuler rejet" OnClientClick="return confirmCancelRejection();" />
                            <asp:Button ID="btnCancelRejectionConfirmation" runat="server" CssClass="button" Text="annulation rejet" OnClick="btnCancelRejection_Click" style="display:none" />
                        </asp:Panel>
                    </div>
                </div>
            </div>
            <h4 class="AccordionTitle">
                <span>Document original</span>
            </h4>
            <div style="width: 900px; margin: auto">
                <iframe id="frameOriginalFile" runat="server" width="900" height="1272" />
            </div>

        </asp:Panel>

         <asp:UpdatePanel ID="upRetreatBtn" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="panelRetreat" runat="server" Visible="false" Style="text-align: right">
                    <input type="button" class="button" value="Retraiter l'avis" onclick="ShowRetreatDialog();" />

                    <div id="dialog-retreat" style="display: none">
                        Confirmez-vous vouloir traiter à nouveau cet avis ?
                        <asp:UpdatePanel ID="upRetreat" runat="server">
                            <ContentTemplate>
                                <asp:Button ID="btnRetreat" runat="server" Text="Enregistrer" OnClick="btnRetreat_Click" Style="display: none" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>



    </div>

    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            pbControl = args.get_postBackElement();

            var containerID = '';
            if (pbControl != null) {
                if (pbControl.id.indexOf('btnSaveForm') != -1 || pbControl.id.indexOf('btnSearchClient') != -1 || pbControl.id.indexOf('btnSaveModificationPostTreatment') != -1)
                    containerID = 'garn-form';
                else if (pbControl.id.indexOf('btnGenerateResponse') != -1)
                    containerID = 'div-response';
                else containerID = 'garnishment-page';
            }

            var width = $("#" + containerID).width();
            var height = $("#" + containerID).height() + 80;
            $('#ar-loading').css('width', width);
            $('#ar-loading').css('height', height);

            $('#ar-loading').show();

            $('#ar-loading').position({ my: "left top", at: "left top", of: "#" + containerID });
        }
        function EndRequestHandler(sender, args) {
            $('#ar-loading').hide();
            initSendMethod();
        }
    </script>

    <div id="ar-loading" style="display: none; position: absolute; background: url('Styles/img/loading.gif') no-repeat center; background-color: rgba(255, 255, 255, 0.5); z-index: 100"></div>

    <asp:HiddenField ID="hdnStatus" runat="server" />

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">
</asp:Content>

