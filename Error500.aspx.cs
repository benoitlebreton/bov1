﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class erreur500 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Exception ex = Server.GetLastError();

            if (ex != null)
            {
                if (ex.GetBaseException() != null)
                    ex = ex.GetBaseException();

                try
                {
                    HttpException httpEx = (HttpException)ex;
                    throw new Exception();
                }
                catch (Exception ex2)
                {
                    lblErrorTitle.Text = "500";
                    lblErrorMessage.Text = "Une erreur s'est produite.<br/><br/>Notre équipe technique a été informée du problème et se charge de le régler au plus vite.";
                }
            }
            else
                Response.Redirect("Default.aspx");
        }
    }
}