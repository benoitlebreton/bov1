﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="CreOrderNew.aspx.cs" Inherits="CreOrderNew" %>

<%@ Register Src="~/API/ClientFileUpload.ascx" TagName="ClientFileUpload" TagPrefix="asp" %>
<%@ Register Src="~/API/ClientFileManager.ascx" TagName="ClientFileManager" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/API/ClientSearch.ascx" TagName="ClientSearch" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">

    <link rel="Stylesheet" href="Styles/ClientFileUpload.css" />
    <link href="Styles/tipTip.css" rel="Stylesheet" type="text/css" />
    <link href="Styles/UI/jquery-ui-1.9.2.custom.css" rel="Stylesheet" type="text/css" />
    <link href="Styles/Site.css" rel="stylesheet" type="text/css" />
    <link href="Styles/Operations.css?v=20180314" type="text/css" rel="Stylesheet" />
    
    <style type="text/css">
        .zoom-icon {
            position: relative;
            height: 25px;
            cursor: pointer;
        }
    </style>

    <script type="text/javascript">
        $(document).ready(function () {
            

            $('#mpad-details-popup').dialog({
                autoOpen: false,
                resizable: false,
                draggable: false,
                modal: true,
                title: "Détails borne"
            });
            $('#mpad-details-popup').parent().appendTo(jQuery("form:first"));

            $('#dialog-search-customer').dialog({
                draggable: true,
                autoOpen: false,
                resizable: false,
                width: 400,
                modal: true,
                title: "Recherche client",
            });
            $('#dialog-search-customer').parent().appendTo(jQuery("form:first"));
        });

        function ShowNewFileDialog() {
            $('#dialog-add-file').dialog({
                draggable: false,
                resizable: false,
                width: 800,
                //dialogClass: "no-close",
                modal: true,
                title: 'Ajouter un nouveau fichier'
            });
            $('#dialog-add-file').parent().appendTo(jQuery("form:first"));

            InitInputFiles();
        }

        function HideNewFileDialog() {

        }

        function padRight(str, max) {
            str = str.toString();
            return str.length < max ? padRight(str + "0", max) : str;
        }

        function ValidateForm() {
            var isValid = true;
            var str = '';
            var reg = /^\d*([\,\.]\d{1,2}$)?$/g; // test un montant

            if ($('#<%=pMontant1.ClientID%>').is(':visible') == true) {
                $('#<%=tbMontant1Cts.ClientID%>').val(padRight($('#<%=tbMontant1Cts.ClientID%>').val().trim(), 2));
                if ($('#<%=tbMontant1.ClientID%>').val().length == 0)
                    $('#<%=tbMontant1.ClientID%>').val("0");
            }

            if ($('#<%=pMontant2.ClientID%>').is(':visible') == true) {
                $('#<%=tbMontant2Cts.ClientID%>').val(padRight($('#<%=tbMontant2Cts.ClientID%>').val().trim(), 2));
                if ($('#<%=tbMontant2.ClientID%>').val().length == 0)
                    $('#<%=tbMontant2.ClientID%>').val("0");
            }

            var value2Test = $('#<%=tbMontant1.ClientID%>').val() + "." + $('#<%=tbMontant1Cts.ClientID%>').val();
            
            // test des libellés
            reg = /^[0-9a-zA-Z /\-\?:()\.\,'\+]*$/g;
            var minLentgh = parseInt($('#<%=hdnLibelle1.ClientID%>').val())
            if ($('#<%=pLibelle1.ClientID%>').is(':visible') == true) {
                value2Test = $('#<%=tbLibelle1.ClientID%>').val();
                //console.log(value2Test + ":" + reg.test(value2Test) + ";" + value2Test.length + "; minLentgh " + minLentgh);
                if (reg.test(value2Test) == false || (minLentgh != 0 && value2Test.length < minLentgh)) {

                    isValid = false;
                    if (str.length > 0)
                        str += "<br/>";
                    str += 'Libellé 1 : seuls les caractères numériques, alphabétiques (sans accent) et / - ? : ( ) . , \' + sont autorisés.';
                }
            }

            reg = /^[0-9a-zA-Z /\-\?:()\.\,'\+]*$/g;
            minLentgh = parseInt($('#<%=hdnLibelle2.ClientID%>').val())
            if ($('#<%=pLibelle2.ClientID%>').is(':visible') == true) {
                value2Test = $('#<%=tbLibelle2.ClientID%>').val();
                //console.log(value2Test + ":" + reg.test(value2Test) + ";" + value2Test.length);
                if (reg.test(value2Test) == false || (minLentgh != 0 && value2Test.length < minLentgh)) {

                    isValid = false;
                    if (str.length > 0)
                        str += "<br/>";
                    str += 'Libellé 2 : seuls les caractères numériques, alphabétiques (sans accent) et / - ? : ( ) . , \' + sont autorisés.';
                }
            }

            if ($('#<%=pCompte1.ClientID%>').is(':visible') == true) {
                if ($('#<%=tbCompte1.ClientID%>').val().length != 11) {
                    isValid = false;
                    if (str.length > 0)
                        str += "<br/>";
                    str += "Numéro de compte invalide";
                }
            }

            if ($('#<%=pCompte2.ClientID%>').is(':visible') == true) {
                if ($('#<%=tbCompte2.ClientID%>').val().length != 11) {
                    isValid = false;
                    if (str.length > 0)
                        str += "<br/>";
                    str += "Numéro de compte invalide";
                }
            }

            reg = /^[0-9a-zA-Z /\-\?:()\.\,'\+]*$/g;
            if ($('#<%=pUserComments.ClientID%>').is(':visible') == true) {
                var pattern = /\n/g;
                value2Test = $('#<%=tbUserComments.ClientID%>').val().replace(pattern, "");
                if (value2Test.length > 0) {
                    if (reg.test(value2Test) == false || value2Test.length > 255) {

                        isValid = false;
                        if (str.length > 0)
                            str += "<br/>";
                        str += 'Commentaires : seuls les caractères numériques, alphabétiques (sans accent) et / - ? : ( ) . , \' + sont autorisés (255 car max).';
                    }
                    else {
                        $('#<%=tbUserComments.ClientID%>').val(value2Test);
                    }
                }
            }

            //test de la date
            reg = /^\s*(3[01]|[12][0-9]|0?[1-9])\/(1[012]|0?[1-9])\/((?:19|20)\d{2})\s*$/g;
            if ($('#<%=pDate1.ClientID%>').is(':visible') == true) {
                console.log($('#<%=txtDate1.ClientID%>').val());
                value2Test = $('#<%=txtDate1.ClientID%>').val();
                if (reg.test(value2Test) == false) {

                    isValid = false;
                    if (str.length > 0)
                        str += "<br/>";
                    str += 'Date invalide';
                }
                else {
                    var splitDate = value2Test.split('/');
                    if (splitDate[0].length == 1)
                        value2Test = "0" + splitDate[0];
                    else
                        value2Test = splitDate[0];
                    value2Test += "/";
                    if (splitDate[1].length == 1)
                        value2Test += "0" + splitDate[1];
                    else
                        value2Test += splitDate[1];
                    value2Test += "/" + splitDate[2];
                    $('#<%=txtDate1.ClientID%>').val(value2Test);
                }
                console.log($('#<%=txtDate1.ClientID%>').val());
            }
            reg = /^\s*(3[01]|[12][0-9]|0?[1-9])\/(1[012]|0?[1-9])\/((?:19|20)\d{2})\s*$/g;
            if ($('#<%=pDate2.ClientID%>').is(':visible') == true) {
                value2Test = $('#<%=txtDate2.ClientID%>').val();
                if (reg.test(value2Test) == false) {

                    isValid = false;
                    if (str.length > 0)
                        str += "<br/>";
                    str += 'Date invalide';
                }
                else {
                    var splitDate = value2Test.split('/');
                    if (splitDate[0].length == 1)
                        value2Test = "0" + splitDate[0];
                    else
                        value2Test = splitDate[0];
                    value2Test += "/";
                    if (splitDate[1].length == 1)
                        value2Test += "0" + splitDate[1];
                    else
                        value2Test += splitDate[1];
                    value2Test += "/" + splitDate[2];
                    $('#<%=txtDate2.ClientID%>').val(value2Test);
                }
            }

            if (!isValid) {
                AlertMessage("saisie incorrecte", str);
            }
            else {
                showLoading();
            }

            return isValid;
        }

        function AlertMessageOD(title, message, greenMessage) {
            $('#<%=lblMessage.ClientID%>').html(message);
            var lblMsg = document.getElementById('<%=lblMessage.ClientID%>');
            console.log(greenMessage);
            if (greenMessage == 'True')
                lblMsg.style.color = 'green';
            else
                lblMsg.style.color = 'red';

            var btnRedirection = document.getElementById('<%=btnRedirectPage.ClientID%>');
            $('#<%=hdnResultCre.ClientID%>').val(greenMessage);

            $("#dialog-alertMessage").dialog({
                resizable: false,
                width: 700,
                modal: true,
                title: title,
                buttons: {
                    "Fermer": function () {
                        $(this).dialog("close");
                        $(this).dialog("destroy");
                        /*if (redirect != null && redirect.trim().length > 0) {
                            document.location = redirect;
                        }*/

                        btnRedirection.click();
                    }
                }
            });
        }

        function SearchCustomer() {
            console.log("search à faire");
            $('#dialog-search-customer').dialog('open');            
        }

        function CloseSearchClient(NumCompte, CustName, CustBirth) {
            $('#<%=tbCompte1.ClientID%>').val(NumCompte);
            $('#<%=lblName.ClientID%>').text(CustName);
            $('#<%=lblBirthDate.ClientID%>').text(CustBirth);
            $('#dialog-search-customer').dialog('close');
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div style="position: relative;">
        <asp:Button ID="btnPrevious" runat="server" Text="Menu précédent" PostBackUrl="~/CreOrder.aspx" CssClass="button" />
    </div>
    <div style="position: relative; visibility: hidden">
        <asp:Button ID="btnRedirectPage" runat="server" Text="redirection après cre" OnCommand="btnRedirectPage_Click" CommandArgument="false" CssClass="button" />
        <asp:HiddenField ID="hdnResultCre" runat="server" />
    </div>
    <h2 style="color: #344b56; margin-bottom: 10px; text-transform: uppercase">Saisie d'une opération
    </h2>
    <div style="padding: 10px 0;">
        <div style="align-content: center">
            <div style="padding: 10px 0;">
                <asp:Label ID="lblProfil" runat="server" Width="200px"><span style="font-weight:bold">Profil : </span></asp:Label>
                <asp:DropDownList ID="ddlFeature" OnSelectedIndexChanged="ddlFeature_OnChange" runat="server" Enabled="true" AutoPostBack="true"></asp:DropDownList>
            </div>
            <asp:Panel ID="pOperationSaisie" runat="server">
                <asp:UpdatePanel ID="upOperationSaisie" runat="server" UpdateMode="Conditional">
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ddlFeature" EventName="SelectedIndexChanged" />
                    </Triggers>
                    <ContentTemplate>
                        <asp:Panel ID="pOperation" runat="server">
                            <asp:Panel ID="panelOperationField" runat="server" DefaultButton="btnSoummission">
                                <asp:UpdatePanel ID="upOperationField" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div style="padding: 10px 0;">
                                            <asp:Label ID="lblOperation" runat="server" Width="200px"><span style="font-weight:bold">Op&eacute;ration : </span></asp:Label>
                                            <asp:DropDownList ID="ddlTypeOperation" OnSelectedIndexChanged="ddlTypeOperation_OnChange" runat="server" Enabled="true" AutoPostBack="true"></asp:DropDownList>
                                        </div>

                                        <div style="padding: 10px 0px;">
                                            <asp:Literal ID="ltlValideur" runat="server"></asp:Literal>
                                        </div>
                                        <div style="padding: 20px 0px;">
                                            <asp:Label ID="lblParams" runat="server"><span style="font-weight:bold">Param&egrave;tres</span></asp:Label>
                                        </div>
                                        <asp:HiddenField ID="hdnMontant1OrMontant2" Value="false" runat="server" />

                                        <asp:Table ID="tOperation" runat="server" CellPadding="2" CellSpacing="1">
                                            <asp:TableHeaderRow ID="enteteTableParam" runat="server">
                                                <asp:TableHeaderCell ID="libelleRow" runat="server" Width="200px" />
                                                <asp:TableHeaderCell ID="saisieRow" runat="server" Width="250px" />
                                                <asp:TableHeaderCell ID="tollTipRow" runat="server" />
                                            </asp:TableHeaderRow>
                                            <asp:TableRow ID="pEve" runat="server">
                                                <asp:TableCell ID="cell1" runat="server">
                                                    <asp:Label ID="lblEve" runat="server">Type d'opération</asp:Label>
                                                </asp:TableCell>
                                                <asp:TableCell ID="cell2" runat="server">
                                                    <asp:DropDownList ID="ddlEve" runat="server" Width="100%"></asp:DropDownList>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow ID="pCompte1" runat="server">
                                                <asp:TableCell ID="TableCell1" runat="server" VerticalAlign="top">
                                                    <asp:Label ID="lblCompte1" runat="server">Compte client</asp:Label>
                                                </asp:TableCell>
                                                <asp:TableCell ID="TableCell2" runat="server">
                                                    <asp:TextBox ID="tbCompte1" runat="server" MaxLength="11" Width="98%"></asp:TextBox>
                                                    <ajaxToolkit:FilteredTextBoxExtender ID="ftbeCompte1" runat="server" TargetControlID="tbCompte1" FilterType="Numbers" />
                                                    <asp:panel ID="panelClientInfos" runat="server" Visible="true">
                                                        <asp:Label ID="lblName" runat="server" Style="position: relative; top: 2px;">1</asp:Label>
                                                        <asp:Label ID="lblBirthDate" runat="server" style="position:relative; top:2px;"></asp:Label>
                                                    </asp:panel>
                                                </asp:TableCell>
                                                <asp:TableCell ID="TableCell3" runat="server" VerticalAlign="top">
                                                    <img id="zoom-icon1" class="zoom-icon" src="Styles/Img/shop_search_min.png" alt="Search" onclick="SearchCustomer()" />
                                                    <span style="font-style: italic;font-weight: lighter;color:gray">11 chiffres</span>
                                                </asp:TableCell>
                                                
                                            </asp:TableRow>
                                            <asp:TableRow ID="pCompte2" runat="server">
                                                <asp:TableCell ID="TableCell4" runat="server">
                                                    <asp:Label ID="lblCompte2" runat="server">Compte commerçant</asp:Label>
                                                </asp:TableCell>
                                                <asp:TableCell ID="TableCell5" runat="server">
                                                    <asp:TextBox ID="tbCompte2" runat="server" MaxLength="11" Width="98%"></asp:TextBox>
                                                    <ajaxToolkit:FilteredTextBoxExtender ID="ftbeCompte2" runat="server" TargetControlID="tbCompte2" FilterType="Numbers" />
                                                </asp:TableCell>
                                                <asp:TableCell ID="TableCell6" runat="server">
                                                    <span style="font-style: italic;font-weight: lighter;color:gray">11 chiffres</span>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow ID="pDate1" runat="server">
                                                <asp:TableCell ID="TableCell7" runat="server">
                                                    <asp:Label ID="lblDate1" runat="server">Date souhaitée </asp:Label>
                                                </asp:TableCell>
                                                <asp:TableCell ID="TableCell8" runat="server">
                                                    <asp:TextBox ID="txtDate1" runat="server" Width="98%"></asp:TextBox>
                                                    <%--<asp:ImageButton ID="ImgBntCalc1" runat="server" ImageUrl="Styles/Img/calendrier.png" CausesValidation="False"
                                                        AlternateText="Cal" ToolTip="<%=Resources.res.OpenCalendar %>" Width="22px" Style="vertical-align: middle" />
                                                    <asp:RequiredFieldValidator runat="server" ID="reqDate1" ControlToValidate="txtDate1"
                                                        CssClass="reqStarStyle" ValidationGroup="MKE">*</asp:RequiredFieldValidator>
                                                    <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender1" runat="server" TargetControlID="txtDate1"
                                                        Mask="99/99/9999" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                                                        OnInvalidCssClass="MaskedEditError" MaskType="Date" DisplayMoney="Left" AcceptNegative="Left"
                                                        ErrorTooltipEnabled="True" />
                                                    <ajaxToolkit:MaskedEditValidator ID="MaskedEditValidator1" runat="server" ControlExtender="MaskedEditExtender1"
                                                        ControlToValidate="txtDate1" InvalidValueMessage="Date invalide" Display="Dynamic" ForeColor="Red"
                                                        EmptyValueBlurredText="*" InvalidValueBlurredMessage="Date invalide" ValidationGroup="MKE" />
                                                    <ajaxToolkit:FilteredTextBoxExtender ID="ftbeDate1" runat="server" TargetControlID="txtDate1" ValidChars="01234567890/" />--%>
                                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtDate1"
                                                        PopupButtonID="txtDate1" Format="dd/MM/yyyy" />
                                                </asp:TableCell>
                                                <asp:TableCell ID="TableCell9" runat="server">
                                                    <span style="font-style: italic;font-weight: lighter;color:gray">(jj/mm/aaaa)</span>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow ID="pDate2" runat="server">
                                                <asp:TableCell ID="TableCell10" runat="server">
                                                    <asp:Label ID="lblDate2" runat="server">Date d'opération d'origine</asp:Label>
                                                </asp:TableCell>
                                                <asp:TableCell ID="TableCell11" runat="server">
                                                    <asp:TextBox ID="txtDate2" runat="server" Width="98%"></asp:TextBox>
                                                    <%--<asp:ImageButton ID="ImgBntCalc2" runat="server" ImageUrl="~/Img/calendar.png" CausesValidation="False"
                                                        AlternateText="Cal" ToolTip="<%=Resources.res.OpenCalendar %>" Width="22px" Style="vertical-align: middle" />
                                                    <asp:RequiredFieldValidator runat="server" ID="reqDate2" ControlToValidate="txtDate2"
                                                        CssClass="reqStarStyle" ValidationGroup="MKE">*</asp:RequiredFieldValidator>
                                                    <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender2" runat="server" TargetControlID="txtDate2"
                                                        Mask="99/99/9999" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                                                        OnInvalidCssClass="MaskedEditError" MaskType="Date" DisplayMoney="Left" AcceptNegative="Left"
                                                        ErrorTooltipEnabled="True" />
                                                    <ajaxToolkit:MaskedEditValidator ID="MaskedEditValidator2" runat="server" ControlExtender="MaskedEditExtender2"
                                                        ControlToValidate="txtDate2" InvalidValueMessage="Date invalide" Display="Dynamic" ForeColor="Red"
                                                        EmptyValueBlurredText="*" InvalidValueBlurredMessage="Date invalide" ValidationGroup="MKE" />
                                                    <ajaxToolkit:FilteredTextBoxExtender ID="ftbeDate2" runat="server" TargetControlID="txtDate2" ValidChars="01234567890/" />--%>
                                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtDate2"
                                                        PopupButtonID="txtDate2" Format="dd/MM/yyyy" />
                                                </asp:TableCell>
                                                <asp:TableCell ID="TableCell12" runat="server">
                                                    <span style="font-style: italic;font-weight: lighter;color:gray">(jj/mm/aaaa)</span>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow ID="pDI3001" runat="server">
                                                <asp:TableCell ID="TableCell13" runat="server">
                                                    <asp:Label ID="lblDI3001" runat="server">Type d'opération</asp:Label>
                                                </asp:TableCell>
                                                <asp:TableCell ID="TableCell14" runat="server">
                                                    <asp:DropDownList ID="ddlDI3001" runat="server" Width="100%"></asp:DropDownList>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow ID="pDI3002" runat="server">
                                                <asp:TableCell ID="TableCell15" runat="server">
                                                    <asp:Label ID="lblDI3002" runat="server">Type d'opération</asp:Label>
                                                </asp:TableCell>
                                                <asp:TableCell ID="TableCell16" runat="server">
                                                    <asp:DropDownList ID="ddlDI3002" runat="server" Width="100%"></asp:DropDownList>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow ID="pMotif" runat="server">
                                                <asp:TableCell ID="TableCell32" runat="server">
                                                    <asp:Label ID="lblMotif" runat="server">Type d'opération</asp:Label>
                                                </asp:TableCell>
                                                <asp:TableCell ID="TableCell33" runat="server">
                                                    <asp:DropDownList ID="ddlMotif" runat="server" Width="100%"></asp:DropDownList>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow ID="pTypeRemb" runat="server">
                                                <asp:TableCell ID="TableCell34" runat="server">
                                                    <asp:Label ID="lblTypeRemb" runat="server">Type d'opération</asp:Label>
                                                </asp:TableCell>
                                                <asp:TableCell ID="TableCell35" runat="server">
                                                    <asp:DropDownList ID="ddlTypeRemb" runat="server" Width="100%"></asp:DropDownList>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow ID="pLibelle1" runat="server">
                                                <asp:TableCell ID="TableCell17" runat="server">
                                                    <asp:HiddenField ID="hdnLibelle1" Value="0" runat="server" />
                                                    <asp:Label ID="lblLibelle1" runat="server">Libellé 1</asp:Label>
                                                </asp:TableCell>
                                                <asp:TableCell ID="TableCell18" runat="server">
                                                    <asp:TextBox ID="tbLibelle1" runat="server" MaxLength="30" Columns="32" Width="98%"></asp:TextBox>
                                                </asp:TableCell>
                                                <asp:TableCell ID="TableCell19" runat="server">
                                                    <span style="font-style: italic;font-weight: lighter;color:gray">Max. 30 car</span>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow ID="pLibelle2" runat="server">
                                                <asp:TableCell ID="TableCell20" runat="server">
                                                    <asp:HiddenField ID="hdnLibelle2" Value="0" runat="server" />
                                                    <asp:Label ID="lblLibelle2" runat="server">Libellé 2</asp:Label>
                                                </asp:TableCell>
                                                <asp:TableCell ID="TableCell21" runat="server">
                                                    <asp:TextBox ID="tbLibelle2" runat="server" MaxLength="30" Columns="32" Width="98%"></asp:TextBox>
                                                </asp:TableCell>
                                                <asp:TableCell ID="TableCell22" runat="server">
                                                    <span style="font-style: italic;font-weight: lighter;color:gray">Max. 30 car</span>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow ID="pMontant1" runat="server">
                                                <asp:TableCell ID="TableCell23" runat="server">
                                                    <asp:Label ID="lblMontant1" runat="server">Montant</asp:Label>
                                                </asp:TableCell>
                                                <asp:TableCell ID="TableCell24" runat="server" HorizontalAlign="Justify" Width="250px">
                                                    <asp:TextBox ID="tbMontant1" runat="server" Style="text-align: right" Width="212.5px" MaxLength="9"></asp:TextBox>
                                                    <ajaxToolkit:FilteredTextBoxExtender ID="ftbeMontant1" runat="server" TargetControlID="tbMontant1" FilterType="Numbers" />
                                                    <asp:Label runat="server" ID="Virgule1" Font-Bold="true">&nbsp;,&nbsp;</asp:Label>
                                                    <asp:TextBox ID="tbMontant1Cts" runat="server" Width="17px" MaxLength="2">00</asp:TextBox>
                                                    <ajaxToolkit:FilteredTextBoxExtender ID="ftbeMontant1Cts" runat="server" TargetControlID="tbMontant1Cts" ValidChars="1234567890" />
                                                </asp:TableCell>
                                                <asp:TableCell ID="TableCell25" runat="server">
                                                    <span style="font-style: italic;font-weight: bold">&euro;</span>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow ID="pMontant2" runat="server">
                                                <asp:TableCell ID="TableCell26" runat="server">
                                                    <asp:Label ID="lblMontant2" runat="server">Montant commission</asp:Label>
                                                </asp:TableCell>
                                                <asp:TableCell ID="TableCell27" runat="server" HorizontalAlign="Justify" Width="250px">
                                                    <asp:TextBox ID="tbMontant2" runat="server" Style="text-align: right" Width="212.5px" MaxLength="9"></asp:TextBox>
                                                    <ajaxToolkit:FilteredTextBoxExtender ID="ftbeMontant2" runat="server" TargetControlID="tbMontant2" FilterType="Numbers" />
                                                    <asp:Label runat="server" ID="virgule2" Font-Bold="true" >&nbsp;,&nbsp;</asp:Label>
                                                    <asp:TextBox ID="tbMontant2Cts" runat="server" Width="17px" MaxLength="2">00</asp:TextBox>
                                                    <ajaxToolkit:FilteredTextBoxExtender ID="ftbeMontant2Cts" runat="server" TargetControlID="tbMontant2Cts" FilterType="Numbers" />
                                                </asp:TableCell>
                                                <asp:TableCell ID="TableCell28" runat="server">
                                                    <span style="font-style: italic;font-weight: bold">&euro;</span>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow ID="pUserComments" runat="server" VerticalAlign="Top">
                                                <asp:TableCell ID="TableCell29" runat="server">
                                                    <asp:Label ID="lblUserComments" runat="server">Commentaires utilisateur</asp:Label>
                                                </asp:TableCell>
                                                <asp:TableCell ID="TableCell30" runat="server">
                                                    <asp:TextBox ID="tbUserComments" runat="server" MaxLength="255" textmode="MultiLine" Wrap="true" rows="8" Columns="30" style="resize:none; font-family:Arial,Verdana;" Width="98%"></asp:TextBox>
                                                </asp:TableCell>
                                                <asp:TableCell ID="TableCell31" runat="server">
                                                    <span style="font-style: italic;font-weight: lighter;color:gray">Max. 255 car</span>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>

                                        <asp:Panel ID="pMulti" Visible="false" runat="server">
                                            <asp:Label ID="lblMulti" runat="server">Fichier CRE</asp:Label>
                                            <input type="button" value="Parcourrir" class="MiniButton" onclick="ShowNewFileDialog();" />
                                            <div id="dialog-add-file" style="display: none">
                                                chargement Fichier
                                    <asp:ClientFileUpload ID="clientFileUpload1" runat="server" ClearForm="true" />
                                                <div>
                                                    <asp:ClientFileManager ID="clientFileManager1" runat="server" AutoLoad="true" AllowSelect="true" AllowCheck="false" />
                                                </div>
                                            </div>
                                        </asp:Panel>
                                        <asp:Panel ID="pCreNonImplementer" runat="server" Visible="false">
                                            <span>CRE Non implémenté pour l'instant.</span>
                                        </asp:Panel>
                                        <div style="text-align: center; margin: 10px 0 50px 0">
                                            <asp:Button ID="btnSoummission" Enabled="false" runat="server" OnClick="btnSoummission_Click" OnClientClick="return ValidateForm();" Text="Soummettre" Style="padding: 6px 0; width: 200px" CssClass="button" />
                                        </div>
                                        
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </asp:Panel>
                        </asp:Panel>
                        <asp:Panel ID="pNoOperation" runat="server">
                            <span>Vous n'&ecirc;tes pas autoris&eacute;(e) &aacute; saisir des op&eacute;rations avec ce profil.</span>
                        </asp:Panel>                         
                    </ContentTemplate>
                </asp:UpdatePanel>

            </asp:Panel>
        </div>
    </div>
    <div id="dialog-alertMessage" style="display: none">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
    </div>
    <div id="dialog-search-customer" style="display: none">
                                            <asp:ClientSearch ID="clientSearch1" runat="server" HideDetails="true" HideSearchButton="false" HideAlerts="false" />
                                        </div>
    
    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        var sRequestControlID = "";
        function BeginRequestHandler(sender, args) {
        }
        function EndRequestHandler(sender, args) {
            hideLoading();
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">
</asp:Content>

