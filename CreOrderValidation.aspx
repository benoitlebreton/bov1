﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="CreOrderValidation.aspx.cs" Inherits="CreOrderValidation" %>

<%@ Register Src="~/FILTRES/Period2.ascx" TagPrefix="filter" TagName="Period" %>
<%@ Register Src="~/API/DatePicker.ascx" TagPrefix="asp" TagName="DatePicker" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <title>Validation opération diverse</title>
    <link href="Styles/tipTip.css" rel="Stylesheet" type="text/css" />
    <link href="Styles/UI/jquery-ui-1.9.2.custom.css" rel="Stylesheet" type="text/css" />
    <link href="Styles/Site.css" rel="stylesheet" type="text/css" />
    <link href="Styles/Operations.css?v=20180314" type="text/css" rel="Stylesheet" />

    <style type="text/css">
        .operation-table-row-ko td{
            background-color:red;
        }
    </style>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#search-popup').dialog({
                autoOpen: false,
                resizable: false,
                draggable: false,
                modal: true,
                minWidth: 330,
                title: "Filtrer les opérations",
                buttons: [{ text: "Filtres par défaut", click: function () { resetFilter(); } },
                { text: $('#<%=btnSearch.ClientID%>').val(), click: function () { $("#<%=btnSearch.ClientID %>").click(); $(this).dialog('close'); } }]
            });
            $("#search-popup").parent().appendTo(jQuery("form:first"));
        });

        function ToggleOperationDetails(row) {
            if ($(row).next('tr').find('.operation-detail, .operation-detail-alternate, .operation-table-row-ko').is(':hidden')) {
                $(row).next('tr').find('td').css('background-color', $(row).find('td').css('background-color'));
                //$(row).next('tr').find('td').css('padding-bottom', '5px');
                $(row).next('tr').find('.operation-detail, .operation-detail-alternate, .operation-table-row-ko').slideDown(400);
                //$(row).find('td').css('border-bottom-right-radius', '0').css('border-bottom-left-radius', '0');

                $(row).find('.detail-arrow').attr('src', $(row).find('.detail-arrow').attr('src').replace(/down/g, 'up'));
            }
            else {
                $(row).next('tr').find('.operation-detail, .operation-detail-alternate, .operation-table-row-ko').slideUp(400, function () {
                    //$(row).find('td:first-of-type').css('border-top-left-radius', '8px').css('border-bottom-left-radius', '8px');
                    //$(row).find('td:last-of-type').css('border-top-right-radius', '8px').css('border-bottom-right-radius', '8px');
                    //$(row).next('tr').find('td').animate({ paddingBottom: 0 }, 0);
                });

                $(row).find('.detail-arrow').attr('src', $(row).find('.detail-arrow').attr('src').replace(/up/g, 'down'));
            }
        }

        function ShowConfirmCancelOpe(refOperation) {
            $('#<%=hdnRefOperation.ClientID%>').val(refOperation);

            $('#dialog-cancel-operation').dialog({
                autoOpen: true,
                resizable: false,
                width: 400,
                dialogClass: "no-close",
                modal: true,
                title: "suppression opération",

                buttons: [
                    {
                        text: "Annuler",
                        click: function () {
                            $(this).dialog("close");
                            $(this).dialog("destroy");
                        }
                    },
                    {
                        text: "Supprimer l'opération",
                        autofocus: true,
                        click: function () {
                            $('#<%=btnCancelOpe.ClientID%>').click();
                            showLoading();
                            $(this).dialog("close");
                            $(this).dialog("destroy");
                        }
                    }                    
                ]
            });
            //$(".ui-dialog-titlebar").hide();
            $("#dialog-cancel-operation").parent().appendTo(jQuery("form:first"));            
        }

        function closeConfirmCancelOpe() {
            showLoading();
            $("#dialog-cancel-operation").dialog.close();
            $("#dialog-cancel-operation").dialog.destroy();            
        }

        function ShowConfirmValidOpe(refOperation, ErrorMessage) {
            $('#<%=hdnRefOperation.ClientID%>').val(refOperation);

            if (ErrorMessage.length > 0) {
                AlertMessage("erreur", ErrorMessage, null, null);
                return;
            }

            $('#dialog-valid-operation').dialog({
                autoOpen: true,
                resizable: false,
                width: 400,
                dialogClass: "no-close",
                modal: true,
                title: "Validation opération",
                buttons: [
                    {
                        text: "Annuler",
                        click: function () {
                            $(this).dialog("close");
                            $(this).dialog("destroy");
                        }
                    },
                    {
                        text: "Valider l'opération",
                        autofocus: true,
                        click: function () {
                            $('#<%=btnValidOpe.ClientID%>').click();
                            showLoading();
                            $(this).dialog("close");
                            $(this).dialog("destroy");
                        }
                    }                    
                ]                
            });
            //$(".ui-dialog-titlebar").hide();
            $("#dialog-valid-operation").parent().appendTo(jQuery("form:first"));
        }

        function closeConfirmValidOpe() {
            showLoading();
            $("#dialog-valid-operation").dialog.close();
            $("#dialog-valid-operation").dialog.destroy();
        }

        function ShowSearchPopup() {
            $("#search-popup").dialog('open');
        }

        function padLeft(str, max) {
            str = str.toString();
            return str.length < max ? padLeft("0" + str, max) : str;
        }

        function resetFilter() {
            $("#<%=txtDateFrom.ClientID%>").val('01/01/2000');
            var now = new Date();
            $("#<%=txtDateTo.ClientID%>").val(padLeft(now.getDate(), 2) + "/" + padLeft(now.getMonth() + 1, 2) + "/" + now.getFullYear());

            if ($('#<%=rblStatus.ClientID%> input:checked').length > 0) {
                $('#<%=rblStatus.ClientID%> input[type=radio]').each(function () {
                    if ($(this).val() == "2")
                        $(this).attr('checked', true);
                    else $(this).attr('checked', false);
                });
            }

            var ddlClient = document.getElementById('<%=ddlFeature.ClientID%>');
            ddlClient.selectedIndex = 0;

            ddlClient = document.getElementById('<%=ddlTypeCre.ClientID%>');
            ddlClient.selectedIndex = 0;
            
            ddlClient = document.getElementById('<%=ddlOrderAsc.ClientID%>');
            ddlClient.selectedIndex = 0;

            ddlClient = document.getElementById('<%=ddlOrderBy.ClientID%>');
            ddlClient.selectedIndex = 0;
        }

        function AlertMessageOD(title, message, greenMessage) {
            $('#<%=lblMessage.ClientID%>').html(message);
            var lblMsg = document.getElementById('<%=lblMessage.ClientID%>');
            console.log(greenMessage);
            if (greenMessage == 'True')
                lblMsg.style.color = 'green';
            else
                lblMsg.style.color = 'red';

            $("#dialog-alertMessage").dialog({
                resizable: false,
                width: 700,
                modal: true,
                title: title,
                buttons: {
                    "Fermer": function () {
                        $(this).dialog("close");
                        $(this).dialog("destroy");
                    }
                }
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div style="position: relative;">
        <asp:Button ID="btnPrevious" runat="server" Text="Menu précédent" PostBackUrl="~/CreOrder.aspx" CssClass="button" />
    </div>
    <h2 style="color: #344b56; margin-bottom: 10px; text-transform: uppercase">Validation d'une opération
    </h2>
    <div style="padding: 20px 0;">
        <div style="align-content: center">
            <asp:Panel ID="panelOperationListTable" runat="server">
                <asp:UpdatePanel ID="upOpe" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Panel ID="panelFilters" runat="server" Visible="true">
                            <div style="padding: 0 2px; text-align: left; font-size: 1.2em;" class="font-AracneRegular">
                                <div style="width: 90%; float: left">
                                    <asp:Literal ID="ltlFilters" runat="server"></asp:Literal>
                                </div>
                                <div style="width: 10%; float: right; text-align: right">
                                    <asp:Literal ID="ltlNbResults" runat="server"></asp:Literal>
                                </div>
                                <div style="clear: both"></div>
                            </div>
                        </asp:Panel>
                        <div id="ar-loading" style="display: none; position: absolute; background-color: rgba(255, 255, 255, 0.5); z-index: 100"></div>
                        <asp:Panel ID="pOperation" runat="server">
                            <asp:Panel ID="tableOperation" ScrollBars="none" runat="server">
                                <div id="dOperation">
                                    <table id="tOperationList" style="width: 100%; margin-bottom: 10px;" class="operation-table">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <div>Date demande</div>
                                                </th>
                                                <th>
                                                    <div>Code</div>
                                                </th>
                                                <th style="white-space: nowrap">
                                                    <div>Type op&eacute;ration</div>
                                                </th>
                                                <th>
                                                    <div>Compte client</div>
                                                </th>
                                                <th>
                                                    <div>Montant client</div>
                                                </th>
                                                <th>
                                                    <div>Compte buraliste</div>
                                                </th>
                                                <th>
                                                    <div>Montant buraliste</div>
                                                </th>
                                                <%--<th>
                                                    <div>Status Op&eacute;ration</div>
                                                </th> --%>
                                                <th>
                                                    <div>Actions</div>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <asp:Repeater ID="rptOperationList" runat="server">
                                                <HeaderTemplate>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr class='<%# Eval("iCRECr").ToString() != "0" ? "operation-table-row-ko" : (Container.ItemIndex % 2 == 0 ? "operation-table-row" : "operation-table-alternate-row") %>'
                                                        onclick="ToggleOperationDetails(this);">
                                                        <td style="width: 1px; white-space: nowrap; text-align: center">
                                                            <div>
                                                                <span style="position: relative; left: -6px;">
                                                                    <img class="detail-arrow" src="Styles/Img/row-detail-arrow-down.png" />
                                                                </span>
                                                                <asp:Image ID="imStatus" runat="server" ImageUrl='<%#GetImageStatus(Eval("iCREOrderStatus").ToString(), Eval("iCRECr").ToString() ) %>' CssClass="image-tooltip" />
                                                                <%# Eval("dtCREOrderUTCDateTime") %>
                                                            </div>
                                                        </td>
                                                        <td style="width: 1px; white-space: nowrap; text-align: center">
                                                            <div>
                                                                <%# GetCodeOperation(Eval("sOperationTAG").ToString()) %>
                                                            </div>
                                                        </td>
                                                        <td style="text-align: center; width: 300px;height:50px;vertical-align:middle">
                                                            <div>
                                                                <%# Eval("sOperationDescription").ToString() %>
                                                            </div>
                                                        </td>
                                                        <td style="width: 1px; white-space: nowrap; text-align: center">
                                                            <div>
                                                                <%# Eval("sCompte1").ToString() %>
                                                            </div>
                                                        </td>
                                                        <td style="width: 1px; white-space: nowrap; text-align: center">
                                                            <div>
                                                                <%# getFormattedAmount(Eval("mMontant1").ToString()) %>
                                                            </div>
                                                        </td>
                                                        <td style="width: 1px; white-space: nowrap; text-align: center">
                                                            <div>
                                                                <%# Eval("sCompte2").ToString() %>
                                                            </div>
                                                        </td>
                                                        <td style="width: 1px; white-space: nowrap; text-align: center">
                                                            <div>
                                                                <%# getFormattedAmount(Eval("mMontant2").ToString()) %>
                                                            </div>
                                                        </td>
                                                        <%--
                                                        <td style="width: 1px; white-space: nowrap; text-align: center">
                                                            <div>
                                                                <%# getOperationStatus(Eval("iCREOrderStatus").ToString()) %>
                                                            </div>
                                                        </td>
                                                        --%>
                                                        <td style="width: 1px; white-space: nowrap; text-align: center">
                                                            <table width="100%">
                                                                <tr>
                                                                    <%# getActionAuthorized(Eval("iCREOrderStatus").ToString(), Eval("iRefCREOrder").ToString()
                                                                            , Eval("bCanBeRejected").ToString(), Eval("bCanBeDeleted").ToString(), Eval("bCanBeValidated").ToString()
                                                                            , Eval("mMontant1").ToString(), Eval("mMontant2").ToString(), Eval("sOperationTAG").ToString(), Eval("sTAGFeature").ToString()
                                                                            , Eval("iCRECr").ToString()) %>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr style="cursor: auto; border: 0">
                                                        <td id="tdDetails" runat="server" colspan="8" style="padding: 0">
                                                            <!--style="border-bottom-left-radius:8px;border-bottom-right-radius:8px;"-->
                                                            <div class='<%# Eval("iCRECr").ToString() != "0" ? "operation-table-row-ko" : (Container.ItemIndex % 2 == 0 ? "operation-detail" : "operation-detail-alternate") %>' style="display: none; position: relative; z-index: 0">
                                                                <div class="separator"></div>
                                                                <div class="detail-content" style="min-height: 15px;">
                                                                    <asp:HiddenField ID="hdnOpeCode" runat="server" Value='<%# Eval("sOperationTAG")%>' />
                                                                    <asp:HiddenField ID="hdnOpeStatus" runat="server" Value='<%# Eval("iCREOrderStatus")%>' />
                                                                    <div class="ellipsis" style="width: 80%;">
                                                                        <%# GetOpeationDetailsToShow(Eval("sDivers").ToString()
                                                                                                                                                , Eval("sLibelle1").ToString()
                                                                                                                                                , Eval("sCREOrderUserName").ToString()
                                                                                                                                                , Eval("sLabelFeature").ToString()
                                                                                                                                                , Eval("sDescriptionFeature").ToString()
                                                                                                                                                , Eval("sUserComment").ToString()
                                                                                                                                                , Eval("sCREToValidateRefUserLabelGroup").ToString()
                                                                                                                                                , Eval("sCREToValidateUserName").ToString()
                                                                                                                                                , Eval("sCREValidationUserName").ToString()
                                                                                                                                                , Eval("dtCREValidationUTCDateTime").ToString()
                                                                                                                                                , Eval("iCREOrderStatus").ToString()
                                                                                                                                                , Eval("iCRECr").ToString()
                                                                                                                                                , Eval("iRefCRE").ToString()
                                                                                                                                                , Eval("sCREErrorMessage").ToString()
                                                                                                                                                ) %>
                                                                    </div>

                                                                    <%--<div class="ellipsis" style="width: 46%;">Divers <%# Eval("sDivers").ToString() %></div>
                                                                    <div class="ellipsis" style="width: 46%;">Libelle1 <%# Eval("sLibelle1").ToString() %></div>
                                                                    <div class="ellipsis" style="width: 46%;">Demand&eacute; par <%# Eval("sCREOrderUserName").ToString() %></div>
                                                                    <%-- <div class="ellipsis" style="width: 46%;">Profil <%# GetFeatureDetails(Eval("sTagFeature")) %></div> 
                                                                    <div class="ellipsis" style="width: 46%;">Commentaires <%# Eval("sUserComment").ToString() %></div>
                                                                    <div class="ellipsis" style="width: 46%;">A faire valider par <%# GetValidatorDetails(Eval("sCREToValidateRefUserLabelGroup").ToString(), Eval("sCREToValidateUserName").ToString()) %></div>
                                                                    <%-- <div class="ellipsis" style="width: 46%;">valider par <%# GetValidatorDetails(Eval("iCREValidationRefUser").ToString(), Eval("sCREValidateUserName").ToString()) %></div>
                                                                    <div class="ellipsis" style="width: 46%;">Date validation <%# Eval("dtCREValidationUTCDateTime").ToString() %></div>--%>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </tbody>
                                    </table>
                                </div>
                            </asp:Panel>
                        </asp:Panel>

                        <asp:Panel ID="panelNoSearchResult" runat="server" Visible="false" Style="margin-top: 10px">
                            Votre recherche n'a retourné aucun résultat.
                        </asp:Panel>
                        <div style="text-align: center; margin: 50px 0 50px 0">
                            <asp:Button ID="btnShowMore" runat="server" CssClass="button" Text="Afficher plus d'opérations" Style="padding-left: 50px; padding-right: 50px; margin-right: 1px" OnClick="btnShowMore_Click" />
                            <input type="button" value="Filtrer les opérations" class="button" onclick="ShowSearchPopup();" style="padding-left: 50px; padding-right: 50px;" />
                            <%-- <asp:Button ID="btnExcelExport" runat="server" Visible="false" CssClass="button excel-button" Text="Export" OnClick="btnExportExcel_Click" /> display: none; --%>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <div id="search-popup" style="display: none">
                    <span class="ui-helper-hidden-accessible">
                        <input type="text" />
                    </span>
                    <div>
                        <div style="font-weight: bold; text-transform: capitalize; margin-top: 10px">
                            Date de demande
                        </div>
                        <div style="white-space: nowrap">
                            <table>
                                <tr valign="middle">
                                    <td>
                                        <asp:Label ID="lblDateFrom" runat="server" AssociatedControlID="txtDateFrom" CssClass="font-AracneRegular font-orange" Font-Bold="true">Du</asp:Label>&nbsp;</td>
                                    <td>
                                        <asp:TextBox ID="txtDateFrom" runat="server" Style="width: 100px" ReadOnly="true"></asp:TextBox>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtDateFrom"
                                            PopupButtonID="txtDateFrom" Format="dd/MM/yyyy" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblDateTo" runat="server" AssociatedControlID="txtDateTo" CssClass="font-AracneRegular font-orange" Font-Bold="true">Au</asp:Label>&nbsp;</td>
                                    <td>
                                        <asp:TextBox ID="txtDateTo" runat="server" Style="width: 100px" ReadOnly="true"></asp:TextBox>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtDateTo"
                                            PopupButtonID="txtDateTo" Format="dd/MM/yyyy" PopupPosition="BottomRight" />
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div style="font-weight: bold; text-transform: capitalize; margin-top: 10px">
                            Statut opération
                        </div>
                        <div>
                            <asp:RadioButtonList ID="rblStatus" runat="server" AutoPostBack="false" RepeatDirection="Vertical"
                                CssClass="RadioButtonList" Height="20px"
                                Style="position: relative; left: 0px; text-transform: uppercase">
                                <asp:ListItem Value="0">tous</asp:ListItem>
                                <asp:ListItem Value="2" Selected="True"><span style="color:gold;text-shadow: -1px -1px 0px #FFF, 1px 1px 0px #FFF, -1px 1px 0px #FFF, 1px -1px 0px #FFF;">en attente de validation</span></asp:ListItem>
                                <asp:ListItem Value="5"><span style="color:green;text-shadow: -1px -1px 0px #FFF, 1px 1px 0px #FFF, -1px 1px 0px #FFF, 1px -1px 0px #FFF;">valid&eacute;e</span></asp:ListItem>
                                <asp:ListItem Value="3"><span style="color:gray;text-shadow: -1px -1px 0px #FFF, 1px 1px 0px #FFF, -1px 1px 0px #FFF, 1px -1px 0px #FFF;">annul&eacute;e par demandeur</span></asp:ListItem>
                                <asp:ListItem Value="4"><span style="color:darkorange;text-shadow: -1px -1px 0px #FFF, 1px 1px 0px #FFF, -1px 1px 0px #FFF, 1px -1px 0px #FFF;">rejet&eacute;e par valideur</span></asp:ListItem>
                                <asp:ListItem Value="6"><span style="color:red;text-shadow: -1px -1px 0px #FFF, 1px 1px 0px #FFF, -1px 1px 0px #FFF, 1px -1px 0px #FFF;">CRE KO</span></asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                        <div style="font-weight: bold; text-transform: capitalize; margin-top: 10px">
                            Profil opération
                        </div>
                        <div>
                            <asp:DropDownList ID="ddlFeature" runat="server" Enabled="true" AutoPostBack="false"></asp:DropDownList>
                        </div>
                        <div style="font-weight: bold; text-transform: capitalize; margin-top: 10px">
                            Type opération
                        </div>
                        <div>
                            <asp:DropDownList ID="ddlTypeCre" runat="server" Enabled="true" AutoPostBack="false" Style="width: 280px"></asp:DropDownList>
                        </div>
                        <div style="font-weight: bold; text-transform: capitalize; margin-top: 10px">
                            Type de tri
                        </div>
                        <div>
                            <asp:DropDownList ID="ddlOrderAsc" runat="server" Enabled="true" AutoPostBack="false">
                                <asp:ListItem Value="DESC" Selected="True">Descendant</asp:ListItem>
                                <asp:ListItem Value="">Ascendant</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div style="font-weight: bold; text-transform: capitalize; margin-top: 10px">
                            Trier sur
                        </div>
                        <div>
                            <asp:DropDownList ID="ddlOrderBy" runat="server" Enabled="true" AutoPostBack="false">
                                <asp:ListItem Value="iRefCREOrder" Selected="True">Date demande</asp:ListItem>
                                <asp:ListItem Value="sTAGFeature">Profil opération</asp:ListItem>
                                <asp:ListItem Value="sOperationTAG">Type d'opération</asp:ListItem>
                                <asp:ListItem Value="iCREOrderStatus">Statut d'opération</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div style="text-align: center; margin-top: 20px">
                        <asp:Button ID="btnSearch" runat="server" Text="Rechercher" OnClick="btnSearch_Click" CssClass="button" Style="display: none" />
                    </div>
                </div>
            </asp:Panel>
        </div>
    </div>
    <asp:HiddenField ID="hdnRefOperation" runat="server" />
    <div id="dialog-cancel-operation" style="display: none">
        Vous êtes sur le point de supprimer cette opération.
        <br />
        <br />
        Cette action est immédiate et irrévocable.
        <br />
        <br />
        <div style="text-align: right">
            <asp:Button ID="btnCancelOpe" runat="server" Text="Supprimer l'opération" OnClick="btnCancelOpe_Click" CssClass="orange-big-button" style="display:none" />
        </div>
    </div>
    <div id="dialog-valid-operation" style="display: none">
        Vous êtes sur le point de valider cette opération.
        <br />
        <br />
        Cette action est immédiate et irrévocable.
        <br />
        <br />
        <div style="text-align: right">
            <asp:Button ID="btnValidOpe" runat="server" Text="Valider l'opération" OnClick="btnValidOpe_Click" CssClass="orange-big-button" OnClientClick="showLoading();" style="display:none"/>
        </div>
    </div>
    <div id="dialog-alertMessage" style="display: none">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
    </div>
        <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        var sRequestControlID = "";
        function BeginRequestHandler(sender, args) {
        }
        function EndRequestHandler(sender, args) {
            hideLoading();
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">
</asp:Content>

