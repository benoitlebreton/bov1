﻿<%@ Page Title="Recherche client" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ClientSearch.aspx.cs" Inherits="ClientSearch" %>
<%@ Register Src="~/API/DatePicker.ascx" TagPrefix="asp" TagName="DatePicker" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">

    <script type="text/javascript">
        $(document).ready(function () {

            $("#HelpGlobalSearch").tooltip({
                position: {
                    my: "center bottom-10",
                    at: "center top",
                    using: function (position, feedback) {
                        $(this).css(position);
                        $("<div>")
                            .addClass("arrow")
                            .addClass(feedback.vertical)
                            .addClass(feedback.horizontal)
                            .appendTo(this);
                    }
                }
            });

            if ($('#ClientGlobalSearch').is(':visible')) {
                $('#<%=txtGlobalSearch.ClientID%>').focus();
            }

            $('.RadioButtonList').buttonset();
            $("#divFilters").accordion({
                heightStyle: "content",
                beforeActivate: function (event, ui) {
                    var active = $("#divFilters").accordion("option", "active");
                    if (active == 0) {
                        $('#<%=txtGlobalSearch.ClientID%>').val('');
                    }
                    else if (active == 1) {
                        $('#<%=txtClientLastName.ClientID%>').val('');
                        $('#<%=txtClientFirstName.ClientID%>').val('');
                        $('#<%=rblAddressStatus.ClientID%> input[value=""]').attr('checked', 'checked');
                        $("#<%=rblAddressStatus.ClientID%> input").button("refresh");
                    }
                    else if (active == 2) {
                        $('#<%=txtRegistrationNumber.ClientID%>').val('');
                        $('#<%=txtBarcode.ClientID%>').val('');
                    }
                }
            });
            $("#divFilters").accordion("option", "animate", true);
            $('#<%=divFilters2.ClientID%>').accordion({
                animate: true,
                heightStyle: "content",
                active: false,
                collapsible: true
            });

            if ($('#<%=txtClientLastName.ClientID%>').val().length > 0 || $('#<%=txtClientFirstName.ClientID%>').val().length > 0) {
                $("#divFilters").accordion({ active: 0 });
            }
            else if ($('#<%=txtBarcode.ClientID%>').val().length > 0 || $('#<%=txtRegistrationNumber.ClientID%>').val().length > 0) {
                $("#divFilters").accordion({ active: 1 });
            }

            $('#divFiltersErrorPopup').dialog({
                autoOpen: false,
                resizable: false,
                draggable: false,
                modal: true,
                minWidth: 400,
                title: "Erreur",
                buttons: [{ text: "OK", click: function () { $(this).dialog('close'); } }]
            });
        });

        function CheckFilters() {
            var minGlobalSearchAlpha = 3;
            var minGlobalSearchNum = 6;

            if ($('#<%=txtClientLastName.ClientID%>').val().trim() == '' && $('#<%=txtClientFirstName.ClientID%>').val().trim() == ''
                && $('#<%=rblAddressStatus.ClientID%> input:checked').val().trim() == ''
                && $('#<%=txtBarcode.ClientID%>').val().trim() == ''
                && $('#<%=txtRegistrationNumber.ClientID%>').val().trim() == ''
                && $('#<%=txtGlobalSearch.ClientID%>').val().trim() == '') {
                $('#sFiltersError').html("Veuillez remplir l'un des champs de recherche");
                $('#divFiltersErrorPopup').dialog('open');
                return false;
            }
            else { 
                var isOK = true;
                if ($('#<%=txtGlobalSearch.ClientID%>').val().trim() != '') {
                //check global search value
                    if (parseInt($('#<%=txtGlobalSearch.ClientID%>').val().trim()) && $('#<%=txtGlobalSearch.ClientID%>').val().trim().length < minGlobalSearchNum) {
                        isOK = false;
                        $('#sFiltersError').html("La recherche globale doit contenir au minimum " + minGlobalSearchNum + " caractères numériques ou " + minGlobalSearchAlpha + " caractères alphanumériques.");
                        $('#divFiltersErrorPopup').dialog('open');
                    }
                    else if ($('#<%=txtGlobalSearch.ClientID%>').val().trim().length < minGlobalSearchAlpha) {
                        isOK = false;
                        $('#sFiltersError').html("La recherche globale doit contenir au minimum " + minGlobalSearchNum + " caractères numériques ou " + minGlobalSearchAlpha + " caractères alphanumériques.");
                        $('#divFiltersErrorPopup').dialog('open');
                    }
                }

                if (isOK)
                    showLoading();

                return isOK;
            }
        }

        function ShowClientDetails(sRef) {
            document.location.href = "ClientDetails.aspx?ref=" + sRef;
        }

        function SearchSurveillance() {
            $('#<%=btnSearchSurveillance.ClientID%>').click();
        }

        function ShowExportsExcelTab() {
            $("#<%=divFilters2.ClientID%>").accordion({ active: 0 });
        }

        function ShowExportsExcelLoadingScreen() {
            $('.div-exports-excel-loading').width($('#ClientExportExcel').width());
            $('.div-exports-excel-loading').height($('#ClientExportExcel').height());
            $('.div-exports-excel-loading').show();
        }

        var iCpt = 4;
        var interval = null;
        function OpenZendeskTab(id) {
            OpenZendeskInterval(id);
            interval = setInterval(function () { OpenZendeskInterval(id) }, 1000);
        }
        function OpenZendeskInterval() {
            iCpt--;
            $('.zendesk-interval').html(iCpt);
            if (iCpt == 0) {
                $('#zendesk-countdown').hide();
                clearInterval(interval);
                var newwin = window.open('https://comptenickel.zendesk.com/agent/tickets/new/1');
                if (!newwin)
                    AlertMessage("Erreur", "Veuillez autoriser l'ouverture de popup puis recharger la page.", null, null);
            }
        }
    </script>

    <style type="text/css">

        .buraliste-icon {
            position:relative;
        }
        .buraliste-icon img {
            position:absolute;
            top:4px;
            left:15px;
            width:auto;
            height:35px;
        }
        
        .custom-table {
        margin-top:10px;
        }
        .custom-table .head, .custom-table .head-light {
            font-weight:bold;
            color:#fff;
            background-color:#344b56;
            text-transform:uppercase;
        }
        .custom-table .head-light {
            background-color:#517384;
        }
        .custom-table input[type=checkbox] {
            display:none;
        }
        .custom-table input[type=text] {
            width:150px;
        }
        .custom-table .table-row .table-cell {
            border-bottom:1px solid #ddd;
            vertical-align:middle;
            height:30px;
        }
        .custom-table .table-row .table-cell .toggle-soft {
            position:relative;
            top:-3px;
        }
        .custom-table .table-row .table-cell:nth-of-type(2) {
            text-align:center;
        }
        .custom-table .table-row .table-cell:nth-of-type(3), .custom-table .table-row .table-cell:nth-of-type(4) {
            padding:0 5px;
        }
        .custom-table .head .table-cell, .custom-table .head-light .table-cell {
            padding:0 10px;
            height:20px;
        }

        .div-exports-excel-loading {
            background:url('Styles/img/loading2.gif') no-repeat center;
            background-size: 35px 27px;
            display:none;
            position:absolute;
            background-color:rgba(255, 255, 255, 0.5);
            z-index:10000;
        }

        table#MainContent_gvClientSearchResult tr td:first-of-type {
            padding:5px 0 1px 0 !important;
            background-color:#fff;
            vertical-align:middle;
        }

        .svi-headband {
            position:fixed;
            z-index:99995;
            top:0;
            left:0;
            width:100%;
            text-align:center;
            padding:10px;
            text-transform:uppercase;
            font-weight:bold;
            color:#fff;
            box-shadow:1px 1px 3px 0px #999;
        }
        .svi-headband.svi-not-auth {
            background-color:#f00;
        }
        .svi-headband.svi-auth {
            background-color:#0b0;
        }
        .svi-headband.svi-offset {
            top:93px;
        }
        #zendesk-countdown {
            display:inline-block;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

    <asp:Panel ID="panelSVIAuthenticated" runat="server" Visible="false" CssClass="svi-headband">
        <asp:Literal ID="litSVIauth" runat="server"></asp:Literal>
        <div id="zendesk-countdown">&nbsp;(Zendesk s'ouvrira dans <span class="zendesk-interval"></span>s)</div>
    </asp:Panel>

    <asp:Panel ID="panelClientSearch" runat="server" DefaultButton="btnSearch">
        <h2 style="color: #344b56">
            RECHERCHE CLIENT
        </h2>
        <div style="margin: 20px auto; width: 90%">
            <div id="divFilters">
                <h3 style="text-transform:uppercase">
                    Globale
                </h3>
                <div id="ClientGlobalSearch" style="margin:auto">
                    <div class="table" style="width:100%">
                        <div class="row">
                            <div class="cell" style="width:600px;">
                                <asp:TextBox ID="txtGlobalSearch" runat="server" autocomplete="off" style="width:600px;" MaxLength="200"></asp:TextBox>
                            </div>
                            <div class="cell" style="padding-left:10px;vertical-align:middle;padding-top:2px ">
                                <img id="HelpGlobalSearch" title="<span>Recherche par nom, prénom, n&deg téléphone,<br/>n&deg; compte, n&deg; pack, ...</span>" 
                                    src="Styles/Img/help.png" alt="?" style="max-height:25px; max-width:25px" />
                            </div>
                        </div>
                    </div>
                </div>
                <h3 style="text-transform: uppercase">
                    par informations personnelles</h3>
                <div id="ClientSearchByClient" style="margin: auto">
                    <div style="display: table; width: 100%">
                        <div style="display: table-row">
                            <div style="display: table-cell; width: 33%">
                                <div style="font-weight: bold">
                                    Nom</div>
                                <div>
                                    <asp:TextBox ID="txtClientLastName" runat="server" autocomplete="off"></asp:TextBox>
                                </div>
                            </div>
                            <div style="display: table-cell; width: 33%">
                                <div style="font-weight: bold">
                                    Prénom</div>
                                <div>
                                    <asp:TextBox ID="txtClientFirstName" runat="server" autocomplete="off"></asp:TextBox>
                                </div>
                            </div>
                            <div style="display: table-cell; width: 33%">
                            </div>
                        </div>
                        <div style="display:table-row">
                            <div style="display: table-cell; width: 33%">
                                <div style="font-weight: bold">
                                    Etat adresse
                                </div>
                                <div>
                                    <asp:RadioButtonList ID="rblAddressStatus" runat="server" AutoPostBack="false" RepeatDirection="Horizontal" CssClass="RadioButtonList" Height="20px" Style="position: relative; left: 0px">
                                        <asp:ListItem Value="" Selected="True">TOUS</asp:ListItem>
                                        <asp:ListItem Value="2"><span style="color:gold;text-shadow: -1px -1px 0px #000, 1px 1px 0px #000, -1px 1px 0px #000, 1px -1px 0px #000;white-space:nowrap;letter-spacing:2px">EN ATTENTE</span></asp:ListItem>
                                        <asp:ListItem Value="3"><span style="color:green;text-shadow: -1px -1px 0px #000, 1px 1px 0px #000, -1px 1px 0px #000, 1px -1px 0px #000;white-space:nowrap;letter-spacing:2px">VALID&Eacute;E</span></asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <h3 style="text-transform: uppercase">
                    par informations de compte</h3>
                <div id="ClientSearchByAccount" style="margin: auto">
                    <div style="font-weight: bold">
                        Code barre carte
                    </div>
                    <div>
                        <asp:TextBox ID="txtBarcode" runat="server" autocomplete="off"></asp:TextBox>
                    </div>
                    <div style="font-weight: bold">
                        N° inscription
                    </div>
                    <div>
                        <asp:TextBox ID="txtRegistrationNumber" runat="server" autocomplete="off"></asp:TextBox>
                    </div>
                </div>

                <h3 style="text-transform:uppercase" onclick="SearchSurveillance();" class="ui-accordion-header ui-helper-reset ui-state-default ui-accordion-icons ui-corner-all">
                    sous surveillance
                </h3>
            </div>
            <asp:Panel id="divFilters2" runat="server">
                <h3 style="text-transform:uppercase">
                    exports excel
                </h3>
                <div id="ClientExportExcel" style="margin: auto;">
                    <div class="div-exports-excel-loading"></div>
                    <asp:Repeater ID="rptExportsExcel" runat="server">
                        <HeaderTemplate>
                            <div class="table custom-table" style="width:100%">
                                <div class="table-row head">
                                    <div class="table-cell">
                                        Export
                                    </div>
                                    <div class="table-cell">
                                        Date<br />mise à jour
                                    </div>
                                    <div class="table-cell">

                                    </div>
                                </div>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <div class="table-row">
                                <div class="table-cell">
                                    <asp:Label ID="lblExport" runat="server" Visible='<%#Eval("Link").ToString().Length > 0 ? false : true %>'><%#Eval("Label") %></asp:Label>
                                    <asp:HyperLink ID="lnkExport" runat="server" Visible='<%#Eval("Link").ToString().Length > 0 ? true : false %>' NavigateUrl='<%#Eval("Link") %>' Target="_blank"><%#Eval("Label") %></asp:HyperLink>
                                </div>
                                <div class="table-cell">
                                    <%#Eval("Date") %>
                                </div>
                                <div class="table-cell" style="vertical-align:middle">
                                    <asp:Button ID="btnRefreshExport" runat="server" Text="Actualiser" OnClick="btnRefreshExport_Click" CommandArgument='<%#Eval("Tag") %>' Visible='<%#Eval("Tag").ToString().Length > 0 ? true : false %>' OnClientClick="ShowExportsExcelLoadingScreen();" CssClass="button" style="position:relative;top:5px;" />
                                </div>
                            </div>
                        </ItemTemplate>
                        <FooterTemplate>
                            </div>
                            <div style="text-align:right;font-size:0.8em;margin-top:5px;">
                                <b><u>Note :</u></b> Le rafraîchissement des données peut prendre plusieurs minutes
                            </div>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
            </asp:Panel>
            <div id="divFiltersErrorPopup" style="display:none">
                <span id="sFiltersError">Veuillez remplir l'un des champs de recherche</span>
            </div>
            <div style="text-align:center;margin-top:20px">
                <asp:Button ID="btnSearch" runat="server" Text="Rechercher" OnClick="btnSearch_Click" OnClientClick="return CheckFilters();" style="width: 200px" CssClass="buttonHigher" />
                <asp:Button ID="btnSearchSurveillance" runat="server" style="display:none" OnClick="btnSearchSurveillance_Click" OnClientClick="showLoading();" />
            </div>

            <asp:UpdatePanel ID="upSearch" runat="server" UpdateMode="Conditional">
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnSearchSurveillance" EventName="Click" />
                </Triggers>
                <ContentTemplate>
                    <asp:Panel ID="panelGrid" runat="server" Visible="false">
                        <div style="text-align:right;margin-top:10px">Nombre de résultat(s) : <asp:Label ID="lblRowCount" runat="server"></asp:Label></div>
                        <div style="border: 3px solid #344b56; border-top-left-radius: 8px; border-top-right-radius: 8px;margin-top:5px">
                            <asp:GridView ID="gvClientSearchResult" runat="server" AllowSorting="false"
                                AllowPaging="false" AutoGenerateColumns="False" GridLines="None" CellPadding="10"
                                DataKeyNames="RefCustomer" ShowHeader="true" Width="100%" SortedDescendingCellStyle-Font-Overline="true"
                                ViewStateMode="Disabled" BorderWidth="0" BackColor="Transparent" OnRowDataBound="GridView1_RowDataBound">
                                <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="#344b56" Font-Bold="true" />
                                <EmptyDataTemplate>
                                    Aucune donnée ne correspond à la recherche
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="" ControlStyle-Width="1px" ControlStyle-BackColor="White">
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hdnServiceTag" runat="server" Value='<%#Eval("AccountKind") %>' />
                                            <img src='<%#tools.GetAccountTypeImg(Eval("AccountKind").ToString()) %>' style='<%#(Eval("AccountKind").ToString() == "2") ? "display:none" : "" %>' width="50px" />
                                            <asp:Image ID="imgBuraliste" runat="server" ImageUrl="~/Styles/Img/bura-orange.png" Visible='<%#(Eval("AccountKind").ToString() == "2") ? true : false %>' AlternateText="Buraliste" ToolTip="Buraliste" style="width:25px;position:relative;left:8px;" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="AccountNumber" InsertVisible="false" ItemStyle-CssClass="buraliste-icon"
                                        ItemStyle-HorizontalAlign="Center" HeaderText="N° Compte">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblAccountNumber" Text='<%# Eval("AccountNumber")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="false" InsertVisible="false">
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hdnRefCustomer" runat="server" Value='<%# Eval("RefCustomer")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Nom" SortExpression="LastName" InsertVisible="false"
                                        ControlStyle-CssClass="trimmer" ControlStyle-Width="100px">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblClientLastName" Text='<%# Eval("LastName")%>' CssClass="tooltip"
                                                ToolTip='<%# Eval("LastName")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Prénom" SortExpression="FirstName" InsertVisible="false"
                                        ControlStyle-CssClass="trimmer" ControlStyle-Width="100px">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblClientFirstName" Text='<%# Eval("FirstName") %>'
                                                CssClass="tooltip" ToolTip='<%# Eval("FirstName")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="BirthDate" HeaderText="Date de naissance" InsertVisible="false" ControlStyle-CssClass=""
                                        ControlStyle-Width="70px" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblClientBirthDate" Text='<%# Eval("BirthDate")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle BackColor="#344b56" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#344b56" Font-Bold="True" ForeColor="White" Height="25px" />
                                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                <RowStyle CssClass="GridViewRowStyle" />
                                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                <%--<PagerStyle CssClass="GridViewPager" />--%>
                            </asp:GridView>
                        </div>
                        <div style=" background-color: #344b56; color: White; border: 3px solid #344b56;border-bottom-left-radius: 8px; border-bottom-right-radius: 8px; height: 25px">
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </asp:Panel>

    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        var sRequestControlID = "";
        function BeginRequestHandler(sender, args) {
        }
        function EndRequestHandler(sender, args) {
            hideLoading();
        }
    </script>

</asp:Content>

