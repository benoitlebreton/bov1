﻿<%@ Page Title="Informations inscription - BO Compte-Nickel" Language="C#" AutoEventWireup="true" CodeFile="RegistrationDetails.aspx.cs" Inherits="RegistrationDetails" MasterPageFile="~/Site.master" %>
<%--<%@ Register Namespace="CuteWebUI" Assembly="CuteWebUI.AjaxUploader" TagPrefix="CuteWebUI" %>--%>
<%@ Register Src="~/API/OnfidoResult.ascx" TagPrefix="asp" TagName="OnfidoResult" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link href="Styles/OnfidoResult.css" type="text/css" rel="Stylesheet" />
    
    
    <script src="Scripts/jQueryRotateCompressed.js" type="text/javascript" ></script>
    <style type="text/css">
        .divZoom {
            width: 300px;
            height: 200px;
            cursor: move;
            margin-bottom: 0;
            overflow: hidden; /*keep map contents from spilling over if JS is disabled*/
            border: 2px solid #e8571d;
        }
    </style>

    <script src="Scripts/jQuery.FileUpload/vendor/jquery.ui.widget.js" type="text/javascript"></script>
    <script src="Scripts/jQuery.FileUpload/jquery.iframe-transport.js" type="text/javascript"></script>
    <script src="Scripts/jQuery.FileUpload/jquery.fileupload.js" type="text/javascript"></script>

    <script type="text/javascript">
        (function ($) {
            $.widget("ui.combobox", {
                _create: function () {
                    var input,
                        that = this,
                        wasOpen = false,
                        select = this.element.hide(),
                        selected = select.children(":selected"),
                        value = selected.val() ? selected.text() : "",
                        wrapper = this.wrapper = $("<span>")
                        .addClass("ui-combobox")
                        .insertAfter(select);
                    function removeIfInvalid(element) {
                        var value = $(element).val(),
                            matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex(value) + "$", "i"),
                            valid = false;
                        select.children("option").each(function () {
                            if ($(this).text().match(matcher)) {
                                this.selected = valid = true;
                                return false;
                            }
                        });
                        if (!valid) {
                            // remove invalid value, as it didn't match anything
                            $(element).val("").attr("title", value + " n'existe pas").tooltip("open");
                            select.val("");
                            setTimeout(function () {
                                input.tooltip("close").attr("title", "");
                            }, 2500);
                            input.data("ui-autocomplete").term = "";
                        }
                    }
                    input = $("<input>")
                        .appendTo(wrapper)
                        .val(value)
                        .attr("title", "")
                        .addClass("ui-state-default ui-combobox-input")
                        .autocomplete({
                            delay: 0,
                            minLength: 0,
                            source: function (request, response) {
                                var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
                                response(select.children("option").map(function () {
                                    var text = $(this).text();
                                    if (this.value && (!request.term || matcher.test(text)))
                                        return {
                                            label: text.replace(
                                                new RegExp(
                                                "(?![^&;]+;)(?!<[^<>]*)(" +
                                                $.ui.autocomplete.escapeRegex(request.term) +
                                                ")(?![^<>]*>)(?![^&;]+;)", "gi"
                                                ), "<strong>$1</strong>"),
                                            value: text,
                                            option: this
                                        };
                                }));
                            },
                            select: function (event, ui) {
                                ui.item.option.selected = true;
                                that._trigger("selected", event, {
                                    item: ui.item.option
                                });
                            },
                            change: function (event, ui) {
                                if (!ui.item) {
                                    removeIfInvalid(this);
                                }
                            }
                        })
                        .addClass("ui-widget ui-widget-content ui-corner-left");
                    input.data("ui-autocomplete")._renderItem = function (ul, item) {
                        return $("<li>")
                        .append("<a>" + item.label + "</a>")
                        .appendTo(ul);
                    };
                    $("<a>")
                        .attr("tabIndex", -1)
                        .attr("title", "Afficher tout")
                        .tooltip()
                        .appendTo(wrapper)
                        .button(
                            {
                                icons: {
                                    primary: "ui-icon-triangle-1-s"
                                },
                                text: false
                            }
                        )
                        .removeClass("ui-corner-all")
                        .addClass("ui-corner-right ui-combobox-toggle")
                        .mousedown(function () {
                            wasOpen = input.autocomplete("widget").is(":visible");
                        })
                        .click(function () {
                            input.focus();
                            // close if already visible
                            if (wasOpen) {
                                return;
                            }
                            // pass empty string as value to search for, displaying all results
                            input.autocomplete("search", "");
                        });
                    input.tooltip({
                        tooltipClass: "ui-state-highlight"
                    });
                },
                _destroy: function () {
                    this.wrapper.remove();
                    this.element.show();
                }
            });

            hideLoading();

        })(jQuery);

        function showOcrCheck(cssClass, imgUrl, altText){
            $("." + cssClass.trim()).each(function () {
                var block = $(this);
                var _altText = "";

                if(altText != null && altText.trim().length > 0)
                    _altText = altText;

                if (imgUrl.length > 0) {
                    block.parent().after('<div class="cell" style="text-align:top; padding-left:2px; padding-bottom:8px"><img class="checkInputImg" alt="' + _altText + '" src="Styles/Img/' + imgUrl + '" style="width:30px;position:relative;top:6px" /></div>');
                }
            });
        }

        function initTooltip() {
            $('.cell').tooltip({
                position: {
                    my: "center bottom-10",
                    at: "center top",
                    using: function (position, feedback) {
                        $(this).css(position);
                        $("<div>")
                            .addClass("arrow")
                            .addClass(feedback.vertical)
                            .addClass(feedback.horizontal)
                            .appendTo(this);
                    }
                }
            });

            $('.checkInputImg').tooltip({
                items: "img[alt]",
                content: function () { console.log($(this).attr("alt")); return $(this).attr("alt") } ,
                position: {
                    my: "center bottom-10",
                    at: "center top",
                    using: function (position, feedback) {
                        $(this).css(position);
                        $("<div>")
                            .addClass("arrow")
                            .addClass(feedback.vertical)
                            .addClass(feedback.horizontal)
                            .appendTo(this);
                    }
                }
            });

            $('.tooltip-image').tooltip({
                items: "img[alt]",
                content: function () { console.log($(this).attr("alt")); return $(this).attr("alt") } ,
                position: {
                    my: "center bottom-10",
                    at: "center top",
                    using: function (position, feedback) {
                        $(this).css(position);
                        $("<div>")
                            .addClass("arrow")
                            .addClass(feedback.vertical)
                            .addClass(feedback.horizontal)
                            .appendTo(this);
                    }
                }
            });
        }

        function onClickChecked() {
            $('#divSetNoComplianceList').hide();
            if($('#<%=ddlSubscriptionCheckStatus.ClientID%>').val() == "R")
                $('#divSetNoComplianceList').show();

            $("#dialog-confirmSubscription").dialog({
                resizable: false,
                width: 700,
                modal: true,
                title: "Vérification de l'inscription",
                buttons: {
                    "Annuler": function () { $(this).dialog("close"); },
                    "Valider": function () {
                        if (CheckedValuesValidation()) {
                            $(this).dialog("close");
                            $('#<%=btnRegistrationChecked.ClientID%>').click();
                            //__doPostBack("__Page", "SubscriptionChecked;" +
                            //    $('#<%= ddlSubscriptionCheckStatus.ClientID %>').val() + ";" +
                            //$('#<%= txtSubscriptionCheckComment.ClientID %>').val());
                        }
                    }
                }
            });
            $("#dialog-confirmSubscription").parent().appendTo(jQuery("form:first"));
        }

        function onClickRectified(){
            $('#<%= ddlSubscriptionCheckStatus.ClientID %> ').val('G').attr('selected','selected').attr('disabled','disabled');
            $("#dialog-confirmSubscription").dialog({
                resizable: false,
                width: 700,
                modal: true,
                title: "Vérification de l'inscription",
                buttons: {
                    "Annuler": function () { $(this).dialog("close"); },
                    "Valider": function () {
                        if (CheckedValuesValidation()) {

                            var comment = $('#<%= txtSubscriptionCheckComment.ClientID %>').val();

                            if($('#<%= lblSubscriptionCheckCommentHistory.ClientID %>').html().trim().length > 0) {
                                comment = $('#<%= lblSubscriptionCheckCommentHistory.ClientID %>').html().trim() + " [" + comment + "]";
                            }

                            //alert(comment);
                            $(this).dialog("close");
                            //__doPostBack("__Page", "SubscriptionChecked;G;" +comment);
                            $('#<%=btnRegistrationRectified.ClientID%>').click();

                        }
                    }
                }
            });
            $("#dialog-confirmSubscription").parent().appendTo(jQuery("form:first"));
        }

        function alertMessage(title, message) {
            $('#lblMessage').html(message);
            $("#dialog-alertMessage").dialog({
                resizable: false,
                width: 700,
                modal: true,
                title: title,
                buttons: {
                    "Fermer": function () { $(this).dialog("close"); }
                }
            });
        }

        function confirmMessage(title, message, btn) {
            $('#lblMessage').html(message);
            $("#dialog-alertMessage").dialog({
                resizable: false,
                width: 700,
                modal: true,
                title: title,
                buttons: {
                    "NON": function () { $(this).dialog("close"); return false;},
                    "OUI": function () { $(this).dialog("close"); $(btn).click();}
                }
            });
        }

        function CheckedValuesValidation() {
            isOK = false;

            $('#lblSetNoComplianceEmpty').css('visibility','hidden');
            $('#reqSubscriptionStatus').css('visibility', 'hidden');
            if ($('#<%= ddlSubscriptionCheckStatus.ClientID %>').val().length == 0) {
                $('#reqSubscriptionStatus').css('visibility','visible');
            }
            else {

                if($('#<%=ddlSubscriptionCheckStatus.ClientID%>').val() == "R")
                {
                    list = '';
                    $('.SetNoComplianceCbList :checked').each(
                        function() {
                            list += $(this).val();
                        }
                    );
                    //alert(list.trim());
                    if(list.trim().length > 0)
                        isOK = true;
                    else
                        $('#lblSetNoComplianceEmpty').css('visibility','visible');

                }
                else
                    isOK = true;
            }

            //isOK = false;

            return isOK;
        }

        function MaxLength(text, maxlength) {
            //asp.net textarea maxlength doesnt work; do it by hand
            //var maxlength = 2000; //set your value here (or add a parm and pass it in)
            if(maxlength == null) {
                maxlength = $('#<%=lblNbMaxCheckComment.ClientID%>').html();
            }

            var object = document.getElementById(text.id)  //get your object
            if (object.value.length > maxlength) {
                object.focus(); //set focus to prevent jumping
                object.value = text.value.substring(0, maxlength); //truncate the value
                object.scrollTop = object.scrollHeight; //scroll to the end to prevent jumping
                $('#lblNbChar').css('color', '#FF0000');
                return false;
            }
            else if (object.value.length == maxlength) {
                $('#lblNbChar').css('color', '#FF0000');
            }
            else {
                $('#lblNbChar').css('color', '#344B56');
            }

            $('#lblNbChar').html(object.value.length);
            return true;
        }

        function RadioButtonClassPeriod() {
        };

        function date_picker() {
        }

        $(function () {
            //slidePanel("AutoCheckResult");
            slidePanel("Id");
            slidePanel("Ho");
            slidePanel("Fb");
            slidePanel("Kyc");
            slidePanel("InfoSup");
            slidePanel("Cgv");
            slidePanel("PinBySms");
            slidePanel("WebPassword");
            slidePanel("RegistrationAttempts");
            slidePanel("Pdf");
            slidePanel("RegistrationStatus");

            mapIdObjects();
            var HoPath = $('#<%=hfHoPath.ClientID %>').val();
            mapObject("Ho", HoPath);

            //PARENT
            //console.log($('#<%=divIDParent.ClientID%>'));
            //console.log($('#<%=divIDParent.ClientID%>').is(':visible'));
            //console.log($('#<%=hfParentFaceCapturePath.ClientID%>').val());

            if($('#<%=divIDParent.ClientID%>') != null && $('#<%=divIDParent.ClientID%>').is(':visible')) {
                var IdParentRectoPath = $('#<%=hfIdParentRectoPath.ClientID %>').val();
                var IdParentVersoPath = $('#<%=hfIdParentVersoPath.ClientID %>').val();
                var FB1Path = $('#<%=hfFB1Path.ClientID%>').val();
                var FB2Path = $('#<%=hfFB2Path.ClientID%>').val();
                var ParentFaceCapturePath = $('#<%=hfParentFaceCapturePath.ClientID%>').val();
                slidePanel("IdParent");
                slidePanel("ParentFC");
                mapObject("IdParentRecto", IdParentRectoPath);
                mapObject("IdParentVerso", IdParentVersoPath);
                mapObject("FB1", FB1Path);
                mapObject("FB2", FB2Path);
                mapObject("ParentFaceCapture", ParentFaceCapturePath);
            }
            else {
                var FaceCapture = $('#<%=hfFaceCapturePath.ClientID%>').val();
                slidePanel("FC");
                mapObject("FaceCapture", FaceCapture);
            }

            modifyButtonsManagement();
        });

        function mapIdObjects() {
            var IdRectoPath = $('#<%=hfIdRectoPath.ClientID %>').val();
            var IdVersoPath = $('#<%=hfIdVersoPath.ClientID %>').val();
            mapObject("IdRecto", IdRectoPath);
            mapObject("IdVerso", IdVersoPath);
        }

        function modifyButtonsManagement(){
            if($('#<%=hfRegistrationClosed.ClientID%>').val().trim().toLowerCase() != 'y') {
                var show = true;
                $('.btn-modify').each(function(index) {
                    show = true;
                    var txtID = $(this).attr('associatedID');
                    if($('#'+txtID) != null && $('#'+txtID).hasClass('OcrChecked'))
                        show = false;

                    if(show)
                        $(this).show();
                    else
                        $(this).hide();
                });
            }
            else
                $('.btn-modify').hide();
        }

        function getClientModification() {
            if(!isClientModified) {
                alertMessage("Erreur", "Aucune modification à enregistrer.");
            }
            else {
                confirmMessage("Enregistrer les modifications", "Voulez-vous vraiment modifier les informations ?", $('#<%=btnModifyValidation.ClientID%>'));
            }

            return false;
        }

        function slidePanel(id) {
            if ($('#' + id + 'Panel').is(':visible'))
                $('#' + id + 'Arrow').removeAttr("src").attr('src', './Styles/Img/downArrow.png');
            else
                $('#' + id + 'Arrow').removeAttr("src").attr('src', './Styles/Img/rightArrow.png');

            $('#' + id + 'Title').click(function () {
                if ($('#' + id + 'Panel').is(':visible')) {
                    //alert("visible");
                    $('#' + id + 'Arrow').removeAttr("src").attr('src', './Styles/Img/rightArrow.png');
                    $('#' + id + 'Panel').slideUp();
                }
                else {
                    $('#' + id + 'Panel').slideDown();
                    $('#' + id + 'Arrow').attr('src', './Styles/Img/downArrow.png');
                }
            });
        }

        //function ShowImgLoading(id){
        //}

        function HideImgLoading(id){
            $(id + '-loading').hide();
            $(id).show();
        }

        function mapObject(id, path) {
            if (path != null && path.trim().length == 0) {
                $('#div' + id).hide();
            }
            else {
                $("#img" + id).attr("src", '.' + path)
                //$("#img" + id + "-min").attr("src", '.' + path)

                //ShowImgLoading("#img" + id + "-min");
                //$("#img" + id).one("load", function () {
                    //HideImgLoading("#img" + id + "-min");
                //}).attr("src", '.'+ path);

                $("#img" + id + "-min")
                    .one("load", function () {
                        HideImgLoading("#img" + id + "-min");
                    })
                    .one("error", function () {
                        $("#img" + id + "-min").attr("src", './Styles/Img/img_not_found.png')
                    })
                    .attr("src", '.' + path);

                var map = $("#" + id).mapbox({
                    mousewheel: false,
                    doubleClickZoom: false,
                    layerSplit: 3,
                    mapContent: "#Zoom" + id,
                    defaultLayer: 0,
                    pan: true,
                    zoom: true
                });

                $('#up' + id).click(function () { map.mapbox("up", 80); return false; });
                $('#left' + id).click(function () { map.mapbox("left", 80); return false; });
                $('#right' + id).click(function () { map.mapbox("right", 80); return false; });
                $('#down' + id).click(function () { map.mapbox("down", 80); return false; });
                $('#zoom' + id).click(function () { map.mapbox("zoom"); return false; });
                $('#back' + id).click(function () { map.mapbox("back"); return false; });

                $('#div' + id).mouseover(function () {
                    $('#' + id + 'ImgPanel').show();
                });
                $('#div' + id).mouseout(function () {
                    $('#' + id + 'ImgPanel').hide();
                });
            }
        }

        function clickAttemptDetails() {
            if ($('#panelAttemptDetails').is(':visible')) {
                $('#panelAttemptDetails').slideUp();
                $('#<%=btnAttemptDetails.ClientID %>').val('afficher détails');
            }
            else {
                $('#panelAttemptDetails').slideDown();
                $('#<%=btnAttemptDetails.ClientID %>').val('masquer détails');
            }
        }

        function clickShowOriginalImage(filePath, title) {
            filename = filePath.substr(filePath.lastIndexOf('/')+1, filePath.length-filePath.lastIndexOf('/'))
            filetype = filename.split('.')[1];

            if(filetype.toLowerCase() == "jpg") {
                $('#imgOriginalImage').show().attr('src', filePath);;
                $('#panelButtonOriginalImage').show();
                $('#OriginalImageFrame').hide();
            }
            else {
                $('#imgOriginalImage').hide();
                $('#panelButtonOriginalImage').hide();
                $('#OriginalImageFrame').attr('src', filePath).show();
            }

            $('#dialog-OriginalImage').attr('title', title);
            $('#NoOriginalImage').hide();

            $("#dialog-OriginalImage").dialog({
                resizable: false,
                width: 840,
                modal: true,
                buttons: {
                    "Fermer": function () { $(this).dialog("close"); $(this).dialog("destroy"); }
                }
            });
        }

        function clickShowXML(filePath, title) {

            $('#xmlFrame').attr('src', filePath);
            $('#dialog-XML').attr('title', title);

            $('#navigator').html(navigator.appName);

            var userAgent = navigator.userAgent.toLowerCase();
            jQuery.browser = {
                version: (userAgent.match(/.+(?:rv|it|ra|ie|me)[\/: ]([\d.]+)/) || [])[1],
                chrome: /chrome/.test(userAgent),
                safari: /webkit/.test(userAgent) && !/chrome/.test(userAgent),
                opera: /opera/.test(userAgent),
                msie: /msie/.test(userAgent) && !/opera/.test(userAgent),
                mozilla: /mozilla/.test(userAgent) && !/(compatible|webkit)/.test(userAgent)
            };

            var showChromePluginLink = false;
            $.each($.browser, function (i, val) {
                if (i == 'chrome' && val)
                    showChromePluginLink = true;
            });

            if(showChromePluginLink)
                $("#ChromeXmlPlugin").show();
            else
                $("#ChromeXmlPlugin").hide();

            if (filePath.trim().length == 0) {
                $('#NoXML').show();
                $('#xmlFrame').hide();

                $("#dialog-XML").dialog({
                    resizable: false,
                    width: 400,
                    modal: true,
                    buttons: {
                        "Fermer": function () { $(this).dialog("close"); $(this).dialog("destroy"); }
                    }
                });
            }
            else {
                $('#NoXML').hide();
                $('#xmlFrame').show();

                $("#dialog-XML").dialog({
                    resizable: false,
                    width: 840,
                    modal: true,
                    buttons: {
                        "Fermer": function () { $(this).dialog("close"); $(this).dialog("destroy"); }
                    }
                });
            }
        }

        function clickShowPdf(filePath, title) {

            $('#pdfFrame').attr('src', filePath);
            $('#dialog-PDF').attr('title', title);

            $('#NoPDF').hide();
            $('#pdfFrame').show();

            $("#dialog-PDF").dialog({
                autoOpen: false,
                resizable: false,
                draggable: false,
                modal: true,
                title: "Dossier de souscription",
                width: 850,
                buttons: {
                    "Fermer": function () { $(this).dialog("close"); $(this).dialog("destroy"); }
                }
            });

        }

        function showError() {
            $('#dialog-error').dialog({
                resizable: false,
                width: 800,
                modal: true,
                buttons: {
                    "OK": function () { $(this).dialog("close"); $(this).dialog("destroy"); location.href = "RegistrationSearch.aspx"; }
                }
            });
        }

        function showMpadStepDetails() {
            //alert("afficher étapes MPAD");
            $('#dialogMpadStepsDetails').dialog({
                resizable: false,
                width: 800,
                modal: true,
                title:"Détails du parcours client MPAD",
                buttons: {
                    "Fermer": function () { $(this).dialog("close"); $(this).dialog("destroy"); }
                }
            });
        }

        var isClientModified = false;
        function modifyClientField(btn,id, ddlId) {
            isClientModified = true;
            if(ddlId != null && ddlId.length > 0) {
                $('#'+id).hide();
                $('#'+ddlId).show();
                $(btn).css('visibility','hidden');
            }
            else {
                $('#'+id).unbind('click').addClass('BoModify').focus();
                $(btn).css('visibility','hidden');
            }
        }

        function showValues(){
            //alert($('#<%= txtIDFirstName.ClientID  %>').val());
        }

        function GoToClient(regID) {
            document.location.href = "ClientDetails.aspx?No=" + regID;
        }
        var rotationValue=0;
        function rotateOriginalImage(rotateValue) {
            rotationValue = rotationValue+rotateValue;
            $('#imgOriginalImage').rotate(rotationValue);
        }

        function showForceSubscription() {
            $('#dialog-ForceSubscription').dialog({
                resizable: false,
                width: 600,
                modal: true,
                title:"Forcer la création du compte",
                buttons: {
                    "Annuler": function () { $(this).dialog("close"); $(this).dialog("destroy"); },
                    "Valider": function () {
                        if(checkReqForceSubscription()) {
                            $('#<%= btnValidForceSubscription.ClientID %>').click();
                        }
                    }
                },
            });
            $("#dialog-ForceSubscription").parent().appendTo(jQuery("form:first"));

            return false;
        }

        function checkReqForceSubscription(){
            var isOK = true;
            var trackingNumberID = $('#<%=txtForceSubscriptionTrackingNumber.ClientID%>').attr('id');
            var accountNumberID = $('#<%=txtForceSubscriptionAccountNumber.ClientID%>').attr('id');
            $('#req_'+trackingNumberID).css('visibility','hidden');
            $('#req_'+accountNumberID).css('visibility','hidden');

            if($('#'+trackingNumberID).val().trim().length.toString() != $('#'+trackingNumberID).attr('maxlength').trim()) {
                isOK = false;
                $('#req_'+trackingNumberID).css('visibility','visible');
            }

            if($('#'+accountNumberID).val().trim().length.toString() != $('#'+accountNumberID).attr('maxlength').trim()) {
                isOK = false;
                $('#req_'+accountNumberID).css('visibility','visible');
            }

            return isOK;
        }

        function checkSubscriptionStatus(){
            if($('#<%=ddlSubscriptionCheckStatus.ClientID%>').val() == "R" && !$('#divSetNoComplianceList').is(':visible')) {
                $('#divSetNoComplianceList').slideDown();
            }
            else if($('#divSetNoComplianceList').is(':visible')) {
                $('#divSetNoComplianceList').slideUp();
            }
        }

        function showRedMailDetails() {
            $("#divRedMail-popup").dialog({
                resizable: false,
                width: 700,
                modal: true,
                title:"Liste des inscriptions à la même adresse"
            });
        }

        function showRegistration(sRegistrationCode) {
            if(sRegistrationCode != null && sRegistrationCode.trim().length > 0) {
                window.open("RegistrationDetails.aspx?No="+sRegistrationCode, '_blank')
            }
        }
    </script>
    <script type="text/javascript">
        var tryAgain = true;
        var nbTryFindFile;
        function ShowRegistrationFile() {
            $("#dialog-PDF").dialog({
                autoOpen: false,
                resizable: false,
                draggable: false,
                modal: true,
                title: "Dossier de souscription",
                width: 850,
                buttons: {
                    "Fermer": function () { $(this).dialog("close"); $(this).dialog("destroy"); }
                }
            });

            nbTryFindFile = 3;
            ShowFile("Dossier de souscription", $('#<%=lblRegistrationSelected.ClientID%>').html());
        }


        function ShowFile(title, file) {
            showLoading();
            if ($('#<%=hdnRegistrationFilePath.ClientID%>').val().trim().length > 0) {
                $.ajax({
                    url: $('#<%=hdnRegistrationFilePath.ClientID%>').val(),
                    success: function (data) {
                        $('#noPDF').hide();
                        $('#pdfFrame').attr('src', $('#<%=hdnRegistrationFilePath.ClientID%>').val());
                        $('#dialog-PDF').dialog({ title: title });
                        $('#dialog-PDF').dialog('open');
                        hideLoading();
                    },
                    error: function (data) {
                        SearchFileOnCloud(title, file);
                    }
                });
            }
            else {
                SearchFileOnCloud(title, file);
            }
        }
        function SearchFileOnCloud(title, file) {
            var sFilePath = "";
            $.ajax({
                url: "./ws_tools.asmx/GetClientOpeningFile",
                data: '{ "sFileName": "' + file + '", "sRegistrationCode": "'+$('#<%=lblRegistrationSelected.ClientID%>').html() +'"}', //, culture: 'fr-FR', order: 'ASC'
                dataType: "json",
                type: "POST",
                cache: false,
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    //var BoolData = (data == "True") ? true : false;
                    //console.log(data);
                    sFilePath = data.d.toString().trim();
                    var isOK = false;
                    if (sFilePath.length > 0) {
                        ShowFileGetted(title, sFilePath);
                    }
                    else {
                        $('#dialog-PDF').dialog({ title: title });
                        $('#dialog-PDF').dialog('open');
                        $('#noPDF').show();
                        $('#pdfFrame').hide();
                        hideLoading();
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    console.log(XMLHttpRequest);
                    console.log(textStatus);
                    console.log(errorThrown);
                    ShowFileGetted(title, sFilePath);
                }
            });
        }

        function ShowFileGetted(title, file) {
            if (file.indexOf(';') != -1) {
                var isOK = false;
                for (var i = 0; i < file.split(';') ; i++) {
                    if (!isOK) {
                        $.get(file[i]).done(function () {
                            isOK = true;
                            $('#noPDF').hide();
                            $('#pdfFrame').show();
                            $('#pdfFrame').attr('src', file);
                            $('#dialog-PDF').dialog({ title: title });
                            $('#dialog-PDF').dialog('open');
                            hideLoading();
                        }).fail(function () {
                            isOK = false;
                        });
                    }
                }

                if (!isOK) {
                    nbTryFindFile--;
                    if (nbTryFindFile > 0) {
                        setTimeout(function () { ShowFileGetted(title, file); }, 2000);
                    }
                    else {
                        if(tryAgain) {
                            tryAgain = false;
                            ShowRegistrationFile();
                        }
                        else {
                            $('#noPDF').show();
                            $('#pdfFrame').hide();
                            $('#dialog-PDF').dialog({ title: title });
                            $('#dialog-PDF').dialog('open');
                            hideLoading();
                        }
                    }
                }
            }
            else {
                $.get(file).done(function () {
                    $('#noPDF').hide();
                    $('#pdfFrame').show();
                    $('#pdfFrame').attr('src', file);
                    $('#dialog-PDF').dialog({ title: title });
                    $('#dialog-PDF').dialog('open');
                    hideLoading();
                }).fail(function () {
                    nbTryFindFile--;
                    if (nbTryFindFile > 0) {
                        setTimeout(function () { ShowFileGetted(title, file); }, 2000);
                    }
                    else {
                        if(tryAgain) {
                            tryAgain = false;
                            ShowRegistrationFile();
                        }
                        else {
                            $('#noPDF').show();
                            $('#pdfFrame').hide();
                            $('#dialog-PDF').dialog({ title: title });
                            $('#dialog-PDF').dialog('open');
                            hideLoading();
                        }
                    }
                });

            }
            return isOK;
        }

        function ShowPanelLoading(panelID) {
            console.log(panelID);
            var panel = $('#'+panelID);
            if(panelID.trim().length > 0 && panel != null){

                $('.panel-loading').show();
                $('.panel-loading').width(panel.outerWidth());
                $('.panel-loading').height(panel.outerHeight());

                $('.panel-loading').position({
                    my: 'center',
                    at: 'center',
                    of: panel
                });
            }
        }
        function HidePanelLoading(){
            $('.panel-loading').hide();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div style="position: relative">
        <div style="position: absolute; top: 142px">
            <asp:Button ID="btnPrevious_2" runat="server" Text="Recherche inscription" OnClick="onClickPreviousButton" CssClass="button" Visible="false" />
        </div>
    </div>
    <div style="margin-bottom: 20px" id="divRegistrationDetails" runat="server" visible="false">
        <div style="position:relative">
            <asp:Button ID="btnPrevious_1" runat="server" Text="Recherche inscription" OnClick="onClickPreviousButton" CssClass="button" Visible="true" />
            <div id="panelBtnShowClient" runat="server" style="position:absolute;right:0;top:10px">
                <input id="btnShowClient" runat="server" type="button" value="accéder à la fiche du client" class="MiniButton" style="text-align:center; width:200px;" />
            </div>
        </div>
        <div style="position:relative">
            <div style="position:absolute; top:0; right:0;">
                <asp:Image ID="imgRegistrationHW" runat="server" ImageUrl="~/Styles/Img/borne.png" style="max-height:45px;" CssClass="tooltip-image" />
            </div>
        </div>
        <div class="table" style="width:100%">
            <div class="row">
                <div class="cell" style="vertical-align:middle;">
                    <h2 style="color: #344b56; margin-bottom: 10px; margin-top:5px; white-space:nowrap">
                        PACK N° <asp:Label ID="lblPackNumberSelected" runat="server"></asp:Label> -
                        INSCRIPTION N° <asp:Label ID="lblRegistrationSelected" runat="server"></asp:Label>
                    </h2>
                </div>
                <div class="cell" style="padding-left:20px; vertical-align:middle;width:100%">
                    <asp:Panel ID="panelRedMail" CssClass="redMail" runat="server" Visible="false">
                        <img alt="" src="./Styles/Img/red-mail.png" style="height:30px;" onclick="showRedMailDetails();" />

                        <div id="divRedMail-popup" style="display:none">
                            <div style="max-height:300px; overflow:auto">
                                <asp:Repeater ID="rptRedMail" runat="server">
                                    <HeaderTemplate>
                                        <table class="tableOrange">
                                            <tr>
                                                <th>N&deg; inscription</th>
                                                <th>Nom</th>
                                                <th>Prénom</th>
                                                <th>Date création</th>
                                                <th>Profil AML</th>
                                            </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr class='<%# Container.ItemIndex % 2 == 0 ? "aml-table-row" : "aml-table-alternate-row" %>' onclick='showRegistration("<%# Eval("RegistrationCode")%>")'>
                                            <td><%# Eval("RegistrationCode")%></td>
                                            <td><%# Eval("LastName")%></td>
                                            <td><%# Eval("FirstName")%></td>
                                            <td><%# Eval("CreationDate")%></td>
                                            <td style="text-align:center"><%# Eval("AMLProfile")%></td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
            </div>
        </div>

        <div style="margin: -5px 0 30px 0; width: 100%;">
            <div style="display: table; width: 100%;">
                <div style="display: table-row">
                    <div id="panelSubscriptionPhoneNumber" runat="server" style="display: table-cell; width: 20%">
                        <div class="label">Mobile client</div>
                        <div>
                            <asp:Label ID="lblRegistrationPhoneNumber" runat="server" Style="font-size: 16px"></asp:Label>
                        </div>
                    </div>
                    <div id="panelSubscriptionDate" runat="server" style="display: table-cell; width: 20%">
                        <div class="label">Inscrit le</div>
                        <div>
                            <asp:Label ID="lblRegistrationDate" runat="server" Style="font-size: 16px"></asp:Label>
                        </div>
                    </div>
                    <div id="panelSubscriptionMpadTime" runat="server" style="display: table-cell; width: 20%">
                        <div class="label">sur <asp:Label ID="lblRegistrationHW" runat="server">Borne</asp:Label> en</div>
                        <div style="display: table; padding: 5px">
                            <div style="display: table-row">
                                <div style="display: table-cell">
                                    <asp:Label ID="lblRegistrationMpadTime" runat="server" Style="font-size: 16px"></asp:Label>
                                </div>
                                <div style="display: table-cell; padding-left: 10px">
                                    <input type="button" class="MiniButton" style="width: 80px" value="détails" onclick="showMpadStepDetails();" />
                                </div>
                            </div>

                            <div id="dialogMpadStepsDetails" style="display: none; width: 850px">
                                <div style="width: 100%; background-color: #344B56;">
                                    <div style="display: table; width: 98%; color: #FFF; font-weight: bold">
                                        <div style="display: table-row">
                                            <div style="display: table-cell; width: 55%; text-align: center; vertical-align: middle; padding: 5px">Etape</div>
                                            <div style="display: table-cell; width: 23%; text-align: center; vertical-align: middle; padding: 5px">Date</div>
                                            <div style="display: table-cell; width: 11%; text-align: center; vertical-align: middle; padding: 5px">Temps écoulé</div>
                                            <div style="display: table-cell; width: 11%; text-align: center; vertical-align: middle; padding: 5px">Durée</div>
                                        </div>
                                    </div>
                                </div>
                                <div style="overflow: auto; max-height: 400px; border: #344B56 solid 2px;">
                                    <asp:Repeater ID="rMpadStepsDetails" runat="server">
                                        <HeaderTemplate>
                                            <div style="display: table; width: 100%;">
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div style="display: table-row">
                                                <div style="display: table-cell; width: 55%; vertical-align: middle; padding: 5px">
                                                    <asp:Label runat="server" ID="lblStepLabel" Text='<%# Eval("StepLabel") %>'></asp:Label>
                                                </div>
                                                <div style="display: table-cell; width: 23%; vertical-align: middle; padding: 5px">
                                                    <asp:Label runat="server" ID="lblStepDate" Text='<%# Eval("StepDate") %>'></asp:Label>
                                                </div>
                                                <div style="display: table-cell; width: 11%; text-align: right; vertical-align: middle; padding: 5px">
                                                    <asp:Label runat="server" ID="lblDurationFromStart" Text='<%# getDurationFormat(Eval("DurationFromStart").ToString()) %>'></asp:Label>
                                                </div>
                                                <div style="display: table-cell; width: 11%; text-align: right; vertical-align: middle; padding: 5px">
                                                    <asp:Label runat="server" ID="lblStepDuration" Text='<%# getDurationFormat(Eval("StepDuration").ToString()) %>'></asp:Label>
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                        <AlternatingItemTemplate>
                                            <div style="display: table-row">
                                                <div style="display: table-cell; width: 55%; padding: 5px; background-color: #F7CFAF; vertical-align: middle">
                                                    <asp:Label runat="server" ID="lblStepLabel" Text='<%# Eval("StepLabel") %>'></asp:Label>
                                                </div>
                                                <div style="display: table-cell; width: 23%; padding: 5px; background-color: #F7CFAF; vertical-align: middle">
                                                    <asp:Label runat="server" ID="lblStepDate" Text='<%# Eval("StepDate") %>'></asp:Label>
                                                </div>
                                                <div style="display: table-cell; width: 11%; text-align: right; padding: 5px; background-color: #F7CFAF; vertical-align: middle">
                                                    <asp:Label runat="server" ID="lblDurationFromStart" Text='<%# getDurationFormat(Eval("DurationFromStart").ToString()) %>'></asp:Label>
                                                </div>
                                                <div style="display: table-cell; width: 11%; text-align: right; padding: 5px; background-color: #F7CFAF; vertical-align: middle">
                                                    <asp:Label runat="server" ID="lblStepDuration" Text='<%# getDurationFormat(Eval("StepDuration").ToString()) %>'></asp:Label>
                                                </div>
                                            </div>
                                        </AlternatingItemTemplate>
                                        <FooterTemplate>
                                            </div>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div id="panelSubscriptionTpeTime" runat="server" style="display: table-cell; width: 20%">
                        <div class="label">sur le TPE en</div>
                        <div>
                            <asp:Label ID="lblRegistrationTpeTime" runat="server" Style="font-size: 16px"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
            <div id="panelSubscriptionAgency" runat="server" style="width: 100%">
                <div class="label">Chez</div>
                <div class="trimmer" style="width: 800px; margin-top: 0; padding-top: 0">
                    <asp:Label ID="lblRegistrationAgencyName" runat="server" Style="font-size: 16px;"></asp:Label>
                    à <asp:Label ID="lblRegistrationAgencyCity" runat="server" Style="font-size: 16px"></asp:Label>
                </div>
            </div>
            <div id="panelSubscriptionCashier" runat="server" style="width: 100%">
                <div class="label">Vendeur</div>
                <div class="trimmer" style="width: 800px; margin-top: 0; padding-top: 0">
                    <asp:Label ID="lblRegistrationCashierName" runat="server" Style="font-size: 16px;"></asp:Label>
                </div>
            </div>
        </div>

        <div style="display: table; width: 100%; margin-bottom:5px;">
            <div style="display: table-row">
                <div style="display: table-cell; width: 25%; text-align: left; vertical-align: middle" visible="false">
                </div>
                <div style="display: table-cell; width: 40%; text-align: left; color: black; vertical-align: middle">
                    <span class="OcrChecked" style="display: inline; padding: 5px;">informatiquement verifié</span>
                    <span style="display: inline; padding: 5px" class="ClientModification">modifié par le client</span>
                </div>
                <div style="display: table-cell; width:25%; text-align:right">
                    <div>
                        <asp:Button ID="btnValidate" runat="server" Text="Vérifié" OnClientClick="onClickChecked(); return false;" CssClass="button" Visible="false" Style="margin: 0;" />
                        <asp:Button ID="btnRectified" runat="server" Text="Corrigé" OnClientClick="onClickRectified(); return false;" CssClass="button" Visible="false" Style="margin: 0;" />
                    </div>
                </div>
            </div>
        </div>

        <%--<h3 style="padding-left:10px">DOCUMENTS</h3>--%>
        <div style="border: 3px solid #344b56; border-radius: 8px; width: 100%; margin: auto;" class="RegistrationClass">
            <asp:Panel ID="divRegistrationStatus" runat="server" Visible="true">
                <h4 id="RegistrationStatusTitle" class="AccordionTitle">
                    <div style="display: table">
                        <div style="display: table-row">
                            <div style="display: table-cell; vertical-align: middle">
                                <img id="RegistrationStatusArrow" alt="" src="" />
                                Etat vérification
                            </div>
                            <div style="padding-left: 10px; display: table-cell; vertical-align: middle">
                                <asp:Panel ID="panelRegistrationCheckedStatus" runat="server" Visible="false">
                                    <asp:Image ID="imgRegistrationCheckedStatus" runat="server" Style="position: relative; top: 2px" />
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                </h4>
                <div id="RegistrationStatusPanel" style="padding: 0 10px 10px 10px; margin-bottom: 10px; display: none">
                    <div style="display: table; margin-top: 5px; width: 100%">
                        <div style="display: table-row">
                            <div style="display: table-cell; width: 10%; padding-right: 10px">
                                <div style="margin-top: 5px">
                                    <div class="label">
                                        Vérifiée</div>
                                    <div>
                                        <asp:Label ID="lblRegistrationChecked" runat="server" Style="text-transform: uppercase; width: 50px">NON</asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div style="display: table-cell; width: 20%; padding: 0 10px">
                                <asp:Panel ID="panelRegistrationAgentCheckDate" runat="server" Visible="false">
                                    <div class="label">
                                        le</div>
                                    <div>
                                        <asp:Label ID="lblRegistrationCheckedDate" runat="server" Style="width: 150px">NON</asp:Label>
                                    </div>
                                </asp:Panel>
                            </div>
                            <div style="display: table-cell; width: 70%; padding-left: 10px">
                                <asp:Panel ID="panelRegistrationAgentCheck" runat="server" Visible="false">
                                    <div class="label">
                                        par l&#39;agent</div>
                                    <div>
                                        <asp:Label ID="lblRegistrationAgentCheck" runat="server" Style="width: 600px"></asp:Label>
                                    </div>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                    <asp:Panel ID="panelRegistrationChecked" runat="server" Style="margin-top: 20px;" Visible="false">
                        <div style="display: table; width: 100%">
                            <div style="display: table-row">
                            <div style="display: table-cell; width: 30%; padding-right: 10px">
                                <div class="label">
                                    Evaluation</div>
                                <div>
                                    <asp:Label ID="lblRegistrationCheckedStatus" runat="server" Style="text-transform: uppercase; font-weight: bold"></asp:Label>
                                </div>
                            </div>
                            <div style="display: table-cell; width: 70%; padding-left: 10px">
                                <asp:Panel ID="panelRegistrationCheckedComment" runat="server" Visible="false">
                                    <div class="label">
                                        Remarque(s)</div>
                                    <div>
                                        <asp:Label ID="lblRegistrationCheckedComment" runat="server" Height="20px" Style="font-family: din; font-size: 16px; width: 600px;" Visible="false" Width="600px"></asp:Label>
                                        <asp:TextBox ID="txtRegistrationCheckedComment" runat="server" Height="20px" Style="font-family: din; font-size: 16px; max-width: 600px; min-width: 600px; min-height: 20px" TextMode="MultiLine" Width="600px"></asp:TextBox>
                                    </div>
                                </asp:Panel>
                            </div>
                        </div>
                        </div>
                        <asp:Panel ID="panelRegistrationNoCompliance" runat="server" style="width:100%;margin-top:10px;" Visible="false">
                            <asp:Repeater ID="rptGetNoComplianceList" runat="server" OnItemDataBound="rptGetNoComplianceList_ItemDataBound">
                                <HeaderTemplate>
                                    <table cellpadding="0" cellspacing="0" style="width:100%; border:1px solid #e8571d; margin:10px 0; border-collapse:collapse;">
                                        <thead>
                                            <tr style="background-color:#e8571d; color:#FFF;">
                                                <th style="text-align:center;">Non conformité</th>
                                                <th style="text-align:center;">Priorité</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                </HeaderTemplate>
                                <ItemTemplate>
                                            <tr>
                                                <td style="padding:2px 5px;">
                                                    <%# Eval("Compliance") %>
                                                </td>
                                                <td style="padding:2px 5px; text-align:center">
                                                    <%# Eval("Priority") %>
                                                </td>
                                                <td style="padding-left:2px 5px;">
                                                    <asp:HiddenField ID="hfComplianceValue" runat="server" Value='<%# Eval("ComplianceTAG") %>' />
                                                    <asp:CheckBox ID="cbNoCompliance" runat="server" Enabled="false" CssClass="GetNoComplianceCbList" />
                                                </td>
                                            </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                        </tbody>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                            <asp:HiddenField ID="hfNoComplianceSelectedList" runat="server"/>
                        </asp:Panel>
                    </asp:Panel>

                    <div>
                        <asp:OnfidoResult ID="OnfidoResult1" runat="server" />
                    </div>
                </div>
            </asp:Panel>

            <%--<asp:Panel ID="divAutoCheckResult" runat="server" Visible="true">
                <h4 id="AutoCheckResultTitle" class="AccordionTitle">
                    <img id="AutoCheckResultArrow" alt="" src="" />
                    Résultat Onfido
                </h4>
                <div id="AutoCheckResultPanel" style="padding:0 10px 10px 10px;display:none">
                    <asp:OnfidoResult ID="OnfidoResult1" runat="server" />
                </div>
            </asp:Panel>--%>

            <asp:Panel ID="divFC" runat="server" Visible="false">
                <h4 id="FCTitle" class="AccordionTitle">
                    <img id="FCArrow" alt="" src="" />
                    Capture faciale
                </h4>
                <div id="FCPanel" style="padding: 10px; margin-bottom: 10px;">
                    <div id="divFaceCapture" style="margin: 0; padding: 0; width: 100%">
                        <div style="position: relative; float:right">
                        <div id="FaceCapture" class="divZoom" style="margin-bottom: 10px">
                            <div style="width: 100%;">
                                <img id="imgFaceCapture-min-loading" src="Styles/Img/loading.gif" alt="loading..." style="position:absolute;top:80px;left:135px"/>
                                <img id="imgFaceCapture-min" alt="" style="width: 300px" />
                            </div>
                            <div style="height: 650px; width: 900px">
                                <img id="imgFaceCapture" src="" alt="" />
                                <div id="ZoomFaceCapture">
                                </div>
                            </div>
                        </div>
                        <div style="position: absolute; top: -5px; right: -83px; line-height: 1.4em; padding: 5px; z-index: 99; width: 80px; height: 200px;">
                            <div id="FaceCaptureImgPanel" style="display: none; background-color: white; border: 2px solid #e8571d">
                                <div style="margin: auto; display: table;">
                                    <div style="display: table-row">
                                        <div style="display: table-cell; padding: 5px">
                                            <div id="zoomFaceCapture" class="ImageButton zoom" style="">
                                            </div>
                                        </div>
                                        <div style="display: table-cell; padding: 5px">
                                            <div id="backFaceCapture" class="ImageButton back">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style="width: 100%; margin-top: 10px">
                                    <div id="upFaceCapture" class="ImageButton up">
                                    </div>
                                </div>
                                <div style="margin: auto; display: table;">
                                    <div style="display: table-row">
                                        <div style="display: table-cell; padding-right: 20px;">
                                            <div id="leftFaceCapture" class="ImageButton left-arrow">
                                            </div>
                                        </div>
                                        <div style="display: table-cell">
                                            <div id="rightFaceCapture" class="ImageButton right">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style="width: 100%; margin-bottom: 10px">
                                    <div id="downFaceCapture" class="ImageButton down">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div style="clear:both"></div>
                </div>
            </asp:Panel>
            <asp:Panel ID="divID" runat="server" Visible="true">
                <h4 id="IdTitle" class="AccordionTitle">        
                    <img id="IdArrow" alt="" src="" style="float:left" />
                    <asp:UpdatePanel ID="upDocSelect" runat="server">
                        <ContentTemplate>        
                            <asp:Label ID="lblIDDocumentType" runat="server" style="margin-left:5px"></asp:Label>
                            <asp:DropDownList ID="ddlDocs" runat="server" DataTextField="label" DataValueField="index" Visible="false" onclick="event.stopPropagation();" OnSelectedIndexChanged="ddlDocs_SelectedIndexChanged" AutoPostBack="true" style="float:right;margin-right:5px"></asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </h4>
                <div id="IdPanel" style="padding: 10px; margin-bottom: 10px">
                    <asp:UpdatePanel ID="upDocDetails" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="table" style="width:100%">
                        <div class="row">
                            <div class="cell" style="width:70%; vertical-align:top">
                                <div style="display: table; width: 100%">
                                    <div style="display: table-row">
                                        <div style="display: table-cell; vertical-align: top;">
                                            <div style="margin-top: 5px">
                                                <div>
                                                    <label for="<%= txtIDLastName.ClientID%>" class="label"><%=Resources.res.LastName %></label>
                                                </div>
                                                <div class="table">
                                                    <div class="row">
                                                        <div class="cell" style="vertical-align:middle">
                                                            <asp:Label ID="lblIDLastName" runat="server" CssClass="UiTooltip" style="max-width:200px"></asp:Label>
                                                            <asp:TextBox ID="txtIDLastName" runat="server" AutoPostBack="false" autocomplete="off" style="max-width:200px"></asp:TextBox>
                                                        </div>
                                                        <div class="cell" style="vertical-align:middle">
                                                            <img id="btnModify-txtIDLastName" src="./Styles/Img/modify.png" class="btn-modify" height="18" alt="modifier" title="modifier" style="padding: 0 4px"
                                                                onclick="modifyClientField(this,'<%= lblIDLastName.ClientID %>','<%= txtIDLastName.ClientID %>')" />
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div id="panelMarriedLastName" runat="server" visible="false">
                                                <div>
                                                    <label for="<%= txtIDMarriedLastName.ClientID %>" class="label">Nom marital</label>
                                                </div>
                                                <div class="table">
                                                    <div class="row">
                                                        <div class="cell" style="vertical-align:middle">
                                                            <asp:Label ID="lblIDMarriedLastName" runat="server" CssClass="UiTooltip" style="max-width:200px"></asp:Label>
                                                            <asp:TextBox ID="txtIDMarriedLastName" runat="server" autocomplete="off" style="max-width:200px"></asp:TextBox>
                                                        </div>
                                                        <div class="cell" style="vertical-align:middle">
                                                            <img id="btnModify-txtIDMarriedLastName" src="./Styles/Img/modify.png" class="btn-modify" height="18" alt="modifier" title="modifier" style="padding: 0 4px"
                                                                onclick="modifyClientField(this,'<%= lblIDMarriedLastName.ClientID %>','<%= txtIDMarriedLastName.ClientID %>')" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div style="margin-top: 5px">
                                                <div>
                                                    <label for="<%=txtIDFirstName.ClientID %>" class="label"><%=Resources.res.FirstName %></label>
                                                </div>
                                                <div class="table">
                                                    <div class="row">
                                                        <div class="cell" style="vertical-align:middle">
                                                            <asp:Label ID="lblIDFirstName" runat="server" CssClass="UiTooltip" style="max-width:200px"></asp:Label>
                                                            <asp:TextBox ID="txtIDFirstName" runat="server" autocomplete="off" style="max-width:200px"></asp:TextBox>
                                                        </div>
                                                        <div class="cell" style="vertical-align:middle">
                                                            <img id="btnModify-txtIDFirstName" src="./Styles/Img/modify.png" class="btn-modify" height="18" alt="modifier" title="modifier" style="padding: 0 4px"
                                                                onclick="modifyClientField(this,'<%= lblIDFirstName.ClientID %>','<%= txtIDFirstName.ClientID %>')" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div style="margin-top: 5px">
                                                <div>
                                                    <label for="<%=rblGender.ClientID %>" class="label"><%=Resources.res.Gender %></label>
                                                </div>
                                                <div class="table">
                                                    <div class="row">
                                                        <div class="cell" style="vertical-align:middle">
                                                            <asp:Label ID="lblGender" runat="server" Style="width: 20px" CssClass="UiTooltip"></asp:Label>
                                                            <asp:RadioButtonList ID="rblGender" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="radiobuttonlist">
                                                                <asp:ListItem Text="M" Value="M" Selected="True"></asp:ListItem>
                                                                <asp:ListItem Text="F" Value="F"></asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </div>
                                                        <div class="cell" style="vertical-align:middle">
                                                            <img id="btnModify-txtGender" src="./Styles/Img/modify.png" class="btn-modify" height="18" alt="modifier" title="modifier" style="padding: 0 4px"
                                                                onclick="modifyClientField(this,'<%= lblGender.ClientID %>','<%= rblGender.ClientID %>')" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div style="margin-top: 5px">
                                                <div>
                                                    <label for="<%= lblBirthDate.ClientID %>" class="label">
                                                        <%=Resources.res.BirthDate %></label>
                                                </div>
                                                <div class="table">
                                                    <div class="row">
                                                        <div class="cell" style="vertical-align:middle">
                                                            <asp:Label ID="lblBirthDate" runat="server" Style="width: 100px; text-align: center"></asp:Label>
                                                            <asp:TextBox ID="txtBirthDate" runat="server" Style="width: 100px; text-align: center" MaxLength="10" autocomplete="off"></asp:TextBox>
                                                        </div>
                                                        <div class="cell" style="vertical-align:middle">
                                                            <img id="btnModify-txtBirthDate" src="./Styles/Img/modify.png" class="btn-modify" height="18" alt="modifier" title="modifier" style="padding: 0 4px"
                                                                onclick="modifyClientField(this,'<%= lblBirthDate.ClientID %>','<%= txtBirthDate.ClientID %>')" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div style="margin-top: 5px">
                                                <div>
                                                    <label for="<%= lblBirthCity.ClientID %>" class="label">
                                                        <%=Resources.res.BirthCity %></label>
                                                </div>
                                                <div class="table">
                                                    <div class="row">
                                                        <div class="cell" style="vertical-align:middle">
                                                            <asp:Label ID="lblBirthCity" runat="server" CssClass="UiTooltip" style="max-width:200px"></asp:Label>
                                                            <asp:TextBox ID="txtBirthCity" runat="server" autocomplete="off" style="max-width:200px"></asp:TextBox>
                                                        </div>
                                                        <div class="cell" style="vertical-align:middle">
                                                            <img id="btnModify-txtBirthCity" src="./Styles/Img/modify.png" class="btn-modify" height="18" alt="modifier" title="modifier" style="padding: 0 4px"
                                                                onclick="modifyClientField(this,'<%= lblBirthCity.ClientID %>','<%= txtBirthCity.ClientID %>')" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div style="margin-top: 5px">
                                                <div>
                                                    <label for="<%= lblBirthCountry.ClientID%>" class="label">
                                                        <%=Resources.res.BirthCountry %></label>
                                                </div>
                                                <div style="display:block;min-height:20px;">
                                                    <div class="table">
                                                        <div class="row">
                                                            <div class="cell" style="vertical-align:middle">
                                                                <asp:Label ID="lblBirthCountry" runat="server" CssClass="UiTooltip" style="max-width:200px;"></asp:Label>
                                                                <asp:DropDownList ID="ddlBirthCountry" runat="server" DataValueField="ISO2" DataTextField="CountryName" AppendDataBoundItems="true"
                                                                    Style="display: none;" Width="200">
                                                                    <asp:ListItem Text="" Value=""></asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                            <div class="cell" style="vertical-align:middle">
                                                                <img id="btnModify-txtBirthCountry" src="./Styles/Img/modify.png" class="btn-modify" height="18" alt="modifier" title="modifier" style="padding: 0 4px"
                                                                    onclick="modifyClientField(this,'<%= lblBirthCountry.ClientID %>','<%= ddlBirthCountry.ClientID %>')" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="display: table-cell; padding: 0 10px; vertical-align: top;">
                                            <div style="margin-top: 5px; clear: both">
                                                <div>
                                                    <label for="<%= txtDocumentNumber.ClientID%>" class="label"><%=Resources.res.DocumentNumber %></label>
                                                </div>
                                                <div class="table">
                                                    <div class="row">
                                                        <div class="cell" style="vertical-align:middle">
                                                            <asp:Label ID="lblDocumentNumber" runat="server" CssClass="UiTooltip" style="max-width:200px"></asp:Label>
                                                            <asp:TextBox ID="txtDocumentNumber" runat="server" autocomplete="off" style="max-width:200px"></asp:TextBox>
                                                        </div>
                                                        <div class="cell" style="vertical-align:middle">
                                                            <img id="btnModify-txtDocumentNumber" src="./Styles/Img/modify.png" class="btn-modify" height="18" alt="modifier" title="modifier" style="padding: 0 4px"
                                                                onclick="modifyClientField(this,'<%= lblDocumentNumber.ClientID %>', '<%= txtDocumentNumber.ClientID %>')" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="panelExpirationDate" runat="server" style="margin-top: 5px">
                                                <div>
                                                    <label for="<%= txtExpirationDate.ClientID%>" class="label"><%=Resources.res.DateExpiration %></label>
                                                </div>
                                                <div class="table">
                                                    <div class="row">
                                                        <div class="cell" style="vertical-align:middle">
                                                            <asp:Label ID="lblExpirationDate" runat="server" Style="width: 100px; text-align: center"></asp:Label>
                                                            <asp:TextBox ID="txtExpirationDate" runat="server" Style="width: 100px; text-align: center" MaxLength="10" autocomplete="off"></asp:TextBox>
                                                        </div>
                                                        <div class="cell" style="vertical-align:middle">
                                                            <img id="btnModify-txtExpirationDate" src="./Styles/Img/modify.png" class="btn-modify" height="18" alt="modifier" title="modifier" style="padding: 0 4px"
                                                                onclick="modifyClientField(this,'<%= lblExpirationDate.ClientID %>','<%= txtExpirationDate.ClientID %>')" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="divIssuedID" runat="server" style="margin-top: 5px" visible="false">
                                                <div>
                                                    <label for="<%= txtIssuedID.ClientID%>" class="label"><%=Resources.res.Issuedate %></label>
                                                </div>
                                                <div class="table">
                                                    <div class="row">
                                                        <div class="cell" style="vertical-align:middle">
                                                            <asp:Label ID="lblIssuedID" runat="server" Style="width: 100px; text-align: center"></asp:Label>
                                                            <asp:TextBox ID="txtIssuedID" runat="server" Style="width: 100px; text-align: center" MaxLength="10" autocomplete="off"></asp:TextBox>
                                                        </div>
                                                        <div class="cell" style="vertical-align:middle">
                                                            <img id="btnModify-txtIssuedID" src="./Styles/Img/modify.png" class="btn-modify" height="18" alt="modifier" title="modifier" style="padding: 0 4px"
                                                                onclick="modifyClientField(this,'<%= lblIssuedID.ClientID %>', '<%=txtIssuedID.ClientID %>')" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div style="margin-top: 5px">
                                                <div>
                                                    <label for="<%= ddlDocumentCountry.ClientID%>" class="label">
                                                        <%=Resources.res.Country %></label>
                                                </div>
                                                <div class="table">
                                                    <div class="row">
                                                        <div class="cell" style="vertical-align:middle">
                                                            <asp:Label ID="lblDocumentCountry" runat="server" CssClass="UiTooltip" style="max-width:200px"></asp:Label>
                                                            <asp:DropDownList ID="ddlDocumentCountry" runat="server" DataTextField="CountryName"
                                                                DataValueField="ISO2" AppendDataBoundItems="true" Width="200" Style="display: none">
                                                                <asp:ListItem Text="" Value=""></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="cell" style="vertical-align:middle">
                                                            <img id="btnModify-txtDocumentCountry" src="./Styles/Img/modify.png" class="btn-modify" height="18" alt="modifier" title="modifier" style="padding: 0 4px"
                                                                onclick="modifyClientField(this,'<%= lblDocumentCountry.ClientID %>', '<%= ddlDocumentCountry.ClientID %>')" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="divDocState">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style="margin-top: 20px">
                                    <div>
                                        <label class="label">
                                        MRZ</label>
                                    </div>
                                    <div>
                                        <div>
                                            <asp:Label ID="lblMRZ1" runat="server" style=""></asp:Label>
                                        </div>
                                        <div>
                                            <asp:Label ID="lblMRZ2" runat="server" style=""></asp:Label>
                                        </div>
                                        <div>
                                            <asp:Label ID="lblMRZ3" runat="server" style=""></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="display: table-cell; width: 300px; padding: 0;">
                                <div id="divIdRecto" style="margin: 0; padding: 0; width: 100%">
                                    <div style="position: relative;">
                                        <div id="IdRecto" class="divZoom" style="margin-bottom: 10px">
                                            <div style="width: 100%;">
                                                <img id="imgIdRecto-min-loading" src="Styles/Img/loading.gif" alt="loading..." style="position:absolute;top:80px;left:135px"/>
                                                <img id="imgIdRecto-min" alt="" style="width: 300px" />
                                            </div>
                                            <div style="height: 650px; width: 900px">
                                                <img id="imgIdRecto" src="" alt="" />
                                                <div id="ZoomIdRecto">
                                                </div>
                                            </div>
                                        </div>
                                        <div style="position: absolute; top: -5px; right: -83px; line-height: 1.4em; padding: 5px; z-index: 99; width: 80px; height: 200px;">
                                            <div id="IdRectoImgPanel" style="display: none; background-color: white; border: 2px solid #e8571d">
                                                <div style="margin: auto; display: table;">
                                                    <div style="display: table-row">
                                                        <div style="display: table-cell; padding: 5px">
                                                            <div id="zoomIdRecto" class="ImageButton zoom" style="">
                                                            </div>
                                                        </div>
                                                        <div style="display: table-cell; padding: 5px">
                                                            <div id="backIdRecto" class="ImageButton back">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div style="width: 100%; margin-top: 10px">
                                                    <div id="upIdRecto" class="ImageButton up">
                                                    </div>
                                                </div>
                                                <div style="margin: auto; display: table;">
                                                    <div style="display: table-row">
                                                        <div style="display: table-cell; padding-right: 20px;">
                                                            <div id="leftIdRecto" class="ImageButton left-arrow">
                                                            </div>
                                                        </div>
                                                        <div style="display: table-cell">
                                                            <div id="rightIdRecto" class="ImageButton right">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div style="width: 100%; margin-bottom: 10px">
                                                    <div id="downIdRecto" class="ImageButton down">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <asp:Panel ID="panelUploadRecto" runat="server" Visible="true" style="width:100%; margin:5px 0; text-align:center;">
                                        <input type="button" class="MiniButton" value="Upload RECTO" onclick="showUploadImagesDialog('recto');" />
                                    </asp:Panel>
                                </div>
                                <asp:Panel ID="panelIdVerso" runat="server">
                                    <div style="margin: 0; padding: 0; width: 100%" id="divIdVerso">
                                        <div style="position: relative;">
                                            <div id="IdVerso" class="divZoom">
                                                <div style="width: 100%;">
                                                    <img id="imgIdVerso-min-loading" src="Styles/Img/loading.gif" alt="loading..." style="position:absolute;top:80px;left:135px"/>
                                                    <img id="imgIdVerso-min" alt="" style="width: 300px" />
                                                </div>
                                                <div style="height: 650px; width: 900px">
                                                    <img id="imgIdVerso" src="" alt="" />
                                                    <div id="ZoomIdVerso">
                                                    </div>
                                                </div>
                                            </div>
                                            <div style="position: absolute; top: -5px; right: -83px; line-height: 1.4em; padding: 5px; z-index: 99; width: 80px; height: 200px;">
                                                <div id="IdVersoImgPanel" style="display: none; background-color: white; border: 2px solid #e8571d">
                                                    <div style="margin: auto; display: table;">
                                                        <div style="display: table-row">
                                                            <div style="display: table-cell; padding: 5px">
                                                                <div id="zoomIdVerso" class="ImageButton zoom" style="">
                                                                </div>
                                                            </div>
                                                            <div style="display: table-cell; padding: 5px">
                                                                <div id="backIdVerso" class="ImageButton back">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div style="width: 100%; margin-top: 10px">
                                                        <div id="upIdVerso" class="ImageButton up">
                                                        </div>
                                                    </div>
                                                    <div style="margin: auto; display: table;">
                                                        <div style="display: table-row">
                                                            <div style="display: table-cell; padding-right: 20px;">
                                                                <div id="leftIdVerso" class="ImageButton left-arrow">
                                                                </div>
                                                            </div>
                                                            <div style="display: table-cell">
                                                                <div id="rightIdVerso" class="ImageButton right">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div style="width: 100%; margin-bottom: 10px">
                                                        <div id="downIdVerso" class="ImageButton down">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <asp:Panel ID="panelUploadVerso" runat="server" Visible="true" style="width:100%; margin:5px 0; text-align:center;">
                                            <input type="button" class="MiniButton" value="Upload VERSO" onclick="showUploadImagesDialog('verso');" />
                                        </asp:Panel>
                                    </div>
                                </asp:Panel>

                                <div style="width:100%; text-align:center; margin-top:10px">
                                    <asp:Button ID="btnShowOriginalImage" runat="server" CssClass="MiniButton" Text="Afficher fichier original" OnClientClick="return false;" Visible="false" />
                                    <asp:Button ID="btnGetArchivedImage" runat="server" CssClass="MiniButton" Text="Récupérer fichiers archivés" OnClick="btnGetArchivedImage_Click" OnClientClick="showLoading();" Visible="false" />
                                    <div id="dialog-OriginalImage" title="Image originale" style="display: none">
                                        <div id="panelButtonOriginalImage"  style="display:none;position:relative;height:40px">
                                            <div style="position:absolute;z-index:99">
                                                Rotation :
                                                <input type="button" class="button" value="- 90&deg;" onclick="rotateOriginalImage(-90)" />
                                                <input type="button" class="button" value="+ 90&deg;" onclick="rotateOriginalImage(+90)" />
                                            </div>
                                        </div>
                                        <iframe id="OriginalImageFrame" width="800" height="400"></iframe>
                                        <div style="max-height:370px; max-width:800px; overflow:auto">
                                            <img id="imgOriginalImage" alt="" style="display:none;width:1000px" src="" />
                                        </div>
                                        <div id="noOriginalImage" style="display: none; font-weight: bold">Fichier original non récupéré!</div>
                                    </div>
                                </div>
                                <asp:HiddenField ID="hfIdRectoPath" runat="server" />
                                <asp:HiddenField ID="hfIdVersoPath" runat="server" />
                                <asp:HiddenField ID="hfFaceCapturePath" runat="server" />

                                <asp:Panel ID="panelUploadImages" runat="server" Visible="true">
                                    <asp:HiddenField ID="hfUploadFace" runat="server" />
                                    <asp:HiddenField ID="hfUploadFileName" runat="server" />
                                    <div id="dialog-UploadImages" style="display:none">
                                        <asp:UpdatePanel ID="upUploadImages" runat="server">
                                            <ContentTemplate>
                                                <div id="uploadZone" class="table" style="width:100%">
                                                    <div class="row">
                                                        <div class="cell" style="padding:0 10px 0 0">
                                                            <span class="btnUpload btnUpload-success fileinput-button">
                                                                <i class="glyphicon glyphicon-plus"></i>
                                                                <span>Sélectionnez un fichier</span>
                                                                <!-- The file input field used as target for the file upload widget -->
                                                                <input id="fileupload" type="file" name="files[]">
                                                            </span>
                                                            <div style="margin-top:10px;">
                                                                <div id="progress" style="border:1px solid #808080; width:100%">
                                                                    <div class="bar" style="width: 0%;"></div>
                                                                </div>
                                                                <div id="uploadMessage" style="margin-top:5px;"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <asp:Button ID="btnGoogleUpload" runat="server" style="display:none" OnClick="btnGoogleUpload_Click" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                    <script type="text/javascript">
                                        function browseFileToUpload(){
                                            $('#fileupload').click();
                                        }

                                        function closeUploadImagesDialog(){
                                            $("#dialog-UploadImages").dialog('close');
                                            $("#dialog-UploadImages").dialog('destroy');
                                        }
                                        function showUploadImagesDialog(face) {
                                            var urlToUpload = "";
                                            var filenameToUpload = "";
                                            if(face.trim().length > 0)
                                            {
                                                //console.log($('#<%=hfIdRectoPath.ClientID%>').val());
                                                //console.log($('#<%=hfIdVersoPath.ClientID%>').val());
                                                $('#<%=hfUploadFace.ClientID%>').val(face);
                                                try {
                                                    if(face == 'recto') {
                                                        filenameToUpload = $('#<%=hfIdRectoPath.ClientID%>').val().substr($('#<%=hfIdRectoPath.ClientID%>').val().lastIndexOf('/')+1);
                                                    }
                                                    else if(face == 'verso') {
                                                        filenameToUpload = $('#<%=hfIdVersoPath.ClientID%>').val().substr($('#<%=hfIdVersoPath.ClientID%>').val().lastIndexOf('/')+1);
                                                    }
                                                } catch(ex){}
                                                urlToUpload = "Tools/UploadHandler.aspx?filename="+filenameToUpload;
                                                $('#<%=hfUploadFileName.ClientID%>').val(filenameToUpload);
                                                //console.log(filenameToUpload);

                                                $("#dialog-UploadImages").dialog({
                                                    title: "Upload du "+face+" de la pièce d'identité",
                                                    resizable: false,
                                                    width: 900,
                                                    modal: true,
                                                    buttons: {
                                                        "Fermer": function () { $(this).dialog("close"); $(this).dialog("destroy"); }
                                                    }
                                                });
                                                $("#dialog-UploadImages").parent().appendTo(jQuery("form:first"));

                                                initfileupload(urlToUpload);
                                            }
                                        }

                                        function initfileupload(urlToUpload){
                                            $('#fileupload').fileupload({
                                                url : urlToUpload,//'Tools/UploadHandler.aspx?filename='+filenameToUpload,
                                                maxNumberOfFiles : 1,
                                                sequentialUploads: true,
                                                limitConcurrentUploads: 1,
                                                dropZone: $(),
                                                add: function (e, data) {
                                                    var uploadErrors = [];
                                                    var acceptFileTypes = /^image\/(jpeg)$/i;
                                                    if(data.originalFiles[0]['type'].length && !acceptFileTypes.test(data.originalFiles[0]['type'])) {
                                                        uploadErrors.push('Veuillez sélectionner un type de fichier .jpg');
                                                    }
                                                    if(data.originalFiles[0]['size'].length && data.originalFiles[0]['size'] > (1024*1024)) {
                                                        uploadErrors.push('Fichier trop lourd > 1Mo');
                                                    }
                                                    if(uploadErrors.length > 0) {
                                                        showAlertError(uploadErrors.join("\n"));
                                                    } else {
                                                        //console.log(urlToUpload);
                                                        $('#uploadMessage').html("Upload en cours...");
                                                        //data.context = $('<p/>').text('Uploading...').appendTo(document.body);
                                                        data.submit();
                                                    }
                                                },
                                                done: function (e, data) {
                                                    //data.context.text('Upload finished.');
                                                    $('#uploadMessage').html("Upload terminé");
                                                    $('#progress .bar').show().css(
                                                        'width','0%'
                                                    );

                                                    //console.log($("#fileupload"));
                                                    //console.log(data);
                                                    $("#fileupload").find(".files").empty();
                                                    $('#fileupload table tbody tr.template-download').remove();
                                                    //data.files.empty();
                                                    //for(var i=0; i < data.files.length; i++)
                                                        //data.files[i].remove();
                                                    //console.log(data);


                                                    $('#<%=btnGoogleUpload.ClientID%>').click();
                                                    $('#uploadMessage').html("Sauvegarde Google...");
                                                },
                                                progressall: function (e, data) {
                                                    var progress = parseInt(data.loaded / data.total * 100, 10);
                                                    $('#progress .bar').show().css(
                                                        'width',
                                                        progress + '%'
                                                    );
                                                },
                                                cancel : function(e,data){
                                                    console.log('cancel');
                                                }
                                            }).unbind('fileuploadadd').bind('fileuploadadd', function(e, data) {
                                                $(".files tr").remove();
                                            });
                                        }
                                    </script>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </asp:Panel>
            <asp:Panel ID="divIDParent" runat="server" Visible="false">
                <h4 id="IdParentTitle" class="AccordionTitle">
                    <img id="IdParentArrow" alt="" src="" />
                    <asp:Label ID="lblIDParentDocumentType" runat="server"></asp:Label></h4>
                <div id="IdParentPanel" style="padding: 10px; margin-bottom: 10px">
                    <asp:Panel ID="panelDocInfos" runat="server">
                        <div class="table" style="width:100%">
                        <div class="row">
                            <div class="cell" style="width:70%; vertical-align:top">
                                <div style="display: table; width: 100%">
                                    <div style="display: table-row">
                                        <div style="display: table-cell; vertical-align: top;">
                                            <div style="margin-top: 5px">
                                                <div>
                                                    <label for="<%= txtIDLastNameParent.ClientID%>" class="label"><%=Resources.res.LastName %></label>
                                                </div>
                                                <div class="table">
                                                    <div class="row">
                                                        <div class="cell" style="vertical-align:middle">
                                                            <asp:Label ID="lblIDLastNameParent" runat="server" CssClass="UiTooltip" style="max-width:200px"></asp:Label>
                                                            <asp:TextBox ID="txtIDLastNameParent" runat="server" AutoPostBack="false" autocomplete="off" style="max-width:200px"></asp:TextBox>
                                                        </div>
                                                        <div class="cell" style="vertical-align:middle">
                                                            <img id="btnModify-txtIDLastNameParent" src="./Styles/Img/modify.png" class="btn-modify" height="18" alt="modifier" title="modifier" style="padding: 0 4px"
                                                                onclick="modifyClientField(this,'<%= lblIDLastNameParent.ClientID %>','<%= txtIDLastNameParent.ClientID %>')" />
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div id="panelMarriedLastNameParent" runat="server" visible="false">
                                                <div>
                                                    <label for="<%= txtIDMarriedLastNameParent.ClientID %>" class="label">Nom marital</label>
                                                </div>
                                                <div class="table">
                                                    <div class="row">
                                                        <div class="cell" style="vertical-align:middle">
                                                            <asp:Label ID="lblIDMarriedLastNameParent" runat="server" CssClass="UiTooltip" style="max-width:200px"></asp:Label>
                                                            <asp:TextBox ID="txtIDMarriedLastNameParent" runat="server" autocomplete="off" style="max-width:200px"></asp:TextBox>
                                                        </div>
                                                        <div class="cell" style="vertical-align:middle">
                                                            <img id="btnModify-txtMarriedLastNameParent" src="./Styles/Img/modify.png" class="btn-modify" height="18" alt="modifier" title="modifier" style="padding: 0 4px"
                                                                onclick="modifyClientField(this,'<%= lblIDMarriedLastNameParent.ClientID %>','<%= txtIDMarriedLastNameParent.ClientID %>')" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div style="margin-top: 5px">
                                                <div>
                                                    <label for="<%=txtIDFirstNameParent.ClientID %>" class="label"><%=Resources.res.FirstName %></label>
                                                </div>
                                                <div class="table">
                                                    <div class="row">
                                                        <div class="cell" style="vertical-align:middle">
                                                            <asp:Label ID="lblIDFirstNameParent" runat="server" CssClass="UiTooltip" style="max-width:200px"></asp:Label>
                                                            <asp:TextBox ID="txtIDFirstNameParent" runat="server" autocomplete="off" style="max-width:200px"></asp:TextBox>
                                                        </div>
                                                        <div class="cell" style="vertical-align:middle">
                                                            <img id="btnModify-txtIDFirstNameParent" src="./Styles/Img/modify.png" class="btn-modify" height="18" alt="modifier" title="modifier" style="padding: 0 4px"
                                                                onclick="modifyClientField(this,'<%= lblIDFirstNameParent.ClientID %>','<%= txtIDFirstNameParent.ClientID %>')" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div style="margin-top: 5px">
                                                <div>
                                                    <label for="<%=rblGenderParent.ClientID %>" class="label"><%=Resources.res.Gender %></label>
                                                </div>
                                                <div class="table">
                                                    <div class="row">
                                                        <div class="cell" style="vertical-align:middle">
                                                            <asp:Label ID="lblGenderParent" runat="server" Style="width: 20px" CssClass="UiTooltip"></asp:Label>
                                                            <asp:RadioButtonList ID="rblGenderParent" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="radiobuttonlist">
                                                                <asp:ListItem Text="M" Value="M" Selected="True"></asp:ListItem>
                                                                <asp:ListItem Text="F" Value="F"></asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </div>
                                                        <div class="cell" style="vertical-align:middle">
                                                            <img id="btnModify-txtGenderParent" src="./Styles/Img/modify.png" class="btn-modify" height="18" alt="modifier" title="modifier" style="padding: 0 4px"
                                                                onclick="modifyClientField(this,'<%= lblGenderParent.ClientID %>','<%= rblGenderParent.ClientID %>')" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div style="margin-top: 5px">
                                                <div>
                                                    <label for="<%= lblBirthDateParent.ClientID %>" class="label">
                                                        <%=Resources.res.BirthDate %></label>
                                                </div>
                                                <div class="table">
                                                    <div class="row">
                                                        <div class="cell" style="vertical-align:middle">
                                                            <asp:Label ID="lblBirthDateParent" runat="server" Style="width: 100px; text-align: center"></asp:Label>
                                                            <asp:TextBox ID="txtBirthDateParent" runat="server" Style="width: 100px; text-align: center" MaxLength="10" autocomplete="off"></asp:TextBox>
                                                        </div>
                                                        <div class="cell" style="vertical-align:middle">
                                                            <img id="btnModify-txtBirthDateParent" src="./Styles/Img/modify.png" class="btn-modify" height="18" alt="modifier" title="modifier" style="padding: 0 4px"
                                                                onclick="modifyClientField(this,'<%= lblBirthDateParent.ClientID %>','<%= txtBirthDateParent.ClientID %>')" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div style="margin-top: 5px">
                                                <div>
                                                    <label for="<%= lblBirthCityParent.ClientID %>" class="label">
                                                        <%=Resources.res.BirthCity %></label>
                                                </div>
                                                <div class="table">
                                                    <div class="row">
                                                        <div class="cell" style="vertical-align:middle">
                                                            <asp:Label ID="lblBirthCityParent" runat="server" CssClass="UiTooltip" style="max-width:200px"></asp:Label>
                                                            <asp:TextBox ID="txtBirthCityParent" runat="server" autocomplete="off" style="max-width:200px"></asp:TextBox>
                                                        </div>
                                                        <div class="cell" style="vertical-align:middle">
                                                            <img id="btnModify-txtBirthCityParent" src="./Styles/Img/modify.png" class="btn-modify" height="18" alt="modifier" title="modifier" style="padding: 0 4px"
                                                                onclick="modifyClientField(this,'<%= lblBirthCityParent.ClientID %>','<%= txtBirthCityParent.ClientID %>')" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div style="margin-top: 5px">
                                                <div>
                                                    <label for="<%= lblBirthCountryParent.ClientID%>" class="label">
                                                        <%=Resources.res.BirthCountry %></label>
                                                </div>
                                                <div style="display:block;min-height:20px;">
                                                    <div class="table">
                                                        <div class="row">
                                                            <div class="cell" style="vertical-align:middle">
                                                                <asp:Label ID="lblBirthCountryParent" runat="server" CssClass="UiTooltip" style="max-width:200px;"></asp:Label>
                                                                <asp:DropDownList ID="ddlBirthCountryParent" runat="server" DataValueField="ISO2" DataTextField="CountryName" AppendDataBoundItems="true"
                                                                    Style="display: none;" Width="200">
                                                                    <asp:ListItem Text="" Value=""></asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                            <div class="cell" style="vertical-align:middle">
                                                                <img id="btnModify-txtBirthCountryParent" src="./Styles/Img/modify.png" class="btn-modify" height="18" alt="modifier" title="modifier" style="padding: 0 4px"
                                                                    onclick="modifyClientField(this,'<%= lblBirthCountryParent.ClientID %>','<%= ddlBirthCountryParent.ClientID %>')" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="display: table-cell; padding: 0 10px; vertical-align: top;">
                                            <div style="margin-top: 5px; clear: both">
                                                <div>
                                                    <label for="<%= txtDocumentNumberParent.ClientID%>" class="label"><%=Resources.res.DocumentNumber %></label>
                                                </div>
                                                <div class="table">
                                                    <div class="row">
                                                        <div class="cell" style="vertical-align:middle">
                                                            <asp:Label ID="lblDocumentNumberParent" runat="server" CssClass="UiTooltip" style="max-width:200px"></asp:Label>
                                                            <asp:TextBox ID="txtDocumentNumberParent" runat="server" autocomplete="off" style="max-width:200px"></asp:TextBox>
                                                        </div>
                                                        <div class="cell" style="vertical-align:middle">
                                                            <img id="btnModify-txtDocumentNumberParent" src="./Styles/Img/modify.png" class="btn-modify" height="18" alt="modifier" title="modifier" style="padding: 0 4px"
                                                                onclick="modifyClientField(this,'<%= lblDocumentNumberParent.ClientID %>', '<%= txtDocumentNumberParent.ClientID %>')" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="panelExpirationDateParent" runat="server" style="margin-top: 5px">
                                                <div>
                                                    <label for="<%= txtExpirationDateParent.ClientID%>" class="label"><%=Resources.res.DateExpiration %></label>
                                                </div>
                                                <div class="table">
                                                    <div class="row">
                                                        <div class="cell" style="vertical-align:middle">
                                                            <asp:Label ID="lblExpirationDateParent" runat="server" Style="width: 100px; text-align: center"></asp:Label>
                                                            <asp:TextBox ID="txtExpirationDateParent" runat="server" Style="width: 100px; text-align: center" MaxLength="10" autocomplete="off"></asp:TextBox>
                                                        </div>
                                                        <div class="cell" style="vertical-align:middle">
                                                            <img id="btnModify-txtExpirationDateParent" src="./Styles/Img/modify.png" class="btn-modify" height="18" alt="modifier" title="modifier" style="padding: 0 4px"
                                                                onclick="modifyClientField(this,'<%= lblExpirationDateParent.ClientID %>','<%= txtExpirationDateParent.ClientID %>')" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="divIssuedIDParent" runat="server" style="margin-top: 5px" visible="false">
                                                <div>
                                                    <label for="<%= txtIssuedIDParent.ClientID%>" class="label"><%=Resources.res.Issuedate %></label>
                                                </div>
                                                <div class="table">
                                                    <div class="row">
                                                        <div class="cell" style="vertical-align:middle">
                                                            <asp:Label ID="lblIssuedIDParent" runat="server" Style="width: 100px; text-align: center"></asp:Label>
                                                            <asp:TextBox ID="txtIssuedIDParent" runat="server" Style="width: 100px; text-align: center" MaxLength="10" autocomplete="off"></asp:TextBox>
                                                        </div>
                                                        <div class="cell" style="vertical-align:middle">
                                                            <img id="btnModify-txtIssuedIDParent" src="./Styles/Img/modify.png" class="btn-modify" height="18" alt="modifier" title="modifier" style="padding: 0 4px"
                                                                onclick="modifyClientField(this,'<%= lblIssuedIDParent.ClientID %>', '<%=txtIssuedIDParent.ClientID %>')" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div style="margin-top: 5px">
                                                <div>
                                                    <label for="<%= ddlDocumentCountryParent.ClientID%>" class="label">
                                                        <%=Resources.res.Country %></label>
                                                </div>
                                                <div class="table">
                                                    <div class="row">
                                                        <div class="cell" style="vertical-align:middle">
                                                            <asp:Label ID="lblDocumentCountryParent" runat="server" CssClass="UiTooltip" style="max-width:200px"></asp:Label>
                                                            <asp:DropDownList ID="ddlDocumentCountryParent" runat="server" DataTextField="CountryName"
                                                                DataValueField="ISO2" AppendDataBoundItems="true" Width="200" Style="display: none">
                                                                <asp:ListItem Text="" Value=""></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="cell" style="vertical-align:middle">
                                                            <img id="btnModify-txtDocumentCountryParent" src="./Styles/Img/modify.png" class="btn-modify" height="18" alt="modifier" title="modifier" style="padding: 0 4px"
                                                                onclick="modifyClientField(this,'<%= lblDocumentCountryParent.ClientID %>', '<%= ddlDocumentCountryParent.ClientID %>')" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="div7">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style="margin-top: 20px">
                                    <div>
                                        <label class="label">
                                        MRZ</label>
                                    </div>
                                    <div>
                                        <div>
                                            <asp:Label ID="lblMRZ1Parent" runat="server" style=""></asp:Label>
                                        </div>
                                        <div>
                                            <asp:Label ID="lblMRZ2Parent" runat="server" style=""></asp:Label>
                                        </div>
                                        <div>
                                            <asp:Label ID="lblMRZ3Parent" runat="server" style=""></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="display: table-cell; width: 300px; padding: 0;">
                                <div style="margin: 0; padding: 0; width: 100%" id="divIdParentRecto">
                                    <div style="position: relative;">
                                        <div id="IdParentRecto" class="divZoom" style="margin-bottom: 10px">
                                            <div style="width: 100%;">
                                                <img id="imgIdParentRecto-min-loading" src="Styles/Img/loading.gif" alt="loading..." style="position:absolute;top:80px;left:135px"/>
                                                <img id="imgIdParentRecto-min" alt="" style="width: 300px" />
                                            </div>
                                            <div style="height: 650px; width: 900px">
                                                <img id="imgIdParentRecto" src="" alt="" />
                                                <div id="ZoomIdParentRecto"></div>
                                            </div>
                                        </div>
                                        <div style="position: absolute; top: -5px; right: -83px; line-height: 1.4em; padding: 5px; z-index: 99; width: 80px; height: 200px;">
                                            <div id="IdParentRectoImgPanel" style="display: none; background-color: white; border: 2px solid #e8571d">
                                                <div style="margin: auto; display: table;">
                                                    <div style="display: table-row">
                                                        <div style="display: table-cell; padding: 5px">
                                                            <div id="zoomIdParentRecto" class="ImageButton zoom" style="">
                                                            </div>
                                                        </div>
                                                        <div style="display: table-cell; padding: 5px">
                                                            <div id="backIdParentRecto" class="ImageButton back">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div style="width: 100%; margin-top: 10px">
                                                    <div id="upIdParentRecto" class="ImageButton up">
                                                    </div>
                                                </div>
                                                <div style="margin: auto; display: table;">
                                                    <div style="display: table-row">
                                                        <div style="display: table-cell; padding-right: 20px;">
                                                            <div id="leftIdParentRecto" class="ImageButton left-arrow">
                                                            </div>
                                                        </div>
                                                        <div style="display: table-cell">
                                                            <div id="rightIdParentRecto" class="ImageButton right">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div style="width: 100%; margin-bottom: 10px">
                                                    <div id="downIdParentRecto" class="ImageButton down">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <asp:Panel ID="panelIdParentVerso" runat="server">
                                    <div style="margin: 0; padding: 0; width: 100%" id="divIdParentVerso">
                                        <div style="position: relative;">
                                            <div id="IdParentVerso" class="divZoom">
                                                <div style="width: 100%;">
                                                    <img id="imgIdParentVerso-min-loading" src="Styles/Img/loading.gif" alt="loading..." style="position:absolute;top:80px;left:135px"/>
                                                    <img id="imgIdParentVerso-min" alt="" style="width: 300px" />
                                                </div>
                                                <div style="height: 650px; width: 900px">
                                                    <img id="imgIdParentVerso" src="" alt="" />
                                                    <div id="ZoomIdParentVerso">
                                                    </div>
                                                </div>
                                            </div>
                                            <div style="position: absolute; top: -5px; right: -83px; line-height: 1.4em; padding: 5px; z-index: 99; width: 80px; height: 200px;">
                                                <div id="IdParentVersoImgPanel" style="display: none; background-color: white; border: 2px solid #e8571d">
                                                    <div style="margin: auto; display: table;">
                                                        <div style="display: table-row">
                                                            <div style="display: table-cell; padding: 5px">
                                                                <div id="zoomIdParentVerso" class="ImageButton zoom" style="">
                                                                </div>
                                                            </div>
                                                            <div style="display: table-cell; padding: 5px">
                                                                <div id="backIdParentVerso" class="ImageButton back">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div style="width: 100%; margin-top: 10px">
                                                        <div id="upIdParentVerso" class="ImageButton up">
                                                        </div>
                                                    </div>
                                                    <div style="margin: auto; display: table;">
                                                        <div style="display: table-row">
                                                            <div style="display: table-cell; padding-right: 20px;">
                                                                <div id="leftIdParentVerso" class="ImageButton left-arrow">
                                                                </div>
                                                            </div>
                                                            <div style="display: table-cell">
                                                                <div id="rightIdParentVerso" class="ImageButton right">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div style="width: 100%; margin-bottom: 10px">
                                                        <div id="downIdParentVerso" class="ImageButton down">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>

                                <div style="width:100%; text-align:center; margin-top:10px">
                                    <asp:Button ID="btnShowOriginalImageParent" runat="server" CssClass="MiniButton" Text="Afficher fichier original" OnClientClick="return false;" Visible="false" />
                                    <asp:Button ID="btnGetArchivedImageParent" runat="server" CssClass="MiniButton" Text="Récupérer fichiers archivés" OnClick="btnGetArchivedImageParent_Click" OnClientClick="showLoading();" Visible="false" />
                                    <div id="dialog-OriginalImageParent" title="Image originale" style="display: none">
                                        <div id="panelButtonOriginalImageParent"  style="display:none;position:relative;height:40px">
                                            <div style="position:absolute;z-index:99">
                                                Rotation :
                                                <input type="button" class="button" value="- 90&deg;" onclick="rotateOriginalImage(-90)" />
                                                <input type="button" class="button" value="+ 90&deg;" onclick="rotateOriginalImage(+90)" />
                                            </div>
                                        </div>
                                        <iframe id="OriginalImageFrameParent" width="800" height="400">
                                        </iframe>
                                        <div style="max-height:370px; max-width:800px; overflow:auto">
                                            <img id="imgOriginalImageParent" alt="" style="display:none;width:1000px" src="" />
                                        </div>
                                        <div id="noOriginalImageParent" style="display: none; font-weight: bold">Fichier original non récupéré!</div>
                                    </div>
                                </div>
                                <asp:HiddenField ID="hfIdParentRectoPath" runat="server" />
                                <asp:HiddenField ID="hfIdParentVersoPath" runat="server" />

                            </div>
                        </div>
                    </div>
                    </asp:Panel>
                    <asp:Panel ID="panelClientInfos" runat="server" Visible="false">
                        <div class="label">
                            Nom
                        </div>
                        <div>
                            <asp:Label ID="lblParentClientLastName" runat="server"></asp:Label>
                        </div>
                        <div class="label">
                            Prénom
                        </div>
                        <div>
                            <asp:Label ID="lblParentClientFirstName" runat="server"></asp:Label>
                        </div>
                        <div class="label">
                            Sexe
                        </div>
                        <div>
                            <asp:Label ID="lblParentClientGender" runat="server"></asp:Label>
                        </div>
                        <div class="label">
                            Date de Naissance
                        </div>
                        <div>
                            <asp:Label ID="lblParentClientBirthDate" runat="server"></asp:Label>
                        </div>
                    </asp:Panel>
                </div>
            </asp:Panel>
            <asp:HiddenField ID="hfParentFaceCapturePath" runat="server" />

            <asp:Panel ID="divParentFC" runat="server" Visible="false">
                <h4 id="ParentFCTitle" class="AccordionTitle">
                    <img id="ParentFCArrow" alt="" src="" />
                    Capture faciale
                </h4>
                <div id="ParentFCPanel" style="padding: 10px; margin-bottom: 10px;">
                    <div id="divParentFaceCapture" style="margin: 0; padding: 0; width: 100%">
                        <div style="position: relative; float:right">
                        <div id="ParentFaceCapture" class="divZoom" style="margin-bottom: 10px">
                            <div style="width: 100%;">
                                <img id="imgParentFaceCapture-min-loading" src="Styles/Img/loading.gif" alt="loading..." style="position:absolute;top:80px;left:135px"/>
                                <img id="imgParentFaceCapture-min" alt="" style="width: 300px" />
                            </div>
                            <div style="height: 650px; width: 900px">
                                <img id="imgParentFaceCapture" src="" alt="" />
                                <div id="ZoomParentFaceCapture">
                                </div>
                            </div>
                        </div>
                        <div style="position: absolute; top: -5px; right: -83px; line-height: 1.4em; padding: 5px; z-index: 99; width: 80px; height: 200px;">
                            <div id="ParentFaceCaptureImgPanel" style="display: none; background-color: white; border: 2px solid #e8571d">
                                <div style="margin: auto; display: table;">
                                    <div style="display: table-row">
                                        <div style="display: table-cell; padding: 5px">
                                            <div id="zoomParentFaceCapture" class="ImageButton zoom" style="">
                                            </div>
                                        </div>
                                        <div style="display: table-cell; padding: 5px">
                                            <div id="backParentFaceCapturee" class="ImageButton back">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style="width: 100%; margin-top: 10px">
                                    <div id="upParentFaceCapture" class="ImageButton up">
                                    </div>
                                </div>
                                <div style="margin: auto; display: table;">
                                    <div style="display: table-row">
                                        <div style="display: table-cell; padding-right: 20px;">
                                            <div id="leftParentFaceCapture" class="ImageButton left-arrow">
                                            </div>
                                        </div>
                                        <div style="display: table-cell">
                                            <div id="rightParentFaceCapture" class="ImageButton right">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style="width: 100%; margin-bottom: 10px">
                                    <div id="downParentFaceCapture" class="ImageButton down">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div style="clear:both"></div>
                </div>
            </asp:Panel>
            <asp:Panel ID="divFB" runat="server" Visible="false">
                <h4 id="FbTitle" class="AccordionTitle">
                    <img id="FbArrow" alt="" src="" />
                    LIVRET DE FAMILLE</h4>
                <div id="FbPanel" style="padding: 10px; margin-bottom: 10px">
                    <asp:Panel ID="panelFBjpg" runat="server" Visible="false">

                        <div style="margin: 0; padding: 0; width:50%;float:left" id="divFB1">
                            <div style="position: relative;">
                                <div id="FB1" class="divZoom" style="width: 400px;height: 280px;">
                                    <div style="width: 100%;">
                                        <img id="imgFB1-min-loading" src="Styles/Img/loading.gif" alt="loading..." style="position:absolute;top:80px;left:135px"/>
                                        <img id="imgFB1-min" alt="" style="width: 400px" />
                                    </div>
                                    <div style="height: 650px; width: 900px">
                                        <img id="imgFB1" src="" alt="" />
                                        <div id="ZoomFB1">
                                        </div>
                                    </div>
                                </div>
                                <div style="position: absolute; top: -5px; right: -37px; line-height: 1.4em; padding: 5px; z-index: 99; width: 80px; height: 200px;">
                                    <div id="FB1ImgPanel" style="display: none; background-color: white; border: 2px solid #e8571d">
                                        <div style="margin: auto; display: table;">
                                            <div style="display: table-row">
                                                <div style="display: table-cell; padding: 5px">
                                                    <div id="zoomFB1" class="ImageButton zoom" style="">
                                                    </div>
                                                </div>
                                                <div style="display: table-cell; padding: 5px">
                                                    <div id="backFB1" class="ImageButton back">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="width: 100%; margin-top: 10px">
                                            <div id="upFB1" class="ImageButton up">
                                            </div>
                                        </div>
                                        <div style="margin: auto; display: table;">
                                            <div style="display: table-row">
                                                <div style="display: table-cell; padding-right: 20px;">
                                                    <div id="leftFB1" class="ImageButton left-arrow">
                                                    </div>
                                                </div>
                                                <div style="display: table-cell">
                                                    <div id="rightFB1" class="ImageButton right">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="width: 100%; margin-bottom: 10px">
                                            <div id="downFB1" class="ImageButton down">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <asp:Panel ID="panelFB2" runat="server" style="width:50%;float:right">
                            <div style="margin: 0; padding: 0;" id="divFB2">
                                <div style="position: relative;">
                                    <div id="FB2" class="divZoom" style="width: 400px;height: 280px;float:right">
                                        <div style="width: 100%;">
                                            <img id="imgFB2-min-loading" src="Styles/Img/loading.gif" alt="loading..." style="position:absolute;top:80px;left:135px"/>
                                            <img id="imgFB2-min" alt="" style="width: 400px" />
                                        </div>
                                        <div style="height: 650px; width: 900px">
                                            <img id="imgFB2" src="" alt="" />
                                            <div id="ZoomFB2">
                                            </div>
                                        </div>
                                    </div>
                                    <div style="position: absolute; top: -5px; right: -83px; line-height: 1.4em; padding: 5px; z-index: 99; width: 80px; height: 200px;">
                                        <div id="FB2ImgPanel" style="display: none; background-color: white; border: 2px solid #e8571d">
                                            <div style="margin: auto; display: table;">
                                                <div style="display: table-row">
                                                    <div style="display: table-cell; padding: 5px">
                                                        <div id="zoomFB2" class="ImageButton zoom" style="">
                                                        </div>
                                                    </div>
                                                    <div style="display: table-cell; padding: 5px">
                                                        <div id="backFB2" class="ImageButton back">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div style="width: 100%; margin-top: 10px">
                                                <div id="upFB2" class="ImageButton up">
                                                </div>
                                            </div>
                                            <div style="margin: auto; display: table;">
                                                <div style="display: table-row">
                                                    <div style="display: table-cell; padding-right: 20px;">
                                                        <div id="leftFB2" class="ImageButton left-arrow">
                                                        </div>
                                                    </div>
                                                    <div style="display: table-cell">
                                                        <div id="rightFB2" class="ImageButton right">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div style="width: 100%; margin-bottom: 10px">
                                                <div id="downFB2" class="ImageButton down">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                        <div style="clear:both"></div>

                        <asp:HiddenField ID="hfFB1Path" runat="server" />
                        <asp:HiddenField ID="hfFB2Path" runat="server" />

                    </asp:Panel>
                    <asp:Panel ID="panelFBpdf" runat="server" Visible="false">
                        <iframe id="frameFBpdf" runat="server" width="99.5%" height="400"></iframe>
                    </asp:Panel>
                    <asp:Panel ID="panelFBmissing" runat="server" Visible="false" style="text-align:center;color:red;padding:20px 0;">
                        Document indisponible.
                    </asp:Panel>
                </div>

            </asp:Panel>
            <asp:Panel ID="divHO" runat="server" Visible="true">
                <h4 id="HoTitle" class="AccordionTitle">
                    <img id="HoArrow" alt="" src="" />
                    <asp:Label ID="lblHoDocumentType" runat="server"></asp:Label></h4>
                <div id="HoPanel" style="padding: 10px; margin-bottom: 10px">
                    <div style="color: Green; font-weight: bold">
                        Adresse valide
                    </div>
                    <div style="display: table; margin-top: 5px; width: 100%">
                        <div style="display: table-row">
                            <div id="divHoFactInfo" runat="server" style="display: table-cell;" visible="false">
                                <div style="margin-top: 5px">
                                    <div>
                                        <label for="<%= txtTo.ClientID%>" class="label"><%=Resources.res.Receiver2 %></label>
                                    </div>
                                    <div class="table">
                                        <div class="row">
                                            <div class="cell">
                                                <asp:Label ID="lblTo" runat="server"></asp:Label>
                                                <asp:TextBox ID="txtTo" runat="server" TextMode="MultiLine" Style="display: none; min-width: 250px; max-width: 250px; min-height: 20px; height: 20px"></asp:TextBox>
                                            </div>
                                            <div class="cell">
                                                <img id="btnModify-txtTo" src="./Styles/Img/modify.png" class="btn-modify" height="18" alt="modifier" title="modifier" style="padding: 0 4px"
                                                    onclick="modifyClientField(this,'<%= lblTo.ClientID %>','<%= txtTo.ClientID %>')" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style="margin-top: 5px">
                                    <div>
                                        <label for="<%= txtBiller.ClientID %>" class="label"><%=Resources.res.Biller%></label>
                                    </div>
                                    <div class="table">
                                        <div class="row">
                                            <div class="cell">
                                                <asp:Label ID="lblBiller" runat="server"></asp:Label>
                                                <asp:TextBox ID="txtBiller" runat="server" Style="display: none"></asp:TextBox>
                                                <%--<asp:DropDownList ID="ddlBiller" runat="server" DataTextField="Name" DataValueField="refBiller" AppendDataBoundItems="true" CssClass="higherSelect">
                                                            <asp:ListItem Text="" Value=""></asp:ListItem>
                                                        </asp:DropDownList>--%>
                                            </div>
                                            <div class="cell">
                                                <img id="btnModify-txtBiller" src="./Styles/Img/modify.png" class="btn-modify" height="18" alt="modifier" title="modifier" style="padding: 0 4px"
                                                    onclick="modifyClientField(this,'<%= lblBiller.ClientID %>','<%= txtBiller.ClientID %>')" />
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div style="margin-top: 5px">
                                    <div>
                                        <label for="txtIssuedFact" class="label"><%=Resources.res.Issuedate %></label>
                                    </div>
                                    <div class="table">
                                        <div class="row">
                                            <div class="cell">
                                                <asp:Label ID="lblIssuedFact" runat="server"></asp:Label>
                                                <asp:TextBox ID="txtIssuedFact" runat="server" Style="display: none; width: 100px; text-align: center"></asp:TextBox>
                                            </div>
                                            <div class="cell">
                                                <img id="btnModify-txtIssuedFact" src="./Styles/Img/modify.png" class="btn-modify" height="18" alt="modifier" title="modifier" style="padding-right: 2px"
                                                    onclick="modifyClientField(this, '<%= lblIssuedFact.ClientID %>','<%= txtIssuedFact.ClientID %>')" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <%--<div>
                                                <div>
                                                    <asp:Label ID="lblResidentStatus" runat="server" AssociatedControlID="ddlResidentStatus" CssClass="label"><%=Resources.res.ResidentStatus %></asp:Label>
                                                    &nbsp;<span id="ddlResidentStatusRequired" class="required">*</span>
                                                </div>
                                                <div>
                                                    <asp:DropDownList ID="ddlResidentStatus" runat="server" DataTextField="Label" DataValueField="Ref" AppendDataBoundItems="true">
                                                        <asp:ListItem Text="" Value=""></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>--%>
                            </div>
                            <div style="display: table-cell; padding: 0 10px;">
                                <div style="clear: both; margin-top: 5px">
                                    <div>
                                        <label for="<%= txtAddress1.ClientID%>" class="label"><%=Resources.res.Address %> 1</label>
                                    </div>
                                    <div class="table">
                                        <div class="row">
                                            <div class="cell">
                                                <asp:Label ID="lblAddress1" runat="server" CssClass="UiTooltip" style="max-width:450px"></asp:Label>
                                                <asp:TextBox ID="txtAddress1" runat="server" TextMode="MultiLine" Columns="1" Height="20px" Width="450px"
                                                    Style="font-family: din; font-size: 16px; max-width: 250px; min-width: 250px; min-height: 20px;"></asp:TextBox>
                                            </div>
                                            <div class="cell">
                                                <img id="btnModify-txtAddress1" src="./Styles/Img/modify.png" class="btn-modify" height="18" alt="modifier" title="modifier" style="padding: 0 4px"
                                                    onclick="modifyClientField(this,'<%= lblAddress1.ClientID %>','<%= txtAddress1.ClientID %>')" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style="margin-top: 5px">
                                    <div>
                                        <label for="<%= txtAddress2.ClientID%>" class="label"><%=Resources.res.Address %> 2</label>
                                    </div>
                                    <div class="table">
                                        <div class="row">
                                            <div class="cell" style="height:20px">
                                                <asp:Label ID="lblAddress2" runat="server" style="min-width: 450px; max-width: 450px" CssClass="UiTooltip"></asp:Label>
                                                <asp:TextBox ID="txtAddress2" runat="server" TextMode="MultiLine"
                                                    Style="display: none; max-width: 450px; min-width: 450px; min-height: 20px; height: 20px"></asp:TextBox>
                                            </div>
                                            <div class="cell">
                                                <img id="btnModify-txtAddress2" src="./Styles/Img/modify.png" class="btn-modify" height="18" alt="modifier" title="modifier" style="padding: 0 4px"
                                                    onclick="modifyClientField(this,'<%= lblAddress2.ClientID %>','<%= txtAddress2.ClientID %>')" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style="margin-top: 5px">
                                    <div>
                                        <label for="<%= txtZipCode.ClientID%>" class="label"><%=Resources.res.ZipCode %></label>
                                    </div>
                                    <div class="table">
                                        <div class="row">
                                            <div class="cell">
                                                <asp:Label ID="lblZipCode" runat="server" CssClass="UiTooltip" style="width:80px"/>
                                                <%--<asp:DropDownList ID="ddlZipcode" runat="server" DataTextField="Zipcode" DataValueField="Zipcode" AppendDataBoundItems="true">
                                                            <asp:ListItem Text="" Value="" Selected="True"></asp:ListItem>
                                                        </asp:DropDownList>--%>
                                                <asp:TextBox ID="txtZipCode" runat="server" CssClass="zipcode" MaxLength="5" style="width:80px"></asp:TextBox><!--keyboard-num-->
                                            </div>
                                            <div class="cell">
                                                <img id="btnModify-txtZipCode" src="./Styles/Img/modify.png" class="btn-modify" height="18" alt="modifier" title="modifier" style="padding: 0 4px"
                                                    onclick="modifyClientField(this,'<%= lblZipCode.ClientID %>','<%= txtZipCode.ClientID %>')" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="divCityComponent" style="margin-top: 5px">
                                    <asp:Panel ID="panelCityText" runat="server">
                                        <div>
                                            <label for="<%= txtCity.ClientID%>" class="label"><%=Resources.res.City %></label>
                                        </div>
                                        <div class="table">
                                            <div class="row">
                                                <div class="cell">
                                                    <asp:Label ID="lblCity" runat="server" CssClass="UiTooltip" style="width:500px" />
                                                    <asp:TextBox ID="txtCity" runat="server" TextMode="MultiLine" Height="20px"
                                                        Width="250px" Style="font-family: din; font-size: 16px; max-width: 450px; min-width: 450px; min-height: 20px"></asp:TextBox>
                                                </div>
                                                <div class="cell">
                                                    <img id="btnModify-txtCity" src="./Styles/Img/modify.png" class="btn-modify" height="18" alt="modifier" title="modifier" style="padding: 0 4px"
                                                        onclick="modifyClientField(this,'<%= lblCity.ClientID %>','<%= txtCity.ClientID %>')" />
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                    <asp:Panel ID="panelCityDdl" runat="server" Visible="false">
                                        <div>
                                            <label for="<%= ddlCity.ClientID%>" class="label"><%=Resources.res.City %></label>
                                        </div>
                                        <div class="table">
                                            <div class="row">
                                                <div class="cell">
                                                    <asp:Label ID="lblCity2" runat="server" CssClass="UiTooltip" />
                                                    <asp:DropDownList ID="ddlCity" runat="server" DataTextField="City" DataValueField="City">
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="cell">
                                                </div>
                                            </div>

                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                            <div style="display: table-cell; padding-left: 0px; vertical-align: middle; text-align: center; width: 300px">
                                <div style="margin: 0; padding: 0; width: 100%" id="divHo">
                                    <div style="position: relative;">
                                        <div id="Ho" class="divZoom">
                                            <div style="width: 100%;">
                                                <img id="imgHo-min" src="" alt="" style="width: 300px" />
                                            </div>
                                            <div style="height: 650px; width: 900px">
                                                <img id="imgHo" src="" alt="" />
                                                <div id="ZoomHo">
                                                </div>
                                            </div>
                                        </div>
                                        <div style="position: absolute; top: -5px; right: -83px; line-height: 1.4em; padding: 5px; z-index: 99; width: 80px; height: 200px;">
                                            <div id="HoImgPanel" style="display: none; background-color: white; border: 2px solid #e8571d">
                                                <div style="margin: auto; display: table;">
                                                    <div style="display: table-row">
                                                        <div style="display: table-cell; padding: 5px">
                                                            <div id="zoomHo" class="ImageButton zoom" style="">
                                                            </div>
                                                        </div>
                                                        <div style="display: table-cell; padding: 5px">
                                                            <div id="backHo" class="ImageButton back">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div style="width: 100%; margin-top: 10px">
                                                    <div id="upHo" class="ImageButton up">
                                                    </div>
                                                </div>
                                                <div style="margin: auto; display: table;">
                                                    <div style="display: table-row">
                                                        <div style="display: table-cell; padding-right: 20px;">
                                                            <div id="leftHo" class="ImageButton left-arrow">
                                                            </div>
                                                        </div>
                                                        <div style="display: table-cell">
                                                            <div id="rightHo" class="ImageButton right">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div style="width: 100%; margin-bottom: 10px">
                                                    <div id="downHo" class="ImageButton down">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <asp:HiddenField ID="hfHoPath" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="divKYC" runat="server">
                <h4 id="KycTitle" class="AccordionTitle">
                    <img id="KycArrow" alt="" src="" />
                    KYC<span id="closeKYC" style="display: none; font-size: 14px"></span>
                </h4>
                <div id="KycPanel" style="padding: 10px; margin-bottom: 10px; display: none">
                    <asp:Panel ID="panelKYCHolder" runat="server">
                        <div style="display: table">
                            <div style="display: table-row">
                                <asp:Panel ID="panelKYCHolderLeft" runat="server" Style="display: table-cell">
                                </asp:Panel>
                                <asp:Panel ID="panelKYCHolderMiddle" runat="server" Style="display: table-cell; padding-left: 20px">
                                </asp:Panel>
                                <asp:Panel ID="panelKYCHolderRight" runat="server" Style="display: table-cell; padding-left: 20px">
                                </asp:Panel>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
            </asp:Panel>
            <asp:Panel ID="divInfoSup" runat="server">
                <h4 id="InfoSupTitle" class="AccordionTitle">
                    <img id="InfoSupArrow" alt="" src="" />
                    Informations complémentaires <span id="closeInfoSup" style="display: none; font-size: 14px"></span>
                </h4>
                <div id="InfoSupPanel" style="padding: 10px; margin-bottom: 10px; display: none">
                    <div style="display: table; margin-top: 5px">
                        <div style="display: table-row">
                            <div style="display: table-cell">
                                <div style="margin-top: 5px">
                                    <div>
                                        <label for="txtDepositAmount" class="label">
                                            Montant dépôt</label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtDepositAmount" runat="server" ReadOnly="true"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div style="display: table-cell; padding-left: 20px;">
                                <div style="margin-top: 5px">
                                    <div>
                                        <label for="txtPackSaleWithActivation" class="label">
                                            Coffret vendu avec l'activation</label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtPackSaleWithActivation" runat="server" ReadOnly="true"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div style="display: table-cell; padding-left: 20px;">
                                <div style="margin-top: 5px">
                                    <div>
                                        <label for="txtCardNumber" class="label">
                                            Numéro de la carte</label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtCardNumber" runat="server" ReadOnly="true"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="divRegistrationAttempts" runat="server">
                <h4 id="RegistrationAttemptsTitle" class="AccordionTitle">
                    <img id="RegistrationAttemptsArrow" alt="" src="" />
                    Tentative(s) ouverture compte dans SI bancaire<span id="closeRegistrationAttempts" style="display: none; font-size: 14px"></span>
                </h4>
                <div id="RegistrationAttemptsPanel" style="padding: 10px; display: none">
                    <asp:Repeater ID="rRegistrationAttempts" runat="server">
                        <HeaderTemplate>
                            <table id="tRegistrationAttemp" style="width: 80%">
                                <tr>
                                    <th>Requête</th>
                                    <th>Date requête</th>
                                    <th>Réponse</th>
                                    <th>Date réponse</th>
                                    <th style="text-align: right">Temps écoulé</th>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td style="text-align: center; vertical-align: middle;">
                                    <input type="button" onclick='clickShowXML(<%# getXmlFormat(Eval("RequestXML").ToString(), Eval("RequestDate").ToString(), "REQUETE XML")%>);' value='afficher XML' class="MiniButton" <%# getStatusButton(Eval("RequestXML").ToString()) %> />
                                </td>
                                <td style="text-align: center; vertical-align: middle;"><%# getXmlDate(Eval("RequestDate").ToString()) %></td>
                                <td style="text-align: center; vertical-align: middle;">
                                    <input type="button" onclick='clickShowXML(<%# getXmlFormat(Eval("AnswerXML").ToString(),Eval("AnswerDate").ToString(), "REPONSE XML") %>);' value='afficher XML' class='MiniButton' <%# getStatusButton(Eval("AnswerXML").ToString()) %> />
                                </td>
                                <td style="text-align: center; vertical-align: middle;"><%# getXmlDate(Eval("AnswerDate").ToString())%></td>
                                <td style="text-align: right; vertical-align: middle;"><%# Eval("ElapsedTimeSAB").ToString() + "s" %></td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <div id="dialog-XML" title="XML" style="display: none">
                        <div id="ChromeXmlPlugin" style="display: none; margin-bottom: 10px">
                            Vous utilisez Google Chrome et le XML ne s'affiche pas correctement
                            <a href="https://chrome.google.com/webstore/detail/xml-tree/gbammbheopgpmaagmckhpjbfgdfkpadb?hl=fr" style="color: #E8571D" target="_blank">Cliquez ici</a>
                        </div>
                        <%--
                            <xml id="fichierxml" src="XmlTmp/xml_20130809100436.xml"></xml>
                            <object type="text/html" data="XmlTmp/xml_20130809100436.xml" width="880" height="490"></object>
                        --%>
                        <iframe id="xmlFrame" width="800" height="400"></iframe>
                        <div id="NoXML" style="display: none; font-weight: bold">XML non récupéré!</div>
                    </div>

                    <div style="width:100%; text-align:center">
                        <asp:Button ID="btnForceSubscription" runat="server" Visible="true" style="margin-top:20px" CssClass="button" Text="Forcer la création du compte"
                            OnClientClick="return showForceSubscription();" />
                    </div>
                    <div id="dialog-ForceSubscription" style="display:none">
                        <div class="table" style="width:100%">
                            <div class="row">
                                <div class="cell">
                                    <div>
                                        Numéro de compte
                                        <span id="req_<%= txtForceSubscriptionAccountNumber.ClientID %>" class="reqStarStyle" style="visibility:hidden">*</span>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtForceSubscriptionAccountNumber" runat="server" MaxLength="11" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="cell">
                                    <div>
                                        Numéro de suivi
                                        <span id="req_<%= txtForceSubscriptionTrackingNumber.ClientID %>" class="reqStarStyle" style="visibility:hidden">*</span>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtForceSubscriptionTrackingNumber" runat="server" MaxLength="15" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <asp:Button ID="btnValidForceSubscription" runat="server" OnClick="clickForceSubscription" style="display:none" />
                    </div>

                </div>
            </asp:Panel>

            <asp:Panel ID="divPinBySms" runat="server">
                <h4 id="PinBySmsTitle" class="AccordionTitle">

                    <div style="display: table">
                        <div style="display: table-row">
                            <div style="display: table-cell; vertical-align: middle">
                                <img id="PinBySmsArrow" alt="" src="" />
                                Envoi du PIN par SMS<span id="closePinBySms" style="display: none; font-size: 14px"></span>
                            </div>
                            <div style="padding-left: 10px; display: table-cell; vertical-align: middle">
                                ( Etat :
                            </div>
                            <div style="padding-left: 5px; display: table-cell; vertical-align: middle">
                                <asp:Image ID="ImgPinBySmsStatus" runat="server" Style="position: relative; top: 1px" />
                                )
                            </div>
                        </div>
                    </div>

                </h4>
                <div id="PinBySmsPanel" style="padding: 10px; margin-bottom: 10px; display: none">
                    <div style="display: table; margin-top: 5px">
                        <div style="display: table-row">
                            <div style="display: table-cell">
                                <div style="margin-top: 5px">
                                    <div>
                                        <label for="txtExpectingPhoneNumber" class="label">
                                            Numéro tel. attendu</label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtExpectingPhoneNumber" runat="server" ReadOnly="true"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div style="display: table-cell; padding-left: 20px;">
                                <div style="margin-top: 5px">
                                    <div>
                                        <label for="txtSuccessfullCheckDate" class="label">
                                            Vérification réussie le</label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtSuccessfullCheckDate" runat="server" ReadOnly="true"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div style="display: table-cell; padding-left: 20px;">
                                <div style="margin-top: 5px">
                                    <div>
                                        <label for="txtSmsSent" class="label">
                                            SMS envoyé</label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtSmsSent" runat="server" ReadOnly="true" Width="40"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div id="panelSmsElapsedTime" runat="server" style="display: table-cell; padding-left: 20px;">
                                <div style="margin-top: 5px">
                                    <div>
                                        <label for="txtSmsElapsedTime" class="label">
                                            En</label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtSmsElapsedTime" runat="server" ReadOnly="true" Width="100" style="text-transform:lowercase"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="divPinBySmsAttempts" style="display: table; margin-top: 5px">
                        <div style="display: table-row">
                            <div style="display: table-cell">
                                <div style="display: table">
                                    <div style="display: table-row">
                                        <div style="display: table-cell; vertical-align: middle">
                                            <label for="txtNbAttempts" class="label">Nombre d'essai(s)</label>
                                        </div>
                                        <div style="display: table-cell; vertical-align: middle; padding-left: 5px">
                                            <asp:Button ID="btnAttemptDetails" runat="server" Visible="false" OnClientClick="clickAttemptDetails();return false;"
                                                Text="afficher détails" TabIndex="10" CssClass="MiniButton" Width="120px" Style="margin-bottom: 2px"></asp:Button>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <asp:TextBox ID="txtNbAttempts" runat="server" ReadOnly="true"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="panelAttemptDetails" style="display: none; padding: 5px">
                        <asp:GridView ID="gvAttemptDetails" runat="server" AllowPaging="False" Style="width: 98%"
                            AutoGenerateColumns="False" CssClass="grid">
                            <Columns>
                                <asp:BoundField DataField="PhoneNumber" HeaderText="N° Téléphone" SortExpression="PhoneNumber" />
                                <asp:BoundField DataField="ChallengeReceived" HeaderText="Code reçu" SortExpression="ChallengeReceived" />
                                <asp:BoundField DataField="Date" HeaderText="Date" SortExpression="Date" />
                                <asp:TemplateField HeaderText="Etat" SortExpression="Status">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAttemptStatus" runat="server" ForeColor='<%# getColor(Eval("StatusColor").ToString()) %>' Text='<%# Eval("Status").ToString() %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <PagerStyle CssClass="GridViewPager" />
                            <EmptyDataTemplate>
                                Aucun résultat ne correspond à la recherche
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="divWebPassword" runat="server">
                <h4 id="WebPasswordTitle" class="AccordionTitle">

                    <div style="display: table">
                        <div style="display: table-row">
                            <div style="display: table-cell; vertical-align: middle">
                                <img id="WebPasswordArrow" alt="" src="" />
                                Envoi du password Web par SMS<span id="closeWebPassword" style="display: none; font-size: 14px"></span>
                            </div>
                            <div style="padding-left: 10px; display: table-cell; vertical-align: middle">
                                ( Etat :
                            </div>
                            <div style="padding-left: 5px; display: table-cell; vertical-align: middle">
                                <asp:Image ID="ImgWebPassBySmsStatus" runat="server" Style="position: relative; top: 1px" />
                                )
                            </div>
                        </div>
                    </div>
                </h4>
                <div id="WebPasswordPanel" style="padding: 10px; margin-bottom: 10px; display: none">
                    <div style="display: table; margin-top: 5px">
                        <div style="display: table-row">
                            <div style="display: table-cell">
                                <div style="margin-top: 5px">
                                    <div>
                                        <label for="txtWebPasswordPhoneNumber" class="label">
                                            N° Téléphone</label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtWebPasswordPhoneNumber" runat="server" ReadOnly="true"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div style="display: table-cell; padding-left: 20px;">
                                <div style="margin-top: 5px">
                                    <div>
                                        <label for="txtWebPasswordSmsSent" class="label">
                                            SMS envoyé</label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtWebPasswordSmsSent" runat="server" ReadOnly="true"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div style="display: table-cell; padding-left: 20px;">
                                <div style="margin-top: 5px">
                                    <div>
                                        <label for="txtWebPasswordLastSentDate" class="label">
                                            Dernier SMS envoyé le</label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtWebPasswordLastSentDate" runat="server" ReadOnly="true"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="display: table-row">
                            <div style="display: table-cell">
                                <div style="margin-top: 5px">
                                    <div>
                                        <label for="txtWebPasswordLastReceiveDate" class="label">
                                            Dernier SMS reçu par le client le</label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtWebPasswordLastReceiveDate" runat="server" ReadOnly="true"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div style="display: table-cell">
                            </div>
                            <div style="display: table-cell">
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>

            <asp:Panel ID="divPdf" runat="server">
                <h4 id="PdfTitle" class="AccordionTitle">
                    <img id="PdfArrow" alt="" src="" />
                    Dossier d'ouverture (PDF)<span id="closePdf" style="display: none; font-size: 14px"></span>
                </h4>
                <div id="PdfPanel" style="padding: 10px; width: 100%; margin: 10px 0; display: none">
                    <div style="display: table; margin-top: 5px">
                        <div style="display: table-row">
                            <div style="display: table-cell;">
                                <div>
                                    <label for="txtPdfSent" class="label">
                                        Envoyé</label>
                                </div>
                                <div>
                                    <asp:TextBox ID="txtPdfSent" runat="server" ReadOnly="true"></asp:TextBox>
                                </div>
                            </div>
                            <div style="display: table-cell; padding-left: 20px;">
                                <asp:Panel ID="panelPdfSent_Date" runat="server" Visible="false">
                                    <div>
                                        <label for="txtPdfSent_Date" class="label">
                                            le</label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtPdfSent_Date" runat="server" ReadOnly="true"></asp:TextBox>
                                    </div>
                                </asp:Panel>
                            </div>
                            <div style="display: table-cell; padding: 0 0 5px 20px; vertical-align:bottom">
                                <%--<asp:HyperLink ID="hlTestPdf" runat="server" Visible="false">pdf</asp:HyperLink>
                                <asp:Button ID="btnShowSubscriptionPdf" runat="server" Visible="false" OnClientClick="return false;"
                                    Text="afficher" TabIndex="10" CssClass="MiniButton" Width="100px" Style="position: relative; top: 22px"></asp:Button>--%>
                                <input type="button" id="btnShowFile" value="afficher" class="MiniButton" style="width:100px" onclick="ShowRegistrationFile();" />
                                <asp:HiddenField ID="hdnRegistrationFilePath" runat="server" />
                                <div id="dialog-PDF" title="PDF" style="display: none">
                                    <iframe id="pdfFrame" width="800" height="400"></iframe>
                                    <div id="noPDF" style="display: none; font-weight: bold">PDF non récupéré! Veuillez réessayer.</div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </asp:Panel>

            <asp:Panel ID="divCgv" runat="server">
                <h4 id="CgvTitle" class="AccordionTitle">
                    <img id="CgvArrow" alt="" src="" />
                    Signature des Conditions Générales de Vente
                </h4>
                <div id="CgvPanel" style="padding: 10px; width: 100%; margin: 10px 0; text-align: center">
                    <asp:Image ID="imgSignatureCgv" runat="server" AlternateText="" style="max-width:90%" />
                </div>
            </asp:Panel>

            <asp:HiddenField ID="hfRegistrationChecked" runat="server" Value="N" />
            <asp:HiddenField ID="hfRegistrationClosed" runat="server" Value="N" />
            <asp:HiddenField ID="hfRegistrationCheckStatus" runat="server" Value="" />

        </div>
        <div id="dialog-error" title="Erreur" style="display: none">
            <asp:Label ID="lblError" runat="server" Style="font-weight: bold; color: Red"></asp:Label>
        </div>

        <div id="dialog-confirmSubscription" style="display: none">
            <div style="font-weight: bold">
                Statut
                <span id="reqSubscriptionStatus" class="reqStarStyle" style="visibility: hidden; position: relative; top: 7px">*</span>
            </div>
            <asp:DropDownList ID="ddlSubscriptionCheckStatus" runat="server" onchange="checkSubscriptionStatus();">
                <asp:ListItem Value="" Selected="True"></asp:ListItem>
                <asp:ListItem Value="G" style="font-weight: bold; color: Green">Conforme</asp:ListItem>
                <asp:ListItem Value="O" style="font-weight: bold; color: Orange">Suspecte</asp:ListItem>
                <asp:ListItem Value="R" style="font-weight: bold; color: Red">Non conforme</asp:ListItem>
            </asp:DropDownList>
            <div id="divSetNoComplianceList" style="display:none; margin:10px 0">
                <div id="lblSetNoComplianceEmpty" style="visibility:hidden; color:red; font-weight:bold">Veuillez sélectionner au moins une non conformité.</div>
                <asp:Repeater ID="rptSetNoComplianceList" runat="server">
                    <HeaderTemplate>
                        <table cellpadding="0" cellspacing="0" style="width:100%; border:1px solid #e8571d; border-collapse:collapse;">
                            <thead>
                                <tr style="background-color:#e8571d; color:#FFF;">
                                    <th style="text-align:center;">Non conformité</th>
                                    <th style="text-align:center;">Priorité</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                                <tr>
                                    <td style="padding:2px 5px;">
                                        <%# Eval("Compliance") %>
                                    </td>
                                    <td style="padding:2px 5px; text-align:center">
                                        <%# Eval("Priority") %>
                                    </td>
                                    <td style="padding:2px 5px;">
                                        <asp:HiddenField ID="hfComplianceValue" runat="server" Value='<%# Eval("ComplianceTAG") %>' />
                                        <asp:CheckBox ID="cbNoCompliance" runat="server" CssClass="SetNoComplianceCbList" />
                                    </td>
                                </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                            </tbody>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
            <div style="margin-top: 10px; font-weight: bold">
                Remarque(s)
                        (<label id="lblNbChar">0</label>
                / <asp:Label ID="lblNbMaxCheckComment" runat="server">200</asp:Label> caractères)
            </div>
            <asp:Label ID="lblSubscriptionCheckCommentHistory" runat="server"></asp:Label>
            <asp:TextBox ID="txtSubscriptionCheckComment" runat="server" TextMode="MultiLine" MaxLength="200"
                onKeyUp="javascript:MaxLength(this);" onChange="javascript:MaxLength(this);"
                Width="660" Height="80" Style="max-height: 80px; min-height: 80px; max-width: 660px; min-width: 660px">
            </asp:TextBox>

            <asp:Button ID="btnRegistrationChecked" runat="server" OnClick="btnRegistrationChecked_click" style="display:none" />
            <asp:Button ID="btnRegistrationRectified" runat="server" OnClick="btnRegistrationRectified_click" style="display:none" />
        </div>
    </div>

    <div style="width: 100%; display: table">
        <div style="display: table-row">
            <div style="display: table-cell; text-align: left">
                <asp:Button ID="btnPrevious" runat="server" Text="Recherche inscription" OnClick="onClickPreviousButton" CssClass="button" />
            </div>
            <div style="display: table-cell; text-align: right">
                <asp:Button ID="btnModify" runat="server" Text="Enregistrer les modifications" CssClass="button" Visible="false" style="margin: 0;" OnClientClick="return getClientModification();"/>
                <asp:Button ID="btnModifyValidation" runat="server" Text="Enregistrer les modifications" CssClass="button" Visible="false" style="margin:0; display:none"  OnClick="onClickSaveClientModification" />
            </div>
        </div>
    </div>

    <div id="dialog-alertMessage" style="display: none">
        <label id="lblMessage"></label>
    </div>

    <link href="Styles/Upload.css" rel="Stylesheet" type="text/css" />

</asp:Content>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="FooterContent">
    <div class="panel-loading"></div>

    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginClientDetailsRequestHandler);
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndClientDetailsRequestHandler);
        function BeginClientDetailsRequestHandler(sender, args) {
            try{
                pbControl = args.get_postBackElement();
                console.log(pbControl.id);
                var containerID = '';
                if (pbControl != null) {
                    if (pbControl.id.indexOf('ddlDocs') != -1)
                        containerID = "IdPanel";
                }

                if(containerID.length > 0)
                    ShowPanelLoading(containerID);
            }
            catch(e)
            {

            }
        }
        function EndClientDetailsRequestHandler(sender, args) {
            HidePanelLoading();
        }
    </script>
</asp:Content>