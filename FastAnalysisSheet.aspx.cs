﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using XMLMethodLibrary;

public partial class FastAnalysisSheet : System.Web.UI.Page
{
    private const int _sizeMax = 400;
    protected int getCurrentRefCustomer()
    {
        int iRefCustomer = 0;

        if (!(Request.Params["client"] != null && int.TryParse(Request.Params["client"], out iRefCustomer)))
            if (!(ViewState["client"] != null && int.TryParse(ViewState["client"].ToString(), out iRefCustomer)))
                Response.Redirect("SearchAlert.aspx", true);

        return iRefCustomer;
    }
    protected int getCurrentRefAlert()
    {
        int iRefAlert = 0;

        if (Request.Params["ref"] != null)
            int.TryParse(Request.Params["ref"], out iRefAlert);
        else if (ViewState["ref"] != null)
            int.TryParse(ViewState["ref"].ToString(), out iRefAlert);

            //Response.Redirect("SearchAlert.aspx", true);

            return iRefAlert;
    }
    protected void getPeriodDate(string sMonth, out DateTime dtFrom, out DateTime dtTo)
    {
        dtFrom = DateTime.Now;
        dtTo = DateTime.Now;

        switch (sMonth)
        {
            case "1":
                dtFrom = DateTime.Now.AddMonths(-1);
                break;
            case "3":
                dtFrom = DateTime.Now.AddMonths(-3);
                break;
            case "6":
                dtFrom = DateTime.Now.AddMonths(-6);
                break;
            case "12":
                dtFrom = DateTime.Now.AddMonths(-12);
                break;
            case "-1":
                dtFrom = DateTime.Parse("01/01/2014");
                break;
        }

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            int iRefCustomer = getCurrentRefCustomer();
            ViewState["client"] = iRefCustomer;
            int iRefAlert = getCurrentRefAlert();
            if (iRefAlert != 0)
            {
                ViewState["ref"] = iRefAlert;
                setAlertDetails(iRefAlert);
            }
            else
            {
                panelAlertDetails.Visible = false;
                btnAlertTreatmentSheet.Visible = false;
                panelScoring2.Visible = false;
                panelScoring3.Visible = false;
            }

            setCustomerInfos(iRefCustomer);
            setCustomerFlow();
            setCustomerCashInOut();
            SetCharts();

            BeneficiaryList1.iRefCustomer = iRefCustomer;
            SpecificityHistory1.iRefCustomer = iRefCustomer;
            CardsHistory1.iRefCustomer = iRefCustomer;
            ProfileHistory1.iRefCustomer = iRefCustomer;
            BlockingHistory1.iRefCustomer = iRefCustomer;
            IPConnectinoList1.iRefCustomer = iRefCustomer;
            IPConnectinoList1.Type = "FIRSTLAST";

            LightLock1.RefCustomer = iRefCustomer;
            proNotificationCounter1.RefCustomer = iRefCustomer;

            checkPreviousButtonUrl();

            BindAmlComments();

            clientRelation1.RefCustomer = iRefCustomer;
            if (!clientRelation1.HasViewRight)
                panelRelation.Visible = false;

            IPConnectinoList1.BindIPLocation();
        }
    }

    protected void BindAmlComments()
    {
        DataTable dt = AML.getClientNote(getCurrentRefCustomer());

        var sortedTable = dt;

        if (dt.Rows.Count == 0)
            panelPrevAmlComment.CssClass = "hidden";
        else
        {
            panelPrevAmlComment.CssClass = "visible";

            // Order comments by date
            sortedTable = dt.AsEnumerable()
                .OrderBy(r => DateTime.ParseExact(r.Field<string>("NoteDate"), "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture))
                .CopyToDataTable();
        }

        rptAmlComment.DataSource = sortedTable;
        rptAmlComment.DataBind();
        upAmlComment.Update();

        BindAMLCommentJS();
    }

    protected void BindAMLCommentJS()
    {
        AmlComment.BindCommentLengthCheck();
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "InitAmlMultiComment();", true);
    }

    protected void setAlertDetails(int iRefAlert)
    {
        DataTable dt = AML.GetAlertGeneralInfo(iRefAlert);

        if (dt.Rows.Count > 0)
        {
            DataTable dtInfos = new DataTable();
            dtInfos.Columns.Add("Label");
            dtInfos.Columns.Add("Value");

            for(int i = 0; i < dt.Columns.Count; i++)
            {
                if (i == 0)
                    dtInfos.Rows.Add(new object[] { "Type", dt.Rows[0][i].ToString() });
                else if(dt.Columns[i].Caption.Contains("Label") && !String.IsNullOrWhiteSpace(dt.Rows[0][i].ToString()))
                {
                    if (i + 1 < dt.Columns.Count)
                    {
                        bool bShow = true;
                        string sLabel, sValue;
                        sLabel = dt.Rows[0][i].ToString();
                        sValue = dt.Rows[0][i + 1].ToString();

                        if (dt.Rows[0][0].ToString().ToLower().Contains("réquisition"))
                        {
                            if (i == 1)
                                bShow = false;
                            if (i == 5)
                            {
                                int iCloseAccount;
                                int.TryParse(sValue.Split('.')[0], out iCloseAccount);

                                sValue = (iCloseAccount == 0) ? "Non" : "Oui";
                            }
                            if (i == 7)
                            {
                                DataTable dtReq = Requisition.GetMotifRequisitionList();
                                String[] arMotif = sValue.Split(';');
                                if (dtReq != null && dtReq.Rows.Count > 0 && arMotif.Length > 0)
                                {
                                    StringBuilder sb = new StringBuilder();

                                    for (int j = 0; j < arMotif.Length; j++)
                                    {
                                        for (int k = 0; k < dtReq.Rows.Count; k++)
                                        {
                                            if (arMotif[j] == dtReq.Rows[k]["Value"].ToString())
                                            {
                                                sb.AppendLine(dtReq.Rows[k]["Text"].ToString());
                                                break;
                                            }
                                        }
                                    }

                                    sValue = sb.ToString().Replace(Environment.NewLine, "<br/>");
                                }
                            }
                        }

                        if (sLabel == "Scoring")
                        {
                            bShow = false;
                            if (!String.IsNullOrWhiteSpace(sValue))
                                scoring2.Score = int.Parse(sValue);
                            else scoring2.Score = 30;
                        }
                        else if (sLabel == "Scoring Details")
                        {
                            bShow = false;
                            if (!String.IsNullOrWhiteSpace(sValue))
                                scoring2.ScoreDetails = sValue;
                        }

                        if (bShow)
                            dtInfos.Rows.Add(new object[] { sLabel, sValue });
                    }
                }
            }

            rptAlertInfos.DataSource = dtInfos;
            rptAlertInfos.DataBind();

            string sAlert = AML.getAlertDetails(iRefAlert.ToString());
            if (!String.IsNullOrWhiteSpace(sAlert))
            {
                List<string> listDetails = CommonMethod.GetTags(sAlert, "ALERTE/DETAIL/table");

                if (listDetails.Count > 0)
                {
                    litAlertDetailsTable.Text = listDetails[0];
                    panelAlertDetailsTable.Visible = true;
                }
            }
        }
        else panelAlertDetails.Visible = false;
    }

    protected void setCustomerInfos(int iRefCustomer)
    {
        string sXmlCustomerInfos = Client.GetCustomerInformations(iRefCustomer);

        if(sXmlCustomerInfos.Length > 0) {

            LightLock1.RefreshClientDebitStatus(sXmlCustomerInfos);
            scoring1.XmlCustomerInfos = sXmlCustomerInfos;
            scoring3.Score = scoring1.Score + scoring2.Score;

            List<string> listCivility = CommonMethod.GetAttributeValues(sXmlCustomerInfos, "ALL_XML_OUT/Customer", "Politeness");
            List<string> listLastName = CommonMethod.GetAttributeValues(sXmlCustomerInfos, "ALL_XML_OUT/Customer", "LastName");
            List<string> listFirstName = CommonMethod.GetAttributeValues(sXmlCustomerInfos, "ALL_XML_OUT/Customer", "FirstName");
            List<string> listAccountNumber = CommonMethod.GetAttributeValues(sXmlCustomerInfos, "ALL_XML_OUT/Customer", "AccountNumber");
            List<string> listWebIdentifier = CommonMethod.GetAttributeValues(sXmlCustomerInfos, "ALL_XML_OUT/Customer", "WebIdentifier");

            List<string> listLockCustomerDebit = CommonMethod.GetAttributeValues(sXmlCustomerInfos, "ALL_XML_OUT/Customer", "LockCustomerDebit");
            List<string> listLockCustomerDebitRefReason = CommonMethod.GetAttributeValues(sXmlCustomerInfos, "ALL_XML_OUT/Customer", "LockCustomerDebitRefReason");
            List<string> listLockList = CommonMethod.GetTags(sXmlCustomerInfos, "ALL_XML_OUT/Customer/LOCKLIST/LOCK");

            lblClientCardNumber.Text = (listWebIdentifier.Count > 0) ? listWebIdentifier[0] : "";
            lblClientAccountNumber.Text = listAccountNumber[0];
            lblClientName.Text = listCivility[0] + " " + listFirstName[0] + " " + listLastName[0];

            // date inscription
            List<string> listCreationDate = CommonMethod.GetAttributeValues(sXmlCustomerInfos, "ALL_XML_OUT/Customer", "CreationDate");
            litInscriptionDate.Text = listCreationDate[0].Substring(0, 10);
            // lieu inscription
            List<string> listIDPDVSubscription = CommonMethod.GetAttributeValues(sXmlCustomerInfos, "ALL_XML_OUT/Customer", "IDPDVSubscription");
            litIDPDV.Text = listIDPDVSubscription[0];
            List<string> listPDVSubscriptionName = CommonMethod.GetAttributeValues(sXmlCustomerInfos, "ALL_XML_OUT/Customer", "PDVSubscriptionName");
            litPDVName.Text = listPDVSubscriptionName[0];
            List<string> listPDVCity = CommonMethod.GetAttributeValues(sXmlCustomerInfos, "ALL_XML_OUT/Customer", "SubscriptionCity");
            litPDVCity.Text = listPDVCity[0];

            List<string> listEmail = CommonMethod.GetAttributeValues(sXmlCustomerInfos, "ALL_XML_OUT/Customer", "Email");
            litEmail.Text = (listEmail.Count > 0) ? listEmail[0] : "N.C.";

            // age
            List<string> listBirthDate = CommonMethod.GetAttributeValues(sXmlCustomerInfos, "ALL_XML_OUT/Customer", "BirthDate");
            DateTime bday = DateTime.ParseExact(listBirthDate[0], "dd/MM/yyyy", CultureInfo.InvariantCulture);
            DateTime today = DateTime.Today;
            int age = today.Year - bday.Year;
            if (bday > today.AddYears(-age))
                age--;
            litAge.Text = age.ToString();

            // nationalite
            List<string> listBirthCountryISO2 = CommonMethod.GetAttributeValues(sXmlCustomerInfos, "ALL_XML_OUT/Customer", "BirthCountryISO2");
            DataTable dt = tools.GetCountryList();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if(dt.Rows[i]["ISO2"].ToString() == listBirthCountryISO2[0])
                {
                    litNationalite.Text = dt.Rows[i]["CountryName"].ToString();
                    break;
                }
            }

            // zipcode
            List<string> listZipcode = CommonMethod.GetAttributeValues(sXmlCustomerInfos, "ALL_XML_OUT/Customer", "Zipcode");
            litZipCode.Text = listZipcode[0];

            // KYC
            List<string> listCodePRO = CommonMethod.GetAttributeValues(sXmlCustomerInfos, "ALL_XML_OUT/Customer", "codPro");
            List<string> listModHab = CommonMethod.GetAttributeValues(sXmlCustomerInfos, "ALL_XML_OUT/Customer", "modHab");
            List<string> listIncome = CommonMethod.GetAttributeValues(sXmlCustomerInfos, "ALL_XML_OUT/Customer", "IncomeTAG");
            List<string> listEstate = CommonMethod.GetAttributeValues(sXmlCustomerInfos, "ALL_XML_OUT/Customer", "Estate");

            string sXmlKYC = Client.GetKYCChoices();
            List<string> listKYC = CommonMethod.GetTags(sXmlKYC, "ALL_XML_OUT/List");
            for (int i = 0; i < listKYC.Count; i++)
            {
                List<string> listTAGQuestion = CommonMethod.GetAttributeValues(listKYC[i], "List", "TAGQuestion");
                List<string> listTAGChoice = CommonMethod.GetAttributeValues(listKYC[i], "List", "ChoiceTag");
                List<string> listChoice = CommonMethod.GetAttributeValues(listKYC[i], "List", "Choice");

                if (listTAGQuestion[0] == "PROF" && listTAGChoice[0] == listCodePRO[0])
                    litCSP.Text = listChoice[0];
                if (listTAGQuestion[0] == "SIT_PATRIM" && listTAGChoice[0] == listModHab[0])
                    litHab.Text = listChoice[0];
                if (listTAGQuestion[0] == "REVENUS" && listTAGChoice[0] == listIncome[0])
                    litRevenus.Text = listChoice[0];
                if (listTAGQuestion[0] == "PATRIMOINE" && listTAGChoice[0] == listEstate[0])
                    litPatrimoine.Text = listChoice[0];
            }
        }
    }

    protected void getCustomerFlow(int iRefCustomer, DateTime dtFrom, DateTime dtTo, out string sAmountIn, out string sAmountOut, out string sNbIn, out string sNbOut)
    {
        sAmountIn = "0"; sAmountOut = "0"; sNbIn = "0"; sNbOut = "0";
        SqlConnection conn = null;
        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("BankSI.P_GetCustomerFlow", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN", new XElement("Flow",
                                            new XAttribute("RefCustomer", iRefCustomer.ToString()),
                                            new XAttribute("From", dtFrom.ToString("dd/MM/yyyy")),
                                            new XAttribute("To", dtTo.ToString("dd/MM/yyyy HH:mm")))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            List<string> lsAmountIn = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Flow/IN", "Amount");
            List<string> lsAmountOut = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Flow/OUT", "Amount");
            List<string> lsNbIn = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Flow/IN", "Nb");
            List<string> lsNbOut = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Flow/OUT", "Nb");

            if (lsAmountIn.Count > 0 && lsAmountIn[0].Trim().Length > 0)
                sAmountIn = lsAmountIn[0];
            if (lsAmountOut.Count > 0 && lsAmountOut[0].Trim().Length > 0)
                sAmountOut = lsAmountOut[0];
            if (lsNbIn.Count > 0 && lsNbIn[0].Trim().Length > 0)
                sNbIn = lsNbIn[0];
            if (lsNbOut.Count > 0 && lsNbOut[0].Trim().Length > 0)
                sNbOut = lsNbOut[0];
        }
        catch (Exception ex)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }
    }
    protected void setCustomerFlow()
    {
        string sAmountIn = "", sAmountOut = "", sNbIn = "", sNbOut = "";
        DateTime dtFrom, dtTo;
        getPeriodDate(rblPeriodFlow.SelectedValue, out dtFrom, out dtTo);
        getCustomerFlow(getCurrentRefCustomer(), dtFrom, dtTo, out sAmountIn, out sAmountOut, out sNbIn, out sNbOut);
        int iNbIn = 0, iNbOut = 0;
        int.TryParse(sNbIn, out iNbIn);
        int.TryParse(sNbOut, out iNbOut);
        lblClientFlow.Text = "<span class=\"bold font-orange\">Sur la période du "+dtFrom.ToString("dd/MM/yyyy")+" au "+ dtTo.ToString("dd/MM/yyyy") + " :</span><br/>";
        lblClientFlow.Text += "<span class=\"bold\">"+sNbIn+ " flux entrant" + ((iNbIn > 1) ? "s" : "") + "</span> pour un montant de <span class=\"bold\">" + sAmountIn+" euros</span>.<br/>";
        lblClientFlow.Text += "<span class=\"bold\">"+sNbOut+ " flux sortant" + ((iNbOut > 1) ? "s" : "") + "</span> pour un montant de <span class=\"bold\">" + sAmountOut+" euros</span>.";
    }

    protected void getCustomerCashInOut(int iRefCustomer, DateTime dtFrom, DateTime dtTo, out string sAmountIn, out string sAmountOut, out string sNbIn, out string sNbOut)
    {
        sAmountIn = "0"; sAmountOut = "0"; sNbIn = "0"; sNbOut = "0";
        SqlConnection conn = null;
        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("BankSI.P_GetCustomerCashInOut", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN", new XElement("Cash",
                                            new XAttribute("RefCustomer", iRefCustomer.ToString()),
                                            new XAttribute("From", dtFrom.ToString("dd/MM/yyyy")),
                                            new XAttribute("To", dtTo.ToString("dd/MM/yyyy HH:mm")))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            List<string> lsAmountIn = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Cash/IN", "Amount");
            List<string> lsAmountOut = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Cash/OUT", "Amount");
            List<string> lsNbIn = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Cash/IN", "Nb");
            List<string> lsNbOut = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Cash/OUT", "Nb");

            if (lsAmountIn.Count > 0 && lsAmountIn[0].Trim().Length > 0)
                sAmountIn = lsAmountIn[0];
            if (lsAmountOut.Count > 0 && lsAmountOut[0].Trim().Length > 0)
                sAmountOut = lsAmountOut[0];
            if (lsNbIn.Count > 0 && lsNbIn[0].Trim().Length > 0)
                sNbIn = lsNbIn[0];
            if (lsNbOut.Count > 0 && lsNbOut[0].Trim().Length > 0)
                sNbOut = lsNbOut[0];
        }
        catch (Exception ex)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }
    }
    protected void setCustomerCashInOut()
    {
        string sAmountIn = "", sAmountOut = "", sNbIn = "", sNbOut = "";
        DateTime dtFrom, dtTo;
        getPeriodDate(rblPeriodCashInOut.SelectedValue, out dtFrom, out dtTo);
        getCustomerCashInOut(getCurrentRefCustomer(), dtFrom, dtTo, out sAmountIn, out sAmountOut, out sNbIn, out sNbOut);
        int iNbIn = 0, iNbOut = 0;
        int.TryParse(sNbIn, out iNbIn);
        int.TryParse(sNbOut, out iNbOut);
        lblClientCashInOut.Text = "<span class=\"bold font-orange\">Sur la période du " + dtFrom.ToString("dd/MM/yyyy") + " au "+ dtTo.ToString("dd/MM/yyyy") + " :</span><br/>";
        lblClientCashInOut.Text += "<span class=\"bold\">" + sNbIn + " dépôt" + ((iNbIn > 1) ? "s" : "") + "</span> cash pour un montant de <span class=\"bold\">" + sAmountIn + " euros</span>.<br/>";
        lblClientCashInOut.Text += "<span class=\"bold\">"+ sNbOut + " retrait" + ((iNbOut > 1) ? "s" : "") + "</span> cash pour un montant de <span class=\"bold\">" + sAmountOut + " euros</span>.";
    }

    protected void getCustomerOperationsByCategory(int iRefCustomer, DateTime dtFrom, DateTime dtTo, out string sXmlOut)
    {
        sXmlOut = "";
        SqlConnection conn = null;
        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("BankSI.P_GetCustomerOperationsCategorie", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN", new XElement("Operations",
                                            new XAttribute("RefCustomer", iRefCustomer.ToString()),
                                            new XAttribute("From", dtFrom.ToString("dd/MM/yyyy")),
                                            new XAttribute("To", dtTo.ToString("dd/MM/yyyy HH:mm")))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
            cmd.ExecuteNonQuery();

            sXmlOut = cmd.Parameters["@OUT"].Value.ToString();
        }
        catch (Exception ex)
        {
            sXmlOut = "";
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }
    }
    [Serializable]
    protected class HighChartOptions
    {
        public string title;
        public List<string> categories;
        public List<HighChartSeries> series;
    }
    [Serializable]
    protected class HighChartSeries
    {
        public string name;
        public List<float> data;
        public int qty;
        public string amnt;
        public string details;
        public bool stickyTracking = true;
    }
    protected void SetCharts()
    {
        StringBuilder chaineFluxPave2 = new StringBuilder();
        OperationsFlux operationsFlux = new OperationsFlux();
        var operationFlux = new OperationFlux();

        List<HighChartOptions> listOptions = new List<HighChartOptions>();
        string sXmlOut = "";
        DateTime dtFrom, dtTo;
        getPeriodDate(rblOperationByCategoryPeriod.SelectedValue, out dtFrom, out dtTo);
        getCustomerOperationsByCategory(getCurrentRefCustomer(), dtFrom, dtTo, out sXmlOut);

        List<string> listQuantityParent = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Operations/IN", "Nb");
        List<string> listLabelParent = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Operations/IN", "Label");
        List<string> listAmountParent = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Operations/IN", "Amount");
        List<string> listDetails = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/Operations/IN/DETAIL");
        List<string> listDetailsOpe = new List<string>();
        List <string> listCategories = new List<string>();
        HighChartOptions opt = null;
        List<HighChartSeries> listSeries = null;
        decimal dTotalAmountIN = -1;
        StringBuilder sbDetails = null;

        opt = new HighChartOptions();
        opt.title = "Opérations Entrantes du " + dtFrom.ToString("dd/MM/yyyy") + " au " + dtTo.ToString("dd/MM/yyyy");

        if (listQuantityParent.Count > 0 && listQuantityParent[0].Length > 0 && listQuantityParent[0] != "0")
        {
            int iTotalQtyIN = int.Parse(listQuantityParent[0].ToString());
            dTotalAmountIN = decimal.Parse(listAmountParent[0], CultureInfo.InvariantCulture);

            listSeries = new List<HighChartSeries>();
            listCategories = new List<string>();
            listDetailsOpe = new List<string>();

            for (int i = 0; i < listDetails.Count; i++)
            {
                List<string> listLabel = CommonMethod.GetAttributeValues(listDetails[i], "DETAIL", "Label");
                List<string> listAmount = CommonMethod.GetAttributeValues(listDetails[i], "DETAIL", "Amount");
                List<string> listQuantity = CommonMethod.GetAttributeValues(listDetails[i], "DETAIL", "Nb");

                sbDetails = new StringBuilder();
                listDetailsOpe = CommonMethod.GetTags(listDetails[i], "DETAIL/DETAILS");
                operationFlux = new OperationFlux();
                operationFlux.Libelle = listLabel[0];
                operationFlux.Montant = decimal.Parse(listAmount[0], CultureInfo.InvariantCulture);
                if (listDetailsOpe.Count > 0)
                {

                    sbDetails.Append("<table class=\"hc-tooltip-table\"><tr><th>Qui</th><th>Qté</th><th>Montant</th></tr>");
                    for (int j = 0; j < listDetailsOpe.Count; j++)
                    {
                        List<string> lsWho = CommonMethod.GetAttributeValues(listDetailsOpe[j], "DETAILS", "Who");
                        List<string> lsNbTransfers = CommonMethod.GetAttributeValues(listDetailsOpe[j], "DETAILS", "NbTransfers");
                        List<string> lsSumTransfers = CommonMethod.GetAttributeValues(listDetailsOpe[j], "DETAILS", "SumTransfers");

                        sbDetails.Append(String.Format("<tr><td>{0}</td><td>{1}</td><td>{2}</td></tr>", lsWho[0], lsNbTransfers[0], lsSumTransfers[0] + " €"));

                        if (!string.IsNullOrEmpty(lsWho[0]))
                            operationFlux.WhoList.Add(lsWho[0]);
                    }
                    
                    sbDetails.Append("</table>");
                }
                if (operationFlux.Montant != 0 || operationFlux.WhoList.Any())
                    operationsFlux.OperationInList.Add(operationFlux);

                HighChartSeries series = new HighChartSeries();
                series.name = getStringTrimmed(listLabel[0], 15);
                series.data = new List<float>();
                series.data.Add(getAmountPercentage(decimal.Parse(listAmount[0], CultureInfo.InvariantCulture), dTotalAmountIN));
                series.qty = int.Parse(listQuantity[0]);
                series.amnt = listAmount[0] + " €";
                series.details = sbDetails.ToString().Replace("\r\n", "<br/>");

                listSeries.Add(series);
            }

            listSeries = listSeries.OrderBy(o => o.data[0]).ToList();
            opt.series = listSeries;

            for (int i = 0; i < listSeries.Count; i++)
            {
                listCategories.Add(listSeries[i].name);
            }
            opt.categories = listCategories;
        }

        listOptions.Add(opt);

        listQuantityParent = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Operations/OUT", "Nb");
        listLabelParent = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Operations/OUT", "Label");
        listAmountParent = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Operations/OUT", "Amount");
        listDetails = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/Operations/OUT/DETAIL");
        decimal dTotalAmountOUT = -1;

        opt = new HighChartOptions();
        opt.title = "Opérations Sortantes du " + dtFrom.ToString("dd/MM/yyyy") + " au " + dtTo.ToString("dd/MM/yyyy");

        if (listQuantityParent.Count > 0 && listQuantityParent[0].Length > 0 && listQuantityParent[0] != "0")
        {
            decimal pourcent = 0;

            
            dTotalAmountOUT = decimal.Parse(listAmountParent[0], CultureInfo.InvariantCulture);
            listSeries = new List<HighChartSeries>();
            listCategories = new List<string>();
            listDetailsOpe = new List<string>();

            
            for (int i = 0; i < listDetails.Count; i++)
            {
                List<string> listLabel = CommonMethod.GetAttributeValues(listDetails[i], "DETAIL", "Label");
                List<string> listAmount = CommonMethod.GetAttributeValues(listDetails[i], "DETAIL", "Amount");
                List<string> listQuantity = CommonMethod.GetAttributeValues(listDetails[i], "DETAIL", "Nb");

                sbDetails = new StringBuilder();
                listDetailsOpe = CommonMethod.GetTags(listDetails[i], "DETAIL/DETAILS");
                operationFlux = new OperationFlux();
                operationFlux.Libelle = listLabel[0];
                operationFlux.Montant = decimal.Parse(listAmount[0], CultureInfo.InvariantCulture);

                if (listDetailsOpe.Count > 0)
                {
                    
                    sbDetails.Append("<table class=\"hc-tooltip-table\"><tr><th>Qui</th><th>Qté</th><th>Montant</th></tr>");
                    for (int j = 0; j < listDetailsOpe.Count; j++)
                    {
                        
                        List<string> lsWho = CommonMethod.GetAttributeValues(listDetailsOpe[j], "DETAILS", "Who");
                        List<string> lsNbTransfers = CommonMethod.GetAttributeValues(listDetailsOpe[j], "DETAILS", "NbTransfers");
                        List<string> lsSumTransfers = CommonMethod.GetAttributeValues(listDetailsOpe[j], "DETAILS", "SumTransfers");
                        sbDetails.Append(String.Format("<tr><td>{0}</td><td>{1}</td><td>{2}</td></tr>", lsWho[0], lsNbTransfers[0], lsSumTransfers[0] + " €"));

                        if(!string.IsNullOrEmpty(lsWho[0]))
                            operationFlux.WhoList.Add(lsWho[0]);

                    }
                    
                    sbDetails.Append("</table>");
                }
                if(operationFlux.Montant != 0 || operationFlux.WhoList.Any())
                    operationsFlux.OperationOutList.Add(operationFlux);

                HighChartSeries series = new HighChartSeries();
                series.name = getStringTrimmed(listLabel[0], 15);
                series.data = new List<float>();
                series.data.Add(getAmountPercentage(decimal.Parse(listAmount[0], CultureInfo.InvariantCulture), (dTotalAmountIN > dTotalAmountOUT) ? dTotalAmountIN : dTotalAmountOUT));
                series.qty = int.Parse(listQuantity[0]);
                series.amnt = listAmount[0] + " €";
                series.details = sbDetails.ToString().Replace("\r\n", "<br/>");

                listSeries.Add(series);
            }

            listSeries = listSeries.OrderBy(o => o.data[0]).ToList();
            opt.series = listSeries;

            for (int i = 0; i < listSeries.Count; i++)
            {
                listCategories.Add(listSeries[i].name);
            }
            opt.categories = listCategories;

            opt.categories = listCategories;
        }
        listOptions.Add(opt);

        //Génération du Flux pour le pavé 2
        if (operationsFlux.OperationInList.Any())
        {
            chaineFluxPave2.Append(string.Format("Entrée de flux :").ToUpper());


            foreach (var op in operationsFlux.OperationInList.OrderByDescending(o => o.Montant))
            {
                chaineFluxPave2.Append(string.Format(" {0}% ({1}€)", Math.Round(getAmountPercentage(op.Montant, operationsFlux.OperationInList.Sum(sum => sum.Montant)), 1, MidpointRounding.AwayFromZero), op.Montant));

                chaineFluxPave2.Append(string.Format(" EN {0} ", op.Libelle.ToUpper()));
                if (op.WhoList.Any())
                    chaineFluxPave2.Append(string.Concat("(", string.Join(", ", op.WhoList.ToArray()), ")."));
            }
            chaineFluxPave2.AppendLine(".");
        }

        txtFluxEntrant.Text = chaineFluxPave2.ToString();

        chaineFluxPave2.Clear();
        if (operationsFlux.OperationOutList.Any()) {
            
            chaineFluxPave2.Append("SORTIE DE FLUX :");

            
            foreach (var op in operationsFlux.OperationOutList.OrderByDescending(o=>o.Montant))
            {
                chaineFluxPave2.Append(string.Format(" {0}% ({1}€)", Math.Round(getAmountPercentage( op.Montant ,operationsFlux.OperationOutList.Sum(sum => sum.Montant)),1,MidpointRounding.AwayFromZero), op.Montant));
                
                chaineFluxPave2.Append(string.Format(" EN {0} ", op.Libelle.ToUpper()));
                if(op.WhoList.Any())
                    chaineFluxPave2.Append(string.Concat("(",string.Join(", ", op.WhoList.ToArray()),")."));
            }
        }
        txtFluxSortant.Text = chaineFluxPave2.ToString();
        
        string sOptionsJSON = JsonConvert.SerializeObject(listOptions);
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "InitGraphs(" + sOptionsJSON + ");", true);
    }

    protected float getOperationPercentage(int NbOperations,int Total)
    {
        float OperationPercentage = 0;

        try
        {
            OperationPercentage = (float.Parse(NbOperations.ToString()) / float.Parse(Total.ToString())) * 100;
        }
        catch (Exception ex)
        {

        }

        return OperationPercentage;
    }
    protected float getAmountPercentage(decimal dAmount, decimal dTotal)
    {
        float OperationPercentage = 0;

        try
        {
            OperationPercentage = (float.Parse(dAmount.ToString()) / float.Parse(dTotal.ToString())) * 100;
        }
        catch (Exception ex)
        {

        }

        return OperationPercentage;
    }

    protected string getStringTrimmed(string sValue, int iMaxLength)
    {
        string sTrimmedValue = sValue.Trim();

        if (sValue.Trim().Length > iMaxLength)
        {
            sTrimmedValue = sValue.Trim().Substring(0, iMaxLength - 1) + ".";
        }
        return sTrimmedValue;
    }

    /*** BOUTONS BAS DE PAGE ***/
    protected void btnAML_Click(object sender, EventArgs e)
    {
        int iRefAlert = getCurrentRefAlert();
        int iRefCustomer = getCurrentRefCustomer();
        tools.SetBackUrl("FastAnalysisSheet.aspx?ref=" + iRefAlert.ToString() + "&client=" + iRefCustomer.ToString(), "Fiche d'analyse rapide", "ClientDetails");
        Response.Redirect("ClientDetails.aspx?ref=" + iRefCustomer.ToString() + "&view=aml");
    }

    protected void btnAlertTreatmentSheet_Click(object sender, EventArgs e)
    {
        int iRefAlert = getCurrentRefAlert();
        int iRefCustomer = getCurrentRefCustomer();
        tools.SetBackUrl("FastAnalysisSheet.aspx?ref=" + iRefAlert.ToString() + "&client=" + iRefCustomer.ToString(), "Fiche d'analyse rapide", "AlertTreatmentSheet");

        Response.Redirect("AlertTreatmentSheet.aspx?ref=" + iRefAlert.ToString() + "&client=" + iRefCustomer.ToString());
    }

    protected void btnAccountStatement_Click(object sender, EventArgs e)
    {
        int iRefAlert = getCurrentRefAlert();
        int iRefCustomer = getCurrentRefCustomer();
        tools.SetBackUrl("FastAnalysisSheet.aspx?ref=" + iRefAlert.ToString() + "&client=" + iRefCustomer.ToString(), "Fiche d'analyse rapide", "ClientDetails");

        Response.Redirect("ClientDetails.aspx?ref=" + iRefCustomer.ToString() + "&view=operations");
    }

    protected void rblPeriodFlow_SelectedIndexChanged(object sender, EventArgs e)
    {
        setCustomerFlow();
    }

    protected void rblPeriodCashInOut_SelectedIndexChanged(object sender, EventArgs e)
    {
        setCustomerCashInOut();
    }

    protected void rblOperationByCategoryPeriod_SelectedIndexChanged(object sender, EventArgs e)
    {
        //setCustomerOperationsByCategory();
        SetCharts();

        //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "initClientOperationsStats();", true);
    }

    protected void checkPreviousButtonUrl()
    {
        string sUrl = "";
        string sButtonText = "";
        if (tools.GetBackUrl(out sUrl, out sButtonText))
        {
            btnPrev.Text = sButtonText;
            btnPrev.PostBackUrl = sUrl;
        }
        else
        {
            btnPrev.PostBackUrl = "SearchAlert.aspx";
        }
    }

    protected void btnAddComment_Click(object sender, EventArgs e)
    {
        if (!String.IsNullOrWhiteSpace(AmlComment.Commentary))
        {
            if (AML.addClientNote(authentication.GetCurrent().sToken, getCurrentRefCustomer(), AmlComment.Commentary.Trim()))
            {
                AmlComment.Commentary = "";
                BindAmlComments();
            }
        }
    }

    protected void btnAddKYCComment_Click(object sender, EventArgs e)
    {
        //AmlComment.CommentaryFocus();
        DateTime dateSouscr = DateTime.ParseExact(litInscriptionDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
        TimeSpan span = (DateTime.Now - dateSouscr);

        AmlComment.Commentary = String.Format("CONNAISSANCE CLIENT : {0} ans, {1}, {2}{3}{4}{5}, compte ouvert il y a {6} j.", litAge.Text, litCSP.Text, litHab.Text, (litRevenus.Text != "N.C.") ? ", Revenus " + litRevenus.Text : "", (litPatrimoine.Text != "N.C.") ? ", Patrimoine " + litPatrimoine.Text : "", AmlComment.Commentary, span.Days);
        if (AmlComment.Commentary.Length < _sizeMax)
        {
            int sizeTxt = (AmlComment.Commentary.Length + txtFluxEntrant.Text.Length);
            // La taille max est de 400 caractères
            if (sizeTxt > _sizeMax)
                AmlComment.Commentary = AmlComment.Commentary + " " + txtFluxEntrant.Text.Substring(0, (_sizeMax -1 ) - AmlComment.Commentary.Length);
            else
                AmlComment.Commentary = AmlComment.Commentary + " " + txtFluxEntrant.Text;
        }

        BindAMLCommentJS();
        //upAmlComment.Update();
    }

    protected void btnAddFluxComment_Click(object sender, EventArgs e)
    {
        int sizeTxt = AmlComment.Commentary.Length + txtFluxSortant.Text.Length;
        if (AmlComment.Commentary.Length < _sizeMax)
        {
            if (sizeTxt > _sizeMax)
                AmlComment.Commentary = AmlComment.Commentary + " " + txtFluxSortant.Text.Substring(0, (_sizeMax - 1 ) - AmlComment.Commentary.Length);
            else
                AmlComment.Commentary = String.Format(txtFluxSortant.Text);
        }
        BindAMLCommentJS();
    }

    /// <summary>
    /// Classe permettant de créer le flux pour le Pavé 2
    /// </summary>
    private class OperationsFlux
    {
        public List<OperationFlux> OperationInList { get; set; }
        public List<OperationFlux> OperationOutList { get; set; }
        public OperationsFlux()
        {
            OperationInList = new List<OperationFlux>();
            OperationOutList = new List<OperationFlux>();
        }
    }

    /// <summary>
    /// Classe permettant de créer le flux pour le Pavé 2
    /// </summary>
    private class OperationFlux
    {
        public string Libelle { get; set; }
        public decimal Montant { get; set; }
        public decimal Pourcentage { get; set; }
        public List<string> WhoList { get; set; }

        public OperationFlux()
        {
            WhoList = new List<string>();
        }

    } 
}