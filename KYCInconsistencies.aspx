﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="KYCInconsistencies.aspx.cs" Inherits="KYCInconsistencies" %>

<%@ Register Src="~/API/ClientSearch.ascx" TagPrefix="asp" TagName="ClientSearch"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">

    <link rel="Stylesheet" href="Styles/KYCInconsistencies.css" />

    <script type="text/javascript">
        function ShowAlertDetails(ref) {
            console.log(ref);
            $('#<%=hdnRefAlertDetails.ClientID%>').val(ref);
            $('#<%=btnShowAlertDetails.ClientID%>').click();
        }
        function ShowAlertDetailsPopup() {
            $('#div-alert-details').dialog({
                autoOpen: true,
                resizable: false,
                draggable: false,
                modal: true,
                title: "Détails de l'alerte",
                close: function (event, ui) {
                    $(this).dialog('destroy');
                }
            });
            $("#div-alert-details").parent().appendTo(jQuery("form:first"));
        }
        function CloseAlertDetailsPopup() {
            $('#div-alert-details').dialog('close');
        }
        function RefreshAlertDetailsPopup(ref) {
            $('#div-alert-details').dialog('close');
            ShowAlertDetails(ref);
        }
        function ApplySpecification(alertTag, ref) {
            console.log('ApplySpecification, ' + alertTag + "," + ref);
            $('#<%=hfRefCustomer.ClientID%>').val("");
            $('#<%=hfAlertTAG.ClientID%>').val("");
            if (alertTag.trim().length > 0 && ref.toString().trim().length > 0) {
                $('#<%=hfRefCustomer.ClientID%>').val(ref);
                $('#<%=hfAlertTAG.ClientID%>').val(alertTag);
                $('#<%=btnApplySpecification.ClientID%>').click();
            }
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

    <div id="ar-loading" class="kyc-inconsistencies-loading"></div>

    <div id="KYCInconstencies">
        <asp:UpdatePanel ID="upKycInconsistencies" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div>
                    <asp:LinkButton ID="lbDeleteFilters" runat="server" CssClass="orangeLink" OnClick="lbDeleteFilters_Click">Appliquer filtres par défaut</asp:LinkButton>
                </div>
                <div class="filters" style="width:70%">
                    <div>
                        <asp:CheckBox ID="ckbShowOnlyMine" runat="server" Text="Mes alertes réservées" OnCheckedChanged="RefreshList_event" AutoPostBack="true" />
                    </div>
                    <div style="align-self: auto;">
                        <div class="label">Traité</div>
                        <div>
                            <asp:RadioButtonList ID="rblTreated" runat="server" RepeatDirection="Horizontal" OnSelectedIndexChanged="RefreshList_event" AutoPostBack="true">
                                <asp:ListItem Text="Non" Value="0" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Oui" Value="1"></asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                    </div>
                    <div> 
                        <div class="label">Par client</div>
                        <div style="padding-top:5px;">
                            <asp:ClientSearch ID="csClientSearchFilter" runat="server" />
                        </div>
                    </div>
                </div>
                <div>
                    <div style="width:40%;float:left">
                        <asp:Literal ID="litNbResults" runat="server"></asp:Literal> résultat(s)
                    </div>
                    <div style="width:40%;float:right;text-align:right">
                        Nb de résultats par page :
                        <asp:DropDownList ID="ddlPageSize" runat="server" AutoPostBack="true" OnSelectedIndexChanged="RefreshList_event">
                            <asp:ListItem Value="10" Selected="True"></asp:ListItem>
                            <asp:ListItem Value="25"></asp:ListItem>
                            <asp:ListItem Value="50"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div style="clear:both"></div>
                </div>
                <div id="div-kyc-inconsistencies">
                    <asp:Repeater ID="rptKycInconsistencies" runat="server">
                        <HeaderTemplate>
                            <div class="list-kyc-inconsistencies-border">
                                <table class="list-kyc-inconsistencies">
                                    <thead>
                                        <tr>
                                            <th>Type</th>
                                            <th>Client(s)</th>
                                            <th>Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                                        <tr onclick='<%# String.Format("ShowAlertDetails({0});", Eval("iRefAlert")) %>'>
                                            <td><%#GetAlertTypeLabel(Eval("sAlertIdTAG").ToString()) %></td>
                                            <td><%#ParseCustomerListToHTML(Eval("sCustomerList").ToString()) %></td>
                                            <td><%#Eval("dtAlertDate") %></td>
                                        </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                                    </tbody>
                                </table>
                            </div>
                        </FooterTemplate>
                    </asp:Repeater>
                    <asp:Panel ID="panelPagination" runat="server" style="text-align:center">
                        <asp:Button ID="btnPrevPage" runat="server" CommandArgument="prev" OnClick="btnChangePage_Click" Text="<" Font-Bold="true" Width="30px" />
                        <asp:DropDownList ID="ddlPageIndex" runat="server" DataTextField="T" DataValueField="V" AutoPostBack="true" OnSelectedIndexChanged="RefreshList_event"></asp:DropDownList>
                        /
                        <asp:Literal ID="litTotalPages" runat="server"></asp:Literal>
                        <asp:Button ID="btnNextPage" runat="server" CommandArgument="next" OnClick="btnChangePage_Click" Text=">" Font-Bold="true" Width="30px" />
                    </asp:Panel>
                </div>

                

                <asp:HiddenField ID="hdnRefAlertDetails" runat="server" />
                <asp:Button ID="btnShowAlertDetails" runat="server" OnClick="btnShowAlertDetails_Click" style="display:none" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <div id="div-alert-details" style="display:none">
        <asp:UpdatePanel ID="upAlertDetails" runat="server">
            <ContentTemplate>
                <asp:HiddenField ID="hdnRefAlert" runat="server" />
                <asp:Button ID="btnReserveUnreserve" runat="server" CssClass="MiniButton" OnClick="btnReserveUnreserve_Click" />
                <div>
                    <div class="label">
                        Date
                    </div>
                    <div>
                        <asp:Literal ID="litDate" runat="server"></asp:Literal>
                    </div>
                </div>
                <div>
                    <div class="label">
                        Type
                    </div>
                    <div>
                        <asp:Literal ID="litAlertType" runat="server"></asp:Literal>
                    </div>
                </div>
                <div>
                    <div class="label">
                        Client(s)
                    </div>
                    <div>
                        <asp:Literal ID="litCustomer" runat="server"></asp:Literal>
                    </div>
                </div>
                <div>
                    <div class="label">
                        Commentaire
                    </div>
                    <div>
                        <asp:TextBox ID="txtComment" runat="server" TextMode="MultiLine"></asp:TextBox>
                    </div>
                </div>
                <div>
                    <div class="label">
                        Etat
                    </div>
                    <div>
                        <asp:DropDownList ID="ddlStatus" runat="server">
                            <asp:ListItem Value="_PAUSED_" Text="En attente"></asp:ListItem>
                            <asp:ListItem Value="_CHECKED_" Text="Fausse alerte"></asp:ListItem>
                            <asp:ListItem Value="_TREATED_" Text="Traitée"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div style="margin-top:10px">
                    <asp:Button ID="btnSave" runat="server" Text="Enregistrer" CssClass="button" OnClick="btnSave_Click" />
                </div>
                            
                <asp:HiddenField ID="hfAlertTAG" runat="server" />
                <asp:HiddenField ID="hfRefCustomer" runat="server" />
                <asp:Button ID="btnApplySpecification" runat="server" OnClick="btnApplySpecification_Click" style="display:none" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" Runat="Server">

    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            console.log(sender);
            console.log(args);
            pbControl = args.get_postBackElement();
            var panel;
            if (pbControl != null && pbControl.id != null) {
                console.log(pbControl.id);
                console.log('<%=btnNextPage.ClientID%>');
                if (pbControl.id == '<%=btnSave.ClientID %>' ||
                    pbControl.id == '<%=btnReserveUnreserve.ClientID%>' ||
                    pbControl.id == '<%=btnApplySpecification.ClientID%>')
                    panel = '.ui-dialog';
                else if (pbControl.id == '<%=ckbShowOnlyMine.ClientID%>' ||
                    pbControl.id == '<%=ddlPageIndex.ClientID%>' ||
                    pbControl.id == '<%=ddlPageSize.ClientID%>' ||
                    pbControl.id == '<%=btnShowAlertDetails.ClientID%>' ||
                    pbControl.id == '<%=btnPrevPage.ClientID%>' ||
                    pbControl.id == '<%=btnNextPage.ClientID%>')
                        
                    panel = '#div-kyc-inconsistencies';
                else {
                    panel = '#KYCInconstencies';
                }

                ShowLoading(panel);
            }
        }
        function ShowLoading(panel) {
            console.log('showloading');
            console.log(panel);
            if (panel != null && panel.length > 0) {
                var width = $(panel).outerWidth();
                var height = $(panel).outerHeight();
                $('#ar-loading').css('width', width);
                $('#ar-loading').css('height', height);
                $('#ar-loading').show();
                $('#ar-loading').position({ my: "left bottom", at: "left bottom", of: panel });
            }
        }
        function EndRequestHandler(sender, args) {
            HideLoading();
        }
        function HideLoading() {
            $('#ar-loading').hide();
        }
    </script>

</asp:Content>