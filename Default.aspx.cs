﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using XMLMethodLibrary;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindMenu();
        }
    }

    protected void BindMenu()
    {
        DataTable dtMenu = new DataTable();
        dtMenu.Columns.Add("Ref");
        dtMenu.Columns.Add("Label");
        dtMenu.Columns.Add("Value");

        string sXmlOut = tools.GetRights(authentication.GetCurrent().sToken, "SC_Menu");

        List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Page", "RC");

        if (listRC.Count > 0 && listRC[0] == "0")
        {
            List<string> listAction = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/Page/Action");
            
            if (authentication.GetCurrent().sTagProfile.ToUpper() == "ADMIN"
                || authentication.GetCurrent().sEmail == "cyriane.lacoste@compte-nickel.fr"
                || authentication.GetCurrent().sEmail == "aurelie.gaillard@compte-nickel.fr"
                || authentication.GetCurrent().sEmail == "charlene.gimblett@compte-nickel.fr"
                || authentication.GetCurrent().sEmail == "michael.marchant@compte-nickel.fr")
                listAction.Add("<Action TagAction=\"Logistic\" ViewAllowed = \"1\" />");
            bool bClientMessage = false;

            for (int i = 0; i < listAction.Count; i++)
            {
                List<string> listTagAction = CommonMethod.GetAttributeValues(listAction[i], "Action", "TagAction");
                List<string> listViewAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ViewAllowed");

                if (listTagAction.Count > 0 && listViewAllowed.Count > 0 && listViewAllowed[0] == "1")
                {
                    switch (listTagAction[0])
                    {
                        case "CustomerSearch":
                            dtMenu.Rows.Add(new object[] { "01", "RECHERCHE CLIENT", "ClientSearch.aspx" });
                            break;
                        case "AgencySearch":
                            dtMenu.Rows.Add(new object[] { "07", "RECHERCHE AGENCE", "AgencySearch.aspx" });
                            break;
                        case "RegistrationSearch":
                            dtMenu.Rows.Add(new object[] { "02", "RECHERCHE INSCRIPTION", "RegistrationSearch.aspx" });
                            break;
                        case "RegistrationControlSearch":
                            dtMenu.Rows.Add(new object[] { "02", "RECHERCHE CONTROLE INSCRIPTION", "RegistrationSearchV2.aspx" });
                            break;
                        case "StoreLocator":
                            dtMenu.Rows.Add(new object[] { "09", "CARTE DES POINTS DE VENTE", "StoreLocator.aspx" });
                            break;
                        case "SellerPincodeReset":
                            dtMenu.Rows.Add(new object[] { "10", "R&Eacute;INITIALISATION CODE PIN VENDEUR", "PincodeReset.aspx" });
                            break;
                        case "SearchAlert":
                            dtMenu.Rows.Add(new object[] { "03", "RECHERCHE ALERTE", "SearchAlert.aspx" });
                            break;
                        case "HomeMessage":
                        case "ClientMessage":
                            if (!bClientMessage)
                            {
                                dtMenu.Rows.Add(new object[] { "06", "MESSAGE CLIENT", "ClientMessage.aspx" });
                                bClientMessage = true;
                            }
                            break;
                        case "ManageProMessage":
                            dtMenu.Rows.Add(new object[] { "11", "MESSAGE BURALISTE", "AgencyMessage.aspx" });
                            break;
                        case "Tools":
                            dtMenu.Rows.Add(new object[] { "14", "OUTILS", "Tools.aspx" });
                            break;
                        case "UserManagement":
                            dtMenu.Rows.Add(new object[] { "12", "GESTION DES UTILISATEURS", "UserBOManagement.aspx" });
                            break;
                        case "TransferSettings":
                            dtMenu.Rows.Add(new object[] { "05", "R&Eacute;GLAGES CONFORMIT&Eacute;", "ComplianceSettings.aspx" });
                            break;
                        case "AgencyAlert":
                            dtMenu.Rows.Add(new object[] { "08", "RECHERCHE INCIDENT KYC", "AgencyAlertSearch.aspx" });
                            break;
                        case "PendingTransfer":
                            dtMenu.Rows.Add(new object[] { "04", "RECHERCHE VIREMENT", "BlockedTransfer.aspx" });
                            break;
                        case "GarnishmentSearch":
                            dtMenu.Rows.Add(new object[] { "13", "RECHERCHE AVIS", "GarnishmentSearch.aspx" });
                            break;
                        case "RequisitionDroitCommunication":
                            dtMenu.Rows.Add(new object[] { "041", "R&Eacute;QUISITION & DROIT DE COMMUNICATION", "RequisitionDroitCommunication.aspx" });
                            break;
                        case "KYCInconsistencies":
                            dtMenu.Rows.Add(new object[] { "15", "INCOHERENCES KYC", "KYCInconsistencies.aspx" });
                            break;
                        case "SearchTracfinStatement":
                            dtMenu.Rows.Add(new object[] { "16", "RECHERCHE D&Eacute;CLARATION TRACFIN", "SearchTracfinStatement.aspx" });
                            break;
                        case "Logistic":
                            dtMenu.Rows.Add(new object[] { "17", "LOGISTIQUE", "LogistiqueCommande.aspx" });
                            break;
						case "BoCobalt":
                            
                            break;
                    }
                }
            }
        }

        //Droits spécifiques CRE
        if (CRETools.CheckRights(authentication.GetCurrent().sToken))
        {
            //dtMenu.Rows.Add(new object[] { "05", "BO COBALT", "BoCobalt.aspx" });
            dtMenu.Rows.Add(new object[] { "05", "OP&Eacute;RATIONS COMPTABLES (CRE)", "CreOrder.aspx" });
        }

        if (dtMenu.Rows.Count > 0)
        {
            DataView dv = dtMenu.DefaultView;
            dv.Sort = "Ref asc";

            rptMenu.DataSource = dv.ToTable();
            rptMenu.DataBind();
        }
    }
}
