﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.master" CodeFile="BoCobalt.aspx.cs" Inherits="BoCobalt" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2 style="color:#344b56; margin-bottom:10px; text-transform:uppercase">
        BO COBALT
    </h2>
    <div style="padding:20px 0;">
        <div style="margin:auto;border:4px solid #f57527; border-radius:8px; display:table; width:80%">
            <%--<asp:TreeView runat="server" ID="tvOperationType" ImageSet="Custom" NodeStyle-CssClass="classMenu"></asp:TreeView>--%>

            <asp:Repeater ID="rptMenu" runat="server">
                <ItemTemplate>
                    <div style="display:table-row">
                        <div style="display:table-cell;height:40px; vertical-align:middle; text-align:center;" class="classMenu" onclick='document.location.href="<%#Eval("Value") %>"'>
                            <%#Eval("Label") %>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>

        </div>
    </div>
</asp:Content>

