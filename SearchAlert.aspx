﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="SearchAlert.aspx.cs" Inherits="SearchAlert" EnableEventValidation="false" %>

<%@ Register Src="~/API/AmlComment.ascx" TagName="AmlComment" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <script type="text/javascript">
        function init() {
            initTooltip();
            $('.RadioButtonList').buttonset();
        }

        function BlockedTransfer_CheckMessages() {
            if ($('#<%=hdnMessage.ClientID%>').val().length > 0) {
                $('#message').empty().append($('#<%=hdnMessage.ClientID%>').val());
                $('#popup-message').dialog({
                    autoOpen: true,
                    resizable: false,
                    draggable: false,
                    modal: true,
                    zIndex: 10000,
                    title: "Message",
                    buttons: [{ text: 'Fermer', click: function () { $(this).dialog('close'); $(this).dialog('destroy'); } }]
                });
                $('#<%=hdnMessage.ClientID%>').val('');
            }
        }

        function AlertFastAnalysis(refAlert, refCustomer, status, reserve) {
            $('#<%=hdnRefCustomerSelected.ClientID%>').val(refCustomer);
            $('#<%=hdnRefAlertSelected.ClientID%>').val(refAlert);
            if (status.toLowerCase() == "en attente") {
                if (reserve) {
                    $("#dialog-alert-treatment-reservation").dialog({
                        autoOpen: false,
                        resizable: false,
                        draggable: false,
                        modal: true,
                        zIndex: 10000,
                        minWidth: 380,
                        title: "Assignation"
                    });

                    if($('#<%=btnShowAlertWithoutCharge.ClientID%>').length > 0)
                    {
                        $("#dialog-alert-treatment-reservation").dialog('option',
                            'buttons', [
                            { text: 'Voir', click: function () { showLoading(); $('#<%=btnShowAlertWithoutCharge.ClientID%>').click(); $(this).dialog('close'); } },
                            { text: "Je m'en occupe", click: function () { showLoading(); $('#<%=btnAlertTreatmentReservation.ClientID%>').click(); $(this).dialog('close'); $(this).dialog('destroy'); } }]
                        );
                    }
                    else {
                        $("#dialog-alert-treatment-reservation").dialog('option',
                            'buttons', [
                            //{ text: 'Voir', click: function () { showLoading(); $('#<%=btnShowAlertWithoutCharge.ClientID%>').click(); $(this).dialog('close'); } },
                            { text: "Je m'en occupe", click: function () { showLoading(); $('#<%=btnAlertTreatmentReservation.ClientID%>').click(); $(this).dialog('close'); $(this).dialog('destroy'); } }]
                        );
                    }
                    $("#dialog-alert-treatment-reservation").dialog('open');

                    $("#dialog-alert-treatment-reservation").parent().appendTo(jQuery("form:first"));
                }
                else { $('#<%=btnAlertTreatmentReservation.ClientID%>').click(); }
            }
            else { document.location = "FastAnalysisSheet.aspx?ref=" + refCustomer.toString(); }
        }

        function AlertTreatmentUnassign(refAlert, refCustomer, refAgent, agentName) {
            $('#<%=hdnRefCustomerSelected.ClientID%>').val(refCustomer);
            $('#<%=hdnRefAlertSelected.ClientID%>').val(refPendingTransfer);
            $("#dialog-alert-treatment-unreservation").dialog({
                autoOpen: true,
                resizable: false,
                draggable: false,
                modal: true,
                pminWidth: 380,
                zIndex: 10000,
                title: "Assignation",
                buttons: [{ text: 'Annuler', click: function () { $(this).dialog('close'); $(this).dialog('destroy'); } }, { text: 'Désaffecter', click: function () { $('#<%=btnAlertTreatmentUnreservation.ClientID%>').click(); $(this).dialog('close'); $(this).dialog('destroy'); } }]
            });
            $("#dialog-alert-treatment-unreservation").parent().appendTo(jQuery("form:first"));

            //console.log('current refAgent*' + $('#<%=hdnCurrentRefAgent.ClientID%>').val().trim() + '*');
            //console.log('refAgent*' + refAgent + '*');
            $('#label-alert-treatment-unreservation').html("Le traitement de cette opération est déjà affecté.<br/>Voulez-vous la désaffecter?");
            if(agentName.trim().length > 0)
                $('#label-alert-treatment-unreservation').html("Le traitement de cette opération est affecté à " + agentName + ".<br/>Voulez-vous lui désaffecter?");
            if($('#<%=hdnCurrentRefAgent.ClientID%>').val().trim() == refAgent)
                $('#label-alert-treatment-unreservation').html("Vous ne vous occupez plus de cette opération?");

        }

        function ShowDREOKConfirmationDialog(refAlert) {
            $('#<%=hdnRefAlertSelected.ClientID%>').val(refAlert);
            $('#MainContent_amlComment_txtComment').val('');
            $('#MainContent_amlComment_lblNbChar').html('0');
            $("#dialog-dreok-confirmation").dialog({
                autoOpen: true,
                resizable: false,
                draggable: false,
                modal: true,
                minWidth: 380,
                zIndex: 10000,
                title: "Confirmez le statut DREOK",
                buttons: [{ text: 'Annuler', click: function () { $(this).dialog('close'); $(this).dialog('destroy'); } }, { text: 'Confirmer', click: function () { $('#<%=btnChangeStatusDREOK.ClientID%>').click(); $(this).dialog('close'); $(this).dialog('destroy'); } }]
            });
            $("#dialog-dreok-confirmation").parent().appendTo(jQuery("form:first"));
        }

        function SelectGroupAlert(ref) {
            $('#<%=hdnRefAlertGroupSelected.ClientID%>').val(ref);
            $('#<%=btnSelectRefAlertGroup.ClientID%>').click();
        }

        function SelectSortExpression(expr) {
            $('#<%=hdnSort.ClientID%>').val(expr);
            $('#<%=btnSort.ClientID%>').click();
        }
    </script>
    <style type="text/css">
        .page {
            width:1100px;
        }
        #ar-loading {
            background:url('Styles/img/loading2.gif') no-repeat center;
            background-size: 35px 27px;
            display:none;
            position:absolute;
            background-color:rgba(255, 255, 255, 0.5);
            z-index:100000;
        }

        table th a {
            color: #fff!important;
            text-decoration: none!important;
        }
        .GridViewRowStyle td {
            position:relative;
            height: 69px;
            box-sizing: border-box;
        }
        .GridViewRowStyle:hover .dreok-quick-access {
            display:initial;
        }
        .dreok-quick-access {
            display:none;
            position:absolute;
            right: -83px;
            top: -1px;
            height:69px;
            line-height:69px;
        }
        .dreok-quick-access input[type=button] {
            border-top-left-radius:0;
            border-bottom-left-radius:0;
            width: 80px;
            height:85px;
        }
        .no-padding {
            padding:0 !important;
        }

        .gv-alert-group {

        }
        .gv-alert-group tr:first-of-type {
            border-top:0;
        }
        .gv-alert-group tr:first-of-type td {
            background-color:#344b56;
            color:#fff;
            text-align:center;
            font-weight:bold;
            border-top:0;
            height:auto;
        }
        .gv-alert-group tr td:first-of-type {
            display:none;
        }

        .arrow-up, .arrow-down {
            width: 0;
            height: 0;
            display: inline-block;
            border-left: 6px solid transparent;
            border-right: 6px solid transparent;
        }
        .arrow-up {
            border-bottom: 6px solid #fff;
        }
        .arrow-down {
            border-top: 6px solid #fff;
        }

        .position-relative {
            position:relative;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <h2 style="color: #344b56;margin-bottom:10px;text-transform:uppercase">Recherche alerte</h2>
    <div id="loading-area">
        <asp:UpdatePanel ID="upSearch" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnPreviousPage" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnNextPage" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="ddlPage" EventName="SelectedIndexChanged" />
                    <asp:AsyncPostBackTrigger ControlID="ddlNbResult" EventName="SelectedIndexChanged" />
                    <asp:AsyncPostBackTrigger ControlID="ddlAlertType" EventName="SelectedIndexChanged" />
                    <asp:AsyncPostBackTrigger ControlID="rblGroupType" EventName="SelectedIndexChanged" />
                    <asp:AsyncPostBackTrigger ControlID="ddlGroupTagAlert" EventName="SelectedIndexChanged" />
                    <asp:AsyncPostBackTrigger ControlID="btnSelectRefAlertGroup" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnSort" EventName="Click" />
                </Triggers>
                <ContentTemplate>
                    <asp:UpdatePanel ID="upFilters" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:RadioButtonList ID="rblGroupType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="rblGroupType_SelectedIndexChanged" CssClass="RadioButtonList" RepeatDirection="Horizontal">
                                <asp:ListItem Text="Simple" Value="0" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Groupée" Value="1"></asp:ListItem>
                            </asp:RadioButtonList>

                            <div style="margin-top:10px">
                                <asp:Panel ID="panelFiltersMultiple" runat="server" Visible="false">
                                    <div class="table">
                                        <div class="row">
                                            <div class="cell" style="padding-right:5px;">
                                                Type alerte :
                                            </div>
                                            <div class="cell">
                                                <asp:DropDownList ID="ddlGroupTagAlert" runat="server" DataValueField="GroupTAG" DataTextField="GroupName" AutoPostBack="true" OnSelectedIndexChanged="ddlGroupTagAlert_SelectedIndexChanged" AppendDataBoundItems="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>

                                <asp:Panel ID="panelFiltersSingle" runat="server">
                                    <div class="table">
                                        <div class="row">
                                            <div class="cell" style="padding-right:5px;">
                                                Type alerte :
                                            </div>
                                            <div class="cell">
                                                <asp:DropDownList ID="ddlAlertType" runat="server" DataTextField="TypeAlerte" DataValueField="TypeAlerte"
                                                    AutoPostBack="true" AppendDataBoundItems="true" OnSelectedIndexChanged="onChangeAlertType" Visible="false">
                                                </asp:DropDownList>
                                                <asp:DropDownList ID="ddlTagAlert" runat="server" DataTextField="Subject" DataValueField="TagAlerte"
                                                    AutoPostBack="true" AppendDataBoundItems="true" OnSelectedIndexChanged="onChangeAlertType">
                                                </asp:DropDownList>

                                            </div>
                                        </div>
                                    </div>
                                    <asp:DropDownList Visible="false" ID="ddlProfile" runat="server" AutoPostBack="true" AppendDataBoundItems="true"
                                        DataTextField="Profil" DataValueField="Profil">
                                        <asp:ListItem Value="">Tous</asp:ListItem>
                                    </asp:DropDownList>
                                </asp:Panel>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="rblGroupType" EventName="SelectedIndexChanged" />
                        </Triggers>
                    </asp:UpdatePanel>

                    <asp:Panel ID="panelGroupResult" runat="server" Visible="false" style="border: 3px solid #344b56; border-radius: 8px;margin-top:20px">
                        <asp:GridView ID="gvGroupResult" runat="server" AutoGenerateColumns="true" ShowHeader="false" AllowSorting="false"
                                AllowPaging="false" GridLines="None" CellPadding="10" Width="100%" SortedDescendingCellStyle-Font-Overline="true"
                                ViewStateMode="Disabled" BorderWidth="0" BackColor="Transparent" OnRowDataBound="gvGroupResult_RowDataBound" CssClass="gv-alert-group">
                            <RowStyle CssClass="GridViewRowStyle" />
                            <Columns>
                                <asp:TemplateField Visible="false" InsertVisible="false">
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hdnRefAlertGroup" runat="server" Value='<%# Eval("RefAlertGroup")%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <asp:Panel ID="panelGroupNoResult" runat="server" Visible="false" HorizontalAlign="Center" BackColor="#344b56" ForeColor="White" style="padding:10px 0">
                            Aucun résultat
                        </asp:Panel>
                        <asp:HiddenField ID="hdnRefAlertGroupSelected" runat="server" />
                        <asp:Button ID="btnSelectRefAlertGroup" runat="server" OnClick="btnSelectRefAlertGroup_Click" style="display:none" />
                    </asp:Panel>

                    <div style="margin-top:15px">
                        <asp:Button ID="btnBackToGroup" runat="server" Text="< Revenir au groupe" Visible="false" OnClick="btnBackToGroup_Click" CssClass="MiniButton" style="margin-right:10px;" />
                    </div>

                    <asp:Panel ID="panelGrid" runat="server" Visible="false">
                        <div id="divGridSettings" runat="server" visible="false" style="display: table; width: 100%; margin-bottom: 10px">
                            <div style="display: table-row">
                                <div style="display: table-cell; width: 50%; padding-top: 15px">
                                    <asp:Label ID="lblNbOfResults" runat="server">0</asp:Label>
                                    résultat(s)
                                </div>
                                <div style="display: table-cell; width: 50%; text-align: right;">
                                    Nb de résultats par page :
                                    <asp:DropDownList ID="ddlNbResult" runat="server" AutoPostBack="true" OnSelectedIndexChanged="onChangeNbResult">
                                        <asp:ListItem Value="10" Selected="True">10</asp:ListItem>
                                        <asp:ListItem Value="25">25</asp:ListItem>
                                        <asp:ListItem Value="50">50</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div style="border: 3px solid #344b56; border-top-left-radius: 8px; border-top-right-radius: 8px;margin-top:5px">
                            <asp:GridView ID="gvClientAlertList" runat="server" AllowSorting="false"
                                AllowPaging="false" AutoGenerateColumns="False" GridLines="None" CellPadding="10"
                                DataKeyNames="RefCustomer" ShowHeader="true" Width="100%" SortedDescendingCellStyle-Font-Overline="true"
                                ViewStateMode="Disabled" BorderWidth="0" BackColor="Transparent" OnRowDataBound="gvClientAlertList_RowDataBound">
                                <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="#344b56" Font-Bold="true" />
                                <EmptyDataTemplate>
                                    Aucune alerte
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField Visible="false" InsertVisible="false">
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hdnRefCustomer" runat="server" Value='<%# Eval("RefCustomer")%>' />
                                            <asp:HiddenField ID="hdnRefAlert" runat="server" Value='<%# Eval("RefAlert") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="" InsertVisible="false" ItemStyle-Width="0px" ItemStyle-CssClass="no-padding">
                                        <ItemTemplate>
                                            <asp:Image ID="imgLock" runat="server" ImageUrl="~/Styles/Img/no-billets.png" Height="25px" CssClass="trimmer tooltip" style="width:auto"
                                                Visible='<%#(Eval("LockReason").ToString().Length == 0) ? false : true %>'
                                                ToolTip='<%#Eval("LockReason") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="AlertWatched" HeaderText="" InsertVisible="false" ItemStyle-Width="0px" ItemStyle-CssClass="no-padding">
                                        <ItemTemplate>
                                            <asp:Image ID="imgWatched" runat="server" ImageUrl="./Styles/Img/orange-eye.png" style="width:30px;" CssClass="trimmer tooltip"
                                                Visible='<%# AML.getWatchStatus(Eval("Watched").ToString()) %>'
                                                ToolTip='<%# AML.getWatchDetails(Eval("WatchedBy").ToString(),Eval("WatchedDate").ToString()) %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Nom" SortExpression="LastName" InsertVisible="false"
                                        ControlStyle-CssClass="trimmer tooltip" ControlStyle-Width="100">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblClientLastName" Text='<%# Eval("LastName")%>' CssClass="tooltip"
                                                ToolTip='<%# Eval("LastName")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Prénom" SortExpression="FirstName" InsertVisible="false"
                                        ControlStyle-CssClass="trimmer tooltip" ControlStyle-Width="100">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblClientFirstName" Text='<%# Eval("FirstName") %>'
                                                CssClass="tooltip" ToolTip='<%# Eval("FirstName")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Age" SortExpression="Age" InsertVisible="false" HeaderStyle-Width="1">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblClientAge" Text='<%# Eval("Age") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="CP" SortExpression="CodePostal" InsertVisible="false" HeaderStyle-Width="1">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblZipCode" Text='<%# Eval("CodePostal") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Profil" SortExpression="Profil" InsertVisible="false" HeaderStyle-Width="1" ControlStyle-CssClass="tooltip">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblProfil" Text='<%# Eval("Profil") %>' ToolTip='<%# GetCustomerXMLInfos(Eval("CustomerXML").ToString()) %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date souscr." SortExpression="CreationDate" InsertVisible="false" HeaderStyle-Width="1">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblCreationDate" Text='<%# Eval("CreationDate") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="AlertDescription" HeaderText="Description" InsertVisible="false"
                                        ControlStyle-CssClass="trimmer tooltip" ControlStyle-Width="130">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblAlertDescription" Text='<%# Eval("AlertDescription")%>'
                                                CssClass="tooltip" ToolTip='<%# Eval("AlertDescription")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="Alert_Details" HeaderText="Détails" InsertVisible="false" ItemStyle-HorizontalAlign="Left" ControlStyle-CssClass="tooltip">
                                        <ItemTemplate>
                                            <%# getAmlDetails(Eval("CategoryTAG").ToString(), Eval("Critere_INT1").ToString(), Eval("Critere_INT2").ToString(), Eval("Critere_VARCHAR1").ToString(), Eval("Critere_MONEY1").ToString(), Eval("TransferXML").ToString())%>
                                            <asp:HiddenField ID="hdnRowStyle" runat="server" Value='<%# getRowStyle(Eval("CategoryTAG").ToString(), Eval("Critere_INT1").ToString(), Eval("Critere_INT2").ToString(), Eval("Critere_VARCHAR1").ToString(), Eval("Critere_MONEY1").ToString())%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="AlertDescription" InsertVisible="false" HeaderStyle-Width="1px" ItemStyle-HorizontalAlign="Right">
                                        <HeaderTemplate>
                                            <asp:LinkButton ID="lnkDate" runat="server" OnClientClick="SelectSortExpression('AlertDate');return false;">
                                                Date
                                            </asp:LinkButton>
                                            <div class='<%=GetSortDirection("AlertDate") %>'></div>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblAlertDate" Text='<%# tools.getFormattedDate(Eval("AlertDate").ToString())%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="ClientScore" InsertVisible="false" HeaderStyle-Width="1" HeaderStyle-CssClass="position-relative" ItemStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            <asp:LinkButton ID="lnkClientScore" runat="server" OnClientClick="SelectSortExpression('ClientScore');return false;">
                                                Scoring client
                                            </asp:LinkButton>
                                            <div class='<%=GetSortDirection("ClientScore") %>' style="position:absolute;bottom:13px;right:1px;"></div>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblClientScore" Text='<%# Eval("ClientScore") %>'></asp:Label>
                                            <%--<asp:Label runat="server" ID="lblAlertScore" Text='<%# Eval("AlertScore") %>'></asp:Label>
                                            <asp:Label runat="server" ID="lblGlobalScore" Text='<%# Eval("GlobalScore") %>'></asp:Label>--%>

                                            <asp:Panel ID="panelQuickAccess" runat="server" CssClass="dreok-quick-access" Visible='<%# (Eval("AlertTag").ToString() == "OPE_VIR_IN_FRACT") ? true : false %>'>
                                                <input type="button" value="DREOK" class="button" onclick='ShowDREOKConfirmationDialog(<%#Eval("RefAlert")%>); event.stopPropagation();' />
                                            </asp:Panel>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle BackColor="#344b56" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#344b56" Font-Bold="True" ForeColor="White" Height="25px" />
                                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                <RowStyle CssClass="GridViewRowStyle" />
                                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                <%--<PagerStyle CssClass="GridViewPager" />--%>
                            </asp:GridView>
                            
                            <asp:HiddenField ID="hdnSort" runat="server" />
                            <asp:Button ID="btnSort" runat="server" style="display:none" OnClick="btnSort_Click" />
                        </div>
                        <div style="border: 3px solid #344b56; border-bottom-left-radius: 8px; border-bottom-right-radius: 8px; height: 25px; background-color: #344b56; color: White; ">
                            <asp:Panel ID="divPagination" runat="server" Visible="false" Style="width: 100%; display: table; margin: auto">
                                <div style="display: table-row;">
                                    <div style="display: table-cell; height: 20px; vertical-align: middle; width: 57%; text-align: right">
                                        <asp:Button ID="btnPreviousPage" runat="server" onclick="previousPage" Text="<" Font-Bold="true" Width="30px" />
                                        <asp:DropDownList ID="ddlPage" runat="server" OnSelectedIndexChanged="onChangePageNumber" AutoPostBack="true"
                                            DataTextField="L" DataValueField="V">
                                        </asp:DropDownList>
                                        <asp:Label ID="lblTotalPage" runat="server" Style="font-weight: bold"></asp:Label>
                                        <asp:Button ID="btnNextPage" runat="server" OnClick="nextPage" Text=">" Font-Bold="true" Width="30px" />
                                    </div>
                                    <div style="display: table-cell; padding-left: 5px">
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                    </asp:Panel>

                    <asp:HiddenField ID="hdnMessage" runat="server" />
                    <asp:HiddenField ID="hdnCurrentRefAgent" runat="server" />
                    <asp:HiddenField ID="hdnRefAlertSelected" runat="server" />
                    <asp:HiddenField ID="hdnRefCustomerSelected" runat="server" />
                    <asp:HiddenField ID="hdnAlertTreatmentActionSelected" runat="server" />

                    <asp:Panel ID="panelError" runat="server" Visible="false" HorizontalAlign="Center" BackColor="#344b56" ForeColor="White" style="border-radius:8px;margin-top:20px;padding:10px 0">
                        <asp:Label ID="lblError" runat="server"></asp:Label>
                    </asp:Panel>

                    <div id="dialog-alert-treatment-reservation" style="display: none">
                        Vous allez accéder à la fiche d'analyse rapide du client.<br />
                        <span style="font-weight:bold" class="font-orange">En cliquant sur "Je m'en occupe", le traitement de cette alerte vous sera attribuée.</span><br />
                        <asp:Button ID="btnAlertTreatmentReservation" runat="server" OnClick="btnAlertTreatmentReservation_Click" Style="display: none" />
                        <asp:Button ID="btnShowAlertWithoutCharge" runat="server" OnClick="btnShowAlertWithoutCharge_Click" Visible="false" style="display:none" />
                    </div>

                    <div id="dialog-alert-treatment-unreservation" style="display: none">
                        <label id="label-alert-treatment-unreservation"></label>
                        <asp:Button ID="btnAlertTreatmentUnreservation" runat="server" OnClick="btnAlertTreatmentUnreservation_Click" Style="display: none" />
                    </div>

                    <div id="dialog-dreok-confirmation" style="display:none">
                        <asp:AmlComment ID="amlComment" runat="server" MaxLength="400" />
                        <asp:Button ID="btnChangeStatusDREOK" runat="server" OnClick="btnChangeStatusDREOK_Click" style="display:none" />
                    </div>

                    <div id="popup-message">
                        <span id="message"></span>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnAlertTreatmentReservation" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnAlertTreatmentUnreservation" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnChangeStatusDREOK" EventName="Click" />
                </Triggers>
        </asp:UpdatePanel>
    </div>

    <div id="ar-loading"></div>

    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            pbControl = args.get_postBackElement();
            //console.log(pbControl.id);
            //if (pbControl != null) {
                var panel = '#loading-area';
                <%--//if (pbControl.id == '<=btnLoadDashboard.ClientID%>')
                    //panel = '#<=panelAgencyDashboard.ClientID %>';--%>

                if (panel.length > 0) {
                    var width = $(panel).outerWidth();
                    var height = $(panel).outerHeight();
                    $('#ar-loading').css('width', width);
                    $('#ar-loading').css('height', height);

                    $('#ar-loading').show();

                    $('#ar-loading').position({ my: "left top", at: "left top", of: panel });
                }
            //}
        }
        function EndRequestHandler(sender, args) {
            $('#ar-loading').hide();
            hideLoading();
        }
    </script>

</asp:Content>
