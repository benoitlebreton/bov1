﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using XMLMethodLibrary;

public partial class ComplianceSettings : System.Web.UI.Page
{
    protected bool bActionAllowed { get { return (ViewState["ActionAllowed"] != null) ? bool.Parse(ViewState["ActionAllowed"].ToString()) : false; } set { ViewState["ActionAllowed"] = value; } }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            CheckRights();

            string sValidationAmount;
            DataTable dtIBANs;
            DataTable dtBICs;
            DataTable dtCountries;

            GetTransferParams(authentication.GetCurrent().sToken, out sValidationAmount, out dtIBANs, out dtBICs, out dtCountries);

            BindValidationAmount(sValidationAmount);
            BindIBANs(dtIBANs);
            BindBICs(dtBICs);
            BindCountries(dtCountries);

            BindClients("");
        }

        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "InitKey", "Init();", true);
    }

    protected void CheckRights()
    {
        string sXmlOut = tools.GetRights(authentication.GetCurrent().sToken, "SC_TransferSettings");

        List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Page", "RC");

        if (listRC.Count == 0 || listRC[0] != "0")
        {
            Response.Redirect("Default.aspx", true);
        }
        else {
            List<string> listAction = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/Page/Action");

            for (int i = 0; i < listAction.Count; i++)
            {
                List<string> listTag = CommonMethod.GetAttributeValues(listAction[i], "Action", "TagAction");
                List<string> listActionAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ActionAllowed");
                List<string> listViewAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ViewAllowed");

                if (listTag.Count > 0 && listTag[0] == "TransferSettings")
                {
                    if (listActionAllowed[0] != "1")
                    {
                        btnSaveLimitAmount.Visible = false;
                        panelAddBIC.Visible = false;
                        panelAddIBAN.Visible = false;
                        panelAddCountry.Visible = false;
                        panelAddClient.Visible = false;
                    }
                    else bActionAllowed = true;
                    if (listViewAllowed[0] == "0")
                    {
                    }
                    break;
                }
            }
        }
    }

    protected void GetTransferParams(string sCashierToken, out string sValidationAmount, out DataTable dtBlIBANs, out DataTable dtBlBICs, out DataTable dtBlCountries)
    {
        SqlConnection conn = null;
        sValidationAmount = "";
        dtBlIBANs = new DataTable();
        dtBlIBANs.Columns.Add("iban");
        dtBlIBANs.Columns.Add("date");
        dtBlIBANs.Columns.Add("action");
        dtBlBICs = new DataTable();
        dtBlBICs.Columns.Add("bic");
        dtBlBICs.Columns.Add("date");
        dtBlBICs.Columns.Add("action");
        dtBlCountries = new DataTable();
        dtBlCountries.Columns.Add("iso2");
        dtBlCountries.Columns.Add("date");
        dtBlCountries.Columns.Add("action");

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_GetTransferParam", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                            new XElement("Transfer",
                                                new XAttribute("CashierToken", sCashierToken))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if (sXmlOut.Length > 0)
            {
                List<string> listValidationAmount = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Transfer", "MaxAmountBeforeValidation");
                if (listValidationAmount.Count > 0)
                    sValidationAmount = listValidationAmount[0];

                List<string> listIBANs = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/Transfer/IBANS/IBAN");
                for (int i = 0; i < listIBANs.Count; i++)
                {
                    List<string> listIBAN = CommonMethod.GetAttributeValues(listIBANs[i], "IBAN", "IBAN");
                    List<string> listDate = CommonMethod.GetAttributeValues(listIBANs[i], "IBAN", "InsertDate");
                    List<string> listAction = CommonMethod.GetAttributeValues(listIBANs[i], "IBAN", "Do");
                    dtBlIBANs.Rows.Add(new object[] { listIBAN[0], listDate[0], listAction[0] });
                }

                List<string> listBICs = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/Transfer/BICS/BIC");
                for (int i = 0; i < listBICs.Count; i++)
                {
                    List<string> listBIC = CommonMethod.GetAttributeValues(listBICs[i], "BIC", "BIC");
                    List<string> listDate = CommonMethod.GetAttributeValues(listBICs[i], "BIC", "InsertDate");
                    List<string> listAction = CommonMethod.GetAttributeValues(listBICs[i], "BIC", "Do");
                    dtBlBICs.Rows.Add(new object[] { listBIC[0], listDate[0], listAction[0] });
                }

                List<string> listCountries = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/Transfer/COUNTRIES/COUNTRY");
                for (int i = 0; i < listCountries.Count; i++)
                {
                    List<string> listCountry = CommonMethod.GetAttributeValues(listCountries[i], "COUNTRY", "ISO2");
                    List<string> listDate = CommonMethod.GetAttributeValues(listCountries[i], "COUNTRY", "InsertDate");
                    List<string> listAction = CommonMethod.GetAttributeValues(listCountries[i], "COUNTRY", "Do");
                    dtBlCountries.Rows.Add(new object[] { listCountry[0], listDate[0], listAction[0] });
                }
            }
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        
    }

    protected void BindValidationAmount(string sValidationAmount)
    {
        txtLimiteValidation.Text = sValidationAmount;
    }
    protected void btnSaveLimitAmount_Click(object sender, EventArgs e)
    {
        int iDeleted = 0;
        int iAdded = 0;
        string sXmlIn = new XElement("ALL_XML_IN",
                            new XElement("Transfer",
                                new XAttribute("CashierToken", authentication.GetCurrent().sToken),
                                new XAttribute("MaxAmountBeforeValidation", txtLimiteValidation.Text))).ToString();

        if (!SetBlacklist(sXmlIn, out iAdded, out iDeleted))
            UpdateMessage("Une erreur est survenue.");
        else UpdateMessage("Enregistré");
    }

    protected void BindIBANs(DataTable dt)
    {
        rptAML_IBANBlacklist.DataSource = dt;
        rptAML_IBANBlacklist.DataBind();
    }
    protected void btnBlackListIBAN_Click(object sender, EventArgs e)
    {
        int iDeleted = 0;
        int iAdded = 0;
        string sXmlIn = new XElement("ALL_XML_IN",
                            new XElement("Transfer",
                                new XAttribute("CashierToken", authentication.GetCurrent().sToken),
                                new XElement("IBANS",
                                    new XElement("IBAN",
                                        new XAttribute("Action", "ADD"),
                                        new XAttribute("IBAN", txtBlacklistIBAN.Text.ToUpper()),
                                        new XAttribute("DO", ddlIBANAction.SelectedValue))))).ToString();

        if (SetBlacklist(sXmlIn, out iAdded, out iDeleted))
        {
            if (iAdded > 0)
            {
                string s = "";
                DataTable dt = null;
                DataTable dt2 = null;
                DataTable dtIBANs = new DataTable();
                GetTransferParams(authentication.GetCurrent().sToken, out s, out dtIBANs, out dt2, out dt);
                BindIBANs(dtIBANs);

                txtBlacklistIBAN.Text = "";

                upIBAN.Update();
            }
        }
        else UpdateMessage("Une erreur est survenue.");
    }
    protected void btnRemoveIBAN_Click(object sender, EventArgs e)
    {
        int iDeleted = 0;
        int iAdded = 0;
        string sXmlIn = new XElement("ALL_XML_IN",
                            new XElement("Transfer",
                                new XAttribute("CashierToken", authentication.GetCurrent().sToken),
                                new XElement("IBANS",
                                    new XElement("IBAN",
                                        new XAttribute("Action", "DELETE"),
                                        new XAttribute("IBAN", hdnIBANToDelete.Value))))).ToString();

        if (SetBlacklist(sXmlIn, out iAdded, out iDeleted))
        {
            if (iDeleted > 0)
            {
                string s = "";
                DataTable dt = null;
                DataTable dt2 = null;
                DataTable dtIBANs = new DataTable();
                GetTransferParams(authentication.GetCurrent().sToken, out s, out dtIBANs, out dt2, out dt);
                BindIBANs(dtIBANs);

                upIBAN.Update();
            }
        }
        else UpdateMessage("Une erreur est survenue.");
    }

    protected void BindBICs(DataTable dt)
    {
        rptBIC.DataSource = dt;
        rptBIC.DataBind();
    }
    protected void btnBlackListBIC_Click(object sender, EventArgs e)
    {
        int iDeleted = 0;
        int iAdded = 0;
        string sXmlIn = new XElement("ALL_XML_IN",
                            new XElement("Transfer",
                                new XAttribute("CashierToken", authentication.GetCurrent().sToken),
                                new XElement("BICS",
                                    new XElement("BIC",
                                        new XAttribute("Action", "ADD"),
                                        new XAttribute("BIC", txtBlacklistBIC.Text.ToUpper()),
                                        new XAttribute("DO", ddlBICAction.SelectedValue))))).ToString();

        if (SetBlacklist(sXmlIn, out iAdded, out iDeleted))
        {
            if (iAdded > 0)
            {
                string s = "";
                DataTable dt = null;
                DataTable dt2 = null;
                DataTable dtBICs = new DataTable();
                GetTransferParams(authentication.GetCurrent().sToken, out s, out dt2, out dtBICs, out dt);
                BindBICs(dtBICs);

                txtBlacklistBIC.Text = "";

                upBIC.Update();
            }
        }
        else UpdateMessage("Une erreur est survenue.");
    }
    protected void btnRemoveBIC_Click(object sender, EventArgs e)
    {
        int iDeleted = 0;
        int iAdded = 0;
        string sXmlIn = new XElement("ALL_XML_IN",
                            new XElement("Transfer",
                                new XAttribute("CashierToken", authentication.GetCurrent().sToken),
                                new XElement("BICS",
                                    new XElement("BIC",
                                        new XAttribute("Action", "DELETE"),
                                        new XAttribute("BIC", hdnBICToDelete.Value))))).ToString();

        if (SetBlacklist(sXmlIn, out iAdded, out iDeleted))
        {
            if (iDeleted > 0)
            {
                string s = "";
                DataTable dt = null;
                DataTable dt2 = null;
                DataTable dtBICs = new DataTable();
                GetTransferParams(authentication.GetCurrent().sToken, out s, out dt2, out dtBICs, out dt);
                BindBICs(dtBICs);

                upBIC.Update();
            }
        }
        else UpdateMessage("Une erreur est survenue.");
    }

    protected void BindCountries(DataTable dt)
    {
        DataTable dtCountryList = tools.GetCountryList();
        DataTable dtCountryListFinal = dtCountryList.Clone();
        dtCountryListFinal.Rows.Add(new object[]{});
        dtCountryListFinal.Merge(dtCountryList);

        ddlCountries.DataSource = dtCountryListFinal;
        ddlCountries.DataBind();

        DataView dv = dt.DefaultView;
        dv.Sort = "iso2 asc";

        rptCountries.DataSource = dv.ToTable();
        rptCountries.DataBind();
    }
    protected void btnBlackListCountry_Click(object sender, EventArgs e)
    {
        int iDeleted = 0;
        int iAdded = 0;
        string sXmlIn = new XElement("ALL_XML_IN",
                            new XElement("Transfer",
                                new XAttribute("CashierToken", authentication.GetCurrent().sToken),
                                new XElement("COUNTRIES",
                                    new XElement("COUNTRY",
                                        new XAttribute("Action", "ADD"),
                                        new XAttribute("ISO2", ddlCountries.SelectedValue),
                                        new XAttribute("DO", ddlCountryAction.SelectedValue))))).ToString();

        if (SetBlacklist(sXmlIn, out iAdded, out iDeleted))
        {
            if (iAdded > 0)
            {
                string s = "";
                DataTable dt = null;
                DataTable dt2 = null;
                DataTable dtCountries = new DataTable();
                GetTransferParams(authentication.GetCurrent().sToken, out s, out dt, out dt2, out dtCountries);
                BindCountries(dtCountries);

                upCountry.Update();
            }
        }
        else UpdateMessage("Une erreur est survenue.");
    }
    protected void btnRemoveCountry_Click(object sender, EventArgs e)
    {
        int iDeleted = 0;
        int iAdded = 0;
        string sXmlIn = new XElement("ALL_XML_IN", 
                            new XElement("Transfer",
                                new XAttribute("CashierToken", authentication.GetCurrent().sToken),
                                new XElement("COUNTRIES",
                                    new XElement("COUNTRY",
                                        new XAttribute("Action", "DELETE"),
                                        new XAttribute("ISO2", hdnCountryToRemove.Value))))).ToString();

        if (SetBlacklist(sXmlIn, out iAdded, out iDeleted))
        {
            if (iDeleted > 0)
            {
                string s = "";
                DataTable dt = null;
                DataTable dt2 = null;
                DataTable dtCountries = new DataTable();
                GetTransferParams(authentication.GetCurrent().sToken, out s, out dt, out dt2, out dtCountries);
                BindCountries(dtCountries);

                upCountry.Update();
            }
        }
        else UpdateMessage("Une erreur est survenue.");

    }

    protected bool SetBlacklist(string sXmlIn, out int iAdded, out int iDeleted)
    {
        SqlConnection conn = null;
        iAdded = 0;
        iDeleted = 0;
        bool bOk = false;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_AddDelSetTransferParam", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);

            cmd.Parameters["@IN"].Value = sXmlIn;
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if (sXmlOut.Length > 0)
            {
                List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Result", "RC");
                if(listRC.Count > 0 && listRC[0] == "0")
                {
                    bOk = true;
                    List<string> listAdded = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Result", "Inserted");
                    if (listAdded.Count > 0)
                        int.TryParse(listAdded[0], out iAdded);
                    List<string> listDeleted = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Result", "Deleted");
                    if (listDeleted.Count > 0)
                        int.TryParse(listDeleted[0], out iDeleted);
                }
            }
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return bOk;
    }

    protected string GetActionLabel(string sTag)
    {
        string sLabel = "";

        switch (sTag)
        {
            case "LOCK":
                sLabel = "<b style=\"color:red\">BLOQUER</b>";
                break;
            case "BOVALIDATION":
                sLabel = "<b style=\"color:darkorange\">VALIDER</b>";
                break;
        }

        return sLabel;
    }
    protected string GetCountryFromISO2(string sISO2)
    {
        ListItem item = ddlCountries.Items.FindByValue(sISO2);
        if(item != null)
            item.Attributes["disabled"] = "disabled";

        return (item != null) ? item.Text : "";
    }

    protected void imgSearchClients_Click(object sender, ImageClickEventArgs e)
    {
        BindClients(txtSearchClients.Text);
    }
    protected void BindClients(string sSearch)
    {
        DataTable dt = GetClientBlacklist(sSearch);
        rptClients.Visible = false;
        panelNoClientFound.Visible = false;
        panelNoClient.Visible = false;

        if (dt.Rows.Count > 0)
        {
            rptClients.Visible = true;

            rptClients.DataSource = dt;
            rptClients.DataBind();
        }
        else
        {
            if(sSearch.Length > 0)
                panelNoClientFound.Visible = true;
            else panelNoClient.Visible = true;
        }
    }
    protected DataTable GetClientBlacklist(string sSearch)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("Ref");
        dt.Columns.Add("LastName");
        dt.Columns.Add("FirstName");
        dt.Columns.Add("BirthDate");
        dt.Columns.Add("BirthPlace");
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("WEB.P_SearchBlackListCustomer", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@ReturnSelect", SqlDbType.Bit);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                            new XElement("BLACKLIST",
                                                new XAttribute("CashierToken", authentication.GetCurrent().sToken),
                                                new XAttribute("Search", sSearch))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
            cmd.Parameters["@ReturnSelect"].Value = 0;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if (sXmlOut.Length > 0)
            {
                List<string> listClient = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/BLACKLIST/CLIENT");

                for (int i = 0; i < listClient.Count; i++)
                {
                    List<string> listRef = CommonMethod.GetAttributeValues(listClient[i], "CLIENT", "Ref");
                    List<string> listLastName = CommonMethod.GetAttributeValues(listClient[i], "CLIENT", "LastName");
                    List<string> listFirstName = CommonMethod.GetAttributeValues(listClient[i], "CLIENT", "FirstName");
                    List<string> listBirthDate = CommonMethod.GetAttributeValues(listClient[i], "CLIENT", "BirthDate");
                    string sDate = (listBirthDate.Count > 0 && listBirthDate[0].Length >= 10) ? listBirthDate[0].Substring(0, 10) : "";
                    List<string> listBirthPlace = CommonMethod.GetAttributeValues(listClient[i], "CLIENT", "BirthPlace");

                    dt.Rows.Add(new object[] { listRef[0], listLastName[0], listFirstName[0], sDate, listBirthPlace[0] });
                }
            }
        }
        catch (Exception e) { }
        finally
        {
            if (conn != null) conn.Close();
        }

        return dt;
    }

    protected void btnBlackListClient_Click(object sender, EventArgs e)
    {
        if (txtLastName.Text.Length > 0)
        {
            string sXmlIn = new XElement("ALL_XML_IN",
                                new XElement("BLACKLIST",
                                    new XAttribute("CashierToken", authentication.GetCurrent().sToken),
                                    new XElement("CLIENT",
                                        new XAttribute("Action", "ADD"),
                                        new XAttribute("LastName", txtLastName.Text),
                                        new XAttribute("FirstName", txtFirstName.Text),
                                        new XAttribute("BirthDate", txtBirthDate.Text),
                                        new XAttribute("BirthPlace", txtBirthPlace.Text)))).ToString();

            if (AddDelSetBlacklistClient("ADD", sXmlIn))
            {
                txtLastName.Text = "";
                txtFirstName.Text = "";
                txtBirthDate.Text = "";
                txtBirthPlace.Text = "";
                BindClients("");
            }
            else UpdateMessage("Une erreur est survenue.");
        }
        else UpdateMessage("Veuillez renseigner le NOM du client.");

    }
    protected void btnRemoveClient_Click(object sender, EventArgs e)
    {
        string sXmlIn = new XElement("ALL_XML_IN",
                            new XElement("BLACKLIST",
                                new XAttribute("CashierToken", authentication.GetCurrent().sToken),
                                new XElement("CLIENT",
                                    new XAttribute("Action", "DELETE"),
                                    new XAttribute("Ref", hdnClientToRemove.Value)))).ToString();

        if (AddDelSetBlacklistClient("DELETE", sXmlIn))
            BindClients(txtSearchClients.Text);
        else UpdateMessage("Une erreur est survenue.");
    }
    protected bool AddDelSetBlacklistClient(string sAction, string sXmlIn)
    {
        bool bActionOk = false;
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_AddDelSetCustomerExternalBlackList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);

            cmd.Parameters["@IN"].Value = sXmlIn;
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if (sXmlOut.Length > 0)
            {
                List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Result", "RC");
                List<string> listInserted = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Result", "Inserted");
                List<string> listDeleted = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Result", "Deleted");

                if (listRC.Count > 0 && listRC[0] == "0")
                {
                    if (sAction == "ADD" && listInserted.Count > 0 && int.Parse(listInserted[0]) > 0)
                        bActionOk = true;
                    if (sAction == "DELETE" && listDeleted.Count > 0 && int.Parse(listDeleted[0]) > 0)
                        bActionOk = true;
                }
            }
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }


        return bActionOk;
    }

    protected void UpdateMessage(string sMessage)
    {
        hdnMessage.Value = sMessage;
        upMessage.Update();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "UpdateMessageKEY", "CheckMessages();", true);
    }
}