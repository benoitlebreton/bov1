﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ClientMessage.aspx.cs" Inherits="ClientMessage" %>
<%@ Register Src="~/API/CreateMessage.ascx" TagName="CreateMessage" TagPrefix="asp" %>
<%@ Register Src="~/API/MessageHistory.ascx" TagName="MessageHistory" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">

    <script type="text/javascript">
        $(document).ready(function () {
            $('.RadioButtonList').buttonset();
        });
    </script>

    <style type="text/css">
        #ar-loading {
            background:url('Styles/img/loading2.gif') no-repeat center;
            background-size: 35px 27px;
            display:none;
            position:absolute;
            background-color:rgba(255, 255, 255, 0.5);
            z-index:10000;
        }
        .ui-button .ui-button-text {
            line-height: 0.8;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

    <div id="ar-loading"></div>

    <h2 style="color: #344b56;margin-bottom:10px;text-transform:uppercase">Compte-Nickel - Vos messages</h2>
        
        <div style="border: 3px solid #344b56; border-radius: 8px; width: 100%; margin: auto;margin-bottom:20px;">
            
            <asp:RadioButtonList ID="rblMessageType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="rblMessageType_SelectedIndexChanged" RepeatDirection="Horizontal" CssClass="RadioButtonList" Height="20px" Style="position: relative; left: 0px">
                <asp:ListItem Value="home">Accueil</asp:ListItem>
                <%--<asp:ListItem Value="dm">Message direct</asp:ListItem>--%>
            </asp:RadioButtonList>

            <div style="margin:20px">

                <asp:Panel ID="panelDirectMessage" runat="server" Visible="false">
                    <div id="panelCreate">
                        <asp:UpdatePanel ID="upCreate" runat="server">
                            <ContentTemplate>
                                <asp:CreateMessage ID="CreateMessage1" runat="server" IsPro="false" IsHome="false" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </asp:Panel>
            
                <asp:Panel ID="panelHomeMessage" runat="server" Visible="false">
                    <div id="divCreateHomeMessage">
                        <asp:UpdatePanel ID="upCreate2" runat="server">
                            <ContentTemplate>
                                <asp:Panel ID="panelAdd" runat="server">
                                    <asp:CreateMessage ID="CreateMessage2" runat="server" IsHome="true" />
                                </asp:Panel>
                            </ContentTemplate>
                        </asp:UpdatePanel>                
                    </div>
            
                    <div id="divHistory">
                        <asp:UpdatePanel ID="upHistory" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:Panel ID="panelHistory" runat="server" style="margin-top:10px">
                                    <asp:MessageHistory ID="MessageHistory1" runat="server" IsHome="true" />
                                </asp:Panel>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>

                    <asp:Panel ID="panelSearch" runat="server">

                    </asp:Panel>
                </asp:Panel>
            </div>
        </div>
    

    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        var sRequestControlID = "";
        function BeginRequestHandler(sender, args) {
            pbControl = args.get_postBackElement();
            //console.log(pbControl.id);
            var panel = '';

            if (pbControl.id.indexOf('_CreateMessage1_') != -1)
                panel = '#panelCreate';
            else if (pbControl.id.indexOf('_CreateMessage2_') != -1)
                panel = '#divCreateHomeMessage';
            else if (pbControl.id.indexOf('_MessageHistory1_') != -1)
                panel = '#divHistory';

            if (panel.length > 0) {
                var width = $(panel).outerWidth();
                var height = $(panel).outerHeight();
                $('#ar-loading').css('width', width);
                $('#ar-loading').css('height', height);

                $('#ar-loading').show();

                $('#ar-loading').position({ my: "left top", at: "left top", of: panel });
            }

            sRequestControlID = pbControl.id;
        }
        function EndRequestHandler(sender, args) {
            $('#ar-loading').hide();

            $('.RadioButtonList').buttonset();
        }
    </script>

</asp:Content>