﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ComplianceSettings.aspx.cs" Inherits="ComplianceSettings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">

    <script type="text/javascript">

        function Init() {
            InitTabs();
            InitDialogs();
        }

        function InitTabs() {
            $('.blacklist-tabs').tabs({
                activate: function (event, ui) {
                }
            });
        }

        function InitDialogs() {
            $('#dialog-RemoveIBANBlacklist').dialog({
                autoOpen: false,
                resizable: false,
                draggable: false,
                modal: true,
                minWidth: 350,
                title: "Confirmation"
                        , buttons: [{ text: 'Annuler', click: function () { $(this).dialog('close'); } }, { text: 'OK', click: function () { $('#<%=btnRemoveIBAN.ClientID%>').click(); $(this).dialog('close'); } }]
            });
            $('#dialog-RemoveIBANBlacklist').parent().appendTo(jQuery("form:first"));
            $('#dialog-RemoveBICBlacklist').dialog({
                autoOpen: false,
                resizable: false,
                draggable: false,
                modal: true,
                minWidth: 350,
                title: "Confirmation"
                        , buttons: [{ text: 'Annuler', click: function () { $(this).dialog('close'); } }, { text: 'OK', click: function () { $('#<%=btnRemoveBIC.ClientID%>').click(); $(this).dialog('close'); } }]
            });
            $('#dialog-RemoveBICBlacklist').parent().appendTo(jQuery("form:first"));
            $('#dialog-RemoveCountryBlacklist').dialog({
                autoOpen: false,
                resizable: false,
                draggable: false,
                modal: true,
                minWidth: 350,
                title: "Confirmation"
                        , buttons: [{ text: 'Annuler', click: function () { $(this).dialog('close'); } }, { text: 'OK', click: function () { $('#<%=btnRemoveCountry.ClientID%>').click(); $(this).dialog('close'); } }]
            });
            $('#dialog-RemoveCountryBlacklist').parent().appendTo(jQuery("form:first"));
            $('#dialog-RemoveClientBlacklist').dialog({
                autoOpen: false,
                resizable: false,
                draggable: false,
                modal: true,
                minWidth: 350,
                title: "Confirmation"
                        , buttons: [{ text: 'Annuler', click: function () { $(this).dialog('close'); } }, { text: 'OK', click: function () { $('#<%=btnRemoveClient.ClientID%>').click(); $(this).dialog('close'); } }]
            });
            $('#dialog-RemoveClientBlacklist').parent().appendTo(jQuery("form:first"));

            $('#popup-message').dialog({
                autoOpen: false,
                resizable: false,
                draggable: false,
                modal: true,
                title: "Message",
                buttons: [{ text: 'Fermer', click: function () { $(this).dialog('close'); } }]
            });
        }

        function ShowConfirmRemoveIBANBlacklist(iban) {
            $('#<%=hdnIBANToDelete.ClientID%>').val(iban);
            $('#dialog-RemoveIBANBlacklist .iban-selected').html(iban);
            $('#dialog-RemoveIBANBlacklist').dialog('open');
        }
        function ShowConfirmRemoveBICBlacklist(bic) {
            $('#<%=hdnBICToDelete.ClientID%>').val(bic);
            $('#dialog-RemoveBICBlacklist .bic-selected').html(bic);
            $('#dialog-RemoveBICBlacklist').dialog('open');
        }
        function ShowConfirmRemoveCountryBlacklist(iso2, label) {
            $('#<%=hdnCountryToRemove.ClientID%>').val(iso2);
            $('#dialog-RemoveCountryBlacklist .iban-selected').html(label);
            $('#dialog-RemoveCountryBlacklist').dialog('open');
        }
        function ShowConfirmRemoveClientBlacklist(ref, lastname, firstname, birthdate, birthplace) {
            $('#<%=hdnClientToRemove.ClientID%>').val(ref);
            var label = '';
            if (lastname.length > 0)
                label += lastname + ' ';
            if (firstname.length > 0)
                label += firstname + ' ';
            if (birthdate.length > 0 || birthplace.length > 0)
                label += 'né(e) ';
            if (birthdate.length > 0)
                label += 'le ' + birthdate + ' ';
            if (birthplace.length > 0)
                label += 'à ' + birthplace;
            $('#dialog-RemoveClientBlacklist .client-selected').html(label);
            $('#dialog-RemoveClientBlacklist').dialog('open');
        }

        function CheckMessages() {
            if ($('#<%=hdnMessage.ClientID%>').val().length > 0) {
                $('#message').empty().append($('#<%=hdnMessage.ClientID%>').val());
                $('#popup-message').dialog('open');
                $('#<%=hdnMessage.ClientID%>').val('');
            }
        }

    </script>

    <style type="text/css">

        .alert-table {
        margin-top:10px;
        }
        .alert-table .table-row.head, .alert-table .table-row.head-light {
            font-weight:bold;
            color:#fff;
            background-color:#344b56;
            text-transform:uppercase;
        }
        .alert-table .head-light {
            background-color:#517384;
        }
        .alert-table input[type=checkbox] {
            display:none;
        }
        .alert-table input[type=text] {
            width:150px;
        }
        .alert-table .table-row .table-cell {
            border-bottom:1px solid #ddd;
            vertical-align:middle;
            height:30px;
        }
        .alert-table .table-row .table-cell .toggle-soft {
            position:relative;
            top:-3px;
        }
        .alert-table .table-row .table-cell:nth-of-type(2) {
            text-align:center;
        }
        .alert-table .table-row .table-cell:nth-of-type(3), .alert-table .table-row .table-cell:nth-of-type(4) {
            padding:0 5px;
        }
        .alert-table .head .table-cell, .alert-table .head-light .table-cell {
            padding:0 10px;
            height:20px;
            text-align:left;
        }

        #ar-loading {
            background: url('Styles/img/loading.gif') no-repeat center;
            display: none;
            position: absolute;
            background-color: rgba(249, 249, 249, 0.5);
            z-index: 100;
        }

        .default-row-style .table-cell {
            background-color:#ebecec;
        }
        .alternate-row-style .table-cell {
            background-color:#fff;
        }

        .ui-tabs .ui-tabs-panel {
            padding:3px 0 0 0;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    
    <asp:UpdatePanel ID="upMessage" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:HiddenField ID="hdnMessage" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="popup-message">
        <span id="message"></span>
    </div>

    <div id="ar-loading"></div>

    <h2 style="color: #344b56;margin-bottom:10px;text-transform:uppercase">Réglages CONFORMIT&Eacute;</h2>

    <div style="color:#344b56;font-weight:bold;text-transform:uppercase;border-bottom:1px solid #344b56;margin-top:20px">
        Virements sortants
    </div>
    <asp:UpdatePanel ID="upTransferValidation" runat="server">
        <ContentTemplate>
            <div id="transfer-validation" class="table alert-table" style="width:100%;border-collapse:collapse;text-transform:none">
                <div class="table-row">
                    <div class="table-cell" style="border-bottom:0">
                        Validation systématique des virements d'un montant supérieur ou égal à
                        <asp:TextBox ID="txtLimiteValidation" runat="server" MaxLength="9" style="text-align:right;font-size:1.2em"></asp:TextBox> &euro;
                    </div>
                    <div class="table-cell" style="width:1%;border-bottom:0;">
                        <asp:Button ID="btnSaveLimitAmount" runat="server" Text="Enregistrer" CssClass="button" OnClick="btnSaveLimitAmount_Click" style="position:relative;top:5px" />
                        <%--<div id="toggleLimiteValidation" class="toggle-soft"></div>
                        <asp:CheckBox ID="ckbLimiteValidation" runat="server" />--%>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div style="color:#344b56;font-weight:bold;text-transform:uppercase;border-bottom:1px solid #344b56;margin-top:10px">
        Listes oranges
    </div>
    <div style="margin-top:10px">
        <div class="blacklist-tabs">
            <ul>
                <li>
                    <a href="#bic-blacklist">BIC</a>
                </li>
                <li>
                    <a href="#iban-blacklist">IBAN</a>
                </li>
                <li>
                    <a href="#country-blacklist">PAYS</a>
                </li>
                <li>
                    <a href="#client-blacklist">CLIENT</a>
                </li>
            </ul>

            <div id="bic-blacklist">
                <asp:UpdatePanel ID="upBIC" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <%--<div class="table alert-table" style="width:100%;border-collapse:collapse;text-transform:none">
                            <div class="table-row head">
                                <div class="table-cell">
                                    BIC
                                </div>
                            </div>
                        </div>--%>
                        <div style="max-height:500px;overflow-y:scroll">
                            <asp:Repeater ID="rptBIC" runat="server">
                                <HeaderTemplate>
                                    <div class="table alert-table" style="width:100%;border-collapse:collapse;text-transform:none;margin-top:0">
                                </HeaderTemplate>
                                <ItemTemplate>
                                        <div class='table-row <%# Container.ItemIndex % 2 == 0 ? "default-row-style" : "alternate-row-style" %>'>
                                            <div class="table-cell" style="text-align:left;vertical-align:middle;padding-left:10px">
                                                <%#Eval("BIC") %>
                                            </div>
                                            <div class="table-cell" style="text-align:center;vertical-align:middle;width:160px">
                                                <%# GetActionLabel(Eval("action").ToString()) %>
                                            </div>
                                            <div class="table-cell" style="text-align:center;vertical-align:middle;width:60px;">
                                                <asp:ImageButton ID="imgRemoveBIC" runat="server" Visible="<%# bActionAllowed %>" CommandArgument='<%#Eval("BIC") %>' ImageUrl="~/Styles/Img/delete.png" OnClientClick=<%# string.Format("ShowConfirmRemoveBICBlacklist('{0}');return false;", Eval("bic")) %> Height="25px" style="position:relative;top:2px" ToolTip="Supprimer" />
                                            </div>
                                        </div>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </div>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                        <asp:Panel ID="panelAddBIC" runat="server" CssClass="table alert-table" style="margin-top:0">
                            <div class="table-row head">
                                <div class="table-cell">
                                    <asp:TextBox ID="txtBlacklistBIC" runat="server" style="width:100%;text-transform:uppercase" MaxLength="11"></asp:TextBox>
                                </div>
                                <div class="table-cell" style="width:102px">
                                    <asp:DropDownList ID="ddlBICAction" runat="server" style="width:100%">
                                        <asp:ListItem Value="LOCK" Text="BLOQUER"></asp:ListItem>
                                        <asp:ListItem Value="BOVALIDATION" Text="VALIDER"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="table-cell" style="width:1%">
                                    <asp:Button ID="btnBlacklistBIC" runat="server" Text="Ajouter" OnClick="btnBlackListBIC_Click" CssClass="button" style="position:relative;top:5px" />
                                </div>
                            </div>
                        </asp:Panel>

                        <div id="dialog-RemoveBICBlacklist" style="display:none">
                            &Ecirc;tes vous sûr de vouloir supprimer le BIC<br />
                            <b><span class="bic-selected"></span></b><br />
                            de la liste orange ?
                            <asp:HiddenField ID="hdnBICToDelete" runat="server" />
                            <asp:Button ID="btnRemoveBIC" runat="server" OnClick="btnRemoveBIC_Click" style="display:none" />
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnRemoveBIC" EventName="click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>

            <div id="iban-blacklist">
                <asp:UpdatePanel ID="upIBAN" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
<%--                        <div class="table alert-table" style="width:100%;border-collapse:collapse;text-transform:none">
                            <div class="table-row head">
                                <div class="table-cell">
                                    IBAN
                                </div>
                            </div>
                        </div>--%>
                        <div style="max-height:500px;overflow-y:scroll">
                            <asp:Repeater ID="rptAML_IBANBlacklist" runat="server">
                                <HeaderTemplate>
                                    <div class="table alert-table" style="width:100%;border-collapse:collapse;text-transform:none;margin-top:0">
                                </HeaderTemplate>
                                <ItemTemplate>
                                        <div class='table-row <%# Container.ItemIndex % 2 == 0 ? "default-row-style" : "alternate-row-style" %>'>
                                            <div class="table-cell" style="text-align:left;vertical-align:middle;padding-left:10px">
                                                <%#Eval("IBAN") %>
                                            </div>
                                            <div class="table-cell" style="text-align:center;vertical-align:middle;width:160px">
                                                <%# GetActionLabel(Eval("action").ToString()) %>
                                            </div>
                                            <div class="table-cell" style="text-align:center;vertical-align:middle;width:60px;">
                                                <asp:ImageButton ID="imgRemoveIBAN" runat="server" Visible="<%# bActionAllowed %>" CommandArgument='<%#Eval("IBAN") %>' ImageUrl="~/Styles/Img/delete.png" OnClientClick=<%# string.Format("ShowConfirmRemoveIBANBlacklist('{0}');return false;", Eval("iban")) %> Height="25px" style="position:relative;top:2px" ToolTip="Supprimer" />
                                            </div>
                                        </div>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </div>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                        <asp:Panel ID="panelAddIBAN" runat="server" CssClass="table alert-table" style="margin-top:0">
                            <div class="table-row head">
                                <div class="table-cell">
                                    <asp:TextBox ID="txtBlacklistIBAN" runat="server" style="width:100%;text-transform:uppercase" MaxLength="32"></asp:TextBox>
                                </div>
                                <div class="table-cell" style="width:102px">
                                    <asp:DropDownList ID="ddlIBANAction" runat="server" style="width:100%">
                                        <asp:ListItem Value="LOCK" Text="BLOQUER"></asp:ListItem>
                                        <asp:ListItem Value="BOVALIDATION" Text="VALIDER"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="table-cell" style="width:1%">
                                    <asp:Button ID="btnBlackListIBAN" runat="server" Text="Ajouter" OnClick="btnBlackListIBAN_Click" CssClass="button" style="position:relative;top:5px" />
                                </div>
                            </div>
                        </asp:Panel>

                        <div id="dialog-RemoveIBANBlacklist" style="display:none">
                            &Ecirc;tes vous sûr de vouloir supprimer l'IBAN<br />
                            <b><span class="iban-selected"></span></b><br />
                            de la liste orange ?
                            <asp:HiddenField ID="hdnIBANToDelete" runat="server" />
                            <asp:Button ID="btnRemoveIBAN" runat="server" OnClick="btnRemoveIBAN_Click" style="display:none" />
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnRemoveIBAN" EventName="click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>

            <div id="country-blacklist">
                <asp:UpdatePanel ID="upCountry" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <%--<div class="table alert-table" style="width:100%;border-collapse:collapse;text-transform:none">
                            <div class="table-row head">
                                <div class="table-cell">
                                    Pays
                                </div>
                                <div class="table-cell"></div>
                                <div class="table-cell"></div>
                            </div>
                        </div>--%>
                        <div style="max-height:500px;overflow-y:scroll">
                            <asp:Repeater ID="rptCountries" runat="server">
                                <HeaderTemplate>
                                    <div class="table alert-table" style="width:100%;border-collapse:collapse;text-transform:none;margin-top:0">
                                </HeaderTemplate>
                                <ItemTemplate>
                                        <div class='table-row <%# Container.ItemIndex % 2 == 0 ? "default-row-style" : "alternate-row-style" %>'>
                                            <div class="table-cell" style="text-align:left;vertical-align:middle;padding-left:10px">
                                                <%# GetCountryFromISO2(Eval("iso2").ToString()) %>
                                            </div>
                                            <div class="table-cell" style="text-align:center;vertical-align:middle;width:160px;">
                                                <%# GetActionLabel(Eval("action").ToString()) %>
                                            </div>
                                            <div class="table-cell" style="text-align:center;vertical-align:middle;width:60px">
                                                <asp:ImageButton ID="imgRemoveCountry" runat="server" Visible="<%# bActionAllowed %>" CommandArgument='<%#Eval("iso2") %>' ImageUrl="~/Styles/Img/delete.png" OnClientClick=<%# string.Format("ShowConfirmRemoveCountryBlacklist('{0}', '{1}');return false;", Eval("iso2"), GetCountryFromISO2(Eval("iso2").ToString()) ) %> Height="25px" style="position:relative;top:2px" ToolTip="Supprimer" />
                                            </div>
                                        </div>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </div>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                        <asp:Panel ID="panelAddCountry" runat="server" CssClass="table alert-table" style="margin-top:0">
                            <div class="table-row head">
                                <div class="table-cell">
                                    <asp:DropDownList ID="ddlCountries" runat="server" style="width:100%" DataTextField="countryname" DataValueField="iso2">
                                    </asp:DropDownList>
                                </div>
                                <div class="table-cell" style="width:102px">
                                    <asp:DropDownList ID="ddlCountryAction" runat="server" style="width:100%">
                                        <asp:ListItem Value="LOCK" Text="BLOQUER"></asp:ListItem>
                                        <asp:ListItem Value="BOVALIDATION" Text="VALIDER"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="table-cell" style="width:1%">
                                    <asp:Button ID="btnBlackListCountry" runat="server" Text="Ajouter" OnClick="btnBlackListCountry_Click" CssClass="button" style="position:relative;top:5px" />
                                </div>
                            </div>
                        </asp:Panel>
                        <div id="dialog-RemoveCountryBlacklist" style="display:none">
                            &Ecirc;tes vous sûr de vouloir supprimer le pays<br />
                            <b><span class="iban-selected"></span></b><br />
                            de la liste orange ?
                            <asp:HiddenField ID="hdnCountryToRemove" runat="server" />
                            <asp:Button ID="btnRemoveCountry" runat="server" OnClick="btnRemoveCountry_Click" style="display:none" />
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnRemoveCountry" EventName="click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>

            <div id="client-blacklist">
                <asp:UpdatePanel ID="upClient" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="table alert-table" style="width:100%;border-collapse:collapse;text-transform:none;margin-top:0">
                            <div class="table-row head">
                                <div class="table-cell" style="padding:5px">
                                    <asp:Panel ID="panelSearchClients" runat="server" DefaultButton="imgSearchClients">
                                        <asp:TextBox ID="txtSearchClients" runat="server" placeholder="Rechercher un client (nom, prénom)" style="width:95%;float:left;padding:1px 3px;height:27px;box-sizing:border-box;border:1px solid #a9a9a9;border-right:0"></asp:TextBox>
                                        <span style="width:5%;padding-right:5px;box-sizing:border-box;height:27px;float:right;text-align:right;background-color:#fff;border:1px solid #a9a9a9;border-left:0">
                                            <asp:ImageButton ID="imgSearchClients" runat="server" ImageUrl="~/Styles/Img/shop_search_min.png" OnClick="imgSearchClients_Click" Height="27px" ToolTip="Rechercher" />
                                        </span><div style="clear:both"></div>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>
                        <div style="max-height:500px;overflow-y:scroll">
                            <asp:Repeater ID="rptClients" runat="server" Visible="false">
                                <HeaderTemplate>
                                    <div class="table alert-table" style="width:100%;border-collapse:collapse;text-transform:none;margin-top:0">
                                </HeaderTemplate>
                                <ItemTemplate>
                                        <div class='table-row <%# Container.ItemIndex % 2 == 0 ? "default-row-style" : "alternate-row-style" %>'>
                                            <div class="table-cell" style="text-align:center;vertical-align:middle;width:210px">
                                                <%# Eval("LastName") %>
                                            </div>
                                            <div class="table-cell" style="text-align:center;vertical-align:middle;width:205px">
                                                <%# Eval("FirstName") %>
                                            </div>
                                            <div class="table-cell" style="text-align:center;vertical-align:middle;width:155px">
                                                <%# Eval("BirthDate") %>
                                            </div>
                                            <div class="table-cell" style="text-align:center;vertical-align:middle">
                                                <%# Eval("BirthPlace") %>
                                            </div>
                                            <div class="table-cell" style="text-align:center;vertical-align:middle;width:75px">
                                                <asp:ImageButton ID="imgRemoveClient" runat="server" Visible="<%# bActionAllowed %>" CommandArgument='<%#Eval("Ref") %>' ImageUrl="~/Styles/Img/delete.png" OnClientClick=<%# string.Format("ShowConfirmRemoveClientBlacklist('{0}', '{1}', '{2}', '{3}', '{4}');return false;", Eval("Ref"), Eval("LastName"), Eval("FirstName"), Eval("BirthDate"), Eval("BirthPlace") ) %> Height="25px" style="position:relative;top:2px" ToolTip="Supprimer" />
                                            </div>
                                        </div>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </div>
                                </FooterTemplate>
                            </asp:Repeater>
                            <asp:Panel ID="panelNoClient" runat="server" style="padding:5px 0;text-align:center">
                                Aucun client dans la liste.
                            </asp:Panel>
                            <asp:Panel ID="panelNoClientFound" runat="server" Visible="false" style="padding:5px 0;text-align:center">
                                Aucune correspondance trouvée.
                            </asp:Panel>
                        </div>
                        <asp:Panel ID="panelAddClient" runat="server" CssClass="table alert-table" style="margin-top:0">
                            <asp:Panel ID="panelBlacklistClient" runat="server" CssClass="table-row head" DefaultButton="btnBlackListClient">
                                <div class="table-cell" style="width:200px;padding: 0 3px;">
                                    <asp:TextBox ID="txtLastName" runat="server" placeholder="Nom" style="width:100%;padding:1px 3px;box-sizing:border-box"></asp:TextBox>
                                </div>
                                <div class="table-cell" style="width:200px;padding: 0 3px;">
                                    <asp:TextBox ID="txtFirstName" runat="server" placeholder="Prénom" style="width:100%;padding:1px 3px;box-sizing:border-box"></asp:TextBox>
                                </div>
                                <div class="table-cell" style="width:160px;padding:0 3px;">
                                    <asp:TextBox ID="txtBirthDate" runat="server" placeholder="Date de naissance" style="width:100%;padding:1px 3px;box-sizing:border-box"></asp:TextBox>
                                </div>
                                <div class="table-cell" style="padding:0 3px">
                                    <asp:TextBox ID="txtBirthPlace" runat="server" placeholder="Commune de naissance" style="width:100%;padding:1px 3px;box-sizing:border-box"></asp:TextBox>
                                </div>
                                <div class="table-cell" style="width:1%">
                                    <asp:Button ID="btnBlackListClient" runat="server" Text="Ajouter" OnClick="btnBlackListClient_Click" CssClass="button" style="position:relative;top:5px" />
                                </div>
                            </asp:Panel>
                        </asp:Panel>
                        <div id="dialog-RemoveClientBlacklist" style="display:none">
                            &Ecirc;tes vous sûr de vouloir supprimer le client<br />
                            <b><span class="client-selected"></span></b><br />
                            de la liste orange ?
                            <asp:HiddenField ID="hdnClientToRemove" runat="server" />
                            <asp:Button ID="btnRemoveClient" runat="server" OnClick="btnRemoveClient_Click" style="display:none" />
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnRemoveClient" EventName="click" />
                        <asp:AsyncPostBackTrigger ControlID="imgSearchClients" EventName="click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
        
        <div style="margin-top:10px;text-align:center">
            <span style="text-decoration:underline">L&Eacute;GENDE</span>
            <div style="color:red;margin-top:5px;">
                <b>BLOQUER</b> : le virement est directement bloqué sur l'espace client.
            </div>
            <div style="color:darkorange">
                <b>VALIDER</b> : le virement ne sera émis qu'à la suite d'une approbation sur le BO.
            </div>
        </div>
    </div>

    <script type="text/javascript">

        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            pbControl = args.get_postBackElement();
            //console.log(pbControl.id);
            var target = '';
            if (pbControl.id == '<%=btnBlackListCountry.ClientID %>' || pbControl.id == '<%=btnRemoveCountry.ClientID %>')
                target = '#country-blacklist';
            else if (pbControl.id == '<%=btnBlackListIBAN.ClientID %>' || pbControl.id == '<%=btnRemoveIBAN.ClientID %>')
                target = '#iban-blacklist';
            else if (pbControl.id == '<%=btnBlacklistBIC.ClientID %>' || pbControl.id == '<%=btnRemoveBIC.ClientID %>')
                target = '#bic-blacklist';
            else if (pbControl.id == '<%=imgSearchClients.ClientID %>' || pbControl.id == '<%=btnBlackListClient.ClientID %>' || pbControl.id == '<%=btnRemoveClient.ClientID %>')
                target = '#client-blacklist';
            else if (pbControl.id == '<%=btnSaveLimitAmount.ClientID %>')
                target = '#transfer-validation';
            var width = $(target).width();
            var height = $(target).height();
            $('#ar-loading').css('width', width);
            $('#ar-loading').css('height', height);

            $('#ar-loading').show();

            $('#ar-loading').position({ my: "left top", at: "left top", of: target });
        }
        function EndRequestHandler(sender, args) {
            $('#ar-loading').hide();
        }

    </script>

</asp:Content>