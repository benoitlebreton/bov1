﻿<%@ Page Title="Recherche inscription - BO Compte-Nickel" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="RegistrationSearch.aspx.cs" Inherits="RegistrationSearch" %>

<%@ Register Src="~/FILTRES/Period2.ascx" TagPrefix="filter" TagName="Period" %>
<%@ Register Src="~/API/DatePicker.ascx" TagPrefix="asp" TagName="DatePicker" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .ui-autocomplete {
            max-height: 130px;
        }

        .ui-combobox {
            position: relative;
            display: inline-block;
        }

        .ui-combobox-toggle {
            position: absolute;
            top: 0;
            bottom: 0;
            margin-left: -1px;
            padding: 0;
            /* support: IE7 */
            *height: 1.7em;
            *top: 0.1em;
        }

        .ui-combobox-input {
            margin: 0;
            padding: 0.3em;
            width: 300px;
            height: 24px;
        }

        table#MainContent_gvRegistrationSearchResult tr td:first-of-type {
            padding:0 5px 0 0 !important;
            background-color:#fff;
        }
    </style>
    <script type="text/javascript">
        (function ($) {
            $.widget("ui.combobox", {
                _create: function () {
                    var input,
                        that = this,
                        wasOpen = false,
                        select = this.element.hide(),
                        selected = select.children(":selected"),
                        value = selected.val() ? selected.text() : "",
                        wrapper = this.wrapper = $("<span>")
                        .addClass("ui-combobox")
                        .insertAfter(select);
                    function removeIfInvalid(element) {
                        var value = $(element).val(),
                            matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex(value) + "$", "i"),
                            valid = false;
                        select.children("option").each(function () {
                            if ($(this).text().match(matcher)) {
                                this.selected = valid = true;
                                return false;
                            }
                        });
                        if (!valid) {
                            // remove invalid value, as it didn't match anything
                            $(element)
                                .val("")
                                .attr("title", value + " n'existe pas")
                                .tooltip("open");
                            select.val("");
                            setTimeout(function () {
                                input.tooltip("close").attr("title", "");
                            }, 2500);
                            input.data("ui-autocomplete").term = "";
                        }
                    }
                    input = $("<input>")
                        .appendTo(wrapper)
                        .val(value)
                        .attr("title", "")
                        .addClass("ui-state-default ui-combobox-input")
                        .autocomplete({
                            delay: 0,
                            minLength: 0,
                            source: function (request, response) {
                                var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
                                response(select.children("option").map(function () {
                                    var text = $(this).text();
                                    if (this.value && (!request.term || matcher.test(text)))
                                        return {
                                            label: text.replace(
                                                new RegExp(
                                                "(?![^&;]+;)(?!<[^<>]*)(" +
                                                $.ui.autocomplete.escapeRegex(request.term) +
                                                ")(?![^<>]*>)(?![^&;]+;)", "gi"
                                                ), "<strong>$1</strong>"),
                                            value: text,
                                            option: this
                                        };
                                }));
                            },
                            select: function (event, ui) {
                                ui.item.option.selected = true;
                                that._trigger("selected", event, {
                                    item: ui.item.option
                                });
                            },
                            change: function (event, ui) {
                                if (!ui.item) {
                                    removeIfInvalid(this);
                                }
                            }
                        })
                        .addClass("ui-widget ui-widget-content ui-corner-left");
                    input.data("ui-autocomplete")._renderItem = function (ul, item) {
                        return $("<li>")
                        .append("<a>" + item.label + "</a>")
                        .appendTo(ul);
                    };
                    $("<a>")
                        .attr("tabIndex", -1)
                        .attr("title", "Afficher tout")
                        .tooltip()
                        .appendTo(wrapper)
                        .button(
                            {
                                icons: {
                                    primary: "ui-icon-triangle-1-s"
                                },
                                text: false
                            }
                        )
                        .removeClass("ui-corner-all")
                        .addClass("ui-corner-right ui-combobox-toggle")
                        .mousedown(function () {
                            wasOpen = input.autocomplete("widget").is(":visible");
                        })
                        .click(function () {
                            input.focus();
                            // close if already visible
                            if (wasOpen) {
                                return;
                            }
                            // pass empty string as value to search for, displaying all results
                            input.autocomplete("search", "");
                        });
                    input.tooltip({
                        tooltipClass: "ui-state-highlight"
                    });
                },
                _destroy: function () {
                    this.wrapper.remove();
                    this.element.show();
                }
            });

            //$('.tooltip').tooltip({ items: "span[tooltip]" });
        })(jQuery);

        function FilterStatusDisplayManagement() {
            //if ($('#<%= divGridView.ClientID%>') != null && $('#<%= divGridView.ClientID%>').is(':visible'))
            //$('#panelFilterStatus').show();
            //else
            //$('#panelFilterStatus').hide();
        }

        function initTooltip() {
            $('.trimmer').tooltip({
                position: {
                    my: "center bottom-10",
                    at: "center top",
                    using: function (position, feedback) {
                        $(this).css(position);
                        $("<div>")
                            .addClass("arrow")
                            .addClass(feedback.vertical)
                            .addClass(feedback.horizontal)
                            .appendTo(this);
                    }
                }
            });
        }

        function InitAccordion(activePanel) {
            //alert("init accordion");
            $("#<%= ddlAgencyID.ClientID %>").combobox();
            //$('#<%= rblCashierCheck.ClientID %>').buttonset();
            $('.RadioButtonList').buttonset();

            if ($('#<%= hfFilterVisible.ClientID%>').val().trim() == "true") {
                $('#panelFilterStatus').show();
            }
            else {
                $('#panelFilterStatus').hide();
            }
            $("#FilterList").accordion({
                heightStyle: "content",
                change: function (event, ui) {
                    //alert('change' + $('#panelFilterStatus').is(':visible'));
                    if ($(this).accordion("option", "active") != 4) {
                        $('#<%= hfFilterVisible.ClientID%>').val('false');
                        if ($('#panelFilterStatus').is(':visible'))
                            $('#panelFilterStatus').slideUp();
                    }
                    else {
                        $('#<%= hfFilterVisible.ClientID%>').val('true');
                        if (!$('#panelFilterStatus').is(':visible'))
                            $('#panelFilterStatus').slideDown();
                    }
                }
            });

            if (activePanel != null && $.isNumeric(activePanel)) {
                $("#FilterList").accordion("option", "animate", false);
                $("#FilterList").accordion("option", "active", activePanel);
            }
            else {
                if ($('#<%= hfPanelActive.ClientID %>').val().trim() != "") {
                    $("#FilterList").accordion("option", "animate", false);
                    $("#FilterList").accordion("option", "active", parseInt($('#<%= hfPanelActive.ClientID %>').val().trim()));
                }
                else {
                    $("#FilterList").accordion("option", "animate", false);
                    $("#FilterList").accordion("option", "active", 4);
                }
            }

            $("#FilterList").accordion({
                activate: function (event, ui) {
                    //alert($("#FilterList").accordion("option", "active"));
                    $('#<%= hfPanelActive.ClientID %>').val($("#FilterList").accordion("option", "active"));
                }
            });

                $("#FilterList").accordion("option", "animate", true);
            }

            function clickRegistrationSearch(setPageIndex) {
                if (setPageIndex == null)
                    setPageIndex = 1;

                $('#<%=hfPageSelected.ClientID%>').val(setPageIndex);
            $('#<%=btnRegistrationSearch.ClientID %>').click();
        }

        function onChangeNbResult() {
            clickRegistrationSearch(1);
            return false;
        }

        function nextPage() {
            var pageIndex = parseInt($('#<%= ddlPage.ClientID %>').val()) + 1;
            $('#<%= ddlPage.ClientID %>').val(pageIndex);

            clickRegistrationSearch(pageIndex);
            return false;
        }

        function previousPage() {
            var pageIndex = parseInt($('#<%= ddlPage.ClientID %>').val()) - 1;
            $('#<%= ddlPage.ClientID %>').val(pageIndex);

            clickRegistrationSearch(pageIndex);
            return false;
        }

        function onChangePageNumber() {
            var pageIndex = $('#<%= ddlPage.ClientID %>').val();

            clickRegistrationSearch(pageIndex);
            return false;
        }

        function showRegistration(registrationNumber) {
            __doPostBack("upRegistration", "showRegistration;" + registrationNumber);
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div style="position: relative">
        <div style="position: absolute; top: 30px">
            <asp:Button ID="btnPrevious_2" runat="server" Text="Précédent" OnClick="onClickPreviousButton" CssClass="button" />
        </div>
    </div>

    <div style="margin-bottom: 20px" id="divRegistrationSearch" runat="server" visible="true">
        <asp:Panel ID="panelRegistrationSearch" runat="server" DefaultButton="btnRegistrationSearch">
            <h2 style="color: #344b56">RECHERCHE INSCRIPTION</h2>
            <asp:UpdatePanel ID="upFilter" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="FilterList" style="margin: 20px auto; width: 90%">
                        <h3 style="text-transform: uppercase">par Numéro de pack</h3>
                        <div id="RegistrationSearchByPack" style="margin: auto">
                            <div style="font-weight: bold">
                                N&deg; pack
                            </div>
                            <div>
                                <asp:TextBox ID="txtPackNumber" runat="server" autocomplete="off"></asp:TextBox>
                            </div>
                        </div>
                        <h3 style="text-transform: uppercase">par Numéro d'Inscription</h3>
                        <div id="RegistrationSearchByRegistration" style="margin: auto">
                            <div style="font-weight: bold">
                                N&deg; inscription
                            </div>
                            <div>
                                <asp:TextBox ID="txtRegistrationNumber" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <h3 style="text-transform: uppercase">par Numéro de Téléphone</h3>
                        <div id="RegistrationSearchByPhone" style="margin: auto">
                            <div style="font-weight: bold">
                                N&deg; Téléphone
                            </div>
                            <div>
                                <asp:TextBox ID="txtPhoneNumber" runat="server" autocomplete="off"></asp:TextBox>
                            </div>
                        </div>
                        <h3 style="text-transform: uppercase">par Client</h3>
                        <div id="RegistrationSearchByClient" style="margin: auto">
                            <div style="display: table; width: 100%">
                                <div style="display: table-row">
                                    <div style="display: table-cell; width: 33%">
                                        <div style="font-weight: bold">
                                            Nom
                                        </div>
                                        <div>
                                            <asp:TextBox ID="txtClientLastName" runat="server" autocomplete="off"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div style="display: table-cell; width: 33%">
                                        <div style="font-weight: bold">
                                            Prénom
                                        </div>
                                        <div>
                                            <asp:TextBox ID="txtClientFirstName" runat="server" autocomplete="off"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div style="display: table-cell; width: 33%">
                                        <div style="font-weight: bold">
                                            Date de Naissance
                                        </div>
                                        <div>
                                            <asp:TextBox ID="txtClientBirthDate" runat="server" Visible="false" autocomplete="off"></asp:TextBox>
                                            <asp:DatePicker ID="dpClientBirthDate" runat="server" sYearType="past" bGetDateByPostback="true"
                                                sPostbackName="BirthDatePicker" />
                                            <asp:HiddenField ID="hfClientBirthDate" runat="server" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h3 style="text-transform: uppercase">par Point de vente</h3>
                        <div id="RegistrationSearchByAgency" style="margin: auto">
                            <div style="display: table; width: 100%">
                                <div style="display: table-row;">
                                    <div style="display: table-cell">
                                        <div style="font-weight: bold">
                                            Point de vente
                                        </div>
                                        <div>
                                            <div class="ui-widget">
                                                <asp:DropDownList ID="ddlAgencyID" runat="server" DataTextField="AgencyName" DataValueField="AgencyID"
                                                    Width="300px" AppendDataBoundItems="true" Height="30px">
                                                    <asp:ListItem Selected="True" Value=""></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <asp:Panel ID="divNotUsed" runat="server" Visible="false">
                            <h3 style="text-transform: uppercase">par Document</h3>
                            <div id="RegistrationSearchByDocument">
                                <div style="display: table; width: 100%">
                                    <div style="display: table-row">
                                        <div style="display: table-cell; width: 33%">
                                            <div style="font-weight: bold">
                                                N&deg; Document
                                            </div>
                                            <div>
                                                <asp:TextBox ID="txtDocumentNumber" runat="server" autocomplete="off"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div style="display: table-cell; width: 33%">
                                            <div style="font-weight: bold">
                                                Type Document
                                            </div>
                                            <div>
                                                <asp:DropDownList ID="ddlDocumentType" runat="server">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div style="display: table-cell; width: 33%">
                                            <div style="font-weight: bold">
                                                Pays Document
                                            </div>
                                            <div>
                                                <asp:DropDownList ID="ddlDocumentCountry" runat="server">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                    </div>
                    <asp:HiddenField ID="hfFilterVisible" runat="server" Value="true" />
                    <asp:HiddenField ID="hfPanelActive" runat="server" Value="" />
                </ContentTemplate>
            </asp:UpdatePanel>
            <div style="width: 100%; text-align: center">
                <asp:Button ID="btnDeleteFilter" runat="server" OnClientClick="showLoading();" OnClick="btnDeleteFilter_Click" Text="Supprimer les filtres" Style="width: 250px" CssClass="buttonHigher" />
                <asp:Button ID="btnRegistrationSearch" runat="server" OnClick="btnRegistrationSearch_Click" OnClientClick="showLoading();" Text="RECHERCHER" Style="width: 200px" CssClass="buttonHigher" />
                <asp:UpdateProgress ID="upGridViewProgress" runat="server" AssociatedUpdatePanelID="upGridView">
                    <ProgressTemplate>
                        <div style="display: inline-block;">
                            <img src="./Styles/Img/loading.gif" alt="loading" width="30px" height="30px" />
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
            </div>
            <div style="width: 90%; margin: auto">
                <asp:UpdatePanel ID="upFilterStatus" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="panelFilterStatus">
                            <div style="width: 100%; margin: 10px 0 0 0">
                                <filter:Period ID="RegistrationSearchByAgencyPeriod" runat="server" isPeriodBold="true" />
                                <asp:HiddenField ID="hfBeginDate" runat="server" />
                                <asp:HiddenField ID="hfEndDate" runat="server" />
                                <asp:HiddenField ID="hfPeriod" runat="server" />
                            </div>
                            <div style="width:50%;float:left;margin-top:-6px">
                                <asp:Label ID="lblAccountType" runat="server" AssociatedControlID="ddlAccountType" style="font-weight: bold; text-transform: none">
                                    Type de Compte
                                </asp:Label>
                                <div style="margin-top:3px;">
                                    <asp:DropDownList ID="ddlAccountType" runat="server">
                                        <asp:ListItem Text="Tous" Value=""></asp:ListItem>
                                        <asp:ListItem Text="Compte-Nickel" Value="NOB"></asp:ListItem>
                                        <asp:ListItem Text="Compte-Nickel 12/18 ans" Value="CNJ"></asp:ListItem>
                                        <asp:ListItem Text="Compte-Nickel Professionnel" Value="CNP"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div style="clear:both"></div>
                            <div style="display: table; width: 100%; margin:15px 0 5px 0">
                                <div style="display: table-row">
                                    <div style="display: table-cell;">
                                        <div style="font-weight: bold; text-transform: capitalize">
                                            état inscription
                                        </div>
                                        <asp:RadioButtonList ID="rblStatus" runat="server" AutoPostBack="false" RepeatDirection="Horizontal"
                                            CssClass="RadioButtonList" Height="20px" onchange="clickRegistrationSearch(1);"
                                            Style="position: relative; left: 0px; text-transform: uppercase">
                                            <asp:ListItem Value="15" Selected="True"><span style="color:green;text-shadow: -1px -1px 0px #FFF, 1px 1px 0px #FFF, -1px 1px 0px #FFF, 1px -1px 0px #FFF;">réussi</span></asp:ListItem>
                                            <asp:ListItem Value="22"><span style="color:red;text-shadow: -1px -1px 0px #FFF, 1px 1px 0px #FFF, -1px 1px 0px #FFF, 1px -1px 0px #FFF;">échec</span></asp:ListItem>
                                            <asp:ListItem Value="125"><span style="color:blue;text-shadow: -1px -1px 0px #FFF, 1px 1px 0px #FFF, -1px 1px 0px #FFF, 1px -1px 0px #FFF;">mpad terminé</span></asp:ListItem>
                                            <asp:ListItem Value="">tous</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                    <div style="display: table-cell;">
                                        <div style="font-weight: bold; text-transform: capitalize">
                                            état évaluation
                                        </div>
                                        <asp:RadioButtonList ID="rblStatusCheck" runat="server" AutoPostBack="false" RepeatDirection="Horizontal"
                                            CssClass="RadioButtonList" Height="20px" onchange="clickRegistrationSearch(1);"
                                            Style="position: relative; left: 0px">
                                            <asp:ListItem Value="G"><img src="./Styles/Img/green_status.png" alt="" /></asp:ListItem>
                                            <asp:ListItem Value="O"><img src="./Styles/Img/yellow_status.png" alt="" /></asp:ListItem>
                                            <asp:ListItem Value="R"><img src="./Styles/Img/red_status.png" alt=""/></asp:ListItem>
                                            <asp:ListItem Value="W"><img src="./Styles/Img/gray_status.png" alt=""/></asp:ListItem>
                                            <asp:ListItem Value="" Selected="True">TOUS</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                    <div id="divFilterCashierCheck" runat="server" style="display: table-cell; text-align: right;" visible="false">
                                        <div>
                                            Validée par caissier
                                        </div>
                                        <asp:RadioButtonList ID="rblCashierCheck" runat="server" AutoPostBack="false" RepeatDirection="Horizontal"
                                            CssClass="RadioButtonList" Height="20px" onchange="clickRegistrationSearch(1);"
                                            Style="position: relative; left: 15px">
                                            <asp:ListItem Value="Y" Selected="True">OUI</asp:ListItem>
                                            <asp:ListItem Value="N">NON</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>

                <asp:UpdatePanel ID="upGridView" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="divGridViewInfo" runat="server" visible="false" style="display: table; width: 100%; margin-bottom: 10px">
                            <div style="display: table-row">
                                <div style="display: table-cell; width: 50%; padding-top: 20px">
                                    <asp:Label ID="lblNbOfResults" runat="server">0</asp:Label>
                                    résultat(s)
                                </div>
                                <div style="display: table-cell; width: 50%; text-align: right;">
                                    Nb de résultats par page :
                                <asp:DropDownList ID="ddlNbResult" runat="server" AutoPostBack="false" onchange="onChangeNbResult();">
                                    <asp:ListItem Value="10">10</asp:ListItem>
                                    <asp:ListItem Value="25" Selected="True">25</asp:ListItem>
                                    <asp:ListItem Value="50">50</asp:ListItem>
                                </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <asp:Panel ID="divGridView" runat="server" Visible="false">
                            <div style="border: 3px solid #344b56; border-top-left-radius: 8px; border-top-right-radius: 8px; width: 100%; margin: auto"
                                visible="false">
                                <%--OnPreRender="GridView1_PreRender" OnRowCreated="GridView1_RowCreated" OnRowDataBound="GridView1_RowDataBound"--%>
                                <asp:GridView ID="gvRegistrationSearchResult" runat="server" AllowSorting="false"
                                    AllowPaging="false" AutoGenerateColumns="False" GridLines="None" CellPadding="10"
                                    DataKeyNames="RefTransaction" ShowHeader="true" Width="100%" SortedDescendingCellStyle-Font-Overline="true"
                                    ViewStateMode="Disabled" EnableSortingAndPagingCallbacks="false" BorderWidth="0"
                                    BackColor="Transparent" OnRowDataBound="GridView1_RowDataBound">
                                    <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="#344b56" Font-Bold="true" />
                                    <EmptyDataTemplate>
                                        Aucune donnée ne correspond à la recherche
                                    </EmptyDataTemplate>
                                    <Columns>
                                        <asp:TemplateField HeaderText="" ControlStyle-Width="1px" ControlStyle-BackColor="White">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnServiceTag" runat="server" Value='<%#Eval("ServiceTag") %>' />
                                                <img src='<%#tools.GetAccountTypeImg(Eval("ServiceTag").ToString()) %>' width="40px" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="N° Pack" SortExpression="RegistrationNumber" InsertVisible="false"
                                            ControlStyle-CssClass="" ControlStyle-Width="70px">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblRegistrationNumber" Text='<%# Eval("RegistrationCode") %>'
                                                    Style="display: none"></asp:Label>
                                                <asp:Label runat="server" ID="lblPackNumber" Text='<%# Eval("PackNumber") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Nom" SortExpression="LastName" InsertVisible="false"
                                            ControlStyle-CssClass="trimmer" ControlStyle-Width="100px">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblClientLastName" Text='<%# Eval("LastName")%>' CssClass="tooltip"
                                                    ToolTip='<%# Eval("LastName")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Prénom" SortExpression="FirstName" InsertVisible="false"
                                            ControlStyle-CssClass="trimmer" ControlStyle-Width="100px">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblClientFirstName" Text='<%# Eval("FirstName") %>'
                                                    CssClass="tooltip" ToolTip='<%# Eval("FirstName")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField SortExpression="BirthDate" InsertVisible="false" ControlStyle-CssClass=""
                                            ControlStyle-Width="70px" ItemStyle-HorizontalAlign="Center">
                                            <HeaderTemplate>
                                                Date<br />
                                                de
                                            <br />
                                                naissance
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblClientBirthDate" Text='<%# Eval("BirthDate")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Etat" InsertVisible="False" SortExpression="Status"
                                            ControlStyle-CssClass="trimmer" ControlStyle-Width="170px">
                                            <ItemTemplate>
                                                <asp:Label ID="status" runat="server" Font-Bold="true" Text='<%#Eval("Status") %>'
                                                    ForeColor='<%# ColorStatusLabel(Eval("DisplayColor").ToString())%>' CssClass="tooltip"
                                                    ToolTip='<%#Eval("Status") %>'></asp:Label>
                                                <asp:Image ID="ErrorMessageImg" runat="server" ImageUrl="~/Styles/Img/help.png" ToolTip=""
                                                    Width="15px" Visible="false" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Date insertion" InsertVisible="False" SortExpression="InsertedDate"
                                            ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Label ID="InsertedDate" runat="server" Text='<%# Eval("InsertionDate") %>'></asp:Label>
                                                <%--<asp:Label ID="Label1" runat="server" Text='<%# ConvertToLocalDateTime(Eval("InsertionDate").ToString())%>'></asp:Label>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Check" InsertVisible="False" ControlStyle-CssClass="trimmer"
                                            ControlStyle-Width="50px" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Image ID="ImgCheckStatus" runat="server" CssClass="tooltip" Style="width: 20px;"
                                                    ToolTip='<%# getCheckStatus(Eval("BackOfficeOpinion").ToString() +";"+ Eval("BackOfficeOpinionDate").ToString()+";"+Eval("BackOfficeUser").ToString(), "Tooltip") %>'
                                                    AlternateText='<%# getCheckStatus(Eval("BackOfficeOpinion").ToString(), "AlternateText")%>'
                                                    ImageUrl='<%# getCheckStatus(Eval("BackOfficeOpinion").ToString(), "ImageUrl")%>' />
                                                <%--<asp:Label ID="lblCheckStatus" runat="server" Text='<%# getCheckStatus(Eval("BackOfficeOpinion").ToString(), "AlternateText")%>' CssClass="tooltip" 
                                            ToolTip='<%# getCheckStatus(Eval("BackOfficeOpinion").ToString() +";"+ Eval("BackOfficeOpinionDate").ToString()+";"+Eval("BackOfficeUser").ToString(), "Tooltip") %>' ></asp:Label>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--<asp:BoundField DataField="InsertedDate"HeaderText="InsertedDate" SortExpression="InsertedDate" ItemStyle-HorizontalAlign="Center"/>--%>
                                        <asp:TemplateField HeaderText="RefTransaction" InsertVisible="False" SortExpression="RefTransaction"
                                            Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="RefTransaction" runat="server" Text='<%#Eval("RefTransaction") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="bDelete" InsertVisible="False" SortExpression="bDelete"
                                            Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="bDelete" runat="server" Text=''></asp:Label><%--<%#Eval("bDelete") %>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle BackColor="#344b56" Font-Bold="True" ForeColor="White" />
                                    <HeaderStyle BackColor="#344b56" Font-Bold="True" ForeColor="White" Height="25px" />
                                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                    <RowStyle CssClass="GridViewRowStyle" />
                                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                    <%--<PagerStyle CssClass="GridViewPager" />--%>
                                </asp:GridView>
                            </div>
                            <div style="width: 100%; background-color: #344b56; color: White; border: 3px solid #344b56; border-bottom-left-radius: 8px; border-bottom-right-radius: 8px; height: 25px">
                                <asp:Panel ID="divPagination" runat="server" Visible="false" Style="width: 100%; display: table; margin: auto">
                                    <div style="display: table-row;">
                                        <div style="display: table-cell; height: 20px; vertical-align: middle; width: 57%; text-align: right">
                                            <asp:Button ID="btnPreviousPage" runat="server" OnClientClick="previousPage(); return false;" Text="<"
                                                Font-Bold="true" Width="30px" />
                                            <%--<input id="btnPreviousPage" runat="server"type="button" class="button" value="<" style="width:30px" onclick="previousPage();"/>--%>
                                            <%--OnSelectedIndexChanged="onChangePage"--%>
                                            <asp:DropDownList ID="ddlPage" runat="server" onchange="onChangePageNumber();" DataTextField="L"
                                                DataValueField="V" AutoPostBack="false">
                                            </asp:DropDownList>
                                            <asp:Label ID="lblTotalPage" runat="server" Style="font-weight: bold"></asp:Label>
                                            <%--<input id="btnNextPage" runat="server"type="button" class="button" value=">" style="width:30px" onclick="nextPage();"/>--%>
                                            <asp:Button ID="btnNextPage" runat="server" OnClientClick="nextPage(); return false;" Text=">"
                                                Font-Bold="true" Width="30px" />
                                            <%--<div style="padding-left: 10px"> <img src="Styles/Img/loading.gif"alt="loading" width="25px" height="25px" /> </div>--%>
                                        </div>
                                        <div style="display: table-cell; padding-left: 5px">
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>
                        </asp:Panel>
                        <asp:HiddenField ID="hfPageSelected" runat="server" Value="" />
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnRegistrationSearch" EventName="click" />
                        <asp:AsyncPostBackTrigger ControlID="btnDeleteFilter" EventName="click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </asp:Panel>
        <div style="height: 30px">
        </div>
        <div id="#EndofGridView">
        </div>
    </div>

    <div style="width: 100%; display: table">
        <div style="display: table-row">
            <div style="display: table-cell; text-align: left">
                <asp:Button ID="btnPrevious" runat="server" Text="Précédent" OnClick="onClickPreviousButton" CssClass="button" />
            </div>
            <div style="display: table-cell; text-align: right">
                <asp:Button ID="btnValidate" runat="server" Text="Valider" OnClick="onClickValidate" CssClass="button" Visible="false" Style="margin: 0;" />
            </div>
        </div>
    </div>

</asp:Content>

