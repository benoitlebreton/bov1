﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="TracfinStatement.aspx.cs" Inherits="TracfinStatement" %>
<%@ Register Src="~/API/DropdownListMultiSelect.ascx" TagName="MultiSelect" TagPrefix="asp" %>
<%@ Register Src="~/API/ClientSearch.ascx" TagName="ClientSearch" TagPrefix="asp" %>
<%@ Register Src="~/API/ClientFileUpload.ascx" TagName="ClientFileUpload" TagPrefix="asp" %>
<%@ Register Src="~/API/ClientFileManager.ascx" TagName="ClientFileManager" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">

    <link rel="Stylesheet" type="text/css" href="Styles/TracfinStatement.css?v=20180320" />
    <link rel="Stylesheet" type="text/css" href="Styles/ClientSearch.css" />
    <link rel="Stylesheet" href="Styles/ClientFileUpload.css" />
    <link type="text/css" rel="Stylesheet" href="Styles/DropdownListMultiSelect.css" />

    <script type="text/javascript">

        $(document).ready(function () {
            InitControls();
        });

        function EnableAutoSave() {
            setInterval(function () {
                SetAutoSaveStatus();
                $('#<%=btnAutoSave.ClientID%>').click();
            }, 60000);
        }
        function SetAutoSaveStatus(saved) {
            $('.saving-status img').fadeOut('fast');
            if (saved == null)
                $('.icon-saving').fadeIn('fast');
            else {
                if (saved)
                    $('.icon-saving-done').fadeIn('fast');
                else $('.icon-saving-fail').fadeIn('fast');

                setTimeout(function () { HideAutoSaveStatus(); }, 3000);
            }
            if ($('.saving-status').is(':hidden'))
                $('.saving-status').fadeIn();
        }
        function HideAutoSaveStatus() {
            if ($('.saving-status').is(':visible'))
                $('.saving-status').fadeOut();
        }

        function InitControls() {
            $('#<%=txtDREndDate.ClientID%>').on('change', function () {
                setTimeout(function () {
                    $('#<%=txtPeriodeEndDate.ClientID%>').val($('#<%=txtDREndDate.ClientID%>').val());
                }, 100);
            });

            InitDatePicker();
            UpdateNbDays();

            $('.radioButtonList').buttonset();
        }

        function InitDatePicker() {
            $('#<%=txtOpeOriginDRDate.ClientID %>').mask("99/99/9999");
            $('#<%=txtDRStartDate.ClientID %>').mask("99/99/9999");
            $('#<%=txtDREndDate.ClientID %>').mask("99/99/9999");

            $('#<%=txtOpeOriginDRDate.ClientID%>:not([readonly])').datepicker({
                defaultDate: "0",
                dateFormat: "dd/mm/yy",
                dayNamesMin: ["Di", "Lu", "Ma", "Me", "Je", "Ve", "Sa"],
                monthNames: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"],
                firstDay: 1,
                maxDate: "0",
                numberOfMonths: 1,
                onClose: function (selectedDate) {
                    $("#<%=txtDRStartDate.ClientID %>").datepicker("option", "minDate", selectedDate);
                    $("#<%=txtDREndDate.ClientID %>").datepicker("option", "minDate", selectedDate);

                    UpdateNbDays();
                }
            });
            $('#<%=txtDRStartDate.ClientID%>:not([readonly])').datepicker({
                defaultDate: "0",
                dateFormat: "dd/mm/yy",
                dayNamesMin: ["Di", "Lu", "Ma", "Me", "Je", "Ve", "Sa"],
                monthNames: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"],
                firstDay: 1,
                maxDate: "0",
                numberOfMonths: 1,
                onClose: function (selectedDate) {
                    $("#<%=txtDREndDate.ClientID %>").datepicker("option", "minDate", selectedDate);
                    $("#<%=txtOpeOriginDRDate.ClientID %>").datepicker("option", "maxDate", selectedDate);

                    UpdateNbDays();
                }
            });
            $('#<%=txtDREndDate.ClientID%>:not([readonly])').datepicker({
                defaultDate: "0",
                dateFormat: "dd/mm/yy",
                dayNamesMin: ["Di", "Lu", "Ma", "Me", "Je", "Ve", "Sa"],
                monthNames: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"],
                firstDay: 1,
                maxDate: "0",
                numberOfMonths: 1,
                onClose: function (selectedDate) {
                    $("#<%=txtDRStartDate.ClientID %>").datepicker("option", "maxDate", selectedDate);
                    $("#<%=txtOpeOriginDRDate.ClientID %>").datepicker("option", "maxDate", selectedDate);

                    UpdateNbDays();
                }
            });
        }
        function UpdateNbDays() {
            var nbDays;
            if ($('#<%=txtOpeOriginDRDate.ClientID%>').val().length > 0 && $('#<%=txtDRStartDate.ClientID%>').val().length > 0) {
                nbDays = NbDaysBetween('<%=txtOpeOriginDRDate.ClientID%>', '<%=txtDRStartDate.ClientID%>');
                $('#<%=lblNbDaysSinceOpeOriginDRDate.ClientID%>').empty().append('+'+nbDays+'j');
            }
            if ($('#<%=txtDRStartDate.ClientID%>').val().length > 0 && $('#<%=txtDREndDate.ClientID%>').val().length > 0) {
                nbDays = NbDaysBetween('<%=txtDRStartDate.ClientID%>', '<%=txtDREndDate.ClientID%>');
                $('#<%=lblNbDaysDRStartDate.ClientID%>').empty().append('+' + nbDays + 'j');
            }
        }
        function NbDaysBetween(IDstart, IDend) {
            if ($('#' + IDstart).val().length == 10 && $('#' + IDend).val().length == 10)
            {
                var arStart = $('#' + IDstart).val().split('/'),
                    arEnd = $('#' + IDend).val().split('/');

                if(arStart.length == 3 && arEnd.length == 3)
                {
                    var dateStart = new Date(arStart[2], arStart[1], arStart[0]),
                        dateEnd = new Date(arEnd[2], arEnd[1], arEnd[0]);

                    var a = dateStart.getTime(),
                        b = dateEnd.getTime(),
                        c = 24 * 60 * 60 * 1000;
                    return Math.round(Math.abs((a - b) / (c)));
                }
            }
            return null;
        }

        function SelectAllCheckbox(selector, ckb) {
            var check = $(ckb).prop('checked');
            if(check == null)
                check = $(ckb).find('input[type=checkbox]').prop('checked');
            $(selector + ' input[type=checkbox]').prop('checked', check);
        }

        function ShowOpeSelectionDialog() {
            var dateFrom = $('#<%=txtPeriodeStartDate.ClientID%>').val();
            var dateTo = $('#<%=txtPeriodeEndDate.ClientID%>').val();

            $('#dialog-ope-select').dialog({
                draggable: false,
                resizable: false,
                width: 1000,
                //dialogClass: "no-close",
                modal: true,
                title: 'Opérations créditrices du ' + dateFrom + ' au ' + dateTo,
            });
            if ($('#<%=hdnEditable.ClientID%>').val() != '0') {
                $('#dialog-ope-select').dialog({
                    buttons: {
                        'Mettre à jour': function () {
                            $('#<%=btnUpdateOpeDetails.ClientID%>').click();
                            $(this).dialog('destroy');
                        }
                    }
                });
            }
            else {
                $('#dialog-ope-select').parent().removeClass('no-close');
            }

            $('#dialog-ope-select').parent().appendTo(jQuery("form:first"));
        }
        function UpdateNbOpeSelected()
        {
            $('#<%=lblNbOpeSelect.ClientID%>').html($('#div-ope-table td input[type=checkbox]:checked').length);
        }

        function ShowNewFileDialog() {
            $('#dialog-add-file').dialog({
                draggable: false,
                resizable: false,
                width: 800,
                //dialogClass: "no-close",
                modal: true,
                title: 'Ajouter un nouveau fichier'
            });
            $('#dialog-add-file').parent().appendTo(jQuery("form:first"));

            InitInputFiles();
        }
        function HideNewFileDialog() {

        }

        function FireCustomerCheckChanged() {
            $('#<%=btnCustomerCheckChanged.ClientID%>').click();
        }

        function ToggleMoreOption() {
            $('.more-option-bar').slideToggle();
        }

        function ShowCommentDialog() {
            $('#dialog-comment').dialog({
                draggable: false,
                resizable: false,
                width: 600,
                //dialogClass: "no-close",
                modal: true,
                title: 'Rédiger un commentaire',
                buttons: {
                    'Valider': function () {
                        ShowLoading('.bottom-bar');
                            $('#<%=btnOtherActionComment.ClientID%>').click();
                            $(this).dialog('destroy');
                        }
                    }
            });
            $('#dialog-comment').parent().appendTo(jQuery("form:first"));
        }
        function DestroyCommentDialog() {
            try
            {
                $('#dialog-comment').dialog('destroy');
            }
            catch(e){}
        }

        function OrderOpe(orderBy) {
            if ($('#<%=hdnOrderByOpe.ClientID%>').val() == orderBy) {
                if ($('#<%=hdnOrderDirectionOpe.ClientID%>').val() == 'ASC')
                    $('#<%=hdnOrderDirectionOpe.ClientID%>').val('DESC');
                else $('#<%=hdnOrderDirectionOpe.ClientID%>').val('ASC');
            }
            else $('#<%=hdnOrderDirectionOpe.ClientID%>').val('ASC');

            $('#<%=hdnOrderByOpe.ClientID%>').val(orderBy);
            $('#<%=btnOrderOpe.ClientID%>').click();
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

    <div id="ar-loading" class="tracfin-statement-loading"></div>
    
    <div>
        <asp:Button ID="btnPrev" runat="server" CssClass="button" Text="Suivi d'alerte" PostBackUrl="StatusTrackingAccount.aspx" />
    </div>
    <h2 style="color: #344b56;text-transform:uppercase;">Fiche d'analyse préalable à déclaration de soupçon</h2>

    <div id="div-form">
        <asp:HiddenField ID="hdnEditable" runat="server" />
        <asp:UpdatePanel ID="upAutoSave" runat="server">
            <ContentTemplate>
                <asp:Panel ID="panelAutoSave" runat="server" CssClass="saving-status">
                    <img class="icon-saving" alt="Enregistrement en cours..." src="Styles/Img/saving.png" />
                    <img class="icon-saving-fail" alt="Enregistrement échoué" src="Styles/Img/saving_fail.png" />
                    <img class="icon-saving-done" alt="Enregistrement réussi" src="Styles/Img/saving_done.png" />
                </asp:Panel>
                <asp:Button ID="btnAutoSave" runat="server" OnClick="btnAutoSave_Click" style="display:none" />
            </ContentTemplate>
        </asp:UpdatePanel>
        <fieldset>
            <legend>Références</legend>
            <div>
                <asp:Panel ID="panelRefStatementInt" runat="server">
                    <div class="label">
                        Référence de la déclaration
                    </div>
                    <div>
                        <asp:TextBox ID="txtRefStatementInt" runat="server" Enabled="false"></asp:TextBox>
                    </div>
                </asp:Panel>

                <asp:Panel ID="panelTRACFIN" runat="server" Visible="false">
                    <div class="label">
                        Référence TRACFIN
                    </div>
                    <div>
                        <asp:TextBox ID="txtRefStatementExt" runat="server"></asp:TextBox>
                    </div>
                </asp:Panel>

                <div class="label">
                    Porteur
                </div>
                <div>
                    <asp:TextBox ID="txtBoUserOwner" runat="server" Enabled="false"></asp:TextBox>
                </div>

                <asp:Panel ID="panelBoUserValidator" runat="server" Visible="false">
                    <div class="label">
                        Valideur(s) FLAB-FT
                    </div>
                    <div>
                        <asp:TextBox ID="txtBoUserValidator" runat="server" Enabled="false"></asp:TextBox>
                    </div>
                </asp:Panel>

                <asp:Panel ID="panelDateToValidation" runat="server" Visible="false">
                    <div class="label">
                        Date de présentation à validation
                    </div>
                    <div>
                        <asp:TextBox ID="txtDateToValidation" runat="server" Enabled="false" MaxLength="10"></asp:TextBox>
                    </div>
                </asp:Panel>

                <div class="label">
                    N° déclaration initiale
                </div>
                <div class="info-block">
                    <div class="label">
                        Interne
                    </div>
                    <div>
                        <asp:TextBox ID="txtRefInitStatementInt" runat="server"></asp:TextBox>
                    </div>
                    <div class="label">
                        TRACFIN
                    </div>
                    <div>
                        <asp:TextBox ID="txtRefInitStatementExt" runat="server"></asp:TextBox>
                    </div>
                </div>
            </div>
        </fieldset>

        <fieldset>
            <legend>Identification</legend>
            <asp:UpdatePanel ID="upClient" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="div-client-table">
                        <asp:Repeater ID="rptClient" runat="server" OnItemDataBound="rptClient_ItemDataBound">
                            <HeaderTemplate>
                                <table class="defaultTable2">
                                    <tr>
                                        <th>
                                            <asp:CheckBox ID="ckbCheckAll" runat="server" onchange="SelectAllCheckbox('#div-client-table', $(this)); FireCustomerCheckChanged();" Visible='<%# (hdnEditable.Value == "0") ? false : true %>' />
                                        </th>
                                        <th>Nom</th>
                                        <th>Prénom</th>
                                        <th>DDN</th>
                                        <th>N° compte</th>
                                        <th>IBAN</th>
                                        <th>Clôture</th>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr class='<%#Container.ItemIndex % 2 == 0 ? "row-even" : "row-odd" %>'>
                                    <td style="text-align:center;vertical-align:middle">
                                        <asp:CheckBox ID="ckbCheck" runat="server" Checked='<%# bool.Parse(Eval("Selected").ToString()) %>' onchange="FireCustomerCheckChanged();" Visible='<%# (hdnEditable.Value == "0" || int.Parse(Eval("RefCustomer").ToString()) == _iRefMainCustomer) ? false : true %>' />
                                        <asp:HiddenField ID="hdnRefCustomer" runat="server" Value='<%#Eval("RefCustomer") %>' />
                                    </td>
                                    <td style="vertical-align:middle"><%# Eval("LastName") %></td>
                                    <td style="vertical-align:middle"><%# Eval("FirstName") %></td>
                                    <td style="vertical-align:middle;text-align:center"><%# Eval("BirthDate") %></td>
                                    <td style="vertical-align:middle;text-align:center"><%# Eval("AccountNumber") %></td>
                                    <td style="vertical-align:middle"><%# Eval("IBAN") %></td>
                                    <td style="text-align:center">
                                        <asp:HiddenField ID="hdnCloseAction" runat="server" Value='<%# Eval("RefCloseAction") %>' />
                                        <asp:DropDownList ID="ddlCloseAction" runat="server" Enabled='<%# (hdnEditable.Value == "0") ? false : true %>'>
                                            <asp:ListItem Text="immédiate" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="avec préavis" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="ne pas clôturer" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="en cours de clôture" Value="3" Enabled="false"></asp:ListItem>
                                            <asp:ListItem Text="compte clos" Value="4"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                    <asp:ClientSearch ID="clientSearch1" runat="server" HideDetails="true" />
                    <asp:Button ID="btnCustomerCheckChanged" runat="server" OnClick="ckbCheckClient_CheckedChanged" style="display:none" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </fieldset>

        <fieldset>
            <legend>Motif de la déclaration et nature</legend>
            <asp:UpdatePanel ID="upReasons" runat="server">
                <ContentTemplate>
                    <div style="text-align:center">
                        <asp:Label ID="litArticle" runat="server" CssClass="main-article"></asp:Label>
                    </div>
                    <asp:MultiSelect ID="multiReason" runat="server" />
                    <div>
                        <asp:Panel ID="panelAddOtherReason" runat="server" DefaultButton="btnAddOtherReason">
                            <div class="label">
                                Autre motif
                            </div>
                            <div class="other-reason">
                                <div>
                                    <asp:TextBox ID="txtOtherReasonLabel" runat="server"></asp:TextBox>
                                </div>
                                <div>
                                    <asp:DropDownList ID="ddlOtherReasonArticle" runat="server">
                                        <asp:ListItem Text="Art 1" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Art 2" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="Art 1 et 2" Value="3"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div>
                                    <asp:ImageButton ID="btnAddOtherReason" runat="server" ImageUrl="~/Styles/Img/plus.png" OnClick="btnAddOtherReason_Click" />
                                </div>
                            </div>
                        </asp:Panel>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </fieldset>
    
        <fieldset>
            <legend>Dates référentielles</legend>

            <asp:UpdatePanel ID="upDates" runat="server">
                <ContentTemplate>
                    <div class="label">
                        Date de l’opération à l’origine de la diligence renforcée
                    </div>
                    <div>
                        <asp:TextBox ID="txtOpeOriginDRDate" runat="server" MaxLength="10"></asp:TextBox>
                    </div>

                    <div class="label">
                        Date de la diligence renforcée (<asp:Label ID="lblNbDaysSinceOpeOriginDRDate" runat="server" Text="+0j" />)
                    </div>
                    <div>
                        <asp:TextBox ID="txtDRStartDate" runat="server" MaxLength="10"></asp:TextBox>
                    </div>

                    <div class="label">
                        Date de la décision de déclaration (<asp:Label ID="lblNbDaysDRStartDate" runat="server" Text="+0j" />)
                    </div>
                    <div>
                        <asp:TextBox ID="txtDREndDate" runat="server" MaxLength="10"></asp:TextBox>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </fieldset>

        <fieldset>
            <legend>Période des faits considérés et montants en jeu</legend>

            <div class="label">
                Date de l’ouverture de compte
            </div>
            <div>
                <asp:TextBox ID="txtAccountOpenDate" runat="server" Enabled="false"></asp:TextBox>
            </div>

            <div id="ope-date-sum">
                <asp:UpdatePanel ID="upOpeSum" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="label">
                            Période des faits considérés : 1er flux jusqu’au DRIKO (fin de l’investigation et décision de déclarer)
                        </div>
                        <div class="info-block">
                            <div class="label">
                                Du
                            </div>
                            <div>
                                <asp:TextBox ID="txtPeriodeStartDate" runat="server" Enabled="false" MaxLength="10"></asp:TextBox>
                            </div>
                            <div class="label">
                                Au
                            </div>
                            <div>
                                <asp:TextBox ID="txtPeriodeEndDate" runat="server" Enabled="false" MaxLength="10"></asp:TextBox>
                            </div>
                        </div>

                        <div class="label">
                            Montants en jeu : toutes les opérations créditrices sur le compte toutes catégories confondues - hors flux inter FPE
                        </div>
                        <div style="position:relative">
                            <asp:TextBox ID="txtOpeAmountSum" runat="server" ReadOnly="true" onclick="ShowOpeSelectionDialog();" CssClass="clickable"></asp:TextBox>
                            <asp:Image ID="imgOpe1" runat="server" ImageUrl="~/Styles/Img/list-view.png" Height="28" onclick="ShowOpeSelectionDialog();" CssClass="clickable" Visible="false" style="position:absolute;right:0;top:0" />
                        </div>
                        <div class="label">
                            Nombre d’opérations en jeu : cf ci-dessus
                        </div>
                        <div style="position:relative">
                            <asp:TextBox ID="txtOpeCountSum" runat="server" ReadOnly="true" onclick="ShowOpeSelectionDialog();" CssClass="clickable"></asp:TextBox>
                            <asp:Image ID="imgOpe2" runat="server" ImageUrl="~/Styles/Img/list-view.png" Height="28" onclick="ShowOpeSelectionDialog();" CssClass="clickable" Visible="false" style="position:absolute;right:0;top:0" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>

            <div id="dialog-ope-select" style="display:none;padding:0">
                <asp:UpdatePanel ID="upOpeOffUs" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="div-ope-table">                        
                            <asp:Repeater ID="rptOpeOffUs" runat="server">
                                <HeaderTemplate>
                                    <table class="defaultTable2">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <input type="checkbox" onclick="SelectAllCheckbox('#div-ope-table', $(this)); UpdateNbOpeSelected();"  <%=(hdnEditable.Value == "0") ? "style=\"display:none\"" : "" %> />
                                                </th>
                                                <th><span style="cursor:pointer" onclick="OrderOpe('date')">Date d'opération</span></th>
                                                <th><span style="cursor:pointer" onclick="OrderOpe('date')">Heure</span></th>
                                                <th>Lieu</th>
                                                <th>Objet</th>
                                                <th><span style="cursor:pointer" onclick="OrderOpe('amount')">Montant</span></th>
                                                <th>Catégorie</th>
                                                <th><span style="cursor:pointer" onclick="OrderOpe('client')">Client</span></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                </HeaderTemplate>
                                <ItemTemplate>
                                            <tr class='<%#Container.ItemIndex % 2 == 0 ? "row-even" : "row-odd" %>'>
                                                <td style="text-align:center">
                                                    <asp:CheckBox ID="ckbCheck" runat="server" Checked='<%# bool.Parse(Eval("Selected").ToString()) %>' Enabled='<%# (hdnEditable.Value == "0") ? false : true %>' onchange="UpdateNbOpeSelected();" />
                                                    <asp:HiddenField ID="hdnIdOpe" runat="server" Value='<%# Eval("sIdOpe") %>' />
                                                </td>
                                                <td style="text-align:center"><%# Eval("Date") %></td>
                                                <td style="text-align:center"><%# Eval("Time") %></td>
                                                <td><%# Eval("Place") %></td>
                                                <td><%# Eval("Object") %></td>
                                                <td style="text-align:right"><%# Eval("Amount").ToString().Replace('.',',') %> &euro;</td>
                                                <td><%# Eval("Category") %></td>
                                                <td><%# Eval("CustomerName") %></td>
                                            </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                        </tbody>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                        <div class="div-ope-footer">
                            <asp:Label ID="lblNbOpeSelect" runat="server"></asp:Label> / <asp:Label ID="lblNbOpeTotal" runat="server"></asp:Label>
                        </div>
                        <asp:Button ID="btnUpdateOpeDetails" runat="server" OnClick="btnUpdateOpeDetails_Click" style="display:none" />
                        <asp:HiddenField ID="hdnOrderByOpe" runat="server" Value="date" />
                        <asp:HiddenField ID="hdnOrderDirectionOpe" runat="server" Value="ASC" />
                        <asp:Button ID="btnOrderOpe" runat="server" OnClick="btnOrderOpe_Click" style="display:none" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </fieldset>

        <asp:Panel ID="panelAlertDetails" runat="server">
            <fieldset style="position:relative;">
                <legend>
                    <%--Comment le cas est-il remonté ?--%>
                    <span style="width:440px;display:inline-block;">&nbsp;</span>
                </legend>
                <div id="alert-kind">
                    <asp:UpdatePanel ID="upAlertKind" runat="server" UpdateMode="Always">
                        <ContentTemplate>
                            <asp:Panel ID="panelFakeLegend" runat="server" CssClass="fake-legend">
                                <span class="legend-label">Comment le cas est-il remonté ?</span>
                                <asp:RadioButtonList ID="rblAlertKind" runat="server" CssClass="radioButtonList" AutoPostBack="true" RepeatDirection="Horizontal" OnSelectedIndexChanged="rblAlertKind_SelectedIndexChanged">
                                    <asp:ListItem Text="Automatique" Value="Auto" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="Manuel" Value="Manual"></asp:ListItem>
                                </asp:RadioButtonList>
                            </asp:Panel>
                            <div>
                                <asp:Panel ID="panelAlertAuto" runat="server">
                                    <div class="alert-details">
                                        <asp:Repeater ID="rptAlertInfos" runat="server">
                                            <ItemTemplate>
                                                <div class="alert-infos">
                                                    <div class="label">
                                                        <asp:Label ID="lblAlertInfoLabel" runat="server"><%#Eval("Label") %></asp:Label>
                                                    </div>
                                                    <div>
                                                        <asp:Label ID="lblAlertInfoValue" runat="server"><%#Eval("Value") %></asp:Label>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                    <asp:Panel ID="panelAlertDetailsTable" runat="server" Visible="false" CssClass="alert-details">
                                        <div style="margin-top:5px;overflow:auto;max-height:300px;max-width:929px;display:block">
                                            <asp:Literal ID="litAlertDetailsTable" runat="server"></asp:Literal>
                                        </div>
                                    </asp:Panel>
                                </asp:Panel>
                                <asp:Panel ID="panelAlertManual" runat="server" Visible="false">
                                    <asp:TextBox ID="txtAlertKind" runat="server" MaxLength="500"></asp:TextBox>
                                </asp:Panel>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </fieldset>
        </asp:Panel>

        <fieldset>
            <legend>
                Analyse des faits / Indices de blanchiment
            </legend>
            <div>
                <div>
                    La présente déclaration résulte des constats suivants :
                </div>

                <div class="label">
                    Circulation atypique des fonds
                </div>
                <div class="info-block">
                    <div class="label">
                        Encaissements
                    </div>
                    <div>
                        <asp:TextBox ID="txtAtypicalFundsCirculationIN" runat="server" TextMode="MultiLine"></asp:TextBox>
                    </div>
                    <div class="label">
                        Décaissements
                    </div>
                    <div>
                        <asp:TextBox ID="txtAtypicalFundsCirculationOUT" runat="server" TextMode="MultiLine"></asp:TextBox>
                    </div>
                </div>
                <div class="label">
                    Autres éléments atypiques à la base de cette déclaration
                </div>
                <div>
                    <asp:TextBox ID="txtAtypicalFundsCirculationOTHER" runat="server" TextMode="MultiLine"></asp:TextBox>
                </div>

                <asp:UpdatePanel ID="upWhoStatement" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Panel ID="panelWhoStatement" runat="server" Visible="false">
                            <asp:Literal ID="litFPEDeclare" runat="server"></asp:Literal>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </fieldset>

        <fieldset>
            <legend>
                Documentation annexée à la déclaration
            </legend>
            <div>
                <asp:UpdatePanel ID="upFile" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="div-fs-table">
                            <div class="label">
                                Fiche(s) de synthèse
                            </div>
                            <asp:Repeater ID="rptCustomerFS" runat="server">
                                <HeaderTemplate>
                                    <table class="defaultTable2">
                                        <tr>
                                            <th>Nom</th>
                                            <th>Prénom</th>
                                            <th>DDN</th>
                                            <th>N° compte</th>
                                            <th>IBAN</th>
                                            <th>
                                                <asp:CheckBox ID="ckbCheckAll" runat="server" onchange="SelectAllCheckbox('#div-fs-table', $(this));" Visible='<%# (hdnEditable.Value == "0") ? false : true %>' />
                                            </th>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr class='<%#Container.ItemIndex % 2 == 0 ? "row-even" : "row-odd" %>'>
                                        
                                        <td style="vertical-align:middle"><%# Eval("LastName") %></td>
                                        <td style="vertical-align:middle"><%# Eval("FirstName") %></td>
                                        <td style="vertical-align:middle;text-align:center"><%# Eval("BirthDate") %></td>
                                        <td style="vertical-align:middle;text-align:center"><%# Eval("AccountNumber") %></td>
                                        <td style="vertical-align:middle"><%# Eval("IBAN") %></td>
                                        <td style="text-align:center">
                                            <asp:CheckBox ID="ckbCheck" runat="server" Checked='<%# bool.Parse(Eval("FS").ToString()) %>' Visible='<%# (hdnEditable.Value == "0") ? false : true %>' />
                                            <asp:HiddenField ID="hdnRefCustomer" runat="server" Value='<%#Eval("RefCustomer") %>' />
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                            <asp:Panel ID="panelCustomerFSEmpty" runat="server">
                                Aucune fiche de synthèse
                            </asp:Panel>
                        </div>
                        <div>
                            <div class="label">
                                Autres documents
                            </div>
                            <asp:Repeater ID="rptStatementFiles" runat="server" Visible="false">
                                <HeaderTemplate>
                                    <table class="defaultTable2" style="table-layout:fixed">
                                        <tr>
                                            <th>
                                                Client
                                            </th>
                                            <th style="width:25%;">
                                                Nom
                                            </th>
                                            <th>
                                                Description
                                            </th>
                                            <th>
                                                Type
                                            </th>
                                            <th>
                                                Date
                                            </th>
                                            <th style="width:25px;text-align:center">

                                            </th>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                        <tr class='<%#Container.ItemIndex % 2 == 0 ? "row-even" : "row-odd" %>'>
                                            <td style="vertical-align:middle">
                                                <%#GetCustomerName(Eval("RefCustomer").ToString()) %>
                                            </td>
                                            <td  class="trimmed-filename" style="vertical-align:middle">
                                                <%#Eval("FileName") %>
                                                <asp:Image ID="imgCopy" runat="server" ImageUrl="~/Styles/Img/content_copy.png" title="Copier dans le presse-papier" CssClass="copy-to-clipboard" data-clipboard-text='<%#Eval("FileName") %>' />
                                            </td>
                                            <td style="vertical-align:middle">
                                                <%#Eval("Description") %>
                                            </td>
                                            <td style="vertical-align:middle">
                                                <%#Eval("CategoryLabel") %>
                                            </td>
                                            <td style="vertical-align:middle;text-align:center">
                                                <%#Eval("LastUploadDate") %>
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="ckbSelect" runat="server" Visible='<%# (hdnEditable.Value == "0") ? false : true %>' />
                                            </td>
                                        </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                            <asp:Panel ID="panelStatementFilesEmpty" runat="server">
                                Aucun autre document
                            </asp:Panel>

                            <asp:Panel ID="panelRemoveFileToStatement" runat="server" style="text-align:right;margin-top:10px">
                                <asp:Button ID="btnRemoveFileToStatement" runat="server" Text="Supprimer de la déclaration" CssClass="MiniButton" OnClick="btnRemoveFileToStatement_Click" />
                            </asp:Panel>
                        </div>
                        <asp:Panel ID="panelCustomerFiles" runat="server" Visible="false">
                            <div class="label">
                                Documents client
                            </div>
                            <div class="info-block">
                                <asp:Panel ID="panelFileClientList" runat="server" Visible="false">
                                    <asp:DropDownList ID="ddlFileClientList" runat="server" DataTextField="Text" DataValueField="Value" OnSelectedIndexChanged="ddlFileClientList_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                </asp:Panel>
                                <div>
                                    <asp:ClientFileManager ID="clientFileManager1" runat="server" AutoLoad="true" AllowSelect="true" AllowCheck="false" />
                                </div>
                                <div style="text-align:right;margin-top:10px">
                                    <input type="button" value="Ajouter un nouveau fichier" class="MiniButton" onclick="ShowNewFileDialog();" />
                                </div>
                                <div style="text-align:center;position:relative;top:-18px;width:52%;margin:auto;">
                                    <asp:Button ID="btnAddFileToStatement" runat="server" Text="Annexer les fichiers sélectionnés à la déclaration" CssClass="button" OnClick="btnAddFileToStatement_Click" />
                                </div>
                                <div id="dialog-add-file" style="display:none">
                                    <asp:ClientFileUpload ID="clientFileUpload1" runat="server" ClearForm="true" />
                                </div>
                            </div>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </fieldset>

        <asp:UpdatePanel ID="upSave" runat="server">
            <ContentTemplate>
                <asp:Panel ID="panelSave" runat="server" CssClass="bottom-bar-container">
                    <div class="bottom-bar">
                        <asp:Panel ID="panelMoreOptions" runat="server" CssClass="more-option-bar">
                            <asp:Button ID="btnOtherAction2" runat="server" Text="" Visible="false" CssClass="button" CommandArgument="" OnClick="btnAction_Click" />
                            <asp:Button ID="btnOtherAction" runat="server" Text="Soumettre à validation" CssClass="button" CommandArgument="ENDCREATION" OnClick="btnAction_Click" />
                            <asp:Button ID="btnDownloadXML" runat="server" Text="Télécharger la déclaration au format XML" Visible="false" CssClass="button" OnClick="GetStatementToXml_Click" OnClientClick="ShowLoading('.bottom-bar');setTimeout(function(){HideLoading();},3000);" />
                        </asp:Panel>
                        <div class="main-button-bar">
                            <asp:Button ID="btnSave" runat="server" Text="Enregistrer" CssClass="button" OnClick="btnSave_Click" />
                            <asp:Button ID="btnAction" runat="server" Text="" CommandArgument="" Visible="false" CssClass="button" OnClick="btnAction_Click" />
                            <asp:Button ID="btnMoreOptions" runat="server" CssClass="button more-option-button" OnClientClick="ToggleMoreOption();return false;" />
                        </div>
                    </div>
                </asp:Panel>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnDownloadXML" />
            </Triggers>
        </asp:UpdatePanel>

    </div>

    <div id="dialog-comment" style="display:none">
        <div class="label">
            Commentaire
        </div>
        <div>
            <asp:TextBox ID="txtComment" runat="server" TextMode="MultiLine"></asp:TextBox>
        </div>
        <div style="text-align:right">
            <asp:Button ID="btnOtherActionComment" runat="server" Text="Valider" CssClass="button" OnClick="btnAction_Click" style="display:none" />
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" Runat="Server">

    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            console.log(sender);
            console.log(args);
            pbControl = args.get_postBackElement();
            var panel;
            if (pbControl != null && pbControl.id != null) {
                console.log(pbControl.id);
                if (pbControl.id == '<%=btnSave.ClientID %>' ||
                    pbControl.id == '<%=btnAction.ClientID%>' ||
                    pbControl.id == '<%=btnOtherAction.ClientID%>' ||
                    pbControl.id == '<%=btnOtherAction2.ClientID%>' ||
                    pbControl.id == '<%=btnOtherActionComment.ClientID%>'
                    //|| pbControl.id == '<%=btnDownloadXML.ClientID%>'
                    )
                    panel = '.bottom-bar';
                else if (pbControl.id == '<%=btnUpdateOpeDetails.ClientID%>')
                    panel = '#<%=upOpeSum.ClientID%>';
                else if (pbControl.id.indexOf('rblAlertKind') != -1)
                    panel = '#alert-kind';
                else if (pbControl.id.indexOf('btnCustomerCheckChanged') != -1)
                    panel = '#div-client-table';
                else if (pbControl.id.indexOf('btnUpdateOpeDetails') != -1)
                    panel = '#ope-date-sum';
                else if (pbControl.id.indexOf('btnOrderOpe') != -1)
                    panel = '#div-ope-table';

                ShowLoading(panel);
            }
        }
        function ShowLoading(panel) {
            if (panel != null && panel.length > 0) {
                var width = $(panel).outerWidth();
                var height = $(panel).outerHeight();
                $('#ar-loading').css('width', width);
                $('#ar-loading').css('height', height);
                $('#ar-loading').show();
                $('#ar-loading').position({ my: "left bottom", at: "left bottom", of: panel });
            }
        }
        function EndRequestHandler(sender, args) {
            HideLoading();

            //console.log(args.get_error());
            //if (args.get_error() != undefined) {
            //    AlertMessage('Erreur', 'Une erreur est survenue.');

            //    args.set_errorHandled(true);
            //}
        }
        function HideLoading() {
            $('#ar-loading').hide();
        }
    </script>

</asp:Content>

