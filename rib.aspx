﻿<%@ Page Language="C#" Title="Votre Relevé d'Identité Bancaire - mon Compte-Nickel" AutoEventWireup="true" CodeFile="rib.aspx.cs" Inherits="rib" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    
    <link type="text/css" href="Styles/rib.css" rel="stylesheet" />

    <script type="text/javascript">
        
        setTimeout(function () { window.print(); }, 1000);

        function Print() {
            window.print();
            document.getElementById('message1').style.visibility = 'visible';
        }
    </script>

    <style type="text/css" media="print">
        .NoPrint
        {
            display:none;
        }
        
    </style>

</head>
<body>
    <form id="form1" runat="server">
        <div style="width: 100%;">
            <div style="width: 22cm; margin: auto">
                <div class="NoPrint" id="panelBtn1" runat="server" style="width:19cm;margin:auto">
                    <div style="display:inline-block;width:50%;float:left;text-align:left">
                        <input type="button" id="btnPrint" value="Imprimer" class="button" onclick="javascript: Print();" />
                    </div><div style="display:inline-block;width:50%;float:right;text-align:right">
                        <input type="button" id="btnClose" value="Fermer" class="button" onclick="javascript: window.location = 'vos-operations.aspx';" style="display:none" />
                        <input type="button" id="Button1" value="Fermer" class="button" onclick="javascript: window.close();" />
                    </div>
                    <div>
                        <span id="message1" style="font-size:1.2em;font-style:italic;visibility:hidden">Si le bouton "Imprimer" ne fonctionne pas, veuillez appuyer sur les touches Ctrl+P de votre clavier.</span>
                    </div>
                    <br />
                </div>
                <asp:Repeater ID="rRib" runat="server">
                    <ItemTemplate>
                        <div class="rib">
                            <div style="display: table; width: 100%; margin-top: 20px">
                                <div style="display: table-row">
                                    <div style="display: table-cell; width: 2.5cm">
                                        <img alt="" src="./Styles/Img/logo-nickel.png" style="width: 2.4cm" />
                                    </div>
                                    <div class="ribTitle" style="display: table-cell; vertical-align: middle; text-align: center">
                                        Relevé d'identité bancaire
                                    </div>
                                </div>
                            </div>
                            <div style="display: table; width: 90%; margin: 20px auto;">
                                <div style="display: table-row">
                                    <div style="display: table-cell; width: 50%;">
                                        <div class="box" style="padding-right: 10px">
                                            <div class="box-title">Domiciliation</div>
                                            <div class="box-content">
                                                <div>
                                                    <asp:Label ID="lblAgencyName1" runat="server">
                                        <%# Eval("AgencyName").ToString() %>
                                                    </asp:Label>
                                                </div>
                                                <div>
                                                    <asp:Label ID="lblAgencyAddress1" runat="server">
                                        <%# Eval("AgencyAddress1").ToString() %>
                                                    </asp:Label>
                                                </div>
                                                <div>
                                                    <asp:Label ID="lblAgencyZipcode1" runat="server">
                                        <%# Eval("AgencyZipcode").ToString() %>
                                                    </asp:Label>
                                                    <asp:Label ID="lblAgencyCity1" runat="server">
                                        <%# Eval("AgencyCity").ToString() %>
                                                    </asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="display: table-cell; width: 50%">
                                        <div class="box" style="padding-left: 10px">
                                            <div class="box-title">Titulaire</div>
                                            <div class="box-content">
                                                <div>
                                                    <asp:Label ID="lblClientCivility" runat="server">
                                        <%# Eval("ClientCivility").ToString() %>
                                                    </asp:Label>
                                                    <asp:Label ID="lblClientLastName1" runat="server">
                                        <%# Eval("ClientLastName").ToString() %>
                                                    </asp:Label>
                                                    <asp:Label ID="lblClientFirstName1" runat="server">
                                        <%# Eval("ClientFirstName").ToString() %>
                                                    </asp:Label>
                                                </div>
                                                <div>
                                                    <asp:Label ID="lblClientAddress1" runat="server">
                                        <%# Eval("ClientAddress1").ToString() %>
                                                    </asp:Label>
                                                </div>
                                                <div>
                                                    <asp:Label ID="lblClientZipcode1" runat="server">
                                        <%# Eval("ClientZipcode").ToString() %>
                                                    </asp:Label>
                                                    <asp:Label ID="lblClientCity1" runat="server">
                                        <%# Eval("ClientCity").ToString() %>
                                                    </asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="width: 90%; margin: 20px auto">
                                <div style="display: table; width: 100%">
                                    <div style="display: table-row" class="row-title">
                                        <div style="display: table-cell; width: 27%">
                                            Code banque
                                        </div>
                                        <div style="display: table-cell; width: 27%">
                                            Code guichet
                                        </div>
                                        <div style="display: table-cell; width: 36%">
                                            N° de compte
                                        </div>
                                        <div style="display: table-cell; width: 10%">
                                            Clé
                                        </div>
                                    </div>
                                    <div style="display: table-row" class="row-content">
                                        <div style="display: table-cell; width: 27%">
                                            <div style="position: relative">
                                                <div style="position: absolute; z-index: 10; top: 3px; left: 30px; height: 20px;">
                                                    <asp:Label ID="lblBankCode1" runat="server">
                                        <%# Eval("BankCode").ToString() %>
                                                    </asp:Label>
                                                </div>
                                                <div style="position: absolute; z-index: 1; height: 20px; top: 0; left: 0;">
                                                    <img alt="img" src="./Styles/Img/pixelGris.jpg" style="width: 95px; height: 20px" />
                                                </div>
                                            </div>
                                        </div>
                                        <div style="display: table-cell; width: 27%;">
                                            <div style="position: relative">
                                                <div style="position: absolute; z-index: 10; top: 3px; left: 30px; height: 20px;">
                                                    <asp:Label ID="lblBranchCode1" runat="server">
                                        <%# Eval("BranchCode").ToString() %>
                                                    </asp:Label>
                                                </div>
                                                <div style="position: absolute; z-index: 1; height: 20px; top: 0; left: 0;">
                                                    <img alt="img" src="./Styles/Img/pixelGris.jpg" style="width: 95px; height: 20px" />
                                                </div>
                                            </div>
                                        </div>
                                        <div style="display: table-cell; width: 36%;">
                                            <div style="position: relative">
                                                <div style="position: absolute; z-index: 10; top: 3px; left: 27px; height: 20px;">
                                                    <asp:Label ID="lblAccountNumber1" runat="server">
                                        <%# Eval("AccountNumber").ToString() %>
                                                    </asp:Label>
                                                </div>
                                                <div style="position: absolute; z-index: 1; height: 20px; top: 0; left: 0;">
                                                    <img alt="img" src="./Styles/Img/pixelGris.jpg" style="width: 126px; height: 20px" />
                                                </div>
                                            </div>
                                        </div>
                                        <div style="display: table-cell; width: 10%;">
                                            <div style="position: relative">
                                                <div style="position: absolute; z-index: 10; top: 3px; left: 13px; height: 20px;">
                                                    <asp:Label ID="lblKey1" runat="server">
                                        <%# Eval("Key").ToString() %>
                                                    </asp:Label>
                                                </div>
                                                <div style="position: absolute; z-index: 1; height: 20px; top: 0; left: 0;">
                                                    <img alt="img" src="./Styles/Img/pixelGris.jpg" style="width: 40px; height: 20px" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style="display: table; width: 100%; margin-top: 5px">
                                    <div style="display: table-row" class="row-title">
                                        <div style="display: table-cell; width: 100%; text-align: left; padding-left: 15px;">
                                            B.I.C./SWIFT
                                        </div>
                                    </div>
                                    <div style="display: table-row" class="row-content">
                                        <div style="display: table-cell; width: 100%">
                                            <div style="position: relative">
                                                <div style="position: absolute; z-index: 10; top: 3px; left: 150px; height: 20px;">
                                                    <asp:Label ID="lblBic1" runat="server">
                                        <%# Eval("Bic").ToString() %>
                                                    </asp:Label>
                                                </div>
                                                <div style="position: absolute; z-index: 1; height: 20px; top: 0; left: 0;">
                                                    <img alt="img" src="./Styles/Img/pixelGris.jpg" style="width: 363px; height: 20px" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style="display: table; width: 100%; margin-top: 5px">
                                    <div style="display: table-row" class="row-title">
                                        <div style="display: table-cell; width: 100%; text-align: left; padding-left: 15px">
                                            IBAN (Internationnal Bank Account Number)
                                        </div>
                                    </div>
                                    <div style="display: table-row" class="row-content">
                                        <div style="display: table-cell; width: 100%">
                                            <div style="position: relative">
                                                <div style="position: absolute; z-index: 10; top: 3px; left: 80px; height: 20px;">
                                                    <asp:Label ID="lblIban1" runat="server">
                                        <%# Eval("Iban").ToString() %>
                                                    </asp:Label>
                                                </div>
                                                <div style="position: absolute; z-index: 1; height: 20px; top: 0; left: 0;">
                                                    <img alt="img" src="./Styles/Img/pixelGris.jpg" style="width: 363px; height: 20px" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="rib-footer">
                                <div>FINANCIERE DES PAIEMENTS ELECTRONIQUES S.A.S. au capital de 468 211,00 euros,</div>
                                <div>RCS Créteil B 753 886 092, TVA intracommunautaire FR80753886092</div>
                                <div>Siège social 18 avenue Winston Churchill, 94220 Charenton-le-Pont</div>
                            </div>
                        </div>
                    </ItemTemplate>

                </asp:Repeater>
            </div>
        </div>

        <asp:HiddenField ID="hfNbRib" runat="server" Value="4" /> 
    </form>
</body>


</html>
