﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AgencyAlertSearch : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            authentication auth = authentication.GetCurrent();

            if (auth != null)
            {
                bool bAction = false;
                bool bView = false;
                if (!Agency.HasAlertRights("SC_AgencyAlertSearch", auth.sToken, out bView, out bAction))
                    Response.Redirect("Default.aspx");
                else
                {
                    kycAlert.ViewRights = bView;
                    kycAlert.ActionRights = bAction;
                }
            }
        }
    }
}