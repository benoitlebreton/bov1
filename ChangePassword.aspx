﻿<%@ Page Title="Changement du mot de passe - BO Compte-Nickel" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ChangePassword.aspx.cs" Inherits="ChangePassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <script type="text/javascript">
                _pwdLength = false;
                _pwdCapital = false;
                _pwdNumber = false;
                _pwdSpecial = false;
                _pwdLetter = false;
                pwdValid = false;

                $(document).ready(function () {
                    onLoad();
                });

                function checkAllReqFields() {
                    var isOK = false;
                    if (checkRequiredField("<%=OldPswd.ClientID%>") && checkRequiredField("pswd") && checkRequiredField("<%=pswdConfirm.ClientID%>")) {
                        activeButton();
                        isOK = true;
                    }
                    else {
                        $("input[type=submit]").attr("disabled", "disabled");
                        isOK = false;
                    }
                    return isOK;
                }

                function onLoad() {
                    //alert("document ready");
                    $('.PasswordInfo').hide();
                    $('.PasswordConfirmInfo').hide();

                    $("#<%=OldPswd.ClientID%>").focus(function () {
                        $('.PasswordConfirmInfo').hide();
                        $('.PasswordInfo').hide();
                    }).blur(function () {
                        checkConfirmationNewPassword();
                        checkAllReqFields();
                    });

                    $('#pswd').keyup(function () {

                        var pswd = $(this).val();
                        //validate the length
                        if (pswd.length < 8) {
                            $('#length').removeClass('valid').addClass('invalid');
                            _pwdLength = false;
                        } else {
                            $('#length').removeClass('invalid').addClass('valid');
                            _pwdLength = true;
                        }
                        //validate letter
                        if (pswd.match(/[A-z]/)) {
                            $('#letter').removeClass('invalid').addClass('valid');
                            _pwdLetter = true;
                        } else {
                            $('#letter').removeClass('valid').addClass('invalid');
                            _pwdLetter = false;
                        }

                        //validate capital letter
                        if (pswd.match(/[A-Z]/)) {
                            $('#capital').removeClass('invalid').addClass('valid');
                            _pwdCapital = true;
                        } else {
                            $('#capital').removeClass('valid').addClass('invalid');
                            _pwdCapital = false;
                        }

                        //validate number
                        if (pswd.match(/\d/)) {
                            $('#number').removeClass('invalid').addClass('valid');
                            _pwdNumber = true;
                        } else {
                            $('#number').removeClass('valid').addClass('invalid');
                            _pwdNumber = false;
                        }

                        //validate special character
                        if (pswd.match(/[!$%^&*()_+|~\-={}\[\]:";'<>?,.\/@]/)) {
                            $('#special').removeClass('invalid').addClass('valid');
                            _pwdSpecial = true;
                        } else {
                            $('#special').removeClass('valid').addClass('invalid');
                            _pwdSpecial = false;
                        }

                        if (_pwdCapital && _pwdLength && _pwdLetter && _pwdNumber && _pwdSpecial) {
                            pwdValid = true;
                            //$("input[type=button]").removeAttr("disabled");
                        }
                        else {
                            pwdValid = false;
                            //$("input[type=button]").attr("disabled", "disabled");
                        }

                    }).focus(function () {
                        $('.PasswordConfirmInfo').hide();
                        $('.PasswordInfo').show();
                    }).blur(function () {
                        $('.PasswordInfo').hide();
                        checkAllReqFields();
                    });

                    $("#<%=pswdConfirm.ClientID%>").focus(function () {
                        $('.PasswordConfirmInfo').hide();
                        $('.PasswordInfo').hide();
                        if (pwdValid) {
                            activeButton();
                        }
                        else {
                            $("input[type=submit]").attr("disabled", "disabled");
                        }
                    }).blur(function () {
                        checkConfirmationNewPassword();
                        checkAllReqFields();
                    });
                }

                function onClickChangePassword() {
                    $('#<%= lblInfoMessage.ClientID %>').html('');
                    //alert(pwdValid + "-" + checkAllReqFields() + "-" + checkConfirmationNewPassword());

                    if (pwdValid && checkAllReqFields() && checkConfirmationNewPassword()) {
                        __doPostBack('__Page', 'changePassword');
                    }
                    else {
                        checkConfirmationNewPassword();
                        checkAllReqFields();
                    }

                    return false;
                }

                function checkConfirmationNewPassword() {
                    var pswdNew = $('#pswd').val();
                    var pswdConfirm = $('#<%=pswdConfirm.ClientID%>').val();
                    var isOk = false;
                    //validate Confirmation New Password
                    if (pswdNew != pswdConfirm) {
                        $('.PasswordConfirmInfo').show();
                        isOk = false;
                        $("input[type=submit]").attr("disabled", "disabled");
                    } else {
                        $('.PasswordConfirmInfo').hide();
                        isOk = true;
                        
                    }

                    return isOk;
                }

                function activeButton() {
                    $("input[type=submit]").removeAttr("disabled");
                    $('#<%=btnChangePassword.ClientID%>').click(onClickChangePassword);
                }

                function checkRequiredField(fieldID, clientID) {
                    var isOk = false;
                    var field = fieldID;

                    if (clientID != null) {
                        field = clientID;
                    }
                    else
                        field = fieldID;

                    if ($('#' + field) != null && $('#' + field).val() != null) {
                        if ($('#' + field).val().length > 0) {
                            isOk = true;
                            $('#REQ-' + fieldID).css('visibility', 'hidden');
                        }
                        else {
                            $('#REQ-' + fieldID).css('visibility', 'visible');
                            isOk = false;
                        }
                    }
                    else
                        isOk = true;

                    return isOk;
                }

                function showMessage(title, message, redirect) {
                    var sTitle = "Changement du mot de passe";
                    if (message != null && message.trim().length > 0)
                        $('#<%=lblInfoMessage.ClientID%>').html(message.trim());
                    if (title != null && title.trim().length > 0)
                        sTitle = title;

                    $('#dialog-Message').dialog({
                        resizable: false,
                        draggable: false,
                        modal: true,
                        minWidth: 500,
                        title: sTitle,
                        buttons: [{ text: 'Fermer', click: function () { $(this).dialog('close'); if (redirect == "true") { document.location.href = "Default.aspx"; } } }]
                    });

                }

            </script>
            <h2 style="color: #344b56; margin-bottom: 10px; text-transform:uppercase">Changement du mot de passe</h2>
            <asp:Panel ID="panelAgencyPassword" runat="server" Style="width: 100%" DefaultButton="btnChangePassword">
                <div style="margin: 10px auto; width: 50%">
                    <div class="AgencyPasswordClass" id="AgencyPassword">
                        <asp:Panel ID="panelOldPassword" runat="server" class="PasswordBox">
                            <ul>
                                <li>
                                    <label for="<%=OldPswd.ClientID %>">
                                        Ancien mot de passe :</label>
                                    <label id="REQ-<%= OldPswd.ClientID%>" class="reqStarStyle" style="color: Red; visibility: hidden">
                                        *</label>
                                </li>
                                <li>
                                    <span>
                                        <asp:TextBox ID="OldPswd" runat="server" TextMode="Password" autocomplete="off"></asp:TextBox>
                                    </span>
                                </li>
                            </ul>
                        </asp:Panel>
                        <div id="AgencyPasswordForm" class="PasswordBox">
                            <ul>
                                <li>
                                    <label for="pswd">
                                        Nouveau mot de passe :</label>
                                    <label id="REQ-pswd" class="reqStarStyle" style="color: Red; visibility: hidden">
                                        *</label>
                                    <span>
                                        <input id="pswd" type="password" name="pswd" autocomplete="off"/>
                                    </span></li>
                                <li>
                                    <label for="<%= pswdConfirm.ClientID%>">
                                        Confirmation nouveau mot de passe :</label>
                                    <label id="REQ-<%= pswdConfirm.ClientID%>" class="reqStarStyle" style="color: Red; visibility: hidden">
                                        *</label>
                                    <span>
                                        <asp:TextBox ID="pswdConfirm" runat="server" TextMode="Password" autocomplete="off"></asp:TextBox>
                                    </span></li>
                            </ul>
                        </div>
                        <div style="position:relative">
                            <div class="PasswordInfo" style="display: none">
                                <h4 style="color:#344b56">Le mot de passe doit contenir au moins :</h4>
                                <ul>
                                    <li id="letter" class="invalid"><strong>1 lettre</strong></li>
                                    <li id="capital" class="invalid"><strong>1 lettre majuscule</strong></li>
                                    <li id="number" class="invalid"><strong>1 chiffre</strong></li>
                                    <li id="special" class="invalid"><strong>1 caractère spécial (!, $, %, @, _, ...)</strong></li>
                                    <li id="length" class="invalid"><strong>8 caractères</strong></li>
                                </ul>
                            </div>
                        </div>
                        <div class="PasswordConfirmInfo" style="text-align: center; display: none">
                            <ul style="padding-left: 0; margin: auto">
                                <li class="invalid" style="font-weight: bold;">Les 2 mots de passe sont différents</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div style="margin-top: 10px; text-align: center; padding:10px">
                    <div class="sideBySideDiv" style="height: 20px; position: relative; top: 5px">
                        <asp:UpdateProgress ID="UpdateProgress2" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
                            DisplayAfter="100" DynamicLayout="true">
                            <ProgressTemplate>
                                <img title="" alt="" style="width: 20px" border="0" src="Styles/Img/loading.gif" />
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </div>
                    <div class="sideBySideDiv" style="margin-top: 10px">
                        <%--<asp:Button ID="Button1" runat="server" OnClick="btnChangePassword_Click"
                        OnPreRender="Button_PreRender" CssClass="button" ValidationGroup="PassValidation" />--%>
                        <asp:Button ID="btnChangePassword" runat="server" CssClass="button" OnClientClick="onClickChangePassword()"
                            Enabled="false" Text="Changer le mot de passe" style="width: 300px; height: 40px; padding: 0; font-weight: bold" />
                    </div>
                    
                </div>

                <div id="dialog-Message" style="display:none">
                    <asp:Label ID="lblInfoMessage" runat="server" CssClass="formLabel" Style="font-weight: bold"></asp:Label>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

