﻿<%@ Page Title="Recherche inscription - BO Compte-Nickel" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="RegistrationSearchV2.aspx.cs" Inherits="RegistrationSearchV2" %>

<%@ Register Src="~/FILTRES/Period2.ascx" TagPrefix="asp" TagName="Period" %>
<%@ Register Src="~/API/ClientSearch.ascx" TagPrefix="asp" TagName="ClientSearch" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .ui-autocomplete {
            max-height: 130px;
        }

        .ui-combobox {
            position: relative;
            display: inline-block;
        }

        .ui-combobox-toggle {
            position: absolute;
            top: 0;
            bottom: 0;
            margin-left: -1px;
            padding: 0;
            /* support: IE7 */
            *height: 1.7em;
            *top: 0.1em;
        }

        .ui-combobox-input {
            margin: 0;
            padding: 0.3em;
            width: 300px;
            height: 24px;
        }

        table#MainContent_gvRegistrationSearchResult tr td:first-of-type {
            padding: 0 5px 0 0 !important;
            background-color: #fff;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".tools-tabs").tabs({
                activate: function (event, ui) {
                    $('#<%=hdnActiveTab.ClientID%>').val($(".tools-tabs").tabs("option", "active"));
                }
            });

            //setTimeout(function () { $('#<%=btnRegistrationSearch.ClientID%>').click(); }, 2000);
        });

        function UserGroupChanged() {
            $('#<%=btnGroupUserChanged.ClientID%>').click();
        }

        (function ($) {
                $.widget("ui.combobox", {
                    _create: function () {
                        var input,
                            that = this,
                            wasOpen = false,
                            select = this.element.hide(),
                            selected = select.children(":selected"),
                            value = selected.val() ? selected.text() : "",
                            wrapper = this.wrapper = $("<span>")
                            .addClass("ui-combobox")
                            .insertAfter(select);
                        function removeIfInvalid(element) {
                            var value = $(element).val(),
                                matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex(value) + "$", "i"),
                                valid = false;
                            select.children("option").each(function () {
                                if ($(this).text().match(matcher)) {
                                    this.selected = valid = true;
                                    return false;
                                }
                            });
                            if (!valid) {
                                // remove invalid value, as it didn't match anything
                                $(element)
                                    .val("")
                                    .attr("title", value + " n'existe pas")
                                    .tooltip("open");
                                select.val("");
                                setTimeout(function () {
                                    input.tooltip("close").attr("title", "");
                                }, 2500);
                                input.data("ui-autocomplete").term = "";
                            }
                        }
                        input = $("<input>")
                            .appendTo(wrapper)
                            .val(value)
                            .attr("title", "")
                            .addClass("ui-state-default ui-combobox-input")
                            .autocomplete({
                                delay: 0,
                                minLength: 0,
                                source: function (request, response) {
                                    var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
                                    response(select.children("option").map(function () {
                                        var text = $(this).text();
                                        if (this.value && (!request.term || matcher.test(text)))
                                            return {
                                                label: text.replace(
                                                    new RegExp(
                                                    "(?![^&;]+;)(?!<[^<>]*)(" +
                                                    $.ui.autocomplete.escapeRegex(request.term) +
                                                    ")(?![^<>]*>)(?![^&;]+;)", "gi"
                                                    ), "<strong>$1</strong>"),
                                                value: text,
                                                option: this
                                            };
                                    }));
                                },
                                select: function (event, ui) {
                                    ui.item.option.selected = true;
                                    that._trigger("selected", event, {
                                        item: ui.item.option
                                    });
                                },
                                change: function (event, ui) {
                                    if (!ui.item) {
                                        removeIfInvalid(this);
                                    }
                                }
                            })
                            .addClass("ui-widget ui-widget-content ui-corner-left");
                        input.data("ui-autocomplete")._renderItem = function (ul, item) {
                            return $("<li>")
                            .append("<a>" + item.label + "</a>")
                            .appendTo(ul);
                        };
                        $("<a>")
                            .attr("tabIndex", -1)
                            .attr("title", "Afficher tout")
                            .tooltip()
                            .appendTo(wrapper)
                            .button(
                                {
                                    icons: {
                                        primary: "ui-icon-triangle-1-s"
                                    },
                                    text: false
                                }
                            )
                            .removeClass("ui-corner-all")
                            .addClass("ui-corner-right ui-combobox-toggle")
                            .mousedown(function () {
                                wasOpen = input.autocomplete("widget").is(":visible");
                            })
                            .click(function () {
                                input.focus();
                                // close if already visible
                                if (wasOpen) {
                                    return;
                                }
                                // pass empty string as value to search for, displaying all results
                                input.autocomplete("search", "");
                            });
                        input.tooltip({
                            tooltipClass: "ui-state-highlight"
                        });
                    },
                    _destroy: function () {
                        this.wrapper.remove();
                        this.element.show();
                    }
                });

                //$('.tooltip').tooltip({ items: "span[tooltip]" });
            })(jQuery);
        function date_picker() {
        }
        function RadioButtonClassPeriod() {

        }
        function FilterStatusDisplayManagement() {
                //if ($('#<%= divGridView.ClientID%>') != null && $('#<%= divGridView.ClientID%>').is(':visible'))
            //$('#panelFilterStatus').show();
            //else
            //$('#panelFilterStatus').hide();
        }

        function initTooltip() {
            $('.trimmer').tooltip({
                position: {
                    my: "center bottom-10",
                    at: "center top",
                    using: function (position, feedback) {
                        $(this).css(position);
                        $("<div>")
                            .addClass("arrow")
                            .addClass(feedback.vertical)
                            .addClass(feedback.horizontal)
                            .appendTo(this);
                    }
                }
            });
        }

        function onChangeNbResult() {
            //clickRegistrationSearch(1);
            $('#<%=btnRegistrationSearch.ClientID%>').click();
            return false;
        }

        function nextPage() {
            console.log($('#<%= ddlPage.ClientID %>').val());
            var pageIndex = parseInt($('#<%= ddlPage.ClientID %>').val()) + 1;
            $('#<%= ddlPage.ClientID %>').val(pageIndex);
            console.log(pageIndex);
            console.log($('#<%= ddlPage.ClientID %>').val());

            //clickRegistrationSearch(pageIndex);
            $('#<%=btnRegistrationSearch.ClientID%>').click();
            return false;
        }

        function previousPage() {
            var pageIndex = parseInt($('#<%= ddlPage.ClientID %>').val()) - 1;
            $('#<%= ddlPage.ClientID %>').val(pageIndex);

            //clickRegistrationSearch(pageIndex);
            $('#<%=btnRegistrationSearch.ClientID%>').click();
            return false;
        }

        function onChangePageNumber() {
            var pageIndex = $('#<%= ddlPage.ClientID %>').val();

            //clickRegistrationSearch(pageIndex);
            $('#<%=btnRegistrationSearch.ClientID%>').click();
            return false;
        }

        function showRegistration(registrationNumber, refTransaction) {
            console.log('show registration : '+registrationNumber + "," +refTransaction);
            __doPostBack("upRegistration", "showRegistration;" + registrationNumber + ";" + refTransaction);
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div style="position: relative">
        <div style="position: absolute; top: 30px">
            <asp:Button ID="btnPrevious_2" runat="server" Text="Précédent" OnClick="onClickPreviousButton" CssClass="button" />
        </div>
    </div>

    <div id="ar-loading"></div>

    <div style="border: 3px solid #344b56; border-radius: 8px; width: 100%; margin: auto; margin-bottom: 20px; text-transform: uppercase" class="tools-tabs">

        <asp:Repeater ID="rptTabs" runat="server">
            <HeaderTemplate>
                <ul>
            </HeaderTemplate>
            <ItemTemplate>
                <%# Eval("tab") %>
            </ItemTemplate>
            <FooterTemplate>
                </ul>
            </FooterTemplate>
        </asp:Repeater>

        <div id="divRegistrationSearch" runat="server" visible="true">
            <asp:Panel ID="panelRegistrationSearch" runat="server" DefaultButton="btnRegistrationSearch">
                <asp:UpdatePanel ID="upFilterStatus" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="panelFilterStatus">
                            <asp:Panel ID="panelFilterControlStatus" runat="server" Visible="false" style="width: 25%; float: left; margin-top: -6px;">
                                <asp:Label ID="lblControlStatus" runat="server" AssociatedControlID="ddlControlStatus" Style="font-weight: bold; text-transform: none">
                                Etat contrôle
                                </asp:Label>
                                <div style="margin-top: 3px;">
                                    <asp:DropDownList ID="ddlControlStatus" runat="server" Enabled="true"></asp:DropDownList>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="panelFilterAccountType" runat="server" Visible="false" style="width: 30%; float: left; margin-top: -6px;">
                                <asp:Label ID="lblAccountType" runat="server" AssociatedControlID="ddlAccountType" Style="font-weight: bold; text-transform: none">
                                Type de Compte
                                </asp:Label>
                                <div style="margin-top: 3px;">
                                    <asp:DropDownList ID="ddlAccountType" runat="server">
                                        <asp:ListItem Text="Tous" Value=""></asp:ListItem>
                                        <asp:ListItem Text="Compte-Nickel" Value="NOB"></asp:ListItem>
                                        <asp:ListItem Text="Compte-Nickel 12/18 ans" Value="CNJ"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </asp:Panel>
                            <div style="width: 12%; float: left; margin-top: -6px;">
                                <asp:Label ID="lblByUser" runat="server" AssociatedControlID="ddlByUserGroup" Style="font-weight: bold; text-transform: none">
                                Par utilisateur
                                </asp:Label>
                                <div style="margin-top: 3px;">
                                    <asp:DropDownList ID="ddlByUserGroup" runat="server" AppendDataBoundItems="true" onchange="UserGroupChanged();"></asp:DropDownList>
                                </div>
                                <asp:Button ID="btnGroupUserChanged" runat="server" style="display:none" OnClick="ddlByUserGroup_SelectedIndexChanged" />
                            </div>
                            <div style="width: 25%; float: left; margin-top: -6px;">
                                <div style="margin-top: 24px;">
                                    <asp:DropDownList ID="ddlByUser" runat="server" Visible="false" style="width:300px;max-width:300px;"></asp:DropDownList>
                                </div>
                            </div>
                            <div style="clear: both"></div>
                            <asp:Panel ID="panelFilterByClient" runat="server" Visible="false" style="margin-top:20px;">
                                <asp:Label ID="lblByClient" runat="server" AssociatedControlID="csClientSearchFilter" Style="font-weight: bold; text-transform: none">
                                    Par client
                                </asp:Label>
                                <div style="padding-top:5px;width:500px; height:50px;">
                                    <asp:ClientSearch ID="csClientSearchFilter" runat="server" style="width:500px;height:50px;" />
                                </div>
                            </asp:Panel>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnGroupUserChanged" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <div style="width: 100%; text-align: center; margin-top:20px;">
                    <asp:Button ID="btnDeleteFilter" runat="server" OnClientClick="showLoading();" OnClick="btnDeleteFilter_Click" Text="Supprimer les filtres" Style="width: 250px" CssClass="buttonHigher" Visible="false" />
                    <asp:Button ID="btnRegistrationSearch" runat="server" OnClick="btnRegistrationSearch_Click" OnClientClick="showLoading();" Text="RECHERCHER" Style="width: 200px" CssClass="buttonHigher" />
                    <asp:UpdateProgress ID="upGridViewProgress" runat="server" AssociatedUpdatePanelID="upGridView">
                        <ProgressTemplate>
                            <div style="display: inline-block;">
                                <img src="./Styles/Img/loading.gif" alt="loading" width="30px" height="30px" />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </div>
                
                <asp:UpdatePanel ID="upGridView" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="divGridViewInfo" runat="server" visible="false" style="display: table; width: 100%; margin-bottom: 10px">
                            <div style="display: table-row">
                                <div style="display: table-cell; width: 50%; padding-top: 20px; visibility:hidden">
                                    <asp:Label ID="lblNbOfResults" runat="server">0</asp:Label>
                                    résultat(s)
                                </div>
                                <div style="display: table-cell; width: 50%; text-align: right;">
                                    Nb de résultats par page :
                                    <asp:DropDownList ID="ddlNbResult" runat="server" AutoPostBack="false" onchange="onChangeNbResult();">
                                        <asp:ListItem Value="10">10</asp:ListItem>
                                        <asp:ListItem Value="25" Selected="True">25</asp:ListItem>
                                        <asp:ListItem Value="50">50</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <asp:Panel ID="divGridView" runat="server" Visible="false">
                            <div style="border: 3px solid #344b56; border-top-left-radius: 8px; border-top-right-radius: 8px; width: 100%; margin: auto"
                                visible="false">
                                <%--OnPreRender="GridView1_PreRender" OnRowCreated="GridView1_RowCreated" OnRowDataBound="GridView1_RowDataBound"--%>
                                <asp:GridView ID="gvRegistrationSearchResult" runat="server" AllowSorting="false"
                                    AllowPaging="false" AutoGenerateColumns="False" GridLines="None" CellPadding="10"
                                    DataKeyNames="RefTransaction" ShowHeader="true" Width="100%" SortedDescendingCellStyle-Font-Overline="true"
                                    ViewStateMode="Disabled" EnableSortingAndPagingCallbacks="false" BorderWidth="0"
                                    BackColor="Transparent" OnRowDataBound="GridView1_RowDataBound">
                                    <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="#344b56" Font-Bold="true" />
                                    <EmptyDataTemplate>
                                        Aucune donnée ne correspond à la recherche
                                    </EmptyDataTemplate>
                                    <Columns>
                                        <asp:TemplateField HeaderText="" ControlStyle-Width="1px" ControlStyle-BackColor="White">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnServiceTag" runat="server" Value='<%#Eval("ServiceTag") %>' />
                                                <img src='<%#tools.GetAccountTypeImg(Eval("ServiceTag").ToString()) %>' width="40px" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="N° Pack" SortExpression="RegistrationNumber" InsertVisible="false"
                                            ControlStyle-CssClass="" ControlStyle-Width="100px">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblRegistrationNumber" Text='<%# Eval("RegistrationCode") %>'
                                                    Style="display: none"></asp:Label>
                                                <asp:Label runat="server" ID="lblRefTransaction" Text='<%# Eval("RefTransaction") %>'
                                                    Style="display: none"></asp:Label>

                                                <asp:Label ID="lblRegistrationDetails" runat="server" CssClass="tooltip"
                                                    ToolTip='<%#Eval("PackNumber") + " - " + Eval("LastName") + " " + Eval("FirstName") + " né(e) le " + Eval("BirthDate") %>'>
                                                    <%#  Eval("PackNumber") %>
                                                </asp:Label>
                                                <%--<asp:Label runat="server" ID="lblPackNumber" Text='<%# Eval("PackNumber") %>'></asp:Label>
                                                <asp:Label runat="server" ID="lblClientLastName" Text='<%# Eval("LastName")%>' CssClass="tooltip"
                                                    ToolTip='<%# Eval("LastName")%>'></asp:Label>
                                                <asp:Label runat="server" ID="lblClientFirstName" Text='<%# Eval("FirstName") %>'
                                                    CssClass="tooltip" ToolTip='<%# Eval("FirstName")%>'></asp:Label>
                                                <asp:Label runat="server" ID="lblClientBirthDate" Text='<%# Eval("BirthDate")%>'></asp:Label>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--<asp:TemplateField HeaderText="Etat" InsertVisible="False" SortExpression="Status"
                                            ControlStyle-CssClass="trimmer" ControlStyle-Width="170px">
                                            <ItemTemplate>
                                                <asp:Label ID="status" runat="server" Font-Bold="true" Text='<%#Eval("Status") %>'
                                                    ForeColor='<%# ColorStatusLabel(Eval("DisplayColor").ToString())%>' CssClass="tooltip"
                                                    ToolTip='<%#Eval("Status") + " <div style=\"text-align:center; font-weight:bold;\">" + getCheckStatus(Eval("BackOfficeOpinion").ToString(), "AlternateText") + "</div>" %>'></asp:Label>
                                                <asp:Image ID="ErrorMessageImg" runat="server" ImageUrl="~/Styles/Img/help.png" ToolTip=""
                                                    Width="15px" Visible="false" />
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                                        <asp:TemplateField HeaderText="Date insertion" InsertVisible="False" SortExpression="InsertedDate"
                                            ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Label ID="InsertedDate" runat="server" Text='<%# Eval("InsertionDate") %>'></asp:Label>
                                                <%--<asp:Label ID="Label1" runat="server" Text='<%# ConvertToLocalDateTime(Eval("InsertionDate").ToString())%>'></asp:Label>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--<asp:TemplateField HeaderText="Check" InsertVisible="False" ControlStyle-CssClass="trimmer"
                                            ControlStyle-Width="50px" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Image ID="ImgCheckStatus" runat="server" CssClass="tooltip" Style="width: 20px;"
                                                    ToolTip='<%# getCheckStatus(Eval("BackOfficeOpinion").ToString() +";"+ Eval("BackOfficeOpinionDate").ToString()+";"+Eval("BackOfficeUser").ToString(), "Tooltip") %>'
                                                    AlternateText='<%# getCheckStatus(Eval("BackOfficeOpinion").ToString(), "AlternateText")%>'
                                                    ImageUrl='<%# getCheckStatus(Eval("BackOfficeOpinion").ToString(), "ImageUrl")%>' />
                                                <--<asp:Label ID="lblCheckStatus" runat="server" Text='<%# getCheckStatus(Eval("BackOfficeOpinion").ToString(), "AlternateText")%>' CssClass="tooltip"
                                        ToolTip='<%# getCheckStatus(Eval("BackOfficeOpinion").ToString() +";"+ Eval("BackOfficeOpinionDate").ToString()+";"+Eval("BackOfficeUser").ToString(), "Tooltip") %>' ></asp:Label>-->
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                                        <asp:TemplateField HeaderText="Statut" InsertVisible="False" ControlStyle-Width="200px" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <%# registration.GetLabelFromGeneralStatusValue(Eval("sCheckGeneralStatus").ToString()) %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Réservé par" InsertVisible="False" ControlStyle-CssClass="trimmer"
                                            ControlStyle-Width="200px" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCheckUser" runat="server" CssClass="tooltip" ToolTip='<%# GetCheckUser(Eval("iCheckUser").ToString(), Eval("sCheckUserFirstName").ToString(),Eval("sCheckUserLastName").ToString()) %>'><%# GetCheckUser(Eval("iCheckUser").ToString(), Eval("sCheckUserFirstName").ToString(),Eval("sCheckUserLastName").ToString()) %></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--<asp:BoundField DataField="InsertedDate"HeaderText="InsertedDate" SortExpression="InsertedDate" ItemStyle-HorizontalAlign="Center"/>--%>
                                        <asp:TemplateField HeaderText="RefTransaction" InsertVisible="False" SortExpression="RefTransaction"
                                            Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="RefTransaction" runat="server" Text='<%#Eval("RefTransaction") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="bDelete" InsertVisible="False" SortExpression="bDelete"
                                            Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="bDelete" runat="server" Text=''></asp:Label><%--<%#Eval("bDelete") %>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle BackColor="#344b56" Font-Bold="True" ForeColor="White" />
                                    <HeaderStyle BackColor="#344b56" Font-Bold="True" ForeColor="White" Height="25px" />
                                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                    <RowStyle CssClass="GridViewRowStyle" />
                                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                    <%--<PagerStyle CssClass="GridViewPager" />--%>
                                </asp:GridView>
                            </div>
                            <div style="width: 100%; background-color: #344b56; color: White; border: 3px solid #344b56; border-bottom-left-radius: 8px; border-bottom-right-radius: 8px; height: 25px">
                                <asp:Panel ID="divPagination" runat="server" Visible="false" Style="width: 100%; display: table; margin: auto">
                                    <div style="display: table-row;">
                                        <div style="display: table-cell; height: 20px; vertical-align: middle; width: 57%; text-align: right">
                                            <asp:Button ID="btnPreviousPage" runat="server" OnClientClick="previousPage(); return false;" Text="<"
                                                Font-Bold="true" Width="30px" />
                                            <%--<input id="btnPreviousPage" runat="server"type="button" class="button" value="<" style="width:30px" onclick="previousPage();"/>--%>
                                            <%--OnSelectedIndexChanged="onChangePage"--%>
                                            <asp:DropDownList ID="ddlPage" runat="server" onchange="onChangePageNumber();" DataTextField="L"
                                                DataValueField="V" AutoPostBack="false">
                                            </asp:DropDownList>
                                            <asp:Label ID="lblTotalPage" runat="server" Style="font-weight: bold"></asp:Label>
                                            <%--<input id="btnNextPage" runat="server"type="button" class="button" value=">" style="width:30px" onclick="nextPage();"/>--%>
                                            <asp:Button ID="btnNextPage" runat="server" OnClientClick="nextPage(); return false;" Text=">"
                                                Font-Bold="true" Width="30px" />
                                            <%--<div style="padding-left: 10px"> <img src="Styles/Img/loading.gif"alt="loading" width="25px" height="25px" /> </div>--%>
                                        </div>
                                        <div style="display: table-cell; padding-left: 5px">
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>
                        </asp:Panel>
                        <asp:HiddenField ID="hfPageSelected" runat="server" Value="" />
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnRegistrationSearch" EventName="click" />
                        <asp:AsyncPostBackTrigger ControlID="btnDeleteFilter" EventName="click" />
                    </Triggers>
                </asp:UpdatePanel>
            </asp:Panel>
            <div id="#EndofGridView">
            </div>
        </div>

        <asp:Panel ID="panelReporting" runat="server" Visible="true">
            <div style="">
                <asp:UpdatePanel ID="upReportingPeriod" runat="server">
                    <ContentTemplate>
                        <asp:Period ID="ReportingPeriod" runat="server" isPeriodBold="true" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div style="clear:both;"></div>
            <div style="width:100%;text-align:center">
                <asp:Button ID="btnExportExcel" runat="server" OnClick="btnExportExcel_Click" CssClass="buttonHigher" Text="Exporter Rapport" />
            </div>
            <asp:HiddenField ID="hfReportingBeginDate" runat="server" />
            <asp:HiddenField ID="hfReportingEndDate" runat="server" />
            <asp:HiddenField ID="hfReportingPeriod" runat="server" />
        </asp:Panel>
    </div>

    <div style="width: 100%; display: table">
        <div style="display: table-row">
            <div style="display: table-cell; text-align: left">
                <asp:Button ID="btnPrevious" runat="server" Text="Précédent" OnClick="onClickPreviousButton" CssClass="button" />
            </div>
            <div style="display: table-cell; text-align: right">
                <asp:Button ID="btnValidate" runat="server" Text="Valider" OnClick="onClickValidate" CssClass="button" Visible="false" Style="margin: 0;" />
            </div>
        </div>
    </div>

    <asp:HiddenField ID="hdnActiveTab" runat="server" Value="" />

    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        var sRequestControlID = "";
        function BeginRequestHandler(sender, args) {
            pbControl = args.get_postBackElement();
            var panel = '';
            try {
                switch (pbControl.id) {
                    case "=btnSaveChanges.ClientID%>":
                        panel = '#=panelServiceLock.ClientID %>';
                        break;
                }
            } catch (ex) { console.log(ex.message) }

            if (panel.length > 0) {
                var width = $(panel).outerWidth();
                var height = $(panel).outerHeight();
                $('#ar-loading').css('width', width);
                $('#ar-loading').css('height', height);
                $('#ar-loading').show();
                $('#ar-loading').position({ my: "left top", at: "left top", of: panel });
            }

            sRequestControlID = pbControl.id;
        }
        function EndRequestHandler(sender, args) {
            $('#ar-loading').hide();
            hideLoading();
            $('.RadioButtonList').buttonset();
        }
    </script>

</asp:Content>

