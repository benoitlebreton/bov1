﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PersonalizedCardOrder.ascx.cs" Inherits="API_PersonalizedCardOrder" %>
<asp:Panel ID="panelPersonalizedCardOrder" runat="server">
    <asp:Panel ID="panelPersonalizedCardOrderList" runat="server">
        <script type="text/javascript">
            function ConfirmCancelPersonnalizedOrder(refOrder) {
                $('#<%=hfRefOrderToCancel.ClientID%>').val(refOrder);

                $('#dialog-confirmCancel').dialog({
                    resizable: false,
                    draggable: false,
                    modal: true,
                    minWidth: 500,
                    title: "Annulation commande carte n&deg; " + refOrder,
                    buttons: [{ text: 'OUI', click: function () { showLoading(); $('#<%=btnCancelConfirm.ClientID%>').click(); $(this).dialog('close'); $(this).dialog('destroy'); } },
                            { text: "NON", click: function () { $(this).dialog('close'); $(this).dialog('destroy'); } }]
                });
                $('#dialog-confirmCancel').parent().appendTo(jQuery("form:first"));

                return false;
            }

        </script>
        <asp:HiddenField ID="hfRefOrderToCancel" runat="server" />
        <div id="dialog-confirmCancel" style="display:none">
            <asp:UpdatePanel ID="upCancelPersonnalizedCardOrder" runat="server">
                <ContentTemplate>
                    Confirmez-vous l'annulation de la commande de carte personalisée ?
                    <asp:Button ID="btnCancelConfirm" runat="server" OnClick="btnCancelConfirm_Click" style="display:none" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <asp:UpdatePanel ID="upPersonnalizedCardOrder" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Repeater ID="rptPersonalizedCardOrderList" runat="server">
                    <HeaderTemplate>
                        <table id="tPersonalizedCardOrderList" cellpadding="0" cellspacing="0" style="width: 100%" class="defaultTable2">
                            <thead>
                                <tr>
                                    <th>Date demande</th>
                                    <th>Modèle</th>
                                    <th>Statut</th>
                                    <th>Adresse expédition</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr class='<%# Container.ItemIndex % 2 == 0 ? "beneficiary-table-row" : "beneficiary-table-alternate-row" %>'>
                            <td style="text-align:center;vertical-align:middle">
                                <%# tools.getFormattedDate(Eval("AskDate").ToString()) %>
                            </td>
                            <td style="text-align:center;vertical-align:middle">
                                <img src='./Styles/Img/CustomCards/<%# Eval("Link") %>' alt='<%# Eval("DescriptionCard") %>' height="50" tooltip='<%# Eval("DescriptionCard") %>' />
                            </td>
                            <td style="text-align:center; vertical-align:middle">
                                <%# GetPersonalizeCardOrderStatus(Eval("Paid").ToString(),tools.getFormattedDate(Eval("SendDate").ToString()),Eval("Actived").ToString(),tools.getFormattedDate(Eval("ActivationDate").ToString()), Eval("IsCancelled").ToString()) %>
                            </td>
                            <td style="vertical-align:middle">
                                <%# GetSendAddress(Eval("Address1").ToString(),Eval("Address2").ToString(),Eval("Address3").ToString(),Eval("Address4").ToString(),Eval("Zipcode").ToString(),Eval("City").ToString()) %>
                            </td>
                            <td style="vertical-align:middle">
                                <asp:Button ID="btnCancel" runat="server" CssClass="button" OnClientClick='<%# GetJsCancel(Eval("Reference").ToString()) %>' Text="Annuler" Visible='<%#isCancelButtonVisible(Eval("Paid").ToString(),Eval("Actived").ToString(),Eval("IsCancelled").ToString()) %>' />
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </tbody>
                    </table>
                    </FooterTemplate>
                </asp:Repeater>
            </ContentTemplate>
            <Triggers>

            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Panel>