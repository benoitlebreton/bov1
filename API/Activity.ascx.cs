﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using XMLMethodLibrary;

public partial class API_Activity : System.Web.UI.UserControl
{
    public string LastActivityDate { get; set; }
    public string LastActivityCheck { get; set; }
    public string CDCTransferDate { get; set; }
    public string XmlCustomerInfos
    {
        get { return (Session["XmlCustomerInfos"] != null) ? Session["XmlCustomerInfos"].ToString() : ""; }
        set
        {
            Session["XmlCustomerInfos"] = value;
            BindVariables();
        }
    }

    private void BindVariables()
    {
        string sLastActivityDate, sLastActivityCheck, sCDCTransferDate;
        ParseXmlCustomerInfos(XmlCustomerInfos, out sLastActivityDate, out sLastActivityCheck, out sCDCTransferDate);
        LastActivityCheck = sLastActivityCheck;
        LastActivityDate = sLastActivityDate;
        CDCTransferDate = sCDCTransferDate;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            BindActivity();
        }
    }

    protected void BindActivity()
    {
        if (String.IsNullOrWhiteSpace(LastActivityDate))
            BindVariables();

        if (!String.IsNullOrWhiteSpace(LastActivityDate))
        {
            DateTime date;
            if(DateTime.TryParseExact(LastActivityDate, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out date)
                && DateTime.Compare(date, DateTime.Now.AddYears(-1)) < 0)
            {
                litLastActivityDate.Text = LastActivityDate;
            }
            else panelActivity.Visible = false;
        }
        else panelActivity.Visible = false;
        if (!String.IsNullOrWhiteSpace(LastActivityCheck))
            litLastActivityCheck.Text = LastActivityCheck;
        else panelLastActCheck.Visible = false;
        if (!String.IsNullOrWhiteSpace(CDCTransferDate))
            litCDCTransferDate.Text = CDCTransferDate;
        else panelCDC.Visible = false;
    }

    protected void ParseXmlCustomerInfos(string sXmlCustomerInfos, out string sLastActivityDate, out string sLastActivityCheck, out string sCDCTransferDate)
    {
        List<string> listLastActivityDate = CommonMethod.GetAttributeValues(sXmlCustomerInfos, "ALL_XML_OUT/Customer/ECKERT/INFO", "LastActivityDate");
        sLastActivityDate = (listLastActivityDate.Count > 0) ? listLastActivityDate[0] : "";
        List<string> listLastActivityCheck = CommonMethod.GetAttributeValues(sXmlCustomerInfos, "ALL_XML_OUT/Customer/ECKERT/INFO", "LastActivityCheck");
        sLastActivityCheck = (listLastActivityCheck.Count > 0) ? listLastActivityCheck[0] : "";
        List<string> listCDCTransferDate = CommonMethod.GetAttributeValues(sXmlCustomerInfos, "ALL_XML_OUT/Customer/ECKERT/INFO", "CDCTransferDate");
        sCDCTransferDate = (listCDCTransferDate.Count > 0) ? listCDCTransferDate[0] : "";
    }
}