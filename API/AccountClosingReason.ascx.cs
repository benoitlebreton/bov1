﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using XMLMethodLibrary;

public partial class API_AccountClosingReason : System.Web.UI.UserControl
{
    protected DataTable _dtClosingReasons { get { return (ViewState["dtClosingReasons"] != null) ? (DataTable)ViewState["dtClosingReasons"] : null; } set { ViewState["dtClosingReasons"] = value; } }
    protected DataTable _dtClosingReasonsChoice { get { return (Session["dtClosingReasonsChoice"] != null) ? (DataTable)Session["dtClosingReasonsChoice"] : null; } set { Session["dtClosingReasonsChoice"] = value; } }

    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            BindAccountClosingReasonsChoice();

            Page.Header.Controls.Add(new System.Web.UI.LiteralControl("<link rel=\"stylesheet\" type=\"text/css\" href=\"" + ResolveUrl("~/Styles/AccountClosingControls.css") + "\" />"));
        }
    }

    public void SetAccountClosingReasons(DataTable dt)
    {
        _dtClosingReasons = dt;
        BindAccountClosingReasons();
    }

    protected void BindAccountClosingReasons()
    {
        rptReasons.DataSource = _dtClosingReasons;
        rptReasons.DataBind();
        upReasons.Update();
    }

    protected void BindAccountClosingReasonsChoice()
    {
        if(_dtClosingReasonsChoice == null)
            _dtClosingReasonsChoice = GetAccountClosingReasons();

        DataTable dtTmp = _dtClosingReasonsChoice.Clone();
        dtTmp.Rows.Add(new object[] { "", "" });

        // REMOVE DUPLICATES
        
        for (int i = 0; i < _dtClosingReasonsChoice.Rows.Count; i++)
        {
            bool bExists = false;
            if (_dtClosingReasons != null)
                for (int j = 0; j < _dtClosingReasons.Rows.Count; j++)
                {
                    if (_dtClosingReasons.Rows[j]["Tag"].ToString() == _dtClosingReasonsChoice.Rows[i]["Tag"].ToString())
                    {
                        bExists = true;
                        break;
                    }
                }

            if (!bExists)
                dtTmp.Rows.Add(_dtClosingReasonsChoice.Rows[i].ItemArray);
        }

        ddlReason.Items.Clear();
        ddlReason.DataSource = dtTmp;
        ddlReason.DataBind();

        ReorderAlphabetized(ddlReason);
    }
    protected DataTable GetAccountClosingReasons()
    {
        DataTable dt = new DataTable();
        dt.TableName = "dtAccountClosingReasons";
        dt.Columns.Add("Tag");
        dt.Columns.Add("Label");

        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_GetCloseReasonList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@ReturnSelect", SqlDbType.Bit);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                            new XElement("CloseReason", new XAttribute("RefCustomer", ""))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
            cmd.Parameters["@ReturnSelect"].Value = 0;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if (sXmlOut.Length > 0)
            {
                List<string> listReasons = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/CloseReason");

                for (int i = 0; i < listReasons.Count; i++)
                {
                    List<string> listTag = CommonMethod.GetAttributeValues(listReasons[i], "CloseReason", "TAGReason");
                    List<string> listDesc = CommonMethod.GetAttributeValues(listReasons[i], "CloseReason", "ReasonDescription");

                    if (listTag.Count > 0 && listTag[0].Length > 0 && listDesc.Count > 0 && listDesc[0].Length > 0)
                        dt.Rows.Add(new object[] { listTag[0].ToString(), listDesc[0].ToString() });
                }
            }
        }
        catch (Exception e)
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }

    protected void btnAdd_Click(object sender, ImageClickEventArgs e)
    {
        if (!String.IsNullOrWhiteSpace(ddlReason.SelectedValue))
        {
            DataTable dt = _dtClosingReasons;

            if (dt == null)
            {
                dt = new DataTable();
                dt.Columns.Add("Tag");
                dt.Columns.Add("Label");
            }

            dt.Rows.Add(new object[] { ddlReason.SelectedValue, ddlReason.SelectedItem.Text });
            _dtClosingReasons = dt;
            BindAccountClosingReasons();

            BindAccountClosingReasonsChoice();
        }
    }

    protected void btnRemove_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton btn = (ImageButton)sender;

        HiddenField hdn = (HiddenField)btn.Parent.FindControl("hdnTag");
        Literal lit = (Literal)btn.Parent.FindControl("litLabel");

        DataTable dt = _dtClosingReasons;

        bool bExist = false;
        int iRow = 0;
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["Tag"].ToString() == hdn.Value)
            {
                bExist = true;
                iRow = i;
                break;
            }
        }

        if (bExist)
        {
            dt.Rows.Remove(dt.Rows[iRow]);

            _dtClosingReasons = dt;
            BindAccountClosingReasons();

            BindAccountClosingReasonsChoice();
        }
    }

    protected void ReorderAlphabetized(DropDownList ddl)
    {
        List<ListItem> listCopy = new List<ListItem>();
        foreach (ListItem item in ddl.Items)
            listCopy.Add(item);
        ddl.Items.Clear();
        foreach (ListItem item in listCopy.OrderBy(item => item.Text))
            ddl.Items.Add(item);
    }

    public XElement GetDataToXML()
    {
        XElement xml = null;

        if((_dtClosingReasons != null && _dtClosingReasons.Rows.Count > 0) || !String.IsNullOrWhiteSpace(ddlReason.SelectedValue))
        {
            xml = new XElement("REASONS");

            if (_dtClosingReasons != null && _dtClosingReasons.Rows.Count > 0)
            {
                for (int i = 0; i < _dtClosingReasons.Rows.Count; i++)
                {
                    xml.Add(new XElement("REASON",
                            new XAttribute("TAGReason", _dtClosingReasons.Rows[i]["Tag"].ToString())));
                }
            }

            if(!String.IsNullOrWhiteSpace(ddlReason.SelectedValue))
                xml.Add(new XElement("REASON",
                                new XAttribute("TAGReason", ddlReason.SelectedValue)));
        }

        return xml;
    }

    public bool CheckForm()
    {
        return !((_dtClosingReasons == null || _dtClosingReasons.Rows.Count == 0) && String.IsNullOrWhiteSpace(ddlReason.SelectedValue));
    }

    public string GetTagDescription(string sTag)
    {
        if (_dtClosingReasonsChoice == null)
            _dtClosingReasonsChoice = GetAccountClosingReasons();

        for (int i = 0; i < _dtClosingReasonsChoice.Rows.Count; i++)
        {
            if (_dtClosingReasonsChoice.Rows[i]["Tag"].ToString() == sTag)
                return _dtClosingReasonsChoice.Rows[i]["Label"].ToString();
        }

        return null;
    }
}