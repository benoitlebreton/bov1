﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

public partial class API_OnfidoResult : System.Web.UI.UserControl
{
    public int RefTransaction { get; set; }
    public string RegistrationCode { get; set; }

    protected class OnfidoReport
    {
        public string Result { get; set; }
        public string ElapsedTime { get; set; }
        public OnfidoReport(string sResult, string sElapsedTime)
        {
            this.Result = sResult;
            this.ElapsedTime = sElapsedTime;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            #region // TEST
            /*reports = new List<string>() { "{\"created_at\": \"2018-07-02T09:13:57Z\",\"href\": \"/v2/checks/3bdb9c3e-ed34-4973-b127-ee17b52c76e7/reports/1d3037f1-b559-45b9-8f8d-89eadd18b867\",\"id\": \"1d3037f1-b559-45b9-8f8d-89eadd18b867\",\"name\": \"document\",\"properties\": {\"document_type\": \"national_identity_card\",\"issuing_country\": \"FRA\",\"mrz_line1\": \"IDFRASAVOYE<<<<<<<<<<<<<<<<<<<012018\",\"mrz_line2\": \"1506012004889HAROLD<<<<<<<<8903196M0\",\"date_of_expiry\": \"2030-06-04\",\"first_name\": \"HAROLD\",\"last_name\": \"SAVOYE\",\"date_of_birth\": \"1989-03-19\",\"issuing_date\": \"2015-06\",\"gender\": \"Male\",\"document_numbers\": [{\"type\": \"document_number\",\"value\": \"150601200488\"}]},\"result\": \"consider\",\"status\": \"complete\",\"sub_result\": \"suspected\",\"variant\": \"standard\",\"breakdown\": {\"police_record\": {\"result\": \"clear\"},\"compromised_document\": {\"result\": \"clear\"},\"data_consistency\": {\"result\": \"clear\",\"breakdown\": {\"document_type\": {\"result\": \"clear\",\"properties\": {}},\"issuing_country\": {\"result\": \"clear\",\"properties\": {}},\"document_numbers\": {\"result\": \"clear\",\"properties\": {}},\"gender\": {\"result\": \"clear\",\"properties\": {}},\"date_of_birth\": {\"result\": \"clear\",\"properties\": {}},\"last_name\": {\"result\": \"clear\",\"properties\": {}},\"first_name\": {\"result\": \"clear\",\"properties\": {}},\"date_of_expiry\": {\"result\": null,\"properties\": {}},\"nationality\": {\"result\": null,\"properties\": {}}}},\"data_validation\": {\"result\": \"clear\",\"breakdown\": {\"document_expiration\": {\"result\": \"clear\",\"properties\": {}},\"gender\": {\"result\": \"clear\",\"properties\": {}},\"mrz\": {\"result\": \"clear\",\"properties\": {}},\"date_of_birth\": {\"result\": \"clear\",\"properties\": {}},\"document_numbers\": {\"result\": \"clear\",\"properties\": {}},\"expiry_date\": {\"result\": null,\"properties\": {}}}},\"visual_authenticity\": {\"result\": \"consider\",\"breakdown\": {\"other\": {\"result\": \"consider\",\"properties\": {}},\"original_document_present\": {\"result\": \"consider\",\"properties\": {}},\"face_detection\": {\"result\": \"clear\",\"properties\": {}},\"picture_face_integrity\": {\"result\": \"clear\",\"properties\": {}},\"security_features\": {\"result\": \"clear\",\"properties\": {}},\"template\": {\"result\": \"clear\",\"properties\": {}},\"fonts\": {\"result\": \"clear\",\"properties\": {}},\"digital_tampering\": {\"result\": \"clear\",\"properties\": {}}}},\"image_integrity\": {\"result\": \"clear\",\"breakdown\": {\"colour_picture\": {\"result\": \"clear\",\"properties\": {}},\"conclusive_document_quality\": {\"result\": \"clear\",\"properties\": {}},\"supported_document\": {\"result\": \"clear\",\"properties\": {}},\"image_quality\": {\"result\": \"clear\",\"properties\": {}}}},\"data_comparison\": {\"result\": null,\"breakdown\": {\"date_of_expiry\": {\"result\": null,\"properties\": {}},\"issuing_country\": {\"result\": null,\"properties\": {}},\"document_type\": {\"result\": null,\"properties\": {}},\"document_numbers\": {\"result\": null,\"properties\": {}},\"gender\": {\"result\": null,\"properties\": {}},\"date_of_birth\": {\"result\": null,\"properties\": {}},\"last_name\": {\"result\": null,\"properties\": {}},\"first_name\": {\"result\": null,\"properties\": {}}}}}}",
            "{\"created_at\": \"2018-07-02T09:13:57Z\",\"href\": \"/v2/checks/3bdb9c3e-ed34-4973-b127-ee17b52c76e7/reports/31b42665-7ef1-42da-8084-6a09d76fb2af\",\"id\": \"31b42665-7ef1-42da-8084-6a09d76fb2af\",\"name\": \"facial_similarity\",\"properties\": {},\"result\": \"consider\",\"status\": \"complete\",\"sub_result\": null,\"variant\": \"standard\",\"breakdown\": {\"face_comparison\": {\"result\": null},\"image_integrity\": {\"result\": \"consider\"},\"visual_authenticity\": {\"result\": null}}}" };
            */
            #endregion

            List<OnfidoReport> reports = new List<OnfidoReport>();
            DataTable dtCheck = GetAutoCheckDetails(authentication.GetCurrent().sToken, RefTransaction);

            for (int i = 0; i < dtCheck.Rows.Count; i++)
            {
                //switch (dtCheck.Rows[i]["sReportName"].ToString())
                //{
                //    case "document":
                //        break;
                //    case "facial_similarity":
                //        break;
                //}

                reports.Add(new OnfidoReport(dtCheck.Rows[i]["jCheckResult"].ToString(), dtCheck.Rows[i]["ElapsedTime"].ToString()));
            }

            if (reports != null)
            {
                DataTable dt = ParseReports(reports);

                if (dt.Rows.Count > 0)
                {
                    double dTotalReportSecondes = 0;
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (dt.Rows[i]["ElapsedTime"].ToString().Length == 8)
                        {
                            TimeSpan time = TimeSpan.ParseExact(dt.Rows[i]["ElapsedTime"].ToString(), "g", new CultureInfo("fr-FR"));
                            dTotalReportSecondes += time.TotalSeconds;
                        }
                    }
                    if (dTotalReportSecondes > 300)
                        lblTotalReportElapsedTime.ForeColor = System.Drawing.Color.Red;
                    lblTotalReportElapsedTime.Text = TimeSpan.FromSeconds(dTotalReportSecondes).ToString("hh\\:mm\\:ss");

                    rptReport.DataSource = dt;
                    rptReport.DataBind();
                }
                else
                {
                    panelResult.Visible = false;
                    panelNoResult.Visible = true;
                }

                dt = GetFormattedOCRElapsedTime(GetOCRElapsedTime(RegistrationCode));
                if (dt.Rows.Count > 0)
                {
                    decimal dTotalOCRTime =0;
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        decimal d = decimal.Parse(dt.Rows[i]["ElapsedTime"].ToString(), NumberStyles.Float, CultureInfo.GetCultureInfo("en-GB"));
                        dTotalOCRTime += d;
                    }
                    if (dTotalOCRTime > 30)
                        lblTotalOCRTime.ForeColor = System.Drawing.Color.Red;
                    lblTotalOCRTime.Text = dTotalOCRTime.ToString().Replace(',','.') + "s";

                    rptOCRElapsedTime.DataSource = dt;
                    rptOCRElapsedTime.DataBind();
                }
            }
            else
            {
                panelResult.Visible = false;
                panelNoResult.Visible = true;
            }
        }
    }

    public static Dictionary<string, string> dicDocType = new Dictionary<string, string>()
    {
        { "passport", "passeport" },
        { "national_identity_card", "carte d'identité" },
        { "residence_permit", "titre de séjour" },
        { "driving_licence", "permis de conduire" },
        { "unknown", "N.C." }
    };
    public static Dictionary<string, string> dicDocSide = new Dictionary<string, string>()
    {
        { "front", "recto" },
        { "back", "verso" }
    };
    private DataTable GetFormattedOCRElapsedTime(DataTable dataTable)
    {
        DataTable dt = dataTable.Copy();

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            // correspondance label
            string sTrad;
            if (dicDocType.TryGetValue(dt.Rows[i]["DocType"].ToString(), out sTrad))
                dt.Rows[i]["DocType"] = sTrad;
            if (dicDocSide.TryGetValue(dt.Rows[i]["DocSide"].ToString(), out sTrad))
                dt.Rows[i]["DocSide"] = sTrad;

            // arrondi temps de traitement
            decimal d = decimal.Parse(dt.Rows[i]["ElapsedTime"].ToString(), NumberStyles.Float, CultureInfo.GetCultureInfo("en-GB"));
            dt.Rows[i]["ElapsedTime"] = Math.Round(d, 2, MidpointRounding.AwayFromZero).ToString().Replace(',','.');
        }

        return dt;
    }

    private DataTable ParseReports(List<OnfidoReport> reports)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("Name");
        dt.Columns.Add("Result");
        dt.Columns.Add("Subresult");
        dt.Columns.Add("Breakdown", typeof(DataTable));
        dt.Columns.Add("ElapsedTime");

        List<string> listBreakdownToBeClear = new List<string>() { "colour_picture", "digital_tampering", "picture_face_integrity", "mrz", "document_numbers" };

        foreach (OnfidoReport report in reports)
        {
            try
            {
                var o = JObject.Parse(report.Result);
                bool bReportClear = o["result"].ToString() == "clear";
                bool bReportBreakdownClear = true;

                DataRow rowMain = dt.NewRow();

                rowMain["Name"] = CleanBreakdownName(o["name"].ToString());
                rowMain["Result"] = o["result"].ToString();
                rowMain["Subresult"] = o["sub_result"].ToString();
                rowMain["ElapsedTime"] = report.ElapsedTime;

                if (!String.IsNullOrWhiteSpace(o["sub_result"].ToString()))
                    rowMain["Result"] = o["sub_result"].ToString();

                DataTable dt2 = new DataTable();
                dt2.Columns.Add("Name");
                dt2.Columns.Add("Result");
                dt2.Columns.Add("Breakdown", typeof(DataTable));

                foreach (JProperty breakdown in o["breakdown"])
                {
                    if (breakdown.Name != "data_comparison"
                        //|| !String.IsNullOrWhiteSpace(breakdown.Value["result"].ToString())
                        )
                    {
                        DataRow row = dt2.NewRow();
                        row["Name"] = CleanBreakdownName(breakdown.Name);
                        row["Result"] = breakdown.Value["result"].ToString();

                        DataTable dtBreak = new DataTable();
                        if (breakdown.Value["breakdown"] != null)
                        {
                            dtBreak.Columns.Add("Name");
                            dtBreak.Columns.Add("Result");
                            dtBreak.Columns.Add("Important", typeof(bool));

                            foreach (JProperty innerBreakdown in breakdown.Value["breakdown"])
                            {
                                bool bImportant = false;

                                // Le résultat global d'une vérification ne peut pas être "OK" si un ou plusieus contrôles critiques n'ont pas pu être réalisés
                                if (bReportClear
                                    && listBreakdownToBeClear.Contains(innerBreakdown.Name)
                                    && innerBreakdown.Value["result"].ToString() != "clear")
                                {
                                    bReportBreakdownClear = false;
                                    bImportant = true;
                                }

                                dtBreak.Rows.Add(new object[] { CleanBreakdownName(innerBreakdown.Name), innerBreakdown.Value["result"], bImportant });
                            }
                        }
                        row["Breakdown"] = dtBreak;
                        dt2.Rows.Add(row);
                    }
                }
                rowMain["Breakdown"] = dt2;

                if (!bReportBreakdownClear)
                    rowMain["Result"] = "caution";

                dt.Rows.Add(rowMain);
            }
            catch(Exception e)
            {

            }
        }

        return dt;
    }

    protected string GetImageUrl(string status)
    {
        switch(status)
        {
            case "clear":
                return "~/Styles/Img/onfido/clear.png";
            case "consider":
                return "~/Styles/Img/onfido/warning.png";
            case "error":
            case "unidentified":
                return "~/Styles/Img/onfido/error.png";
            case "":
            default:
                return "~/Styles/Img/onfido/none.png";
        }
    }

    protected string GetFriendlyLabel(string status, bool subStatus)
    {
        string htmlElement = "";
        string htmlClass = "";

        if (subStatus)
            htmlElement = "span";
        else
            htmlElement = "div";

        switch (status)
        {
            case "clear":
                htmlClass = "result-pass";
                break;
            case "consider":
            case "caution":
                htmlClass = "result-warning";
                break;
            case "rejected":
                htmlClass = "result-reject";
                break;
            case "suspected":
            case "error":
            case "unidentified":
                htmlClass = "result-fail";
                break;
            case "":
            default:
                htmlClass = "result-not-found";
                break;
        }

        return string.Format("<{0} class=\"{1}\">{2}</{3}>", htmlElement, htmlClass, TranslateOnfidoLabel(status), htmlElement);
    }

    protected string CleanBreakdownName(string name)
    {
        return FirstCharToUpper(TranslateOnfidoLabel(name.Replace('_', ' ')));
    }
    protected static string FirstCharToUpper(string input)
    {
        if (String.IsNullOrEmpty(input))
            throw new ArgumentException("ARGH!");
        return input.First().ToString().ToUpper() + input.Substring(1);
    }
    public static Dictionary<string, string> dicOnfidoTranslation = new Dictionary<string, string>()
    {
        { "document", "document" },
        { "facial similarity", "similarité faciale" },
        { "face comparison", "comparaison faciale" },
        { "image integrity", "intégrité de l'image" },
        { "visual authenticity", "authenticité visuelle" },
        { "police record", "casier judiciaire" },
        { "compromised document", "document compromis" },
        { "data consistency", "cohérence des données" },
        { "date of expiry", "date d'expiration" },
        { "document type", "type de document" },
        { "nationality", "nationalité" },
        { "gender", "sexe" },
        { "date of birth", "date de naissance" },
        { "issuing country", "pays d'émission" },
        { "document numbers", "numéros de document" },
        { "last name", "nom" },
        { "first name", "prénom" },
        { "data validation", "validation des données" },
        { "document expiration", "péremption du document" },
        { "expiry date", "date d'expiration" },
        { "mrz", "MRZ" },
        { "other", "autre" },
        { "original document present", "document original présent" },
        { "picture face integrity", "intégrité de l'image faciale" },
        { "security features", "fonctions de sécurité" },
        { "template", "modèle" },
        { "fonts", "polices" },
        { "digital tampering", "altération numérique" },
        { "face detection", "détection faciale" },
        { "colour picture", "image couleur" },
        { "conclusive document quality", "qualité d'image concluante" },
        { "supported document", "document supporté" },
        { "image quality", "qualité d'image" },
        { "not found", "non trouvé" },
        { "", "non trouvé" },
        { "clear", "validé" },
        { "passed", "validé" },
        { "warning", "attention" },
        { "failed", "échec" },
        { "consider", "à examiner" },
        { "caution", "à examiner" },
        { "rejected", "rejeté" },
        { "suspected", "suspect" },
        { "unidentified", "non identifié" }
    };
    protected string TranslateOnfidoLabel(string name)
    {
        string sTranslated = "";
        if (dicOnfidoTranslation.TryGetValue(name.ToLower(), out sTranslated))
            return sTranslated;
        else
            return name;
    }

    protected DataTable GetAutoCheckDetails(string sToken, int iRefTransaction)
    {
        DataTable dt = null;
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Partner"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("[Registration].[P_GetRegistrationAutoReportsDetails]", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@sXmlIn", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@sXmlOut", SqlDbType.VarChar, -1);

            cmd.Parameters["@sXmlIn"].Value = new XElement("ALL_XML_IN",
                                                new XElement("RegistrationAutoCheckInfos",
                                                    new XAttribute("TOKEN", sToken),
                                                    new XAttribute("iRefTransaction", iRefTransaction.ToString()))).ToString();
            cmd.Parameters["@sXmlOut"].Direction = ParameterDirection.Output;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            dt = new DataTable();
            da.Fill(dt);
        }
        catch (Exception e)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }

    protected DataTable GetOCRElapsedTime(string sRegistrationCode)
    {
        DataTable dt = new DataTable();
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["UniversalID"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("Doc.P_GetOnfidoTreatmentTime", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@RegistrationCode", SqlDbType.VarChar, 20);
            cmd.Parameters["@RegistrationCode"].Value = sRegistrationCode;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
        }
        catch(Exception e)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }
}