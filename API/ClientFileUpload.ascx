﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ClientFileUpload.ascx.cs" Inherits="API_ClientFileUpload" %>

<div class="clientfileupload-panel-loading panel-loading"></div>

<div class="file-upload-form">

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="hdnRefCustomer" runat="server" />
            <asp:Button ID="btnFileUploaded" runat="server" OnClick="btnFileUploaded_Click" Style="display: none" />
            <asp:Button ID="btnFileUploadError" runat="server" OnClick="btnFileUploadError_Click" Style="display: none" />
            <asp:HiddenField ID="hdnFileName" runat="server" />
            <asp:HiddenField ID="hdnClearForm" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdatePanel ID="upOtherInputs" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div>
                <span class="label">Type</span>
                <div>
                    <asp:UpdatePanel ID="upDocumentType" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="ddlDocumentType" runat="server" CssClass="" DataTextField="Label" DataValueField="TAG"
                                AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlDocumentType_SelectedIndexChanged">
                                <asp:ListItem Text="" Value=""></asp:ListItem>
                            </asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>

            <asp:Panel ID="panelDesc" runat="server" Style="margin-top: 10px">
                <span class="label">Description</span>
                <div>
                    <asp:TextBox ID="txtDesc" runat="server" placeholder="" MaxLength="200" autocomplete="off" Style="width: 100%; box-sizing: border-box; height: 26px; font-size: 1em;"></asp:TextBox>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div style="margin-top: 10px">
        <div class="label">Fichier</div>
        <div class="file-tabs">
            <asp:Repeater ID="rptFileTabs" runat="server">
                <HeaderTemplate>
                    <ul>
                </HeaderTemplate>
                <ItemTemplate>
                    <li><a href="<%# Eval("href") %>"><%# Eval("label") %></a></li>
                </ItemTemplate>
                <FooterTemplate>
                    </ul>
                </FooterTemplate>
            </asp:Repeater>

            <div id="tab-desktop-file">
                <asp:UpdatePanel ID="upDeskFileUp" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <input id="fileUpload" type="file" accept='<%=AllowedDocType %>' class="inputfile" />
                        <label for="fileUpload">
                            <span></span>
                            <strong>Parcourir...</strong>
                        </label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div id="tab-zendesk-file">
                <asp:UpdatePanel ID="upZendeskFile" runat="server">
                    <ContentTemplate>
                        <div>
                            <asp:TextBox ID="txtTicketID" runat="server" placeholder="n° de ticket" MaxLength="10" autocomplete="off" oninput="ZendeskFileSearch();" Style="width: 100%; box-sizing: border-box; height: 26px; font-size: 1em;"></asp:TextBox>
                        </div>
                        <div id="ticketID-file" style="min-height: 50px">
                            <asp:Repeater ID="rptZendeskFile" runat="server">
                                <HeaderTemplate>
                                    <table class="defaultTable2">
                                        <tr>
                                            <th style="width: 20px; text-align: center">
                                                <input type="checkbox" onclick="SelectAllZendeskFile($(this));" /></th>
                                            <th>Pièces jointes</th>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td style="text-align: center">
                                            <asp:CheckBox ID="ckbSelect" runat="server" />
                                            <asp:HiddenField ID="hdnURL" runat="server" Value='<%#Eval("URL") %>' />
                                        </td>
                                        <td style="text-transform: none">
                                            <a href='<%#Eval("URL") %>' target="_blank">
                                                <asp:Label ID="lblFilename" runat="server" Text='<%#Eval("Filename") %>'></asp:Label>
                                            </a>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                            <asp:HiddenField ID="hdnOverwrite" runat="server" />
                            <asp:Panel ID="panelNoZendeskFile" runat="server" Visible="false" HorizontalAlign="Center" Style="padding-top: 14px">
                                Aucune pièce jointe
                            </asp:Panel>
                            <asp:Button ID="btnSearchTicketID" runat="server" OnClick="btnSearchTicketID_Click" Style="display: none" />
                            <asp:Button ID="btnUploadZendeskFile" runat="server" OnClick="btnUploadZendeskFile_Click" Style="display: none" />
                            <asp:Button ID="btnUploadServerFile" runat="server" OnClick="btnUploadServerFile_Click" style="display:none" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div id="tab-server-file">
                <asp:Panel ID="panelServerFile" runat="server" Visible="false">
                    <asp:DropDownList ID="ddlServerFile" runat="server" AppendDataBoundItems="true" DataValueField="url" DataTextField="filename" style="width:calc(100% - 105px)"
                        OnSelectedIndexChanged="ddlServerFile_SelectedIndexChanged" AutoPostBack="true">
                        <asp:ListItem Text="" Value=""></asp:ListItem>
                    </asp:DropDownList>
                    <input type="button" value="Ouvrir" class="file-open-button" onclick="OpenServerFile();" style="width:100px;height:26px;position:relative;top:1px;" />
                </asp:Panel>
            </div>

        </div>
    </div>
    <asp:Panel ID="panelSaveFile" runat="server" HorizontalAlign="Center" Style="margin-top: 20px">
        <input type="button" onclick="UploadFile()" value="Ajouter" class="button" />
    </asp:Panel>
</div>

<div id="dialog-overwritefile" style="display: none">
    Un fichier portant le même nom existe déjà.
    <br />
    Voulez-vous l'écraser ?
    <br />
    <br />
    <b>Attention :</b> cette opération est irréversible.
</div>

<div id="dialog-serverfile" style="display:none;">
    <iframe id="iframeServerFile" style="border:0;width:100%;margin:auto;height:calc(100% - 40px);"></iframe>
    <div style="text-align:center;font-style:italic;font-size:0.8em">Ce popup peut être redimensionné (en bas à droite) et déplacé (sur le bandeau en haut) si besoin</div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        InitInputFiles();
    });

    function InitInputFiles() {
        var inputs = document.querySelectorAll('.inputfile');
        Array.prototype.forEach.call(inputs, function (input) {
            var label = input.nextElementSibling,
                labelVal = label.innerHTML;

            input.addEventListener('change', function (e) {
                var fileName = '';
                if (this.files && this.files.length > 1)
                    fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);
                else
                    fileName = e.target.value.split('\\').pop();

                if (fileName)
                    label.querySelector('span').innerHTML = fileName;
                else
                    label.innerHTML = labelVal;
            });
        });
    }

    var timeout;
    function ZendeskFileSearch() {
        clearTimeout(timeout);
        if ($('#<%=txtTicketID.ClientID%>').val().length > 0) {
            ShowZendeskTicketSearch('#ticketID-file');
            timeout = setTimeout(function () {
                HideZendeskTicketSearch();

                var patt = /^\d+$/;
                if (patt.test($('#<%=txtTicketID.ClientID%>').val())) {
                    $('#<%=btnSearchTicketID.ClientID%>').click();
                }
                else {
                    AlertMessage('Erreur', 'N° ticket : format invalide');
                }
            }, 2000);

        }
    }
    function SelectAllZendeskFile(ckb) {
        $('#ticketID-file input[type=checkbox]').prop('checked', $(ckb).prop('checked'));
    }

    function UploadFile(overwrite) {
        var data = new FormData();

        var files = $("#fileUpload").get(0).files;
        var ref = $('#<%=hdnRefCustomer.ClientID%>').val();
        var doccattag = $('#<%=ddlDocumentType.ClientID%>').val();
        var desc = $('#<%=txtDesc.ClientID%>').val();
        if (overwrite == null)
            overwrite = false;
        $('#<%=hdnOverwrite.ClientID%>').val(overwrite);

        var msg = '';

        if (doccattag.length > 0 && ref.length > 0) {

            var activeTab = $(".file-tabs").tabs("option", "active");

            switch ($('.file-tabs ul li:eq(' + activeTab + ') a').attr('href')) {
                case "#tab-desktop-file": // file upload
                    if (files.length > 0) {
                        data.append("RefCustomer", ref);
                        data.append("DocCategoryTAG", doccattag);
                        data.append("Files", files[0]);
                        data.append("Description", desc);
                        data.append("Overwrite", overwrite);

                        ShowFileUploadLoading();

                        $.ajax({
                            type: "POST",
                            url: "Tools/SaveClientFile.aspx",
                            contentType: false,
                            processData: false,
                            data: data,
                            dataType: 'json',
                            success: function (data, textStatus, jqXHR) {
                                if (data.rc == 0) {
                                    $('#<%=hdnFileName.ClientID%>').val(data.originalfilename);
                                    if ($('#<%=hdnClearForm.ClientID%>').val() == '1') {
                                        $('label[for=fileUpload] span').empty();
                                        $('#<%=ddlDocumentType.ClientID%>').val('');
                                        $('#<%=txtDesc.ClientID%>').val('');
                                    }
                                    $('#<%=btnFileUploaded.ClientID%>').click();
                                }
                                else if (data.rc == 9995) {
                                    ShowOverwriteDialog();
                                }
                                else {
                                    console.log(data.rc);
                                    $('#<%=btnFileUploadError.ClientID%>').click();
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                // Handle errors here
                                console.log(textStatus);
                            },
                            complete: function () {
                                HideFileUploadLoading();
                            }
                        });
                    }
                    else {
                        msg += "Fichier : champ requis";

                        AlertMessage("Erreur", msg);
                    }
                    break;
                case "#tab-zendesk-file": // Zendesk file
                    if ($('#ticketID-file table td input[type=checkbox]:checked').length > 0) {
                        $('#<%=btnUploadZendeskFile.ClientID%>').click();
                    }
                    else {
                        msg += "Fichier : champ requis";

                        $('#<%=btnFileUploadError.ClientID%>').click();
                        AlertMessage("Erreur", msg);
                    }
                    break;
                case "#tab-server-file": // Server file
                    if ($('#<%=ddlServerFile.ClientID%>').val().length > 0) {
                        $('#<%=btnUploadServerFile.ClientID%>').click();
                    }
                    else {
                        msg += "Fichier : champ requis";

                        $('#<%=btnFileUploadError.ClientID%>').click();
                        AlertMessage("Erreur", msg);
                    }
                    break;
            }
        }
        else {
            if (ref.length == 0)
                msg += "Client : champ requis<br/>";
            if (doccattag.length == 0)
                msg += "Type : champ requis<br/>";

            $('#<%=btnFileUploadError.ClientID%>').click();
            AlertMessage("Erreur", msg);
        }
    }

    function OpenServerFile()
    {
        if ($('#<%=ddlServerFile.ClientID%>').val().length > 0) {
            var title = $('#<%=ddlServerFile.ClientID%> option:selected').text();
            var url = $('#<%=ddlServerFile.ClientID%>').val().split(';')[0];
            $('#iframeServerFile').attr('src', url);
            $('#dialog-serverfile').dialog({
                draggable: true,
                resizable: true,
                width: 500,
                modal: false,
                title: title
            });

            $('#dialog-serverfile').css('height', 'calc(100% - 56px)');
        }
    }

    function ShowOverwriteDialog() {
        $('#dialog-overwritefile').dialog({
            draggable: false,
            resizable: false,
            width: 400,
            dialogClass: "no-close",
            modal: true,
            title: 'Ecraser le fichier',
            buttons:
                {
                    Ecraser: function () { $(this).dialog('destroy'); UploadFile(true); },
                    Annuler: function () { $(this).dialog('destroy'); $('#<%=btnFileUploadError.ClientID%>').click(); }
                }
        });
        $('#dialog-overwritefile').parent().appendTo(jQuery("form:first"));
    }

    Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(ClientFileUploadBeginRequestHandler);
    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(ClientFileUploadEndRequestHandler);
    function ClientFileUploadBeginRequestHandler(sender, args) {
        try {
            pbControl = args.get_postBackElement();

            if (pbControl != null) {
                //console.log(pbControl.id);
                if (pbControl.id.indexOf('btnFileUploaded') != -1
                    || pbControl.id.indexOf('btnUploadZendeskFile') != -1)
                    ShowFileUploadLoading();
                else if (pbControl.id.indexOf('btnSearchTicketID') != -1)
                    ShowZendeskTicketSearch('#ticketID-file');
            }
        }
        catch (e) {

        }
    }
    function ClientFileUploadEndRequestHandler(sender, args) {
        HideFileUploadLoading();
    }
    function ShowFileUploadLoading(controlID) {
        if (controlID == null)
            controlID = '.file-upload-form';
        var panel = $(controlID);

        $('.clientfileupload-panel-loading').show();
        $('.clientfileupload-panel-loading').width(panel.outerWidth());
        $('.clientfileupload-panel-loading').height(panel.outerHeight());

        $('.clientfileupload-panel-loading').position({
            my: 'center',
            at: 'center',
            of: controlID
        });
    }
    function HideFileUploadLoading() {
        $('.clientfileupload-panel-loading').hide();
    }

    function ShowZendeskTicketSearch(controlID) {
        if (controlID == null)
            controlID = '.file-upload-form';
        var panel = $(controlID);

        $('.clientfileupload-panel-loading').show();
        $('.clientfileupload-panel-loading').width(panel.outerWidth());
        $('.clientfileupload-panel-loading').height(panel.outerHeight());

        $('.clientfileupload-panel-loading').position({
            my: 'center',
            at: 'center',
            of: controlID
        });
    }
    function HideZendeskTicketSearch() {
        $('.clientfileupload-panel-loading').hide();
    }
</script>
