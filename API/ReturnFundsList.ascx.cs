﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using XMLMethodLibrary;

public partial class API_ReturnFundsList : System.Web.UI.UserControl
{
    public int RefCustomer { get { return (ViewState["RefCustomer"] != null) ? int.Parse(ViewState["RefCustomer"].ToString()) : 0; } set { ViewState["RefCustomer"] = value; } }
    public bool ShowLabel { get; set; }
    protected bool _bActionRights { get { return (Session["ReturnFundsActionRight"] != null && Session["ReturnFundsActionRight"].ToString() == "1") ? true : false; } set { Session["ReturnFundsActionRight"] = (value) ? "1" : "0"; } }
    private DataTable _dtReturnFunds { get { return (Session["dtAccountClose"] != null) ? (DataTable)Session["dtAccountClose"] : null; } set { Session["dtAccountClose"] = value; } }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            CheckRights();

            if (ShowLabel)
                panelLabel.Visible = true;
            else panelLabel.Visible = false;

            BindReturnFunds(true);
        }
    }

    protected void CheckRights()
    {
        string sXmlOut = tools.GetRights(authentication.GetCurrent().sToken, "SC_SearchReturnFunds");

        List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Page", "RC");

        if (listRC.Count > 0 && listRC[0] == "0")
        {
            List<string> listAction = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/Page/Action");

            for (int i = 0; i < listAction.Count; i++)
            {
                List<string> listTag = CommonMethod.GetAttributeValues(listAction[i], "Action", "TagAction");
                List<string> listActionAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ActionAllowed");
                List<string> listViewAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ViewAllowed");

                if (listTag.Count > 0)
                {
                    switch (listTag[0])
                    {
                        case "ReturnFunds":
                            if (listViewAllowed.Count > 0 && listViewAllowed[0] == "1")
                                panelList.Visible = true;
                            else panelList.Visible = false;
                            if (listActionAllowed.Count > 0 && listActionAllowed[0] == "1")
                                _bActionRights = true;
                            else _bActionRights = false;

                            //panelSurveillance.Visible = false; // TEST
                            break;
                    }
                }
            }
        }
    }

    protected void BindReturnFunds(bool bForceRefresh)
    {
        if (_dtReturnFunds == null || bForceRefresh)
        {
            _dtReturnFunds = (RefCustomer != 0) ? GetReturnFundsInformation(authentication.GetCurrent().sToken, RefCustomer.ToString()) : GetReturnFundsInformation(authentication.GetCurrent().sToken, "");

            if (RefCustomer != 0)
            {
                listHeader.Attributes["class"] = "rf-sum-short";
                headAccountNumber.Attributes["style"] = "display:none";
            }
        }

        if (_dtReturnFunds.Rows.Count > 0)
        {
            rptRF.DataSource = _dtReturnFunds;
            rptRF.DataBind();

            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "InitDetailsSlider();InitTooltip();", true);
        }
        else
        {
            panelNoResult.Visible = true;
            panelList.Visible = false;
        }

        upRF.Update();
    }

    protected DataTable GetReturnFundsInformation(string sToken, string sRefCustomer)
    {
        DataTable dtReturnFunds = new DataTable();
        dtReturnFunds.Columns.Add("RefReturnFunds");
        dtReturnFunds.Columns.Add("AccountNumber");
        dtReturnFunds.Columns.Add("ShowAccountNumber");
        dtReturnFunds.Columns.Add("Amount");
        dtReturnFunds.Columns.Add("BeneficiaryName");
        dtReturnFunds.Columns.Add("IBAN");
        dtReturnFunds.Columns.Add("BIC");
        dtReturnFunds.Columns.Add("Label");
        dtReturnFunds.Columns.Add("Status");
        dtReturnFunds.Columns.Add("StatusLabel");
        dtReturnFunds.Columns.Add("BOUser");

        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("BankSI.P_GetCustomerReturnFundsInformation", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);

            XElement xml = new XElement("ReturnFunds",
                                            new XAttribute("CashierToken", sToken));
            if (!String.IsNullOrWhiteSpace(sRefCustomer))
                xml.Add(new XAttribute("RefCustomer", sRefCustomer));

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                            xml).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if (sXmlOut.Length > 0)
            {
                List<string> listReturnFunds = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/ReturnFunds");

                for (int j = 0; j < listReturnFunds.Count; j++)
                {
                    List<string> listRef = CommonMethod.GetAttributeValues(listReturnFunds[j], "ReturnFunds", "RefReturnFunds");
                    List<string> listAccountNumber = CommonMethod.GetAttributeValues(listReturnFunds[j], "ReturnFunds", "BankAccountNumber");
                    List<string> listAmount = CommonMethod.GetAttributeValues(listReturnFunds[j], "ReturnFunds", "TransferAmount");
                    List<string> listBenef = CommonMethod.GetAttributeValues(listReturnFunds[j], "ReturnFunds", "BeneficiaryName");
                    List<string> listIBAN = CommonMethod.GetAttributeValues(listReturnFunds[j], "ReturnFunds", "IBAN");
                    List<string> listBIC = CommonMethod.GetAttributeValues(listReturnFunds[j], "ReturnFunds", "BIC");
                    List<string> listLabel = CommonMethod.GetAttributeValues(listReturnFunds[j], "ReturnFunds", "Libelle");
                    List<string> listStatus = CommonMethod.GetAttributeValues(listReturnFunds[j], "ReturnFunds", "DoneStatus");
                    List<string> listEtat = CommonMethod.GetAttributeValues(listReturnFunds[j], "ReturnFunds", "Etat");
                    List<string> listExecutionDate = CommonMethod.GetAttributeValues(listReturnFunds[j], "ReturnFunds", "ExecutionDate");
                    List<string> listBOUser = CommonMethod.GetAttributeValues(listReturnFunds[j], "ReturnFunds", "BOUser");

                    if (listRef.Count > 0 && listRef[0].Length > 0)
                        dtReturnFunds.Rows.Add(new object[] { listRef[0], listAccountNumber[0], (sRefCustomer.Length > 0) ? "0" : "1", listAmount[0].Replace('.', ',') + " €", listBenef[0], listIBAN[0], listBIC[0], listLabel[0], listStatus[0], listEtat[0] + ((listStatus[0] == "1") ? " le " + listExecutionDate[0] : ""), listBOUser[0] });
                }
            }
        }
        catch (Exception e)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dtReturnFunds;
    }

    protected bool ApproveReturnFunds(string sRefReturnFunds, string sToken, out string sErrorMessage)
    {
        sErrorMessage = "";
        bool bApproved = false;
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("Web.P_ApproveReturnFunds", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                                new XElement("ReturnFunds",
                                                    new XAttribute("CashierToken", sToken),
                                                    new XAttribute("RefReturnFunds", sRefReturnFunds))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if (sXmlOut.Length > 0)
            {
                List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/ReturnFunds", "RC");

                if (listRC.Count > 0)
                {
                    switch (listRC[0])
                    {
                        case "0":
                            bApproved = true;
                            break;
                        case "72170":
                            sErrorMessage = "Opération refusée.<br/>Veuillez demander à un autre manager de valider ce retour de fonds.";
                            break;
                    }
                }
            }
        }
        catch (Exception e)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return bApproved;
    }

    protected void btnApprove_Click(object sender, EventArgs e)
    {
        string sRefClose = hdnRefClose.Value;
        string sErrorMessage = "";

        if (ApproveReturnFunds(sRefClose, authentication.GetCurrent().sToken, out sErrorMessage))
            BindReturnFunds(true);
        else ScriptManager.RegisterStartupScript(this.Page, this.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Erreur', \"" + sErrorMessage + "\");", true);
    }

    protected void ddlNotifyBefore_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindReturnFunds(false);
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        string sRefClose = hdnRefClose.Value;
        string sErrorMessage = "";

        if (CancelReturnFunds(sRefClose, authentication.GetCurrent().sToken, out sErrorMessage))
            BindReturnFunds(true);
        else ScriptManager.RegisterStartupScript(this.Page, this.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Erreur', \"" + sErrorMessage + "\");", true);
    }

    protected bool CancelReturnFunds(string sRefReturnFunds, string sToken, out string sErrorMessage)
    {
        sErrorMessage = "";
        bool bCanceled = false;
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("Web.P_CancelReturnFunds", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                                new XElement("ReturnFunds",
                                                    new XAttribute("CashierToken", sToken),
                                                    new XAttribute("RefReturnFunds", sRefReturnFunds))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if (sXmlOut.Length > 0)
            {
                List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/ReturnFunds", "RC");

                if (listRC.Count > 0)
                {
                    switch(listRC[0])
                    {
                        case "0":
                            bCanceled = true;
                            break;
                        case "72170":
                            sErrorMessage = "Opération refusée.<br/>Veuillez demander à un autre manager d'annuler ce retour de fonds.";
                            break;
                    }
                }
            }
        }
        catch (Exception e)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return bCanceled;
    }

    public string GetStatusImageURL(string sStatus)
    {
        string sImageURL = "";

        switch(sStatus)
        {
            case "0":
                sImageURL = "gray_status.png";
                break;
            case "1":
                sImageURL = "green_status.png";
                break;
            case "2":
                sImageURL = "red_status.png";
                break;
        }

        return String.Format("~/Styles/Img/{0}", sImageURL);
    }
}