﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GarnishmentList.ascx.cs" Inherits="API_GarnishmentList" %>

<script type="text/javascript">

    $(document).ready(function () {
        $('.radioButtonList').buttonset();
        $('#<%=txtAccountNumber.ClientID%>').bind("input", SearchByAccountNumber);
    });

    function SearchByAccountNumber() {
        setTimeout(function () {
            //console.log($.trim($('#<%=txtAccountNumber.ClientID%>').val()).length);
            var len = $.trim($('#<%=txtAccountNumber.ClientID%>').val()).length;
            if (len == 11 || len == 0)
                $('#<%=btnSearchByAccountNumber.ClientID%>').click();
        }, 100);
    }

    function CheckGarnishmentAvailability(refGarnishment, isCourrierCS) {
        $('#<%=hdnRefGarnishmentToCheck.ClientID%>').val(refGarnishment);
        if (isCourrierCS)
            $('#<%=btnCheckCourrierCSAvailability.ClientID%>').click();
        else
            $('#<%=btnCheckGarnishmentAvailability.ClientID%>').click();
    }

    function Garnishment_RequestAccess(refGarnishment, boUser, prevFC, isCourrierCS) {
        if (boUser.length > 0) {
            $('#lblReservedBy').empty().append(boUser);
            $('#panelReservedBy').show();
        }
        else $('#panelReservedBy').hide();

        var url = "GarnishmentDetails.aspx?ref=" + refGarnishment.toString();
        if (isCourrierCS)
            url = "AvisCSDetails.aspx?ref=" + refGarnishment.toString();
        if(prevFC)
            url += "&prev=fc";
        
        $("#dialog-garnishment-reservation").dialog({
        autoOpen: true,
            resizable: false,
            draggable: false,
            modal: true,
            zIndex: 10000,
            minWidth: 380,
            title: "Attribution",
            buttons: [{
            text: 'Voir', click: function () {
                    showLoading();
                    $(this).dialog('close');
                    $(this).dialog('destroy');
                    document.location = url;
                }
            },
                {
                    text: "Je m'en occupe", click: function () {
                        $('#<%=hdnSelectedGarnishment.ClientID%>').val(refGarnishment);
                        if (isCourrierCS)
                            $('#<%=btnCourrierCSReservation.ClientID%>').click();
                        else
                            $('#<%=btnGarnishmentReservation.ClientID%>').click();
                        $(this).dialog('close');
                        $(this).dialog('destroy');
                    }
                }]
        });
        $("#dialog-garnishment-reservation").parent().appendTo(jQuery("form:first"));
    }

</script>

<div id="ar-loading" style="display:none;position:absolute;background: url('Styles/img/loading.gif') no-repeat center;background-color:rgba(255, 255, 255, 0.5);z-index:100"></div>

<asp:Panel ID="panelGridFilters" runat="server" Visible="true" DefaultButton="btnSearchByAccountNumber">
    <style>
        div.cell.valign-middle {
            vertical-align:middle;
        }
    </style>
    <div class="table">
        <div class="row">
            <div class="cell valign-middle" style="padding-right: 5px; font-weight: bold">
                Statut
            </div>
            <div class="cell valign-middle">
                <asp:DropDownList ID="ddlGarnishmentStatus" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlGarnishment_SelectedIndexChanged">
                    <asp:ListItem Text="Non traité" Value="Untreated" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Certifié" Value="Certified"></asp:ListItem>
                    <asp:ListItem Text="Terminé" Value="Treated"></asp:ListItem>
                    <asp:ListItem Text="Tous" Value=""></asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="cell valign-middle" style="padding:0 5px 0 20px; font-weight: bold">
                Type
            </div>
            <div class="cell valign-middle">
                <asp:UpdatePanel ID="upDocumentType" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:CheckBoxList ID="cblDocumentType" runat="server" onchange="return checkDocumentType();" AutoPostBack="false"  RepeatDirection="Horizontal">
                            <%--<asp:ListItem Value="Revival">Relance</asp:ListItem>--%>
                            <asp:ListItem Value="Indebtedness">Surendettement</asp:ListItem>
                            <asp:ListItem Value="Others">ATD/OA</asp:ListItem>
                        </asp:CheckBoxList>
                        <asp:Button style="display:none" ID="btnDocumentType" runat="server" OnClick="cblDocumentType_SelectedIndexChanged" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div class="cell valign-middle" style="padding:0 5px 0 20px; font-weight: bold">
                Réservé
            </div>
            <div class="cell valign-middle">
                <asp:DropDownList ID="ddlGarnishmentReserved" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlGarnishment_SelectedIndexChanged">
                    <asp:ListItem Text="Non" Value="0"></asp:ListItem>
                    <asp:ListItem Text="Oui" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Tous" Value="" Selected="True"></asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="cell valign-middle" style="padding:0 5px 0 20px;font-weight:bold">
                N° Compte
            </div>
            <div class="cell valign-middle">
                <asp:TextBox ID="txtAccountNumber" runat="server" MaxLength="11"></asp:TextBox>
                <asp:Button ID="btnSearchByAccountNumber" runat="server" OnClick="btnSearchByAccountNumber_Click" style="display:none" />
            </div>
        </div>
    </div>
</asp:Panel>

<asp:UpdatePanel ID="upSearch" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="btnGarnishmentReservation" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID="btnCourrierCSReservation" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID="btnPreviousPage" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID="btnNextPage" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID="btnSearchByAccountNumber" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID="ddlPage" EventName="SelectedIndexChanged" />
        <asp:AsyncPostBackTrigger ControlID="ddlNbResult" EventName="SelectedIndexChanged" />
        <asp:AsyncPostBackTrigger ControlID="ddlGarnishmentStatus" EventName="SelectedIndexChanged" />
        <asp:AsyncPostBackTrigger ControlID="ddlGarnishmentReserved" EventName="SelectedIndexChanged" />
        <asp:AsyncPostBackTrigger ControlID="btnDocumentType" EventName="Click" />
    </Triggers>
    <ContentTemplate>

        <asp:Panel ID="panelGrid" runat="server" style="margin-top:0">
            <div id="divGridSettings" runat="server" style="display: table; width: 100%; margin-bottom: 10px">
                <div style="display: table-row">
                    <div style="display: table-cell; width: 50%; padding-top: 20px">
                        <asp:Label ID="lblNbOfResults" runat="server">0</asp:Label>
                        résultat(s)
                    </div>
                    <div style="display: table-cell; width: 50%; text-align: right;">
                        Nb de résultats par page :
                        <asp:DropDownList ID="ddlNbResult" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlNbResult_SelectedIndexChanged">
                            <asp:ListItem Value="50" Selected="True">50</asp:ListItem>
                            <asp:ListItem Value="75">75</asp:ListItem>
                            <asp:ListItem Value="100">100</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div id="div-grid">
                <div style="border: 3px solid #344b56; border-top-left-radius: 8px; border-top-right-radius: 8px;margin-top:5px">
                    <asp:GridView ID="gvGarnishment" runat="server" AllowSorting="true"
                        AllowPaging="false" AutoGenerateColumns="False" GridLines="None" CellPadding="10"
                        DataKeyNames="Ref" ShowHeader="true" Width="100%" SortedDescendingCellStyle-Font-Overline="true"
                        ViewStateMode="Disabled" BorderWidth="0" BackColor="Transparent" OnRowDataBound="gvGarnishment_RowDataBound">
                        <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="#344b56" Font-Bold="true" />
                        <EmptyDataTemplate>
                            Aucun avis
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField Visible="false" InsertVisible="false">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdnRefGarnishment" runat="server" Value='<%# Eval("Ref")%>' />
                                    <asp:HiddenField ID="hdnTreated" runat="server" Value='<%# Eval("Treated")%>' />
                                    <asp:HiddenField ID="hdnFlagCourrierSurendettement" runat="server" Value='<%# Eval("isCourrierSurEndettement")%>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Date" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="1%">
                                <ItemTemplate>
                                    <asp:Label ID="lblDate" runat="server" Text='<%#Eval("Date")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Nom">
                                <ItemTemplate>
                                    <asp:Label ID="lblLastName" runat="server" Text='<%#Eval("AccountLastName")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Prénom">
                                <ItemTemplate>
                                    <asp:Label ID="lblFirstName" runat="server" Text='<%#Eval("AccountFirstName")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Montant" ItemStyle-HorizontalAlign="Right">
                                <ItemTemplate>
                                    <asp:Label ID="lblAmount" runat="server" Text='<%#String.Format("{0} {1}", Eval("Amount"), "€") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Réservé par">
                                <ItemTemplate>
                                    <asp:Label ID="lblBOAgent" runat="server" Text='<%#Eval("BOUser")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#344b56" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#344b56" Font-Bold="True" ForeColor="White" Height="25px" />
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                        <RowStyle CssClass="GridViewRowStyle" />
                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                        <%--<PagerStyle CssClass="GridViewPager" />--%>
                    </asp:GridView>
                </div>
                <div style="border: 3px solid #344b56; border-bottom-left-radius: 8px; border-bottom-right-radius: 8px; height: 25px; background-color: #344b56; color: White; ">
                    <asp:Panel ID="divPagination" runat="server" Visible="false" Style="width: 100%; display: table; margin: auto">
                        <div style="display: table-row;">
                            <div style="display: table-cell; height: 20px; vertical-align: middle; width: 57%; text-align: right">
                                <asp:Button ID="btnPreviousPage" runat="server" onclick="btnPreviousPage_Click" Text="<" Font-Bold="true" Width="30px" />
                                <asp:DropDownList ID="ddlPage" runat="server" OnSelectedIndexChanged="ddlPage_SelectedIndexChanged" AutoPostBack="true"
                                    DataTextField="L" DataValueField="V">
                                </asp:DropDownList>
                                <asp:Label ID="lblTotalPage" runat="server" Style="font-weight: bold"></asp:Label>
                                <asp:Button ID="btnNextPage" runat="server" OnClick="btnNextPage_Click" Text=">" Font-Bold="true" Width="30px" />
                            </div>
                            <div style="display: table-cell; padding-left: 5px">
                            </div>
                        </div>
                    </asp:Panel>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="panelError" runat="server" Visible="false" style="text-align:center;margin-top:20px">
            <asp:Label ID="lblError" runat="server"></asp:Label>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>

<asp:UpdatePanel ID="upCheckGarnishmentAvailability" runat="server">
    <ContentTemplate>
        <asp:HiddenField ID="hdnRefGarnishmentToCheck" runat="server" />
        <asp:Button ID="btnCheckGarnishmentAvailability" runat="server" OnClick="btnCheckGarnishmentAvailability_Click" style="display:none" />
        <asp:Button ID="btnCheckCourrierCSAvailability" runat="server" OnClick="btnCheckCourrierCSAvailability_Click" style="display:none" />
    </ContentTemplate>
</asp:UpdatePanel>

<div id="dialog-garnishment-reservation" style="display:none">
    <asp:UpdatePanel ID="upReservation" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            Vous allez accéder au détail de l'avis.
            <div id="panelReservedBy" style="font-weight:bold">
                <span style="color:red">ATTENTION :</span> Il est actuellement réservé par <span id="lblReservedBy"></span>.
            </div><br />
            En cliquant sur "Je m'en occupe", le traitement de ce virement vous sera attribué.

            <asp:HiddenField ID="hdnSelectedGarnishment" runat="server" />
            <asp:Button ID="btnGarnishmentReservation" runat="server" OnClick="btnGarnishmentReservation_Click" style="display:none" />
            <asp:Button ID="btnCourrierCSReservation" runat="server" OnClick="btnCourrierCSReservation_Click" style="display:none" />
        </ContentTemplate>
    </asp:UpdatePanel>
</div>

<script type="text/javascript">
    Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
    function BeginRequestHandler(sender, args) {
        pbControl = args.get_postBackElement();

        var containerID = 'div-grid';

        var width = $("#" + containerID).width();
        var height = $("#" + containerID).height();
        $('#ar-loading').css('width', width);
        $('#ar-loading').css('height', height);

        $('#ar-loading').show();

        $('#ar-loading').position({ my: "left top", at: "left top", of: "#"+containerID });
    }
    function EndRequestHandler(sender, args) {
        $('#ar-loading').hide();
        $('.radioButtonList').buttonset();
    }

    var _selectedValues;
    function initDocumentType() {
        $("[id*=cblDocumentType] input:checked").each(function () {           
            _selectedValues.push($(this).val());
        });
    }
    function checkDocumentType() {
        var selectedValues = [];
        $("[id*=cblDocumentType] input:checked").each(function () {           
            selectedValues.push($(this).val());
        });
        if (selectedValues.length > 0) {
            _selectedValues = selectedValues;
            $('#<%=btnDocumentType.ClientID%>').click();
        } else {
            //console.log($("[id*=cblDocumentType] input:checkbox").eq(_selectedValues));
            $("[id*=cblDocumentType] input[value=" + _selectedValues + "]").attr('checked', 'checked');
            AlertMessage('Erreur', "Veuillez cocher au moins un type de document");
            //console.log($("[id*=cblDocumentType] input:checked"));
        }

        return false;
    }
</script>