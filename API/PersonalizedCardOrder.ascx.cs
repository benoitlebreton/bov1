﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using XMLMethodLibrary;

public partial class API_PersonalizedCardOrder : System.Web.UI.UserControl
{
    public int iRefCustomer
    {
        get
        {
            int _iRefCustomer = 0;
            int.TryParse((ViewState["iRefCustomer"] != null) ? ViewState["iRefCustomer"].ToString() : "0", out _iRefCustomer);
            return _iRefCustomer;
        }
        set { ViewState["iRefCustomer"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            BindPersonnalizedCardOrderList();
        }
    }

    protected string GetSendAddress(string sAddress1, string sAddress2, string sAddress3, string sAddress4, string sZipcode, string sCity)
    {
        string sAddressComplete = "";

        sAddressComplete += sAddress1.Trim();

        if (sAddress2.Trim().Length > 0)
            sAddressComplete += " " + sAddress2.Trim();
        if (sAddress3.Trim().Length > 0)
            sAddressComplete += " " + sAddress3.Trim();
        if (sAddress4.Trim().Length > 0)
            sAddressComplete += " " + sAddress4.Trim();

        sAddressComplete += "<br/>" + sZipcode.Trim() + " " + sCity.Trim();

        return sAddressComplete;
    }

    public void BindPersonnalizedCardOrderList()
    {
        rptPersonalizedCardOrderList.DataSource = GetCustomerPersonalizedCardOrder(iRefCustomer);
        rptPersonalizedCardOrderList.DataBind();
    }

    protected DataTable GetCustomerPersonalizedCardOrder(int iRefCustomer)
    {
        DataTable dt = new DataTable();
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_GetPersonalizedCardInformation", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@RefCustomer", SqlDbType.Int);

            cmd.Parameters["@RefCustomer"].Value = iRefCustomer;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);

            cmd.ExecuteNonQuery();
        }
        catch (Exception e)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }

    protected string GetPersonalizeCardOrderStatus(string sPaid,string sSendDate, string sActivated,string sActivationDate, string sIsCancelled)
    {
        string sStatus = "";

        if(sActivated == "YES")
        {
            sStatus = "Activée";
            if (sActivationDate.Trim().Length > 0)
                sStatus += " le " + sActivationDate;
        }
        else
        {
            if(sIsCancelled.Trim().Length > 0 && sIsCancelled.Trim() == "1")
            {
                sStatus = "Annulée";
            }
            else if(sSendDate.Trim().Length > 0)
            {
                sStatus = "Envoyée le "+sSendDate;
            }
            else
            {
                if (sPaid == "YES")
                    sStatus = "Payée";
                else
                    sStatus = "Non payée";
            }
        }

        return sStatus;
    }

    protected bool isCancelButtonVisible(string sPaid, string sActivated, string sIsCancelled)
    {
        bool ShowCancelButton = false;
        if(checkCancelButtonRight() && sActivated != "YES" && sPaid == "YES" && sIsCancelled.Trim() != "1")
        {
            ShowCancelButton = true;
        }
        return ShowCancelButton;
    }

    protected bool checkCancelButtonRight()
    {
        bool isOK = false;
        authentication auth = authentication.GetCurrent();

        string sXmlOut = tools.GetRights(auth.sToken, "SC_Customer");
        bool bAction, bView;
        if (tools.GetTagActionRight(sXmlOut, "CancelPersonnalizedCardOrder", out bAction, out bView) && bAction && bView)
        {
            isOK = true;
        }

        return isOK;
    }

    protected string GetJsCancel(string sRefOrder)
    {
        return "return ConfirmCancelPersonnalizedOrder('" + sRefOrder + "');";
    }

    protected void btnCancelConfirm_Click(object sender, EventArgs e)
    {
        string sRefOrder = hfRefOrderToCancel.Value;
        string sTitle = "Annulation commande carte n&deg; " + sRefOrder;
        string sMessage = "";
        if (CancelPersonnalizedCardOrder(sRefOrder))
        {
            BindPersonnalizedCardOrderList();
            upPersonnalizedCardOrder.Update();
            sMessage = "<span style=\"color:green;\">Commande annulée.</span>";
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "CancelPersonnalizedCardOrder_KEY", "hideLoading();;AlertMessage('" + sTitle + "', '" + sMessage + "');", true);
        }
        else
        {
            sMessage = "Une erreur est survenue.<br/>La commande n&apos;a pas pu être annulée.";
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "CancelPersonnalizedCardOrder_KEY", "hideLoading();AlertMessage('" + sTitle + "', '" + sMessage + "');", true);
        }
    }

    protected bool CancelPersonnalizedCardOrder(string sRefOrder)
    {
        bool isOK = false;

        if (checkCancelButtonRight())
        {

            authentication auth = authentication.GetCurrent();
            SqlConnection conn = null;

            try
            {

                /*
                SET @IN = '<ALL_XML_IN>
                                < Order CashierToken = "DBTREATMENT" RefOrder = "" RefCustomer = "37235" BankAccountNumber = "" />
                                   </ ALL_XML_IN > '
               --EXEC @RC = [web].[P_CancelCustomerPersonalizedCardOrder] @IN,@OUT OUTPUT
                SELECT @RC, CAST(@OUT AS XML)
                */
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
                conn.Open();

                SqlCommand cmd = new SqlCommand("web.P_CancelCustomerPersonalizedCardOrder", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@IN", SqlDbType.VarChar, -1);
                cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);
                cmd.Parameters.Add("@RC", SqlDbType.Int);

                cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                                    new XElement("Order",
                                                        new XAttribute("CashierToken", auth.sToken),
                                                        new XAttribute("RefOrder", sRefOrder))).ToString();
                cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
                cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;
                cmd.ExecuteNonQuery();
                string sRC = cmd.Parameters["@RC"].Value.ToString();
                string sXmlOUT = cmd.Parameters["@OUT"].Value.ToString();

                //TEST
                //string sRC = "99";
                //string sXmlOUT = "<ALL_XML_OUT><Order RC=\"0\" ErrorLabel=\"\"/></ALL_XML_OUT>";

                List<string> lsRC = XMLMethodLibrary.CommonMethod.GetAttributeValues(sXmlOUT, "ALL_XML_OUT/Order", "RC");

                if (sRC.Trim() == "0" && lsRC.Count > 0 && lsRC[0] == "0")
                {
                    isOK = true;
                }
            }
            catch (Exception e)
            {
                isOK = false;
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }
        return isOK;
    }
}