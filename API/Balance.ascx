﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Balance.ascx.cs" Inherits="API_Balance" %>

<div class="balancerefresh-panel-loading panel-loading"></div>
<asp:UpdatePanel ID="upBalance" runat="server">
    <ContentTemplate>
        <div class="div-balance" style="display:inline-block">
            <div>
                <span class="label">Solde</span>
            </div>
            <div>
                <asp:Label ID="lblBalance" runat="server">- -</asp:Label>
                &nbsp;
                <asp:ImageButton ID="btnRefresh" runat="server" ImageUrl="~/Styles/Img/refresh.png" OnClick="btnRefresh_Click" style="width:15px;position:relative;top:1px" />
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>

<script type="text/javascript">
    Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BalanceRefreshBeginRequestHandler);
    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(BalanceRefreshEndRequestHandler);
    function BalanceRefreshBeginRequestHandler(sender, args) {
        try {
            pbControl = args.get_postBackElement();

            if (pbControl != null) {
                if (pbControl.id.indexOf('btnRefresh') != -1) {
                    var controlID = '.div-balance'
                    var panel = $(controlID);
                    $('.balancerefresh-panel-loading').show();
                    $('.balancerefresh-panel-loading').width(panel.outerWidth());
                    $('.balancerefresh-panel-loading').height(panel.outerHeight());

                    $('.balancerefresh-panel-loading').position({
                        my: 'center',
                        at: 'center',
                        of: controlID
                    });
                }
            }
        }
        catch (e) {

        }
    }
    function BalanceRefreshEndRequestHandler(sender, args) {
        $('.balancerefresh-panel-loading').hide();
    }
</script>