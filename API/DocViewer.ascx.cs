﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.IO;

public partial class DocViewer : System.Web.UI.UserControl
{
    private string _OCRCroppedPath = "";
    private string _OCROriginalPath = "";
    private string _OCRErrorPath = "";
    private bool _IsInError;
    private string _FileName;
    private int _NbPages;

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public string OCRCroppedPath
    {
        get { return _OCRCroppedPath; }
        set { _OCRCroppedPath = value; }
    }
    public string OCROriginalPath
    {
        get { return _OCROriginalPath; }
        set { _OCROriginalPath = value; }
    }
    public string OCRErrorPath
    {
        get { return _OCRErrorPath; }
        set { _OCRErrorPath = value; }
    }
    public bool IsInError
    {
        get { return _IsInError; }
        set { _IsInError = value; }
    }
    public string FileName
    {
        get { return _FileName; }
        set { _FileName = value; }
    }
    public int NbPages
    {
        get { return _NbPages; }
        set { _NbPages = value; }
    }

    public void InitDocViewer()
    {
        if (_FileName != null && _FileName.Length > 0)
        {
            if (_IsInError)
            {
                originalFilePath.Value = _OCRErrorPath + _FileName;
            }
            else
            {
                //string sMapPath = System.Web.HttpContext.Current.Server.MapPath("~" + _OCROriginalPath + _FileName);
                if(File.Exists(System.Web.HttpContext.Current.Server.MapPath("~" + _OCROriginalPath + _FileName)))
                    originalFilePath.Value = _OCROriginalPath + _FileName;
                else originalFilePath.Value = _OCRErrorPath + _FileName;
            }
        }
        else originalFilePath.Value = "";

        if (_NbPages > 0)
        {
            firstFilePath.Value = _OCRCroppedPath + "_1_" + _FileName.Substring(0, _FileName.LastIndexOf('.')) + ".jpg";

            if (_NbPages > 1)
                secondFilePath.Value = _OCRCroppedPath + "_2_" + _FileName.Substring(0, _FileName.LastIndexOf('.')) + ".jpg";
            else secondFilePath.Value = "";
        }
        else
        {
            firstFilePath.Value = "";
            secondFilePath.Value = "";
        }

    }
}