﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ChurnResult.ascx.cs" Inherits="API_ChurnResult" %>

<asp:Panel ID="panelChurn" runat="server">
    <div class="scoring-container churn-container">
        <asp:Panel ID="panelIcon" runat="server" CssClass="scoring-icon" onclick="ToggleScoringDetails($(this));">
            <div>
                <asp:Image ID="imgChurnIcon" runat="server" />
            </div>
            <div>
                <div>
                    Risque <span>Churn</span>
                </div>
                <div>
                    <asp:Label ID="lblScore" runat="server"></asp:Label>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="panelDetails" runat="server" CssClass="scoring-details">
            <div class="arrow-up"></div>
            <div class="arrow-bg"></div>
            <div class="scoring-details-loading"></div>
            <div class="scoring-details-header">
                <div>
                    Critère
                    <%--<asp:LinkButton ID="lnkCritera" runat="server" OnClick="lnkHeader_Click" OnClientClick="ShowLoadingScore()">
                        Critère
                    </asp:LinkButton>--%>
                </div>
                <div>
                    Valeur
                    <%--<asp:LinkButton ID="lnkScore" runat="server" OnClick="lnkHeader_Click" OnClientClick="ShowLoadingScore()">
                        Score
                    </asp:LinkButton>--%>
                </div>
            </div>
            <div class="scoring-details-inner">
                <asp:Repeater ID="rptDetails" runat="server">
                    <ItemTemplate>
                        <div class="scoring-details-item">
                            <div>
                                <%#Eval("Label") %>
                            </div>
                            <div>
                                <%#Eval("Value") %>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </asp:Panel>
    </div>
</asp:Panel>