﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class API_CAPEmail : System.Web.UI.UserControl
{
    public EventHandler EmailFinal;
    public string Email { get { return txtEmail.Text; } set { txtEmail.Text = value; } }
    public string QualityCode { get { return txtQualityCode.Text; } }
    public Dictionary<string, string> dicQualityCode = new Dictionary<string, string>
    {
        { "0", "Adresse correcte" },
        { "1", "Adresse corrigée" },
        { "2", "Propositions d'adresse" },
        { "3", "Adresse non joignable" },
        { "4", "Adresse rejetée" },
        { "5", "Adresse quasiment sûre" },
        { "6", "Serveur indisponible" },
        { "7", "Adresse corrigée syntaxiquement, mais injoignable" }
    };

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnValidateEmail_Click(object sender, EventArgs e)
    {
        if (EmailFinal != null)
            EmailFinal(sender, e);
    }
}