﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class API_IPConnectionList : System.Web.UI.UserControl
{
    public int iRefCustomer
    {
        get
        {
            int _iRefCustomer = 0;
            int.TryParse((ViewState["iRefCustomer"] != null) ? ViewState["iRefCustomer"].ToString() : "0", out _iRefCustomer);
            return _iRefCustomer;
        }
        set { ViewState["iRefCustomer"] = value; }
    }
    public string Type
    {
        get
        {
            return (ViewState["Type"] != null) ? ViewState["Type"].ToString() : "";
        }
        set { ViewState["Type"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindIP();

            ScriptManager.RegisterClientScriptInclude(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "Scripts/IPConnectionList.js?v=20181017");
        }
    }

    public void BindIPLocation()
    {
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "SearchIPAddressLocation();", true);
    }

    public void BindIP()
    {
        rIPList.Visible = true;
        rIPList2.Visible = false;
        DataTable dt = GetCustomerIPAddress(iRefCustomer, Type);
        if (Type == "SUMMARY")
        {
            rIPList.Visible = false;
            rIPList2.Visible = true;
            rIPList2.DataSource = dt;
            rIPList2.DataBind();
        }
        else
        {
            rIPList.DataSource = dt;
            rIPList.DataBind();
        }

        if (dt.Rows.Count > 0)
        {
            panelIPList.Visible = true;
            panelIPList_Empty.Visible = false;
        }
        else
        {
            panelIPList.Visible = false;
            panelIPList_Empty.Visible = true;
        }
    }

    protected DataTable GetCustomerIPAddress(int iRefCustomer, string sType)
    {
        DataTable dt = new DataTable();
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_GetIPAddressList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@RefCustomer", SqlDbType.Int);
            cmd.Parameters.Add("@Type", SqlDbType.VarChar, 100);

            cmd.Parameters["@RefCustomer"].Value = iRefCustomer;
            cmd.Parameters["@Type"].Value = sType;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);

            cmd.ExecuteNonQuery();
        }
        catch(Exception e)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }

    public void ExportExcel()
    {
        DumpExcel(dtTransactionExcelExport(GetCustomerIPAddress(iRefCustomer, Type)), "Liste_IP_"+Type+"_" + iRefCustomer.ToString() + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xls");
    }
    protected DataTable dtTransactionExcelExport(DataTable dt)
    {
        DataTable dtExcelExport = new DataTable();
        dtExcelExport.Columns.Add("Adresse IP");

        if (Type != "SUMMARY")
        {
            dtExcelExport.Columns.Add("Date connexion");
        }
        else
        {
            dtExcelExport.Columns.Add("Date min");
            dtExcelExport.Columns.Add("Date max");
            dtExcelExport.Columns.Add("Nb connexions", typeof(Int32));
        }

        string sIPAddress = "";
        string sConnectionDate = "";
        string sMinDate = "";
        string sMaxDate = "";
        int iNbConnections = 0;

        foreach (DataRow row in dt.Rows)
        {
            sIPAddress = "";
            sConnectionDate = "";
            sMinDate = "";
            sMaxDate = "";
            iNbConnections = 0;

            if (row.Field<string>("IPAddress") != null)
                sIPAddress = row.Field<string>("IPAddress").ToString();

            if (Type == "SUMMARY")
            {
                if (row.Field<string>("MinDate") != null)
                    sMinDate = row.Field<string>("MinDate").ToString();
                if (row.Field<string>("MaxDate") != null)
                    sMaxDate = row.Field<string>("MaxDate").ToString();

                //iNbConnections = row.Field<int>("NbConnections");
                int.TryParse(row["NbConnections"].ToString(), out iNbConnections);
            }
            else
            {
                if (row.Field<string>("ConnectionDate") != null)
                    sConnectionDate = row.Field<string>("ConnectionDate").ToString();
            }

            if (Type == "SUMMARY")
                dtExcelExport.Rows.Add(new object[] { sIPAddress, sMinDate, sMaxDate, iNbConnections });
            else
                dtExcelExport.Rows.Add(new object[] { sIPAddress, sConnectionDate });
        }

        return dtExcelExport;
    }

    private void DumpExcel(DataTable tbl, string filename)
    {
        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.ClearContent();
        HttpContext.Current.Response.ClearHeaders();
        HttpContext.Current.Response.Buffer = true;
        HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.UTF8;
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + filename + ".xlsx");

        using (ExcelPackage pack = new ExcelPackage())
        {
            ExcelWorksheet ws = pack.Workbook.Worksheets.Add(filename);
            ws.Cells["A1"].LoadFromDataTable(tbl, true);
            //Set columns width
            string sRange = "A1:B1";
            ws.Column(1).Width = 20;
            ws.Column(2).Width = 20;
            if(tbl.Columns.Count == 4)
            {
                ws.Column(3).Width = 20;
                ws.Column(4).Width = 15;
                sRange = "A1:D1";

                //Apply date format
                using (ExcelRange rng = ws.Cells[2, 3, tbl.Rows.Count + 2, 3])
                {
                    rng.Style.Numberformat.Format = "dd/MM/yyyy HH:mm:ss";
                }
                //Apply int format
                using (ExcelRange rng = ws.Cells[2, 4, tbl.Rows.Count + 2, 4])
                {
                    rng.Style.Numberformat.Format = "0";
                }
            }

            //Set header style
            using (ExcelRange rng = ws.Cells[sRange])
            {
                rng.Style.Font.Bold = true;
                rng.Style.Fill.PatternType = ExcelFillStyle.Solid;
                rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(245, 117, 39));
                rng.Style.Font.Color.SetColor(Color.White);
            }
            //Apply date format
            using (ExcelRange rng = ws.Cells[2, 2, tbl.Rows.Count + 2, 2])
            {
                rng.Style.Numberformat.Format = "dd/MM/yyyy HH:mm:ss";
            }

            var ms = new System.IO.MemoryStream();
            pack.SaveAs(ms);
            ms.WriteTo(HttpContext.Current.Response.OutputStream);
        }

        HttpContext.Current.Response.Flush();
        HttpContext.Current.Response.End();

    }
}