﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using XMLMethodLibrary;

public partial class API_CreateMessage : System.Web.UI.UserControl
{
    public bool IsHome { get { return (ViewState["IsHome"] != null) ? bool.Parse(ViewState["IsHome"].ToString()) : false; } set { ViewState["IsHome"] = value; } }
    public string RefCustomer { get { return (ViewState["RefCustomerMessage"] != null) ? ViewState["RefCustomerMessage"].ToString() : ""; } set { ViewState["RefCustomerMessage"] = value; } }
    public bool ActionAllowed { get { return (ViewState["ActionAllowed"] != null) ? bool.Parse(ViewState["ActionAllowed"].ToString()) : false; } set { ViewState["ActionAllowed"] = value; } }
    public bool IsPro { get { return (ViewState["IsPro"] != null) ? bool.Parse(ViewState["IsPro"].ToString()) : false; } set { ViewState["IsPro"] = value; } }
    public bool AllowChangingMode { get { return (ViewState["AllowChangingMode"] != null) ? bool.Parse(ViewState["AllowChangingMode"].ToString()) : false; } set { ViewState["AllowChangingMode"] = value; } }
    private List<string> _listSelectedAgencies { get { return (ViewState["SelectedAgencies"] != null) ? (List<string>)ViewState["SelectedAgencies"] : null; } set { ViewState["SelectedAgencies"] = value; } }
    private List<string> _listSelectedCustomer { get { return (ViewState["SelectedCustomers"] != null) ? (List<string>)ViewState["SelectedCustomers"] : new List<string>(); } set { ViewState["SelectedCustomers"] = value; } }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (ActionAllowed)
                btnValidate.Visible = true;
            else btnValidate.Visible = false;

            if (IsPro)
            {
                panelTo.Visible = true;
                panelToAgency.Visible = true;

                BindAgencyList();
            }
            else
            {
                if (RefCustomer.Length == 0)
                {
                    panelTo.Visible = true;
                    panelToClients.Visible = true;
                }
            }
        }

        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "PostBackLoadKey", "PostBackLoad();", true);
    }

    public event EventHandler validated;
    protected void btnValidate_Click(object sender, EventArgs e)
    {
        if (CheckForm())
        {
            string sMessage = "";
            string sTitle = "";

            string sFrom = (txtFrom.Text.Length > 0) ? txtFrom.Text + " 00:00:00" : "";
            string sTo = (txtTo.Text.Length > 0) ? txtTo.Text + " 23:59:59" : "";
            bool bIsHome = IsHome;
            bool bAllCustomer = (IsPro) ? ((rblSendToAgency.SelectedValue == "all") ? true : false) : ((rblSendToClient.SelectedValue == "all") ? true : false);
            bool bTrack = ckbTracking.Checked;

            List<string> listRefCustomer = new List<string>();
            if (IsPro && rblSendToAgency.SelectedValue == "select")
            {
                for (int i = 0; i < _listSelectedAgencies.Count; i++)
                    listRefCustomer.Add(_listSelectedAgencies[i].Split('|')[1]);
            }
            else
            {
                if (rblSendToClient.SelectedValue == "select")
                {
                    for (int i = 0; i < _listSelectedCustomer.Count; i++)
                        listRefCustomer.Add(_listSelectedCustomer[i].Split('|')[0]);
                }
                else
                {
                    if (RefCustomer.Length > 0) listRefCustomer.Add(RefCustomer);
                }

                if (!bIsHome)
                    bTrack = true;
            }


            string sKind = null;

            if (rblDisplayMethod.SelectedIndex != -1)
            {
                sKind = rblDisplayMethod.SelectedValue;
                if (rblMessageFormat.SelectedValue == "html")
                    sKind += "URL";
            }
            if (rblDisplayMethodAppMobile.SelectedIndex != -1)
            {
                if (!string.IsNullOrWhiteSpace(sKind))
                    sKind += ";";
                sKind += rblDisplayMethodAppMobile.SelectedValue;
            }

            string sContent = "";
            if (rblMessageFormat.SelectedValue == "text")
                sContent = txtContent.Text;
            else sContent = txtIFrameURL.Text;

            if (SaveMessage("", txtTitle.Text, sContent, bIsHome, IsPro, sFrom, sTo, listRefCustomer, bAllCustomer, ckbImportant.Checked, bTrack, sKind))
            {
                sTitle = "Information";
                sMessage = "<span style='color:black'>Message enregistré</span>";

                txtTitle.Text = "";
                txtContent.Text = "";
                txtFrom.Text = "";
                txtTo.Text = "";
                txtAgenciesSelected.Text = "";
                txtSearchClient.Text = "";

                DataTable dt = new DataTable();
                rptSendToClient.DataSource = dt;
                rptSendToClient.DataBind();
                btnDeleteSendToClient.Visible = false;

                rblSendToAgency.SelectedIndex = 0;
                rblSendToClient.SelectedIndex = 0;

                if (_listSelectedAgencies != null)
                    _listSelectedAgencies.Clear();

                if (_listSelectedCustomer != null)
                    _listSelectedCustomer.Clear();

                if (validated != null)
                    validated(sender, e);
            }
            else
            {
                sTitle = "Erreur";
                sMessage = "Une erreur est survenue.<br/>Veuillez réessayer.";
            }

            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "AlertMessageValidate_Key", "AlertMessage('" + sTitle + "', \"" + sMessage + "\", null, null);", true);
        }
        else
        {
            if (validated != null)
                validated(sender, e);
        }
    }
    protected bool CheckForm()
    {
        bool bValid = true;
        string sMessage = "";

        if (rblMessageFormat.SelectedValue == "text" && txtTitle.Text.Length == 0)
        {
            bValid = false;
            sMessage += "Titre : champ requis<br/>";
        }
        if (rblMessageFormat.SelectedValue == "text" && txtContent.Text.Length == 0)
        {
            bValid = false;
            sMessage += "Contenu : champ requis<br/>";
        }
        if (rblMessageFormat.SelectedValue == "html" && txtIFrameURL.Text.Length == 0)
        {
            bValid = false;
            sMessage += "Nom du fichier HTML : champ requis<br/>";
        }
        if ((txtFrom.Text.Length > 0 && !tools.IsDateValid(txtFrom.Text)) || (txtTo.Text.Length > 0 && !tools.IsDateValid(txtTo.Text)))
        {
            bValid = false;
            sMessage += "Date : format incorrect<br/>";
        }
        if (!IsHome && RefCustomer.Length == 0 && !((rblSendToClient.SelectedValue == "all") ? true : false) && _listSelectedCustomer.Count == 0 || (IsPro && rblSendToAgency.SelectedValue == "select" && _listSelectedAgencies.Count == 0))
        {
            bValid = false;
            sMessage += "Destinataire : champ requis<br/>";
        }

        if(rblDisplayMethod.SelectedIndex == -1 && rblDisplayMethodAppMobile.SelectedIndex == -1)
        {
            bValid = false;
            sMessage += "Méthode d'affichage : champ requis<br/>";
        }

        if (!bValid)
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "AlertMessage_Key", "AlertMessage('Erreur', \"" + sMessage + "\", null, null);", true);

        return bValid;
    }
    protected bool SaveMessage(string sRefMessage, string sTitle, string sContent, bool bHome, bool bIsPro, string sDateFrom, string sDateTo, List<string> listRefCustomer, bool bAll, bool bImportant, bool bTracking, string sKind)
    {
        bool bSaved = false;
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_AddDelSetMessage", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);

            XElement xmlMessage = new XElement("Message",
                            new XAttribute("RefMessage", sRefMessage),
                            new XAttribute("Title", sTitle),
                            new XAttribute("Content", sContent),
                            new XAttribute("FromDate", sDateFrom),
                            new XAttribute("ToDate", sDateTo),
                            new XAttribute("Important", (bImportant) ? "1" : "0"),
                            new XAttribute("HomePage", (bHome) ? "1" : "0"),
                            new XAttribute("IsPro", (bIsPro) ? "1" : "0"),
                            new XAttribute("AllCustomer", (bAll) ? "1" : "0"),
                            new XAttribute("Tracking", (bTracking) ? "1" : "0"),
                            new XAttribute("Action", (sRefMessage.Length > 0) ? "U" : "A"));

            for (int i = 0; i < listRefCustomer.Count; i++)
            {
                xmlMessage.Add(new XElement("TO", new XAttribute("RefCustomer", listRefCustomer[i])));
            }

            XElement xml = new XElement("ALL_XML_IN");
            string[] arKind = sKind.Split(';');
            for (int i = 0; i < arKind.Length; i++)
            {
                XElement xmlMessageTmp = xmlMessage;
                xmlMessageTmp.Add(new XAttribute("Kind", arKind[i]));
                xml.Add(xmlMessageTmp);
            }

            cmd.Parameters["@IN"].Value = xml.ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if (sXmlOut.Length > 0)
            {
                List<string> listInsert = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Result", "Inserted");
                List<string> listUpdate = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Result", "Updated");
                int iInsert = (listInsert.Count > 0 && listInsert[0].Length > 0) ? int.Parse(listInsert[0].ToString()) : 0;
                int iUpdate = (listUpdate.Count > 0 && listUpdate[0].Length > 0) ? int.Parse(listUpdate[0].ToString()) : 0;

                if ((sRefMessage.Length > 0 && iUpdate >= 1) || (sRefMessage.Length == 0 && iInsert >= 1))
                    bSaved = true;
            }
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return bSaved;
    }

    protected string ConvertToHTML(string sContent)
    {
        string sContentTmp = "";

        sContentTmp += sContent.Replace("[b]", "<b>").Replace("[/b]", "</b>");
        sContentTmp += sContent.Replace("[i]", "<i>").Replace("[/i]", "</i>");
        sContentTmp += sContent.Replace("[u]", "<u>").Replace("[/u]", "</u>");
        sContentTmp += sContent.Replace("[o]", "<span style=\"color:#f57527\">").Replace("[/o]", "</span>");

        return sContentTmp;
    }

    protected void BindAgencyList()
    {
        DataTable dt = tools.GetAgencyList();
        ddlAgency.DataSource = dt;
        ddlAgency.DataBind();
    }
    protected void rblSendToAgency_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rblSendToAgency.SelectedValue == "select")
            panelSelectAgency.Visible = true;
        else panelSelectAgency.Visible = false;
    }
    protected void btnAddAgencyTo_Click(object sender, EventArgs e)
    {
        if (_listSelectedAgencies == null)
            _listSelectedAgencies = new List<string>();
        string sRefCustomer = GetRefCustomerFromAgencyID(ddlAgency.SelectedItem.Value);

        if (ddlAgency.SelectedValue.Length > 0 && sRefCustomer.Length > 0 && !_listSelectedAgencies.Contains(ddlAgency.SelectedItem.Text + "|" + sRefCustomer))
        {
            txtAgenciesSelected.Text += ((txtAgenciesSelected.Text.Length > 0) ? "; " : "") + ddlAgency.SelectedItem.Text;
            _listSelectedAgencies.Add(ddlAgency.SelectedItem.Text + "|" + sRefCustomer);
        }
    }

    protected string GetRefCustomerFromAgencyID(string sAgencyID)
    {
        string sRefCustomer = "";
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_GetRefCustomerFromAgencyID", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@AgencyID", SqlDbType.VarChar, 20);
            cmd.Parameters.Add("@RefCustomer", SqlDbType.Int);

            cmd.Parameters["@AgencyID"].Value = sAgencyID;
            cmd.Parameters["@RefCustomer"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            sRefCustomer = cmd.Parameters["@RefCustomer"].Value.ToString();
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return sRefCustomer;
    }
    protected void rblSendToClient_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rblSendToClient.SelectedValue == "select")
            panelSelectClients.Visible = true;
        else panelSelectClients.Visible = false;
    }

    protected void btnSearchClient_Click(object sender, EventArgs e)
    {
        if (txtSearchClient.Text.Length > 0)
        {
            string sToken = authentication.GetCurrent(false).sToken;
            DataTable dt = Client.GetClientList("", "", "", "", "", txtSearchClient.Text, sToken);
            rptSearchClient.DataSource = dt;
            rptSearchClient.DataBind();

            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "ShowSearchResult_Key", "ShowSearchResult();", true);
        }
        else ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "AlertMessage_Key", "AlertMessage('Erreur', \"Recherche : champ requis\", null, null);", true);
    }

    protected void btnAddSearchClient_Click(object sender, EventArgs e)
    {
        List<string> list = _listSelectedCustomer;

        for (int i = 0; i < rptSearchClient.Items.Count; i++)
        {
            HiddenField hdn = (HiddenField)rptSearchClient.Items[i].FindControl("hdnSearchClientRef");
            CheckBox ckb = (CheckBox)rptSearchClient.Items[i].FindControl("ckbSelectSearchClient");
            Label lbl = (Label)rptSearchClient.Items[i].FindControl("lblSearchClientInfos");

            if (hdn != null && hdn.Value.Length > 0 && ckb != null && ckb.Checked)
            {
                if (!IsCustomerSelected(hdn.Value))
                {
                    list.Add(hdn.Value + "|" + lbl.Text);
                }
            }
        }

        _listSelectedCustomer = list;

        RefreshClientSelection();
    }

    private bool IsCustomerSelected(string sRefCustomer)
    {
        if (_listSelectedCustomer != null)
        {
            for (int i = 0; i < _listSelectedCustomer.Count; i++)
            {
                if (_listSelectedCustomer[i].Split('|')[0] == sRefCustomer)
                    return true;
            }
        }
        return false;
    }

    protected void RefreshClientSelection()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("RefCustomer");
        dt.Columns.Add("Label");

        for (int i = 0; i < _listSelectedCustomer.Count; i++)
        {
            DataRow row = dt.NewRow();
            row["RefCustomer"] = _listSelectedCustomer[i].Split('|')[0];
            row["Label"] = _listSelectedCustomer[i].Split('|')[1];

            dt.Rows.Add(row);
        }

        rptSendToClient.DataSource = dt;
        rptSendToClient.DataBind();

        if (dt.Rows.Count > 0)
            btnDeleteSendToClient.Visible = true;
        else btnDeleteSendToClient.Visible = false;
    }

    protected void btnDeleteSendToClient_Click(object sender, EventArgs e)
    {
        List<string> list = _listSelectedCustomer;

        for (int i = 0; i < rptSendToClient.Items.Count; i++)
        {
            HiddenField hdn = (HiddenField)rptSendToClient.Items[i].FindControl("hdnSendToClientRef");
            CheckBox ckb = (CheckBox)rptSendToClient.Items[i].FindControl("ckbSendToClient");

            if (hdn != null && hdn.Value.Length > 0 && ckb != null && ckb.Checked)
            {
                for (int j = 0; j < _listSelectedCustomer.Count; j++)
                {
                    if (_listSelectedCustomer[j].Split('|')[0] == hdn.Value)
                    {
                        list.RemoveAt(j);
                        break;
                    }
                }
            }
        }

        _listSelectedCustomer = list;

        RefreshClientSelection();
    }

    protected void rblMessageFormat_SelectedIndexChanged(object sender, EventArgs e)
    {
        panelTextFormat.Visible = false;
        panelIframeFormat.Visible = false;

        switch (rblMessageFormat.SelectedValue)
        {
            case "text":
                panelTextFormat.Visible = true;
                break;
            case "html":
                panelIframeFormat.Visible = true;
                break;
        }
    }
}