﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class API_Balance : System.Web.UI.UserControl
{
    public int RefCustomer { get { return (ViewState["RefCustomer"] != null) ? int.Parse(ViewState["RefCustomer"].ToString()) : 0; } set { ViewState["RefCustomer"] = value; } }

    protected void Page_Load(object sender, EventArgs e)
    {
        //if(!IsPostBack)
        //    BindAccountBalance(RefCustomer);
    }

    protected void BindAccountBalance(int iRefCustomer)
    {
        if (iRefCustomer != 0)
        {
            decimal dAccountBalance = -1;
            string sFormattedAccountBalance = "";
            tools.GetAccountBalance(iRefCustomer, out dAccountBalance, out sFormattedAccountBalance);
            lblBalance.Text = sFormattedAccountBalance;
        }
    }

    protected void btnRefresh_Click(object sender, ImageClickEventArgs e)
    {
        BindAccountBalance(RefCustomer);
    }
}