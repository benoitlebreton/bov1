﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AccountGroupManager.ascx.cs" Inherits="API_AccountGroupManager" %>

<div class="accountgroupmanager-panel-loading panel-loading"></div>

<div class="accountgroupmanager-form">
    <asp:UpdatePanel ID="upGroup" runat="server">
        <ContentTemplate>
            <asp:Repeater ID="rptGroupContent" runat="server">
                <HeaderTemplate>
                    <table class="defaultTable2">
                        <tr>
                            <th>Prénom</th>
                            <th>Nom</th>
                            <th>N° compte</th>
                            <th></th>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                        <tr class='<%#Container.ItemIndex % 2 == 0 ? "row-even" : "row-odd" %>'>
                            <td style="vertical-align:middle">
                                <asp:Label ID="lblFirstName" runat="server"><%#Eval("FirstName") %></asp:Label>
                            </td>
                            <td style="vertical-align:middle">
                                <asp:Label ID="lblLastName" runat="server"><%#Eval("LastName") %></asp:Label>
                            </td>
                            <td style="text-align:center;vertical-align:middle">
                                <asp:Label ID="lblAccountNumber" runat="server"><%#Eval("AccountNumber") %></asp:Label>
                            </td>
                            <td style="width:20px;vertical-align:middle">
                                <asp:HiddenField ID="hdnRefGroup" runat="server" Value='<%#Eval("RefGroup") %>' />
                                <asp:HiddenField ID="hdnRefCustomer" runat="server" Value='<%#Eval("RefCustomer") %>' />
                                <asp:CheckBox ID="ckbSelect" runat="server" Visible='<%# !HideEdit %>' />
                            </td>
                        </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                    <div style="text-align:right">
                        <input type="button" class='delete-link<%= HideEdit ? " hidden" : "" %>' onclick="ShowDeleteLinkDialog(this);" />
                    </div>
                </FooterTemplate>
            </asp:Repeater>
            <asp:Button ID="btnDeleteLink" runat="server" Text="" OnClick="btnDeleteLink_Click" style="display:none" />

            <asp:Panel ID="panelNoGroup" runat="server" Visible="false">
                Le client n'est lié à aucun compte
            </asp:Panel>
            <div style="position:relative;height:0">
                <asp:Panel ID="panelLinkAccount" runat="server" DefaultButton="btnAddToGroup" CssClass="shorten-link">
                    <div class="link-account">
                        <div>
                            <asp:TextBox ID="txtLinkAccountNumber" runat="server" placeholder="n° compte"></asp:TextBox>
                        </div>
                        <div>
                            <asp:Button ID="btnAddToGroup" runat="server" Text="Lier" OnClick="btnAddToGroup_Click" CssClass="link-button" />
                        </div>
                    </div>
                </asp:Panel>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>

<div id="create-group-dialog" style="display:none">
    <asp:DropDownList ID="ddlGroupKind" runat="server" AppendDataBoundItems="true" DataTextField="text" DataValueField="value">
        <asp:ListItem Text="" Value=""></asp:ListItem>
    </asp:DropDownList>
</div>

<div id="dialog-confirm-delete-link" style="display:none">
    Voulez-vous vraiment supprimer <span class="link-text"></span> ?
    <br />
    <b>Attention :</b> cette opération est irréversible.
</div>

<script type="text/javascript">
    Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(AccountGroupManagerBeginRequestHandler);
    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(AccountGroupManagerEndRequestHandler);
    function AccountGroupManagerBeginRequestHandler(sender, args) {
        try{
            pbControl = args.get_postBackElement();

            if (pbControl != null) {
                //console.log(pbControl.id);
                if (pbControl.id.indexOf('btnAddToGroup') != -1 ||
                    pbControl.id.indexOf('btnDeleteLink') != -1) {

                    var controlID = '.accountgroupmanager-form';
                    var panel = $(controlID);

                    $('.accountgroupmanager-panel-loading').show();
                    $('.accountgroupmanager-panel-loading').width(panel.outerWidth());
                    $('.accountgroupmanager-panel-loading').height(panel.outerHeight());

                    $('.accountgroupmanager-panel-loading').position({
                        my: 'center',
                        at: 'center',
                        of: controlID
                    });
                }
            }
        }
        catch(e)
        {

        }
    }
    function AccountGroupManagerEndRequestHandler(sender, args) {
        $('.accountgroupmanager-panel-loading').hide();
    }

    function ShowDeleteLinkDialog(btn) {
        var nbCkb = $(btn).parent().parent().find('table td input[type=checkbox]:checked').length;

        if (nbCkb > 0) {
            var title = '';
            if (nbCkb > 1)
            {
                title = 'Supprimer les liens';
                $('.link-text').html('les liens');
            }
            else {
                title = 'Supprimer le lien';
                $('.link-text').html('le lien');
            }
            $('#dialog-confirm-delete-link').dialog({
                draggable: false,
                resizable: false,
                width: 400,
                dialogClass: "no-close",
                modal: true,
                title: title,
                buttons:
                    {
                        Supprimer: function () { $(this).dialog('destroy'); $('#<%=btnDeleteLink.ClientID%>').click(); },
                        Annuler: function () { $(this).dialog('destroy'); }
                    }
            });
            $('#dialog-confirm-delete-link').parent().appendTo(jQuery("form:first"));
        }
    }
</script>