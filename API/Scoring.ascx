﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Scoring.ascx.cs" Inherits="API_Scoring" %>

<asp:Panel ID="panelScoring" runat="server">
    <div class="scoring-container">
        <asp:Panel ID="panelIcon" runat="server" CssClass="scoring-icon" onclick="ToggleScoringDetails($(this));">
            <div>
                <asp:Label ID="lblLabel" runat="server">Scoring client</asp:Label>
            </div>
            <div>
                <asp:Label ID="lblScore" runat="server"></asp:Label>
            </div>
        </asp:Panel>
        <asp:Panel ID="panelDetails" runat="server" CssClass="scoring-details">
            <div class="arrow-up"></div>
            <div class="arrow-bg"></div>
            <asp:UpdatePanel ID="upDetails" runat="server">
                <ContentTemplate>
                    <div class="scoring-details-loading"></div>
                    <asp:Panel ID="panelCategory" runat="server" Visible="false" CssClass="scoring-details-category">
                        <asp:Repeater ID="rptCategory" runat="server">
                            <ItemTemplate>
                                <div class='<%#bool.Parse(Eval("IsMax").ToString()) ? "active" : "" %>' onmouseover="ShowCategoryDetails(<%# Container.ItemIndex %>);">
                                    <div><%#Eval("Label") %></div>
                                    <div><%#Eval("Value") %></div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </asp:Panel>
                    <div class="scoring-details-header">
                        <div>
                            Critère
                            <%--<asp:LinkButton ID="lnkCritera" runat="server" OnClick="lnkHeader_Click" OnClientClick="ShowLoadingScore()">
                                Critère
                            </asp:LinkButton>--%>
                        </div>
                        <div>
                            Valeur
                            <%--<asp:LinkButton ID="lnkScore" runat="server" OnClick="lnkHeader_Click" OnClientClick="ShowLoadingScore()">
                                Score
                            </asp:LinkButton>--%>
                        </div>
                    </div>
                    <div class="scoring-details-inner">
                        <asp:Repeater ID="rptDetailsRepeat" runat="server">
                            <ItemTemplate>
                                <div class='<%#bool.Parse(Eval("IsMax").ToString()) ? "active" : "" %>'>
                                    <asp:Repeater ID="rptDetails" runat="server" DataSource='<%#Eval("Details") %>'>
                                        <ItemTemplate>
                                            <div class="scoring-details-item">
                                                <div>
                                                    <asp:Image ID="imgAlert" runat="server" ImageUrl="~/Styles/Img/score_alert.png" Visible='<%#bool.Parse(Eval("Alert").ToString()) && bool.Parse(DataBinder.Eval(Container.Parent.Parent, "DataItem.IsMax").ToString()) && int.Parse(DataBinder.Eval(Container.Parent.Parent, "DataItem.Value").ToString()) > 30 %>' />
                                                </div>
                                                <div>
                                                    <%#Eval("Label") %>
                                                </div>
                                                <div>
                                                    <%#Eval("Value") %>
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </div>
</asp:Panel>