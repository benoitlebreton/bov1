﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using XMLMethodLibrary;

public partial class API_sms_gage : System.Web.UI.UserControl
{
    public string RefCustomer { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindSMSGage(RefCustomer);
        }
    }

    protected void BindSMSGage(string sRefCustomer)
    {
        string sSMSYearAllowed = "";
        string sSMSYearUsed = "";
        string sSMSMonthUsed = "";
        string sSMSTotalUsed = "";
        string sSMSPerPack = "";
        string sSMSPackUsed = "";
        string sSMSActivePackUsed = "";
        string sSMSPackPrice = "";

        GetCustomerSMSSituation(sRefCustomer, out sSMSYearAllowed, out sSMSYearUsed, out sSMSMonthUsed, out sSMSTotalUsed, out sSMSPerPack, out sSMSPackUsed, out sSMSActivePackUsed, out sSMSPackPrice);

        // TEST
        //sSMSYearUsed = "3";
        //sSMSPackUsed = "1";
        //sSMSActivePackUsed = "3";

        int i = 0;
        if (sSMSPackUsed == "0")
        {
            lblSMSTotal.Text = sSMSYearAllowed;
            lblSMSUsed.Text = sSMSYearUsed;
        }
        else
        {
            lblSMSTotal.Text = sSMSPerPack;
            lblSMSUsed.Text = sSMSActivePackUsed;
        }
        lblTotalSMSYear.Text = sSMSYearUsed;
        lblSMSYearAllowed.Text = sSMSYearAllowed;
        lblSMSPerPack.Text = sSMSPerPack;
        lblSMSPackUsed.Text = sSMSPackUsed;
        lblSMSActivePackUsed.Text = sSMSActivePackUsed;
        lblSMSPackPrice.Text = sSMSPackPrice;

        // ACCORDS
        int iTmp = 0;
        int.TryParse(sSMSYearAllowed, out iTmp);
        if (iTmp > 1)
            lblTotalSMSYearAccord.Text = "s";
        int.TryParse(sSMSPackUsed, out iTmp);
        if (iTmp > 1)
        {
            lblSMSPackUsedAccord1.Text = "s";
            lblSMSPackUsedAccord2.Text = "s";
            lblSMSPackUsedAccord3.Text = "s";
        }
        int.TryParse(sSMSActivePackUsed, out iTmp);
        if (iTmp > 1)
            lblSMSActivePackUsedAccord.Text = "s";

        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "InitSMSGageKey", "InitSMSGage('" + lblSMSUsed.Text + "', '" + lblSMSTotal.Text + "');", true);
    }

    public static void GetCustomerSMSSituation(string sRefCustomer, out string sSMSYearAllowed, out string sSMSYearUsed, out string sSMSMonthUsed, out string sSMSTotalUsed, out string sSMSPerPack, out string sSMSPackUsed, out string sSMSActivePackUsed, out string sSMSPackPrice)
    {
        string sXmlOut = "";
        SqlConnection conn = null;
        sSMSYearAllowed = "";
        sSMSYearUsed = "";
        sSMSMonthUsed = "";
        sSMSTotalUsed = "";
        sSMSPerPack = "";
        sSMSPackUsed = "";
        sSMSActivePackUsed = "";
        sSMSPackPrice = "";

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("Web.P_GetCustomerSMSSituation", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN", new XElement("Situation", new XAttribute("RefCustomer", sRefCustomer))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if (sXmlOut.Length > 0)
            {
                List<string> listNbSentSMSThisYear = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Situation", "NbSentSMSThisYear");
                sSMSYearUsed = (listNbSentSMSThisYear.Count > 0) ? listNbSentSMSThisYear[0] : "";
                List<string> listNbSentSMSThisMonth = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Situation", "NbSentSMSThisMonth");
                sSMSMonthUsed = (listNbSentSMSThisMonth.Count > 0) ? listNbSentSMSThisMonth[0] : "";
                List<string> listNbSentSMSTot = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Situation", "NbSentSMSTot");
                sSMSTotalUsed = (listNbSentSMSTot.Count > 0) ? listNbSentSMSTot[0] : "";
                List<string> listNbSMSAllowedperYear = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Situation", "NbSMSAllowedperYear");
                sSMSYearAllowed = (listNbSMSAllowedperYear.Count > 0) ? listNbSMSAllowedperYear[0] : "";
                List<string> listNbSMSPerNewPack = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Situation", "NbSMSPerNewPack");
                sSMSPerPack = (listNbSMSPerNewPack.Count > 0) ? listNbSMSPerNewPack[0] : "";
                List<string> listNbBoughtPackThisYear = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Situation", "NbBoughtPackThisYear");
                sSMSPackUsed = (listNbBoughtPackThisYear.Count > 0) ? listNbBoughtPackThisYear[0] : "";
                List<string> listNbSMSinActivePack = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Situation", "NbSMSinActivePack");
                sSMSActivePackUsed = (listNbSMSinActivePack.Count > 0) ? listNbSMSinActivePack[0] : "";
                List<string> listSMSPackPrice = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Situation", "SMSPackPrice");
                sSMSPackPrice = (listSMSPackPrice.Count > 0) ? listSMSPackPrice[0] : "";
            }
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }
    }
}