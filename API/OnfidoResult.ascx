﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="OnfidoResult.ascx.cs" Inherits="API_OnfidoResult" %>

<asp:Panel ID="panelNoResult" runat="server" Visible="false" CssClass="no-result-panel">
    Aucun résultat Onfido
</asp:Panel>

<asp:Panel ID="panelResult" runat="server" CssClass="onfido-result">
    <asp:Repeater ID="rptReport" runat="server">
        <ItemTemplate>
            <div class="onfido-report">
                <div class="report-title">
                    <div class="report-name"><asp:Label ID="lblName" runat="server" Text='<%#Eval("Name") %>'></asp:Label></div>
                    <%--<div><asp:Label ID="lblResult" runat="server" Text='<%#Eval("Result") %>'></asp:Label></div>--%>
                    <div class="report-result">
                        <%#GetFriendlyLabel(Eval("Result").ToString(), false) %>
                    </div>
                    <div>
                        <%#Eval("ElapsedTime") %>
                    </div>
                </div>
                <div class="breakdown-list">
                    <asp:Repeater ID="rptBreakdowns" runat="server" DataSource='<%#Eval("Breakdown") %>'>
                        <ItemTemplate>
                            <div class="breakdown">
                                <asp:Image ID="imgBreakdownCat" runat="server" ImageUrl='<%#GetImageUrl(Eval("Result").ToString()) %>' />
                                <span class="breakdown-cat">
                                    <asp:Label ID="lblBreakdownCat" runat="server"><%#Eval("Name") %></asp:Label> : 
                                    <%#GetFriendlyLabel(Eval("Result").ToString(), true) %>
                                </span>
                                <asp:Repeater ID="rptBreakdownDetails" runat="server" DataSource='<%#Eval("Breakdown") %>'>
                                    <ItemTemplate>
                                        <div class="inner-breakdown">
                                            <asp:Image ID="imgBreakdown" runat="server" ImageUrl='<%#GetImageUrl(Eval("Result").ToString()) %>' />
                                            <asp:Label ID="lblBreakdown" runat="server" CssClass='<%#(bool.Parse(Eval("Important").ToString())) ? "important" : "" %>'><%#Eval("Name") %></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </ItemTemplate>
    </asp:Repeater>
    <div style="text-align:right;font-weight:bold;padding:5px 15px 0 0;">
        Total : <asp:Label ID="lblTotalReportElapsedTime" runat="server"></asp:Label>
    </div>

    <div class="onfido-report">
        <div class="report-title">
            <div class="report-name">Traitement OCR</div>
            <div></div>
            <div><asp:Label ID="lblTotalOCRTime" runat="server"></asp:Label></div>
        </div>
        <asp:Repeater ID="rptOCRElapsedTime" runat="server">
            <HeaderTemplate>
                <div>
                    <table class="table-elapsed-time">
                        <tr>
                            <th>Type</th>
                            <th>Face</th>
                            <th>Temps de traitement</th>
                        </tr>
            </HeaderTemplate>
            <ItemTemplate>
                        <tr>
                            <td><%#Eval("DocType") %></td>
                            <td><%#Eval("DocSide") %></td>
                            <td><%#Eval("ElapsedTime") %>s</td>
                        </tr>
            </ItemTemplate>
            <FooterTemplate>
                    </table>
                </div>
            </FooterTemplate>
        </asp:Repeater>
    </div>
</asp:Panel>