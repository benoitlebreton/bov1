﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SABCobaltOpeComparison.ascx.cs" Inherits="API_SABCobaltOpeComparison" %>

<div class="sab-cobalt-comparison-cont">
    <div>
        <div style="color: #344b56; font-weight: bold; text-transform: uppercase; border-bottom: 1px solid #344b56; margin-bottom: 10px">
            COBALT
        </div>
        <iframe id="cobalt-frame" src="ClientBalanceOperations_Frame.aspx?ref=<%=RefCustomer%>&accountno=<%=AccountNumber%>&isCobalt=1&iban=<%=IBAN%>&erp=1"></iframe>
    </div>
    <div>
        <div style="color: #344b56; font-weight: bold; text-transform: uppercase; border-bottom: 1px solid #344b56; margin-bottom: 10px">
            SAB
        </div>
        <iframe id="sab-frame" src="ClientBalanceOperations_Frame.aspx?ref=<%=RefCustomer%>&accountno=<%=AccountNumber%>&isCobalt=0&iban=<%=IBAN%>&erp=1"></iframe>
    </div>
</div>