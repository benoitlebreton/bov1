﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MessageHistory.ascx.cs" Inherits="API_MessageHistory" %>

<asp:Repeater ID="rptMessageHistory" runat="server" OnItemDataBound="rptMessageHistory_ItemDataBound">
    <HeaderTemplate>
        <table class="tableHistory" style="width:100%;border-collapse:collapse;">
            <tr>
                <th>Titre</th>
                <th>Contenu</th>
                <th>
                    <asp:Label ID="lblNbDestHeader" runat="server"></asp:Label>
                </th>
                <th>Du</th>
                <th>Au</th>
                <th></th>
            </tr>
    </HeaderTemplate>
    <ItemTemplate>
            <tr>
                <td style="width:30%;word-break:break-all">
                    <%#Eval("Title") %>
                </td>
                <td style="word-break:break-all">
                    <%#Eval("Content") %>
                </td>
                <td style="text-align:center; width:1px;white-space:nowrap">
                    <%#Eval("NbDest") %>
                </td>
                <td style="text-align:center; width:1px;white-space:nowrap">
                    <%#Eval("From") %>
                </td>
                <td style="text-align:center; width:1px;white-space:nowrap">
                    <%#Eval("To") %>
                </td>
                <td style="width:1px;text-align:right">
                    <asp:HiddenField ID="hdnHide" runat="server" Value='<%#Eval("Hide") %>' />
                    <asp:Button ID="btnUnpublish" runat="server" Text="Dépublier" OnClick="btnUnpublish_Click" CommandArgument='<%#Eval("Ref") %>' Visible="false" />
                </td>
            </tr>
    </ItemTemplate>
    <FooterTemplate>
        </table>
    </FooterTemplate>
</asp:Repeater>