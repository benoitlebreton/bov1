﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BeneficiaryList.ascx.cs" Inherits="API_BeneficiaryList" %>

<asp:Panel ID="panelBeneficiaries" runat="server">
    <asp:Panel ID="panelBeneficiaryList" runat="server">
        <asp:Repeater ID="rBeneficiaryList" runat="server">
            <HeaderTemplate>
                <table id="tBeneficiaryList" cellpadding="0" cellspacing="0" style="width: 100%" class="defaultTable2">
                    <thead>
                        <tr>
                            <th>Statut</th>
                            <th>Nom</th>
                            <th>Banque</th>
                            <th>IBAN</th>
                            <th>BIC</th>
                            <th>Confirmé le</th>
                            <!--<th></th>
                            <th></th>-->
                        </tr>
                    </thead>
                    <tbody>
            </HeaderTemplate>
            <ItemTemplate>
                <tr class='<%# Container.ItemIndex % 2 == 0 ? "beneficiary-table-row" : "beneficiary-table-alternate-row" %>'>
                    <td style="white-space:nowrap;">
                        <div  style="display:inline-block">
                            <img src='<%# Eval("UrlImageStatut1") %>'  Title='<%# Eval("StatutDescription") %>' width="25px" height ="25px" <%# Eval("VisibilityImageStatut1") %>/> 
                            <img src='<%# Eval("UrlImageStatut2") %>'  Title='<%# Eval("StatutDescription") %>' width="25px" height ="25px" <%# Eval("VisibilityImageStatut2") %> /> 
                        </div>
                    </td>
                    <td <%# ((bool)Eval("IsBeneficiaryValidate")) ? "" : "style='text-decoration: line-through;color:red;'" %> >
                        <%# Eval("Name") %>
                        <span id="refBenef" style="display: none"><%# Eval("Reference") %></span>
                    </td>
                    <td <%# ((bool)Eval("IsBeneficiaryValidate")) ? "" : "style='text-decoration: line-through;color:red;'" %> >
                        <%# Eval("BankName") %>
                    </td>
                    <td <%# ((bool)Eval("IsBeneficiaryValidate")) ? "" : "style='text-decoration: line-through;color:red;'" %> >
                        <%# Eval("Iban") %>
                    </td>
                    <td <%# ((bool)Eval("IsBeneficiaryValidate")) ? "" : "style='text-decoration: line-through;color:red;'" %> >
                        <%# Eval("Bic") %>
                    </td>
                            
                    <asp:Image ID="imgAccountType" runat="server" />
                    <td <%# ((bool)Eval("IsBeneficiaryValidate")) ? "style='text-align: center'" : "style='text-align: center;text-decoration: line-through;color:red;'" %> >
                        <%# Eval("ConfirmeLe") %>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </tbody>
            </table>
            </FooterTemplate>
        </asp:Repeater>
    </asp:Panel>
    <asp:Panel ID="panelBeneficiaryList_Empty" runat="server" Visible="false" Style="margin-top: 10px; font-weight:bold">Aucun bénéficiaire</asp:Panel>
</asp:Panel>