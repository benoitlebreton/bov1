﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using XMLMethodLibrary;

public partial class API_ClientRelation : System.Web.UI.UserControl
{
    public int RefCustomer
    {
        get { return (ViewState["RefCustomer"] != null) ? int.Parse(ViewState["RefCustomer"].ToString()) : 0; }
        set { ViewState["RefCustomer"] = value; }
    }
    public bool IsPopup
    {
        get { return (ViewState["IsPopup"] != null) ? bool.Parse(ViewState["IsPopup"].ToString()) : false; }
        set { ViewState["IsPopup"] = value; }
    }
    protected bool IsEditable
    {
        get { return (ViewState["IsEditable"] != null) ? bool.Parse(ViewState["IsEditable"].ToString()) : false; }
        set { ViewState["IsEditable"] = value; }
    }
    protected string sClientRelations
    {
        get { return (Session["ClientRelations"] != null) ? Session["ClientRelations"].ToString() : ""; }
        set { Session["ClientRelations"] = value; }
    }
    public bool HasViewRight
    {
        get { return (ViewState["HasViewRight"] != null) ? bool.Parse(ViewState["HasViewRight"].ToString()) : false; }
        set { ViewState["HasViewRight"] = value; }
    }
    protected bool HasActionRight
    {
        get { return (ViewState["HasActionRight"] != null) ? bool.Parse(ViewState["HasActionRight"].ToString()) : false; }
        set { ViewState["HasActionRight"] = value; }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        if (!IsPostBack)
            CheckRights();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            if (HasViewRight)
            {
                if (IsPopup)
                {
                    panelRelation.CssClass = "relation-container hidden";
                    rblPeriodFlow.Visible = false;
                }
                else
                {
                    imgRelation.Visible = false;
                    panelRelationInner.CssClass = "lower-text";
                }

                BindClientRelation(true);

                if (!HasActionRight)
                    btnEdit.Visible = false;
            }
            else
            {
                imgRelation.Visible = false;
                panelRelation.Visible = false;
            }
        }

        if(HasViewRight)
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "InitRelationControls(" + ((IsPopup) ? "4" : "0") + ");", true);
    }

    protected void CheckRights()
    {
        string sXmlOut = tools.GetRights(authentication.GetCurrent().sToken, "SC_Customer");

        List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Page", "RC");

        if (listRC.Count > 0 && listRC[0] == "0")
        {
            List<string> listAction = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/Page/Action");

            for (int i = 0; i < listAction.Count; i++)
            {
                List<string> listTag = CommonMethod.GetAttributeValues(listAction[i], "Action", "TagAction");
                List<string> listActionAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ActionAllowed");
                List<string> listViewAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ViewAllowed");

                if (listTag.Count > 0)
                {
                    switch (listTag[0])
                    {
                        case "ClientRelation":
                            if (listViewAllowed.Count > 0 && listViewAllowed[0] == "1")
                                HasViewRight = true;
                            if (listActionAllowed.Count > 0 && listActionAllowed[0] == "1")
                                HasActionRight = true;
                            break;
                    }
                }
            }
        }
    }

    protected void BindClientRelation(bool bRefresh)
    {
        DataTable dt = GetClientRelation(bRefresh);
        imgRelation.Attributes["title"] = ((dt.Rows.Count > 0) ? dt.Rows.Count.ToString() : "aucune") + " relation" + ((dt.Rows.Count > 1) ? "s" : "");

        if (dt.Rows.Count > 0)
        {
            if(!IsEditable)
                btnEdit.Visible = true;
            rptRelation.Visible = true;
            panelNoRelation.Visible = false;
            rptRelation.DataSource = dt;
            rptRelation.DataBind();
        }
        else
        {
            btnEdit.Visible = false;
            rptRelation.Visible = false;
            panelNoRelation.Visible = true;
        }
        upClientRelation.Update();
    }
    protected DataTable GetClientRelation(bool bRefresh)
    {
        DataTable dtRelationTypes = GetRelationTypes();

        DataTable dt = new DataTable();
        dt.Columns.Add("RefRelation");
        dt.Columns.Add("RefStatus");
        dt.Columns.Add("StatusLabel");
        dt.Columns.Add("RelationTypes", typeof(DataTable));
        dt.Columns.Add("RefRelationType");
        dt.Columns.Add("RelationTypeLabel");
        dt.Columns.Add("RefCustomer");
        dt.Columns.Add("BankAccountNumber");
        dt.Columns.Add("LastName");
        dt.Columns.Add("FirstName");
        dt.Columns.Add("Age");
        dt.Columns.Add("Sex");
        dt.Columns.Add("Profession");
        dt.Columns.Add("IsProAccount");
        dt.Columns.Add("IsSameName");
        dt.Columns.Add("IsSameAddress");
        dt.Columns.Add("TransferOutAmount");
        dt.Columns.Add("TransferOutCount");
        dt.Columns.Add("TransferProOut");
        dt.Columns.Add("TransferInAmount");
        dt.Columns.Add("TransferInCount");
        dt.Columns.Add("TransferProIn");

        SqlConnection conn = null;

        try
        {
            string sXmlOut = "";

            if (bRefresh || String.IsNullOrWhiteSpace(sClientRelations))
            {
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
                conn.Open();

                SqlCommand cmd = new SqlCommand("Web.P_GetCustomerRelations", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
                cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);

                cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN", new XElement("RELATIONS", new XAttribute("RefCustomer", RefCustomer.ToString()))).ToString();
                cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

                cmd.ExecuteNonQuery();

                sXmlOut = cmd.Parameters["@OUT"].Value.ToString();
            }
            else sXmlOut = sClientRelations;

            if (!String.IsNullOrWhiteSpace(sXmlOut))
            {
                sClientRelations = sXmlOut;

                List<string> listRelation = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/RELATIONS/RELATION");

                for (int i = 0; i < listRelation.Count; i++)
                {
                    List<string> listRefRelation = CommonMethod.GetAttributeValues(listRelation[i], "RELATION", "RefRelation");
                    List<string> listRefStatus = CommonMethod.GetAttributeValues(listRelation[i], "RELATION", "RefRelationStatus");
                    List<string> listStatus = CommonMethod.GetAttributeValues(listRelation[i], "RELATION", "Status");
                    List<string> listRefRelationType = CommonMethod.GetAttributeValues(listRelation[i], "RELATION", "RefRelationType");
                    List<string> listRelationTypeLabel = CommonMethod.GetAttributeValues(listRelation[i], "RELATION", "RelationTypeLabel");
                    List<string> listRefCustomer = CommonMethod.GetAttributeValues(listRelation[i], "RELATION", "RefCustomer");
                    List<string> listAccountNumber = CommonMethod.GetAttributeValues(listRelation[i], "RELATION", "BankAccountNumber");
                    List<string> listLastName = CommonMethod.GetAttributeValues(listRelation[i], "RELATION", "LastName");
                    List<string> listFirstName = CommonMethod.GetAttributeValues(listRelation[i], "RELATION", "FirstName");
                    List<string> listSex = CommonMethod.GetAttributeValues(listRelation[i], "RELATION", "Sex");
                    List<string> listProfession = CommonMethod.GetAttributeValues(listRelation[i], "RELATION", "Profession");
                    List<string> listAge = CommonMethod.GetAttributeValues(listRelation[i], "RELATION", "Age");
                    List<string> listSameName = CommonMethod.GetAttributeValues(listRelation[i], "RELATION", "SameNameRelated");
                    List<string> listSameAddress = CommonMethod.GetAttributeValues(listRelation[i], "RELATION", "SameAdressRelated");

                    // ALL TIME
                    List<string> listAmountToRelated = CommonMethod.GetAttributeValues(listRelation[i], "RELATION", "AmountToRelated");
                    List<string> listNumberToRelated = CommonMethod.GetAttributeValues(listRelation[i], "RELATION", "NumberToRelated");
                    List<string> listAmountFromRelated = CommonMethod.GetAttributeValues(listRelation[i], "RELATION", "AmountFromRelated");
                    List<string> listNumberFromRelated = CommonMethod.GetAttributeValues(listRelation[i], "RELATION", "NumberFromRelated");
                    List<string> listNumberProFromRelated = CommonMethod.GetAttributeValues(listRelation[i], "RELATION", "NumberProFromRelated");
                    List<string> listNumberProToRelated = CommonMethod.GetAttributeValues(listRelation[i], "RELATION", "NumberProToRelated");

                    // 1 MONTH
                    List<string> listAmountToRelated_1 = CommonMethod.GetAttributeValues(listRelation[i], "RELATION", "AmountToRelated_1");
                    List<string> listNumberToRelated_1 = CommonMethod.GetAttributeValues(listRelation[i], "RELATION", "NumberToRelated_1");
                    List<string> listAmountFromRelated_1 = CommonMethod.GetAttributeValues(listRelation[i], "RELATION", "AmountFromRelated_1");
                    List<string> listNumberFromRelated_1 = CommonMethod.GetAttributeValues(listRelation[i], "RELATION", "NumberFromRelated_1");
                    List<string> listNumberProFromRelated_1 = CommonMethod.GetAttributeValues(listRelation[i], "RELATION", "NumberProFromRelated_1");
                    List<string> listNumberProToRelated_1 = CommonMethod.GetAttributeValues(listRelation[i], "RELATION", "NumberProToRelated_1");

                    // 3 MONTH
                    List<string> listAmountToRelated_3 = CommonMethod.GetAttributeValues(listRelation[i], "RELATION", "AmountToRelated_3");
                    List<string> listNumberToRelated_3 = CommonMethod.GetAttributeValues(listRelation[i], "RELATION", "NumberToRelated_3");
                    List<string> listAmountFromRelated_3 = CommonMethod.GetAttributeValues(listRelation[i], "RELATION", "AmountFromRelated_3");
                    List<string> listNumberFromRelated_3 = CommonMethod.GetAttributeValues(listRelation[i], "RELATION", "NumberFromRelated_3");
                    List<string> listNumberProFromRelated_3 = CommonMethod.GetAttributeValues(listRelation[i], "RELATION", "NumberProFromRelated_3");
                    List<string> listNumberProToRelated_3 = CommonMethod.GetAttributeValues(listRelation[i], "RELATION", "NumberProToRelated_3");

                    // 6 MONTH
                    List<string> listAmountToRelated_6 = CommonMethod.GetAttributeValues(listRelation[i], "RELATION", "AmountToRelated_6");
                    List<string> listNumberToRelated_6 = CommonMethod.GetAttributeValues(listRelation[i], "RELATION", "NumberToRelated_6");
                    List<string> listAmountFromRelated_6 = CommonMethod.GetAttributeValues(listRelation[i], "RELATION", "AmountFromRelated_6");
                    List<string> listNumberFromRelated_6 = CommonMethod.GetAttributeValues(listRelation[i], "RELATION", "NumberFromRelated_6");
                    List<string> listNumberProFromRelated_6 = CommonMethod.GetAttributeValues(listRelation[i], "RELATION", "NumberProFromRelated_6");
                    List<string> listNumberProToRelated_6 = CommonMethod.GetAttributeValues(listRelation[i], "RELATION", "NumberProToRelated_6");

                    // 12 MONTH
                    List<string> listAmountToRelated_12 = CommonMethod.GetAttributeValues(listRelation[i], "RELATION", "AmountToRelated_12");
                    List<string> listNumberToRelated_12 = CommonMethod.GetAttributeValues(listRelation[i], "RELATION", "NumberToRelated_12");
                    List<string> listAmountFromRelated_12 = CommonMethod.GetAttributeValues(listRelation[i], "RELATION", "AmountFromRelated_12");
                    List<string> listNumberFromRelated_12 = CommonMethod.GetAttributeValues(listRelation[i], "RELATION", "NumberFromRelated_12");
                    List<string> listNumberProFromRelated_12 = CommonMethod.GetAttributeValues(listRelation[i], "RELATION", "NumberProFromRelated_12");
                    List<string> listNumberProToRelated_12 = CommonMethod.GetAttributeValues(listRelation[i], "RELATION", "NumberProToRelated_12");

                    DataRow row = dt.NewRow();
                    if (listRefCustomer.Count > 0)
                        row["RefCustomer"] = listRefCustomer[0];
                    if (listRefRelation.Count > 0)
                        row["RefRelation"] = listRefRelation[0];
                    if (listRefStatus.Count > 0)
                        row["RefStatus"] = listRefStatus[0];
                    if (listStatus.Count > 0)
                        row["StatusLabel"] = listStatus[0];
                    if (listRefRelationType.Count > 0)
                        row["RefRelationType"] = listRefRelationType[0];
                    if (listRelationTypeLabel.Count > 0)
                        row["RelationTypeLabel"] = listRelationTypeLabel[0];
                    if (listRefCustomer.Count > 0)
                        row["RefCustomer"] = listRefCustomer[0];
                    if (listAccountNumber.Count > 0)
                        row["BankAccountNumber"] = listAccountNumber[0];
                    if (listLastName.Count > 0)
                        row["LastName"] = listLastName[0];
                    if (listFirstName.Count > 0)
                        row["FirstName"] = listFirstName[0];
                    if (listSex.Count > 0)
                        row["Sex"] = listSex[0];
                    if (listAge.Count > 0)
                        row["Age"] = listAge[0];
                    if (listProfession.Count > 0)
                        row["Profession"] = listProfession[0];
                    if (listSameAddress.Count > 0)
                        row["IsSameAddress"] = listSameAddress[0];
                    if (listSameName.Count > 0)
                        row["IsSameName"] = listSameName[0];
                    row["RelationTypes"] = dtRelationTypes;

                    switch (rblPeriodFlow.SelectedValue)
                    {
                        case "-1":
                            if (listAmountToRelated.Count > 0)
                                row["TransferOutAmount"] = listAmountToRelated[0];
                            if (listNumberToRelated.Count > 0)
                                row["TransferOutCount"] = listNumberToRelated[0];
                            if (listAmountFromRelated.Count > 0)
                                row["TransferInAmount"] = listAmountFromRelated[0];
                            if (listNumberFromRelated.Count > 0)
                                row["TransferInCount"] = listNumberFromRelated[0];
                            if (listNumberProToRelated.Count > 0)
                                row["TransferProOut"] = listNumberProToRelated[0];
                            if (listNumberProFromRelated.Count > 0)
                                row["TransferProIn"] = listNumberProFromRelated[0];
                            break;
                        case "1":
                            if (listAmountToRelated_1.Count > 0)
                                row["TransferOutAmount"] = listAmountToRelated_1[0];
                            if (listNumberToRelated_1.Count > 0)
                                row["TransferOutCount"] = listNumberToRelated_1[0];
                            if (listAmountFromRelated_1.Count > 0)
                                row["TransferInAmount"] = listAmountFromRelated_1[0];
                            if (listNumberFromRelated_1.Count > 0)
                                row["TransferInCount"] = listNumberFromRelated_1[0];
                            if (listNumberProToRelated_1.Count > 0)
                                row["TransferProOut"] = listNumberProToRelated_1[0];
                            if (listNumberProFromRelated_1.Count > 0)
                                row["TransferProIn"] = listNumberProFromRelated_1[0];
                            break;
                        case "3":
                            if (listAmountToRelated_3.Count > 0)
                                row["TransferOutAmount"] = listAmountToRelated_3[0];
                            if (listNumberToRelated_3.Count > 0)
                                row["TransferOutCount"] = listNumberToRelated_3[0];
                            if (listAmountFromRelated_3.Count > 0)
                                row["TransferInAmount"] = listAmountFromRelated_3[0];
                            if (listNumberFromRelated_3.Count > 0)
                                row["TransferInCount"] = listNumberFromRelated_3[0];
                            if (listNumberProToRelated_3.Count > 0)
                                row["TransferProOut"] = listNumberProToRelated_3[0];
                            if (listNumberProFromRelated_3.Count > 0)
                                row["TransferProIn"] = listNumberProFromRelated_3[0];
                            break;
                        case "6":
                            if (listAmountToRelated_6.Count > 0)
                                row["TransferOutAmount"] = listAmountToRelated_6[0];
                            if (listNumberToRelated_6.Count > 0)
                                row["TransferOutCount"] = listNumberToRelated_6[0];
                            if (listAmountFromRelated_6.Count > 0)
                                row["TransferInAmount"] = listAmountFromRelated_6[0];
                            if (listNumberFromRelated_6.Count > 0)
                                row["TransferInCount"] = listNumberFromRelated_6[0];
                            if (listNumberProToRelated_6.Count > 0)
                                row["TransferProOut"] = listNumberProToRelated_6[0];
                            if (listNumberProFromRelated_6.Count > 0)
                                row["TransferProIn"] = listNumberProFromRelated_6[0];
                            break;
                        case "12":
                            if (listAmountToRelated_12.Count > 0)
                                row["TransferOutAmount"] = listAmountToRelated_12[0];
                            if (listNumberToRelated_12.Count > 0)
                                row["TransferOutCount"] = listNumberToRelated_12[0];
                            if (listAmountFromRelated_12.Count > 0)
                                row["TransferInAmount"] = listAmountFromRelated_12[0];
                            if (listNumberFromRelated_12.Count > 0)
                                row["TransferInCount"] = listNumberFromRelated_12[0];
                            if (listNumberProToRelated_12.Count > 0)
                                row["TransferProOut"] = listNumberProToRelated_12[0];
                            if (listNumberProFromRelated_12.Count > 0)
                                row["TransferProIn"] = listNumberProFromRelated_12[0];
                            break;
                    }

                    dt.Rows.Add(row);
                }
            }
        }
        catch (Exception e)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }

    protected DataTable GetRelationTypes()
    {
        SqlConnection conn = null;
        DataTable dt = new DataTable();
        dt.Columns.Add("RefRelationType");
        dt.Columns.Add("RelationTypeLabel");

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_GetRelationType", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@ALL_XML_IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@ALL_XML_OUT", SqlDbType.VarChar, 8000);

            cmd.Parameters["@ALL_XML_IN"].Value = "<ALL_XML_IN><RELATIONS/></ALL_XML_IN>";
            cmd.Parameters["@ALL_XML_OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@ALL_XML_OUT"].Value.ToString();

            if (!String.IsNullOrWhiteSpace(sXmlOut))
            {
                List<string> listRelationType = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/RELATION_TYPES/RELATION_TYPE");

                for (int i = 0; i < listRelationType.Count; i++)
                {
                    List<string> listRef = CommonMethod.GetAttributeValues(listRelationType[i], "RELATION_TYPE", "RefRelationType");
                    List<string> listLabel = CommonMethod.GetAttributeValues(listRelationType[i], "RELATION_TYPE", "RelationTypeLabel");

                    dt.Rows.Add(new object[] { listRef[0], listLabel[0] });
                }
            }
        }
        catch(Exception e)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }

    protected void rptRelation_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item ||
       e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hdn = (HiddenField)e.Item.FindControl("hdnRefRelationType");
            DropDownList ddl = (DropDownList)e.Item.FindControl("ddlRelationTypes");

            if (hdn != null && ddl != null)
            {
                ListItem item = ddl.Items.FindByValue(hdn.Value);
                if (item != null)
                {
                    item.Selected = true;
                }
            }
        }
    }

    protected bool AddDelSetRelation(string sXmlIn)
    {
        bool bOk = false;
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_AddDelSetRelations", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@RC", SqlDbType.Int);

            cmd.Parameters["@IN"].Value = sXmlIn;
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
            cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;

            cmd.ExecuteNonQuery();

            if (cmd.Parameters["@RC"].Value.ToString() == "0")
                bOk = true;
        }
        catch(Exception e)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return bOk;
    }

    protected void rblPeriodFlow_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindClientRelation(false);
    }

    protected void btnEdit_Click(object sender, EventArgs e)
    {
        btnEdit.Visible = false;
        btnValidate.Visible = true;
        btnCancel.Visible = true;

        IsEditable = true;
        BindClientRelation(true);
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        btnEdit.Visible = true;
        btnValidate.Visible = false;
        btnCancel.Visible = false;

        IsEditable = false;
        BindClientRelation(true);
    }

    protected void btnValidate_Click(object sender, EventArgs e)
    {
        btnEdit.Visible = true;
        btnValidate.Visible = false;
        btnCancel.Visible = false;

        XElement xml = new XElement("RELATIONS", new XAttribute("RefCustomer", RefCustomer));

        foreach (RepeaterItem item in rptRelation.Items)
        {
            if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
            {
                HiddenField hdn = (HiddenField)item.FindControl("hdnRefCustomer");
                DropDownList ddl = (DropDownList)item.FindControl("ddlRelationTypes");
                CheckBox ckb = (CheckBox)item.FindControl("ckbCheck");

                if (hdn != null && ddl != null && ckb != null)
                {
                    xml.Add(
                        new XElement("RELATION",
                            new XAttribute("Action", "U"),
                            new XAttribute("RefRelationType", ddl.SelectedValue),
                            new XAttribute("RefRelationStatus", ckb.Checked ? "1" : "0"),
                            new XAttribute("RefCustomer", hdn.Value)
                            ));
                }
            }
        }

        if (!AddDelSetRelation(new XElement("ALL_XML_IN", xml).ToString()))
            AlertMessage("Erreur", "L'enregistrement a échoué. Veuillez réessayer.");

        IsEditable = false;
        BindClientRelation(true);
    }

    protected void AlertMessage(string sTitle, string sMessage)
    {
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('" + sTitle.Replace("'", "&#39;") + "', '" + sMessage.Replace("'", "&#39;") + "');", true);
    }
}