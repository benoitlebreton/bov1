﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using XMLMethodLibrary;

public partial class API_StatsPRO : System.Web.UI.UserControl
{
    public string AccountNumber { get { return (ViewState["AccountNumberPRO"] != null) ? ViewState["AccountNumberPRO"].ToString() : ""; } set { ViewState["AccountNumberPRO"] = value; } }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            RefreshDashboard();
        }

        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "InitPostBackKey", "InitPostBack();", true);
    }

    public void RefreshDashboard()
    {
        if (AccountNumber.Length > 0)
        {
            BindStatistics(GetStatistics(AccountNumber));
        }
    }

    protected string GetStatistics(string sAccountNumber)
    {
        string sXmlOut = "";
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("WEB.P_Statistics", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@ReturnSelect", SqlDbType.Bit);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN", new XElement("Stats", new XAttribute("Kind", "SHOPDASHBOARD"), new XAttribute("AccountNumber", sAccountNumber))).ToString();
            cmd.Parameters["@ReturnSelect"].Value = 0;
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            sXmlOut = cmd.Parameters["@OUT"].Value.ToString();
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return sXmlOut;
    }
    protected void BindStatistics(string sXmlOut)
    {
        if (sXmlOut.Length > 0)
        {
            List<string> listNbTotalAccountYesterday = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Stats", "NbSubscriptionsYesterday");
            List<string> listNbAgenciesActiveYesterday = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Stats", "NbShopActiveYesterday");
            List<string> listNbClient = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Stats", "NbSubscriptions");
            List<string> listNbAccountYesterday = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Stats", "NbSubscriptionsYesterdayForThisShop");
            List<string> listNbAccountWeek = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Stats", "NbSubscriptionsWeekForThisShop");

            int iTmp = (listNbTotalAccountYesterday.Count > 0 && listNbTotalAccountYesterday[0].Length > 0) ? int.Parse(listNbTotalAccountYesterday[0]) : 0;
            lblTotalNbAccountYesterday.Text = iTmp.ToString();
            if (iTmp > 1)
                lblTotalNbAccountYesterdayAccord.Text = "s";
            iTmp = (listNbAgenciesActiveYesterday.Count > 0 && listNbAgenciesActiveYesterday[0].Length > 0) ? int.Parse(listNbAgenciesActiveYesterday[0]) : 0;
            lblNbAgenciesActiveYesterday.Text = iTmp.ToString();
            if (iTmp > 1)
            {
                lblNbAgenciesActiveYesterdayAccord1.Text = "s";
                lblNbAgenciesActiveYesterdayAccord2.Text = "ont";
            }
            else lblNbAgenciesActiveYesterdayAccord2.Text = "a";
            iTmp = (listNbClient.Count > 0 && listNbClient[0].Length > 0) ? int.Parse(listNbClient[0]) : 0;
            lblNbClient.Text = iTmp.ToString();
            if (iTmp > 1)
                lblNbClientAccord.Text = "s";
            iTmp = (listNbAccountYesterday.Count > 0 && listNbAccountYesterday[0].Length > 0) ? int.Parse(listNbAccountYesterday[0]) : 0;
            lblNbAccountYesterday.Text = iTmp.ToString();
            if (iTmp > 1)
            {
                lblNbAccountYesterdayAccord1.Text = "s";
                lblNbAccountYesterdayAccord2.Text = "s";
            }
            iTmp = (listNbAccountWeek.Count > 0 && listNbAccountWeek[0].Length > 0) ? int.Parse(listNbAccountWeek[0]) : 0;
            lblNbAccountWeek.Text = iTmp.ToString();
            if (iTmp > 1)
            {
                lblNbAccountWeekAccord1.Text = "s";
                lblNbAccountWeekAccord2.Text = "s";
            }

            DataTable dt = new DataTable();
            dt.Columns.Add("Index");
            dt.Columns.Add("Place");
            dt.Columns.Add("ShopName");
            dt.Columns.Add("City");
            dt.Columns.Add("NbAccount");

            List<string> listTopShops = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/Stats/TOPSHOP/SHOP");
            int iPlace = 1;
            int iLastNbSub = 0;
            for (int i = 0; i < listTopShops.Count; i++)
            {
                List<string> listAgencyName = CommonMethod.GetAttributeValues(listTopShops[i], "SHOP", "AgencyName");
                List<string> listCity = CommonMethod.GetAttributeValues(listTopShops[i], "SHOP", "City");
                List<string> listNbSubscription = CommonMethod.GetAttributeValues(listTopShops[i], "SHOP", "NbSubscription");

                int iNbSubscription = (listNbSubscription.Count > 0) ? int.Parse(listNbSubscription[0]) : 0;
                string sNbSubscription = iNbSubscription.ToString() + " " + ((iNbSubscription > 1) ? "comptes ouverts" : "compte ouvert");

                if (i == 0)
                    iLastNbSub = iNbSubscription;
                else
                {
                    if (iNbSubscription < iLastNbSub)
                    {
                        iPlace = i + 1;
                        iLastNbSub = iNbSubscription;
                    }
                }

                dt.Rows.Add(new object[] { i + 1, iPlace, (listAgencyName.Count > 0) ? listAgencyName[0] : "", (listCity.Count > 0) ? listCity[0] : "", sNbSubscription });
            }
            rptTopShopYesterday.DataSource = dt;
            rptTopShopYesterday.DataBind();

            List<string> listSameShopLabel = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Stats/TopSameShops", "Libel");
            lblTopSameShop.Text = (listSameShopLabel.Count > 0) ? listSameShopLabel[0] : "";
            List<string> listSameShopDate = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Stats/TopSameShops", "ReferenceDate");
            lblDateTop1.Text = (listSameShopDate.Count > 0) ? listSameShopDate[0] : "";
            lblDateTop2.Text = (listSameShopDate.Count > 0) ? listSameShopDate[0] : "";
            dt.Rows.Clear();
            List<string> listTopSameShop = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/Stats/TopSameShops/SHOP");
            for (int i = 0; i < listTopSameShop.Count; i++)
            {
                List<string> listAgencyName = CommonMethod.GetAttributeValues(listTopSameShop[i], "SHOP", "AgencyName");
                List<string> listCity = CommonMethod.GetAttributeValues(listTopSameShop[i], "SHOP", "City");
                List<string> listNbSubscription = CommonMethod.GetAttributeValues(listTopSameShop[i], "SHOP", "NbSubscription");

                int iNbSubscription = (listNbSubscription.Count > 0 && listNbSubscription[0].Length > 0) ? int.Parse(listNbSubscription[0]) : 0;
                string sNbSubscription = iNbSubscription.ToString() + " " + ((iNbSubscription > 1) ? "comptes" : "compte");

                dt.Rows.Add(new object[] { i + 1, "", (listAgencyName.Count > 0) ? listAgencyName[0] : "", (listCity.Count > 0) ? listCity[0] : "", sNbSubscription });
            }
            rptTopSameShop.DataSource = dt;
            rptTopSameShop.DataBind();

            dt.Rows.Clear();
            List<string> listNewShop = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/Stats/NEWSHOP/SHOP");
            for (int i = 0; i < listNewShop.Count; i++)
            {
                List<string> listAgencyName = CommonMethod.GetAttributeValues(listNewShop[i], "SHOP", "AgencyName");
                List<string> listCity = CommonMethod.GetAttributeValues(listNewShop[i], "SHOP", "City");

                dt.Rows.Add(new object[] { i + 1, "", (listAgencyName.Count > 0) ? listAgencyName[0] : "", (listCity.Count > 0) ? listCity[0] : "" });
            }
            rptNewShops.DataSource = dt;
            rptNewShops.DataBind();
        }
    }
}