﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using XMLMethodLibrary;

public partial class API_SearchTracfinStatement : System.Web.UI.UserControl
{
    public int RefCustomer {
        get { return (ViewState["TracfinStatementRefCustomer"] != null) ? int.Parse(ViewState["TracfinStatementRefCustomer"].ToString()) : 0; }
        set { ViewState["TracfinStatementRefCustomer"] = value; }
    }
    public bool ShowSubtitle
    {
        get;set;
    }
    public bool HideFilters
    {
        get; set;
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        clientSearch1.ClientFound += new EventHandler(ClientFound);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            if(RefCustomer != 0)
                BindTracfinStatement();

            panelTitle.Visible = ShowSubtitle;

            panelFilters.Visible = !HideFilters;
        }
    }

    protected void BindTracfinStatement()
    {
        rptTracfinStatement.DataSource = ParseInfos();
        rptTracfinStatement.DataBind();
    }

    protected DataTable ParseInfos()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("Ref");
        dt.Columns.Add("RefStatementInt");
        dt.Columns.Add("RefStatementExt");
        dt.Columns.Add("Date");
        dt.Columns.Add("Reason");
        string sStatementDetails = Tracfin.SearchStatement(RefCustomer, 0, authentication.GetCurrent().sToken, txtReference.Text, txtDateFrom.Text, txtDateTo.Text);

        List<string> listRC = CommonMethod.GetAttributeValues(sStatementDetails, "ALL_XML_OUT/TRACFIN_STATEMENT", "RC");
        List<string> listErrorMessage = CommonMethod.GetAttributeValues(sStatementDetails, "ALL_XML_OUT/TRACFIN_STATEMENT", "ErrorMessage");

        //if (listRC.Count > 0 && listRC[0] == "0")
        {
            List<string> listStatement = CommonMethod.GetTags(sStatementDetails, "ALL_XML_OUT/TRACFIN_STATEMENT");

            for (int i = 0; i < listStatement.Count; i++)
            {
                //List<string> listCreationDate = CommonMethod.GetAttributeValues(listStatement[i], "TRACFIN_STATEMENT", "CreationDate");
                List<string> listRef = CommonMethod.GetAttributeValues(listStatement[i], "TRACFIN_STATEMENT", "RefStatement");
                List<string> listRefStatementExt = CommonMethod.GetAttributeValues(listStatement[i], "TRACFIN_STATEMENT", "RefStatementExt");
                List<string> listRefStatementInt = CommonMethod.GetAttributeValues(listStatement[i], "TRACFIN_STATEMENT", "RefStatementInt");
                List<string> listCompletionDate = CommonMethod.GetAttributeValues(listStatement[i], "TRACFIN_STATEMENT", "TracfinDeclarationDate");

                StringBuilder sbReasons = new StringBuilder();
                List<string> listReasons = CommonMethod.GetTags(listStatement[i], "TRACFIN_STATEMENT/REASONS/REASON");
                for (int j = 0; j < listReasons.Count; j++)
                {
                    List<string> listArticle = CommonMethod.GetAttributeValues(listReasons[j], "REASON", "RefArticle");
                    List<string> listLabel = CommonMethod.GetAttributeValues(listReasons[j], "REASON", "StmtReasonLabel");

                    if (j != 0)
                        sbReasons.Append(", ");
                    sbReasons.Append(listLabel[0] + " (" + listArticle[0] + ")");
                }

                dt.Rows.Add(new object[] { listRef[0], listRefStatementInt[0], listRefStatementExt[0], listCompletionDate[0], sbReasons.ToString() });

                //List<string> listCustomers = CommonMethod.GetTags(listStatement[i], "TRACFIN_STATEMENT/CUSTOMERS/CUSTOMER");
                //List<string> listActions = CommonMethod.GetTags(listStatement[i], "TRACFIN_STATEMENT/ACTIONS/ACTION");
            }

        }

        return dt;
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        clientSearch1.btnClientSearch_Click(sender, e);
        RefCustomer = clientSearch1.RefCustomer;

        BindTracfinStatement();
    }

    protected void btnGotoStatement_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("URL");
        dt.Columns.Add("ButtonText");

        string sURL = "SearchTracfinStatement.aspx";
        string sText = "Recherche déclaration";
        if (Request.Url.ToString().Contains("ClientDetails"))
        {
            sURL = "ClientDetails.aspx";
        }

        dt.Rows.Add(new object[] { sURL, sText });
        Session["BackUrl"] = dt;

        Response.Redirect("TracfinStatement.aspx?ref=" + hdnRefStatement.Value);
    }

    protected void ClientFound(object sender, EventArgs e)
    {
        RefCustomer = clientSearch1.RefCustomer;
        BindTracfinStatement();
    }
}