﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using XMLMethodLibrary;

public partial class API_DeathStatus : System.Web.UI.UserControl
{
    public int iRefCustomer
    {
        get
        {
            int _iRefCustomer = 0;
            int.TryParse((ViewState["iRefCustomer"] != null) ? ViewState["iRefCustomer"].ToString() : "0", out _iRefCustomer);
            return _iRefCustomer;
        }
        set { ViewState["iRefCustomer"] = value; }
    }
    public string DeathDate { get; set; }
    public string DeathPlace { get; set; }
    public string DeathActNumber { get; set; }
    public string XmlCustomerInfos
    {
        get { return (Session["XmlCustomerInfos"] != null) ? Session["XmlCustomerInfos"].ToString() : ""; }
        set
        {
            Session["XmlCustomerInfos"] = value;
            BindVariables();
        }
    }

    private void BindVariables()
    {
        string sDeathDate, sDeathPlace, sDeathActNumber;
        ParseXmlCustomerInfos(XmlCustomerInfos, out sDeathDate, out sDeathPlace, out sDeathActNumber);
        DeathActNumber = sDeathActNumber;
        DeathDate = sDeathDate;
        DeathPlace = sDeathPlace;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            CheckRights();
            BindDeathStatus();
        }

        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "InitSuccessionDatePicker();", true);
    }

    protected void CheckRights()
    {
        imgFalseAlarm.Visible = false;
        btnStartSuccession.Visible = false;
        btnSaveSuccessionInfos.Visible = false;

        string sXmlOut = tools.GetRights(authentication.GetCurrent().sToken, "SC_Customer");

        List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Page", "RC");

        if (listRC.Count > 0 && listRC[0] == "0")
        {
            List<string> listAction = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/Page/Action");

            for (int i = 0; i < listAction.Count; i++)
            {
                List<string> listTag = CommonMethod.GetAttributeValues(listAction[i], "Action", "TagAction");
                List<string> listActionAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ActionAllowed");
                List<string> listViewAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ViewAllowed");

                if (listTag.Count > 0)
                {
                    switch (listTag[0])
                    {
                        case "DeathFalseAlarm":
                            if (listActionAllowed.Count > 0 && listActionAllowed[0] == "1")
                                imgFalseAlarm.Visible = true;
                            break;
                        case "DeathSuccession":
                            if (listActionAllowed.Count > 0 && listActionAllowed[0] == "1")
                            {
                                btnStartSuccession.Visible = true;
                                btnSaveSuccessionInfos.Visible = true;
                            }
                            break;
                    }
                }
            }
        }
    }

    protected void BindDeathStatus()
    {
        if (String.IsNullOrWhiteSpace(DeathDate))
            BindVariables();

        if (!String.IsNullOrWhiteSpace(DeathDate))
        {
            DateTime date;
            if (DateTime.TryParseExact(DeathDate, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out date)
                //&& DateTime.Compare(date, DateTime.Now.AddYears(-1)) < 0
                )
            {
                litDeathDate.Text = date.ToString("dd/MM/yyyy");
            }
            else litDeathDate.Text = "error";
        }
        else litDeathDate.Text = "N.C.";
        if (!String.IsNullOrWhiteSpace(DeathActNumber))
            litDeathActNumber.Text = DeathActNumber;
        if (!String.IsNullOrWhiteSpace(DeathPlace))
            litDeathPlace.Text = DeathPlace;

    }

    protected void ParseXmlCustomerInfos(string sXmlCustomerInfos, out string sDeathDate, out string sDeathPlace, out string sDeathActNumber)
    {
        List<string> listDeathDate = CommonMethod.GetAttributeValues(sXmlCustomerInfos, "ALL_XML_OUT/Customer/ECKERT/INFO", "DeathDate");
        sDeathDate = (listDeathDate.Count > 0 && !String.IsNullOrWhiteSpace(listDeathDate[0])) ? listDeathDate[0] : "";
        List<string> listDeathPlace = CommonMethod.GetAttributeValues(sXmlCustomerInfos, "ALL_XML_OUT/Customer/ECKERT/INFO", "DeathCity");
        sDeathPlace = (listDeathPlace.Count > 0 && !String.IsNullOrWhiteSpace(listDeathPlace[0])) ? listDeathPlace[0] : "N.C.";
        List<string> listDeathActNumber = CommonMethod.GetAttributeValues(sXmlCustomerInfos, "ALL_XML_OUT/Customer/ECKERT/INFO", "DeathActNumber");
        sDeathActNumber = (listDeathActNumber.Count > 0 && !String.IsNullOrWhiteSpace(listDeathActNumber[0])) ? listDeathActNumber[0] : "N.C.";

        List<string> listLastFalseAlarm = CommonMethod.GetAttributeValues(sXmlCustomerInfos, "ALL_XML_OUT/Customer/ECKERT/INFO", "LastFalseAlarm");
        List<string> listDeathDetectionDate = CommonMethod.GetAttributeValues(sXmlCustomerInfos, "ALL_XML_OUT/Customer/ECKERT/INFO", "DeathDetectionDate");
        DateTime dtDetection, dtFalseAlarm;

        List<string> listIsDead = CommonMethod.GetAttributeValues(sXmlCustomerInfos, "ALL_XML_OUT/Customer/ECKERT/INFO", "IsDead");
        if (listIsDead.Count == 0 || ((listIsDead[0].Length == 0 || listIsDead[0] == "0") && string.IsNullOrWhiteSpace(sDeathDate)))
            panelDeathStatus.Visible = false;
        else if (listIsDead.Count > 0 && listIsDead[0] == "1")
            lblSubLabel.Text = "confirmé";
        else
        {
            lblSubLabel.Text = "détecté";
            panelStartSuccession.Visible = false;
        }

        if (listLastFalseAlarm.Count > 0 && !String.IsNullOrWhiteSpace(listLastFalseAlarm[0])
            && listDeathDetectionDate.Count > 0 && !String.IsNullOrWhiteSpace(listDeathDetectionDate[0])
            && DateTime.TryParseExact(listLastFalseAlarm[0], "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out dtFalseAlarm)
            && DateTime.TryParseExact(listDeathDetectionDate[0], "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out dtDetection)
            && DateTime.Compare(dtFalseAlarm, dtDetection) >= 0)
        {
            panelDeathStatus.Visible = false;
        }

        List<string> listIsSuccession = CommonMethod.GetAttributeValues(sXmlCustomerInfos, "ALL_XML_OUT/Customer/ECKERT/INFO", "IsSuccession");
        if (listIsSuccession.Count > 0 && listIsSuccession[0] == "1")
        {
            lblLabel.Text = "Décédé";
            lblSubLabel.Text = "Succession";
            panelSuccession.Visible = true;
            panelStartSuccession.Visible = false;

            List<string> listSuccessionDate = CommonMethod.GetAttributeValues(sXmlCustomerInfos, "ALL_XML_OUT/Customer/ECKERT/INFO", "SuccessionDate");
            txtSuccessionDate.Text = (listSuccessionDate.Count > 0) ? listSuccessionDate[0] : "";
            List<string> listBeneficiaryFirstName = CommonMethod.GetAttributeValues(sXmlCustomerInfos, "ALL_XML_OUT/Customer/ECKERT/INFO", "BeneficiaryFirstName");
            txtBeneficiaryFirstName.Text = (listBeneficiaryFirstName.Count > 0) ? listBeneficiaryFirstName[0] : "";
            List<string> listBeneficiaryLastName = CommonMethod.GetAttributeValues(sXmlCustomerInfos, "ALL_XML_OUT/Customer/ECKERT/INFO", "BeneficiaryLastName");
            txtBeneficiaryLastName.Text = (listBeneficiaryLastName.Count > 0) ? listBeneficiaryLastName[0] : "";
            List<string> listBeneficiaryEmail = CommonMethod.GetAttributeValues(sXmlCustomerInfos, "ALL_XML_OUT/Customer/ECKERT/INFO", "BeneficiaryEmail");
            txtBeneficiaryEmail.Text = (listBeneficiaryEmail.Count > 0) ? listBeneficiaryEmail[0] : "";
            List<string> listBeneficiaryPhoneNumber = CommonMethod.GetAttributeValues(sXmlCustomerInfos, "ALL_XML_OUT/Customer/ECKERT/INFO", "BeneficiaryPhoneNumber");
            txtBeneficiaryPhoneNumber.Text = (listBeneficiaryPhoneNumber.Count > 0) ? listBeneficiaryPhoneNumber[0] : "";
            List<string> listBeneficiaryAddress = CommonMethod.GetAttributeValues(sXmlCustomerInfos, "ALL_XML_OUT/Customer/ECKERT/INFO", "BeneficiaryAddress");
            txtBeneficiaryAddress.Text = (listBeneficiaryAddress.Count > 0) ? listBeneficiaryAddress[0] : "";
            List<string> listBeneficiaryZipCode = CommonMethod.GetAttributeValues(sXmlCustomerInfos, "ALL_XML_OUT/Customer/ECKERT/INFO", "BeneficiaryZipCode");
            txtBeneficiaryZipCode.Text = (listBeneficiaryZipCode.Count > 0) ? listBeneficiaryZipCode[0] : "";
            List<string> listBeneficiaryCity = CommonMethod.GetAttributeValues(sXmlCustomerInfos, "ALL_XML_OUT/Customer/ECKERT/INFO", "BeneficiaryCity");
            txtBeneficiaryCity.Text = (listBeneficiaryCity.Count > 0) ? listBeneficiaryCity[0] : "";
            List<string> listNotaryFirstName = CommonMethod.GetAttributeValues(sXmlCustomerInfos, "ALL_XML_OUT/Customer/ECKERT/INFO", "NotaryFirstName");
            txtNotaryFirstName.Text = (listNotaryFirstName.Count > 0) ? listNotaryFirstName[0] : "";
            List<string> listNotaryLastName = CommonMethod.GetAttributeValues(sXmlCustomerInfos, "ALL_XML_OUT/Customer/ECKERT/INFO", "NotaryLastName");
            txtNotaryLastName.Text = (listNotaryLastName.Count > 0) ? listNotaryLastName[0] : "";
            List<string> listNotaryEmail = CommonMethod.GetAttributeValues(sXmlCustomerInfos, "ALL_XML_OUT/Customer/ECKERT/INFO", "NotaryEmail");
            txtNotaryEmail.Text = (listNotaryEmail.Count > 0) ? listNotaryEmail[0] : "";
            List<string> listNotaryPhoneNumber = CommonMethod.GetAttributeValues(sXmlCustomerInfos, "ALL_XML_OUT/Customer/ECKERT/INFO", "NotaryPhoneNumber");
            txtNotaryPhoneNumber.Text = (listNotaryPhoneNumber.Count > 0) ? listNotaryPhoneNumber[0] : "";
            List<string> listNotaryAddress = CommonMethod.GetAttributeValues(sXmlCustomerInfos, "ALL_XML_OUT/Customer/ECKERT/INFO", "NotaryAddress");
            txtNotaryAddress.Text = (listNotaryAddress.Count > 0) ? listNotaryAddress[0] : "";
            List<string> listNotaryZipCode = CommonMethod.GetAttributeValues(sXmlCustomerInfos, "ALL_XML_OUT/Customer/ECKERT/INFO", "NotaryZipCode");
            txtNotaryZipCode.Text = (listNotaryZipCode.Count > 0) ? listNotaryZipCode[0] : "";
            List<string> listNotaryCity = CommonMethod.GetAttributeValues(sXmlCustomerInfos, "ALL_XML_OUT/Customer/ECKERT/INFO", "NotaryCity");
            txtNotaryCity.Text = (listNotaryCity.Count > 0) ? listNotaryCity[0] : "";
        }
    }

    protected void btnFalseAlarm_Click(object sender, EventArgs e)
    {
        bool bError = false;
        DataTable dt = AML.getClientSpecifities(iRefCustomer, false);

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            switch(dt.Rows[i]["RefSpecificity"].ToString())
            {
                case "54": // DECEDE
                case "74": // SUSPICION DECES
                case "78": // NON DECEDE
                    bError = !AML.delClientSpecificity(authentication.GetCurrent().sToken, iRefCustomer, dt.Rows[i]["RefSpecificity"].ToString());
                    break;
            }
        }

        bError = !AML.addClientSpecificity(authentication.GetCurrent().sToken, iRefCustomer, "78");

        if (bError)
            AlertMessage("Erreur", "Une erreur est survenue.");
        else
        {
            panelDeathStatus.Visible = false;
            upDeathStatus.Update();
        }
    }

    protected void AlertMessage(string sTitle, string sMessage)
    {
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "ShowDialogMessage(\"" + sTitle + "\", \"" + sMessage + "\");", true);
    }

    protected void btnSaveSuccessionInfos_Click(object sender, EventArgs e)
    {
        XElement xml = new XElement("ALL_XML_IN",
                            new XElement("SUCCESSION_INFOS",
                                new XAttribute("CashierToken", authentication.GetCurrent().sToken),
                                new XAttribute("RefCustomer", iRefCustomer),
                                //new XAttribute("DeathDate", ""),
                                //new XAttribute("DeathActNumber", ""),
                                //new XAttribute("DeathPlace", ""),
                                new XAttribute("SuccessionDate", txtSuccessionDate.Text),
                                new XAttribute("BeneficiaryFirstName", txtBeneficiaryFirstName.Text),
                                new XAttribute("BeneficiaryLastName", txtBeneficiaryLastName.Text),
                                new XAttribute("BeneficiaryEmail", txtBeneficiaryEmail.Text),
                                new XAttribute("BeneficiaryPhoneNumber", txtBeneficiaryPhoneNumber.Text),
                                new XAttribute("BeneficiaryAddress", txtBeneficiaryAddress.Text),
                                new XAttribute("BeneficiaryZipCode",txtBeneficiaryZipCode.Text),
                                new XAttribute("BeneficiaryCity", txtBeneficiaryCity.Text),
                                new XAttribute("NotaryFirstName", txtNotaryFirstName.Text),
                                new XAttribute("NotaryLastName", txtNotaryLastName.Text),
                                new XAttribute("NotaryEmail", txtNotaryEmail.Text),
                                new XAttribute("NotaryPhoneNumber", txtNotaryPhoneNumber.Text),
                                new XAttribute("NotaryAddress", txtNotaryAddress.Text),
                                new XAttribute("NotaryZipCode", txtNotaryZipCode.Text),
                                new XAttribute("NotaryCity", txtNotaryCity.Text)
                            )
                        );

        if (!SaveSuccessionInfos(xml.ToString()))
            AlertMessage("Erreur", "Une erreur est survenue lors de l'enregistrement.");
    }
    protected bool SaveSuccessionInfos(string sXmlIn)
    {
        bool bOk = false;
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("BankSI.P_SetSuccessionInfos", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);

            cmd.Parameters["@IN"].Value = sXmlIn;
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if(!String.IsNullOrWhiteSpace(sXmlOut))
            {
                List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Result", "RC");
                if (listRC.Count > 0 && listRC[0] == "0")
                    bOk = true;
            }
        }
        catch (Exception e)
        {
            
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return bOk;
    }

    protected void btnStartSuccession_Click(object sender, EventArgs e)
    {
        if (!AML.addClientSpecificity(authentication.GetCurrent().sToken, iRefCustomer, "77"))
            AlertMessage("Erreur", "Une erreur est survenue lors de l'ajout de la spécificité.");
        else
        {
            lblLabel.Text = "Décédé";
            lblSubLabel.Text = "Succession";
            panelStartSuccession.Visible = false;
            panelSuccession.Visible = true;
            upDeathStatus.Update();
        }
    }
}