﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LightLock.ascx.cs" Inherits="API_LightLock" %>

<div id="divClientDebitStatus">
<asp:UpdatePanel ID="upClientDebitStatusImg" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div style="display:inline-block;width:66px;text-align:center">
            <asp:Image ID="imgClientDebitStatus" runat="server" CssClass="ImageClickable image-tooltip" ImageUrl="~/Styles/Img/no-billets.png" style="height:40px;max-width:66px" onclick="showDebitStatusDetails();" />
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
<div id="divDebitStatus-popup" style="display:none">
    <div class="lightlock-panel-loading panel-loading"></div>
    <asp:UpdatePanel ID="upClientDebitStatusContent" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="table">
                <div class="table-row">
                    <div class="table-cell">
                        Statut :
                    </div>
                    <div class="table-cell" style="padding-left:10px;">
                        <asp:Label ID="lblClientDebitStatus" runat="server" style="text-transform:uppercase" CssClass="bold"></asp:Label>
                        <asp:HiddenField ID="hfClientDebitStatus" runat="server" />
                    </div>
                </div>
            </div>
            <asp:Panel ID="panelClientDebitAddLock" runat="server" CssClass="table" style="margin-top:5px;width:100%;">
                <div class="row">
                    <div class="cell">
                        <asp:DropDownList ID="ddlClientDebitLockReason" runat="server" AppendDataBoundItems="true"
                            DataTextField="LockReason" DataValueField="RefLockReason" onchange="clientDebitRefReasonChange(this);" style="width:96%">
                        </asp:DropDownList>
                        <span id="ddlClientDebitLockReason_req" style="visibility:hidden" class="reqStarStyle">*</span>
                        <input type="hidden" id="hfClientDebitLockRefReason_selected" />
                    </div>
                    <div class="cell" style="padding-left:10px;width:1px;">
                        <asp:Button ID="btnClientDebitAddLock" runat="server" CssClass="button" Text="Ajout blocage" OnClientClick="return checkClientDebitAddLock();" OnClick="btnClientDebitAddLock_Click" />
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="panelClientDebitActivatedLock" runat="server">
                <div style="margin-top:10px; max-height:300px; overflow-y:auto;border:1px solid #344b56; border:1px 0 1px 0; padding:2px 0">
                    <asp:Repeater ID="rptClientDebitActivatedLockList" runat="server">
                        <HeaderTemplate>
                            <table class="defaultTable" style="width:100%">
                                <tr>
                                    <th style="width:200px">Raison</th>
                                    <th style="width:400px">Blocage</th>
                                    <th style="width:30px;">
                                        <asp:CheckBox ID="cbAllClientDebitLockReason" runat="server" Visible="false" onclick="selectAllClientDebitLockReason();" CssClass="cbAllClientDebitLockReason"></asp:CheckBox>
                                    </th>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                                <tr class="RedLabel bold">
                                    <td><%# Eval("Reason") %></td>
                                    <td>
                                        <div class="trimmer" style="padding:0;width:400px;" >
                                            <asp:Label CssClass="tooltip" ID="lblClientDebitLockDetails" runat="server"
                                                ToolTip='<%# GetClientDebitLockUnlockDetails(Eval("LockDate").ToString(), Eval("LockBOUser").ToString(), true) %>'>
                                                <%# GetClientDebitLockUnlockDetails(Eval("LockDate").ToString(), Eval("LockBOUser").ToString(), true) %>
                                            </asp:Label>
                                        </div>
                                    </td>
                                    <td style="text-align:center">
                                        <asp:CheckBox ID="cbClientDebitLockReason" runat="server" CssClass="cbClientDebitLockReason" Visible='<%# GetBoolFromClientDebitLockUnlockStatus(Eval("Active").ToString()) %>' />
                                        <asp:HiddenField ID="hfClientDebitLockRefReason" runat="server" Value='<%# Eval("RefReason") %>' />
                                    </td>
                                </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
                <div style="width:100%; margin-top:5px; text-align:right">
                    <asp:Button ID="btnClientDebitUnlock" runat="server" CssClass="button" Text="Débloquer" OnClick="btnClientDebitUnlock_Click" class="notVisible" />
                </div>
            </asp:Panel>
            <asp:Panel ID="panelClientDebitLockUnlockListHistory" runat="server">
                <a id="linkClientDebitLockUnlockHistory_DisplayManagement" class="orangeLink" onclick="ClientDebitLockUnlockHistory_DisplayManagement();">Afficher historique</a>
                    <div id="divClientDebitLockUnlockListHistory" class="notVisible" style="margin-top:10px; max-height:300px; overflow-y:auto;border:1px solid #344b56; border:1px 0 1px 0; padding:2px 0">
                    <asp:Repeater ID="rptClientDebitLockUnlockListHistory" runat="server">
                        <HeaderTemplate>
                            <table class="defaultTable" style="width:100%">
                                <tr>
                                    <th>Raison</th>
                                    <th>Blocage</th>
                                    <th>Déblocage</th>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td><%# Eval("Reason") %></td>
                                <td>
                                    <div class="trimmer" style="width:250px; padding:0;" >
                                        <asp:Label CssClass="tooltip" ID="lblClientDebitLockDetails" runat="server"
                                            ToolTip='<%# GetClientDebitLockUnlockDetails(Eval("LockDate").ToString(), Eval("LockBOUser").ToString(), true) %>'>
                                            <%# GetClientDebitLockUnlockDetails(Eval("LockDate").ToString(), Eval("LockBOUser").ToString(), true) %>
                                        </asp:Label>
                                    </div>
                                </td>
                                <td>
                                    <div class="trimmer" style="width:250px;padding:0;" >
                                        <asp:Label CssClass="tooltip" ID="lblClientUnLockDetails" runat="server"
                                            ToolTip='<%# GetClientDebitLockUnlockDetails(Eval("UnLockDate").ToString(), Eval("UnLockBOUser").ToString(), false) %>'>
                                            <%# GetClientDebitLockUnlockDetails(Eval("UnLockDate").ToString(), Eval("UnLockBOUser").ToString(), false) %>
                                        </asp:Label>
                                    </div>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnClientDebitAddLock" EventName="click" />
            <asp:AsyncPostBackTrigger ControlID="btnClientDebitUnlock" EventName="click" />
        </Triggers>
    </asp:UpdatePanel>
</div>
</div>

<script type="text/javascript">
    Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(LightLockBeginRequestHandler);
    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(LightLockEndRequestHandler);
    function LightLockBeginRequestHandler(sender, args) {
        pbControl = args.get_postBackElement();
        //console.log(pbControl.id);
        var containerID = '';
        if (pbControl != null) {
            if (pbControl.id.indexOf('btnClientDebitUnlock') != -1)
                containerID = '<%=panelClientDebitActivatedLock.ClientID%>';
        }

        if(containerID.length > 0)
            LightLockShowPanelLoading(containerID);
    }
    function LightLockEndRequestHandler(sender, args) {
        LightLockHidePanelLoading();
    }
</script>