﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AmlComment.ascx.cs" Inherits="API_AmlComment" %>

<div class="label">
    <asp:Label ID="lblTextLabel" runat="server">Commentaire</asp:Label>

    <asp:Panel ID="panelCharCounter" runat="server" CssClass="comment-counter">
        <asp:Label ID="lblNbChar" runat="server"></asp:Label>
        /
        <asp:Label ID="lblMaxChar" runat="server"></asp:Label>
    </asp:Panel>
</div>
<div>
    <asp:TextBox ID="txtComment" runat="server" TextMode="MultiLine" Width="100%" Height="80px" style="font-size:15px;font-size:15px;font-family:Arial;box-sizing:border-box"></asp:TextBox>
</div>