﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using XMLMethodLibrary;

public partial class API_AccountGroupManager : System.Web.UI.UserControl
{
    public int RefCustomer { get { return (ViewState["RefCustomer"] != null) ? int.Parse(ViewState["RefCustomer"].ToString()) : 0; } set { ViewState["RefCustomer"] = value; } }
    public bool HideEdit { get { return (ViewState["HideEdit"] != null) ? bool.Parse(ViewState["HideEdit"].ToString()) : false; } set { ViewState["HideEdit"] = value; } }

    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack && RefCustomer != 0)
        {
            Update();
        }
    }

    public void Update()
    {
        //GetGroupKind();
        BindCustomerGroupInfos(RefCustomer);

        panelLinkAccount.Visible = !HideEdit;
    }

    protected DataTable GetGroupKind()
    {
        SqlConnection conn = null;
        DataTable dt = new DataTable();

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("Web.P_GetGroupKinds", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 100);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@ReturnSelect", SqlDbType.Bit);

            cmd.Parameters["@IN"].Value = "<ALL_XML_IN />";
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
            cmd.Parameters["@ReturnSelect"].Value = 0;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();
        }
        catch(Exception e)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }

    protected void BindCustomerGroupInfos(int iRefCustomer)
    {
        panelNoGroup.Visible = false;
        rptGroupContent.Visible = false;

        DataTable dt = GetCustomerGroupInfos(iRefCustomer);

        if (dt.Rows.Count > 0)
            rptGroupContent.Visible = true;
        else
        {
            panelNoGroup.Visible = true;
            panelLinkAccount.CssClass = "";
        }
        
        rptGroupContent.DataSource = dt;
        rptGroupContent.DataBind();
    }

    protected DataTable GetCustomerGroupInfos(int iRefCustomer)
    {
        DataTable dtLinkAccount = new DataTable();
        dtLinkAccount.Columns.Add("RefGroup");
        dtLinkAccount.Columns.Add("RefCustomer");
        dtLinkAccount.Columns.Add("AccountNumber");
        dtLinkAccount.Columns.Add("LastName");
        dtLinkAccount.Columns.Add("FirstName");
        dtLinkAccount.Columns.Add("IsStar");

        string sXmlGroup = GetCustomerGroupOrGroupCustomer(iRefCustomer, "RefCustomer");

        if(sXmlGroup.Length > 0)
        {
            List<string> listGroup = CommonMethod.GetTags(sXmlGroup, "ALL_XML_OUT/Group");

            for (int i = 0; i < listGroup.Count; i++)
            {
                List<string> listRef = CommonMethod.GetAttributeValues(listGroup[i], "Group", "RefGroup");

                int iGroup = 0;
                if (int.TryParse(listRef[0].ToString(), out iGroup))
                {
                    string sXmlCustomers = GetCustomerGroupOrGroupCustomer(iGroup, "RefGroup");

                    List<string> listGroup2 = CommonMethod.GetTags(sXmlCustomers, "ALL_XML_OUT/Group");

                    for (int j = 0; j < listGroup2.Count; j++)
                    {
                        List<string> listRefCustomer = CommonMethod.GetAttributeValues(listGroup2[j], "Group", "RefCustomer");
                        List<string> listAccountNumber = CommonMethod.GetAttributeValues(listGroup2[j], "Group", "AccountNumber");
                        List<string> listLastName = CommonMethod.GetAttributeValues(listGroup2[j], "Group", "LastName");
                        List<string> listFirstName = CommonMethod.GetAttributeValues(listGroup2[j], "Group", "FirstName");
                        List<string> listIsStar = CommonMethod.GetAttributeValues(listGroup2[j], "Group", "IsStar");

                        if(listRefCustomer[0] != iRefCustomer.ToString())
                            dtLinkAccount.Rows.Add(new object[] { iGroup, listRefCustomer[0], listAccountNumber[0], listLastName[0], listFirstName[0], listIsStar[0] });
                    }
                }
            }
        }

        return dtLinkAccount;
    }
    protected string GetCustomerGroupOrGroupCustomer(int i, string sTag)
    {
        SqlConnection conn = null;
        string sXmlOut = "";

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_GetGroupCustomers", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN", 
                                            new XElement("Group", 
                                            new XAttribute(sTag, i))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            sXmlOut = cmd.Parameters["@OUT"].Value.ToString();
        }
        catch (Exception e)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return sXmlOut;
    }

    protected void btnAddToGroup_Click(object sender, EventArgs e)
    {
        if (txtLinkAccountNumber.Text.Length == 11)
        {
            DataTable dt = Client.GetClientList("", "", "", "", "", txtLinkAccountNumber.Text, authentication.GetCurrent().sToken);

            if(dt.Rows.Count > 0)
            {
                int iLinkRefCustomer = 0;
                if (int.TryParse(dt.Rows[0]["RefCustomer"].ToString(), out iLinkRefCustomer))
                {
                    if (LinkAccounts(RefCustomer, iLinkRefCustomer))
                    {
                        BindCustomerGroupInfos(RefCustomer);
                        txtLinkAccountNumber.Text = "";
                    }
                    else ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Erreur', \"Le lien n'a pas pu être créé\", null, null);", true);
                }
                else ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Erreur', \"Aucun client trouvé\", null, null);", true);
            }
            else ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Erreur', \"Aucun client trouvé\", null, null);", true);
        }
        else ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Erreur', \"N° compte : format non valide\", null, null);", true);
    }

    protected bool LinkAccounts(int iRefCustomer, int iLinkRefCustomer)
    {
        bool bLinked = false;
        int iRefGroup = CreateGroup(iRefCustomer + "_" + iLinkRefCustomer + "_" + DateTime.Now.ToString("ddMMyyyyhhmmss"));

        if(iRefGroup > 0)
        {
            string sXmlIn = new XElement("ALL_XML_IN",
                                new XElement("Group",
                                    new XAttribute("RefGroup", iRefGroup),
                                    new XAttribute("RefCustomer", iLinkRefCustomer),
                                    new XAttribute("Action", "A"),
                                    new XAttribute("IsStar", "1")),
                                new XElement("Group",
                                    new XAttribute("RefGroup", iRefGroup),
                                    new XAttribute("RefCustomer", iRefCustomer),
                                    new XAttribute("Action", "A"))).ToString();
            string sXmlOut = AddDelSetGroupCustomer(sXmlIn);

            if(sXmlOut.Length > 0)
            {
                List<string> listInserted = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Result", "Inserted");
                if (listInserted.Count > 0 && listInserted[0] == "2")
                    bLinked = true;
            }
        }

        return bLinked;
    }

    protected int CreateGroup(string sGroupName)
    {
        int iRefGroup = 0;

        string sXmlIn = new XElement("ALL_XML_IN",
                            new XElement("Group",
                                new XAttribute("Action", "A"),
                                new XAttribute("GroupName", sGroupName))).ToString();

        string sXmlOut = AddDelSetGroup(sXmlIn);

        if (sXmlOut.Length > 0) {
            List<string> listRefGroupInserted = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Result", "RefGroupInserted");

            if (listRefGroupInserted.Count > 0)
                int.TryParse(listRefGroupInserted[0], out iRefGroup);
        }

        return iRefGroup;
    }
    protected string AddDelSetGroup(string sXmlIn)
    {
        SqlConnection conn = null;
        string sXmlOut = "";

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("Web.P_AddDelSetGroup", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);

            cmd.Parameters["@IN"].Value = sXmlIn;
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            sXmlOut = cmd.Parameters["@OUT"].Value.ToString();
        }
        catch(Exception e)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return sXmlOut;
    }

    protected string AddDelSetGroupCustomer(string sXmlIn)
    {
        SqlConnection conn = null;
        string sXmlOut = "";

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("Web.P_AddDelSetGroupCustomer", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);

            cmd.Parameters["@IN"].Value = sXmlIn;
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            sXmlOut = cmd.Parameters["@OUT"].Value.ToString();
        }
        catch (Exception e)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return sXmlOut;
    }

    protected void btnDeleteLink_Click(object sender, EventArgs e)
    {
        bool bUpdate = false;

        foreach (RepeaterItem item in rptGroupContent.Items)
        {
            if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
            {
                CheckBox ckb = item.FindControl("ckbSelect") as CheckBox;
                HiddenField hdnRefGroup = item.FindControl("hdnRefGroup") as HiddenField;
                HiddenField hdnRefCustomer = item.FindControl("hdnRefCustomer") as HiddenField;

                if (ckb != null && ckb.Checked &&
                    hdnRefGroup != null && !String.IsNullOrWhiteSpace(hdnRefGroup.Value) &&
                    hdnRefCustomer != null && !String.IsNullOrWhiteSpace(hdnRefCustomer.Value))
                {
                    string sXmlIn = new XElement("ALL_XML_IN",
                                new XElement("Group",
                                    new XAttribute("RefGroup", hdnRefGroup.Value),
                                    new XAttribute("RefCustomer", RefCustomer),
                                    new XAttribute("Action", "D"))
                                    /*, new XElement("Group",
                                        new XAttribute("RefGroup", hdnRefGroup.Value),
                                        new XAttribute("RefCustomer", hdnRefCustomer.Value),
                                        new XAttribute("Action", "D"))*/
                                    ).ToString();
                    string sXmlOut = AddDelSetGroupCustomer(sXmlIn);

                    if (sXmlOut.Length > 0)
                    {
                        List<string> listDeleted = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Result", "Deleted");
                        if (listDeleted.Count > 0 && listDeleted[0] == "1")
                            bUpdate = true;
                        else ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Erreur', \"Le lien n'a pas pu être supprimé\", null, null);", true);
                    }
                }
            }
        }

        if(bUpdate)
            Update();
    }
}