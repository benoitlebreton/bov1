﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class API_ProfileHistory : System.Web.UI.UserControl
{
    public int iRefCustomer
    {
        get
        {
            int _iRefCustomer = 0;
            int.TryParse((ViewState["iRefCustomer"] != null) ? ViewState["iRefCustomer"].ToString() : "0", out _iRefCustomer);
            return _iRefCustomer;
        }
        set { ViewState["iRefCustomer"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
            BindClientProfile();
    }

    protected void BindClientProfile()
    {
        DataTable dtCurrentProfile = new DataTable();
        DataTable dtProfileHistory = new DataTable();
        DataTable dtAllProfileHistory = new DataTable();
        dtAllProfileHistory.Columns.Add("TAG");
        dtAllProfileHistory.Columns.Add("Date");
        dtAllProfileHistory.Columns.Add("ByUser");
        dtAllProfileHistory.Columns.Add("Reason");

        AML.getClientProfile(iRefCustomer, out dtProfileHistory, out dtCurrentProfile);

        if (dtCurrentProfile.Rows.Count == 0)
        {
            panelProfileHistoryList.Visible = false;
            panelProfileHistoryListEmpty.Visible = true;
        }
        else
        {
            dtAllProfileHistory.Rows.Add(new object[] {
                dtCurrentProfile.Rows[0]["TAG"].ToString() ,
                (dtCurrentProfile.Rows[0]["Date"].ToString().Trim().Length > 0) ? tools.getFormattedDate(dtCurrentProfile.Rows[0]["Date"].ToString()) : "-",
                (dtCurrentProfile.Rows[0]["ByUser"].ToString().Trim().Length > 0) ? dtCurrentProfile.Rows[0]["ByUser"].ToString() : "-",
                (dtCurrentProfile.Rows[0]["Reason"].ToString().Trim().Length > 0) ? dtCurrentProfile.Rows[0]["Reason"].ToString() : "-"
            });

            for(int i = 0; i < dtProfileHistory.Rows.Count; i++)
            {
                dtAllProfileHistory.Rows.Add(new object[] {
                    dtProfileHistory.Rows[i]["TAG"].ToString() ,
                    (dtProfileHistory.Rows[i]["Date"].ToString().Trim().Length > 0) ? tools.getFormattedDate(dtProfileHistory.Rows[i]["Date"].ToString()) : "-",
                    (dtProfileHistory.Rows[i]["ByUser"].ToString().Trim().Length > 0) ? dtProfileHistory.Rows[i]["ByUser"].ToString() : "-",
                    ""
                });
            }

            rptAML_ClientProfile_History.DataSource = dtAllProfileHistory;
            rptAML_ClientProfile_History.DataBind();
        }
    }
}