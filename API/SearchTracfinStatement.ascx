﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SearchTracfinStatement.ascx.cs" Inherits="API_SearchTracfinStatement" %>
<%@ Register Src="~/API/ClientSearch.ascx" TagName="ClientSearch" TagPrefix="asp" %>

<asp:Panel ID="panelTracfinStatement" runat="server">
    
    <div class="panel-loading"></div>

    <asp:Panel ID="panelTitle" runat="server" Visible="false" CssClass="tabSubtitle" style="margin-top:20px;">
        Déclaration(s) TRACFIN
    </asp:Panel>

    <asp:UpdatePanel ID="upTracfinStatement" runat="server">
        <ContentTemplate>
        
            <asp:Panel ID="panelFilters" runat="server" style="margin-bottom:15px" DefaultButton="btnSearch">
                <div style="display:flex">
                    <div>
                        <div class="label">
                            Référence
                        </div>
                        <div>
                            <asp:TextBox ID="txtReference" runat="server"></asp:TextBox>
                        </div>
                    </div>
                    <div style="margin-left:10px;width:40%">
                        <div class="label">
                            Client
                        </div>
                        <div>
                            <asp:ClientSearch ID="clientSearch1" runat="server" HideDetails="true" HideSearchButton="true" HideAlerts="true" />
                        </div>
                    </div>
                    <div style="margin-left:10px">
                        <div class="label">
                            Date du
                        </div>
                        <div>
                            <asp:TextBox ID="txtDateFrom" runat="server"></asp:TextBox>
                        </div>
                    </div>
                    <div style="margin-left:10px">
                        <div class="label">
                            Date au
                        </div>
                        <div>
                            <asp:TextBox ID="txtDateTo" runat="server"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div style="text-align:center;margin-top:5px">
                    <asp:Button ID="btnSearch" runat="server" Text="Rechercher" CssClass="button" OnClick="btnSearch_Click" style="margin-bottom:0" />
                </div>
            </asp:Panel>
                
            <asp:Repeater ID="rptTracfinStatement" runat="server">
                <HeaderTemplate>
                    <table id="statement-list" class="defaultTable2">
                        <tr>
                            <th>Référence Int.</th>
                            <th>Référence Ext.</th>
                            <th>Date</th>
                            <th>Motif</th>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                        <tr class='row-over-color <%#Container.ItemIndex % 2 == 0 ? "row-even" : "row-odd" %>' onclick='<%# String.Format("ShowPanelLoading(\"statement-list\");$(\"#{0}\").val({1});$(\"#{2}\").click();", hdnRefStatement.ClientID, Eval("Ref"), btnGotoStatement.ClientID) %>'>
                            <td><%#Eval("RefStatementInt") %></td>
                            <td><%#Eval("RefStatementExt") %></td>
                            <td><%#Eval("Date") %></td>
                            <td><%#Eval("Reason") %></td>
                        </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>

            <asp:HiddenField ID="hdnRefStatement" runat="server" />
            <asp:Button ID="btnGotoStatement" runat="server" OnClick="btnGotoStatement_Click" style="display:none" />
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Panel>

<script type="text/javascript">
    Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(SearchTracfinStatementBeginRequestHandler);
    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(SearchTracfinStatementEndRequestHandler);
    function SearchTracfinStatementBeginRequestHandler(sender, args) {
        try{
            pbControl = args.get_postBackElement();
            console.log(pbControl.id);
            var containerID = '';
            if (pbControl != null)
                if (pbControl.id.indexOf('btnSearch') != -1 ||
                    pbControl.id.indexOf('btnClientSearch') != -1)
                    containerID = '<%=panelFilters.ClientID%>';

            if(containerID.length > 0)
                ShowPanelLoading(containerID);
        }
        catch(e)
        {

        }
    }
    function SearchTracfinStatementEndRequestHandler(sender, args) {
        HidePanelLoading();
    }
</script>