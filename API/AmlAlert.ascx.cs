﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using XMLMethodLibrary;

public partial class API_AmlAlert : System.Web.UI.UserControl
{
    private int _iNbAlert;
    private int _iNbWatched;
    private int _iRefCustomer
    {
        get { return int.Parse(ViewState["RefCustomer"].ToString()); }
        set { ViewState["RefCustomer"] = value; }
    }

    public int RefCustomer
    {
        get { return _iRefCustomer; }
        set { _iRefCustomer = value; }
    }

    public int NbAlert
    {
        get { return _iNbAlert; }
    }
    public int NbWatched
    {
        get { return _iNbWatched;  }
    }

    public event EventHandler AlertCheckedEvent;

    public void Bind(bool bForce, int iPageIndex)
    {
        _iNbAlert = 0; _iNbWatched = 0;
        authentication auth = authentication.GetCurrent();

        if (!IsPostBack || bForce)
        {
            string sXmlOut = "";
            string sXmlIn = new XElement("ALL_XML_IN",
                                new XElement("Alert",
                                    new XAttribute("PageSize", "5"),
                                    new XAttribute("PageIndex", iPageIndex.ToString()),
                                    new XAttribute("CashierToken", auth.sToken),
                                    new XAttribute("RefCustomer", _iRefCustomer.ToString()),
                                    new XAttribute("OrderBy", "refAlertStatus ASC,RefAlert"),
                                    new XAttribute("Culture", (Session["CultureID"] != null) ? Session["CultureID"].ToString() : "fr-FR"))).ToString();

            gvClientAlertList.DataSource = AML.getClientAlertList(sXmlIn, out sXmlOut);
            List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/RESULT", "RC");
            List<string> listErrorLabel = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/RESULT", "ErrorLabel");
            List<string> listRowCount = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Alert", "NbResult");
            List<string> listWatchCount = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Alert", "NbWatched");
            if (listRC.Count == 0 && listRowCount != null && listRowCount.Count > 0)
            {
                panelGrid.Visible = true;
                panelError.Visible = false;
                lblRowCount.Text = listRowCount[0].ToString();
                //int.TryParse(listRowCount[0].ToString().Trim(), out _iNbAlert);

                //getDynamicTitleColumns(sXmlOut);
                getAmlDynamicColumns(sXmlOut);

                if (listWatchCount != null && listWatchCount.Count > 0)
                    int.TryParse(listWatchCount[0].ToString().Trim(), out _iNbWatched);
            }
            else
            {
                if (listErrorLabel != null && listErrorLabel.Count > 0)
                {
                    panelGrid.Visible = false;
                    panelError.Visible = true;
                    lblError.Text = listErrorLabel[0].ToString().ToUpper();
                }
            }

            try
            {
                gvClientAlertList.DataBind();
                if (gvClientAlertList.Rows.Count > 0)
                    panelGrid.Visible = true;

                if (int.Parse(lblRowCount.Text) > 5)
                {

                    int selectedPage = ddlPage.SelectedIndex;

                    DataTable dtPages = getPages(CalculateTotalPages(int.Parse(lblRowCount.Text), 5));
                    ddlPage.DataSource = dtPages;
                    ddlPage.DataBind();

                    string sPageIndex = "";
                    sPageIndex = iPageIndex.ToString();
                    if (sPageIndex.Trim().Length > 0 && int.TryParse(sPageIndex, out iPageIndex))
                    {
                        if (ddlPage.Items.Count > 0 && ddlPage.Items.FindByValue(iPageIndex.ToString()) != null)
                            ddlPage.Items.FindByValue(iPageIndex.ToString()).Selected = true;
                    }
                    else if (ViewState["AlertAmlPageIndex"] != null)
                    {
                        if (ddlPage.Items.Count > 0)
                            ddlPage.Items.FindByValue(ViewState["AlertAmlPageIndex"].ToString()).Selected = true;
                    }
                    else if (ddlPage.Items.Count > selectedPage)
                        ddlPage.SelectedIndex = selectedPage;

                    if (int.Parse(ddlPage.SelectedValue) == ddlPage.Items.Count)
                        btnNextPage.Enabled = false;
                    else
                        btnNextPage.Enabled = true;

                    if (int.Parse(ddlPage.SelectedValue) == 1)
                        btnPreviousPage.Enabled = false;
                    else
                        btnPreviousPage.Enabled = true;

                    if (ddlPage.Items.Count > 1)
                        divPagination.Visible = true;
                    else
                        divPagination.Visible = false;

                    lblTotalPage.Text = " / " + ddlPage.Items.Count.ToString();


                }
                else
                {
                    divPagination.Visible = false;
                }
            }
            catch (Exception ex)
            {
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Bind(false, 1);
        }

        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "initAmlAlert();", true);
    }

    protected void gvClientAlertList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HiddenField hdnAlertDetails = (HiddenField)e.Row.FindControl("hdnAlertDetails");
            HiddenField hdnRefAlertStatus = (HiddenField)e.Row.FindControl("hdnRefAlertStatus");
            HiddenField hdnAlertTAG = (HiddenField)e.Row.FindControl("hdnAlertTAG");
            HiddenField hdnRefAlert = (HiddenField)e.Row.FindControl("hdnRefAlert");
            
            if (hdnAlertDetails != null)
            {
                
                e.Row.Attributes.Add("onmouseover", "this.style.cursor = 'pointer';this.style.backgroundColor = '#ff9966'");
                e.Row.Attributes.Add("onmouseout", "this.style.cursor = 'auto';this.style.backgroundColor = '#fff'");
            }

            if (hdnRefAlertStatus != null)
            {
                string sAlertTAG = (hdnAlertTAG != null) ? hdnAlertTAG.Value.Trim() : "";
                int iRefAlert = 0;
                if (hdnRefAlert != null)
                    int.TryParse(hdnRefAlert.Value.Trim(), out iRefAlert);

                string sAlertDetails = hdnAlertDetails.Value.Replace("'", "&apos;");
                switch (hdnRefAlertStatus.Value.Trim()) 
                {
                    case "1":
                        _iNbAlert++;
                        
                        e.Row.Attributes.Add("style", "font-weight:bold;color:red");
                        e.Row.Attributes.Add("onclick", "showDetails('" + sAlertDetails + "','1','" + sAlertTAG + "','" + iRefAlert.ToString() + "')");
                        break;
                    case "2":
                        e.Row.Attributes.Add("onclick", "showDetails('" + sAlertDetails + "','2','" + sAlertTAG + "','" + iRefAlert.ToString() + "')");
                        break;
                    case "3":
                        //e.Row.Attributes.Add("style", "font-weight:bold;color:green");
                        e.Row.Attributes.Add("onclick", "showDetails('" + sAlertDetails + "','3','" + sAlertTAG + "','" + iRefAlert.ToString() + "')");
                        break;
                    default:
                        e.Row.Attributes.Add("onclick", "showDetails('" + sAlertDetails + "')");
                        break;
                }
                
            }

        }
    }
    protected void getDynamicTitleColumns(string sXML)
    {
        string sINT1 = "", sINT2 = "", sMONEY1 = "", sVARCHAR1 = "";

        sINT1 = tools.GetValueFromXml(sXML, "ALL_XML_OUT/Alert/CriteriaDescription", "Critere_INT1");
        sINT2 = tools.GetValueFromXml(sXML, "ALL_XML_OUT/Alert/CriteriaDescription", "Critere_INT2");
        sVARCHAR1 = tools.GetValueFromXml(sXML, "ALL_XML_OUT/Alert/CriteriaDescription", "Critere_VARCHAR1");
        sMONEY1 = tools.GetValueFromXml(sXML, "ALL_XML_OUT/Alert/CriteriaDescription", "Critere_MONEY1");
        gvClientAlertList.Columns[2].Visible = false;
        gvClientAlertList.Columns[3].Visible = false;
        gvClientAlertList.Columns[4].Visible = false;
        gvClientAlertList.Columns[5].Visible = false;

        if (sINT1.Trim().Length > 0)
        {
            gvClientAlertList.Columns[2].Visible = true;
            gvClientAlertList.Columns[2].HeaderText = sINT1.Trim();
        }
        if (sINT2.Trim().Length > 0)
        {
            gvClientAlertList.Columns[3].HeaderText = sINT2.Trim();
            gvClientAlertList.Columns[3].Visible = true;
        }
        if (sMONEY1.Trim().Length > 0)
        {
            gvClientAlertList.Columns[4].HeaderText = sMONEY1.Trim();
            gvClientAlertList.Columns[4].Visible = true;
        }
        if (sVARCHAR1.Trim().Length > 0)
        {
            gvClientAlertList.Columns[5].HeaderText = sVARCHAR1.Trim();
            gvClientAlertList.Columns[5].Visible = true;
        }
    }
    protected void getAmlDynamicColumns(string sXML)
    {
        ViewState["AmlDynamicColumns"] = null;

        DataTable dt = new DataTable();
        dt.Columns.Add("TAG");
        dt.Columns.Add("INT1");
        dt.Columns.Add("INT2");
        dt.Columns.Add("VARCHAR1");
        dt.Columns.Add("MONEY1");

        string sINT1 = "", sINT2 = "", sMONEY1 = "", sVARCHAR1 = "", sTAG = "";
        List<string> listAlertCriteriaDescription = XMLMethodLibrary.CommonMethod.GetTags(sXML, "ALL_XML_OUT/Alert/CriteriaDescription");
        for (int i = 0; i < listAlertCriteriaDescription.Count; i++)
        {
            sTAG = tools.GetValueFromXml(listAlertCriteriaDescription[i], "CriteriaDescription", "CategoryTAG");
            sINT1 = tools.GetValueFromXml(listAlertCriteriaDescription[i], "CriteriaDescription", "Critere_INT1");
            sINT2 = tools.GetValueFromXml(listAlertCriteriaDescription[i], "CriteriaDescription", "Critere_INT2");
            sVARCHAR1 = tools.GetValueFromXml(listAlertCriteriaDescription[i], "CriteriaDescription", "Critere_VARCHAR1");
            sMONEY1 = tools.GetValueFromXml(listAlertCriteriaDescription[i], "CriteriaDescription", "Critere_MONEY1");

            dt.Rows.Add(new object[] { sTAG, sINT1, sINT2, sVARCHAR1, sMONEY1 });
        }

        ViewState["AmlDynamicColumns"] = dt;
    }

    protected void btnConfirmAlertChecked_Click(object sender, EventArgs e)
    {
        authentication auth = authentication.GetCurrent();

        if (txtAlertCheckedComment.Text.Trim().Length > 0)
        {
            string sComment = hfRefAlert.Value.ToString() + " : " + txtAlertCheckedComment.Text;
            string sXml = getXmlCheckAlert();
            if (sXml.Trim().Length > 0)
            {
                AML.changeClientAlertStatus(sXml);
                AML.addClientNote(auth.sToken, _iRefCustomer, sComment);
            }
        }
        int iPageIndex = 1;
        int.TryParse(ddlPage.SelectedValue, out iPageIndex);
        Bind(true, iPageIndex);
        if(AlertCheckedEvent != null)
            AlertCheckedEvent(null, null);
    }
    protected string getXmlCheckAlert()
    {
        string sXml = "";

        string sRefAlert = hfRefAlert.Value;
        int iRefAlert;
        authentication auth = authentication.GetCurrent();
        
        if (int.TryParse(sRefAlert, out iRefAlert) && !cbAlertAllChecked.Checked)
        {
            //Alerte vérifiée
            sXml = new XElement("ALL_XML_IN",
                new XElement("Alerts",
                    new XAttribute("CashierToken", auth.sToken),
                    new XElement("Alert",
                        new XAttribute("RefAlert", iRefAlert.ToString()),
                        new XAttribute("Comment", txtAlertCheckedComment.Text.Trim()),
                        new XAttribute("RefAlertStatus", "3")))).ToString();
        }
        else if (cbAlertAllChecked.Checked && hfTagAlert.Value.Trim().Length > 0 && hfAlertStatus.Value.Trim().Length > 0)
        {
            //Toutes les alertes du meme type/status vérifiées
            sXml = new XElement("ALL_XML_IN",
                new XElement("Alerts",
                    new XAttribute("CashierToken", auth.sToken),
                    new XElement("Alert",
                        new XAttribute("RefCustomer", _iRefCustomer.ToString()),
                        new XAttribute("PreviousRefAlertStatus", hfAlertStatus.Value.Trim()),
                        new XAttribute("TAGAlert", hfTagAlert.Value.Trim()),
                        new XAttribute("Comment", txtAlertCheckedComment.Text.Trim()),
                        new XAttribute("RefAlertStatus", "3")))).ToString();
        }

        return sXml;
    }

    protected DataTable getPages(int nbPages)
    {
        DataTable dt = new DataTable();

        dt.Columns.Add("L");
        dt.Columns.Add("V");

        for (int i = 1; i <= nbPages; i++)
        {
            dt.Rows.Add(i.ToString(), i.ToString());
        }

        return dt;
    }
    protected int CalculateTotalPages(double totalRows, int pageSize)
    {
        int totalPages = (int)Math.Ceiling(totalRows / pageSize);

        return totalPages;
    }

    protected void btnNextPage_Click(object sender, EventArgs e)
    {
        int iPageIndex;
        if(int.TryParse(ddlPage.SelectedValue, out iPageIndex))
        {
            iPageIndex++;
            ddlPage.SelectedValue = (iPageIndex).ToString();
            ViewState["AlertAmlPageIndex"] = iPageIndex;
            Bind(true, iPageIndex);
        }
    }
    protected void ddlPage_SelectedIndexChanged(object sender, EventArgs e)
    {
        int iPageIndex;
        int.TryParse(ddlPage.SelectedValue, out iPageIndex);
        ViewState["AlertAmlPageIndex"] = iPageIndex;
        Bind(true, iPageIndex);
    }
    protected void btnPreviousPage_Click(object sender, EventArgs e)
    {
        int iPageIndex;
        if (int.TryParse(ddlPage.SelectedValue, out iPageIndex))
        {
            iPageIndex--;
            ddlPage.SelectedValue = (iPageIndex).ToString();
            ViewState["AlertAmlPageIndex"] = iPageIndex;
            Bind(true, iPageIndex);
        }
    }

    protected string getAmlDetails(string sTagAlert, string sCritere_INT1, string sCritere_INT2, string sCritere_VARCHAR1, string sCritere_MONEY1)
    {
        string sAmlDetails = "";

        if (ViewState["AmlDynamicColumns"] != null)
        {
            try
            {
                DataTable dt = (DataTable)ViewState["AmlDynamicColumns"];
                sAmlDetails = "<div style=\"display:table\" class=\"tableAmlSearch\"><div style=\"display:table-row\">";

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["TAG"].ToString().Trim() == sTagAlert.Trim())
                    {
                        if (dt.Rows[i]["INT1"].ToString().Trim().Length > 0)
                        {
                            sAmlDetails += "<div style=\"display:table-cell;text-align:center;padding: 0 5px;\"><div style=\"font-weight:bold\">" + dt.Rows[i]["INT1"].ToString().Trim() + "</div>";
                            sAmlDetails += "<div style=\"text-align:right; border:0;\">" + sCritere_INT1 + "</div></div>";

                        }
                        if (dt.Rows[i]["INT2"].ToString().Trim().Length > 0)
                        {
                            sAmlDetails += "<div style=\"display:table-cell;text-align:center;padding: 0 5px;\"><div style=\"font-weight:bold\">" + dt.Rows[i]["INT2"].ToString().Trim() + "</div>";
                            sAmlDetails += "<div style=\"text-align:right; border:0;\">" + sCritere_INT2 + "</div></div>";

                        }
                        if (dt.Rows[i]["VARCHAR1"].ToString().Trim().Length > 0)
                        {
                            sAmlDetails += "<div style=\"display:table-cell;text-align:center;padding: 0 5px;\"><div style=\"font-weight:bold\">" + dt.Rows[i]["VARCHAR1"].ToString().Trim() + "</div>";
                            sAmlDetails += "<div style=\"border:0;\">" + sCritere_VARCHAR1 + "</div></div>";

                        }
                        if (dt.Rows[i]["MONEY1"].ToString().Trim().Length > 0)
                        {
                            sAmlDetails += "<div style=\"display:table-cell;text-align:center;padding: 0 5px;\"><div style=\"font-weight:bold\">" + dt.Rows[i]["MONEY1"].ToString().Trim() + "</div>";
                            sAmlDetails += "<div style=\"text-align:right; border:0;\">" + tools.getFormattedMoney(sCritere_MONEY1) + "</div></div>";

                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        return sAmlDetails;
    }
    
}