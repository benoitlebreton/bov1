﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AmlSearch.ascx.cs" Inherits="API_AmlSearch" %>

<%--<asp:UpdatePanel ID="upSearch" runat="server" UpdateMode="Conditional">
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="btnPreviousPage" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID="btnNextPage" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID="ddlPage" EventName="SelectedIndexChanged" />
        <asp:AsyncPostBackTrigger ControlID="ddlNbResult" EventName="SelectedIndexChanged" />
    </Triggers>
    <ContentTemplate>--%>
                    
        <asp:Panel ID="panelGrid" runat="server" Visible="false">
            <div id="divGridSettings" runat="server" visible="false" style="display: table; width: 100%; margin-bottom: 10px">
                <div style="display: table-row">
                    <div style="display: table-cell; width: 50%; padding-top: 20px">
                        <asp:Label ID="lblNbOfResults" runat="server">0</asp:Label>
                        résultat(s)
                    </div>
                    <div style="display: table-cell; width: 50%; text-align: right;">
                        Nb de résultats par page :
                        <asp:DropDownList ID="ddlNbResult" runat="server" AutoPostBack="true" OnSelectedIndexChanged="onChangeNbResult">
                            <asp:ListItem Value="10" Selected="True">10</asp:ListItem>
                            <asp:ListItem Value="25">25</asp:ListItem>
                            <asp:ListItem Value="50">50</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div style="border: 3px solid #344b56; border-top-left-radius: 8px; border-top-right-radius: 8px;margin-top:5px">
                <asp:GridView ID="gvClientAlertList" runat="server" AllowSorting="false"
                    AllowPaging="false" AutoGenerateColumns="False" GridLines="None" CellPadding="10"
                    DataKeyNames="RefCustomer" ShowHeader="true" Width="100%" SortedDescendingCellStyle-Font-Overline="true"
                    ViewStateMode="Disabled" BorderWidth="0" BackColor="Transparent" OnRowDataBound="gvClientAlertList_RowDataBound">
                    <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="#344b56" Font-Bold="true" />
                    <EmptyDataTemplate>
                        Aucune alerte
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField Visible="false" InsertVisible="false">
                            <ItemTemplate>
                                <asp:HiddenField ID="hdnRefCustomer" runat="server" Value='<%# Eval("RefCustomer")%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Nom" SortExpression="LastName" InsertVisible="false"
                            ControlStyle-CssClass="trimmer" ControlStyle-Width="120">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblClientLastName" Text='<%# Eval("LastName")%>' CssClass="tooltip"
                                    ToolTip='<%# Eval("LastName")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Prénom" SortExpression="FirstName" InsertVisible="false"
                            ControlStyle-CssClass="trimmer" ControlStyle-Width="120">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblClientFirstName" Text='<%# Eval("FirstName") %>'
                                    CssClass="tooltip" ToolTip='<%# Eval("FirstName")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField SortExpression="AlertDescription" HeaderText="Description" InsertVisible="false" 
                            ControlStyle-Width="150">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblAlertDescription" Text='<%# Eval("AlertDescription")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField SortExpression="Alert_INT1" InsertVisible="false" ControlStyle-Width="20" ItemStyle-HorizontalAlign="Right">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblAlert_INT1" Text='<%# Eval("Critere_INT1")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField SortExpression="Alert_INT2" InsertVisible="false" ControlStyle-Width="20" ItemStyle-HorizontalAlign="Right">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblAlert_INT2" Text='<%# Eval("Critere_INT2")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField SortExpression="Alert_MONEY1" InsertVisible="false" ControlStyle-Width="100" ItemStyle-HorizontalAlign="Right">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblAlert_MONEY1" Text='<%# tools.getFormattedMoney(Eval("Critere_MONEY1").ToString()) %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField SortExpression="Alert_VARCHAR1" InsertVisible="false" ControlStyle-Width="150" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblAlert_VARCHAR1" Text='<%# Eval("Critere_VARCHAR1")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField SortExpression="AlertDescription" HeaderText="Date" InsertVisible="false" ControlStyle-Width="150" ItemStyle-HorizontalAlign="Right">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblAlertDate" Text='<%# tools.getFormattedDate(Eval("AlertDate").ToString())%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#344b56" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#344b56" Font-Bold="True" ForeColor="White" Height="25px" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle CssClass="GridViewRowStyle" />
                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                    <%--<PagerStyle CssClass="GridViewPager" />--%>
                </asp:GridView>
            </div>
            <div style="border: 3px solid #344b56; border-bottom-left-radius: 8px; border-bottom-right-radius: 8px; height: 25px; background-color: #344b56; color: White; ">
                <asp:Panel ID="divPagination" runat="server" Visible="false" Style="width: 100%; display: table; margin: auto">
                    <div style="display: table-row;">
                        <div style="display: table-cell; height: 20px; vertical-align: middle; width: 57%; text-align: right">
                            <asp:Button ID="btnPreviousPage" runat="server" onclick="previousPage" Text="<" Font-Bold="true" Width="30px" />
                            <asp:DropDownList ID="ddlPage" runat="server" OnSelectedIndexChanged="onChangePageNumber" AutoPostBack="true"
                                DataTextField="L" DataValueField="V">
                            </asp:DropDownList>
                            <asp:Label ID="lblTotalPage" runat="server" Style="font-weight: bold"></asp:Label>
                            <asp:Button ID="btnNextPage" runat="server" OnClick="nextPage" Text=">" Font-Bold="true" Width="30px" />
                        </div>
                        <div style="display: table-cell; padding-left: 5px">
                        </div>
                    </div>
                </asp:Panel>
            </div>
        </asp:Panel>
        <asp:Panel ID="panelError" runat="server" Visible="false" style="margin-top:20px">
            <asp:Label ID="lblError" runat="server"></asp:Label>
        </asp:Panel>
   <%-- </ContentTemplate>
</asp:UpdatePanel>--%>