﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MessageWithCharCounter.ascx.cs" Inherits="API_MessageWithCharCounter" %>

<div class="label">
    <asp:Label ID="lblTextLabel" runat="server">Message</asp:Label>

    <asp:Panel ID="panelCharCounter" runat="server" CssClass="message-counter">
        <asp:Label ID="lblNbChar" runat="server"></asp:Label>
        /
        <asp:Label ID="lblMaxChar" runat="server"></asp:Label>
    </asp:Panel>
    <asp:Panel ID="panelSplittedCharCounter" runat="server" CssClass="message-counter">
        <asp:Label ID="lblSplittedNbChar" runat="server"></asp:Label>
        (<asp:Label ID="lblSplittedCounter" runat="server"></asp:Label>
        <asp:Label ID="lblSplittedKindMessage" runat="server"></asp:Label><asp:Label ID="lblSplittedKindMessagePlural" runat="server"></asp:Label>)
    </asp:Panel>
</div>
<div>
    <asp:TextBox ID="txtMessage" runat="server" TextMode="MultiLine" Width="100%" Height="80px" style="font-size:15px;font-size:15px;font-family:Arial;box-sizing:border-box"></asp:TextBox>
</div>
