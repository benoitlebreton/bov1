﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="StatsPRO.ascx.cs" Inherits="API_StatsPRO" %>

<h2 class="font-bold uppercase" style="font-size:1.5em;color:#000;margin-top:0">
    Hier, <asp:Label ID="lblNbAgenciesActiveYesterday" runat="server" CssClass="font-orange"></asp:Label> buraliste<asp:Label ID="lblNbAgenciesActiveYesterdayAccord1" runat="server"></asp:Label> <asp:Label ID="lblNbAgenciesActiveYesterdayAccord2" runat="server"></asp:Label> ouvert
    <asp:Label ID="lblTotalNbAccountYesterday" runat="server" CssClass="font-orange"></asp:Label> compte<asp:Label ID="lblTotalNbAccountYesterdayAccord" runat="server"></asp:Label> nickel
</h2>

<div class="table" id="stats-table" style="width:100%;border-collapse:collapse;margin-top:20px">
    <div class="table-row">
        <div class="table-cell" style="width:28%">
            <div class="message-box" style="padding:8px 8px 10px 8px">
                <div class="font-bold font-orange uppercase">
                    Vos statistiques
                </div>
                <div style="width:80%;border-top:1px solid #000"></div>
                <div style="margin-top:10px">
                    <asp:Label ID="lblNbAccountYesterday" runat="server" CssClass="font-bold font-orange" style="font-size:1.6em"></asp:Label>
                    <span class="font-AracneRegular">Compte<asp:Label ID="lblNbAccountYesterdayAccord1" runat="server"></asp:Label> ouvert<asp:Label ID="lblNbAccountYesterdayAccord2" runat="server"></asp:Label> hier</span>
                </div>
                <div style="margin-top:10px">
                    <asp:Label ID="lblNbAccountWeek" runat="server" CssClass="font-bold font-orange" style="font-size:1.6em"></asp:Label>
                    <span class="font-AracneRegular">Compte<asp:Label ID="lblNbAccountWeekAccord1" runat="server"></asp:Label> ouvert<asp:Label ID="lblNbAccountWeekAccord2" runat="server"></asp:Label> cette semaine</span>
                </div>
                <div style="margin-top:10px">
                    <asp:Label ID="lblNbClient" runat="server" CssClass="font-bold font-orange" style="font-size:1.6em"></asp:Label>
                    <span class="font-AracneRegular">Client<asp:Label ID="lblNbClientAccord" runat="server"></asp:Label> compte-nickel chez vous</span>
                </div>
            </div>
        </div>
        <div class="table-cell" style="width:15px"></div>
        <div class="table-cell message-box">
            <div id="div-top-ten" style="padding:8px">
                <div class="font-bold font-orange uppercase">
                    Top 10 buralistes du <asp:Label ID="lblDateTop1" runat="server"></asp:Label>
                </div>
                <div style="width:80%;border-top:1px solid #000"></div>
            </div>
            <asp:Repeater ID="rptTopShopYesterday" runat="server">
                <HeaderTemplate>
                    <div class="stats-repeater font-normal" style="font-size:0.9em">
                </HeaderTemplate>
                <ItemTemplate>
                        <div class="<%# Container.ItemIndex % 2 == 0 ? "row-even" : "row-odd" %><%# Eval("Index").ToString() == "1" ? " font-orange" : "" %>" style="padding:2px 0 2px 16px">
                            <span class="font-bold"><%# Eval("Place").ToString()%></span>. <%# Eval("ShopName").ToString()%> - <%# Eval("City").ToString()%> : <%# Eval("NbAccount").ToString()%>
                        </div>
                </ItemTemplate>
                <FooterTemplate>
                    </div>
                </FooterTemplate>
            </asp:Repeater>
        </div>
        <div class="table-cell" style="width:15px"></div>
        <div class="table-cell message-box" style="width:28%">
            <div style="padding:8px;padding-right:0">
                <div class="font-bold font-orange">
                    <span class="uppercase">Top 3 du <asp:Label ID="lblDateTop2" runat="server"></asp:Label></span>
                    <span style="font-size:0.7em">(<asp:Label ID="lblTopSameShop" runat="server"></asp:Label>)</span>
                </div>
                <div style="width:80%;border-top:1px solid #000"></div>
            </div>
            <asp:Repeater ID="rptTopSameShop" runat="server">
                <HeaderTemplate>
                    <div class="stats-repeater font-normal">
                </HeaderTemplate>
                <ItemTemplate>
                        <div class="<%# Container.ItemIndex % 2 == 0 ? "row-even" : "row-odd"%> font-bold uppercase" style="padding:10px 0 10px 20px">
                            <div class="table" style="width:100%;border-collapse:collapse">
                                <div class="table-row">
                                    <div class="table-cell">
                                        <span class="font-orange"><%# Eval("Index").ToString()%>.</span>&nbsp;
                                    </div>
                                    <div class="table-cell">
                                        <%# Eval("ShopName").ToString()%> - <%# Eval("City").ToString()%>
                                        <br />
                                        <span class="font-orange"><%# Eval("NbAccount").ToString()%></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                </ItemTemplate>
                <FooterTemplate>
                    </div>
                </FooterTemplate>
            </asp:Repeater>
        </div>
    </div>
</div>

<div class="message-box" style="width:28%;">
    <div style="padding:8px 8px 0 8px;position:relative;z-index:300;background-color:#fff;">
        <div class="font-bold font-orange uppercase">
            Les nouveaux buralistes nickel
        </div>
        <div style="width:80%;border-top:1px solid #000"></div>
    </div>
</div>
<div class="message-box" style="z-index:200;position:relative;top:-15px">
    <asp:Repeater ID="rptNewShops" runat="server">
        <HeaderTemplate>
            <div class="new-shop-scroller marquee" style="width:830px;margin:auto;">
        </HeaderTemplate>
        <ItemTemplate>
            <div class="<%# Container.ItemIndex % 2 == 0 ? "" : "font-orange"%> font-bold uppercase">
                <%# Eval("ShopName").ToString()%> - <%# Eval("City").ToString()%>
            </div>
        </ItemTemplate>
        <FooterTemplate>
            </div>
        </FooterTemplate>
    </asp:Repeater>
</div>

<script type="text/javascript">
    function InitPostBack() {
        $('.marquee').marquee({ duration: 15000, duplicated: true, pauseOnHover: true });
        setTimeout(function () {
            var height = $('#stats-table .table-row:first .message-box:eq(1)').height();
            if (navigator.userAgent.indexOf('Mozilla') != -1)
                height = height - 40;
            $('#stats-table .table-row:first .message-box:first').height(height + 10);
        }, 100);
    }
</script>