﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ClientSearch.ascx.cs" Inherits="API_ClientSearch" %>

<div class="clientsearch-panel-loading panel-loading"></div>

<div class="searchclient-form">
    <asp:UpdatePanel ID="upClientSearch" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Panel ID="panelClientSearch" runat="server" DefaultButton="btnClientSearch" style="margin-bottom:10px; height:50px;">
                <div class="client-search">
                    <div style="<%=GetRepeatDirection() %>">
                        <asp:TextBox ID="txtClientSearch" runat="server" autocomplete="off" placeholder='n° compte, nom, prénom, etc. puis "Rechercher" >'></asp:TextBox>
                    </div>
                    <div style="<%=GetRepeatDirection() %>">
                        <asp:Button ID="btnClientSearch" runat="server" CssClass="search-button" Text="Rechercher" OnClick="btnClientSearch_Click" />
                    </div>
                </div>
                <asp:Panel ID="panelClientDetails" runat="server" Visible="false" style="padding:5px;border: 1px solid #bbb;background-color: #fafafa;border-top-width: 0;">
                    <asp:Label ID="lblAccountNumber" runat="server"></asp:Label>
                    <asp:Label ID="lblFirstName" runat="server"></asp:Label>
                    <asp:Label ID="lblLastName" runat="server"></asp:Label>
                    <asp:Label ID="lblBirthDate" runat="server"></asp:Label>
                </asp:Panel>
            </asp:Panel>
            <asp:HiddenField ID="hdnIndex" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
</div>

<div id="dialog-client-multi-choice" style="display:none;padding:0">
    <asp:UpdatePanel ID="upClientMultiChoice" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Repeater ID="rptClientChoice" runat="server">
                <HeaderTemplate>
                    <table class="defaultTable2">
                        <thead>
                            <tr>
                                <th>N° de compte</th>
                                <th>Prénom</th>
                                <th>Nom</th>
                                <th style="width:1px;">Date de naissance</th>
                            </tr>
                        </thead>
                        <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                            <tr class='<%#Container.ItemIndex % 2 == 0 ? "row-even" : "row-odd" %>' onclick='<%#String.Format("SelectSearchResult({0});", Container.ItemIndex) %>'>
                                <td style="text-align:center"><%#Eval("AccountNumber") %></td>
                                <td><%#Eval("FirstName") %></td>
                                <td><%#Eval("LastName") %></td>
                                <td style="text-align:center;"><%#Eval("BirthDate") %></td>
                            </tr>
                </ItemTemplate>
                <FooterTemplate>
                        </tbody>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
            <asp:Button ID="btnUpdateDetails" runat="server" OnClick="btnUpdateDetails_Click" style="display:none" />
        </ContentTemplate>
    </asp:UpdatePanel>
</div>

<script type="text/javascript">
    Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(ClientSearchBeginRequestHandler);
    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(ClientSearchEndRequestHandler);
    function ClientSearchBeginRequestHandler(sender, args) {
        try{
            pbControl = args.get_postBackElement();

            if (pbControl != null) {
                //console.log(pbControl.id);
                if (pbControl.id.indexOf('btnClientSearch') != -1 ||
                    pbControl.id.indexOf('btnUpdateDetails') != -1)
                    ShowClientSearchLoading('.searchclient-form');
            }
        }
        catch(e)
        {

        }
    }
    function ClientSearchEndRequestHandler(sender, args) {
        $('.clientsearch-panel-loading').hide();
    }
    function ShowClientSearchLoading(controlID) {
        if(controlID == null)
            controlID = '.file-upload-form';
        var panel = $(controlID);

        $('.clientsearch-panel-loading').show();
        $('.clientsearch-panel-loading').width(panel.outerWidth());
        $('.clientsearch-panel-loading').height(panel.outerHeight());

        $('.clientsearch-panel-loading').position({
            my: 'center',
            at: 'center',
            of: controlID
        });
    }

    function ShowSearchResult() {
        $('#dialog-client-multi-choice').dialog({
            draggable: false,
            resizable: false,
            width: 600,
            dialogClass: "no-close",
            modal: true,
            title: 'Votre recherche retourne plusieurs résultats',
            buttons: {
                Annuler: function () { $(this).dialog('destroy'); }
            }
        });
        $('#dialog-client-multi-choice').parent().appendTo(jQuery("form:first"));
    }
    function SelectSearchResult(ref) {
        $('#<%=hdnIndex.ClientID%>').val(ref);
        $('#<%=btnUpdateDetails.ClientID%>').click();
        $('#dialog-client-multi-choice').dialog('destroy');
    }
</script>