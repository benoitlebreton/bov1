﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class API_DropdownListMultiSelect : System.Web.UI.UserControl
{
    public DataTable ListChoicesSelected { get { return (ViewState["ListChoicesSelected"] != null) ? (DataTable)ViewState["ListChoicesSelected"] : null; } set { ViewState["ListChoicesSelected"] = value; } }
    public DataTable ListChoices { get { return (ViewState["ListChoices"] != null) ? (DataTable)ViewState["ListChoices"] : null; } set { ViewState["ListChoices"] = value; } }
    public bool AlphaOrder { get { return (ViewState["AlphaOrder"] != null) ? (bool)ViewState["AlphaOrder"] : false; } set { ViewState["AlphaOrder"] = value; } }
    public bool HideEdit { get { return (ViewState["HideEdit"] != null) ? bool.Parse(ViewState["HideEdit"].ToString()) : false; } set { ViewState["HideEdit"] = value; } }
    public event EventHandler choiceAdded;
    public event EventHandler choiceRemoved;

    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            if (HideEdit)
                panelAdd.Visible = !HideEdit;

            BindListChoice();

            Page.Header.Controls.Add(new System.Web.UI.LiteralControl("<link rel=\"stylesheet\" type=\"text/css\" href=\"" + ResolveUrl("~/Styles/AccountClosingControls.css") + "\" />"));
        }
    }

    protected void BindListChoice()
    {
        if (ListChoices != null)
        {
            DataTable dtTmp = ListChoices.Clone();
            dtTmp.Rows.Add(new object[] { "", "" });

            // REMOVE SELECTED ITEMS

            for (int i = 0; i < ListChoices.Rows.Count; i++)
            {
                bool bExists = false;
                if (ListChoicesSelected != null)
                    for (int j = 0; j < ListChoicesSelected.Rows.Count; j++)
                    {
                        if (ListChoicesSelected.Rows[j]["Value"].ToString() == ListChoices.Rows[i]["Value"].ToString())
                        {
                            bExists = true;
                            break;
                        }
                    }

                if (!bExists)
                    dtTmp.Rows.Add(ListChoices.Rows[i].ItemArray);
            }

            ddlChoice.Items.Clear();
            ddlChoice.DataSource = dtTmp;
            ddlChoice.DataBind();

            if (AlphaOrder)
                tools.ReorderAlphabetized(ddlChoice);
        }
    }

    public void BindListChoiceSelected()
    {
        rptChoice.DataSource = ListChoicesSelected;
        rptChoice.DataBind();
        upDropdownList.Update();
    }

    public void BindListChoiceSelected(string sListChoices)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("Value");
        dt.Columns.Add("Text");

        string[] arChoices = sListChoices.Split(';');

        for (int i = 0; i < arChoices.Length; i++)
        {
            for (int j = 0; j < ListChoices.Rows.Count; j++)
            {
                if (arChoices[i] == ListChoices.Rows[j]["Value"].ToString())
                    dt.Rows.Add(ListChoices.Rows[j].ItemArray);
            }
        }

        ListChoicesSelected = dt;
        BindListChoiceSelected();
    }

    protected void btnAdd_Click(object sender, ImageClickEventArgs e)
    {
        if (!String.IsNullOrWhiteSpace(ddlChoice.SelectedValue))
        {
            DataTable dt = ListChoicesSelected;
            if (dt == null)
            {
                dt = new DataTable();
                dt.Columns.Add("Value");
                dt.Columns.Add("Text");
            }

            dt.Rows.Add(new object[] { ddlChoice.SelectedValue, ddlChoice.SelectedItem.Text });
            ListChoicesSelected = dt;
            BindListChoiceSelected();

            BindListChoice();

            if (choiceAdded != null)
                choiceAdded(sender, e);
        }
    }

    protected void btnRemove_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton btn = (ImageButton)sender;

        HiddenField hdn = (HiddenField)btn.Parent.FindControl("hdnValue");
        Literal lit = (Literal)btn.Parent.FindControl("litText");

        DataTable dt = ListChoicesSelected;

        bool bExist = false;
        int iRow = 0;
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["Value"].ToString() == hdn.Value)
            {
                bExist = true;
                iRow = i;
                break;
            }
        }

        if (bExist)
        {
            dt.Rows.Remove(dt.Rows[iRow]);

            ListChoicesSelected = dt;
            BindListChoiceSelected();

            BindListChoice();

            if (choiceRemoved != null)
                choiceRemoved(sender, e);
        }
    }

    public DataTable GetSelectedChoices()
    {
        DataTable dt = null;

        if (ListChoicesSelected != null)
            dt = ListChoicesSelected.Copy();
        else
        {
            dt = new DataTable();
            dt.Columns.Add("Value");
            dt.Columns.Add("Text");
        }

        if (!String.IsNullOrWhiteSpace(ddlChoice.SelectedValue))
            dt.Rows.Add(new object[] { ddlChoice.SelectedValue, ddlChoice.SelectedItem.Text });

        return dt;
    }

    public void AddListChoice(string sText, string sValue, bool bAddToSelection)
    {
        object[] data = new object[] { sValue, sText };
        if (bAddToSelection)
        {
            if (ListChoicesSelected == null)
            {
                ListChoicesSelected = new DataTable();
                ListChoicesSelected.Columns.Add("Value");
                ListChoicesSelected.Columns.Add("Text");
            }

            ListChoicesSelected.Rows.Add(data);
        }
        else ListChoices.Rows.Add(data);

        BindListChoiceSelected();
        BindListChoice();

        if (bAddToSelection && choiceAdded != null)
            choiceAdded(new object(), new EventArgs());
    }
}