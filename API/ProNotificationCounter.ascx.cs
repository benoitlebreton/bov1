﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Xml.Linq;
using XMLMethodLibrary;

public partial class API_ProNotificationCounter : System.Web.UI.UserControl
{
    public int RefCustomer { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            BindProNotificationCounter();
        }
    }

    protected void BindProNotificationCounter()
    {
        int iNbNotification = GetProNotificationCounter();

        if (iNbNotification > 0)
            lblNbProNotification.Text = iNbNotification.ToString();
        else panelNbProNotification.Visible = false;
    }

    protected int GetProNotificationCounter()
    {
        string sDateFirst, sDateLast;
        string sToken = authentication.GetCurrent().sToken;
        int iNbEvent = GetNbAlertEvent(sToken, RefCustomer, 24, out sDateFirst, out sDateLast);
        int iNbMacro = GetNbMacroZendesk(sToken, RefCustomer, "NPRO", out sDateFirst, out sDateLast);

        return iNbEvent + iNbMacro;
    }

    protected int GetNbAlertEvent(string sCashierToken, int iRefCustomer, int iRefEvent, out string sDateFirst, out string sDateLast)
    {
        int iNbAlertEvent = 0;
        sDateFirst = "";
        sDateLast = "";
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("AML.P_GetNbEvents", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                            new XElement("Event",
                                                new XAttribute("CashierToken", sCashierToken),
                                                new XAttribute("RefCustomer", iRefCustomer.ToString()),
                                                new XAttribute("RefEvent", iRefEvent.ToString()))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if(sXmlOut.Length > 0)
            {
                List<string> listNbNotifications = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Event", "NbNotifications");
                List<string> listMinDate = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Event", "MINDATE");
                List<string> listMaxDate = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Event", "MAXDATE");

                iNbAlertEvent = int.Parse(listNbNotifications[0]);
                sDateFirst = listMinDate[0];
                sDateLast = listMaxDate[0];
            }
        }
        catch (Exception e)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return iNbAlertEvent;
    }

    protected int GetNbMacroZendesk(string sCashierToken, int iRefCustomer, string sTagMacro, out string sDateFirst, out string sDateLast)
    {
        int iNbMacro = 0;
        sDateFirst = "";
        sDateLast = "";
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("AML.P_GetNbMacrosZendesk", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                            new XElement("MacroZendesk",
                                                new XAttribute("CashierToken", sCashierToken),
                                                new XAttribute("RefCustomer", iRefCustomer.ToString()),
                                                new XAttribute("TagMacro", sTagMacro))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if (sXmlOut.Length > 0)
            {
                List<string> listNbMacro = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/MacroZendesk ", "NbMacrosZendesk");
                List<string> listMinDate = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/MacroZendesk ", "MINDATE");
                List<string> listMaxDate = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/MacroZendesk ", "MAXDATE");

                iNbMacro = int.Parse(listNbMacro[0]);
                sDateFirst = listMinDate[0];
                sDateLast = listMaxDate[0];
            }
        }
        catch (Exception e)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return iNbMacro;
    }
}