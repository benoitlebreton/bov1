﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DropdownListMultiSelect.ascx.cs" Inherits="API_DropdownListMultiSelect" %>

<asp:UpdatePanel ID="upDropdownList" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div>
            <asp:Repeater ID="rptChoice" runat="server">
                <ItemTemplate>
                    <div class="multi-item <%# Container.ItemIndex % 2 == 0 ? "even-row" : "odd-row" %>">
                        <div class="multi-row-data">
                            <asp:Literal ID="litText" runat="server" Text='<%# Eval("Text") %>'></asp:Literal>
                            <asp:HiddenField ID="hdnValue" runat="server" Value='<%# Eval("Value") %>' />
                        </div>
                        <div class="multi-row-btn"<%= HideEdit ? " style=\"display:none\"" : "" %>>
                            <asp:ImageButton ID="btnRemove" runat="server" ImageUrl="~/Styles/Img/moins.png" OnClick="btnRemove_Click" CssClass="minus" />
                        </div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </div>
        <asp:Panel ID="panelAdd" runat="server" CssClass="multi-row-footer">
            <asp:DropDownList ID="ddlChoice" runat="server" CssClass="multi-row-data" DataTextField="Text" DataValueField="Value">
            </asp:DropDownList>
            <div class="multi-row-btn">
                <asp:ImageButton ID="btnAdd" runat="server" ImageUrl="~/Styles/Img/plus.png" OnClick="btnAdd_Click" />
            </div>
            <div style="clear:both"></div>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>