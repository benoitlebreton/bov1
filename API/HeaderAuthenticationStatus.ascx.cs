﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class API_HeaderAuthenticationStatus : System.Web.UI.UserControl
{
    public bool isAuthenticated 
    { 
        get 
        { 
            bool bAuthenticated = false;
            bool.TryParse(hfIsAuthenticated.Value, out bAuthenticated);
            return bAuthenticated;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        lblAgent.Visible = false;
        divChangePassword.Visible = true;
        btnDisconnect.Text = Resources.res.LogOff;

        authentication auth = authentication.GetCurrent(false);

        if (auth != null && auth.sRef != null && auth.sRef.Length > 0)
        {
            hfIsAuthenticated.Value = true.ToString();
            divDisconnect.Visible = true;
            divStatus.Visible = false;

            divAgent.Visible = true;
            lblAgentName.Text = auth.sLastName + " " + auth.sFirstName;

            if (auth.bChangePassword)
            {
                divChangePassword.Visible = false;
                string sPageName = Request.Url.AbsoluteUri.Substring(Request.Url.AbsoluteUri.LastIndexOf('/') + 1);
                if (sPageName.ToLower().Trim() != "changepassword.aspx")
                    Response.Redirect("ChangePassword.aspx");
            }
        }
        else
        {
            hfIsAuthenticated.Value = false.ToString();
            divAgency.Visible = false;
            divAgent.Visible = false;
            //lblStatus.Text = Resources.res.Disconnected.ToUpper();
            lblStatus.Text = "";
            divDisconnect.Visible = false;
            divStatus.Visible = true;
        }

    }

    protected void onClickDisconnect(object sender, EventArgs e)
    {
        Session.RemoveAll();
        Response.Redirect("./Authentication.aspx");
    }
}