﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using XMLMethodLibrary;

public partial class API_ClientFileUpload : System.Web.UI.UserControl
{
    public event EventHandler ClientFileUploaded;
    public event EventHandler ClientFileUploadError;
    public event EventHandler DocumentTypeChanged;
    public event EventHandler ServerFileSelected;
    public int RefCustomer { get { return (ViewState["RefCustomer"] != null) ? int.Parse(ViewState["RefCustomer"].ToString()) : 0; } set { ViewState["RefCustomer"] = value; hdnRefCustomer.Value = value.ToString(); } }
    public bool HideSaveButton { get { return !panelSaveFile.Visible; } set { panelSaveFile.Visible = !value; } }
    public bool HideDesc { get { return !panelDesc.Visible; } set { panelDesc.Visible = !value; } }
    public string TypeFilters { get; set; }
    public string ServerFilePath { get { return (ViewState["ServerFilePath"] != null) ? ViewState["ServerFilePath"].ToString() : ""; } set { ViewState["ServerFilePath"] = value; } }
    public string DocumentType { get { return ddlDocumentType.SelectedValue; } }
    protected const int iNbMaxServerFile = 10;
    public bool ClearForm { get { return (ViewState["ClearForm"] != null) ? bool.Parse(ViewState["ClearForm"].ToString()) : false; } set { ViewState["ClearForm"] = value; } }
    public string AllowedDocType { get { return (ViewState["AllowedDocType"] != null) ? ViewState["AllowedDocType"].ToString() : ""; } set { ViewState["AllowedDocType"] = value; } }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindInputs();
        }

        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "$('.file-tabs').tabs();", true);
    }

    protected void BindInputs()
    {
        DataTable dtTabs = new DataTable();
        dtTabs.Columns.Add("href");
        dtTabs.Columns.Add("label");

        hdnClearForm.Value = ClearForm ? "1" : "0";

        if (!String.IsNullOrWhiteSpace(ServerFilePath))
        {
            panelServerFile.Visible = true;
            dtTabs.Rows.Add(new object[] { "#tab-server-file", "Sur le serveur" });

            string[] arFiles = Directory.GetFiles(HttpContext.Current.Server.MapPath("~/" + ServerFilePath));

            if (arFiles.Length > 0)
            {
                DataTable dtFiles = new DataTable();
                dtFiles.Columns.Add("filename");
                dtFiles.Columns.Add("url");

                string[] arRandomFiles = shuffle((string[])arFiles.Clone());
                if (arRandomFiles.Length > iNbMaxServerFile)
                    arRandomFiles = arRandomFiles.Take(iNbMaxServerFile).Select(i => i.ToString()).ToArray();

                for (int i = 0; i < arRandomFiles.Length; i++)
                {
                    // Get file creation date
                    FileInfo fi = new FileInfo(arRandomFiles[i]);

                    string sFilename = arRandomFiles[i].Substring(arRandomFiles[i].LastIndexOf('\\') + 1);
                    dtFiles.Rows.Add(new object[] { sFilename, ServerFilePath + sFilename + ";" + fi.CreationTime.ToString("dd/MM/yyyy") });
                }

                ddlServerFile.DataSource = dtFiles;
                ddlServerFile.DataBind();
            }
        }

        dtTabs.Rows.Add(new object[] { "#tab-zendesk-file", "Sur Zendesk" });
        dtTabs.Rows.Add(new object[] { "#tab-desktop-file", "Sur votre ordinateur" });
        rptFileTabs.DataSource = dtTabs;
        rptFileTabs.DataBind();

        DataTable dt = ClientFile.GetDocumentType();

        if (!String.IsNullOrWhiteSpace(TypeFilters))
        {
            DataTable dtNew = dt.Clone();
            string[] arTypeFilters = TypeFilters.Split(';');

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (arTypeFilters.Contains(dt.Rows[i]["TAG"]))
                    dtNew.Rows.Add(dt.Rows[i].ItemArray);
            }

            dt = dtNew;
        }

        ddlDocumentType.DataSource = dt;
        ddlDocumentType.DataBind();
    }

    protected string[] shuffle(string[] arString)
    {
        string[] shuffledArray = new string[arString.Length];
        int rndNo;

        Random rnd = new Random();
        for (int i = arString.Length; i >= 1; i--)
        {
            rndNo = rnd.Next(1, i + 1) - 1;
            shuffledArray[i - 1] = arString[rndNo];
            arString[rndNo] = arString[i - 1];
        }
        return shuffledArray;
    }

    protected void btnFileUploaded_Click(object sender, EventArgs e)
    {
        if (ClientFileUploaded != null)
            ClientFileUploaded(hdnFileName.Value, e);
    }
    protected void btnFileUploadError_Click(object sender, EventArgs e)
    {
        if (ClientFileUploadError != null)
            ClientFileUploadError(hdnFileName.Value, e);
    }

    protected void btnSearchTicketID_Click(object sender, EventArgs e)
    {
        string sTicketID = txtTicketID.Text;
        string sXmlOut = "";
        try
        {
            WS_Zendesk.WsZendeskClient wsZendesk = new WS_Zendesk.WsZendeskClient();
            sXmlOut = wsZendesk.GetAttachmentsFromTicketId(new XElement("ALL_XML_IN", new XElement("ID", sTicketID)).ToString());
        }
        catch (Exception ex)
        {

        }

        if (sXmlOut.Length > 0)
        {
            List<string> listRC = CommonMethod.GetTagValues(sXmlOut, "ALL_XML_OUT/RESULT_CODE");

            if (listRC.Count > 0 && listRC[0] == "0")
            {
                List<string> listFileURL = CommonMethod.GetTagValues(sXmlOut, "ALL_XML_OUT/URL");

                rptZendeskFile.Visible = false;
                panelNoZendeskFile.Visible = false;

                if (listFileURL.Count > 0)
                {
                    DataTable dt = new DataTable();
                    dt.Columns.Add("Filename");
                    dt.Columns.Add("URL");

                    string fileTag = "?name=";
                    for (int i = 0; i < listFileURL.Count; i++)
                    {
                        dt.Rows.Add(new object[] { Server.UrlDecode(listFileURL[i].Substring(listFileURL[i].LastIndexOf(fileTag) + fileTag.Length)), listFileURL[i] });
                    }

                    rptZendeskFile.DataSource = dt;
                    rptZendeskFile.DataBind();
                    rptZendeskFile.Visible = true;
                }
                else
                {
                    panelNoZendeskFile.Visible = true;
                }
            }
            else ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage(\"Erreur\", \"Veuillez vérifier le n° de ticket et réessayer.\");", true);
        }
    }

    protected void btnUploadZendeskFile_Click(object sender, EventArgs e)
    {
        string sUploadPath = HttpContext.Current.Server.MapPath("~/UploadTmp");
        // Remove untreated files
        ClientFile.RemoveUntreatedDocs(sUploadPath, 15);

        string sDocType = ddlDocumentType.SelectedValue;
        bool bError = false;
        bool bClearForm = true;

        foreach (RepeaterItem item in rptZendeskFile.Items)
        {
            if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
            {
                CheckBox ckb = item.FindControl("ckbSelect") as CheckBox;
                HiddenField hdn = item.FindControl("hdnURL") as HiddenField;
                Label lbl = item.FindControl("lblFilename") as Label;

                if (ckb != null && ckb.Checked
                    && hdn != null && !String.IsNullOrWhiteSpace(hdn.Value)
                    && lbl != null && !String.IsNullOrWhiteSpace(lbl.Text))
                {
                    string sOriginalFilename = ClientFile.CleanFileName(lbl.Text);
                    string sFileExtension = sOriginalFilename.Substring(sOriginalFilename.LastIndexOf('.'));

                    string sNewOriginalFileName = sOriginalFilename.Substring(0, sOriginalFilename.LastIndexOf('.'));
                    if (sNewOriginalFileName.Length > 40)
                        sNewOriginalFileName = sNewOriginalFileName.Substring(0, 40);
                    sNewOriginalFileName = String.Format("{0}_{1}_{2}{3}", sNewOriginalFileName, Guid.NewGuid(), DateTime.Now.ToString("ddMMyyyyHHmmss"), sFileExtension);
                    sOriginalFilename = sNewOriginalFileName;

                    if (IsDocTypeAllowed(sFileExtension))
                    {
                        string sRemoteUrl = Server.UrlDecode(hdn.Value);
                        string sTmpFilename = Guid.NewGuid().ToString() + "." + sFileExtension;
                        string sFileSavePath = Path.Combine(sUploadPath, sTmpFilename);
                        bool bOverwrite = bool.Parse(hdnOverwrite.Value);

                        try
                        {
                            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
                            WebClient wc = new WebClient();
                            wc.DownloadFile(sRemoteUrl, sFileSavePath);

                            string sContentType = wc.ResponseHeaders[HttpResponseHeader.ContentType];

                            int iRC;
                            string sErrorMessage;

                            if (ClientFile.SaveFileCustomerDir(RefCustomer, "/UploadTmp/", sTmpFilename, sOriginalFilename, bOverwrite, true, out iRC, out sErrorMessage))
                            {
                                if (ClientFile.SaveFileDB(authentication.GetCurrent().sToken, RefCustomer, sContentType, sOriginalFilename, ddlDocumentType.SelectedValue, txtDesc.Text))
                                {
                                    if (ClientFileUploaded != null)
                                        ClientFileUploaded(sOriginalFilename, e);
                                }
                                else
                                {
                                    iRC = 9994;
                                    bError = true;
                                }
                            }
                            else if (iRC == 9995)
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "ShowOverwriteDialog();", true);
                                bClearForm = false;
                            }
                            else bError = true;
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                    else bError = true;
                }
            }
        }

        if (bError)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage(\"Attention\", \"Une erreur est survenue lors de l'enregistrement d'un ou plusieurs fichiers.\");", true);

            if (ClientFileUploadError != null)
                ClientFileUploadError(sender, e);
        }

        bClearForm = ClearForm; // UC param overwrite everything
        if (bClearForm)
        {
            ddlDocumentType.SelectedIndex = 0;
            txtDesc.Text = "";
            upOtherInputs.Update();
            txtTicketID.Text = "";
            rptZendeskFile.Visible = false;
        }
    }

    public void InitInputFiles()
    {
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "InitInputFiles();", true);
    }

    protected void btnUploadServerFile_Click(object sender, EventArgs e)
    {
        bool bOverwrite = bool.Parse(hdnOverwrite.Value);
        bool bClearForm = true, bError = false;

        try
        {
            int iRC;
            string sErrorMessage;
            string sFilename = ddlServerFile.SelectedItem.Text;
            string sOriginalFilename = ClientFile.CleanFileName(ddlServerFile.SelectedItem.Text);
            string sContentType = "";

            if (ClientFile.SaveFileCustomerDir(RefCustomer, ServerFilePath, sFilename, sOriginalFilename, bOverwrite, false, out iRC, out sErrorMessage))
            {
                if (ClientFile.SaveFileDB(authentication.GetCurrent().sToken, RefCustomer, sContentType, sOriginalFilename, ddlDocumentType.SelectedValue, txtDesc.Text))
                {
                    if (ClientFileUploaded != null)
                        ClientFileUploaded(sFilename, e);
                }
                else
                {
                    iRC = 9994;
                    bError = true;
                }
            }
            else if (iRC == 9995)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "ShowOverwriteDialog();", true);
                bClearForm = false;
            }
            else bError = true;
        }
        catch (Exception ex)
        {

        }

        if (bError)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage(\"Attention\", \"Une erreur est survenue lors de l'enregistrement du fichier.\");", true);

            if (ClientFileUploadError != null)
                ClientFileUploadError(sender, e);
        }

        if (bClearForm)
        {
            ddlServerFile.SelectedIndex = 0;
            ddlDocumentType.SelectedIndex = 0;
            txtDesc.Text = "";
            upOtherInputs.Update();
        }
    }

    protected void ddlDocumentType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DocumentTypeChanged != null)
            DocumentTypeChanged(sender, e);
    }

    public void ManualUploadScript()
    {
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "UploadFile();", true);
    }

    protected void ddlServerFile_SelectedIndexChanged(object sender, EventArgs e)
    {
        string sFilename = ddlServerFile.SelectedItem.Text;
        string sFileCreationDate = ddlServerFile.SelectedValue.Split(';')[1];

        if (ServerFileSelected != null)
            ServerFileSelected(String.Format("{0};{1}", sFilename, sFileCreationDate), e);
    }

    protected bool IsDocTypeAllowed(string sDocType)
    {
        bool bAllowed = true;

        if(!String.IsNullOrWhiteSpace(AllowedDocType))
        {
            bAllowed = false;

            string[] arAllowedDocType = AllowedDocType.Split(',');

            for(int i =0;i<arAllowedDocType.Length;i++)
            {
                if(arAllowedDocType[i] == sDocType)
                {
                    bAllowed = true;
                    break;
                }
            }
        }

        return bAllowed;
    }

    public void RefreshAllowedDocType()
    {
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "InitInputFiles();", true);
        upDeskFileUp.Update();
    }
}