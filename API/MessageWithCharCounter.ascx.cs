﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class API_MessageWithCharCounter : System.Web.UI.UserControl
{
    public int MaxLength { get { return (ViewState["MaxLength"] != null) ? int.Parse(ViewState["MaxLength"].ToString()) : 0; } set { ViewState["MaxLength"] = value; } }
    public bool Splitted { get { return (ViewState["Splitted"] != null) ? bool.Parse(ViewState["Splitted"].ToString()) : false; } set { ViewState["Splitted"] = value; } }
    public int SplitLength { get { return (ViewState["SplitLength"] != null) ? int.Parse(ViewState["SplitLength"].ToString()) : 0; } set { ViewState["SplitLength"] = value; } }
    public string SplittedKindMessage { get { return lblSplittedKindMessage.Text; } set { lblSplittedKindMessage.Text = value; } }
    public bool SplittedKindMessagePlural { get { return (ViewState["SplittedKindMessagePlural"] != null) ? bool.Parse(ViewState["SplittedKindMessagePlural"].ToString()) : true; } set { ViewState["SplittedKindMessagePlural"] = value; } }
    public string Message { get { return txtMessage.Text; } set { txtMessage.Text = value; } }
    public string Label { get { return lblTextLabel.Text; } set { lblTextLabel.Text = value; } }
    public bool ReadOnly { set { txtMessage.ReadOnly = value; } get { return txtMessage.ReadOnly; } }
    public bool Disabled { set { if (value) txtMessage.Attributes["disabled"] = "disabled"; else txtMessage.Attributes.Remove("disabled"); } get { return (txtMessage.Attributes["disabled"] != null && txtMessage.Attributes["disabled"] == "disabled") ? true : false; } }
    public int Height { set { txtMessage.Height = new Unit(value, UnitType.Pixel); } get { return (int)txtMessage.Height.Value; } }

    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            bool bAddJS = false;

            panelSplittedCharCounter.Visible = false;
            panelCharCounter.Visible = true;
            if (MaxLength != -1)
            {
                txtMessage.Attributes["maxlength"] = MaxLength.ToString();
                lblMaxChar.Text = MaxLength.ToString();
                bAddJS = true;
            }
            else
            {
                panelCharCounter.Visible = false;
            }

            if(Splitted && SplitLength > 0)
            {
                if (string.IsNullOrEmpty(SplittedKindMessage.Trim())) { SplittedKindMessage = "message"; }
                panelSplittedCharCounter.Visible = true;
                panelCharCounter.Visible = false;
                bAddJS = true;
            }

            if (bAddJS)
            {
                ScriptManager.RegisterClientScriptInclude(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "Scripts/MessageWithCharCounter.js?v=20171116_1657");
            }

            Page.Header.Controls.Add(new System.Web.UI.LiteralControl("<link rel=\"stylesheet\" type=\"text/css\" href=\"" + ResolveUrl("~/Styles/MessageWithCharCounter.css") + "?v=20171115\" />"));
        }
    }

    public void BindMessageLengthCheck()
    {
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "BindMessageLengthCheck('" + txtMessage.ClientID + "', '" + lblNbChar.ClientID + "');", true);
    }

    public void BindSplittedMessageLengthCheck()
    {
        //inputID, nbCharCounterID, splittedCounterID, splittedLength, splittedKindMessageID, plural, message
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "BindSplittedMessageLengthCheck('" + 
            txtMessage.ClientID + "', '" + lblSplittedNbChar.ClientID + "', '" + lblSplittedCounter.ClientID + "', '" + SplitLength + "', '" + lblSplittedKindMessage.ClientID + "', " + SplittedKindMessagePlural.ToString().ToLower() + ", '"+lblSplittedKindMessagePlural.ClientID+"');", true);
    }

    public void MessageFocus()
    {
        txtMessage.Focus();
    }
}