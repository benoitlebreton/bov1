﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DocViewer.ascx.cs" Inherits="DocViewer" %>
<script language="javascript" type="text/javascript">
    function showFlex() {
        document.getElementById("PdfObjDiv").style.display = "none";
        document.getElementById("iFrameDiv").style.display = "none";

        document.getElementById("FlexDiv").style.display = "block";

        document.getElementById("btnChangeMode").style.visibility = "hidden";
        document.getElementById("btnChangeMode").value = "Fichier original";
        document.getElementById("btnChangeMode").onclick = showPdf;
    }

    function showPdf(filePath, goBack) {
        document.getElementById("FlexDiv").style.display = "none";

        if (navigator.appVersion.indexOf("MSIE") == -1 && filePath.substring(filePath.lastIndexOf('.') + 1, filePath.length).toLowerCase() != "bmp") {
            var PdfObj = document.getElementById("PdfObj");
            PdfObj.data = filePath;
            PdfObj.innerHTML = "<param name='src' value='" + filePath + "'/>Cet objet nécessite un plugin pour être lu ou le fichier n'est pas accessible.<br/>Vous pouvez tenter d'ouvrir directement le fichier par ce <a href='" + filePath + "'>lien</a>";
            document.getElementById("PdfObjDiv").style.display = "block";
        }
        else {
            var iFrame = document.getElementById("iFrame");
            iFrame.src = filePath;
            document.getElementById("iFrameDiv").style.display = "block";
        }

        if (goBack == true)
            document.getElementById("btnChangeMode").style.visibility = "visible";
        document.getElementById("btnChangeMode").value = "Fichier traité";
        document.getElementById("btnChangeMode").onclick = showFlex;
    }
</script>

<asp:HiddenField ID="originalFilePath" runat="server" />
<asp:HiddenField ID="firstFilePath" runat="server" />
<asp:HiddenField ID="secondFilePath" runat="server" />

<div id="FlexDiv" style="width:767px;height:352px;margin:auto;background: url(Styles/Img/loading.gif) 50% 50% no-repeat;">
    <script type="text/javascript" src="MG_API/ImageViewer/swfobject.js"></script>
    <script type="text/javascript">

        // Récupération du ref doc dans l'url
        var refDoc = location.search.substring(location.search.lastIndexOf("=", location.search.length) + 1, location.search.length);
        var originalFilePath = document.getElementById('<%=originalFilePath.ClientID %>').value;
        var firstFilePath = document.getElementById('<%=firstFilePath.ClientID %>').value;
        var secondFilePath = document.getElementById('<%=secondFilePath.ClientID %>').value;

        //alert(originalFilePath + "-" + firstFilePath + "-" + secondFilePath);

        var swfVersionStr = "10.0.0";
        var xiSwfUrlStr = "MG_API/ImageViewer/playerProductInstall.swf";
        var flashvars = {
            refDoc: parseInt(refDoc),
            originalFilePath: originalFilePath.toString(),
            firstFilePath: firstFilePath.toString(),
            secondFilePath: secondFilePath.toString()
        };
        var params = {};
        params.quality = "high";
        params.bgcolor = "#ffffff";
        params.allowscriptaccess = "sameDomain";
        params.allowfullscreen = "true";
        params.wmode = "transparent";
        var attributes = {};
        attributes.id = "ImageViewer";
        attributes.name = "ImageViewer";
        attributes.align = "middle";
        swfobject.embedSWF(
            "MG_API/ImageViewer/ImageViewer.swf", "flashContent",
            "767", "352",
            swfVersionStr, xiSwfUrlStr,
            flashvars, params, attributes);
        swfobject.createCSS("#flashContent", "display:block;text-align:left;");
    </script>
                                                
    <div id="flashContent">
	    <p>
    	    To view this page ensure that Adobe Flash Player version 
		    10.0.0 or greater is installed. 
	    </p>
	    <script type="text/javascript">
	        var pageHost = ((document.location.protocol == "https:") ? "https://" : "http://");
	        document.write("<a href='http://www.adobe.com/go/getflashplayer'><img src='"
						    + pageHost + "www.adobe.com/images/shared/download_buttons/get_flash_player.gif' alt='Get Adobe Flash player' /></a>"); 
	    </script> 
    </div>

    <noscript>
        <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="767" height="352" id="ImageViewer">
            <param name="movie" value="MG_API/ImageViewer/ImageViewer.swf" />
            <param name="quality" value="high" />
            <param name="bgcolor" value="#ffffff" />
            <param name="allowScriptAccess" value="sameDomain" />
            <param name="allowFullScreen" value="true" />
            <param name="wmode" value="transparent" />
            <!--[if !IE]>-->
            <object type="application/x-shockwave-flash" data="MG_API/ImageViewer/ImageViewer.swf" width="750" height="350">
                <param name="quality" value="high" />
                <param name="bgcolor" value="#ffffff" />
                <param name="allowScriptAccess" value="sameDomain" />
                <param name="allowFullScreen" value="true" />
                <param name="wmode" value="transparent" />
            <!--<![endif]-->
            <!--[if gte IE 6]>-->
        	    <p> 
        		    Either scripts and active content are not permitted to run or Adobe Flash Player version
        		    10.0.0 or greater is not installed.
        	    </p>
            <!--<![endif]-->
                <a href="http://www.adobe.com/go/getflashplayer">
                    <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash Player" />
                </a>
            <!--[if !IE]>-->
            </object>
            <!--<![endif]-->
        </object>
    </noscript>
</div>

<div id="PdfObjDiv" style="display:none;width:767px;height:352px;margin:auto; background: url(Styles/Img/loading.gif) 50% 50% no-repeat;border:1px solid #888">
    <object id="PdfObj" type="application/pdf" data="" style="width:765px;height:350px;z-index:100">
        <param name="src" value=""/>
        <param name="wmode" value="transparent" />Cette page nécessite le plug-in Flash et javascript.
    </object>
</div>

<div id="iFrameDiv" style="display:none;width:767px;height:352px;margin:auto; background: url(Styles/Img/loading.gif) 50% 50% no-repeat;border:1px solid #888;z-index:100">
    <iframe id="iFrame" src="" width="765px" height="350px" style="z-index:100">Votre navigateur ne supporte pas les iFrames.</iframe>
</div>

<input type="button" id="btnChangeMode" class="button" value="<-" onclick="javascript:showFlex()" style="visibility:hidden"/>
