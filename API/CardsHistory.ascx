﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CardsHistory.ascx.cs" Inherits="API_CardsHistory" %>

<asp:Panel ID="panelCardsHistory" runat="server">
    <asp:Panel ID="panelCardsHistoryList" runat="server">
        <asp:Repeater ID="rptCardsHisto" runat="server">
            <HeaderTemplate>
                <table style="width:100%;border-collapse:collapse;" class="tableHistory">
                    <tr>
                        <th style="text-align:left;">N&deg; carte</th>
                        <th>Statut</th>
                        <th>Date d'expiration</th>
                        <th style="">Limite SAB</th>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                    <tr class="table-row <%# Container.ItemIndex % 2 == 0 ? "default-table-row-style" : "table-alternate-row-style" %>">
                        <td style="width:150px">
                            <asp:Label ID="lblCardNumber" runat="server" Text='<%#Eval("CardNumber") %>'></asp:Label>
                        </td>
                        <td style="width:200px;text-align:center">
                            <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("Status") %>'></asp:Label>
                        </td>
                        <td style="width:150px;text-align:center">
                            <asp:Label ID="lblExpirationDate" runat="server" Text='<%#Eval("ExpirationDate") %>'></asp:Label>
                        </td>
                        <td>
                            <span style="display:inline-block;width:80px;font-weight:bold">Achat :</span><asp:Label ID="lblPaymentLimit" runat="server" Text='<%#Eval("PaymentLimit") %>'></asp:Label>&euro; sur <asp:Label ID="lblPaymentPeriod" runat="server" Text='<%#Eval("PaymentPeriod") %>'></asp:Label>j<br />
                            <span style="display:inline-block;width:80px;font-weight:bold">Retrait :</span><asp:Label ID="lblWithdrawMoneyLimit" runat="server" Text='<%#Eval("WithdrawMoneyLimit") %>'></asp:Label>&euro; sur <asp:Label ID="lblWithdrawMoneyPeriod" runat="server" Text='<%#Eval("WithdrawMoneyPeriod") %>'></asp:Label>j
                        </td>
                    </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </asp:Panel>
    <asp:Panel ID="panelCardsHistoryListEmpty" runat="server">Aucune carte</asp:Panel>
</asp:Panel>
