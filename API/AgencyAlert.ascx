﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AgencyAlert.ascx.cs" Inherits="API_AgencyAlert" %>

<link rel="stylesheet" href="Styles/toggles-soft.css" type="text/css" media="all" />
<style type="text/css">
    .ui-autocomplete {
        max-height: 130px;
    }

    .ui-combobox {
        position: relative;
        display: inline-block;
    }

    .ui-combobox-toggle {
        position: absolute;
        top: 0;
        bottom: 0;
        right:-2px;
        height:26px;
        margin-left: -1px;
        padding: 0;
        /* support: IE7 */
        *height: 1.7em;
        *top: 0.1em;
    }

    .ui-combobox-input {
        margin: 0;
        padding: 0.3em;
        width: 275px;
        height: 15px;
        border-top-right-radius:5px;
        border-bottom-right-radius:5px;
    }

    #popup-details input[type=checkbox] {
        width:20px;
        height:20px;
        position:relative;
        top:4px;
        cursor:pointer;
    }

    #popup-details .hidden {
        display:none;
    }
</style>

<script type="text/javascript" src="Scripts/toggles.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#<%= ddlAgencyID.ClientID %>").combobox();
        date_picker();
        $("#<%=panelExport.ClientID%>").accordion({
            animate: true,
            heightStyle: "content",
            active: false,
            collapsible: true
        });
    });
    function ShowDetailsPopup() {
        $('#popup-details').dialog({
            title: "Détails de l'incident",
            width: 600,
            draggable: false,
            resizable: false,
            modal: true,
            dialogClass : 'dialog-agency-alert'
        });
        $('#popup-details').parent().appendTo(jQuery("form:first"));
    }
    function HideDetailsPopup() {
        $('#popup-details').dialog('close');
    }
    function ShowPopupLoading() {
        $('.alert-details-loading').show();
        $('.alert-details-loading').width($('.dialog-agency-alert').outerWidth());
        $('.alert-details-loading').height($('.dialog-agency-alert').outerHeight());

        $('.alert-details-loading').position({
            my: 'center',
            at: 'center',
            of: $('.dialog-agency-alert')
            });
    }

    function ShowAlertMessage(message) {
        $('#agency-alert-message').html(message);
        $('#popup-agency-alert-message').dialog({
            autoOpen: true,
            resizable: false,
            draggable: false,
            modal: true,
            title: "Message",
            buttons: [{ text: 'Fermer', click: function () { $(this).dialog('close'); } }]
        });
    }

    function GetAlertDetails(refAlert) {
        $('#<%=hdnRefAlertToSearch.ClientID%>').val(refAlert);
        $('#<%=btnGetAlertDetails.ClientID%>').click();
    }

    (function ($) {
        $.widget("ui.combobox", {
            _create: function () {
                var input,
                    that = this,
                    wasOpen = false,
                    select = this.element.hide(),
                    selected = select.children(":selected"),
                    value = selected.val() ? selected.text() : "",
                    wrapper = this.wrapper = $("<span>")
                    .addClass("ui-combobox")
                    .insertAfter(select);
                function removeIfInvalid(element) {
                    var value = $(element).val(),
                        matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex(value) + "$", "i"),
                        valid = false;
                    select.children("option").each(function () {
                        if ($(this).text().match(matcher)) {
                            this.selected = valid = true;
                            return false;
                        }
                    });
                    if (!valid) {
                        // remove invalid value, as it didn't match anything
                        $(element)
                            .val("")
                            .attr("title", value + " n'existe pas")
                            .tooltip("open");
                        select.val("");
                        setTimeout(function () {
                            input.tooltip("close").attr("title", "");
                        }, 2500);
                        input.data("ui-autocomplete").term = "";
                    }
                }
                input = $("<input>")
                    .appendTo(wrapper)
                    .val(value)
                    .attr("title", "")
                    .addClass("ui-state-default ui-combobox-input")
                    .autocomplete({
                        delay: 0,
                        minLength: 0,
                        source: function (request, response) {
                            var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
                            response(select.children("option").map(function () {
                                var text = $(this).text();
                                if (this.value && (!request.term || matcher.test(text)))
                                    return {
                                        label: text.replace(
                                            new RegExp(
                                            "(?![^&;]+;)(?!<[^<>]*)(" +
                                            $.ui.autocomplete.escapeRegex(request.term) +
                                            ")(?![^<>]*>)(?![^&;]+;)", "gi"
                                            ), "<strong>$1</strong>"),
                                        value: text,
                                        option: this
                                    };
                            }));
                        },
                        select: function (event, ui) {
                            ui.item.option.selected = true;
                            that._trigger("selected", event, {
                                item: ui.item.option
                            });
                        },
                        change: function (event, ui) {
                            if (!ui.item) {
                                removeIfInvalid(this);
                            }
                        }
                    })
                    .addClass("ui-widget ui-widget-content ui-corner-left");
                input.data("ui-autocomplete")._renderItem = function (ul, item) {
                    return $("<li>")
                    .append("<a>" + item.label + "</a>")
                    .appendTo(ul);
                };
                $("<a>")
                    .attr("tabIndex", -1)
                    .attr("title", "Afficher tout")
                    .tooltip()
                    .appendTo(wrapper)
                    .button(
                        {
                            icons: {
                                primary: "ui-icon-triangle-1-s"
                            },
                            text: false
                        }
                    )
                    .removeClass("ui-corner-all")
                    .addClass("ui-corner-right ui-combobox-toggle")
                    .mousedown(function () {
                        wasOpen = input.autocomplete("widget").is(":visible");
                    })
                    .click(function () {
                        input.focus();
                        // close if already visible
                        if (wasOpen) {
                            return;
                        }
                        // pass empty string as value to search for, displaying all results
                        input.autocomplete("search", "");
                    });
                input.tooltip({
                    tooltipClass: "ui-state-highlight"
                });
            },
            _destroy: function () {
                this.wrapper.remove();
                this.element.show();
            }
        });

        //$('.tooltip').tooltip({ items: "span[tooltip]" });
    })(jQuery);

    function date_picker() {
        $('#<%=txtDateTo.ClientID %>').mask("99/99/9999");
        $('#<%=txtDateFrom.ClientID %>').mask("99/99/9999");
        $('#<%=txtStartDate.ClientID %>').mask("99/99/9999");

        $.datepicker.setDefaults($.datepicker.regional["fr"]);

        $('#<%=txtDateFrom.ClientID %>').datepicker($.datepicker.regional['fr'] = {
            closeText: 'Fermer',
            prevText: '&#x3c;Préc',
            nextText: 'Suiv&#x3e;',
            currentText: 'Courant',
            monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
            monthNamesShort: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
            dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
            dayNamesShort: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],
            dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
            weekHeader: 'Sm',
            dateFormat: 'dd/mm/yy',
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: '',
            changeMonth: true,
            changeYear: true,
            defaultDate: 0,
            minDate: new Date(2013, 1 - 1, 1)
        });


        $('#<%=txtDateTo.ClientID %>').datepicker($.datepicker.regional['fr'] = {
            closeText: 'Fermer',
            prevText: '&#x3c;Préc',
            nextText: 'Suiv&#x3e;',
            currentText: 'Courant',
            monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
            monthNamesShort: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
            //monthNamesShort: ['Jan', 'Fév', 'Mar', 'Avr', 'Mai', 'Jun', 'Jul', 'Aoû', 'Sep', 'Oct', 'Nov', 'Déc'],
            dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
            dayNamesShort: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],
            dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
            weekHeader: 'Sm',
            dateFormat: 'dd/mm/yy',
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: '',
            changeMonth: true,
            changeYear: true,
            defaultDate: 0,
            minDate: new Date(2013, 1 - 1, 1)
        });

        $('#<%=txtStartDate.ClientID %>').datepicker($.datepicker.regional['fr'] = {
            closeText: 'Fermer',
            prevText: '&#x3c;Préc',
            nextText: 'Suiv&#x3e;',
            currentText: 'Courant',
            monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
            monthNamesShort: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
            //monthNamesShort: ['Jan', 'Fév', 'Mar', 'Avr', 'Mai', 'Jun', 'Jul', 'Aoû', 'Sep', 'Oct', 'Nov', 'Déc'],
            dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
            dayNamesShort: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],
            dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
            weekHeader: 'Sm',
            dateFormat: 'dd/mm/yy',
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: '',
            changeMonth: true,
            changeYear: true,
            //defaultDate: "-1w",
            //minDate: "-1y",
            maxDate: "-1d"
        });

    }

    function initTooltip() {
        $('.trimmer').each(function () {
            //console.log($(this).html());
            if (this.offsetWidth < this.scrollWidth) {
                $(this).tooltip({
                    position: {
                        my: "center bottom-10",
                        at: "center top",
                        using: function (position, feedback) {
                            $(this).css(position);
                            $("<div>")
                                .addClass("arrow")
                                .addClass(feedback.vertical)
                                .addClass(feedback.horizontal)
                                .appendTo(this);
                        }
                    }
                });
            }
            else {
                $(this).removeAttr('title');
            }
        });

        $('.image-tooltip').tooltip({
            position: {
                my: "center bottom-10",
                at: "center top",
                using: function (position, feedback) {
                    $(this).css(position);
                    $("<div>")
                        .addClass("arrow")
                        .addClass(feedback.vertical)
                        .addClass(feedback.horizontal)
                        .appendTo(this);
                }
            }
        });
    }

    function InitToggles() {
        $('#toggleAlertTreated').toggles({
            text: { on: 'oui', off: 'non' },
            drag: false,
            on: $('#<%=ckbTreated.ClientID%>').is(':checked'),
                        checkbox: $('#<%=ckbTreated.ClientID%>'),
            width: 109,
            height: 20
        });
        $('#toggleFalseAlarm').toggles({
            text: { on: 'oui', off: 'non' },
            drag: false,
            on: $('#<%=ckbFalseAlarm.ClientID%>').is(':checked'),
            checkbox: $('#<%=ckbFalseAlarm.ClientID%>'),
            width: 125,
            height: 20
        });
    }

    function CheckExportForm() {
        var bOk = ($('#<%=txtStartDate.ClientID%>').val().length == 10) ? true : false;

        if (bOk)
            setTimeout(function () { $('#<%=btnExport.ClientID%>').attr('disabled', true); }, 200);

        return bOk;
    }
</script>

<div>

    <div id="ar-agency-alert-loading" style="display:none;position:absolute;background:url('Styles/img/loading2.gif') no-repeat center;background-size: 35px 27px;background-color:rgba(249, 249, 249, 0.5);z-index:100"></div>

    <asp:Panel ID="panelExport" runat="server">
        <h3 style="text-transform:uppercase">
            Export Excel
        </h3>
        <div id="ClientGlobalSearch" style="margin:auto;font-size:0.9em;line-height:1em;">
            <asp:UpdatePanel ID="upExport" runat="server">
                <ContentTemplate>
                    <div style="display:inline-block;">
                        <b>Depuis le</b><br />
                        <asp:TextBox ID="txtStartDate" runat="server" MaxLength="10" Width="100px" Height="20px" style="text-align:center"></asp:TextBox>
                    </div>
                    <asp:Button ID="btnExport" runat="server" Text="Télécharger l'export" CssClass="button" OnClientClick="return CheckExportForm()" OnClick="btnExport_Click" style="margin-left:10px" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </asp:Panel>

    <asp:Panel ID="panelFilters" runat="server" style="width:100%;margin:30px auto 0 auto">

        <div style="display:flex;flex-direction:row;align-items:flex-end;justify-content:space-between">
            <div>
                <b>Date</b><br />
                du <em>(jj/mm/aaaa)</em><br />
                <asp:TextBox ID="txtDateFrom" runat="server" Width="100px" Height="20px" style="text-align:center"></asp:TextBox>
            </div>
            <div>
                au <em>(jj/mm/aaaa)</em><br />
                <asp:TextBox ID="txtDateTo" runat="server" Width="100px" Height="20px" style="text-align:center"></asp:TextBox>
            </div>
            <div>
                <b>Type d'incident</b><br />
                <asp:DropDownList ID="ddlAlertType" runat="server" Height="26px" DataTextField="DescriptionAlerte" DataValueField="CodeAlerte" AppendDataBoundItems="true" style="max-width:150px">
                    <asp:ListItem Value="" Text=""></asp:ListItem>
                </asp:DropDownList>
            </div>
            <div>
                <b>Traité</b><br />
                <asp:DropDownList ID="ddlTreated" runat="server" Height="26px">
                    <asp:ListItem Text="" Value=""></asp:ListItem>
                    <asp:ListItem Text="Oui" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Non" Value="0"></asp:ListItem>
                </asp:DropDownList>
            </div>
            <div style="text-align:center">
                <asp:Image ID="Image2" runat="server" ImageUrl="~/Styles/Img/alerte_kyc_fausse_alerte.png" Height="25px" CssClass="image-tooltip" ToolTip="Fausse alerte" /><br />
                <asp:DropDownList ID="ddlFalseAlarm" runat="server" Height="26px">
                    <asp:ListItem Text="" Value=""></asp:ListItem>
                    <asp:ListItem Text="Oui" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Non" Value="0"></asp:ListItem>
                </asp:DropDownList>
            </div>
            <div>
                <b>Type d'action</b><br />
                <asp:DropDownList ID="ddlActionType" runat="server" Height="26px" DataTextField="Description" DataValueField="IdAction" AppendDataBoundItems="true" style="max-width:150px">
                    <asp:ListItem Value="" Text=""></asp:ListItem>
                </asp:DropDownList>
            </div>
            <asp:Panel ID="panelAgencyControl" runat="server" CssClass="cell" style="vertical-align:top">
                <b>Point de vente</b><br />
                <asp:DropDownList ID="ddlAgencyID" runat="server" DataTextField="AgencyName" DataValueField="AgencyID"
                    AppendDataBoundItems="true" Height="20px">
                    <asp:ListItem Selected="True" Value=""></asp:ListItem>
                </asp:DropDownList>
            </asp:Panel>
        </div>

        <div style="text-align:center;margin-top:10px">
            <asp:Button ID="btnSearch" runat="server" Text="Rechercher" OnClick="btnSearch_Click" CssClass="button" />
        </div>
    </asp:Panel>
    <asp:UpdatePanel ID="upSearch" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnPreviousPage" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnNextPage" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="ddlPage" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="ddlNbResult" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
        </Triggers>
        <ContentTemplate>

            <asp:Panel ID="panelGrid" runat="server" Visible="false" style="margin-top:0">
                <div id="divGridSettings" runat="server" style="display: table; width: 100%; margin-bottom: 10px">
                    <div style="display: table-row">
                        <div style="display: table-cell; width: 50%; padding-top: 20px">
                            <asp:Label ID="lblNbOfResults" runat="server">0</asp:Label>
                        </div>
                        <div style="display: table-cell; width: 50%; text-align: right;">
                            Nb de résultats par page :
                            <asp:DropDownList ID="ddlNbResult" runat="server" AutoPostBack="true" OnSelectedIndexChanged="onChangeNbResult">
                                <asp:ListItem Value="50" Selected="True">50</asp:ListItem>
                                <asp:ListItem Value="75">75</asp:ListItem>
                                <asp:ListItem Value="100">100</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div id="div-grid">
                    <div style="border: 3px solid #344b56; border-top-left-radius: 8px; border-top-right-radius: 8px;margin-top:5px">
                        <asp:GridView ID="gvKYCAlert" runat="server" AllowSorting="true"
                            AllowPaging="false" AutoGenerateColumns="False" GridLines="None" CellPadding="10"
                            DataKeyNames="RefAlert" ShowHeader="true" Width="100%" SortedDescendingCellStyle-Font-Overline="true"
                            ViewStateMode="Disabled" BorderWidth="0" BackColor="Transparent" OnRowDataBound="gvKYCAlert_RowDataBound" OnSorting="gvKYCAlert_Sorting">
                            <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="#344b56" Font-Bold="true" />
                            <EmptyDataTemplate>
                                Aucune alerte
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField Visible="false" InsertVisible="false">
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hdnRefAlert" runat="server" Value='<%# Eval("RefAlert")%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField SortExpression="DateAlerte" HeaderText="Date" InsertVisible="false" ItemStyle-Width="70" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAlertDate" runat="server" Text='<%# Eval("NoConformDate") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Type" InsertVisible="false" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAlertType" runat="server" Text='<%# Eval("AlertType") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Agence" InsertVisible="false" ControlStyle-CssClass="trimmer" ControlStyle-Width="400px">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAgencyName" runat="server" Text='<%# Eval("AgencyName") %>' CssClass="tooltip" ToolTip='<%# Eval("AgencyName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Etat" InsertVisible="false" ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAlertStatus" runat="server" Text='<%# Eval("StatusLabel") %>' ForeColor='<%# (Eval("Status").ToString() == "1") ? System.Drawing.Color.Green : System.Drawing.Color.Orange  %>' Font-Bold="true" style="text-transform:uppercase"></asp:Label>
                                        <asp:Panel ID="panelIcons" runat="server" style="position:relative;top:4px;">
                                            <asp:Image ID="imgComment" runat="server" ImageUrl="~/Styles/Img/alerte_kyc_commentaire.png" Height="25px" CssClass="image-tooltip" ToolTip='<%# String.Format("<b style=\"text-decoration:underline\">Commentaire :</b> <br/>{0}", Eval("Comment").ToString()) %>' Visible='<%# (Eval("Comment").ToString().Length > 0) ? true : false %>' />
                                            <asp:Image ID="imgFalseAlarm" runat="server" ImageUrl="~/Styles/Img/alerte_kyc_fausse_alerte.png" Height="25px" CssClass="image-tooltip" ToolTip="Fausse alerte" Visible='<%#(Eval("FalseAlarm").ToString() == "1") ? true : false %>' />
                                            <%# GetActionImg(Eval("Actions").ToString()) %>
                                        </asp:Panel>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle BackColor="#344b56" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="#344b56" Font-Bold="True" ForeColor="White" Height="25px" />
                            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                            <RowStyle CssClass="GridViewRowStyle" />
                            <SortedAscendingCellStyle BackColor="#F5F7FB" />
                            <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                            <SortedDescendingCellStyle BackColor="#E9EBEF" />
                            <SortedDescendingHeaderStyle BackColor="#4870BE" />
                            <%--<PagerStyle CssClass="GridViewPager" />--%>
                        </asp:GridView>
                    </div>
                    <div style="border: 3px solid #344b56; border-bottom-left-radius: 8px; border-bottom-right-radius: 8px; height: 25px; background-color: #344b56; color: White; ">
                        <asp:Panel ID="divPagination" runat="server" Visible="false" Style="width: 100%; display: table; margin: auto">
                            <div style="display: table-row;">
                                <div style="display: table-cell; height: 20px; vertical-align: middle; width: 57%; text-align: right">
                                    <asp:Button ID="btnPreviousPage" runat="server" onclick="previousPage" Text="<" Font-Bold="true" Width="30px" />
                                    <asp:DropDownList ID="ddlPage" runat="server" OnSelectedIndexChanged="onChangePageNumber" AutoPostBack="true"
                                        DataTextField="L" DataValueField="V">
                                    </asp:DropDownList>
                                    <asp:Label ID="lblTotalPage" runat="server" Style="font-weight: bold"></asp:Label>
                                    <asp:Button ID="btnNextPage" runat="server" OnClick="nextPage" Text=">" Font-Bold="true" Width="30px" />
                                </div>
                                <div style="display: table-cell; padding-left: 5px">
                                </div>
                            </div>
                        </asp:Panel>
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="panelError" runat="server" Visible="false" style="text-align:center;margin-top:20px">
                <asp:Label ID="lblError" runat="server"></asp:Label>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:HiddenField ID="hdnRefAlertToSearch" runat="server" />
    <asp:Button ID="btnGetAlertDetails" runat="server" OnClick="btnGetAlertDetails_Click" style="display:none" />
    <asp:UpdatePanel ID="upDetailsLoading" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="alert-details-loading"></div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnEditAlert" EventName="click" />
        </Triggers>
    </asp:UpdatePanel>
    <div id="popup-details" style="display:none">
        <asp:UpdatePanel ID="upDetails" runat="server" UpdateMode="Conditional">
            <ContentTemplate>

                <div style="text-align:right;font-size:0.7em;">
                    ref <asp:Label ID="lblAlertRef" runat="server"></asp:Label>
                </div>

                <div style="float:left;width:40%">
                    <b>Type d'incident</b><br />
                    <asp:Label ID="lblAlertType" runat="server"></asp:Label>
                </div>
                <div style="float:right;width:60%;text-align:right">
                    <asp:HyperLink ID="linkAgencyDetails" runat="server"></asp:HyperLink><br />
                    <asp:HyperLink ID="linkClientDetails" runat="server" Text="accéder au dossier d'inscription" Target="_blank" CssClass="MiniButton" style="color:#fff;text-decoration:none;padding:2px 10px"></asp:HyperLink>
                </div>
                <div style="clear:both"></div>

                <div style="border-left:3px solid #f57527">
                    <div>
                        <div style="font-size:1.2em;color:#f57527;font-weight:bold;border-bottom:1px solid #f57527;margin:15px 0 10px 0;padding-left:10px;text-transform:uppercase">
                            Déclaration NON CONFORME
                        </div>
                        <div style="padding-left:10px">
                            <div style="width:50%;float:left">
                                <b>Date</b><br />
                                <asp:Label ID="lblNoConformDate" runat="server"></asp:Label>
                            </div>
                            <div style="width:50%;float:right">
                                <b>Agent</b><br />
                                <asp:Label ID="lblNoConformAgent" runat="server"></asp:Label>
                            </div>
                            <div style="clear:both"></div>
                        </div>
                    </div>

                    <asp:Panel ID="panelEdit" runat="server">
                        <div style="font-size:1.2em;color:#f57527;font-weight:bold;border-bottom:1px solid #f57527;margin:15px 0 10px 0;padding-left:10px;text-transform:uppercase">
                            Modification valeur
                        </div>
                        <div style="padding-left:10px">
                            <div>
                                <div style="float:left;width:50%">
                                    <b>Date</b><br />
                                    <asp:Label ID="lblEditDate" runat="server"></asp:Label>
                                </div>
                                <div style="float:right;width:50%">
                                    <b>Agent</b><br />
                                    <asp:Label ID="lblEditAgent" runat="server"></asp:Label>
                                </div>
                                <div style="clear:both"></div>
                            </div>
                            <div>
                                <div style="float:left;width:50%">
                                    <b>Ancienne valeur</b><br />
                                    <asp:Label ID="lblEditOldValue" runat="server"></asp:Label>
                                </div>
                                <div style="float:right;width:50%">
                                    <b>Nouvelle valeur</b><br />
                                    <asp:Label ID="lblEditNewValue" runat="server"></asp:Label>
                                </div>
                                <div style="clear:both"></div>
                            </div>
                        </div>
                    </asp:Panel>

                    <div>
                        <div style="font-size:1.2em;color:#f57527;font-weight:bold;border-bottom:1px solid #f57527;margin:15px 0 10px 0;padding-left:10px;text-transform:uppercase">
                            Déclaration CONFORME
                        </div>
                        <div style="padding-left:10px">
                            <div style="float:left;width:50%">
                                <b>Date</b><br />
                                <asp:Label ID="lblConformDate" runat="server"></asp:Label>
                            </div>
                            <div style="float:right;width:50%">
                                <b>Agent</b><br />
                                <asp:Label ID="lblConformAgent" runat="server"></asp:Label>
                            </div>
                            <div style="clear:both"></div>
                        </div>
                    </div>

                    <div>
                        <div style="font-size:1.2em;color:#f57527;font-weight:bold;border-bottom:1px solid #f57527;margin:15px 0 10px 0;padding-left:10px;text-transform:uppercase">
                            Action
                        </div>
                        <div>
                            <asp:Panel ID="panelAlertTreated" runat="server" style="padding-left:10px;">
                                <div style="float:left;width:50%">
                                    <b>Date</b><br />
                                    <asp:Label ID="lblAlertTreatedDate" runat="server"></asp:Label>
                                </div>
                                <div style="float:right;width:50%">
                                    <b>Agent</b><br />
                                    <asp:Label ID="lblAlertTreatedAgent" runat="server"></asp:Label>
                                </div>
                                <div style="clear:both"></div>
                            </asp:Panel>
                            <asp:Repeater ID="rptAction" runat="server">
                                <HeaderTemplate>
                                    <div style="margin-top:10px">
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <div style="display:inline-block;margin:0 10px">
                                        <asp:HiddenField ID="hdnIdAction" runat="server" Value='<%#Eval("IdAction") %>' />
                                        <asp:CheckBox ID="ckbAction" runat="server" />
                                        <asp:Label ID="lblAction" runat="server" AssociatedControlID="ckbAction" Font-Bold="true" style="cursor:pointer">
                                            <span style="position:relative;top:6px;"><%#GetActionImg(Eval("IdAction").ToString()) %></span>
                                            <%#Eval("Description") %>
                                        </asp:Label>
                                    </div>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </div>
                                </FooterTemplate>
                            </asp:Repeater>
                            <div style="width:95%;margin:10px auto 0 auto">
                                <span style="font-weight:bold">Commentaire</span><br />
                                <asp:TextBox ID="txtComment" runat="server" TextMode="MultiLine" Rows="5" style="width:100%;resize:none"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>

                <asp:Panel ID="panelEditButton" runat="server" style="margin:20px 0 0 0">
                    <div style="float:left;width:50%;text-align:left">
                        <div style="margin-left:15px;display:inline-block">
                            <asp:Image ID="imgFalseAlarm" runat="server" ImageUrl="~/Styles/Img/alerte_kyc_fausse_alerte.png" Height="25px" />
                            <b style="position:relative;top:-3px;">Fausse alerte</b>
                            <div>
                                <div id="toggleFalseAlarm" class="toggle-soft"></div>
                                <asp:CheckBox ID="ckbFalseAlarm" runat="server" CssClass="hidden" />
                            </div>
                        </div>
                        <div style="margin-left:20px;display:inline-block">
                            <b style="position:relative;top:-3px;">Clore l'incident</b>
                            <div>
                                <div id="toggleAlertTreated" class="toggle-soft"></div>
                                <asp:CheckBox ID="ckbTreated" runat="server" CssClass="hidden" />
                            </div>
                        </div>
                    </div>
                    <div style="float:right;width:50%;text-align:right;position:relative;top:27px">
                        <asp:Button ID="btnEditAlert" runat="server" CssClass="button" Text="Enregistrer" OnClientClick="ShowPopupLoading();" OnClick="btnEditAlert_Click" style="margin-bottom:0" />
                    </div>
                    <div style="clear:both"></div>
                </asp:Panel>

                <asp:Panel ID="panelFalseAlarm" runat="server" Visible="false" style="margin:20px 0 0 15px">
                    <asp:Image ID="imgFalseAlarm2" runat="server" ImageUrl="~/Styles/Img/alerte_kyc_fausse_alerte.png" Height="25px" />
                    <b style="position:relative;top:-3px;">Fausse alerte</b>
                </asp:Panel>

            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnGetAlertDetails" EventName="click" />
                <asp:AsyncPostBackTrigger ControlID="btnEditAlert" EventName="click" />
            </Triggers>
        </asp:UpdatePanel>
    </div>

    <div id="popup-agency-alert-message" style="display:none">
        <span id="agency-alert-message"></span>
    </div>

    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(AgencyAlertBeginRequestHandler);
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(AgencyAlertEndRequestHandler);
        function AgencyAlertBeginRequestHandler(sender, args) {
            pbControl = args.get_postBackElement();
            try {
                if (pbControl != null)
                {
                    var containerID = '';

                    if (pbControl.id.indexOf('btnExport') != -1)
                        containerID = '<%=panelExport.ClientID%>';
                    else containerID = 'div-grid';
                    var width = $('#' + containerID).width();
                    var height = $('#' + containerID).height();
                    $('#ar-agency-alert-loading').css('width', width);
                    $('#ar-agency-alert-loading').css('height', height);

                    $('#ar-agency-alert-loading').show();

                    $('#ar-agency-alert-loading').position({ my: "left top", at: "left top", of: "#" + containerID });
                }
            }
            catch(ex)
            {
                console.log(ex.toString());
            }

        }
        function AgencyAlertEndRequestHandler(sender, args) {
            $('#ar-agency-alert-loading').hide();
        }
    </script>

</div>