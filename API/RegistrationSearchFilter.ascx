﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RegistrationSearchFilter.ascx.cs"
    Inherits="API_RegistrationSearchFilter" %>
<%@ Register Src="~/FILTRES/Period2.ascx" TagPrefix="filter" TagName="Period" %>
<%@ Register Src="~/API/DatePicker.ascx" TagPrefix="asp" TagName="DatePicker" %>
<style type="text/css">
    .ui-combobox {
    position: relative;
    display: inline-block;
    }
    .ui-combobox-toggle {
    position: absolute;
    top: 0;
    bottom: 0;
    margin-left: -1px;
    padding: 0;
    /* support: IE7 */
    *height: 1.7em;
    *top: 0.1em;
    }
    .ui-combobox-input {
    margin: 0;
    padding: 0.3em;
    width:300px;
    height:30px;
    }
</style>
<script type="text/javascript">

    (function ($) {
        $.widget("ui.combobox", {
            _create: function () {
                var input,
                    that = this,
                    wasOpen = false,
                    select = this.element.hide(),
                    selected = select.children(":selected"),
                    value = selected.val() ? selected.text() : "",
                    wrapper = this.wrapper = $("<span>")
                    .addClass("ui-combobox")
                    .insertAfter(select);
                function removeIfInvalid(element) {
                    var value = $(element).val(),
                        matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex(value) + "$", "i"),
                        valid = false;
                    select.children("option").each(function () {
                        if ($(this).text().match(matcher)) {
                            this.selected = valid = true;
                            return false;
                        }
                    });
                    if (!valid) {
                        // remove invalid value, as it didn't match anything
                        $(element)
                            .val("")
                            .attr("title", value + " n'existe pas")
                            .tooltip("open");
                        select.val("");
                        setTimeout(function () {
                            input.tooltip("close").attr("title", "");
                        }, 2500);
                        input.data("ui-autocomplete").term = "";
                    }
                }
                input = $("<input>")
                    .appendTo(wrapper)
                    .val(value)
                    .attr("title", "")
                    .addClass("ui-state-default ui-combobox-input")
                    .autocomplete({
                        delay: 0,
                        minLength: 0,
                        source: function (request, response) {
                            var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
                            response(select.children("option").map(function () {
                                var text = $(this).text();
                                if (this.value && (!request.term || matcher.test(text)))
                                    return {
                                        label: text.replace(
                                            new RegExp(
                                            "(?![^&;]+;)(?!<[^<>]*)(" +
                                            $.ui.autocomplete.escapeRegex(request.term) +
                                            ")(?![^<>]*>)(?![^&;]+;)", "gi"
                                            ), "<strong>$1</strong>"),
                                        value: text,
                                        option: this
                                    };
                            }));
                        },
                        select: function (event, ui) {
                            ui.item.option.selected = true;
                            that._trigger("selected", event, {
                                item: ui.item.option
                            });
                        },
                        change: function (event, ui) {
                            if (!ui.item) {
                                removeIfInvalid(this);
                            }
                        }
                    })
                    .addClass("ui-widget ui-widget-content ui-corner-left");
                input.data("ui-autocomplete")._renderItem = function (ul, item) {
                    return $("<li>")
                    .append("<a>" + item.label + "</a>")
                    .appendTo(ul);
                };
                $("<a>")
                    .attr("tabIndex", -1)
                    .attr("title", "Afficher tout")
                    .tooltip()
                    .appendTo(wrapper)
                    .button(
                        {
                            icons: {
                                primary: "ui-icon-triangle-1-s"
                            },
                            text: false
                        }
                    )
                    .removeClass("ui-corner-all")
                    .addClass("ui-corner-right ui-combobox-toggle")
                    .mousedown(function () {
                        wasOpen = input.autocomplete("widget").is(":visible");
                    })
                    .click(function () {
                        input.focus();
                        // close if already visible
                        if (wasOpen) {
                            return;
                        }
                        // pass empty string as value to search for, displaying all results
                        input.autocomplete("search", "");
                    });
                input.tooltip({
                    tooltipClass: "ui-state-highlight"
                });
            },
            _destroy: function () {
                this.wrapper.remove();
                this.element.show();
            }
        });

        //$('.tooltip').tooltip({ items: "span[tooltip]" });
    })(jQuery);

    function FilterStatusDisplayManagement() {
        //if ($('#<%= divGridView.ClientID%>') != null && $('#<%= divGridView.ClientID%>').is(':visible'))
            //$('#panelFilterStatus').show();
        //else
            //$('#panelFilterStatus').hide();
    }

    function initTooltip() {
        $('.trimmer').tooltip({
            position: {
                my: "center bottom-10",
                at: "center top",
                using: function (position, feedback) {
                    $(this).css(position);
                    $("<div>")
                        .addClass("arrow")
                        .addClass(feedback.vertical)
                        .addClass(feedback.horizontal)
                        .appendTo(this);
                }
            }
        });
    }

    function InitAccordion() {
        //alert("init accordion");
        $("#<%= ddlAgencyID.ClientID %>").combobox();
        //$('#<%= rblCashierCheck.ClientID %>').buttonset();
        $('.RadioButtonList').buttonset();

        if ($('#<%= hfFilterVisible.ClientID%>').val().trim() == "true") {
            $('#panelFilterStatus').show();
        }
        else {
            $('#panelFilterStatus').hide();
        }
        $("#FilterList").accordion({
            heightStyle: "content",
            change: function (event, ui) {
                //alert('change' + $('#panelFilterStatus').is(':visible'));
                if ($(this).accordion("option", "active") != 4) {
                    $('#<%= hfFilterVisible.ClientID%>').val('false');
                    if ($('#panelFilterStatus').is(':visible'))
                        $('#panelFilterStatus').slideUp();
                }
                else {
                    $('#<%= hfFilterVisible.ClientID%>').val('true');
                    if(!$('#panelFilterStatus').is(':visible'))
                        $('#panelFilterStatus').slideDown();
                }
            }
        });

        if ($('#<%= hfPanelActive.ClientID %>').val().trim() != "") {
            $("#FilterList").accordion("option", "animate", false);
            $("#FilterList").accordion("option", "active", parseInt($('#<%= hfPanelActive.ClientID %>').val().trim()));
        }
        else {
            $("#FilterList").accordion("option", "animate", false);
            $("#FilterList").accordion("option", "active", 4);
        }

        $("#FilterList").accordion({
            activate: function (event, ui) {
                //alert($("#FilterList").accordion("option", "active"));
                $('#<%= hfPanelActive.ClientID %>').val($("#FilterList").accordion("option", "active"));
            }
        });

        $("#FilterList").accordion("option", "animate", true);
    }

    function clickRegistrationSearch(setPageIndex) {
        var values = "";
        //alert("registration search");

        if ($('#RegistrationSearchByPack').is(':visible')) {
            values = "PackNumber!" + $('#<%=txtPackNumber.ClientID%>').val().trim();
            values += ",BeginDate!";
            values += ",EndDate!";
            values += ",Period!3";
        }
        else if ($('#RegistrationSearchByRegistration').is(':visible')) {
            values = "RegistrationNumber!" + $('#<%=txtRegistrationNumber.ClientID%>').val().trim();
            values += ",BeginDate!";
            values += ",EndDate!";
            values += ",Period!3";
        }
        else if ($('#RegistrationSearchByPhone').is(':visible')) {
            values = "PhoneNumber!" + $('#<%=txtPhoneNumber.ClientID%>').val().trim();
            values += ",BeginDate!";
            values += ",EndDate!";
            values += ",Period!3";
        }
        else if ($('#RegistrationSearchByClient').is(':visible')) {
            values = "ClientLastName!" + $('#<%=txtClientLastName.ClientID%>').val().trim();
            values += ",ClientFirstName!" + $('#<%=txtClientFirstName.ClientID%>').val().trim();
            values += ",ClientBirthDate!" + $('#<%=hfClientBirthDate.ClientID%>').val().trim();
            values += ",BeginDate!";
            values += ",EndDate!";
            values += ",Period!3";
        }
        else {
            values = "AgencyID!" + $('#<%=ddlAgencyID.ClientID%>').val().trim();
            values += ",BeginDate!" + $('#<%=hfBeginDate.ClientID%>').val().trim();
            values += ",EndDate!" + $('#<%=hfEndDate.ClientID%>').val().trim();
            values += ",Period!" + $('#<%=hfPeriod.ClientID%>').val().trim();

            if ($('#<%= rblStatus.ClientID %>').val() != null) {
                values += ",RefStatus!" + $('#<%= rblStatus.ClientID %> input:checked').val();
                //if ($('#<%= rblStatus.ClientID %> input:checked').val() == "125")
                //$('#<%= rblCashierCheck.ClientID %> input:checked').val("N");
            }
            else
                values += ",RefStatus!";

            if ($('#<%= rblStatusCheck.ClientID %>').val() != null)
                values += ",StatusCheck!" + $('#<%= rblStatusCheck.ClientID %> input:checked').val();
            else
                values += ",StatusCheck!";
        }

        if (setPageIndex != null) {
            values += ",PageIndex!" + setPageIndex;
        }

        __doPostBack("MainContent_upRegistrationSearch", "GetRegistrationSearchFilter;" + values);

    }

    function onChangeNbResult() {
        clickRegistrationSearch(1);
        return false;
    }

    function nextPage() {
        var pageIndex = parseInt($('#<%= ddlPage.ClientID %>').val()) + 1;
        $('#<%= ddlPage.ClientID %>').val(pageIndex);

        clickRegistrationSearch(pageIndex);
        return false;
    }

    function previousPage() {
        var pageIndex = parseInt($('#<%= ddlPage.ClientID %>').val()) - 1;
        $('#<%= ddlPage.ClientID %>').val(pageIndex);

        clickRegistrationSearch(pageIndex);
        return false;
    }

    function onChangePageNumber() {
        var pageIndex = $('#<%= ddlPage.ClientID %>').val();

        clickRegistrationSearch(pageIndex);
        return false;
    }

    function showRegistration(registrationNumber) {
        __doPostBack("upRegistration", "showRegistration;" + registrationNumber);
    }

    function defaultFilter() {
        var values = "";
        values += "AgencyID!,RefStatus!15,StatusCheck!,Period!4";

        $('#<%=ddlAgencyID.ClientID%>').val("");

        __doPostBack("MainContent_upRegistrationSearch", "GetRegistrationSearchFilter;"+values);
    }

</script>
<asp:Panel ID="panelRegistrationSearch" runat="server" DefaultButton="btnRegistrationSearch">
    <h2 style="color: #344b56">
        RECHERCHE INSCRIPTION
    </h2>
    <div id="FilterList" style="margin: 20px auto; width: 90%">
        <h3 style="text-transform: uppercase">
            par Numéro de pack</h3>
        <div id="RegistrationSearchByPack" style="margin: auto">
            <div style="font-weight: bold">
                N&deg; pack</div>
            <div>
                <asp:TextBox ID="txtPackNumber" runat="server" autocomplete="off"></asp:TextBox>
            </div>
        </div>
        <h3 style="text-transform: uppercase">
            par Numéro d'Inscription</h3>
        <div id="RegistrationSearchByRegistration" style="margin: auto">
            <div style="font-weight: bold">
                N&deg; inscription</div>
            <div>
                <asp:TextBox ID="txtRegistrationNumber" runat="server"></asp:TextBox>
            </div>
        </div>
        <h3 style="text-transform: uppercase">
            par Numéro de Téléphone</h3>
        <div id="RegistrationSearchByPhone" style="margin: auto">
            <div style="font-weight: bold">
                N&deg; Téléphone</div>
            <div>
                <asp:TextBox ID="txtPhoneNumber" runat="server" autocomplete="off"></asp:TextBox>
            </div>
        </div>
        <h3 style="text-transform: uppercase">
            par Client</h3>
        <div id="RegistrationSearchByClient" style="margin: auto">
            <div style="display: table; width: 100%">
                <div style="display: table-row">
                    <div style="display: table-cell; width: 33%">
                        <div style="font-weight: bold">
                            Nom</div>
                        <div>
                            <asp:TextBox ID="txtClientLastName" runat="server" autocomplete="off"></asp:TextBox>
                        </div>
                    </div>
                    <div style="display: table-cell; width: 33%">
                        <div style="font-weight: bold">
                            Prénom</div>
                        <div>
                            <asp:TextBox ID="txtClientFirstName" runat="server" autocomplete="off"></asp:TextBox>
                        </div>
                    </div>
                    <div style="display: table-cell; width: 33%">
                        <div style="font-weight: bold">
                            Date de Naissance</div>
                        <div>
                            <asp:TextBox ID="txtClientBirthDate" runat="server" Visible="false" autocomplete="off"></asp:TextBox>
                            <asp:DatePicker ID="dpClientBirthDate" runat="server" sYearType="past" bGetDateByPostback="true"
                                sPostbackName="BirthDatePicker" />
                            <asp:HiddenField ID="hfClientBirthDate" runat="server" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <h3 style="text-transform: uppercase">
            par Point de vente</h3>
        <div id="RegistrationSearchByAgency" style="margin: auto">
            <div style="display: table; width: 100%">
                <div style="display: table-row;">
                    <div style="display: table-cell">
                        <div style="font-weight: bold">
                            Point de vente</div>
                        <div>
                            <div class="ui-widget">
                                <asp:DropDownList ID="ddlAgencyID" runat="server" DataTextField="AgencyName" DataValueField="AgencyID"
                                    Width="300px" AppendDataBoundItems="true" Height="30px">
                                    <asp:ListItem Selected="True" Value=""></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        <asp:Panel ID="divNotUsed" runat="server" Visible="false">
            <h3 style="text-transform: uppercase">
                par Document</h3>
            <div id="RegistrationSearchByDocument">
                <div style="display: table; width: 100%">
                    <div style="display: table-row">
                        <div style="display: table-cell; width: 33%">
                            <div style="font-weight: bold">
                                N&deg; Document</div>
                            <div>
                                <asp:TextBox ID="txtDocumentNumber" runat="server" autocomplete="off"></asp:TextBox>
                            </div>
                        </div>
                        <div style="display: table-cell; width: 33%">
                            <div style="font-weight: bold">
                                Type Document</div>
                            <div>
                                <asp:DropDownList ID="ddlDocumentType" runat="server">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div style="display: table-cell; width: 33%">
                            <div style="font-weight: bold">
                                Pays Document</div>
                            <div>
                                <asp:DropDownList ID="ddlDocumentCountry" runat="server">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </div>
    <div style="width: 100%; text-align: center">
        <asp:Button ID="btnDeleteFilter" runat="server" OnClientClick="defaultFilter(); return false;" Text="Supprimer les filtres" style="width: 250px" CssClass="buttonHigher" />
        <asp:Button ID="btnRegistrationSearch" runat="server" OnClientClick="clickRegistrationSearch(); return false;" Text="RECHERCHER" style="width: 200px" CssClass="buttonHigher" />
    </div>
    <div style="width: 90%; margin: auto">
        <div id="panelFilterStatus">
            <div style="width: 100%; margin: 10px 0 0 0">
                <filter:Period ID="RegistrationSearchByAgencyPeriod" runat="server" isPeriodBold="true" />
                <asp:HiddenField ID="hfBeginDate" runat="server" />
                <asp:HiddenField ID="hfEndDate" runat="server" />
                <asp:HiddenField ID="hfPeriod" runat="server" />
            </div>
            <div style="display: table; width: 100%; margin-bottom: 5px">
                <div style="display: table-row">
                    <div style="display: table-cell; ">
                        <div style="font-weight:bold; text-transform:capitalize">
                            état inscription
                        </div>
                        <asp:RadioButtonList ID="rblStatus" runat="server" AutoPostBack="false" RepeatDirection="Horizontal"
                            CssClass="RadioButtonList" Height="20px" onchange="clickRegistrationSearch();"
                            Style="position: relative; left: 0px;text-transform:uppercase">
                            <asp:ListItem Value="15" Selected="True"><span style="color:green;text-shadow: -1px -1px 0px #FFF, 1px 1px 0px #FFF, -1px 1px 0px #FFF, 1px -1px 0px #FFF;">réussi</span></asp:ListItem>
                            <asp:ListItem Value="22"><span style="color:red;text-shadow: -1px -1px 0px #FFF, 1px 1px 0px #FFF, -1px 1px 0px #FFF, 1px -1px 0px #FFF;">échec</span></asp:ListItem>
                            <asp:ListItem Value="125"><span style="color:blue;text-shadow: -1px -1px 0px #FFF, 1px 1px 0px #FFF, -1px 1px 0px #FFF, 1px -1px 0px #FFF;">mpad terminé</span></asp:ListItem>
                            <asp:ListItem Value="">tous</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                    <div style="display: table-cell; ">
                        <div style="font-weight:bold; text-transform:capitalize">
                            état évaluation
                        </div>
                        <asp:RadioButtonList ID="rblStatusCheck" runat="server" AutoPostBack="false" RepeatDirection="Horizontal"
                            CssClass="RadioButtonList" Height="20px" onchange="clickRegistrationSearch();"
                            Style="position: relative; left: 0px">
                            <asp:ListItem Value="G"><img src="./Styles/Img/green_status.png" alt="" /></asp:ListItem>
                            <asp:ListItem Value="O"><img src="./Styles/Img/yellow_status.png" alt="" /></asp:ListItem>
                            <asp:ListItem Value="R"><img src="./Styles/Img/red_status.png" alt=""/></asp:ListItem>
                            <asp:ListItem Value="W"><img src="./Styles/Img/gray_status.png" alt=""/></asp:ListItem>
                            <asp:ListItem Value="" Selected="True">TOUS</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                    <div id="divFilterCashierCheck" runat="server" style="display: table-cell; text-align: right;" visible="false">
                        <div>
                            Validée par caissier</div>
                        <asp:RadioButtonList ID="rblCashierCheck" runat="server" AutoPostBack="false" RepeatDirection="Horizontal"
                            CssClass="RadioButtonList" Height="20px" onchange="clickRegistrationSearch();"
                            Style="position: relative;left:15px">
                            <asp:ListItem Value="Y" Selected="True">OUI</asp:ListItem>
                            <asp:ListItem Value="N">NON</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                </div>
            </div>
        </div>
        <div id="divGridViewInfo" runat="server" visible="false" style="display: table; width:100%; margin-bottom:10px">
            <div style="display: table-row">
                <div style="display: table-cell; width: 50%; padding-top: 20px">
                    <asp:Label ID="lblNbOfResults" runat="server">0</asp:Label>
                    résultat(s)
                </div>
                <div style="display: table-cell; width: 50%; text-align: right;">
                    Nb de résultats par page :
                    <asp:DropDownList ID="ddlNbResult" runat="server" AutoPostBack="false" onchange="onChangeNbResult();">
                        <asp:ListItem Value="10">10</asp:ListItem>
                        <asp:ListItem Value="25" Selected="True">25</asp:ListItem>
                        <asp:ListItem Value="50">50</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
        </div>
        <asp:Panel ID="divGridView" runat="server" Visible="false">
            <div style="border: 3px solid #344b56; border-top-left-radius: 8px; border-top-right-radius: 8px;
                width: 100%; margin: auto" visible="false">
                <%--OnPreRender="GridView1_PreRender" OnRowCreated="GridView1_RowCreated" OnRowDataBound="GridView1_RowDataBound"--%>
                <asp:GridView ID="gvRegistrationSearchResult" runat="server" AllowSorting="false"
                    AllowPaging="false" AutoGenerateColumns="False" GridLines="None" CellPadding="10"
                    DataKeyNames="RefTransaction" ShowHeader="true" Width="100%" SortedDescendingCellStyle-Font-Overline="true"
                    ViewStateMode="Disabled" EnableSortingAndPagingCallbacks="true" BorderWidth="0"
                    BackColor="Transparent" OnRowDataBound="GridView1_RowDataBound">
                    <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="#344b56" Font-Bold="true" />
                    <EmptyDataTemplate>
                        Aucune donnée ne correspond à la recherche</EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="N° Pack" SortExpression="RegistrationNumber" InsertVisible="false"
                            ControlStyle-CssClass="" ControlStyle-Width="70px">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblRegistrationNumber" Text='<%# Eval("RegistrationCode") %>'
                                    Style="display: none"></asp:Label>
                                <asp:Label runat="server" ID="lblPackNumber" Text='<%# Eval("PackNumber") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Nom" SortExpression="LastName" InsertVisible="false"
                            ControlStyle-CssClass="trimmer" ControlStyle-Width="100px">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblClientLastName" Text='<%# Eval("LastName")%>' CssClass="tooltip"
                                    ToolTip='<%# Eval("LastName")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Prénom" SortExpression="FirstName" InsertVisible="false"
                            ControlStyle-CssClass="trimmer" ControlStyle-Width="100px">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblClientFirstName" Text='<%# Eval("FirstName") %>'
                                    CssClass="tooltip" ToolTip='<%# Eval("FirstName")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField SortExpression="BirthDate" InsertVisible="false" ControlStyle-CssClass=""
                            ControlStyle-Width="70px" ItemStyle-HorizontalAlign="Center">
                            <HeaderTemplate>
                                Date<br />
                                de
                                <br />
                                naissance
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblClientBirthDate" Text='<%# Eval("BirthDate")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Etat" InsertVisible="False" SortExpression="Status"
                            ControlStyle-CssClass="trimmer" ControlStyle-Width="170px">
                            <ItemTemplate>
                                <asp:Label ID="status" runat="server" Font-Bold="true" Text='<%#Eval("Status") %>'
                                    ForeColor='<%# ColorStatusLabel(Eval("DisplayColor").ToString())%>' CssClass="tooltip"
                                    ToolTip='<%#Eval("Status") %>'></asp:Label>
                                <asp:Image ID="ErrorMessageImg" runat="server" ImageUrl="~/Styles/Img/help.png" ToolTip=""
                                    Width="15px" Visible="false" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Date insertion" InsertVisible="False" SortExpression="InsertedDate"
                            ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="InsertedDate" runat="server" Text='<%# Eval("InsertionDate") %>'></asp:Label>
                                <%--<asp:Label ID="Label1" runat="server" Text='<%# ConvertToLocalDateTime(Eval("InsertionDate").ToString())%>'></asp:Label>--%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Check" InsertVisible="False" ControlStyle-CssClass="trimmer"
                            ControlStyle-Width="50px" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Image ID="ImgCheckStatus" runat="server" CssClass="tooltip" Style="width: 20px;"
                                    ToolTip='<%# getCheckStatus(Eval("BackOfficeOpinion").ToString() +";"+ Eval("BackOfficeOpinionDate").ToString()+";"+Eval("BackOfficeUser").ToString(), "Tooltip") %>'
                                    AlternateText='<%# getCheckStatus(Eval("BackOfficeOpinion").ToString(), "AlternateText")%>'
                                    ImageUrl='<%# getCheckStatus(Eval("BackOfficeOpinion").ToString(), "ImageUrl")%>' />
                                <%--<asp:Label ID="lblCheckStatus" runat="server" Text='<%# getCheckStatus(Eval("BackOfficeOpinion").ToString(), "AlternateText")%>' CssClass="tooltip" 
                                ToolTip='<%# getCheckStatus(Eval("BackOfficeOpinion").ToString() +";"+ Eval("BackOfficeOpinionDate").ToString()+";"+Eval("BackOfficeUser").ToString(), "Tooltip") %>' ></asp:Label>--%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--<asp:BoundField DataField="InsertedDate"HeaderText="InsertedDate" SortExpression="InsertedDate" ItemStyle-HorizontalAlign="Center"/>--%>
                        <asp:TemplateField HeaderText="RefTransaction" InsertVisible="False" SortExpression="RefTransaction"
                            Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="RefTransaction" runat="server" Text='<%#Eval("RefTransaction") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="bDelete" InsertVisible="False" SortExpression="bDelete"
                            Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="bDelete" runat="server" Text=''></asp:Label><%--<%#Eval("bDelete") %>--%>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#344b56" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#344b56" Font-Bold="True" ForeColor="White" Height="25px" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle CssClass="GridViewRowStyle" />
                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                    <%--<PagerStyle CssClass="GridViewPager" />--%>
                </asp:GridView>
            </div>
            <div style="width: 100%; background-color: #344b56; color: White; border: 3px solid #344b56;
                border-bottom-left-radius: 8px; border-bottom-right-radius: 8px; height: 25px">
                <asp:Panel ID="divPagination" runat="server" Visible="false" Style="width: 100%;
                    display: table; margin: auto">
                    <div style="display: table-row;">
                        <div style="display: table-cell; height: 20px; vertical-align: middle; width: 57%;
                            text-align: right">
                            <asp:Button ID="btnPreviousPage" runat="server" OnClientClick="previousPage();" Text="<"
                                Font-Bold="true" Width="30px" />
                            <%--<input id="btnPreviousPage" runat="server"type="button" class="button" value="<" style="width:30px" onclick="previousPage();"/>--%>
                            <%--OnSelectedIndexChanged="onChangePage"--%>
                            <asp:DropDownList ID="ddlPage" runat="server" onchange="onChangePageNumber();" DataTextField="L"
                                DataValueField="V" AutoPostBack="false">
                            </asp:DropDownList>
                            <asp:Label ID="lblTotalPage" runat="server" Style="font-weight: bold"></asp:Label>
                            <%--<input id="btnNextPage" runat="server"type="button" class="button" value=">" style="width:30px" onclick="nextPage();"/>--%>
                            <asp:Button ID="btnNextPage" runat="server" OnClientClick="nextPage();" Text=">"
                                Font-Bold="true" Width="30px" />
                            <%--<div style="padding-left: 10px"> <img src="Styles/Img/loading.gif"alt="loading" width="25px" height="25px" /> </div>--%>
                        </div>
                        <div style="display: table-cell; padding-left: 5px">
                        </div>
                    </div>
                </asp:Panel>
            </div>
        </asp:Panel>
        <div style="height: 30px">
        </div>
        <div id="#EndofGridView">
        </div>
    </div>
    <asp:HiddenField ID="hfPanelActive" runat="server" Value="" />
    <asp:HiddenField ID="hfFilterVisible" runat="server" Value="true" />
</asp:Panel>
