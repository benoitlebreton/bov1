﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AmlAlert.ascx.cs" Inherits="API_AmlAlert" %>
<script type="text/javascript">

    function initAmlAlert() {
        initTooltip();

        $('#AmlAlert_popup-details').dialog({
            autoOpen: false,
            resizable: false,
            draggable: false,
            modal: true,
            minWidth: 700,
            title: "Détail alerte"
        });
        $('#AmlAlert_popup-details').parent().appendTo(jQuery("form:first"));
    }

    function showDetails(sDetails, sStatus, sTAG, sRefAlert) {
        getAlertDetails(sDetails, sStatus, sTAG, sRefAlert)
    }

    function hideAlert() {
        $('#divAlertDetails').hide();
        $('#divAlertCheck').hide();
        $('#divAlertHide').show();
    }
    function checkAlert() {
        $('#divAlertDetails').hide();
        $('#divAlertHide').hide();
        $('#divAlertCheck').show();
    }

    function closeAlertDetails() {
        $('#AmlAlert_popup-details').dialog("close");
    }
    function showAlertDetails() {
        $('#divAlertHide').hide();
        $('#divAlertCheck').hide();
        $('#divAlertDetails').show();
    }

    function getAlertDetails(sDetails, sStatus, sTAG, sRefAlert) {
        if (sRefAlert != null && sRefAlert.toString().trim().length > 0) {
            $.ajax({
                type: "POST",
                url: 'ws_tools.asmx/getAmlAlertDetails',
                data: "{ sRefAlert: '" + sRefAlert + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $('#<%=hfRefAlert.ClientID%>').val("");
                    $('#<%=hfTagAlert.ClientID%>').val("");
                    $('#<%=hfAlertStatus.ClientID%>').val("");
                    $('#divAmlAlertDetails').html(msg.d);
                    $('#AmlAlertDetails').html(sDetails);
                    $('#divAlertDetailsChecked').hide();
                    getAlertDetails(sRefAlert);
                    $('#AmlAlert_popup-details').dialog("open");

                    if (sStatus != null && (sStatus == "1" || sStatus == "2") && sRefAlert != null) {
                        $('#<%=hfRefAlert.ClientID%>').val(sRefAlert);
                        $('#<%=hfTagAlert.ClientID%>').val(sTAG);
                        $('#<%=hfAlertStatus.ClientID%>').val(sStatus);
                        if (sStatus == "1")
                            $('.sAlertStatus').html("actives")
                        else
                            $('.sAlertStatus').html("gelées")
                        $('#divAlertDetailsChecked').show();
                    }
                },
                error: function (e) {
                    alert("Web service error");
                    //$("#divResult").html("WebService unreachable");
                }
            });
        }
    }

    function MaxLength(text, type, maxlength) {
        //asp.net textarea maxlength doesnt work; do it by hand
        //var maxlength = 2000; //set your value here (or add a parm and pass it in)
        if (maxlength == null) {
            if(type == "check")
                maxlength = $('#<%=lblNbMaxCheckComment.ClientID%>').html();
            
        }

        var object = document.getElementById(text.id)  //get your object
        if (object.value.length > maxlength) {
            object.focus(); //set focus to prevent jumping
            object.value = text.value.substring(0, maxlength); //truncate the value
            object.scrollTop = object.scrollHeight; //scroll to the end to prevent jumping
            $('#lblNbChar_'+type).css('color', '#FF0000');
            return false;
        }
        else if (object.value.length == maxlength) {
            $('#lblNbChar_'+type).css('color', '#FF0000');
        }
        else {
            $('#lblNbChar_'+type).css('color', '#344B56');
        }

        $('#lblNbChar_'+type).html(object.value.length);
        return true;
    }

    function clearComment(type) {
        $('#<%=txtAlertCheckedComment.ClientID%>').val("");
        $('#lblNbChar_'+type).html("0");
    }

    function checkConfirmAlertChecked() {
        var isCommentOK = false;

        $('#reqCheckComment').css("visibility", "hidden");
        if ($('#<%=txtAlertCheckedComment.ClientID%>').val().trim().length > 0)
            isCommentOK = true;
        else
            $('#reqCheckComment').css("visibility", "visible");

        return isCommentOK;
    }

</script>

<div>
    <asp:Panel ID="panelGrid" runat="server" Visible="false">
        
        <div style="margin-top:10px;text-align: right;">Nombre d'alerte(s) :
            <asp:Label ID="lblRowCount" runat="server"></asp:Label></div>
        <div style="border: 3px solid #344b56; border-top-left-radius: 8px; border-top-right-radius: 8px; margin-top: 5px">
            <asp:GridView ID="gvClientAlertList" runat="server" AllowSorting="false"
                AllowPaging="false" AutoGenerateColumns="False" GridLines="None" CellPadding="5"
                DataKeyNames="RefCustomer" ShowHeader="true" Width="100%" SortedDescendingCellStyle-Font-Overline="true"
                ViewStateMode="Disabled" BorderWidth="0" BackColor="Transparent" OnRowDataBound="gvClientAlertList_RowDataBound">
                <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="#344b56" Font-Bold="true" />
                <EmptyDataTemplate>
                    Aucune alerte
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField Visible="false" InsertVisible="false">
                        <ItemTemplate>
                            <asp:HiddenField ID="hdnAlertDetails" runat="server" Value='<%# Eval("AlertDescription")%>' />
                            <asp:HiddenField ID="hdnRefAlertStatus" runat="server" Value='<%# Eval("refAlertStatus") %>' />
                            <asp:HiddenField ID="hdnAlertTag" runat="server" Value='<%# Eval("AlertTAG") %>' />
                            <asp:HiddenField ID="hdnRefAlert" runat="server" Value='<%# Eval("RefAlert") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField SortExpression="AlertWatched" HeaderText="" InsertVisible="false">
                        <ItemTemplate>
                            <asp:Image ID="imgWatched" runat="server" ImageUrl="../Styles/Img/orange-eye.png" style="width:30px;" CssClass="tooltip" 
                                Visible='<%# AML.getWatchStatus(Eval("Watched").ToString()) %>' 
                                ToolTip='<%# AML.getWatchDetails(Eval("WatchedBy").ToString(),Eval("WatchedDate").ToString()) %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField SortExpression="AlertSubject" HeaderText="Alerte" InsertVisible="false"
                        ControlStyle-Width="200" ControlStyle-CssClass="trimmer" >
                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblAlertSubject" Text='<%# Eval("Subject")%>' CssClass="tooltip" ToolTip='<%# Eval("Subject")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField SortExpression="Alert_Details" HeaderText="Détails" InsertVisible="false" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <%# getAmlDetails(Eval("CategoryTAG").ToString(), Eval("Critere_INT1").ToString(), Eval("Critere_INT2").ToString(), Eval("Critere_VARCHAR1").ToString(), Eval("Critere_MONEY1").ToString())%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <%--<asp:TemplateField SortExpression="Alert_INT1" InsertVisible="false" ControlStyle-Width="20" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblAlert_INT1" Text='<%# Eval("Critere_INT1")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField SortExpression="Alert_INT2" InsertVisible="false" ControlStyle-Width="20" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblAlert_INT2" Text='<%# Eval("Critere_INT2")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField SortExpression="Alert_MONEY1" InsertVisible="false" ControlStyle-Width="100" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblAlert_MONEY1" Text='<%# tools.getFormattedMoney(Eval("Critere_MONEY1").ToString()) %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField SortExpression="Alert_VARCHAR1" InsertVisible="false" ControlStyle-Width="100" ItemStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblAlert_VARCHAR1" Text='<%# Eval("Critere_VARCHAR1")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>--%>
                    <asp:TemplateField SortExpression="AlertDescription" HeaderText="Date" InsertVisible="false" ControlStyle-CssClass=""
                        ControlStyle-Width="100" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblAlertDate" Text='<%# tools.getFormattedDate(Eval("AlertDate").ToString())%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField SortExpression="AlertScore" HeaderText="Scoring alerte" InsertVisible="false" HeaderStyle-Width="1" HeaderStyle-CssClass="position-relative" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblAlertScore" Text='<%# Eval("AlertScore") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="#344b56" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#344b56" Font-Bold="True" ForeColor="White" Height="25px" />
                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle CssClass="GridViewRowStyle" />
                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                <SortedDescendingHeaderStyle BackColor="#4870BE" />
            </asp:GridView>
        </div>
        <div style="background-color: #344b56; color: White; border: 3px solid #344b56; 
            border-bottom-left-radius: 8px; border-bottom-right-radius: 8px; height: 30px;">
            <asp:Panel ID="divPagination" runat="server" Visible="false" Style="width: 100%; display: table; margin: auto;">
                <div style="display: table-row;">
                    <div style="display: table-cell; height: 20px; vertical-align: middle; width: 57%; text-align: right">
                        <asp:Button ID="btnPreviousPage" runat="server" OnClick="btnPreviousPage_Click" OnClientClick="$('#imgAmlAlertPaginationLoading').show();" Text="<" Font-Bold="true" Width="30px" />
                        <asp:DropDownList ID="ddlPage" runat="server" OnSelectedIndexChanged="ddlPage_SelectedIndexChanged" onchange="$('#imgAmlAlertPaginationLoading').show();" DataTextField="L" DataValueField="V" AutoPostBack="true">
                        </asp:DropDownList>
                        <asp:Label ID="lblTotalPage" runat="server" Style="font-weight: bold"></asp:Label>
                        <asp:Button ID="btnNextPage" runat="server" OnClick="btnNextPage_Click" Text=">" OnClientClick="$('#imgAmlAlertPaginationLoading').show();" Font-Bold="true" Width="30px" />
                    </div>
                    <div style="display: table-cell; padding-left: 5px; vertical-align:middle">
                        <img id="imgAmlAlertPaginationLoading" alt="loading..." src="Styles/Img/loading.gif" style="display:none; height:20px;" />
                    </div>
                </div>
            </asp:Panel>
        </div>
    </asp:Panel>
    <asp:Panel ID="panelError" runat="server" Visible="false">
        <asp:Label ID="lblError" runat="server"></asp:Label>
    </asp:Panel>

    <asp:HiddenField ID="hfRefCustomer" runat="server" Value="" />
</div>
<div id="AmlAlert_popup-details">
    <style type="text/css">

        .AlertDetails table {
            border:2px solid #ff6a00;
            border-spacing: 0;
            border-collapse: collapse;
        }
        .AlertDetails td{
            padding:5px 10px;
            border:0;
            border-top:1px;
            border-top-style:solid;
            border-top-color:#ff6a00;
        }
        .AlertDetails th{
            border:0;
            font-weight:bold;
            padding:2px 5px;
            background-color:#ff6a00;
            color:#fff;
        }
    </style>
    <div id="divAlertDetails" style="width:670px;">
        <span id="AmlAlertDetails" style="font-weight:bold"></span>
        <div id="divAmlAlertDetails" style="display:block;margin-top:10px;max-width:670px;max-height:200px; overflow:auto" class="AlertDetails"></div>
        <div id="divAlertDetailsChecked" style="display:none; margin-top:20px;width:100%;text-align:right">
            <div style="display:inline-block;">
                <div class="table">
                    <div class="row">
                        <div class="cell" style="padding-left:10px;">
                            <input id="btnAlertChecked" type="button" value="Vérifié" class="button" onclick="checkAlert();" />
                            <input type="button" value="Fermer" style="display:none" class="button" onclick="closeAlertDetails();" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="divAlertCheck" style="display:none">
        <span id="sAlertCheckedConfirm">Confirmez-vous avoir vérifié cette alerte ?</span>
        <div class="table" style="margin:5px 0 0 0;width:100%; padding:0;">
            <div class="row">
                <div class="cell" style="width:100%; vertical-align:middle">
                    Commentaire(s)
                    <label id="lblNbChar_check">0</label>/
                    <asp:Label ID="lblNbMaxCheckComment" runat="server" Text="100"></asp:Label> :
                    <span id="reqCheckComment" class="reqStarStyle" style="visibility:hidden">*</span>
                </div>
                <div class="cell" style="text-align:right; vertical-align:middle">
                    <input type="button" class="button" value="effacer" onclick="clearComment('check');" />
                </div>
            </div>
        </div>
        <div>
            <asp:TextBox ID="txtAlertCheckedComment" MaxLength="100" runat="server" TextMode="MultiLine" autocomplete="off"
                onKeyUp="javascript:MaxLength(this, 'check');" onChange="javascript:MaxLength(this, 'check');" 
                style="max-width:100%; min-width:100%; min-height:100px; max-height:100px"></asp:TextBox>
        </div>
        <div style="margin-top:10px;">
            <asp:CheckBox ID="cbAlertAllChecked" runat="server" Checked="true" />
            <label for="<%= cbAlertAllChecked.ClientID %>">Appliquer à toutes les alertes <span class="sAlertStatus"></span> du même type pour ce client.</label>
        </div>
        <div style="width:100%;text-align:right;margin-top:10px;">
            <input type="button" value="Annuler" class="button" onclick="showAlertDetails();"/>
            <asp:Button ID="btnConfirmAlertChecked" runat="server" Text="Valider" CssClass="button" OnClientClick="return checkConfirmAlertChecked();" OnClick="btnConfirmAlertChecked_Click" />
        </div>
    </div>
    
    <asp:HiddenField ID="hfRefAlert" runat="server" />
    <asp:HiddenField ID="hfTagAlert" runat="server" />
    <asp:HiddenField ID="hfAlertStatus" runat="server" />
</div>
