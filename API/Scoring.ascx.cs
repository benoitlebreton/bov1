﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using XMLMethodLibrary;

public partial class API_Scoring : System.Web.UI.UserControl
{
    public int? Score { get; set; }
    public string ScoreDetails { get; set; }
    public string Label { get; set; }
    public string Type { get; set; }
    public int? RefCustomer { get; set; }
    public string XmlCustomerInfos
    {
        get { return (ViewState["XmlCustomerInfos"] != null) ? ViewState["XmlCustomerInfos"].ToString() : ""; }
        set {
            ViewState["XmlCustomerInfos"] = value;
            string sScore, sScoreDetails;
            ParseXmlCustomerInfos(XmlCustomerInfos, out sScore, out sScoreDetails);
            Score = int.Parse(sScore);
            ScoreDetails = sScoreDetails;
        }
    }
    private string _sSortColumn { get { return (ViewState["SortColumn"] != null ? ViewState["SortColumn"].ToString() : null); } set { ViewState["SortColumn"] = value; } }
    private string _sSortDirection { get { return (ViewState["SortDirection"] != null ? ViewState["SortDirection"].ToString() : "ASC"); } set { ViewState["SortDirection"] = value; } }
    private string _sClientScore { get { return (Session["ClientScore"] != null ? Session["ClientScore"].ToString() : null); } set { Session["ClientScore"] = value; } }
    private string _sClientScoreDetails { get { return (Session["ClientScoreDetails"] != null ? Session["ClientScoreDetails"].ToString() : null); } set { Session["ClientScoreDetails"] = value; } }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindScore();
        }
    }

    protected void BindScore()
    {
        try
        {
            string sScore = "", sScoreDetails = "";

            if (ScoreDetails != null)
                sScoreDetails = ScoreDetails;

            if (Score != null)
                sScore = Score.ToString();
            else if (RefCustomer != null)
            {
                if (RefCustomer != null)
                    XmlCustomerInfos = Client.GetCustomerInformations((int)RefCustomer);

                ParseXmlCustomerInfos(XmlCustomerInfos, out sScore, out sScoreDetails);
            }
            else if (_sClientScore != null)
            {
                sScore = _sClientScore;
                //sScoreDetails = _sClientScoreDetails;
            }

            //SAVE IN SESSION
            _sClientScore = sScore;
            _sClientScoreDetails = sScoreDetails;

            lblScore.Text = sScore;
            if (!String.IsNullOrWhiteSpace(sScoreDetails))
            {
                DataTable dtResult = new DataTable();

                string[] arCategory = sScoreDetails.Split('|');
                if(arCategory.Length > 1) // multi
                {
                    dtResult.Columns.Add("Label");
                    dtResult.Columns.Add("Value");
                    dtResult.Columns.Add("Details", typeof(DataTable));
                    dtResult.Columns.Add("IsMax", typeof(bool));

                    int iScoreMax = 0;
                    char cCatMax = ' ';

                    for (int i = 0; i < arCategory.Length; i++)
                    {
                        string[] arCategoryDetails = arCategory[i].Trim().Split('/');
                        if (arCategoryDetails.Length == 3)
                        {
                            int iValue;
                            if (int.TryParse(arCategoryDetails[1].Trim(), out iValue))
                            {
                                if (iValue > iScoreMax)
                                {
                                    iScoreMax = iValue;
                                    cCatMax = arCategoryDetails[0].Trim().ToString()[0];
                                }
                                else if (iValue == iScoreMax)
                                {
                                    if (arCategoryDetails[0].Trim().ToString()[0] != 'L')
                                    {
                                        if (arCategoryDetails[0].Trim().ToString()[0] == 'V')
                                            cCatMax = arCategoryDetails[0].Trim().ToString()[0];
                                    }
                                }

                                dtResult.Rows.Add(new object[] { arCategoryDetails[0].Trim(), iValue, GetScoreDetails(arCategoryDetails[2].Trim()), false });
                            }
                        }
                    }

                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {
                        if (dtResult.Rows[i]["Label"].ToString().StartsWith(cCatMax.ToString()))
                        {
                            dtResult.Rows[i]["IsMax"] = true;
                        }
                    }

                    //lblScore.Text = iScoreMax + "(" + cCatMax + ")";
                    lblScore.Text = iScoreMax.ToString();

                    if (dtResult.Rows.Count > 0)
                    {
                        //panelCategory.Visible = true; // never show after scoring changes
                        rptCategory.DataSource = dtResult;
                        rptCategory.DataBind();
                    }
                }
                else
                {
                    dtResult.Columns.Add("Details", typeof(DataTable));
                    dtResult.Columns.Add("IsMax", typeof(bool));
                    dtResult.Rows.Add(new object[] { GetScoreDetails(arCategory[0]), true });
                }

                //if (_sSortColumn != null)
                //    dt.DefaultView.Sort = _sSortColumn + " " + _sSortDirection;
                //dt = dt.DefaultView.ToTable();

                rptDetailsRepeat.DataSource = dtResult;
                rptDetailsRepeat.DataBind();
            }
            else panelDetails.Visible = false;

            if (!String.IsNullOrWhiteSpace(Label))
                lblLabel.Text = Label;
            switch (Type)
            {
                case "client":
                    panelIcon.CssClass = "scoring-icon client-icon";
                    break;
                case "alert":
                    panelIcon.CssClass = "scoring-icon alert-icon";
                    break;
                case "global":
                    panelIcon.CssClass = "scoring-icon global-icon";
                    break;
            }
        } catch(Exception e)
        {
            lblScore.Text = "-";
        }
    }

    protected DataTable GetScoreDetails(string sDetails)
    {
        string[] arData = sDetails.Split(';');

        DataTable dt = new DataTable();
        dt.Columns.Add("Label");
        dt.Columns.Add("Value");
        dt.Columns.Add("Alert", typeof(bool));

        for (int i = 0; i < arData.Length; i++)
        {
            if (!String.IsNullOrWhiteSpace(arData[i].Trim()))
            {
                string[] arInnerData = arData[i].Trim().Split(':');
                if (arInnerData.Length >= 2)
                {
                    DataRow row = dt.NewRow();
                    row["Label"] = arInnerData[0].Trim();
                    row["Value"] = arInnerData[1].Trim();
                    if (arInnerData.Length > 2)
                        row["Alert"] = arInnerData[2].Trim() == "YES" ? true : false;
                    else row["Alert"] = false;
                    dt.Rows.Add(row);
                }
            }
        }

        if (dt.Rows.Count == 0)
            dt.Rows.Add(new object[] { "RAS", "", false });

        dt.DefaultView.Sort = "Alert DESC";
        dt = dt.DefaultView.ToTable();
        return dt;
    }

    protected void ParseXmlCustomerInfos(string sXmlCustomerInfos, out string sScore, out string sScoreDetails)
    {
        List<string> listScoring = CommonMethod.GetAttributeValues(XmlCustomerInfos, "ALL_XML_OUT/Customer", "ClientScore");
        sScore = listScoring[0];

        List<string> listScoreDetails = CommonMethod.GetAttributeValues(XmlCustomerInfos, "ALL_XML_OUT/Customer", "ScoreDetails");
        sScoreDetails = listScoreDetails[0];
    }

    protected void lnkHeader_Click(object sender, EventArgs e)
    {
        LinkButton lnk = (LinkButton)sender;
        string sOrderBy = null;

        if(lnk != null)
        {
            switch (lnk.ID)
            {
                case "lnkCritera":
                    sOrderBy = "Label";
                    break;
                case "lnkScore":
                    sOrderBy = "Value";
                    break;
            }
        }

        if (_sSortColumn != null && _sSortColumn == sOrderBy)
            _sSortDirection = (_sSortDirection == "ASC") ? "DESC" : "ASC";
        else _sSortDirection = "ASC";

        _sSortColumn = sOrderBy;

        BindScore();
    }
}