﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ClientFileManager.ascx.cs" Inherits="API_ClientFileManager" %>

<style>
    .green-button {
        background-color:green;
        color:#fff;
        border:1px solid green;
    }
    .red-button {
        background-color:red;
        color:#fff;
        border:1px solid red;
    }
</style>

<div class="clientfileManager-panel-loading panel-loading"></div>

<div class="file-Manager-form">
    <asp:UpdatePanel ID="upClientFile" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
        <ContentTemplate>

            <div style="margin:10px 0">
                <span class="label">Type</span>
                <div>
                    <asp:DropDownList ID="ddlDocumentTypeFilter" runat="server" CssClass="" DataTextField="Label" DataValueField="TAG" 
                        AppendDataBoundItems="true" OnSelectedIndexChanged="ddlDocumentType_SelectedIndexChanged" AutoPostBack="true" style="width:100%">
                        <asp:ListItem Text="" Value=""></asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>

            <asp:Repeater ID="rptClientFile" runat="server">
                <HeaderTemplate>
                    <table class="defaultTable2" style="table-layout:fixed">
                        <tr>
                            <th style="width:25%;">
                                Nom
                            </th>
                            <%--<th>
                                Description
                            </th>--%>
                            <th style="padding-left:10px">
                                Type
                            </th>
                            <th style="padding-left:10px">
                                Date
                            </th>
                            <th style='<%=String.Format("text-align:center;width:75px;padding-left:15px{0}", !AllowCheck ? ";display:none" : "") %>'>
                            </th>
                            <th style='<%=String.Format("text-align:center;width:75px{0}", !AllowCheck ? ";display:none" : "") %>'>
                            </th>
                            <th style="text-align:center;width:140px">
                            </th>
                            <th style='<%=String.Format("width:25px;text-align:center{0}", !AllowSelect ? ";display:none" : "") %>'></th>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                        <tr class='<%#Container.ItemIndex % 2 == 0 ? "row-even" : "row-odd" %>'>
                            <td  class="trimmed-filename" style="vertical-align:middle">
                                <%#Eval("FileName") %>
                                <asp:Image ID="imgCopy" runat="server" ImageUrl="~/Styles/Img/content_copy.png" title="Copier dans le presse-papier" CssClass="copy-to-clipboard" data-clipboard-text='<%#Eval("FileName") %>' />
                            </td>
                            <%--<td style="vertical-align:middle">
                                <%#Eval("Description") %>
                            </td>--%>
                            <td style="vertical-align:middle">
                                <%#Eval("CategoryLabel") %>
                            </td>
                            <td style="vertical-align:middle;text-align:center">
                                <%#Eval("LastUploadDate").ToString().Length >= 16 ? Eval("LastUploadDate").ToString().Substring(0,16) : Eval("LastUploadDate").ToString() %>
                            </td>
                            <td style='<%=String.Format("vertical-align:middle;text-align:center{0}", !AllowCheck ? ";display:none" : "") %>'>
                                <%--<asp:Button ID="btnAccept" runat="server" CssClass="green-button" Text="Accepter" OnClientClick='<%# "return AcceptFile(\"" + Eval("FileName").ToString().Trim() + "\");" %>' CommandArgument='<%#Eval("FileName") %>' Visible='<%# (Eval("CheckFile").ToString() == "1") ? true : false %>' />--%>
                                <asp:Panel ID="panelBtnAccept" runat="server" Visible='<%# (Eval("CheckFile").ToString() == "1") ? true : false %>'>
                                    <%# GetInput(Eval("FileName").ToString(), true) %>
                                </asp:Panel>
                            </td>
                            <td style='<%=String.Format("vertical-align:middle;text-align:center{0}", !AllowCheck ? ";display:none" : "") %>'>
                                <%--<asp:Button ID="btnDecline" runat="server" CssClass="red-button" Text="Refuser" OnClientClick='<%# "return DeclineFile(\"" + Eval("FileName").ToString().Trim() + "\");" %>' CommandArgument='<%#Eval("FileName") %>' Visible='<%# (Eval("CheckFile").ToString() == "1") ? true : false %>' />--%>
                                <asp:Panel ID="panelBtnDecline" runat="server" Visible='<%# (Eval("CheckFile").ToString() == "1") ? true : false %>'>
                                    <%# GetInput(Eval("FileName").ToString(), false) %>
                                </asp:Panel>
                            </td>
                            <td style="vertical-align:middle;text-align:center">
                                <asp:HyperLink ID="btnOpen" runat="server" CssClass="file-open-button" Target="_blank" NavigateUrl='<%#Eval("URL") %>' Text="Ouvrir" Visible='<%#Eval("LocalStorage") %>' />
                                <asp:Button ID="btnCloudDownload" runat="server" CssClass="cloud-download-button" Text="Télécharger" CommandArgument='<%#Eval("FileName") %>' OnClick="btnCloudDownload_Click" Visible='<%#!(bool)Eval("LocalStorage")%>' />
                            </td>
                            <td style='<%=String.Format("{0}", !AllowSelect ? "display:none" : "") %>'>
                                <asp:CheckBox ID="ckbSelect" runat="server" />
                            </td>
                        </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
    </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upClientFileCheck" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
        <ContentTemplate>
            <asp:Button ID="btnDecline" runat="server" OnClick="btnDecline_Click" style="display:none" />
            <asp:Button ID="btnAccept" runat="server" OnClick="btnAccept_Click" style="display:none" />
            <asp:HiddenField ID="hfFileNameToCheck" runat="server" Value="" />
            
            <div style="display:none;">
                <asp:Literal ID="hfDocsMessages" runat="server"></asp:Literal>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnAccept" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnDecline" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
</div>

<script type="text/javascript">
    Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(ClientFileManagerBeginRequestHandler);
    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(ClientFileManagerEndRequestHandler);
    function ClientFileManagerBeginRequestHandler(sender, args) {
        try {
            pbControl = args.get_postBackElement();

            if (pbControl != null) {
                if (pbControl.id.indexOf('btnOpen') != -1
                    || pbControl.id.indexOf('btnCloudDownload') != -1
                    || pbControl.id.indexOf('ddlDocumentTypeFilter') != -1
                    || pbControl.id.indexOf('btnAccept') != -1
                    || pbControl.id.indexOf('btnDecline') != -1)
                    ShowFileManagerLoading();
            }
        }
        catch (e) {
            console.log(e);
        }
    }
    function ClientFileManagerEndRequestHandler(sender, args) {
        HideFileManagerLoading();
        CheckDocsMessages();
    }
    function ShowFileManagerLoading() {
        try {
            var controlID = '.file-Manager-form';
            var panel = $(controlID);

            $('.clientfileManager-panel-loading').show();
            $('.clientfileManager-panel-loading').width(panel.outerWidth());
            $('.clientfileManager-panel-loading').height(panel.outerHeight());

            $('.clientfileManager-panel-loading').position({
                my: 'center',
                at: 'center',
                of: controlID
            });
        }
        catch (ex) { console.log(ex);}
    }
    function HideFileManagerLoading() {
        $('.clientfileManager-panel-loading').hide();
    }
    function InitCopyToClipboard() {
        new Clipboard('.copy-to-clipboard');
    }

    function CheckDocsMessages() {
        try {
            if ($('#hfDocsMessages').html() != null && $('#hfDocsMessages').html().trim().length > 0) {
                var sTitle = $('#hfDocsMessages').html().split('|')[0];
                var sMessage = $('#hfDocsMessages').html().split('|')[1];
                $('#hfDocsMessages').html('');

                AlertMessage(sTitle, sMessage);
            }
        }
        catch (ex) {
            console.log(ex.toString());
        }
    }

    function AcceptFile(cmdArg) {
        console.log(cmdArg);
        $('#<%=hfFileNameToCheck.ClientID%>').val("");
        if (cmdArg != null && cmdArg.trim().length > 0) {
            $('#<%=hfFileNameToCheck.ClientID%>').val(cmdArg);
            $('#<%=btnAccept.ClientID%>').click();
        }
        //return false;
    }

    function DeclineFile(cmdArg) {
        console.log(cmdArg);
        $('#<%=hfFileNameToCheck.ClientID%>').val("");
        if (cmdArg != null && cmdArg.trim().length > 0) {
            $('#<%=hfFileNameToCheck.ClientID%>').val(cmdArg);
            $('#<%=btnDecline.ClientID%>').click();
        }
        //return false;
    }
</script>