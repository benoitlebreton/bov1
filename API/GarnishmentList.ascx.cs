﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using XMLMethodLibrary;

public partial class API_GarnishmentList : System.Web.UI.UserControl
{
    [Serializable]
    public class GarnishmentFilters
    {
        public int iRefCustomer { get; set; }
        public string sStatus { get; set; }
        public DocumentType dtDocumentType { get; set; }
        public string sRevival { get; set; }
        public string sReserved { get; set; }
        public int iPageSize { get; set; }
        public int iPageIndex { get; set; }
    }
    [Serializable]
    public class DocumentType
    {
        public bool bRevival { get; set; }
        public bool bIndebtedness { get; set; }
        public bool bOthers { get; set; }
    }

    public string RefCustomer { get { return (ViewState["RefCustomer"] != null) ? ViewState["RefCustomer"].ToString() : ""; } set { ViewState["RefCustomer"] = value; } }
    private string _RefCustomer { get { return (ViewState["_RefCustomer"] != null) ? ViewState["_RefCustomer"].ToString() : ""; } set { ViewState["_RefCustomer"] = value; } }
    public string Status { get { return (ViewState["GarnishmentStatus"] != null) ? ViewState["GarnishmentStatus"].ToString() : ""; } set { ViewState["GarnishmentStatus"] = value; } }
    private int _iPage
    {
        get { try { return int.Parse(ViewState["G_Page"].ToString()); } catch (Exception ex) { return 1; } }
        set { ViewState["G_Page"] = value; }
    }
    private int _iPageSize
    {
        get { try { return int.Parse(ViewState["G_PageSize"].ToString()); } catch (Exception ex) { return 10; } }
        set { ViewState["G_PageSize"] = value; }
    }
    public GarnishmentFilters _GarnishmentFilters
    {
        get
        {
            try
            {
                return (ViewState["GarnishmentFilters"] != null) ? (GarnishmentFilters)ViewState["GarnishmentFilters"] : null;
            }
            catch(Exception ex)
            {
                return null;
            }
        }
        set { ViewState["GarnishmentFilters"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            CheckRights();
            ddlGarnishmentStatus.SelectedIndex = ddlGarnishmentStatus.Items.IndexOf(ddlGarnishmentStatus.Items.FindByValue(Status));

            SetGarnishmentList(false);
        }
    }

    protected void CheckRights()
    {
        string sXmlOut = tools.GetRights(authentication.GetCurrent().sToken, "SC_GarnishmentDetails");

        List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Page", "RC");

        if (listRC.Count == 0 || listRC[0] == "0")
        {
            List<string> listAction = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/Page/Action");

            for (int i = 0; i < listAction.Count; i++)
            {
                List<string> listTag = CommonMethod.GetAttributeValues(listAction[i], "Action", "TagAction");
                List<string> listActionAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ActionAllowed");
                List<string> listViewAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ViewAllowed");

                if (listTag.Count > 0)
                {
                    switch (listTag[0])
                    {
                        case "GarnishmentRejected":
                            if (listViewAllowed.Count > 0 && listViewAllowed[0] == "1")
                                ddlGarnishmentStatus.Items.Insert(ddlGarnishmentStatus.Items.Count-1, new ListItem("Rejeté", "Rejected"));
                            break;
                        case "GarnishmentRejectedFinal":
                            if (listViewAllowed.Count > 0 && listViewAllowed[0] == "1")
                                ddlGarnishmentStatus.Items.Insert(ddlGarnishmentStatus.Items.Count - 1, new ListItem("Rejeté def.", "RejectedFinal"));
                            break;
                        case "GarnishmentSecondTreatment":
                            if (listViewAllowed.Count > 0 && listViewAllowed[0] == "1")
                                ddlGarnishmentStatus.Items.Insert(ddlGarnishmentStatus.Items.Count - 1, new ListItem("A retraiter", "SecondTreatment"));
                            break;
                    }
                }
            }
        }
    }

    protected void SetGarnishmentList(bool bOnChange)
    {
        string sPage = "1";
        string sPageSize = "10";

        string sSelectedPage = (ddlPage.SelectedValue.ToString().Trim().Length > 0) ? ddlPage.SelectedValue : "1";
        if (!bOnChange) { sPage = (_iPage > 0) ? _iPage.ToString() : sSelectedPage; }
        else { sPage = sSelectedPage; }
        _iPage = int.Parse(sSelectedPage);

        string sSelectedPageSize = ddlNbResult.SelectedValue;
        sPageSize = (_iPageSize > 0) ? _iPageSize.ToString() : sSelectedPageSize;
        if (ddlNbResult.Items.FindByValue(sPageSize) == null)
        {
            ddlNbResult.Items.Add(new ListItem(sPageSize, sPageSize));
            ddlNbResult.Items.FindByValue(ddlNbResult.SelectedValue).Selected = false;
            ddlNbResult.Items.FindByValue(sPageSize).Selected = true;
        }
        _iPageSize = int.Parse(sPageSize);

        int iRefCustomer = 0;
        if (!String.IsNullOrWhiteSpace(RefCustomer) && int.TryParse(RefCustomer, out iRefCustomer))
        {
            _RefCustomer = RefCustomer;
            ddlGarnishmentReserved.Items.FindByValue("").Selected = true;
            //ddlRevival.Items.FindByValue("NotIncluded").Selected = true;
            ddlGarnishmentStatus.Items.FindByValue("Treated").Selected = true;
            panelGridFilters.Visible = false;
        }
        else if(!bOnChange && !string.IsNullOrEmpty(_RefCustomer)) { int.TryParse(_RefCustomer, out iRefCustomer); }

        _GarnishmentFilters = new GarnishmentFilters();
        _GarnishmentFilters.iRefCustomer = iRefCustomer;
        _GarnishmentFilters.sReserved = ddlGarnishmentReserved.SelectedValue;
        _GarnishmentFilters.sStatus = ddlGarnishmentStatus.SelectedValue;
        //_GarnishmentFilters.sRevival = ddlRevival.SelectedValue;
        _GarnishmentFilters.iPageSize = _iPageSize;
        _GarnishmentFilters.iPageIndex = _iPage;

        if (!bOnChange)
        {
            foreach (ListItem _listItem in cblDocumentType.Items) { _listItem.Selected = true; }
            upDocumentType.Update();
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "DocumentTypeKEY", "initDocumentType();", true);
        }

        _GarnishmentFilters.dtDocumentType = new DocumentType();
        foreach (ListItem _listItem in cblDocumentType.Items)
        {
            if (_listItem.Value == "Revival")
                _GarnishmentFilters.dtDocumentType.bRevival = _listItem.Selected;
            if (_listItem.Value == "Indebtedness")
                _GarnishmentFilters.dtDocumentType.bIndebtedness = _listItem.Selected;
            if (_listItem.Value == "Others")
                _GarnishmentFilters.dtDocumentType.bOthers = _listItem.Selected;
        }

        DataTable dt = GetGarnishmentList(authentication.GetCurrent().sToken, _GarnishmentFilters);
        gvGarnishment.DataSource = dt;
        gvGarnishment.DataBind();
    }

    protected DataTable GetGarnishmentList(string sCashierToken, GarnishmentFilters _filters)
    {
        SqlConnection conn = null;
        DataTable dt = new DataTable();
        if (!string.IsNullOrEmpty(sCashierToken) && _filters != null)
        {
            try
            {
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
                conn.Open();

                SqlCommand cmd = new SqlCommand("BankSI.P_GetGarnishments_V2", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
                cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);
                cmd.Parameters.Add("@ReturnSelect", SqlDbType.Bit);

                XElement xelem = new XElement("Garnishment",
                                    new XAttribute("CashierToken", sCashierToken),
                                    new XAttribute("RefCustomer", _filters.iRefCustomer.ToString()),
                                    new XAttribute("PageSize", _filters.iPageSize.ToString()),
                                    new XAttribute("PageIndex", _filters.iPageIndex.ToString()),
                                    new XAttribute("BOUserReserved", _filters.sReserved));

                if (!string.IsNullOrEmpty(_filters.sStatus))
                    xelem.Add(new XAttribute("StatusTAG", _filters.sStatus));

                //if (!string.IsNullOrEmpty(_filters.sRevival))
                    //xelem.Add(new XAttribute("RevivalTAG", _filters.sRevival));

                if(_filters.dtDocumentType != null)
                {
                    string sIndebtednessTAG = "Included";
                    string sRevivalTAG = "Included";

                    if(!_filters.dtDocumentType.bIndebtedness) { sIndebtednessTAG = "NotIncluded"; }
                    if (!_filters.dtDocumentType.bRevival) { sRevivalTAG = "NotIncluded"; }

                    if (_filters.dtDocumentType.bIndebtedness && !_filters.dtDocumentType.bOthers && !_filters.dtDocumentType.bRevival)
                    {
                        sIndebtednessTAG = "Only";
                    }
                    else if(!_filters.dtDocumentType.bIndebtedness && !_filters.dtDocumentType.bOthers && _filters.dtDocumentType.bRevival)
                    {
                        sRevivalTAG = "Only";
                    }

                    if(!string.IsNullOrEmpty(sIndebtednessTAG))
                        xelem.Add(new XAttribute("IndebtednessTAG", sIndebtednessTAG));
                    if(!string.IsNullOrEmpty(sRevivalTAG))
                        xelem.Add(new XAttribute("RevivalTAG", sRevivalTAG));
                }

                cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN", xelem).ToString();

                cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
                cmd.Parameters["@ReturnSelect"].Value = 1;

                cmd.ExecuteNonQuery();

                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;

                da.Fill(dt);

                // EDIT RESULTS
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        try
                        {
                            decimal dAmount = decimal.Round(decimal.Parse(dt.Rows[i]["Amount"].ToString(), NumberStyles.Currency, CultureInfo.GetCultureInfo("fr-FR")), 2, MidpointRounding.AwayFromZero);
                            dt.Rows[i]["Amount"] = dAmount.ToString();


                        }
                        catch (Exception e)
                        {
                        }

                    }

                    // GET NB RESULT
                    string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();
                    List<string> listRowCount = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Garnishment", "NbResult");
                    PaginationManagement((listRowCount.Count > 0) ? listRowCount[0] : "0", _filters.iPageIndex.ToString());
                }
                else
                {
                    divPagination.Visible = false;
                    lblNbOfResults.Text = "0";
                }
                
            }
            catch (Exception e)
            {
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        return dt;
    }

    protected DataTable GetGarnishmentList(string sCashierToken, string sRefCustomer, string sStatus, string sBoUserReserved, string sPageSize, string sPageIndex)
    {
        SqlConnection conn = null;
        DataTable dt = new DataTable();

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("BankSI.P_GetGarnishments_V2", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@ReturnSelect", SqlDbType.Bit);

            XElement xelem = new XElement("Garnishment",
                                new XAttribute("CashierToken", sCashierToken),
                                new XAttribute("RefCustomer", sRefCustomer),
                                new XAttribute("PageSize", sPageSize),
                                new XAttribute("PageIndex", sPageIndex),
                                new XAttribute("BOUserReserved", sBoUserReserved));
            if (sStatus.Length > 0)
                xelem.Add(new XAttribute("StatusTAG", sStatus));

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN", xelem).ToString();

            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
            cmd.Parameters["@ReturnSelect"].Value = 1;

            cmd.ExecuteNonQuery();

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;

            da.Fill(dt);

            // EDIT RESULTS
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                try
                {
                    decimal dAmount = decimal.Round(decimal.Parse(dt.Rows[i]["Amount"].ToString(), NumberStyles.Currency, CultureInfo.GetCultureInfo("fr-FR")), 2, MidpointRounding.AwayFromZero);
                    dt.Rows[i]["Amount"] = dAmount.ToString();
                }
                catch (Exception e)
                {
                }
            }

            // GET NB RESULT
            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();
            List<string> listRowCount = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Garnishment", "NbResult");
            PaginationManagement((listRowCount.Count > 0) ? listRowCount[0] : "0", sPageIndex);
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }

    protected void gvGarnishment_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HiddenField hdnRefGarnishment = (HiddenField)e.Row.FindControl("hdnRefGarnishment");
            HiddenField hdnTreated = (HiddenField)e.Row.FindControl("hdnTreated");
            Label lblBOUser = (Label)e.Row.FindControl("lblBOAgent");

            HiddenField hdnFlagCourrierSurendettement = (HiddenField)e.Row.FindControl("hdnFlagCourrierSurendettement");

            if (hdnRefGarnishment != null)
            {
                string sClickFunction = "";
                if (hdnTreated.Value.ToLower() == "true" || lblBOUser.Text == authentication.GetCurrent().sFirstName + " " + authentication.GetCurrent().sLastName)
                {
                    string sURL = "GarnishmentDetails.aspx?ref=" + hdnRefGarnishment.Value;
                    if (hdnFlagCourrierSurendettement.Value.ToLower() == "true")
                        sURL = "AvisCSDetails.aspx?ref=" + hdnRefGarnishment.Value;
                    if (!String.IsNullOrWhiteSpace(RefCustomer))
                        sURL += "&prev=fc";
                    sClickFunction = "document.location.href='" + sURL + "';";
                }
                else
                {
                    string sPrevFC = "";
                    if (!String.IsNullOrWhiteSpace(RefCustomer))
                        sPrevFC = "true";
                    else sPrevFC = "false";

                    if (!String.IsNullOrWhiteSpace(lblBOUser.Text))
                        sClickFunction = "Garnishment_RequestAccess('" + hdnRefGarnishment.Value + "', '" + lblBOUser.Text + "', " + sPrevFC + ", "+ hdnFlagCourrierSurendettement.Value.ToLower() + "); ";
                    else
                        sClickFunction = "CheckGarnishmentAvailability('" + hdnRefGarnishment.Value + "', " + hdnFlagCourrierSurendettement.Value.ToLower() + "); ";
                }
                e.Row.Attributes.Add("onclick", sClickFunction);
                e.Row.Attributes.Add("onmouseover", "this.style.cursor = 'pointer';this.style.backgroundColor = '#ff9966'");
                e.Row.Attributes.Add("onmouseout", "this.style.cursor = 'auto';this.style.backgroundColor = '#fff'");
            }
        }
    }

    protected DataTable getPages(int nbPages)
    {
        DataTable dt = new DataTable();

        dt.Columns.Add("L");
        dt.Columns.Add("V");

        for (int i = 1; i <= nbPages; i++)
        {
            dt.Rows.Add(i.ToString(), i.ToString());
        }

        return dt;
    }
    protected int CalculateTotalPages(double totalRows, int pageSize)
    {
        int totalPages = (int)Math.Ceiling(totalRows / pageSize);

        return totalPages;
    }
    protected void PaginationManagement(string sRowCount, string sPageIndex)
    {
        lblNbOfResults.Text = sRowCount;

        if (int.Parse(sRowCount) > int.Parse(ddlNbResult.SelectedValue.ToString()))
        {

            int selectedPage = ddlPage.SelectedIndex;

            DataTable dtPages = getPages(CalculateTotalPages(int.Parse(sRowCount), int.Parse(ddlNbResult.SelectedValue)));
            ddlPage.DataSource = dtPages;
            ddlPage.DataBind();

            int iPageIndex = 0;
            if (sPageIndex.Trim().Length > 0 && int.TryParse(sPageIndex, out iPageIndex))
            {
                if (ddlPage.Items.Count > 0 && ddlPage.Items.FindByValue(iPageIndex.ToString()) != null)
                    ddlPage.Items.FindByValue(iPageIndex.ToString()).Selected = true;
            }
            else if (Session["RegistrationPageIndex"] != null)
            {
                if (ddlPage.Items.Count > 0)
                    ddlPage.Items.FindByValue(Session["RegistrationPageIndex"].ToString()).Selected = true;
            }
            else if (ddlPage.Items.Count > selectedPage)
                ddlPage.SelectedIndex = selectedPage;

            if (int.Parse(ddlPage.SelectedValue) == ddlPage.Items.Count)
                btnNextPage.Enabled = false;
            else
                btnNextPage.Enabled = true;

            if (int.Parse(ddlPage.SelectedValue) == 1)
                btnPreviousPage.Enabled = false;
            else
                btnPreviousPage.Enabled = true;

            if (ddlPage.Items.Count > 1)
                divPagination.Visible = true;
            else
                divPagination.Visible = false;

            lblTotalPage.Text = " / " + ddlPage.Items.Count.ToString();
        }
        else
        {
            divPagination.Visible = false;
        }
    }

    protected void ddlNbResult_SelectedIndexChanged(object sender, EventArgs e)
    {
        _iPage = 1;
        SetGarnishmentList(true);
    }

    protected void btnPreviousPage_Click(object sender, EventArgs e)
    {
        int iNbPage;

        int.TryParse(ddlPage.SelectedValue, out iNbPage);
        iNbPage--;
        if (ddlPage.Items.FindByValue(iNbPage.ToString()) != null)
            ddlPage.SelectedValue = iNbPage.ToString();

        SetGarnishmentList(true);
    }

    protected void ddlPage_SelectedIndexChanged(object sender, EventArgs e)
    {
        SetGarnishmentList(true);
    }

    protected void btnNextPage_Click(object sender, EventArgs e)
    {
        int iNbPage;

        int.TryParse(ddlPage.SelectedValue, out iNbPage);
        iNbPage++;
        if (ddlPage.Items.FindByValue(iNbPage.ToString()) != null)
            ddlPage.SelectedValue = iNbPage.ToString();

        SetGarnishmentList(true);
    }

    protected void ddlGarnishment_SelectedIndexChanged(object sender, EventArgs e)
    {
        SetGarnishmentList(true);
    }

    protected void btnGarnishmentReservation_Click(object sender, EventArgs e)
    {
        string sRefGarnishment = hdnSelectedGarnishment.Value;
        if (Garnishment.SetGarnishmentStatus(sRefGarnishment, authentication.GetCurrent().sToken, "RESERVED", ""))
            Response.Redirect("GarnishmentDetails.aspx?ref=" + sRefGarnishment);
        else
        {
            SetGarnishmentList(false);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "AlertKEY", "AlertMessage('Erreur', \"Erreur lors de l'attribution.\");", true);
        }
    }

    protected void btnCourrierCSReservation_Click(object sender, EventArgs e)
    {
        string sRefGarnishment = hdnSelectedGarnishment.Value;
        if (AvisCS.SetAvisCSStatus(sRefGarnishment, authentication.GetCurrent().sToken, "RESERVED", ""))
            Response.Redirect("AvisCSDetails.aspx?ref=" + sRefGarnishment);
        else
        {
            SetGarnishmentList(false);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "AlertKEY", "AlertMessage('Erreur', \"Erreur lors de l'attribution.\");", true);
        }
    }

    protected void btnSearchByAccountNumber_Click(object sender, EventArgs e)
    {
        if (txtAccountNumber.Text.Trim().Length == 11)
        {
            DataTable dt = Client.GetClientList(txtAccountNumber.Text.Trim(), authentication.GetCurrent().sToken);
            if (dt.Rows.Count > 0)
            {
                _RefCustomer = dt.Rows[0]["RefCustomer"].ToString();
            }
            else _RefCustomer = "";
        }
        else _RefCustomer = "";

        SetGarnishmentList(false);
    }

    protected void btnCheckGarnishmentAvailability_Click(object sender, EventArgs e)
    {
        string sXmlOut = Garnishment.GetGarnishmentDetails(hdnRefGarnishmentToCheck.Value, authentication.GetCurrent().sToken);
        if (sXmlOut.Length > 0)
        {
            List<string> listBOUserReserved = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Garnishment", "BOUserReserved");

            string sBOUserReserved = "";
            if (listBOUserReserved.Count > 0 && !String.IsNullOrWhiteSpace(listBOUserReserved[0]))
                sBOUserReserved = listBOUserReserved[0];

            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "Garnishment_RequestAccess('" + hdnRefGarnishmentToCheck.Value + "', '" + sBOUserReserved + "');", true);
        }
    }

    protected void btnCheckCourrierCSAvailability_Click(object sender, EventArgs e)
    {
        string sXmlOut = AvisCS.GetAvisCSDetails(hdnRefGarnishmentToCheck.Value, authentication.GetCurrent().sToken);
        if (sXmlOut.Length > 0)
        {
            List<string> listBOUserReserved = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Garnishment", "BOUserReserved");

            string sBOUserReserved = "";
            if (listBOUserReserved.Count > 0 && !String.IsNullOrWhiteSpace(listBOUserReserved[0]))
                sBOUserReserved = listBOUserReserved[0];

            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "Garnishment_RequestAccess('" + hdnRefGarnishmentToCheck.Value + "', '" + sBOUserReserved + "', false, true);", true);
        }
    }

    protected void cblDocumentType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if(!string.IsNullOrEmpty(cblDocumentType.SelectedValue))
            SetGarnishmentList(true);
        else
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "AlertKEY", "AlertMessage('Erreur', \"Veuillez cocher au moins un type de document\");", true);
    }
}