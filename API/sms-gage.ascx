﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="sms-gage.ascx.cs" Inherits="API_sms_gage" %>

<div id="block-sms-gage">
    <div style="margin:2px 0 1px 0;">
        <img src="./Styles/Img/sms.png" />
        <span style="position:relative;left:2px;top:-4px;">
            <asp:Label ID="lblSMSUsed" runat="server" ForeColor="Black"></asp:Label>
            sur
            <asp:Label ID="lblSMSTotal" runat="server"></asp:Label>
            SMS consommés
        </span>
    </div>
    <div>
        <div id="HeaderSMSProgress"></div>
    </div>
    <div class="sms-infos">
        <ul>
            <li>
                <span>Total SMS utilisé<asp:Label ID="lblTotalSMSYearAccord" runat="server"></asp:Label> dans l'année :</span>
                <asp:Label ID="lblTotalSMSYear" runat="server" CssClass="font-orange"></asp:Label>
            </li>
            <li>
                <span>SMS inclus dans l'offre :</span>
                <asp:Label ID="lblSMSYearAllowed" runat="server" CssClass="font-orange"></asp:Label>
            </li>
            <li>
                <span>Pack<asp:Label ID="lblSMSPackUsedAccord1" runat="server"></asp:Label> de SMS additionnel<asp:Label ID="lblSMSPackUsedAccord2" runat="server"></asp:Label> facturé<asp:Label ID="lblSMSPackUsedAccord3" runat="server"></asp:Label><br />
                (<asp:Label ID="lblSMSPackPrice" runat="server"></asp:Label>&euro; le pack de <asp:Label ID="lblSMSPerPack" runat="server"></asp:Label> SMS) :</span>
                <asp:Label ID="lblSMSPackUsed" runat="server" CssClass="font-orange"></asp:Label>
            </li>
            <li class="last">
                <span>SMS utilisé<asp:Label ID="lblSMSActivePackUsedAccord" runat="server"></asp:Label> dans le pack actuel :</span>
                <asp:Label ID="lblSMSActivePackUsed" runat="server" CssClass="font-orange"></asp:Label>
            </li>
        </ul>
    </div>
</div>