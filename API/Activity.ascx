﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Activity.ascx.cs" Inherits="API_Activity" %>

<asp:Panel ID="panelActivity" runat="server" style="padding-left:10px">
    <div class="activity-container">
        <asp:Panel ID="panelIcon" runat="server" CssClass="activity-icon">
            <div>
                <asp:Label ID="lblLabel" runat="server">Inactif</asp:Label>
            </div>
            <div>
                <img src="../Styles/Img/sleep-512.png" />
            </div>
        </asp:Panel>
        <asp:Panel ID="panelDetails" runat="server" CssClass="activity-details">
            <div class="arrow-up"></div>
            <div class="arrow-bg"></div>
            <asp:Panel ID="panelLastActDate" runat="server">
                <div class="label">
                    <asp:Label ID="lblLastActivityDate" runat="server">Date de dernière activité</asp:Label>
                </div>
                <div>
                    <asp:Literal ID="litLastActivityDate" runat="server"></asp:Literal>
                </div>
            </asp:Panel>
            <asp:Panel ID="panelLastActCheck" runat="server">
                <div class="label">
                    <asp:Label ID="lblLastActivityCheck" runat="server">Date de dernière relance</asp:Label>
                </div>
                <div>
                    <asp:Literal ID="litLastActivityCheck" runat="server"></asp:Literal>
                </div>
            </asp:Panel>
            <asp:Panel ID="panelCDC" runat="server">
                <div class="label">
                    <asp:Label ID="lblCDCDate" runat="server">Date transfert CDC</asp:Label>
                </div>
                <div>
                    <asp:Literal ID="litCDCTransferDate" runat="server"></asp:Literal>
                </div>
            </asp:Panel>
        </asp:Panel>
    </div>
</asp:Panel>