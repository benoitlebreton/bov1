﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CAPAddress.ascx.cs" Inherits="API_CAPAddress" %>

<input type="button" class="button" value="Modifier l'adresse" onclick="javascript:ShowCAPDialog();" />

<div id="dialog-cap">
    <div>
        <div class="label">Code postal</div>
        <asp:TextBox ID="txtZipcode" runat="server" CssClass="txtZipcode" />
    </div>
    <div>
        <div class="label">Ville</div>
        <asp:TextBox ID="txtCity" runat="server" CssClass="txtCity" />
    </div>
    <div>
        <div class="label">Lieu dit / BP</div>
        <asp:TextBox ID="txtComplCity" runat="server" CssClass="txtComplCity" />
    </div>
    <div>
        <div class="label">Voie</div>
        <asp:TextBox ID="txtStreet" runat="server" CssClass="txtStreet" />
    </div>
    <div>
        <div class="label">Numéro</div>
        <asp:TextBox ID="txtStreetNumber" runat="server" CssClass="txtStreetNumber" />
    </div>
    <div>
        <div class="label">Complément d'adresse 1</div>
        <asp:TextBox ID="txtCompl1" runat="server" CssClass="txtCompl1" />
    </div>
    <div>
        <div class="label">Complément d'adresse 2</div>
        <asp:TextBox ID="txtCompl2" runat="server" CssClass="txtCompl2" />
    </div>

    <asp:TextBox ID="txtQualityCode" runat="server" CssClass="txtAddressQualityCode" style="display:none" />

    <div>
        <input type="button" value="Valider" class="button" onclick="javascript:ValidateAddress()" />
        <asp:UpdatePanel ID="upValidateAddress" runat="server">
            <ContentTemplate>
                <asp:Button ID="btnValidateAddress" runat="server" CssClass="btnValidateAddress" OnClick="btnValidate_Click" style="display:none" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</div>

<div id="dialog-cap-multi-address">
    <div id="select-list">

    </div>
    <div>
        <input type="button" value="Utiliser l'adresse saisie" class="button" onclick="javascript:ForceValidateAddress()" />
    </div>
</div>

<div id="dialog-cap-force-address">
    <div class="message"></div>
    <div>
        Voulez-vous quand même continuer ?
    </div>
    <div>
        <input type="button" value="OUI" class="button" onclick="javascript:ForceValidateAddress()" />
    </div>
</div>