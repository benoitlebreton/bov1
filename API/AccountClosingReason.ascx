﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AccountClosingReason.ascx.cs" Inherits="API_AccountClosingReason" %>

<asp:UpdatePanel ID="upReasons" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div>
            <asp:Repeater ID="rptReasons" runat="server">
                <ItemTemplate>
                    <div class="multi-item <%# Container.ItemIndex % 2 == 0 ? "even-row" : "odd-row" %>">
                        <div class="multi-row-data">
                            <asp:Literal ID="litLabel" runat="server" Text='<%# Eval("Label") %>'></asp:Literal>
                            <asp:HiddenField ID="hdnTag" runat="server" Value='<%# Eval("Tag") %>' />
                        </div>
                        <div class="multi-row-btn">
                            <asp:ImageButton ID="btnRemove" runat="server" ImageUrl="~/Styles/Img/moins.png" OnClick="btnRemove_Click" CssClass="minus" />
                        </div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </div>
        <div style="margin-top:5px">
            <asp:DropDownList ID="ddlReason" runat="server" CssClass="multi-row-data" DataTextField="Label" DataValueField="Tag">
            </asp:DropDownList>
            <div class="multi-row-btn">
                <asp:ImageButton ID="btnAdd" runat="server" ImageUrl="~/Styles/Img/plus.png" OnClick="btnAdd_Click" />
            </div>
            <div style="clear:both"></div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>