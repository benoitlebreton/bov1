﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ReturnFunds.ascx.cs" Inherits="API_ReturnFunds" %>

<asp:UpdatePanel ID="upReturnFunds" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:Repeater ID="rptReturnFunds" runat="server">
            <HeaderTemplate>
                <div class="histo-rf">
                    <div class="header">
                        <div class="multi-row-data">
                            <div class="flex-like-row">
                                <div>Montant</div>
                                <div>Bénéficiaire</div>
                                <div>IBAN</div>
                                <div>BIC</div>
                                <div>Libellé</div>
                            </div>
                        </div>
                    </div>
            </HeaderTemplate>
            <ItemTemplate>
                <div class="multi-item <%# Container.ItemIndex % 2 == 0 ? "even-row" : "odd-row" %>">
                    <div class="multi-row-data">
                        <div class="flex-like-row">
                            <div><asp:Literal ID="litAmount" runat="server" Text='<%#String.Format("{0} €", Eval("Amount")) %>'></asp:Literal></div>
                            <div><asp:Literal ID="litBeneficiary" runat="server" Text='<%#Eval("Beneficiary") %>'></asp:Literal></div>
                            <div><asp:Literal ID="litIBAN" runat="server" Text='<%#Eval("IBAN") %>'></asp:Literal></div>
                            <div><asp:Literal ID="litBIC" runat="server" Text='<%#Eval("BIC") %>'></asp:Literal></div>
                            <div><asp:Literal ID="litLabel" runat="server" Text='<%#Eval("Label") %>'></asp:Literal></div>
                        </div>
                    </div>
                    <div class="multi-row-btn">
                        <asp:ImageButton ID="btnRemove" runat="server" CssClass="minus" ImageUrl="~/Styles/Img/moins.png" CommandArgument="<%#Container.ItemIndex %>" OnClick="btnRemove_Click" />
                    </div>
                </div>
            </ItemTemplate>
            <FooterTemplate>
                </div>
            </FooterTemplate>
        </asp:Repeater>

        <asp:Panel ID="panelReturnFunds" runat="server" DefaultButton="btnAdd">
            <div id="return-funds-form">
                <div class="label">
                    Montant à envoyer
                </div>
                <div>
                    <asp:TextBox ID="txtAmount" runat="server" CssClass="multi-row-data" style="text-align:right" autocomplete="off"></asp:TextBox>
                    <span class="multi-row-btn">€</span>
                </div>

                <div class="label">
                    Nom bénéficiaire
                </div>
                <div>
                    <asp:TextBox ID="txtBeneficiary" runat="server" autocomplete="off"></asp:TextBox>
                </div>

                <div class="label">
                    IBAN
                </div>
                <div>
                    <asp:TextBox ID="txtIBAN" runat="server" CssClass="iban-input" autocomplete="off" onchange="SearchBIC()" />
                </div>

                <div class="label">
                    BIC
                </div>
                <div>
                    <asp:TextBox ID="txtBIC" runat="server" CssClass="bic-input" autocomplete="off" />
                </div>

                <div class="label">
                    Libellé
                </div>
                <div>
                    <asp:TextBox ID="txtLabel" runat="server" autocomplete="off"></asp:TextBox>
                </div>
            </div>
            <div style="margin-top:10px;text-align:right">
                <asp:Button ID="btnAdd" runat="server" Text="Ajouter" CssClass="MiniButton" OnClick="btnAdd_Click" style="margin-bottom:0" />
            </div>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>