﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DeathStatus.ascx.cs" Inherits="API_DeathStatus" %>

<asp:UpdatePanel ID="upDeathStatus" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:Panel ID="panelDeathStatus" runat="server" style="padding-left:10px">
            <div class="death-status-container">
                <asp:Panel ID="panelIcon" runat="server" CssClass="death-status-icon">
                    <div>
                        <asp:Label ID="lblLabel" runat="server">Décédé</asp:Label>
                    </div>
                    <div>
                        <asp:Label ID="lblSubLabel" runat="server"></asp:Label>
                    </div>
                    <div>
                        <img src="../Styles/Img/dcd_logo.png" />
                    </div>
                </asp:Panel>
                <asp:Panel ID="panelDetails" runat="server" CssClass="death-status-details">
                    <div class="arrow-up"></div>
                    <div class="arrow-bg"></div>
                    <div class="top-right-corner">
                        <asp:ImageButton ID="imgFalseAlarm" runat="server" CssClass="image-tooltip" ToolTip="Faux positif" src="../Styles/Img/no_dcd_logo.png" OnClientClick="ShowConfirmDeathFalseAlarmDialog();return false;" />
                    </div>
                    <asp:Panel ID="panelDeathDate" runat="server">
                        <div class="label">
                            <asp:Label ID="lblDeathDate" runat="server">Date de décès</asp:Label>
                        </div>
                        <div>
                            <asp:Literal ID="litDeathDate" runat="server"></asp:Literal>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="panelDeathPlace" runat="server">
                        <div class="label">
                            <asp:Label ID="lblDeathPlace" runat="server">Lieu de décès</asp:Label>
                        </div>
                        <div>
                            <asp:Literal ID="litDeathPlace" runat="server"></asp:Literal>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="panelDeathActNumber" runat="server">
                        <div class="label">
                            <asp:Label ID="lblDeathActNumber" runat="server">Numéro d'acte de décès</asp:Label>
                        </div>
                        <div>
                            <asp:Literal ID="litDeathActNumber" runat="server"></asp:Literal>
                        </div>
                    </asp:Panel>

                    <asp:UpdatePanel ID="upSuccessionInfos" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="panelStartSuccession" runat="server" style="margin-top:10px">
                                <asp:Button ID="btnStartSuccession" runat="server" Text="Succession" OnClick="btnStartSuccession_Click" CssClass="button" style="width:100%;margin:0" />
                            </asp:Panel>
                            <asp:Panel ID="panelSuccession" runat="server" Visible="false" CssClass="succession-details">
                                <div class="main-title">
                                    Succession
                                </div>
                                <div class="succession-infos">
                                    <div>
                                        <div class="label">
                                            <asp:Label ID="lblSuccessionDate" runat="server" AssociatedControlID="txtSuccessionDate">
                                                Date de manifestation
                                            </asp:Label>
                                        </div>
                                        <div>
                                            <asp:TextBox ID="txtSuccessionDate" runat="server" placeholder="jj/mm/yyyy" MaxLength="10"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="contact-infos">
                                        <div class="group-title">
                                            Ayant-droit
                                        </div>
                                        <div class="group-infos">
                                            <div>
                                                <div class="label">
                                                    <asp:Label ID="lblBeneficiaryFirstName" runat="server" AssociatedControlID="txtBeneficiaryFirstName">
                                                        Prénom
                                                    </asp:Label>
                                                </div>
                                                <div>
                                                    <asp:TextBox ID="txtBeneficiaryFirstName" runat="server"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div>
                                                <div class="label">
                                                    <asp:Label ID="lblBeneficiaryLastName" runat="server" AssociatedControlID="txtBeneficiaryLastName">
                                                        Nom
                                                    </asp:Label>
                                                </div>
                                                <div>
                                                    <asp:TextBox ID="txtBeneficiaryLastName" runat="server"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div>
                                                <div class="label">
                                                    <asp:Label ID="lblBeneficiaryPhoneNumber" runat="server" AssociatedControlID="txtBeneficiaryPhoneNumber">
                                                        Téléphone
                                                    </asp:Label>
                                                </div>
                                                <div>
                                                    <asp:TextBox ID="txtBeneficiaryPhoneNumber" runat="server"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div>
                                                <div class="label">
                                                    <asp:Label ID="lblBeneficiaryEmail" runat="server" AssociatedControlID="txtBeneficiaryEmail">
                                                        Email
                                                    </asp:Label>
                                                </div>
                                                <div>
                                                    <asp:TextBox ID="txtBeneficiaryEmail" runat="server"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div>
                                                <div class="label">
                                                    <asp:Label ID="lblBeneficiaryAddress" runat="server" AssociatedControlID="txtBeneficiaryAddress">
                                                        Adresse
                                                    </asp:Label>
                                                </div>
                                                <div>
                                                    <asp:TextBox ID="txtBeneficiaryAddress" runat="server" placeholder="N°, type, nom de voie"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div>
                                                <div class="label">
                                                    <asp:Label ID="lblBeneficiaryZipCode" runat="server" AssociatedControlID="txtBeneficiaryZipCode">
                                                        Code postal
                                                    </asp:Label>
                                                </div>
                                                <div>
                                                    <asp:TextBox ID="txtBeneficiaryZipCode" runat="server" MaxLength="5"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div>
                                                <div class="label">
                                                    <asp:Label ID="lblBeneficiaryCity" runat="server" AssociatedControlID="txtBeneficiaryCity">
                                                        Ville
                                                    </asp:Label>
                                                </div>
                                                <div>
                                                    <asp:TextBox ID="txtBeneficiaryCity" runat="server"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="group-title">
	                                        Notaire
                                        </div>
                                        <div class="group-infos">
	                                        <div>
		                                        <div class="label">
			                                        <asp:Label ID="lblNotaryFirstName" runat="server" AssociatedControlID="txtNotaryFirstName">
				                                        Prénom
			                                        </asp:Label>
		                                        </div>
		                                        <div>
			                                        <asp:TextBox ID="txtNotaryFirstName" runat="server"></asp:TextBox>
		                                        </div>
	                                        </div>
	                                        <div>
		                                        <div class="label">
			                                        <asp:Label ID="lblNotaryLastName" runat="server" AssociatedControlID="txtNotaryLastName">
				                                        Nom
			                                        </asp:Label>
		                                        </div>
		                                        <div>
			                                        <asp:TextBox ID="txtNotaryLastName" runat="server"></asp:TextBox>
		                                        </div>
	                                        </div>
	                                        <div>
		                                        <div class="label">
			                                        <asp:Label ID="lblNotaryPhoneNumber" runat="server" AssociatedControlID="txtNotaryPhoneNumber">
				                                        Téléphone
			                                        </asp:Label>
		                                        </div>
		                                        <div>
			                                        <asp:TextBox ID="txtNotaryPhoneNumber" runat="server"></asp:TextBox>
		                                        </div>
	                                        </div>
	                                        <div>
		                                        <div class="label">
			                                        <asp:Label ID="lblNotaryEmail" runat="server" AssociatedControlID="txtNotaryEmail">
				                                        Email
			                                        </asp:Label>
		                                        </div>
		                                        <div>
			                                        <asp:TextBox ID="txtNotaryEmail" runat="server"></asp:TextBox>
		                                        </div>
	                                        </div>
	                                        <div>
		                                        <div class="label">
			                                        <asp:Label ID="lblNotaryAddress" runat="server" AssociatedControlID="txtNotaryAddress">
				                                        Adresse
			                                        </asp:Label>
		                                        </div>
		                                        <div>
			                                        <asp:TextBox ID="txtNotaryAddress" runat="server" placeholder="N°, type, nom de voie"></asp:TextBox>
		                                        </div>
	                                        </div>
	                                        <div>
		                                        <div class="label">
			                                        <asp:Label ID="lblNotaryZipCode" runat="server" AssociatedControlID="txtNotaryZipCode">
				                                        Code postal
			                                        </asp:Label>
		                                        </div>
		                                        <div>
			                                        <asp:TextBox ID="txtNotaryZipCode" runat="server" MaxLength="5"></asp:TextBox>
		                                        </div>
	                                        </div>
	                                        <div>
		                                        <div class="label">
			                                        <asp:Label ID="lblNotaryCity" runat="server" AssociatedControlID="txtNotaryCity">
				                                        Ville
			                                        </asp:Label>
		                                        </div>
		                                        <div>
			                                        <asp:TextBox ID="txtNotaryCity" runat="server"></asp:TextBox>
		                                        </div>
	                                        </div>
                                        </div>
                                    </div>
                                </div>
                                <div style="text-align:center;margin-top:5px">
                                    <asp:Button ID="btnSaveSuccessionInfos" runat="server" Text="Enregistrer" CssClass="button" OnClick="btnSaveSuccessionInfos_Click" style="width:100%;margin:0" />
                                </div>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:Panel>
            </div>

        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>

<div id="dialog-confirm-false-alarm" style="display:none">
    <asp:UpdatePanel ID="upFalseAlarm" runat="server">
        <ContentTemplate>
            Voulez-vous supprimer le statut "décédé" du profil client ?
            <asp:Button ID="btnFalseAlarm" runat="server" OnClick="btnFalseAlarm_Click" style="display:none" />
        </ContentTemplate>
    </asp:UpdatePanel>
</div>

<script type="text/javascript">
    function InitSuccessionDatePicker() {
        $('#<%=txtSuccessionDate.ClientID %>').mask("99/99/9999");

        <%--$('#<%=txtSuccessionDate.ClientID%>:not([readonly])').datepicker({
            defaultDate: "0",
            dateFormat: "dd/mm/yy",
            dayNamesMin: ["Di", "Lu", "Ma", "Me", "Je", "Ve", "Sa"],
            monthNames: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"],
            firstDay: 1,
            maxDate: "0",
            numberOfMonths: 1
        });--%>
    }

    function ShowConfirmDeathFalseAlarmDialog()
    {
        $('#dialog-confirm-false-alarm').dialog({
            autoOpen: true,
            resizable: false,
            draggable: false,
            modal: true,
            minWidth: 400,
            zIndex: 99997,
            title: "Faux positif",
            buttons: [{ text: 'Confirmer', click: function () { $("#<%=btnFalseAlarm.ClientID %>").click(); $(this).dialog('destroy'); } }, { text: 'Annuler', click: function () { $(this).dialog('destroy'); } }]
        });
        $("#dialog-confirm-false-alarm").parent().appendTo(jQuery("form:first"));
    }
</script>