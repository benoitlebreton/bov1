﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IPConnectionList.ascx.cs" Inherits="API_IPConnectionList" %>

<asp:Panel ID="panelIP" runat="server">
    <asp:Panel ID="panelIPList" runat="server">
        <asp:Repeater ID="rIPList" runat="server">
            <HeaderTemplate>
                <table id="tIPList" cellpadding="0" cellspacing="0" style="width: 100%" class="defaultTable2">
                    <thead>
                        <tr>
                            <th>Adresse IP</th>
                            <th>Date connexion</th>
                        </tr>
                    </thead>
                    <tbody>
            </HeaderTemplate>
            <ItemTemplate>
                <tr class='<%# Container.ItemIndex % 2 == 0 ? "beneficiary-table-row" : "beneficiary-table-alternate-row" %>'>
                    <td style="text-align:center">
                        <span class="ip-address-to-lookup"><%# Eval("IpAddress") %></span>
                    </td>
                    <td style="text-align:center">
                        <%# Eval("ConnectionDate") %>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </tbody>
            </table>
            </FooterTemplate>
        </asp:Repeater>

        <asp:Repeater ID="rIPList2" runat="server" Visible="false">
            <HeaderTemplate>
                <table id="tIPList" cellpadding="0" cellspacing="0" style="width: 100%" class="defaultTable2">
                    <thead>
                        <tr>
                            <th>Adresse IP</th>
                            <th>Date min</th>
                            <th>Date max</th>
                            <th>Nb connexions</th>
                        </tr>
                    </thead>
                    <tbody>
            </HeaderTemplate>
            <ItemTemplate>
                <tr class='<%# Container.ItemIndex % 2 == 0 ? "beneficiary-table-row" : "beneficiary-table-alternate-row" %>'>
                    <td style="text-align:center">
                        <span class="ip-address-to-lookup"><%# Eval("IpAddress") %></span>
                    </td>
                    <td style="text-align:center">
                        <%# Eval("MinDate") %>
                    </td>
                    <td style="text-align:center">
                        <%# Eval("MaxDate") %>
                    </td>
                    <td style="text-align:center">
                        <%# Eval("NbConnections") %>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </tbody>
            </table>
            </FooterTemplate>
        </asp:Repeater>
    </asp:Panel>
    <asp:Panel ID="panelIPList_Empty" runat="server" Visible="false" Style="margin-top: 10px; font-weight:bold">Aucune connexion</asp:Panel>
</asp:Panel>