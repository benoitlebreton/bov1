﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class API_BlockingHistory : System.Web.UI.UserControl
{
    public int iRefCustomer
    {
        get
        {
            int _iRefCustomer = 0;
            int.TryParse((ViewState["iRefCustomer"] != null) ? ViewState["iRefCustomer"].ToString() : "0", out _iRefCustomer);
            return _iRefCustomer;
        }
        set { ViewState["iRefCustomer"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            panelBlockingHistoryList.Visible = true;
            panelBlockingHistoryListEmpty.Visible = false;

            DataTable dtLockList = ClientAlert.getCustomerLockList(iRefCustomer);

            if (dtLockList.Rows.Count > 0)
            {
                rptClientDebitActivatedLockList.DataSource = dtLockList;
                rptClientDebitActivatedLockList.DataBind();
            }
            else
            {
                panelBlockingHistoryList.Visible = false;
                panelBlockingHistoryListEmpty.Visible = true;
            }
        }
    }

    protected string GetClientDebitLockUnlockDetails(string sLockDate, string LockBOUser, bool bLock)
    {
        string sLabel = "";
        if (sLockDate.Trim().Length > 0)
        {
            sLabel = "le " + sLockDate + ((LockBOUser.Trim().Length > 0) ? " par " + LockBOUser : "").ToString();
        }

        return sLabel;
    }
    protected bool GetBoolFromClientDebitLockUnlockStatus(string sStatus)
    {
        bool bStatus = false;

        if (sStatus.Trim() == "1")
            bStatus = true;

        return bStatus;
    }

    protected string GetBlockingStatus(string sActive)
    {
        string sClass = "";

        if (sActive.Trim() == "0")
            sClass = "ClientUnblocked";

        return sClass;
    }
}