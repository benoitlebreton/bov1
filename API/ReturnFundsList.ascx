﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ReturnFundsList.ascx.cs" Inherits="API_ReturnFundsList" %>

<script type="text/javascript">
    function InitDetailsSlider() {
        $('.account-close-row, .account-close-alt-row').unbind('click').click(function () {
            if($(this).find('.close-details').is(':hidden'))
                $(this).find('.close-details').slideDown();
            else $(this).find('.close-details').slideUp();
        });

        $('#<%=listHeader.ClientID%>').sticky({topSpacing:0});
    }

    function ShowConfirmReturnFundsDialog(refClose, clientName, refRow) {
        //console.log(refClose + ',' + clientName + ',' + refRow);
        event.stopPropagation();

        $('#<%=hdnRefClose.ClientID%>').val(refClose);
        $('#hdnRefRow').val(refRow);

        $('.return-funds-dest').empty().append(clientName);
        $('#div-approve-return-funds').dialog({
            autoOpen: true,
            resizable: false,
            draggable: false,
            modal: true,
            title: 'Approuver le retour de fonds',
            width: '350px',
            dialogClass: "no-close",
            buttons: [{ text: "Oui", click: function () { $(this).dialog("destroy"); $('#<%=btnApprove.ClientID%>').click(); } }, { text: "Non", click: function () { $(this).dialog("destroy"); } }]
            });
        $('#div-approve-return-funds').parent().appendTo(jQuery("form:first"));
    }
    function ShowCancelReturnFundsDialog(refClose, clientName, refRow) {
        //console.log(refClose + ',' + clientName + ',' + refRow);
        event.stopPropagation();

        $('#<%=hdnRefClose.ClientID%>').val(refClose);
        $('#hdnRefRow').val(refRow);

        $('.return-funds-dest').empty().append(clientName);
        $('#div-cancel-return-funds').dialog({
            autoOpen: true,
            resizable: false,
            draggable: false,
            modal: true,
            title: 'Annuler le retour de fonds',
            width: '350px',
            dialogClass: "no-close",
            buttons: [{ text: "Oui", click: function () { $(this).dialog("destroy"); $('#<%=btnCancel.ClientID%>').click(); } }, { text: "Non", click: function () { $(this).dialog("destroy"); } }]
            });
        $('#div-cancel-return-funds').parent().appendTo(jQuery("form:first"));
    }

    function InitTooltip() {
        $('.image-tooltip').tooltip({
            position: {
                my: "center bottom-10",
                at: "center top",
                using: function (position, feedback) {
                    $(this).css(position);
                    $("<div>")
                        .addClass("arrow")
                        .addClass(feedback.vertical)
                        .addClass(feedback.horizontal)
                        .appendTo(this);
                }
            }
        });
    }
</script>

<div>
    <asp:UpdatePanel ID="upRF" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Panel ID="panelLabel" runat="server" style="color:#344b56;font-weight:bold;text-transform:uppercase;border-bottom:1px solid #344b56; margin:10px 0 5px 0;">
                Retour de fonds
            </asp:Panel>
            <asp:Panel ID="panelList" runat="server">
                <div id="divList">
                    <table id="listHeader" runat="server" class="rf-sum">
                        <thead>
                            <tr>
                                <th></th>
                                <th id="headAccountNumber" runat="server">
                                    N° Compte
                                </th>
                                <th>Montant</th>
                                <th>Bénéficiaire</th>
                                <th>IBAN / BIC</th>
                                <th>Libellé</th>
                                <th>Par</th>
                            </tr>
                        </thead>
                    </table>
                    <asp:Repeater ID="rptRF" runat="server">
                        <ItemTemplate>
                            <div id="close-row-<%#Container.ItemIndex %>" class='<%# Container.ItemIndex % 2 == 0 ? "account-close-alt-row" : "account-close-row" %>'>
                                <%--<asp:Label ID="lblRefClose" runat="server" Text='<%#Eval("RefClose") %>'></asp:Label>
                                <asp:Label ID="lblRefCustomer" runat="server" Text='<%#Eval("RefCustomer") %>'></asp:Label>--%>
                    
                                <table class='<%#Eval("ShowAccountNumber").ToString() == "1" ? "rf-sum" : "rf-sum-short" %>'>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <asp:Image ID="imStatus" runat="server" ImageUrl='<%#GetStatusImageURL(Eval("Status").ToString()) %>' ToolTip='<%#Eval("StatusLabel") %>' CssClass="image-tooltip" />
                                            </td>
                                            <td style='<%#Eval("ShowAccountNumber").ToString() == "1" ? "" : "display:none" %>'>
                                                <asp:Label ID="lblAccountNumber" runat="server" Text='<%#Eval("AccountNumber") %>'></asp:Label>
                                            </td>
                                            <td style="text-align:right">
                                                <asp:Label ID="lblAmount" runat="server" Text='<%#Eval("Amount") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblBeneficiaryName" runat="server" Text='<%#Eval("BeneficiaryName") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblIBAN" runat="server" Text='<%#Eval("IBAN") %>'></asp:Label><br />
                                                <asp:Label ID="lblBIC" runat="server" Text='<%#Eval("BIC") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblLabel" runat="server" Text='<%#Eval("Label") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblBOUser" runat="server" Text='<%#Eval("BOUser") %>'></asp:Label>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <asp:Panel ID="panelDetails" runat="server" Visible='<%#(Eval("Status").ToString() != "1" && _bActionRights) ? true : false %>' CssClass="close-details">
                                    <div class="close-action">
                                        <div>
                                            <input id="btnCancel" type="button" value="Annuler" class="button red" onclick='<%#String.Format("ShowCancelReturnFundsDialog({0},\"{1}\",{2});", Eval("RefReturnFunds"), Eval("BeneficiaryName").ToString().Replace("'", "&apos;"), Container.ItemIndex) %>' />
                                        </div>
                                        <div>
                                            <input id="btnApprove" type="button" value="Approuver" class="button green" onclick='<%#String.Format("ShowConfirmReturnFundsDialog({0},\"{1}\",{2});", Eval("RefReturnFunds"), Eval("BeneficiaryName").ToString().Replace("'", "&apos;"), Container.ItemIndex) %>' />
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
                <div class="panel-loading"></div>
                <input type="hidden" id="hdnRefRow" />
            </asp:Panel>
            <asp:Panel ID="panelNoResult" runat="server" Visible="false" HorizontalAlign="Center">
                Aucun retour de fonds
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>

<div style="display:none">
    <asp:UpdatePanel ID="upAction" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="hdnRefClose" runat="server" />
            <asp:Button ID="btnApprove" runat="server" OnClick="btnApprove_Click" />
            <asp:Button ID="btnCancel" runat="server" OnClick="btnCancel_Click" />
        </ContentTemplate>
    </asp:UpdatePanel>
</div>

<div id="div-approve-return-funds" style="display:none">
    Etes-vous sûr de vouloir approuver le retour de fonds au nom de <span class="return-funds-dest"></span> ?
    <br /><br />
    <b>Attention :</b> cette opération est irréversible.
</div>

<div id="div-cancel-return-funds" style="display:none">
    Etes-vous sûr de vouloir annuler le retour de fonds au nom de <span class="return-funds-dest"></span> ?
</div>

<script type="text/javascript">
    Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(ReturnFundsListBeginRequestHandler);
    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(ReturnFundsListEndRequestHandler);
    function ReturnFundsListBeginRequestHandler(sender, args) {
        pbControl = args.get_postBackElement();

        var containerID = '';
        if ($('#hdnRefRow').length > 0 && pbControl != null) {
            //console.log(pbControl.id);
            if ($('#hdnRefRow').val().length > 0) {
                if (pbControl.id.indexOf('btnApprove') != -1 || pbControl.id.indexOf('btnEdit') != -1 || pbControl.id.indexOf('btnCancel') != -1)
                    ShowLoading('close-row-' + $('#hdnRefRow').val());
            }
            //else ShowLoading('divList');
        }
        //else ShowLoading('divList');
    }
    function ShowLoading(containerID) {
        var panel = $('#' + containerID);
        if (containerID.trim().length > 0 && $(panel).length) {

            $('.panel-loading').show();
            $('.panel-loading').width(panel.outerWidth());
            $('.panel-loading').height(panel.outerHeight());

            $('.panel-loading').position({
                my: 'center',
                at: 'center',
                of: panel
            });
        }
    }
    function ReturnFundsListEndRequestHandler(sender, args) {
        $('.panel-loading').hide();
    }
</script>