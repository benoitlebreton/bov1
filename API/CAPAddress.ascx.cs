﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class API_CAPAddress : System.Web.UI.UserControl
{
    public EventHandler AddressFinal;
    public string Zipcode { get { return txtZipcode.Text; } set { txtZipcode.Text = value; } }
    public string City { get { return txtCity.Text; } set { txtCity.Text = value; } }
    public string ComplementCity { get { return txtComplCity.Text; } set { txtComplCity.Text = value; } }
    public string Street { get { return txtStreet.Text; } set { txtStreet.Text = value; } }
    public string StreetNumber { get { return txtStreetNumber.Text; } set { txtStreetNumber.Text = value; } }
    public string Complement1 { get { return txtCompl1.Text; } set { txtCompl1.Text = value; } }
    public string Complement2 { get { return txtCompl2.Text; } set { txtCompl2.Text = value; } }
    public string QualityCode { get { return txtQualityCode.Text; } }
    public Dictionary<string, string> dicQualityCode = new Dictionary<string, string>
    {
        { "00", "Adresse valide" },
        { "04", "Entreprise forcée possédant un cedex" },
        { "10", "Adresse litigieuse" },
        { "20", "Adresse rejetée" },
        { "41", "CP/LOC forcé" },
        { "42", "Voie forcée" },
        { "53", "Numéro de voie forcé et adresse valide" },
        { "60", "Code qualité par défaut" },
        { "63", "Numéro de voie forcé dans une voie bornée" },
        { "70", "Adresse étrangère non traitée" },
        { "80", "Adresse tronquée par rapport à la norme du pays en cours" }
    };

    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!IsPostBack)
        //    InitScript();
    }

    public void InitScript()
    {
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(),"InitCAPAddress();", true);
    }

    protected void btnValidate_Click(object sender, EventArgs e)
    {
        if (AddressFinal != null)
            AddressFinal(sender, e);
    }
}