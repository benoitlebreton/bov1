﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="StaffManagement.ascx.cs" Inherits="API_StaffManagement" %>

<div style="background-color:#fff">
    <asp:UpdatePanel ID="upVendors" runat="server" UpdateMode="Conditional">
        <ContentTemplate>

            <asp:Repeater ID="rptList" runat="server">
                <ItemTemplate>
                    <div <%# Container.ItemIndex % 2 == 0 ? "" : " style=\"margin-top:15px\"" %>>
                        <div class="separator"><%#Eval("Label") %></div>
                        <asp:Repeater ID="rptVendors" runat="server" DataSource='<%#Eval("List") %>'>
                            <HeaderTemplate>
                                <table class="staff-table" style='width:100%'>
                                    <thead <%# ((RepeaterItem)Container.Parent.Parent).ItemIndex % 2 == 0 ? "" : "style=\"display:none\"" %>>
                                        <tr>
                                            <th><div>Nom</div></th>
                                            <th><div>Prénom</div></th>
                                            <th><div>Date de naissance</div></th>
                                            <th><div>Email</div></th>
                                            <th><div>Etat de la formation</div></th>
                                            <%--<th><div></div></th>--%>
                                        </tr>
                                    </thead>
                            </HeaderTemplate>
                            <ItemTemplate>
                                    <tr class='<%# Container.ItemIndex % 2 == 0 ? "staff-table-alternate-row" : "staff-table-row" %>'>
                                        <td style="width:18%"><%#Eval("LastName") %></td>
                                        <td style="width:18%"><%#Eval("FirstName") %></td>
                                        <td style="width:14%;text-align:center"><%#Eval("BirthDate") %></td>
                                        <td style="width:30%"><%#Eval("Email") %></td>
                                        <td style="text-align:center;padding:0;vertical-align:middle;">
                                            <div class="div-training">
                                                <span style='<%# String.Format("color:{0}", Eval("TrainingStatusColor")) %>'><%#Eval("TrainingStatusLabel") %></span>
                                                <div class="training-details">
                                                    <div>
                                                        <div>
                                                        Date inscription :<br />
                                                        <b><%#Eval("DateInscr") %></b>
                                                        </div>
                                                        <div style="margin-top:5px">
                                                        Date validation :<br />
                                                        <b><%#Eval("DateValid") %></b>
                                                        </div>
                                                        <div style='<%#String.Format("text-align:center;margin-top:5px;{0}", (Eval("TrainingStatus").ToString() == "0") ? "" : "display:none") %>'>
                                                            <asp:UpdatePanel ID="upSendTrainingNotification" runat="server">
                                                                <ContentTemplate>
                                                                    <input type="button" value="Renvoyer la formation" onclick='<%# string.Format("SendNotification({0});", Eval("Ref"))%>' class="MiniButton" style="font-size:1em;white-space:pre-wrap;height:auto;" />
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <%--<td style="width:25px;text-align:center">
                                            <img src="../Styles/Img/delete.png" height="25" 
                                                <%# ((RepeaterItem)Container.Parent.Parent).ItemIndex % 2 != 0 ? "" : "style=\"display:none\"" %>
                                                onclick='<%# String.Format("ShowConfirmDeleteDialog({0}, \"{1}\");", Eval("Ref"), Eval("LastName") + " " + Eval("FirstName")) %>' />
                                        </td>--%>
                                    </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                        <asp:Panel ID="panelEmpty" runat="server" Visible='<%#(int)Eval("Count") > 0 ? false : true %>' BackColor="White" style="padding:5px">
                            Aucun
                        </asp:Panel>
                    </div>
                </ItemTemplate>
            </asp:Repeater>

            <asp:HiddenField ID="hdnRefVendorNotification" runat="server" />
            <asp:Button ID="btnSendTrainingNotification" runat="server" Text="Renvoyer la formation" OnClick="btnSendTrainingNotification_Click" style="display:none" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <div style="padding:5px;background-color:#f57527;color:#fff;">
        <asp:Literal ID="litRatioDesc" runat="server"></asp:Literal>
    </div>
</div>

<%--<div style="text-align:center;margin-top:15px">
    <input type="button" class="orange-big-button" value="Ajouter un vendeur" onclick="ShowAddVendorDialog();" style="width:250px" />
</div>--%>
<div id="dialog-add-vendor" style="display:none">
    <asp:UpdatePanel ID="upAddVendor" runat="server">
        <ContentTemplate>    
            <div class="font-bold uppercase" style="margin:5px 0">
                Nom
            </div>
            <div>
                <asp:TextBox ID="txtLastName" runat="server" style="width:98%"></asp:TextBox>
            </div>
            <div class="font-bold uppercase" style="margin:10px 0 5px 0">
                Prénom
            </div>
            <div>
                <asp:TextBox ID="txtFirstName" runat="server" style="width:98%"></asp:TextBox>
            </div>
            <div class="font-bold uppercase" style="margin:10px 0 5px 0">
                Date de naissance
            </div>
            <div>
                <asp:TextBox ID="txtBirthDate" runat="server" placeholder="JJ/MM/AAAA" style="width:98%"></asp:TextBox>
            </div>
            <div class="font-bold uppercase" style="margin:10px 0 5px 0">
                Email
            </div>
            <div>
                <asp:TextBox ID="txtEmail" runat="server" style="width:98%"></asp:TextBox>
            </div>

            <div id="dialog-confirm-add-vendor" style="display:none">
                Je certifie que <span id="vendorNameToAdd"></span> est déclaré(e) auprès des organismes sociaux et fiscaux (URSSAF, TVA, etc. et est employé(e) régulièrement au regard des articles L. 320, L.143-3, R. 143-2 et L. 620-3 du Code du Travail.
                <asp:Button ID="btnAddVendor" runat="server" OnClick="btnAddVendor_Click" style="display:none" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
<div id="dialog-confirm-delete-vendor" style="display:none">
    <asp:UpdatePanel ID="upDeleteVendor" runat="server">
        <ContentTemplate>    
            <span class="vendorNameToDel"></span> n'aura plus le droit de réaliser les opérations Compte-Nickel et son accès au TPE Compte-Nickel sera bloqué.
            <br /><br />
            Etes-vous sûr de vouloir supprimer le vendeur <span class="vendorNameToDel"></span> ?
            <br /><br />
            <b>Rappel :</b> Il est rigoureusement interdit de travailler avec le code TPE d’une autre personne. En cas d’erreur, est responsable la personne dont le code a été utilisé pour réaliser l’opération.
            <asp:HiddenField ID="hdnRefVendorToDelete" runat="server" />
            <asp:Button ID="btnDeleteVendor" runat="server" OnClick="btnDeleteVendor_Click" style="display:none" />
        </ContentTemplate>
    </asp:UpdatePanel>
</div>

<script type="text/javascript">
    function ShowAddVendorDialog() {
        $('#dialog-add-vendor').dialog({
            autoOpen: true,
            resizable: false,
            draggable: false,
            modal: true,
            title: "Ajouter un nouveau vendeur",
            width: 400,
            buttons: [{
                text: 'Suivant', class: 'dialog-button', click: function () {
                    var vendorName = $('#<%=txtLastName.ClientID%>').val() + ' ' + $('#<%=txtFirstName.ClientID%>').val();
                    if(vendorName.trim().length > 0)
                        ShowConfirmAddVendorDialog(vendorName);
                }
            }]
        });
        $("#dialog-add-vendor").parent().appendTo(jQuery("form:first"));
    }
    function DestroyAddVendorDialog() {
        $('#dialog-add-vendor').dialog('destroy');
    }
    function ShowConfirmAddVendorDialog(vendorName) {
        $('#vendorNameToAdd').empty().append(vendorName);
        $('#dialog-confirm-add-vendor').dialog({
            autoOpen: true,
            resizable: false,
            draggable: false,
            modal: true,
            title: "Ajouter un nouveau vendeur",
            width: 360,
            buttons: [{ text: 'Ajouter', class: 'dialog-button', click: function () { ToggleDialogLoading('visible'); $("#<%=btnAddVendor.ClientID %>").click(); } }]
        });
        $("#dialog-confirm-add-vendor").parent().appendTo(jQuery("form:first"));
    }

    function ShowConfirmDeleteDialog(ref, vendorName) {
        $('#<%=hdnRefVendorToDelete.ClientID%>').val(ref);
        $('.vendorNameToDel').empty().append(vendorName);

        $('#dialog-confirm-delete-vendor').dialog({
            autoOpen: true,
            resizable: false,
            draggable: false,
            modal: true,
            title: "Supprimer un vendeur",
            width: 400,
            buttons: [
                {
                    text: 'Annuler', class: 'dialog-button', click: function () { $(this).dialog('destroy'); }
                },
                {
                    text: 'Supprimer', class: 'dialog-button', click: function () { ToggleDialogLoading('visible'); $("#<%=btnDeleteVendor.ClientID %>").click(); }
                }]
        });
        $("#dialog-confirm-delete-vendor").parent().appendTo(jQuery("form:first"));
    }
    function DestroyConfirmDeleteDialog() {
        $('#dialog-confirm-delete-vendor').dialog('destroy');
    }

    function ToggleDialogLoading(state) {
        $('.ui-dialog-buttonpane:visible .mini-loading').remove();
        if (state == "visible")
            $('<img class="mini-loading" src="Styles/Img/loading.gif" width="25px" height="25px" style="float:right;position:relative;left:-10px;top:10px" />').appendTo('.ui-dialog-buttonpane:visible');
    }

    function SendNotification(ref)
    {
        $('#<%=hdnRefVendorNotification.ClientID%>').val(ref);
        $('#<%=btnSendTrainingNotification.ClientID%>').click();
    }
</script>