﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using XMLMethodLibrary;

public partial class API_AgencyAlert : System.Web.UI.UserControl
{
    public string RefAgency
    {
        get { return (ViewState["RefAgency"] != null) ? ViewState["RefAgency"].ToString() : null; }
        set { ViewState["RefAgency"] = value; }
    }
    public bool ViewRights
    {
        get { return (ViewState["ViewRights"] != null) ? bool.Parse(ViewState["ViewRights"].ToString()) : false; }
        set { ViewState["ViewRights"] = value; }
    }
    public bool ActionRights
    {
        get { return (ViewState["ActionRights"] != null) ? bool.Parse(ViewState["ActionRights"].ToString()) : false; }
        set { ViewState["ActionRights"] = value; }
    }

    private DataTable _dtAgencyAlert {
        get { return (Session["AgencyAlert"] != null) ? (DataTable)Session["AgencyAlert"] : null; }
        set { Session["AgencyAlert"] = value; }
    }

    private string _sOrderBy {
        get { return (ViewState["OrderBy"] != null) ? ViewState["OrderBy"].ToString() : "DateAlerte"; }
        set { ViewState["OrderBy"] = value; }
    }
    private string _sOrderAsc
    {
        get { return (ViewState["OrderAsc"] != null) ? ViewState["OrderAsc"].ToString() : "Asc"; }
        set { ViewState["OrderAsc"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindFilters();

            BindKYCAlert();
        }

        ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
        scriptManager.RegisterPostBackControl(btnExport);
    }

    protected void BindFilters()
    {
        txtStartDate.Text = "01/01/2014";

        BindAgencyList();
        if (RefAgency != null)
        {
            panelAgencyControl.Visible = false;
            panelFilters.Attributes["style"] = "width:70%;margin:auto";
            panelExport.Visible = false;
        }
        else
        {
            if (!IsPostBack)
                ddlTreated.SelectedValue = "0";
        }

        DataTable dt = GetAlertType();
        ddlAlertType.DataSource = dt;
        ddlAlertType.DataBind();

        dt = GetActionType();
        ddlActionType.DataSource = dt;
        ddlActionType.DataBind();
        rptAction.DataSource = dt;
        rptAction.DataBind();
    }
    protected DataTable GetAlertType()
    {
        SqlConnection conn = null;
        DataTable dt = new DataTable();

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Distri"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("KYC.P_GetTypeAlerte", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }
    protected DataTable GetActionType()
    {
        SqlConnection conn = null;
        DataTable dt = new DataTable();

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Distri"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("KYC.P_GetTypeAction", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }

    protected void BindAgencyList()
    {
        try
        {
            ddlAgencyID.Items.Clear();
            DataTable dt = tools.GetAgencyList();
            DataRow dr = dt.NewRow();
            dr.ItemArray = new object[] { "TOUS", " " };
            dt.Rows.InsertAt(dr, 0);
            //ddlAgencyID.DataSource = GetAgencyList();
            ddlAgencyID.DataSource = dt;
            ddlAgencyID.DataBind();
        }
        catch (Exception ex)
        {
        }
    }

    protected void BindKYCAlert()
    {
        SqlConnection conn = null;
        DataTable dt = new DataTable();
        dt.Columns.Add("RefAlert");
        dt.Columns.Add("RefAgency");
        dt.Columns.Add("AgencyID");
        dt.Columns.Add("AgencyName");
        dt.Columns.Add("RefCustomer");
        dt.Columns.Add("AlertType");
        dt.Columns.Add("NoConformDate");
        dt.Columns.Add("NoConformAgent");
        dt.Columns.Add("EditDate");
        dt.Columns.Add("EditAgent");
        dt.Columns.Add("EditOldValue");
        dt.Columns.Add("EditNewValue");
        dt.Columns.Add("ConformDate");
        dt.Columns.Add("ConformAgent");
        dt.Columns.Add("AlertTreatedDate");
        dt.Columns.Add("AlertTreatedAgent");
        dt.Columns.Add("Actions");
        dt.Columns.Add("Comment");
        dt.Columns.Add("Status");
        dt.Columns.Add("StatusLabel");
        dt.Columns.Add("FalseAlarm");

        string sNbAlerts = "";
        string sPageIndex = "";

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Distri"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("KYC.P_SearchAlert", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@ReturnSelect", SqlDbType.Bit);

            XElement xmlin = new XElement("Alert");
            xmlin.Add(new XAttribute("CashierToken", authentication.GetCurrent().sToken));
            xmlin.Add(new XAttribute("DateFrom", txtDateFrom.Text));
            xmlin.Add(new XAttribute("DateTo", txtDateTo.Text));
            xmlin.Add(new XAttribute("AlertKind", ddlAlertType.SelectedValue));
            if(ddlTreated.SelectedValue.Length > 0)
                xmlin.Add(new XAttribute("Treated", ddlTreated.SelectedValue));
            if (ddlFalseAlarm.SelectedValue.Length > 0)
                xmlin.Add(new XAttribute("FalseAlarm", ddlFalseAlarm.SelectedValue));
            xmlin.Add(new XAttribute("IdAction", ddlActionType.SelectedValue));
            if(!string.IsNullOrWhiteSpace(ddlAgencyID.SelectedValue))
                xmlin.Add(new XAttribute("AgencyId", ddlAgencyID.SelectedValue));
            else if(RefAgency != null && RefAgency.Length > 0)
                xmlin.Add(new XAttribute("AgencyId", RefAgency));
            xmlin.Add(new XAttribute("PageSize", ddlNbResult.SelectedValue));
            if(ddlPage.SelectedValue.Length > 0)
                xmlin.Add(new XAttribute("PageIndex", ddlPage.SelectedValue));
            if(_sOrderBy.Length > 0)
                xmlin.Add(new XAttribute("OrderBy", _sOrderBy));
            if(_sOrderAsc.Length > 0)
                xmlin.Add(new XAttribute("OrderAsc", _sOrderAsc));

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN", xmlin).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
            cmd.Parameters["@ReturnSelect"].Value = 0;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if (sXmlOut.Length > 0)
            {
                // Get page settings
                List<string> listNbAlerts = CommonMethod.GetTagValues(sXmlOut, "ALL_XML_OUT/NB_ALERTS");
                List<string> listNbResults = CommonMethod.GetTagValues(sXmlOut, "ALL_XML_OUT/NB_RESULTS");
                List<string> listPageIndex = CommonMethod.GetTagValues(sXmlOut, "ALL_XML_OUT/PAGE_INDEX");
                if (listNbResults.Count > 0 && listNbResults[0].Length > 0)
                {
                    sNbAlerts = listNbResults[0];

                    lblNbOfResults.Text = listNbResults[0] + " résultat(s)";
                    if (listNbAlerts.Count > 0 && listNbAlerts[0].Length > 0)
                        lblNbOfResults.Text += " dont " + listNbAlerts[0] + " alerte(s)";
                }
                else
                {
                    lblNbOfResults.Text = "N.C.";
                    sNbAlerts = "0";
                }
                if (listPageIndex.Count > 0 && listPageIndex[0].Length > 0)
                    sPageIndex = listPageIndex[0];


                List<string> listAlerts = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/AGENCY_ALERT");

                for (int i = 0; i < listAlerts.Count; i++)
                {
                    List<string> listAgencyID = CommonMethod.GetTagValues(listAlerts[i], "AGENCY_ALERT/AGENCY_ID");
                    List<string> listRefAgency = CommonMethod.GetTagValues(listAlerts[i], "AGENCY_ALERT/REF_AGENCY");
                    List<string> listRefCustomer = CommonMethod.GetTagValues(listAlerts[i], "AGENCY_ALERT/REF_CUSTOMER");
                    List<string> listAlertRef = CommonMethod.GetTagValues(listAlerts[i], "AGENCY_ALERT/REF_ALERT");
                    List<string> listAlertType = CommonMethod.GetTagValues(listAlerts[i], "AGENCY_ALERT/ALERT_TYPE");
                    List<string> listAlertTreatedDate = CommonMethod.GetTagValues(listAlerts[i], "AGENCY_ALERT/ALERT_TREATED_DATE");
                    List<string> listAlertTreatedAgent = CommonMethod.GetTagValues(listAlerts[i], "AGENCY_ALERT/ALERT_TREATED_AGENT_NAME");
                    List<string> listActions = CommonMethod.GetTagValues(listAlerts[i], "AGENCY_ALERT/ACTIONS/ACTION");
                    List<string> listComment = CommonMethod.GetTagValues(listAlerts[i], "AGENCY_ALERT/COMMENT");
                    List<string> listFalseAlarm = CommonMethod.GetTagValues(listAlerts[i], "AGENCY_ALERT/FALSE_ALARM");

                    List<string> listNoConformDate = CommonMethod.GetTagValues(listAlerts[i], "AGENCY_ALERT/NOCONFORM_DATE");
                    List<string> listNoConformAgent = CommonMethod.GetTagValues(listAlerts[i], "AGENCY_ALERT/NOCONFORM_AGENT_NAME");

                    List<string> listEditDate = CommonMethod.GetTagValues(listAlerts[i], "AGENCY_ALERT/EDIT_DATE");
                    List<string> listEditAgent = CommonMethod.GetTagValues(listAlerts[i], "AGENCY_ALERT/EDIT_AGENT_NAME");
                    List<string> listEditOldValue = CommonMethod.GetTagValues(listAlerts[i], "AGENCY_ALERT/EDIT_OLD_VALUE");
                    List<string> listEditNewValue = CommonMethod.GetTagValues(listAlerts[i], "AGENCY_ALERT/EDIT_NEW_VALUE");

                    List<string> listConformDate = CommonMethod.GetTagValues(listAlerts[i], "AGENCY_ALERT/CONFORM_DATE");
                    List<string> listConformAgent = CommonMethod.GetTagValues(listAlerts[i], "AGENCY_ALERT/CONFORM_AGENT_NAME");

                    string sActions = "";
                    for (int x = 0; x < listActions.Count; x++)
                    {
                        if (x > 0)
                            sActions += ";";
                        sActions += listActions[x].ToString();
                    }

                    dt.Rows.Add(new object[] {
                        listAlertRef.Count > 0 ? listAlertRef[0] : "",
                        listRefAgency.Count > 0 ? listRefAgency[0] : "",
                        listAgencyID.Count > 0 ? listAgencyID[0] : "",
                        listAgencyID.Count > 0 ? GetAgencyName(listAgencyID[0]) : "",
                        listRefCustomer.Count > 0 ? listRefCustomer[0] : "",
                        listAlertType.Count > 0 ? GetAlertTypeLabel(listAlertType[0]) : "",
                        listNoConformDate.Count > 0 ? listNoConformDate[0] : "",
                        listNoConformAgent.Count > 0 ? listNoConformAgent[0] : "",
                        listEditDate.Count > 0 ? listEditDate[0] : "",
                        listEditAgent.Count > 0 ? listEditAgent[0] : "",
                        listEditOldValue.Count > 0 ? listEditOldValue[0] : "",
                        listEditNewValue.Count > 0 ? listEditNewValue[0] : "",
                        listConformDate.Count > 0 ? listConformDate[0] : "",
                        listConformAgent.Count > 0 ? listConformAgent[0] : "",
                        listAlertTreatedDate.Count> 0 ? listAlertTreatedDate[0] : "",
                        listAlertTreatedAgent.Count > 0 ? listAlertTreatedAgent[0] : "",
                        sActions,
                        listComment.Count > 0 ? listComment[0] : "",
                        listAlertTreatedDate.Count > 0 && listAlertTreatedDate[0].Length > 0 ? "1" : "0",
                        listAlertTreatedDate.Count > 0 && listAlertTreatedDate[0].Length > 0 ? "Traité" : "Non traité",
                        listFalseAlarm.Count > 0 ? listFalseAlarm[0] : ""
                    });
                }
            }
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        _dtAgencyAlert = dt;

        if (dt.Rows.Count > 0)
        {
            panelGrid.Visible = true;
            panelError.Visible = false;

            PaginationManagement(sNbAlerts, sPageIndex);

            gvKYCAlert.DataSource = dt;
            gvKYCAlert.DataBind();
            upSearch.Update();

            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "InitTooltipKEY", "initTooltip();", true);
        }
        else
        {
            panelGrid.Visible = false;
            panelError.Visible = true;
            lblError.Text = "Aucune alerte.";
        }
    }
    protected string GetAgencyName(string sAgencyID)
    {
        ListItem item = ddlAgencyID.Items.FindByValue(sAgencyID);
        if (item != null)
            return item.Text;
        else return sAgencyID;
    }
    protected string GetAlertTypeLabel(string sAlertCode)
    {
        ListItem item = ddlAlertType.Items.FindByValue(sAlertCode);
        if (item != null)
            return item.Text;
        else return sAlertCode;
    }

    protected void gvKYCAlert_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (RefAgency != null)
        {
            gvKYCAlert.Columns[1].ItemStyle.Width = Unit.Pixel(200);
            e.Row.Cells[3].Visible = false;
        }

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HiddenField hdnRefAlert = (HiddenField)e.Row.FindControl("hdnRefAlert");
            //Label bDeleteField = (Label)e.Row.FindControl("bDelete");

            if (hdnRefAlert != null)
            {
                for (int i = 0; i < e.Row.Cells.Count; i++)
                {
                    e.Row.Cells[i].Attributes.Add("onclick", "GetAlertDetails('" + hdnRefAlert.Value + "')");
                }
            }

            e.Row.Attributes.Add("onmouseover", "this.style.cursor = 'pointer';this.style.outline = '3px solid #ff9966';this.style.outlineOffset = '-3px'");
            e.Row.Attributes.Add("onmouseout", "this.style.cursor = 'auto';this.style.outline = 'none'");
        }
    }

    protected DataTable getPages(int nbPages)
    {
        DataTable dt = new DataTable();

        dt.Columns.Add("L");
        dt.Columns.Add("V");

        for (int i = 1; i <= nbPages; i++)
        {
            dt.Rows.Add(i.ToString(), i.ToString());
        }

        return dt;
    }
    protected int CalculateTotalPages(double totalRows, int pageSize)
    {
        int totalPages = (int)Math.Ceiling(totalRows / pageSize);

        return totalPages;
    }
    protected void PaginationManagement(string sRowCount, string sPageIndex)
    {
        if (int.Parse(sRowCount) > int.Parse(ddlNbResult.SelectedValue.ToString()))
        {
            int selectedPage = ddlPage.SelectedIndex;

            DataTable dtPages = getPages(CalculateTotalPages(int.Parse(sRowCount), int.Parse(ddlNbResult.SelectedValue)));
            ddlPage.DataSource = dtPages;
            ddlPage.DataBind();

            int iPageIndex = 0;
            if (sPageIndex.Trim().Length > 0 && int.TryParse(sPageIndex, out iPageIndex))
            {
                if (ddlPage.Items.Count > 0 && ddlPage.Items.FindByValue(iPageIndex.ToString()) != null)
                    ddlPage.Items.FindByValue(iPageIndex.ToString()).Selected = true;
            }
            else if (Session["RegistrationPageIndex"] != null)
            {
                if (ddlPage.Items.Count > 0)
                    ddlPage.Items.FindByValue(Session["RegistrationPageIndex"].ToString()).Selected = true;
            }
            else if (ddlPage.Items.Count > selectedPage)
                ddlPage.SelectedIndex = selectedPage;

            if (int.Parse(ddlPage.SelectedValue) == ddlPage.Items.Count)
                btnNextPage.Enabled = false;
            else
                btnNextPage.Enabled = true;

            if (int.Parse(ddlPage.SelectedValue) == 1)
                btnPreviousPage.Enabled = false;
            else
                btnPreviousPage.Enabled = true;

            if (ddlPage.Items.Count > 1)
                divPagination.Visible = true;
            else
                divPagination.Visible = false;

            lblTotalPage.Text = " / " + ddlPage.Items.Count.ToString();
        }
        else
        {
            divPagination.Visible = false;
        }
    }
    protected void nextPage(object sender, EventArgs e)
    {
        int iNbPage;

        int.TryParse(ddlPage.SelectedValue, out iNbPage);
        iNbPage++;
        if (ddlPage.Items.FindByValue(iNbPage.ToString()) != null)
            ddlPage.SelectedValue = iNbPage.ToString();

        BindKYCAlert();
    }
    protected void previousPage(object sender, EventArgs e)
    {
        int iNbPage;

        int.TryParse(ddlPage.SelectedValue, out iNbPage);
        iNbPage--;
        if (ddlPage.Items.FindByValue(iNbPage.ToString()) != null)
            ddlPage.SelectedValue = iNbPage.ToString();

        BindKYCAlert();
    }
    protected void onChangePageNumber(object sender, EventArgs e)
    {
        BindKYCAlert();
    }
    protected void onChangeNbResult(object sender, EventArgs e)
    {
        BindKYCAlert();
    }
    protected void btnGetAlertDetails_Click(object sender, EventArgs e)
    {
        ckbTreated.Checked = true;
        ckbFalseAlarm.Checked = false;

        string sRefAlert = hdnRefAlertToSearch.Value;
        bool bAlertFound = false;

        if (_dtAgencyAlert != null)
        {
            for (int i = 0; i < _dtAgencyAlert.Rows.Count; i++)
            {
                if (_dtAgencyAlert.Rows[i]["RefAlert"].ToString() == sRefAlert)
                {
                    bAlertFound = true;
                    DataRow row = _dtAgencyAlert.Rows[i];
                    lblAlertRef.Text = row["RefAlert"].ToString();
                    lblAlertType.Text = row["AlertType"].ToString();
                    linkAgencyDetails.Text = row["AgencyName"].ToString();
                    linkAgencyDetails.NavigateUrl = "~/AgencyDetails.aspx?ref=" + row["RefAgency"].ToString() + "&tab=alerts";
                    linkClientDetails.NavigateUrl = "~/ClientDetails.aspx?ref=" + row["RefCustomer"].ToString() + "&redirect=registration";

                    lblNoConformDate.Text = row["NoConformDate"].ToString();
                    lblNoConformAgent.Text = row["NoConformAgent"].ToString();

                    if (row["EditDate"].ToString().Length > 0)
                    {
                        panelEdit.Visible = true;
                        lblEditDate.Text = row["EditDate"].ToString();
                        lblEditAgent.Text = row["EditAgent"].ToString();
                        lblEditOldValue.Text = row["EditOldValue"].ToString();
                        lblEditNewValue.Text = row["EditNewValue"].ToString();
                    }
                    else
                    {
                        panelEdit.Visible = false;
                    }

                    lblConformDate.Text = row["ConformDate"].ToString();
                    lblConformAgent.Text = row["ConformAgent"].ToString();

                    if (row["AlertTreatedDate"].ToString().Length > 0 || !ActionRights)
                    {
                        panelAlertTreated.Visible = true;
                        panelEditButton.Visible = false;
                        lblAlertTreatedDate.Text = row["AlertTreatedDate"].ToString();
                        lblAlertTreatedAgent.Text = row["AlertTreatedAgent"].ToString();
                        ToggleActionCheckbox(false);
                        txtComment.Enabled = false;

                        if (row["FalseAlarm"].ToString() == "1")
                            panelFalseAlarm.Visible = true;
                        else panelFalseAlarm.Visible = false;
                    }
                    else
                    {
                        panelAlertTreated.Visible = false;
                        panelEditButton.Visible = true;
                        ToggleActionCheckbox(true);
                        txtComment.Enabled = true;
                    }

                    string[] arActions = row["Actions"].ToString().Split(';');
                    for (int y = 0; y < rptAction.Items.Count; y++)
                    {
                        HiddenField hdnRefAction = (HiddenField)rptAction.Items[y].FindControl("hdnIdAction");
                        CheckBox ckbAction = (CheckBox)rptAction.Items[y].FindControl("ckbAction");

                        if (arActions.Contains(hdnRefAction.Value))
                            ckbAction.Checked = true;
                        else ckbAction.Checked = false;
                    }

                    txtComment.Text = row["Comment"].ToString();
                }
            }

            if (bAlertFound)
            {
                upDetails.Update();
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "ShowDetailsPopupKey", "InitToggles(); ShowDetailsPopup();", true);
            }
        }
    }
    protected void ToggleActionCheckbox(bool bEnabled)
    {
        for (int i = 0; i < rptAction.Items.Count; i++)
        {
            CheckBox ckb = (CheckBox)rptAction.Items[i].FindControl("ckbAction");
            if (ckb != null)
                ckb.Enabled = bEnabled;
        }
    }

    protected string[] GetCheckedActions(){

        List<string> listActions = new List<string>();

        for (int i = 0; i < rptAction.Items.Count; i++)
        {
            HiddenField hdn = (HiddenField)rptAction.Items[i].FindControl("hdnIdAction");
            CheckBox ckb = (CheckBox)rptAction.Items[i].FindControl("ckbAction");

            if (hdn != null && ckb != null && ckb.Checked)
                listActions.Add(hdn.Value);
        }

        return listActions.ToArray();
    }

    protected void btnEditAlert_Click(object sender, EventArgs e)
    {
        if (SetAlert())
        {
            BindKYCAlert();
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "HideDetailsPopupKEY", "HideDetailsPopup();", true);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "HideDetailsPopupKEY", "InitToggles(); ShowAlertMessage(\"Une erreur est survenue.<br/>Veuillez réessayer.\");", true);
        }
    }

    protected bool SetAlert()
    {
        string sRefAlert = hdnRefAlertToSearch.Value;
        SqlConnection conn = null;
        bool bOk = false;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Distri"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("KYC.P_SetAlert", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);

            XElement xmlin = new XElement("ALERT",
                                            new XAttribute("CashierToken", authentication.GetCurrent(true).sToken),
                                            new XElement("ID", sRefAlert));
            string[] arActions = GetCheckedActions();
            for (int i = 0; i < arActions.Length; i++)
            {
                xmlin.Add(new XElement("ACTION", new XAttribute("ID", arActions[i])));
            }
            xmlin.Add(new XElement("COMMENT", txtComment.Text));
            xmlin.Add(new XElement("TREATED", ckbTreated.Checked ? "1" : "0"));
            xmlin.Add(new XElement("FALSE_ALARM", ckbFalseAlarm.Checked ? "1" : "0"));

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN", xmlin).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if (sXmlOut.Length > 0)
            {
                List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/RESULT", "RC");
                if (listRC.Count > 0 && listRC[0] == "0")
                    bOk = true;
            }
        }
        catch (Exception ex)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return bOk;
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindKYCAlert();
    }

    protected void gvKYCAlert_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (e.SortExpression == _sOrderBy)
        {
            if (_sOrderAsc == "Asc")
                _sOrderAsc = "Desc";
            else _sOrderAsc = "Asc";
        }
        else
        {
            _sOrderAsc = "Asc";
        }

        _sOrderBy = e.SortExpression;

        BindKYCAlert();
    }

    public string GetActionImg(string sActions)
    {
        StringBuilder sbImages = new StringBuilder();

        string[] arActions = sActions.Split(';');

        for (int i = 0; i < arActions.Length; i++)
        {
            string sImgUrl = "";
            string sTooltip = "";

            switch (arActions[i])
            {
                case "1":
                    sImgUrl = "Styles/Img/alerte_kyc_telephone.png";
                    break;
                case "2":
                    sImgUrl = "Styles/Img/alerte_kyc_lettre.png";
                    break;
                case "3":
                    sImgUrl = "Styles/Img/alerte_kyc_decommission.png";
                    break;
            }

            ListItem item = ddlActionType.Items.FindByValue(arActions[i]);

            if(item != null)
                sTooltip = item.Text;

            if(sImgUrl.Length > 0)
                sbImages.Append("<img src=\"" + sImgUrl + "\" class=\"image-tooltip\" title=\"" + sTooltip + "\" height=\"25px\" />");
        }

        return sbImages.ToString();
    }


    protected DataTable GetKYCAlert(string sDateFrom)
    {
        DataTable dt = new DataTable();
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Distri"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("KYC.P_ExportIncidentKYC", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN", new XElement("Customer", new XAttribute("DateStart", sDateFrom))).ToString();

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
        }
        catch(Exception e)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }
    protected void KYCAlertExport(DataTable dt, string sFilename)
    {
        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.ClearContent();
        HttpContext.Current.Response.ClearHeaders();
        HttpContext.Current.Response.Buffer = true;
        HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.UTF8;
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + sFilename + ".xlsx");

        using (ExcelPackage pack = new ExcelPackage())
        {
            ExcelWorksheet ws = pack.Workbook.Worksheets.Add(sFilename);
            ws.Cells["A1"].LoadFromDataTable(dt, true);

            //Set header style
            using (ExcelRange rng = ws.Cells["A1:W1"])
            {
                rng.Style.Font.Bold = true;
                rng.Style.Fill.PatternType = ExcelFillStyle.Solid;
                rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(245, 117, 39));
                rng.Style.Font.Color.SetColor(Color.White);
                rng.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            }

            ws.Column(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            ws.Column(2).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            ws.Column(4).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            ws.Column(9).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            ws.Column(11).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            ws.Column(14).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            ws.Column(15).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            ws.Column(16).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            ws.Column(19).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            ws.Column(20).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            ws.Column(23).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            ws.Cells[ws.Dimension.Address].AutoFitColumns();

            var ms = new System.IO.MemoryStream();
            pack.SaveAs(ms);
            ms.WriteTo(HttpContext.Current.Response.OutputStream);
        }

        HttpContext.Current.Response.Flush();
        HttpContext.Current.Response.End();
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        DateTime date;
        if(DateTime.TryParseExact(txtStartDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
        {
            string sFileName = "export_alertes_kyc_" + date.ToString("yyyyMMdd");
            KYCAlertExport(GetKYCAlert(date.ToString("dd/MM/yyyy")), sFileName);
        }
    }

    public int GetAgencyNbKYCAlert(int iRefAgency)
    {
        int iNbAlert = 0;
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Distri"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("KYC.F_GetNbPendingAlert", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@RefAgency", SqlDbType.VarChar, 8000);
            cmd.Parameters["@RefAgency"].Value = iRefAgency.ToString();

            cmd.Parameters.Add("@NbPendingAlertTransfer", SqlDbType.Int);
            cmd.Parameters["@NbPendingAlertTransfer"].Direction = ParameterDirection.ReturnValue;
            cmd.ExecuteNonQuery();

            iNbAlert = int.Parse(cmd.Parameters["@NbPendingAlertTransfer"].Value.ToString());
        }
        catch(Exception e)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return iNbAlert;
    }

    public void ShowUntreatedOnly()
    {
        txtDateFrom.Text = "";
        txtDateTo.Text = "";
        ddlFalseAlarm.SelectedIndex = 0;
        ddlAlertType.SelectedIndex = 0;
        ddlActionType.SelectedIndex = 0;
        ddlTreated.Items.FindByValue("0").Selected = true;

        BindKYCAlert();
    }
}