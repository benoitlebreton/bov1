﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

public partial class API_AmlSurveillance : System.Web.UI.UserControl
{
    private int _iRefCustomer { get { return int.Parse(ViewState["RefCustomer"].ToString()); } set { ViewState["RefCustomer"] = value; } }
    private bool _bSousSurveillance { get { return (ViewState["SousSurveillance"] != null) ? bool.Parse(ViewState["SousSurveillance"].ToString()) : false; } set { ViewState["SousSurveillance"] = value; } }
    private bool _bLockTransfer { get { return (ViewState["LockTransfer"] != null) ? bool.Parse(ViewState["LockTransfer"].ToString()) : false; } set { ViewState["LockTransfer"] = value; } }
    private bool _bRequestTransferValidation { get { return (ViewState["RequestTransferValidation"] != null) ? bool.Parse(ViewState["RequestTransferValidation"].ToString()) : false; } set { ViewState["RequestTransferValidation"] = value; } }
    private string _sRequestTransferValidationAmount { get { return (ViewState["RequestTransferValidationAmount"] != null) ? ViewState["RequestTransferValidationAmount"].ToString() : ""; } set { ViewState["RequestTransferValidationAmount"] = value; } }
    public bool ActionAllowed { get { return (ViewState["AmlSurvActionAllowed"] != null) ? bool.Parse(ViewState["AmlSurvActionAllowed"].ToString()) : false; } set { ViewState["AmlSurvActionAllowed"] = value; } }
    public string DisplayMode { get { return (ViewState["AmlSurvDisplayMode"] != null) ? ViewState["AmlSurvDisplayMode"].ToString() : ""; } set { ViewState["AmlSurvDisplayMode"] = value; } }
    public bool IsSurveillanceActive { get { return ckbMiseSousSurveillance.Checked; } }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            CheckRights();
            ScriptManager.RegisterClientScriptInclude(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "Scripts/toggles.js");
            Page.Header.Controls.Add(new System.Web.UI.LiteralControl("<link rel=\"stylesheet\" type=\"text/css\" href=\"" + ResolveUrl("~/Styles/AmlSurveillance.css") + "\" />"));
            hdnDisplayMode.Value = DisplayMode;
            if (DisplayMode == "AlertTreatment")
            {
                hdnMiseSousSurveillanceWidth.Value = "210";
                Page.Header.Controls.Add(new System.Web.UI.LiteralControl("<link rel=\"stylesheet\" type=\"text/css\" href=\"" + ResolveUrl("~/Styles/AmlSurveillanceAlt.css") + "\" />"));
                btnSaveAMLSurveillance.Visible = false;
            }
            else hdnMiseSousSurveillanceWidth.Value = "170";
            Page.Header.Controls.Add(new System.Web.UI.LiteralControl("<link rel=\"stylesheet\" type=\"text/css\" href=\"" + ResolveUrl("~/Styles/toggles-soft.css") + "\" />"));
        }
    }

    public void InitScripts()
    {
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "InitAMLToggles();InitAMLSurveillance();", true);
    }

    protected void CheckRights()
    {
        if (ActionAllowed)
        {
            btnSaveAMLSurveillance.Visible = true;
            //btnMiseSousSurveillance.Visible = true;
            panelSousSurveillanceReadonly.Visible = false;
        }
        else
        {
            btnSaveAMLSurveillance.Visible = false;
            //btnMiseSousSurveillance.Visible = false;
            panelSousSurveillanceReadonly.Visible = true;
        }
    }

    public void BindAmlSurveillance(int iRefCustomer, bool bSousSurveillance, bool bLockTransfer, bool bRequestTransferValidation, string sRequestTransferValidationAmount)
    {
        _iRefCustomer = iRefCustomer;
        _bSousSurveillance = bSousSurveillance;
        _bLockTransfer = bLockTransfer;
        _bRequestTransferValidation = bRequestTransferValidation;
        _sRequestTransferValidationAmount = sRequestTransferValidationAmount;

        //BindSurveillance();

        BindAmlAlerts();
        BindAmlTransferOutSituation();
    }

    //protected void btnMiseSousSurveillance_Click(object sender, EventArgs e)
    //{
    //    if (AML.setSurveillance(ckbMiseSousSurveillance.Checked, authentication.GetCurrent(false).sToken, _iRefCustomer))
    //    {
    //        _bSousSurveillance = ckbMiseSousSurveillance.Checked;
    //        BindSurveillance();

    //        if (_bSousSurveillance && txtAlerteVirementEntrantMontant.Text.Length == 0)
    //        {
    //            txtAlerteVirementEntrantMontant.Text = "300";
    //            ckbAlerteVirementEntrantService.Checked = true;
    //        }
    //    }
    //    else
    //    {
    //        if (ckbMiseSousSurveillance.Checked == true)
    //            ckbMiseSousSurveillance.Checked = false;
    //        else ckbMiseSousSurveillance.Checked = true;

    //        UpdateMessage("Une erreur est survenue.<br/>Veuillez contacter un technicien.");
    //    }
    //}
    //protected void BindSurveillance()
    //{
    //    if (_bSousSurveillance)
    //    {
    //        ckbMiseSousSurveillance.Checked = true;
    //        panelSousSurveillanceLock.Visible = false;
    //        //imgSurveillanceIcon.Visible = true;
    //    }
    //    else
    //    {
    //        ckbMiseSousSurveillance.Checked = false;
    //        panelSousSurveillanceLock.Visible = true;
    //        //imgSurveillanceIcon.Visible = false;
    //    }

    //    //upSurveillanceIcon.Update();
    //}
    protected void BindAmlAlerts()
    {
        bool bWatch = false;
        string sPeriod = "";
        string sWatchStart = "";
        string sWatchEnd = "";
        DataTable dt = AML.GetCustomerAMLAlerts(_iRefCustomer.ToString(), authentication.GetCurrent(false).sToken, out bWatch, out sPeriod, out sWatchStart, out sWatchEnd);

        ckbMiseSousSurveillance.Checked = bWatch;
        if (!String.IsNullOrWhiteSpace(sPeriod))
        {
            ListItem item = rblAMLSurveillancePeriod.Items.FindByValue(sPeriod);
            if(item != null)
                item.Selected = true;
        }
        if (!String.IsNullOrWhiteSpace(sWatchStart) && !String.IsNullOrWhiteSpace(sWatchEnd))
            lblPeriod.Text = "du " + sWatchStart + " au " + sWatchEnd;

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            switch (dt.Rows[i]["TAG"].ToString())
            {
                case "TransferIN":
                    txtAlerteVirementEntrantMontant.Text = (dt.Rows[i]["MinAmount"].ToString() != "0") ? dt.Rows[i]["MinAmount"].ToString() : "";
                    ckbAlerteVirementEntrantAgent.Checked = (dt.Rows[i]["Agent"].ToString() == "1") ? true : false;
                    ckbAlerteVirementEntrantService.Checked = (dt.Rows[i]["Service"].ToString() == "1") ? true : false;
                    break;
                case "TransferOUT":
                    txtAlerteVirementSortantMontant.Text = (dt.Rows[i]["MinAmount"].ToString() != "0") ? dt.Rows[i]["MinAmount"].ToString() : "";
                    ckbAlerteVirementSortantAgent.Checked = (dt.Rows[i]["Agent"].ToString() == "1") ? true : false;
                    ckbAlerteVirementSortantService.Checked = (dt.Rows[i]["Service"].ToString() == "1") ? true : false;
                    break;
                case "Card":
                    txtAlerteOpertationsCarteMontant.Text = (dt.Rows[i]["MinAmount"].ToString() != "0") ? dt.Rows[i]["MinAmount"].ToString() : "";
                    ckbAlerteOperationsCarteAgent.Checked = (dt.Rows[i]["Agent"].ToString() == "1") ? true : false;
                    ckbAlerteOperationsCarteService.Checked = (dt.Rows[i]["Service"].ToString() == "1") ? true : false;
                    break;
                case "All":
                    txtAlerteTouteOperationMontant.Text = (dt.Rows[i]["MinAmount"].ToString() != "0") ? dt.Rows[i]["MinAmount"].ToString() : "";
                    ckbAlerteTouteOperationAgent.Checked = (dt.Rows[i]["Agent"].ToString() == "1") ? true : false;
                    ckbAlerteTouteOperationService.Checked = (dt.Rows[i]["Service"].ToString() == "1") ? true : false;
                    break;
            }
        }
    }
    protected void BindAmlTransferOutSituation()
    {
        ckbBlocageTousVirements.Checked = _bLockTransfer;
        ckbLimiteValidation.Checked = _bRequestTransferValidation;
        txtLimiteValidation.Text = (_sRequestTransferValidationAmount != "0") ? _sRequestTransferValidationAmount : "";
    }
    
    protected void btnSaveAMLSurveillance_Click(object sender, EventArgs e)
    {
        string sErrorMessage = "";
        
        if (SaveAMLAlerts(ref sErrorMessage) && SaveTransferOutSituation(ref sErrorMessage))
        {
            UpdateMessage("Paramètres de surveillance enregistrés");
        }
        else
        {
            UpdateMessage(sErrorMessage);
        }
    }
    public bool SaveAMLAlerts(ref string sErrorMessage)
    {
        bool bOk = false;
        int i;

        if ((ckbAlerteOperationsCarteAgent.Checked || ckbAlerteOperationsCarteService.Checked) && (txtAlerteOpertationsCarteMontant.Text.Length == 0 || !int.TryParse(txtAlerteOpertationsCarteMontant.Text, out i))
            || (ckbAlerteTouteOperationAgent.Checked || ckbAlerteTouteOperationService.Checked) && (txtAlerteTouteOperationMontant.Text.Length == 0 || !int.TryParse(txtAlerteTouteOperationMontant.Text, out i))
            || (ckbAlerteVirementEntrantAgent.Checked || ckbAlerteVirementEntrantService.Checked) && (txtAlerteVirementEntrantMontant.Text.Length == 0 || !int.TryParse(txtAlerteVirementEntrantMontant.Text, out i))
            || (ckbAlerteVirementSortantAgent.Checked || ckbAlerteVirementSortantService.Checked) && (txtAlerteVirementSortantMontant.Text.Length == 0 || !int.TryParse(txtAlerteVirementSortantMontant.Text, out i)))
        {
            sErrorMessage += "Veuillez renseigner tous les montants minimum des alertes actives.<br/>";
        }
        else
        {
            XElement xmlAlerts, xmlTransfer;
            GetDataToXml(true, out xmlAlerts, out xmlTransfer);
            bOk = AML.SetCustomerAMLAlerts(xmlAlerts.ToString());

            if (!bOk)
                sErrorMessage += "Une erreur est survenue lors de la sauvegarde des alertes.<br/>";
        }

        return bOk;
    }
    public bool SaveTransferOutSituation(ref string sErrorMessage)
    {
        bool bOk = false;

        string sAction = "";

        if (ckbBlocageTousVirements.Checked)
            sAction = "LOCK";
        else sAction = "UNLOCK";

        bOk = AML.SetCustomerTransferSituation(_iRefCustomer.ToString(), authentication.GetCurrent(false).sToken, sAction, "");

        if (!bOk)
            sErrorMessage += "Une erreur est survenue lors de la sauvegarde du réglage de blocage.<br/>";
        else
        {
            int i;

            if (ckbLimiteValidation.Checked && (txtLimiteValidation.Text.Length == 0 || !int.TryParse(txtLimiteValidation.Text, out i)))
            {
                sErrorMessage += "Veuillez renseigner le montant à partir duquel la validation est nécessaire.<br/>";
                bOk = false;
            }
            else
            {
                string sLimit = "";

                if (!ckbLimiteValidation.Checked)
                    sAction = "NORESTRICTION";
                else
                {
                    sAction = "BOVALIDATION";
                    sLimit = txtLimiteValidation.Text;
                }

                bOk = AML.SetCustomerTransferSituation(_iRefCustomer.ToString(), authentication.GetCurrent(false).sToken, sAction, sLimit);

                if (!bOk)
                    sErrorMessage += "Une erreur est survenue lors de la sauvegarde du réglage de validation.<br/>";
            }
        }

        return bOk;
    }

    protected void UpdateMessage(string sMessage)
    {
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "ShowAmlSurvMessage(\"" + sMessage + "\");", true);
    }

    public bool IsFormValid()
    {
        bool bValid = true;
        int i;

        if ((ckbAlerteOperationsCarteAgent.Checked || ckbAlerteOperationsCarteService.Checked) && (txtAlerteOpertationsCarteMontant.Text.Length == 0 || !int.TryParse(txtAlerteOpertationsCarteMontant.Text, out i))
            || (ckbAlerteTouteOperationAgent.Checked || ckbAlerteTouteOperationService.Checked) && (txtAlerteTouteOperationMontant.Text.Length == 0 || !int.TryParse(txtAlerteTouteOperationMontant.Text, out i))
            || (ckbAlerteVirementEntrantAgent.Checked || ckbAlerteVirementEntrantService.Checked) && (txtAlerteVirementEntrantMontant.Text.Length == 0 || !int.TryParse(txtAlerteVirementEntrantMontant.Text, out i))
            || (ckbAlerteVirementSortantAgent.Checked || ckbAlerteVirementSortantService.Checked) && (txtAlerteVirementSortantMontant.Text.Length == 0 || !int.TryParse(txtAlerteVirementSortantMontant.Text, out i)))
            bValid = false;
        else if (ckbLimiteValidation.Checked && (txtLimiteValidation.Text.Length == 0 || !int.TryParse(txtLimiteValidation.Text, out i)))
            bValid = false;

        return bValid;
    }
    public void GetDataToXml(bool bXmlComplete, out XElement xmlAlerts, out XElement xmlTransfer)
    {
        xmlAlerts = new XElement("AML_ALERTS",
                            new XAttribute("Watch", (ckbMiseSousSurveillance.Checked) ? "1" : "0"),
                            new XAttribute("Period", rblAMLSurveillancePeriod.SelectedValue),
                            new XElement("ALERT",
                                new XAttribute("TAG", "TransferIN"),
                                new XAttribute("MinAmount", txtAlerteVirementEntrantMontant.Text),
                                new XAttribute("Agent", (ckbAlerteVirementEntrantAgent.Checked) ? "1" : "0"),
                                new XAttribute("ServiceConformite", (ckbAlerteVirementEntrantService.Checked) ? "1" : "0")),
                            new XElement("ALERT",
                                new XAttribute("TAG", "TransferOUT"),
                                new XAttribute("MinAmount", txtAlerteVirementSortantMontant.Text),
                                new XAttribute("Agent", (ckbAlerteVirementSortantAgent.Checked) ? "1" : "0"),
                                new XAttribute("ServiceConformite", (ckbAlerteVirementSortantService.Checked) ? "1" : "0")),
                            new XElement("ALERT",
                                new XAttribute("TAG", "Card"),
                                new XAttribute("MinAmount", txtAlerteOpertationsCarteMontant.Text),
                                new XAttribute("Agent", (ckbAlerteOperationsCarteAgent.Checked) ? "1" : "0"),
                                new XAttribute("ServiceConformite", (ckbAlerteOperationsCarteService.Checked) ? "1" : "0")),
                            new XElement("ALERT",
                                new XAttribute("TAG", "All"),
                                new XAttribute("MinAmount", txtAlerteTouteOperationMontant.Text),
                                new XAttribute("Agent", (ckbAlerteTouteOperationAgent.Checked) ? "1" : "0"),
                                new XAttribute("ServiceConformite", (ckbAlerteTouteOperationService.Checked) ? "1" : "0")));
        if (bXmlComplete)
        {
            xmlAlerts.Add(new XAttribute("CashierToken", authentication.GetCurrent(false).sToken),
                        new XAttribute("RefCustomer", _iRefCustomer.ToString()));
            xmlAlerts = new XElement("ALL_XML_IN", xmlAlerts);
        }

        string sAction = "";
        if (ckbBlocageTousVirements.Checked)
            sAction = "LOCK";
        else sAction = "UNLOCK";

        string sLimit = "";

        if (!ckbLimiteValidation.Checked)
            sAction = "NORESTRICTION";
        else
        {
            sAction = "BOVALIDATION";
            sLimit = txtLimiteValidation.Text;
        }

        xmlTransfer = new XElement("Transfer",
                            new XAttribute("Action", sAction),
                            new XAttribute("LimitAmount", sLimit));
    }
}