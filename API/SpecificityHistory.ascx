﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SpecificityHistory.ascx.cs" Inherits="API_SpecificityHistory" %>

<asp:Panel ID="panel_ClientSpecificities" runat="server">
    <asp:Panel ID="panelClientSpecificityList" runat="server">
        <asp:Repeater ID="rptAML_ClientSpecificities" runat="server">
            <HeaderTemplate>
                <table style="width:100%;border-collapse:collapse;" class="tableHistory">
                    <tr>
                        <th style="text-align:left;padding-left:5px; min-width:250px;max-width:300px;">Spécificité</th>
                        <th style="text-align:left;padding-left:10px;">Date</th>
                        <th style="text-align:left;padding-left:10px;">Par</th>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                    <tr class='<%# GetSpecificityStatus(Eval("IsDeleted").ToString()) %>'>
                        <td style="padding-left:5px;">
                            <%#Eval("SpecificityLabel") %>
                        </td>
                        <td style="padding-left:10px; text-align:left">
                            <%# tools.getFormattedDate(Eval("SpecificityDate").ToString()) %>
                        </td>
                        <td style="padding-left:10px">
                            <%#Eval("ByUser") %>
                        </td>
                    </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </asp:Panel>
    <asp:Panel ID="panel_ClientSpecificitiesNoResult" runat="server" Visible="false" style="font-weight:bold">Aucune spécificité</asp:Panel>
</asp:Panel>