﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using XMLMethodLibrary;

public partial class API_BlockedTransfer : System.Web.UI.UserControl
{
    public string JsToCallAfterAmlClientNote
    {
        get { return _JsToCallAfterAmlClientNote; }
        set { _JsToCallAfterAmlClientNote = value; }
    }
    public int iPage {
        get { return _iPage; }
        set { _iPage = value; }
    }
    public int iPageSize {
        get { return _iPageSize; }
        set { _iPageSize = value; }
    }
    public string sStatus {
        get { return _sStatus; }
        set { _sStatus = value; }
    }
    public bool isFilterVisible {
        get { return _isFilterVisible; }
        set { _isFilterVisible = value; }
    }
    public bool isClientDetailsLink {
        get { return _isClientDetailsLink; }
        set { _isClientDetailsLink = value; }
    }
    public int RefCustomer
    {
        get { return _iRefCustomer; }
        set { _iRefCustomer = value; }
    }

    private bool ActionRight
    {
        get { try { return bool.Parse(ViewState["BT_ActionRight"].ToString()); } catch (Exception ex) { return false; } }
        set { ViewState["BT_ActionRight"] = value; }
    }
    private bool ViewRight
    {
        get { try { return bool.Parse(ViewState["BT_ViewRight"].ToString()); } catch (Exception ex) { return false; } }
        set { ViewState["BT_ViewRight"] = value; }
    }
    private int _iRefCustomer
    {
        get { return int.Parse(ViewState["BT_RefCustomer"].ToString()); }
        set { ViewState["BT_RefCustomer"] = value; }
    }
    private int _iPage
    {
        get { try { return int.Parse(ViewState["BT_Page"].ToString()); } catch (Exception ex) { return 1; } }
        set { ViewState["BT_Page"] = value; }
    }
    private int _iPageSize
    {
        get { try { return int.Parse(ViewState["BT_PageSize"].ToString()); } catch (Exception ex) { return 10; } }
        set { ViewState["BT_PageSize"] = value; }
    }
    private string _sStatus
    {
        get { try { return ViewState["BT_Status"].ToString(); } catch (Exception ex) { return ""; } }
        set { ViewState["BT_Status"] = value; }
    }
    private bool _isFilterVisible
    {
        get { try { return bool.Parse(ViewState["BT_isFilterVisible"].ToString()); } catch (Exception ex) { return false; } }
        set { ViewState["BT_isFilterVisible"] = value; }
    }
    private string _JsToCallAfterAmlClientNote 
    {
        get { try { return ViewState["BT_JsToCallAfterAmlClientNote"].ToString(); } catch (Exception ex) { return ""; } } 
        set { ViewState["BT_JsToCallAfterAmlClientNote"] = value; }
    }
    private bool _isClientDetailsLink
    {
        get { try { return bool.Parse(ViewState["BT_isClientDetailsLink"].ToString()); } catch (Exception ex) { return false; } }
        set { ViewState["BT_isClientDetailsLink"] = value; }
    }

    public void Init()
    {
        panelGridFilters.Visible = true;
        authentication auth = authentication.GetCurrent();
        if (auth != null)
        {
            getParam();

            if (!_isFilterVisible)
            {
                panelGridFilters.Visible = false;
                panelPageSize.Visible = false;
            }
         
            bindTransferGrid(false);
        }

        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "init();", true);
    }

    protected void checkRights()
    {
        panelBlockedTransfer.Visible = false;
        gvBlockedTransferList.Columns[gvBlockedTransferList.Columns.Count - 1].Visible = false;
        panelBlockedTransferUnauthorized.Visible = true;

        authentication auth = authentication.GetCurrent();
        if (auth != null)
        {
            bool bView, bAction;
            AML.HasPendingTransferRights(auth.sToken, out bView, out bAction);
            ViewRight = bView;
            ActionRight = bAction;
            if (bView)
            {
                panelBlockedTransfer.Visible = true;
                panelBlockedTransferUnauthorized.Visible = false;
            }
            if (bAction)
                gvBlockedTransferList.Columns[gvBlockedTransferList.Columns.Count - 1].Visible = true;

            hdnCurrentRefAgent.Value = auth.sRef;
        }
    }
    protected void bindTransferGrid(bool bOnChange)
    {
        checkRights();
        authentication auth = authentication.GetCurrent();
        string sXmlOut = "";
        string sPage = "1";
        string sPageSize = "10";

        try
        {
            string sSelectedPage = (ddlPage.SelectedValue.ToString().Trim().Length > 0) ? ddlPage.SelectedValue : "1";
            if (!bOnChange) { sPage = (_iPage > 0) ? _iPage.ToString() : sSelectedPage; }
            else { sPage = sSelectedPage; }

            string sSelectedPageSize = ddlNbResult.SelectedValue;
            sPageSize = (_iPageSize > 0) ? _iPageSize.ToString() : sSelectedPageSize;
            if (!panelPageSize.Visible && ddlNbResult.Items.FindByValue(sPageSize) == null)
            {
                ddlNbResult.Items.Add(new ListItem(sPageSize, sPageSize));
                ddlNbResult.Items.FindByValue(ddlNbResult.SelectedValue).Selected = false;
                ddlNbResult.Items.FindByValue(sPageSize).Selected = true;
            }

            XElement xPendingTransfer = new XElement("PendingTransfer");

            if (_iRefCustomer > 0)
                xPendingTransfer.Add(new XAttribute("RefCustomer", _iRefCustomer.ToString()));
            xPendingTransfer.Add(new XAttribute("TransferStatus", ddlTransferStatus.SelectedValue));
            xPendingTransfer.Add(new XAttribute("CashierToken", auth.sToken));
            xPendingTransfer.Add(new XAttribute("PageSize", sPageSize));
            xPendingTransfer.Add(new XAttribute("PageIndex", sPage));
            xPendingTransfer.Add(new XAttribute("OrderBy", "RefTransferPending"));
            xPendingTransfer.Add(new XAttribute("OrderASC", "DESC"));
            xPendingTransfer.Add(new XAttribute("Culture", ((Session["CultureID"] != null) ? Session["CultureID"].ToString() : "fr-FR")));

            string sXmlIn = new XElement("ALL_XML_IN", xPendingTransfer).ToString();
            gvBlockedTransferList.DataSource = AML.getPendingTransferList(sXmlIn, out sXmlOut);
            List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/RESULT", "RC");
            List<string> listErrorLabel = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/RESULT", "ErrorLabel");
            List<string> listRowCount = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/PendingTransfer", "NbResult");
            if (listRC.Count == 0 && listRowCount != null && listRowCount.Count > 0)
            {
                panelGrid.Visible = true;
                panelError.Visible = false;
                lblNbOfResults.Text = listRowCount[0].ToString();
                PaginationManagement(lblNbOfResults.Text, sXmlOut);
            }
            else
            {
                if (listErrorLabel != null && listErrorLabel.Count > 0)
                {
                    panelGrid.Visible = false;
                    panelError.Visible = true;
                    lblError.Text = listErrorLabel[0].ToString().ToUpper();
                }
            }
        
            gvBlockedTransferList.DataBind();
            if (gvBlockedTransferList.Rows.Count > 0)
            {
                panelGrid.Visible = true;
                divGridSettings.Visible = true;

            }
        }
        catch (Exception ex)
        {
        }
        upSearch.Update();
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "init();", true);
    }
    protected void gvBlockedTransferList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HiddenField hdnRefCustomer = (HiddenField)e.Row.FindControl("hdnRefCustomer");
            if (hdnRefCustomer != null)
            {
                e.Row.Attributes.Add("onclick", "BlockedTransfer_ShowClientDetails('" + hdnRefCustomer.Value + "')");
                e.Row.Attributes.Add("onmouseover", "this.style.cursor = 'pointer';this.style.backgroundColor = '#ff9966'");
                e.Row.Attributes.Add("onmouseout", "this.style.cursor = 'auto';this.style.backgroundColor = '#fff'");
            }
        }
    }
    protected DataTable getPages(int nbPages)
    {
        DataTable dt = new DataTable();

        dt.Columns.Add("L");
        dt.Columns.Add("V");

        for (int i = 1; i <= nbPages; i++)
        {
            dt.Rows.Add(i.ToString(), i.ToString());
        }

        return dt;
    }
    protected int CalculateTotalPages(double totalRows, int pageSize)
    {
        int totalPages = (int)Math.Ceiling(totalRows / pageSize);

        return totalPages;
    }
    protected void PaginationManagement(string sRowCount, string sXml)
    {
        if (int.Parse(sRowCount) > int.Parse(ddlNbResult.SelectedValue.ToString()))
        {

            int selectedPage = ddlPage.SelectedIndex;

            DataTable dtPages = getPages(CalculateTotalPages(int.Parse(sRowCount), int.Parse(ddlNbResult.SelectedValue)));
            ddlPage.DataSource = dtPages;
            ddlPage.DataBind();

            string sPageIndex = ""; int iPageIndex = 0;
            sPageIndex = tools.GetValueFromXml(sXml, "ALL_XML_IN/Registration", "PageIndex");
            if (sPageIndex.Trim().Length > 0 && int.TryParse(sPageIndex, out iPageIndex))
            {
                if (ddlPage.Items.Count > 0 && ddlPage.Items.FindByValue(iPageIndex.ToString()) != null)
                    ddlPage.Items.FindByValue(iPageIndex.ToString()).Selected = true;
            }
            else if (Session["RegistrationPageIndex"] != null)
            {
                if (ddlPage.Items.Count > 0)
                    ddlPage.Items.FindByValue(Session["RegistrationPageIndex"].ToString()).Selected = true;
            }
            else if (ddlPage.Items.Count > selectedPage)
                ddlPage.SelectedIndex = selectedPage;

            if (int.Parse(ddlPage.SelectedValue) == ddlPage.Items.Count)
                btnNextPage.Enabled = false;
            else
                btnNextPage.Enabled = true;

            if (int.Parse(ddlPage.SelectedValue) == 1)
                btnPreviousPage.Enabled = false;
            else
                btnPreviousPage.Enabled = true;

            if (ddlPage.Items.Count > 1)
                divPagination.Visible = true;
            else
                divPagination.Visible = false;

            lblTotalPage.Text = " / " + ddlPage.Items.Count.ToString();


        }
        else
        {
            divPagination.Visible = false;
        }
    }
    protected void nextPage(object sender, EventArgs e)
    {
        int iNbPage;

        int.TryParse(ddlPage.SelectedValue, out iNbPage);
        iNbPage++;
        if (ddlPage.Items.FindByValue(iNbPage.ToString()) != null)
            ddlPage.SelectedValue = iNbPage.ToString();

        bindTransferGrid(true);
    }
    protected void previousPage(object sender, EventArgs e)
    {
        int iNbPage;

        int.TryParse(ddlPage.SelectedValue, out iNbPage);
        iNbPage--;
        if (ddlPage.Items.FindByValue(iNbPage.ToString()) != null)
            ddlPage.SelectedValue = iNbPage.ToString();

        bindTransferGrid(true);
    }
    protected void onChangePageNumber(object sender, EventArgs e)
    {
        bindTransferGrid(true);
    }
    protected void onChangeNbResult(object sender, EventArgs e)
    {
        _iPage = 1;
        bindTransferGrid(false);
    }
    protected void onChangeAlertType(object sender, EventArgs e)
    {
        bindTransferGrid(true);
    }
    //protected void saveParam()
    //{
    //    DataTable dtParam = new DataTable();
    //    dtParam.Columns.Add("Name");
    //    dtParam.Columns.Add("Value");
    //    dtParam.Rows.Add(new object[] { "transferStatus", ddlTransferStatus.SelectedValue });
    //    dtParam.Rows.Add(new object[] { "nbResult", ddlNbResult.SelectedValue });
    //    dtParam.Rows.Add(new object[] { "nbPages", (lblTotalPage.Text.Trim().Length > 0) ? lblTotalPage.Text.Replace("/", "").Trim() : "1" });
    //    dtParam.Rows.Add(new object[] { "pageIndex", ddlPage.SelectedValue });

    //    Session["SearchTransferParam"] = dtParam;
    //}
    protected void getParam()
    {
        if (Request.Params["Reserved"] != null && Request.Params["Reserved"].ToString() == "Y")
        {
            //if (ddlAssignmentStatus.Items.FindByValue("RESERVED") != null)
            //ddlAssignmentStatus.SelectedValue = "RESERVED";
            if (ddlTransferStatus.Items.FindByValue("RESERVED") != null)
            {
                ddlTransferStatus.ClearSelection();
                ddlTransferStatus.SelectedValue = "RESERVED";
            }
        }
        else if (Request.Params["NotChecked"] != null && Request.Params["NotChecked"].ToString() == "Y")
        {
            //if (ddlAssignmentStatus.Items.FindByValue("RESERVED") != null)
            //ddlAssignmentStatus.SelectedValue = "RESERVED";
            if (ddlTransferStatus.Items.FindByValue("PENDINGNOTRESERVED") != null)
            {
                ddlTransferStatus.ClearSelection();
                ddlTransferStatus.SelectedValue = "PENDINGNOTRESERVED";
            }
        }
        else if (Session["SearchTransferParam"] != null)
        {
            try
            {
                DataTable dtParam = (DataTable)Session["SearchTransferParam"];
                string sParamName = "", sParamValue = "";

                for (int i = 0; i < dtParam.Rows.Count; i++)
                {

                    sParamValue = dtParam.Rows[i]["Value"].ToString().Trim();
                    sParamName = dtParam.Rows[i]["Name"].ToString().Trim();

                    if (sParamName.Length > 0 && sParamValue.Length > 0)
                    {
                        switch (sParamName)
                        {
                            case "transferStatus":
                                if (ddlTransferStatus.Items.FindByValue(sParamValue) != null)
                                    ddlTransferStatus.SelectedValue = sParamValue;
                                break;
                            case "nbResult":
                                if (ddlNbResult.Items.FindByValue(sParamValue) != null)
                                    ddlNbResult.SelectedValue = sParamValue;
                                break;
                            case "nbPages":
                                int iNbPages = 1;
                                int.TryParse(sParamValue, out iNbPages);
                                ddlPage.DataSource = getPages(iNbPages);
                                ddlPage.DataBind();
                                break;
                            case "pageIndex":
                                if (ddlPage.Items.FindByValue(sParamValue) != null)
                                    ddlPage.SelectedValue = sParamValue;
                                break;
                        }
                    }
                }

            }
            catch (Exception ex)
            {

            }
        }
        else
        {
            ddlPage.DataSource = getPages(1);
            ddlPage.DataBind();
            if (ddlTransferStatus.Items.Count > 0)
                ddlTransferStatus.Items[0].Selected = true;
        }
    }
    protected void btnPendingTransferAction_Click(object sender, EventArgs e)
    {
        if (hdnRefCustomerSelected.Value.Length > 0 && hdnRefPendingTransferSelected.Value.Length > 0 && hdnPendingTransferActionSelected.Value.Length > 0)
        {
            string sErrorMessage = "";
            BT_txtClientNote.Text = "";
            if (AML.SetPendingTransferStatus(hdnRefPendingTransferSelected.Value, hdnRefCustomerSelected.Value, authentication.GetCurrent().sToken, hdnPendingTransferActionSelected.Value, out sErrorMessage))
            {
                BT_txtClientNote.Text = getOperationDetailsForAmlNote(hdnRefPendingTransferSelected.Value);
                BT_lblNbCharClientNote.Text = BT_txtClientNote.Text.Length.ToString();
                BT_txtClientNote.Attributes.Add("onKeyUp", "javascript:checkMaxLength(this, null, \"" + BT_lblNbCharClientNote.ClientID + "\", \"" + BT_lblNbMaxClientNote.ClientID + "\")");
                BT_txtClientNote.Attributes.Add("onChange", "javascript:checkMaxLength(this, null, \"" + BT_lblNbCharClientNote.ClientID + "\", \"" + BT_lblNbMaxClientNote.ClientID + "\")");
                btnPendingTransferAddAmlNote.OnClientClick = "return checkClientNote(getInputValue('" + BT_txtClientNote.ClientID + "'));";
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "BlockedTransfer_AddAmlNote();", true);
            }
            else
                UpdateMessage("Une erreur est survenue." + "<br/>Raison : " + sErrorMessage);
        }
        else
            UpdateMessage("Une erreur est survenue.");
        bindTransferGrid(true);

        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "hideLoading();", true);
    }
    protected string GetStatusLabel(string isRefused, string sRefusedDate, string isCancelled, string sCancelledDate, string isTerminated, string sTerminatedDate, string isReserved, string sReservedDate)
    {
        string sLabel = "";

        if (isRefused == "True")
            sLabel = "Refusé" + ((sRefusedDate.Trim().Length > 0)? " le " + tools.getFormattedDate(sRefusedDate) : "").ToString();
        else if (isCancelled == "True")
            sLabel = "Annulé" + ((sCancelledDate.Trim().Length > 0) ? " le " + tools.getFormattedDate(sCancelledDate) : "").ToString();
        else if (isTerminated == "True")
            sLabel = "Approuvé" + ((sTerminatedDate.Trim().Length > 0) ? " le " + tools.getFormattedDate(sTerminatedDate) : "").ToString();
        else if (isReserved == "True")
            sLabel = "Assigné" + ((sReservedDate.Trim().Length > 0) ? " le " + tools.getFormattedDate(sReservedDate) : "").ToString();
        else sLabel = "En attente";

        return sLabel;
    }

    protected string GetStatusLabel(string isRefused, string isCancelled, string isTerminated, string isReserved)
    {
        string sLabel = "";

        if (isRefused == "True")
            sLabel = "Refusé";
        else if (isCancelled == "True")
            sLabel = "Annulé";
        else if (isTerminated == "True")
            sLabel = "Approuvé";
        else if (isReserved == "True")
            sLabel = "Assigné";
        else sLabel = "En attente";

        return sLabel;
    }

    protected void ddlTransferStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        bindTransferGrid(true);
    }
    protected void UpdateMessage(string sMessage)
    {
        hdnMessage.Value = sMessage;
        //upMessage.Update();
        ScriptManager.RegisterStartupScript(upSearch, upSearch.GetType(), "blockedTransfer_UpdateMessageKEY", "BlockedTransfer_CheckMessages();", true);
        upSearch.Update();
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "blockedTransfer_UpdateMessageKEY", "CheckMessages();", true);
    }
    protected void ddlReserveStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        bindTransferGrid(true);
    }

    protected void btnPendingTransferReservation_Click(object sender, EventArgs e)
    {
        if (hdnRefCustomerSelected.Value.Trim().Length > 0 && hdnRefPendingTransferSelected.Value.Length > 0)
        {
            checkRights();
            string sErrorMessage = "";
            if (ActionRight)
            {
                if (AML.SetPendingTransferStatus(hdnRefPendingTransferSelected.Value.ToString(), hdnRefCustomerSelected.Value.ToString(), authentication.GetCurrent().sToken, "RESERVED", out sErrorMessage))
                    Response.Redirect("ClientDetails.aspx?ref=" + hdnRefCustomerSelected.Value + "&view=aml", true);
                else
                    UpdateMessage("Une erreur est survenue." + "<br/>Raison : " + sErrorMessage);
            }
            else
                Response.Redirect("ClientDetails.aspx?ref=" + hdnRefCustomerSelected.Value + "&view=aml", true);
        }
        else
            UpdateMessage("Une erreur est survenue.");

        bindTransferGrid(true);
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "getNbPendingTranferReservedCount();getNbPendingTransferCount();", true);
    }

    protected string GetAccountNumberLabel(string sAccountNumber, string sRefTransferPending, string sRefCustomer, string isRefused, string sRefusedDate, string isCancelled, string sCancelledDate, string isTerminated, string sTerminatedDate, string isReserved, string sReservedDate)
    {
        string sHtmlAccountNumber = "";
        if(_isClientDetailsLink) 
            sHtmlAccountNumber += "<a onclick=\"BlockedTransfer_ClientDetails(" + sRefTransferPending + ", " + sRefCustomer + ", '" + GetStatusLabel(isRefused, sRefusedDate, isCancelled, sCancelledDate, isTerminated, sTerminatedDate, isReserved, sReservedDate).ToString() + "', "+ActionRight.ToString().ToLower()+");\" class=\"orangeLink\">";
        sHtmlAccountNumber += "<span class=\"tooltip\">"+sAccountNumber+"</span>";
        if (_isClientDetailsLink) 
            sHtmlAccountNumber += "</a>";
        return sHtmlAccountNumber;
    }

    protected string GetReservedStatusImage(string sIsReserved, string sReservedDate, string sRefAgentName, string sAgentName, string sRefTransferPending, string sRefCustomer)
    {
        string sImage = "";

        if (sIsReserved == "True")
            sImage = "<img src=\"../Styles/Img/orange-eye.png\" style=\"width:24px;\" class=\"tooltip\" title=\"Assigné à " + sAgentName + "\" onclick=\"BlockedTransfer_Unassign(" + sRefTransferPending + "," + sRefCustomer + ", '" + sRefAgentName + "', '" + sAgentName + "');\" />";

        return sImage;
    }
    protected void btnPendingTransferUnreservation_Click(object sender, EventArgs e)
    {
        if (hdnRefCustomerSelected.Value.Trim().Length > 0 && hdnRefPendingTransferSelected.Value.Length > 0)
        {
            string sErrorMessage = "";
            if (AML.SetPendingTransferStatus(hdnRefPendingTransferSelected.Value.ToString(), hdnRefCustomerSelected.Value.ToString(), authentication.GetCurrent().sToken, "UNRESERVED", out sErrorMessage))
                UpdateMessage("Cette transaction ne vous est plus affectée.");
            else
                UpdateMessage("Une erreur est survenue." + "<br/>Raison : " + sErrorMessage);
        }
        else
            UpdateMessage("Une erreur est survenue.");

        bindTransferGrid(true);
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "getNbPendingTranferReservedCount();getNbPendingTransferCount();", true);
    }
    protected void btnPendingTransferViewClientDetails_Click(object sender, EventArgs e)
    {
        Response.Redirect("ClientDetails.aspx?ref=" + hdnRefCustomerSelected.Value + "&view=aml", true);
    }
    protected void btnPendingTransferAddAmlNote_Click(object sender, EventArgs e)
    {
        authentication auth = authentication.GetCurrent();
        int iRefCustomerNote = -1;
        int.TryParse(hdnRefCustomerSelected.Value, out iRefCustomerNote);
        AML.addClientNote(auth.sToken, iRefCustomerNote, BT_txtClientNote.Text.Trim());
        bindTransferGrid(true);
        if (_JsToCallAfterAmlClientNote != null && _JsToCallAfterAmlClientNote.Trim().Length > 0)
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), _JsToCallAfterAmlClientNote, true);
    }

    protected string getOperationDetailsForAmlNote(string RefPendingTransfer)
    {
        string sAmlNote = "";

        authentication auth = authentication.GetCurrent();
        DataTable dt = new DataTable();
        string sXmlOut = "";
        XElement xPendingTransfer = new XElement("PendingTransfer");

        if (_iRefCustomer > 0)
            xPendingTransfer.Add(new XAttribute("RefCustomer", _iRefCustomer.ToString()));
        xPendingTransfer.Add(new XAttribute("CashierToken", auth.sToken));
        xPendingTransfer.Add(new XAttribute("RefTransferPending", RefPendingTransfer));
        xPendingTransfer.Add(new XAttribute("PageSize", "1"));
        xPendingTransfer.Add(new XAttribute("PageIndex", "1"));
        xPendingTransfer.Add(new XAttribute("OrderBy", "RefTransferPending"));
        xPendingTransfer.Add(new XAttribute("OrderASC", "DESC"));
        xPendingTransfer.Add(new XAttribute("Culture", ((Session["CultureID"] != null) ? Session["CultureID"].ToString() : "fr-FR")));

        string sXmlIn = new XElement("ALL_XML_IN", xPendingTransfer).ToString();
        dt = AML.getPendingTransferList(sXmlIn, out sXmlOut);

        if (dt.Rows.Count > 0)
        {
            string sBankName = tools.GetBankNameFromBic(dt.Rows[0]["BIC"].ToString());
            if (sBankName.Trim().Length == 0)
                sBankName = "BANQUE INCONNUE (" + dt.Rows[0]["BIC"].ToString() + ")";
            sAmlNote = "Virement sortant vers " + dt.Rows[0]["BeneficiaryName"].ToString().ToUpper().Trim() + " (" + dt.Rows[0]["IBAN"].ToString() + " - " + sBankName  + ")";
            sAmlNote += " de " + tools.getFormattedAmount(dt.Rows[0]["Amount"].ToString(), false).Replace("+", "").Replace(" &euro;", "").Replace(" ","") + " " + dt.Rows[0]["Currency"].ToString();
            sAmlNote += " - " + GetStatusLabel(dt.Rows[0]["isRefused"].ToString(), dt.Rows[0]["isCancelled"].ToString(), dt.Rows[0]["isTerminated"].ToString(), dt.Rows[0]["isReserved"].ToString()).ToUpper();
        }

        return sAmlNote;
    }

    protected string getAmountCleaned(string sAmount)
    {
        string sCleanValue = "";
        sCleanValue = sAmount.Replace("+", "").Replace(" &amp;euro;", "").Replace(" &euro;","");
        return sCleanValue;
    }
}