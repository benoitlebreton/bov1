﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class API_ChurnResult : System.Web.UI.UserControl
{
    public string Score { get; set; }
    public string ScoreDetails { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            BindResults();
    }

    protected void BindResults()
    {
        // LABEL & ICON
        if (!string.IsNullOrWhiteSpace(Score))
        {
            string sImg = "", sScore = Score;
            switch (Score.ToUpper())
            {
                case "FAIBLE":
                    sImg = "churn-low.png";
                    sScore = "Faible";
                    break;
                case "MOYEN":
                    sImg = "churn-medium.png";
                    sScore = "Moyen";
                    break;
                case "ELEVE":
                    sImg = "churn-high.png";
                    sScore = "Elevé";
                    break;
                default:
                    sScore = "N.C.";
                    break;
            }
            lblScore.Text = sScore;
            if(!string.IsNullOrWhiteSpace(sImg))
                imgChurnIcon.ImageUrl = "~/Styles/Img/" + sImg;
        }

        // DETAILS
        if(!string.IsNullOrWhiteSpace(ScoreDetails))
        {
            DataTable dtResult = new DataTable();
            dtResult.Columns.Add("Label");
            dtResult.Columns.Add("Value");

            string[] arDetails = ScoreDetails.Split(';');

            for (int i = 0; i < arDetails.Length; i++)
            {
                string[] arDetail = arDetails[i].Split(':');

                if (arDetail.Length == 2)
                    dtResult.Rows.Add(new object[] { arDetail[0].Trim(), arDetail[1].Trim() });
            }

            rptDetails.DataSource = dtResult;
            rptDetails.DataBind();
        }
    }
}