﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using XMLMethodLibrary;

public partial class API_LightLock : System.Web.UI.UserControl
{
    public int RefCustomer { set { _iRefCustomer = value; } }
    public bool ActionAllowed;

    private int _iRefCustomer
    {
        get { return int.Parse(ViewState["RefCustomer"].ToString()); }
        set { ViewState["RefCustomer"] = value; }
    }
    private bool _bLockCustomerDebit
    {
        get
        {
            bool bLockCustomerDebit = false;
            return (ViewState["LockCustomerDebit"] != null && bool.TryParse(ViewState["LockCustomerDebit"].ToString(), out bLockCustomerDebit)) ? bLockCustomerDebit : false;
        }
        set { ViewState["LockCustomerDebit"] = value; }
    }
    private string _sLockCustomerDebitReason
    {
        get { return (ViewState["LockCustomerDebitReason"] != null) ? ViewState["LockCustomerDebitReason"].ToString() : ""; }
        set { ViewState["LockCustomerDebitReason"] = value; }
    }
    private int _iLockCustomerDebitRefReason
    {
        get
        {
            int iLockCustomerDebitRefReason = -1;
            return (ViewState["LockCustomerDebitReason"] != null && int.TryParse(ViewState["LockCustomerDebitRefReason"].ToString(), out iLockCustomerDebitRefReason)) ? iLockCustomerDebitRefReason : -1;
        }
        set { ViewState["LockCustomerDebitRefReason"] = value; }
    }
    private string _sLockCustomerDebitDate
    {
        get { return (ViewState["LockCustomerDebitDate"] != null) ? ViewState["LockCustomerDebitDate"].ToString() : ""; }
        set { ViewState["LockCustomerDebitDate"] = value; }
    }
    private DataTable _dtClientDebitLockUnlock
    {
        get
        {
            DataTable dt = new DataTable();
            try { dt = (DataTable)ViewState["dtClientDebitLockUnlock"]; }
            catch (Exception ex) { dt = null; }
            return dt;
        }
        set { ViewState["dtClientDebitLockUnlock"] = value; }
    }
    public event EventHandler LockUnlockStatusChanged;

    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            if(!ActionAllowed)
            {
                btnClientDebitAddLock.Visible = false;
                btnClientDebitUnlock.Visible = false;
                panelClientDebitAddLock.Visible = false;
            }

            ScriptManager.RegisterClientScriptInclude(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "Scripts/LightLock.js");
            InitTooltipScript();
        }
    }

    protected void SetClientDebitLockUnlockList(List<string> listLockList)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("Active");
        dt.Columns.Add("RefReason");
        dt.Columns.Add("Reason");
        dt.Columns.Add("LockDate");
        dt.Columns.Add("UnLockDate");
        dt.Columns.Add("LockBOUser");
        dt.Columns.Add("UnLockBOUser");

        for (int i = 0; i < listLockList.Count; i++)
        {
            List<string> listActive = CommonMethod.GetAttributeValues(listLockList[i], "LOCK", "Active");
            List<string> listRefReason = CommonMethod.GetAttributeValues(listLockList[i], "LOCK", "RefReason");
            List<string> listReason = CommonMethod.GetAttributeValues(listLockList[i], "LOCK", "Reason");
            List<string> listLockDate = CommonMethod.GetAttributeValues(listLockList[i], "LOCK", "LockDate");
            List<string> listUnlockDate = CommonMethod.GetAttributeValues(listLockList[i], "LOCK", "UnLockDate");
            List<string> listLockBOUser = CommonMethod.GetAttributeValues(listLockList[i], "LOCK", "LockBOUser");
            List<string> listUnLockBOUser = CommonMethod.GetAttributeValues(listLockList[i], "LOCK", "UnLockBOUser");

            dt.Rows.Add(new object[] { listActive[0], listRefReason[0], listReason[0], listLockDate[0], listUnlockDate[0], listLockBOUser[0], listUnLockBOUser[0] });
        }

        _dtClientDebitLockUnlock = dt;

        if (dt.Rows.Count > 0)
        {
            DataTable dtLockList = getClientDebitLockUnlockList(dt, true);
            if (dtLockList.Rows.Count > 0)
            {
                panelClientDebitActivatedLock.Visible = true;
                rptClientDebitActivatedLockList.DataSource = dtLockList;
                rptClientDebitActivatedLockList.DataBind();
            }
            else panelClientDebitActivatedLock.Visible = false;
            DataTable dtHistory = getClientDebitLockUnlockList(dt, false);
            if (dtHistory.Rows.Count > 0)
            {
                panelClientDebitLockUnlockListHistory.Visible = true;
                rptClientDebitLockUnlockListHistory.DataSource = dtHistory;
                rptClientDebitLockUnlockListHistory.DataBind();
            }
            else panelClientDebitLockUnlockListHistory.Visible = false;
        }
        else
        {
            panelClientDebitActivatedLock.Visible = false;
            panelClientDebitLockUnlockListHistory.Visible = false;
        }
    }
    protected DataTable getClientDebitLockUnlockList(DataTable dtClientLockUnlockList, bool isActive)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("Active");
        dt.Columns.Add("RefReason");
        dt.Columns.Add("Reason");
        dt.Columns.Add("LockDate");
        dt.Columns.Add("UnLockDate");
        dt.Columns.Add("LockBOUser");
        dt.Columns.Add("UnLockBOUser");

        string sActive = (isActive) ? "1" : "0";
        foreach (DataRow dr in dtClientLockUnlockList.Rows)
        {
            if (dr["Active"].ToString().Trim() == sActive)
                dt.Rows.Add(new object[] { dr["Active"], dr["RefReason"], dr["Reason"], dr["LockDate"], dr["UnLockDate"], dr["LockBOUser"], dr["UnLockBOUser"] });
        }

        return dt;
    }
    protected DataTable getClientDebitLockReasonList()
    {
        DataTable dt = new DataTable();
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("Web.P_GetLockReasonList", conn);
            cmd.CommandType = CommandType.StoredProcedure;


            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;
            da.Fill(dt);
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }
    protected string getClientDebitLockReasonFromRef(int refReason)
    {
        string sReason = "";

        DataTable dt = getClientDebitLockReasonList();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["RefLockReason"].ToString() == refReason.ToString())
                sReason = dt.Rows[i]["LockReason"].ToString();
        }

        return sReason;
    }
    protected bool clientDebitLock(string sCashierToken, int iRefCustomer, string sRefReason)
    {
        bool isOK = false;
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_LockCustomer", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);
            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                                new XElement("Customer",
                                                    new XAttribute("RefCustomer", iRefCustomer),
                                                    new XAttribute("CashierToken", sCashierToken),
                                                    new XAttribute("RefReason", sRefReason))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if (sXmlOut.Length > 0)
            {
                List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Result", "RC");

                if (listRC.Count > 0 && listRC[0] == "0")
                    isOK = true;
            }

        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return isOK;
    }
    protected bool clientDebitUnlock(string sCashierToken, int iRefCustomer, int iLockCustomerRefReason)
    {
        bool isOK = false;
        SqlConnection conn = null;

        try
        {
            if (iLockCustomerRefReason > 0)
            {

                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
                conn.Open();

                SqlCommand cmd = new SqlCommand("web.P_UnLockCustomer", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
                cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);
                cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                                    new XElement("Customer",
                                                        new XAttribute("RefCustomer", iRefCustomer),
                                                        new XAttribute("RefLockReason", iLockCustomerRefReason.ToString()),
                                                        new XAttribute("CashierToken", sCashierToken))).ToString();
                cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

                cmd.ExecuteNonQuery();

                string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

                if (sXmlOut.Length > 0)
                {
                    List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Result", "RC");

                    if (listRC.Count > 0 && listRC[0] == "0")
                        isOK = true;
                }
            }
            else
                isOK = false;

        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return isOK;
    }
    protected string GetClientDebitLockUnlockDetails(string sLockDate, string LockBOUser, bool bLock)
    {
        string sLabel = "";
        if (sLockDate.Trim().Length > 0)
        {
            sLabel = "le " + sLockDate + ((LockBOUser.Trim().Length > 0) ? " par " + LockBOUser : "").ToString();
        }

        return sLabel;
    }
    protected bool GetBoolFromClientDebitLockUnlockStatus(string sStatus)
    {
        bool bStatus = false;

        if (sStatus.Trim() == "1")
            bStatus = true;

        return bStatus;
    }
    protected void btnClientDebitAddLock_Click(object sender, EventArgs e)
    {
        string sCashierToken = authentication.GetCurrent(false).sToken;

        if (!clientDebitLock(sCashierToken, _iRefCustomer, ddlClientDebitLockReason.SelectedValue))
            UpdateMessage("Erreur", "La demande de bloquage du client en débit/crédit a échoué.");

        if (LockUnlockStatusChanged != null)
            LockUnlockStatusChanged(this, EventArgs.Empty);
        else RefreshClientDebitStatus(Client.GetCustomerInformations(_iRefCustomer));
    }
    protected void btnClientDebitUnlock_Click(object sender, EventArgs e)
    {
        string sCashierToken = authentication.GetCurrent(false).sToken;
        bool isOK = true;

        foreach (RepeaterItem item in rptClientDebitActivatedLockList.Items)
        {
            CheckBox cbClientDebitLockReason = (CheckBox)item.FindControl("cbClientDebitLockReason");
            HiddenField hfClientDebitLockRefReason = (HiddenField)item.FindControl("hfClientDebitLockRefReason");
            if (cbClientDebitLockReason != null && hfClientDebitLockRefReason != null && cbClientDebitLockReason.Checked)
            {
                int iLockRefReason = -1;

                if (int.TryParse(hfClientDebitLockRefReason.Value, out iLockRefReason) && !clientDebitUnlock(sCashierToken, _iRefCustomer, iLockRefReason))
                    isOK = false;
            }
        }

        if (!isOK)
            UpdateMessage("Erreur", "La demande de bloquage du client en débit/crédit a échoué.");

        if (LockUnlockStatusChanged != null)
            LockUnlockStatusChanged(this, EventArgs.Empty);
        else RefreshClientDebitStatus(Client.GetCustomerInformations(_iRefCustomer));
    }
    public void RefreshClientDebitStatus(string sXmlInfos)
    {
        List<string> listLockCustomerDebit = CommonMethod.GetAttributeValues(sXmlInfos, "ALL_XML_OUT/Customer", "LockCustomerDebit");
        List<string> listLockCustomerDebitRefReason = CommonMethod.GetAttributeValues(sXmlInfos, "ALL_XML_OUT/Customer", "LockCustomerDebitRefReason");
        SetClientDebitLockUnlockList(CommonMethod.GetTags(sXmlInfos, "ALL_XML_OUT/Customer/LOCKLIST/LOCK"));

        _bLockCustomerDebit = (listLockCustomerDebit.Count > 0 && listLockCustomerDebit[0].ToString().Trim() == "1") ? true : false;
        if (_bLockCustomerDebit)
        {
            int iLockCustomerRefReason;
            if (listLockCustomerDebitRefReason.Count > 0 && int.TryParse(listLockCustomerDebitRefReason[0].ToString(), out iLockCustomerRefReason))
            {
                _iLockCustomerDebitRefReason = iLockCustomerRefReason;
                _sLockCustomerDebitReason = getClientDebitLockReasonFromRef(iLockCustomerRefReason);
            }

            //_sLockedDate = (listLastActionDate.Count > 0) ? listLastActionDate[0] : "";
        }

        hfClientDebitStatus.Value = _bLockCustomerDebit.ToString().ToLower();
        DataTable dtClientDebitLockReasonAvailable = new DataTable();
        dtClientDebitLockReasonAvailable = getClientDebitLockReasonAvailable(getClientDebitLockReasonList());

        if (dtClientDebitLockReasonAvailable.Rows.Count > 0)
        {
            ddlClientDebitLockReason.Items.Clear();
            ddlClientDebitLockReason.Items.Add(new ListItem("", ""));
            ddlClientDebitLockReason.DataSource = dtClientDebitLockReasonAvailable;
            ddlClientDebitLockReason.DataBind();
            panelClientDebitAddLock.Visible = true;
        }
        else { panelClientDebitAddLock.Visible = false; }

        if (_bLockCustomerDebit)
        {
            lblClientDebitStatus.Text = "<span style=\"color:red;\">bloqué</span>";
            imgClientDebitStatus.ImageUrl = "~/Styles/Img/no-billets.png";
            imgClientDebitStatus.ToolTip = "Raison blocage : " + _sLockCustomerDebitReason + ((_sLockCustomerDebitDate.Length > 0) ? " le " + _sLockCustomerDebitDate : "");
        }
        else
        {
            lblClientDebitStatus.Text = "<span style=\"color:green;\">débloqué</span>";
            ddlClientDebitLockReason.CssClass = "";
            imgClientDebitStatus.ImageUrl = "~/Styles/Img/billets.png";
            imgClientDebitStatus.ToolTip = "";
        }

        try
        {
            CheckBox cbAllLockReason = (CheckBox)rptClientDebitActivatedLockList.Controls[0].Controls[0].FindControl("cbAllClientDebitLockReason");
            cbAllLockReason.Visible = false;
            if (_bLockCustomerDebit)
                cbAllLockReason.Visible = true;
        }
        catch (Exception ex)
        {

        }

        upClientDebitStatusImg.Update();
        upClientDebitStatusContent.Update();
        InitTooltipScript();
    }
    protected DataTable getClientDebitLockReasonAvailable(DataTable dtLockReasonList)
    {
        DataTable dt = dtLockReasonList;
        DataTable dtLockList = getClientDebitLockUnlockList(_dtClientDebitLockUnlock, true);

        if (dtLockList != null)
        {
            foreach (DataRow drLockList in dtLockList.Rows)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    if (dr.RowState != DataRowState.Deleted && dr["RefLockReason"].ToString().Trim() == drLockList["RefReason"].ToString().Trim())
                        dr.Delete();
                }
            }
            dt.AcceptChanges();
        }

        return dt;
    }

    protected void UpdateMessage(string sTitle, string sMessage)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "AlertMessage(\"" + sTitle + "\", \"" + sMessage + "\");", true);
    }

    protected void InitTooltipScript()
    {
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "initTooltip();", true);
    }
}