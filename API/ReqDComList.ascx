﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ReqDComList.ascx.cs" Inherits="API_ReqDComList" %>

<asp:Panel ID="panelLabel" runat="server" style="color:#344b56;font-weight:bold;text-transform:uppercase;border-bottom:1px solid #344b56; margin:10px 0 5px 0;">
    Réquisition
</asp:Panel>
<asp:Repeater ID="rptRJDC" runat="server">
    <HeaderTemplate>
        <table class="defaultTable2">
            <tr>
                <th>Raison</th>
                <th style="width:1px;padding:0 5px;white-space:nowrap">Date traitement</th>
            </tr>
    </HeaderTemplate>
    <ItemTemplate>
            <tr class='row-over-color <%#Container.ItemIndex % 2 == 0 ? "row-even" : "row-odd" %>' onclick='window.location.href="RequisitionDroitCommunication.aspx?ref=<%# Eval("Ref") %>"'>
                <td><%#Eval("Description") %></td>
                <td style="text-align:center"><%#Eval("Date") %></td>
            </tr>
    </ItemTemplate>
    <FooterTemplate>
        </table>
    </FooterTemplate>
</asp:Repeater>
<asp:Panel ID="panelNoRJDC" runat="server" Visible="false" HorizontalAlign="Center">
    Aucune réquisition
</asp:Panel>