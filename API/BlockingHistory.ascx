﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BlockingHistory.ascx.cs" Inherits="API_BlockingHistory" %>

<asp:Panel ID="panelBlockingHistory" runat="server">
    <asp:Panel ID="panelBlockingHistoryList" runat="server">
        <asp:Repeater ID="rptClientDebitActivatedLockList" runat="server">
            <HeaderTemplate>
                <table id="tBeneficiaryList" cellpadding="0" cellspacing="0" style="width: 100%" class="defaultTable2">
                    <thead>
                    <tr>
                        <th>Raison</th>
                        <th style="width:50%">Blocage</th>
                    </tr>
                    </thead>
                    <tbody>
            </HeaderTemplate>
            <ItemTemplate>
                <tr class='<%# GetBlockingStatus(Eval("Active").ToString()) %>'>
                    <td><%# Eval("Reason") %></td>
                    <td>
                        <div class="trimmer" style="padding:0;width:200px;" >
                            <asp:Label CssClass="tooltip" ID="lblClientDebitLockDetails" runat="server" 
                                ToolTip='<%# GetClientDebitLockUnlockDetails(Eval("LockDate").ToString(), Eval("LockBOUser").ToString(), true) %>'>
                                <%# GetClientDebitLockUnlockDetails(Eval("LockDate").ToString(), Eval("LockBOUser").ToString(), true) %>
                            </asp:Label>
                        </div>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                    </tbody>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </asp:Panel>
    <asp:Panel ID="panelBlockingHistoryListEmpty" runat="server" style="font-weight:bold">Aucun blocage</asp:Panel>
</asp:Panel>