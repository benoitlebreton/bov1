﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CreateMessage.ascx.cs" Inherits="API_CreateMessage" %>

<div class="add-message">
    <asp:UpdatePanel ID="upMessage" runat="server">
        <ContentTemplate>
            <div style="color:#344b56;font-weight:bold;text-transform:uppercase;border-bottom:1px solid #344b56;margin-bottom:5px">
                Message
            </div>
            <div style="display:table;width:100%;border-collapse:collapse">
                <div style="display:table-row">
                    <%--<div style="display:table-cell;padding-right:5px;vertical-align:bottom">
                        <img src="Styles/Img/important-uncheck.png" style="height:25px;cursor:pointer" alt="Important" title="Important" onclick="ToggleImportant(this);" />
                        <asp:CheckBox ID="ckbImportant" runat="server" Checked="false" style="display:none" />
                    </div>--%>
                    <div style="display:table-cell;width:100%;padding-bottom:5px;">
                        <asp:TextBox ID="txtTitle" runat="server" placeholder="Titre"></asp:TextBox>
                    </div>
                </div>
            </div>
            <asp:UpdatePanel ID="upTextFormat" runat="server">
                <ContentTemplate>
                    <div>
                        <b>Type de contenu :</b>
                        <asp:RadioButtonList ID="rblMessageFormat" runat="server" CssClass="RadioButtonList" RepeatDirection="Horizontal" RepeatLayout="Flow" AutoPostBack="true" OnSelectedIndexChanged="rblMessageFormat_SelectedIndexChanged">
                            <asp:ListItem Selected="True" Text="Texte" Value="text"></asp:ListItem>
                            <asp:ListItem Text="HTML" Value="html"></asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                    <asp:Panel ID="panelTextFormat" runat="server">
                        <div style="margin-top:5px">
                            <asp:TextBox ID="txtContent" runat="server" TextMode="MultiLine" Rows="5" placeholder="Contenu"></asp:TextBox>
                        </div>
                        <div>
                            <div class="format" style="float:left">
                                <div>
                                    <input class="big-size" type="button" value="A" onclick="AddTag('[size=40]', '[/size]');" />
                                    <input class="small-size" type="button" value="A" onclick="AddTag('[size=10]', '[/size]');" />
                                    <input type="button" value="Gras" style="font-weight:bold" onclick="AddTag('[b]', '[/b]');" />
                                    <input type="button" value="Italic" style="font-style:italic" onclick="AddTag('[i]', '[/i]');" />
                                    <input type="button" value="Souligné" style="text-decoration:underline" onclick="AddTag('[u]', '[/u]');" />
                                    <input type="button" value="Orange" class="font-orange" onclick="AddTag('[o]', '[/o]');" />
                                </div>
                                <div style="margin-top:2px">
                                    <input class="tab-icon" type="button" value="Tab" onclick="AddTag('[tab]', '[/tab]');" />
                                    <input class="align-left-icon" type="button" value="Gauche" onclick="AddTag('[tal]', '[/tal]');" />
                                    <input class="align-center-icon" type="button" value="Centre" onclick="AddTag('[tac]', '[/tac]');" />
                                    <input class="align-right-icon" type="button" value="Droite" onclick="AddTag('[tar]', '[/tar]');" />
                                </div>
                                <div style="margin-top:3px">
                                    <input class="link-icon" type="button" value="Lien" onclick="AddTag('[url=Votre lien ici]', '[/url]');" />
                                    <input class="image-icon" type="button" value="Image" onclick="AddTag('[img]', '[/img]');" />
                                </div>
                            </div>
                            <div style="float:right">
                                <input type="button" value="Aperçu" class="button" onclick="MessageToHTML();" />
                            </div>
                            <div style="clear:both"></div>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="panelIframeFormat" runat="server" Visible="false">
                        <div style="display:flex;flex-direction:row;height:25px;margin-top:5px">
                            <asp:TextBox ID="txtIFrameURL" runat="server" placeholder="Nom du fichier HTML (qui doit se trouver dans le répertoire &laquo;message&raquo; du site)"></asp:TextBox>
                            <input type="button" class="button" value="Aperçu" onclick="ShowIFrame();" style="position:relative;top:-3px;margin-left:5px" />
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>

            <asp:Panel ID="panelTo" runat="server" Visible="false">
                <div style="color:#344b56;font-weight:bold;text-transform:uppercase;border-bottom:1px solid #344b56;margin:10px 0 5px 0">
                    Envoyé à
                </div>
                <div style="margin-top:5px">
                    <asp:Panel ID="panelToClients" runat="server" Visible="false">
                        <asp:RadioButtonList ID="rblSendToClient" runat="server" CssClass="RadioButtonList" RepeatDirection="Horizontal" OnSelectedIndexChanged="rblSendToClient_SelectedIndexChanged" AutoPostBack="true">
                            <asp:ListItem Text="Tous les clients" Value="all" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Sélection" Value="select"></asp:ListItem>
                        </asp:RadioButtonList>

                        <asp:Panel ID="panelSelectClients" runat="server" Visible="false" DefaultButton="btnSearchClient">
                            <div style="margin-top:5px;">
                                <div class="table" style="width:100%;border-collapse:collapse">
                                    <div class="table-row">
                                        <div class="table-cell">
                                            <asp:TextBox ID="txtSearchClient" runat="server" style="width:100%" placeholder="nom, prénom, n° téléphone, n° compte, n° pack, ..."></asp:TextBox>
                                        </div>
                                        <div class="table-cell" style="width:1px;padding-left:10px">
                                            <asp:Button ID="btnSearchClient" runat="server" CssClass="button" Text="Rechercher" OnClick="btnSearchClient_Click" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <asp:Repeater ID="rptSendToClient" runat="server">
                                    <HeaderTemplate>
                                        <div style="max-height:400px;overflow:hidden;overflow-y:auto">
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <div class='<%# Container.ItemIndex % 2 != 0 ? "" : "alternate-row" %>' style="padding:5px">
                                            <div style="float:left;width:95%">
                                                <asp:Label ID="lblSendToClientInfos" runat="server" AssociatedControlID="ckbSendToClient" style="cursor:pointer">
                                                    <%#Eval("Label") %>
                                                </asp:Label>
                                            </div>
                                            <div style="float:right;width:5%">
                                                <asp:HiddenField ID="hdnSendToClientRef" runat="server" Value='<%#Eval("RefCustomer") %>' />
                                                <asp:CheckBox ID="ckbSendToClient" runat="server" />
                                            </div>
                                            <div style="clear:both"></div>
                                        </div>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </div>
                                    </FooterTemplate>
                                </asp:Repeater>
                                <div style="text-align:right;padding-top:10px;">
                                    <asp:Button ID="btnDeleteSendToClient" runat="server" CssClass="button" Text="Supprimer" OnClick="btnDeleteSendToClient_Click" Visible="false" />
                                </div>
                            </div>

                            <div id="popup-search-client" style="display:none">
                                <asp:Repeater ID="rptSearchClient" runat="server">
                                    <HeaderTemplate>
                                        <div style="max-height:400px;overflow:hidden;overflow-y:auto">
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <div class='<%# Container.ItemIndex % 2 != 0 ? "" : "alternate-row" %>' style="padding:5px">
                                            <div style="float:left;width:95%">
                                                <asp:Label ID="lblSearchClientInfos" runat="server" AssociatedControlID="ckbSelectSearchClient"
                                                    Text='<%#Eval("LastName").ToString() + " " + Eval("FirstName") + ", " + Eval("BirthDate") %>' style="cursor:pointer">
                                                </asp:Label>
                                            </div>
                                            <div style="float:right;width:5%">
                                                <asp:HiddenField ID="hdnSearchClientRef" runat="server" Value='<%#Eval("RefCustomer") %>' />
                                                <asp:CheckBox ID="ckbSelectSearchClient" runat="server" />
                                            </div>
                                            <div style="clear:both"></div>
                                        </div>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </div>
                                    </FooterTemplate>
                                </asp:Repeater>

                                <asp:Button ID="btnAddSearchClient" runat="server" Text="Ajouter" OnClick="btnAddSearchClient_Click" style="display:none" />
                            </div>
                        </asp:Panel>

                    </asp:Panel>
                    <asp:Panel ID="panelToAgency" runat="server" Visible="false">
                        <div>
                            <asp:RadioButtonList ID="rblSendToAgency" runat="server" CssClass="RadioButtonList" RepeatDirection="Horizontal" OnSelectedIndexChanged="rblSendToAgency_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Text="tous les buralistes" Value="all" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="sélection" Value="select"></asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                        <asp:Panel ID="panelSelectAgency" runat="server" Visible="false">
                            <div class="table" style="width:100%;border-collapse:collapse">
                                <div class="row">
                                    <div class="cell" style="white-space:nowrap">
                                        <asp:DropDownList ID="ddlAgency" runat="server" CssClass="multiselect" DataTextField="AgencyName" DataValueField="AgencyID" AppendDataBoundItems="true">
                                            <asp:ListItem Text="" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="cell" style="width:1px;padding-left:10px">
                                        <asp:Button ID="btnAddAgencyTo" runat="server" Text="Ajouter" CssClass="button" OnClick="btnAddAgencyTo_Click" />
                                    </div>
                                </div>
                            </div>
                            <div>
                                <asp:TextBox ID="txtAgenciesSelected" runat="server" TextMode="MultiLine" ReadOnly="true" style="background-color:#fff;border:1px solid #a9a9a9;height:50px;overflow-y:scroll;white-space:normal" placeholder="Agences sélectionnées..."></asp:TextBox>
                            </div>
                        </asp:Panel>
                    </asp:Panel>
                </div>
            </asp:Panel>
            <div style="color:#344b56;font-weight:bold;text-transform:uppercase;border-bottom:1px solid #344b56;margin:10px 0 5px 0">
                Options
            </div>
            <asp:Panel ID="panelDate" runat="server" style="margin-top:5px">
                <asp:Label ID="lblFrom" runat="server" AssociatedControlID="txtFrom">Afficher du</asp:Label>
                <asp:TextBox ID="txtFrom" runat="server" style="width:100px" placeholder="JJ/MM/AAAA" MaxLength="10"></asp:TextBox>
                <asp:Label ID="lblTo" runat="server" AssociatedControlID="txtTo">au</asp:Label>
                <asp:TextBox ID="txtTo" runat="server" style="width:100px" placeholder="JJ/MM/AAAA" MaxLength="10"></asp:TextBox>
                <i>(inclus)</i>
            </asp:Panel>
            <asp:Panel ID="panelDisplayMethod" runat="server" style="margin-top:5px">
                <div>Afficher sous forme de</div>
                <div style="display:inline-block">
                    Site web :
                    <asp:RadioButtonList ID="rblDisplayMethod" runat="server" CssClass="RadioButtonList" RepeatDirection="Horizontal" RepeatLayout="Flow">
                        <asp:ListItem Text="Bannière" Value="Banner"></asp:ListItem>
                        <asp:ListItem Text="Popup" Value="Popup"></asp:ListItem>
                    </asp:RadioButtonList>
                    <a href="../Styles/Img/methode_affichage.jpg" target="_blank">?</a>
                </div>
                <div style="display:inline-block;margin-left:20px;">
                    Application mobile :
                    <asp:RadioButtonList ID="rblDisplayMethodAppMobile" runat="server" CssClass="RadioButtonList" RepeatDirection="Horizontal" RepeatLayout="Flow">
                        <asp:ListItem Text="Information" Value="AppInfo"></asp:ListItem>
                    </asp:RadioButtonList>
                </div>
            </asp:Panel>
            <asp:Panel ID="panelImportant" runat="server" style="margin-top:5px" Visible="false">
                <asp:CheckBox ID="ckbImportant" runat="server" />
                <asp:Label ID="lblImportant" runat="server" AssociatedControlID="ckbImportant">Important <i>(ce message apparaîtra en tête de fil)</i></asp:Label>
            </asp:Panel>
            <asp:Panel ID="panelTracking" runat="server" style="margin-top:5px" Visible="false">
                <asp:CheckBox ID="ckbTracking" runat="server" />
                <asp:Label ID="Label1" runat="server" AssociatedControlID="ckbTracking">Suivi <i>(nombre de fois lu, etc.)</i></asp:Label>
            </asp:Panel>

            <div style="margin-top:20px;text-align:center">
                <asp:Button ID="btnValidate" runat="server" Text="Enregistrer" CssClass="button" OnClick="btnValidate_Click" />
            </div>

            <div id="message-to-html" class="message" style="display:none">

            </div>
            <div id="iframe-test" class="message" style="display:none">
                <iframe style="width:98.5%;height:450px;border:none"></iframe>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnAddSearchClient" EventName="click" />
        </Triggers>
    </asp:UpdatePanel>

</div>

<script type="text/javascript">

    //function ToggleImportant(img) {
    //    var check = $('#ckbImportant.ClientID').attr('checked');

    //    if (check != 'checked') {
    //        $('#ckbImportant.ClientID').attr('checked', 'true');
    //        $(img).attr('src', 'Styles/Img/important-check.png');
    //    }
    //    else {
    //        $('#ckbImportant.ClientID').removeAttr('checked');
    //        $(img).attr('src', 'Styles/Img/important-uncheck.png');
    //    }
    //}

    function ShowSearchResult() {
        if (!$('#popup-search-client').hasClass('ui-dialog-content')) {
            $('#popup-search-client.ui-dialog-content').remove();
            $('#popup-search-client').dialog({
                title: "Résultat de la recherche",
                width: "500px",
                autoOpen: true,
                modal: true,
                resizable: false,
                draggable: false,
                buttons: [{ text: $('#<%=btnAddSearchClient.ClientID%>').val(), click: function () { $("#<%=btnAddSearchClient.ClientID %>").click(); $(this).dialog('close'); } }]
            });
            $("#popup-search-client").parent().appendTo(jQuery("form:first"));
        }
        else {
            $('#popup-search-client').dialog('open');
        }
    }

    function PostBackLoad() {
        $('.multiselect').combobox();
        $('.ui-combobox .ui-combobox-input').each(function () {
            $(this).width('95%').height('28px');
            $(this).parent('.ui-combobox').find('.ui-button').width('5%').height('30px').css('top', '11px');
        });
    }

    function AddTag(tagStart, tagEnd) {
        var textTmp = "";
        if (tagStart == '[img]')
            textTmp = "Votre image ici";
        else if(tagStart == '[tab]')
            textTmp = "";
        else textTmp = "Votre texte ici";

        if (tagEnd == null)
            textTmp = "";

        var caretPos = doGetCaretPosition($('#<%=txtContent.ClientID%>')[0]);
        //console.log(caretPos);

        var oldVal = $('#<%=txtContent.ClientID%>').val();
        var insertText = tagStart + textTmp;
        if (tagEnd != null)
            insertText += tagEnd;
        var newVal = [oldVal.slice(0, caretPos), insertText, oldVal.slice(caretPos)].join('');

        $('#<%=txtContent.ClientID%>').val(newVal);

        var indexStart = 0;
        if (tagStart != '[tab]') {
            indexStart = caretPos + tagStart.length;
        }
        else {
            indexStart = newVal.length;
        }

        $('#<%=txtContent.ClientID%>').selectRange(indexStart, indexStart + textTmp.length);
    }

    $.fn.selectRange = function (start, end) {
        if (!end) end = start;
        return this.each(function () {
            if (this.setSelectionRange) {
                this.focus();
                this.setSelectionRange(start, end);
            } else if (this.createTextRange) {
                var range = this.createTextRange();
                range.collapse(true);
                range.moveEnd('character', end);
                range.moveStart('character', start);
                range.select();
            }
        });
    };

    function MessageToHTML() {
        var val = $('#<%=txtContent.ClientID%>').val();
        if (val.length > 0) {
            /*val = val.replace(/\[b\]/g, '<b>')
                .replace(/\[\/b\]/g, '</b>')
                .replace(/\[i\]/g, '<i>')
                .replace(/\[\/i\]/g, '</i>')
                .replace(/\[u\]/g, '<span style="text-decoration:underline">')
                .replace(/\[\/u\]/g, '</span>')
                .replace(/\[o\]/g, '<span class="font-orange">')
                .replace(/\[\/o\]/g, '</span>')
                .replace(/\[link\]/g, '<span class="font-orange">')
                .replace(/\[\/link\]/g, '</a>');
            nl2br(val)*/

            var result = XBBCODE.process({
                text: val,
                removeMisalignedTags: false,
                addInLineBreaks: true
            });

            $('#message-to-html').empty().append(result.html);

            $('#message-to-html').dialog({
                draggable: false,
                resizable: false,
                width: 800,
                dialogClass: "no-close",
                modal: true,
                title: 'Aperçu',
                buttons: {
                    Ok: function () {
                        $(this).dialog("close");
                    }
                }
            });
        }
    }

    function nl2br(str, is_xhtml) {
        var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
        return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
    }

    function doGetCaretPosition(ctrl) {
        var CaretPos = 0;
        // IE Support
        if (document.selection) {
            ctrl.focus();
            var Sel = document.selection.createRange();
            Sel.moveStart('character', -ctrl.value.length);
            CaretPos = Sel.text.length;
        }
            // Firefox support
        else if (ctrl.selectionStart || ctrl.selectionStart == '0')
            CaretPos = ctrl.selectionStart;
        return (CaretPos);
    }


    (function ($) {
        $.widget("ui.combobox", {
            _create: function () {
                var input,
                    that = this,
                    wasOpen = false,
                    select = this.element.hide(),
                    selected = select.children(":selected"),
                    value = selected.val() ? selected.text() : "",
                    wrapper = this.wrapper = $("<span>")
                    .addClass("ui-combobox")
                    .insertAfter(select);
                function removeIfInvalid(element) {
                    var value = $(element).val(),
                        matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex(value) + "$", "i"),
                        valid = false;
                    select.children("option").each(function () {
                        if ($(this).text().match(matcher)) {
                            this.selected = valid = true;
                            return false;
                        }
                    });
                    if (!valid) {
                        // remove invalid value, as it didn't match anything
                        $(element)
                            .val("")
                            .attr("title", value + " n'existe pas")
                            .tooltip("open");
                        select.val("");
                        setTimeout(function () {
                            input.tooltip("close").attr("title", "");
                        }, 2500);
                        input.data("ui-autocomplete").term = "";
                    }
                }
                input = $("<input>")
                    .appendTo(wrapper)
                    .val(value)
                    .attr("title", "")
                    .addClass("ui-state-default ui-combobox-input")
                    .autocomplete({
                        delay: 0,
                        minLength: 0,
                        source: function (request, response) {
                            var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
                            response(select.children("option").map(function () {
                                var text = $(this).text();
                                if (this.value && (!request.term || matcher.test(text)))
                                    return {
                                        label: text.replace(
                                            new RegExp(
                                            "(?![^&;]+;)(?!<[^<>]*)(" +
                                            $.ui.autocomplete.escapeRegex(request.term) +
                                            ")(?![^<>]*>)(?![^&;]+;)", "gi"
                                            ), "<strong>$1</strong>"),
                                        value: text,
                                        option: this
                                    };
                            }));
                        },
                        select: function (event, ui) {
                            ui.item.option.selected = true;
                            that._trigger("selected", event, {
                                item: ui.item.option
                            });
                        },
                        change: function (event, ui) {
                            if (!ui.item) {
                                removeIfInvalid(this);
                            }
                        }
                    })
                    .addClass("ui-widget ui-widget-content ui-corner-left");
                input.data("ui-autocomplete")._renderItem = function (ul, item) {
                    return $("<li>")
                    .append("<a>" + item.label + "</a>")
                    .appendTo(ul);
                };
                $("<a>")
                    .attr("tabIndex", -1)
                    .attr("title", "Afficher tout")
                    .tooltip()
                    .appendTo(wrapper)
                    .button(
                        {
                            icons: {
                                primary: "ui-icon-triangle-1-s"
                            },
                            text: false
                        }
                    )
                    .removeClass("ui-corner-all")
                    .addClass("ui-corner-right ui-combobox-toggle")
                    .mousedown(function () {
                        wasOpen = input.autocomplete("widget").is(":visible");
                    })
                    .click(function () {
                        input.focus();
                        // close if already visible
                        if (wasOpen) {
                            return;
                        }
                        // pass empty string as value to search for, displaying all results
                        input.autocomplete("search", "");
                    });
                input.tooltip({
                    tooltipClass: "ui-state-highlight"
                });
            },
            _destroy: function () {
                this.wrapper.remove();
                this.element.show();
            }
        });

        //$('.tooltip').tooltip({ items: "span[tooltip]" });
    })(jQuery);

    function ShowIFrame() {
        $('#iframe-test iframe').attr('src', 'https://mon.compte-nickel.fr/message/' + $('#<%=txtIFrameURL.ClientID%>').val());

        $('#iframe-test').dialog({
            draggable: false,
            resizable: false,
            width: 1000,
            dialogClass: "no-close",
            modal: true,
            title: 'Aperçu',
            buttons: {
                Ok: function () {
                    $(this).dialog("close");
                }
            }
        });
    }
</script>