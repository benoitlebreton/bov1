﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CAPEmail.ascx.cs" Inherits="API_CAPEmail" %>

<input type="button" class="button" value="Modifier l'email" onclick="javascript:ShowCAPEmailDialog();" />

<div id="dialog-cap-email">
    <div>
        <div class="label">Email</div>
        <asp:TextBox ID="txtEmail" runat="server" CssClass="txtEmail" />
    </div>

    <asp:TextBox ID="txtQualityCode" runat="server" CssClass="txtEmailQualityCode" style="display:none" />

    <div>
        <input type="button" value="Valider" class="button" onclick="javascript:ValidateEmail()" />
        <asp:UpdatePanel ID="upValidateEmail" runat="server">
            <ContentTemplate>
                <asp:Button ID="btnValidateEmail" runat="server" CssClass="btnValidateEmail" OnClick="btnValidateEmail_Click" style="display:none" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</div>

<div id="dialog-cap-multi-email">
    <div id="select-list">

    </div>
    <div>
        <input type="button" value="Utiliser l'email saisie" class="button" onclick="javascript:ForceValidateEmail(true)" />
    </div>
</div>

<div id="dialog-cap-force-email">
    <div class="message">

    </div>
    <div>
        Voulez-vous poursuivre avec l'email saisi ?
    </div>
    <div>
        <input type="button" value="OUI" class="button" onclick="javascript:ForceValidateEmail(true)" />
    </div>
</div>