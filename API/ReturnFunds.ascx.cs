﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

public partial class API_ReturnFunds : System.Web.UI.UserControl
{
    private DataTable _dtReturnFunds
    {
        get { return (ViewState["dtReturnFunds"] != null) ? (DataTable)ViewState["dtReturnFunds"] : null; }
        set { ViewState["dtReturnFunds"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            //Page.Header.Controls.Add(new System.Web.UI.LiteralControl("<link rel=\"stylesheet\" type=\"text/css\" href=\"" + ResolveUrl("~/Styles/ReturnFunds.css") + "\" />"));

            ScriptManager.RegisterClientScriptInclude(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "Scripts/jquery.mask.js");
            ScriptManager.RegisterClientScriptInclude(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "Scripts/ReturnFunds.js?v=20181010");
        }
    }

    public void SetReturnFunds(DataTable dt)
    {
        _dtReturnFunds = dt;
        BindReturnFunds();
    }

    public void InitMask()
    {
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "InitIBANinput();", true);
    }

    public bool CheckForm(out string sErrorMessage, out bool bEmpty)
    {
        bool bOk = true;
        sErrorMessage = "";
        StringBuilder sbErrorMessage = new StringBuilder();
        bEmpty = true;

        if (String.IsNullOrWhiteSpace(txtAmount.Text))
            sbErrorMessage.AppendLine("Montant à envoyer : champ requis");
        else
        {
            bEmpty = false;

            decimal dAmount = -1;
            if (!Decimal.TryParse(txtAmount.Text.Replace('.', ','), NumberStyles.Currency, new CultureInfo("fr-FR"), out dAmount))
                sbErrorMessage.AppendLine("Montant à envoyer : valeur incorrecte");
        }

        if (String.IsNullOrWhiteSpace(txtBeneficiary.Text))
            sbErrorMessage.AppendLine("Nom bénéficiaire : champ requis");
        else bEmpty = false;

        if (String.IsNullOrWhiteSpace(txtIBAN.Text))
            sbErrorMessage.AppendLine("IBAN : champ requis");
        else
        {
            bEmpty = false;

            if (!tools.IsIbanChecksumValid(txtIBAN.Text.Replace(" ", "")))
                sbErrorMessage.AppendLine("IBAN : valeur incorrecte");
        }

        if (String.IsNullOrWhiteSpace(txtBIC.Text))
            sbErrorMessage.AppendLine("BIC : champ requis");

        if (String.IsNullOrWhiteSpace(txtLabel.Text))
            sbErrorMessage.AppendLine("Libellé : champ requis");
        else bEmpty = false;

        sErrorMessage = sbErrorMessage.Replace(Environment.NewLine, "<br />").ToString();

        // Validate if empty but previous inserted
        if (bEmpty && _dtReturnFunds != null && _dtReturnFunds.Rows.Count > 0)
            sErrorMessage = "";

        if (sErrorMessage.Length > 0)
            bOk = false;

        return bOk;
    }

    public XElement GetDataToXML()
    {
        XElement xml = null;
        string sErrorMessage;
        bool bEmpty;

        bool bFormValid = CheckForm(out sErrorMessage, out bEmpty);

        if (_dtReturnFunds != null && _dtReturnFunds.Rows.Count > 0 || (bFormValid && !bEmpty))
        {
            xml = new XElement("RETURN_FUNDS");

            if (_dtReturnFunds != null && _dtReturnFunds.Rows.Count > 0)
            {
                for (int i = 0; i < _dtReturnFunds.Rows.Count; i++)
                {
                    xml.Add(new XElement("RETURN_FUND",
                            new XAttribute("TransferAmount", _dtReturnFunds.Rows[i]["Amount"].ToString()),
                            new XAttribute("BeneficiaryName", _dtReturnFunds.Rows[i]["Beneficiary"].ToString()),
                            new XAttribute("IBAN", _dtReturnFunds.Rows[i]["IBAN"].ToString().Replace(" ", "").ToUpper()),
                            new XAttribute("BIC", _dtReturnFunds.Rows[i]["BIC"].ToString().Replace(" ", "").ToUpper()),
                            new XAttribute("CreditReference", _dtReturnFunds.Rows[i]["Label"].ToString())
                            ));
                }
            }

            if(bFormValid && !bEmpty)
            {
                decimal dAmount = 0;

                if (Decimal.TryParse(txtAmount.Text.Replace('.', ','), NumberStyles.Currency, new CultureInfo("fr-FR"), out dAmount))
                {
                    dAmount = decimal.Round(dAmount, 2, MidpointRounding.AwayFromZero);

                    xml.Add(new XElement("RETURN_FUND",
                            new XAttribute("TransferAmount", dAmount.ToString("0.00")),
                            new XAttribute("BeneficiaryName", txtBeneficiary.Text),
                            new XAttribute("IBAN", txtIBAN.Text.Replace(" ", "").ToUpper()),
                            new XAttribute("BIC", txtBIC.Text.Replace(" ", "").ToUpper()),
                            new XAttribute("CreditReference", txtLabel.Text)
                            ));
                }
            }
        }

        return xml;
    }

    public event EventHandler ReturnFundAddedRemoved;
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        string sErrorMessage;
        bool bEmpty;
        bool bFormValid = CheckForm(out sErrorMessage, out bEmpty);

        if (bFormValid && !bEmpty)
        {
            if(_dtReturnFunds == null)
            {
                _dtReturnFunds = new DataTable();
                _dtReturnFunds.Columns.Add("Amount");
                _dtReturnFunds.Columns.Add("Beneficiary");
                _dtReturnFunds.Columns.Add("IBAN");
                _dtReturnFunds.Columns.Add("BIC");
                _dtReturnFunds.Columns.Add("Label");
            }

            decimal dAmount = 0;
            if (Decimal.TryParse(txtAmount.Text.Replace('.', ','), NumberStyles.Currency, new CultureInfo("fr-FR"), out dAmount))
            {
                dAmount = decimal.Round(dAmount, 2, MidpointRounding.AwayFromZero);

                _dtReturnFunds.Rows.Add(new object[] { dAmount.ToString("0.00"), txtBeneficiary.Text, txtIBAN.Text.ToUpper(), txtBIC.Text.ToUpper(), txtLabel.Text });

                txtAmount.Text = "";
                txtBeneficiary.Text = "";
                txtIBAN.Text = "";
                txtBIC.Text = "";
                txtLabel.Text = "";

                BindReturnFunds();

                if (ReturnFundAddedRemoved != null)
                    ReturnFundAddedRemoved(this, EventArgs.Empty);
            }
        }
        else AlertMessage("Erreur", sErrorMessage);
    }

    protected void btnRemove_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton btn = (ImageButton)sender;

        int i = int.Parse(btn.CommandArgument);

        if (_dtReturnFunds.Rows.Count > i)
        {
            _dtReturnFunds.Rows.RemoveAt(i);

            BindReturnFunds();

            if (ReturnFundAddedRemoved != null)
                ReturnFundAddedRemoved(this, EventArgs.Empty);
        }
    }

    protected void BindReturnFunds()
    {
        rptReturnFunds.DataSource = _dtReturnFunds;
        rptReturnFunds.DataBind();

        if (_dtReturnFunds.Rows.Count == 0)
            rptReturnFunds.Visible = false;
        else rptReturnFunds.Visible = true;

        upReturnFunds.Update();
    }

    public decimal GetTotalReturnFunds()
    {
        decimal dTotal = 0;
        decimal dTmp;

        if (_dtReturnFunds != null)
        {
            for (int i = 0; i < _dtReturnFunds.Rows.Count; i++)
            {
                if (Decimal.TryParse(_dtReturnFunds.Rows[i]["Amount"].ToString().Replace(".", ","), NumberStyles.Currency, new CultureInfo("fr-FR"), out dTmp))
                    dTotal += dTmp;
            }
        }

        string sErrorMessage;
        bool bEmpty;
        bool bFormValid = CheckForm(out sErrorMessage, out bEmpty);

        if (bFormValid && !bEmpty)
        {
            if (Decimal.TryParse(txtAmount.Text.Replace(".", ","), NumberStyles.Currency, new CultureInfo("fr-FR"), out dTmp))
                dTotal += dTmp;
        }

        return dTotal;
    }

    protected void AlertMessage(string sTitle, string sMessage)
    {
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('" + sTitle + "', \"" + sMessage + "\");", true);
    }
}