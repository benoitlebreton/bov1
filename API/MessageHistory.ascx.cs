﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using XMLMethodLibrary;

public partial class API_MessageHistory : System.Web.UI.UserControl
{
    public bool ActionAllowed { get { return (ViewState["ActionAllowed"] != null) ? bool.Parse(ViewState["ActionAllowed"].ToString()) : false; } set { ViewState["ActionAllowed"] = value; } }
    public bool IsHome { get { return (ViewState["HistoryIsHome"] != null) ? bool.Parse(ViewState["HistoryIsHome"].ToString()) : false; } set { ViewState["HistoryIsHome"] = value; } }
    public bool IsPro { get { return (ViewState["HistoryIsPro"] != null) ? bool.Parse(ViewState["HistoryIsPro"].ToString()) : false; } set { ViewState["HistoryIsPro"] = value; } }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SetMessages();
        }
    }

    public void SetMessages()
    {
        DataTable dt = GetMessages(IsHome, IsPro);
        rptMessageHistory.DataSource = dt;
        rptMessageHistory.DataBind();
    }

    protected DataTable GetMessages(bool bIsHome, bool bIsPro)
    {
        SqlConnection conn = null;
        DataTable dt = new DataTable();
        dt.Columns.Add("Ref");
        dt.Columns.Add("Title");
        dt.Columns.Add("Content");
        dt.Columns.Add("From");
        dt.Columns.Add("To");
        dt.Columns.Add("Hide");
        dt.Columns.Add("NbDest");

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_GetMessage", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                            new XElement("Message",
                                                new XAttribute("RefCustomer", ""),
                                                new XAttribute("HomePage", (IsHome) ? "1" : "0"),
                                                new XAttribute("IsPro", (IsPro) ? "1" : "0"))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOUT = cmd.Parameters["@OUT"].Value.ToString();

            if (sXmlOUT.Length > 0)
            {
                List<string> listMessage = CommonMethod.GetTags(sXmlOUT, "ALL_XML_OUT/Message");

                for (int i = 0; i < listMessage.Count; i++)
                {
                    List<string> listRef = CommonMethod.GetAttributeValues(listMessage[i], "Message", "RefMessage");
                    List<string> listTitle = CommonMethod.GetAttributeValues(listMessage[i], "Message", "Title");
                    List<string> listContent = CommonMethod.GetAttributeValues(listMessage[i], "Message", "Content");
                    List<string> listFrom = CommonMethod.GetAttributeValues(listMessage[i], "Message", "FromDate");
                    List<string> listTo = CommonMethod.GetAttributeValues(listMessage[i], "Message", "ToDate");
                    List<string> listHide = CommonMethod.GetAttributeValues(listMessage[i], "Message", "Hide");
                    List<string> listDest = CommonMethod.GetAttributeValues(listMessage[i], "Message/CustomerList/TO", "RefCustomer");

                    DataRow row = dt.NewRow();
                    row["Ref"] = (listRef.Count > 0) ? listRef[0] : "";
                    row["Title"] = (listTitle.Count > 0) ? listTitle[0] : "";
                    row["Content"] = (listContent.Count > 0) ? listContent[0] : "";
                    row["From"] = (listFrom.Count > 0) ? listFrom[0].Substring(0, 10) : "";
                    row["To"] = (listTo.Count > 0) ? listTo[0].Substring(0, 10) : "";
                    row["Hide"] = (listHide.Count > 0 && listHide[0].Length > 0) ? listHide[0] : "0";
                    if(IsPro)
                        row["NbDest"] = (listDest.Count > 0) ? listDest.Count.ToString() : "tous";

                    dt.Rows.Add(row);
                }
            }
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }

    protected void rptMessageHistory_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Header)
        {
            Label lbl = (Label)e.Item.FindControl("lblNbDestHeader");
            if (lbl != null && IsPro)
            {
                lbl.Text = "Buraliste(s)";
            }
        }
        else if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hdn = (HiddenField)e.Item.FindControl("hdnHide");

            if (hdn != null && hdn.Value == "0")
            {
                Button btn = (Button)e.Item.FindControl("btnUnpublish");
                if (btn != null && ActionAllowed)
                    btn.Visible = true;
            }
        }
    }

    public event EventHandler unpublished;
    protected void btnUnpublish_Click(object sender, EventArgs e)
    {
        Button btn = (Button)sender;

        if (btn != null)
        {
            string sRefMessage = btn.CommandArgument;

            if (sRefMessage.Length > 0)
            {
                string sTitle = "";
                string sMessage = "";

                if (UnpublishMessage(sRefMessage))
                {
                    sTitle = "Information";
                    sMessage = "<span style='color:black'>Message dépublié</span>";

                    if (unpublished != null)
                        unpublished(sender, e);
                }
                else
                {
                    sTitle = "Erreur";
                    sMessage = "Une erreur est survenue.<br/>Veuillez réessayer.";
                }

                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "AlertMessage_Key", "AlertMessage('" + sTitle + "', \"" + sMessage + "\", null, null);", true);
            }
        }
    }

    protected bool UnpublishMessage(string sRefMessage)
    {
        bool bOk = false;
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_AddDelSetMessage", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                                new XElement("Message",
                                                    new XAttribute("RefMessage", sRefMessage),
                                                    new XAttribute("Hide", "1"),
                                                    new XAttribute("Action", "U"))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if (sXmlOut.Length > 0)
            {
                List<string> listUpdate = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Result", "Updated");
                if (listUpdate.Count > 0 && listUpdate[0] == "1")
                    bOk = true;
            }
        }
        catch (Exception e)
        {
        }
        finally
        {
        }

        return bOk;
    }
}