﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Xml.Linq;
using XMLMethodLibrary;
using System.Globalization;
using System.Text;

public partial class API_StaffManagement : System.Web.UI.UserControl
{
    public string IdPro { get { return (ViewState["IdPro"] != null) ? ViewState["IdPro"].ToString() : ""; } set { ViewState["IdPro"] = value; } }
    private int _iRefAgency { get { return (Session["RefAgency"] != null) ? int.Parse(Session["RefAgency"].ToString()) : 0; } set { Session["RefAgency"] = value; } }
    public int? iNbCashierList
    {
        get
        {
            if (System.Web.HttpContext.Current.Session["NbCashierList"] != null)
            {
                int i;
                if (int.TryParse(System.Web.HttpContext.Current.Session["NbCashierList"].ToString(), out i))
                    return i;
                else return null;
            }
            else return null;
        }
        set { System.Web.HttpContext.Current.Session["NbCashierList"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
            BindManagerVendorList(IdPro);
    }

    protected void BindManagerVendorList(string sIdPro)
    {
        DataTable dtManager, dtVendor;
        DataTable dtList = new DataTable();
        dtList.Columns.Add("Label");
        dtList.Columns.Add("List", typeof(DataTable));
        dtList.Columns.Add("Count", typeof(int));

        GetManagerVendorList(sIdPro, out dtManager, out dtVendor);

        iNbCashierList = dtVendor.Rows.Count;

        dtList.Rows.Add(new object[] { "Gérant(s)", dtManager, dtManager.Rows.Count });
        dtList.Rows.Add(new object[] { "Vendeur(s) (conjoint-collaborateur inclus)", dtVendor, dtVendor.Rows.Count });

        UpdateRatio(dtManager, dtVendor);

        rptList.DataSource = dtList;
        rptList.DataBind();
        upVendors.Update();
    }
    protected void GetManagerVendorList(string sIdPro, out DataTable dtManager, out DataTable dtVendor)
    {
        dtManager = new DataTable();
        dtManager.Columns.Add("Ref");
        dtManager.Columns.Add("LastName");
        dtManager.Columns.Add("FirstName");
        dtManager.Columns.Add("BirthDate");
        dtManager.Columns.Add("Email");
        dtManager.Columns.Add("DateInscr");
        dtManager.Columns.Add("DateValid");
        dtManager.Columns.Add("TrainingStatus");
        dtManager.Columns.Add("TrainingStatusLabel");
        dtManager.Columns.Add("TrainingStatusColor");

        dtVendor = dtManager.Clone();

        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK2"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_GetPdvUserStatus", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                            new XElement("PdvUser", 
                                                new XAttribute("IdPRO", sIdPro))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if(sXmlOut.Length > 0)
            {
                List<string> listPdvUser = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/PdvUser");

                for (int i = 0; i < listPdvUser.Count; i++)
                {
                    List<string> listRef = CommonMethod.GetAttributeValues(listPdvUser[i], "PdvUser", "RefUser");
                    List<string> listRefAgency = CommonMethod.GetAttributeValues(listPdvUser[i], "PdvUser", "refagency");
                    List<string> listLastName = CommonMethod.GetAttributeValues(listPdvUser[i], "PdvUser", "LastName");
                    List<string> listFirstName = CommonMethod.GetAttributeValues(listPdvUser[i], "PdvUser", "FirstName");
                    List<string> listBirthDate = CommonMethod.GetAttributeValues(listPdvUser[i], "PdvUser", "BirthDate");
                    List<string> listEmail = CommonMethod.GetAttributeValues(listPdvUser[i], "PdvUser", "Email");
                    List<string> listCreationDate = CommonMethod.GetAttributeValues(listPdvUser[i], "PdvUser", "DateCreation");
                    List<string> listFonction = CommonMethod.GetAttributeValues(listPdvUser[i], "PdvUser", "Fonction");
                    List<string> listTrainingStatus = CommonMethod.GetAttributeValues(listPdvUser[i], "PdvUser", "EtatFormation");
                    List<string> listTrainingInscrDate = CommonMethod.GetAttributeValues(listPdvUser[i], "PdvUser", "DateInsciptionFormation");
                    List<string> listTrainingValidDate = CommonMethod.GetAttributeValues(listPdvUser[i], "PdvUser", "DateDerniereFormation");

                    if (!String.IsNullOrWhiteSpace(listRef[0]))
                    {
                        try
                        {
                            _iRefAgency = int.Parse(listRefAgency[0]);
                            string sTrainingStatusLabel = "";
                            string sTrainingStatusColor = "";
                            if (listTrainingStatus[0] == "0")
                            {
                                sTrainingStatusColor = "#dd2a1b";
                                if (String.IsNullOrWhiteSpace(listTrainingValidDate[0]))
                                    sTrainingStatusLabel = "Inscrit";
                                else sTrainingStatusLabel = "Expirée (profil bloqué dans le TPE)";
                            }
                            else
                            {
                                sTrainingStatusColor = "#67cd00";
                                sTrainingStatusLabel = "Formé";

                                DateTime dateForm = new DateTime();
                                if (DateTime.TryParseExact(listTrainingValidDate[0], "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateForm))
                                {
                                    if (DateTime.Compare(DateTime.Now, dateForm.AddYears(1).AddMonths(-1)) > 0)
                                    {
                                        sTrainingStatusColor = "#dd2a1b";
                                        sTrainingStatusLabel = "A refaire";
                                    }
                                }
                            }

                            var row = new object[] { listRef[0], listLastName[0], listFirstName[0], listBirthDate[0], listEmail[0], (!String.IsNullOrWhiteSpace(listTrainingInscrDate[0])) ? listTrainingInscrDate[0] : "N.C.", (!String.IsNullOrWhiteSpace(listTrainingValidDate[0])) ? listTrainingValidDate[0] : "N.C.", listTrainingStatus[0], sTrainingStatusLabel, sTrainingStatusColor };
                            if (listFonction[0].ToString() == "MANAGER")
                                dtManager.Rows.Add(row);
                            else dtVendor.Rows.Add(row);
                        }
                        catch (Exception ex1)
                        { }
                    }
                }
            }
        }
        catch(Exception e)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }
    }

    protected void UpdateRatio(DataTable dtManager, DataTable dtVendor)
    {
        try
        {
            int iQualify = 0;
            iQualify = GetQualifyCount(dtManager) + GetQualifyCount(dtVendor);

            decimal dRatioQualify = ((decimal)iQualify / ((decimal)dtManager.Rows.Count + (decimal)dtVendor.Rows.Count)) * 100;

            if (dRatioQualify < 50)
            {
                //litRatioDesc.Text = "Trop peu de vos vendeurs sont formés. C’est dommage, vous prenez des risques et vous manquez des ventes, des dépôts et des commissions. Inscrivez tous vos vendeurs et envoyez-les à tour de rôle sur la borne Nickel pour qu’ils valident leur formation.";
                litRatioDesc.Text = "Trop peu de vos vendeurs sont formés. C’est dommage, vous prenez des risques et vous manquez des ventes, des dépôts et des commissions. Inscrivez tous vos vendeurs et dites-leur de valider leur formation en suivant les instructions reçues par email.";
            }
            else if (dRatioQualify < 100)
            {
                //litRatioDesc.Text = "Vous êtes en bonne voie, plus de la moitié de votre personnel peut vendre Compte-Nickel. Inscrivez vos derniers vendeurs et envoyez-les à tour de rôle sur la borne Nickel pour qu’ils valident leur formation.";
                litRatioDesc.Text = "Vous êtes en bonne voie, plus de la moitié de votre personnel peut vendre Compte-Nickel. Inscrivez vos derniers vendeurs et dites-leur de valider leur formation en suivant les instructions reçues par email.";
            }
            else
            {
                litRatioDesc.Text = "Excellent. Tout votre personnel est formé et apte à vendre Compte-Nickel.<br/>Réflexe 1 : un dépliant Nickel remis avec chaque produit vendu<br/>Réflexe 2 : Assurez-vous que votre Compte-Nickel distributeur est approvisionné quand les clients viennent faire des dépôts chez vous : 1000 euros de dépôts = 7,50€ de commissions pour vous.";
            }
        }
        catch(Exception e)
        { }
    }
    protected int GetQualifyCount(DataTable dt)
    {
        int iQualify = 0;

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["TrainingStatus"].ToString() == "1")
                iQualify++;
        }

        return iQualify;
    }

    protected void btnAddVendor_Click(object sender, EventArgs e)
    {
        StringBuilder sbr = new StringBuilder();
        if(String.IsNullOrWhiteSpace(txtLastName.Text))
            sbr.AppendLine("Nom : champ requis");
        if (String.IsNullOrWhiteSpace(txtFirstName.Text))
            sbr.AppendLine("Prénom : champ requis");
        if (String.IsNullOrWhiteSpace(txtBirthDate.Text))
            sbr.AppendLine("Date de naissance : champ requis");
        else
        {
            DateTime date;
            if (!DateTime.TryParseExact(txtBirthDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
            {
                sbr.AppendLine("Date de naissance : format non valide");
            }
        }

        if (String.IsNullOrWhiteSpace(txtEmail.Text))
            sbr.AppendLine("Email : champ requis");
        else
        {
            if (!tools.IsEmailValid(txtEmail.Text))
            {
                sbr.AppendLine("Email : format non valide");
            }
        }

        if (sbr.Length > 0)
        {
            AlertMessage("Erreur", sbr.ToString().Replace(Environment.NewLine, "<br/>"));
        }
        else
        {
            bool bOk = false;
            string sRegistrationCode = GetNewRegistrationCode("PAR");
            if(!String.IsNullOrWhiteSpace(sRegistrationCode))
            {
                string sRefUser;
                if(addCashier(txtLastName.Text, txtFirstName.Text, txtBirthDate.Text, txtEmail.Text, _iRefAgency, "HMB", sRegistrationCode, out sRefUser))
                {
                    string sErrorMessage;

                    if (saveCustomerInfo(sRefUser, txtEmail.Text, sRegistrationCode, out sErrorMessage))
                        bOk = true;
                    else AlertMessage("Erreur", sErrorMessage);
                }
            }

            if (!bOk)
                AlertMessage("Erreur", "Une erreur est survenue lors de l'enregistrement.");
            else
            {
                txtLastName.Text = "";
                txtFirstName.Text = "";
                txtBirthDate.Text = "";
                txtEmail.Text = "";
                BindManagerVendorList(IdPro);
                //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "DestroyAddVendorDialog();", true);
            }
        }
    }

    protected void btnDeleteVendor_Click(object sender, EventArgs e)
    {
        if (delCashier(_iRefAgency, "HMB", hdnRefVendorToDelete.Value))
            BindManagerVendorList(IdPro);
        else AlertMessage("Erreur", "Une erreur est survenue lors de la suppression.");

        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "DestroyConfirmDeleteDialog();", true);
    }

    protected bool addCashier(string sLastName, string sFirstName, string sBirthDate, string sEmail, int iRefAgency, string sToken, string sRegistrationCode, out string sRefUser)
    {
        bool isOK = false;
        string sXmlIn = new XElement("ALL_XML_IN",
                            new XElement("U",
                                new XAttribute("LASTNAME", sLastName),
                                new XAttribute("FIRSTNAME", sFirstName),
                                new XAttribute("DATEOFBIRTH", sBirthDate),
                                new XAttribute("EMAIL", sEmail),
                                new XAttribute("REGISTRATIONCODE", sRegistrationCode),
                                new XAttribute("REFAGENCY", iRefAgency.ToString()),
                                new XAttribute("ACTION", "A"),
                                new XAttribute("ROLE", "CASHIER"),
                                new XAttribute("CASHIERTOKEN", sToken))).ToString();

        string sErrorMessage = "";
        isOK = addDelCashier(sXmlIn, out sRefUser, out sErrorMessage);

        return isOK;
    }

    protected bool delCashier(int iRefAgency, string sToken, string sUidCode)
    {
        bool isOK = false;
        string sXmlIn = new XElement("ALL_XML_IN",
                            new XElement("U",
                                new XAttribute("REFUSER", sUidCode),
                                new XAttribute("REFAGENCY", iRefAgency.ToString()),
                                new XAttribute("ACTION", "D"),
                                new XAttribute("ROLE", "CASHIER"),
                                new XAttribute("CASHIERTOKEN", sToken))).ToString();

        string sErrorMessage = "", sNotUsed;
        isOK = addDelCashier(sXmlIn, out sNotUsed, out sErrorMessage);

        return isOK;
    }

    protected bool addDelCashier(string sXml, out string sRefUser, out string sErrorMessage)
    {
        bool isOK = false;
        sErrorMessage = "";
        sRefUser = "";

        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["PARTNER"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("Partner.P_AddDelCashierManagerToAgency", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@RC", SqlDbType.Int);
            cmd.Parameters.Add("@XMLIn", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@XMLOut", SqlDbType.VarChar, -1);

            cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;
            cmd.Parameters["@XMLIn"].Value = sXml;
            cmd.Parameters["@XMLOut"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            //Other.LogToFile(cmd.Parameters["@RC"].Value.ToString().Trim() + Environment.NewLine + cmd.Parameters["@XMLOut"].Value.ToString(), "Add/Del cashier");

            if (cmd.Parameters["@RC"].Value.ToString().Trim() == "0")
            {
                List<string> AttValues = XMLMethodLibrary.CommonMethod.GetAttributeValues(cmd.Parameters["@XMLOut"].Value.ToString(), "ALL_XML_OUT/U", "RC");
                if (AttValues.Count > 0 && AttValues[0].Trim() == "0")
                {
                    isOK = true;
                    AttValues = XMLMethodLibrary.CommonMethod.GetAttributeValues(cmd.Parameters["@XMLOut"].Value.ToString(), "ALL_XML_OUT/U", "RefUID");
                    if (AttValues.Count > 0 && AttValues[0].Trim().Length > 0)
                        sRefUser = AttValues[0].Trim();
                }
                else
                {
                    isOK = false;
                    //List<string> AttErrorValues = XMLMethodLibrary.CommonMethod.GetAttributeValues(cmd.Parameters["@XMLOut"].Value.ToString(), "U", "RC");
                    sErrorMessage = "Une erreur est survenue.";
                }
            }
            else isOK = false;


        }
        catch (Exception e)
        {
            isOK = false;
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return isOK;
    }

    protected bool saveCustomerInfo(string sRefUser, string sEmail, string sRegistrationCode, out string sErrorMessage)
    {
        bool isOK = false;

        sErrorMessage = "";
        string sXml = new XElement("ALL_XML_IN",
                        new XElement("User",
                            new XAttribute("RefUser", sRefUser),
                            new XAttribute("Email", sEmail),
                            new XAttribute("RegistrationCode", sRegistrationCode),
                            new XAttribute("InstituteRegistered", "1"))).ToString();

        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("[PDV].[P_SetUserInformation]", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@RC", SqlDbType.Int);
            cmd.Parameters.Add("@IN", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);

            cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;
            cmd.Parameters["@IN"].Value = sXml;
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            if (cmd.Parameters["@RC"].Value.ToString().Trim() == "0")
            {
                List<string> AttRC = XMLMethodLibrary.CommonMethod.GetAttributeValues(cmd.Parameters["@OUT"].Value.ToString(), "ALL_XML_OUT/Result", "RC");
                List<string> AttUpdated = XMLMethodLibrary.CommonMethod.GetAttributeValues(cmd.Parameters["@OUT"].Value.ToString(), "ALL_XML_OUT/Result", "Updated");
                if (AttRC.Count > 0 && AttUpdated.Count > 0 && AttRC[0].Trim() == "0" && AttUpdated[0].Trim() != "0")
                    isOK = true;
                else
                {
                    isOK = false;
                    sErrorMessage = "Une erreur est survenue.";
                }
            }
            else isOK = false;


        }
        catch (Exception e)
        {
            isOK = false;
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }


        return isOK;
    }

    protected string GetNewRegistrationCode(string sServiceTag)
    {
        string sRegistrationCode = "";
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["PARTNER"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("Registration.P_AddTransaction", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@ServiceTAG", SqlDbType.VarChar, 10);
            cmd.Parameters.Add("@NewRegistrationCode", SqlDbType.VarChar, 100);
            cmd.Parameters.Add("@RefTransaction", SqlDbType.Int);

            cmd.Parameters["@ServiceTAG"].Value = sServiceTag;
            cmd.Parameters["@NewRegistrationCode"].Direction = ParameterDirection.Output;
            cmd.Parameters["@RefTransaction"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            sRegistrationCode = cmd.Parameters["@NewRegistrationCode"].Value.ToString();
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return sRegistrationCode;
    }

    protected void btnSendTrainingNotification_Click(object sender, EventArgs e)
    {
        if (!String.IsNullOrWhiteSpace(hdnRefVendorNotification.Value))
        {
            if (!SendTrainingNotification(hdnRefVendorNotification.Value))
                AlertMessage("Erreur", "Une erreur est survenue lors de l'envoi.");
            else AlertMessage("Email envoyé", "Votre vendeur va recevoir un email avec les insctructions pour effectuer sa formation.");
        }
    }
    protected bool SendTrainingNotification(string sRefUID)
    {
        bool bSent = false;
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("[PDV].[P_Send_LCB_FT_Notification]", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 1000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 1000);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN", new XElement("PdvUser", new XAttribute("RefUser", sRefUID))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if(sXmlOut.Length > 0)
            {
                List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/PdvUser", "RC");

                if (listRC.Count > 0 && listRC[0] == "0")
                    bSent = true;
            }
        }
        catch(Exception e)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return bSent;
    }

    protected void AlertMessage(string sTitle, string sMessage)
    {
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "ToggleDialogLoading();ShowDialogMessage(\"" + sTitle + "\", \"" + sMessage + "\");", true);
    }
}