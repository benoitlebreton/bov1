﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DatePicker.ascx.cs" Inherits="MG_API_DatePicker" %>

<script type="text/javascript">
    function onChangeDate() {
        //alert("onchange");
        //alert($('#<%=hfGetDateByPostback.ClientID %>').val().trim() + "," + $('#<%=ddlDay.ClientID %>').val() + "/" + $('#<%=ddlMonth.ClientID %>').val() + "/" + $('#<%=ddlYear.ClientID %>').val());
        if ($('#<%=hfGetDateByPostback.ClientID %>').val().trim().toLowerCase() == "true" &&
            $('#<%=ddlDay.ClientID %> option:selected').val() != "" && $('#<%=ddlMonth.ClientID %> option:selected').val() != "" && $('#<%=ddlYear.ClientID %> option:selected').val() != "") {
            //alert($('#<%=ddlDay.ClientID %>').val() + "/" + $('#<%=ddlMonth.ClientID %>').val() + "/" + $('#<%=ddlYear.ClientID %>').val());
            __doPostBack("MainContent_upRegistrationSearch", $('#<%=hfPostbackName.ClientID %>').val() + ";" + $('#<%=ddlDay.ClientID %>').val() + "/" + $('#<%=ddlMonth.ClientID %>').val() + "/" + $('#<%=ddlYear.ClientID %>').val());
        }
        return false;
    }
</script>

<div>
    <div style="display: inline-block">
        <asp:DropDownList ID="ddlDay" runat="server" onblur="onChangeDate()" onchange="onChangeDate()">
        </asp:DropDownList>
    </div>
    <div style="display: inline-block">
        <asp:DropDownList ID="ddlMonth" runat="server" onblur="onChangeDate()" onchange="onChangeDate()">
        </asp:DropDownList>
    </div>
    <div style="display: inline-block">
        <asp:DropDownList ID="ddlYear" runat="server" onblur="onChangeDate()" onchange="onChangeDate()">
        </asp:DropDownList>
    </div>

    <asp:HiddenField ID="hfGetDateByPostback" runat="server" Value="false" />
    <asp:HiddenField ID="hfPostbackName" runat="server" Value="" />
</div>