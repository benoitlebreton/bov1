﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using XMLMethodLibrary;

public partial class API_AccountClosingFee : System.Web.UI.UserControl
{
    protected DataTable _dtAvailableFees { get { return (ViewState["dtAvailableFees"] != null) ? (DataTable)ViewState["dtAvailableFees"] : null; } set { ViewState["dtAvailableFees"] = value; } }
    protected DataTable _dtFees { get { return (ViewState["dtFees"] != null) ? (DataTable)ViewState["dtFees"] : null; } set { ViewState["dtFees"] = value; } }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindFeesChoice();

            //Page.Header.Controls.Add(new System.Web.UI.LiteralControl("<link rel=\"stylesheet\" type=\"text/css\" href=\"" + ResolveUrl("~/Styles/AccountClosingControls.css") + "\" />"));
        }
    }

    public void SetFees(DataTable dt)
    {
        _dtFees = dt;
        BindFees();
    }

    protected void BindFees()
    {
        rptFees.DataSource = _dtFees;
        rptFees.DataBind();
        upFees.Update();
    }

    protected void BindFeesChoice()
    {
        if (_dtAvailableFees == null)
            _dtAvailableFees = GetFees();

        DataTable dtTmp = _dtAvailableFees.Clone();
        dtTmp.Rows.Add(new object[] { "", "", "" });

        for (int i = 0; i < _dtAvailableFees.Rows.Count; i++)
        {
            bool bExists = false;
            if (_dtFees != null)
                for (int j = 0; j < _dtFees.Rows.Count; j++)
                {
                    if (_dtFees.Rows[j]["Tag"].ToString() == _dtAvailableFees.Rows[i]["Tag"].ToString())
                    {
                        bExists = true;
                        break;
                    }
                }

            // REMOVE DUPLICATES
            //if (!bExists)
            dtTmp.Rows.Add(_dtAvailableFees.Rows[i].ItemArray);
        }

        ddlFee.Items.Clear();
        ddlFee.DataSource = dtTmp;
        ddlFee.DataBind();
    }
    protected DataTable GetFees()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("Tag");
        dt.Columns.Add("Label");
        dt.Columns.Add("Amount");

        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_GetFeesList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@ReturnSelect", SqlDbType.Bit);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN", new XElement("FEES", new XAttribute("Kind", "CIRC"))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
            cmd.Parameters["@ReturnSelect"].Value = 0;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if (sXmlOut.Length > 0)
            {
                List<string> listFee = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/FEES");

                for(int i=0;i<listFee.Count;i++)
                {
                    List<string> listTag = CommonMethod.GetAttributeValues(listFee[i], "FEES", "TAGFee");
                    List<string> listDesc = CommonMethod.GetAttributeValues(listFee[i], "FEES", "FeeDescription");
                    List<string> listAmount = CommonMethod.GetAttributeValues(listFee[i], "FEES", "FeeAmount");

                    dt.Rows.Add(new object[] { listTag[0].ToString(), listDesc[0].ToString(), listAmount[0].ToString() });
                }
            }
        }
        catch (Exception e)
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }

    public event EventHandler FeeAddedRemoved;
    protected void btnAdd_Click(object sender, ImageClickEventArgs e)
    {
        string sErrorMessage = "";

        if (CheckForm(out sErrorMessage))
        {
            DataTable dt = _dtFees;

            if (dt == null)
            {
                dt = new DataTable();
                dt.Columns.Add("Tag");
                dt.Columns.Add("Label");
                dt.Columns.Add("Amount");
            }

            decimal dAmount = 0;
            if (Decimal.TryParse(txtFeeAmount.Text.Replace('.', ','), NumberStyles.Currency, new CultureInfo("fr-FR"), out dAmount))
            {
                dAmount = decimal.Round(dAmount, 2, MidpointRounding.AwayFromZero);

                dt.Rows.Add(new object[] { ddlFee.SelectedValue, ddlFee.SelectedItem.Text, dAmount.ToString("0.00") });
                _dtFees = dt;
                BindFees();

                BindFeesChoice();
            }

            ddlFee.SelectedIndex = 0;
            txtFeeAmount.Text = "";

            if (FeeAddedRemoved != null)
                FeeAddedRemoved(this, EventArgs.Empty);
        }
        else AlertMessage("Erreur", sErrorMessage);
    }

    public bool CheckForm(out string sErrorMessage)
    {
        bool bOk = true;
        sErrorMessage = "";
        StringBuilder sbErrorMessage = new StringBuilder();

        if(String.IsNullOrWhiteSpace(ddlFee.SelectedValue))
            sbErrorMessage.AppendLine("Type de frais : champ requis");

        if (String.IsNullOrWhiteSpace(txtFeeAmount.Text))
            sbErrorMessage.AppendLine("Montant : champ requis");
        else
        {
            decimal dAmount = -1;
            if (!Decimal.TryParse(txtFeeAmount.Text.Replace('.', ','), NumberStyles.Currency, new CultureInfo("fr-FR"), out dAmount) || dAmount <= 0)
                sbErrorMessage.AppendLine("Montant : valeur incorrecte");
            else
            {
                dAmount = decimal.Round(dAmount, 2, MidpointRounding.AwayFromZero);

                for (int i = 0; i < _dtAvailableFees.Rows.Count; i++)
                {
                    if (_dtAvailableFees.Rows[i]["Tag"].ToString() == ddlFee.SelectedValue)
                    {
                        decimal dMaxAmount = 0;
                        if (Decimal.TryParse(_dtAvailableFees.Rows[i]["Amount"].ToString().Replace('.', ','), NumberStyles.Currency, new CultureInfo("fr-FR"), out dMaxAmount))
                        {
                            if (dAmount > dMaxAmount)
                                sbErrorMessage.AppendLine("Montant : valeur trop élevée (max. : " + dMaxAmount.ToString("0.00") + " &euro;).");
                        }
                    }
                }
            }
        }

        sErrorMessage = sbErrorMessage.Replace(Environment.NewLine, "<br />").ToString();

        if (sErrorMessage.Length > 0)
            bOk = false;

        return bOk;
    }

    protected void AlertMessage(string sTitle, string sMessage)
    {
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('" + sTitle + "', \"" + sMessage + "\");", true);
    }

    protected void btnRemove_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton btn = (ImageButton)sender;

        HiddenField hdn = (HiddenField)btn.Parent.FindControl("hdnTag");
        Literal lit = (Literal)btn.Parent.FindControl("litLabel");

        DataTable dt = _dtFees;

        bool bExist = false;
        int iRow = 0;
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["Tag"].ToString() == hdn.Value)
            {
                bExist = true;
                iRow = i;
                break;
            }
        }

        if (bExist)
        {
            dt.Rows.Remove(dt.Rows[iRow]);

            _dtFees = dt;
            BindFees();

            BindFeesChoice();

            if (FeeAddedRemoved != null)
                FeeAddedRemoved(this, EventArgs.Empty);
        }
    }

    protected void ReorderAlphabetized(DropDownList ddl)
    {
        List<ListItem> listCopy = new List<ListItem>();
        foreach (ListItem item in ddl.Items)
            listCopy.Add(item);
        ddl.Items.Clear();
        foreach (ListItem item in listCopy.OrderBy(item => item.Text))
            ddl.Items.Add(item);
    }

    public XElement GetDataToXML()
    {
        XElement xml = null;

        if (_dtFees != null && _dtFees.Rows.Count > 0)
        {
            xml = new XElement("FEES");

            for (int i = 0; i < _dtFees.Rows.Count; i++)
            {
                xml.Add(new XElement("FEE",
                        new XAttribute("TAGFee", _dtFees.Rows[i]["Tag"].ToString()),
                        new XAttribute("FeeAmount", _dtFees.Rows[i]["Amount"].ToString())));
            }
        }

        string sErrorMessage;
        if(CheckForm(out sErrorMessage))
        {
            decimal dAmount;
            if (Decimal.TryParse(txtFeeAmount.Text.Replace('.', ','), NumberStyles.Currency, new CultureInfo("fr-FR"), out dAmount))
            {
                dAmount = decimal.Round(dAmount, 2, MidpointRounding.AwayFromZero);

                if (xml == null)
                    xml = new XElement("FEES");

                xml.Add(new XElement("FEE",
                        new XAttribute("TAGFee", ddlFee.SelectedValue),
                        new XAttribute("FeeAmount", dAmount)));
            }
        }

        return xml;
    }

    public decimal GetTotalFees()
    {
        decimal dTotal = 0;
        decimal dTmp;

        if (_dtFees != null)
        {
            for (int i = 0; i < _dtFees.Rows.Count; i++)
            {
                if (Decimal.TryParse(_dtFees.Rows[i]["Amount"].ToString().Replace(".", ","), NumberStyles.Currency, new CultureInfo("fr-FR"), out dTmp))
                    dTotal += dTmp;
            }
        }

        string sErrorMessage;
        if (CheckForm(out sErrorMessage))
        {
            if (Decimal.TryParse(txtFeeAmount.Text.Replace(".", ","), NumberStyles.Currency, new CultureInfo("fr-FR"), out dTmp))
                dTotal += dTmp;
        }

        return dTotal;
    }

    protected string GetFeeAmount(string sTagFee)
    {
        string sAmount = "";

        if (_dtAvailableFees != null)
        {
            for (int i = 0; i < _dtAvailableFees.Rows.Count; i++)
            {
                if (_dtAvailableFees.Rows[i]["Tag"].ToString() == sTagFee)
                {
                    sAmount = _dtAvailableFees.Rows[i]["Amount"].ToString();
                    break;
                }
            }
        }

        return sAmount;
    }

    protected void ddlFee_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtFeeAmount.Text = GetFeeAmount(ddlFee.SelectedValue);
    }
}