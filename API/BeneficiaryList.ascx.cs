﻿using System;
using System.Data;
using System.Linq;

public partial class API_BeneficiaryList : System.Web.UI.UserControl
{
    public int iRefCustomer
    {
        get
        {
            int _iRefCustomer = 0;
            int.TryParse((ViewState["iRefCustomer"] != null) ? ViewState["iRefCustomer"].ToString() : "0", out _iRefCustomer);
            return _iRefCustomer;
        }
        set { ViewState["iRefCustomer"] = value; }
    } 

    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
            BindBeneficiaries();
    }

    protected void BindBeneficiaries()
    {
        var beneficiarylist = BeneficiaryService.getBeneficiaryList(iRefCustomer, true);
        rBeneficiaryList.DataSource = BeneficiaryService.ConvertBeneficiaryListToBeneficiaryViewList(beneficiarylist);
        
        rBeneficiaryList.DataBind();

        if (beneficiarylist.Any() )
        {
            panelBeneficiaryList.Visible = true;
            panelBeneficiaryList_Empty.Visible = false;
        }
        else
        {
            panelBeneficiaryList.Visible = false;
            panelBeneficiaryList_Empty.Visible = true;
        }
    }
}