﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MG_API_DatePicker : System.Web.UI.UserControl
{
    private string _sYearType = "";
    private string _sDate;
    private bool _bGetDateByPostback = false;
    private string _sPostbackName = "DatePicker";

    public bool bGetDateByPostback
    {
        set { _bGetDateByPostback = value; }
    }

    public string sPostbackName
    {
        set { _sPostbackName = value; }
    }

    public string sYearType
    {
        set { _sYearType = value; }
    }

    public string sSelectedDate
    {
        get
        {
            string sDate = "";

            if (ddlDay.SelectedValue.Length > 0 && ddlMonth.SelectedValue.Length > 0 && ddlYear.SelectedValue.Length > 0)
            {
                DateTime dtDate;
                if (DateTime.TryParse(ddlDay.SelectedValue + "/" + ddlMonth.SelectedValue + "/" + ddlYear.SelectedValue, out dtDate))
                {
                    sDate = dtDate.ToString("dd/MM/yyyy");
                }
            }

            return sDate;
        }

        set
        {
            _sDate = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            InitializeControl();
    }

    protected void InitializeControl()
    {
        hfGetDateByPostback.Value = _bGetDateByPostback.ToString();
        hfPostbackName.Value = _sPostbackName;

        ddlDay.Items.Add(new ListItem("Jour", ""));
        for (int i = 1; i <= 31; i++)
        {
            ddlDay.Items.Add(new ListItem(NumberToString(i), NumberToString(i)));
        }

        ddlMonth.Items.Add(new ListItem("Mois", ""));
        for (int i = 1; i <= 12; i++)
        {
            ddlMonth.Items.Add(new ListItem(NumberToString(i), NumberToString(i)));
        }

        ddlYear.Items.Add(new ListItem("Année", ""));
        int iFrom = DateTime.Now.Year;
        int iTo = DateTime.Now.Year;

        switch (_sYearType.ToLower())
        {
            case "past":
                iFrom = DateTime.Now.Year - 150;
                break;
            case "future":
                iTo = DateTime.Now.Year + 150;
                break;
            case "both":
                iFrom = DateTime.Now.Year - 150;
                iTo = DateTime.Now.Year + 150;
                break;
            default:
                iFrom = DateTime.Now.Year - 150;
                iTo = DateTime.Now.Year + 150;
                break;
        }

        for (int i = iFrom; i <= iTo; i++)
        {
            ddlYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }

        if (_sDate != null)
        {
            string[] tsDate = _sDate.Split('/');

            if (ddlDay.Items.Contains(new ListItem(tsDate[0], tsDate[0])))
            {
                ddlDay.Items.FindByValue(tsDate[0]).Selected = true;
            }
            if (ddlMonth.Items.Contains(new ListItem(tsDate[1], tsDate[1])))
            {
                ddlMonth.Items.FindByValue(tsDate[1]).Selected = true;
            }
            if (ddlYear.Items.Contains(new ListItem(tsDate[2], tsDate[2])))
            {
                ddlYear.Items.FindByValue(tsDate[2]).Selected = true;
            }
        }
    }

    protected string NumberToString(int iNum)
    {
        if (iNum < 10)
            return "0" + iNum.ToString();
        else return iNum.ToString();
    }
}