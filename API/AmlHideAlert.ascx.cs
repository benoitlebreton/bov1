﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class API_AmlHideAlert : System.Web.UI.UserControl
{
    private int _iRefCustomer
    {
        get { return int.Parse(ViewState["RefCustomer"].ToString()); }
        set { ViewState["RefCustomer"] = value; }
    }
    public int RefCustomer
    {
        get { return _iRefCustomer; }
        set { _iRefCustomer = value; }
    }
    private bool _bActionAllowed
    {
        get { return (ViewState["AmlHideAlertActionAllowed"] != null) ? bool.Parse(ViewState["AmlHideAlertActionAllowed"].ToString()) : false; }
        set { ViewState["AmlHideAlertActionAllowed"] = value; }
    }
    public bool ActionAllowed
    {
        get { return _bActionAllowed; }
        set { _bActionAllowed = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ddlTagAlertList.DataSource = AML.getAlertTagList();
            ddlTagAlertList.DataBind();
            Bind();

            if (!_bActionAllowed)
                panelHideAlertType.Visible = false;
        }

        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "initAmlHideAlert();", true);
    }

    public void Bind() 
    {
        
        authentication auth = authentication.GetCurrent();

        panelHiddenAlertList.Visible = true;
        panelHiddenAlertListEmpty.Visible = false;
        DataTable dt = AML.getClientHiddenAlert(auth.sToken, _iRefCustomer);
        if (dt.Columns.Contains("Subject") && dt.Columns.Contains("EndDate") && dt.Columns.Contains("StartDate") && dt.Columns.Contains("ByUser"))
        {
            rptHiddenAlertList.DataSource = dt;
            rptHiddenAlertList.DataBind();
        }

        if (rptHiddenAlertList.Items.Count == 0)
        {
            panelHiddenAlertList.Visible = false;
            panelHiddenAlertListEmpty.Visible = true;
        }
    }

    protected void btnConfirmHideAlert_Click(object sender, EventArgs e)
    {
        string sTagAlert = ddlTagAlertList.SelectedValue;
        authentication auth = authentication.GetCurrent();
        if(txtEndHideAlert.Text.Trim().Length == 10)
            AML.hideClientAlert(auth.sToken, _iRefCustomer, sTagAlert, txtEndHideAlert.Text);
        Bind();
    }
}