﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ClientRelation.ascx.cs" Inherits="API_ClientRelation" %>

<div class="client-relation">
    <asp:Image ID="imgRelation" runat="server" ImageUrl="~/Styles/Img/icons8-user-groups-50.png" Height="40" CssClass="clickable image-tooltip" onclick="ShowPanelRelation()" />
    <asp:Panel ID="panelRelation" runat="server" CssClass="relation-container">
        <asp:UpdatePanel ID="upClientRelation" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="button-container">
                    <asp:Button ID="btnEdit" runat="server" Text="Modifier" CssClass="MiniButton" OnClick="btnEdit_Click" />
                    <asp:Button ID="btnCancel" runat="server" Text="Annuler" CssClass="MiniButton" OnClick="btnCancel_Click" Visible="false" />
                    <asp:Button ID="btnValidate" runat="server" Text="Valider" CssClass="MiniButton" OnClick="btnValidate_Click" Visible="false" />
                </div>
                <div class="relation-row">
                    <asp:Panel ID="panelRelationInner" runat="server">
                        <asp:Repeater ID="rptRelation" runat="server" OnItemDataBound="rptRelation_ItemDataBound">
                            <HeaderTemplate>
                                <table>
                                    <thead>
                                    <tr>
                                        <th id="divStatusH" runat="server" visible='<%#!IsPopup %>'>Statut</th>
                                        <th>Type relation</th>
                                        <th>N° de compte</th>
                                        <th>Nom</th>
                                        <th>Prénom</th>
                                        <th>Age</th>
                                        <th>Sexe</th>
                                        <th>Profession</th>
                                        <th>Compte professionnel</th>
                                        <th>Un même nom</th>
                                        <th>Même adresse</th>
                                        <th>Montant des virements émis</th>
                                        <th>Nombre des virements émis</th>
                                        <th>Nombre des virements émis avec libellé professionnel</th>
                                        <th>Montant des virements reçus</th>
                                        <th>Nombre des virements reçus</th>
                                        <th>Nombre des virements reçus avec libellé professionnel</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                            </HeaderTemplate>
                            <ItemTemplate>
                                    <asp:HiddenField ID="hdnRefRelation" runat="server" Value='<%#Eval("RefRelation") %>' />
                                <asp:HiddenField ID="hdnRefCustomer" runat="server" Value='<%#Eval("RefCustomer") %>' />
                                    <asp:HiddenField ID="hdnRefRelationType" runat="server" Value='<%#Eval("RefRelationType") %>' />
                                    <tr class='<%#Container.ItemIndex % 2 == 0 ? "row-even" : "row-odd" %>'>
                                        <td id="divStatusV" runat="server" visible='<%#!IsPopup %>'>
                                            <asp:Literal ID="lblStatus" runat="server" Text='<%# Eval("StatusLabel") %>' Visible='<%#!IsEditable || Eval("RefStatus").ToString() != "3" %>'></asp:Literal>
                                            <asp:CheckBox ID="ckbCheck" runat="server" Text="Valider" Visible='<%#IsEditable && Eval("RefStatus").ToString() == "3" %>' Checked='<%#Eval("RefStatus").ToString() != "3" %>' />
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlRelationTypes" runat="server" AutoPostBack="false" AppendDataBoundItems="true" DataValueField="RefRelationType" DataTextField="RelationTypeLabel" DataSource='<%# Eval("RelationTypes") %>' Visible='<%#IsEditable%>'>
                                            </asp:DropDownList>
                                            <asp:Literal ID="litRelationTypeLabel" runat="server" Visible='<%#!IsEditable%>' Text='<%#Eval("RelationTypeLabel") %>'></asp:Literal>
                                        </td>
                                        <td><a href='ClientDetails.aspx?ref=<%#Eval("RefCustomer") %>'><%# Eval("BankAccountNumber") %></a></td>
                                        <td><%# Eval("LastName") %></td>
                                        <td><%# Eval("FirstName") %></td>
                                        <td><%# Eval("Age") %></td>
                                        <td><%# Eval("Sex") %></td>
                                        <td><%# Eval("Profession") %></td>
                                        <td><%# Eval("IsProAccount") %></td>
                                        <td><%# Eval("IsSameName") %></td>
                                        <td><%# Eval("IsSameAddress") %></td>
                                        <td><%# Eval("TransferOutAmount") %></td>
                                        <td><%# Eval("TransferOutCount") %></td>
                                        <td><%# Eval("TransferProOut") %></td>
                                        <td><%# Eval("TransferInAmount") %></td>
                                        <td><%# Eval("TransferInCount") %></td>
                                        <td><%# Eval("TransferProIn") %></td>
                                    </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                    </tbody>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </asp:Panel>
                    <asp:Panel ID="panelNoRelation" runat="server" CssClass="no-relation">
                        Aucune relation
                    </asp:Panel>
                </div>

                <div class="relation-period">
                    <asp:RadioButtonList ID="rblPeriodFlow" runat="server" RepeatDirection="Horizontal" CssClass="radioButtonList" AutoPostBack="true" OnSelectedIndexChanged="rblPeriodFlow_SelectedIndexChanged">
                        <asp:ListItem Value="1">1 mois</asp:ListItem>
                        <asp:ListItem Value="3">3 mois</asp:ListItem>
                        <asp:ListItem Value="6">6 mois</asp:ListItem>
                        <asp:ListItem Value="12">1 an</asp:ListItem>
                        <asp:ListItem Value="-1" Selected="True">Depuis ouverture</asp:ListItem>
                    </asp:RadioButtonList>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="rblPeriodFlow" EventName="SelectedIndexChanged" />
            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>
</div>