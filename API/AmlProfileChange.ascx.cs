﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class API_AmlProfileChange : System.Web.UI.UserControl
{
    public string SelectedProfile { get { return hdnSelectedProfile.Value; } set { hdnSelectedProfile.Value = value; lblSelectedProfil.Text = value; } }
    public bool ReadOnly { set { if(value) btnProfileChoice.Visible = false; } }

    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            BindProfile();

            Page.Header.Controls.Add(new System.Web.UI.LiteralControl("<link rel=\"stylesheet\" type=\"text/css\" href=\"" + ResolveUrl("~/Styles/AmlProfileChange.css") + "\" />"));
            ScriptManager.RegisterClientScriptInclude(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "Scripts/AmlProfileChange.js");
        }
    }

    protected void BindProfile()
    {
        DataTable dt = AML.getProfiles();
        rptProfileList.DataSource = dt;
        rptProfileList.DataBind();
    }
}