﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AmlProfileChange.ascx.cs" Inherits="API_AmlProfileChange" %>

<div class="label">
    Profil
</div>
<div>
    <asp:Label ID="lblSelectedProfil" runat="server" class="profile-label">&nbsp;</asp:Label><asp:Button ID="btnProfileChoice" runat="server" Text="Choisir un profil" CssClass="button" OnClientClick="ShowProfileSelector(); return false;" style="border-top-left-radius: 0;border-bottom-left-radius: 0;" />
    <asp:HiddenField ID="hdnSelectedProfile" runat="server" />
</div>

<div id="popup-profile-selector" style="display: none">
    <div style="padding-bottom: 10px;">
        <asp:Repeater ID="rptProfileList" runat="server">
            <HeaderTemplate>
                <table cellspacing="0">
                    <tr>
                        <th>Type</th>
                        <th>Risque</th>
                        <th>Description</th>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr onclick="SelectClientProfile('<%# Eval("Profil") %>', '<%=lblSelectedProfil.ClientID %>', '<%=hdnSelectedProfile.ClientID %>');"
                    onmouseover="this.style.cursor = 'pointer';this.style.backgroundColor = '#ff9966'; this.style.color = '#ffffff';"
                    onmouseout="this.style.cursor = 'auto';this.style.backgroundColor = '#fff'; this.style.color = '#344b56';">
                    <td style="padding-left: 10px; vertical-align: middle; padding-top: 5px;">
                        <%# Eval("Profil") %>
                    </td>
                    <td style="padding-left: 10px; vertical-align: middle; padding-top: 5px;">
                        <%# Eval("NiveauRisque") %>
                    </td>
                    <td style="padding-left: 10px; vertical-align: middle">
                        <div class="trimmer" style="width: 400px;">
                            <asp:Label ID="lblProfileDescription" runat="server" ToolTip='<%# Eval("Description") %>' CssClass="tooltip"><%# Eval("Description") %></asp:Label>
                        </div>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </div>
</div>