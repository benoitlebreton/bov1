﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AmlHideAlert.ascx.cs" Inherits="API_AmlHideAlert" %>
<script type="text/javascript">
    function initAmlHideAlert() {
        $('#<%=txtEndHideAlert.ClientID %>').datepicker($.datepicker.regional['fr'] = {
            closeText: 'Fermer',
            prevText: 'Précédent',
            nextText: 'Suivant',
            currentText: 'Courant',
            monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
            monthNamesShort: ['Jan', 'Fév', 'Mar', 'Avr', 'Mai', 'Jun', 'Jul', 'Aoû', 'Sep', 'Oct', 'Nov', 'Déc'],
            dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
            dayNamesShort: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],
            dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
            weekHeader: 'Sm',
            dateFormat: 'dd/mm/yy',
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: '',
            minDate: +1,
        });

        $('#popup_AlertHide').dialog({
            autoOpen: false,
            resizable: false,
            draggable: false,
            modal: true,
            minWidth: 500,
            title: "Ajout période de gel"
        });
        $('#popup_AlertHide').parent().appendTo(jQuery("form:first"));

        $('#<%=txtEndHideAlert.ClientID%>').watermark("JJ/MM/AAAA");
    }

    function checkAlertType() {
        if ($('#<%= ddlTagAlertList.ClientID%>').val().trim().length > 0 && $('#<%= txtEndHideAlert.ClientID%>').val().trim().length == 10) {
            $('#divAlertHideError').html('');
            $('#divAlertHideConfirm').show();
            $('#lblEndHideAlert').html($('#<%=txtEndHideAlert.ClientID%>').val());
            $('#lblTagAlertToHide').html($('#<%=ddlTagAlertList.ClientID%> :selected').text());
            $('#popup_AlertHide').dialog("open");
        }
        else {
            var sError = "<ul>";
            if ($('#<%=ddlTagAlertList.ClientID%>').val().trim().length == 0)
                sError += "<li>Type d'alerte</li>";
            if ($('#<%=txtEndHideAlert.ClientID%>').val().trim().length != 10)
                sError += "<li>Date de fin</li>";
            sError += "</ul>";

            $('#divAlertHideConfirm').hide();
            $('#divAlertHideError').html('Veuillez corriger le(s) champ(s) suivant(s) : '+sError);
            $('#popup_AlertHide').dialog("open");
        }
    }

    function validateAddHideAlert() {
        //__doPostBack("upAmlClientHideAlert", "AddHideAlert;" + $('#<%=txtEndHideAlert.ClientID%>').val()+";"+$('#<%=ddlTagAlertList.ClientID%>').val());
        $('#<%=btnConfirmHideAlert.ClientID%>').click();
        $('#popup_AlertHide').dialog('close');
    }
</script>

<asp:UpdatePanel ID="upAmlClientHideAlert" runat="server" UpdateMode="Conditional" ChildrenAsTrigger="True">
    <ContentTemplate>
        <div style="margin-top:10px">
            <h3 style="font-size:1em;margin-top:0">Période(s) de gel</h3>
            <div>
                <asp:Panel ID="panelHiddenAlertList" runat="server">
                <asp:Repeater ID="rptHiddenAlertList" runat="server">
                    <HeaderTemplate>
                        <div style="margin-top:5px;">
                        <table style="width:100%;border-collapse:collapse;" class="tableHistory">
                            <tr>
                                <th>Type alerte</th>
                                <th>Jusqu'au</th>
                                <th>Effectué le</th>
                                <th>Par</th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                            <tr>
                                <td>
                                    <%#Eval("Subject") %>
                                </td>
                                <td style="text-align:center">
                                    <%# tools.getFormattedShortDate(Eval("EndDate").ToString(), '/') %>
                                </td>
                                <td style="text-align:center">
                                    <%# tools.getFormattedDate(Eval("StartDate").ToString()) %>
                                </td>
                                <td style="text-align:center">
                                    <%#Eval("ByUser") %>
                                </td>
                            </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                        </div>
                    </FooterTemplate>
                </asp:Repeater>
                </asp:Panel>
                <asp:Panel ID="panelHiddenAlertListEmpty" runat="server" Visible="false" style="padding-left:5px;">Aucune alerte gelée</asp:Panel>
            </div>
            <asp:Panel ID="panelHideAlertType" runat="server" style="margin-top:10px;width:100%" CssClass="table">
                <div class="row">
                    <div class="cell" style="width:370px;">
                        <asp:DropDownList ID="ddlTagAlertList" runat="server" DataTextField="Subject" DataValueField="TagAlerte" 
                            AutoPostBack="false" AppendDataBoundItems="true" style="width:350px;">
                            <asp:ListItem Value="">- Sélectionner un type alerte -</asp:ListItem>
                        </asp:DropDownList>
                    </div> 
                    <div class="cell" style="width:90px">
                        jusqu'au
                    </div>
                    <div class="cell" style="width:120px;">
                        <asp:TextBox ID="txtEndHideAlert" runat="server" style="width:100px;"></asp:TextBox>
                    </div>
                    <div class="cell" style="text-align:right">
                        <input type="button" id="btnHideAlertType" class="button" value="Ajouter" onclick="checkAlertType();" />
                    </div>
                </div>
            </asp:Panel>
        </div>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="btnConfirmHideAlert" EventName="click" />
    </Triggers>
</asp:UpdatePanel>
<div id="popup_AlertHide" style="display:none">
    <div id="divAlertHideConfirm">
        <div>Confirmez-vous masquer les alertes
            <label id="lblTagAlertToHide" style="font-weight:bold"></label> jusqu'au 
            <label id="lblEndHideAlert" style="font-weight:bold"></label> ?</div>
        <div style="width:100%; text-align:right; margin-top:20px">
            <input type="button" value="Valider" class="button" OnClick="validateAddHideAlert();" />
            <div style="display:none">
                <asp:Button ID="btnConfirmHideAlert" runat="server" Text="Valider" CssClass="button" OnClick="btnConfirmHideAlert_Click" />
            </div>
        </div>
    </div>
    <div id="divAlertHideError" style="color:red"></div>

</div>