﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProfileHistory.ascx.cs" Inherits="API_ProfileHistory" %>

<asp:Panel ID="panelProfileHistory" runat="server">
    <asp:Panel ID="panelProfileHistoryList" runat="server">
        <asp:Repeater ID="rptAML_ClientProfile_History" runat="server">
            <HeaderTemplate>
                <table style="width:100%;border-collapse:collapse;" class="tableHistory">
                    <tr>
                        <th>Type</th>
                        <th>Date</th>
                        <th>Par</th>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td style="text-align:center"><%# Eval("TAG") %></td>
                    <td style="text-align:center"><%# Eval("Date") %></td>
                    <td><%# Eval("ByUser") %></td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </asp:Panel>
    <asp:Panel ID="panelProfileHistoryListEmpty" runat="server" Visible="false">Aucun profil</asp:Panel>
</asp:Panel>
        