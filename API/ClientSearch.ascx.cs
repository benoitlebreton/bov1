﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class API_ClientSearch : System.Web.UI.UserControl
{
    public EventHandler ClientFound;
    private DataTable _dtSearchCustomer { get { return (Session["SearchCustomer"] != null) ? (DataTable)Session["SearchCustomer"] : null; } set { Session["SearchCustomer"] = value; } }
    public int RefCustomer { get { return (ViewState["RefCustomer"] != null) ? int.Parse(ViewState["RefCustomer"].ToString()) : 0; } set { ViewState["RefCustomer"] = value; } }
    public bool HideDetails { get { return (ViewState["HideDetails"] != null) ? bool.Parse(ViewState["HideDetails"].ToString()) : false; } set { ViewState["HideDetails"] = value; } }
    public bool HideSearchButton { get { return (ViewState["HideSearchButton"] != null) ? bool.Parse(ViewState["HideSearchButton"].ToString()) : false; } set { ViewState["HideSearchButton"] = value; } }
    public bool HideAlerts { get { return (ViewState["HideAlerts"] != null) ? bool.Parse(ViewState["HideAlerts"].ToString()) : false; } set { ViewState["HideAlerts"] = value; } }
    public enum ObjRepeatDirection { Horizontal, Vertical }
    public ObjRepeatDirection RepeatDirection {
        get
        {
            ObjRepeatDirection rptDirection = ObjRepeatDirection.Vertical;
            try { rptDirection = (ViewState["RepeatDirection"] != null) ? (ObjRepeatDirection)ViewState["RepeatDirection"] : ObjRepeatDirection.Horizontal; }
            catch(Exception ex) { }
            return rptDirection;
        }
        set { ViewState["RepeatDirection"] = value; } }

    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            if (HideSearchButton)
                btnClientSearch.Visible = false;
        }
    }

    protected string GetRepeatDirection()
    {
        string sStyle = "";

        if(RepeatDirection == ObjRepeatDirection.Horizontal)
        {
            sStyle = "display:inline-block;";
        }

        return sStyle;
    }

    public void btnClientSearch_Click(object sender, EventArgs e)
    {
        if (txtClientSearch.Text.Length > 0)
        {
            string sToken = authentication.GetCurrent(false).sToken;
            DataTable dt = Client.GetClientList("", "", "", "", "", txtClientSearch.Text, sToken);

            if (dt.Rows.Count > 0)
            {
                _dtSearchCustomer = dt;

                if (dt.Rows.Count == 1)
                {
                    UpdateClientPanel("0");
                }
                else
                {
                    rptClientChoice.DataSource = dt;
                    rptClientChoice.DataBind();
                    upClientMultiChoice.Update();
                    UpdateClientPanel("");
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "ShowSearchResult();", true);
                }
            }
            else
            {
                if (!HideAlerts)
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Aucun client trouvé', \"Votre recherche ne retourne aucun résultat.<br/>Veuillez vérifier la saisie et réessayer.\", null, null);", true);
            }
        }
        else
        {
            if (!HideAlerts)
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Erreur', \"Recherche : champ requis\", null, null);", true);
        }
    }

    protected void UpdateClientPanel(string sIndex)
    {
        if (sIndex.Length > 0)
        {
            int i = int.Parse(sIndex);

            if (_dtSearchCustomer.Rows.Count >= i)
            {
                int iTmp;
                if (int.TryParse(_dtSearchCustomer.Rows[i]["RefCustomer"].ToString(), out iTmp))
                {
                    RefCustomer = iTmp;
                    if (ClientFound != null)
                        ClientFound(new object(), new EventArgs());
                }

                if (!HideDetails)
                {
                    lblFirstName.Text = _dtSearchCustomer.Rows[i]["FirstName"].ToString();
                    lblLastName.Text = _dtSearchCustomer.Rows[i]["LastName"].ToString();
                    lblBirthDate.Text = _dtSearchCustomer.Rows[i]["BirthDate"].ToString();
                    panelClientDetails.Visible = true;
                }

                txtClientSearch.Text = "";
                upClientSearch.Update();

                return;
            }
        }

        RefCustomer = 0;
        lblFirstName.Text = "";
        lblLastName.Text = "";
        lblBirthDate.Text = "";
        panelClientDetails.Visible = false;
        upClientSearch.Update();
    }

    protected void btnUpdateDetails_Click(object sender, EventArgs e)
    {
        UpdateClientPanel(hdnIndex.Value);
    }
}