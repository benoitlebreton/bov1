﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BlockedTransfer.ascx.cs" Inherits="API_BlockedTransfer" %>
<script type="text/javascript">
    function init() {
        initTooltip();
    }

    function BlockedTransfer_ShowClientDetails(sRef) {
        document.location.href = "ClientDetails.aspx?ref=" + sRef;
    }
    function BlockedTransfer_PendingTransferAction(refPendingTransfer, refCustomer, action) {
        switch (action) {
            case "TERMINATE":
                $('#pending-transfer-action').html('approuver');
                break;
            case "REFUSE":
                $('#pending-transfer-action').html('refuser');
                break;
            case "CANCEL":
                $('#pending-transfer-action').html('annuler');
                break;
        }

        $('#<%=hdnRefCustomerSelected.ClientID%>').val(refCustomer);
        $('#<%=hdnRefPendingTransferSelected.ClientID%>').val(refPendingTransfer);
        $('#<%=hdnPendingTransferActionSelected.ClientID%>').val(action);

        $('#dialog-pending-transfer-action').dialog({
            autoOpen: true,
            resizable: false,
            draggable: false,
            modal: true,
            minWidth: 380,
            zIndex: 10000,
            title: "Confirmation",
            buttons: [{ text: 'Annuler', click: function () { $(this).dialog('close'); $(this).dialog('destroy'); } },
                {
                    text: 'OK', click: function () {
                        showLoading();
                        $('#<%=btnPendingTransferAction.ClientID%>').click();
                        $(this).dialog('close'); $(this).dialog('destroy');
                    }
                }]
        });
        $("#dialog-pending-transfer-action").parent().appendTo(jQuery("form:first"));
    }

    function BlockedTransfer_CheckMessages() {
        if ($('#<%=hdnMessage.ClientID%>').val().length > 0) {
            $('#message').empty().append($('#<%=hdnMessage.ClientID%>').val());
            $('#popup-message').dialog({
                autoOpen: true,
                resizable: false,
                draggable: false,
                modal: true,
                zIndex: 10000,
                title: "Message",
                buttons: [{ text: 'Fermer', click: function () { $(this).dialog('close'); $(this).dialog('destroy'); } }]
            });
            $('#<%=hdnMessage.ClientID%>').val('');
        }
    }

    function BlockedTransfer_ClientDetails(refPendingTransfer, refCustomer, status, reserve) {
        console.log(reserve);
        $('#<%=hdnRefCustomerSelected.ClientID%>').val(refCustomer);
        $('#<%=hdnRefPendingTransferSelected.ClientID%>').val(refPendingTransfer);
        if (status.toLowerCase() == "en attente") {
            if (reserve) {
                $("#dialog-pending-transfer-reservation").dialog({
                    autoOpen: true,
                    resizable: false,
                    draggable: false,
                    modal: true,
                    zIndex: 10000,
                    minWidth: 380,
                    title: "Assignation",
                    buttons: [{ text: 'Voir', click: function () { showLoading(); $(this).dialog('close'); $(this).dialog('destroy'); document.location = "ClientDetails.aspx?ref=" + refCustomer.toString() + "&view=aml"; } },
                        { text: "Je m'en occupe", click: function () { showLoading(); $('#<%=btnPendingTransferReservation.ClientID%>').click(); $(this).dialog('close'); $(this).dialog('destroy'); } }]
                });
                $("#dialog-pending-transfer-reservation").parent().appendTo(jQuery("form:first"));
            }
            else { $('#<%=btnPendingTransferReservation.ClientID%>').click(); }
        }
        else { document.location = "ClientDetails.aspx?ref=" + refCustomer.toString() + "&view=aml"; }
    }

    function BlockedTransfer_Unassign(refPendingTransfer, refCustomer, refAgent, agentName) {
        $('#<%=hdnRefCustomerSelected.ClientID%>').val(refCustomer);
        $('#<%=hdnRefPendingTransferSelected.ClientID%>').val(refPendingTransfer);
        $("#dialog-pending-transfer-unreservation").dialog({
            autoOpen: true,
            resizable: false,
            draggable: false,
            modal: true,
            pminWidth: 380,
            zIndex: 10000,
            title: "Assignation",
            buttons: [{ text: 'Annuler', click: function () { $(this).dialog('close'); $(this).dialog('destroy'); } }, { text: 'Désaffecter', click: function () { $('#<%=btnPendingTransferUnreservation.ClientID%>').click(); $(this).dialog('close'); $(this).dialog('destroy'); } }]
        });
        $("#dialog-pending-transfer-unreservation").parent().appendTo(jQuery("form:first"));

        //console.log('current refAgent*' + $('#<%=hdnCurrentRefAgent.ClientID%>').val().trim() + '*');
        //console.log('refAgent*' + refAgent + '*');
        $('#label-pending-transfer-unreservation').html("Le traitement de cette opération est déjà affecté.<br/>Voulez-vous la désaffecter?");
        if(agentName.trim().length > 0)
            $('#label-pending-transfer-unreservation').html("Le traitement de cette opération est affecté à " + agentName + ".<br/>Voulez-vous lui désaffecter?");
        if($('#<%=hdnCurrentRefAgent.ClientID%>').val().trim() == refAgent)
            $('#label-pending-transfer-unreservation').html("Vous ne vous occupez plus de cette opération?");
            
    }

    function BlockedTransfer_AddAmlNote() {
        $("#dialog-pending-transfer-action-aml-note").dialog({
            autoOpen: true,
            resizable: false,
            draggable: false,
            modal: true,
            minWidth: 880,
            zIndex: 10000,
            dialogClass: "no-close",
            title: "AML - Ajouter un commentaire",
            buttons: [{ text: 'Valider', click: function () { $('#<%=btnPendingTransferAddAmlNote.ClientID%>').click(); $(this).dialog('close'); $(this).dialog('destroy'); } }]
        });
        $("#dialog-pending-transfer-action-aml-note").parent().appendTo(jQuery("form:first"));
    }
</script>

<style type="text/css">
    .transfer-motive .tooltip {
        display: inline-block;
        padding-top: 10px;
        font-size: 0.9em;
        font-weight: bolder;
    }

    .transfer-motive .tooltip:empty {
        display: none;
    }

    .orangeLink {
        color: #f57527;
    }

    .orangeLink:hover{
        text-decoration:underline;
        cursor:pointer;
    }
</style>

<asp:Panel ID="panelBlockedTransfer" runat="server">
    <asp:Panel ID="panelGridFilters" runat="server" Visible="true">
        <div class="table">
            <div class="row">
                <div class="cell" style="padding-right: 5px; font-weight: bold">
                    Statut
                </div>
                <div class="cell">
                    <asp:DropDownList ID="ddlTransferStatus" runat="server" OnSelectedIndexChanged="ddlTransferStatus_SelectedIndexChanged" AutoPostBack="true">
                        <asp:ListItem Text="En attente (assigné inclus)" Value="PENDING" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Non traité" Value="PENDINGNOTRESERVED"></asp:ListItem>
                        <asp:ListItem Text="Assigné" Value="RESERVED"></asp:ListItem>
                        <asp:ListItem Text="Refusé" Value="REFUSED"></asp:ListItem>
                        <asp:ListItem Text="Annulé" Value="CANCELLED"></asp:ListItem>
                        <asp:ListItem Text="Terminé" Value="TERMINATED"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <%--<div class="cell" style="padding:0 5px 0 10px; font-weight: bold">
                    Attribution
                </div>
                <div class="cell">
                    <asp:DropDownList ID="ddlAssignmentStatus" runat="server" OnSelectedIndexChanged="ddlReserveStatus_SelectedIndexChanged" AutoPostBack="true">
                        <asp:ListItem Text="Tous" Value=""></asp:ListItem>
                        <asp:ListItem Text="Non attribué" Value="UNRESERVED"></asp:ListItem>
                        <asp:ListItem Text="Attribué" Value="RESERVED"></asp:ListItem>
                    </asp:DropDownList>
                </div>--%>
            </div>
        </div>
    </asp:Panel>
    <asp:UpdatePanel ID="upSearch" runat="server" UpdateMode="Conditional">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnPreviousPage" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnNextPage" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="ddlPage" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="ddlNbResult" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="ddlTransferStatus" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="btnPendingTransferAction" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnPendingTransferReservation" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnPendingTransferUnreservation" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnPendingTransferAddAmlNote" EventName="Click" />
        </Triggers>
        <ContentTemplate>

            <asp:Panel ID="panelGrid" runat="server" Visible="false">
                <div id="divGridSettings" runat="server" visible="false" style="display: table; width: 100%; margin-bottom: 10px">
                    <div style="display: table-row">
                        <div style="display: table-cell; width: 50%; padding-top: 20px">
                            <asp:Label ID="lblNbOfResults" runat="server">0</asp:Label>
                            résultat(s)
                        </div>
                        <div style="display: table-cell; width: 50%; text-align: right;">
                            <asp:Panel ID="panelPageSize" runat="server">
                                Nb de résultats par page :
                                <asp:DropDownList ID="ddlNbResult" runat="server" AutoPostBack="true" OnSelectedIndexChanged="onChangeNbResult">
                                    <asp:ListItem Value="10" Selected="True">10</asp:ListItem>
                                    <asp:ListItem Value="25">25</asp:ListItem>
                                    <asp:ListItem Value="50">50</asp:ListItem>
                                </asp:DropDownList>
                            </asp:Panel>
                        </div>
                    </div>
                </div>
                <div style="border: 3px solid #344b56; border-top-left-radius: 8px; border-top-right-radius: 8px; margin-top: 5px; font-size: 0.8em;">
                    <asp:GridView ID="gvBlockedTransferList" runat="server" AllowSorting="false"
                        AllowPaging="false" AutoGenerateColumns="False" GridLines="None" CellPadding="10"
                        DataKeyNames="RefCustomer" ShowHeader="true" Width="100%" SortedDescendingCellStyle-Font-Overline="true"
                        ViewStateMode="Disabled" BorderWidth="0" BackColor="Transparent">
                        <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="#344b56" Font-Bold="true" />
                        <EmptyDataTemplate>
                            Aucun virement
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField InsertVisible="false" ControlStyle-Width="30" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <%# GetReservedStatusImage(Eval("isReserved").ToString(), Eval("ReservedDate").ToString(), Eval("refBOReserved").ToString(), Eval("BOReservedUserName").ToString(), Eval("RefTransferPending").ToString(), Eval("RefCustomer").ToString()) %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Etat" InsertVisible="false" ControlStyle-Width="70" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblStatus" Text='<%# GetStatusLabel(
                                        Eval("isRefused").ToString(), Eval("RefusedDate").ToString(), 
                                        Eval("isCancelled").ToString(), Eval("CancelledDate").ToString(), 
                                        Eval("isTerminated").ToString(), Eval("TerminatedDate").ToString(),
                                        Eval("isReserved").ToString(), Eval("ReservedDate").ToString()) %>' CssClass="tooltip"
                                        ToolTip='<%# GetStatusLabel(
                                                        Eval("isRefused").ToString(), Eval("RefusedDate").ToString(),
                                                        Eval("isCancelled").ToString(),Eval("CancelledDate").ToString(),
                                                        Eval("isTerminated").ToString(),Eval("TerminatedDate").ToString(),
                                                        Eval("isReserved").ToString(), Eval("ReservedDate").ToString()) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="N° Compte" InsertVisible="false">
                                <ItemTemplate>
                                    <%# GetAccountNumberLabel(
                                            Eval("CustomerAccountNumber").ToString(),
                                            Eval("RefTransferPending").ToString(),
                                            Eval("RefCustomer").ToString(),
                                            Eval("isRefused").ToString(),Eval("RefusedDate").ToString(),
                                            Eval("isCancelled").ToString(),Eval("CancelledDate").ToString(),
                                            Eval("isTerminated").ToString(),Eval("TerminatedDate").ToString(),
                                            Eval("isReserved").ToString(), Eval("ReservedDate").ToString()) %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Raison" InsertVisible="false">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblReason" Text='<%# Eval("PendingStatusLabel")%>' CssClass="tooltip" ToolTip='<%# Eval("PendingStatusLabel")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="IBAN / Motif" InsertVisible="false">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblDestIBAN" Text='<%# Eval("iban")%>' CssClass="tooltip" ToolTip='<%# Eval("iban")%>'></asp:Label>
                                    <div class="transfer-motive">
                                        <asp:Label runat="server" ID="lblMotive" Text='<%# Eval("Libl1")%>' CssClass="tooltip" ToolTip='<%# Eval("Libl1")%>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Date" InsertVisible="false" ControlStyle-Width="100" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblStatus" Text='<%# tools.getFormattedDate(Eval("RequestDate").ToString()) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Montant" InsertVisible="false" ItemStyle-HorizontalAlign="Right">
                                <ItemTemplate>
                                    <span style="white-space: nowrap">
                                        <asp:Label runat="server" ID="lblAmount" Text='<%# getAmountCleaned(tools.getFormattedAmount(Eval("Amount").ToString(), false)) %>'></asp:Label>&nbsp;
                                                <asp:Label runat="server" ID="lblCurrency" Text='<%# Eval("Currency")%>' CssClass="tooltip"
                                                    ToolTip='<%# Eval("Currency")%>'></asp:Label>
                                    </span>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Action">
                                <ItemTemplate>
                                    <div class="trimmer" style="width: auto; white-space: nowrap; <%# (Eval("isRefused").ToString() == "True" || Eval("isTerminated").ToString() == "True" || Eval("isCancelled").ToString() == "True") ? "display:none": "" %>">
                                        <asp:ImageButton ID="imgAuthorize" runat="server" ImageUrl="~/Styles/Img/check.png" ToolTip="Approuver" OnClientClick=<%# string.Format("BlockedTransfer_PendingTransferAction('{0}', {1}, 'TERMINATE');return false;", Eval("RefTransferPending"), Eval("RefCustomer")) %> Style="height: 25px" />
                                        <asp:ImageButton ID="imgDeny" runat="server" ImageUrl="~/Styles/Img/deny.png" ToolTip="Refuser" OnClientClick=<%# string.Format("BlockedTransfer_PendingTransferAction('{0}', {1}, 'REFUSE');return false;", Eval("RefTransferPending"), Eval("RefCustomer")) %> Style="height: 25px" />
                                        <asp:ImageButton ID="imgCancel" runat="server" ImageUrl="~/Styles/Img/cancel.png" ToolTip="Annuler" OnClientClick=<%# string.Format("BlockedTransfer_PendingTransferAction('{0}', {1}, 'CANCEL');return false;", Eval("RefTransferPending"), Eval("RefCustomer")) %> Style="height: 25px" />
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#344b56" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#344b56" Font-Bold="True" ForeColor="White" Height="25px" />
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                        <RowStyle CssClass="GridViewRowStyle" />
                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                        <%--<PagerStyle CssClass="GridViewPager" />--%>
                    </asp:GridView>
                </div>
                <div style="border: 3px solid #344b56; border-bottom-left-radius: 8px; border-bottom-right-radius: 8px; height: 25px; background-color: #344b56; color: White;">
                    <asp:Panel ID="divPagination" runat="server" Visible="false" Style="width: 100%; display: table; margin: auto">
                        <div style="display: table-row;">
                            <div style="display: table-cell; height: 20px; vertical-align: middle; width: 57%; text-align: right">
                                <asp:Button ID="btnPreviousPage" runat="server" OnClick="previousPage" Text="<" Font-Bold="true" Width="30px" />
                                <asp:DropDownList ID="ddlPage" runat="server" OnSelectedIndexChanged="onChangePageNumber" AutoPostBack="true"
                                    DataTextField="L" DataValueField="V">
                                </asp:DropDownList>
                                <asp:Label ID="lblTotalPage" runat="server" Style="font-weight: bold"></asp:Label>
                                <asp:Button ID="btnNextPage" runat="server" OnClick="nextPage" Text=">" Font-Bold="true" Width="30px" />
                            </div>
                            <div style="display: table-cell; padding-left: 5px">
                            </div>
                        </div>
                    </asp:Panel>
                </div>
            </asp:Panel>
            <asp:Panel ID="panelError" runat="server" Visible="false" Style="margin-top: 20px">
                <asp:Label ID="lblError" runat="server"></asp:Label>
            </asp:Panel>

            <div id="dialog-pending-transfer-action" style="display: none">
                &Ecirc;tes-vous sûr de vouloir <span id="pending-transfer-action"></span> ce virement ?
                
                <asp:Button ID="btnPendingTransferAction" runat="server" OnClick="btnPendingTransferAction_Click" Style="display: none" />
            </div>

            <div id="dialog-pending-transfer-action-aml-note" style="display:none">
                <div style="font-weight: bold">
                    <asp:Label ID="BT_lblNbCharClientNote" runat="server">0</asp:Label>
                    / <asp:Label ID="BT_lblNbMaxClientNote" runat="server">400</asp:Label> caractères
                </div>
                <div>
                    <asp:TextBox ID="BT_txtClientNote" runat="server" TextMode="MultiLine" MaxLength="400" 
                        style="min-width:830px;max-width:830px; min-height:80px; max-height:80px;">
                    </asp:TextBox>
                </div>
                <asp:Button ID="btnPendingTransferAddAmlNote" runat="server" OnClick="btnPendingTransferAddAmlNote_Click" style="display:none;" />
            </div>

            <div id="dialog-pending-transfer-reservation" style="display: none">
                Vous allez accéder à la fiche du client.<br />
                <span style="font-weight:bold" class="font-orange">En cliquant sur "Je m'en occupe", le traitement de ce virement vous sera attribué.</span><br />
                <asp:Button ID="btnPendingTransferReservation" runat="server" OnClick="btnPendingTransferReservation_Click" Style="display: none" />
            </div>

            <div id="dialog-pending-transfer-unreservation" style="display: none">
                <label id="label-pending-transfer-unreservation"></label>
                <asp:Button ID="btnPendingTransferUnreservation" runat="server" OnClick="btnPendingTransferUnreservation_Click" Style="display: none" />
            </div>

            <asp:HiddenField ID="hdnMessage" runat="server" />
            <asp:HiddenField ID="hdnRefPendingTransferSelected" runat="server" />
            <asp:HiddenField ID="hdnRefCustomerSelected" runat="server" />
            <asp:HiddenField ID="hdnPendingTransferActionSelected" runat="server" />
            <asp:HiddenField ID="hdnAmlNoteAutoFill" runat="server"/>
            <asp:HiddenField ID="hdnCurrentRefAgent" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
<asp:Panel ID="panelBlockedTransferUnauthorized" runat="server" Visible="false" CssClass="font-orange bold" style="padding:10px 0;">
    Vous n'êtes pas autorisé à voir ce contenu
</asp:Panel>

<div id="popup-message">
    <span id="message"></span>
</div>
