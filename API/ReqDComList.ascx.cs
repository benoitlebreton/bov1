﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using XMLMethodLibrary;

public partial class API_ReqDComList : System.Web.UI.UserControl
{
    public int RefCustomer { get; set; }
    public string Filter { get; set; }
    public bool ShowLabel { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            BindRJDCList();

            if (ShowLabel)
                panelLabel.Visible = true;
        }
    }

    protected void BindRJDCList()
    {
        rptRJDC.Visible = false;
        panelNoRJDC.Visible = false;

        DataTable dt = Requisition.GetRJDCList(0, RefCustomer);

        if (dt.Rows.Count > 0)
        {
            rptRJDC.DataSource = dt;
            rptRJDC.DataBind();
            rptRJDC.Visible = true;
        }
        else panelNoRJDC.Visible = true;
    }
}