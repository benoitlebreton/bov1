﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AmlSurveillance.ascx.cs" Inherits="API_AmlSurveillance" %>

<script type="text/javascript">
    function InitAMLToggles() {
        // Init AML toggles
        $('#toggleMiseSousSurveillance').toggles({
            text: { on: 'activée', off: 'désactivée' },
            drag: false,
            on: $('input[id$=ckbMiseSousSurveillance]').is(':checked'),
            checkbox: $('input[id$=ckbMiseSousSurveillance]'),
            width: $('input[id$=hdnMiseSousSurveillanceWidth]').val(),
            height: 20
        });
        $('.mise-sous-surveillance input[type=checkbox]').unbind('change').change(function () {
            <%--$('#<%=btnMiseSousSurveillance.ClientID%>').click();--%>
            <%--console.log($('#<%=ckbMiseSousSurveillance.ClientID%>').attr('checked'));--%>
            InitAMLSurveillance();
        });
        $('#toggleAlerteVirementEntrantAgent').toggles({
            text: { on: 'oui', off: 'non' },
            drag: false,
            on: $('input[id$=ckbAlerteVirementEntrantAgent]').is(':checked'),
            checkbox: $('input[id$=ckbAlerteVirementEntrantAgent]'),
            width: 80,
            height: 20
        });
        $('#toggleAlerteVirementEntrantService').toggles({
            text: { on: 'oui', off: 'non' },
            drag: false,
            on: $('input[id$=ckbAlerteVirementEntrantService]').is(':checked'),
            checkbox: $('input[id$=ckbAlerteVirementEntrantService]'),
            width: 80,
            height: 20
        });
        $('#toggleAlerteVirementSortantAgent').toggles({
            text: { on: 'oui', off: 'non' },
            drag: false,
            on: $('input[id$=ckbAlerteVirementSortantAgent]').is(':checked'),
            checkbox: $('input[id$=ckbAlerteVirementSortantAgent]'),
            width: 80,
            height: 20
        });
        $('#toggleAlerteVirementSortantService').toggles({
            text: { on: 'oui', off: 'non' },
            drag: false,
            on: $('input[id$=ckbAlerteVirementSortantService]').is(':checked'),
            checkbox: $('input[id$=ckbAlerteVirementSortantService]'),
            width: 80,
            height: 20
        });
        $('#toggleAlerteOpertationsCarteAgent').toggles({
            text: { on: 'oui', off: 'non' },
            drag: false,
            on: $('input[id$=ckbAlerteOperationsCarteAgent]').is(':checked'),
            checkbox: $('input[id$=ckbAlerteOperationsCarteAgent]'),
            width: 80,
            height: 20
        });
        $('#toggleAlerteOpertationsCarteService').toggles({
            text: { on: 'oui', off: 'non' },
            drag: false,
            on: $('input[id$=ckbAlerteOperationsCarteService]').is(':checked'),
            checkbox: $('input[id$=ckbAlerteOperationsCarteService]'),
            width: 80,
            height: 20
        });
        $('#toggleAlerteTouteOperationAgent').toggles({
            text: { on: 'oui', off: 'non' },
            drag: false,
            on: $('input[id$=ckbAlerteTouteOperationAgent]').is(':checked'),
            checkbox: $('input[id$=ckbAlerteTouteOperationAgent]'),
            width: 80,
            height: 20
        });
        $('#toggleAlerteTouteOperationService').toggles({
            text: { on: 'oui', off: 'non' },
            drag: false,
            on: $('input[id$=ckbAlerteTouteOperationService]').is(':checked'),
            checkbox: $('input[id$=ckbAlerteTouteOperationService]'),
            width: 80,
            height: 20
        });
        $('#toggleBlocageTousVirements').toggles({
            text: { on: 'oui', off: 'non' },
            drag: false,
            on: $('input[id$=ckbBlocageTousVirements]').is(':checked'),
            checkbox: $('input[id$=ckbBlocageTousVirements]'),
            width: 80,
            height: 20
        });
        $('input[id$=ckbBlocageTousVirements]').unbind('change').bind('change', function(){
            console.log($('input[id$=ckbBlocageTousVirements]').attr('checked'));
            if ($('input[id$=ckbBlocageTousVirements]').attr('checked') == 'checked') {
                $('input[id$=ckbLimiteValidation]').removeAttr('checked').attr('disabled', true);
                $('input[id$=txtLimiteValidation]').val('').attr('disabled', true);
            } else {
                $('input[id$=ckbLimiteValidation]').attr('disabled', false);
                $('input[id$=txtLimiteValidation]').attr('disabled', false);
            }
            
            // refresh toggle
            $('#toggleLimiteValidation').toggles({
                text: { on: 'oui', off: 'non' },
                drag: false,
                click: ($('input[id$=ckbLimiteValidation]').attr('disabled') == 'disabled') ? false : true,
                on: $('input[id$=ckbLimiteValidation]').is(':checked'),
                checkbox: $('input[id$=ckbLimiteValidation]'),
                width: 80,
                height: 20
            });

            InitAMLSurveillance();
            }
        );
        $('#toggleLimiteValidation').toggles({
            text: { on: 'oui', off: 'non' },
            drag: false,
            click: ($('input[id$=ckbLimiteValidation]').attr('disabled') == 'disabled') ? false : true,
            on: $('input[id$=ckbLimiteValidation]').is(':checked'),
            checkbox: $('input[id$=ckbLimiteValidation]'),
            width: 80,
            height: 20
        });
                    
    }

    function InitAMLSurveillance() {
        $('.RadioButtonList').buttonset();

        if ($('input[id$=ckbMiseSousSurveillance]').attr('checked') != null) {
            $('#panelSousSurveillanceLock').hide();
            if ($('input[id$=hdnDisplayMode]').val() == 'AlertTreatment')
                $('#div-sous-surveillance').show();

            if ($('input[id$=txtAlerteVirementEntrantMontant]').val().length == 0 || $('input[id$=txtAlerteVirementEntrantMontant]').val() == "0") {
                $('input[id$=txtAlerteVirementEntrantMontant]').val('300');
                $('input[id$=ckbAlerteVirementEntrantService]').attr('checked', 'checked');

                // refresh toggle
                $('#toggleAlerteVirementEntrantService').toggles({
                    text: { on: 'oui', off: 'non' },
                    drag: false,
                    on: $('input[id$=ckbAlerteVirementEntrantService]').is(':checked'),
                    checkbox: $('input[id$=ckbAlerteVirementEntrantService]'),
                    width: 80,
                    height: 20
                });
            }
        }
        else {
            $('#panelSousSurveillanceLock').show();
            if ($('input[id$=hdnDisplayMode]').val() == 'AlertTreatment')
                $('#div-sous-surveillance').hide();
            $('#div-sous-surveillance input[type=checkbox]').removeAttr('checked');
            $('#div-sous-surveillance input[type=text]').val('');
            InitAMLToggles();
        }

        $('.div-sous-surveillance-lock').width($('#div-sous-surveillance').outerWidth());
        $('.div-sous-surveillance-lock').height($('#div-sous-surveillance').outerHeight());
        $('.div-sous-surveillance-readonly').width($('.div-outer-sous-surveillance').outerWidth());
        $('.div-sous-surveillance-readonly').height($('.div-outer-sous-surveillance').outerHeight());
    }

    function ShowAmlSurvMessage(mess) {
            if (mess.length > 0) {
                $('.aml-surv-message').empty().append(mess);
                $('#aml-surv-popup-message').dialog({
                    resizable: false,
                    draggable: false,
                    modal: true,
                    title: "Message",
                    zIndex: 99997,
                    buttons: [{ text: 'Fermer', click: function () { $(this).dialog('close');$(this).dialog('destroy'); } }]
                });
            }

            $('.loading').hide();
        }
</script>

<asp:HiddenField ID="hdnMiseSousSurveillanceWidth" runat="server" />
<div class="div-outer-sous-surveillance">
    <asp:HiddenField ID="hdnDisplayMode" runat="server" />
    <asp:Panel ID="panelSousSurveillanceReadonly" runat="server" CssClass="div-sous-surveillance-readonly"></asp:Panel>
    <asp:UpdatePanel ID="upAmlSurveillance" runat="server">
        <ContentTemplate>
            <div class="tabSubtitle">
                <div>
                    Mise sous surveillance
                </div>
                <div class="mise-sous-surveillance">
                    <div id="toggleMiseSousSurveillance" class="toggle-soft"></div>
                    <asp:CheckBox ID="ckbMiseSousSurveillance" runat="server" CssClass="ui-helper-hidden" />
                    <%--<asp:Button ID="btnMiseSousSurveillance" runat="server" CssClass="ui-helper-hidden" OnClick="btnMiseSousSurveillance_Click" />--%>
                </div>
                <div style="clear:both"></div>
            </div>
            <div id="div-sous-surveillance">
                <div id="panelSousSurveillanceLock" class="div-sous-surveillance-lock"></div>
                <div class="table alert-table" style="width:100%;border-collapse:collapse;text-transform:none">
                    <div class="table-row head">
                        <div class="table-cell" style="text-align:left;padding-left:20px">
                            Alerter en cas de
                        </div>
                        <div class="table-cell" style="width:45%">
                            d'un montant minimum de
                        </div>
                        <div class="table-cell" style="text-align:center;width:1%">
                            Agent
                        </div>
                        <div class="table-cell" style="text-align:center;width:1%">
                            Service
                        </div>
                    </div>
                    <div class="table-row">
                        <div class="table-cell">
                            Virement entrant (SEPA)
                        </div>
                        <div class="table-cell">
                            <asp:TextBox ID="txtAlerteVirementEntrantMontant" runat="server" CssClass="alert-amount" MaxLength="9" style="text-align:right"></asp:TextBox> &euro;
                        </div>
                        <div class="table-cell">
                            <div id="toggleAlerteVirementEntrantAgent" class="toggle-soft"></div>
                            <asp:CheckBox ID="ckbAlerteVirementEntrantAgent" runat="server" />
                        </div>
                        <div class="table-cell">
                            <div id="toggleAlerteVirementEntrantService" class="toggle-soft"></div>
                            <asp:CheckBox ID="ckbAlerteVirementEntrantService" runat="server" />
                        </div>
                    </div>
                    <div class="table-row">
                        <div class="table-cell">
                            Virement sortant
                        </div>
                        <div class="table-cell">
                            <asp:TextBox ID="txtAlerteVirementSortantMontant" runat="server" CssClass="alert-amount" MaxLength="9" style="text-align:right"></asp:TextBox> &euro;
                        </div>
                        <div class="table-cell">
                            <div id="toggleAlerteVirementSortantAgent" class="toggle-soft"></div>
                            <asp:CheckBox ID="ckbAlerteVirementSortantAgent" runat="server" />
                        </div>
                        <div class="table-cell">
                            <div id="toggleAlerteVirementSortantService" class="toggle-soft"></div>
                            <asp:CheckBox ID="ckbAlerteVirementSortantService" runat="server" />
                        </div>
                    </div>
                    <div class="table-row">
                        <div class="table-cell">
                            Opération carte
                        </div>
                        <div class="table-cell">
                            <asp:TextBox ID="txtAlerteOpertationsCarteMontant" runat="server" CssClass="alert-amount" MaxLength="9" style="text-align:right"></asp:TextBox> &euro;
                        </div>
                        <div class="table-cell">
                            <div id="toggleAlerteOpertationsCarteAgent" class="toggle-soft"></div>
                            <asp:CheckBox ID="ckbAlerteOperationsCarteAgent" runat="server" />
                        </div>
                        <div class="table-cell">
                            <div id="toggleAlerteOpertationsCarteService" class="toggle-soft"></div>
                            <asp:CheckBox ID="ckbAlerteOperationsCarteService" runat="server" />
                        </div>
                    </div>
                    <div class="table-row">
                        <div class="table-cell">
                            Toute opération
                        </div>
                        <div class="table-cell">
                            <asp:TextBox ID="txtAlerteTouteOperationMontant" runat="server" CssClass="alert-amount" MaxLength="9" style="text-align:right"></asp:TextBox> &euro;
                        </div>
                        <div class="table-cell">
                            <div id="toggleAlerteTouteOperationAgent" class="toggle-soft"></div>
                            <asp:CheckBox ID="ckbAlerteTouteOperationAgent" runat="server" />
                        </div>
                        <div class="table-cell">
                            <div id="toggleAlerteTouteOperationService" class="toggle-soft"></div>
                            <asp:CheckBox ID="ckbAlerteTouteOperationService" runat="server" />
                        </div>
                    </div>
                </div>
                <div class="table alert-table" style="width:100%;border-collapse:collapse;text-transform:none;margin-top:20px;">
                    <div class="table-row head">
                        <div class="table-cell" style="text-align:left;padding-left:20px">
                            Virements sortants
                        </div>
                        <div class="table-cell" style="width:45%;text-align:center;">
                        </div>
                        <div class="table-cell" style="text-align:center;width:80px">
                        </div>
                        <div class="table-cell" style="text-align:center;width:1%">
                        </div>
                    </div>
                    <div class="table-row">
                        <div class="table-cell">
                            Bloquer tous les virements
                        </div>
                        <div class="table-cell">
                        </div>
                        <div class="table-cell">
                        </div>
                        <div class="table-cell">
                            <div id="toggleBlocageTousVirements" class="toggle-soft"></div>
                            <asp:CheckBox ID="ckbBlocageTousVirements" runat="server" />
                        </div>
                    </div>
                    <div class="table-row">
                        <div class="table-cell">
                            Activer la validation des virements à partir de
                        </div>
                        <div class="table-cell" style="text-align:center;">
                            <asp:TextBox ID="txtLimiteValidation" runat="server" MaxLength="9" style="text-align:right;"></asp:TextBox> &euro;
                        </div>
                        <div class="table-cell">
                        </div>
                        <div class="table-cell">
                            <div id="toggleLimiteValidation" class="toggle-soft"></div>
                            <asp:CheckBox ID="ckbLimiteValidation" runat="server" />
                        </div>
                    </div>
                </div>

                <div class="table alert-table" style="width:100%;border-collapse:collapse;text-transform:none;margin-top:20px;">
                    <div class="table-row head">
                        <div class="table-cell" style="text-align:left;padding-left:20px">
                            Durée du suivi
                        </div>
                        <div class="table-cell"></div>
                    </div>
                    <div class="table-row">
                        <div class="table-cell" style="width:300px">
                            <asp:RadioButtonList ID="rblAMLSurveillancePeriod" runat="server" RepeatDirection="Horizontal" CssClass="RadioButtonList">
                                <asp:ListItem Text="1 mois" Value="1" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="2 mois" Value="2"></asp:ListItem>
                                <asp:ListItem Text="3 mois" Value="3"></asp:ListItem>
                            </asp:RadioButtonList>    
                        </div>
                        <div class="table-cell" style="text-align:left">
                            <asp:Label ID="lblPeriod" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
            <div style="text-align:center;margin-top:10px;position:relative;">
                <asp:Button ID="btnSaveAMLSurveillance" runat="server" Text="Enregistrer les paramètres de surveillance" CssClass="button" OnClick="btnSaveAMLSurveillance_Click" />
                <asp:UpdateProgress ID="updateProgressSurveillance" runat="server" AssociatedUpdatePanelID="upAmlSurveillance">
                    <ProgressTemplate>
                        <img src="Styles/Img/loading.gif" style="position:absolute;left:58%;top:0" />
                    </ProgressTemplate>
                </asp:UpdateProgress>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>

<div id="aml-surv-popup-message" style="display:none">
    <span class="aml-surv-message"></span>
</div>