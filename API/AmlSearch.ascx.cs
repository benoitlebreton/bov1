﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using XMLMethodLibrary;

public partial class API_AmlSearch : System.Web.UI.UserControl
{
    private string _sTagAlert;
    private string _sAlertCategory;
    private string _sProfile;
    public string sTagAlert
    {
        get { return _sTagAlert; }
        set { _sTagAlert = value; }
    }
    public string sAlertCategory
    {
        get { return _sAlertCategory; }
        set { _sAlertCategory = value; }
    }
    public string sProfile
    {
        get { return _sProfile; }
        set { _sProfile = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            getParam();
            bindAlertGrid();
        }
    }

    public void bindAlertGrid()
    {
        authentication auth = authentication.GetCurrent();
        string sXmlOut = "";
        string sXmlIn = new XElement("ALL_XML_IN",
                                            new XElement("Alert",
                                                new XAttribute("RefAlertStatus", "1"),
                                                new XAttribute("CashierToken", auth.sToken),
                                                new XAttribute("PageSize", ddlNbResult.SelectedValue),
                                                new XAttribute("PageIndex", ddlPage.SelectedValue),
                                                new XAttribute("TAGAlert", sTagAlert),
                                                new XAttribute("AlertCategory", sAlertCategory),
                                                new XAttribute("Culture", ((Session["CultureID"] != null) ? Session["CultureID"].ToString() : "fr-FR")))).ToString();
        DataTable dtClientAlertList = AML.getClientAlertList(sXmlIn, out sXmlOut);

        List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/RESULT", "RC");
        List<string> listErrorLabel = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/RESULT", "ErrorLabel");
        List<string> listRowCount = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Alert", "NbResult");
        if (listRC.Count == 0 && listRowCount != null && listRowCount.Count > 0)
        {
            dtClientAlertList.DefaultView.Sort = "AlertDate DESC";
            gvClientAlertList.DataSource = dtClientAlertList;
            panelGrid.Visible = true;
            panelError.Visible = false;
            lblNbOfResults.Text = listRowCount[0].ToString();
            PaginationManagement(lblNbOfResults.Text, sXmlOut);
            getDynamicTitleColumns(sXmlOut);

        }
        else
        {
            if (listErrorLabel != null && listErrorLabel.Count > 0)
            {
                panelGrid.Visible = false;
                panelError.Visible = true;
                lblError.Text = listErrorLabel[0].ToString().ToUpper();
            }
        }
        try
        {
            gvClientAlertList.DataBind();
            if (gvClientAlertList.Rows.Count > 0)
            {
                panelGrid.Visible = true;
                divGridSettings.Visible = true;

            }
        }
        catch (Exception ex)
        {
        }

        //saveParam();
    }

    public void saveParam()
    {
        DataTable dtParam = new DataTable();
        dtParam.Columns.Add("Name");
        dtParam.Columns.Add("Value");
        dtParam.Rows.Add(new object[] { "tagAlert", _sTagAlert });
        dtParam.Rows.Add(new object[] { "alertType", _sAlertCategory });
        dtParam.Rows.Add(new object[] { "nbResult", ddlNbResult.SelectedValue });
        dtParam.Rows.Add(new object[] { "nbPages", (lblTotalPage.Text.Trim().Length > 0) ? lblTotalPage.Text.Replace("/", "").Trim() : "1" });
        dtParam.Rows.Add(new object[] { "pageIndex", ddlPage.SelectedValue });
        dtParam.Rows.Add(new object[] { "profile", _sProfile });

        ViewState["SearchAlertParam"] = dtParam;
    }
    public void getParam()
    {
        if (ViewState["SearchAlertParam"] != null)
        {
            try
            {
                DataTable dtParam = (DataTable)Session["SearchAlertParam"];
                string sParamName = "", sParamValue = "";

                for (int i = 0; i < dtParam.Rows.Count; i++)
                {

                    sParamValue = dtParam.Rows[i]["Value"].ToString().Trim();
                    sParamName = dtParam.Rows[i]["Name"].ToString().Trim();

                    if (sParamName.Length > 0 && sParamValue.Length > 0)
                    {
                        switch (sParamName)
                        {
                            case "tagAlert":
                            //    if (ddlTagAlert.Items.FindByValue(sParamValue) != null)
                            //        ddlTagAlert.SelectedValue = sParamValue;
                                sTagAlert = sParamValue;
                                break;
                            case "alertType":
                            //    if (ddlAlertType.Items.FindByValue(sParamValue) != null)
                            //        ddlAlertType.SelectedValue = sParamValue;
                                sAlertCategory = sParamValue;
                                break;
                            case "nbResult":
                                if (ddlNbResult.Items.FindByValue(sParamValue) != null)
                                    ddlNbResult.SelectedValue = sParamValue;
                                break;
                            case "nbPages":
                                int iNbPages = 1;
                                int.TryParse(sParamValue, out iNbPages);
                                ddlPage.DataSource = getPages(iNbPages);
                                ddlPage.DataBind();
                                break;
                            case "pageIndex":
                                if (ddlPage.Items.FindByValue(sParamValue) != null)
                                    ddlPage.SelectedValue = sParamValue;
                                break;
                            case "profile":
                                //if (ddlProfile.Items.FindByValue(sParamValue) != null)
                                    //ddlProfile.SelectedValue = sParamValue;
                                sProfile = sParamValue;
                                break;
                        }
                    }
                }

            }
            catch (Exception ex)
            {

            }
        }
        else
        {
            ddlPage.DataSource = getPages(1);
            ddlPage.DataBind();
            sAlertCategory = "";
        }
    }

    protected void gvClientAlertList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HiddenField hdnRefCustomer = (HiddenField)e.Row.FindControl("hdnRefCustomer");
            if (hdnRefCustomer != null)
            {
                e.Row.Attributes.Add("onclick", "ShowClientDetails('" + hdnRefCustomer.Value + "')");
                e.Row.Attributes.Add("onmouseover", "this.style.cursor = 'pointer';this.style.backgroundColor = '#fb7844'");
                e.Row.Attributes.Add("onmouseout", "this.style.cursor = 'auto';this.style.backgroundColor = '#fff'");
            }
        }
    }
    protected void getDynamicTitleColumns(string sXML)
    {
        string sINT1 = "", sINT2 = "", sMONEY1 = "", sVARCHAR1 = "";

        sINT1 = tools.GetValueFromXml(sXML, "ALL_XML_OUT/Alert/CriteriaDescription", "Critere_INT1");
        sINT2 = tools.GetValueFromXml(sXML, "ALL_XML_OUT/Alert/CriteriaDescription", "Critere_INT2");
        sVARCHAR1 = tools.GetValueFromXml(sXML, "ALL_XML_OUT/Alert/CriteriaDescription", "Critere_VARCHAR1");
        sMONEY1 = tools.GetValueFromXml(sXML, "ALL_XML_OUT/Alert/CriteriaDescription", "Critere_MONEY1");
        gvClientAlertList.Columns[4].Visible = false;
        gvClientAlertList.Columns[5].Visible = false;
        gvClientAlertList.Columns[6].Visible = false;
        gvClientAlertList.Columns[7].Visible = false;

        if (sINT1.Trim().Length > 0)
        {
            gvClientAlertList.Columns[4].Visible = true;
            gvClientAlertList.Columns[4].HeaderText = sINT1.Trim();
        }
        if (sINT2.Trim().Length > 0)
        {
            gvClientAlertList.Columns[5].HeaderText = sINT2.Trim();
            gvClientAlertList.Columns[5].Visible = true;
        }
        if (sMONEY1.Trim().Length > 0)
        {
            gvClientAlertList.Columns[6].HeaderText = sMONEY1.Trim();
            gvClientAlertList.Columns[6].Visible = true;
        }
        if (sVARCHAR1.Trim().Length > 0)
        {
            gvClientAlertList.Columns[7].HeaderText = sVARCHAR1.Trim();
            gvClientAlertList.Columns[7].Visible = true;
        }
    }

    protected DataTable getPages(int nbPages)
    {
        DataTable dt = new DataTable();

        dt.Columns.Add("L");
        dt.Columns.Add("V");

        for (int i = 1; i <= nbPages; i++)
        {
            dt.Rows.Add(i.ToString(), i.ToString());
        }

        return dt;
    }

    protected int CalculateTotalPages(double totalRows, int pageSize)
    {
        int totalPages = (int)Math.Ceiling(totalRows / pageSize);

        return totalPages;
    }
    protected void PaginationManagement(string sRowCount, string sXml)
    {
        if (int.Parse(sRowCount) > int.Parse(ddlNbResult.SelectedValue.ToString()))
        {

            int selectedPage = ddlPage.SelectedIndex;

            DataTable dtPages = getPages(CalculateTotalPages(int.Parse(sRowCount), int.Parse(ddlNbResult.SelectedValue)));
            ddlPage.DataSource = dtPages;
            ddlPage.DataBind();

            string sPageIndex = ""; int iPageIndex = 0;
            sPageIndex = tools.GetValueFromXml(sXml, "ALL_XML_IN/Registration", "PageIndex");
            if (sPageIndex.Trim().Length > 0 && int.TryParse(sPageIndex, out iPageIndex))
            {
                if (ddlPage.Items.Count > 0 && ddlPage.Items.FindByValue(iPageIndex.ToString()) != null)
                    ddlPage.Items.FindByValue(iPageIndex.ToString()).Selected = true;
            }
            else if (Session["RegistrationPageIndex"] != null)
            {
                if (ddlPage.Items.Count > 0)
                    ddlPage.Items.FindByValue(Session["RegistrationPageIndex"].ToString()).Selected = true;
            }
            else if (ddlPage.Items.Count > selectedPage)
                ddlPage.SelectedIndex = selectedPage;

            if (int.Parse(ddlPage.SelectedValue) == ddlPage.Items.Count)
                btnNextPage.Enabled = false;
            else
                btnNextPage.Enabled = true;

            if (int.Parse(ddlPage.SelectedValue) == 1)
                btnPreviousPage.Enabled = false;
            else
                btnPreviousPage.Enabled = true;

            if (ddlPage.Items.Count > 1)
                divPagination.Visible = true;
            else
                divPagination.Visible = false;

            lblTotalPage.Text = " / " + ddlPage.Items.Count.ToString();


        }
        else
        {
            divPagination.Visible = false;
        }
    }

    protected void nextPage(object sender, EventArgs e)
    {
        int iNbPage;

        int.TryParse(ddlPage.SelectedValue, out iNbPage);
        iNbPage++;
        if (ddlPage.Items.FindByValue(iNbPage.ToString()) != null)
            ddlPage.SelectedValue = iNbPage.ToString();

        bindAlertGrid();
    }
    protected void previousPage(object sender, EventArgs e)
    {
        int iNbPage;

        int.TryParse(ddlPage.SelectedValue, out iNbPage);
        iNbPage--;
        if (ddlPage.Items.FindByValue(iNbPage.ToString()) != null)
            ddlPage.SelectedValue = iNbPage.ToString();

        bindAlertGrid();
    }
    protected void onChangePageNumber(object sedner, EventArgs e)
    {
        bindAlertGrid();
    }
    protected void onChangeNbResult(object sedner, EventArgs e)
    {
        bindAlertGrid();
    }
}