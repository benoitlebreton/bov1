﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using XMLMethodLibrary;

public partial class API_CardsHistory : System.Web.UI.UserControl
{
    public int iRefCustomer
    {
        get
        {
            int _iRefCustomer = 0;
            int.TryParse((ViewState["iRefCustomer"] != null) ? ViewState["iRefCustomer"].ToString() : "0", out _iRefCustomer);
            return _iRefCustomer;
        }
        set { ViewState["iRefCustomer"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
            BindCardHistoryList(GetCardHistoryList(authentication.GetCurrent().sToken, iRefCustomer.ToString()));
    }

    protected void BindCardHistoryList(DataTable dt)
    {
        panelCardsHistoryList.Visible = true;
        panelCardsHistoryListEmpty.Visible = false;
        if (dt.Rows.Count > 0)
        {
            rptCardsHisto.DataSource = dt;
            rptCardsHisto.DataBind();
        }
        else
        {
            panelCardsHistoryList.Visible = false;
            panelCardsHistoryListEmpty.Visible = true;
        }
    }
    protected DataTable GetCardHistoryList(string sToken, string sRefCustomer)
    {
        SqlConnection conn = null;
        DataTable dt = new DataTable();
        dt.Columns.Add("CardNumber");
        dt.Columns.Add("Status");
        dt.Columns.Add("ExpirationDate");
        dt.Columns.Add("PaymentLimit");
        dt.Columns.Add("PaymentPeriod");
        dt.Columns.Add("WithdrawMoneyLimit");
        dt.Columns.Add("WithdrawMoneyPeriod");

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("Web.P_GetCustomerCardsInfo", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@ReturnSelect", SqlDbType.Bit);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                                new XElement("Cards",
                                                    new XAttribute("CashierToken", sToken),
                                                    new XAttribute("RefCustomer", sRefCustomer),
                                                    new XAttribute("Culture", "fr-FR"))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
            cmd.Parameters["@ReturnSelect"].Value = 0;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if (sXmlOut.Length > 0)
            {
                List<string> listCard = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/Cards/Card");

                for (int i = 0; i < listCard.Count; i++)
                {
                    List<string> listCardNumber = CommonMethod.GetAttributeValues(listCard[i], "Card", "CardNumber");
                    List<string> listCardStatus = CommonMethod.GetAttributeValues(listCard[i], "Card", "CardStatus");
                    List<string> listCardExpireDate = CommonMethod.GetAttributeValues(listCard[i], "Card", "CardExpireDate");
                    List<string> listPaymentLimit = CommonMethod.GetAttributeValues(listCard[i], "Card", "PaymentLimit");
                    List<string> listPaymentPeriod = CommonMethod.GetAttributeValues(listCard[i], "Card", "PaymentPeriod");
                    List<string> listWithdrawMoneyLimit = CommonMethod.GetAttributeValues(listCard[i], "Card", "WithdrawMoneyLimit");
                    List<string> listWithdrawMoneyPeriod = CommonMethod.GetAttributeValues(listCard[i], "Card", "WithdrawMoneyPeriod");

                    dt.Rows.Add(new object[] { ((listCardNumber.Count > 0) ? listCardNumber[0] : ""), ((listCardStatus.Count > 0) ? listCardStatus[0] : ""), ((listCardExpireDate.Count > 0) ? listCardExpireDate[0] : ""), ((listPaymentLimit.Count > 0) ? listPaymentLimit[0] : ""), ((listPaymentPeriod.Count > 0) ? listPaymentPeriod[0] : ""), ((listWithdrawMoneyLimit.Count > 0) ? listWithdrawMoneyLimit[0] : ""), ((listWithdrawMoneyPeriod.Count > 0) ? listWithdrawMoneyPeriod[0] : "") });
                }
            }
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }
}
