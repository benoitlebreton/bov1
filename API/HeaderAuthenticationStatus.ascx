﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HeaderAuthenticationStatus.ascx.cs" Inherits="API_HeaderAuthenticationStatus" %>

<script type="text/javascript" language="javascript">

    /*function ButtonKeyPress() {
        alert("enter pressed");

        var code = (window.event.which) ? window.event.which : window.event.keyCode;
        if (code == 13) {
            event.returnValue = false;
            return false;
        }
        return true;
    }*/

</script>

<div>
    <asp:Panel runat="server" ID="divStatus" Visible="true" style="font-weight:bold; color:#f57527"> 
        <asp:Label runat="server" ID="lblStatus"></asp:Label>
    </asp:Panel>
    <div style="display:table;text-align:right; width:100%; font-weight:bold; color:#f57527; ">
        <div id="divAgency" runat="server" style="display:table-row" visible="false">
            <div style="display:table-cell;text-align:left;color:#344b56; width:70px">
                <asp:Label ID="lblAgency" runat="server" Visible="false"><%= Resources.res.Agency %> : </asp:Label>
            </div>
            <div style="display:table-cell;text-align:right;width:190px">
                <asp:Label ID="lblAgencyName" runat="server" Visible="false" CssClass="trimmer" style="width:200px"/>  
            </div>
        </div>
        <div id="divAgent" runat="server" style="display:table-row" visible="false">
            <div style="display:table-cell;text-align:left;color:#344b56; width:70px">
                <asp:Label ID="lblAgent" runat="server" Visible="false"><%= Resources.res.Agent %> : </asp:Label>
            </div>
            <div style="display:table-cell;text-align:right;width:190px">
                <asp:Label ID="lblAgentName" runat="server" CssClass="trimmer" style="width:200px; color:#344b56;float:right" />
                <div style="clear:both"></div>
                <div id="divChangePassword" runat="server" style="margin:5px auto">
                    <a href="ChangePassword.aspx">changer votre mot de passe</a>
                </div>
            </div>
        </div>
    </div>
    <asp:Panel runat="server" ID="divDisconnect" Visible="false" style="margin-top:10px; text-align:right">
            <input type="button" style="display:none" />
            <asp:LinkButton runat="server" ID="btnDisconnect" OnClick="onClickDisconnect" AccessKey="9" CssClass="Linkbutton" />
    </asp:Panel>
    <asp:HiddenField ID="hfIsAuthenticated" runat="server" />
</div>