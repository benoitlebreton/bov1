﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class API_SpecificityHistory : System.Web.UI.UserControl
{
    public int iRefCustomer
    {
        get
        {
            int _iRefCustomer = 0;
            int.TryParse((ViewState["iRefCustomer"] != null) ? ViewState["iRefCustomer"].ToString() : "0", out _iRefCustomer);
            return _iRefCustomer;
        }
        set { ViewState["iRefCustomer"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
            BindClientSpecificities();
    }

    protected bool IsSpecificityDeleted(string sIsDeleted)
    {
        if (sIsDeleted.Trim().ToLower() == "true")
            return true;

        return false;
    }
    protected string GetSpecificityStatus(string sIsDeleted)
    {
        if (sIsDeleted.Trim().ToLower() == "true")
            return "ClientSpecificityDeleted";

        return "";
    }

    protected void BindClientSpecificities()
    {
        DataTable dtClientSpecificities = AML.getClientSpecifities(iRefCustomer, true);
        DataTable dtSpecificityList = AML.getSpecificityList();
        DataTable dtSpecificityListFiltered = dtSpecificityList;

        rptAML_ClientSpecificities.Visible = true;
        panel_ClientSpecificitiesNoResult.Visible = false;
        rptAML_ClientSpecificities.DataSource = dtClientSpecificities;
        rptAML_ClientSpecificities.DataBind();
        if (rptAML_ClientSpecificities.Items.Count == 0)
        {
            rptAML_ClientSpecificities.Visible = false;
            panel_ClientSpecificitiesNoResult.Visible = true;
        }

        string sRefSpecificityCurrent = "";
        string sRefSpecificity = "";
        for (int i = 0; i < dtClientSpecificities.Rows.Count; i++)
        {
            if (!IsSpecificityDeleted(dtClientSpecificities.Rows[i]["IsDeleted"].ToString()))
            {
                sRefSpecificityCurrent = dtClientSpecificities.Rows[i]["RefSpecificity"].ToString().Trim();
                for (int j = 0; j < dtSpecificityListFiltered.Rows.Count; j++)
                {
                    sRefSpecificity = dtSpecificityListFiltered.Rows[j]["RefSpecificity"].ToString().Trim();
                    if (sRefSpecificity == sRefSpecificityCurrent)
                    {
                        dtSpecificityListFiltered.Rows[j].Delete();
                        dtSpecificityListFiltered.AcceptChanges();
                    }
                }
            }
        }

        Panel panel = (Panel)rptAML_ClientSpecificities.Controls[rptAML_ClientSpecificities.Controls.Count - 1].Controls[0].FindControl("panelShowDelClientSpecificities");
        if (panel != null)
        {
            if (getNbClientSpecificities(dtClientSpecificities, true) < dtClientSpecificities.Rows.Count)
                panel.Visible = true;
            else
                panel.Visible = false;
        }
    }

    protected int getNbClientSpecificities(DataTable dt, bool isDeleted)
    {
        int Nb = 0;

        if (isDeleted)
        {
            foreach (DataRow dr in dt.Rows)
                if (dr["IsDeleted"].ToString().Trim().ToLower() == "true")
                    Nb++;
        }
        else
        {
            foreach (DataRow dr in dt.Rows)
                if (dr["IsDeleted"].ToString().Trim().ToLower() != "true")
                    Nb++;
        }

        return Nb;
    }
}