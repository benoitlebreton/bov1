﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using XMLMethodLibrary;

public partial class API_ClientFileManager : System.Web.UI.UserControl
{
    public int RefCustomer { get { return (ViewState["RefCustomer"] != null) ? int.Parse(ViewState["RefCustomer"].ToString()) : 0; } set { ViewState["RefCustomer"] = value; } }
    private DataTable _dtDocumentType { get { return (Session["DocumentType"] != null) ? (DataTable)Session["DocumentType"] : null; } set { Session["DocumentType"] = value; } }
    private string _sClientDirPath { get { return (ViewState["ClientDirPath"] != null) ? ViewState["ClientDirPath"].ToString() : ""; } set { ViewState["ClientDirPath"] = value; } }
    private DataTable _dtClientFiles { get { return (Session["ClientFiles"] != null) ? (DataTable)Session["ClientFiles"] : null; } set { Session["ClientFiles"] = value; } }
    public bool AutoLoad { get; set; }
    public bool AllowSelect { get { return (ViewState["AllowSelect"] != null) ? bool.Parse(ViewState["AllowSelect"].ToString()) : false; } set { ViewState["AllowSelect"] = value; } }
    public bool AllowCheck { get { return (ViewState["AllowCheck"] != null) ? bool.Parse(ViewState["AllowCheck"].ToString()) : false; } set { ViewState["AllowCheck"] = value; } }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (RefCustomer != 0 && AutoLoad)
            {
                LoadData();
            }
        }
    }

    public void LoadData()
    {
        BindInputs();
        BindClientFiles();
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "InitInputFiles();InitCopyToClipboard();", true);
    }

    protected void BindInputs()
    {
        string sCustomerDirPath = ConfigurationManager.AppSettings["NickelDocPath"].ToString();
        string sCustomerWebDirName = tools.GetCustomerWebDirectoryName(RefCustomer);
        _sClientDirPath = "~" + sCustomerDirPath + "/" + sCustomerWebDirName;

        _dtDocumentType = ClientFile.GetDocumentType();
        ddlDocumentTypeFilter.Items.Clear();
        ddlDocumentTypeFilter.Items.Add(new ListItem("", ""));
        ddlDocumentTypeFilter.DataSource = _dtDocumentType;
        ddlDocumentTypeFilter.DataBind();
        //upClientFile.Update();
    }

    protected void BindClientFiles()
    {
        _dtClientFiles = GetClientFilesList(authentication.GetCurrent().sToken, RefCustomer, ddlDocumentTypeFilter.SelectedValue);
        rptClientFile.DataSource = _dtClientFiles;
        rptClientFile.DataBind();
        upClientFile.Update();
    }

    protected DataTable GetClientFilesList(string sToken, int iRefCustomer, string sDocCategoryTAG)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("FileName");
        dt.Columns.Add("CategoryTAG");
        dt.Columns.Add("CategoryLabel");
        dt.Columns.Add("ContentType");
        dt.Columns.Add("LocalStorage", typeof(bool));
        dt.Columns.Add("URL");
        dt.Columns.Add("Description");
        dt.Columns.Add("LastUploadDate");
        dt.Columns.Add("StorePath");
        dt.Columns.Add("CheckFile");
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("Web.P_GetCustomerStoreDocuments", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@sXmlIn", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@sXmlOut", SqlDbType.VarChar, -1);

            XElement xmlDoc = new XElement("Doc",
                                new XAttribute("CashierToken", sToken),
                                new XAttribute("RefCustomer", iRefCustomer.ToString()));
            if (sDocCategoryTAG.Length > 0)
                xmlDoc.Add(new XAttribute("DocCategoryTAG", sDocCategoryTAG));

            cmd.Parameters["@sXmlIn"].Value = new XElement("ALL_XML_IN",
                                            xmlDoc).ToString();
            cmd.Parameters["@sXmlOut"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@sXmlOut"].Value.ToString();

            if (sXmlOut.Length > 0)
            {
                List<string> listDoc = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/Doc");

                for (int i = 0; i < listDoc.Count; i++)
                {
                    List<string> listFileName = CommonMethod.GetAttributeValues(listDoc[i], "Doc", "StoreFileName");
                    List<string> listStorePath = CommonMethod.GetAttributeValues(listDoc[i], "Doc", "StorePath");
                    List<string> listDocCatTAG = CommonMethod.GetAttributeValues(listDoc[i], "Doc", "DocCategoryTAG");
                    List<string> listContentType = CommonMethod.GetAttributeValues(listDoc[i], "Doc", "ContentType");
                    List<string> listDescription = CommonMethod.GetAttributeValues(listDoc[i], "Doc", "Description");
                    List<string> listLastUploadDate = CommonMethod.GetAttributeValues(listDoc[i], "Doc", "LastUploadDate");
                    List<string> listCheckFile = CommonMethod.GetAttributeValues(listDoc[i], "Doc", "CheckFile");
                    bool bLocalStorage = File.Exists(HostingEnvironment.MapPath(_sClientDirPath + "/" + listFileName[0]));

                    dt.Rows.Add(new object[] { listFileName[0], listDocCatTAG[0], GetDocumentTypeLabel(listDocCatTAG[0]), listContentType[0], bLocalStorage, _sClientDirPath + "/" + listFileName[0], listDescription[0], listLastUploadDate[0], listStorePath[0], listCheckFile[0] });
                }
            }
        }
        catch(Exception e)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }

    protected string GetDocumentTypeLabel(string sTag)
    {
        string sLabel = "";

        if (_dtDocumentType != null)
            for (int i = 0; i < _dtDocumentType.Rows.Count; i++)
            {
                if (_dtDocumentType.Rows[i]["TAG"].ToString() == sTag)
                {
                    sLabel = _dtDocumentType.Rows[i]["Label"].ToString();
                    break;
                }
            }

        return sLabel;
    }

    protected void ddlDocumentType_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindClientFiles();
    }

    protected void btnCloudDownload_Click(object sender, EventArgs e)
    {
        Button btn = (Button)sender;

        if (btn != null)
        {
            string sFileName = btn.CommandArgument;

            for (int i = 0; i < 4; i++)
            {
                if (ClientFile.GetFileFromCloudStorage(authentication.GetCurrent().sToken, RefCustomer, _sClientDirPath, sFileName))
                    break;
            }

            for (int i = 0; i < 5; i++)
            {
                System.Threading.Thread.Sleep(1000);
                if (File.Exists(HostingEnvironment.MapPath(_sClientDirPath + "/" + sFileName)))
                    break;
            }
        }

        BindClientFiles();
    }

    public void UpdateClientFiles()
    {
        BindClientFiles();
    }

    public DataTable GetSelectedFiles()
    {
        DataTable dt = null;

        if (_dtClientFiles != null && _dtClientFiles.Rows.Count > 0)
        {
            dt = _dtClientFiles.Clone();
            dt.Columns.Add("RefCustomer");

            foreach (RepeaterItem item in rptClientFile.Items)
            {
                if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                {
                    CheckBox ckb = item.FindControl("ckbSelect") as CheckBox;

                    if (ckb != null && ckb.Checked)
                    {
                        if (_dtClientFiles.Rows.Count > item.ItemIndex)
                        {
                            dt.Rows.Add(_dtClientFiles.Rows[item.ItemIndex].ItemArray);
                            dt.Rows[dt.Rows.Count - 1]["RefCustomer"] = RefCustomer;
                        }
                    }
                }
            }

        }

        return dt;
    }

    protected void btnDecline_Click(object sender, EventArgs e)
    {
        Button btn = (Button)sender;

        if (!string.IsNullOrWhiteSpace(hfFileNameToCheck.Value))
        {
            string sFileName = hfFileNameToCheck.Value;
            if (ClientFile.SetDocumentChecked(authentication.GetCurrent(), RefCustomer, sFileName, false))
            {
                showMessage("Document refusé", "<span style=\"color:black\">Traitement effectué avec succès.</span>");
            }
            else { showMessage("Document refusé", "<span style=\"color:red;\">Une erreur est survenue.</span>"); }
        }
        else { showMessage("Document refusé", "<span style=\"color:red;\">Une erreur est survenue.</span>"); }

        BindClientFiles();
    }

    protected void btnAccept_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(hfFileNameToCheck.Value))
        {
            string sFileName = hfFileNameToCheck.Value;

            if (ClientFile.SetDocumentChecked(authentication.GetCurrent(), RefCustomer, sFileName, true))
            {
                showMessage("Document accepté", "<span style=\"color:black\">Traitement effectué avec succès.</span>");
            }
            else { showMessage("Document accepté", "<span style=\"color:red;\">Une erreur est survenue.</span>"); }
        }
        else { showMessage("Document accepté", "<span style=\"color:red;\">Une erreur est survenue.</span>"); }

        BindClientFiles();
    }

    protected void showMessage(string sTitle, string sMessage)
    {
        //ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "AlertMessage('"+sTitle+"', '"+sMessage+"'); HidePanelLoading();", true);
        hfDocsMessages.Text = "<div id=\"hfDocsMessages\">" + sTitle + '|' + sMessage + "</div>";
    }

    protected string GetInput(string sFilename, bool bAccept)
    {
        string sInput = "";

        if (!string.IsNullOrWhiteSpace(sFilename)) {
            if (bAccept)
            {
                sInput = "<input type=\"button\" class=\"green-button\" value=\"Accepter\" onclick=\"AcceptFile('" + sFilename + "');\" />";
            }
            else
            {
                sInput = "<input type=\"button\" class=\"red-button\" value=\"Refuser\" onclick=\"DeclineFile('" + sFilename + "');\" />";
            }
        }
        return sInput;
    }
}