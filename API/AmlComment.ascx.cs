﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class API_AmlComment : System.Web.UI.UserControl
{
    public int MaxLength { get { return (ViewState["MaxLength"] != null) ? int.Parse(ViewState["MaxLength"].ToString()) : 0; } set { ViewState["MaxLength"] = value; } }
    public string Commentary { get { return txtComment.Text; } set { txtComment.Text = value; } }
    public string Label { get { return lblTextLabel.Text; } set { lblTextLabel.Text = value; } }
    public bool ReadOnly { set { txtComment.ReadOnly = value; } get { return txtComment.ReadOnly; } }
    public bool Disabled { set { if (value) txtComment.Attributes["disabled"] = "disabled"; else txtComment.Attributes.Remove("disabled"); } get { return (txtComment.Attributes["disabled"] != null && txtComment.Attributes["disabled"] == "disabled") ? true : false; } }
    public int Height { set { txtComment.Height = new Unit(value, UnitType.Pixel); } get { return (int)txtComment.Height.Value; } }

    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            if (MaxLength != -1)
            {
                txtComment.Attributes["maxlength"] = MaxLength.ToString();
                lblMaxChar.Text = MaxLength.ToString();
                ScriptManager.RegisterClientScriptInclude(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "Scripts/AmlComment.js?v=20161005");
            }
            else
            {
                panelCharCounter.Visible = false;
            }
            Page.Header.Controls.Add(new System.Web.UI.LiteralControl("<link rel=\"stylesheet\" type=\"text/css\" href=\"" + ResolveUrl("~/Styles/AmlComment.css") + "?v=20161005\" />"));
        }
    }

    public void BindCommentLengthCheck()
    {
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "BindCommentLengthCheck('" + txtComment.ClientID + "', '" + lblNbChar.ClientID + "');", true);
    }

    public void CommentaryFocus()
    {
        txtComment.Focus();
    }
}