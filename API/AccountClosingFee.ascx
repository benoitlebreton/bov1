﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AccountClosingFee.ascx.cs" Inherits="API_AccountClosingFee" %>

<asp:UpdatePanel ID="upFees" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div>
            <asp:Repeater ID="rptFees" runat="server">
                <ItemTemplate>
                    <div class="multi-item <%# Container.ItemIndex % 2 == 0 ? "even-row" : "odd-row" %>">
                        <div class="multi-row-data">
                            <asp:Literal ID="litLabel" runat="server" Text='<%# String.Format("{0} : {1} €", Eval("Label"), Eval("Amount")) %>'></asp:Literal>
                            <asp:HiddenField ID="hdnTag" runat="server" Value='<%# Eval("Tag") %>' />
                        </div>
                        <div class="multi-row-btn">
                            <asp:ImageButton ID="btnRemove" runat="server" CssClass="minus" ImageUrl="~/Styles/Img/moins.png" OnClick="btnRemove_Click" />
                        </div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </div>
        <asp:Panel ID="panelFees" runat="server" DefaultButton="btnAdd" style="margin-top:5px">
            <div class="multi-row-data" style="font-size:0">
                <asp:DropDownList ID="ddlFee" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlFee_SelectedIndexChanged" DataTextField="Label" DataValueField="Tag" style="width:80%;box-sizing:border-box">
                </asp:DropDownList>
                <asp:TextBox ID="txtFeeAmount" runat="server" style="text-align:right;width: calc(20% - 24px);box-sizing: border-box;border: 1px solid #aaa;border-right-style: none;margin-left: 2px;"></asp:TextBox>
                <span style="display: inline-block;box-sizing: border-box;height: 21px;line-height: 20px;width: 22px;text-align:right;border: 1px solid #aaa;border-left-style: none;position: relative;top: 0px;left: -4px;font-size: 13.3333px;padding: 0px 4px 0 0;">€</span>
            </div>
            <div class="multi-row-btn">
                <asp:ImageButton ID="btnAdd" runat="server" ImageUrl="~/Styles/Img/plus.png" OnClick="btnAdd_Click" />
            </div>
            <div style="clear:both"></div>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>