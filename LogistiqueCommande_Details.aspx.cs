﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using XMLMethodLibrary;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Text;
using System.Net;
using System.ComponentModel;

public partial class LogistiqueCommande_Details : System.Web.UI.Page
{
    private string _sRefCommand
    {
        get { return ViewState["RefCommand"].ToString(); }
        set { ViewState["RefCommand"] = value; }
    }
    private long _lTotalCommande
    {
        get { return long.Parse(ViewState["TotalCommande"].ToString()); }
        set { ViewState["TotalCommande"] = value.ToString(); }
    }
    private long _lTotalLot
    {
        get { return long.Parse(ViewState["TotalLot"].ToString()); }
        set { ViewState["TotalLot"] = value.ToString(); }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.Params["RefCommand"] != null && Request.Params["RefCommand"].Length > 0)
            {
                _sRefCommand = Request.Params["RefCommand"].ToString();
                _lTotalCommande = 0;

                if (BindCommand() == false)
                    Response.Redirect("Default.aspx", true);
            }
        }
    }

    protected bool BindCommand()
    {
        int iIsOk = 0;
        DataTable dtLot = new DataTable();
        DataTable dtBon = new DataTable();

        _lTotalLot = 0;
        dtLot.Columns.Add("NumeroLot");
        dtLot.Columns.Add("QuantityLot");
        dtLot.Columns.Add("LivraisonDate");
        dtBon.Columns.Add("NumeroLot");
        dtBon.Columns.Add("BonReceptionDate");
        dtBon.Columns.Add("QuantityLot");
        dtBon.Columns.Add("LivraisonDate");
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NoBank"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_GetCommandCardDetails", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN"
                , new XElement("Command", new XAttribute("RefCommand", _sRefCommand)
                    ,new XAttribute("TOKEN", authentication.GetCurrent(false).sToken))
                ).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();
/*            string sXmlOut = "<ALL_XML_OUT>"
                + "<Command RefCommand=\"1\" CommandNumber=\"PO0001\" CommandQuantity =\"80000\" CommandDate=\"21/06/2017\" VisualType=\"Standard\" ValidationDate=\"04/06/2017\" PrixUnitaire=\"2\">"
                + "<CommandDetails NumeroLot=\"1\" QuantityLot=\"100\" BonReceptionDate=\"01/02/1976\" />"
                + "<CommandDetails NumeroLot=\"2\" QuantityLot=\"1000\" BonReceptionDate=\"01/09/1976\" />"
                + "</Command></ALL_XML_OUT>";*/

            List<string> listCommandQuantity = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Command", "CommandQuantity");
            if (listCommandQuantity.Count > 0)
            {
                _lTotalCommande = int.Parse(listCommandQuantity[0]);
                lblQuantity.Text = String.Format("{0:### ### ### ###}", _lTotalCommande).Trim();
            }
            else
            {
                _lTotalCommande = 0;
                lblQuantity.Text = "0";
            }

            List<string> listDateCommande = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Command", "CommandDate");
            if (listDateCommande.Count > 0)
            {
                lblDateCommand.Text = listDateCommande[0];
            }

            List<string> listVisuel = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Command", "VisualType");
            if (listVisuel.Count > 0)
            {
                lblVisuelType.Text = listVisuel[0];
                switch (listVisuel[0])
                {
                    case "VISTAND":
                        lblVisuelType.Text = "Standard";
                        break;
                    default:
                        lblVisuelType.Text = "";
                        break;
                }
            }
            List<string> listPrix = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Command", "PrixUnitaire");
            if (listPrix.Count > 0)
            {
                lblPrice.Text = listPrix[0] + "€";
            }

            List<string> listLot = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/Command/CommandDetails");
            if (listLot.Count > 0)
            {
                for (int i = 0; i < listLot.Count; i++)
                {
                    List<string> listNumeroLot = CommonMethod.GetAttributeValues(listLot[i], "CommandDetails", "NumeroLot");
                    List<string> listQuantityLot = CommonMethod.GetAttributeValues(listLot[i], "CommandDetails", "QuantityLot");
                    List<string> listLivraisonDate = CommonMethod.GetAttributeValues(listLot[i], "CommandDetails", "LivraisonDate");
                    List<string> listBonReceptionDate = CommonMethod.GetAttributeValues(listLot[i], "CommandDetails", "BonReceptionDate");

                    if (listNumeroLot.Count > 0 && listQuantityLot.Count > 0)
                    {
                        DataRow row = dtLot.NewRow();
                        row["NumeroLot"] = listNumeroLot[0];
                        row["QuantityLot"] = listQuantityLot[0];
                        row["LivraisonDate"] = listLivraisonDate[0];
                        dtLot.Rows.Add(row);
                        _lTotalLot += int.Parse(listQuantityLot[0]);
                    }

                    if (listNumeroLot.Count > 0 && listBonReceptionDate.Count > 0)
                    {
                        DataRow row = dtBon.NewRow();
                        row["NumeroLot"] = listNumeroLot[0];
                        row["BonReceptionDate"] = listBonReceptionDate[0];
                        row["QuantityLot"] = listQuantityLot[0];
                        row["LivraisonDate"] = listLivraisonDate[0];
                        dtBon.Rows.Add(row);
                    }
                }
                iIsOk++;
            }
            else
                iIsOk = 0;
        }
        catch (Exception ex)
        {
            iIsOk = 0;
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        if (dtLot.Rows.Count > 0)
        {
            rptLotList.DataSource = dtLot;
            rptLotList.DataBind();
            rptLotList.Visible = true;

            rptBonReception.DataSource = dtBon;
            rptBonReception.DataBind();
            rptBonReception.Visible = true;
            iIsOk++;
        }
        else
        {
            rptBonReception.Visible = false;
            rptLotList.Visible = false;
        }

        if (_lTotalCommande > _lTotalLot)
        {
            pEnvoiBon.Visible = true;
            hdnMaxAttendu.Value = (_lTotalCommande - _lTotalLot).ToString();
            lblQuantityAttendue.Text = "Quantité attendue (Max " + hdnMaxAttendu.Value + ")";
            iIsOk++;
        }
        else
        {
            pEnvoiBon.Visible = false;
            hdnMaxAttendu.Value = "0";
        }

        return (iIsOk >= 1);
    }

    protected string getLibelleLot(string sNumeroLot, string sQuantityLot, string sLivraisonDate)
    {
        return "Quantité " + sNumeroLot + " envoyée : " + sQuantityLot + " cartes, livraison estimée : " + sLivraisonDate;
    }

    protected string getLibelleBon(string sNumeroLot, string sBonReceptionDate)
    {
        return "Bon de réception " + sNumeroLot + " envoyé à Mobiltron le " + sBonReceptionDate;
    }

    protected void btnExtraction_Click(object sender, EventArgs e)
    {
        Button btn = (Button)sender;
        SqlConnection conn = null;
        DataTable dt = new DataTable();
        string sNumLot = "";

        try
        {
            if (btn != null)
            {
                string[] cmdArgs = btn.CommandArgument.Split(',');

                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NoBank"].ConnectionString);
                conn.Open();

                SqlCommand cmd = new SqlCommand("web.P_CommandCardExtractSerialNb", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@RefCommand", SqlDbType.Int);
                cmd.Parameters.Add("@RefLot", SqlDbType.Int);

                cmd.Parameters["@RefCommand"].Value = int.Parse(_sRefCommand);
                cmd.Parameters["@RefLot"].Value = int.Parse(cmdArgs[0]);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                cmd.ExecuteNonQuery();
                sNumLot = cmdArgs[0];
            }            

        }
        catch (Exception ex)
        {
            tools.LogToFile(ex.Message, "LogistiqueCommande_Details");
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        if(sNumLot != "")
        {
            ExtractionNumSerie(dt, sNumLot);            
        }
    }

    protected void ExtractionNumSerie(DataTable dt, string sNumeroLot)
    {
        string sExportsFilePath = ConfigurationManager.AppSettings["ExportsExcelFilePath"].ToString();
        string sFileName = "FPE_Extraction_NumSerie_Lot_" + sNumeroLot + "_" + DateTime.Now.ToString("yyyy-MM-dd_HH-mm") + ".csv";
        string sFullFileName = Server.MapPath("~" + sExportsFilePath + sFileName);

        DeleteOldFile(sExportsFilePath, "FPE_Extraction_NumSerie_Lot_", "*.csv");
        
        string sBarCode = "";
        string sProductId = "";
        string sName = "";
        string sDateFabrication = "";
        string sDateExpiration = "";

        using (TextWriter tr = new StreamWriter(sFullFileName))
        {
            tr.WriteLine("id;product_id;name;date;life_date");
            // data
            foreach (DataRow row in dt.Rows)
            {
                sBarCode = "";
                sProductId = "";
                sName = "";
                sDateFabrication = "";
                sDateExpiration = "";

                if (row.Field<string>("sBarCode") != null)
                    sBarCode = row.Field<string>("sBarCode").ToString();
                if (row.Field<string>("sProductId") != null)
                    sProductId = row.Field<string>("sProductId").ToString();
                if (row.Field<string>("sName") != null)
                    sName = row.Field<string>("sName").ToString();
                else
                    sName = sBarCode;
                if (row.Field<string>("sDateFabrication") != null)
                    sDateFabrication = row.Field<string>("sDateFabrication").ToString();
                if (row.Field<string>("sDateExpiration") != null)
                    sDateExpiration = row.Field<string>("sDateExpiration").ToString();

                tr.WriteLine("'" + sBarCode + ";"
                    + sProductId + ";"
                    + "'" + sName + ";"
                    + sDateFabrication + ";"
                    + sDateExpiration);
            }
        }
        
        StartDownload(sFileName, sFullFileName);
    }

    protected void DeleteOldFile(string sDirectory, string sFileTAG, string sExtension)
    {
        DirectoryInfo di = new DirectoryInfo(Request.MapPath("~" + sDirectory));
        FileInfo[] Files = di.GetFiles(sExtension);

        foreach (FileInfo file in Files)
        {
            if (file.Name.StartsWith(sFileTAG) == true)
                File.Delete(file.FullName);
        }
    }

    protected void StartDownload(string sFileName, string sFullFileName)
    {
        /*
        try
        {
            string appDataPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string filePath = Path.Combine(appDataPath, sFileName);
            WebClient wc = new WebClient();
            wc.DownloadFile(sFullFileName, filePath);
            wc.Dispose();
            File.Delete(sFullFileName);
            AlertMessage("Extraction finie", "Le fichier " + sFileName + " a été téléchargé sur votre bureau");
        }
        catch (Exception ex)
        {
            AlertMessage("Erreur", ex.Message);
        }*/
        string sExportsExcelPath = ConfigurationManager.AppSettings["ExportsExcelFilePath"].ToString();
        //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "DownloadFile('" + sFullFileName.Replace("\\", "/") + "');", true);
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "DownloadFile('." + sExportsExcelPath + sFileName + "');", true);
        //Response.Write("<script>window.open("+ sFullFileName + ")</script>");

        
    }

    protected void AlertMessage(string sTitle, string sMessage)
    {
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('" + sTitle.Replace("'", "&#39;") + "', '" + sMessage.Replace("'", "&#39;") + "');", true);
    }

    protected void btnRenvoyer_Click(object sender, EventArgs e)
    {
        Button btn = (Button)sender;

        if (btn != null)
        {
            string[] cmdArgs = btn.CommandArgument.Split(',');
            generationBonReception(cmdArgs[0], cmdArgs[1], cmdArgs[2]);
        }        
    }

    protected void btnEnvoyer_Click(object sender, EventArgs e)
    {
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NoBank"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_AddBonReception", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN"
                , new XElement("Command"
                    , new XAttribute("RefCommand", _sRefCommand)
                    , new XAttribute("Quantity", txtQuantity.Text)
                    , new XAttribute("EstimatedDate", txtDateLivraison.Text)
                    , new XAttribute("TOKEN", authentication.GetCurrent(false).sToken))
                ).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/RESULT", "RC");

            if (listRC.Count > 0 && listRC[0] == "0")
            {
                List<string> listNumLot = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/RESULT", "NumeroLot");
                if (listNumLot.Count > 0)
                {
                    generationBonReception(listNumLot[0], txtQuantity.Text, txtDateLivraison.Text);
                }
                BindCommand();
            }
        }
        catch(Exception ex)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }
    }

    protected void generationBonReception(string sNumeroLot, string sQuantity, string sLivraisondate)
    {
        string sExportsExcelFilePath = ConfigurationManager.AppSettings["ExportsExcelFilePath"].ToString();
        string sFilename = "FPE_CdesFournisseur_C_" + DateTime.Now.ToString("yyyy-MM-dd_HH-mm") + ".csv";
        DeleteOldFile(sExportsExcelFilePath, "FPE_CdesFournisseur_C_", "*.csv");
        using (TextWriter tr = new StreamWriter(Server.MapPath("~/" + sExportsExcelFilePath + sFilename)))
        {
            tr.WriteLine("NUM_CDE_CLIENT;NUM_LIGNE_CLIENT;CODE_PRODUIT;QTE_ATTENDUE;DATE_LIVRAISON_ATTENDUE;FOURNISSEUR;GENCODE;OU;EAN;COMMENTAIRE;PHOTO");
            // data
            tr.WriteLine("GEMALTO" + DateTime.Now.ToString("ddMMyy") + ";'" + "1" + ";" + "CarteNickelV1" + ";'" + sQuantity + ";" + sLivraisondate + ";" + "Gemalto" + ";" + ";" + ";" + ";" + ";");
        }
        StartDownload(sFilename, Server.MapPath("~/" + sExportsExcelFilePath + sFilename));
    }
    
}