﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;

public partial class ClientAuthorizations_Frame : System.Web.UI.Page
{
    private string _sClientBankAccount
    {
        get { return ViewState["ClientBankAccount"].ToString(); }
        set { ViewState["ClientBankAccount"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("fr-FR");
        Thread.CurrentThread.CurrentUICulture = new CultureInfo("fr-FR");
        ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
        scriptManager.RegisterPostBackControl(this.btnExcelExport);

        panelAuthorizationListTable.Visible = true;
        lblDateNow.Text = DateTime.Now.ToString("dd MMMM yyyy");

        if (!IsPostBack)
        {
            if (Request.Params["ref"] != null && Request.Params["ref"].Length > 0)
            {
                _sClientBankAccount = Request.Params["ref"].ToString();
                //rptAuthorizationList.DataSource = operation.getUserAuthorizationList(_sClientBankAccount.Trim(), 1, 100, "", "", "ALL");
                //rptAuthorizationList.DataBind();

                operation ope = new operation();
                int iSize = 15;
                int iStep = 1;
                operation.SearchCriteria searchCriteria = new operation.SearchCriteria(iSize, iStep, "", "", _sClientBankAccount, "ALL", true);
                UpdateOperationList(searchCriteria);
                searchCriteria.bForceRefresh = false;
                ViewState["SearchCriteria"] = searchCriteria;

                if (rptAuthorizationList.Items.Count > 0)
                {
                    panelNoSearchResult.Visible = false;
                    panelAuthorizationListTable.Visible = true;
                }
                else
                {
                    panelNoSearchResult.Visible = true;
                    panelAuthorizationListTable.Visible = false;
                }
            }
            else
            {
                panelAuthorizationListTable.Visible = false;
            }

        }
    }

    protected void rptAuthorizationList_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
    }

    protected void btnShowMore_Click(object sender, EventArgs e)
    {
        int iSize = 15;
        int iStep = 1;
        operation ope = new operation();
        int iRowCount;

        if (ViewState["SearchCriteria"] != null)
        {
            operation.SearchCriteria searchCriteria = (operation.SearchCriteria)ViewState["SearchCriteria"];
            iSize = searchCriteria.iPageSize;
            iStep = searchCriteria.iPageStep;
            iSize += iSize;
            searchCriteria.iPageSize = iSize;

            ViewState["SearchCriteria"] = searchCriteria;
            UpdateOperationList(searchCriteria);
        }
        else
        {
            operation.SearchCriteria searchCriteria = new operation.SearchCriteria(iSize, iStep, "", "", _sClientBankAccount);

            UpdateOperationList(searchCriteria);
            UpdateSearchCriteria(searchCriteria);

            ViewState["SearchCriteria"] = searchCriteria;
        }
    }

    protected void UpdateSearchCriteria(operation.SearchCriteria searchCriteria)
    {
        DateTime date;
        string sDateFrom = "";
        string sDateTo = "";
        panelNoFilters.Visible = false;
        panelFilters.Visible = true;
        string sFiltersLabel = "Autorisations/Refus ";

        if (DateTime.TryParse(searchCriteria.sDateFrom, out date))
            sDateFrom = date.ToString("dd MMMM yyyy");
        if (DateTime.TryParse(searchCriteria.sDateTo, out date))
            sDateTo = date.ToString("dd MMMM yyyy");

        string sAuthorizationType = ddlAuthorizationType.Items.FindByValue(searchCriteria.sOperationType).Text;

        if (sAuthorizationType.Length > 0 && sAuthorizationType != "TOUS")
            sFiltersLabel += ": <span class=\"font-bold uppercase\">" + sAuthorizationType + "</span> ";

        if (sDateFrom.Length > 0 && sDateTo.Length > 0)
        {
            sFiltersLabel += "du <span class=\"font-bold\"  style=\"text-transform:uppercase\">" + sDateFrom + "</span> au <span class=\"font-bold\"  style=\"text-transform:uppercase\">" + sDateTo + "</span>";
        }
        else
        {
            if (sDateFrom.Length > 0)
            {
                sFiltersLabel += "depuis le <span class=\"font-bold uppercase\">" + sDateFrom + "</span>";
            }
            else if (sDateTo.Length > 0)
            {
                sFiltersLabel += "au <span class=\"font-bold uppercase\">" + sDateTo + "</span>";
            }
            else
            {
                sFiltersLabel += "au <span class=\"font-bold uppercase\">" + DateTime.Now.ToString("dd MMMM yyyy") + "</span>, à <span class=\"lblClientTime font-bold\"></span>";
            }
        }

        ltlFilters.Text = sFiltersLabel;

        /*if (sDateFrom.Length == 0 && sDateTo.Length == 0 && sPaymentMethod.Length == 0)
        {
            panelFilters.Visible = false;
            panelNoFilters.Visible = true;
        }*/
    }
    protected void UpdateNbPages(int iPageSize, int iNbTotal)
    {
        ltlNbResults.Text = "<span class=\"font-bold\">" + iPageSize.ToString() + "</span>/<span class=\"font-bold\">" + iNbTotal.ToString() + "</span>";
    }

    protected void UpdateOperationList(operation.SearchCriteria searchCriteria)
    {
        operation ope = new operation();
        int iRowCount;

        DataTable dt = operation.getUserAuthorizationList(searchCriteria, out iRowCount);
        if (iRowCount == dt.Rows.Count)
        {
            btnShowMore.Visible = false;
            UpdateNbPages(iRowCount, iRowCount);
        }
        else
        {
            btnShowMore.Visible = true;
            UpdateNbPages(searchCriteria.iPageSize, iRowCount);
        }

        if (dt.Rows.Count > 0)
        {
            rptAuthorizationList.Visible = true;
            panelNoSearchResult.Visible = false;
            rptAuthorizationList.DataSource = dt;
            rptAuthorizationList.DataBind();
        }
        else
        {
            rptAuthorizationList.Visible = false;
            panelNoSearchResult.Visible = true;
            rptAuthorizationList.DataSource = null;
            rptAuthorizationList.DataBind();
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        if (ViewState["SearchCriteria"] != null)
        {
            operation.SearchCriteria searchCriteria = (operation.SearchCriteria)ViewState["SearchCriteria"];
            DateTime date;
            if (txtDateFrom.Text.Length > 0 && DateTime.TryParse(txtDateFrom.Text, out date))
                searchCriteria.sDateFrom = txtDateFrom.Text;
            else searchCriteria.sDateFrom = "";
            if (txtDateTo.Text.Length > 0 && DateTime.TryParse(txtDateTo.Text, out date))
                searchCriteria.sDateTo = txtDateTo.Text;
            else searchCriteria.sDateTo = "";
            searchCriteria.iPageSize = 50;
            searchCriteria.iPageStep = 50;
            searchCriteria.sOperationType= ddlAuthorizationType.SelectedValue;

            UpdateOperationList(searchCriteria);

            ViewState["SearchCriteria"] = searchCriteria;

            UpdateSearchCriteria(searchCriteria);
        }
    }

    protected void btnExportExcel_Click(object sender, EventArgs e)
    {
        if (ViewState["SearchCriteria"] != null)
        {
            int iRowCount;
            operation ope = new operation();
            operation.SearchCriteria searchCriteria = (operation.SearchCriteria)ViewState["SearchCriteria"];
            DateTime date;
            if (txtDateFrom.Text.Length > 0 && DateTime.TryParse(txtDateFrom.Text, out date))
                searchCriteria.sDateFrom = txtDateFrom.Text;
            else searchCriteria.sDateFrom = "";
            if (txtDateTo.Text.Length > 0 && DateTime.TryParse(txtDateTo.Text, out date))
                searchCriteria.sDateTo = txtDateTo.Text;
            else searchCriteria.sDateTo = "";
            searchCriteria.iPageSize = -1;
            searchCriteria.iPageStep = -1;
            searchCriteria.sOperationType = ddlAuthorizationType.SelectedValue;
            searchCriteria.sOutputFormat = "EXCEL";
            ViewState["SearchCriteria"] = searchCriteria;
            //UpdateSearchCriteria(searchCriteria);

            DataTable dtExcelExport = new DataTable();
            dtExcelExport.Columns.Add("Date");
            dtExcelExport.Columns.Add("Statut");
            dtExcelExport.Columns.Add("Code reponse");
            dtExcelExport.Columns.Add("N° Autorisation");
            dtExcelExport.Columns.Add("Commerçant");
            dtExcelExport.Columns.Add("Montant");
            dtExcelExport.Columns["Montant"].DataType = System.Type.GetType("System.Decimal");
            dtExcelExport.Columns["Date"].DataType = System.Type.GetType("System.DateTime");
            //dtExcelExport.Rows.Add(new object[] { "Date", "Type", "Lieu", "Objet", "Categorie", "Details", "Montant" });
            DataTable dtAuthorisationList = operation.getUserAuthorizationList(searchCriteria, out iRowCount);
            string sAuthorizationDate = "";
            string sAuthorizationStatus = "";
            string sAuthorizationResponseCode = "";
            string sAuthorizationNum = "";
            string sAuthorizationPlace = "";
            string sAuthorizationAmount = "";
            decimal dAuthorizationAmount = 0;
            DateTime dtAuthorizationDate = DateTime.MinValue;
            foreach (DataRow row in dtAuthorisationList.Rows)
            {
                sAuthorizationDate = "";
                sAuthorizationStatus = "";
                sAuthorizationResponseCode = "";
                sAuthorizationNum = "";
                sAuthorizationPlace = "";
                sAuthorizationAmount = "";
                dAuthorizationAmount = 0;
                dtAuthorizationDate = DateTime.MinValue;

                DateTime dtValue = new DateTime();
                if (DateTime.TryParse(row["DateLocale"].ToString(), out dtValue) && row.Field<DateTime>("DateLocale") != null)
                {
                    try
                    {
                        if (row.Field<DateTime>("DateLocale") != null)
                            sAuthorizationDate = row.Field<DateTime>("DateLocale").ToString("dd/MM/yyyy HH:mm:ss");
                    }
                    catch (Exception ex) { sAuthorizationDate = row.Field<DateTime>("DateLocale").ToString("dd/MM/yyyy"); }
                    finally
                    {
                        try { dtAuthorizationDate = DateTime.Parse(sAuthorizationDate, new CultureInfo("fr-FR", true)); }
                        catch (Exception ex) { }
                    }
                }
                else if(DateTime.TryParse(row["DateGMT"].ToString(), out dtValue) && row.Field<DateTime>("DateGMT") != null)
                {
                    try
                    {
                        if (row.Field<DateTime>("DateGMT") != null)
                            sAuthorizationDate = row.Field<DateTime>("DateGMT").ToString("dd/MM/yyyy HH:mm:ss");
                    }
                    catch (Exception ex) { sAuthorizationDate = row.Field<DateTime>("DateGMT").ToString("dd/MM/yyyy"); }
                    finally
                    {
                        try { dtAuthorizationDate = DateTime.Parse(sAuthorizationDate, new CultureInfo("fr-FR", true)); }
                        catch (Exception ex) { }
                    }
                }

                if (row.Field<string>("ReponseAutorisation") != null)
                    sAuthorizationStatus = row.Field<string>("ReponseAutorisation").ToString();
                if (row.Field<string>("CodeReponseAutorisation") != null)
                    sAuthorizationResponseCode = row.Field<string>("CodeReponseAutorisation").ToString();
                if (row.Field<string>("NumAutorisation") != null)
                    sAuthorizationNum = row.Field<string>("NumAutorisation").ToString();

                if (row.Field<string>("LocalisationAccepteur") != null)
                    sAuthorizationPlace = row.Field<string>("LocalisationAccepteur").ToString();

                decimal dValue;
                if (decimal.TryParse(row["MontantEnCentimes"].ToString().Replace(".",","), NumberStyles.Any, new CultureInfo("fr-FR", true), out dValue))
                {
                    string sSign = row.Field<Decimal>("MontantTransaction").ToString().Trim().Substring(0,1);
                    sAuthorizationAmount = ((sSign == "-")?"-":"") + row["MontantEnCentimes"].ToString().Replace('.', ',');
                    try { dAuthorizationAmount = decimal.Parse(sAuthorizationAmount, new CultureInfo("fr-FR", true)); } catch(Exception ex) { }
                }

                dtExcelExport.Rows.Add(new object[] { dtAuthorizationDate, sAuthorizationStatus, sAuthorizationResponseCode, sAuthorizationNum, sAuthorizationPlace, dAuthorizationAmount });
            }

            DumpExcel(dtExcelExport, "Autorisations_Refus_" + _sClientBankAccount + "_" + DateTime.Now.ToString("yyyyMMddHHmmss"));
            //ExportDataSetToExcel(dgTransactionExcelExport(operation.getUserAuthorizationList(searchCriteria, out iRowCount)), "Autorisations_Refus_" + _sClientBankAccount + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xls");
        }
    }
    private void DumpExcel(DataTable tbl, string filename)
    {
        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.ClearContent();
        HttpContext.Current.Response.ClearHeaders();
        HttpContext.Current.Response.Buffer = true;
        HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.UTF8;
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + filename + ".xlsx");

        using (ExcelPackage pack = new ExcelPackage())
        {
            ExcelWorksheet ws = pack.Workbook.Worksheets.Add(filename);
            ws.Cells["A1"].LoadFromDataTable(tbl, true);
            //Set columns width
            ws.Column(1).Width = 20;
            ws.Column(2).Width = 15;
            ws.Column(3).Width = 15;
            ws.Column(4).Width = 15;
            ws.Column(5).Width = 20;
            ws.Column(6).Width = 15;
            //Set header style
            using (ExcelRange rng = ws.Cells["A1:F1"])
            {
                rng.Style.Font.Bold = true;
                rng.Style.Fill.PatternType = ExcelFillStyle.Solid;
                rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(245, 117, 39));
                rng.Style.Font.Color.SetColor(Color.White);
            }

            //Apply amount format
            using (ExcelRange rng = ws.Cells[2, 6, tbl.Rows.Count + 2, 6])
            {
                rng.Style.Numberformat.Format = "0.00 €";
            }

            //Apply date format
            using (ExcelRange rng = ws.Cells[2, 1, tbl.Rows.Count + 2, 1])
            {
                rng.Style.Numberformat.Format = "dd/MM/yyyy HH:mm:ss";
            }

            //Set RED Color when amount < 0
            for (int i = 0; i < tbl.Rows.Count; i++)
            {
                string sMontant = tbl.Rows[i]["Montant"].ToString().Trim();
                if (sMontant.Length > 0 && sMontant.Substring(0, 1) == "-") { ws.Cells[i + 2, 6].Style.Font.Color.SetColor(Color.Red); }
                else { ws.Cells[i + 2, 6].Style.Font.Color.SetColor(Color.Green); }
            }

            var ms = new System.IO.MemoryStream();
            pack.SaveAs(ms);
            ms.WriteTo(HttpContext.Current.Response.OutputStream);
        }

        HttpContext.Current.Response.Flush();
        HttpContext.Current.Response.End();

    }
}