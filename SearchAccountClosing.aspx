﻿<%@ Page Title="Recherche clôture de compte" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="SearchAccountClosing.aspx.cs" Inherits="SearchAccountClosing" %>
<%@ Register Src="~/API/AccountClosingReason.ascx" TagName="ClosingReason" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">

    <style type="text/css">
        .close-details {
            display:none;
        }
        .account-close-row, .account-close-alt-row {
            padding:5px;
            border-bottom:1px solid #808080;
            border-left:1px solid #808080;
            border-right:1px solid #808080;
            cursor:pointer;
        }
        .account-close-row {
            background-color:#ffffff;
        }

        .account-close-alt-row {
            background-color:#e5e5e5;
        }

        .close-sum, .close-details table {
            width:100%;
            border-collapse:collapse;
        }
        .close-sum th, .close-details table th {
            text-align:center;
            background-color:#344B56;
            color:#fff;
            padding:10px 0;
        }
        .close-sum tr th:nth-of-type(1), .close-sum tr td:nth-of-type(1) {
            width:30%;
        }
        .close-sum tr th:nth-of-type(2), .close-sum tr td:nth-of-type(2) {
            width:10%;
        }
        .close-sum tr td:nth-of-type(2) {
            text-align: right;
            padding-right:10px;
            box-sizing:border-box;
        }
        .close-sum tr th:nth-of-type(3), .close-sum tr td:nth-of-type(3) {
            width:40%;
        }
        .close-sum tr th:nth-of-type(4), .close-sum tr td:nth-of-type(4) {
            width:20%;
        }

        .close-action {
            display:flex;
            flex-direction:row;
            width:100%;
            margin-top:15px;
        }
        .close-action div {
            flex:1;
            flex-grow:1;
            text-align:center;
        }
        .close-action div .button {
            width:250px;
        }
        .close-action div:first-of-type .button.red {
            background-color:red;
            border-color:red;
        }
        .close-action div:last-of-type .button.green {
            background-color:limegreen;
            border-color:limegreen;
        }
        .close-action div:first-of-type .button:hover, .close-action div:last-of-type .button:hover {
            border-color:black;
        }

        .account-close-row .close-details table td {
            background-color:#e5e5e5;
        }
        .account-close-alt-row .close-details table td {
            background-color:#fff;
        }
        .close-details table {
            border:1px solid #808080;
        }
        .close-details table th {
            padding:4px 5px;
        }
        .close-details table td {
            padding:2px 5px;
        }
        .close-details table td {
            border-top:1px solid #808080;
            border-bottom:1px solid #808080;
        }

        #div-histo {
            position:relative;
        }
        #div-histo:hover #div-histo-details {
            display:block;
        }
        #div-histo-icon {
            position:relative;
            top:2px;
        }
        #div-histo-details {
            display:none;
            position:absolute;
            z-index:100;
            right:0;
            top:20px;
            width:300px;
            height:200px;
            overflow-y:scroll;
            background-color:#fff;
            box-shadow:1px 1px 3px 0px #999;
        }
        #div-histo-details table {
            width:100%;
            border-collapse:collapse;
        }
        #div-histo-details table td {
            border-bottom:1px solid #ccc;
            padding:2px 5px;
        }
    </style>

    <script type="text/javascript" src="Scripts/jquery.sticky.js"></script>
    <script type="text/javascript">
        function InitDetailsSlider() {
            $('.account-close-row, .account-close-alt-row').unbind('click').click(function () {
                if($(this).find('.close-details').is(':hidden'))
                    $(this).find('.close-details').slideDown();
                else $(this).find('.close-details').slideUp();
            });

            $('#list-header').sticky({topSpacing:0});
        }

        function ShowConfirmCloseDialog(refClose, clientName, refRow) {
            //console.log(refClose + ',' + clientName + ',' + refRow);
            event.stopPropagation();

            $('#<%=hdnRefClose.ClientID%>').val(refClose);
            $('#hdnRefRow').val(refRow);

            $('.client-to-close').empty().append(clientName);
            $('#div-approve-close').dialog({
                autoOpen: true,
                resizable: false,
                draggable: false,
                modal: true,
                title: 'Approuver la clôture',
                width: '350px',
                dialogClass: "no-close",
                buttons: [{ text: "Oui", click: function () { $(this).dialog("destroy"); $('#<%=btnApprove.ClientID%>').click(); } }, { text: "Non", click: function () { $(this).dialog("destroy"); } }]
                });
            $('#div-approve-close').parent().appendTo(jQuery("form:first"));
        }
        function ShowCancelCloseDialog(refClose, clientName, refRow) {
            //console.log(refClose + ',' + clientName + ',' + refRow);
            event.stopPropagation();

            $('#<%=hdnRefClose.ClientID%>').val(refClose);
            $('#hdnRefRow').val(refRow);

            $('.client-to-close').empty().append(clientName);
            $('#div-cancel-close').dialog({
                autoOpen: true,
                resizable: false,
                draggable: false,
                modal: true,
                title: 'Annuler la clôture',
                width: '350px',
                dialogClass: "no-close",
                buttons: [{ text: "Oui", click: function () { $(this).dialog("destroy"); $('#<%=btnCancel.ClientID%>').click(); } }, { text: "Non", click: function () { $(this).dialog("destroy"); } }]
                });
            $('#div-cancel-close').parent().appendTo(jQuery("form:first"));
        }

        function GotoEditPage(refClient, refClose, refRow) {
            event.stopPropagation();

            $('#hdnRefRow').val(refRow);
            ShowLoading('close-row-' + $('#hdnRefRow').val());
            document.location.href = "AccountClosing.aspx?ref=" + refClient + "&edit=" + refClose;
        }

        function InitTooltip() {
            $('.image-tooltip').tooltip({
                position: {
                    my: "center bottom-10",
                    at: "center top",
                    using: function (position, feedback) {
                        $(this).css(position);
                        $("<div>")
                            .addClass("arrow")
                            .addClass(feedback.vertical)
                            .addClass(feedback.horizontal)
                            .appendTo(this);
                    }
                }
            });
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

    <h2 style="color: #344b56;text-transform:uppercase">Recherche clôture de compte</h2>

    <div style="margin:5px 0 15px 0;">
        <asp:UpdatePanel ID="upClose" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="panelFilter" runat="server" DefaultButton="btnSearch" style="margin-bottom:5px;width:60%;display:inline-block">
                    <%--<div class="label">Avec préavis</div>--%>
                    <div style="display:inline-block">
                        <div class="label">
                            Type
                        </div>
                        <asp:DropDownList ID="ddlNotifyBefore" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlNotifyBefore_SelectedIndexChanged" style="height:21px">
                            <asp:ListItem Text="Immédiate" Value="0"></asp:ListItem>
                            <asp:ListItem Text="Avec préavis" Value="1"></asp:ListItem>
                            <asp:ListItem Text="Toutes" Value=""></asp:ListItem>
                        </asp:DropDownList>
                    </div>

                    <div style="display:inline-block">
                        <div class="label">
                            Filtre
                        </div>
                        <asp:TextBox ID="txtFilter" runat="server" placeholder="Nom, n° de compte"></asp:TextBox>
                    </div>

                    <asp:Button ID="btnSearch" runat="server" Text="Rechercher" CssClass="button" OnClick="ddlNotifyBefore_SelectedIndexChanged" />
                </asp:Panel>

                <%--<div style="text-align:right;width:39%;display:inline-block;vertical-align:bottom;margin-bottom:10px;">
                    Nb de résultats par page
                    <asp:DropDownList ID="ddlPageCount" runat="server">
                        <asp:ListItem Text="10" Value="10"></asp:ListItem>
                        <asp:ListItem Text="25" Value="25"></asp:ListItem>
                        <asp:ListItem Text="50" Value="50"></asp:ListItem>
                    </asp:DropDownList>
                </div>--%>
                <asp:Panel ID="panelList" runat="server">
                <table class="close-sum">
                    <thead>
                        <tr>
                            <th>Client</th>
                            <th>Solde</th>
                            <th>Raison</th>
                            <th>Par</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
                <asp:Repeater ID="rptClose" runat="server">
                    <ItemTemplate>
                        <div id="close-row-<%#Container.ItemIndex %>" class='<%# Container.ItemIndex % 2 == 0 ? "account-close-alt-row" : "account-close-row" %>'>
                            <%--<asp:Label ID="lblRefClose" runat="server" Text='<%#Eval("RefClose") %>'></asp:Label>
                            <asp:Label ID="lblRefCustomer" runat="server" Text='<%#Eval("RefCustomer") %>'></asp:Label>--%>
                    
                            <table class="close-sum">
                                <tbody>
                                    <tr>
                                        <td>
                                            <div>
                                                <asp:Label ID="lblAccountNumber" runat="server" Text='<%#Eval("AccountNumber") %>'></asp:Label>
                                            </div>
                                            <div>
                                                <asp:Label ID="lblFirstName" runat="server" Text='<%#Eval("FirstName") %>'></asp:Label>
                                                <asp:Label ID="lblLastName" runat="server" Text='<%#Eval("LastName") %>'></asp:Label>
                                            </div>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblAccountBalance" runat="server" Text='<%#Eval("CustomerBalance") %>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Repeater ID="rptReasons" runat="server" DataSource='<%#Eval("Reasons") %>'>
                                                <ItemTemplate>
                                                    <div>
                                                        <asp:Label ID="lblTag" runat="server" Text='<%#Eval("Tag") %>'></asp:Label>
                                                        <asp:Label ID="lblDesc" runat="server" Text='<%#Eval("Desc") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblBoUser" runat="server" Text='<%#Eval("BOUser") %>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Image ID="imgClock" runat="server" ImageUrl="~/Styles/Img/clock.png" Height="20px" Visible='<%#Eval("NotifyBefore")%>' CssClass="image-tooltip" ToolTip='<%#String.Format("Préavis au {0}", Eval("NoticeEndDate")) %>' />
                                            <asp:Panel ID="panelHisto" runat="server" Visible='<%#(int)Eval("NbLogs") > 0 ? true : false %>'>
                                                <div id="div-histo">
                                                    <div id="div-histo-icon">
                                                        <img src="Styles/Img/history.png" alt="Historique" height="20px" />
                                                    </div>
                                                    <div id="div-histo-details">
                                                        <asp:Repeater ID="rptLogs" runat="server" DataSource='<%#Eval("Logs") %>'>
                                                            <HeaderTemplate>
                                                                <table>
                                                                    <%--<thead>
                                                                        <tr>
                                                                            <th>Date</th>
                                                                            <th>Description</th>
                                                                        </tr>
                                                                    </thead>--%>
                                                                    <tbody>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                        <tr>
                                                                            <td style="text-align:center"><%#Eval("LogDate") %></td>
                                                                            <td style="width:70%"><%#Eval("Log") %></td>
                                                                        </tr>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                    </tbody>
                                                                </table>
                                                            </FooterTemplate>
                                                        </asp:Repeater>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <div class="close-details" style="cursor:auto" onclick="event.stopPropagation();">
                                <div class="label">
                                    Retour de fonds
                                </div>
                                <asp:Repeater ID="rptRF" runat="server" DataSource='<%#Eval("ReturnFunds") %>' Visible='<%#int.Parse(Eval("NbReturnFunds").ToString()) > 0 ? true : false %>'>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <th>Montant</th>
                                                <th>Bénéficiaire</th>
                                                <th>IBAN</th>
                                                <th>BIC</th>
                                                <th>Libellé</th>
                                                <th></th>
                                            </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                            <tr>
                                                <td style="text-align:right">
                                                    <asp:Label ID="lblAmount" runat="server" Text='<%#Eval("Amount") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblBeneficiaryName" runat="server" Text='<%#Eval("BeneficiaryName") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblIBAN" runat="server" Text='<%#Eval("IBAN") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblBIC" runat="server" Text='<%#Eval("BIC") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblLabel" runat="server" Text='<%#Eval("Label") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Image ID="imgError" runat="server" Height="30" title='<%#Eval("SabErrorLabel") %>' CssClass="image-tooltip" ImageUrl="~/Styles/Img/error_sign.png" Visible='<%#!String.IsNullOrWhiteSpace(Eval("SabErrorLabel").ToString()) %>' />
                                                </td>
                                            </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                                <asp:Label ID="lblRFEmptyData" runat="server" Visible='<%#int.Parse(Eval("NbReturnFunds").ToString()) == 0 ? true : false %>'>Aucun</asp:Label>

                                <div class="label">
                                    Frais
                                </div>
                                <asp:Repeater ID="rptFees" runat="server" DataSource='<%#Eval("Fees") %>' Visible='<%#int.Parse(Eval("NbFees").ToString()) > 0 ? true : false %>'>
                                    <HeaderTemplate>
                                        <table>
                                            <%--<tr>
                                                <th style="width:1%">Montant</th>
                                                <th>Description</th>
                                            </tr>--%>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblDesc" runat="server" Text='<%#Eval("Description") %>'></asp:Label>
                                                    &nbsp;:&nbsp;
                                                    <asp:Label ID="lblAmount" runat="server" Text='<%#Eval("Amount") %>'></asp:Label>
                                                <%--</td>
                                                <td>--%>
                                            
                                                </td>    
                                            </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                                <asp:Label ID="lblFeesEmptyData" runat="server" Visible='<%#int.Parse(Eval("NbFees").ToString()) == 0 ? true : false %>'>Aucun</asp:Label>

                                <asp:Panel ID="panelSum" runat="server" Visible='<%#Decimal.Parse(Eval("Sum").ToString()) > 0 ? true : false %>'>
                                    <div class="label">
                                        Total
                                    </div>
                                    <asp:Literal ID="litSum" runat="server" Text='<%#String.Format("{0} €", DecimalToString(Decimal.Parse(Eval("Sum").ToString()))) %>'></asp:Literal>
                                </asp:Panel>

                                <asp:Panel ID="panelAction" runat="server" CssClass="close-action" Visible='<%#bActionRights %>'>
                                    <div>
                                        <input id="btnCancel" type="button" value="Annuler" class="button red" onclick='<%#String.Format("ShowCancelCloseDialog({0},\"{1}\",{2});", Eval("RefClose"), (Eval("FirstName") + " " + Eval("LastName")).Replace("'", "&#39;"), Container.ItemIndex) %>' />
                                    </div>
                                    <div>
                                        <input id="btnEdit" type="button" value="Modifier" class="button" onclick='<%#String.Format("GotoEditPage({0}, {1}, {2});", Eval("RefCustomer"), Eval("RefClose"), Container.ItemIndex)%>' />
                                    </div>
                                    <asp:Panel ID="panelApprove" runat="server" Visible='<%#bool.Parse(Eval("NotifyBefore").ToString()) ? false : true %>'>
                                        <input id="btnApprove" type="button" value="Approuver" class="button green" onclick='<%#String.Format("ShowConfirmCloseDialog({0},\"{1}\",{2});", Eval("RefClose"), (Eval("FirstName") + " " + Eval("LastName")).Replace("'", "&#39;"), Container.ItemIndex) %>' />
                                    </asp:Panel>
                                </asp:Panel>

                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
                    <%--<div style="text-align:center;background-color:#344B56;padding:4px;">
                        <asp:Button ID="btnPrev" runat="server" Text="<" CommandArgument="prev" OnClick="btnPrevNext_Click" />
                        <asp:DropDownList ID="ddlPageIndex" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlNotifyBefore_SelectedIndexChanged">
                            <asp:ListItem Text="1" Value="1"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:Button ID="btnNext" runat="server" Text=">" CommandArgument="next" OnClick="btnPrevNext_Click" />
                    </div>--%>
                </asp:Panel>
                <asp:Panel ID="panelNoResult" runat="server" Visible="false" HorizontalAlign="Center" style="padding:10px 0">
                    Aucun résultat
                </asp:Panel>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ddlNotifyBefore" EventName="SelectedIndexChanged" />
                <%--<asp:AsyncPostBackTrigger ControlID="ddlPageIndex" EventName="SelectedIndexChanged" />--%>
            </Triggers>
        </asp:UpdatePanel>
    </div>

    <div style="display:none">
        <asp:UpdatePanel ID="upAction" runat="server">
            <ContentTemplate>
                <asp:HiddenField ID="hdnRefClose" runat="server" />
                <asp:Button ID="btnApprove" runat="server" OnClick="btnApprove_Click" />
                <asp:Button ID="btnCancel" runat="server" OnClick="btnCancel_Click" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <div id="div-approve-close" style="display:none">
        Etes-vous sûr de vouloir clôturer le compte de <span class="client-to-close"></span> ?
        <br /><br />
        <b>Attention :</b> cette opération est irréversible.
    </div>

    <div id="div-cancel-close" style="display:none">
        Etes-vous sûr de vouloir annuler la clôture du compte de <span class="client-to-close"></span> ?
    </div>

    <div style="display:none">
        <asp:ClosingReason ID="ClosingReason1" runat="server" />
    </div>

    <div class="panel-loading"></div>
    <input type="hidden" id="hdnRefRow" />

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" Runat="Server">

    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(SearchAccountClosingBeginRequestHandler);
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(SearchAccountClosingEndRequestHandler);
        function SearchAccountClosingBeginRequestHandler(sender, args) {
            pbControl = args.get_postBackElement();

            var containerID = '';
            if (pbControl != null) {
                console.log(pbControl.id);
                if ($('#hdnRefRow').val().length > 0) {
                    if (pbControl.id.indexOf('btnApprove') != -1 || pbControl.id.indexOf('btnEdit') != -1 || pbControl.id.indexOf('btnCancel') != -1)
                        ShowLoading('close-row-' + $('#hdnRefRow').val());
                }
                else ShowLoading('MainContent_panelList');
            }
            else ShowLoading('MainContent_panelList');
        }
        function ShowLoading(containerID) {
            //console.log(containerID);
            var panel = $('#' + containerID);
            if (containerID.trim().length > 0 && panel != null) {
                $('.panel-loading').show();
                $('.panel-loading').width(panel.outerWidth());
                $('.panel-loading').height(panel.outerHeight());

                $('.panel-loading').position({
                    my: 'center',
                    at: 'center',
                    of: panel
                });
            }
        }
        function SearchAccountClosingEndRequestHandler(sender, args) {
            $('.panel-loading').hide();
        }
    </script>

</asp:Content>

