﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="CreOrder.aspx.cs" Inherits="CreOrder" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
<!--    <div class="menu">
        <asp:Button ID="btnPrevious" runat="server" Text="Menu précédent" PostBackUrl="~/BoCobalt.aspx" CssClass="button" />
    </div>-->
    <h2 style="color:#344b56; margin-bottom:10px; text-transform:uppercase">
        OP&Eacute;RATIONS COMPTABLES (CRE)
    </h2>
    <div style="padding:20px 0;">
        <div style="margin:auto;border:4px solid #f57527; border-radius:8px; display:table; width:80%">
            <asp:Repeater ID="rptMenu" runat="server">
                <ItemTemplate>
                    <div style="display:table-row">
                        <div style="display:table-cell;height:40px; vertical-align:middle; text-align:center;" class="classMenu" onclick='document.location.href="<%#Eval("Value") %>"'>
                            <%#Eval("Label") %>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>

        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" Runat="Server">
</asp:Content>

