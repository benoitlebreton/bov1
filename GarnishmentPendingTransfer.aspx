﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="GarnishmentPendingTransfer.aspx.cs" Inherits="GarnishmentPendingTransfer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">

    <script type="text/javascript">
        function Garnishment_PendingTransferAction(refPendingTransfer, refCustomer, action) {
            switch (action) {
                case "TERMINATE":
                    $('#pending-transfer-action').html('approuver');
                    break;
                case "REFUSE":
                    $('#pending-transfer-action').html('refuser');
                    break;
                case "CANCEL":
                    $('#pending-transfer-action').html('annuler');
                    break;
            }

            $('#<%=hdnRefPendingTransferSelected.ClientID%>').val(refPendingTransfer);
            $('#<%=hdnRefCustomerSelected.ClientID%>').val(refCustomer);
            $('#<%=hdnPendingTransferActionSelected.ClientID%>').val(action);

            $('#dialog-pending-transfer-action').dialog({
                autoOpen: true,
                resizable: false,
                draggable: false,
                modal: true,
                minWidth: 380,
                zIndex: 10000,
                title: "Confirmation",
                buttons: [{ text: 'Annuler', click: function () { $(this).dialog('close'); $(this).dialog('destroy'); } },
                    {
                        text: 'OK', click: function () {
                            showLoading();
                            $('#<%=btnPendingTransferAction.ClientID%>').click();
                            $(this).dialog('close'); $(this).dialog('destroy');
                        }
                    }]
            });
            $("#dialog-pending-transfer-action").parent().appendTo(jQuery("form:first"));
        }

        function Garnishment_PendingTransferModifyIBAN(refPendingTransfer) {
            $('.lblIBAN_' + refPendingTransfer).hide();
            $('.txtIBAN_' + refPendingTransfer).show();
            return false;
        }
        function Garnishment_PendingTransferValidateNewIBAN(refPendingTransfer, refCustomer) {
            var newIban = $('.txtIBAN_' + refPendingTransfer + ' input[type=text]').val();
            if (validateIBAN(newIban.trim())) {
                showLoading();
                SetNewIban(refPendingTransfer, refCustomer, newIban);
            }
            else {
                AlertMessage("Erreur", "Iban non valide");
            }
            
            return false;
        }
        function Garnishment_PendingTransferCancelIBANModification(refPendingTransfer) {
            $('.lblIBAN_' + refPendingTransfer).show();
            $('.txtIBAN_' + refPendingTransfer).hide();
            return false;
        }

        function validateIBAN($v) { //This function check if the checksum if correct
            $v = $v.replace(/^(.{4})(.*)$/, "$2$1"); //Move the first 4 chars from left to the right
            $v = $v.replace(/[A-Z]/g, function ($e) { return $e.charCodeAt(0) - 'A'.charCodeAt(0) + 10 }); //Convert A-Z to 10-25
            var $sum = 0;
            var $ei = 1; //First exponent 
            for (var $i = $v.length - 1; $i >= 0; $i--) {
                $sum += $ei * parseInt($v.charAt($i), 10); //multiply the digit by it's exponent 
                $ei = ($ei * 10) % 97; //compute next base 10 exponent  in modulus 97
            };
            return $sum % 97 == 1;
        }

        var returnData;
        function SetNewIban(refPendingTransfer, refCustomer, iban) {
            var sFilePath = "";
            $.ajax({
                url: "./ws_tools.asmx/GarnishmentPendingModifyIban",
                data: '{ "sRefCustomer": "' + refCustomer + '", "sRefPendingTransfer": "' + refPendingTransfer + '", "sIban": "' + iban + '" }', //, culture: 'fr-FR', order: 'ASC' 
                dataType: "json",
                type: "POST",
                cache: false,
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    hideLoading();
                    returnData = data;
                    var BoolData = (data.d.toString().trim() == "True") ? true : false;
                    if (BoolData) {
                        $('.lblIBAN_' + refPendingTransfer + ' label').html($('.txtIBAN_' + refPendingTransfer + ' input[type=text]').val().trim());
                        $('.lblIBAN_' + refPendingTransfer).show();
                        $('.txtIBAN_' + refPendingTransfer).hide();
                    }
                    else {
                        AlertMessage("Erreur", "IBAN non valide");
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    hideLoading();
                    console.log(returnData);
                    console.log(errorThrown);
                    AlertMessage("Erreur", "Une erreur est survenue.<br/>Le nouvel IBAN n'a pu être enregistré.");
                }
            });
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

<asp:Panel ID="panelGarnishmentPending" runat="server" Visible="false">
    <h2 style="color: #344b56;margin-bottom:10px;text-transform:uppercase">Avis en attente de validation</h2>

    <asp:UpdatePanel ID="upGarnishmentPending" runat="server" UpdateMode="Conditional">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ddlNbResult" EventName="SelectedIndexChanged" />
        </Triggers>
        <ContentTemplate>
            <div id="divGridSettings" runat="server" style="display: table; width: 100%; margin-bottom: 10px">
                <div style="display: table-row">
                    <div style="display: table-cell; width: 50%; padding-top: 20px">
                        <asp:Label ID="lblNbOfResults" runat="server">0</asp:Label>
                        résultat(s)
                    </div>
                    <div style="display: table-cell; width: 50%; text-align: right;">
                        <asp:Panel ID="panelPageSize" runat="server">
                            Nb de résultats par page :
                            <asp:DropDownList ID="ddlNbResult" runat="server" AutoPostBack="true" OnSelectedIndexChanged="RefreshPendingList">
                                <asp:ListItem Value="10" Selected="True">10</asp:ListItem>
                                <asp:ListItem Value="25">25</asp:ListItem>
                                <asp:ListItem Value="50">50</asp:ListItem>
                            </asp:DropDownList>
                        </asp:Panel>
                    </div>
                </div>
            </div>
            <div style="border: 3px solid #344b56; border-top-left-radius: 8px; border-top-right-radius: 8px; margin-top: 5px;">
                <asp:GridView ID="gvGarnishmentPendingTransfer" runat="server" AllowSorting="false"
                    AllowPaging="false" AutoGenerateColumns="False" GridLines="None" CellPadding="10"
                    DataKeyNames="RefTransferPending" ShowHeader="true" Width="100%" SortedDescendingCellStyle-Font-Overline="true"
                    ViewStateMode="Disabled" BorderWidth="0" BackColor="Transparent">
                    <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="#344b56" Font-Bold="true" />
                    <EmptyDataTemplate>
                        Aucun virement
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="Avis" HeaderStyle-Width="1px">
                            <ItemTemplate>
                                <asp:HyperLink ID="hlGarnishmentDetails" runat="server" CssClass="MiniButton" style="display:inline-block;width:160px" NavigateUrl='<%#String.Format("GarnishmentDetails.aspx?ref={0}", Eval("LinkedRef")) %>' Target="_blank">accéder à l'avis</asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="N° de compte" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:HyperLink ID="hlCustomerAccountNumber" runat="server" NavigateUrl='<%#String.Format("ClientDetails.aspx?account={0}", Eval("CustomerAccountNumber")) %>' Target="_blank"><%# Eval("CustomerAccountNumber") %></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Montant" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                            <ItemTemplate>
                                <asp:Label ID="lblAmount" runat="server"><%# getAmountCleaned(tools.getFormattedAmount(Eval("Amount").ToString(), false)) %></asp:Label>
                                <asp:Label runat="server" ID="lblCurrency" Text='<%# Eval("Currency")%>' CssClass="tooltip" ToolTip='<%# Eval("Currency")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="IBAN" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <div class=<%# string.Format("'table lblIBAN_{0}'", Eval("RefTransferPending")) %>>
                                    <div class="row">
                                        <div class="cell" style="vertical-align:middle;">
                                            <asp:Label ID="lblIBAN" runat="server"><%#Eval("IBAN") %></asp:Label>
                                        </div>
                                        <div class="cell" style="vertical-align:middle;padding-left:5px">
                                            <asp:ImageButton ID="imgModifyIban" runat="server" ImageUrl="~/Styles/Img/modify.png" ToolTip="Modify" OnClientClick=<%# string.Format("return Garnishment_PendingTransferModifyIBAN('{0}', '"+Eval("RefCustomer").ToString()+"');", Eval("RefTransferPending")) %> Style="height: 25px" Visible='<%#bHasActionRights %>' />
                                        </div>
                                    </div>
                                </div>
                                <div class=<%# string.Format("'table txtIBAN_{0}'", Eval("RefTransferPending")) %> style="display:none">
                                    <div class="row">
                                        <div class="cell" style="vertical-align:middle;">
                                            <asp:TextBox ID="txtIBAN" runat="server" Text='<%#Eval("IBAN") %>' style="width:210px; height:25px;"></asp:TextBox>
                                        </div>
                                        <div class="cell" style="vertical-align:middle;padding-left:5px">
                                            <asp:ImageButton ID="imgCancelModifyIban" runat="server" ImageUrl="~/Styles/Img/cancel_2.png" ToolTip="Annuler" OnClientClick=<%# string.Format("return Garnishment_PendingTransferCancelIBANModification('{0}');", Eval("RefTransferPending")) %> Style="height: 15px" />
                                        </div>
                                        <div class="cell" style="vertical-align:middle;padding-left:5px">
                                            <asp:ImageButton ID="imgValidateNewIban" runat="server" ImageUrl="~/Styles/Img/check_2.png" ToolTip="Valider" OnClientClick=<%# string.Format("return Garnishment_PendingTransferValidateNewIBAN('{0}', '"+Eval("RefCustomer").ToString()+"');", Eval("RefTransferPending")) %> Style="height: 15px" />
                                        </div>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Action" ItemStyle-Width="1px">
                            <ItemTemplate>
                                <asp:Panel ID="panelActions" runat="server" Visible='<%#bHasActionRights %>'>
                                    <div class="trimmer" style="width: auto; white-space: nowrap; <%# (Eval("isRefused").ToString().ToLower() == "true" || Eval("isTerminated").ToString().ToLower() == "true" || Eval("isCancelled").ToString().ToLower() == "true") ? "display:none": "" %>">
                                        <asp:ImageButton ID="imgAuthorize" runat="server" ImageUrl="~/Styles/Img/check.png" ToolTip="Approuver" OnClientClick=<%# string.Format("Garnishment_PendingTransferAction('{0}', '"+Eval("RefCustomer").ToString()+"', 'TERMINATE');return false;", Eval("RefTransferPending"), Eval("CustomerAccountNumber")) %> Style="height: 25px" />
                                        <asp:ImageButton ID="imgDeny" runat="server" ImageUrl="~/Styles/Img/deny.png" ToolTip="Refuser" OnClientClick=<%# string.Format("Garnishment_PendingTransferAction('{0}', '"+Eval("RefCustomer").ToString()+"', 'REFUSE');return false;", Eval("RefTransferPending"), Eval("CustomerAccountNumber")) %> Style="height: 25px" />
                                        <asp:ImageButton ID="imgCancel" runat="server" ImageUrl="~/Styles/Img/cancel.png" ToolTip="Annuler" OnClientClick=<%# string.Format("Garnishment_PendingTransferAction('{0}', '"+Eval("RefCustomer").ToString()+"', 'CANCEL');return false;", Eval("RefTransferPending"), Eval("CustomerAccountNumber")) %> Style="height: 25px" />
                                    </div>
                                </asp:Panel>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#344b56" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#344b56" Font-Bold="True" ForeColor="White" Height="25px" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle CssClass="GridViewRowStyle" />
                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                </asp:GridView>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:HiddenField ID="hdnRefPendingTransferSelected" runat="server" />
    <asp:HiddenField ID="hdnRefCustomerSelected" runat="server" />
    <asp:HiddenField ID="hdnPendingTransferActionSelected" runat="server" />

    <div id="dialog-pending-transfer-action" style="display: none">
        &Ecirc;tes-vous sûr de vouloir <span id="pending-transfer-action"></span> ce virement ?
        <asp:Button ID="btnPendingTransferAction" runat="server" OnClick="btnPendingTransferAction_Click" Style="display: none" />
    </div>

</asp:Panel>
<asp:Panel ID="panelGarnishmentPendingUnauthorized" runat="server" Visible="false" CssClass="font-orange bold" style="padding:10px 0;">
    Vous n'êtes pas autorisé à voir ce contenu
</asp:Panel>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" Runat="Server">
</asp:Content>

