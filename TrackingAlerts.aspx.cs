﻿
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;


public partial class TrackingAlerts : System.Web.UI.Page
{
    private bool _bCounterAll { get { return (Session["AlertCounterAll"] != null) ? bool.Parse(Session["AlertCounterAll"].ToString()) : false; } }
    private string _BoUser { get { return (_bCounterAll && Request.Params["ra"] != null) ? Request.Params["ra"].ToString() : authentication.GetCurrent().sRef; } }
    protected string daysCriticalAlert = ConfigurationManager.AppSettings["DaysCriticalAlert"].ToString();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            GetListeTracking(authentication.GetCurrent().sToken);
            SetUserList();
        }
    }

    protected void GetListeTracking(string sToken)
    {

        List<TrackingAlert> trackingAlertList = null;    
        
        try
        {
            trackingAlertList = TrackingAlertService.GetTrackingAlertList(sToken, Convert.ToInt32(daysCriticalAlert));
            
        }
        catch (Exception e)
        {

        }
        finally
        {
            rptAlertProgressList.DataSource = trackingAlertList;
            rptAlertProgressList.DataBind();

            if (trackingAlertList == null || !trackingAlertList.Any())
                panelEmpty.Visible = true;
            else panelEmpty.Visible = false;

            upAlertProgress.Update();
            
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "initProgressBarList();", true);

        }
        
    }

    protected void RepeaterAlertList_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {

        Repeater repeaterAlertList = (Repeater)e.Item.Parent;
        
        if (repeaterAlertList ==null || repeaterAlertList.Items.Count < 1)
        {
            if (e.Item.ItemType == ListItemType.Footer)
            {
                // Dans le cas ou on n'a pas d'item on affiche le panel aucun résultat
                Panel panelAlertEmpty  = (Panel)e.Item.FindControl("panelAlertEmpty");
                panelAlertEmpty.Visible = true;
            }
        }
    }

    //private List<Alert> GetListeAlertToBO(string bOUserRef)
    //{
    //    List<Alert> listeAlertTracking = new List<Alert>();

    //    try
    //    {
    //        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString))
    //        {
    //            conn.Open();

    //            SqlCommand cmd = new SqlCommand("AML.P_GetAlertEvents", conn);
    //            cmd.CommandType = CommandType.StoredProcedure;

    //            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
    //            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);

    //            XElement xml = new XElement("Alert",
    //                                new XAttribute("CashierToken", authentication.GetCurrent().sToken),
    //                                new XAttribute("Treated", "0"));
    //            //if (!ckbViewAll.Checked)
    //            xml.Add(new XAttribute("RefBOUser", bOUserRef));

    //            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN", xml).ToString();
    //            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

    //            cmd.ExecuteNonQuery();

    //            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

    //            if (sXmlOut.Length > 0)
    //            {
    //                List<string> listAlert = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/Alert");

    //                if (listAlert.Count > 0)
    //                {
    //                    //dtAlert = new DataTable();
    //                    //dtAlert.Columns.Add("RefAlert");
    //                    //dtAlert.Columns.Add("AlertLabel");
    //                    //dtAlert.Columns.Add("RefCustomer");
    //                    //dtAlert.Columns.Add("CustomerName");
    //                    //dtAlert.Columns.Add("Steps", typeof(DataTable));
    //                    //dtAlert.Columns.Add("StepMarkers");
    //                    //dtAlert.Columns.Add("CurrentStep");
    //                    //dtAlert.Columns.Add("StatusClass");
    //                    //dtAlert.Columns.Add("Notification");
    //                    //dtAlert.Columns.Add("NotificationIcon");
    //                    //dtAlert.Columns.Add("NotificationLabel");

    //                    for (int x = 0; x < listAlert.Count; x++)
    //                    {
    //                        Alert alert = new Alert();

    //                        List<string> listRefAlert = CommonMethod.GetAttributeValues(listAlert[x], "Alert", "refAlert");
    //                        List<string> listAlertLabel = CommonMethod.GetAttributeValues(listAlert[x], "Alert", "AlertLabel");
    //                        List<string> listRefCustomer = CommonMethod.GetAttributeValues(listAlert[x], "Alert", "RefCustomer");
    //                        List<string> listCustomerPoliteness = CommonMethod.GetAttributeValues(listAlert[x], "Alert", "Politeness");
    //                        List<string> listCustomerLastName = CommonMethod.GetAttributeValues(listAlert[x], "Alert", "CustomerLastName");
    //                        List<string> listCustomerFirstName = CommonMethod.GetAttributeValues(listAlert[x], "Alert", "CustomerFirstName");
    //                        List<string> listNotification = CommonMethod.GetAttributeValues(listAlert[x], "Alert", "AlertNotification");
    //                        List<string> listNotificationKind = CommonMethod.GetAttributeValues(listAlert[x], "Alert", "TagAlertNotificationKind");
    //                        List<string> listNotificationLabel = CommonMethod.GetAttributeValues(listAlert[x], "Alert", "AlertNotificationLabel");

    //                        List<string> listEvents = CommonMethod.GetTags(listAlert[x], "Alert/Events/AlertEvent");

    //                        string sCustomerFullName = listCustomerPoliteness[0] + " " + listCustomerFirstName[0] + " " + listCustomerLastName[0];

    //                        alert.CustomerName = sCustomerFullName;
    //                        alert.AlertLabel = listAlertLabel[0];

    //                        StringBuilder sbStepMarkers = new StringBuilder();

    //                        // Recuperation date ORIGINE ZERO
    //                        string sOriginDate = "";
    //                        for (int y = 0; y < listEvents.Count; y++)
    //                        {
    //                            List<string> listDate = CommonMethod.GetAttributeValues(listEvents[y], "AlertEvent", "Date");
    //                            List<string> listTag = CommonMethod.GetAttributeValues(listEvents[y], "AlertEvent", "Tag");

    //                            string sDate = (listDate.Count > 0) ? listDate[0] : "";

    //                            //if(listTag[0] == "FIRSTCHECK")
    //                            if (listTag[0] == "COUNTERPART"
    //                                || listTag[0] == "FUNDSOURCE"
    //                                || listTag[0] == "LOCKACCOUNTINFO"
    //                                || listTag[0] == "SELFEMPLOYEDSPLIT"
    //                                )
    //                            {
    //                                sOriginDate = sDate;
    //                                break;
    //                            }
    //                        }

    //                        // Parsing EVENTS
    //                        for (int y = 0; y < listEvents.Count; y++)
    //                        {
    //                            List<string> listDate = CommonMethod.GetAttributeValues(listEvents[y], "AlertEvent", "Date");
    //                            List<string> listLabel = CommonMethod.GetAttributeValues(listEvents[y], "AlertEvent", "Label");
    //                            List<string> listBOUser = CommonMethod.GetAttributeValues(listEvents[y], "AlertEvent", "BOUser");
    //                            List<string> listTag = CommonMethod.GetAttributeValues(listEvents[y], "AlertEvent", "Tag");

    //                            string sDate = (listDate.Count > 0) ? listDate[0] : "";
    //                            string sNbDays = "";
    //                            if (!String.IsNullOrWhiteSpace(sDate))
    //                                sNbDays = tools.GetNbDaysFromDate(sDate, sOriginDate).ToString();

    //                            string sIcon = "";
    //                            switch (listTag[0])
    //                            {
    //                                case "GENERATION":
    //                                    sIcon = "alarm";
    //                                    break;
    //                                case "FIRSTCHECK":
    //                                    sIcon = "visibility";
    //                                    break;
    //                                case "COUNTERPART":
    //                                case "FUNDSOURCE":
    //                                case "LOCKACCOUNTINFO":
    //                                case "SELFEMPLOYEDSPLIT":
    //                                    sIcon = "mail_outline";
    //                                    break;
    //                                case "NoContact":
    //                                    sIcon = "mic_off";
    //                                    break;
    //                                case "ContactCounterpartAgain":
    //                                case "RELANCEJ+7":
    //                                case "RELANCEJ+14":
    //                                case "RELANCEJ+30":
    //                                    sIcon = "email";
    //                                    break;
    //                                case "UnfreezeAccount":
    //                                    sIcon = "lock_open";
    //                                    break;
    //                                case "CONTACTCLIENTPHONE":
    //                                case "CallCounterpart":
    //                                    sIcon = "phone";
    //                                    break;
    //                                case "AlertResponseOK":
    //                                    sIcon = "sentiment_satisfied";
    //                                    break;
    //                                case "AlertResponseKO":
    //                                    sIcon = "sentiment_dissatisfied";
    //                                    break;
    //                                case "AlertResponseNotSatisfactory":
    //                                    sIcon = "sentiment_neutral";
    //                                    break;
    //                                case "CounterpartAnswer":
    //                                case "CallCounterpartAnswerOk":
    //                                    sIcon = "thumb_up";
    //                                    break;
    //                                case "CounterpartNoAnwser":
    //                                    sIcon = "thumb_down";
    //                                    break;
    //                                case "CallCounterpartAnswerPending":
    //                                    sIcon = "more_horiz";
    //                                    break;
    //                                case "REASSIGN":
    //                                    sIcon = "supervisor_account";
    //                                    break;
    //                                default:
    //                                    sIcon = "fiber_manual_record";
    //                                    break;
    //                            }
    //                            //var step = new Step() {Date=  DateTime.Parse(sDate), Days= sNbDays, Label= listLabel[0], Icon= String.Format("<i class=\"material-icons\" tooltip=\"{1}\">{0}</i>", sIcon, listLabel[0]), BOUser=  (listBOUser.Count > 0) ? listBOUser[0] : "" };
    //                            var step = new Step() { Date = DateTime.Parse(sDate), Days = sNbDays, Label = listLabel[0], Icon = string.Empty, BOUser = (listBOUser.Count > 0) ? listBOUser[0] : "" };
    //                            alert.ListeStep.Add(step);

    //                            //dtEvents.Rows.Add(new object[] { sDate, sNbDays, listLabel[0], String.Format("<i class=\"material-icons\" tooltip=\"{1}\">{0}</i>", sIcon, listLabel[0]), (listBOUser.Count > 0) ? listBOUser[0] : "" });
    //                        }

    //                        // Creation markers
    //                        DataTable dtEventMarkers = new DataTable();
    //                        dtEventMarkers.Columns.Add("Days");
    //                        dtEventMarkers.Columns.Add("OverLabel");
    //                        dtEventMarkers.Columns.Add("BottomLabel");
    //                        for (int i = 0; i < alert.ListeStep.Count; i++)
    //                        {
    //                            if (dtEventMarkers.Rows.Count > 0)
    //                            {
    //                                // Même jour marker précédent
    //                                if (alert.ListeStep[i].Days == dtEventMarkers.Rows[dtEventMarkers.Rows.Count - 1]["Days"].ToString())
    //                                {
    //                                    dtEventMarkers.Rows[dtEventMarkers.Rows.Count - 1]["OverLabel"] += "<br/>" + alert.ListeStep[i].Label;
    //                                    dtEventMarkers.Rows[dtEventMarkers.Rows.Count - 1]["BottomLabel"] += alert.ListeStep[i].Icon;
    //                                }
    //                                else
    //                                {
    //                                    dtEventMarkers.Rows.Add(new object[] { alert.ListeStep[i].Days, alert.ListeStep[i].Label, alert.ListeStep[i].Icon });
    //                                }
    //                            }
    //                            else
    //                            {
    //                                dtEventMarkers.Rows.Add(new object[] { alert.ListeStep[i].Days, alert.ListeStep[i].Label, alert.ListeStep[i].Icon });
    //                            }
    //                        }
    //                        for (int i = 0; i < dtEventMarkers.Rows.Count; i++)
    //                        {
    //                            sbStepMarkers.Append(dtEventMarkers.Rows[i]["Days"] + ";" + dtEventMarkers.Rows[i]["OverLabel"] + ";" + dtEventMarkers.Rows[i]["BottomLabel"] + "|");
    //                        }
    //                        string sStepMarkers = sbStepMarkers.ToString().TrimEnd('|');
    //                        int iCurrentStep = tools.GetNbDaysFromDate(DateTime.Now.ToString("dd/MM/yyyy"), sOriginDate);
    //                        alert.StepMarkers = sStepMarkers;
    //                        alert.CurrentStep = iCurrentStep;
    //                        //    dtAlert.Rows.Add(new object[] {
    //                        //    listRefAlert[0],
    //                        //    listAlertLabel[0],
    //                        //    listRefCustomer[0],
    //                        //    sCustomerFullName,
    //                        //    dtEvents,
    //                        //    sStepMarkers,
    //                        //    iCurrentStep.ToString(),
    //                        //    (iCurrentStep >= 15) ? "step-progressbar-bar-red" : "",
    //                        //    (listNotification.Count > 0) ? listNotification[0] : "0",
    //                        //    String.Format("<i class=\"material-icons\" tooltip=\"{1}\">{0}</i>", "priority_high", listNotificationLabel[0]),
    //                        //    listNotificationLabel[0]
    //                        //});
    //                        listeAlertTracking.Add(alert);
    //                    }
    //                }
    //            }
    //        }
    //    }
    //    catch (Exception e)
    //    {

    //    }
    //    finally
    //    {
    //    }

    //    return listeAlertTracking;
    //}


    protected void SetUserList()
    {
        ddlBOUsers.DataSource = AML.GetAgentConformityList();
        ddlBOUsers.DataBind();
    }

    protected void btnChangeAlertAttribution_Click(object sender, EventArgs e)
    {
        XElement xml = new XElement("ALL_XML_IN",
                                new XElement("ALERT_TREATMENT",
                                    new XAttribute("CashierToken", authentication.GetCurrent().sToken),
                                    new XAttribute("RefAlert", hdnRefAlert.Value),
                                    new XAttribute("RefBOUserReassign", ddlBOUsers.SelectedValue),
                                    new XAttribute("ReserveAlert", "1")
                                    ));
        if (AML.SetAlertTreatment(xml.ToString()))
        {
            AlertMessage("Réattribution réussie", "<span style='color:green'>L'alerte a été réattribuée.</span>");
            SetAlertProgressList();
        }
        else AlertMessage("Erreur", "Une erreur est survenue lors de la réattribution.");
    }

    protected void SetAlertProgressList()
    {
        GetListeTracking(authentication.GetCurrent().sToken);
    }

    protected void AlertMessage(string sTitle, string sMessage)
    {
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('" + sTitle + "', \"" + sMessage + "\");", true);
    }

}