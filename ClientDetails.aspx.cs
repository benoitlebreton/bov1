﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using XMLMethodLibrary;

public partial class ClientDetails : System.Web.UI.Page
{
    #region parameters
    private int _iRefCustomer
    {
        get { return int.Parse(ViewState["RefCustomer"].ToString()); }
        set { ViewState["RefCustomer"] = value; }
    }
    private string _sAccountNumber
    {
        get { return (ViewState["AccountNumber"] != null) ? ViewState["AccountNumber"].ToString() : ""; }
        set { ViewState["AccountNumber"] = value; }
    }
    private string _sRegistrationCode
    {
        get { return (ViewState["RegistrationCode"] != null) ? ViewState["RegistrationCode"].ToString() : ""; }
        set { ViewState["RegistrationCode"] = value; }
    }
    private Decimal _dCardPaymentLimit
    {
        get { return (ViewState["CardPaymentLimit"] != null) ? Decimal.Parse(ViewState["CardPaymentLimit"].ToString(), new CultureInfo("fr-FR")) : 0; }
        set { ViewState["CardPaymentLimit"] = value; }
    }
    private Decimal _dCardPaymentForNow
    {
        get { return (ViewState["CardPaymentForNow"] != null) ? Decimal.Parse(ViewState["CardPaymentForNow"].ToString(), new CultureInfo("fr-FR")) : 0; }
        set { ViewState["CardPaymentForNow"] = value; }
    }
    private Decimal _dCardPaymentAvailable
    {
        get { return (ViewState["CardPaymentAvailable"] != null) ? Decimal.Parse(ViewState["CardPaymentAvailable"].ToString(), new CultureInfo("fr-FR")) : 0; }
        set { ViewState["CardPaymentAvailable"] = value; }
    }
    private Decimal _dWithdrawLimit
    {
        get { return (ViewState["WithdrawLimit"] != null) ? Decimal.Parse(ViewState["WithdrawLimit"].ToString(), new CultureInfo("fr-FR")) : 0; }
        set { ViewState["WithdrawLimit"] = value; }
    }
    private Decimal _dWithdrawForNow
    {
        get { return (ViewState["WithdrawForNow"] != null) ? Decimal.Parse(ViewState["WithdrawForNow"].ToString(), new CultureInfo("fr-FR")) : 0; }
        set { ViewState["WithdrawForNow"] = value; }
    }
    private Decimal _dWithdrawAvailable
    {
        get { return (ViewState["WithdrawAvailable"] != null) ? Decimal.Parse(ViewState["WithdrawAvailable"].ToString(), new CultureInfo("fr-FR")) : 0; }
        set { ViewState["WithdrawAvailable"] = value; }
    }
    private Decimal _dTransferIN
    {
        get { return (ViewState["TransferIN"] != null) ? Decimal.Parse(ViewState["TransferIN"].ToString(), new CultureInfo("fr-FR")) : 0; }
        set { ViewState["TransferIN"] = value; }
    }
    private Decimal _dCashIN
    {
        get { return (ViewState["CashIN"] != null) ? Decimal.Parse(ViewState["CashIN"].ToString(), new CultureInfo("fr-FR")) : 0; }
        set { ViewState["CashIN"] = value; }
    }
    private bool _bLocked
    {
        get
        {
            bool bLocked = false;
            return (ViewState["Locked"] != null && bool.TryParse(ViewState["Locked"].ToString(), out bLocked)) ? bLocked : false;
        }
        set { ViewState["Locked"] = value; }
    }
    private string _sLockedDate
    {
        get { return (ViewState["LockedDate"] != null) ? ViewState["LockedDate"].ToString() : ""; }
        set { ViewState["LockedDate"] = value; }
    }
    private string _sLockReason
    {
        get { return (ViewState["LockReason"] != null) ? ViewState["LockReason"].ToString() : ""; }
        set { ViewState["LockReason"] = value; }
    }
    private string _sPaymentKind
    {
        get { return (ViewState["PaymentKind"] != null) ? ViewState["PaymentKind"].ToString() : ""; }
        set { ViewState["PaymentKind"] = value; }
    }
    private string _sPaymentLimitPeriod
    {
        get { return (ViewState["PaymentLimitPeriod"] != null) ? ViewState["PaymentLimitPeriod"].ToString() : ""; }
        set { ViewState["PaymentLimitPeriod"] = value; }
    }
    private string _sWithdrawKind
    {
        get { return (ViewState["WithdrawKind"] != null) ? ViewState["WithdrawKind"].ToString() : ""; }
        set { ViewState["WithdrawKind"] = value; }
    }
    private string _sWithdrawLimitPeriod
    {
        get { return (ViewState["WithdrawLimitPeriod"] != null) ? ViewState["WithdrawLimitPeriod"].ToString() : ""; }
        set { ViewState["WithdrawLimitPeriod"] = value; }
    }
    private bool _bActionWebMessage
    {
        get { return (ViewState["ActionWebMessage"] != null) ? bool.Parse(ViewState["ActionWebMessage"].ToString()) : false; }
        set { ViewState["ActionWebMessage"] = value; }
    }
    private bool _bSousSurveillance { get { return (ViewState["SousSurveillance"] != null) ? bool.Parse(ViewState["SousSurveillance"].ToString()) : false; } set { ViewState["SousSurveillance"] = value; } }
    private bool _bLockTransfer
    {
        get { return (ViewState["LockTransfer"] != null) ? bool.Parse(ViewState["LockTransfer"].ToString()) : false; }
        set { ViewState["LockTransfer"] = value; }
    }
    private bool _bRequestTransferValidation
    {
        get { return (ViewState["RequestTransferValidation"] != null) ? bool.Parse(ViewState["RequestTransferValidation"].ToString()) : false; }
        set { ViewState["RequestTransferValidation"] = value; }
    }
    private string _sRequestTransferValidationAmount
    {
        get { return (ViewState["RequestTransferValidationAmount"] != null) ? ViewState["RequestTransferValidationAmount"].ToString() : ""; }
        set { ViewState["RequestTransferValidationAmount"] = value; }
    }
    private bool _bHideCardActionButtons
    {
        get { return (ViewState["HideCardActionButtons"] != null) ? bool.Parse(ViewState["HideCardActionButtons"].ToString()) : false; }
        set { ViewState["HideCardActionButtons"] = value; }
    }
    private bool _bOtherDocsLoaded
    {
        get { return (ViewState["OtherDocsLoaded"] != null) ? bool.Parse(ViewState["OtherDocsLoaded"].ToString()) : false; }
        set { ViewState["OtherDocsLoaded"] = value; }
    }
    private Button btnAddSpecificity_repeater;
    private DataTable _dtBarcodeToPAN
    {
        get { return (Session["BarcodeToPAN"] != null) ? (DataTable)Session["BarcodeToPAN"] : null; }
        set { Session["BarcodeToPAN"] = value; }
    }

    private bool _bEditKYC
    {
        get { return (ViewState["EditKYC"] != null) ? bool.Parse(ViewState["EditKYC"].ToString()) : false; }
        set { ViewState["EditKYC"] = value; }
    }
    private bool _bEditTaxResidence
    {
        get { return (ViewState["EditTaxResidence"] != null) ? bool.Parse(ViewState["EditTaxResidence"].ToString()) : false; }
        set { ViewState["EditTaxResidence"] = value; }
    }
    private bool _bAccountClosed
    {
        get { return (ViewState["AccountClosed"] != null) ? bool.Parse(ViewState["AccountClosed"].ToString()) : false; }
        set { ViewState["AccountClosed"] = value; }
    }

    private bool _isCobalt
    {
        get { return (ViewState["isCobalt"] != null) ? bool.Parse(ViewState["isCobalt"].ToString()) : false; }
        set { ViewState["isCobalt"] = value; }
    }

    private bool _IsNickelChrome
    {
        get { return (ViewState["NickelChromeClient"] != null) ? bool.Parse(ViewState["NickelChromeClient"].ToString()) : false; }
        set { ViewState["NickelChromeClient"] = value; }
    }
    private bool isNickelChromeRefundAuthorized
    {
        get { return (ViewState["isNickelChromeRefundAuthorized"] != null) ? bool.Parse(ViewState["isNickelChromeRefundAuthorized"].ToString()) : false; }
        set { ViewState["isNickelChromeRefundAuthorized"] = value; }
    }

    private NickelChrome _NickelChrome
    {
        get
        {
            NickelChrome _nickelChrome = null;
            try
            {
                _nickelChrome = (ViewState["NickelChrome"] != null) ? (NickelChrome)ViewState["NickelChrome"] : null;
            }
            catch(Exception ex) { }

            return _nickelChrome;
        }
        set { ViewState["NickelChrome"] = value; }
    }
    #endregion parameters

    [Serializable]
    private class NickelChrome
    {
        public bool IsNickelChromeOrder { get; set; }
        public int NickelChromeCardOrderStatus { get; set; }
        public string NickelChromeCardOrderDate { get; set; }
        public string NickelChromeCardShippingDate { get; set; }
        public string NickelChromeCardShippingAddress { get; set; }
        public string NickelChromeCardName { get; set; }
        public string NickelChromeCardTAG { get; set; }

        public bool IsNickelChromeClient { get; set; }
        public string NickelChromeModelName { get; set; }
        public string NickelChromeEndDate { get; set; }
        public bool NickelChromeRenewal { get; set; }
        public int NickelChromeRefund_RefCREOrder { get; set; }
        public string NickelChromeRefOrder { get; set; }
    }

    

    protected void Page_Init(object sender, EventArgs e)
    {
        string sRef = "";

        if (Request.Params["ref"] != null && Request.Params["ref"].Length > 0)
        {
            sRef = Request.Params["ref"].ToString();
        }
        LightLock1.LockUnlockStatusChanged += new EventHandler(LightLockStatusChanged);
        clientFileUpload1.ClientFileUploaded += new EventHandler(ClientFileUploaded);
        clientFileUpload1.ClientFileUploadError += new EventHandler(ClientFileUploadError);
        capAddress1.AddressFinal += new EventHandler(AddressChanged);
        capEmail1.EmailFinal += new EventHandler(EmailChanged);
    }

    private void AddressChanged(object sender, EventArgs e)
    {
        txtCity.Text = capAddress1.City;
        txtZipcode.Text = capAddress1.Zipcode;
        txtAddress1.Text = (capAddress1.StreetNumber + " " + capAddress1.Street).Trim();
        if (String.IsNullOrWhiteSpace(capAddress1.ComplementCity))
        {
            txtAddress2.Text = capAddress1.Complement1;
            txtAddress3.Text = capAddress1.Complement2;
        }
        else
        {
            txtAddress2.Text = capAddress1.ComplementCity;
            txtAddress3.Text = capAddress1.Complement1;
            txtAddress4.Text = capAddress1.Complement2;
        }

        hdnComplCity.Value = capAddress1.ComplementCity;
        hdnStreetNumber.Value = capAddress1.StreetNumber;
        hdnStreetName.Value = capAddress1.Street;
        hdnAddressQualityCode.Value = capAddress1.QualityCode;

        upAddress.Update();
    }
    private void EmailChanged(object sender, EventArgs e)
    {
        txtMail.Text = capEmail1.Email;
        hdnMailQualityCode.Value = capEmail1.QualityCode;
        upMail.Update();
    }

    protected void LightLockStatusChanged(object sender, EventArgs e)
    {
        string sXmlCustomerInfos = Client.GetCustomerInformations(_iRefCustomer);
        BindCustomerInformations(sXmlCustomerInfos, true);
        LightLock1.RefreshClientDebitStatus(sXmlCustomerInfos);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            btnPrev.Text = "Recherche client";
            btnPrev.PostBackUrl = "~/ClientSearch.aspx";
            hdnMessage.Value = "";
            string sToken = authentication.GetCurrent(false).sToken;

            BindKYCDropdownlistValues(Client.GetKYCChoices());
            ReadOnlyKyc();
            BindCountryList();

            string sRef = "";

            if (Request.Params["ref"] != null && Request.Params["ref"].Length > 0)
            {
                sRef = Request.Params["ref"].ToString();
            }
            else if (Request.Params["No"] != null && Request.Params["No"].Length > 0)
            {
                DataTable dt = Client.GetClientList("", "", "", "", Request.Params["No"].ToString(), "", sToken);
                if (dt.Rows.Count > 0)
                {
                    sRef = dt.Rows[0]["RefCustomer"].ToString();
                    ViewState["RefCustomer"] = sRef;
                }
            }
            else if (Request.Params["account"] != null && Request.Params["account"].Length > 0)
            {
                DataTable dt = Client.GetClientList("", "", "", "", "", Request.Params["account"].ToString(), sToken);
                if (dt.Rows.Count > 0)
                {
                    sRef = dt.Rows[0]["RefCustomer"].ToString();
                    ViewState["RefCustomer"] = sRef;
                }
            }
            else if (Request.Params["iban"] != null && Request.Params["iban"].Length > 0)
            {
                DataTable dt = Client.GetClientList("", "", "", "", "", Request.Params["iban"].ToString(), sToken);
                if (dt.Rows.Count > 0)
                {
                    sRef = dt.Rows[0]["RefCustomer"].ToString();
                    ViewState["RefCustomer"] = sRef;
                }
            }
            int iRef = 0;
            if (int.TryParse(sRef, out iRef) && iRef != 0)
            {
                btnCloseAccount.PostBackUrl = "AccountClosing.aspx?ref=" + iRef;

                _iRefCustomer = iRef;
                string sXmlCustomerInfos = Client.GetCustomerInformations(iRef);
                BindCustomerInformations(sXmlCustomerInfos, true);
                LightLock1.RefCustomer = iRef;
                LightLock1.RefreshClientDebitStatus(sXmlCustomerInfos);


                if (Request.Params["redirect"] != null && Request.Params["redirect"] == "registration")
                    Response.Redirect("RegistrationDetails.aspx?No=" + _sRegistrationCode);

                BindPINBySMSHistory(GetPINBySMSHistory(_iRefCustomer));
                BindSMSHistory(GetSMSHistory(_iRefCustomer));
                SmsGage1.RefCustomer = _iRefCustomer.ToString();
                BindLockUnlockInfos();
                BindAML();
                BindBeneficiaries();
                initTaxResidence(_iRefCustomer);

                SetHistoryChangeID(_iRefCustomer);
                returnFundsList1.RefCustomer = _iRefCustomer;
                btnReturnFunds.PostBackUrl = "Transfer.aspx?fromClient=" + _iRefCustomer.ToString();
                Req1.RefCustomer = _iRefCustomer;

                clientFileManager1.RefCustomer = _iRefCustomer;
                clientFileUpload1.RefCustomer = _iRefCustomer;
                clientFileUpload1.ClearForm = true;

                BindCardPreference();

                proNotificationCounter1.RefCustomer = _iRefCustomer;

                // AVIS
                garnishment1.RefCustomer = _iRefCustomer.ToString();

                BindLockBankSIReasonList();

                clientRelation1.RefCustomer = _iRefCustomer;
                if (!clientRelation1.HasViewRight)
                    panelRelation.Visible = false;
            }
            else Response.Redirect("ClientSearch.aspx" + (!String.IsNullOrWhiteSpace(Request.QueryString.ToString()) ? "?" + Request.QueryString : ""));

            if (Session["ClientAccountNumber"] != null && Session["ClientAccountNumber"].ToString().Trim().Length > 0)
            {
                _sAccountNumber = Session["ClientAccountNumber"].ToString().Trim();
                lblClientAccountNumber.Text = _sAccountNumber;
            }

            panelRedMail.Visible = false;
            rptRedMail.DataSource = Client.getIdenticalAddressList(lblClientRegistrationNumber.Text);
            rptRedMail.DataBind();

            if (rptRedMail.Items.Count > 0)
                panelRedMail.Visible = true;

            CheckRights();

            if (Request.Params["view"] != null && Request.Params["view"].Trim().Length > 0)
                ForceShowTab(Request.Params["view"]);

            hfCardLock_CardNumber.Value = "";

            // CHECK FOR AUTO OPEN ZENDESK
            if (Request.Params["zendesk"] != null && !String.IsNullOrWhiteSpace(Request.Params["zendesk"]))
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "$(window).load(function(){OpenZendeskTab('" + Request.Params["zendesk"] + "');});", true);
            if (Request.Params["svi-auth"] != null && !String.IsNullOrWhiteSpace(Request.Params["svi-auth"]))
            {
                panelSVIAuthenticated.Visible = true;
                if (Request.Params["svi-auth"] == "1")
                {
                    litSVIauth.Text = "Client authentifié sur SVI";
                    panelSVIAuthenticated.CssClass = "svi-headband svi-auth";
                }
                else
                {
                    litSVIauth.Text = "Client non authentifié sur SVI";
                    panelSVIAuthenticated.CssClass = "svi-headband svi-not-auth";
                }

                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "CheckSVIheadbandPosition();", true);
            }
        }
        else
        {
            AmlAlert1.AlertCheckedEvent += new EventHandler(refreshAml);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "InitControlsKEY", "InitControls();InitAML();", true);

            ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
            scriptManager.RegisterPostBackControl(btnIpListExcelExport);
        }

        checkPreviousButtonUrl();
    }

    protected void ForceShowTab(string tabName)
    {
        hdnForceShowTab.Value = "";
        switch (tabName)
        {
            case "aml":
                if (panelAML.Visible)
                    hdnForceShowTab.Value = panelAML.ClientID;
                break;
            case "operations":
                if (panelBalanceOperations.Visible)
                    hdnForceShowTab.Value = panelBalanceOperations.ClientID;
                break;
            case "fatca":
                if (panelDocs.Visible)
                    hdnForceShowTab.Value = panelDocs.ClientID;
                break;
            default:
                hdnForceShowTab.Value = "";
                break;
        }
    }
    protected void checkPreviousButtonUrl()
    {
        string sUrl = "";
        string sButtonText = "";
        if (tools.GetBackUrl(out sUrl, out sButtonText))
        {
            btnPrev.Text = sButtonText;
            btnPrev.PostBackUrl = sUrl;
        }
    }

    private static bool bPersonnalInfoEdit { get; set; }
    private static bool bKYCEdit { get; set; }
    protected void CheckRights()
    {
        panelPersonalInfo.Visible = false;
        panelDocs.Visible = false;
        panelLimits.Visible = false;
        panelPINBySMS.Visible = false;
        panelWEBPassword.Visible = false;
        panelLockUnlock.Visible = false;
        panelSMS.Visible = false;
        btnRegistration.Visible = false;
        panelBalanceOperations.Visible = false;
        //panelCardReplacement.Visible = false;
        panelCardsHisto.Visible = false;
        panelRib.Visible = false;
        panelAML.Visible = false;
        btnForceAddressConfirmation.Visible = false;
        btnGetSummarySheet.Visible = false;
        btnOpenGetSummarySheet.Visible = false;
        panelGarnishment.Visible = false;
        panelAskKYCUpdate.Visible = false;

        bool bPanelPINBySMS = false, bPanelLimits = false; bool bPanelCardReplacement = false;
        bool bPanelLockUnlock = false, bPanelWEBPassword = false;
        panelCards.Visible = false;
        panelWeb.Visible = false;
        panelBeneficiaries.Visible = false;

        isNickelChromeRefundAuthorized = false;

        string sXmlOut = tools.GetRights(authentication.GetCurrent().sToken, "SC_Customer");
        DataTable dtTabs = new DataTable();
        dtTabs.Columns.Add("tab");

        List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Page", "RC");

        if (listRC.Count > 0 && listRC[0] == "0")
        {
            List<string> listAction = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/Page/Action");
            bool bMessagesMenu = false;

            for (int i = 0; i < listAction.Count; i++)
            {
                List<string> listTag = CommonMethod.GetAttributeValues(listAction[i], "Action", "TagAction");
                List<string> listActionAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ActionAllowed");
                List<string> listViewAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ViewAllowed");

                if (listTag.Count > 0)
                {
                    switch (listTag[0])
                    {
                        case "PersonalInfo":
                            if (listViewAllowed.Count > 0 && listViewAllowed[0] == "1")
                            {
                                panelPersonalInfo.Visible = true;
                                dtTabs.Rows.Add(new object[] { "<li><a href=\"#" + panelPersonalInfo.ClientID + "\">Fiche client</a></li>" });

                                bPersonnalInfoEdit = false;
                                if (listActionAllowed.Count > 0 && listActionAllowed[0] == "1")
                                {
                                    bPersonnalInfoEdit = true;


                                    //panelSavePersonalInfo.Visible = true;
                                }
                                else
                                {
                                    rblCivility.Enabled = false;
                                    txtFirstName.ReadOnly = true;
                                    txtLastName.ReadOnly = true;
                                    txtMaidenName.ReadOnly = true;
                                    txtNewLastName.ReadOnly = true;
                                    txtMarriedLastName.ReadOnly = true;
                                    txtBirthDate.ReadOnly = true;
                                    ddlBirthCountry.Enabled = false;
                                    txtBirthDepartment.ReadOnly = true;
                                    txtBirthCity.ReadOnly = true;
                                    txtAddress1.ReadOnly = true;
                                    txtAddress2.ReadOnly = true;
                                    txtAddress3.ReadOnly = true;
                                    txtAddress4.ReadOnly = true;
                                    txtCity.ReadOnly = true;
                                    txtZipcode.ReadOnly = true;
                                    txtPhoneNumber.ReadOnly = true;
                                    txtMail.ReadOnly = true;
                                    capAddress1.Visible = false;
                                    capEmail1.Visible = false;
                                }
                                //else { panelSavePersonalInfo.Visible = false; }
                            }
                            else panelPersonalInfo.Visible = false;
                            break;
                        case "KYC":
                            if (listViewAllowed.Count > 0 && listViewAllowed[0] == "1")
                            {
                                bKYCEdit = false;
                                if (listActionAllowed.Count > 0 && listActionAllowed[0] == "1")
                                {
                                    bKYCEdit = true;
                                    //panelSavePersonalInfo.Visible = true;
                                }
                                //else { panelSavePersonalInfo.Visible = false; }
                            }
                            else panelPersonalInfo.Visible = false;
                            break;
                        case "Document":
                            if (listViewAllowed.Count > 0 && listViewAllowed[0] == "1")
                            {
                                panelDocs.Visible = true;
                                dtTabs.Rows.Add(new object[] { "<li><a href=\"#" + panelDocs.ClientID + "\">Documents</a></li>" });

                                if (listActionAllowed.Count > 0 && listActionAllowed[0] == "1")
                                    panelSaveDocs.Visible = true;
                                else panelSaveDocs.Visible = false;
                            }
                            else panelDocs.Visible = false;
                            break;
                        case "PaymentLimit":
                            if (listViewAllowed.Count > 0 && listViewAllowed[0] == "1")
                            {
                                panelLimits.Visible = true;
                                bPanelPINBySMS = true;
                                if (listActionAllowed.Count > 0 && listActionAllowed[0] == "1")
                                {
                                    foreach (RepeaterItem item in rptLimitRequests.Items)
                                    {
                                        Panel panel = (Panel)item.FindControl("panelLimitRequestsActions");
                                        if (panel != null)
                                            panel.Visible = true;
                                    }
                                }
                                else
                                {
                                    foreach (RepeaterItem item in rptLimitRequests.Items)
                                    {
                                        Panel panel = (Panel)item.FindControl("panelLimitRequestsActions");
                                        if (panel != null)
                                            panel.Visible = false;
                                    }
                                }
                            }
                            else panelLimits.Visible = false;
                            break;
                        case "Pincode":
                            if (listViewAllowed.Count > 0 && listViewAllowed[0] == "1")
                            {
                                panelPINBySMS.Visible = true;
                                bPanelPINBySMS = true;
                                if (listActionAllowed.Count > 0 && listActionAllowed[0] == "1")
                                    panelNewPINBySMS.Visible = true;
                                else panelNewPINBySMS.Visible = false;
                            }
                            else panelPINBySMS.Visible = false;
                            break;
                        case "WebPassword":
                            if (listViewAllowed.Count > 0 && listViewAllowed[0] == "1")
                            {
                                panelWEBPassword.Visible = true;
                                bPanelWEBPassword = true;
                                if (listActionAllowed.Count > 0 && listActionAllowed[0] == "1")
                                {
                                    panelResetWebPassword.Visible = true;
                                    if (hdnAllowCourrierActions.Value != "0")
                                        hdnAllowCourrierActions.Value = "1";
                                }
                                else
                                {
                                    panelResetWebPassword.Visible = false;
                                    if (hdnAllowCourrierActions.Value != "0")
                                        hdnAllowCourrierActions.Value = "0";
                                }
                            }
                            else
                            {
                                panelWEBPassword.Visible = false;
                            }
                            break;
                        case "WebLockUnlock":
                            if (listActionAllowed.Count > 0 && listActionAllowed[0] == "1")
                            {
                                panelLockUnlock.Visible = true;

                                bPanelLockUnlock = true;
                            }
                            else
                            {
                                panelLockUnlock.Visible = false;
                            }
                            break;
                        case "SMS":
                            if (listViewAllowed.Count > 0 && listViewAllowed[0] == "1")
                            {
                                panelSMS.Visible = true;
                                if (!bMessagesMenu)
                                {
                                    dtTabs.Rows.Add(new object[] { "<li><a href=\"#" + panelMessageExchange.ClientID + "\">Messages</a></li>" });
                                    bMessagesMenu = true;
                                }
                            }
                            else panelSMS.Visible = false;
                            break;
                        case "WebMessage":
                            if (listViewAllowed.Count > 0 && listViewAllowed[0] == "1")
                            {
                                panelWebMessage.Visible = true;
                                if (!bMessagesMenu)
                                {
                                    dtTabs.Rows.Add(new object[] { "<li><a href=\"#" + panelMessageExchange.ClientID + "\">Messages</a></li>" });
                                    bMessagesMenu = true;
                                }
                            }
                            else panelWebMessage.Visible = false;

                            if (listActionAllowed.Count > 0 && listActionAllowed[0] == "1")
                                _bActionWebMessage = true;
                            else _bActionWebMessage = false;
                            break;
                        case "RegistrationDetailsLink":
                            if (listActionAllowed.Count > 0 && listActionAllowed[0] == "1")
                                btnRegistration.Visible = true;
                            else btnRegistration.Visible = false;
                            break;
                        case "BalanceOperationsLink":
                            if (listViewAllowed.Count > 0 && listViewAllowed[0] == "1")
                            {
                                panelBalanceOperations.Visible = true;
                                panelBalanceOperationsLink.Visible = true;
                                //dtTabs.Rows.Add(new object[] { "<li><a href=\"#" + panelBalanceOperations.ClientID + "\" onclick=\"balanceOperation();\">Opérations</a></li>" });
                                dtTabs.Rows.Add(new object[] { "<li><a href=\"#" + panelBalanceOperations.ClientID + "\">Opérations</a></li>" });
                            }
                            else panelBalanceOperations.Visible = false;
                            break;
                        case "BankStatement":
                            if (listActionAllowed.Count > 0 && listActionAllowed[0] == "1")
                            {
                                panelBankStatement.Visible = true;
                                //dtTabs.Rows.Add(new object[] { "<li><a href=\"#" + panelBalanceOperations.ClientID + "\" onclick=\"balanceOperation();\">Opérations</a></li>" });
                                if (panelBalanceOperations.Visible == false)
                                {
                                    panelBalanceOperations.Visible = true;
                                    dtTabs.Rows.Add(new object[] { "<li><a href=\"#" + panelBalanceOperations.ClientID + "\">Opérations</a></li>" });
                                }
                            }
                            else panelBankStatement.Visible = false;
                            break;
                        case "CardReplacement":
                            if (listActionAllowed.Count > 0 && listActionAllowed[0] == "1")
                            {
                                //panelCardReplacement.Visible = true;
                                panelCardsHisto.Visible = true;
                                bPanelCardReplacement = true;
                            }
                            else
                            {
                                if (listViewAllowed.Count > 0 && listViewAllowed[0] == "1")
                                {
                                    panelCardsHisto.Visible = true;
                                    _bHideCardActionButtons = true;
                                }
                                else panelCardsHisto.Visible = false;
                            }
                            break;
                        case "RIB":
                            if (listViewAllowed.Count > 0 && listViewAllowed[0] == "1")
                            {
                                panelRibButtons.Visible = false;
                                panelRib.Visible = true;
                                dtTabs.Rows.Add(new object[] { "<li><a href=\"#" + panelRib.ClientID + "\"  onclick=\"$('#" + btnRibRefresh.ClientID + "').click();\">RIB</a></li>" });

                                if (listActionAllowed.Count > 0 && listActionAllowed[0] == "1")
                                    panelRibButtons.Visible = true;
                            }
                            else panelRib.Visible = false;
                            break;
                        case "AML":
                            if (listViewAllowed.Count > 0 && listViewAllowed[0] == "1")
                            {
                                panelAML.Visible = true;
                                dtTabs.Rows.Add(new object[] { "<li><a href=\"#" + panelAML.ClientID + "\">AML</a></li>" });
                                AmlHideAlert1.ActionAllowed = true;

                                if (listActionAllowed.Count == 0 || listActionAllowed[0] != "1")
                                {
                                    btnOpenClientWatch.Visible = false;
                                    AmlHideAlert1.ActionAllowed = false;
                                    panelShowModifyClientProfile.Visible = false;
                                    panelClientNoteAdd.Visible = false;
                                }
                                else
                                {
                                    panelClientNoteAdd.Visible = true;
                                }
                            }
                            else panelAML.Visible = false;
                            break;
                        case "AMLCommentary":
                            if (listViewAllowed.Count > 0 && listViewAllowed[0] == "1")
                            {
                                if (listActionAllowed.Count == 0 || listActionAllowed[0] != "1")
                                {
                                    panelClientNoteAdd.Visible = false;
                                }
                                else panelClientNoteAdd.Visible = true;
                            }
                            //else panelAMLCommentary.Visible = false;
                            break;
                        case "AMLSpecificities":
                            if (listViewAllowed.Count > 0 && listViewAllowed[0] == "1")
                            {
                                panelAMLSpecificities.Visible = true;

                                if (listActionAllowed.Count == 0 || listActionAllowed[0] != "1")
                                {
                                    btnShowAddClientSpecificity2.Visible = false;
                                    Button btn = (Button)rptAML_ClientSpecificities.Controls[rptAML_ClientSpecificities.Controls.Count - 1].Controls[0].FindControl("btnShowAddClientSpecificity1");
                                    if (btn != null)
                                        btn.Visible = false;
                                    Panel panel = (Panel)rptAML_ClientSpecificities.Controls[rptAML_ClientSpecificities.Controls.Count - 1].Controls[0].FindControl("panelShowDelClientSpecificities");
                                    if (panel != null)
                                        panel.Visible = false;
                                }
                            }

                            break;
                        case "AddManualAlert":
                            if (listViewAllowed.Count > 0 && listViewAllowed[0] == "1" && listActionAllowed.Count == 0 || listActionAllowed[0] == "1")
                            {
                                if(!_bAccountClosed)
                                    panelCreateManualAlert.Visible = true;
                            }
                            break;
                        case "Surveillance":
                            if (listViewAllowed.Count > 0 && listViewAllowed[0] == "1")
                            {
                                panelSurveillance.Visible = true;
                            }
                            else
                            {
                                panelSurveillance.Visible = false;
                            }

                            if (listActionAllowed.Count > 0 && listActionAllowed[0] == "1")
                                AmlSurveillance1.ActionAllowed = true;
                            else AmlSurveillance1.ActionAllowed = false;
                            break;
                        case "ForceAddressConfirmation":
                            if (listViewAllowed.Count > 0 && listViewAllowed[0] == "1")
                                btnForceAddressConfirmation.Visible = true;
                            else btnForceAddressConfirmation.Visible = false;
                            break;
                        case "SendSummarySheet":
                            if (listViewAllowed.Count > 0 && listViewAllowed[0] == "1")
                            {
                                btnGetSummarySheet.Visible = true;
                                btnOpenGetSummarySheet.Visible = true;
                            }
                            else
                            {
                                btnGetSummarySheet.Visible = false;
                                btnOpenGetSummarySheet.Visible = false;
                            }
                            break;
                        case "Beneficiaries":
                            if (listViewAllowed.Count > 0 && listViewAllowed[0] == "1")
                            {
                                panelBeneficiaries.Visible = true;
                                dtTabs.Rows.Add(new object[] { "<li><a href=\"#" + panelBeneficiaries.ClientID + "\">Bénéficiaires</a></li>" });
                            }
                            else panelBeneficiaries.Visible = false;
                            break;
                        case "ForceAnnualFee":
                            if (listViewAllowed.Count > 0 && listViewAllowed[0] == "1")
                            {
                                panelForceAnnualFee.Visible = true;
                            }
                            break;
                        case "AccountLockLight":
                            if (listViewAllowed.Count == 0 || listViewAllowed[0] != "1")
                            {
                                panelAccountLockLight.Visible = false;
                            }

                            if (listActionAllowed.Count == 0 || listActionAllowed[0] != "1")
                            {
                                //btnClientDebitLockUnlock.Visible = false;
                                LightLock1.ActionAllowed = false;
                            }
                            break;
                        case "IdentifierHistory":
                            if (listViewAllowed.Count > 0 && listViewAllowed[0] == "1")
                            {
                                bPanelLockUnlock = true;
                            }
                            break;
                        case "GarnishmentList":
                            if (listViewAllowed.Count > 0 && listViewAllowed[0] == "1")
                            {
                                panelGarnishment.Visible = true;
                            }
                            break;
                        case "LinkFastAnalysisSheet":
                            if (listViewAllowed.Count > 0 && listViewAllowed[0] == "1")
                            {
                                btnFastAnalysisSheet.Visible = true;
                            }
                            break;
                        case "KYCUpdate":
                            if (listViewAllowed.Count > 0 && listViewAllowed[0] == "1")
                            {
                                panelAskKYCUpdate.Visible = true;
                            }
                            break;
                        case "CardPreference":
                            if (listActionAllowed.Count > 0 && listActionAllowed[0] == "1")
                            {
                                panelCardPreferences.Visible = true;
                            }
                            break;
                        case "CardLimits":
                            if (listViewAllowed.Count > 0 && listViewAllowed[0] == "1")
                            {
                                panelLimitsContainer.Visible = true;
                            }
                            break;
                        case "CustomCards":
                            if (listViewAllowed.Count > 0 && listViewAllowed[0] == "1")
                            {
                                panelPersonalizedCardOrderList.Visible = true;
                            }
                            break;
                        case "IPConnectionHisto":
                            if (listViewAllowed.Count > 0 && listViewAllowed[0] == "1")
                            {
                                panelIpConnectionList.Visible = true;
                            }
                            break;
                        case "ChangeIDHistory":
                            if (listViewAllowed.Count > 0 && listViewAllowed[0] == "1")
                            {
                                panelIDWebHisto.Visible = true;
                            }
                            break;
                        case "TracfinStatement":
                            if (listViewAllowed.Count > 0 && listViewAllowed[0] == "1")
                            {
                                panelTracfinStatement.Visible = true;
                                searchTracfin1.RefCustomer = _iRefCustomer;
                            }
                            break;
                        case "SabStatus":
                            if (listActionAllowed.Count > 0 && listActionAllowed[0] == "1")
                                imgBankStatus.Attributes["onclick"] = "ShowSabStatusDialog();";
                            break;
                        case "Balance":
                            if (listViewAllowed.Count > 0 && listViewAllowed[0] == "1")
                            {
                                panelBalanceOnly.Visible = true;
                                Balance1.RefCustomer = _iRefCustomer;

                                if (panelBalanceOperations.Visible == false)
                                {
                                    panelBalanceOperations.Visible = true;
                                    dtTabs.Rows.Add(new object[] { "<li><a href=\"#" + panelBalanceOperations.ClientID + "\">Opérations</a></li>" });
                                }
                            }
                            break;
                        case "ChangeLimit":
                            if (listActionAllowed.Count > 0 && listActionAllowed[0] == "1")
                                panelChangeLimit.Visible = true;
                            break;
                        case "NickelChromeRefund":
                            if(listActionAllowed.Count > 0 && listActionAllowed[0] == "1" && CRETools.CheckRights(authentication.GetCurrent().sToken))
                            {
                                isNickelChromeRefundAuthorized = true;
                            }
                            break;
                        case "ReturnFunds":
                            if (listViewAllowed.Count > 0 && listViewAllowed[0] == "1")
                            {
                                panelReturnFunds.Visible = true;

                                if (listActionAllowed.Count > 0 && listActionAllowed[0] == "1")
                                    btnReturnFunds.Visible = true;
                            }
                            break;
                        case "SABCobaltOpeComparison":
                            if (listViewAllowed.Count > 0 && listViewAllowed[0] == "1" && listActionAllowed.Count > 0 && listActionAllowed[0] == "1")
                                panelSABCobaltOpeComparison.Visible = true;
                            break;
                    }
                }
            }

            if(btnNickelChromeCancel.Visible && !isNickelChromeRefundAuthorized) { btnNickelChromeCancel.Visible = false; }

            if (bPanelPINBySMS || bPanelLimits || bPanelCardReplacement)
            {
                panelCards.Visible = true;
                dtTabs.Rows.Add(new object[] { "<li><a href=\"#" + panelCards.ClientID + "\">Cartes</a></li>" });
            }

            if (bPanelLockUnlock || bPanelWEBPassword)
            {
                panelWeb.Visible = true;
                dtTabs.Rows.Add(new object[] { "<li><a href=\"#" + panelWeb.ClientID + "\">Identifiants</a></li>" });
            }

            if (panelGarnishment.Visible == true)
                dtTabs.Rows.Add(new object[] { "<li><a href=\"#" + panelGarnishment.ClientID + "\">Avis</a></li>" });

            rptTabs.DataSource = dtTabs;
            rptTabs.DataBind();

            panelSavePersonalInfo.Visible = false;
            if (bKYCEdit || bPersonnalInfoEdit) { panelSavePersonalInfo.Visible = true; }

            upKYCUpdateStatus.Update();

        }
        else Response.Redirect("Default.aspx", true);

        bool bAction, bView;
        tools.GetTagActionRight(tools.GetRights(authentication.GetCurrent().sToken, "SC_AccountClosing"), "RequestAccountClose", out bAction, out bView);
        if (!bAction)
            btnCloseAccount.Visible = false;
    }
    protected string GetAccountNumber()
    {
        return _sAccountNumber;
    }
    protected void BindCustomerInformations(string sXMLCustomerInfos, bool isKycRefresh)
    {
        List<string> listCivility = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "PolitenessTAG");
        List<string> listMaidenName = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "MaidenName");
        List<string> listLastName = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "LastName");
        List<string> listNewLastName = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "NewLastName");
        List<string> listMarriedLastName = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "MarriedLastName");
        List<string> listFirstName = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "FirstName");
        List<string> listPhoneNumber = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "PhoneNumber");
        List<string> listEmail = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "Email");
        List<string> listEmailQualityCode = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "qualityCodeEmail");
        List<string> listAddress1 = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "Address1");
        List<string> listAddress2 = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "Address2");
        List<string> listAddress3 = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "Address3");
        List<string> listAddress4 = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "Address4");
        List<string> listZipcode = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "Zipcode");
        List<string> listCity = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "City");
        List<string> listStreet = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "street");
        List<string> listStreetNumber = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "streetNumber");
        List<string> listAddressQualityCode = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "qualityCodeAddress");

        List<string> listAskDate = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "AskDate");
        List<string> listCheckDate = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "AddressCheckDate");
        List<string> listCheckSts = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "AddressCheckStatus");
        List<string> listCheckLbl = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "AddressCheckStatusLabel");
        List<string> listLetters = CommonMethod.GetTags(sXMLCustomerInfos, "ALL_XML_OUT/Customer/LETTERS/LETTER");

        List<string> listRegistrationCode = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "RegistrationCode");
        List<string> listRegistrationDate = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "CreationDate");
        List<string> listAccountNumber = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "AccountNumber");
        List<string> listAccountKindTAG = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "AccountKindTAG");
        List<string> listDocs = CommonMethod.GetTags(sXMLCustomerInfos, "ALL_XML_OUT/Customer/DOCS/DOC");
        List<string> listBirthPlace = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "BirthPlace");
        List<string> listBirthDepartment = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "BirthDepartment");
        List<string> listBirthDate = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "BirthDate");
        List<string> listBirthCountry = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "BirthCountryISO2");

        List<string> listLockUnlockStatus = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "IsLocked");
        List<string> listLockUnlockDate = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "LockedDate");
        List<string> listLockUnlockReason = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "LockReason");
        List<string> listWebIdentifier = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "WebIdentifier");

        List<string> listLockCustomerDebit = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "LockCustomerDebit");
        List<string> listLockCustomerDebitRefReason = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "LockCustomerDebitRefReason");

        List<string> listLastActionDate = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "LastActionDate");
        List<string> listLastActionUserBOLastName = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "LastActionUserBOLastName");
        List<string> listLastActionUserBOFirstName = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "LastActionUserBOFirstName");

        List<string> listScoring = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "ClientScore");
        //scoring1.Score = (listScoring.Count > 0 && !String.IsNullOrWhiteSpace(listScoring[0])) ? int.Parse(listScoring[0]) : (int?)null;
        scoring1.XmlCustomerInfos = sXMLCustomerInfos;

        List<string> listScoreChurn = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "ScoreChurn");
        List<string> listScoreChurnDetails = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "ScoreChurnDetails");
        if (listScoreChurn.Count > 0 && !string.IsNullOrWhiteSpace(listScoreChurn[0]))
        {
            churnResult1.Score = listScoreChurn[0];
            churnResult1.ScoreDetails = listScoreChurnDetails[0];
        }
        else panelScoreChurn.Visible = false;

        List<string> listRenew = CommonMethod.GetTags(sXMLCustomerInfos, "ALL_XML_OUT/Customer/ALLRENEWAL/RENEW");

        List<string> listBankStatus = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "BankStatus");
        List<string> listBankStatusLabel = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "BankStatusLabel");
        List<string> listBankStatusHistory = CommonMethod.GetTags(sXMLCustomerInfos, "ALL_XML_OUT/Customer/BANKSTATUSHISTORY/BANKSTATUS");
        List<string> listClosingInProgress = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "ClosingInProgress");
        List<string> listClosingNotifyBefore = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "ClosingNotifyBefore");

        List<string> listWatch = CommonMethod.GetTags(sXMLCustomerInfos, "ALL_XML_OUT/Customer/WATCHS/WATCH");

        List<string> listLockTransfer = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "LockTransfer");
        List<string> listPendingTransfer = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "PendingTransfer");
        List<string> listPendingTransferAmount = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "PendingTransferAmount");

        List<string> listDri = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "DRI");
        List<string> listClientSurendette = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "StatusEndettement");

        List<string> listParentAccounts = CommonMethod.GetTags(sXMLCustomerInfos, "ALL_XML_OUT/Customer/PARENT_ACCOUNTS/ACCOUNT");
        List<string> listOtherAccounts = CommonMethod.GetTags(sXMLCustomerInfos, "ALL_XML_OUT/Customer/ACCOUNTS/ACCOUNT");

        List<string> listActiveAlertRefBOUser = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "ActiveAlertRefBOUser");
        List<string> listActiveAlertBOUser = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "ActiveAlertBOUser");

        List<string> listIsCobalt = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "IsCobalt");
        List<string> listIsCobaltMigrated = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "IsCobaltMigrated");
        List<string> listCobaltMigrationDate = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "MigrationDate");

        List<string> listIBAN = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "IBAN");

        _isCobalt = false; bool isConverted = false;
        if (listIsCobalt != null && listIsCobalt.Count > 0)
        {
            if (listIsCobalt[0] == "1")
            {
                _isCobalt = true;
                imgBankStatus.ToolTip = "Client Cobalt";
                imgBankStatus.ImageUrl = @"~/Styles/Img/Cobalt_logo.png";

                if (listIsCobaltMigrated != null && listIsCobaltMigrated.Count > 0 && listIsCobaltMigrated[0] == "1")
                {
                    isConverted = true;
                    imgBankStatus.ToolTip = "Client SAB migré Cobalt";
                    imgBankStatus.ToolTip += (listCobaltMigrationDate.Count > 0 && !string.IsNullOrWhiteSpace(listCobaltMigrationDate[0]))? " le " + listCobaltMigrationDate[0].Trim().Substring(0,16) : "";
                    imgBankStatus.ImageUrl = @"~/Styles/Img/cobalt_converted.png";
                }
				pGestionMandat.Visible = true;
            }

            hdnIsCobalt.Value = listIsCobalt[0];
        }
        hdnIBAN.Value = (listIBAN != null && listIBAN.Count > 0) ? listIBAN[0] : "";

        if (listBankStatusHistory.Count > 0)
        {
            DataTable dtBankStatusHistory = new DataTable();
            dtBankStatusHistory.Columns.Add("Date");
            dtBankStatusHistory.Columns.Add("Old");
            dtBankStatusHistory.Columns.Add("New");
            dtBankStatusHistory.Columns.Add("BOUser");

            for (int i = 0; i < listBankStatusHistory.Count; i++)
            {
                List<string> listDate = CommonMethod.GetAttributeValues(listBankStatusHistory[i], "BANKSTATUS", "ChangeDate");
                List<string> listOld = CommonMethod.GetAttributeValues(listBankStatusHistory[i], "BANKSTATUS", "OldStatus");
                List<string> listNew = CommonMethod.GetAttributeValues(listBankStatusHistory[i], "BANKSTATUS", "NewStatus");
                List<string> listBoUser = CommonMethod.GetAttributeValues(listBankStatusHistory[i], "BANKSTATUS", "BOUser");

                dtBankStatusHistory.Rows.Add(new object[] { listDate[0], listOld[0], listNew[0], listBoUser.Count > 0 ? listBoUser[0] : "" });
            }

            rptSabStatusHistory.DataSource = dtBankStatusHistory;
            rptSabStatusHistory.DataBind();
        }
        if (listBankStatus.Count > 0)// && listBankStatus[0] != "0"
        {
                
            ddlSabStatus.SelectedValue = listBankStatus[0];
            btnReopenAccount.Visible = false;

            string sImgUrlSuffix = "";
            string sImgUrlSAB = "~/Styles/Img/sab";
            string sImgUrlCobalt = "~/Styles/Img/cobalt";
            string sImgUrlCobaltConverted = "~/Styles/Img/cobalt_converted";
            switch (listBankStatus[0])
            {
                case "0":
                    ddlLockSIReason.Attributes["style"] = "display:none;width:34%;box-sizing:border-box";
                    ddlSabStatus.Attributes["style"] = "width:75%;box-sizing:border-box";
                    break;
                case "1":
                    sImgUrlSuffix += "_bloque_debit";
                    break;
                case "2":
                    sImgUrlSuffix += "_bloque_credit";
                    break;
                case "3":
                    sImgUrlSuffix += "_bloque_debit_credit";
                    break;
                case "4":
                    sImgUrlSuffix += "_cloture";
                    panelCreateManualAlert.Visible = false;
                    btnCloseAccount.Visible = false;
                    _bAccountClosed = true;
                    panelActiveAlert.Visible = false;

                    bool bAction, bView;
                    tools.GetTagActionRight(tools.GetRights(authentication.GetCurrent().sToken, "SC_ReopenAccount"), "ReopenAccount", out bAction, out bView);
                    if (bAction && bView) { btnReopenAccount.Visible = true; }
                    break;
            }

            //TEST
            //sImgUrlSuffix = ""; isConverted = false;

            if (!_isCobalt)
            {
                imgBankStatus.Height = 40;
                imgBankStatus.ImageUrl = sImgUrlSAB + sImgUrlSuffix + ".png";
                imgBankStatus.ToolTip = String.Format("<b>État SAB</b> : {0} ({1})", (listBankStatusLabel.Count > 0) ? listBankStatusLabel[0] : "Inconnu", listBankStatus[0]);
            }
            else
            {
                imgBankStatus.Height = 60;
                imgBankStatus.ImageUrl = sImgUrlCobalt + sImgUrlSuffix + ".png";
                if (isConverted)
                    imgBankStatus.ImageUrl = sImgUrlCobaltConverted + sImgUrlSuffix + ".png";
                imgBankStatus.ToolTip += "<br/>" + String.Format("<b>État Cobalt</b> : {0} ({1})", (listBankStatusLabel.Count > 0) ? listBankStatusLabel[0] : "Inconnu", listBankStatus[0]);
            }
        }

        activity1.XmlCustomerInfos = sXMLCustomerInfos;
        deathStatus1.XmlCustomerInfos = sXMLCustomerInfos;
        deathStatus1.iRefCustomer = _iRefCustomer;

        if (listRenew.Count > 0)
        {
            List<string> listWantCloseDate = CommonMethod.GetAttributeValues(listRenew[0], "RENEW", "WantToCloseDate");
            List<string> listPaid = CommonMethod.GetAttributeValues(listRenew[0], "RENEW", "Paid");
            List<string> listRenewFailed = CommonMethod.GetAttributeValues(listRenew[0], "RENEW", "WarningMailBalanceIssue");
            List<string> listSMSFailed = CommonMethod.GetAttributeValues(listRenew[0], "RENEW", "WarningSMSDate");
            List<string> listPending = CommonMethod.GetAttributeValues(listRenew[0], "RENEW", "Pending");

            if (listWantCloseDate.Count > 0 && listWantCloseDate[0].Length > 0)
            {
                imgAnnualFeeStatus.ImageUrl = "~/Styles/Img/annual_fee_closed.png";
                imgAnnualFeeStatus.ToolTip = "Statut renouvellement : demande de cloture (" + listWantCloseDate[0] + ")";
                imgAnnualFeeStatus.Visible = true;
            }
            else if (listPending.Count > 0 && listPending[0] == "1")
            {
                imgAnnualFeeStatus.ImageUrl = "~/Styles/Img/annual_fee_pending.png";
                imgAnnualFeeStatus.ToolTip = "Statut renouvellement : en attente";
                lblAnnualFeeStatus.Text = "En attente";
                imgAnnualFeeStatus.Attributes["style"] = "margin-left:10px;cursor:pointer";
                imgAnnualFeeStatus.Attributes["onclick"] = "ShowAnnualFeeStatus();";
                imgAnnualFeeStatus.Visible = true;
            }
            else if (((listRenewFailed.Count > 0 && listRenewFailed[0].Length > 0) || (listSMSFailed.Count > 0 && listSMSFailed[0].Length > 0)) && listPaid.Count > 0 && listPaid[0] == "0")
            {
                string sDate = (listRenewFailed.Count > 0 && listRenewFailed[0].Length > 0) ? listRenewFailed[0] : listSMSFailed[0];
                imgAnnualFeeStatus.ImageUrl = "~/Styles/Img/annual_fee_failed.png";
                imgAnnualFeeStatus.ToolTip = "Statut renouvellement : échec prélèvement (" + sDate + ")";
                lblAnnualFeeStatus.Text = "Echec prélèvement (" + sDate + ")";
                imgAnnualFeeStatus.Attributes["onclick"] = "ShowAnnualFeeStatus();";
                imgAnnualFeeStatus.Attributes["style"] = "margin-left:10px;cursor:pointer";
                imgAnnualFeeStatus.Visible = true;
            }
            else imgAnnualFeeStatus.Visible = false;
        }

        LightLock1.RefreshClientDebitStatus(sXMLCustomerInfos);

        if (listClosingInProgress.Count > 0 && listClosingInProgress[0] == "1"
            && listClosingNotifyBefore.Count > 0 && listClosingNotifyBefore[0] == "0")
        {
            btnCloseAccount.Visible = false;
            panelActiveAlert.Visible = false;
            panelCreateManualAlert.Visible = false;
            _bAccountClosed = true;

            litCloseAccountStatus.Text = "Demande de clôture immédiate active";
            litCloseAccountStatus.Visible = true;
        }
        else if (listClosingNotifyBefore.Count > 0 && listClosingNotifyBefore[0] == "1")
        {
            litCloseAccountStatus.Text = "Demande de clôture avec préavis active";
            litCloseAccountStatus.Visible = true;
        }
        else litCloseAccountStatus.Visible = false;

        if ((listClosingInProgress.Count == 0 || listClosingInProgress[0] == "0") && 
            listActiveAlertRefBOUser.Count > 0 && listActiveAlertRefBOUser[0].Length > 0 &&
            (listBankStatus.Count == 0 || listBankStatus[0] != "4"))
        {
            panelActiveAlert.Visible = true;
            imgActiveAlert.ToolTip = "Alerte en cours de traitement par " + listActiveAlertBOUser[0];
        }

        lblClientCardNumber.Text = (listWebIdentifier.Count > 0) ? listWebIdentifier[0] : "";

        //_bLocked = (listLockUnlockStatus.Count > 0 && bool.TryParse(listLockUnlockStatus[0].ToString(), out bLocked)) ? bLocked : false;
        _bLocked = (listLockUnlockStatus.Count > 0 && listLockUnlockStatus[0].ToString().Trim() == "1") ? true : false;
        if (_bLocked)
        {
            _sLockedDate = (listLockUnlockDate.Count > 0) ? listLockUnlockDate[0] : "";
            _sLockReason = (listLockUnlockReason.Count > 0) ? listLockUnlockReason[0] : "";
        }

        string sCivility = (listCivility.Count > 0) ? listCivility[0] : "";
        if (sCivility.Length > 0)
        {
            ListItem item = rblCivility.Items.FindByValue(sCivility);
            if (item != null)
                rblCivility.SelectedIndex = rblCivility.Items.IndexOf(item);
        }

        txtFirstName.Text = (listFirstName.Count > 0) ? listFirstName[0] : "";
        txtLastName.Text = (listLastName.Count > 0) ? listLastName[0] : "";
        txtNewLastName.Text = (listNewLastName.Count > 0) ? listNewLastName[0] : "";
        txtMarriedLastName.Text = (listMarriedLastName.Count > 0) ? listMarriedLastName[0] : "";
        txtMaidenName.Text = (listMaidenName.Count > 0) ? listMaidenName[0] : "";
        lblClientName.Text = sCivility.Replace("MR", "M.") + " " + txtFirstName.Text + " " + txtLastName.Text;

        txtBirthDate.Text = (listBirthDate.Count > 0) ? listBirthDate[0] : "";
        txtBirthCity.Text = (listBirthPlace.Count > 0) ? listBirthPlace[0] : "";
        txtBirthDepartment.Text = (listBirthDepartment.Count > 0) ? listBirthDepartment[0] : "";
        if (listBirthCountry.Count > 0)
        {
            ListItem item = ddlBirthCountry.Items.FindByValue(listBirthCountry[0]);
            if (item != null)
                ddlBirthCountry.SelectedIndex = ddlBirthCountry.Items.IndexOf(item);
        }

        txtAddress1.Text = (listAddress1.Count > 0) ? listAddress1[0] : "";
        txtAddress2.Text = (listAddress2.Count > 0) ? listAddress2[0] : "";
        txtAddress3.Text = (listAddress3.Count > 0) ? listAddress3[0] : "";
        txtAddress4.Text = (listAddress4.Count > 0) ? listAddress4[0] : "";
        txtZipcode.Text = (listZipcode.Count > 0) ? listZipcode[0] : "";
        txtCity.Text = (listCity.Count > 0) ? listCity[0] : "";
        if (listAddressQualityCode != null && listAddressQualityCode.Count > 0 && !String.IsNullOrWhiteSpace(listAddressQualityCode[0]))
        {
            string sLabel = "";
            if (capAddress1.dicQualityCode.TryGetValue(listAddressQualityCode[0], out sLabel))
            {
                lblCapAdresseStatus.Attributes["title"] = sLabel;

                string sImgPath = "~/Styles/Img/";
                switch (listAddressQualityCode[0])
                {
                    case "00":
                        imgCapAdresseStatus.ImageUrl = sImgPath + "green_status.png";
                        break;
                    case "60":
                        imgCapAdresseStatus.ImageUrl = sImgPath + "yellow_status.png";
                        break;
                    default:
                        imgCapAdresseStatus.ImageUrl = sImgPath + "red_status.png";
                        break;
                }
            }
        }
        else lblCapAdresseStatus.Visible = false;

        System.Drawing.Color cCheckColor = System.Drawing.Color.Black;
        if (listCheckSts.Count > 0)
        {
            string sImgPath = "~/Styles/Img/";
            hdnAllowCourrierActions.Value = "1";
            switch (listCheckSts[0])
            {
                case "1":
                case "2":
                    imgCheckAddressStatus.ImageUrl = sImgPath + "yellow_status.png";
                    cCheckColor = System.Drawing.Color.Gold;
                    break;
                case "3":
                    imgCheckAddressStatus.ImageUrl = sImgPath + "green_status.png";
                    cCheckColor = System.Drawing.Color.LimeGreen;
                    hdnAllowCourrierActions.Value = "0";
                    break;
                case "4":
                    imgCheckAddressStatus.ImageUrl = sImgPath + "red_status.png";
                    cCheckColor = System.Drawing.Color.Red;
                    break;
                default:
                    imgCheckAddressStatus.ImageUrl = sImgPath + "gray_status.png";
                    cCheckColor = System.Drawing.Color.Gray;
                    break;
            }
        }
        if (listCheckLbl.Count > 0)
        {
            lblCheckAddressLink.Attributes["title"] = listCheckLbl[0];
            lblCheckAddressStatus.Text = listCheckLbl[0].ToString();
            lblCheckAddressStatus.ForeColor = cCheckColor;
        }
        lblCheckAddressAskDate.Text = (listAskDate.Count > 0 && listAskDate[0].Length > 0) ? listAskDate[0] : "-";
        lblCheckAddressCheckDate.Text = (listCheckDate.Count > 0 && listCheckDate[0].Length > 0) ? listCheckDate[0] : "-";

        if (listLetters.Count > 0)
        {
            panelHistoCheckAddress.Visible = true;
            DataTable dt = new DataTable();
            dt.Columns.Add("Address");
            dt.Columns.Add("SentDate");

            for (int i = 0; i < listLetters.Count; i++)
            {
                List<string> listLetterAddress1 = CommonMethod.GetAttributeValues(listLetters[i], "LETTER", "Address1");
                List<string> listLetterAddress2 = CommonMethod.GetAttributeValues(listLetters[i], "LETTER", "Address2");
                List<string> listLetterAddress3 = CommonMethod.GetAttributeValues(listLetters[i], "LETTER", "Address3");
                List<string> listLetterAddress4 = CommonMethod.GetAttributeValues(listLetters[i], "LETTER", "Address4");
                List<string> listLetterCity = CommonMethod.GetAttributeValues(listLetters[i], "LETTER", "City");
                List<string> listLetterZipcode = CommonMethod.GetAttributeValues(listLetters[i], "LETTER", "Zipcode");
                List<string> listLetterSentDate = CommonMethod.GetAttributeValues(listLetters[i], "LETTER", "SentDate");

                string sAddress = (listLetterAddress1.Count > 0 && listLetterAddress1[0].Length > 0) ? listLetterAddress1[0] : "";
                sAddress += (listLetterAddress2.Count > 0 && listLetterAddress2[0].Length > 0) ? "<br/>" + listLetterAddress2[0] : "";
                sAddress += (listLetterAddress3.Count > 0 && listLetterAddress3[0].Length > 0) ? "<br/>" + listLetterAddress3[0] : "";
                sAddress += (listLetterAddress4.Count > 0 && listLetterAddress4[0].Length > 0) ? "<br/>" + listLetterAddress4[0] : "";

                string sCity = (listLetterZipcode.Count > 0 && listLetterZipcode[0].Length > 0) ? listLetterZipcode[0] : "";
                sCity += (listLetterCity.Count > 0 && listLetterCity[0].Length > 0) ? " " + listLetterCity[0] : "";

                sAddress += (sCity.Length > 0) ? "<br/>" + sCity : "";

                dt.Rows.Add(new object[] { sAddress, (listLetterSentDate.Count > 0) ? listLetterSentDate[0] : "NC" });
            }

            rptHistoCheckAddress.DataSource = dt;
            rptHistoCheckAddress.DataBind();
        }
        else
        {
            panelHistoCheckAddress.Visible = false;
        }

        if (listPhoneNumber.Count > 0)
        {
            txtPhoneNumber.Text = listPhoneNumber[0];
            lblCurrentPhone2.Text = listPhoneNumber[0];
        }
        if (listEmail.Count > 0)
        {
            txtMail.Text = listEmail[0];
            capEmail1.Email = listEmail[0];
            hdnMail.Value = listEmail[0];
            lblCurrentEmail.Text = listEmail[0];
            lblCurrentEmail2.Text = listEmail[0];
        }
        
        if (listEmailQualityCode != null && listEmailQualityCode.Count > 0 && !String.IsNullOrWhiteSpace(listEmailQualityCode[0]))
        {
            string sLabel = "";
            if (capEmail1.dicQualityCode.TryGetValue(listEmailQualityCode[0], out sLabel))
            {
                lblCapEmailStatus.Attributes["title"] = sLabel;

                string sImgPath = "~/Styles/Img/";
                switch (listEmailQualityCode[0])
                {
                    case "0":
                    case "1":
                        imgCapEmailStatus.ImageUrl = sImgPath + "green_status.png";
                        break;
                    default:
                        imgCapEmailStatus.ImageUrl = sImgPath + "red_status.png";
                        break;
                }
            }
        }
        else lblCapEmailStatus.Visible = false;

        string sAccountKindTAG = listAccountKindTAG[0].Trim();
        if (listRegistrationCode.Count > 0 && listRegistrationCode[0].Length > 0 && listAccountKindTAG.Count > 0 && listAccountKindTAG[0].Trim().Length > 0)
        {
            _sRegistrationCode = listRegistrationCode[0];
            hdnRegistrationCode.Value = _sRegistrationCode;
            btnRegistration.Attributes.Add("onclick", "GoToRegistration('" + listRegistrationCode[0] + "');");

            lblClientRegistrationNumber.Text = listRegistrationCode[0];
            //lblClientRegistrationDate.Text = listRegistrationDate[0].Trim().Substring(0, 16);
            lblRegistrationDate.Text = listRegistrationDate[0].Trim().Substring(0, 16);

            //Label lblClientRegistrationNumber1218 = (Label)rptAccounts1218.FindControl("lblClientRegistrationNumber1218");
            //if(lblClientRegistrationNumber1218 != null) { lblClientRegistrationNumber1218.Text = listRegistrationCode[0]; }

            //Label lblClientRegistrationNumberParent = (Label)rptAccounts1218.FindControl("lblClientRegistrationNumberParent");
            //if (lblClientRegistrationNumberParent != null) { lblClientRegistrationNumberParent.Text = listRegistrationCode[0]; }

            if (sAccountKindTAG.Length >= 3)
            {
                imgAccountType.ImageUrl = tools.GetAccountTypeImg(sAccountKindTAG);
                string sStyle = "width:50px;position:absolute;left:0";

                switch (sAccountKindTAG)
                {
                    case "NOB":
                        lblAccountType.Text = "Compte particulier";
                        sStyle += ";top:-3px";
                        break;
                    case "CNJ":
                        lblAccountType.Text = "Compte mineur";
                        sStyle += ";top:-11px";
                        break;
                    case "CNV":
                        lblAccountType.Text = "Représentant légal sans compte";
                        break;
                    case "CNP":
                        lblAccountType.Text = "Compte professionnel";
                        break;
                    case "CNT":
                        lblAccountType.Text = "Compte sous tutelle";
                        break;
                    case "FCN":
                        lblAccountType.Text = "Faux compte particulier";
                        break;
                }

                imgAccountType.Attributes["style"] = sStyle;
            }
        }
        else
        {
            btnRegistration.Visible = false;
            bool isBuraliste = false;

            try
            {
                if (listWebIdentifier.Count > 0 && listWebIdentifier[0].Substring(0, 3).ToUpper() == "PRO")
                {
                    lblAccountType.Text = "Compte buraliste";
                    imgAccountType.ImageUrl = tools.GetAccountTypeImg("PRO");
                    imgAccountType.Attributes["style"] = "width:20px;position:absolute;left:23px;top:-16px";
                    isBuraliste = true;
                }
            }
            catch(Exception ex) { isBuraliste = false; }

            if (!isBuraliste && sAccountKindTAG.Length >= 3 && sAccountKindTAG == "FCN")
            {
                lblAccountType.Text = "Faux compte particulier";
                imgAccountType.ImageUrl = tools.GetAccountTypeImg(sAccountKindTAG);
                imgAccountType.Attributes["style"] = "width:50px;position:absolute;left:0";
                if (lblClientCardNumber.Text.Trim().Length == 0)
                {
                    lblClientCardNumber.Text = "Non renseigné";
                    lblClientCardNumber.Attributes["style"] = "color:#aaa; font-style: italic;";
                }
                if (lblRegistrationDate.Text.Trim().Length == 0)
                {
                    lblRegistrationDate.Text = "Non renseigné";
                    lblRegistrationDate.Attributes["style"] = "color:#aaa; font-style: italic;";
                }
            }
        }

        if (listAccountNumber.Count > 0)
        {
            Session["ClientAccountNumber"] = listAccountNumber[0];
            hdnRefCustomer.Value = _iRefCustomer.ToString();
        }

        for (int i = 0; i < listDocs.Count; i++)
        {
            List<string> listRef = CommonMethod.GetAttributeValues(listDocs[i], "DOC", "RefDocument");
            List<string> listCat = CommonMethod.GetAttributeValues(listDocs[i], "DOC", "CategoryTAG");
            List<string> listType = CommonMethod.GetAttributeValues(listDocs[i], "DOC", "DocumentTAG");
            List<string> listExpire = CommonMethod.GetAttributeValues(listDocs[i], "DOC", "ExpireDate");
            List<string> listDelivery = CommonMethod.GetAttributeValues(listDocs[i], "DOC", "DeliveryDate");
            List<string> listNumber = CommonMethod.GetAttributeValues(listDocs[i], "DOC", "DocumentNumber");

            if (listCat.Count > 0)
            {
                switch (listCat[0])
                {
                    case "ID":
                        txtDeliveryDateID.Text = (listDelivery.Count > 0) ? listDelivery[0] : "";
                        txtExpireDateID.Text = (listExpire.Count > 0) ? listExpire[0] : "";
                        txtNumberID.Text = (listNumber.Count > 0) ? listNumber[0] : "";
                        hdnRefDocID.Value = (listRef.Count > 0) ? listRef[0] : "";
                        break;
                    case "HO":
                        hdnRefDocHO.Value = (listRef.Count > 0) ? listRef[0] : "";
                        if (listType.Count > 0)
                        {
                            switch (listType[0])
                            {
                                case "FE":
                                    txtDeliveryDateHO.Text = (listDelivery.Count > 0) ? listDelivery[0] : "";
                                    txtExpireDateHO.Text = (listExpire.Count > 0) ? listExpire[0] : "";
                                    break;
                                default:
                                    txtDeliveryDateHO.Enabled = false;
                                    txtExpireDateHO.Enabled = false;
                                    break;
                            }
                        }
                        break;
                }
            }
        }

        if (listWatch.Count > 0)
        {
            List<string> listActive = CommonMethod.GetAttributeValues(listWatch[0], "WATCH", "IsActive");

            if (listActive.Count > 0)
                _bSousSurveillance = (listActive[0] == "1") ? true : false;

            // Historique
            //for (int i = 0; i < listWatch.Count; i++)
            //{

            //}
        }

        if (listLockTransfer.Count > 0)
            _bLockTransfer = (listLockTransfer[0] == "1") ? true : false;
        if (listPendingTransfer.Count > 0)
            _bRequestTransferValidation = (listPendingTransfer[0] == "1") ? true : false;
        if (listPendingTransferAmount.Count > 0)
            _sRequestTransferValidationAmount = listPendingTransferAmount[0];


        string sDri = "";
        string sClientSurendette = "";
        //DRI
        sDri = (listDri.Count > 0) ? listDri[0] : "";
        //Client surendette
        sClientSurendette = (listClientSurendette.Count > 0) ? listClientSurendette[0] : "";

        //TEST
        //sDri = "DRIKO"; sClientSurendette = "CS";

        AddHeadBand(sClientSurendette, sDri);

        // PARENTS
        if (listParentAccounts.Count > 0)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("ref");
            dt.Columns.Add("name");
            dt.Columns.Add("cardnumber");
            dt.Columns.Add("accountnumber");

            for (int i = 0; i < listParentAccounts.Count; i++)
            {
                List<string> listRef = CommonMethod.GetAttributeValues(listParentAccounts[i], "ACCOUNT", "RefCustomer");
                List<string> listParentCivility = CommonMethod.GetAttributeValues(listParentAccounts[i], "ACCOUNT", "PolitenessTAG");
                string sParentCivility = listParentCivility[0].Replace("MR", "M.");
                List<string> listParentLastName = CommonMethod.GetAttributeValues(listParentAccounts[i], "ACCOUNT", "LastName");
                List<string> listParentFirstName = CommonMethod.GetAttributeValues(listParentAccounts[i], "ACCOUNT", "FirstName");
                List<string> listCardNumber = CommonMethod.GetAttributeValues(listParentAccounts[i], "ACCOUNT", "BarCode");
                List<string> listParentAccountNumber = CommonMethod.GetAttributeValues(listParentAccounts[i], "ACCOUNT", "AccountNumber");
                dt.Rows.Add(new object[] { listRef[0], sParentCivility + " " + listParentFirstName[0] + " " + listParentLastName[0], listCardNumber[0], listParentAccountNumber[0] });
            }

            rptParentAccounts.DataSource = dt;
            rptParentAccounts.DataBind();
        }
        else rptParentAccounts.Visible = false;

        // 12/18 ans
        if (listOtherAccounts.Count > 0)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("ref");
            dt.Columns.Add("name");
            dt.Columns.Add("cardnumber");
            dt.Columns.Add("accountnumber");

            for (int i = 0; i < listOtherAccounts.Count; i++)
            {
                List<string> listRef = CommonMethod.GetAttributeValues(listOtherAccounts[i], "ACCOUNT", "RefCustomer");
                List<string> listParentCivility = CommonMethod.GetAttributeValues(listOtherAccounts[i], "ACCOUNT", "PolitenessTAG");
                string sParentCivility = listParentCivility[0].Replace("MR", "M.");
                List<string> listParentLastName = CommonMethod.GetAttributeValues(listOtherAccounts[i], "ACCOUNT", "LastName");
                List<string> listParentFirstName = CommonMethod.GetAttributeValues(listOtherAccounts[i], "ACCOUNT", "FirstName");
                List<string> listCardNumber = CommonMethod.GetAttributeValues(listOtherAccounts[i], "ACCOUNT", "BarCode");
                List<string> listChildAccountNumber = CommonMethod.GetAttributeValues(listOtherAccounts[i], "ACCOUNT", "AccountNumber");
                List<string> listAccountKind = CommonMethod.GetAttributeValues(listOtherAccounts[i], "ACCOUNT", "AccountKindTAG");

                if (listAccountKind.Count > 0 && (listAccountKind[0] == "CNJ" || listAccountKind[0] == "CNT"))
                    dt.Rows.Add(new object[] { listRef[0], sParentCivility + " " + listParentFirstName[0] + " " + listParentLastName[0], listCardNumber[0], listChildAccountNumber[0] });
            }

            rptAccounts1218.DataSource = dt;
            rptAccounts1218.DataBind();
        }
        else rptAccounts1218.Visible = false;

        //IP CONNECTION LIST
        IpConnectionList1.iRefCustomer = _iRefCustomer;
        IpConnectionList1.Type = rblIpConnectionListType.SelectedValue;

        //CUSTOMER PERSONALIZED CARD ORDER LIST
        PersonalizedCardOrderList1.iRefCustomer = _iRefCustomer;

        //KYC
        if (isKycRefresh) { BindKYCValues(sXMLCustomerInfos); }

        //NICKEL CHROME
        BindNickelChrome(sXMLCustomerInfos);
    }
    protected void BindCountryList()
    {
        DataTable dt = tools.GetCountryList();

        ddlBirthCountry.DataSource = dt;
        ddlBirthCountry.DataBind();
    }
    protected void BindNickelChrome(string sXMLCustomerInfos)
    {
        try
        {
            _NickelChrome = new NickelChrome();
            List<string> Values = new List<string>();

            Values = XMLMethodLibrary.CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "isNickelChromeOrder");
            _NickelChrome.IsNickelChromeOrder = (Values != null && Values.Count > 0 && Values[0].Trim().Length > 0 && Values[0].Trim() == "1") ? true : false;
            Values = XMLMethodLibrary.CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "NickelChromeCardOrderStatus");
            int iNickelChromeCardOrderStatus = 0;
            _NickelChrome.NickelChromeCardOrderStatus = (Values != null && Values.Count > 0 && Values[0].Trim().Length > 0 && int.TryParse(Values[0], out iNickelChromeCardOrderStatus)) ? iNickelChromeCardOrderStatus : 0;
            Values = XMLMethodLibrary.CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "NickelChromeCardOrderDate");
            _NickelChrome.NickelChromeCardOrderDate = (Values != null && Values.Count > 0 && Values[0].Trim().Length > 0) ? Values[0] : "";
            Values = XMLMethodLibrary.CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "NickelChromeCardShippingDate");
            _NickelChrome.NickelChromeCardShippingDate = (Values != null && Values.Count > 0 && Values[0].Trim().Length > 0) ? Values[0] : "";
            Values = XMLMethodLibrary.CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "NickelChromeCardShippingAddress");
            _NickelChrome.NickelChromeCardShippingAddress = (Values != null && Values.Count > 0 && Values[0].Trim().Length > 0) ? Values[0] : "";
            Values = XMLMethodLibrary.CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "NickelChromeCardName");
            _NickelChrome.NickelChromeCardName = (Values != null && Values.Count > 0 && Values[0].Trim().Length > 0) ? Values[0] : "";
            Values = XMLMethodLibrary.CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "NickelChromeCardTAG");
            _NickelChrome.NickelChromeCardTAG = (Values != null && Values.Count > 0 && Values[0].Trim().Length > 0) ? Values[0] : "";

            Values = XMLMethodLibrary.CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "isNickelChromeClient");
            _NickelChrome.IsNickelChromeClient = (Values != null && Values.Count > 0 && Values[0].Trim().Length > 0 && Values[0].Trim() == "1") ? true : false;
            Values = XMLMethodLibrary.CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "NickelChromeModelName");
            _NickelChrome.NickelChromeModelName = (Values != null && Values.Count > 0 && Values[0].Trim().Length > 0) ? Values[0] : "Nickel-Chrome-orange.png";
            Values = XMLMethodLibrary.CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "NickelChromeEndDate");
            _NickelChrome.NickelChromeEndDate = (Values != null && Values.Count > 0 && Values[0].Trim().Length > 0) ? Values[0] : "";
            Values = XMLMethodLibrary.CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "NickelChromeRenewal");
            _NickelChrome.NickelChromeRenewal = (Values != null && Values.Count > 0 && Values[0].Trim().Length > 0 && Values[0].Trim() == "1") ? true : false;
            Values = XMLMethodLibrary.CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "NickelChromePendingRefCREOrder"); 
            try { _NickelChrome.NickelChromeRefund_RefCREOrder = (Values != null && Values.Count > 0 && Values[0].Trim().Length > 0) ? int.Parse(Values[0]) : 0; }
            catch (Exception ex) { _NickelChrome.NickelChromeRefund_RefCREOrder = 0; }

            //TEST
            //_NickelChrome.IsNickelChromeOrder = true;
            //_NickelChrome.NickelChromeCardOrderDate = "01/05/2018";
            //_NickelChrome.NickelChromeCardShippingDate = "13/05/2018";
            //_NickelChrome.NickelChromeModelName = "Nickel-Chrome-black.png";

            _IsNickelChrome = (_NickelChrome.IsNickelChromeClient || _NickelChrome.IsNickelChromeOrder) ? true : false;

            InitNickelChrome();
        }
        catch(Exception ex)
        {

        }

        upNickelChromeInformations.Update();
        upNickelChromeReorder.Update();
    }
    protected void InitNickelChrome()
    {
        try
        {
            if (_IsNickelChrome)
            {
                panelNickelChrome.Visible = true;
                lblNickelChromeOrderDate.Text = _NickelChrome.NickelChromeCardOrderDate;
                imgClientNickelChromeCard.ImageUrl = "Styles/Img/nickel-chrome/" + _NickelChrome.NickelChromeModelName;

                panelNickelChromeReorderPossible.Visible = false;

                if (_NickelChrome.IsNickelChromeOrder && _NickelChrome.NickelChromeCardOrderStatus >= 3)
                {
                    DateTime dtToday = DateTime.Now;
                    DateTime dtShippingDate = DateTime.ParseExact(_NickelChrome.NickelChromeCardShippingDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    DateTime dtOrderDate = DateTime.ParseExact(_NickelChrome.NickelChromeCardOrderDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    if (dtToday > dtShippingDate)
                    {
                        panelNickelChromeReorderPossible.Visible = true;
                        lblNickelChromeClientAddress.Text = txtAddress1.Text;
                        lblNickelChromeClientAddress.Text += (!string.IsNullOrWhiteSpace(txtAddress2.Text)) ? "<br/>" + txtAddress2.Text : "";
                        lblNickelChromeClientAddress.Text += (!string.IsNullOrWhiteSpace(txtAddress3.Text)) ? "<br/>" + txtAddress3.Text : "";
                        lblNickelChromeClientAddress.Text += (!string.IsNullOrWhiteSpace(txtAddress4.Text)) ? "<br/>" + txtAddress4.Text : "";
                        lblNickelChromeClientAddress.Text += "<br/>" + txtZipcode.Text;
                        lblNickelChromeClientAddress.Text += " " + txtCity.Text;
                        lblNickelChromeClientPhone.Text = txtPhoneNumber.Text;
                    }

                    btnNickelChromeCancel.Visible = false;
                    lblNickelChromeRefundStatus.Visible = false;
                    bool isRefundInProgress = false;
                    if (_NickelChrome.NickelChromeRefund_RefCREOrder > 0)
                    {
                        CRETools.CREOrderStatus creOrderStatus;
                        CRETools.GetCREOrderStatus(authentication.GetCurrent().sToken, _NickelChrome.NickelChromeRefund_RefCREOrder, out creOrderStatus);
                        switch (creOrderStatus)
                        {
                            case CRETools.CREOrderStatus.InProgress:
                                isRefundInProgress = true;
                                lblNickelChromeRefundStatus.Visible = true;
                                lblNickelChromeRefundStatus.Text = "Remboursement en cours de traitement";
                                break;
                            case CRETools.CREOrderStatus.Done:
                                lblNickelChromeRefundStatus.Visible = true;
                                lblNickelChromeRefundStatus.Text = "Remboursement effectué";
                                break;
                            case CRETools.CREOrderStatus.Failed:
                                lblNickelChromeRefundStatus.Visible = true;
                                lblNickelChromeRefundStatus.Text = "<span style=\"color:red;\">Remboursement échoué</span>";
                                break;
                            case CRETools.CREOrderStatus.Refused:
                                lblNickelChromeRefundStatus.Visible = true;
                                lblNickelChromeRefundStatus.Text = "<span style=\"color:red;\">Remboursement refusé</span>";
                                break;
                        }
                    }

                    //TEST
                    //dtOrderDate = DateTime.Now;
                    //if (dtToday < dtOrderDate.AddDays(15) && !isRefundInProgress && isCancelChromeCardPossible())
                    //if (!isRefundInProgress && isCancelChromeCardPossible())
                    //{
                        btnNickelChromeCancel.Visible = true;
                    //}
                    //if (!isRefundInProgress) { btnNickelChromeCancel.Visible = true; }
                }
            }
            else
            {
                panelNickelChrome.Visible = false;
            }
        }
        catch(Exception ex)
        {

        }

        upNickelChromeInformations.Update();
        upNickelChromeReorder.Update();
    }

    protected bool isCancelChromeCardPossible()
    {
        bool isOK = false;

        try
        {
            DataTable dt = Card.GetPersonnalizedCardList(_iRefCustomer);
            if (dt.Rows.Count > 0) 
            {
                isOK = Card.isCancelPersonnalizedCardPossible(dt.Rows[0]["Paid"].ToString(), dt.Rows[0]["Actived"].ToString(), dt.Rows[0]["IsCancelled"].ToString());
                _NickelChrome.NickelChromeRefOrder = dt.Rows[0]["Reference"].ToString();
            }
        }
        catch(Exception ex) { isOK = false; }

        return isOK;
    }

    protected void BindKYCValues(string sXMLCustomerInfos)
    {
        List<string> listCodePRO = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "codPro");
        List<string> listTagHAB = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "modHab");
        List<string> listTagMAT = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "sitFam");
        List<string> listNationalityISO2 = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "NationalityISO2");
        List<string> listProfession = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "Profession");
        List<string> listOldProfessionTAG = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "OldProfessionTAG");
        List<string> listProfessionPlusTAG = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "ProfessionPlusTAG");
        List<string> listAccountUsageTAGList = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "AccountUsageTAGList");
        List<string> listIncomeTAG = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "IncomeTAG");
        List<string> listEstate = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "Estate");
        List<string> listCNUsage = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "CNUsage");
        List<string> listCNUsagePro = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "CNUsagePro");
        List<string> listAt = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "At");
        List<string> listTaxResidence = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "TaxResidenceISO2");

        BindFatcaAeoi(sXMLCustomerInfos);

        if (listTagMAT.Count > 0)
        {
            ListItem item = ddlKYCmat.Items.FindByValue(listTagMAT[0]);
            if (item != null)
                ddlKYCmat.SelectedIndex = ddlKYCmat.Items.IndexOf(item);
        }
        if (listNationalityISO2.Count > 0)
        {
            ListItem item = ddlKYCnationality.Items.FindByValue(listNationalityISO2[0]);
            if (item != null)
                ddlKYCnationality.SelectedIndex = ddlKYCnationality.Items.IndexOf(item);
        }
        if (listIncomeTAG.Count > 0)
        {
            ListItem item = ddlKYCincome.Items.FindByValue(listIncomeTAG[0]);
            if (item != null)
                ddlKYCincome.SelectedIndex = ddlKYCincome.Items.IndexOf(item);
        }
        if (listAccountUsageTAGList.Count > 0 && listAccountUsageTAGList[0].Trim().Length > 0)
        {
            setAccountUsageAnswerList(listAccountUsageTAGList[0]);
        }
        if (listCNUsage.Count > 0 && listCNUsage[0].Trim().Length > 0)
        {
            ListItem item = ddlKYCCNusage.Items.FindByValue(listCNUsage[0]);
            if (item != null)
                ddlKYCCNusage.SelectedIndex = ddlKYCCNusage.Items.IndexOf(item);
        }

        if(listTaxResidence.Count > 0 && !string.IsNullOrWhiteSpace(listTaxResidence[0]))
        {
            ListItem item = ddlKYCTaxResidence.Items.FindByValue(listTaxResidence[0]);
            if(item != null) { ddlKYCTaxResidence.SelectedIndex = ddlKYCTaxResidence.Items.IndexOf(item); }
        }

        if (listCodePRO.Count > 0)
        {
            ListItem item = ddlKYCpro.Items.FindByValue(listCodePRO[0]);
            if (item != null)
                ddlKYCpro.SelectedIndex = ddlKYCpro.Items.IndexOf(item);
        }
        if (listTagHAB.Count > 0)
        {
            ListItem item = ddlKYChab.Items.FindByValue(listTagHAB[0]);
            if (item != null)
                ddlKYChab.SelectedIndex = ddlKYChab.Items.IndexOf(item);
        }
        checkKycHabValue();

        if (panelKYCPropertyStatusPlus.Visible && listAt.Count > 0)
        {
            txtKYCPropertyStatusPlus.Text = listAt[0];
        }

        if (listEstate.Count > 0)
        {
            ListItem item = ddlKYCestate.Items.FindByValue(listEstate[0]);
            if (item != null)
                ddlKYCestate.SelectedIndex = ddlKYCestate.Items.IndexOf(item);
        }
        if (listProfession.Count > 0 && listProfession[0].Trim().Length > 0)
        {
            txtKYCjob.Text = listProfession[0];
        }

        checkKycProValue();
        if (KYC.getKycProA().Contains(ddlKYCpro.SelectedValue) && listProfessionPlusTAG.Count > 0 && listProfessionPlusTAG[0].Trim().Length > 0)
        {
            ListItem item = ddlKYCproPlus.Items.FindByValue(listProfessionPlusTAG[0]);
            if (item != null)
                ddlKYCproPlus.SelectedIndex = ddlKYCproPlus.Items.IndexOf(item);
        }
        else if (KYC.getKycProB().Contains(ddlKYCpro.SelectedValue) && listOldProfessionTAG.Count > 0 && listOldProfessionTAG[0].Trim().Length > 0)
        {
            ListItem item = ddlKYCproPlus.Items.FindByValue(listOldProfessionTAG[0]);
            if (item != null)
                ddlKYCproPlus.SelectedIndex = ddlKYCproPlus.Items.IndexOf(item);
        }
        else if (KYC.getKycProC().Contains(ddlKYCpro.SelectedValue) && listCNUsagePro.Count > 0 && listCNUsagePro[0].Trim().Length > 0)
        {
            ListItem item = ddlKYCproPlus.Items.FindByValue(listCNUsagePro[0]);
            if (item != null)
                ddlKYCproPlus.SelectedIndex = ddlKYCproPlus.Items.IndexOf(item);
        }

        List<string> listAdvancedKYCBlockingDate = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "AdvancedKYCBlockingDate");
        List<string> listPushKYCCompletionDate = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "PushKYCCompletionDate");
        List<string> listLastKYCCompletedDate = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "LastKYCCompletedDate");
        List<string> listBOUserPushKYC = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "BOUserPushKYC");
        DateTime dtAdvancedKYCBlockingDate = new DateTime();
        DateTime dtPushKYCCompletionDate = new DateTime();
        DateTime dtLastKYCCompletedDate = new DateTime();
        string sBOUserPushKYC = "";

        if (listAdvancedKYCBlockingDate.Count > 0 && listAdvancedKYCBlockingDate[0].Trim().Length > 0)
        {
            try { dtAdvancedKYCBlockingDate = DateTime.ParseExact(listAdvancedKYCBlockingDate[0].Substring(0, 19), "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture); } catch (Exception ex) { }
        }
        if (listPushKYCCompletionDate.Count > 0 && listPushKYCCompletionDate[0].Trim().Length > 0)
        {
            try { dtPushKYCCompletionDate = DateTime.ParseExact(listPushKYCCompletionDate[0].Substring(0, 19), "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture); } catch (Exception ex) { }
        }
        if (listLastKYCCompletedDate.Count > 0 && listLastKYCCompletedDate[0].Trim().Length > 0)
        {
            try { dtLastKYCCompletedDate = DateTime.ParseExact(listLastKYCCompletedDate[0].Substring(0, 19), "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture); } catch (Exception ex) { }
        }
        if (listBOUserPushKYC.Count > 0 && listBOUserPushKYC[0].Trim().Length > 0)
        {
            try { sBOUserPushKYC = listBOUserPushKYC[0]; } catch (Exception ex) { }
        }

        GetKYCUpdateStatus(dtAdvancedKYCBlockingDate, dtPushKYCCompletionDate, dtLastKYCCompletedDate, sBOUserPushKYC);

        /*** LECTURE SEULE ***/
        //txtKYCjob.Enabled = false;
        //txtKYCjob.ReadOnly = true;
        //txtKYCPropertyStatusPlus.Enabled = false;
        //txtKYCPropertyStatusPlus.ReadOnly = true;
        //ddlKYCCNusage.Enabled = false;
        //ddlKYCestate.Enabled = false;
        //ddlKYChab.Enabled = false;
        //ddlKYCincome.Enabled = false;
        //ddlKYCmat.Enabled = false;
        //ddlKYCpro.Enabled = false;
        //ddlKYCproPlus.Enabled = false;
        //cblKYCaccountUsageTag.Enabled = false;
        //ddlKYCnationality.Enabled = false;
    }
    protected void BindKYCDropdownlistValues(string sXMLKYCChoices)
    {
        DataTable dtPRO = new DataTable();
        dtPRO.Columns.Add("Label");
        dtPRO.Columns.Add("Value");
        DataTable dtMAT = dtPRO.Clone();
        DataTable dtHAB = dtPRO.Clone();

        List<string> listList = CommonMethod.GetTags(sXMLKYCChoices, "ALL_XML_OUT/List");

        for (int i = 0; i < listList.Count; i++)
        {
            List<string> listTAG = CommonMethod.GetAttributeValues(listList[i], "List", "TAGQuestion");
            List<string> listLabel = CommonMethod.GetAttributeValues(listList[i], "List", "Choice");
            List<string> listValue = CommonMethod.GetAttributeValues(listList[i], "List", "ChoiceTag");

            if (listTAG.Count > 0 && listLabel.Count > 0 && listValue.Count > 0)
            {
                DataRow row;

                switch (listTAG[0])
                {
                    case "PROF":
                        row = dtPRO.NewRow();
                        row["Label"] = listLabel[0];
                        row["Value"] = listValue[0];
                        dtPRO.Rows.Add(row);
                        break;
                    case "SIT_MARIT":
                        row = dtMAT.NewRow();
                        row["Label"] = listLabel[0];
                        row["Value"] = listValue[0];
                        dtMAT.Rows.Add(row);
                        break;
                    case "SIT_PATRIM":
                        row = dtHAB.NewRow();
                        row["Label"] = listLabel[0];
                        row["Value"] = listValue[0];
                        dtHAB.Rows.Add(row);
                        break;
                }
            }
        }

        DataTable dtKycLists = KYC.getKycLists(true);

        ddlKYChab.DataSource = dtHAB;
        ddlKYChab.DataBind();
        ddlKYCmat.DataSource = dtMAT;
        ddlKYCmat.DataBind();
        ddlKYCpro.DataSource = dtPRO;
        ddlKYCpro.DataBind();

        ddlKYCestate.AppendDataBoundItems = true;
        ddlKYCestate.DataValueField = "Value";
        ddlKYCestate.DataTextField = "Label";
        ddlKYCestate.DataSource = KYC.getKycList("PATRIMOINE", dtKycLists);
        ddlKYCestate.DataBind();

        ddlKYCnationality.AppendDataBoundItems = true;
        ddlKYCnationality.DataValueField = "ISO2";
        ddlKYCnationality.DataTextField = "CountryName";
        ddlKYCnationality.DataSource = tools.GetCountryList();
        ddlKYCnationality.DataBind();

        ddlKYCincome.AppendDataBoundItems = true;
        ddlKYCincome.DataValueField = "Value";
        ddlKYCincome.DataTextField = "Label";
        ddlKYCincome.DataSource = KYC.getKycList("REVENUS", dtKycLists);
        ddlKYCincome.DataBind();

        //ddlKYCaccountUsageTag.AppendDataBoundItems = true;
        cblKYCaccountUsageTag.DataTextField = "Label";
        cblKYCaccountUsageTag.DataValueField = "Value";
        cblKYCaccountUsageTag.DataSource = KYC.getKycList("USAGE", dtKycLists);
        cblKYCaccountUsageTag.DataBind();

        ddlKYCCNusage.AppendDataBoundItems = true;
        ddlKYCCNusage.DataTextField = "Label";
        ddlKYCCNusage.DataValueField = "Value";
        ddlKYCCNusage.DataSource = KYC.getKycList("MONCN", dtKycLists);
        ddlKYCCNusage.DataBind();

        ddlKYCTaxResidence.AppendDataBoundItems = true;
        ddlKYCTaxResidence.DataValueField = "ISO2";
        ddlKYCTaxResidence.DataTextField = "CountryName";
        ddlKYCTaxResidence.DataSource = KYC.getResidenceTaxValues();
        ddlKYCTaxResidence.DataBind();
    }
    protected void ddlKYCpro_SelectedIndexChanged(object sender, EventArgs e)
    {
        checkKycProValue();
    }
    protected void ddlKYChab_SelectedIndexChanged(object sender, EventArgs e)
    {
        checkKycHabValue();
    }
    protected void checkKycProValue()
    {
        panelKYCproPlus.Visible = false;
        ddlKYCproPlus.Items.Clear();
        panelKYCjob.Visible = false;
        if (KYC.getKycProA().Contains(ddlKYCpro.SelectedValue) || KYC.getKycProB().Contains(ddlKYCpro.SelectedValue) || KYC.getKycProC().Contains(ddlKYCpro.SelectedValue))
        {
            panelKYCproPlus.Visible = true;
            ddlKYCproPlus.Items.Clear();
            ddlKYCproPlus.AppendDataBoundItems = true;
            ddlKYCproPlus.DataValueField = "Value";
            ddlKYCproPlus.DataTextField = "Label";
            if (KYC.getKycProA().Contains(ddlKYCpro.SelectedValue))
            {
                lblKYCproPlus.Text = "Secteur professionnel";
                ddlKYCproPlus.Items.Add(new ListItem("", ""));
                ddlKYCproPlus.DataSource = KYC.getKycForProA();
                panelKYCjob.Visible = true;
                //txtKYCjob.Text = "";
            }
            else if (KYC.getKycProB().Contains(ddlKYCpro.SelectedValue))
            {
                lblKYCproPlus.Text = "Ancien secteur professionnel";
                ddlKYCproPlus.Items.Add(new ListItem("", ""));
                ddlKYCproPlus.DataSource = KYC.getKycForProB();
            }
            else if (KYC.getKycProC().Contains(ddlKYCpro.SelectedValue))
            {
                lblKYCproPlus.Text = "J'utilise le compte pour une activité pro.";
                ddlKYCproPlus.Items.Add(new ListItem("", ""));
                ddlKYCproPlus.DataSource = KYC.getKycForProC();
            }
            ddlKYCproPlus.DataBind();
        }
    }
    protected void checkKycHabValue()
    {
        panelKYCPropertyStatusPlus.Visible = false;
        if (KYC.getKycPropertyPlusStatus().Contains(ddlKYChab.SelectedValue))
        {
            panelKYCPropertyStatusPlus.Visible = true;
        }
    }
    protected void btnSavePersonalInfo_Click(object sender, EventArgs e)
    {
        authentication auth = authentication.GetCurrent();
        bool bValid = true;
        string sMessage = "";

        if (bPersonnalInfoEdit)
        {
            if (txtBirthDate.Text.Length == 0)
            {
                bValid = false;
                sMessage += "Date de naissance : champ requis<br/>";
            }
            else if (!tools.IsDateValid(txtBirthDate.Text))
            {
                bValid = false;
                sMessage += "Date de naissance : format non valide<br/>";
            }
            if (txtZipcode.Text.Length == 0)
            {
                bValid = false;
                sMessage += "Code postal : champ requis<br/>";
            }
            else if (!tools.IsValid(txtZipcode.Text, @"^[0-9]{5}$"))
            {
                bValid = false;
                sMessage += "Code postal : format non valide<br/>";
            }
            if (txtPhoneNumber.Text.Length == 0)
            {
                bValid = false;
                sMessage += "N° de mobile : champ requis<br/>";
            }
            else if (!tools.IsPhoneValid(txtPhoneNumber.Text, true))
            {
                bValid = false;
                sMessage += "N° de mobile : format non valide<br/>";
            }
            if (txtMail.Text.Length > 0 && !tools.IsEmailValid(txtMail.Text))
            {
                bValid = false;
                sMessage += "Adresse email : format non valide<br/>";
            }

            if (ddlBirthCountry.SelectedValue.Trim().Length == 0)
            {
                bValid = false;
                sMessage += "Pays de naissance : champ requis<br/>";
            }

            if (txtBirthCity.Text.Trim().Length == 0)
            {
                bValid = false;
                sMessage += "Ville de naissance : champ requis<br/>";
            }
            else if (ddlBirthCountry.SelectedValue == "FR")
            {
                if (!tools.checkBirthDep(txtBirthDepartment.Text))
                {
                    bValid = false;
                    sMessage += "Département de naissance non valide";
                }
                else if (!tools.checkBirthCity(txtBirthDepartment.Text, txtBirthCity.Text))
                {
                    bValid = false;
                    sMessage += "Ville de naissance non valide";
                }
            }
        }

        //if (KYC.getKycProC().Contains(ddlKYCpro.SelectedValue))
        //{
        //    if (ddlKYCproPlus.SelectedValue == "PRO")
        //    {
        //        bValid = false;
        //        sMessage += "Compte-Nickel ne peut pas être utilisé à des fins professionnelles !";
        //    }
        //}

        bool RefreshKYC = false;
        if (bValid || (bKYCEdit && !bPersonnalInfoEdit))
        {
            sMessage = "";
            KYC.AnswerList _KycAnswerList = new KYC.AnswerList();
            _KycAnswerList.MaritalStatus = ddlKYCmat.SelectedValue;
            _KycAnswerList.Nationality = ddlKYCnationality.SelectedValue;
            _KycAnswerList.PropertyValue = ddlKYCestate.SelectedValue;
            _KycAnswerList.PropertyStatus = ddlKYChab.SelectedValue;
            _KycAnswerList.AccountUsage = getAccountUsageAnswerList();
            _KycAnswerList.ProfessionalStatus = ddlKYCpro.SelectedValue;
            _KycAnswerList.CNUsage = ddlKYCCNusage.SelectedValue;
            _KycAnswerList.Income = ddlKYCincome.SelectedValue;
            //_KycAnswerList.TaxResidence = ddlKYCTaxResidence.SelectedValue;

            //FATCA/AEOI SAVE
            string sFatcaAeoiMessage = "";
            _KycAnswerList.SaveFatcaAeoiValues = false;
            if (checkFatcaAeoi(out sFatcaAeoiMessage)) { _KycAnswerList.SaveFatcaAeoiValues = true; }
            _KycAnswerList = SetTaxResidenceValues(_KycAnswerList);

            if (KYC.getKycPropertyPlusStatus().Contains(ddlKYChab.SelectedValue))
            {
                _KycAnswerList.At = txtKYCPropertyStatusPlus.Text;
            }

            if (KYC.getKycProA().Contains(ddlKYCpro.SelectedValue))
            {
                _KycAnswerList.ProfessionPlus = ddlKYCproPlus.SelectedValue;
                _KycAnswerList.Profession = txtKYCjob.Text.Trim();
            }
            else if (KYC.getKycProB().Contains(ddlKYCpro.SelectedValue))
                _KycAnswerList.OldProfession = ddlKYCproPlus.SelectedValue;
            else if (KYC.getKycProC().Contains(ddlKYCpro.SelectedValue))
                _KycAnswerList.CNUsagePro = ddlKYCproPlus.SelectedValue;

            _KycAnswerList.Certificate = cbKYCCertificate.Checked;

            _KycAnswerList.SaveKYC = false;
            if (_bEditKYC) { _KycAnswerList.SaveKYC = true; }

            if ((_KycAnswerList.SaveKYC || _KycAnswerList.SaveFatcaAeoiValues) && !KYC.saveKYC(_iRefCustomer, auth.sToken, _KycAnswerList, out sMessage))
            {
                sMessage = "<div style=\"margin:10px auto; width:90%; height:1px; background-color:#f57527;\"></div>" + sMessage + GetFormattingFatcaAeoiMessage(sFatcaAeoiMessage);
                UpdateMessage(sMessage);
            }
            else if (!bPersonnalInfoEdit)
            {
                RefreshKYC = true;
                UpdateMessage("Informations enregistrées." + GetFormattingFatcaAeoiMessage(sFatcaAeoiMessage));
            }

            if (bPersonnalInfoEdit)
            {
                string sToken = authentication.GetCurrent(false).sToken;
                XElement xmlIn = new XElement("Customer",
                                    new XAttribute("CashierToken", sToken),
                                    new XAttribute("RefCustomer", _iRefCustomer),
                                    new XAttribute("PolitenessTAG", rblCivility.SelectedValue),
                                    new XAttribute("LastName", txtLastName.Text),
                                    new XAttribute("NewLastName", txtNewLastName.Text),
                                    new XAttribute("MarriedLastName", txtMarriedLastName.Text),
                                    new XAttribute("FirstName", txtFirstName.Text),
                                    new XAttribute("BirthDate", txtBirthDate.Text),
                                    new XAttribute("BirthPlace", txtBirthCity.Text),
                                    new XAttribute("BirthCountryISO2", ddlBirthCountry.SelectedValue),
                                    new XAttribute("codPro", ddlKYCpro.SelectedValue),
                                    new XAttribute("modHab", ddlKYChab.SelectedValue),
                                    new XAttribute("sitFam", ddlKYCmat.SelectedValue),
                                    new XAttribute("Address1", txtAddress1.Text),
                                    new XAttribute("Address2", txtAddress2.Text),
                                    new XAttribute("Address3", txtAddress3.Text),
                                    new XAttribute("Address4", txtAddress4.Text),
                                    new XAttribute("Zipcode", txtZipcode.Text),
                                    new XAttribute("City", txtCity.Text),
                                    new XAttribute("PhoneNumber", txtPhoneNumber.Text),
                                    new XAttribute("Email", txtMail.Text));

                if (!String.IsNullOrWhiteSpace(hdnStreetNumber.Value))
                    xmlIn.Add(new XAttribute("streetNumber", hdnStreetNumber.Value));
                if (!String.IsNullOrWhiteSpace(hdnStreetName.Value))
                    xmlIn.Add(new XAttribute("street", hdnStreetName.Value));
                if (!String.IsNullOrWhiteSpace(hdnComplCity.Value))
                    xmlIn.Add(new XAttribute("cityComplement", hdnComplCity.Value));
                if (!String.IsNullOrWhiteSpace(hdnAddressQualityCode.Value))
                    xmlIn.Add(new XAttribute("qualityCodeAddress", hdnAddressQualityCode.Value));

                if (!String.IsNullOrWhiteSpace(hdnMailQualityCode.Value))
                    xmlIn.Add(new XAttribute("qualityCodeEmail", hdnMailQualityCode.Value));

                if (rblCivility.SelectedValue == "MME")
                    xmlIn.Add(new XAttribute("MaidenName", txtMaidenName.Text.Trim()));
                else
                    xmlIn.Add(new XAttribute("MaidenName", ""));

                if (ddlBirthCountry.SelectedValue == "FR")
                    xmlIn.Add(new XAttribute("BirthDepartment", txtBirthDepartment.Text.Trim()));
                else
                    xmlIn.Add(new XAttribute("BirthDepartment", ""));

                string sXmlIN = new XElement("ALL_XML_IN", xmlIn).ToString();
                if (SaveInformations(sXmlIN))
                {
                    sMessage = (string.IsNullOrWhiteSpace(sMessage) && !string.IsNullOrWhiteSpace(sFatcaAeoiMessage)) ? GetFormattingFatcaAeoiMessage(sFatcaAeoiMessage) : sMessage;

                    UpdateMessage("Informations enregistrées." + sMessage);
                    //UpdateMessage("Informations enregistrées.");
                    BindCustomerInformations(Client.GetCustomerInformations(_iRefCustomer), RefreshKYC);
                    ReadOnlyKyc();
                }
                else UpdateMessage("Une erreur est survenue.");
            }
        }
        else UpdateMessage(sMessage);

        ScriptManager.RegisterStartupScript(this, this.GetType(), "UpdatePersonalInfoKEY", "ManageBirthPlace();", true);
    }

    protected string GetFormattingFatcaAeoiMessage(string sMessage)
    {
        string sMessageFormatted = "";
        if (!string.IsNullOrWhiteSpace(sMessage))
        {
            sMessageFormatted = "<div style=\"margin:10px auto; width:90%; height:1px; background-color:#f57527;\"></div>";
            sMessageFormatted += sMessage;
        }

        return sMessageFormatted;
    }

    protected string getAccountUsageAnswerList()
    {
        string sAnswerList = "";
        foreach (ListItem li in cblKYCaccountUsageTag.Items)
        {
            if (li.Selected) { sAnswerList += li.Value + ","; }
        }

        if (sAnswerList.Trim().Length > 0)
            sAnswerList = sAnswerList.TrimEnd(new char[] { ',' });
        //sAnswerList.Substring(0, sAnswerList.Length - 2);

        return sAnswerList;
    }
    protected void setAccountUsageAnswerList(string sAccountUsageValues)
    {
        string[] tAccountUsageValues = sAccountUsageValues.Split(',');
        for (int i = 0; i < tAccountUsageValues.Length; i++)
        {
            ListItem item = cblKYCaccountUsageTag.Items.FindByValue(tAccountUsageValues[i]);
            if (item != null)
                cblKYCaccountUsageTag.Items.FindByValue(tAccountUsageValues[i]).Selected = true;
        }
    }
    protected void btnSaveDocsInfo_Click(object sender, EventArgs e)
    {
        string sToken = authentication.GetCurrent(false).sToken;

        string sXmlIN = new XElement("ALL_XML_IN",
                            new XElement("Customer",
                                new XAttribute("CashierToken", sToken),
                                new XAttribute("RefCustomer", _iRefCustomer),
                                new XElement("DOCS",
                                    new XElement("DOC",
                                        new XAttribute("RefDocument", hdnRefDocID.Value),
                                        new XAttribute("DocumentNumber", txtNumberID.Text),
                                        new XAttribute("DeliveryDate", txtDeliveryDateID.Text),
                                        new XAttribute("ExpireDate", txtExpireDateID.Text)),
                                    new XElement("DOC",
                                        new XAttribute("RefDocument", hdnRefDocHO.Value),
                                        new XAttribute("DeliveryDate", txtDeliveryDateHO.Text),
                                        new XAttribute("ExpireDate", txtExpireDateHO.Text))))).ToString();


        if (SaveInformations(sXmlIN))
            UpdateMessage("Informations enregistrées.");
    }
    protected bool SaveInformations(string sXmlIN)
    {
        bool bSaved = false;
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_SetCustomer", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);

            cmd.Parameters["@IN"].Value = sXmlIN;
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOUT = cmd.Parameters["@OUT"].Value.ToString();

            if (sXmlOUT.Length > 0)
            {
                List<string> listRC = CommonMethod.GetAttributeValues(sXmlOUT, "ALL_XML_OUT/Result", "RC");
                List<string> listUpdated = CommonMethod.GetAttributeValues(sXmlOUT, "ALL_XML_OUT/Result", "Updated");

                if (listRC.Count > 0)
                {
                    if (listRC[0] == "0")
                    {
                        if (listUpdated.Count > 0 && listUpdated[0] == "1")
                            bSaved = true;
                    }
                    else if (listRC[0] == "71008")
                    {
                        UpdateMessage("Session terminée. Veuillez vous reconnecter.");
                    }
                    else UpdateMessage("Une erreur est survenue.");
                }
            }
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return bSaved;
    }
    protected string GetChangeLimitRequests(int iRefCustomer, string sCashierToken)
    {
        string sChangeLimitRequests = "";
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_GetCustomerChangeLimitRequests", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN", new XElement("ChangeLimit", new XAttribute("CashierToken", sCashierToken), new XAttribute("RefCustomer", iRefCustomer))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            sChangeLimitRequests = cmd.Parameters["@OUT"].Value.ToString();
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return sChangeLimitRequests;
    }
    protected void BindChangeLimitRequests(string sChangeLimitRequests)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("Amount");
        dt.Columns.Add("Type");
        dt.Columns.Add("Date");
        dt.Columns.Add("Ref");

        if (sChangeLimitRequests.Length > 0)
        {
            List<string> listRequest = CommonMethod.GetTags(sChangeLimitRequests, "ALL_XML_OUT/ChangeLimit");

            for (int i = 0; i < listRequest.Count; i++)
            {
                List<string> listRef = CommonMethod.GetAttributeValues(listRequest[i], "ChangeLimit", "RefRequest");
                List<string> listAmount = CommonMethod.GetAttributeValues(listRequest[i], "ChangeLimit", "LimitAmount");
                List<string> listPaymentLimit = CommonMethod.GetAttributeValues(listRequest[i], "ChangeLimit", "PaymentLimit");
                List<string> listWithdrawLimit = CommonMethod.GetAttributeValues(listRequest[i], "ChangeLimit", "WithDrawMoneyLimit");
                List<string> listDate = CommonMethod.GetAttributeValues(listRequest[i], "ChangeLimit", "RequestDate");
                List<string> listAccepted = CommonMethod.GetAttributeValues(listRequest[i], "ChangeLimit", "Accepted");

                if (listRef.Count > 0 && listAccepted.Count > 0 && listAccepted[0] == "0")
                {
                    DataRow row = dt.NewRow();
                    row["Amount"] = (listAmount.Count > 0) ? listAmount[0].Substring(0, listAmount[0].LastIndexOf('.')) : "";
                    string sType = "";
                    if (listPaymentLimit.Count > 0 && listPaymentLimit[0].Length > 0)
                        sType = "Paiement carte";
                    else if (listWithdrawLimit.Count > 0 && listWithdrawLimit[0].Length > 0)
                        sType = "Retrait";

                    row["Type"] = sType;
                    row["Date"] = (listDate.Count > 0) ? listDate[0] : "";
                    row["Ref"] = listRef[0];

                    dt.Rows.Add(row);
                }
            }
        }

        if (dt.Rows.Count > 0)
        {
            dt = ReverseRowsInDataTable(dt);

            rptLimitRequests.DataSource = dt;
            rptLimitRequests.DataBind();
        }
        else
        {
            panelNoLimitRequest.Visible = true;
            rptLimitRequests.Visible = false;
        }
    }
    protected string CreateAcceptRefuseLimitRequestBtn(string sRefRequest)
    {
        string sButtons = "";

        sButtons += "<input type=\"button\" class=\"MiniButton\" value=\"accepter\" onclick=\"InitLimitRequestAction('" + sRefRequest + "', '1');\"/>";
        sButtons += "<input type=\"button\" class=\"MiniButton\" value=\"refuser\" onclick=\"InitLimitRequestAction('" + sRefRequest + "', '2');\"/>";

        return sButtons;
    }
    protected void btnAcceptRefuseLimitRequest_Click(object sender, EventArgs e)
    {
        if (hdnLimitRequestAction.Value.Length > 0 && hdnRefLimitRequest.Value.Length > 0)
        {
            if (AcceptRefuseLimitRequest(int.Parse(hdnRefLimitRequest.Value.ToString()), int.Parse(hdnLimitRequestAction.Value.ToString())))
            {
                if (hdnLimitRequestAction.Value.ToString() == "1")
                {
                    UpdateMessage("Plafond de paiement modifié.");

                    BindPaymentLimits(_iRefCustomer, _sPaymentKind, _sWithdrawKind);
                    BindClientPaymentLimit(GetClientPaymentLimit(_iRefCustomer, _sPaymentLimitPeriod, _sWithdrawLimitPeriod));
                    BindClientPaymentSituation(GetClientPaymentSituation(_iRefCustomer, _sPaymentLimitPeriod, _sWithdrawLimitPeriod));
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "InitLimitsBarKey", "InitLimitsBar();", true);
                }
                else UpdateMessage("Demande refusée.");

                string sToken = authentication.GetCurrent(false).sToken;
                BindChangeLimitRequests(GetChangeLimitRequests(_iRefCustomer, sToken));
            }
            else UpdateMessage("Une erreur est survenue.");

        }
    }
    protected bool AcceptRefuseLimitRequest(int iRef, int iAction)
    {
        bool bDone = false;
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_ChangeLimitAnswer", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@RefRequest", SqlDbType.Int);
            cmd.Parameters.Add("@AcceptedAnswer", SqlDbType.SmallInt);
            cmd.Parameters.Add("@RC", SqlDbType.Int);

            cmd.Parameters["@RefRequest"].Value = iRef;
            cmd.Parameters["@AcceptedAnswer"].Value = iAction;
            cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;

            cmd.ExecuteNonQuery();

            string sRC = cmd.Parameters["@RC"].Value.ToString();

            if (sRC == "0")
                bDone = true;
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return bDone;
    }

    protected void BindSMSHistory(DataTable dt)
    {
        if (dt.Rows.Count > 0)
        {
            rptSMSHistory.DataSource = dt;
            rptSMSHistory.DataBind();

            panelNoSMSHistory.Visible = false;
            rptSMSHistory.Visible = true;
        }
        else
        {
            panelNoSMSHistory.Visible = true;
            rptSMSHistory.Visible = false;
        }
    }
    protected DataTable GetSMSHistory(int iRefCustomer)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("Content");
        dt.Columns.Add("Type");
        dt.Columns.Add("Date");
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_SearchSMS", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@ReturnSelect", SqlDbType.Bit);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                                new XElement("SMS",
                                                    new XAttribute("RefCustomer", iRefCustomer),
                                                    new XAttribute("PageIndex", "1"),
                                                    new XAttribute("PageSize", "50"))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
            cmd.Parameters["@ReturnSelect"].Value = 1;

            DataTable dtTmp = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dtTmp);

            for (int i = 0; i < dtTmp.Rows.Count; i++)
            {
                DataRow row = dt.NewRow();

                row["Content"] = dtTmp.Rows[i]["MessageText"].ToString();
                row["Date"] = dtTmp.Rows[i]["ActionDate"].ToString();

                switch (dtTmp.Rows[i]["SMSAction"].ToString())
                {
                    case "SENT":
                        row["Type"] = "Envoyé";
                        break;
                    case "RECEIVED":
                        row["Type"] = "Reçu";
                        break;
                    default:
                        row["Type"] = dtTmp.Rows[i]["SMSAction"];
                        break;
                }

                dt.Rows.Add(row);
            }
        }
        catch (Exception ex)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }
    protected void btnRefreshSMSHistory_Click(object sender, EventArgs e)
    {
        BindSMSHistory(GetSMSHistory(_iRefCustomer));
        upSMSHistory.Update();
    }

    protected void btnSendCourrier_Click(object sender, EventArgs e)
    {
        SqlConnection conn = null;
        bool bSent = false;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("BankSI.P_SendLetterCheckAddress", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@RefCustomer", SqlDbType.Int);
            cmd.Parameters.Add("@REASON", SqlDbType.VarChar, 20);
            cmd.Parameters.Add("@RC", SqlDbType.Int);

            cmd.Parameters["@RefCustomer"].Value = _iRefCustomer;
            cmd.Parameters["@REASON"].Value = "LETTERNOTRECEIVED";
            cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;

            cmd.ExecuteNonQuery();

            if (cmd.Parameters["@RC"].Value.ToString() == "0")
                bSent = true;
        }
        catch (Exception ex)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        if (bSent)
            UpdateMessage("Le courrier a été renvoyé au client.");
        else UpdateMessage("Une erreur est survenue.");
    }
    protected void btnNotifyCourrier_Click(object sender, EventArgs e)
    {
        SqlConnection conn = null;
        string sNotifyMethod = rblNotifyMethod.SelectedValue;
        bool bSent = false;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("Web.P_CheckAddressReminder", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@RefCustomer", SqlDbType.Int);
            cmd.Parameters.Add("@ReminderKind", SqlDbType.VarChar, 10);
            cmd.Parameters.Add("@RC", SqlDbType.Int);

            cmd.Parameters["@RefCustomer"].Value = _iRefCustomer;
            cmd.Parameters["@ReminderKind"].Value = sNotifyMethod;
            cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;

            cmd.ExecuteNonQuery();

            if (cmd.Parameters["@RC"].Value.ToString() == "0")
                bSent = true;
        }
        catch (Exception ex)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        if (bSent)
            UpdateMessage("Rappel envoyé par " + rblNotifyMethod.SelectedItem.Text + ".");
        else UpdateMessage("Une erreur est survenue.");
    }

    protected void UpdateMessage(string sMessage)
    {
        hdnMessage.Value = sMessage;
        upMessage.Update();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "UpdateMessageKEY", "CheckMessages(); hideLoading();", true);
    }

    protected void btnPrev_Click(object sender, EventArgs e)
    {
        Response.Redirect("ClientSearch.aspx");
    }

    protected DataTable ReverseRowsInDataTable(DataTable inputTable)
    {
        DataTable outputTable = inputTable.Clone();

        for (int i = inputTable.Rows.Count - 1; i >= 0; i--)
        {
            outputTable.ImportRow(inputTable.Rows[i]);
        }

        return outputTable;
    }

    // CARDS
    #region cards
    protected void BindPINBySMSHistory(DataTable dt)
    {
        if (dt.Rows.Count > 0)
        {
            rptPINBySMSHistory.DataSource = dt;
            rptPINBySMSHistory.DataBind();
        }
        else
        {
            panelNoPINBySMSSent.Visible = true;
            rptPINBySMSHistory.Visible = false;
        }
    }
    protected DataTable GetPINBySMSHistory(int iRefCustomer)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("Label");
        dt.Columns.Add("Desc");
        dt.Columns.Add("Date");
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_PinBySMS_Logs", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@RefCustomer", SqlDbType.Int);

            cmd.Parameters["@RefCustomer"].Value = iRefCustomer;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;

            DataTable dtTmp = new DataTable();
            da.Fill(dtTmp);

            for (int i = 0; i < dtTmp.Rows.Count; i++)
            {
                DataRow row = dt.NewRow();

                switch (dtTmp.Rows[i]["eventID"].ToString())
                {
                    case "308":
                        row["Label"] = "Demande enregistrée";
                        break;
                    case "110":
                        row["Label"] = "SMS envoyé";
                        break;
                    case "220":
                        row["Label"] = "SMS reçu";
                        break;
                    default:
                        row["Label"] = dtTmp.Rows[i]["eventLabel"].ToString();
                        break;
                }
                row["Desc"] = dtTmp.Rows[i]["eventDescription"];
                row["Date"] = dtTmp.Rows[i]["NotificationDate"];

                dt.Rows.Add(row);
            }
        }
        catch (Exception ex)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }
    protected void btnRefreshPINBySMSHistory_Click(object sender, EventArgs e)
    {
        BindPINBySMSHistory(GetPINBySMSHistory(_iRefCustomer));
        upPINBySMSHistory.Update();
    }
    protected void btnSendPIN_Click(object sender, EventArgs e)
    {
        string sToken = authentication.GetCurrent(false).sToken;

        if (SendPINBySMS(_iRefCustomer, ddlSendPINReason.SelectedValue, sToken))
        {
            UpdateMessage("Code PIN renvoyé.");
            BindPINBySMSHistory(GetPINBySMSHistory(_iRefCustomer));
        }
    }
    protected bool SendPINBySMS(int iRefCustomer, string sRefReason, string sCashierToken)
    {
        SqlConnection conn = null;
        bool bSent = false;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("BankSI.P_BackOffice_ResentPinBySMS", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                            new XElement("Pincode",
                                                new XAttribute("CashierToken", sCashierToken),
                                                new XAttribute("RefCustomer", iRefCustomer),
                                                new XAttribute("RefReason", sRefReason))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if (sXmlOut.Length > 0)
            {
                List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Pincode", "RC");
                if (listRC.Count > 0)
                {
                    if (listRC[0] == "0")
                        bSent = true;
                    else if (listRC[0] == "71008")
                    {
                        UpdateMessage("Session terminée. Veuillez vous reconnecter.");
                    }
                    else UpdateMessage("Une erreur est survenue.");
                }
            }
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return bSent;
    }

    protected void btnLoadHisto_Click(object sender, EventArgs e)
    {
        BindCardHistoryList(GetCardHistoryList(authentication.GetCurrent().sToken, _iRefCustomer.ToString()));
        BindCardOppositionList();

    }
    protected void BindCardHistoryList(DataTable dt)
    {
        rptCardsHisto.DataSource = dt;
        rptCardsHisto.DataBind();
        upCardsHisto.Update();
    }
    protected DataTable GetCardHistoryList(string sToken, string sRefCustomer)
    {
        SqlConnection conn = null;
        DataTable dt = new DataTable();
        dt.Columns.Add("CardNumber");
        dt.Columns.Add("Status");
        dt.Columns.Add("ExpirationDate");
        dt.Columns.Add("DateRemise");
        dt.Columns.Add("PaymentLimit");
        dt.Columns.Add("PaymentPeriod");
        dt.Columns.Add("WithdrawMoneyLimit");
        dt.Columns.Add("WithdrawMoneyPeriod");
        dt.Columns.Add("ShowCardLock");
        dt.Columns.Add("ShowCardReplacement");
        dt.Columns.Add("Barcode");
        dt.Columns.Add("CardModelLink");

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("Web.P_GetCustomerCardsInfo", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@ReturnSelect", SqlDbType.Bit);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                                new XElement("Cards",
                                                    new XAttribute("CashierToken", sToken),
                                                    new XAttribute("RefCustomer", sRefCustomer),
                                                    new XAttribute("Culture", "fr-FR"))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
            cmd.Parameters["@ReturnSelect"].Value = 0;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if (sXmlOut.Length > 0)
            {
                List<string> listCard = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/Cards/Card");
                int iNbCardActions = 0;

                int iNbActivedCard = 0;
                for (int i = 0; i < listCard.Count; i++)
                {
                    List<string> listCardStatus = CommonMethod.GetAttributeValues(listCard[i], "Card", "CardStatus");
                    if (listCardStatus.Count > 0 && iNbCardActions == 0 && (listCardStatus[0] == "ACTIVE" || listCardStatus[0] == "MODIFIEE"))
                    {
                        iNbActivedCard++;
                    }
                }

                _dtBarcodeToPAN = new DataTable();
                _dtBarcodeToPAN.Columns.Add("Barcode");
                _dtBarcodeToPAN.Columns.Add("PAN");

                for (int i = 0; i < listCard.Count; i++)
                {
                    List<string> listCardNumber = CommonMethod.GetAttributeValues(listCard[i], "Card", "CardNumber");
                    List<string> listCardStatus = CommonMethod.GetAttributeValues(listCard[i], "Card", "CardStatus");
                    List<string> listCardExpireDate = CommonMethod.GetAttributeValues(listCard[i], "Card", "CardExpireDate");
                    List<string> listCardDateRemise = CommonMethod.GetAttributeValues(listCard[i], "Card", "DateRemise");
                    List<string> listPaymentLimit = CommonMethod.GetAttributeValues(listCard[i], "Card", "PaymentLimit");
                    List<string> listPaymentPeriod = CommonMethod.GetAttributeValues(listCard[i], "Card", "PaymentPeriod");
                    List<string> listWithdrawMoneyLimit = CommonMethod.GetAttributeValues(listCard[i], "Card", "WithdrawMoneyLimit");
                    List<string> listWithdrawMoneyPeriod = CommonMethod.GetAttributeValues(listCard[i], "Card", "WithdrawMoneyPeriod");
                    List<string> listCardModel = CommonMethod.GetAttributeValues(listCard[i], "Card", "typVis");
                    List<string> listCardModelLink = CommonMethod.GetAttributeValues(listCard[i], "Card", "ModelLink");
                    List<string> listBarcode = CommonMethod.GetAttributeValues(listCard[i], "Card", "Barcode");
                    List<string> listPANCardClear = CommonMethod.GetAttributeValues(listCard[i], "Card", "PANCardClear");

                    bool bShowCardLock = (_bHideCardActionButtons) ? false : ((listCardStatus.Count > 0 && (listCardStatus[0] == "ACTIVE" || listCardStatus[0] == "MODIFIEE")) ? true : false);
                    //bool bShowCardLock = true; //TEST
                    bool bShowCardReplacement = (_bHideCardActionButtons || iNbActivedCard > 0) ? false : ((listCardStatus.Count > 0 && iNbCardActions == 0) ? ((listCardStatus[0] == "OPPOSEE") ? true : false) : false);
                    // TEST
                    //bool bShowActions = (listCardStatus.Count > 0 && iNbCardActions == 0) ? true : false;

                    //if (bShowCardLock || bShowCardReplacement)
                    if (bShowCardReplacement)
                        iNbCardActions++;
                    // utilisé pour éviter bug avec multiple cartes actives, à modifier plus tard

                    dt.Rows.Add(new object[] { ((listCardNumber.Count > 0) ? listCardNumber[0] : ""), ((listCardStatus.Count > 0) ? listCardStatus[0] : ""),
                        ((listCardExpireDate.Count > 0) ? listCardExpireDate[0] : ""), ((listCardDateRemise.Count > 0) ? listCardDateRemise[0] : ""), ((listPaymentLimit.Count > 0) ? listPaymentLimit[0] : ""),
                        ((listPaymentPeriod.Count > 0) ? listPaymentPeriod[0] : ""), ((listWithdrawMoneyLimit.Count > 0) ? listWithdrawMoneyLimit[0] : ""),
                        ((listWithdrawMoneyPeriod.Count > 0) ? listWithdrawMoneyPeriod[0] : ""), bShowCardLock, bShowCardReplacement,
                        ((listBarcode.Count > 0) ? listBarcode[0] : ""), ((listCardModelLink.Count > 0) ? listCardModelLink[0] : "") });

                    _dtBarcodeToPAN.Rows.Add(new object[] { ((listBarcode.Count > 0) ? listBarcode[0] : ""), ((listPANCardClear.Count > 0) ? listPANCardClear[0] : "") });
                }
            }
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }

    protected void BindCardOppositionList()
    {
        panelOppositionHisto.Visible = false;
        string sXmlOut = "";

        if (GetOppositionDetails(out sXmlOut))
        {
            DataTable dtOppositionHisto = getCardOppositionListDatatableFromXml(sXmlOut);
            if (dtOppositionHisto.Rows.Count > 0)
            {
                panelOppositionHisto.Visible = true;
                rptOppositionHisto.DataSource = dtOppositionHisto;
                rptOppositionHisto.DataBind();
            }
        }
    }
    protected bool GetOppositionDetails(out string sXmlOut)
    {
        sXmlOut = "";
        bool isOK = false;

        SqlConnection conn = null;
        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("Web.P_GetCustomerOppositionInformation", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@RC", SqlDbType.Int);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN", new XElement("Opposition", new XAttribute("BankAccountNumber", lblClientAccountNumber.Text))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
            cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;

            cmd.ExecuteNonQuery();

            sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if (cmd.Parameters["@RC"].Value.ToString() == "0")
                isOK = true;
        }
        catch (Exception ex)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return isOK;
    }
    protected DataTable getCardOppositionListDatatableFromXml(string sXml)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("Date");
        dt.Columns.Add("Origine");
        dt.Columns.Add("BoUser");
        dt.Columns.Add("Channel");
        dt.Columns.Add("Opposed");
        dt.Columns.Add("AnswerXML");

        List<string> lsOpposition = CommonMethod.GetTags(sXml, "ALL_XML_OUT/Opposition");

        string sOppositionDate = "", sOppositionOrigine = "", sBackofficeUser = "", sChannel = "", sOpposed = "", sAnswerXML = "";
        List<string> lsOppositionDate = new List<string>();
        List<string> lsOppositionOrigine = new List<string>();
        List<string> lsBackOfficeUser = new List<string>();
        List<string> lsChannel = new List<string>();
        List<string> lsOpposed = new List<string>();
        List<string> lsAnswerXML = new List<string>();

        for (int i = 0; i < lsOpposition.Count; i++)
        {
            sOppositionDate = ""; sOppositionOrigine = ""; sBackofficeUser = ""; sChannel = ""; sOpposed = ""; sAnswerXML = "";
            lsOppositionDate = CommonMethod.GetAttributeValues(lsOpposition[i], "Opposition", "OppositionDate");
            lsOppositionOrigine = CommonMethod.GetAttributeValues(lsOpposition[i], "Opposition", "OppositionOrigine");
            lsBackOfficeUser = CommonMethod.GetAttributeValues(lsOpposition[i], "Opposition", "BackOfficeUser");
            lsChannel = CommonMethod.GetAttributeValues(lsOpposition[i], "Opposition", "Channel");
            lsOpposed = CommonMethod.GetAttributeValues(lsOpposition[i], "Opposition", "Opposed");
            lsAnswerXML = CommonMethod.GetAttributeValues(lsOpposition[i], "Opposition", "AnswerXML");

            if (lsOppositionDate != null && lsOppositionDate.Count > 0)
                sOppositionDate = lsOppositionDate[0];
            if (lsOppositionOrigine != null && lsOppositionOrigine.Count > 0)
                sOppositionOrigine = lsOppositionOrigine[0];
            if (lsBackOfficeUser != null && lsBackOfficeUser.Count > 0)
                sBackofficeUser = lsBackOfficeUser[0];
            if (lsChannel != null && lsChannel.Count > 0)
                sChannel = lsChannel[0];
            if (lsOpposed != null && lsOpposed.Count > 0)
                sOpposed = lsOpposed[0];
            if (lsAnswerXML != null && lsAnswerXML.Count > 0)
                sAnswerXML = lsAnswerXML[0];

            dt.Rows.Add(new object[] { sOppositionDate, sOppositionOrigine, sBackofficeUser, sChannel, sOpposed, sAnswerXML });
        }

        return dt;
    }

    public string getOrigineAndBoUserLabel(string sOrigine, string sBoUser)
    {
        if (sBoUser.Trim().Length > 0)
            return "<span style=\"font-weight:bold\">" + sOrigine.ToUpper() + "</span> par " + sBoUser;
        else
            return "<span style=\"font-weight:bold\">" + sOrigine.ToUpper() + "</span>";
    }
    

    // Card lock
    protected void btnCardLock_Click(object sender, EventArgs e)
    {
        string sErrorMessage = "";

        if (hfCardLock_CardNumber.Value.Trim().Length > 0 && Card.CardLock(authentication.GetCurrent().sToken, _iRefCustomer.ToString(), hfCardLock_CardNumber.Value, out sErrorMessage))
        //if(true) // TEST
        {
            string sMessage = "La carte a été opposée";
            if (Card.SendReplacementCardCode(_iRefCustomer, authentication.GetCurrent().sToken, false, ref sErrorMessage))
            {
                UpdateMessage(sMessage + ".");
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "ShowChallengeReceivedDialog();", true);
                BindCardHistoryList(GetCardHistoryList(authentication.GetCurrent().sToken, _iRefCustomer.ToString()));
            }
            else
            {
                UpdateMessage(sMessage + " mais l'envoi du code de remplacement a échoué.<br/><br/>" + sErrorMessage);
            }
        }
        else UpdateMessage("La carte n'a pas pu être opposée.<br/>" + sErrorMessage);
        hfCardLock_CardNumber.Value = "";
    }
    protected void btnSendReplaceCardChallengeToEmail_Click(object sender, EventArgs e)
    {
        string sContent = "";

        if (Card.SendReplacementCardCode(_iRefCustomer, authentication.GetCurrent().sToken, true, ref sContent))
        //if(true) //TEST
        {
            UpdateMessage("Le code de remplacement a été envoyé au client par email.");
        }
        else UpdateMessage(sContent);
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "HideChallengeReceivedDialog();", true);
    }
    protected void btnCardReplacement_Click(object sender, EventArgs e)
    {
        string sMessage = "";
        if(Card.SendReplacementCardCode(_iRefCustomer, authentication.GetCurrent().sToken, false, ref sMessage))
        //if (true) // TEST
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "CheckChallengeReceived(false);", true);
        }
        else UpdateMessage(sMessage);
    }

    protected void btnMonextActivate_Click(object sender, EventArgs e)
    {
        Button btn = (Button)sender;

        if (_dtBarcodeToPAN != null && btn != null)
        {
            string sPAN = "";

            for (int i = 0; i < _dtBarcodeToPAN.Rows.Count; i++)
            {
                if (_dtBarcodeToPAN.Rows[i]["Barcode"].ToString() == btn.CommandArgument.ToString())
                {
                    sPAN = _dtBarcodeToPAN.Rows[i]["PAN"].ToString();
                    break;
                }
            }

            if (!String.IsNullOrWhiteSpace(sPAN))
            {
                string sRcMonext, sMessage;
                bool bOK = MonextActivationCard(authentication.GetCurrent().sToken, _iRefCustomer, sPAN, out sRcMonext, out sMessage);

                ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "AlertMessage('" + ((bOK) ? "Réussite" : "Erreur") + "', '" + ((bOK) ? "Carte activée chez Monext" : sMessage) + "'); HidePanelLoading();", true);
            }
        }
    }
    protected bool MonextActivationCard(string sCashierToken, int iRefCustomer, string sCardPAN, out string sRcMonext, out string sMessage)
    {
        SqlConnection conn = null;
        bool bOK = false;
        sRcMonext = "";
        sMessage = "";

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_ActivationCard", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                                new XElement("Activation",
                                                    new XAttribute("CashierToken", sCashierToken)
                                                    , new XAttribute("RefCustomer", iRefCustomer.ToString())
                                                    , new XAttribute("CardNumber", sCardPAN))).ToString();

            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if (sXmlOut.Length > 0)
            {
                List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Activation", "MONEXT_RC");
                List<string> listMessage = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Activation", "MONEXT_LABEL");

                sRcMonext = listRC[0];
                sMessage = listMessage[0];

                if (sRcMonext.Length == 0 || sRcMonext == "0")
                    bOK = true;
            }
        }
        catch (Exception e)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return bOK;
    }

    // Limites
    protected void BindClientPaymentLimit(string sXmlOut)
    {
        List<string> listPaymentLimit = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Limit", "Payment");
        List<string> listPaymentLimitAmount = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Limit", "PaymentAmount");
        List<string> listRequestPaymentLimit = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Limit", "RequestPaymentLimit");
        List<string> listRequestPaymentLimitDate = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Limit", "RequestDatePaymentLimit");
        List<string> listWithdrawLimit = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Limit", "WithdrawMoney");
        List<string> listWithdrawLimitAmount = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Limit", "WithdrawMoneyAmount");
        List<string> listRequestWithdrawLimit = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Limit", "RequestWithdrawMoneyLimit");
        List<string> listRequestWithdrawLimitDate = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Limit", "RequestDateWithdrawMoneyLimit");
        List<string> listDepositLimitAmount = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Limit", "DepositLimitAmount");

        if (listPaymentLimit.Count > 0 && listPaymentLimitAmount.Count > 0)
        {
            _dCardPaymentLimit = Decimal.Parse(listPaymentLimitAmount[0].Substring(0, listPaymentLimitAmount[0].LastIndexOf(".")), new CultureInfo("fr-FR"));
            hdnCardPaymentLimit.Value = _dCardPaymentLimit.ToString();
            lblCardPaymentLimit.Text = _dCardPaymentLimit.ToString() + " &euro;";

            ListItem item = ddlCardPaymentLimits.Items.FindByValue(listPaymentLimit[0]);
            //int i = ddlCardPaymentLimits.Items.IndexOf(item);
            //if (i == ddlCardPaymentLimits.Items.Count - 1)
            //    i--;
            //else i++;
            //ddlCardPaymentLimits.Items[i].Attributes.Add("selected", "selected");
            //item.Attributes.Add("disabled", "disabled");
            item.Attributes.Add("selected", "selected");
        }

        if (listWithdrawLimit.Count > 0 && listWithdrawLimitAmount.Count > 0)
        {
            _dWithdrawLimit = Decimal.Parse(listWithdrawLimitAmount[0].Substring(0, listWithdrawLimitAmount[0].LastIndexOf(".")), new CultureInfo("fr-FR"));
            hdnWithdrawLimit.Value = _dWithdrawLimit.ToString();
            lblWithdrawLimit.Text = _dWithdrawLimit.ToString() + " &euro;";

            ListItem item = ddlWithdrawLimits.Items.FindByValue(listWithdrawLimit[0]);
            //int i = ddlWithdrawLimits.Items.IndexOf(item);
            //if (i == ddlWithdrawLimits.Items.Count - 1)
            //    i--;
            //else i++;
            //ddlWithdrawLimits.Items[i].Attributes.Add("selected", "selected");
            //item.Attributes.Add("disabled", "disabled");
            item.Attributes.Add("selected", "selected");
        }

        if (listDepositLimitAmount.Count > 0)
        {
            Decimal dCashDepositLimit = Decimal.Parse(listDepositLimitAmount[0].Substring(0, listDepositLimitAmount[0].LastIndexOf(".")), new CultureInfo("fr-FR"));
            hdnCashDepositLimit.Value = dCashDepositLimit.ToString();
            lblCashDepositLimit.Text = dCashDepositLimit.ToString() + " &euro;";
        }
    }
    protected string GetClientPaymentLimit(int iRefCustomer, string sPaymentPeriod, string sWithdrawPeriod)
    {
        SqlConnection conn = null;
        string sXmlOut = "";

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK2"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_GetCustomerLimits", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN", new XElement("Limit",
                                                new XAttribute("RefCustomer", iRefCustomer),
                                                new XAttribute("PaymentPeriod", sPaymentPeriod),
                                                new XAttribute("WithdrawMoneyPeriod", sWithdrawPeriod))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            sXmlOut = cmd.Parameters["@OUT"].Value.ToString();
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return sXmlOut;
    }
    protected void BindClientPaymentSituation(string sXmlOut)
    {
        if (sXmlOut.Length > 0)
        {
            List<string> listPayment = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Situation", "Payment");
            List<string> listWithdraw = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Situation", "WithdrawMoney");
            List<string> listCashIN = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Situation", "CashIN");
            List<string> listTransferIN = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Situation", "TransferMoneyIn");

            if (listPayment.Count > 0)
            {
                try
                {
                    Decimal dCardPaymentLimit = Decimal.Parse(hdnCardPaymentLimit.Value, new CultureInfo("fr-FR"));
                    Decimal dCardPaymentForNow = Decimal.Parse(listPayment[0].Replace(".", ","), new CultureInfo("fr-FR"));
                    Decimal dCardPaymentAvailable = dCardPaymentLimit - dCardPaymentForNow;

                    if (dCardPaymentForNow > dCardPaymentLimit)
                    {
                        dCardPaymentForNow = dCardPaymentLimit;
                        dCardPaymentAvailable = 0;
                    }

                    _dCardPaymentForNow = dCardPaymentForNow;
                    hdnCardPaymentForNow.Value = dCardPaymentForNow.ToString().Replace(".", ",");
                    _dCardPaymentAvailable = dCardPaymentAvailable;
                    lblCardPaymentAvailable.Text = dCardPaymentAvailable.ToString().Replace(".", ",");
                }
                catch (Exception e)
                {
                }
            }
            if (listWithdraw.Count > 0)
            {
                try
                {
                    Decimal dWithdrawLimit = Decimal.Parse(hdnWithdrawLimit.Value, new CultureInfo("fr-FR"));
                    Decimal dWithdrawForNow = Decimal.Parse(listWithdraw[0].Replace(".", ","), new CultureInfo("fr-FR"));
                    Decimal dWithdrawAvailable = dWithdrawLimit - dWithdrawForNow;

                    if (dWithdrawForNow > dWithdrawLimit)
                    {
                        dWithdrawForNow = dWithdrawLimit;
                        dWithdrawAvailable = 0;
                    }

                    _dWithdrawForNow = dWithdrawForNow;
                    hdnWithdrawForNow.Value = dWithdrawForNow.ToString().Replace(".", ",");
                    _dWithdrawAvailable = dWithdrawAvailable;
                    lblWithdrawAvailable.Text = dWithdrawAvailable.ToString().Replace(".", ",");
                }
                catch (Exception e)
                {
                }
            }

            if (listCashIN.Count > 0)
            {
                try
                {
                    _dCashIN = Decimal.Parse(listCashIN[0].Replace(".", ","), new CultureInfo("fr-FR"));
                    Decimal dCashDepositLimit = Decimal.Parse(hdnCashDepositLimit.Value, new CultureInfo("fr-FR"));
                    hdnCashDepositForNow.Value = _dCashIN.ToString().Replace(".", ",");
                    Decimal dCashDepositAvailable = dCashDepositLimit - _dCashIN;
                    lblCashDepositAvailable.Text = dCashDepositAvailable.ToString().Replace(".", ",");
                }
                catch (Exception e) { }
            }
            if (listTransferIN.Count > 0)
            {
                try { _dTransferIN = Decimal.Parse(listTransferIN[0].Replace(".", ","), new CultureInfo("fr-FR")); }
                catch (Exception e) { }
            }

            panelCardPaymentSituation.Visible = true;
            panelWithdrawSituation.Visible = true;
            panelCashDepositSituation.Visible = true;
        }
    }
    protected string GetClientPaymentSituation(int iRefCustomer, string sPaymentPeriod, string sWithdrawPeriod)
    {
        string sXmlOut = "";
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK2"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("Web.P_GetCustomerSituationLimits", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                            new XElement("Situation",
                                                new XAttribute("RefCustomer", iRefCustomer),
                                                new XAttribute("PaymentPeriod", sPaymentPeriod),
                                                new XAttribute("WithdrawMoneyPeriod", sWithdrawPeriod))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            sXmlOut = cmd.Parameters["@OUT"].Value.ToString();
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return sXmlOut;
    }
    private DataTable OrderDtLimits(DataTable dtOut)
    {
        DataTable dt = dtOut.Clone();
        dt.Columns["LimitAmount"].DataType = Type.GetType("System.Single");

        for (int i = 0; i < dtOut.Rows.Count; i++)
        {
            dt.ImportRow(dtOut.Rows[i]);
        }
        dt.AcceptChanges();
        DataView dv = dt.DefaultView;
        dv.Sort = "LimitAmount ASC";

        dtOut = dv.ToTable();

        DataTable dt2 = new DataTable();
        dt2.Columns.Add("Text");
        dt2.Columns.Add("Value");

        for (int i = 0; i < dtOut.Rows.Count; i++)
        {
            DataRow row = dt2.NewRow();

            row["Value"] = dtOut.Rows[i]["LimitTAG"];
            row["Text"] = dtOut.Rows[i]["LimitAmount"].ToString() + " €";

            dt2.Rows.Add(row);
        }

        return dt2;
    }
    private void BindPaymentLimits(int iRefCustomer, string sPaymentKind, string sWithdrawKind)
    {
        DataTable dtPaymentLimits = GetPaymentLimits(iRefCustomer, sPaymentKind);
        DataTable dtWithdrawLimits = GetPaymentLimits(iRefCustomer, sWithdrawKind);

        ddlCardPaymentLimits.DataSource = OrderDtLimits(dtPaymentLimits);
        ddlCardPaymentLimits.DataBind();

        ddlWithdrawLimits.DataSource = OrderDtLimits(dtWithdrawLimits);
        ddlWithdrawLimits.DataBind();

        upChangeLimit.Update();
    }
    protected DataTable GetPaymentLimits(int iRefCustomer, string sType)
    {
        SqlConnection conn = null;
        DataTable dt = new DataTable();

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK2"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_GetAllowedLimitsListForCustomer", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@ReturnSelect", SqlDbType.Bit);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN", new XElement("Limit", new XAttribute("RefCustomer", iRefCustomer.ToString()), new XAttribute("LimitKind", sType))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
            cmd.Parameters["@ReturnSelect"].Value = 1;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;

            da.Fill(dt);
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }
    protected void btnChangeLimit_Click(object sender, EventArgs e)
    {
        if (!ChangeLimit())
            ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Erreur', \"Une erreur est survenue lors de l'enregistrement.\");", true);
        else
        {
            btnLoadLimits_Click(sender, e);
            BindCardHistoryList(GetCardHistoryList(authentication.GetCurrent().sToken, _iRefCustomer.ToString()));
        }

        ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "HideChangeLimitDialog();", true);

        upLimits.Update();
    }
    protected bool ChangeLimit()
    {
        bool bOk = false;
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_ForceCustomerLimit", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                            new XElement("Customer",
                                                new XAttribute("CashierToken", authentication.GetCurrent().sToken),
                                                new XAttribute("RefCustomer", _iRefCustomer),
                                                new XAttribute("PaymentLimit", ddlCardPaymentLimits.SelectedValue),
                                                new XAttribute("WithdrawMoneyLimit", ddlWithdrawLimits.SelectedValue)
                                                //,new XAttribute("DepositLimit", "")
                                                )).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if(!String.IsNullOrWhiteSpace(sXmlOut))
            {
                List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Result", "RC");
                if (listRC.Count > 0 && listRC[0] == "0")
                    bOk = true;
            }
        }
        catch(Exception e)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return bOk;
    }

    protected void btnUpdateCardPreference_Click(object sender, EventArgs e)
    {
        Button btn = (Button)sender;

        if (btn != null)
        {
            XElement xml = new XElement("Card",
                                new XAttribute("CashierToken", authentication.GetCurrent().sToken),
                                new XAttribute("RefCustomer", _iRefCustomer.ToString()));
            switch (btn.ID)
            {
                case "btnUpdateBlockAllPayment":
                    xml.Add(new XAttribute("ForbidAllPayment", ckbBlockAllPayment.Checked ? "1" : "0"));
                    break;
                case "btnUpdateBlockOutFrancePayment":
                    xml.Add(new XAttribute("ForbidForeignPayment", ckbBlockOutFrancePayment.Checked ? "1" : "0"));
                    break;
                case "btnUpdateBlockInternetPayment":
                    xml.Add(new XAttribute("ForbidInternetPayment", ckbBlockInternetPayment.Checked ? "1" : "0"));
                    break;
            }

            if (!UpdateCardPreference(new XElement("ALL_XML_IN", xml).ToString()))
            {
                BindCardPreference();
                ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Erreur', \"Les préférences carte n'ont pas été enregistrées.\");", true);
            }
        }
    }

    protected void BindCardPreference()
    {
        string sXmlOut = GetCardPreference(_iRefCustomer, authentication.GetCurrent().sToken);

        List<string> listForbidAllPayment = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Card", "ForbidAllPayment");
        List<string> listForbidForeignPayment = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Card", "ForbidForeignPayment");
        List<string> listForbidInternetPayment = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Card", "ForbidInternetPayment");

        if (listForbidAllPayment.Count > 0 && listForbidAllPayment[0] == "1")
            ckbBlockAllPayment.Checked = true;
        if (listForbidForeignPayment.Count > 0 && listForbidForeignPayment[0] == "1")
            ckbBlockOutFrancePayment.Checked = true;
        if (listForbidInternetPayment.Count > 0 && listForbidInternetPayment[0] == "1")
            ckbBlockInternetPayment.Checked = true;
    }

    protected bool UpdateCardPreference(string sXmlIn)
    {
        bool bUpdate = false;
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_SetCardPreferences", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);
            cmd.Parameters["@IN"].Value = sXmlIn;
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if (sXmlOut.Length > 0)
            {
                List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Result", "RC");

                if (listRC.Count > 0 && listRC[0] == "0")
                    bUpdate = true;
            }
        }
        catch (Exception e)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }


        return bUpdate;
    }
    protected string GetCardPreference(int iRefCustomer, string sToken)
    {
        string sXmlOut = "";
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_GetCardPreferences", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                        new XElement("Card",
                                            new XAttribute("CashierToken", sToken),
                                            new XAttribute("RefCustomer", iRefCustomer))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            sXmlOut = cmd.Parameters["@OUT"].Value.ToString();
        }
        catch (Exception e)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return sXmlOut;
    }

    #endregion cards

    // RIB
    #region RIB
    private string _RIBClientEmail { get { return (ViewState["RIBClientEmail"] != null) ? ViewState["RIBClientEmail"].ToString() : ""; } set { ViewState["RIBClientEmail"] = value; } }
    private string _RIBClientCivility { get { return (ViewState["RIBClientCivility"] != null) ? ViewState["RIBClientCivility"].ToString() : ""; } set { ViewState["RIBClientCivility"] = value; } }
    private string _RIBClientLastName { get { return (ViewState["RIBClientLastName"] != null) ? ViewState["RIBClientLastName"].ToString() : ""; } set { ViewState["RIBClientLastName"] = value; } }
    private string _RIBClientFirstName { get { return (ViewState["RIBClientFirstName"] != null) ? ViewState["RIBClientFirstName"].ToString() : ""; } set { ViewState["RIBClientFirstName"] = value; } }
    private string _RIBClientAddress1 { get { return (ViewState["RIBClientAddress1"] != null) ? ViewState["RIBClientAddress1"].ToString() : ""; } set { ViewState["RIBClientAddress1"] = value; } }
    private string _RIBClientAddress2 { get { return (ViewState["RIBClientAddress2"] != null) ? ViewState["RIBClientAddress2"].ToString() : ""; } set { ViewState["RIBClientAddress2"] = value; } }
    private string _RIBClientAddress3 { get { return (ViewState["RIBClientAddress3"] != null) ? ViewState["RIBClientAddress3"].ToString() : ""; } set { ViewState["RIBClientAddress3"] = value; } }
    private string _RIBClientAddress4 { get { return (ViewState["RIBClientAddress4"] != null) ? ViewState["RIBClientAddress4"].ToString() : ""; } set { ViewState["RIBClientAddress4"] = value; } }
    private string _RIBClientZipCode { get { return (ViewState["RIBClientZipCode"] != null) ? ViewState["RIBClientZipCode"].ToString() : ""; } set { ViewState["RIBClientZipCode"] = value; } }
    private string _RIBClientCity { get { return (ViewState["RIBClientCity"] != null) ? ViewState["RIBClientCity"].ToString() : ""; } set { ViewState["RIBClientCity"] = value; } }
    private string _RIBAgencyName { get { return (ViewState["RIBAgencyName"] != null) ? ViewState["RIBAgencyName"].ToString() : ""; } set { ViewState["RIBAgencyName"] = value; } }
    private string _RIBAgencyAddress1 { get { return (ViewState["RIBAgencyAddress1"] != null) ? ViewState["RIBAgencyAddress1"].ToString() : ""; } set { ViewState["RIBAgencyAddress1"] = value; } }
    private string _RIBAgencyAddress2 { get { return (ViewState["RIBAgencyAddress2"] != null) ? ViewState["RIBAgencyAddress2"].ToString() : ""; } set { ViewState["RIBAgencyAddress2"] = value; } }
    private string _RIBAgencyAddress3 { get { return (ViewState["RIBAgencyAddress3"] != null) ? ViewState["RIBAgencyAddress3"].ToString() : ""; } set { ViewState["RIBAgencyAddress3"] = value; } }
    private string _RIBAgencyZipCode { get { return (ViewState["RIBAgencyZipCode"] != null) ? ViewState["RIBAgencyZipCode"].ToString() : ""; } set { ViewState["RIBAgencyZipCode"] = value; } }
    private string _RIBAgencyCity { get { return (ViewState["RIBAgencyCity"] != null) ? ViewState["RIBAgencyCity"].ToString() : ""; } set { ViewState["RIBAgencyCity"] = value; } }
    private string _RIBBranchCode { get { return (ViewState["RIBBranchCode"] != null) ? ViewState["RIBBranchCode"].ToString() : ""; } set { ViewState["RIBBranchCode"] = value; } }
    private string _RIBKey { get { return (ViewState["RIBKey"] != null) ? ViewState["RIBKey"].ToString() : ""; } set { ViewState["RIBKey"] = value; } }
    private string _RIBBankCode { get { return (ViewState["RIBBankCode"] != null) ? ViewState["RIBBankCode"].ToString() : ""; } set { ViewState["RIBBankCode"] = value; } }
    private string _RIBBIC { get { return (ViewState["RIBBIC"] != null) ? ViewState["RIBBIC"].ToString() : ""; } set { ViewState["RIBBIC"] = value; } }
    private string _RIBIBAN { get { return (ViewState["RIBIBAN"] != null) ? ViewState["RIBIBAN"].ToString() : ""; } set { ViewState["RIBIBAN"] = value; } }
    private string _RIBAccountNumber { get { return (ViewState["RIBAccountNumber"] != null) ? ViewState["RIBAccountNumber"].ToString() : ""; } set { ViewState["RIBAccountNumber"] = value; } }
    protected void BindRibInfos(int refCustomer)
    {
        if (_sAccountNumber != null && _sAccountNumber.Trim().Length > 0)
        {

            try
            {
                WS_SAB.GetRibResponse wsRes = tools.GetBankDetails(_sAccountNumber, refCustomer);

                if (wsRes != null)
                {
                    if (wsRes.Error.RC == WS_SAB.ErrorCodes.OK)
                    {

                        string sXMLCustomerInfos = tools.GetXMLCustomerInformations(refCustomer);
                        List<string> listCivility = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "Politeness");
                        List<string> listEmail = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "Email");

                        try
                        {
                            lblRIBAgencyName.Text = wsRes.AgencyName;
                            _RIBAgencyName = wsRes.AgencyName;
                            lblRIBAgencyAddress.Text = (wsRes.AgencyAddress1.Length > 0) ? wsRes.AgencyAddress1 : "";
                            _RIBAgencyAddress1 = wsRes.AgencyAddress1;
                            lblRIBAgencyAddress.Text += (wsRes.AgencyAddress2.Length > 0) ? "<br/>" + wsRes.AgencyAddress2 : "";
                            _RIBAgencyAddress2 = wsRes.AgencyAddress2;
                            lblRIBAgencyAddress.Text += (wsRes.AgencyAddress3.Length > 0) ? "<br/>" + wsRes.AgencyAddress3 : "";
                            _RIBAgencyAddress3 = wsRes.AgencyAddress3;
                            lblRIBAgencyZipcode.Text = wsRes.AgencyZipCode;
                            _RIBAgencyZipCode = wsRes.AgencyZipCode;
                            lblRIBAgencyCity.Text = wsRes.AgencyCity;
                            _RIBAgencyCity = wsRes.AgencyCity;
                            _RIBClientCivility = (listCivility.Count > 0) ? listCivility[0] : "";
                            lblRIBClientCivility.Text = _RIBClientCivility;
                            _RIBClientLastName = wsRes.CustomerLastName;
                            lblRIBClientLastName.Text = _RIBClientLastName;
                            _RIBClientFirstName = wsRes.CustomerFirstName;
                            lblRIBClientFirstName.Text = _RIBClientFirstName;
                            _RIBClientAddress1 = wsRes.CustomerAddress1;
                            lblRIBClientAddress.Text = _RIBClientAddress1;
                            _RIBClientAddress2 = wsRes.CustomerAddress2;
                            lblRIBClientAddress.Text += (_RIBClientAddress2.Trim().Length > 0) ? " " + _RIBClientAddress2 : "";
                            _RIBClientAddress3 = wsRes.CustomerAddress3;
                            lblRIBClientAddress.Text += (_RIBClientAddress3.Trim().Length > 0) ? " " + _RIBClientAddress3 : "";
                            _RIBClientAddress4 = wsRes.CustomerAddress4;
                            lblRIBClientAddress.Text += (_RIBClientAddress4.Trim().Length > 0) ? " " + _RIBClientAddress4 : "";
                            _RIBClientZipCode = wsRes.CustomerZipCode;
                            lblRIBClientZipcode.Text = _RIBClientZipCode;
                            _RIBClientCity = wsRes.CustomerCity;
                            lblRIBClientCity.Text = _RIBClientCity;
                            _RIBClientEmail = (listEmail.Count > 0) ? listEmail[0] : "";
                            lblRIBBranchCode.Text = wsRes.OfficeCode;
                            _RIBBranchCode = wsRes.OfficeCode;
                            lblRIBKey.Text = wsRes.RibKey;
                            _RIBKey = wsRes.RibKey;
                            lblRIBBankCode.Text = wsRes.BankCode;
                            _RIBBankCode = wsRes.BankCode;
                            _RIBBIC = "FPELFR21";
                            lblRIBBic.Text = _RIBBIC;
                            _RIBIBAN = wsRes.Iban.Replace("IBAN", "").Trim();
                            lblRIBIban.Text = _RIBIBAN;

                            lblRIBAccountNumber.Text = wsRes.AccountNumber;
                            _RIBAccountNumber = wsRes.AccountNumber;

                            btnPrintRib.OnClientClick = "printRib('" + _iRefCustomer.ToString() + "','" + _sAccountNumber + "'); return false;";
                        }
                        catch (Exception ex)
                        {
                            UpdateMessage("Les informations du RIB n'ont pas pu être récupérées. <br/>Veuillez cliquer sur \"Rafraichir\".");
                        }
                    }
                    else
                    {
                        UpdateMessage("Les informations du RIB n'ont pas pu être récupérées. <br/>" + wsRes.Error.ErrorMsg);
                    }
                }
                else
                {
                    throw new System.ArgumentException("Réponse WEB SERVICE SAB Null", "NullException_wsSAB ");
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

    }
    protected void btnRibSendMail_Click(object sender, EventArgs e)
    {
        try
        {
            if (_RIBAccountNumber != null && _RIBAccountNumber.Length > 0)
            {
                WS_PDFCreator.PDFCreatorClient pdf = new WS_PDFCreator.PDFCreatorClient();

                string sContent = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "mail_rib.html", System.Text.Encoding.UTF8);

                string sPolitness = "";
                switch (_RIBClientCivility)
                {
                    case "MR":
                        sPolitness = "M.";
                        break;
                    case "MME":
                    case "MLLE":
                        sPolitness = "Mme";
                        break;
                    default:
                        sPolitness = _RIBClientCivility;
                        break;
                }
                tools.ReplaceTag(ref sContent, "@@CIVILITY", sPolitness);
                tools.ReplaceTag(ref sContent, "@@LASTNAME", _RIBClientLastName);
                tools.ReplaceTag(ref sContent, "@@FIRSTNAME", _RIBClientFirstName);

                string sAgencyAddress = _RIBAgencyAddress1;
                sAgencyAddress += (_RIBAgencyAddress2.Length > 0) ? "\n" + _RIBAgencyAddress2 : "";
                sAgencyAddress += (_RIBAgencyAddress3.Length > 0) ? "\n" + _RIBAgencyAddress3 : "";
                string sClientAddress = _RIBClientAddress1;
                sClientAddress += (_RIBClientAddress2.Length > 0) ? "\n" + _RIBClientAddress2 : "";
                sClientAddress += (_RIBClientAddress3.Length > 0) ? "\n" + _RIBClientAddress3 : "";
                sClientAddress += (_RIBClientAddress4.Length > 0) ? "\n" + _RIBClientAddress4 : "";

                if (cbChangeRibEmail.Checked)
                    _RIBClientEmail = txtRibMail.Text;


                string sXmlIn = new XElement("ALL_XML_IN",
                                    new XElement("BANKDETAILS",
                                        new XElement("AGENCYNAME", _RIBAgencyName),
                                        new XElement("AGENCYADDRESS", sAgencyAddress),
                                        new XElement("AGENCYZIPCODE", _RIBAgencyZipCode),
                                        new XElement("AGENCYCITY", _RIBAgencyCity),
                                        new XElement("CLIENTCIVILITY", _RIBClientCivility),
                                        new XElement("CLIENTLASTNAME", _RIBClientLastName),
                                        new XElement("CLIENTFIRSTNAME", _RIBClientFirstName),
                                        new XElement("CLIENTADDRESS", sClientAddress),
                                        new XElement("CLIENTZIPCODE", _RIBClientZipCode),
                                        new XElement("CLIENTCITY", _RIBClientCity),
                                        new XElement("BANKCODE", _RIBBankCode),
                                        new XElement("BRANCHCODE", _RIBBranchCode),
                                        new XElement("ACCOUNTNUMBER", _RIBAccountNumber),
                                        new XElement("KEY", _RIBKey),
                                        new XElement("BIC", _RIBBIC),
                                        new XElement("IBAN", _RIBIBAN),
                                        new XElement("MAIL",
                                            new XElement("TO", _RIBClientEmail),
                                            new XElement("FROM", "utile@compte-nickel.fr"),
                                            new XElement("SUBJECT", "Votre RIB Compte-Nickel"),
                                            new XElement("CONTENT", sContent)
                                            ))).ToString();

                string sXmlOut = pdf.SendBankDetails(sXmlIn);

                List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/BANKDETAILS", "RC");

                if (listRC.Count > 0 && listRC[0] == "0")
                {
                    //lblRibSendMailResult.Text = "Votre RIB vient de vous être envoyé à l'adresse " + txtRibMail.Text + ".";
                    UpdateMessage("Votre RIB vient de vous être envoyé à l'adresse " + txtRibMail.Text + ".");
                }
                else throw new Exception();
            }
            else throw new Exception();
        }
        catch (Exception ex)
        {
            //lblRibSendMailResult.Text = "Une erreur est servenue, veuillez essayer à nouveau.";
            //lblRibSendMailResult.ForeColor = System.Drawing.Color.Red;
            UpdateMessage("Une erreur est survenue, veuillez essayer à nouveau.");
        }
        finally
        {
            panelRibSendMailResult.Visible = true;
        }
    }
    protected void btnRibRefresh_Click(object sender, EventArgs e)
    {
        BindRibInfos(_iRefCustomer);
    }
    #endregion RIB

    // LOCK/UNLOCK
    protected void BindLockUnlockInfos()
    {
        if (_bLocked)
        {
            //lblLockUnlockTitle.Text = "Débloquer";
            hfLockUnlockStatus.Value = "true";
            btnShowLockUnlockDialog.Text = "Débloquer";
            btnLockUnlock.Text = "Débloquer";
            lblLockedDate.Text = (_sLockedDate != null && _sLockedDate.Trim().Length > 0) ? "Bloqué le " + _sLockedDate : "<span style=\"font-style:italic\">Non renseigné</span>";
            lblLockReason.Text = (_sLockReason != null && _sLockReason.Trim().Length > 0) ? _sLockReason : "<span style=\"font-style:italic\">Non renseigné</span>";
            divLockInfo.Visible = true;
        }
        else
        {
            //lblLockUnlockTitle.Text = "Bloquer";
            btnShowLockUnlockDialog.Text = "Bloquer";
            btnLockUnlock.Text = "Bloquer";
            lblLockedDate.Text = "";
            lblLockReason.Text = "";
            hfLockUnlockStatus.Value = "false";
            divLockInfo.Visible = false;
        }
    }
    protected void btnLockUnlock_Click(object sender, EventArgs e)
    {
        if (lockUnlock())
        {
            if (!_bLocked)
                UpdateMessage("Compte client bloqué.");
            else
                UpdateMessage("Compte client débloqué.");
        }
        else
        {
            if (!_bLocked)
                UpdateMessage("La demande de bloquage du compte a échoué.");
            else
                UpdateMessage("La demande de débloquage du compte a échoué.");
        }

        BindCustomerInformations(Client.GetCustomerInformations(_iRefCustomer), true);
        BindLockUnlockInfos();
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "InitLockUnlock", "InitLockUnlock();", true);
    }
    protected bool lockUnlock()
    {
        bool isOK = false;
        string sXmlIN = "";
        string sToken = authentication.GetCurrent(false).sToken;

        if (_bLocked)
        {
            sXmlIN = new XElement("ALL_XML_IN",
                        new XElement("Customer",
                            new XAttribute("CashierToken", sToken),
                            new XAttribute("RefCustomer", _iRefCustomer),
                            new XAttribute("IsLocked", false))).ToString();
        }
        else
        {
            sXmlIN = new XElement("ALL_XML_IN",
                        new XElement("Customer",
                            new XAttribute("CashierToken", sToken),
                            new XAttribute("RefCustomer", _iRefCustomer),
                            new XAttribute("IsLocked", true),
                            new XAttribute("LockReason", txtLockReason.Text))).ToString();
        }

        isOK = SaveInformations(sXmlIN);

        return isOK;
    }

    //AML
    #region AML
    protected void BindAML()
    {
        BindClientSpecifities();
        BindClientNote();
        BindClientProfile();
        AmlAlert1.RefCustomer = _iRefCustomer;
        AmlAlert1.Bind(false, 1);
        hdnNbAlert.Value = AmlAlert1.NbAlert.ToString();
        AmlHideAlert1.RefCustomer = _iRefCustomer;
        AmlHideAlert1.Bind();
        if (AmlAlert1.NbAlert > 0)
        {
            //btnOpenClientWatch.Visible = true;
            if (AmlAlert1.NbWatched > 0)
                hfWatchStatus.Value = "1";
            //btnOpenClientWatch.Text = "NE PLUS SURVEILLER";
            else
                hfWatchStatus.Value = "0";
            //btnOpenClientWatch.Text = "SURVEILLER";
        }
        else
            hfWatchStatus.Value = "-1";
        //btnOpenClientWatch.Visible = false;

        authentication auth = authentication.GetCurrent();
        lblEmail_getSummarySheet.Text = auth.sEmail;

        AmlSurveillance1.BindAmlSurveillance(_iRefCustomer, _bSousSurveillance, _bLockTransfer, _bRequestTransferValidation, _sRequestTransferValidationAmount);
        if (_bSousSurveillance) imgSurveillanceIcon.Visible = true;
        else imgSurveillanceIcon.Visible = false;
        upSurveillanceIcon.Update();

        SetBlockedTransfer();
    }
    protected void SetBlockedTransfer()
    {
        panelAmlClientBlockedTransfer.Visible = false;
        DataTable dt = new DataTable();

        authentication auth = authentication.GetCurrent();
        string sXmlIn = new XElement("ALL_XML_IN",
                                            new XElement("PendingTransfer",
                                                new XAttribute("TransferStatus", "PENDING"),
                                                new XAttribute("CashierToken", auth.sToken),
                                                new XAttribute("RefCustomer", _iRefCustomer.ToString()),
                                                new XAttribute("PageSize", "5"),
                                                new XAttribute("PageIndex", "1"),
                                                new XAttribute("OrderBy", "RefTransferPending"),
                                                new XAttribute("OrderASC", "DESC"),
                                                new XAttribute("Culture", ((Session["CultureID"] != null) ? Session["CultureID"].ToString() : "fr-FR")))).ToString(); ;
        string sXmlOut = "";

        dt = AML.getPendingTransferList(sXmlIn, out sXmlOut);
        if (dt.Rows.Count > 0)
        {
            panelAmlClientBlockedTransfer.Visible = true;
            BlockedTransfer1.RefCustomer = _iRefCustomer;
            BlockedTransfer1.isFilterVisible = false;
            BlockedTransfer1.iPage = 1;
            BlockedTransfer1.iPageSize = 5;
            BlockedTransfer1.JsToCallAfterAmlClientNote = "refreshAmlClientNote();";
            BlockedTransfer1.Init();
        }

    }
    protected void btnClientAlert_Click(object sender, EventArgs e)
    {
        Response.Redirect("ClientAlert.aspx?ref=" + _iRefCustomer.ToString());
    }
    protected void BindClientSpecifities()
    {
        refreshClientSpecificities();
    }
    protected void BindClientNote()
    {
        refreshClientNote();
    }
    protected void rptAML_ClientSpecificities_itemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Footer)
        {
            btnAddSpecificity_repeater = (Button)e.Item.FindControl("btnShowAddClientSpecificity1");
            if (btnAddSpecificity_repeater != null)
            {
                if (ddlSpecificityList.Items.Count > 0)
                    btnAddSpecificity_repeater.Visible = true;
                else
                    btnAddSpecificity_repeater.Visible = false;
            }

        }
    }
    protected void refreshClientSpecificities()
    {
        DataTable dtClientSpecificities = AML.getClientSpecifities(_iRefCustomer, true);
        DataTable dtSpecificityList = AML.getSpecificityList();
        DataTable dtSpecificityListFiltered = dtSpecificityList;

        rptAML_ClientSpecificities.Visible = true;
        panel_ClientSpecificitiesNoResult.Visible = false;
        rptAML_ClientSpecificities.DataSource = dtClientSpecificities;
        rptAML_ClientSpecificities.DataBind();
        if (rptAML_ClientSpecificities.Items.Count == 0)
        {
            rptAML_ClientSpecificities.Visible = false;
            panel_ClientSpecificitiesNoResult.Visible = true;
        }

        string sRefSpecificityCurrent = "";
        string sRefSpecificity = "";
        for (int i = 0; i < dtClientSpecificities.Rows.Count; i++)
        {
            if (!IsSpecificityDeleted(dtClientSpecificities.Rows[i]["IsDeleted"].ToString()))
            {
                sRefSpecificityCurrent = dtClientSpecificities.Rows[i]["RefSpecificity"].ToString().Trim();
                for (int j = 0; j < dtSpecificityListFiltered.Rows.Count; j++)
                {
                    sRefSpecificity = dtSpecificityListFiltered.Rows[j]["RefSpecificity"].ToString().Trim();
                    if (sRefSpecificity == sRefSpecificityCurrent)
                    {
                        dtSpecificityListFiltered.Rows[j].Delete();
                        dtSpecificityListFiltered.AcceptChanges();
                    }
                }
            }
        }
        if (dtSpecificityListFiltered.Rows.Count > 0)
        {
            btnShowAddClientSpecificity2.Visible = true;
            if (btnAddSpecificity_repeater != null)
                btnAddSpecificity_repeater.Visible = true;
            ddlSpecificityList.Items.Clear();
            ddlSpecificityList.Items.Add(new ListItem("", ""));
            ddlSpecificityList.DataSource = dtSpecificityListFiltered;
            ddlSpecificityList.DataBind();
        }
        else
        {
            btnShowAddClientSpecificity2.Visible = false;
            if (btnAddSpecificity_repeater != null)
                btnAddSpecificity_repeater.Visible = false;
        }

        Panel panel = (Panel)rptAML_ClientSpecificities.Controls[rptAML_ClientSpecificities.Controls.Count - 1].Controls[0].FindControl("panelShowDelClientSpecificities");
        if (panel != null)
        {
            if (getNbClientSpecificities(dtClientSpecificities, true) < dtClientSpecificities.Rows.Count)
                panel.Visible = true;
            else
                panel.Visible = false;
        }
    }
    protected int getNbClientSpecificities(DataTable dt, bool isDeleted)
    {
        int Nb = 0;

        if (isDeleted)
        {
            foreach (DataRow dr in dt.Rows)
                if (dr["IsDeleted"].ToString().Trim().ToLower() == "true")
                    Nb++;
        }
        else
        {
            foreach (DataRow dr in dt.Rows)
                if (dr["IsDeleted"].ToString().Trim().ToLower() != "true")
                    Nb++;
        }

        return Nb;
    }
    protected bool IsSpecificityDeleted(string sIsDeleted)
    {
        if (sIsDeleted.Trim().ToLower() == "true")
            return true;

        return false;
    }
    protected string GetSpecificityStatus(string sIsDeleted)
    {
        if (sIsDeleted.Trim().ToLower() == "true")
            return "ClientSpecificityDeleted";

        return "";
    }
    protected void btnAddClientSpecificities_Click(object sender, EventArgs e)
    {
        authentication auth = authentication.GetCurrent();
        AML.addClientSpecificity(auth.sToken, _iRefCustomer, ddlSpecificityList.SelectedValue);
        refreshClientSpecificities();
    }
    protected void btnDelClientSpecificities_Click(object sender, EventArgs e)
    {
        authentication auth = authentication.GetCurrent();
        for (int i = 0; i < rptAML_ClientSpecificities.Items.Count; i++)
        {
            CheckBox cb = new CheckBox();
            Label lbl = new Label();
            bool isChecked = false;
            string sRefSpecificity = ""; int iRefSpecificity;

            if (rptAML_ClientSpecificities.Items[i].FindControl("cbClientSpecificity") is CheckBox)
            {
                cb = (CheckBox)rptAML_ClientSpecificities.Items[i].FindControl("cbClientSpecificity");
                if (cb != null && cb.Checked)
                    isChecked = true;
            }

            if (rptAML_ClientSpecificities.Items[i].FindControl("lblClientRefSpecificity") is Label)
            {
                lbl = (Label)rptAML_ClientSpecificities.Items[i].FindControl("lblClientRefSpecificity");
                if (lbl != null && lbl.Text.Trim().Length > 0)
                    sRefSpecificity = lbl.Text.Trim();
            }

            if (isChecked && sRefSpecificity.Trim().Length > 0 && int.TryParse(sRefSpecificity, out iRefSpecificity))
                AML.delClientSpecificity(auth.sToken, _iRefCustomer, iRefSpecificity.ToString());
        }
        refreshClientSpecificities();
    }
    protected void refreshAml(object sender, EventArgs e)
    {
        hdnNbAlert.Value = AmlAlert1.NbAlert.ToString();
        refreshClientNote();
        AmlAlert1.Bind(true, 1);
        upAmlClientAlert.Update();
        BlockedTransfer1.Init();
        upAmlClientBlockedTransfer.Update();
        upAML.Update();
    }
    protected void refreshClientNote(object sender, EventArgs e)
    {
        refreshClientNote();
    }
    protected void refreshClientNote()
    {
        rptAML_ClientNote.Visible = true;
        panel_ClientNoteNoResult.Visible = false;
        rptAML_ClientNote.DataSource = AML.getClientNote(_iRefCustomer);
        rptAML_ClientNote.DataBind();
        if (rptAML_ClientNote.Items.Count == 0)
        {
            rptAML_ClientNote.Visible = false;
            panel_ClientNoteNoResult.Visible = true;
        }

        txtClientNote.Attributes.Add("onKeyUp", "javascript:checkMaxLength(this, null, \"lblNbCharClientNote\", \"" + lblNbMaxClientNote.ClientID + "\")");
        txtClientNote.Attributes.Add("onChange", "javascript:checkMaxLength(this, null, \"lblNbCharClientNote\", \"" + lblNbMaxClientNote.ClientID + "\")");
    }
    protected void btnAddClientNote_Click(object sender, EventArgs e)
    {
        authentication auth = authentication.GetCurrent();
        AML.addClientNote(auth.sToken, _iRefCustomer, txtClientNote.Text.Trim());
        refreshClientNote();
    }
    protected void BindClientProfile()
    {
        DataTable dtCurrentProfile = new DataTable();
        DataTable dtProfileHistory = new DataTable();

        AML.getClientProfile(_iRefCustomer, out dtProfileHistory, out dtCurrentProfile);

        if (dtCurrentProfile.Rows.Count == 0)
        {
            panelClientProfile_Current.Visible = false;
            panelClientProfile_Empty.Visible = true;
        }
        else
        {
            panelClientProfile_Current.Visible = true;
            panelClientProfile_Empty.Visible = false;
            lblAmlClientProfile_TAG.Text = dtCurrentProfile.Rows[0]["TAG"].ToString();
            lblAmlClientProfile_Date.Text = (dtCurrentProfile.Rows[0]["Date"].ToString().Trim().Length > 0) ? tools.getFormattedDate(dtCurrentProfile.Rows[0]["Date"].ToString()) : "-";
            lblAmlClientProfile_ByUser.Text = (dtCurrentProfile.Rows[0]["ByUser"].ToString().Trim().Length > 0) ? dtCurrentProfile.Rows[0]["ByUser"].ToString() : "-";
            lblAmlClientProfile_Reason.Text = (dtCurrentProfile.Rows[0]["Reason"].ToString().Trim().Length > 0) ? dtCurrentProfile.Rows[0]["Reason"].ToString() : "-";
        }

        panelClientProfile_History.Visible = false;
        if (dtProfileHistory.Rows.Count > 0)
        {
            panelClientProfile_History.Visible = true;
            rptAML_ClientProfile_History.DataSource = dtProfileHistory;
            rptAML_ClientProfile_History.DataBind();
        }

        rptProfileList.DataSource = AML.getProfiles();
        rptProfileList.DataBind();

        txtProfileReason.Attributes.Add("onKeyUp", "javascript:checkMaxLength(this, null, \"lblNbCharClientProfileReason\",\"" + lblNbMaxClientProfileReason.ClientID + "\")");
        txtProfileReason.Attributes.Add("onChange", "javascript:checkMaxLength(this, null, \"lblNbCharClientProfileReason\",\"" + lblNbMaxClientProfileReason.ClientID + "\")");
    }
    protected void btnRefreshProfileHistory_Click(object sender, EventArgs e)
    {
        BindClientProfile();
    }
    protected void btnClientProfileChange_Click(object sender, EventArgs e)
    {
        authentication auth = authentication.GetCurrent();

        if (hdnClientProfileSelected.Value.Trim().Length > 0)
        {
            string selectedProfile = hdnClientProfileSelected.Value;
            if (AML.setClientProfile(auth.sToken, _iRefCustomer, selectedProfile, txtProfileReason.Text))
                BindClientProfile();
            else
                UpdateMessage("Le changement du profil client a échoué.");
        }
    }
    protected void btnLoadLimits_Click(object sender, EventArgs e)
    {
        authentication auth = authentication.GetCurrent();
        BindClientLimitInfo(_iRefCustomer);
        BindPaymentLimits(_iRefCustomer, _sPaymentKind, _sWithdrawKind);
        BindClientPaymentLimit(GetClientPaymentLimit(_iRefCustomer, _sPaymentLimitPeriod, _sWithdrawLimitPeriod));
        BindClientPaymentSituation(GetClientPaymentSituation(_iRefCustomer, _sPaymentLimitPeriod, _sWithdrawLimitPeriod));
        BindChangeLimitRequests(GetChangeLimitRequests(_iRefCustomer, auth.sToken));
        panelLimitsToLoad.Visible = true;
        btnLoadLimits.Visible = false;
        upLimits.Update();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "InitLimitsBar", "InitLimitsBar();", true);
    }
    protected void BindClientLimitInfo(int iRefCustomer)
    {
        string sXmlOut = GetClientLimitInfo(iRefCustomer);

        if (sXmlOut.Length > 0)
        {
            //<ALL_XML_OUT>
            //<LimitInfo NbDaysPayment="30" NbDaysWithDrawMoney="30" PaymentKind="ACH" WithDrawMoneyKind="RET" PaymentLimit="PL_ACH_5000" WithDrawMoneyLimit="PL_RET_2000" />
            //</ALL_XML_OUT>

            List<string> listPaymentKind = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/LimitInfo", "PaymentKind");
            List<string> listPaymentLimitPeriod = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/LimitInfo", "NbDaysPayment");
            List<string> listWithdrawKind = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/LimitInfo", "WithDrawMoneyKind");
            List<string> listWithdrawLimitPeriod = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/LimitInfo", "NbDaysWithDrawMoney");
            List<string> listCashDepositPeriod = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/LimitInfo", "NbDaysLoadMoney");

            _sPaymentKind = (listPaymentKind.Count > 0 && listPaymentKind[0].Length > 0) ? listPaymentKind[0] : "";
            _sPaymentLimitPeriod = (listPaymentLimitPeriod.Count > 0 && listPaymentLimitPeriod[0].Length > 0) ? listPaymentLimitPeriod[0] : "";
            _sWithdrawKind = (listWithdrawKind.Count > 0 && listWithdrawKind[0].Length > 0) ? listWithdrawKind[0] : "";
            _sWithdrawLimitPeriod = (listWithdrawLimitPeriod.Count > 0 && listWithdrawLimitPeriod[0].Length > 0) ? listWithdrawLimitPeriod[0] : "";

            lblPaymentLimitPeriod.Text = _sPaymentLimitPeriod;
            lblWithdrawLimitPeriod.Text = _sWithdrawLimitPeriod;
            lblCashDepositLimitPeriod.Text = (listCashDepositPeriod.Count > 0 && listCashDepositPeriod[0].Length > 0) ? listCashDepositPeriod[0] : "";
        }
    }
    public static string GetClientLimitInfo(int iRefCustomer)
    {
        string sXmlOut = "";
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("Web.P_GetCustomerLimitInfo", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                            new XElement("LimitInfo",
                                                new XAttribute("RefCustomer", iRefCustomer))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            sXmlOut = cmd.Parameters["@OUT"].Value.ToString();
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return sXmlOut;
    }
    protected void btnConfirmClientWatched_Click(object sender, EventArgs e)
    {
        authentication auth = authentication.GetCurrent();
        string sError = "";

        if (txtWatchedComment.Text.Trim().Length > 0)
        {
            int iRefSeller;

            if (int.TryParse(auth.sRef, out iRefSeller))
            {
                string sComment = "<b>RAISON DU SUIVI</b> : " + txtWatchedComment.Text;
                if (AML.watchClientAlert(iRefSeller, _iRefCustomer))
                {
                    AML.addClientNote(auth.sToken, _iRefCustomer, sComment);
                    AmlAlert1.Bind(true, 1);
                    refreshClientNote();
                    upAmlClientAlert.Update();
                    upAML.Update();
                    txtWatchedComment.Text = "";
                    if (AmlAlert1.NbAlert > 0)
                    {
                        if (AmlAlert1.NbWatched > 0)
                            hfWatchStatus.Value = "1";
                        else
                            hfWatchStatus.Value = "0";
                    }
                    else
                        hfWatchStatus.Value = "-1";

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "InitControlsKEY", "InitControls();InitAML();", true);

                }
                else
                    sError = "<span style=\"color:red;\">Une erreur est survenue.<br/>Veuillez réessayer.</span>";
            }
            else
                sError = "<span style=\"color:red;\">Une erreur est survenue.<br/>Veuillez réessayer.</span>";
        }
        else
        {
            sError = "<span style=\"color:red;\">Veuillez ajouter un commentaire avant de valider la mise sous surveillance du client.</span>";
        }

        if (sError.Trim().Length > 0)
            UpdateMessage(sError);
    }
    protected void btnConfirmClientUnwatched_Click(object sender, EventArgs e)
    {
        authentication auth = authentication.GetCurrent();
        string sError = "";

        if (txtWatchedComment.Text.Trim().Length > 0)
        {
            int iRefSeller;

            if (int.TryParse(auth.sRef, out iRefSeller))
            {
                string sComment = "<b>RAISON ARRET DU SUIVI</b> : " + txtWatchedComment.Text;
                if (AML.unwatchClientAlert(iRefSeller, _iRefCustomer))
                {
                    AML.addClientNote(auth.sToken, _iRefCustomer, sComment);
                    AmlAlert1.Bind(true, 1);
                    refreshClientNote();
                    upAmlClientAlert.Update();
                    upAML.Update();
                    txtWatchedComment.Text = "";
                    if (AmlAlert1.NbAlert > 0)
                    {
                        if (AmlAlert1.NbWatched > 0)
                            hfWatchStatus.Value = "1";
                        else
                            hfWatchStatus.Value = "0";
                    }
                    else
                        hfWatchStatus.Value = "-1";

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "InitControlsKEY", "InitControls();InitAML();", true);
                }
                else
                    sError = "<span style=\"color:red;\">Une erreur est survenue.<br/>Veuillez réessayer.</span>";
            }
            else
                sError = "<span style=\"color:red;\">Une erreur est survenue.<br/>Veuillez réessayer.</span>";
        }
        else
        {
            sError = "<span style=\"color:red;\">Veuillez ajouter un commentaire avant de supprimer la mise sous surveillance du client.</span>";
        }

        if (sError.Trim().Length > 0)
            UpdateMessage(sError);
    }
    protected void btnRefreshWebMessageHistory_Click(object sender, ImageClickEventArgs e)
    {
        BindWebMessages(GetWebMessages(_iRefCustomer));
        upWebMessage.Update();
    }
    protected DataTable GetWebMessages(int iRefCustomer)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("RefMessage");
        dt.Columns.Add("Title");
        dt.Columns.Add("Content");
        dt.Columns.Add("Date");
        dt.Columns.Add("Status");
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_GetMessage", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@ReturnSelect", SqlDbType.Bit);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN", new XElement("Message", new XAttribute("RefCustomer", iRefCustomer), new XAttribute("HomePage", "0"))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
            cmd.Parameters["@ReturnSelect"].Value = 0;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if (sXmlOut.Length > 0)
            {
                List<string> listMessage = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/Message");

                for (int i = 0; i < listMessage.Count; i++)
                {
                    List<string> listRef = CommonMethod.GetAttributeValues(listMessage[i], "Message", "RefMessage");
                    List<string> listTitle = CommonMethod.GetAttributeValues(listMessage[i], "Message", "Title");
                    List<string> listContent = CommonMethod.GetAttributeValues(listMessage[i], "Message", "Content");
                    List<string> listRead = CommonMethod.GetAttributeValues(listMessage[i], "Message", "Read");
                    List<string> listDate = CommonMethod.GetAttributeValues(listMessage[i], "Message", "InsertDate");

                    string sDate = "";
                    if (listDate.Count > 0 && listDate[0].Length >= 10)
                    {
                        DateTime date = new DateTime();
                        DateTime.TryParse(listDate[0], out date);
                        sDate = date.ToString("dd/MM/yyyy");
                    }

                    dt.Rows.Add(new object[] { listRef[0], listTitle[0], listContent[0], sDate, (listRead[0] == "0") ? "non lu" : "lu" });
                }
            }
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }
    protected void BindWebMessages(DataTable dt)
    {
        if (dt.Rows.Count > 0)
        {
            rptWebMessage.DataSource = dt;
            rptWebMessage.DataBind();

            panelNoWebMessage.Visible = false;
            rptWebMessage.Visible = true;

            panelDeleteWebMessage.Visible = _bActionWebMessage;
        }
        else
        {
            panelNoWebMessage.Visible = true;
            rptWebMessage.Visible = false;
            panelDeleteWebMessage.Visible = false;
        }
    }
    protected void btnDeleteWebMessage_Click(object sender, EventArgs e)
    {
        int iNbMessageDeleted = 0;
        for (int i = 0; i < rptWebMessage.Items.Count; i++)
        {
            CheckBox ckb = (CheckBox)rptWebMessage.Items[i].FindControl("ckbWebMessage");
            HiddenField hdn = (HiddenField)rptWebMessage.Items[i].FindControl("hdnRefMessage");

            if (hdn != null && hdn.Value.Length > 0 && ckb != null && ckb.Checked == true)
            {
                if (DeleteWebMessage(_iRefCustomer, hdn.Value))
                    iNbMessageDeleted++;
            }
        }

        string sMessage = iNbMessageDeleted.ToString();
        sMessage += (iNbMessageDeleted > 1) ? " messages supprimés" : " message supprimé";
        UpdateMessage(sMessage);

        if (iNbMessageDeleted > 0)
            btnRefreshWebMessageHistory_Click(sender, new ImageClickEventArgs(0, 0));
    }
    protected bool DeleteWebMessage(int iRefUser, string sRefMessage)
    {
        bool bDeleted = false;
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_AddDelSetMessage", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);

            XElement xmlMessage = new XElement("Message",
                            new XAttribute("RefCustomer", iRefUser),
                            new XAttribute("RefMessage", sRefMessage),
                            new XAttribute("Action", "D"));

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN", xmlMessage).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if (sXmlOut.Length > 0)
            {
                List<string> listDeleted = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Result", "Deleted");
                int iDeleted = (listDeleted.Count > 0 && listDeleted[0].Length > 0) ? int.Parse(listDeleted[0].ToString()) : 0;

                if ((sRefMessage.Length > 0 && iDeleted >= 1))
                    bDeleted = true;
            }
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return bDeleted;
    }

    protected bool GetDriStatus(string sDriValue, out string sHeadbandLabel, out string sHeabdandImage, out string sHeadbandClass)
    {
        bool isDri = false;
        sHeadbandLabel = "";
        sHeabdandImage = "";
        sHeadbandClass = "";

        switch (sDriValue.Trim().ToUpper())
        {
            case "DRI":
                panelDriStatus.Visible = true;
                imgDriStatus.ImageUrl = "~/Styles/Img/DRI-LABEL.png";
                imgDriStatus.ToolTip = "DRI";
                sHeadbandLabel = "CLIENT SOUS DRI";
                sHeabdandImage = "./Styles/Img/DRI.png";
                sHeadbandClass = "dri";
                isDri = true;
                break;
            case "DRIKO":
            case "DREKO":
                panelDriStatus.Visible = true;
                imgDriStatus.ImageUrl = "~/Styles/Img/DRI-KO-LABEL.png";
                imgDriStatus.ToolTip = "DRI/DRE KO";
                sHeadbandLabel = "CLIENT SOUS DRI/DRE KO";
                sHeabdandImage = "./Styles/Img/DRI-KO.png";
                sHeadbandClass = "dri-ko";
                isDri = true;
                break;
            case "DREOK":
            case "DRIOK":
                panelDriStatus.Visible = true;
                imgDriStatus.ImageUrl = "~/Styles/Img/DRI-OK-LABEL.png";
                imgDriStatus.ToolTip = "DRI/DRE OK";
                sHeadbandLabel = "CLIENT SOUS DRI/DRE OK";
                sHeabdandImage = "./Styles/Img/DRI-OK.png";
                sHeadbandClass = "dri-ok";
                isDri = true;
                break;
            default:
            case "NONE":
                panelDriStatus.Visible = false;
                isDri = false;
                break;
        }

        return isDri;
    }
    protected bool GetEndettementStatus(string sFlagEndettementValue, out string sHeadbandLabel, out string sHeabdandImage, out string sHeadbandClass)
    {
        bool isCS = false;
        sHeadbandLabel = "";
        sHeabdandImage = "";
        sHeadbandClass = "";

        if (sFlagEndettementValue.Trim().ToUpper() == "CS")
        {
            panelClientSurendette.Visible = true;
            imgClientSurendette.ImageUrl = "~/Styles/Img/client_Surendette.png";
            imgClientSurendette.ToolTip = "Client surendetté";
            sHeadbandLabel = "CLIENT SURENDETTE";
            sHeabdandImage = "./Styles/Img/client_Surendette.png";
            sHeadbandClass = "dri";
            isCS = true;
        }
        else if (sFlagEndettementValue.Trim().ToUpper() == "CFA")
        {
            panelClientSurendette.Visible = true;
            imgClientSurendette.ImageUrl = "~/Styles/Img/client_fragile.png";
            imgClientSurendette.ToolTip = "Client en fragilité avancée";
            sHeadbandLabel = "CLIENT EN FRAGILITE AVANCEE";
            sHeabdandImage = "./Styles/Img/client_fragile.png";
            sHeadbandClass = "dri";
            isCS = true;
        }
        else
        {
            panelClientSurendette.Visible = false;
            isCS = false;
        }

        return isCS;
    }

    protected void AddHeadBand(string sFlagEndettementValue, string sFlagDriValue)
    {
        string sHeadbandLabel = "";
        string sHeadbandImage = "";
        string sHeadbandClass = "";
        DataTable dtHeadband = new DataTable();
        dtHeadband.Columns.Add("lblTitle");
        dtHeadband.Columns.Add("imgLogoPath");

        bool isHeadbandVisible = false;

        //<asp:Image ID="imgHeadband" runat="server" style="max-height:60px;" ImageUrl="<%= %>" />
        //<asp:Label ID="lblHeadband" runat="server"></asp:Label>
        try
        {
            MasterPage mp = this.Master;
            Repeater rptHeadband = (Repeater)mp.FindControl("rptHeadband");
            Panel panelHeadband = (Panel)mp.FindControl("panelHeadband");

            if (GetEndettementStatus(sFlagEndettementValue, out sHeadbandLabel, out sHeadbandImage, out sHeadbandClass))
            {
                try
                {
                    dtHeadband.Rows.Add(new object[] { "<label style=\"color:#000;\">" + sHeadbandLabel + "</label>", sHeadbandImage });
                    //hdnDriStatus.Value = sHeadbandClass;
                    isHeadbandVisible = true;
                }
                catch (Exception ex)
                { }
            }

            if (GetDriStatus(sFlagDriValue, out sHeadbandLabel, out sHeadbandImage, out sHeadbandClass))
            {
                try
                {
                    dtHeadband.Rows.Add(new object[] { "<label>" + sHeadbandLabel + "</label>", sHeadbandImage });
                    hdnDriStatus.Value = sHeadbandClass;
                    isHeadbandVisible = true;
                }
                catch (Exception ex)
                { }
            }

            if (isHeadbandVisible)
            {
                panelHeadband.Visible = true;
                panelHeadband.Attributes.Add("class", "headband " + sHeadbandClass);
                rptHeadband.DataSource = dtHeadband;
                rptHeadband.DataBind();
            }

        }
        catch(Exception ex)
        {
    
        }
    }

    protected void btnCreateManualAlert_Click(object sender, EventArgs e)
    {
        string sRefAlert = "";

        if (CreateManualAlert(authentication.GetCurrent().sToken, _iRefCustomer.ToString(), out sRefAlert))
        {
            Response.Redirect("FastAnalysisSheet.aspx?ref=" + sRefAlert + "&client=" + _iRefCustomer.ToString(), false);
        }
        else ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Erreur', \"L'alerte n'a pas pu être créée\"); HidePanelLoading();", true);
    }
    protected bool CreateManualAlert(string sToken, string sRefCustomer, out string sRefAlert)
    {
        bool bOk = false;
        sRefAlert = "";

        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("AML.P_AddManualAlert", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                            new XElement("ALERT",
                                                new XAttribute("CashierToken", sToken),
                                                new XAttribute("RefCustomer", sRefCustomer))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if (sXmlOut.Length > 0)
            {
                List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/ALERT", "RC");
                List<string> listRefAlert = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/ALERT", "RefAlert");

                if (listRC.Count > 0 && listRC[0] == "0" && listRefAlert.Count > 0 && listRefAlert[0].Length > 0)
                {
                    bOk = true;
                    sRefAlert = listRefAlert[0];
                }
            }
        }
        catch (Exception e)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return bOk;
    }
    #endregion AML

    protected bool forceAddressConfirmation()
    {
        bool isOK = false;

        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_ForceAddressConfirmation", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@AccountNumber", SqlDbType.VarChar, 20);
            cmd.Parameters.Add("@RC", SqlDbType.Bit);

            cmd.Parameters["@AccountNumber"].Value = lblClientAccountNumber.Text.Trim();
            cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;

            cmd.ExecuteNonQuery();

            if (cmd.Parameters["@RC"].Value.ToString().Trim() == "0")
                isOK = true;

        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return isOK;
    }
    protected void btnForceAddressConfirmation_Click(object sender, EventArgs e)
    {
        if (forceAddressConfirmation())
            UpdateMessage("L'adresse du client est maintenant confirmée.");
        else UpdateMessage("Une erreur est survenue.");
    }
    protected void btnGetSummarySheet_Click(object sender, EventArgs e)
    {
        if (GetSummarySheet())
            UpdateMessage("La fiche synthèse du client a bien été envoyée.");
        else UpdateMessage("Une erreur est survenue.");
    }

    protected bool GetSummarySheet()
    {
        bool isOK = false;

        SqlConnection conn = null;
        authentication auth = authentication.GetCurrent();

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("BankSI.P_GenerateCustomerSummaryPDF_2", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@RefCustomer", SqlDbType.Int);
            cmd.Parameters.Add("@Mail", SqlDbType.VarChar, 500);
            cmd.Parameters.Add("@RC", SqlDbType.Bit);

            cmd.Parameters["@RefCustomer"].Value = _iRefCustomer;
            cmd.Parameters["@Mail"].Value = auth.sEmail;
            cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;

            cmd.ExecuteNonQuery();

            if (cmd.Parameters["@RC"].Value.ToString().Trim() == "0")
                isOK = true;

        }
        catch (Exception ex)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return isOK;
    }
    protected void BindBeneficiaries()
    {
        var beneficiaryList = BeneficiaryService.getBeneficiaryList(_iRefCustomer, true);

        rBeneficiaryList.DataSource = BeneficiaryService.ConvertBeneficiaryListToBeneficiaryViewList(beneficiaryList);
        rBeneficiaryList.DataBind();

        if (beneficiaryList.Any())
        {
            panelBeneficiaryList.Visible = true;
            panelBeneficiaryList_Empty.Visible = false;
        }
        else
        {
            panelBeneficiaryList.Visible = false;
            panelBeneficiaryList_Empty.Visible = true;
        }

    }
    protected void btnForceAnnualFee_Click(object sender, EventArgs e)
    {
        if (operation.ForceSubscriptionRenew(_iRefCustomer.ToString(), authentication.GetCurrent().sToken))
        {
            UpdateMessage("Le renouvellement a été effectué.");
            string sXmlCustomerInfos = Client.GetCustomerInformations(_iRefCustomer);
            BindCustomerInformations(sXmlCustomerInfos, true);
            upAnnualFeeStatus.Update();
            LightLock1.RefreshClientDebitStatus(sXmlCustomerInfos);
        }
        else
        {
            UpdateMessage("Une erreur est survenue.");
        }

        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "HideAnnualFeeStatusKEY", "HideAnnualFeeStatus();", true);
    }

    //ID
    #region ID
    protected void SetHistoryChangeID(int iRefCustomer)
    {
        rptHistoryChangeID.DataSource = Client_ID.GetHistoryChangeID(iRefCustomer);
        rptHistoryChangeID.DataBind();
    }
    
    
    protected void rblIpConnectionListType_SelectedIndexChanged(object sender, EventArgs e)
    {
        IpConnectionList1.Type = rblIpConnectionListType.SelectedValue;
        IpConnectionList1.iRefCustomer = _iRefCustomer;
        IpConnectionList1.BindIP();
        IpConnectionList1.BindIPLocation();
    }
    protected void btnCardReplacementFix_Click(object sender, EventArgs e)
    {
        SqlConnection conn = null;
        string sMessage = "";
        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("BankSI.P_CheckCorrectCardExchange", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@RefCustomer", SqlDbType.Int);
            cmd.Parameters.Add("@RC", SqlDbType.Bit);
            cmd.Parameters["@RefCustomer"].Value = _iRefCustomer;
            cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;
            cmd.ExecuteNonQuery();

            if (cmd.Parameters["@RC"].Value.ToString() == "0") { sMessage = "<span style=\"color:black;\">Correction appliquée. <br/>Merci de tenter de nouveau le remplacement de carte au point de vente.</span>"; }
            else { sMessage = "<span style=\"color:black;\">Aucune anomalie détectée. <br/>Merci de contacter le service technique pour une analyse approfondie.</span>"; }
        }
        catch (Exception ex)
        {
            sMessage = "Une erreur est survenue.";
        }
        finally
        {
            if (conn != null)
                conn.Close();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "CardReplacementFix", "AlertMessage('PB REMPLACEMENT CARTE', '" + sMessage + "'); HidePanelLoading();", true);
        }
    }
    protected void btnIpListExcelExport_Click(object sender, EventArgs e)
    {
        IpConnectionList1.ExportExcel();
    }
    
    protected void btnResetWEBPassword_Click(object sender, EventArgs e)
    {
        string sMessage = "";
        if (Client_ID.ResetWEBPassword(_iRefCustomer, out sMessage))
            UpdateMessage("Mot de passe WEB réinitialisé.");
        else
        {
            if (!string.IsNullOrWhiteSpace(sMessage)) { UpdateMessage(sMessage); }
            else { UpdateMessage("Une erreur est survenue"); }
        }
    }

    protected void btnSendWEBID_Click(object sender, EventArgs e)
    {
        authentication auth = authentication.GetCurrent();
        string sMessage = "";
        string sXmlIN = new XElement("ALL_XML_IN", new XElement("Customer",
                            new XAttribute("CashierToken",  auth.sToken),
                            new XAttribute("RefCustomer", _iRefCustomer.ToString()))).ToString();

        if (Client_ID.SendWEBID(sXmlIN, out sMessage))
            UpdateMessage("Identifiant WEB envoyé.");
        else
        {
            if (!string.IsNullOrWhiteSpace(sMessage)) { UpdateMessage(sMessage); }
            else { UpdateMessage("Une erreur est survenue"); }
        }
    }

    #endregion ID

    protected void btnFastAnalysisSheet_Click(object sender, EventArgs e)
    {
        tools.SetBackUrl("ClientDetails.aspx?ref=" + _iRefCustomer.ToString(), "Fiche du client", "FastAnalysisSheet");
        Response.Redirect("FastAnalysisSheet.aspx?client=" + _iRefCustomer.ToString());
    }

    //DOCS
    #region DOCS
    protected void ClientFileUploaded(object sender, EventArgs e)
    {
        clientFileManager1.UpdateClientFiles();
    }
    protected void ClientFileUploadError(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Erreur', \"La fichier n'a pas été enregistré\"); HidePanelLoading();", true);
    }

    protected void btnLoadOtherDocs_Click(object sender, EventArgs e)
    {
        if (!_bOtherDocsLoaded)
        {
            clientFileManager1.LoadData();
            _bOtherDocsLoaded = true;
            //upOtherDocs.Update();
        }
    }
    #endregion DOCS

    //KYC
    #region KYC
    protected void btnAskKycUpdate_Click(object sender, EventArgs e)
    {
        string sToken = authentication.GetCurrent(false).sToken;
        string sXmlIN = new XElement("ALL_XML_IN",
                            new XElement("Customer",
                                new XAttribute("CashierToken", sToken),
                                new XAttribute("RefCustomer", _iRefCustomer),
                                new XAttribute("KYCRequired", "1"))).ToString();

        if (SaveInformations(sXmlIN))
        {
            BindKYCValues(Client.GetCustomerInformations(_iRefCustomer));
            UpdateMessage("Demande de mise à jour des KYC du client effectuée.");
        }
    }
    protected void btnKycUpdate_Click(object sender, EventArgs e)
    {
        if(!_bEditKYC)
        {
            btnKycUpdate.Text = "annuler mise à jour";
            EditKyc();
        }
        else
        {
            btnKycUpdate.Text = "mettre à jour (par téléphone)";
            ReadOnlyKyc();
        }
    }
    protected void EditKyc()
    {
        ddlKYCnationality.Enabled = true;
        ddlKYCmat.Enabled = true;
        ddlKYCpro.Enabled = true;
        ddlKYCproPlus.Enabled = true;
        txtKYCjob.ReadOnly = false;
        ddlKYCincome.Enabled = true;
        ddlKYCCNusage.Enabled = true;
        ddlKYChab.Enabled = true;
        ddlKYCestate.Enabled = true;
        txtKYCPropertyStatusPlus.ReadOnly = false;
        cblKYCaccountUsageTag.Enabled = true;
        ddlKYCTaxResidence.Enabled = true;
        cbKYCCertificate.Disabled = false;
        cbKYCCertificate.Checked = false;
        _bEditKYC = true;
        

       
    }
    protected void ReadOnlyKyc()
    {
        ddlKYCnationality.Enabled = false;
        ddlKYCmat.Enabled = false;
        ddlKYCpro.Enabled = false;
        ddlKYCproPlus.Enabled = false;
        txtKYCjob.ReadOnly = true;
        ddlKYCincome.Enabled = false;
        ddlKYCCNusage.Enabled = false;
        ddlKYChab.Enabled = false;
        ddlKYCestate.Enabled = false;
        txtKYCPropertyStatusPlus.ReadOnly = true;
        cblKYCaccountUsageTag.Enabled = false;
        ddlKYCTaxResidence.Enabled = false;
        cbKYCCertificate.Disabled = true;
        _bEditKYC = false;
    }
    protected void GetKYCUpdateStatus(DateTime dtAdvancedKYCBlockingDate, DateTime dtPushKYCCompletionDate, DateTime dtLastKYCCompletedDate, string sBOUserPushKYC)
    {
        string sImgPath = "~/Styles/Img/";
        imgKYCUpdateStatus.Visible = true;
        imgKYCUpdateStatus.ImageUrl = sImgPath + "kyc_unknown.png";
        imgKYCUpdateStatus.ToolTip = "Statut KYC inconnu";
        panelAskKYCUpdateButton.Visible = false;
        lblCheckKYCLink.Attributes.Add("Title", imgKYCUpdateStatus.ToolTip);
        imgCheckKYCStatus.ImageUrl = sImgPath + "gray_status.png";
        cbKYCCertificate.Checked = false;
        cbTaxResidenceCertification.Checked = false;

        //TEST
        //dtPushKYCCompletionDate = DateTime.Now;
        //dtAdvancedKYCBlockingDate = DateTime.Now.AddHours(1);

        if (dtAdvancedKYCBlockingDate == DateTime.MinValue && dtPushKYCCompletionDate == DateTime.MinValue && dtLastKYCCompletedDate == DateTime.MinValue)
        {
            panelAskKYCUpdateButton.Visible = true;
            imgKYCUpdateStatus.ToolTip = "KYC jamais mis à jour";
            lblCheckKYCLink.Attributes.Add("Title", imgKYCUpdateStatus.ToolTip);
        }
        else
        {
            if (dtAdvancedKYCBlockingDate < dtLastKYCCompletedDate && dtLastKYCCompletedDate >= dtPushKYCCompletionDate)
            {
                imgKYCUpdateStatus.ImageUrl = sImgPath + "kyc_updated.png";
                imgKYCUpdateStatus.ToolTip = "KYC demandés " + ((string.IsNullOrWhiteSpace(sBOUserPushKYC)) ? "" : "par " + sBOUserPushKYC).ToString() + " le " + dtPushKYCCompletionDate.ToString("dd/MM/yyyy à HH:mm").Replace(':', 'h') + " et mis à jour le " + dtLastKYCCompletedDate.ToString("dd/MM/yyyy à HH:mm").Replace(':', 'h');
                lblCheckKYCLink.Attributes.Add("Title", imgKYCUpdateStatus.ToolTip);
                imgCheckKYCStatus.ImageUrl = sImgPath + "green_status.png";
                panelAskKYCUpdateButton.Visible = true;
                cbKYCCertificate.Checked = true;
                

                CheckFatcaAlert(true);
            }
            else if (dtAdvancedKYCBlockingDate <= dtLastKYCCompletedDate && dtLastKYCCompletedDate < dtPushKYCCompletionDate)
            {
                imgKYCUpdateStatus.ImageUrl = sImgPath + "kyc_to_update.png";
                imgKYCUpdateStatus.ToolTip = "demande de mise à jour des KYC " + ((string.IsNullOrWhiteSpace(sBOUserPushKYC)) ? "" : "par " + sBOUserPushKYC).ToString() + " le " + dtPushKYCCompletionDate.ToString("dd/MM/yyyy à HH:mm").Replace(':', 'h');
                lblCheckKYCLink.Attributes.Add("Title", imgKYCUpdateStatus.ToolTip);
                imgCheckKYCStatus.ImageUrl = sImgPath + "yellow_status.png";
            }
            else if (dtAdvancedKYCBlockingDate >= dtLastKYCCompletedDate)
            {
                imgKYCUpdateStatus.ImageUrl = sImgPath + "kyc_ko.png";
                imgKYCUpdateStatus.ToolTip = "demande de mise à jour des KYC " + ((string.IsNullOrWhiteSpace(sBOUserPushKYC)) ? "" : "par " + sBOUserPushKYC).ToString() + " le " + dtPushKYCCompletionDate.ToString("dd/MM/yyyy à HH:mm").Replace(':', 'h') + " et bloqué le " + dtAdvancedKYCBlockingDate.ToString("dd/MM/yyyy à HH:mm").Replace(':', 'h');
                lblCheckKYCLink.Attributes.Add("Title", imgKYCUpdateStatus.ToolTip);
                imgCheckKYCStatus.ImageUrl = sImgPath + "red_status.png";
            }
            else
            {
                CheckFatcaAlert(false);
                panelAskKYCUpdateButton.Visible = true;
            }
        }


        upKYCUpdateStatus.Update();
        upButtonKycUpdate.Update();
    }

    protected void CheckFatcaAlert(bool isKycOK)
    {
        string sImgPath = "~/Styles/Img/";

        if (hfFatcaAlert.Value == "1")
        {
            imgKYCUpdateStatus.ImageUrl = sImgPath + "kyc_fatca_aeoi_alert.png";
            imgKYCUpdateStatus.ToolTip = "Alerte FATCA";
            lblCheckKYCLink.Attributes.Add("Title", imgKYCUpdateStatus.ToolTip);
            imgCheckKYCStatus.ImageUrl = sImgPath + "orange_status.png";
        }
        else if (isKycOK){
            if (hfFatcaRequired.Value == "1")
            {
                imgKYCUpdateStatus.ImageUrl = sImgPath + "kyc_fatca_aeoi_to_update.png";
                imgKYCUpdateStatus.ToolTip = "FATCA/AEOI à mettre à jour";
                lblCheckKYCLink.Attributes.Add("Title", imgKYCUpdateStatus.ToolTip);
                imgCheckKYCStatus.ImageUrl = sImgPath + "violet_status.png";
            }
            else if (string.IsNullOrWhiteSpace(ddlKYCFrenchTaxOnly.SelectedValue) || string.IsNullOrWhiteSpace(ddlKYCUsResidentOrBorn.SelectedValue)
            || (ddlKYCFrenchTaxOnly.SelectedValue == "0" && string.IsNullOrWhiteSpace(hfTaxResidenceClientValues.Value)) 
            || (ddlKYCUsResidentOrBorn.SelectedValue == "1" && !rblTaxResidenceFATCA_NO.Checked && !rblTaxResidenceFATCA_YES.Checked))
            {
                imgKYCUpdateStatus.ToolTip += "<br/><b>FATCA/AEOI non demandé</b>";
                lblCheckKYCLink.Attributes.Add("Title", imgKYCUpdateStatus.ToolTip);
            }
            else
            {
                cbTaxResidenceCertification.Checked = true;
            }
        }
    }
    #endregion KYC 

    protected DataTable GetLockBankSIReasonList()
    {
        DataTable dt = new DataTable();
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("Web.P_GetLockBankSIReasonList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;
            da.Fill(dt);
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }
    protected void BindLockBankSIReasonList()
    {
        ddlLockSIReason.DataSource = GetLockBankSIReasonList();
        ddlLockSIReason.DataBind();
    }
    protected void btnSaveSabStatus_Click(object sender, EventArgs e)
    {
        string BankName = "SAB";
        if (_isCobalt) { BankName = "Cobalt"; }
        if(!SaveSabStatus(authentication.GetCurrent().sToken, _iRefCustomer.ToString(), ddlSabStatus.SelectedValue, ddlLockSIReason.SelectedValue))
            ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Erreur', \"L'état "+ BankName + " n'a pas pu être modifié.\");", true);
        else
        {
            BindCustomerInformations(Client.GetCustomerInformations(_iRefCustomer), true);
            upBankStatusImage.Update();
        }
    }
    protected bool SaveSabStatus(string sToken, string sRefCustomer, string sStatus, string sRefReason)
    {
        SqlConnection conn = null;
        bool bOk = false;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_SetClientStatus", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                            new XElement("U",
                                                new XAttribute("CashierToken", sToken),
                                                new XAttribute("RefCustomer", sRefCustomer),
                                                new XAttribute("BankStatus", sStatus),
                                                new XAttribute("RefBankSIReason", sRefReason))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if(!String.IsNullOrWhiteSpace(sXmlOut))
            {
                List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Result", "RC");
                if (listRC.Count > 0 && listRC[0] == "0")
                    bOk = true;
            }
        }
        catch (Exception e)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return bOk;
    }

    //FATCA
    #region FATCA
    private string DocUploaded = "Nous vous remercions d’avoir mis à jour vos informations personnelles !";
    private string DocNotUploadedUSBornNotUsResident = "Afin de valider votre questionnaire merci de nous transmettre via le lien de téléchargement sous le tableau votre certificat de perte de nationalité.<br/><br/>L’équipe Compte-Nickel";
    private string DocNotUploaded = "Afin de valider votre questionnaire merci de nous transmettre <a href=\"../files/fw9.pdf\" target=\"_blank\">via ce formulaire</a> votre certificat de perte de nationalité @@BLOCKINGDATE.<br/>Passé cette date, votre compte sera bloqué.<br/><br/>L’équipe Compte-Nickel";
    private string US_DocUploaded = "Nous vous remercions d’avoir mis à jour vos informations personnelles. Conformément à nos Conditions Générales et Tarifaires, les citoyens ou résidents américains, ne peuvent être clients Compte-Nickel. Nous sommes navrés de vous informer que votre compte sera clôturé sous 30 jours.<br/><br/>Pensez à nous envoyer le RIB d’une autre banque afin de procéder au transfert des fonds.<br/><br/>L’équipe Compte-Nickel";
    private string US_DocNotUploaded = "Afin de respecter nos obligations juridiques, merci de nous envoyer le certificat W-9 scanné et signé à l’adresse e-mail suivante : <a href=\"sos@compte-nickel.fr\" target=\"_blank\">sos@compte-nickel.fr</a>. Nous tenons à vous informer que les citoyens ou résidents américains ne peuvent être clients Compte-Nickel. Conformément à nos Conditions Générales et Tarifaires, votre compte sera clôturé sous 30 jours.<br/><br/>Pensez à nous envoyer le RIB d’une autre banque afin de procéder au transfert des fonds.<br/><br/>L’équipe Compte-Nickel";

    private string W8_DocNotUploaded = "Afin de valider votre questionnaire merci de nous transmettre le document W-8BEN.<br/><br/> L’équipe Compte-Nickel";
    private string W8_LostNationalityCertificate_DocNotUploaded = "Afin de valider votre questionnaire merci de nous transmettre via ce formulaire votre certificat de perte de nationalité.<br/><br/>L’équipe Compte-Nickel";

    private string AEOI_NotComplete = "Merci de renseigner pour chaque pays constituant une résidence fiscale, un code NIF (numéro d’identification fiscale) ou d’expliquer la raison de l’absence d’un code NIF.<br/> Vous pouvez être résident fiscal d’un pays sans avoir de code NIF, notamment pour les raisons suivantes : <ul><li>le pays en question n’attribue pas de code NIF de manière générale,</li><li>le pays en question ne vous a pas attribué de code NIF</li></ul>";
    private string AEOI_NotUeOrAele = "Vous venez de nous indiquer ne pas être résident(e) fiscal(e) d’au moins un pays de l’Union Européenne ou de l’un des pays de l’AELE.<br/><br/>Conformément à nos Conditions Générales et Tarifaires, vous devez avoir au moins une résidence fiscale dans l’Union Européenne ou dans l’un des pays de l’AELE. Nous sommes navrés de vous informer que votre compte sera clôturé @@CLOSEDATE.<br/><br/>Pensez à nous envoyer le RIB d’une autre banque afin de procéder au transfert des fonds.<br/><br/>L’équipe Compte-Nickel";

    protected void EditTaxResidence()
    {
        _bEditTaxResidence = true;
        panelKycTaxResidenceOld.Visible = false;
        panelKycTaxResidence_FATCA_AEOI.Visible = true;
        panelKycTaxResidence_FATCA_AEOI.Enabled = true;
        cbTaxResidenceCertification.Disabled = false;
        cbTaxResidenceCertification.Checked = false;
        cbTaxResidenceCertificateDataUse.Disabled = false;
        panelTaxResidenceListDisabled.Visible = false;
        panelFatcaDisabled.Visible = false;
    }

    protected void ReadOnlyTaxResidence()
    {
        _bEditTaxResidence = false;
        //panelKycTaxResidenceOld.Visible = true;
        //panelKycTaxResidence_FATCA_AEOI.Visible = false;
        panelKycTaxResidence_FATCA_AEOI.Enabled = false;
        cbTaxResidenceCertification.Disabled = true;
        //cbTaxResidenceCertification.Checked = true;
        cbTaxResidenceCertificateDataUse.Disabled = true;
        panelTaxResidenceListDisabled.Visible = true;
        panelFatcaDisabled.Visible = true;
    }

    protected void initTaxResidence(int _iRefCustomer)
    {
        initTaxResidenceList();
    }

    protected void initTaxResidenceList()
    {
        //panelTaxResidenceList.Visible = true;

        DataTable dtCountryList = tools.GetCountryList();
        DataTable _dtCountryList = dtCountryList.Clone();
        DataRow[] result = _dtCountryList.Select("ISO2 = 'US'");

        foreach (DataRow dr in result)
        {
            if (dr["ISO2"].ToString().Trim().ToUpper().Contains("US"))
            {
                _dtCountryList.Rows.Remove(dr);
            }
        }
        _dtCountryList.AcceptChanges();

        ddlKYCCountryList.DataSource = _dtCountryList;
        ddlKYCCountryList.DataBind();
        DataTable dtAEOI = KYC.GetAeoiCountryList();
        hfAEOIList.Value = KYC.GetAeoiCountryListToString(dtAEOI);

        //TEST
        //hfTaxResidenceClientValues.Value = "PORTUGAL;;1";
        //hfNoNifReasonValues.Value = "1;parce que";

        //upKyc_ValidateTaxResidence.Update();
        //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "KYC_TaxResidenceList", "GetCountryList();", true);
    }

    protected void BindFatcaAeoi(string sXMLCustomerInfos)
    {
        List<string> listFrenchTaxOnly = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "FrenchTaxOnly");
        List<string> listUSResidentOrBorn = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "USResidentOrBorn");
        List<string> listUSResident = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "USResident");
        List<string> listUSBorn = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "USBorn");
        List<string> listFatcaAlert = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "FatcaAlert");
        List<string> listFatcaRequired = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "FATCAAEOIRequired");
        List<string> listClientDataUseAuthorized = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "ClientDataUseAuthorized");
        List<string> listTaxResidenceList = CommonMethod.GetTags(sXMLCustomerInfos, "ALL_XML_OUT/Customer/TAX/TAX");

        List<string> listCheckFile = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "CheckFile");

        hdnDocsAlert.Value = "";
        int iCheckFile = -1;
        if (listCheckFile != null && listCheckFile.Count > 0 && int.TryParse(listCheckFile[0], out iCheckFile))
        {
            hdnDocsAlert.Value = iCheckFile.ToString();
        }
        //TEST
        //hdnDocsAlert.Value = "3";
        //FIN TEST


        hfFatcaAlert.Value = "";
        if (listFatcaAlert != null && listFatcaAlert.Count > 0 && listFatcaAlert[0] == "1")
        {
            hfFatcaAlert.Value = "1";
        }
        hfFatcaRequired.Value = "";
        if (listFatcaRequired != null && listFatcaRequired.Count > 0 && listFatcaRequired[0] == "1")
        {
            hfFatcaRequired.Value = "1";
        }

        //Test
        //hfFatcaAlert.Value = "1";
        //Fin test
        upFatca.Update();

        cbTaxResidenceCertificateDataUse.Checked = false;
        if (listClientDataUseAuthorized != null && listClientDataUseAuthorized.Count > 0 && listClientDataUseAuthorized[0] == "1")
        {
            cbTaxResidenceCertificateDataUse.Checked = true;
        }

        ddlKYCFrenchTaxOnly.SelectedItem.Selected = false;
        if (listFrenchTaxOnly != null && !string.IsNullOrWhiteSpace(listFrenchTaxOnly[0]) && ddlKYCFrenchTaxOnly.Items.FindByValue(listFrenchTaxOnly[0]) != null) {
            ddlKYCFrenchTaxOnly.Items.FindByValue(listFrenchTaxOnly[0]).Selected = true;
        }

        ddlKYCUsResidentOrBorn.SelectedItem.Selected = false;
        if (hfFatcaAlert.Value != "1")
        {
            if (listUSResidentOrBorn != null && !string.IsNullOrWhiteSpace(listUSResidentOrBorn[0]) && ddlKYCUsResidentOrBorn.Items.FindByValue(listUSResidentOrBorn[0]) != null)
            {
                ddlKYCUsResidentOrBorn.Items.FindByValue(listUSResidentOrBorn[0]).Selected = true;
            }
        }
        rblTaxResidenceFATCA_YES.Checked = false;
        rblTaxResidenceFATCA_NO.Checked = false;

        panelFATCA.Visible = false;
        if (ddlKYCUsResidentOrBorn.SelectedValue == "1")
        {
            panelFATCA.Visible = true;
            if (listUSResident != null && !string.IsNullOrWhiteSpace(listUSResident[0]) && listUSResident[0] == "1")
            {
                rblTaxResidenceFATCA_YES.Checked = true;
            }
            else if (listUSBorn != null && !string.IsNullOrWhiteSpace(listUSBorn[0]) && listUSBorn[0] == "1")
            {
                rblTaxResidenceFATCA_NO.Checked = true;
            }

            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "KYC_FATCA", "setTimeout(function () { CheckFatcaDisabled(); }, 500);", true);
        }

        hfNoNifReasonValues.Value = "";
        hfTaxResidenceClientValues.Value = "";
        for (int i = 0; i < listTaxResidenceList.Count; i++)
        {
            string TaxResidenceListValue = "";
            string TaxResidenceNoReasonValue = "";
            try
            {
                List<string> listTaxISO2 = CommonMethod.GetAttributeValues(listTaxResidenceList[i], "TAX", "ISO2");
                List<string> listTaxNIF = CommonMethod.GetAttributeValues(listTaxResidenceList[i], "TAX", "TaxID");
                List<string> listTaxNIFNoReason = CommonMethod.GetAttributeValues(listTaxResidenceList[i], "TAX", "ReasonNoTax");

                if (!string.IsNullOrWhiteSpace(tools.GetCountryNameFromISO2(listTaxISO2[0].ToString(), tools.GetCountryList())))
                {
                    TaxResidenceListValue = tools.GetCountryNameFromISO2(listTaxISO2[0].ToString(), tools.GetCountryList()) + ";" + listTaxNIF[0] + ";" + (i+1).ToString() + "|";
                }

                if(listTaxNIFNoReason.Count > 0 && !string.IsNullOrWhiteSpace(listTaxNIFNoReason[0]))
                {
                    TaxResidenceNoReasonValue = (i+1).ToString() + ";" + listTaxNIFNoReason[0] +  "|";
                }
            }
            catch (Exception ex)
            {
                TaxResidenceListValue = "";
                TaxResidenceNoReasonValue = "";
            }
            finally
            {
                hfTaxResidenceClientValues.Value += TaxResidenceListValue;
                hfNoNifReasonValues.Value += TaxResidenceNoReasonValue;
            }
        }

        if (hfTaxResidenceClientValues.Value.LastIndexOf('|') > 0 && hfTaxResidenceClientValues.Value.LastIndexOf('|') == hfTaxResidenceClientValues.Value.Length-1)
        { hfTaxResidenceClientValues.Value = hfTaxResidenceClientValues.Value.Substring(0, hfTaxResidenceClientValues.Value.Length-1); }
        if (hfNoNifReasonValues.Value.LastIndexOf('|') > 0 && hfNoNifReasonValues.Value.LastIndexOf('|') == hfNoNifReasonValues.Value.Length-1)
        { hfNoNifReasonValues.Value = hfNoNifReasonValues.Value.Substring(0, hfNoNifReasonValues.Value.Length-1); }

        if (ddlKYCFrenchTaxOnly.SelectedValue == "0")
        {
            panelTaxResidenceList.Visible = true;
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "KYC_TaxResidenceList", "GetCountryList();", true);
        }

        hfW9Filename.Value = "";
        hfW8Filename.Value = "";

        upTaxResidenceList.Update();
    }

    protected bool checkNoNifReason(string index, out string reason)
    {
        bool isOK = false;
        reason = "";
        string ListReasonValues = hfNoNifReasonValues.Value;

        if (!string.IsNullOrWhiteSpace(ListReasonValues))
        {
            string[] tTaxNoNifReasonList = ListReasonValues.Split('|');

            for (int i = 0; i < tTaxNoNifReasonList.Length; i++)
            {
                if (tTaxNoNifReasonList[i].Split(';')[0].Trim() == index)
                {
                    reason = tTaxNoNifReasonList[i].Split(';')[1].Trim();
                    isOK = true;
                }
            }
        }

        return isOK;
    }
    protected bool checkTaxResidenceFrOnly()
    {
        bool isOK = false;

        if (!string.IsNullOrWhiteSpace(ddlKYCFrenchTaxOnly.SelectedValue)) { isOK = true; }
        else { ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "KYC_TaxResidenceFrOnly", "AlertMessage('', 'Veuillez indiquer si vous êtes résident(e) fiscal(e) en France uniquement ou non.', '600');", true); }

        return isOK;
    }

    protected bool checkTaxResidenceList(out string sMessage)//out DataTable TaxResidenceList
    {
        bool isOK = false;
        sMessage = "";

        bool isUeOrAele = false;
        bool isTaxResidenceListOK = true;
        string ListValues = hfTaxResidenceClientValues.Value;
        string[] tTaxResidenceList = ListValues.Split('|');

        bool checkAEOI = true;
        string sMessageAeoi = "AEOI - Vous devez obligatoirement renseigner le code NIF des résidences fiscales suivantes :<ul>";

        DataTable TaxResidenceList = new DataTable();
        TaxResidenceList.Columns.Add("ISO2");
        TaxResidenceList.Columns.Add("TaxID");
        TaxResidenceList.Columns.Add("Reason");

        if (!string.IsNullOrWhiteSpace(ListValues))
        {
            DataTable dtAEOIList = KYC.GetAeoiCountryList();

            for (int i = 0; i < tTaxResidenceList.Length; i++)
            {
                string sISO2 = "";
                string sCountry = "";
                string sTaxID = "";
                string sReason = "";
                string sIndex = "";

                sCountry = tTaxResidenceList[i].Split(';')[0].Trim();
                sTaxID = tTaxResidenceList[i].Split(';')[1].Trim();
                sIndex = tTaxResidenceList[i].Split(';')[2].Trim();
                //sReason = tTaxResidenceList[i].Split(';')[2];
                if (!string.IsNullOrWhiteSpace(sCountry) && ddlKYCCountryList.Items.FindByText(sCountry.Trim().ToUpper()) != null)
                {
                    sISO2 = ddlKYCCountryList.Items.FindByText(sCountry.Trim().ToUpper()).Value;
                    bool checkUE = KYC.UE_Countries().Contains(sISO2);
                    bool checkAELE = KYC.AELE_Countries().Contains(sISO2);

                    if (sISO2 == "US") { isTaxResidenceListOK = false; }
                    else
                    {

                        if (KYC.UE_Countries().Contains(sISO2) || KYC.AELE_Countries().Contains(sISO2)) { isUeOrAele = true; }

                        if (!string.IsNullOrWhiteSpace(sISO2))
                        {
                            /*
                            WS_CheckTin.checkTinRequest wsCheckTinReq = new WS_CheckTin.checkTinRequest();
                            wsCheckTinReq.countryCode = sISO2;
                            wsCheckTinReq.tinNumber = sTaxID;
                            WS_CheckTin.checkTinResponse wsCheckTinRes = new WS_CheckTin.checkTinResponse();
                            bool isTinSyntaxValid = wsCheckTinRes.validSyntax;
                            bool isTinStructureValid = wsCheckTinRes.validStructure;
                            */

                            sReason = "";
                            if (sISO2 != "FR" && string.IsNullOrWhiteSpace(sTaxID) && KYC.isAeoiCountry(dtAEOIList, sISO2))
                            {
                                isTaxResidenceListOK = false;
                                checkAEOI = false;
                                sMessageAeoi += "<li>" + sCountry + "</li>";
                            }
                            else if (sISO2 != "FR" && string.IsNullOrWhiteSpace(sTaxID) && !checkNoNifReason(sIndex, out sReason))
                            {
                                isTaxResidenceListOK = false;
                            }
                            else
                            {
                                TaxResidenceList.Rows.Add(new object[] { sISO2, sTaxID, sReason });
                            }
                        }
                        else { isTaxResidenceListOK = false; }
                    }
                }
                else
                {
                    isTaxResidenceListOK = false;
                }
            }
        }
        else
        {
            isTaxResidenceListOK = false;
        }

        if (!isTaxResidenceListOK && rblTaxResidenceFATCA_YES.Checked) { isOK = true; }
        else if (isTaxResidenceListOK)
        {
            //SaveTaxResidence();
            isOK = true;
        }
        else
        {
            string sMessageDetails = "";
            if (!checkAEOI) { sMessageDetails += sMessageAeoi + "</ul></br/>"; }

            if (!string.IsNullOrWhiteSpace(sMessageDetails)) { sMessageDetails = "</br><u>Détails</u> : </br>" + sMessageDetails; }
            //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "KYC_TaxResidenceCountryList", "AlertMessage('FATCA/AEOI', 'Pays de résidence(s) non valide(s). Veuillez vérifier les informations saisies." + sMessageDetails + "');", true);
            sMessage = "<span style=\"color:red\">FATCA/AEOI : Pays de résidence(s) non valide(s). Veuillez vérifier les informations saisies.</span>" + sMessageDetails;
        }

        return isOK;
    }
    //protected bool SaveTaxResidence(bool isRedirect)
    //{
    //    bool isOK = false;
    //    try
    //    {
    //        XElement xCust = new XElement("Customer", new XAttribute("RefCustomer", _iRefCustomer.ToString()), new XAttribute("InitialRefCustomer", _iRefCustomer.ToString()), new XAttribute("CashierToken", "HMB"));

    //        if (!string.IsNullOrWhiteSpace(ddlKYCFrenchTaxOnly.SelectedValue)) { xCust.Add(new XAttribute("FrenchTaxOnly", ddlKYCFrenchTaxOnly.SelectedValue)); }
    //        if (!string.IsNullOrWhiteSpace(ddlKYCUsResidentOrBorn.SelectedValue)) { xCust.Add(new XAttribute("USResidentOrBorn", ddlKYCUsResidentOrBorn.SelectedValue)); }
    //        if (panelFATCA.Visible) { xCust.Add(new XAttribute("USBorn", (rblTaxResidenceFATCA_YES.Checked)? "1":"0")); }
    //        if (panelFATCA.Visible) { xCust.Add(new XAttribute("USResident", (rblTaxResidenceFATCA_NO.Checked) ? "1" : "0")); }
    //        xCust.Add(new XAttribute("ClientDataUseAuthorized", (cbTaxResidenceCertificateDataUse.Checked) ? "1" : "0"));
    //        if (panelTaxResidenceList.Visible)
    //        {
    //            XElement xTaxs = new XElement("Taxs");
    //            if (TaxResidenceList != null && TaxResidenceList.Rows.Count > 0)
    //            {

    //                foreach (DataRow dr in TaxResidenceList.Rows)
    //                {
    //                    xTaxs.Add(new XElement("Tax", new XAttribute("Action", "A"), new XAttribute("ISO2", dr["ISO2"].ToString()), new XAttribute("TaxId", dr["TaxID"].ToString()), new XAttribute("ReasonNoTax", dr["Reason"])));
    //                }

    //                xCust.Add(xTaxs);
    //            }
    //        }

    //        xCust.Add(new XAttribute("AllKYCCompleted", "1"));
    //        if (KYC.SetXMLCustomerInformations(new XElement("ALL_XML_IN", xCust).ToString()))
    //        {
    //            Session["HomeNotificationMessage"] = "Merci ! Vos informations ont bien été enregistrées.";
                
    //            isOK = true;
    //            string sMessage = "";
    //            /*
    //            if (panelFATCA_W9.Visible && rblTaxResidenceFATCA_YES.Checked)
    //            {
    //                //sMessage = (!string.IsNullOrEmpty(auth.KycW9FileUpdated)) ? US_DocUploaded.Replace("'", "&apos;") : US_DocNotUploaded.Replace("'", "&apos;");
    //                //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "KYC_SaveTaxResidence", "AlertMessage('', '" + sMessage + "', '600', 'Default.aspx');", true);
    //            }
    //            else if (panelTaxResidenceList.Visible && !isUeOrAele)
    //            {
    //                sMessage = AEOI_NotUeOrAele.Replace("@@CLOSEDATE", ((!string.IsNullOrEmpty(CloseDate)) ? "le " + CloseDate : "")).Replace("'", "&apos;");
    //                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "KYC_SaveTaxResidence", "AlertMessage('', '" + sMessage + "', '600', 'Default.aspx');", true);
    //            }
    //            else if (isRedirect) { Response.Redirect("Default.aspx"); }
    //            */
    //        }
    //        else
    //        {
    //            isOK = false;
    //            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "KYC_SaveTaxResidence", "AlertMessage('', 'Une erreur est survenue lors de l&apos;enregistrement de vos informations.<br/>Veuillez réessayer.');", true);
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        isOK = false;
    //        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "KYC_SaveTaxResidence", "AlertMessage('', 'Une erreur est survenue lors de l&apos;enregistrement de vos informations.<br/>Veuillez réessayer.');", true);
    //    }

    //    return isOK;
    //}
    protected void ddlKYCUsResidentOrBorn_SelectedIndexChanged(object sender, EventArgs e)
    {
        panelFATCA_W8.Visible = false;
        switch (ddlKYCUsResidentOrBorn.SelectedValue)
        {
            case "1":
                panelFATCA.Visible = true;
                break;
            default:
            case "0":
                panelFATCA.Visible = false;
                if (hfFatcaAlert.Value == "1") { panelFATCA_W8.Visible = true; }
                break;
        }
    }

    protected void ddlKYCFrenchTaxOnly_SelectedIndexChanged(object sender, EventArgs e)
    {
        switch (ddlKYCFrenchTaxOnly.SelectedValue)
        {
            case "0":
                panelTaxResidenceList.Visible = true;
                break;
            default:
            case "1":
                panelTaxResidenceList.Visible = false;
                break;
        }
    }

    protected KYC.AnswerList SetTaxResidenceValues(KYC.AnswerList _KycAnswerList)
    {
        _KycAnswerList.FatcaAlert = (hfFatcaAlert.Value == "1")? true:false;
        _KycAnswerList.ClientDataUseAuthorized = cbTaxResidenceCertificateDataUse.Checked;
        _KycAnswerList.TaxResidenceCertificate = cbTaxResidenceCertification.Checked;

        if (!string.IsNullOrWhiteSpace(ddlKYCFrenchTaxOnly.SelectedValue))
        {
            _KycAnswerList.TaxResidentFrOnly = (ddlKYCFrenchTaxOnly.SelectedValue == "1") ? true : false;

            if (_KycAnswerList.TaxResidentFrOnly == false)
            {
                bool isAeleOrUe;
                _KycAnswerList.TaxResidenceList = GetTaxResidenceListFromString(hfTaxResidenceClientValues.Value, out isAeleOrUe);
                _KycAnswerList.IsAeleOrUe = isAeleOrUe;
            }
        }

        if (!string.IsNullOrWhiteSpace(ddlKYCUsResidentOrBorn.SelectedValue))
        {
            _KycAnswerList.USResidentOrBorn = (ddlKYCUsResidentOrBorn.SelectedValue == "1") ? true : false;
            if (_KycAnswerList.USResidentOrBorn == true)
            {
                if (rblTaxResidenceFATCA_YES.Checked || rblTaxResidenceFATCA_NO.Checked)
                {
                    _KycAnswerList.USResident = rblTaxResidenceFATCA_YES.Checked;
                    _KycAnswerList.USBorn = rblTaxResidenceFATCA_NO.Checked;
                }

                _KycAnswerList.W9Filename = hfW9Filename.Value;
            }
            else if (_KycAnswerList.FatcaAlert)
            {
                _KycAnswerList.W8Filename = hfW8Filename.Value;
            }
        }

        return _KycAnswerList;
    }

    private DataTable GetTaxResidenceListFromString(string sTaxREsidenceListValues, out bool bIsUeOrAele)
    {
        bIsUeOrAele = false;
        if (!string.IsNullOrWhiteSpace(sTaxREsidenceListValues))
        {
            try
            {
                bool isTaxResidenceListOK = true;
                string ListValues = sTaxREsidenceListValues;
                string[] tTaxResidenceList = ListValues.Split('|');

                DataTable dtTaxResidenceList = new DataTable();
                dtTaxResidenceList.Columns.Add("ISO2");
                dtTaxResidenceList.Columns.Add("TaxID");
                dtTaxResidenceList.Columns.Add("Reason");

                if (!string.IsNullOrWhiteSpace(ListValues))
                {
                    for (int i = 0; i < tTaxResidenceList.Length; i++)
                    {
                        string sISO2 = "";
                        string sCountry = "";
                        string sTaxID = "";
                        string sReason = "";
                        string sIndex = "";

                        sCountry = tTaxResidenceList[i].Split(';')[0].Trim();
                        sTaxID = tTaxResidenceList[i].Split(';')[1].Trim();
                        sIndex = tTaxResidenceList[i].Split(';')[2].Trim();
                        //sReason = tTaxResidenceList[i].Split(';')[2];
                        if (!string.IsNullOrWhiteSpace(sCountry) && ddlKYCCountryList.Items.FindByText(sCountry.Trim().ToUpper()) != null)
                        {
                            sISO2 = ddlKYCCountryList.Items.FindByText(sCountry.Trim().ToUpper()).Value;
                            bool checkUE = KYC.UE_Countries().Contains(sISO2);
                            bool checkAELE = KYC.AELE_Countries().Contains(sISO2);

                            if (sISO2 == "US") { isTaxResidenceListOK = false; }
                            else
                            {

                                if (KYC.UE_Countries().Contains(sISO2) || KYC.AELE_Countries().Contains(sISO2)) { bIsUeOrAele = true; }

                                if (!string.IsNullOrWhiteSpace(sISO2))
                                {
                                    /*
                                    WS_CheckTin.checkTinRequest wsCheckTinReq = new WS_CheckTin.checkTinRequest();
                                    wsCheckTinReq.countryCode = sISO2;
                                    wsCheckTinReq.tinNumber = sTaxID;
                                    WS_CheckTin.checkTinResponse wsCheckTinRes = new WS_CheckTin.checkTinResponse();
                                    bool isTinSyntaxValid = wsCheckTinRes.validSyntax;
                                    bool isTinStructureValid = wsCheckTinRes.validStructure;
                                    */

                                    sReason = "";
                                    if (sISO2 != "FR" && string.IsNullOrWhiteSpace(sTaxID) && !checkNoNifReason(sIndex, out sReason))
                                    {
                                        isTaxResidenceListOK = false;
                                    }
                                    else
                                    {
                                        dtTaxResidenceList.Rows.Add(new object[] { sISO2, sTaxID, sReason });
                                    }
                                }
                                else { isTaxResidenceListOK = false; }
                            }
                        }
                        else
                        {
                            isTaxResidenceListOK = false;
                        }
                    }

                    
                }
                else
                {
                    isTaxResidenceListOK = false;
                }

                /*if (!isTaxResidenceListOK && rblTaxResidenceFATCA_YES.Checked) { isOK = true; }
                else if (isTaxResidenceListOK)
                {
                    isOK = true;
                }*/

                if (isTaxResidenceListOK)
                {
                    if(ddlKYCUsResidentOrBorn.SelectedValue == "1" && rblTaxResidenceFATCA_YES.Checked && !string.IsNullOrWhiteSpace(txtUsTIN.Text))
                    {
                        dtTaxResidenceList.Rows.Add(new object[] { "US", txtUsTIN.Text.Trim(), "" });
                    }

                    return dtTaxResidenceList;
                }

            }
            catch(Exception ex)
            {
                return null;
            }
        }

        return null;
    }

    protected void btnEditTaxResidence_Click(object sender, EventArgs e)
    {
        Button btn = (Button)sender;

        if (btn.ID == "btnEditTaxResidence_Hidden")
        {
            EditTaxResidence();
            panelCancelFatcaAeoi.Visible = true;
            panelUpdateFatcaAeoi.Visible = false;
        }
        else
        {
            ReadOnlyTaxResidence();
            panelCancelFatcaAeoi.Visible = false;
            panelUpdateFatcaAeoi.Visible = true;
        }
            
    }

    protected bool checkFatcaAeoi(out string sMessage)
    {
        var isOK = true;
        sMessage = "";

        if(ddlKYCFrenchTaxOnly.SelectedValue != "1" && !checkTaxResidenceList(out sMessage))
        {
            isOK = false;
        }

        return isOK;
    }

    #endregion FATCA

    protected void btnInitNickelChromeData_Click(object sender, EventArgs e)
    {
        InitNickelChrome();
        panelNickelChromeInformation.Visible = true;
        panelNickelChromeOrderResult.Visible = false;
        panelNickelChromeReorder.Visible = false;
        upNickelChromeInformations.Update();
        upNickelChromeReorder.Update();

        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "KYC_TaxResidenceCountryList", "showNickelChrome();", true);
    }

    protected void btnNickelChromeReorderDone_Click(object sender, EventArgs e)
    {
        panelNickelChromeReorder.Visible = false;
        panelNickelChromeOrderResult.Visible = true;
        upNickelChromeReorder.Update();
    }

    protected void btnNickelChromeReorder_Click(object sender, EventArgs e)
    {
        OrderCard();

        panelNickelChromeReorder.Visible = false;
        panelNickelChromeOrderResult.Visible = true;
        panelNickelChromeInformation.Visible = false;
        upNickelChromeInformations.Update();
        upNickelChromeReorder.Update();
    }

    protected void btnNickelChromeShowReorder_Click(object sender, EventArgs e)
    {
        panelNickelChromeReorder.Visible = true;
        panelNickelChromeReorderPossible.Visible = false;
        upNickelChromeInformations.Update();
        upNickelChromeReorder.Update();
    }

    protected void OrderCard()
    {
        if (_NickelChrome != null && _iRefCustomer > 0)
        {
            try
            {
                if (Card.OrderNickelChromeCard(_iRefCustomer, _iRefCustomer, _NickelChrome.NickelChromeCardName, _NickelChrome.NickelChromeCardTAG, false))
                {
                    lblNickelChromeOrderMessage.ForeColor = System.Drawing.Color.Green;
                    lblNickelChromeOrderMessage.Text = "Commande effectuée";
                }
                else
                {
                    lblNickelChromeOrderMessage.ForeColor = System.Drawing.Color.Red;
                    lblNickelChromeOrderMessage.Text = "La commande a échoué";
                }
            }
            catch (Exception ex)
            {
                lblNickelChromeOrderMessage.ForeColor = System.Drawing.Color.Red;
                lblNickelChromeOrderMessage.Text = "Une erreur est survenue ("+ex.Message+")";
            }
        }
        else
        {
            lblNickelChromeOrderMessage.ForeColor = System.Drawing.Color.Red;
            lblNickelChromeOrderMessage.Text = "Une erreur est survenue";
        }
    }

    protected void btnReopenAccount_Click(object sender, EventArgs e)
    {
        authentication auth = authentication.GetCurrent();

        string sMessage = "";
        string sPopupMessage = "";
        //sMessage = "Le compte n'est pas à l'état 4 <BR /> <B> test </B>";
        if (Client.ReopenClientAccount(_iRefCustomer, auth.sToken, out sMessage))
        {
            btnReopenAccount.Visible = false;
            upReopenAccount.Update();
            sPopupMessage = "<span style='color:green'>Compte rouvert avec succès</span>";
        }
        else { sPopupMessage = "<span style='color:red'>" + sMessage + "</span>"; }

        string sRedirectUrl = HttpUtility.JavaScriptStringEncode(HttpContext.Current.Request.Url.AbsoluteUri);
        string sJsToCall = "AlertMessage('Rouverture de compte',\"" + HttpUtility.JavaScriptStringEncode(sPopupMessage) + "\", '" + sRedirectUrl + "');";
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), sJsToCall, true);
    }

    protected void btnNickelChromeCancel_Click(object sender, EventArgs e)
    {
        if (_NickelChrome != null && !string.IsNullOrWhiteSpace(_NickelChrome.NickelChromeRefOrder) && Card.CancelPersonnalizedCardOrder(_NickelChrome.NickelChromeRefOrder))
        {

            CRETools.CREValues _CREValues = new CRETools.CREValues();

            _CREValues.sOperationTAG = CRETools.OperationTAG._REMB_CARTE_CHROME_.ToString();
            _CREValues.sTreatmentStatus = CRETools.TreatmentStatus.NotTreated;
            _CREValues.sCompte1 = _sAccountNumber;
            _CREValues.mMontant1 = "30,00";
            _CREValues.sRedirect = "ClientDetails.aspx?ref=" + _iRefCustomer.ToString();
            string sJWT = CRETools.GetJWTFromCREValues(_CREValues);
            Response.Redirect("BoCobalt_OpDiverseSaisie.aspx?cretreatment=" + sJWT);

            /*
            //TRAITEMENT CRE
            CRETools.CREValues _CREValuesRes = new CRETools.CREValues();
            if(CRETools.GetCREValuesFromJWT(sJWT, out _CREValuesRes))
            {
                //APRES TRAITEMENT DU CRE SI OK :
                _CREValuesRes.sTreatmentStatus = CRETools.TreatmentStatus.Successful;
                CRETools.Redirect(_CREValuesRes);
            }
            */
        }
    }

    protected void lnkSABCobaltOpeComparison_Click(object sender, EventArgs e)
    {
        SABCobaltOpeComparison1.RefCustomer = _iRefCustomer;
        SABCobaltOpeComparison1.AccountNumber = _sAccountNumber;
        SABCobaltOpeComparison1.IBAN = hdnIBAN.Value;
        SABCobaltOpeComparison1.Visible = true;
        upSABCobaltOpeComparison.Update();

        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), new Guid().ToString(), "ShowSABCobaltOpeComparisonPopup();", true);
    }

    protected void btnBindIPLocation_Click(object sender, EventArgs e)
    {
        IpConnectionList1.BindIPLocation();
    }
}
