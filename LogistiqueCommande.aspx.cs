﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using XMLMethodLibrary;
using System.Text.RegularExpressions;


public partial class Logistique : System.Web.UI.Page
{
    [Serializable]
    protected class CommandSearchFilters
    {
        public string sCommandNumber { get; set; }
        public string sCommandQuantity { get; set; }
        public string sCommandDate { get; set; }

        public CommandSearchFilters()
        {
        }
        public CommandSearchFilters(string sCommandNumber, string sCommandQuantity, string sCommandDate)
        {
            this.sCommandNumber = sCommandNumber;
            this.sCommandQuantity = sCommandQuantity;
            this.sCommandDate = sCommandDate;
            
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        string sToken = authentication.GetCurrent(false).sToken;
        string sXml = new XElement("ALL_XML_IN", new XElement("CommandSearch",
            new XAttribute("CommandNumber", txtCommandNumber.Text),
            new XAttribute("CommandQuantity", txtCommandQuantity.Text),
            new XAttribute("CommandDate", txtCommandDate.Text),
            new XAttribute("TOKEN", sToken))).ToString();
        DataTable dt = getCommandListFromXML(sXml);
        
        BindCommandList(dt);
    }

    protected static DataTable getCommandListFromXML(string sXml)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("RefCommand");
        dt.Columns.Add("CommandNumber");
        dt.Columns.Add("CommandQuantity");
        dt.Columns.Add("CommandDate");
        dt.Columns.Add("VisualType");
        dt.Columns.Add("ValidationDate");

        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_SearchCardCommand", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);
            //cmd.Parameters.Add("@ReturnSelect", SqlDbType.Bit);

            cmd.Parameters["@IN"].Value = sXml;

            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
            //cmd.Parameters["@ReturnSelect"].Value = 0;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            // pour des tests
            /*string sXmlOut = "<ALL_XML_OUT>"
                + "<Command RefCommand=\"1\" CommandNumber=\"PO0001\" CommandQuantity =\"80000\" CommandDate=\"21/06/2017\" VisualType=\"Standard\" ValidationDate=\"04/06/2017\" />"
                + "<Command RefCommand=\"2\" CommandNumber=\"PO00011\" CommandQuantity =\"8000\" CommandDate=\"21/07/2017\" VisualType=\"Banzai\" ValidationDate=\"Rejetée\" />"
                + "<Command RefCommand=\"3\" CommandNumber=\"PO00012\" CommandQuantity =\"180000\" CommandDate=\"21/08/2017\" VisualType=\"Standard\" ValidationDate=\"04/08/2017\" />"
                + "<Command RefCommand=\"4\" CommandNumber=\"PO0002\" CommandQuantity =\"8880000\" CommandDate=\"21/09/2017\" VisualType=\"Chrome\" ValidationDate=\"En attente\" />"
                + "<Command RefCommand=\"5\" CommandNumber=\"PO0006\" CommandQuantity =\"80000\" CommandDate=\"21/10/2017\" VisualType=\"orange\" ValidationDate=\"04/11/2017\" />"
                + "</ALL_XML_OUT>";*/
            if (sXmlOut.Length > 0)
            {
                List<string> listCommand = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/Command");

                for (int i = 0; i < listCommand.Count; i++)
                {
                    List<string> listRef = CommonMethod.GetAttributeValues(listCommand[i], "Command", "RefCommand");
                    List<string> listCommandNumber = CommonMethod.GetAttributeValues(listCommand[i], "Command", "CommandNumber");
                    List<string> listCommandQuantity = CommonMethod.GetAttributeValues(listCommand[i], "Command", "CommandQuantity");
                    List<string> listCommandDate = CommonMethod.GetAttributeValues(listCommand[i], "Command", "CommandDate");
                    List<string> listVisualType = CommonMethod.GetAttributeValues(listCommand[i], "Command", "VisualType");
                    List<string> listValidationDate = CommonMethod.GetAttributeValues(listCommand[i], "Command", "ValidationDate");
                    

                    DataRow row = dt.NewRow();
                    row["RefCommand"] = (listRef.Count > 0) ? listRef[0] : "";
                    row["CommandNumber"] = (listCommandNumber.Count > 0) ? listCommandNumber[0] : "";
                    try
                    {
                        int iTmp = int.Parse(listCommandQuantity[0]);
                        row["CommandQuantity"] = (listCommandQuantity.Count > 0) ? String.Format("{0:### ### ### ###}", iTmp).Trim() : "";
                    }
                    catch(Exception e)
                    {
                        row["CommandQuantity"] = (listCommandQuantity.Count > 0) ? listCommandQuantity[0] : "";
                    }
                    
                    row["CommandDate"] = (listCommandDate.Count > 0) ? listCommandDate[0] : "";
                    switch(listVisualType[0])
                    {
                        case "VISTAND":
                            row["VisualType"] = "Standard";
                            break;
                        default:
                            row["VisualType"] = "";
                            break;
                    }
                    row["ValidationDate"] = (listValidationDate.Count > 0) ? listValidationDate[0] : "";                    

                    dt.Rows.Add(row);
                }
            }
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }

    protected void BindCommandList(DataTable dt)
    {
        gvCommandSearchResult.DataSource = dt;
        gvCommandSearchResult.DataBind();
        lblRowCount.Text = dt.Rows.Count.ToString();

        panelGrid.Visible = true;
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HiddenField hdnRefCommand = (HiddenField)e.Row.FindControl("hdnRefCommand");
            Label lblCommandDate = (Label)e.Row.FindControl("lblCommandDate");
            
            if (hdnRefCommand != null && lblCommandDate != null)
            {
                //if (Regex.Match(lblCommandDate.Text, "[0-9]{2}/[0-9]{2}/[0-9]{4}") == true)
                {
                    e.Row.Attributes.Add("onclick", "ShowCommandDetails('" + hdnRefCommand.Value + "')");
                    /*HiddenField hdn = (HiddenField)e.Row.FindControl("hdnServiceTag");
                    if (hdn != null && hdn.Value == "4")
                        e.Row.Attributes.Add("onmouseover", "this.style.cursor = 'pointer';this.style.backgroundColor = '#1dc39c'");
                    else */e.Row.Attributes.Add("onmouseover", "this.style.cursor = 'pointer';this.style.backgroundColor = '#ff9966'");
                    e.Row.Attributes.Add("onmouseout", "this.style.cursor = 'auto';this.style.backgroundColor = '#fff'");
                }
            }
        }
    }

    protected void btnConfirmCommand_Click(object sender, EventArgs e)
    {
        SqlConnection conn = null;
        string sMessage = "";

        try
        {
            string sToken = authentication.GetCurrent(false).sToken;
            
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_CreateCardCommand", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);
            //cmd.Parameters.Add("@ReturnSelect", SqlDbType.Bit);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN", new XElement("Command",
                new XAttribute("CommandNumber", txtCommandNumberCreation.Text),
                new XAttribute("CommandQuantity", txtNombreCarteCommand.Text),
                new XAttribute("PrixUnitaire", txtPrixUnitaire.Text.Replace(",", ".")),
                new XAttribute("VisualType", ddlVisuel.SelectedItem.Value),
                new XAttribute("TOKEN", sToken))).ToString();

            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
            //cmd.Parameters["@ReturnSelect"].Value = 0;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            // pour des tests
            /*string sXmlOut = "<ALL_XML_OUT>"
                + "<Command RC=\"-1\" />"
                + "</ALL_XML_OUT>";*/
            if (sXmlOut.Length > 0)
            {
                List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Command", "RC");

                if (listRC.Count > 0 && listRC[0] == "0")
                {
                    sMessage = "Création de commande ";
                    List<string> listNumCommand = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Command", "CommandNumber");
                    if (listNumCommand.Count > 0)
                        sMessage += listNumCommand[0];
                    sMessage += " réussie";
                }
                else
                {
                    sMessage = "Création de commande échouée (CR " + listRC[0] + ")";
                }
            }
        }
        catch (Exception ex)
        {
            sMessage = ex.Message;
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('', \"" + sMessage + "\", null, null);", true);
    }
}
