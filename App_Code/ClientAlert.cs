﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using XMLMethodLibrary;

/// <summary>
/// Description résumée de ClientAlert
/// </summary>
/// 
[Serializable]
public class ClientAlert
{
    public static DataTable getCustomerLockList(int iRefCustomer)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("Active");
        dt.Columns.Add("Reason");
        dt.Columns.Add("LockDate");
        dt.Columns.Add("UnlockDate");
        dt.Columns.Add("LockBoUser");
        dt.Columns.Add("UnlockBoUser");

        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("BankSI.P_GetCustomerLockInformation", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@RC", SqlDbType.Bit);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                            new XElement("Customer", 
                                                new XAttribute("RefCustomer", iRefCustomer.ToString()))).ToString();

            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
            cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if (sXmlOut.Length > 0)
            {
                List<string> listLock = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/Customer/LOCKLIST/LOCK");

                for (int i = 0; i < listLock.Count; i++)
                {
                    List<string> listActive = CommonMethod.GetAttributeValues(listLock[i], "LOCK", "Active");
                    List<string> listReason = CommonMethod.GetAttributeValues(listLock[i], "LOCK", "Reason");
                    List<string> listLockDate = CommonMethod.GetAttributeValues(listLock[i], "LOCK", "LockDate");
                    List<string> listUnLockDate = CommonMethod.GetAttributeValues(listLock[i], "LOCK", "UnLockDate");
                    List<string> listLockBOUser = CommonMethod.GetAttributeValues(listLock[i], "LOCK", "LockBOUser");
                    List<string> listUnLockBOUser = CommonMethod.GetAttributeValues(listLock[i], "LOCK", "UnLockBOUser");

                    DataRow row = dt.NewRow();
                    row["Active"] = (listActive.Count > 0) ? listActive[0] : "";
                    row["Reason"] = (listReason.Count > 0) ? listReason[0] : "";
                    row["LockDate"] = (listLockDate.Count > 0) ? listLockDate[0] : "";
                    row["UnlockDate"] = (listUnLockDate.Count > 0) ? listUnLockDate[0] : "";
                    row["LockBoUser"] = (listLockBOUser.Count > 0) ? listLockBOUser[0] : "";
                    row["UnlockBoUser"] = (listUnLockBOUser.Count > 0) ? listUnLockBOUser[0] : "";

                    dt.Rows.Add(row);
                }
            }
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }


        return dt;
    }

}