﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using XMLMethodLibrary;

/// <summary>
/// Summary description for AvisCS
/// </summary>
[Serializable]
public class AvisCS
{
    public AvisCS()
    {
    }

    public static bool SetAvisCSStatus(string sRefAvisCS, string sToken, string sStatus, string sReason)
    {
        bool bOk = false;
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("bankSI.P_SetAvisCSStatus", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);

            XElement xelem = new XElement("AvisCS",
                                                new XAttribute("Ref", sRefAvisCS),
                                                new XAttribute("CashierToken", sToken),
                                                new XAttribute("Status", sStatus)
                                                );
            if (!String.IsNullOrWhiteSpace(sReason))
                xelem.Add(new XAttribute("RejectedReason", sReason));

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                            xelem
                                            ).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if (sXmlOut.Length > 0)
            {
                List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/AvisCS", "RC");
                if (listRC.Count > 0 && listRC[0] == "0")
                    bOk = true;
            }
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return bOk;
    }

    public static string GetAvisCSDetails(string sRefAvisCS, string sToken)
    {
        string sXmlOut = "";
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("BankSI.P_GetAvisCSDetails", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@ReturnSelect", SqlDbType.Bit);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                            new XElement("AvisCS",
                                            new XAttribute("CashierToken", sToken),
                                            new XAttribute("Ref", sRefAvisCS))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
            cmd.Parameters["@ReturnSelect"].Value = 0;

            cmd.ExecuteNonQuery();

            sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            //<ALL_XML_OUT><Customer RC="77001" RetMessage="L'accès au compte est restreint. Veuillez contacter la DCRCI"/></ALL_XML_OUT>

            List<string> lsRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/AvisCS", "RC");
            List<string> lsRetMessage = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/AvisCS", "RetMessage");
            string sRC = (lsRC.Count > 0 && !string.IsNullOrWhiteSpace(lsRC[0])) ? lsRC[0].ToString().Trim() : "";
            string sMessage = (lsRetMessage.Count > 0 && !string.IsNullOrWhiteSpace(lsRetMessage[0])) ? lsRetMessage[0].ToString().Trim() : "";
            string sCurrentPage = "GarnishmentSearch.aspx";//HttpContext.Current.Request.RawUrl.Replace("/","");

            if (sRC == "77001")
            {
                tools.CheckUnauthorizedAccess(sRC, sMessage, sCurrentPage);
            }
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return sXmlOut;
    }

    public static bool SetAvisCSDetails(string sXmlIn)
    {
        bool bOk = false;
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("BankSI.P_SetAvisCS", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);

            cmd.Parameters["@IN"].Value = sXmlIn;
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if (sXmlOut.Length > 0)
            {
                List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/AvisCS", "RC");
                if (listRC.Count > 0 && listRC[0] == "0")
                    bOk = true;
            }
        }
        catch (Exception e)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return bOk;
    }

    public static bool GetAvisCSFileFromGoogle(string sRefCustomer)
    {
        SqlConnection conn = null;
        bool bOk = false;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("Google.P_GetCSRScanFilesForCustomer", conn);
            cmd.CommandTimeout = 120;
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@RegistrationCode", SqlDbType.VarChar, 300);
            cmd.Parameters.Add("@RefCustomer", SqlDbType.Int);
            cmd.Parameters.Add("@RC", SqlDbType.Int);

            cmd.Parameters["@RegistrationCode"].Value = DBNull.Value;
            cmd.Parameters["@RefCustomer"].Value = sRefCustomer;
            cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;

            cmd.ExecuteNonQuery();

            if (cmd.Parameters["@RC"] != null && cmd.Parameters["@RC"].Value.ToString() == "0")
                bOk = true;
        }
        catch (Exception e)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return bOk;
    }
}