﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using XMLMethodLibrary;

/// <summary>
/// Description résumée de authentication
/// </summary>
/// 
[Serializable]
public class authentication
{
    public string sRef { get; set; }
    public string sToken { get; set; }
    public string sLastName { get; set; }
    public string sFirstName { get; set; }
    public string sTagProfile { get; set; }
    public string sLastConnectionDate { get; set; }
    public bool bChangePassword { get; set; }
    public bool bFirstConnection { get; set; }
    public string sEmail { get; set; }
    public string sCulture { get; set; }
    public CheckUser cuCheckUser { get; set; }

    [Serializable]
    public class CheckUser
    {
        public bool bIsManager { get; set; }
        public int iLevel { get; set; }
    }

    public static CheckUser GetCheckUserInfos(string sToken)
    {
        CheckUser _checkUser = new CheckUser();
        //[Registration].[P_GetCheckUsersManaged]

        string sXmlOut = "";
        if (GetUserInfos(sToken, out sXmlOut))
        {
            List<string> lsGroupLevel = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/UsersInfos", "iCheckUserGroupLevel");
            List<string> lsManager = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/UsersInfos", "bManager");

            _checkUser.iLevel = (lsGroupLevel != null && lsGroupLevel.Count > 0 && lsGroupLevel[0].Trim() == "2") ? 2 : 1;
            _checkUser.bIsManager = (lsManager != null && lsManager.Count > 0 && lsManager[0].Trim() == "1") ? true : false;

            return _checkUser;
        }

        return null;
    }
    public static DataTable GetUserCheckListByLevel(string sToken, string sLevel)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("LastName");
        dt.Columns.Add("FirstName");
        dt.Columns.Add("Ref");
        dt.Columns.Add("CheckLevel");
        //dt.Columns.Add("Manager");
        //dt.Columns.Add("SysAdmin");

        string sXmlOut = "";

        if (GetUserInfos(sToken, out sXmlOut) && !string.IsNullOrWhiteSpace(sXmlOut))
        {
            try
            {
                List<string> lsUserCheckList = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/UsersList/User");
                List<string> lsUserGroupLevel = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/UsersInfos", "iCheckUserGroupLevel");
                List<string> lsUserManager = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/UsersInfos", "bManager");
                List<string> lsRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/UsersInfos", "RC");

                for (int i = 0; i < lsUserCheckList.Count; i++)
                {
                    List<string> lsLastName = CommonMethod.GetAttributeValues(lsUserCheckList[i], "User", "LastName");
                    List<string> lsFirstName = CommonMethod.GetAttributeValues(lsUserCheckList[i], "User", "FirstName");
                    List<string> lsRef = CommonMethod.GetAttributeValues(lsUserCheckList[i], "User", "RefUser");
                    List<string> lsCheckLevel = CommonMethod.GetAttributeValues(lsUserCheckList[i], "User", "iCheckLevel");
                    //List<string> lsManager = CommonMethod.GetAttributeValues(lsUserCheckList[i], "UserCheck", "bManager");
                    //List<string> lsSysAdmin = CommonMethod.GetAttributeValues(lsUserCheckList[i], "UserCheck", "bSysAdmin");

                    if (sLevel == lsCheckLevel[0])
                    {
                        dt.Rows.Add(new object[] { lsLastName[0], lsFirstName[0], lsRef[0], lsCheckLevel[0] });
                    }
                }
            }
            catch (Exception ex) { }
        }

        return dt;
    }
    private static bool GetUserInfos(string sToken, out string sXmlOut)
    {
        bool isOK = false;

        sXmlOut = "";
        SqlConnection conn = null;
        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["PARTNER"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("[Registration].[P_GetCheckUsersManaged]", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            //<ALL_XML_IN><UsersInfos TOKEN = "0X26F295DA3CD07A61AD906D542725BED103718F8B"/></ ALL_XML_IN >

            cmd.Parameters.Add("@sXmlIn", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@sXmlOut", SqlDbType.VarChar, -1);

            cmd.Parameters["@sXmlIn"].Value = new XElement("ALL_XML_IN", new XElement("UsersInfos", new XAttribute("TOKEN", sToken))).ToString();
            cmd.Parameters["@sXmlOut"].Direction = ParameterDirection.Output;
            cmd.ExecuteNonQuery();

            sXmlOut = cmd.Parameters["@sXmlOut"].Value.ToString();

            //<ALL_XML_OUT><UsersInfos RC="0" iCheckUserGroupLevel="2" bManager="1"/>
            List<string> lsRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/UsersInfos", "RC");
            if (lsRC != null && lsRC.Count > 0 && lsRC[0].Trim() == "0")
            {
                isOK = true;
            }
        }
        catch (Exception ex)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return isOK;
    }

    public CountersLastRefresh clrCountersLastRefresh { get; set; }

    [Serializable]
    public class CountersLastRefresh
    {
        private int DefaultDelayInMinuteBeforeRefresh = 10;
        public int DelayInMinuteBeforeRefresh
        {
            get { return DefaultDelayInMinuteBeforeRefresh; }
            set { DefaultDelayInMinuteBeforeRefresh = value; }
        }

        public DateTime dtLastNbNonEvaluateSubscription { get; set; }
        public DateTime dtLastPendingRegistrationToCheckN1 { get; set; }
        public DateTime dtLastPendingRegistrationToCheckN2 { get; set; }
        public DateTime dtLastInProgressOnfido { get; set; }

        public int iNbNonEvaluateSubscription { get; set; }
        public int iPendingRegistrationToCheckN1 { get; set; }
        public int iPendingRegistrationToCheckN2 { get; set; }
        public int iInProgressOnfido { get; set; }
    }

    public static bool CheckForCounterRefresh(string counterName, authentication auth)
    {
        bool isOK = false;
        DateTime dtNow = DateTime.Now;

        DateTime dtToCheck = DateTime.MinValue;
        int iCounterValue = -1;

        try
        {

            int DelayToCheck = (auth.clrCountersLastRefresh != null && auth.clrCountersLastRefresh.DelayInMinuteBeforeRefresh > 0) ? auth.clrCountersLastRefresh.DelayInMinuteBeforeRefresh : 10;

            if (auth.clrCountersLastRefresh != null)
            {
                switch (counterName)
                {
                    case "PendingRegistrationToCheckN1":
                        iCounterValue = auth.clrCountersLastRefresh.iPendingRegistrationToCheckN1;
                        dtToCheck = auth.clrCountersLastRefresh.dtLastPendingRegistrationToCheckN1;
                        break;
                    case "PendingRegistrationToCheckN2":
                        iCounterValue = auth.clrCountersLastRefresh.iPendingRegistrationToCheckN2;
                        dtToCheck = auth.clrCountersLastRefresh.dtLastPendingRegistrationToCheckN2;
                        break;
                    case "NbNonEvaluateSubscription":
                        iCounterValue = auth.clrCountersLastRefresh.iNbNonEvaluateSubscription;
                        dtToCheck = auth.clrCountersLastRefresh.dtLastNbNonEvaluateSubscription;
                        break;
                    case "InProgressOnfido":
                        iCounterValue = auth.clrCountersLastRefresh.iInProgressOnfido;
                        dtToCheck = auth.clrCountersLastRefresh.dtLastInProgressOnfido;
                        break;
                }

                if (dtToCheck == DateTime.MinValue || iCounterValue < 0 || dtToCheck.AddMinutes(DelayToCheck) < dtNow)
                {
                    isOK = true;
                }

            }
            else
            {
                auth.clrCountersLastRefresh = new CountersLastRefresh();
                isOK = true;
            }
        }
        catch(Exception ex)
        {
            isOK = false;
        }

        return isOK;
    }

    public static authentication GetCurrent()
    {
        try
        {
            authentication registr = (authentication)System.Web.HttpContext.Current.Session["Authentication"];
            string test = "";
            if (registr != null)
                return registr;
            else
            {
                if (System.Web.HttpContext.Current.Session["SessionClosed"] != null)
                    test = System.Web.HttpContext.Current.Session["SessionClosed"].ToString();

                tools.SetRedirectCookie(System.Web.HttpContext.Current.Request.Url.PathAndQuery);
                System.Web.HttpContext.Current.Response.Redirect("authentication.aspx", true);
            }
        }
        catch (Exception ex)
        {
            System.Web.HttpContext.Current.Session.RemoveAll();
            System.Web.HttpContext.Current.Response.Redirect("authentication.aspx", true);
        }

        return null;
    }

    public static authentication GetCurrent(bool redirect)
    {
        authentication registr = (authentication)System.Web.HttpContext.Current.Session["Authentication"];

        if (registr != null)
            return registr;
        else if (redirect)
        {
            tools.SetRedirectCookie(System.Web.HttpContext.Current.Request.Url.PathAndQuery);
            System.Web.HttpContext.Current.Response.Redirect("authentication.aspx", true);
        }

        return null;
    }
}