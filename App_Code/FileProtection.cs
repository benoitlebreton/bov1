﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.IO;
using System.Web.SessionState;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Xml.Linq;
using System.Web.Hosting;

/// <summary>
/// Summary description for FileProtection
/// </summary>
namespace CustomFileHandler.Handlers
{
    [Serializable]
    public class FileProtectionHandler : IHttpHandler, IRequiresSessionState
    {
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        public void ProcessRequest(HttpContext context)
        {
            string sPageName = "";
            string sHttp = context.Request.Url.AbsoluteUri.Substring(0,context.Request.Url.AbsoluteUri.IndexOf("://"));
            string requestedFile = context.Server.MapPath(context.Request.Url.AbsolutePath);
            string[] tUrl;
            

            if (context.Request.Url.AbsoluteUri.IndexOf('?') != -1)
            {
                sPageName = context.Request.Url.AbsoluteUri.Substring(context.Request.Url.AbsoluteUri.LastIndexOf('/') + 1, (context.Request.Url.AbsoluteUri.IndexOf('?')) - (context.Request.Url.AbsoluteUri.LastIndexOf('/') + 1));
            }
            else
            {

                sPageName = context.Request.Url.AbsoluteUri.Substring(context.Request.Url.AbsoluteUri.LastIndexOf('/') + 1);

                tUrl = context.Request.Url.AbsoluteUri.Replace(sHttp + "://", "").Split('/');

                if (tUrl.Length > 2 && tUrl[1].ToLower() == "ocr")
                {
                    authentication auth = authentication.GetCurrent(false);
                    int iRefCustomer = -1;

                    //sCurrentNameDirectory = tUrl[3].ToString();
                    if(auth != null && int.TryParse(auth.sRef, out iRefCustomer) && iRefCustomer > 0)
                    {
                        SendContentTypeAndFile(context, requestedFile);
                    }
                    else if (context.Request.Params["token"] != null && checkToken(context.Request.Params["token"].ToString()))
                    {
                        SendContentTypeAndFile(context, requestedFile);
                    }
                    else
                    {
                        context.Response.Redirect("/error.aspx", true);
                    }
                }
                else
                {
                    SendContentTypeAndFile(context, requestedFile);
                }
            }
        }

        private HttpContext SendContentTypeAndFile(HttpContext context, String strFile)
        {
            try 
            {
                FileInfo fileinfo = new FileInfo(strFile);
                if (fileinfo.Exists)
                {
                    context.Response.ContentType = GetContentType(strFile);
                    context.Response.TransmitFile(strFile);
                    context.Response.End();
                }
                else
                {
                    context.Response.StatusCode = 404;
                    context.Response.End();
                }
            }
            catch(Exception ex)
            {

            }

            return context;
        }

        private string GetContentType(string filename)
        {
            // used to set the encoding for the reponse stream
            string res = null;
            FileInfo fileinfo = new FileInfo(filename);

            if (fileinfo.Exists)
            {
                switch (fileinfo.Extension.Remove(0, 1).ToLower())
                {
                    case "pdf":
                        res = "application/pdf";
                        break;
                    case "odt":
                        res = "application/vnd.oasis.opendocument.text";
                        break;
                    case "doc":
                        res = "application/msword";
                        break;
                    case "docx":
                        res = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                        break;
                    case "jpeg":
                        res = "image/jpg";
                        break;
                    case "jpg":
                        res = "image/jpg";
                        break;
                }

                return res;
            }

            return null;
        }

        protected string GetCustomerNameDirectory(int iRefCustomer)
        {
            string sNameDirectory = "";
            SqlConnection conn = null;
            try
            {
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
                conn.Open();

                SqlCommand cmd = new SqlCommand("web.P_GetCustomerWebDirectory", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
                cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);
                cmd.Parameters.Add("@ReturnSelect", SqlDbType.Bit);

                cmd.Parameters["@ReturnSelect"].Value = 0;
                cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN", new XElement("Web", new XAttribute("RefCustomer", iRefCustomer))).ToString();
                cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

                cmd.ExecuteNonQuery();

                //<ALL_XML_OUT><Web Directory="18B14A520F1F508CBA4FF7913F717D5EEF21C2F6" /></ALL_XML_OUT>

                string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

                List<string> lsWebDirectory = XMLMethodLibrary.CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Web", "Directory");
                if (lsWebDirectory.Count > 0)
                    sNameDirectory = lsWebDirectory[0].ToString();

            }
            catch (Exception ex)
            {
                sNameDirectory = "";
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }

            return sNameDirectory;
        }

        protected bool checkToken(string sToken)
        {
            bool isOK = false;
            if(sToken.Trim().Length > 0)
            {
                SqlConnection conn = null;
                try
                {
                    conn = new SqlConnection(ConfigurationManager.ConnectionStrings["HW"].ConnectionString);
                    conn.Open();

                    SqlCommand cmd = new SqlCommand("HW.[P_CheckServiceToken]", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
                    cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);
                    cmd.Parameters.Add("@ReturnSelect", SqlDbType.Bit);

                    cmd.Parameters["@ReturnSelect"].Value = 0;
                    cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN", new XElement("H", new XAttribute("TOKEN", sToken))).ToString();
                    cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    //<ALL_XML_OUT><H RC="0" HWKind="PC" SerialNumber="XXXXXXXX" RefHardware="6293"/></ALL_XML_OUT>

                    string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

                    List<string> lsRC = XMLMethodLibrary.CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/H", "RC");
                    if (lsRC.Count > 0 && lsRC[0].ToString().Trim() == "0")
                        isOK = true;

                }
                catch (Exception ex)
                {
                    isOK = false;
                }
                finally
                {
                    if (conn != null)
                        conn.Close();
                }

            }

            return isOK;
        }
    }
}