﻿/// <summary>
/// Description résumée de BooleanConverter
/// </summary>
public static class BooleanConverter
{
    /// <summary>
    /// Retourne un boolean en fonction d'une chaine
    /// </summary>
    /// <param name="value"></param>
    /// <returns>True si value = true, 1 vrai </returns>
    public static bool ToBoolean(this string value)
    {
        switch (value.ToLower())
        {
            case "true":
            case "1":
            case "vrai":
                return true;
            default:
                return false;
        }

        
    }
}