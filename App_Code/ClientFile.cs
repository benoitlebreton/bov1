﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Hosting;
using System.Xml.Linq;
using XMLMethodLibrary;

/// <summary>
/// Summary description for ClientFile
/// </summary>
/// 
[Serializable]
public static class ClientFile
{
    public static DataTable GetDocumentType()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("TAG");
        dt.Columns.Add("Label");
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_GetDocumentCategoryList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@ReturnSelect", SqlDbType.Bit);

            cmd.Parameters["@IN"].Value = "<ALL_XML_IN><Doc /></ALL_XML_IN>";
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
            cmd.Parameters["@ReturnSelect"].Value = 0;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if (sXmlOut.Length > 0)
            {
                List<string> listDocs = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/Doc");

                for (int i = 0; i < listDocs.Count; i++)
                {
                    List<string> listTag = CommonMethod.GetAttributeValues(listDocs[i], "Doc", "DocCategoryTAG");
                    List<string> listLabel = CommonMethod.GetAttributeValues(listDocs[i], "Doc", "DocCategoryDescription");

                    if (listTag.Count > 0 && listLabel[0].Length > 0)
                        dt.Rows.Add(new object[] { listTag[0], listLabel[0] });
                }
            }
        }
        catch (Exception e)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        dt.DefaultView.Sort = "Label ASC";
        return dt.DefaultView.ToTable();
    }

    public static void RemoveUntreatedDocs(string sDirectoryPath, int iExpirationMinute)
    {
        string[] arFiles = Directory.GetFiles(sDirectoryPath);

        for (int i = 0; i < arFiles.Length; i++)
        {
            string sFilePath = Path.Combine(sDirectoryPath, arFiles[i]);

            int iCompare = DateTime.Compare(File.GetCreationTime(sFilePath).AddMinutes(iExpirationMinute), DateTime.Now);

            if (iCompare < 0)
                File.Delete(sFilePath);
        }
    }

    public static bool SaveFileCustomerDir(int iRefCustomer, string sSourcePath, string sTempFileName, string sOriginalFileName, bool bOverwrite, bool bDeleteOriginal, out int iRC, out string sErrorMessage)
    {
        iRC = 0;
        sErrorMessage = "";
        bool bSaved = false;

        string sCustomerDirPath = ConfigurationManager.AppSettings["NickelDocPath"].ToString();
        string sCustomerWebDirName = tools.GetCustomerWebDirectoryName(iRefCustomer);

        if (!bOverwrite && File.Exists(HostingEnvironment.MapPath("~" + sCustomerDirPath + "/" + sCustomerWebDirName + "/" + sOriginalFileName)))
        {
            iRC = 9995;
            sErrorMessage = "Fichier déjà existant";
        }
        else
        {
            try
            {
                string sSource = HostingEnvironment.MapPath("~" + sSourcePath + sTempFileName);
                string sDest = HostingEnvironment.MapPath("~" + sCustomerDirPath + "/" + sCustomerWebDirName + "/" + sOriginalFileName);
                File.Copy(sSource, sDest, true);

                if (bDeleteOriginal)
                    File.Delete(sSource);

                bSaved = true;
            }
            catch (Exception e)
            {
                iRC = 9998;
                sErrorMessage = "Erreur enregistrement fichier + (source = "+ HostingEnvironment.MapPath("~" + sSourcePath + sTempFileName) + " - destination = "+ HostingEnvironment.MapPath("~" + sCustomerDirPath + "/" + sCustomerWebDirName + "/" + sOriginalFileName) + ")";
            }
        }

        return bSaved;
    }

    public static bool SaveFileDB(string sToken, int iRefCustomer, string sContentType, string sFileName, string sDocCategoryTAG, string sDescription)
    {
        bool bSaved = false;
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("Web.P_CustomerStoreDocumentsUpload", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@sXmlIn", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@sXmlOut", SqlDbType.VarChar, 8000);

            cmd.Parameters["@sXmlIn"].Value = new XElement("ALL_XML_IN",
                                            new XElement("Doc",
                                                new XAttribute("CashierToken", sToken),
                                                new XAttribute("RefCustomer", iRefCustomer.ToString()),
                                                new XAttribute("ContentType", sContentType),
                                                new XAttribute("FileName", sFileName),
                                                new XAttribute("DocCategoryTAG", sDocCategoryTAG),
                                                new XAttribute("Description", sDescription)
                                                )).ToString();
            cmd.Parameters["@sXmlOut"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@sXmlOut"].Value.ToString();

            if (sXmlOut.Length > 0)
            {
                List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Doc", "RC");
                if (listRC.Count > 0 && listRC[0] == "0")
                    bSaved = true;
                else
                {
                    tools.LogToFile("XMLIN =" + cmd.Parameters["@sXmlIn"].Value + '\n' + "XMLOUT = " + sXmlOut, "SaveFileDB");
                }
            }
        }
        catch (Exception e)
        {
            if (conn != null)
                conn.Close();
        }

        return bSaved;
    }

    public static bool SaveFileTmp(HttpPostedFile httpPostedFile, out string sOriginalFileName, out string sFileName, out string sContentType, out int iRC, out string sErrorMessage)
    {
        iRC = 0;
        sErrorMessage = "";
        sFileName = "";
        sOriginalFileName = "";
        sContentType = "";
        bool bSaved = false;

        if (httpPostedFile != null)
        {
            try
            {
                sOriginalFileName = CleanFileName(httpPostedFile.FileName);

                string sNewOriginalFileName = sOriginalFileName.Substring(0, sOriginalFileName.LastIndexOf('.'));
                if (sNewOriginalFileName.Length > 40)
                    sNewOriginalFileName = sNewOriginalFileName.Substring(0, 40);
                sNewOriginalFileName = String.Format("{0}_{1}_{2}{3}", sNewOriginalFileName, Guid.NewGuid(), DateTime.Now.ToString("ddMMyyyyHHmmss"), sOriginalFileName.Substring(sOriginalFileName.LastIndexOf('.')));
                sOriginalFileName = sNewOriginalFileName;

                sFileName = Guid.NewGuid().ToString() + httpPostedFile.FileName.Substring(httpPostedFile.FileName.LastIndexOf('.'));
                sContentType = httpPostedFile.ContentType;

                string sFileSavePath = Path.Combine(HttpContext.Current.Server.MapPath("~/UploadTmp"), sFileName);

                // Save the uploaded file to "UploadedFiles" folder
                httpPostedFile.SaveAs(sFileSavePath);
                bSaved = true;
            }
            catch (Exception ex)
            {
                iRC = 9998;
                sErrorMessage = "Erreur enregistrement fichier";
                sFileName = "";
            }
        }
        else
        {
            iRC = 9999;
            sErrorMessage = "Aucun fichier reçu";
        }

        return bSaved;
    }

    public static string CleanFileName(string sFileName)
    {
        return ReplaceDiacritics(sFileName).Replace(' ', '_').Replace("%", "");
    }
    private static string ReplaceDiacritics(this string source)
    {
        string sourceInFormD = source.Normalize(NormalizationForm.FormD);

        var output = new StringBuilder();
        foreach (char c in sourceInFormD)
        {
            UnicodeCategory uc = CharUnicodeInfo.GetUnicodeCategory(c);
            if (uc != UnicodeCategory.NonSpacingMark)
                output.Append(c);
        }

        return (output.ToString().Normalize(NormalizationForm.FormC));
    }

    public static bool GetFileFromCloudStorage(string sToken, int iRefCustomer, string sPath, string sFileName)
    {
        bool bDownload = false;

        if (File.Exists(HostingEnvironment.MapPath(sPath + "/" + sFileName)))
            bDownload = true;
        else
        {
            SqlConnection conn = null;

            try
            {
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
                conn.Open();

                SqlCommand cmd = new SqlCommand("Web.P_CustomerStoreDocumentsDownload", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@sXmlIn", SqlDbType.VarChar, 8000);
                cmd.Parameters.Add("@sXmlOut", SqlDbType.VarChar, 8000);

                cmd.Parameters["@sXmlIn"].Value = new XElement("ALL_XML_IN",
                                                    new XElement("Doc",
                                                        new XAttribute("CashierToken", sToken),
                                                        new XAttribute("RefCustomer", iRefCustomer.ToString()),
                                                        new XAttribute("FileName", sFileName))).ToString();
                cmd.Parameters["@sXmlOut"].Direction = ParameterDirection.Output;

                cmd.ExecuteNonQuery();

                string sXmlOut = cmd.Parameters["@sXmlOut"].Value.ToString();

                if (sXmlOut.Length > 0)
                {
                    List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Doc", "RC");

                    if (listRC.Count > 0 && listRC[0] == "0")
                        bDownload = true;
                }
            }
            catch (Exception e)
            {

            }
        }

        return bDownload;
    }

    public static bool SetDocumentChecked(authentication auth, int iRefCustomer, string sFileName, bool isAccepted)
    {
        bool isOK = false;
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("Web.P_SetCustomerStoreDocument", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                            new XElement("Doc",
                                                new XAttribute("CashierToken", auth.sToken),
                                                new XAttribute("RefCustomer", iRefCustomer.ToString()),
                                                new XAttribute("FileName", sFileName),
                                                new XAttribute("Accepted", (isAccepted)? "1" :"2")
                                                )).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if (sXmlOut.Length > 0)
            {
                List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Result", "RC");
                if (listRC.Count > 0 && listRC[0] == "0")
                    isOK = true;
            }
        }
        catch (Exception e)
        {
            if (conn != null)
                conn.Close();
        }

        return isOK;
    }
}