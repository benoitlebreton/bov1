﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using XMLMethodLibrary;

[Serializable]
public class CRETools
{
    private static string sSecretForJWT = "VaP&!H9jNzh&Rn!GQu^s7CJBd5pT+5R?aH@bfYD2";

    public enum OperationTAG
    {
        _REMB_CARTE_CHROME_
    }
    public enum TreatmentStatus
    {
        NotTreated,
        Canceled,
        Successful,
        Failed
    }

    [Serializable]
    public class CREValues {
       
        public string sOperationTAG { get; set; }
        public string mMontant1 { get; set; }
        public bool bMontant1Editable { get; set; }
        public string sCompte1 { get; set; }
        public bool bCompte1Editable { get; set; }
        public string sLibelle1 { get; set; }
        public bool bLibelle1Editable { get; set; }
        public string sLibelle2 { get; set; }
        public bool bLibelle2Editable { get; set; }
        public string sCompte2 { get; set; }
        public bool bCompte2Editable { get; set; }
        public string mMontant2 { get; set; }
        public bool bMontant2Editable { get; set; }
        public string sDate1_AAAAMMJJ { get; set; }
        public bool bDate1Editable { get; set; }
        public string sDivers { get; set; }
        public bool bDiversEditable { get; set; }
        public string sUserComment { get; set; }
        public bool bUserCommentEditable { get; set; }
        public string sRedirect { get; set; }
        public TreatmentStatus sTreatmentStatus { get; set; }
        public int iRefCREOrder { get; set; }
		public string sDivers2 { get; set; }
        public bool bDivers2Editable { get; set; }
    }

    public static string GetJWTFromCREValues(CREValues _CREValues)
    {
        try
        {
            return JWTTools.setJWT(_CREValues, sSecretForJWT);
        }
        catch (Exception ex)
        {
            return "";
        }
    }

    public static bool GetCREValuesFromJWT(string sJWT, out CREValues _CREValues)
    {
        bool isOK = false;
        _CREValues = new CREValues();

        JObject jJsonData = null;
        if(JWTTools.getJWTValues(sJWT, sSecretForJWT, out jJsonData))
        {
            try
            {
                dynamic jsonData = jJsonData;
                _CREValues.sOperationTAG = jsonData.sOperationTAG;
                _CREValues.mMontant1 = jsonData.mMontant1;
                _CREValues.bMontant1Editable = jsonData.bMontant1Editable;
                _CREValues.sCompte1 = jsonData.sCompte1;
                _CREValues.sLibelle1 = jsonData.sLibelle1;
                _CREValues.sLibelle2 = jsonData.sLibelle2;
                _CREValues.sCompte2 = jsonData.sCompte2;
                _CREValues.mMontant2 = jsonData.mMontant2;
                _CREValues.bMontant2Editable = jsonData.bMontant2Editable;
                _CREValues.sDivers = jsonData.sDivers;
                _CREValues.sDate1_AAAAMMJJ = jsonData.sDate1_AAAAMMJJ;
                _CREValues.bDate1Editable = jsonData.bDate1Editable;
                _CREValues.sDivers = jsonData.sDivers;
                _CREValues.bDiversEditable = jsonData.bDiversEditable;
                _CREValues.sUserComment = jsonData.sUserComment;
                _CREValues.bUserCommentEditable = jsonData.bUserCommentEditable;
                _CREValues.sRedirect = jsonData.sRedirect;
                _CREValues.sTreatmentStatus = jsonData.sTreatmentStatus;
                _CREValues.iRefCREOrder = jsonData.iRefCREOrder;

                if (Enum.IsDefined(typeof(OperationTAG), _CREValues.sOperationTAG) && 
                    !string.IsNullOrWhiteSpace(_CREValues.mMontant1) && !string.IsNullOrWhiteSpace(_CREValues.sCompte1))
                {
                    isOK = true;
                }
            }
            catch(Exception ex)
            {
                isOK = false;
            }
        }

        return isOK;
    }

    public static void Redirect(CREValues _CREValues)
    {
        if (Enum.IsDefined(typeof(TreatmentStatus), _CREValues.sTreatmentStatus)) 
        {
            HttpContext.Current.Response.Redirect(_CREValues.sRedirect +"&treatmentresult=" + GetJWTFromCREValues(_CREValues));
        }
    }

    public enum CREOrderStatus
    {
        Unknown,
        InProgress,
        Cancelled,
        Refused,
        Failed,
        Done
    }
    public static bool GetCREOrderStatus(string sToken, int iRefCREOrder, out CREOrderStatus creOrderStatus)
    {
        bool isOK = false;
        creOrderStatus = CREOrderStatus.Unknown;
        /*
            SET @sXmlIn =
            '<ALL_XML_IN>
                <CreOrder
	                TOKEN="DBTREATMENT"
	                iRefCREOrder = "89"/>
            </ALL_XML_IN>'

            EXEC @iRC = [CRE].[P_GetCREOrderStatus] @sXmlIn, @sXmlOut OUTPUT
            SELECT @iRC, @sXmlOut, CAST(@sXmlOut AS XML)

            <ALL_XML_OUT>
              <CreOrder RC="0" CRE_ORDER_STATUS="5" CRE_REF="134094" CRE_CR="0" />
            </ALL_XML_OUT>

            CRE_ORDER_STATUS
            1 : cre demandé sans validation
            2 : cre demandé en attente de validation
            3 : cre annulé
            4 : cre rejeté
            5 : cre envoyé

            Demande en cours / en attente :
            CRE_ORDER_STATUS = 1
            CRE_ORDER_STATUS = 2

            Demande KO / refusée
            CRE_ORDER_STATUS = 3
            CRE_ORDER_STATUS = 4
            CRE_ORDER_STATUS = 5 + CRE_CR <> 0

            Demande OK
            CRE_ORDER_STATUS = 5 + CRE_CR = 0
         */

        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRE"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("CRE.P_GetCREOrderStatus", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@sXmlIn", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@sXmlOut", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@iRC", SqlDbType.Int);

            cmd.Parameters["@sXmlIn"].Value = new XElement("ALL_XML_IN",
                                            new XElement("CreOrder",
                                                new XAttribute("TOKEN", sToken),
                                                new XAttribute("iRefCREOrder", iRefCREOrder.ToString()))).ToString();
            cmd.Parameters["@sXmlOut"].Direction = ParameterDirection.Output;
            cmd.Parameters["@iRC"].Direction = ParameterDirection.ReturnValue;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@sXmlOut"].Value.ToString();
            string sRC = cmd.Parameters["@iRC"].Value.ToString(); ;

            if(sRC.Trim() == "0")
            {
                List<string> lsRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/CreOrder", "RC");
                List<string> lsOrderStatus = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/CreOrder", "CRE_ORDER_STATUS");
                List<string> lsRef = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/CreOrder", "CRE_REF");
                List<string> lsCR = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/CreOrder", "CRE_CR");

                int iCREOrderStatus;
                int iCR;
                if(lsRC.Count > 0 && lsRC[0].Trim() == "0" && 
                    lsOrderStatus.Count > 0 && int.TryParse(lsOrderStatus[0],out iCREOrderStatus) && 
                    lsCR.Count > 0 && int.TryParse(lsCR[0], out iCR))
                {
                    isOK = true;

                    switch (iCREOrderStatus)
                    {
                        case 0:
                            creOrderStatus = CREOrderStatus.Unknown;
                            break;
                        case 2:
                        case 1:
                            creOrderStatus = CREOrderStatus.InProgress;
                        
                            break;
                        case 3:
                            creOrderStatus = CREOrderStatus.Cancelled;
                            break;
                        case 4:
                            creOrderStatus = CREOrderStatus.Refused;
                            break;
                        case 5:
                            if(iCR == 0) { creOrderStatus = CREOrderStatus.Done; }
                            else { creOrderStatus = CREOrderStatus.Failed; }
                            break;
                    }

                }
            }

        }
        catch (Exception e)
        {
            isOK = false;
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return isOK;
    }

    public static bool CheckRights(string sToken)
    {
        bool isOK = false;

        /*
            DECLARE @sXmlIn VARCHAR(max)
	        DECLARE @sXmlOut VARCHAR(max)

	        SET @sXmlIn = '<ALL_XML_IN><UserCREFeaturesAutorizations TOKEN = "DBTREATMENT"/></ALL_XML_IN>'

	        EXEC [NoBank].[CRE].[P_GetUserCREFeaturesAutorizations] @sXmlIn, @sXmlOut OUTPUT
	        SELECT @sXmlOut, CAST(@sXmlOut AS XML)
         */
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRE"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("CRE.P_GetUserCREFeaturesAutorizations", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@sXmlIn", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@sXmlOut", SqlDbType.VarChar, -1);
            cmd.Parameters["@sXmlIn"].Value = new XElement("ALL_XML_IN",
                                                new XElement("UserCREFeaturesAutorizations",
                                                    new XAttribute("TOKEN", sToken))).ToString();
            cmd.Parameters["@sXmlOut"].Direction = ParameterDirection.Output;
            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@sXmlOut"].Value.ToString();
            List<string> lsFeatureOrder = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/CREFeaturesOrder/CREFeature/Feature");
            List<string> lsFeatureValidation = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/CREFeaturesValidation/CREFeature/Feature");

            if (lsFeatureOrder.Count > 0 || lsFeatureValidation.Count > 0)
            {
                isOK = true;
            }
        }
        catch (Exception e)
        {
            isOK = false;
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return isOK;
    }

    // nouvelle version CRE
    /*  <CRE>
        <Infos sOperationTAG="_FRAIS_ATD_" sCodeOperation="+AT" sOperationDescription="Prise de Frais sur Avis à Tiers Détenteur (ATD)" bCREValidationRequested="0" sValidationGroup="" iValidationRefUser="0" sValidationUserName="" sValidationUserFirstName="" sValidationUserMail="" />
        <Default>
          <Order mMontant1_Min="1.00" mMontant1_Max="100.00" bCREValidationRequested="0" />
          <Order mMontant1_Min="101.00" mMontant1_Max="500.00" bCREValidationRequested="1" />
        </Default>
        <Specific iRefUser="351">
          <Order mMontant1_Min="1.00" mMontant1_Max="200.00" bCREValidationRequested="0" />
          <Order mMontant1_Min="201.00" mMontant1_Max="500.00" bCREValidationRequested="1" />
          <Validation mMontant1_Min="1.00" mMontant1_Max="100.00" />
        </Specific>
      </CRE> */

    public static bool GetCREInfos(string sCRE, out string sOperationTAG, out string sCodeOperation, out string sOperationDescription)
    {
        sOperationTAG = "";
        sCodeOperation = "";
        sOperationDescription = "";

        List<string> listInfosCRE = CommonMethod.GetTags(sCRE, "CRE/Infos");
        if (listInfosCRE.Count > 0)
        {
            List<string> listOperationTAG = CommonMethod.GetAttributeValues(listInfosCRE[0], "Infos", "sOperationTAG");
            List<string> listCodeOperation = CommonMethod.GetAttributeValues(listInfosCRE[0], "Infos", "sCodeOperation");
            List<string> listOperationDescription = CommonMethod.GetAttributeValues(listInfosCRE[0], "Infos", "sOperationDescription");

            if (listOperationTAG.Count > 0)
                sOperationTAG = listOperationTAG[0];

            if (listCodeOperation.Count > 0)
                sCodeOperation = listCodeOperation[0];

            if (listOperationDescription.Count > 0)
                sOperationDescription = listOperationDescription[0];

            return true;
        }
        else
            return false;
    }

    public static bool GetCREValidationTextInfos(string sCRE, out bool bIsValidationRequested, out string sValidationText)
    {
        string sValidationGroup = "";
        string sValidationUserName = "";
        string sValidationUserMail = "";

        sValidationText = "";
        bIsValidationRequested = true;
        List<string> listInfosCRE = CommonMethod.GetTags(sCRE, "CRE/Infos");
        if (listInfosCRE.Count > 0)
        {
            List<string> listCREValidationRequested = CommonMethod.GetAttributeValues(listInfosCRE[0], "Infos", "bCREValidationRequested");
            List<string> listValidationGroup = CommonMethod.GetAttributeValues(listInfosCRE[0], "Infos", "sValidationGroup");
            List<string> listValidationGroupName = CommonMethod.GetAttributeValues(listInfosCRE[0], "Infos", "sValidationLabelGroup");
            List<string> listValidationUserName = CommonMethod.GetAttributeValues(listInfosCRE[0], "Infos", "sValidationUserName");
            List<string> listValidationUserFirstName = CommonMethod.GetAttributeValues(listInfosCRE[0], "Infos", "sValidationUserFirstName");
            List<string> listValidationUserMail = CommonMethod.GetAttributeValues(listInfosCRE[0], "Infos", "sValidationUserMail");

            if (listCREValidationRequested.Count > 0)
                bIsValidationRequested = (listCREValidationRequested[0] == "1");

            if (listValidationGroupName.Count > 0 && listValidationGroupName[0].Length > 0)
                sValidationGroup = listValidationGroupName[0];
            else if (listValidationGroup.Count > 0)
                sValidationGroup = listValidationGroup[0];

            if (listValidationUserFirstName.Count > 0 && listValidationUserFirstName[0].Length > 0)
                sValidationUserName = listValidationUserFirstName[0] + " ";

            if (listValidationUserName.Count > 0 && listValidationUserName[0].Length > 0)
                sValidationUserName += listValidationUserName[0];

            if (listValidationUserMail.Count > 0)
                sValidationUserMail = listValidationUserMail[0];

            // traitement des valeurs defaults
            string sValidationDefaut = "";
            listInfosCRE.Clear();
            listInfosCRE = CommonMethod.GetTags(sCRE, "CRE/Default");

            if (listInfosCRE.Count > 0)
            {
                List<string> listCREValidationRequestedOrder = CommonMethod.GetAttributeValues(listInfosCRE[0], "Default/Order", "bCREValidationRequested");
                List<string> listMontant1Min = CommonMethod.GetAttributeValues(listInfosCRE[0], "Default/Order", "mMontant1_Min");
                List<string> listMontant1Max = CommonMethod.GetAttributeValues(listInfosCRE[0], "Default/Order", "mMontant1_Max");

                for (int idx = 0; idx < Math.Min(listMontant1Min.Count, Math.Min(listMontant1Max.Count, listCREValidationRequestedOrder.Count)); idx++)
                {
                    if (sValidationDefaut.Length == 0)
                        sValidationDefaut = "<span style=\"font-weight:bold\">Règles de validation (défaut)</span><br/><ul>";
                    sValidationDefaut += "<li>Entre " + listMontant1Min[idx].Replace('.', ',') + "€ et " + listMontant1Max[idx].Replace('.', ',') + "€ : ";
                    if (listCREValidationRequestedOrder[idx] == "1")
                    {
                        if (sValidationUserName.Length > 0)
                            sValidationDefaut += "Validation requise par l'utilisateur <span style=\"font-weight:bold\">" + sValidationUserName + "</span>";
                        else
                            sValidationDefaut += "Validation requise par le groupe d'utilisateur <span style=\"font-weight:bold\">" + sValidationGroup + "</span>";
                    }
                    else
                        sValidationDefaut += "Pas de validation requise";
                    sValidationDefaut += "</li>";
                }
                if (sValidationDefaut.Length > 0)
                    sValidationDefaut += "</ul>";
            }

            // traitement des valeurs defaults
            string sValidationSpecific = "";
            listInfosCRE.Clear();
            listInfosCRE = CommonMethod.GetTags(sCRE, "CRE/Specific");

            if (listInfosCRE.Count > 0)
            {
                List<string> listCREValidationRequestedOrder = CommonMethod.GetAttributeValues(listInfosCRE[0], "Specific/Order", "bCREValidationRequested");
                List<string> listMontant1Min = CommonMethod.GetAttributeValues(listInfosCRE[0], "Specific/Order", "mMontant1_Min");
                List<string> listMontant1Max = CommonMethod.GetAttributeValues(listInfosCRE[0], "Specific/Order", "mMontant1_Max");

                for (int idx = 0; idx < Math.Min(listMontant1Min.Count, Math.Min(listMontant1Max.Count, listCREValidationRequestedOrder.Count)); idx++)
                {
                    if (sValidationSpecific.Length == 0)
                        sValidationSpecific = "<span style=\"font-weight:bold\">Règles de validation (utilisateur)</span><br/><ul>";
                    sValidationSpecific += "<li>Entre " + listMontant1Min[idx].Replace('.', ',') + "€ et " + listMontant1Max[idx].Replace('.', ',') + "€ : ";
                    if (listCREValidationRequestedOrder[idx] == "1")
                    {
                        if (sValidationUserName.Length > 0)
                            sValidationSpecific += "Validation requise par l'utilisateur <span style=\"font-weight:bold\">" + sValidationUserName + "</span>";
                        else
                            sValidationSpecific += "Validation requise par le groupe d'utilisateur <span style=\"font-weight:bold\">" + sValidationGroup + "</span>";
                    }
                    else
                        sValidationSpecific += "Pas de validation requise";
                    sValidationSpecific += "</li>";
                }
                if (sValidationSpecific.Length > 0)
                    sValidationSpecific += "</ul>";
            }

            if (sValidationSpecific.Length > 0)
                sValidationText += sValidationSpecific;
            else if (sValidationDefaut.Length > 0)
                sValidationText += sValidationDefaut;
            
            if (sValidationText.Length == 0 && bIsValidationRequested)
            {
                if (sValidationUserName.Length > 0)
                    sValidationText = "Validation requise par l'utilisateur <span style=\"font-weight:bold\">" + sValidationUserName + "</span>";
                else
                    sValidationText = "Validation requise par le groupe d'utilisateur <span style=\"font-weight:bold\">" + sValidationGroup + "</span>";
            }
            return true;
        }
        else
            return false;
    }

    public static bool GetCREValidationInfos(string sCRE, out string sValidationGroup, out int iValidationRefUser, out string sValidationUserName, out string sValidationUserMail)
    {
        sValidationGroup = "";
        iValidationRefUser = 0;
        sValidationUserName = "";
        sValidationUserMail = "";

        List<string> listInfosCRE = CommonMethod.GetTags(sCRE, "CRE/Infos");
        if (listInfosCRE.Count > 0)
        {
            List<string> listValidationGroup = CommonMethod.GetAttributeValues(listInfosCRE[0], "Infos", "sValidationGroup");
            List<string> listValidationRefUser = CommonMethod.GetAttributeValues(listInfosCRE[0], "Infos", "iValidationRefUser");
            List<string> listValidationUserName = CommonMethod.GetAttributeValues(listInfosCRE[0], "Infos", "sValidationUserName");
            List<string> listValidationUserFirstName = CommonMethod.GetAttributeValues(listInfosCRE[0], "Infos", "sValidationUserFristName");
            List<string> listValidationUserMail = CommonMethod.GetAttributeValues(listInfosCRE[0], "Infos", "sValidationUserMail");

            if (listValidationGroup.Count > 0)
                sValidationGroup = listValidationGroup[0];

            if (listValidationRefUser.Count > 0 )
                iValidationRefUser = int.Parse(listValidationRefUser[0]);

            if (listValidationUserFirstName.Count > 0 && listValidationUserFirstName[0].Length > 0)
                sValidationUserName = listValidationUserFirstName[0] + " ";

            if (listValidationUserName.Count > 0 && listValidationUserName[0].Length > 0)
                sValidationUserName += listValidationUserName[0];

            if (listValidationUserMail.Count > 0)
                sValidationUserMail = listValidationUserMail[0];

            return true;
        }
        else
            return false;
    }

    public static bool IsValidationRequested(string sCRE, string sMontant1, string sMontant2)
    {
        bool bReturn = true;
        decimal dMontantMin;
        decimal dMontantMax;
        decimal dMontant;
        List<string> listInfosCRE = CommonMethod.GetTags(sCRE, "CRE/Infos");

        if (listInfosCRE.Count > 0)
        {
            List<string> listCREValidationRequested = CommonMethod.GetAttributeValues(listInfosCRE[0], "Infos", "bCREValidationRequested");

            if (listCREValidationRequested.Count > 0)
                bReturn = (listCREValidationRequested[0] != "0");
        }

        listInfosCRE.Clear();
        if (sMontant1.Length > 0 && (Decimal.TryParse(sMontant1.Replace('.', ','), NumberStyles.Currency, new CultureInfo("fr-FR"), out dMontant)))
        {
            string sTemplate = "mMontant1";
            dMontant = decimal.Round(dMontant, 2, MidpointRounding.AwayFromZero);
            listInfosCRE.Clear();
            listInfosCRE = CommonMethod.GetTags(sCRE, "CRE/Default");

            if (listInfosCRE.Count > 0)
            {
                List<string> listCREValidationRequested = CommonMethod.GetAttributeValues(listInfosCRE[0], "Default/Order", "bCREValidationRequested");
                List<string> listMontantMin = CommonMethod.GetAttributeValues(listInfosCRE[0], "Default/Order", sTemplate + "_Min");
                List<string> listMontantMax = CommonMethod.GetAttributeValues(listInfosCRE[0], "Default/Order", sTemplate + "_Max");

                for (int idx = 0; idx < Math.Min(listMontantMin.Count, Math.Min(listMontantMax.Count, listCREValidationRequested.Count)); idx++)
                {
                    if( Decimal.TryParse(listMontantMin[idx].Replace('.', ','), NumberStyles.Currency, new CultureInfo("fr-FR"), out dMontantMin)
                        && Decimal.TryParse(listMontantMax[idx].Replace('.', ','), NumberStyles.Currency, new CultureInfo("fr-FR"), out dMontantMax))
                    {
                        if (dMontant >= dMontantMin && dMontant <= dMontantMax)
                            bReturn = (listCREValidationRequested[idx] != "0");
                    }
                }
            }

            // specifique à l'opérateur
            listInfosCRE.Clear();
            listInfosCRE = CommonMethod.GetTags(sCRE, "CRE/Specific");

            if (listInfosCRE.Count > 0)
            {
                List<string> listCREValidationRequested = CommonMethod.GetAttributeValues(listInfosCRE[0], "Specific/Order", "bCREValidationRequested");
                List<string> listMontantMin = CommonMethod.GetAttributeValues(listInfosCRE[0], "Specific/Order", sTemplate + "_Min");
                List<string> listMontantMax = CommonMethod.GetAttributeValues(listInfosCRE[0], "Specific/Order", sTemplate + "_Max");

                for (int idx = 0; idx < Math.Min(listMontantMin.Count, Math.Min(listMontantMax.Count, listCREValidationRequested.Count)); idx++)
                {
                    if (Decimal.TryParse(listMontantMin[idx].Replace('.', ','), NumberStyles.Currency, new CultureInfo("fr-FR"), out dMontantMin)
                        && Decimal.TryParse(listMontantMax[idx].Replace('.', ','), NumberStyles.Currency, new CultureInfo("fr-FR"), out dMontantMax))
                    {
                        if (dMontant >= dMontantMin && dMontant <= dMontantMax)
                            bReturn = (listCREValidationRequested[idx] != "0");
                    }
                }
            }
        }

        // cas du montant2
        listInfosCRE.Clear();
        if (sMontant2.Length > 0 && (Decimal.TryParse(sMontant2.Replace('.', ','), NumberStyles.Currency, new CultureInfo("fr-FR"), out dMontant)))
        {
            string sTemplate = "mMontant2";
            dMontant = decimal.Round(dMontant, 2, MidpointRounding.AwayFromZero);
            listInfosCRE.Clear();
            listInfosCRE = CommonMethod.GetTags(sCRE, "CRE/Default/Order");

            if (listInfosCRE.Count > 0)
            {
                List<string> listCREValidationRequested = CommonMethod.GetAttributeValues(listInfosCRE[0], "Default/Order", "bCREValidationRequested");
                List<string> listMontantMin = CommonMethod.GetAttributeValues(listInfosCRE[0], "Default/Order", sTemplate + "_Min");
                List<string> listMontantMax = CommonMethod.GetAttributeValues(listInfosCRE[0], "Default/Order", sTemplate + "_Max");

                for (int idx = 0; idx < Math.Min(listMontantMin.Count, Math.Min(listMontantMax.Count, listCREValidationRequested.Count)); idx++)
                {
                    if (Decimal.TryParse(listMontantMin[idx].Replace('.', ','), NumberStyles.Currency, new CultureInfo("fr-FR"), out dMontantMin)
                        && Decimal.TryParse(listMontantMax[idx].Replace('.', ','), NumberStyles.Currency, new CultureInfo("fr-FR"), out dMontantMax))
                    {
                        if (dMontant >= dMontantMin && dMontant <= dMontantMax)
                            bReturn = (listCREValidationRequested[idx] != "0");
                    }
                }
            }

            // specifique à l'opérateur
            listInfosCRE.Clear();
            listInfosCRE = CommonMethod.GetTags(sCRE, "CRE/Specific");

            if (listInfosCRE.Count > 0)
            {
                List<string> listCREValidationRequested = CommonMethod.GetAttributeValues(listInfosCRE[0], "Specific/Order", "bCREValidationRequested");
                List<string> listMontantMin = CommonMethod.GetAttributeValues(listInfosCRE[0], "Specific/Order", sTemplate + "_Min");
                List<string> listMontantMax = CommonMethod.GetAttributeValues(listInfosCRE[0], "Specific/Order", sTemplate + "_Max");

                for (int idx = 0; idx < Math.Min(listMontantMin.Count, Math.Min(listMontantMax.Count, listCREValidationRequested.Count)); idx++)
                {
                    if (Decimal.TryParse(listMontantMin[idx].Replace('.', ','), NumberStyles.Currency, new CultureInfo("fr-FR"), out dMontantMin)
                        && Decimal.TryParse(listMontantMax[idx].Replace('.', ','), NumberStyles.Currency, new CultureInfo("fr-FR"), out dMontantMax))
                    {
                        if (dMontant >= dMontantMin && dMontant <= dMontantMax)
                            bReturn = (listCREValidationRequested[idx] != "0");
                    }
                }
            }
        }
        return bReturn;
    }

    public static bool GetCREOrderMontant(string sCRE, int iTypeMontant, out string sMontantMin, out string sMontantMax)
    {
        string sTemplate = "mMontant" + iTypeMontant.ToString();
        decimal dMontantMin = -1;
        decimal dMontantMax = -1;
        decimal dMontantTmp;

        sMontantMin = "0.00";
        sMontantMax = "";
        List<string> listInfosCRE = CommonMethod.GetTags(sCRE, "CRE/Default");
        if (listInfosCRE.Count > 0)
        {
            List<string> listMontantMin = CommonMethod.GetAttributeValues(listInfosCRE[0], "Default/Order", sTemplate + "_Min");
            List<string> listMontantMax = CommonMethod.GetAttributeValues(listInfosCRE[0], "Default/Order", sTemplate + "_Max");
            if(listMontantMin.Count > 0)
            {
                if (Decimal.TryParse(listMontantMin[0].Replace('.', ','), NumberStyles.Currency, new CultureInfo("fr-FR"), out dMontantTmp))
                {
                    dMontantMin = decimal.Round(dMontantTmp, 2, MidpointRounding.AwayFromZero);
                }

                if (listMontantMin.Count >= 2)
                {
                    if (Decimal.TryParse(listMontantMin[1].Replace('.', ','), NumberStyles.Currency, new CultureInfo("fr-FR"), out dMontantTmp))
                    {
                        dMontantTmp = decimal.Round(dMontantTmp, 2, MidpointRounding.AwayFromZero);

                        dMontantMin = (dMontantMin == -1) ? dMontantTmp : Math.Min(dMontantTmp, dMontantMin);
                    }
                }
            }

            if (listMontantMax.Count > 0)
            {
                if (Decimal.TryParse(listMontantMax[0].Replace('.', ','), NumberStyles.Currency, new CultureInfo("fr-FR"), out dMontantTmp))
                {
                    dMontantMax = decimal.Round(dMontantTmp, 2, MidpointRounding.AwayFromZero);
                }

                if (listMontantMax.Count >= 2)
                {
                    if (Decimal.TryParse(listMontantMax[1].Replace('.', ','), NumberStyles.Currency, new CultureInfo("fr-FR"), out dMontantTmp))
                    {
                        dMontantTmp = decimal.Round(dMontantTmp, 2, MidpointRounding.AwayFromZero);

                        dMontantMax = (dMontantMax == -1) ? dMontantTmp : Math.Max(dMontantTmp, dMontantMin);
                    }
                }
            }
        }

        // gestion specifique à l'opérateur
        listInfosCRE.Clear();
        listInfosCRE = CommonMethod.GetTags(sCRE, "CRE/Specific");
        if (listInfosCRE.Count > 0)
        {
            List<string> listMontantMin = CommonMethod.GetAttributeValues(listInfosCRE[0], "Specific/Order", sTemplate + "_Min");
            List<string> listMontantMax = CommonMethod.GetAttributeValues(listInfosCRE[0], "Specific/Order", sTemplate + "_Max");
            if (listMontantMin.Count > 0)
            {
                if (Decimal.TryParse(listMontantMin[0].Replace('.', ','), NumberStyles.Currency, new CultureInfo("fr-FR"), out dMontantTmp))
                {
                    dMontantMin = decimal.Round(dMontantTmp, 2, MidpointRounding.AwayFromZero);
                }

                if (listMontantMin.Count >= 2)
                {
                    if (Decimal.TryParse(listMontantMin[1].Replace('.', ','), NumberStyles.Currency, new CultureInfo("fr-FR"), out dMontantTmp))
                    {
                        dMontantTmp = decimal.Round(dMontantTmp, 2, MidpointRounding.AwayFromZero);

                        dMontantMin = (dMontantMin == -1) ? dMontantTmp : Math.Min(dMontantTmp, dMontantMin);
                    }
                }
            }

            if (listMontantMax.Count > 0)
            {
                if (Decimal.TryParse(listMontantMax[0].Replace('.', ','), NumberStyles.Currency, new CultureInfo("fr-FR"), out dMontantTmp))
                {
                    dMontantMax = decimal.Round(dMontantTmp, 2, MidpointRounding.AwayFromZero);
                }

                if (listMontantMax.Count >= 2)
                {
                    if (Decimal.TryParse(listMontantMax[1].Replace('.', ','), NumberStyles.Currency, new CultureInfo("fr-FR"), out dMontantTmp))
                    {
                        dMontantTmp = decimal.Round(dMontantTmp, 2, MidpointRounding.AwayFromZero);

                        dMontantMax = (dMontantMax == -1) ? dMontantTmp : Math.Max(dMontantTmp, dMontantMin);
                    }
                }
            }
        }

        sMontantMin = dMontantMin.ToString("0.00");
        sMontantMax = dMontantMax.ToString("0.00");
        return true;
    }

    public static bool GetCREValidationMontant(string sCRE, int iTypeMontant, out string sMontantMin, out string sMontantMax)
    {
        string sTemplate = "mMontant" + iTypeMontant.ToString();
        decimal dMontantMin = -1;
        decimal dMontantMax = -1;
        decimal dMontantTmp;

        sMontantMin = "0.00";
        sMontantMax = "";
        List<string> listInfosCRE = CommonMethod.GetTags(sCRE, "CRE/Default");
        if (listInfosCRE.Count > 0)
        {
            List<string> listMontantMin = CommonMethod.GetAttributeValues(listInfosCRE[0], "Default/Validation", sTemplate + "_Min");
            List<string> listMontantMax = CommonMethod.GetAttributeValues(listInfosCRE[0], "Default/Validation", sTemplate + "_Max");
            if (listMontantMin.Count > 0)
            {
                if (Decimal.TryParse(listMontantMin[0].Replace('.', ','), NumberStyles.Currency, new CultureInfo("fr-FR"), out dMontantTmp))
                {
                    dMontantMin = decimal.Round(dMontantTmp, 2, MidpointRounding.AwayFromZero);
                }
            }

            if (listMontantMax.Count > 0)
            {
                if (Decimal.TryParse(listMontantMax[0].Replace('.', ','), NumberStyles.Currency, new CultureInfo("fr-FR"), out dMontantTmp))
                {
                    dMontantMax = decimal.Round(dMontantTmp, 2, MidpointRounding.AwayFromZero);
                }
            }
        }

        listInfosCRE = CommonMethod.GetTags(sCRE, "CRE/Specific");
        if (listInfosCRE.Count > 0)
        {
            List<string> listMontantMin = CommonMethod.GetAttributeValues(listInfosCRE[0], "Specific/Validation", sTemplate + "_Min");
            List<string> listMontantMax = CommonMethod.GetAttributeValues(listInfosCRE[0], "Specific/Validation", sTemplate + "_Max");
            if (listMontantMin.Count > 0)
            {
                if (Decimal.TryParse(listMontantMin[0].Replace('.', ','), NumberStyles.Currency, new CultureInfo("fr-FR"), out dMontantTmp))
                {
                    dMontantMin = decimal.Round(dMontantTmp, 2, MidpointRounding.AwayFromZero);
                }
            }

            if (listMontantMax.Count > 0)
            {
                if (Decimal.TryParse(listMontantMax[0].Replace('.', ','), NumberStyles.Currency, new CultureInfo("fr-FR"), out dMontantTmp))
                {
                    dMontantMax = decimal.Round(dMontantTmp, 2, MidpointRounding.AwayFromZero);
                }
            }
        }
        
        sMontantMin = dMontantMin.ToString("0.00");
        sMontantMax = dMontantMax.ToString("0.00");
        return true;
    }
}
