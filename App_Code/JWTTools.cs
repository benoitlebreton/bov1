﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json.Linq;

/// <summary>
/// Description résumée de JWT
/// </summary>

[Serializable]
public class JWTTools
{
    public static string setJWT(object oPayload, string sSecret)
    {
        return JWT.JsonWebToken.Encode(oPayload, sSecret, JWT.JwtHashAlgorithm.HS256);
    }

    public static bool getJWTValues(string sJWT, string sSecret, out JObject jsonData)
    {
        bool isOK = false;
        jsonData = null;

        try
        {
            string sJWTvalues = JWT.JsonWebToken.Decode(sJWT, sSecret, true);
            jsonData = JObject.Parse(sJWTvalues);
            if(sJWTvalues.Trim().Length > 0 && jsonData != null && jsonData.HasValues)
            {
                isOK = true;
            }
        }
        catch (Exception ex)
        {
            isOK = false;
        }

        return isOK;
    }
}