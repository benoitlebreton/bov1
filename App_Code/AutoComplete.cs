﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Xml.Linq;
using XMLMethodLibrary;
using System.Threading;
using System.Globalization;
using System.IO;
using System.Web.Services;
using System.Web.SessionState;

/// <summary>
/// Description résumée de WebService
/// </summary>
[Serializable]
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class AutoComplete : System.Web.Services.WebService {

    public AutoComplete()
    {
        
    }

    private void CheckIdentity()
    {
        if (HttpContext.Current.Session == null)
            throw new ApplicationException("Access denied");
        else
        {
            HttpSessionState SessionList = HttpContext.Current.Session;

            if (Session["Authentication"] == null)
                throw new InvalidOperationException("Authentication required");
        }
        //if (Session["RefAgency"] != null && Session["SellerRefUser"] != null)
            //return;
        
        //throw new InvalidOperationException("Invalid credentials");
    }

    //private class SessionVar
    //{
    //    static HttpSessionState Session
    //    {
    //        get
    //        {
    //            if (HttpContext.Current == null)
    //                throw new ApplicationException("No Http Context, No Session to Get!");

    //            return HttpContext.Current.Session;
    //        }
    //    }

    //    private static T Get<T>(string key)
    //    {
    //        if (Session[key] == null)
    //            return default(T);
    //        else
    //            return (T)Session[key];
    //    }

    //    private static void Set<T>(string key, T value)
    //    {
    //        Session[key] = value;
    //    }
    //}

    [WebMethod(EnableSession = true)]
    public string[] GetAgencyName(string prefixText, int count)
    {
        CheckIdentity();

        if (count == 0)
            count = 10;

        SqlConnection conn = null;
        DataTable dt = new DataTable();

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MoneyGram"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("P_MG_WEB_GetTagMgAgentList_AutoComplete", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@sAgencyName", SqlDbType.VarChar, 50);
            cmd.Parameters.Add("@sAgencyID", SqlDbType.VarChar, 50);
            cmd.Parameters.Add("@iTop", SqlDbType.Int);
            cmd.Parameters.Add("@ReturnQuery", SqlDbType.Bit);

            cmd.Parameters["@sAgencyName"].Value = prefixText;
            cmd.Parameters["@sAgencyID"].Value = "";
            cmd.Parameters["@ReturnQuery"].Value = 0;
            cmd.Parameters["@iTop"].Value = count;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;

            da.Fill(dt);
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        /*da.SelectCommand.Parameters.Add("@prefixText", SqlDbType.VarChar, 50).Value = prefixText+ "%";
        DataTable dt = new DataTable(); 
        da.Fill(dt); */
        List<string> items = new List<string>(count);
        int i = 0;

        foreach (DataRow dr in dt.Rows)
        {
            items.Add(dr["AgencyName"].ToString());
        }

        return items.ToArray();
    }

    [WebMethod(EnableSession = true)]
    public string[] GetAgencyID(string prefixText, int count)
    {
        CheckIdentity();

        if (count == 0)
            count = 10;

        SqlConnection conn = null;
        DataTable dt = new DataTable();

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MoneyGram"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("P_MG_WEB_GetTagMgAgentList_AutoComplete", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@sAgencyName", SqlDbType.VarChar, 50);
            cmd.Parameters.Add("@sAgencyID", SqlDbType.VarChar, 50);
            cmd.Parameters.Add("@iTop", SqlDbType.Int);
            cmd.Parameters.Add("@ReturnQuery", SqlDbType.Bit);

            cmd.Parameters["@sAgencyName"].Value = "";
            cmd.Parameters["@sAgencyID"].Value = prefixText;
            cmd.Parameters["@ReturnQuery"].Value = 0;
            cmd.Parameters["@iTop"].Value = count;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;

            da.Fill(dt);
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        /*da.SelectCommand.Parameters.Add("@prefixText", SqlDbType.VarChar, 50).Value = prefixText+ "%";
        DataTable dt = new DataTable(); 
        da.Fill(dt); */
        List<string> items = new List<string>(count);
        int i = 0;

        foreach (DataRow dr in dt.Rows)
        {
            items.Add(dr["AgencyName2"].ToString());
        }

        return items.ToArray();
    }

    [WebMethod(EnableSession = true)]
    public string[] GetCountryName(string prefixText, int count)
    {
        CheckIdentity();

        if (count == 0)
            count = 10;

        SqlConnection conn = null;
        DataTable dt = new DataTable();

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MoneyGram"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("P_MG_WEB_GetCountryList_AutoComplete", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@sCountryName", SqlDbType.VarChar, 200);
            cmd.Parameters.Add("@Culture", SqlDbType.VarChar, 50);
            cmd.Parameters.Add("@iTop", SqlDbType.Int);
            cmd.Parameters.Add("@ReturnQuery", SqlDbType.Bit);

            cmd.Parameters["@sCountryName"].Value = prefixText;
            cmd.Parameters["@Culture"].Value = "fr-FR";
            cmd.Parameters["@ReturnQuery"].Value = 0;
            cmd.Parameters["@iTop"].Value = count;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;

            da.Fill(dt);
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        /*da.SelectCommand.Parameters.Add("@prefixText", SqlDbType.VarChar, 50).Value = prefixText+ "%";
        DataTable dt = new DataTable(); 
        da.Fill(dt); */
        List<string> items = new List<string>(count);
        int i = 0;

        foreach (DataRow dr in dt.Rows)
        {
            items.Add(dr["CountryName"].ToString());
        }

        return items.ToArray();
    }
}
