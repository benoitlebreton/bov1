﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Configuration;

[Serializable]
public class OcrFiles
{
    public string OriginalFileName { get; set; }
    public string ABBYYOriginalPath { get; set; }
    public string ABBYYCroppedPath { get; set; }
    public string ICARCroppedPath { get; set; }
    public string ABBYYFilePath { get; set; }
    public string ErrorFilePath { get; set; }
    public string ICAROriginalPath { get; set; }
    public string OnfidoFilePath { get; set; }
    public string FaceCaptureDirectoryPath { get; set; }
    public int NbPages { get; set; }
    public string DocType { get; set; }

    public void getCroppedFilePath(out string FilePath1, out string FilePath2, out bool isFile1Present, out bool isFile2Present)
    {
        FilePath1 = "";
        FilePath2 = "";
        isFile1Present = false;
        isFile2Present = false;

        if (OriginalFileName.Trim() != "")
        {
            //string docType = "";
            //docType = OriginalFileName.Split('_')[0];

            ABBYYFilePath = ABBYYCroppedPath + OriginalFileName;

            if (NbPages > 0)
            {
                if (DocType.Trim().ToUpper() == "ID")
                {
                    if (File.Exists(System.Web.HttpContext.Current.Request.MapPath("~" + ICARCroppedPath + "_1_" + OriginalFileName.Substring(0, OriginalFileName.LastIndexOf('.')) + ".jpg")))
                    {
                        isFile1Present = true;
                        FilePath1 = ICARCroppedPath + "_1_" + OriginalFileName.Substring(0, OriginalFileName.LastIndexOf('.')) + ".jpg";
                    }
                    else if (File.Exists(System.Web.HttpContext.Current.Request.MapPath("~" + ICARCroppedPath + "\\Error\\" + "_1_" + OriginalFileName.Substring(0, OriginalFileName.LastIndexOf('.')) + ".jpg")))
                    {
                        isFile1Present = true;
                        FilePath1 = ICARCroppedPath + "\\Error\\" + "_1_" + OriginalFileName.Substring(0, OriginalFileName.LastIndexOf('.')) + ".jpg";
                    }
                    else if (File.Exists(System.Web.HttpContext.Current.Request.MapPath("~" + ABBYYCroppedPath + "_1_" + OriginalFileName.Substring(0, OriginalFileName.LastIndexOf('.')) + ".jpg")))
                    {
                        isFile1Present = true;
                        FilePath1 = ABBYYCroppedPath + "_1_" + OriginalFileName.Substring(0, OriginalFileName.LastIndexOf('.')) + ".jpg";
                    }
                    else if(File.Exists(System.Web.HttpContext.Current.Request.MapPath("~" + OnfidoFilePath + OriginalFileName.Substring(0, OriginalFileName.LastIndexOf('.')) + "_1.jpg")))
                    {
                        isFile1Present = true;
                        FilePath1 = OnfidoFilePath + OriginalFileName.Substring(0, OriginalFileName.LastIndexOf('.')) + "_1.jpg";
                    }
                    else if (NbPages == 1)
                    {
                        FilePath1 = ABBYYOriginalPath + OriginalFileName.Substring(0, OriginalFileName.LastIndexOf('.')) + ".jpg";
                    }
                    else
                        FilePath1 = ICAROriginalPath + OriginalFileName.Substring(0, OriginalFileName.LastIndexOf('.')) + ".jpg";

                    if (NbPages > 1)
                    {
                        if (File.Exists(System.Web.HttpContext.Current.Request.MapPath("~" + ICARCroppedPath + "_2_" + OriginalFileName.Substring(0, OriginalFileName.LastIndexOf('.')) + ".jpg")))
                        {
                            isFile2Present = true;
                            FilePath2 = ICARCroppedPath + "_2_" + OriginalFileName.Substring(0, OriginalFileName.LastIndexOf(".")) + ".jpg";
                        }
                        else if (File.Exists(System.Web.HttpContext.Current.Request.MapPath("~" + ICARCroppedPath + "\\Error\\" + "_2_" + OriginalFileName.Substring(0, OriginalFileName.LastIndexOf('.')) + ".jpg")))
                        {
                            isFile2Present = true;
                            FilePath2 = ICARCroppedPath + "\\Error\\" + "_2_" + OriginalFileName.Substring(0, OriginalFileName.LastIndexOf('.')) + ".jpg";
                        }
                        else if (File.Exists(System.Web.HttpContext.Current.Request.MapPath("~" + ABBYYCroppedPath + "_2_" + OriginalFileName.Substring(0, OriginalFileName.LastIndexOf('.')) + ".jpg")))
                        {
                            isFile2Present = true;
                            FilePath2 = ABBYYCroppedPath + "_2_" + OriginalFileName.Substring(0, OriginalFileName.LastIndexOf(".")) + ".jpg";
                        }
                        else if (File.Exists(System.Web.HttpContext.Current.Request.MapPath("~" + OnfidoFilePath + OriginalFileName.Substring(0, OriginalFileName.LastIndexOf('.')) + "_2.jpg")))
                        {
                            isFile2Present = true;
                            FilePath2 = OnfidoFilePath + OriginalFileName.Substring(0, OriginalFileName.LastIndexOf('.')) + "_2.jpg";
                        }
                        else
                            FilePath2 = ICARCroppedPath + "_2_" + OriginalFileName.Substring(0, OriginalFileName.LastIndexOf(".")) + ".jpg";
                    }
                    else FilePath2 = "";
                }
                else if (DocType.Trim().ToUpper() == "HO")
                {
                    if (File.Exists(System.Web.HttpContext.Current.Request.MapPath("~") + ABBYYCroppedPath + OriginalFileName.Substring(0, OriginalFileName.LastIndexOf(".")) + ".jpg"))
                        FilePath1 = ABBYYCroppedPath + OriginalFileName.Substring(0, OriginalFileName.LastIndexOf(".")) + ".jpg";
                    else
                        FilePath1 = ABBYYOriginalPath + OriginalFileName.Substring(0, OriginalFileName.LastIndexOf(".")) + ".jpg";
                }
            }
            else
            {
                FilePath1 = "";
                FilePath2 = "";
            }

        }
    }

    public static void getCaptureFacePath(string sRegistrationCode, out string sFaceCapturePath)
    {
        sFaceCapturePath = "";
        string sFaceCaptureDirectoryPath = ConfigurationManager.AppSettings["FacePath"].ToString();
        string sRelativePath = sFaceCaptureDirectoryPath + "FaceCapture_" + sRegistrationCode + ".jpg";
        string sPath = System.Web.HttpContext.Current.Request.MapPath("~" + sRelativePath);
        if (sFaceCaptureDirectoryPath.Trim().Length > 0 && File.Exists(sPath))
        {
            sFaceCapturePath = sRelativePath;
        }
    }

}