﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using XMLMethodLibrary;

[Serializable]
public class OnUs
{
    public bool? status { get; set; }
    public DataTable lastTransactions { get; set; }

    public static bool getGatewayOnUsStatus(string sXmlIn, out string sXmlOut)
    {
        bool isOK = false;
        sXmlOut = "";

        SqlConnection conn = null;
        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("[Web].[P_GetGatewayONUSStatus]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@IN", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@ReturnSelect", SqlDbType.Bit);
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
            cmd.Parameters["@IN"].Value = sXmlIn;
            cmd.Parameters["@ReturnSelect"].Value = true;

            cmd.ExecuteNonQuery();

            sXmlOut = cmd.Parameters["@OUT"].Value.ToString();
            isOK = true;
        }
        catch (Exception e)
        {
            isOK = false;
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return isOK;
    }
    public static bool setGatewayOnUsStatus(string sXmlIn, out string sXmlOut)
    {
        bool isOK = false;
        sXmlOut = "";

        SqlConnection conn = null;
        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("[Web].[P_StopStartOnUsFlow]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@IN", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
            cmd.Parameters["@IN"].Value = sXmlIn;

            cmd.ExecuteNonQuery();

            sXmlOut = cmd.Parameters["@OUT"].Value.ToString();
            isOK = true;
        }
        catch (Exception e)
        {
            isOK = false;
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return isOK;
    }

    public static bool GetGlobalStatus(string sToken, out OnUs _OnUs)
    {
        bool isOK = false;
        _OnUs = null;

        string sXmlOut = "";
        string sXmlIn = new XElement("ALL_XML_IN",
                            new XElement("GatewayOnUs", 
                                new XAttribute("CashierToken", sToken),
                                new XAttribute("Culture", "fr-FR"))).ToString();

        try
        {
            if (getGatewayOnUsStatus(sXmlIn, out sXmlOut) && !string.IsNullOrEmpty(sXmlOut.Trim()))
            {
                _OnUs = new OnUs();
                _OnUs.lastTransactions = getLastOnUsTransactions(sXmlOut);

                List<string> lsOnUsGatewayStatus = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/GatewayOnUs", "ONUS_CUT");
                if (lsOnUsGatewayStatus != null && !string.IsNullOrEmpty(lsOnUsGatewayStatus[0].Trim()))
                {
                    switch (lsOnUsGatewayStatus[0])
                    {
                        case "NO":
                            _OnUs.status = true;
                            break;
                        case "YES":
                            _OnUs.status = false;
                            break;
                    }
                }

                isOK = true;
            }
        }
        catch(Exception ex) { isOK = false; }

        return isOK;
    }
    private static DataTable getLastOnUsTransactions(string sXml)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("TransactionDate");
        dt.Columns.Add("OpeType");
        dt.Columns.Add("Amount");
        dt.Columns.Add("RequestDate");
        dt.Columns.Add("AnswerDate");
        dt.Columns.Add("Hcard");
        dt.Columns.Add("IDCom");
        dt.Columns.Add("FPETransaction");
        dt.Columns.Add("AFSOLRC");
        dt.Columns.Add("FPERC");
        dt.Columns.Add("Erreur");
        dt.Columns.Add("Duration");

        List<string> lsTransactions = CommonMethod.GetTags(sXml, "ALL_XML_OUT/GatewayOnUs/Transactions/T");

        for(int i=0; i < lsTransactions.Count; i++)
        {
            List<string> lsTransactionDate = CommonMethod.GetAttributeValues(lsTransactions[i], "T", "TransactionDate");
            List<string> lsOpeType = CommonMethod.GetAttributeValues(lsTransactions[i], "T", "OpeType");
            List<string> lsAmount = CommonMethod.GetAttributeValues(lsTransactions[i], "T", "Amount");
            List<string> lsRequestDate = CommonMethod.GetAttributeValues(lsTransactions[i], "T", "RequestDate");
            List<string> lsAnswerDate = CommonMethod.GetAttributeValues(lsTransactions[i], "T", "AnswerDate");
            List<string> lsHcard = CommonMethod.GetAttributeValues(lsTransactions[i], "T", "Hcard");
            List<string> lsIDCom = CommonMethod.GetAttributeValues(lsTransactions[i], "T", "IDCom");
            List<string> lsFPETransaction = CommonMethod.GetAttributeValues(lsTransactions[i], "T", "FPETransaction");
            List<string> lsAFSOLRC = CommonMethod.GetAttributeValues(lsTransactions[i], "T", "AFSOLRC");
            List<string> lsFPERC = CommonMethod.GetAttributeValues(lsTransactions[i], "T", "FPERC");
            List<string> lsErreur = CommonMethod.GetAttributeValues(lsTransactions[i], "T", "Erreur");
            List<string> lsDuration = CommonMethod.GetAttributeValues(lsTransactions[i], "T", "Duration");

            dt.Rows.Add(new object[] { lsTransactionDate[0], lsOpeType[0],lsAmount[0], lsRequestDate[0], lsAnswerDate[0],
                                        lsHcard[0], lsIDCom[0], lsFPETransaction[0], lsAFSOLRC[0], lsFPERC[0], lsErreur[0], lsDuration[0] });
        }

        return dt;
    }

    public static bool GetOnUsModelCommunicationList(out DataTable dt)
    {
        bool isOK = false;
        authentication auth = authentication.GetCurrent();
        dt = new DataTable("dtOnUsModelCommunicationList");
        try
        {
            //<ALL_XML_IN>  <Models CashierToken="DBTREATMENT" Target="PDV" ModelKind="SMS" />  </ALL_XML_IN>
            string sXmlIn = new XElement("ALL_XML_IN", new XElement("Models", new XAttribute("CashierToken", auth.sToken), new XAttribute("Target", "PDV"), new XAttribute("ModelKind", "SMS"))).ToString();
            string sXmlOut = "";

            if (tools.GetCommunicationModel(sXmlIn, out sXmlOut))
            {
                dt = GetDatatableFromModelCommunicationListXml(sXmlOut);
                isOK = true;
            }
        }
        catch(Exception ex)
        {
            isOK = false;
        }

        return isOK;
    }
    private static DataTable GetDatatableFromModelCommunicationListXml(string sXml)
    {
        DataTable dt = new DataTable("dtModelCommunicationList");
        dt.Columns.Add("ModelDescription");
        dt.Columns.Add("Model");
        dt.Columns.Add("ModelKind");

        List<string> lsModel = CommonMethod.GetTags(sXml, "ALL_XML_OUT/Models/Model");

        for (int i = 0; i < lsModel.Count; i++)
        {
            List<string> lsModelName = CommonMethod.GetAttributeValues(lsModel[i], "Model", "ModelDescription");
            List<string> lsModelMessage = CommonMethod.GetAttributeValues(lsModel[i], "Model", "Model");
            List<string> lsModelKind = CommonMethod.GetAttributeValues(lsModel[i], "Model", "ModelKind");

            if(lsModelName != null && !string.IsNullOrEmpty(lsModelName[0].ToString().Trim()) &&
                lsModelMessage != null && !string.IsNullOrEmpty(lsModelMessage[0].ToString().Trim()) &&
                lsModelKind != null && !string.IsNullOrEmpty(lsModelKind[0].ToString().Trim()))
            {
                dt.Rows.Add(new object[] { lsModelName[0].ToString().Trim(), lsModelMessage[0].ToString().Trim(), lsModelKind[0].ToString().Trim() });
            }
        }

        return dt;
    }

    public static DataTable GetModelCommunicationListByKind(DataTable dt, string sKind)
    {
        DataTable dtFiltered = new DataTable("dtModelCommunicationListByKind");
        dtFiltered.Columns.Add("ModelDescription");
        dtFiltered.Columns.Add("Model");

        foreach (DataRow dr in dt.Rows)
        {
            if(dr["ModelKind"].ToString() == sKind) { dtFiltered.Rows.Add(new object[] { dr["ModelDescription"], dr["Model"] }); }
        }

        return dtFiltered;
    }

}