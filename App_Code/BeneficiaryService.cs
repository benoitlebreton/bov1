﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using XMLMethodLibrary;

/// <summary>
/// Description résumée de BeneficiaryService
/// </summary>
public class BeneficiaryService
{


    public static List<Beneficiary> getBeneficiaryList(int refCustomer, bool returnUncheckedBenef)
    {
        List<Beneficiary> beneficiaryList = new List<Beneficiary>();
        if (returnUncheckedBenef == null)
            returnUncheckedBenef = false;

        
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK2"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_GetBeneficiaryInformationV2", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@RC", SqlDbType.Int);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                                new XElement("Benef",
                                                    new XAttribute("RefCustomer", refCustomer.ToString()))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
            cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();
            beneficiaryList = getBenefDatatableFromXML(sXmlOut, returnUncheckedBenef);
        }
        catch (Exception ex)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }
        return beneficiaryList;
    }

    protected static List<Beneficiary> getBenefDatatableFromXML(string sXML, bool returnUncheckedBenef)
    {
        List<Beneficiary> beneficiaryList = new List<Beneficiary>();
       
        List<string> lBenef = CommonMethod.GetTags(sXML, "ALL_XML_OUT/Benef");

        for (int i = 0; i < lBenef.Count; i++)
        {
            //if (returnUncheckedBenef || (!returnUncheckedBenef && tools.GetAttributeValueFromXml(lBenef[i], "Benef", "Checked").Trim() == "1"))
            //{
                
                DateTime checkedDate;
                DateTime deletedDate;
                
                

                DateTime.TryParse(tools.GetAttributeValueFromXml(lBenef[i], "Benef", "CheckedDate"), out checkedDate);
                
                DateTime.TryParse(tools.GetAttributeValueFromXml(lBenef[i], "Benef", "DeletionDate"), out deletedDate);

                // Ajout des bénéficiaires
                beneficiaryList.Add(
                                    new Beneficiary()
                                    {
                                        Id = i + 1,
                                        Reference = tools.GetAttributeValueFromXml(lBenef[i], "Benef", "RefBeneficiary"),
                                        Label = tools.GetAttributeValueFromXml(lBenef[i], "Benef", "BeneficiaryLabel"),
                                        Name = tools.GetAttributeValueFromXml(lBenef[i], "Benef", "BeneficiaryName"),
                                        Bic = tools.GetAttributeValueFromXml(lBenef[i], "Benef", "BIC"),
                                        Iban = tools.GetAttributeValueFromXml(lBenef[i], "Benef", "IBAN"),
                                        IsChecked = tools.GetAttributeValueFromXml(lBenef[i], "Benef", "Checked").ToBoolean(),
                                        CheckedDate = checkedDate,
                                        BankName = tools.GetAttributeValueFromXml(lBenef[i], "Benef", "BankName"),
                                        Email = tools.GetAttributeValueFromXml(lBenef[i], "Benef", "BeneficiaryEmail"),
                                        DeletedDate = deletedDate
                                    }
                                    );
                
            //}
        }

        return beneficiaryList.OrderBy(ord => ord.BankName).ThenBy(ord => ord.Iban).ToList();
    }

    public static bool AddBeneficiary(int iRefCustomer, string sLabel, string sName, string sIban, string sBic, string sEmail, out int refBeneficiary, out string sError)
    {
        bool isOK = false;
        refBeneficiary = 0; sError = "";
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK2"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_AddDelSetBeneficiary", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@RC", SqlDbType.Int);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                                new XElement("Benef",
                                                    new XAttribute("Action", "A"),
                                                    new XAttribute("RefCustomer", iRefCustomer.ToString()),
                                                    new XAttribute("BeneficiaryLabel", sLabel),
                                                    new XAttribute("BeneficiaryName", sName),
                                                    new XAttribute("BeneficiaryEmail", sEmail),
                                                    new XAttribute("IBAN", sIban),
                                                    new XAttribute("BIC", sBic))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
            cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            //<ALL_XML_OUT><Result Inserted="1" Updated="0" Deleted="0"  /></ALL_XML_OUT>
            int Inserted = 0;
            string sInserted = tools.GetAttributeValueFromXml(sXmlOut, "ALL_XML_OUT/Result", "Inserted");
            string sRefBeneficiary = tools.GetAttributeValueFromXml(sXmlOut, "ALL_XML_OUT/Result", "RefBeneficiaryInserted");
            string sRC = cmd.Parameters["@RC"].Value.ToString().Trim();

            switch (sRC)
            {
                case "0":
                    if (int.TryParse(sInserted, out Inserted) && Inserted > 0 &&
                        int.TryParse(sRefBeneficiary, out refBeneficiary))
                    {
                        sError = "";
                        isOK = true;
                    }
                    else
                    {
                        sError = "Une erreur innatendue est survenue.";
                        isOK = false;
                    }
                    break;
                case "2601":
                    sError = "Ce bénéficiaire a déjà été ajouté.";
                    isOK = false;
                    break;
                case "75090":
                    sError = "Vous ne pouvez pas ajouter de bénéficiaire car vous avez changé vos informations de contact dans les dernières 24h. <br/>Veuillez réessayer après ce délai.";
                    isOK = false;
                    break;
                default:
                    sError = "Le bénéficiaire n'a pas pu être ajouté car une erreur innatendue est survenue.";
                    isOK = false;
                    break;
            }
        }
        catch (Exception ex)
        {
            isOK = false;
            sError = "Une erreur innatendue est survenue.";
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return isOK;
    }

    public static bool ModifyBeneficiary(int refCustomer, int refBeneficiary, string sName, string sIban, string sBic, string sEmail, out string sError)
    {
        bool isOK = false;
        sError = "";
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK2"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_AddDelSetBeneficiary", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@RC", SqlDbType.Int);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                                new XElement("Benef",
                                                    new XAttribute("Action", "S"),
                                                    new XAttribute("RefCustomer", refCustomer.ToString()),
                                                    new XAttribute("RefBeneficiary", refBeneficiary.ToString()),
                                                    //new XAttribute("BeneficiaryLabel", sLabel),
                                                    new XAttribute("BeneficiaryEmail", sEmail),
                                                    new XAttribute("BeneficiaryName", sName),
                                                    new XAttribute("IBAN", sIban),
                                                    new XAttribute("BIC", sBic))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
            cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            //<ALL_XML_OUT><Result Inserted="1" Updated="0" Deleted="0"  /></ALL_XML_OUT>
            int Inserted = 0;
            string sInserted = tools.GetAttributeValueFromXml(sXmlOut, "ALL_XML_OUT/Result", "Inserted");
            string sRefBeneficiary = tools.GetAttributeValueFromXml(sXmlOut, "ALL_XML_OUT/Result", "RefBeneficiaryInserted");
            string sRC = cmd.Parameters["@RC"].Value.ToString().Trim();

            switch (sRC)
            {
                case "0":
                    if (int.TryParse(sRefBeneficiary, out refBeneficiary))
                    {
                        sError = "";
                        isOK = true;
                    }
                    else
                    {
                        sError = "Une erreur innatendue est survenue.";
                        isOK = false;
                    }
                    break;
                case "2601":
                    sError = "Ce bénéficiaire a déjà été ajouté.";
                    isOK = false;
                    break;
                case "75090":
                    sError = "Vous ne pouvez pas modifier de bénéficiaire car vous avez changé vos informations de contact dans les dernières 24h. <br/>Veuillez réessayer après ce délai.";
                    isOK = false;
                    break;
                default:
                    sError = "Le bénéficiaire n'a pas pu être ajouté car une erreur innatendue est survenue.";
                    isOK = false;
                    break;
            }
        }
        catch (Exception ex)
        {
            sError = "Une erreur innatendue est survenue.";
            isOK = false;
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return isOK;
    }

    public static bool DeleteBeneficiary(int refCustomer, int refBeneficiary)
    {
        bool isOK = false;

        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK2"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_AddDelSetBeneficiary", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@RC", SqlDbType.Int);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                                new XElement("Benef",
                                                    new XAttribute("Action", "D"),
                                                    new XAttribute("RefBeneficiary", refBeneficiary.ToString()))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
            cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            //<ALL_XML_OUT><Result Inserted="1" Updated="0" Deleted="0"  /></ALL_XML_OUT>
            int Deleted = 0;
            string sDeleted = tools.GetAttributeValueFromXml(sXmlOut, "ALL_XML_OUT/Result", "Deleted");
            string sRefBeneficiary = tools.GetAttributeValueFromXml(sXmlOut, "ALL_XML_OUT/Result", "RefBeneficiaryInserted");

            if (cmd.Parameters["@RC"].Value.ToString() == "0" &&
                int.TryParse(sDeleted, out Deleted) && Deleted > 0 &&
                int.TryParse(sRefBeneficiary, out refBeneficiary))
                isOK = true;

        }
        catch (Exception ex)
        {
            isOK = false;
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return isOK;
    }

    /// <summary>
    /// Retourn
    /// </summary>
    /// <param name="beneficiaryList"></param>
    /// <returns></returns>
    public static List<BeneficiaryView> ConvertBeneficiaryListToBeneficiaryViewList(List<Beneficiary> beneficiaryList)
    {
        List<BeneficiaryView> beneficiaryViewList = new List<BeneficiaryView>();

        var serializedParentList = JsonConvert.SerializeObject(beneficiaryList);
        beneficiaryViewList = JsonConvert.DeserializeObject<List<BeneficiaryView>>(serializedParentList);

        string pathImage = @"./Styles/Img/nickel-chrome/";
        foreach (var beneficiaryView in beneficiaryViewList)
        {

            switch (beneficiaryView.Statut)
            {
                case BeneficiaryStatutEnum.Ok:

                    beneficiaryView.UrlImageStatut1 = string.Concat(pathImage, "done.png");
                    beneficiaryView.VisibilityImageStatut1 = "";
                    break;


                case BeneficiaryStatutEnum.OkThenDelete:
                    beneficiaryView.UrlImageStatut1 = string.Concat(pathImage, "done.png");
                    beneficiaryView.VisibilityImageStatut1 = "";

                    beneficiaryView.UrlImageStatut2 = string.Concat(pathImage, "failed.png");
                    beneficiaryView.VisibilityImageStatut2 = "";
                    break;

                case BeneficiaryStatutEnum.Error:
                    beneficiaryView.UrlImageStatut1 = string.Concat(pathImage, "Error.png");
                    beneficiaryView.VisibilityImageStatut1 = "";
                    break;

                case BeneficiaryStatutEnum.ErrorThenDelete:
                    beneficiaryView.UrlImageStatut1 = string.Concat(pathImage, "Error.png");
                    beneficiaryView.VisibilityImageStatut1 = "";
                    beneficiaryView.UrlImageStatut2 = string.Concat(pathImage, "failed.png");
                    beneficiaryView.VisibilityImageStatut2 = "";
                    break;


                default:
                    break;
            }
            
        }
        return beneficiaryViewList;
    }
}