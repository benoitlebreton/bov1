﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Xml.Linq;
using XMLMethodLibrary;

/// <summary>
/// Summary description for Card
/// </summary>
public class Card
{
    public Card()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static bool SendReplacementCardCode(int iRefCustomer, string sCashierToken, bool bEmail, ref string sMessage)
    {
        bool bSent = false;
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_CardExchangeStart", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                            new XElement("CardExchange",
                                                new XAttribute("CashierToken", sCashierToken),
                                                new XAttribute("RefCustomer", iRefCustomer.ToString()),
                                                new XAttribute("PhoneNumber", (bEmail) ? "EMAIL" : DBNull.Value.ToString()))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();
            string sRC = "";

            if (sXmlOut.Length > 0)
            {
                List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/CardExchange", "RC");

                if (listRC.Count > 0)
                    sRC = listRC[0];
            }

            switch (sRC)
            {
                case "0":
                    bSent = true;
                    sMessage = "Votre code de remplacement vient de vous être envoyé par SMS.";
                    break;
                case "74201":
                case "74202":
                    sMessage = "Votre carte n'a pas été opposée. Veuillez d'abord appeler le 01.76.49.48.10.";
                    break;
                case "74203":
                    sMessage = "Envoi impossible. Vous avez déjà reçu 2 codes de remplacement lors des 7 derniers jours.";
                    break;
                default:
                    sMessage = "Une erreur est survenue. Veuillez réessayer.";
                    break;
            }
        }
        catch (Exception e)
        {
            sMessage = "Une erreur est survenue. Veuillez réessayer.";
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return bSent;
    }

    public static bool CardLock(string sToken, string sRefCustomer, string sCardNumber, out string sErrorMessage)
    {
        bool isOK = false;
        sErrorMessage = "";

        SqlConnection conn = null;
        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_OppositionCard", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@RC", SqlDbType.Int);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                            new XElement("Opposition",
                                                new XAttribute("CashierToken", sToken),
                                                new XAttribute("RefCustomer", sRefCustomer),
                                                new XAttribute("CardNumberPCIDSS", sCardNumber),
                                                new XAttribute("Culture", "fr-FR"))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
            cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if (sXmlOut.Length > 0)
            {
                List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Opposition", "RC");
                List<string> listErrorLabel = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Opposition", "ErrorLabel");
                List<string> listMonextLabel = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Opposition", "MONEXT_LABEL");
                List<string> listMonextRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Opposition", "MONEXT_RC");

                if (listRC.Count > 0 && listRC[0] == "0")
                {
                    isOK = true;
                }
                else
                {
                    sErrorMessage += "Raison : " + listErrorLabel[0];

                    if (listMonextRC[0].Length > 0)
                    {
                        sErrorMessage += "<br/>Raison Monext : ";
                        sErrorMessage += listMonextLabel[0] + " (" + listMonextRC[0] + ")";
                    }
                }
            }
        }
        catch (Exception ex)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return isOK;
    }

    public static bool OrderNickelChromeCard(int iRefCustomer, int iInitialRefCustomer, string sDisplayName, string sModelTAG, bool bMobile)
    {
        string sPreviewFileName = "";// CreateCustomCardPreviewFileName(iRefCustomer, sDisplayName, sModelTAG);
        bool isOK = false;
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("Web.P_PersonalizedCardOrder", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                            new XElement("Card",
                                                new XAttribute("RefCustomer", iRefCustomer.ToString()),
                                                new XAttribute("InitialRefCustomer", iInitialRefCustomer.ToString()),
                                                new XAttribute("DisplayName", sDisplayName),
                                                new XAttribute("ModelTAG", sModelTAG),
                                                new XAttribute("PreviewFileName", sPreviewFileName),
                                                new XAttribute("UsedDevice", (bMobile) ? "HMB_MOBILE" : "HMB_DESKTOP")
                                                        )
                                                      ).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if (sXmlOut.Length > 0)
            {
                List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Card", "RC");
                List<string> listErrorLabel = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Card", "ErrorLabel");

                if (listRC.Count > 0 && listRC[0] == "0" && listErrorLabel[0] == "")
                {
                    isOK = true;
                }
            }
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return isOK;
    }

    public static DataTable GetPersonnalizedCardList(int iRefCustomer)
    {
        DataTable dt = new DataTable();
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_GetPersonalizedCardInformation", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@RefCustomer", SqlDbType.Int);

            cmd.Parameters["@RefCustomer"].Value = iRefCustomer;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);

            cmd.ExecuteNonQuery();
        }
        catch (Exception e)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }

    public static bool isCancelPersonnalizedCardPossible(string sPaid, string sActivated, string sIsCancelled)
    {
        bool isCancelPossible = false;

        if (checkCancelButtonRight() && sActivated != "YES" && sPaid == "YES" && sIsCancelled.Trim() != "1")
        {
            isCancelPossible = true;
        }

        return isCancelPossible;
    }

    public static bool CancelPersonnalizedCardOrder(string sRefOrder)
    {
        bool isOK = false;

        if (checkCancelButtonRight())
        {

            authentication auth = authentication.GetCurrent();
            SqlConnection conn = null;

            try
            {

                /*
                SET @IN = '<ALL_XML_IN>
                                < Order CashierToken = "DBTREATMENT" RefOrder = "" RefCustomer = "37235" BankAccountNumber = "" />
                                   </ ALL_XML_IN > '
               --EXEC @RC = [web].[P_CancelCustomerPersonalizedCardOrder] @IN,@OUT OUTPUT
                SELECT @RC, CAST(@OUT AS XML)
                */
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
                conn.Open();

                SqlCommand cmd = new SqlCommand("web.P_CancelCustomerPersonalizedCardOrder", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@IN", SqlDbType.VarChar, -1);
                cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);
                cmd.Parameters.Add("@RC", SqlDbType.Int);

                cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                                    new XElement("Order",
                                                        new XAttribute("CashierToken", auth.sToken),
                                                        new XAttribute("RefOrder", sRefOrder))).ToString();
                cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
                cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;
                cmd.ExecuteNonQuery();
                string sRC = cmd.Parameters["@RC"].Value.ToString();
                string sXmlOUT = cmd.Parameters["@OUT"].Value.ToString();

                //TEST
                //string sRC = "99";
                //string sXmlOUT = "<ALL_XML_OUT><Order RC=\"0\" ErrorLabel=\"\"/></ALL_XML_OUT>";

                List<string> lsRC = XMLMethodLibrary.CommonMethod.GetAttributeValues(sXmlOUT, "ALL_XML_OUT/Order", "RC");

                if (sRC.Trim() == "0" && lsRC.Count > 0 && lsRC[0] == "0")
                {
                    isOK = true;
                }
            }
            catch (Exception e)
            {
                isOK = false;
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }
        return isOK;
    }

    private static bool checkCancelButtonRight()
    {
        bool isOK = false;
        authentication auth = authentication.GetCurrent();

        string sXmlOut = tools.GetRights(auth.sToken, "SC_Customer");
        bool bAction, bView;
        if (tools.GetTagActionRight(sXmlOut, "CancelPersonnalizedCardOrder", out bAction, out bView) && bAction && bView)
        {
            isOK = true;
        }

        return isOK;
    }

}