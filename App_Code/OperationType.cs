﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Description résumée de OperationType
/// </summary>
public class OperationType
{
    public int iCodeNickel { get; set; }
    public string sLabelFR { get; set; }

    public class OperationCategory
    {
        public int iCodeMCC { get; set; }
        public string sCatTag { get; set; }
        public string sLabelENG { get; set; }
        public string sLabelFR { get; set; }
    }

    public static Dictionary<int, string> OperationTypes = new Dictionary<int, string>()
    {
        {1,"Achat"},
        {2,"Retrait cash distributeur"},
        {3,"Dépôt cash"},
        {4,"Virement reçu"},
        {5,"Virement émis"},
        {6,"Frais Nickel"},
        {7,"Retrait point de vente"},
        {8,"Achat internet / VAD"},
        {9,"Régularisation"},
        {10,"Prélèvement"},
        {11,"Remboursement"},
        {12,"Rejet de virement"},
        {13,"Rejet de prélèvement"},
        {14,"Prélèvement TIP"},
        {15,"Opérations Diverses"},
        {16,"Remise de chèque"},
        {100,"Info indisponible"}
    };

    public static Dictionary<int, OperationCategory> OperationCategories = new Dictionary<int, OperationCategory>()
    {
        {40, new OperationCategory{iCodeMCC=40,sCatTag="TRA",sLabelENG="AUTOMOTIVE SERVICE SHOPS",sLabelFR="MAGASINS DE SERVICE AUTOMOBILE"}},
        {71, new OperationCategory{iCodeMCC=71,sCatTag="ADB",sLabelENG="FLORISTS",sLabelFR="FLEURISTES"}},
        {74, new OperationCategory{iCodeMCC=74,sCatTag="ALI",sLabelENG="FAST FOOD RESTAURANTS",sLabelFR="RESTAURATION RAPIDE"}},
        {763, new OperationCategory{iCodeMCC=763,sCatTag="ADB",sLabelENG="AGRICULTURAL COOPERATIVES",sLabelFR="COOPÉRATIVES AGRICOLES"}},
        {3672, new OperationCategory{iCodeMCC=3672,sCatTag="VOY",sLabelENG="CAMPANILE HOTELS",sLabelFR="HÔTEL CAMPANILE"}},
        {4011, new OperationCategory{iCodeMCC=4011,sCatTag="TRA",sLabelENG="RAILROADS - FREIGHT",sLabelFR="CHEMINS DE FER - FRET"}},
        {4111, new OperationCategory{iCodeMCC=4111,sCatTag="TRA",sLabelENG="TRANSPRTN-SUBRBN & LOCAL COMTR PSNGR, INCL FERRIES",sLabelFR="TRANSPORT (TRAIN,METRO,...)"}},
        {4121, new OperationCategory{iCodeMCC=4121,sCatTag="TRA",sLabelENG="LIMOUSINES AND TAXICABS",sLabelFR="LIMOUSINE ET TAXI"}},
        {4468, new OperationCategory{iCodeMCC=4468,sCatTag="ADB",sLabelENG="MARINAS, MARINE SERVICE/SUPPLIES",sLabelFR="DAS, SERVICE / FOURNITURES DE MARINE"}},
        {4511, new OperationCategory{iCodeMCC=4511,sCatTag="TRA",sLabelENG="AIR CARRIERS, AIRLINES-NOT ELSEWHERE CLASSIFIED",sLabelFR="TRANSPORTS AÉRIENS"}},
        {4784, new OperationCategory{iCodeMCC=4784,sCatTag="TRA",sLabelENG="BRIDGE AND ROAD FEES, TOLLS",sLabelFR="PÉAGE : ROUTES ET PONTS"}},
        {4789, new OperationCategory{iCodeMCC=4789,sCatTag="TRA",sLabelENG="TRANSPORTATION SERVICES NOT ELSEWHERE CLASSIFIED",sLabelFR="SERVICE DE TRANSPORT"}},
        {4812, new OperationCategory{iCodeMCC=4812,sCatTag="ADB",sLabelENG="TELECOMMUNICATION EQUIPMENT INCL TELEPHONE SALES",sLabelFR="TELECOMMUNICATION ( VENTE DE TELEPHONE)"}},
        {4814, new OperationCategory{iCodeMCC=4814,sCatTag="ADB",sLabelENG="TELECOM INCL PREPAID/RECURRING PHONE SVCS",sLabelFR="SERVICE DE TELECOMMUNICATION (RECHARGES)"}},
        {4816, new OperationCategory{iCodeMCC=4816,sCatTag="ADB",sLabelENG="COMPUTER NETWORK/INFORMATION SERVICES",sLabelFR="RESEAU & SERVICES INFORMATIQUE"}},
        {5193, new OperationCategory{iCodeMCC=5193,sCatTag="ADB",sLabelENG="FLORIST SUPPLIES,NURSERY STOCK & FLOWERS",sLabelFR="FLEURISTES"}},
        {5200, new OperationCategory{iCodeMCC=5200,sCatTag="ALI",sLabelENG="HOME SUPPLY WAREHOUSE STORES",sLabelFR="ALIMENTATION GENERALE"}},
        {5231, new OperationCategory{iCodeMCC=5231,sCatTag="ADB",sLabelENG="GLASS, PAINT, WALLPAPER STORES",sLabelFR="MAGASIN DE VITRE, PEINTURE , PAPIER PEINT"}},
        {5251, new OperationCategory{iCodeMCC=5251,sCatTag="ADB",sLabelENG="HARDWARE STORES",sLabelFR="MAGASIN INFORMATIQUES & ELECTRONIQUES"}},
        {5309, new OperationCategory{iCodeMCC=5309,sCatTag="ADB",sLabelENG="DUTY FREE STORES",sLabelFR="BOUTIQUE DUTY FREE"}},
        {5311, new OperationCategory{iCodeMCC=5311,sCatTag="ADB",sLabelENG="DEPARTMENT STORES",sLabelFR="GRAND MAGASIN"}},
        {5331, new OperationCategory{iCodeMCC=5331,sCatTag="ADB",sLabelENG="VARIETY STORES",sLabelFR="SUPERMARCHÉ"}},
        {5411, new OperationCategory{iCodeMCC=5411,sCatTag="ALI",sLabelENG="GROCERY STORES, SUPERMARKETS",sLabelFR="MAGASIN, ÉPICERIE OU SUPERMARCHÉ"}},
        {5422, new OperationCategory{iCodeMCC=5422,sCatTag="ALI",sLabelENG="FREEZER, LOCKER MEAT PROVISIONERS",sLabelFR="CONGÉLATEUR, CONSERVATION DE VIANDES"}},
        {5451, new OperationCategory{iCodeMCC=5451,sCatTag="ALI",sLabelENG="DAIRY PRODUCTS STORES",sLabelFR="PRODUITS LAITIERS MAGASINS"}},
        {5462, new OperationCategory{iCodeMCC=5462,sCatTag="ALI",sLabelENG="BAKERIES",sLabelFR="BOULANGERIE"}},
        {5499, new OperationCategory{iCodeMCC=5499,sCatTag="ALI",sLabelENG="MISC FOOD STORE-CONVENIENCE,MRKT,SPLTY,VENDNG MACS",sLabelFR="ALIMENTATION DIVERS"}},
        {5541, new OperationCategory{iCodeMCC=5541,sCatTag="TRA",sLabelENG="SERVICE STATIONS WITH OR WITHOUT ANCILLARY SERVICE",sLabelFR="STATIONS-SERVICE"}},
        {5542, new OperationCategory{iCodeMCC=5542,sCatTag="TRA",sLabelENG="FUEL DISPENSER, AUTOMATED",sLabelFR="DISTRIBUTEUR DE CARBURANT AUTOMATIQUE"}},
        {5571, new OperationCategory{iCodeMCC=5571,sCatTag="TRA",sLabelENG="MOTORCYCLE SHOPS AND DEALERS",sLabelFR="COMMERCES ET CONCESSIONNAIRES MOTO"}},
        {5651, new OperationCategory{iCodeMCC=5651,sCatTag="VES",sLabelENG="FAMILY CLOTHING STORES",sLabelFR="MAGASIN DE VÊTEMENTS"}},
        {5691, new OperationCategory{iCodeMCC=5691,sCatTag="VES",sLabelENG="MEN'S AND WOMEN'S CLOTHING STORES",sLabelFR="MAGASIN DE VÊTEMENTS POUR HOMMES & FEMMES"}},
        {5712, new OperationCategory{iCodeMCC=5712,sCatTag="VES",sLabelENG="EQUIP, FURNITURE, HOME FURNSHNGS STRS (EXCPT APPL)",sLabelFR="VÊTEMENTS & ACCESSOIRES"}},
        {5732, new OperationCategory{iCodeMCC=5732,sCatTag="LOI",sLabelENG="ELECTRONIC SALES",sLabelFR="MAGASIN ÉLECTRONIQUES"}},
        {5735, new OperationCategory{iCodeMCC=5735,sCatTag="LOI",sLabelENG="RECORD SHOPS",sLabelFR="DISQUAIRE"}},
        {5811, new OperationCategory{iCodeMCC=5811,sCatTag="ALI",sLabelENG="CATERERS",sLabelFR="TRAITEUR"}},
        {5812, new OperationCategory{iCodeMCC=5812,sCatTag="ALI",sLabelENG="EATING PLACES, RESTAURANTS",sLabelFR="RESTAURANT, BRASSERIE"}},
        {5813, new OperationCategory{iCodeMCC=5813,sCatTag="LOI",sLabelENG="BAR,LOUNGE,DISCO,NIGHTCLUB,TAVERN-ALCOHOLIC DRINKS",sLabelFR="BAR, SALON , DISCOTHÈQUE , BOITE DE NUIT"}},
        {5814, new OperationCategory{iCodeMCC=5814,sCatTag="ALI",sLabelENG="FAST FOOD RESTAURANTS",sLabelFR="RESTAURATION RAPIDE"}},
        {5912, new OperationCategory{iCodeMCC=5912,sCatTag="ADB",sLabelENG="DRUG STORES, PHARMACIES",sLabelFR="PHARMACIES"}},
        {5941, new OperationCategory{iCodeMCC=5941,sCatTag="LOI",sLabelENG="SPORTING GOODS STORES",sLabelFR="MAGASIN DE SPORTS"}},
        {5942, new OperationCategory{iCodeMCC=5942,sCatTag="ADB",sLabelENG="BOOK STORES",sLabelFR="LIBRAIRIE"}},
        {5944, new OperationCategory{iCodeMCC=5944,sCatTag="ADB",sLabelENG="CLOCK, JEWELRY, WATCH, AND SILVERWARE STORE",sLabelFR="BIJOUTIER, HORLOGER"}},
        {5946, new OperationCategory{iCodeMCC=5946,sCatTag="ADB",sLabelENG="CAMERA AND PHOTOGRAPHIC SUPPLY STORES",sLabelFR="MAGASIN D'APPAREILS ET ACCESSOIRES PHOTO"}},
        {5964, new OperationCategory{iCodeMCC=5964,sCatTag="ADB",sLabelENG="DIRECT MARKETING-CATALOG MERCHANTS",sLabelFR="CATALOGUE & VENTE DIRECTE"}},
        {5965, new OperationCategory{iCodeMCC=5965,sCatTag="ADB",sLabelENG="DIRECT MARKETING-COMBINATION CATALOG/RETAIL MERCH.",sLabelFR="CATALOGUE & VENTE DIRECTE"}},
        {5968, new OperationCategory{iCodeMCC=5968,sCatTag="ADB",sLabelENG="DIRECT MARKETING-CONTINUITY/SUBSCRIPTION MERCHANTS",sLabelFR="ACHAT INTERNET"}},
        {5969, new OperationCategory{iCodeMCC=5969,sCatTag="ADB",sLabelENG="DIRECT MARKETING-OTHER DIRECT MARKETERS/NOT ELSEW.",sLabelFR="CATALOGUE & VENTE DIRECTE"}},
        {5977, new OperationCategory{iCodeMCC=5977,sCatTag="ADB",sLabelENG="COSMETIC STORES",sLabelFR="PARFUMERIE & COSMETIQUE"}},
        {5983, new OperationCategory{iCodeMCC=5983,sCatTag="TRA",sLabelENG="FUEL DEALERS-COAL, FUEL OIL, LIQ PETROLEUM, WOOD",sLabelFR="STATION ESSENCE"}},
        {5992, new OperationCategory{iCodeMCC=5992,sCatTag="ADB",sLabelENG="FLORISTS",sLabelFR="FLEURISTES"}},
        {5993, new OperationCategory{iCodeMCC=5993,sCatTag="ADB",sLabelENG="CIGAR STORES AND STANDS",sLabelFR="TABACS"}},
        {5995, new OperationCategory{iCodeMCC=5995,sCatTag="ADB",sLabelENG="PET SHOPS - PET FOODS AND SUPPLIES",sLabelFR="ANIMALERIES - ALIMENTS & FOURNITURES"}},
        {6211, new OperationCategory{iCodeMCC=6211,sCatTag="ADB",sLabelENG="SECURITIES-BROKERS/DEALERS",sLabelFR="SECURITIES-BROKERS/DEALERS"}},
        {6513, new OperationCategory{iCodeMCC=6513,sCatTag="IMM",sLabelENG="REAL ESTATE AGENTS AND MANAGERS—RENTALS",sLabelFR="AGENTS IMMOBILIERS ET LOCATION"}},
        {7011, new OperationCategory{iCodeMCC=7011,sCatTag="VOY",sLabelENG="LODGING-HOTELS,MOTELS,RESORTS-NOT CLASSIFIED",sLabelFR="HÔTELS , MOTELS"}},
        {7211, new OperationCategory{iCodeMCC=7211,sCatTag="",sLabelENG="LAUNDRY SERVICES - FAMILY AND COMMERICAL",sLabelFR="BLANCHISSERIE"}},
        {7372, new OperationCategory{iCodeMCC=7372,sCatTag="ADB",sLabelENG="COMP PROGRAMING,DATA PRCSNG,INTGRTD SYS DSGN SRVS",sLabelFR="SERVICE INFORMATIQUE"}},
        {7512, new OperationCategory{iCodeMCC=7512,sCatTag="TRA",sLabelENG="AUTOMOBILE RENTAL AGENCY-NOT ELSEWHERE CLASSIFIED",sLabelFR="AGENCE DE LOCATION AUTOMOBILE"}},
        {7523, new OperationCategory{iCodeMCC=7523,sCatTag="TRA",sLabelENG="AUTOMOBILE PARKING LOTS AND GARAGES",sLabelFR="PARCS DE STATIONNEMENT AUTOMOBILE ET GARAGES"}},
        {7538, new OperationCategory{iCodeMCC=7538,sCatTag="TRA",sLabelENG="AUTOMOTIVE SERVICE SHOPS",sLabelFR="MAGASINS DE SERVICE AUTOMOBILE"}},
        {7539, new OperationCategory{iCodeMCC=7539,sCatTag="TRA",sLabelENG="AUTOMOTIVE SERVICE SHOPS",sLabelFR="MAGASINS DE SERVICE AUTOMOBILE"}},
        {7832, new OperationCategory{iCodeMCC=7832,sCatTag="LOI",sLabelENG="MOTION PICTURE THEATERS",sLabelFR="CINÉMA"}},
        {7922, new OperationCategory{iCodeMCC=7922,sCatTag="LOI",sLabelENG="THEATRICAL PRODUCERS(EXCL MOTION PIX),TICKET AGNCY",sLabelFR="THÉÂTRE , BILLETERIE"}},
        {7991, new OperationCategory{iCodeMCC=7991,sCatTag="VOY",sLabelENG="TOURIST ATTRACTIONS AND EXHIBITS",sLabelFR="ATTRACTION TOURISTIQUE, EXPOSITION"}}
    };

    public static Dictionary<int, string> CobaltOperationTypes = new Dictionary<int, string>()
    {
        {100,"Achat"}, //_ACHAT_
        {200,"Retrait Cash Distributeur"}, //_RET_DAB_
        {300,"Dépôt Cash"}, //_DEP_
        {301,"Annulation Dépôt Cash"}, //_ANN_DEP_
        {350,"Dépôt Cash Initial"}, //_DEP_INIT_
        {400,"Virement Reçu"}, //_VIR_RECU_
        {500,"Virement Emis"}, //_VIR_ENV_
        {600,"Commission Dépôt Cash"}, //_COM_DEP_
        {601,"Commission Dépôt Cash Initial"}, //_COM_DEP_INIT_
        {602,"Annulation Commission Dépôt Cash"}, //_ANN_COM_DEP_
        {603,"Annulation Commission Dépôt Cash Initial"}, //_ANN_COM_DEP_INIT_
        {604,"Commission Retrait Cash"}, //_COM_RET_
        {605,"Annulation Commission Retrait Cash"}, //_ANN_COM_RET_
        {606,"Commission Activation"}, //_COM_ACTIV_
        {607,"Commission sur 1er renouvellement cotisation client"}, //_COM_RENOUVEL_COTIS_CLT_1_
        {610,"Frais Nickel"}, //_COM_CLIENT_EDITION_RIB_
        {700,"Retrait Cash"}, //_RET_
        {701,"Annulation Retrait Cash"}, //_ANN_RET_
        {800,"Achat Internet / VAD"}, //_ACHAT_NET_
        {900,"Régularisation"}, //_REGUL_
        {1000,"Prélèvement"}, //_PRELEV_
        {1100,"Remboursement"}, //_REMB_
        {1200,"Rejet Virement"}, //_REJ_VIR_
        {1300,"Rejet Prélèvement"}, //_REJ_PRELEV_
        {1400,"Prélèvement TIP"}, //_PRELEV_TIP_
        {1500,"Opérations Diverses"}, //_OPE_DIVERS_
        {1501,"Vente Pack"}, //_VENTE_PACK_
        {1600,"Remise Chèque"}, //_CHEQUE_
        {10000,"Info indisponible"} //_INCONNU_
    };

    public static Dictionary<int, string> CobaltOperationTypesOther = new Dictionary<int, string>()
    {
        {100,"ACHAT"},
        {200,"RET_DAB"},
        {300,"DEP"},
        {301,"ANN_DEP"},
        {350,"DEP_INIT"},
        {400,"VIR_RECU"},
        {500,"VIR_ENV"},
        {542,"OPE_CODE_PIN_CODE_REMINDER" },
        {600,"COM_DEP"},
        {601,"COM_DEP_INIT"},
        {602,"ANN_COM_DEP"},
        {603,"ANN_COM_DEP_INIT"},
        {604,"COM_RET"},
        {605,"ANN_COM_RET"},
        {606,"COM_ACTIV"},
        {607,"COM_RENOUVEL_COTIS_CLT_1"},
        {700,"RET"},
        {701,"ANN_RET"},
        {800,"ACHAT_NET"},
        {900,"REGUL"},
        {1000,"PRELEV"},
        {1100,"REMB"},
        {1200,"REJ_VIR"},
        {1300,"REJ_PRELEV"},
        {1400,"PRELEV_TIP"},
        {1500,"OPE_DIVERS"},
        {1501,"VENTE_PACK"},
        {1600,"CHEQUE"},
        {10000,"INCONNU"}
    };
}