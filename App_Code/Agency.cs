﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using XMLMethodLibrary;

[Serializable]
public class Agency
{
    public static DataTable getAgencySearchResult(string sSearchValue, int iMaxResult, string sToken, out string sXmlOut)
    {
        sXmlOut = "";
        if (iMaxResult == null && iMaxResult == 0)
            iMaxResult = 20;

        DataTable dt = new DataTable();

        if (sSearchValue.Trim().Length > 0)
        {

            SqlConnection conn = null;

            try
            {
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["PARTNER"].ConnectionString);
                conn.Open();

                SqlCommand cmd = new SqlCommand("[Partner].[P_GetAgenciesList]", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@sXmlIn", SqlDbType.VarChar, -1);
                cmd.Parameters.Add("@sXmlOut", SqlDbType.VarChar, -1);

                cmd.Parameters["@sXmlIn"].Value = new XElement("ALL_XML_IN",
                                                new XElement("H",
                                                    new XAttribute("iTop", iMaxResult.ToString()),
                                                    new XAttribute("TOKEN", sToken),
                                                    new XAttribute("sFilter", sSearchValue))).ToString();

                cmd.Parameters["@sXmlOut"].Direction = ParameterDirection.Output;

                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;

                da.Fill(dt);

                sXmlOut = cmd.Parameters["@sXmlOut"].Value.ToString();


            }
            catch (Exception e)
            {
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        return dt;
    }
    public static DataTable getAgencyMpadList(string sRefAgency, string sToken, int iMaxResult, out string sXmlOut)
    {
        if (iMaxResult == null || iMaxResult == 0)
            iMaxResult = 20;

        string sXmlIn = new XElement("ALL_XML_IN",
                            new XElement("H",
                                new XAttribute("iTop", iMaxResult.ToString()),
                                new XAttribute("iRefAgency", sRefAgency),
                                new XAttribute("TOKEN", sToken),
                                new XAttribute("sFilter", sRefAgency))).ToString();

        return getMpadList(sXmlIn, out sXmlOut);
    }
    public static DataTable getAgencyPosList(string sRefAgency, string sToken, int iMaxResult, out string sXmlOut)
    {
        if (iMaxResult == null || iMaxResult == 0)
            iMaxResult = 20;

        string sXmlIn = new XElement("ALL_XML_IN",
                                            new XElement("H",
                                                new XAttribute("iTop", iMaxResult.ToString()),
                                                new XAttribute("iRefAgency", sRefAgency),
                                                new XAttribute("TOKEN", sToken),
                                                new XAttribute("sFilter", sRefAgency))).ToString();

        return getPosList(sXmlIn, out sXmlOut);
    }
    public static DataTable getAgencyPosList(string sRefAgency, string sToken, int iMaxResult)
    {
        string sXmlOut = "";
        if (iMaxResult == null || iMaxResult == 0)
            iMaxResult = 20;

        string sXmlIn = new XElement("ALL_XML_IN",
                                            new XElement("H",
                                                new XAttribute("iTop", iMaxResult.ToString()),
                                                new XAttribute("iRefAgency", sRefAgency),
                                                new XAttribute("TOKEN", sToken),
                                                new XAttribute("sFilter", sRefAgency))).ToString();

        return getPosList(sXmlIn, out sXmlOut);
    }
    public static DataTable getAgencyMpadList(string sRefAgency, string sToken, int iMaxResult)
    {
        string sXmlOut = "";
        if (iMaxResult == null || iMaxResult == 0)
            iMaxResult = 20;

        string sXmlIn = new XElement("ALL_XML_IN",
                                            new XElement("H",
                                                new XAttribute("iTop", iMaxResult.ToString()),
                                                new XAttribute("iRefAgency", sRefAgency),
                                                new XAttribute("TOKEN", sToken),
                                                new XAttribute("sFilter", sRefAgency))).ToString();

        return getMpadList(sXmlIn, out sXmlOut);
    }
    public static DataTable getAgencyHwList(string sHwType, string sRefAgency, string sToken, int iMaxResult, out string sXmlOut)
    {
        if (iMaxResult == null || iMaxResult == 0)
            iMaxResult = 20;

        string sXmlIn = new XElement("ALL_XML_IN",
                                            new XElement("H",
                                                new XAttribute("iTop", iMaxResult.ToString()),
                                                new XAttribute("iRefAgency", sRefAgency),
                                                new XAttribute("HW_Type", sHwType),
                                                new XAttribute("TOKEN", sToken),
                                                new XAttribute("sFilter", sRefAgency))).ToString();



        return getHwList(sXmlIn, out sXmlOut);
    }

    public static DataTable getAgencyMpadDetails(string sRefAgency, string sMpadSerial, string sToken, out string sXmlOut)
    {
        string sXmlIn = new XElement("ALL_XML_IN",
                                            new XElement("H",
                                                new XAttribute("iTop", 1),
                                                new XAttribute("iRefAgency", sRefAgency),
                                                new XAttribute("MP_Serial", sMpadSerial),
                                                new XAttribute("TOKEN", sToken),
                                                new XAttribute("sFilter", sRefAgency))).ToString();

        return getMpadList(sXmlIn, out sXmlOut);
    }
    public static DataTable getAgencyPosDetails(string sRefAgency, string sPosSerial, string sToken, out string sXmlOut)
    {
        string sXmlIn = new XElement("ALL_XML_IN",
                                            new XElement("H",
                                                new XAttribute("iTop", 1),
                                                new XAttribute("iRefAgency", sRefAgency),
                                                new XAttribute("PS_Serial", sPosSerial),
                                                new XAttribute("TOKEN", sToken),
                                                new XAttribute("sFilter", sRefAgency))).ToString();

        return getPosList(sXmlIn, out sXmlOut);
    }
    public static DataTable getAgencyHwDetails(string sHwType, string sRefAgency, string sHwSerial, string sToken, out string sXmlOut)
    {

        string sXmlIn = new XElement("ALL_XML_IN",
                    new XElement("H",
                        new XAttribute("iTop", 1),
                        new XAttribute("iRefAgency", sRefAgency),
                        new XAttribute("HW_Serial", sHwSerial),
                        new XAttribute("HW_Type", sHwType),
                        new XAttribute("TOKEN", sToken),
                        new XAttribute("sFilter", sRefAgency))).ToString();

        //switch(sHwType)
        //{
        //    case "MPD":
        //        sXmlIn = new XElement("ALL_XML_IN",
        //                    new XElement("H",
        //                        new XAttribute("iTop", 1),
        //                        new XAttribute("iRefAgency", sRefAgency),
        //                        new XAttribute("MP_Serial", sHwSerial),
        //                        new XAttribute("TOKEN", sToken),
        //                        new XAttribute("sFilter", sRefAgency))).ToString();
        //        break;
        //    case "POS":
        //        sXmlIn = new XElement("ALL_XML_IN",
        //                    new XElement("H",
        //                        new XAttribute("iTop", 1),
        //                        new XAttribute("iRefAgency", sRefAgency),
        //                        new XAttribute("POS_Serial", sHwSerial),
        //                        new XAttribute("TOKEN", sToken),
        //                        new XAttribute("sFilter", sRefAgency))).ToString();
        //        break;
        //    case "TAB":
        //        sXmlIn = new XElement("ALL_XML_IN",
        //                    new XElement("H",
        //                        new XAttribute("iTop", 1),
        //                        new XAttribute("iRefAgency", sRefAgency),
        //                        new XAttribute("TAB_Serial", sHwSerial),
        //                        new XAttribute("TOKEN", sToken),
        //                        new XAttribute("sFilter", sRefAgency))).ToString();
        //        break;
        //    case "SCN":
        //        sXmlIn = new XElement("ALL_XML_IN",
        //                            new XElement("H",
        //                                new XAttribute("iTop", 1),
        //                                new XAttribute("iRefAgency", sRefAgency),
        //                                new XAttribute("PS_Serial", sHwSerial),
        //                                new XAttribute("TOKEN", sToken),
        //                                new XAttribute("sFilter", sRefAgency))).ToString();
        //        break;
        //    case "PPD":
        //        sXmlIn = new XElement("ALL_XML_IN",
        //                            new XElement("H",
        //                                new XAttribute("iTop", 1),
        //                                new XAttribute("iRefAgency", sRefAgency),
        //                                new XAttribute("PPD_Serial", sHwSerial),
        //                                new XAttribute("TOKEN", sToken),
        //                                new XAttribute("sFilter", sRefAgency))).ToString();
        //        break;
        //}


        return getHwList(sXmlIn, out sXmlOut);
    }

    protected static DataTable getMpadList(string sXmlIn, out string sXmlOut)
    {
        DataTable dt = new DataTable();
        sXmlOut = "";

        SqlConnection conn = null;
        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["PARTNER"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("HW.P_GetAgencyMPADInformation", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@sXmlIn", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@sXmlOut", SqlDbType.VarChar, -1);

            cmd.Parameters["@sXmlIn"].Value = sXmlIn;

            cmd.Parameters["@sXmlOut"].Direction = ParameterDirection.Output;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;

            da.Fill(dt);

            sXmlOut = cmd.Parameters["@sXmlOut"].Value.ToString();
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }
    protected static DataTable getPosList(string sXmlIn, out string sXmlOut)
    {
        DataTable dt = new DataTable();
        sXmlOut = "";

        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Partner"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("HW.P_GetAgencyPOSInformation", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@sXmlIn", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@sXmlOut", SqlDbType.VarChar, -1);

            cmd.Parameters["@sXmlIn"].Value = sXmlIn;

            cmd.Parameters["@sXmlOut"].Direction = ParameterDirection.Output;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;

            da.Fill(dt);

            sXmlOut = cmd.Parameters["@sXmlOut"].Value.ToString();
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }
    protected static DataTable getHwList(string sXmlIn, out string sXmlOut)
    {
        string sProcStock = "";
        //switch (sHwType)
        //{
        //    case "MPD":
        //        sProcStock = "HW.P_GetAgencyMPADInformation";
        //        break;
        //    case "POS":
        //        sProcStock = "HW.P_GetAgencyPOSInformation";
        //        break;
        //    case "SCN":
        //        sProcStock = "HW.P_GetAgencyScannerInformation";
        //        break;
        //    case "TAB":
        //        sProcStock = "HW.P_GetAgencyTabletInformation";
        //        break;
        //    case "PPD":
        //        sProcStock = "HW.P_GetAgencyPinpadInformation";
        //        break;
        //}

        DataTable dt = new DataTable();
        sXmlOut = "";

        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Partner"].ConnectionString);
            conn.Open();

            //SqlCommand cmd = new SqlCommand(sProcStock, conn);
            SqlCommand cmd = new SqlCommand("HW.P_GetAgencyHWInformation", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@sXmlIn", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@sXmlOut", SqlDbType.VarChar, -1);

            cmd.Parameters["@sXmlIn"].Value = sXmlIn;

            cmd.Parameters["@sXmlOut"].Direction = ParameterDirection.Output;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;

            da.Fill(dt);

            sXmlOut = cmd.Parameters["@sXmlOut"].Value.ToString();
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }

    public static string getPosModelName(string sSerial)
    {
        string sModelName = "";

        if(sSerial != null && sSerial.Trim().Length > 0 && sSerial.Trim().Substring(0,1).ToUpper() == "D")
            sModelName = "VERIFONE";
        else
            sModelName = "INGENICO";

        return sModelName;
    }
    public static string getPosImageName(string sPosModelName)
    {
        string sImageName = "";

        if (sPosModelName.Trim().Length > 0 && sPosModelName.Trim().ToLower() == "verifone")
            sImageName = "Verifone";
        else sImageName = "Ingenico";
        return sImageName;
    }
    public static string getMpadStatus(string sStatusText, string sStatusColor)
    {
        return "<span style=\"color:" + sStatusColor + "\">" + sStatusText + "</span>";
    }
    public static string getPosStatus(string sStatusValue)
    {
        string sStatusText = "<span style=\"font-weight:normal; color: #AAA; font-style:italic;\">INCONNU</span>";

        switch (sStatusValue) {
            case "1":
                sStatusText = "<span style=\"font-weight:bold; color: green;\">OK</span>";
                break;
            case "2":
                sStatusText = "<span style=\"font-weight:bold; color: green;\">OK</span>";
                break;
            case "3":
                sStatusText = "<span style=\"font-weight:bold; color: green;\">OK</span>";
                break;
        }

        return sStatusText;
    }
    public static string getMpadLockStatus(string sIsLocked)
    {
        string sStatus = "<span style=\"font-weight:bold; color:green;\">OK</span>";

        if (sIsLocked.Trim().ToLower() == "true")
            sStatus = "<span style=\"font-weight:bold; color:red;\">Verrouillée</span>";

        return sStatus;
    }
    public static string getPosLockStatus(string sIsLocked)
    {
        string sStatus = "<span style=\"font-weight:bold; color:green;\">OK</span>";

        if(sIsLocked.Trim().ToLower() == "true")
            sStatus = "<span style=\"font-weight:bold; color:red;\">Verrouillé</span>";

        return sStatus;
    }
    public static string getRemoteAccessStatus(string sIsRequested)
    {
        string sStatus = "NON";

        if (sIsRequested.Trim().ToLower() == "true")
            sStatus = "OUI";

        return sStatus;
    }
    public static string getPinpadModelName(string sSerial)
    {
        string sModelName = "";

        if (sSerial != null && sSerial.Trim().Length > 0 && sSerial.Trim().Substring(0, 1).ToUpper() == "D")
            sModelName = "VERIFONE";
        else
            sModelName = "INGENICO";

        return sModelName;
    }
    public static string getPinpadImageName(string sPinpadModelName)
    {
        string sImageName = "";

        if (sPinpadModelName.Trim().Length > 0 && sPinpadModelName.Trim().ToLower() == "verifone")
            sImageName = "Verifone";
        else sImageName = "Ingenico";
        return sImageName;
    }
    public static string getScannerModelName(string sFreeLabel)
    {
        string sModelName = "";

        if (sFreeLabel != null && sFreeLabel.Trim() == "PLUSTEK_550_PLUS")
            sModelName = "PLUSTEK 550 PLUS";
        else
            sModelName = "AVISION AVA5 PLUS";

        return sModelName;
    }
    public static string getScannerImageName(string sScannerModelName)
    {
        string sImageName = "";

        if (sScannerModelName.Trim().Length > 0 && sScannerModelName.Trim() == "PLUSTEK 550 PLUS")
            sImageName = "scanner_Plustek_550_Plus";
        else sImageName = "Scanner";
        return sImageName;
    }
    public static string getTabletSupportModelName(string sSerial)
    {
        string sModelName = "";

        //FPEPI100072
        if (sSerial.Trim().Length > 5 && sSerial.Trim().Substring(0, 5) == "FPECO")
            sModelName = "SANS PIED";
        else
            sModelName = "AVEC PIED";

        return sModelName;
    }
    public static string getTabletSupportImageName(string sTabletSupportModelName)
    {
        string sImageName = "";

        if (sTabletSupportModelName.Trim().Length > 0 && sTabletSupportModelName.Trim().ToUpper() == "SANS PIED")
            sImageName = "TabletSupportLight";
        else sImageName = "TabletSupport";
        return sImageName;
    }

    public static string getSearchValueFromHW(string sHwList, string sHwType, string sSearchValue)
    {
        int nbMaxValues = 2, nbValues = 0;
        string sValuesHighlighted = "";

        if (sHwList.Trim().Length > 0)
        {
            //SUPPRIME DERNIER ';'
            if (sHwList.LastIndexOf(';') == sHwList.Trim().Length - 1)
                sHwList = sHwList.Substring(0, sHwList.Trim().Length - 1);

            string[] hwList = sHwList.Split(';');

            if (hwList.Length > 0)
            {
                for (int i = 0; i < hwList.Length; i++)
                {
                    if (hwList[i].ToUpper().Contains(sSearchValue.ToUpper()) && nbValues < nbMaxValues)
                    {
                        nbValues++;
                        sValuesHighlighted += hwList[i].ToUpper().Trim() + ", ";
                    }
                }

                //SUPPRIME DERNIER ','
                if (sValuesHighlighted.Trim().Length > 0 && sValuesHighlighted.LastIndexOf(',') == sValuesHighlighted.Trim().Length - 1)
                    sValuesHighlighted = sValuesHighlighted.Substring(0, sValuesHighlighted.Trim().Length - 1);

                if (sHwType == "POS" && nbValues > 0)
                    sValuesHighlighted = sValuesHighlighted.Substring(2);

                if (sValuesHighlighted.Trim().Length > 0 && hwList.Length > nbMaxValues)
                    sValuesHighlighted = sValuesHighlighted + ",...";
                else if (sValuesHighlighted.Trim().Length == 0 && hwList.Length > nbMaxValues)
                    sValuesHighlighted = hwList[0].ToUpper().Trim() + ", " + hwList[1].ToUpper().Trim() + ",...";
                else if (sValuesHighlighted.Trim().Length == 0 && hwList.Length == nbMaxValues)
                    sValuesHighlighted = hwList[0].ToUpper().Trim() + ", " + hwList[1].ToUpper().Trim();
                else if (sValuesHighlighted.Trim().Length == 0 && hwList.Length < nbMaxValues)
                    sValuesHighlighted = hwList[0].ToUpper().Trim();
            }
        }

        return sValuesHighlighted;
    }
    public static string getSearchValueFromMPAD(string sMpadList, string sSearchValue)
    {
        int nbMaxValues = 2, nbValues = 0;
        string sValuesHighlighted = "";

        if (sMpadList.Trim().Length > 0)
        {
            //SUPPRIME DERNIER ';'
            if (sMpadList.LastIndexOf(';') == sMpadList.Trim().Length - 1)
                sMpadList = sMpadList.Substring(0, sMpadList.Trim().Length - 1);

            string[] mpadList = sMpadList.Split(';');

            if (mpadList.Length > 0)
            {
                for (int i = 0; i < mpadList.Length; i++)
                {
                    if (mpadList[i].ToUpper().Contains(sSearchValue.ToUpper()) && nbValues < nbMaxValues)
                    {
                        nbValues++;
                        sValuesHighlighted += mpadList[i].ToUpper().Trim() + ", ";
                    }
                }

                //SUPPRIME DERNIER ','
                if (sValuesHighlighted.Trim().Length > 0 && sValuesHighlighted.LastIndexOf(',') == sValuesHighlighted.Trim().Length - 1)
                    sValuesHighlighted = sValuesHighlighted.Substring(0, sValuesHighlighted.Trim().Length - 1);

                if (sValuesHighlighted.Trim().Length > 0 && mpadList.Length > nbMaxValues)
                    sValuesHighlighted = sValuesHighlighted + ",...";
                else if (sValuesHighlighted.Trim().Length == 0 && mpadList.Length > nbMaxValues)
                    sValuesHighlighted = mpadList[0].ToUpper().Trim() + ", " + mpadList[1].ToUpper().Trim() + ",...";
                else if (sValuesHighlighted.Trim().Length == 0 && mpadList.Length == nbMaxValues)
                    sValuesHighlighted = mpadList[0].ToUpper().Trim() + ", " + mpadList[1].ToUpper().Trim();
                else if (sValuesHighlighted.Trim().Length == 0 && mpadList.Length < nbMaxValues)
                    sValuesHighlighted = mpadList[0].ToUpper().Trim();
            }
        }

        return sValuesHighlighted;
    }
    public static string getSearchValueFromPOS(string sPosList, string sSearchValue)
    {
        int nbMaxValues = 2, nbValues = 0;
        string sValuesHighlighted = "";

        if (sPosList.Trim().Length > 0)
        {
            //SUPPRIME DERNIER ';'
            if (sPosList.LastIndexOf(';') == sPosList.Trim().Length - 1)
                sPosList = sPosList.Substring(0, sPosList.Trim().Length - 1);

            string[] posList = sPosList.Split(';');

            if (posList.Length > 0)
            {
                for (int i = 0; i < posList.Length; i++)
                {
                    if (posList[i].Trim().Length > 0 && posList[i].ToUpper().Contains(sSearchValue.ToUpper()) && nbValues < nbMaxValues)
                    {
                        nbValues++;
                        sValuesHighlighted += ", " + posList[i].ToUpper().Trim();
                    }
                }

                //SUPPRIME DERNIER ','
                if (sValuesHighlighted.Trim().Length > 0 && sValuesHighlighted.LastIndexOf(',') == sValuesHighlighted.Trim().Length - 1)
                    sValuesHighlighted = sValuesHighlighted.Substring(0, sValuesHighlighted.Trim().Length - 1);

                if (nbValues > 0)
                    sValuesHighlighted = sValuesHighlighted.Substring(2);

                if (sValuesHighlighted.Trim().Length > 0 && posList.Length > nbMaxValues)
                    sValuesHighlighted = sValuesHighlighted + ",...";
                else if (sValuesHighlighted.Trim().Length == 0 && posList.Length > nbMaxValues)
                    sValuesHighlighted = posList[0].ToUpper().Trim() + ", " + posList[1].ToUpper().Trim() + ",...";
                else if (sValuesHighlighted.Trim().Length == 0 && posList.Length == nbMaxValues)
                    sValuesHighlighted = posList[0].ToUpper().Trim() + ", " + posList[1].ToUpper().Trim();
                else if (sValuesHighlighted.Trim().Length == 0 && posList.Length < nbMaxValues)
                    sValuesHighlighted = posList[0].ToUpper().Trim();
            }
        }

        return sValuesHighlighted;
    }
    public static string getSearchForEmptyValue(string sValue)
    {
        string sSearchForEmptyValue = sValue.Trim();
        if(sValue.Trim().Length == 0)
            sSearchForEmptyValue = "<span style=\"font-style:italic;\">Aucun</span>";
        return sSearchForEmptyValue;
    }

    public static bool MpadReinitCode(string sToken, string sSerial, string sAgencyID, string sRefPartner, out string sCode, out string sError)
    {
        bool isOK = false;
        sCode = ""; sError = "";

        string sXmlOut = "";
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["PARTNER"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("[HW].[P_AddHWToAgency]", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@sXmlIn", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@sXmlOut", SqlDbType.VarChar, -1);

            cmd.Parameters["@sXmlIn"].Value = new XElement("ALL_XML_IN",
                                            new XElement("H",
                                                new XAttribute("HW_Serial", sSerial),
                                                new XAttribute("HW_Type", "MPD"),
                                                new XAttribute("A_ID", sAgencyID),
                                                new XAttribute("P_Ref", sRefPartner),
                                                new XAttribute("TOKEN", sToken))).ToString();

            cmd.Parameters["@sXmlOut"].Direction = ParameterDirection.Output;
            cmd.ExecuteNonQuery();

            sXmlOut = cmd.Parameters["@sXmlOut"].Value.ToString();
            string sRC = tools.GetValueFromXml(sXmlOut, "ALL_XML_OUT/H", "RC");

            if (sRC.Trim() != "0")
                sError = tools.GetValueFromXml(sXmlOut, "ALL_XML_OUT/H", "ERROR_MSG") + "(" + sRC + ")";
            else
            {
                sCode = tools.GetValueFromXml(sXmlOut, "ALL_XML_OUT/H", "MP_Token");
                isOK = true;
            }


            /*** A SUPPRIMER ***/
            //sCode = cmd.Parameters["@sXmlIn"].Value.ToString().Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "");
            //isOK = true;
            /*******************/

        }
        catch (Exception e)
        {
            isOK = false;
            sError = "Une erreur innatendue est survenue.";
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return isOK;
    }
    public static bool addHardwareToAgency(string sToken, string sType, string sSerial, string sAgencyID, string sRefPartner, string sUserComment, out string sCode, out string sError)
    {
        /*** TYPE ***/
        // MPAD : MPD
        // TPE : POS
        // SCANNER : SCN
        // TABLET : TAB
        /************/

        bool isOK = false;
        sCode = ""; sError = "";
        string sXmlOut = "";
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["PARTNER"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("[HW].[P_AddHWToAgency]", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@sXmlIn", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@sXmlOut", SqlDbType.VarChar, -1);

            cmd.Parameters["@sXmlIn"].Value = new XElement("ALL_XML_IN",
                                            new XElement("H",
                                                new XAttribute("HW_Serial", sSerial),
                                                new XAttribute("HW_Type", sType),
                                                new XAttribute("A_ID", sAgencyID),
                                                new XAttribute("P_Ref", sRefPartner),
                                                new XAttribute("sUserComment", sUserComment),
                                                new XAttribute("TOKEN", sToken))).ToString();

            cmd.Parameters["@sXmlOut"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();
            sXmlOut = cmd.Parameters["@sXmlOut"].Value.ToString();

            string sRC = tools.GetValueFromXml(sXmlOut, "ALL_XML_OUT/H", "RC");

            if (sRC.Trim() != "0")
                sError = tools.GetValueFromXml(sXmlOut, "ALL_XML_OUT/H", "ERROR_MSG") + "(" + sRC + ")";
            else
            {
                sCode = tools.GetValueFromXml(sXmlOut, "ALL_XML_OUT/H", "MP_Token");
                isOK = true;
            }

        }
        catch (Exception e)
        {
            isOK = false;
            sError = "Une erreur innatendue est survenue.";
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return isOK;
    }

    public static string GetAgencyDetails(int iRefAgency, string sToken)
    {
        string sXmlOUT = "";
        SqlConnection conn = null;

        string sXmlIN = new XElement("ALL_XML_IN", new XElement("Shop",
                                                    new XAttribute("BOUserToken", sToken),
                                                    new XAttribute("RefAgency", iRefAgency))).ToString();

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("BankSI.P_GetShopGeneralInformation", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);

            cmd.Parameters["@IN"].Value = sXmlIN;
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            sXmlOUT = cmd.Parameters["@OUT"].Value.ToString();
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return sXmlOUT;
    }
    public static string GetAgencyActivity(int iRefAgency, string sMode, string sToken)
    {
        string sXmlOUT = "";
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_GetMerchantActivityInfos", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@sXmlIn", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@sXmlOut", SqlDbType.VarChar, 8000);

            cmd.Parameters["@sXmlIn"].Value = new XElement("ALL_XML_IN", new XElement("MerchantActivityInfos",
                                                                        new XAttribute("iRefAgency", iRefAgency),
                                                                        new XAttribute("iReportMode", sMode),
                                                                        new XAttribute("TOKEN", sToken))).ToString();
            cmd.Parameters["@sXmlOut"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            sXmlOUT = cmd.Parameters["@sXmlOut"].Value.ToString();
        }
        catch (Exception e)
        { }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return sXmlOUT;
    }

    public static bool HasAlertRights(string sPage, string sCashierToken, out bool bView, out bool bAction)
    {
        bView = false;
        bAction = false;
        bool bHasRights = false;
        string sXmlOut = tools.GetRights(authentication.GetCurrent().sToken, sPage);
        List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Page", "RC");

        if (listRC.Count > 0 && listRC[0] == "0")
        {
            List<string> listAction = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/Page/Action");

            for (int i = 0; i < listAction.Count; i++)
            {
                List<string> listTag = CommonMethod.GetAttributeValues(listAction[i], "Action", "TagAction");
                List<string> listActionAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ActionAllowed");
                List<string> listViewAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ViewAllowed");

                if (listTag.Count > 0 && listTag[0] == "AgencyAlert")
                {
                    if (listActionAllowed[0] == "1")
                        bAction = true;
                    if (listViewAllowed[0] == "1")
                        bView = true;
                    if (bAction || bView)
                        bHasRights = true;
                    break;
                }
            }
        }

        return bHasRights;
    }

    public static bool AddCobaltPointOfSale(string sAccountNumber)
    {
        bool isOK = false;

        //[PDV].[P_PushPointOfSaleCobalt]  @AccountNumber VARCHAR(20)
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("[PDV].[P_PushPointOfSaleCobalt]", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@AccountNumber", SqlDbType.VarChar, 8000);

            cmd.Parameters["@AccountNumber"].Value = sAccountNumber;
            cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;
            cmd.ExecuteNonQuery();

            string sRC = cmd.Parameters["@RC"].Value.ToString();
            if (sRC.Trim() == "0") { isOK = true; }
        }
        catch (Exception e)
        { isOK = false; }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return isOK;
    }
}