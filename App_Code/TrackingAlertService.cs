﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using XMLMethodLibrary;


/// <summary>
/// Description résumée de TrackingAlert
/// </summary>
public class TrackingAlertService
{
    
    public TrackingAlertService()
    {
        //
        // TODO: ajoutez ici la logique du constructeur
        //
        
    }

    public List<AgentNickel> GetConformityAgentList()
    {
        List<AgentNickel> list = new List<AgentNickel>();


        return list;
    }

    /// <summary>
    /// Retourne la liste des Agents conformités avec leurs alertes associées
    /// </summary>
    /// <param name="sToken"></param>
    /// <param name="daysCriticalAlert"></param>
    /// <returns></returns>
    public static List<TrackingAlert> GetTrackingAlertList(string sToken, int daysCriticalAlert)
    {
        List<TrackingAlert> listeTrackingAlert = new List<TrackingAlert>();
        try
        {
            string sXmlOut_GetManagedAlertsInformation = string.Empty;
            #region Liste des Agents
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString))
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand("AML.P_GetManagedAlertsInformation", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
                cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);

                cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                                new XElement("ManagedAlerts",
                                                    new XAttribute("CashierToken", sToken)
                                                    )).ToString();
                cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                sXmlOut_GetManagedAlertsInformation = cmd.Parameters["@OUT"].Value.ToString();
                var listeAlerteTousBO = GetListeAlertToBO(conn);
                if (sXmlOut_GetManagedAlertsInformation.Length > 0)
                {

                    List<string> listManagedAlerts = CommonMethod.GetTags(sXmlOut_GetManagedAlertsInformation, "ALL_XML_OUT/ManagedAlerts");
                    if (listManagedAlerts.Count > 0)
                    {
                        for (int i = 0; i < listManagedAlerts.Count; i++)
                        {

                            List<string> listRefBoUser = CommonMethod.GetAttributeValues(listManagedAlerts[i], "ManagedAlerts", "RefBOUser");
                            List<string> listBoUserName = CommonMethod.GetAttributeValues(listManagedAlerts[i], "ManagedAlerts", "BoUserName");

                            var trackingAlert = new TrackingAlert(daysCriticalAlert);
                            trackingAlert.BOUserName = listBoUserName[0];
                            trackingAlert.BOUserRef = listRefBoUser[0];
                            if(listeAlerteTousBO.Any(lst => lst.RefBOUser == trackingAlert.BOUserRef))
                                trackingAlert.AlertList = listeAlerteTousBO.Where(lst => lst.RefBOUser == trackingAlert.BOUserRef).ToList();
                            listeTrackingAlert.Add(trackingAlert);
                        }


                    }
                }

            }
            #endregion Fin Liste des Agents
            
        }
        catch (Exception e)
        {

        }
        finally
        {

        }
        return listeTrackingAlert.OrderByDescending(p => p.NumberCriticalAlert).ToList();
    }


    
    private static List<Alert> GetListeAlertToBO(SqlConnection conn )
    {
        List<Alert> listeAlertTracking = new List<Alert>();

        try
        {
            

                SqlCommand cmd = new SqlCommand("AML.P_GetAlertEvents", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
                cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);

                XElement xml = new XElement("Alert",
                                    new XAttribute("CashierToken", authentication.GetCurrent().sToken),
                                    new XAttribute("Treated", "0"));
                cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN", xml).ToString();
                cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

                cmd.ExecuteNonQuery();

                string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

                if (sXmlOut.Length > 0)
                {
                    List<string> listAlert = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/Alert");

                    if (listAlert.Count > 0)
                    {
                       

                        for (int x = 0; x < listAlert.Count; x++)
                        {
                            Alert alert = new Alert();

                            List<string> listRefAlert = CommonMethod.GetAttributeValues(listAlert[x], "Alert", "refAlert");
                            List<string> listAlertLabel = CommonMethod.GetAttributeValues(listAlert[x], "Alert", "AlertLabel");
                            List<string> listRefCustomer = CommonMethod.GetAttributeValues(listAlert[x], "Alert", "RefCustomer");
                            List<string> listCustomerPoliteness = CommonMethod.GetAttributeValues(listAlert[x], "Alert", "Politeness");
                            List<string> listCustomerLastName = CommonMethod.GetAttributeValues(listAlert[x], "Alert", "CustomerLastName");
                            List<string> listCustomerFirstName = CommonMethod.GetAttributeValues(listAlert[x], "Alert", "CustomerFirstName");
                            List<string> listNotification = CommonMethod.GetAttributeValues(listAlert[x], "Alert", "AlertNotification");
                            List<string> listNotificationKind = CommonMethod.GetAttributeValues(listAlert[x], "Alert", "TagAlertNotificationKind");
                            List<string> listNotificationLabel = CommonMethod.GetAttributeValues(listAlert[x], "Alert", "AlertNotificationLabel");
                            List<string> listRefBOUser = CommonMethod.GetAttributeValues(listAlert[x], "Alert", "RefBOUser"); 

                            List<string> listEvents = CommonMethod.GetTags(listAlert[x], "Alert/Events/AlertEvent");

                            string sCustomerFullName = listCustomerPoliteness[0] + " " + listCustomerFirstName[0] + " " + listCustomerLastName[0];

                            alert.CustomerName = sCustomerFullName;
                            alert.AlertLabel = listAlertLabel[0];
                            alert.RefAlert = listRefAlert[0];
                            StringBuilder sbStepMarkers = new StringBuilder();

                            // Recuperation date ORIGINE ZERO
                            string sOriginDate = "";
                            for (int y = 0; y < listEvents.Count; y++)
                            {
                                List<string> listDate = CommonMethod.GetAttributeValues(listEvents[y], "AlertEvent", "Date");
                                List<string> listTag = CommonMethod.GetAttributeValues(listEvents[y], "AlertEvent", "Tag");

                                string sDate = (listDate.Count > 0) ? listDate[0] : "";

                               
                                if (listTag[0] == "COUNTERPART"
                                    || listTag[0] == "FUNDSOURCE"
                                    || listTag[0] == "LOCKACCOUNTINFO"
                                    || listTag[0] == "SELFEMPLOYEDSPLIT"
                                    )
                                    sOriginDate = sDate;


                                if (listTag[0] == "REASSIGN")
                                {
                                    alert.IsReassigned = true;
                                    alert.ReassignedDate = DateTime.Parse( sDate);
                                }
                            }

                            // Parsing EVENTS
                            for (int y = 0; y < listEvents.Count; y++)
                            {
                                List<string> listDate = CommonMethod.GetAttributeValues(listEvents[y], "AlertEvent", "Date");
                                List<string> listLabel = CommonMethod.GetAttributeValues(listEvents[y], "AlertEvent", "Label");
                                List<string> listBOUser = CommonMethod.GetAttributeValues(listEvents[y], "AlertEvent", "BOUser");
                                List<string> listTag = CommonMethod.GetAttributeValues(listEvents[y], "AlertEvent", "Tag");

                                string sDate = (listDate.Count > 0) ? listDate[0] : "";
                                string sNbDays = "";
                                if (!String.IsNullOrWhiteSpace(sDate))
                                    sNbDays = tools.GetNbDaysFromDate(sDate, sOriginDate).ToString();

                                string sIcon = "";
                                switch (listTag[0])
                                {
                                    case "GENERATION":
                                        sIcon = "alarm";
                                        break;
                                    case "FIRSTCHECK":
                                        sIcon = "visibility";
                                        break;
                                    case "COUNTERPART":
                                    case "FUNDSOURCE":
                                    case "LOCKACCOUNTINFO":
                                    case "SELFEMPLOYEDSPLIT":
                                        sIcon = "mail_outline";
                                        break;
                                    case "NoContact":
                                        sIcon = "mic_off";
                                        break;
                                    case "ContactCounterpartAgain":
                                    case "RELANCEJ+7":
                                    case "RELANCEJ+14":
                                    case "RELANCEJ+30":
                                        sIcon = "email";
                                        break;
                                    case "UnfreezeAccount":
                                        sIcon = "lock_open";
                                        break;
                                    case "CONTACTCLIENTPHONE":
                                    case "CallCounterpart":
                                        sIcon = "phone";
                                        break;
                                    case "AlertResponseOK":
                                        sIcon = "sentiment_satisfied";
                                        break;
                                    case "AlertResponseKO":
                                        sIcon = "sentiment_dissatisfied";
                                        break;
                                    case "AlertResponseNotSatisfactory":
                                        sIcon = "sentiment_neutral";
                                        break;
                                    case "CounterpartAnswer":
                                    case "CallCounterpartAnswerOk":
                                        sIcon = "thumb_up";
                                        break;
                                    case "CounterpartNoAnwser":
                                        sIcon = "thumb_down";
                                        break;
                                    case "CallCounterpartAnswerPending":
                                        sIcon = "more_horiz";
                                        break;
                                    case "REASSIGN":
                                        sIcon = "supervisor_account";
                                        break;
                                    default:
                                        sIcon = "fiber_manual_record";
                                        break;
                                }
                                
                                var step = new Step() { Date = DateTime.Parse(sDate), Days = sNbDays, Label = listLabel[0], Icon = string.Empty, BOUser = (listBOUser.Count > 0) ? listBOUser[0] : "" };
                                alert.ListeStep.Add(step);
                                alert.RefBOUser = listRefBOUser[0]; 


                            }

                            // Creation markers
                            DataTable dtEventMarkers = new DataTable();
                            dtEventMarkers.Columns.Add("Days");
                            dtEventMarkers.Columns.Add("OverLabel");
                            dtEventMarkers.Columns.Add("BottomLabel");
                       
                        foreach (var step in  alert.ListeStep.OrderByDescending(p=>p.Date).ToList())
                        {
                            if (dtEventMarkers.Rows.Count > 0)
                            {
                                // Même jour marker précédent
                                if (step.Days == dtEventMarkers.Rows[dtEventMarkers.Rows.Count - 1]["Days"].ToString())
                                {
                                    dtEventMarkers.Rows[dtEventMarkers.Rows.Count - 1]["OverLabel"] += "<br/>" + step.Label;
                                    dtEventMarkers.Rows[dtEventMarkers.Rows.Count - 1]["BottomLabel"] += step.Icon;
                                }
                                else
                                {
                                    dtEventMarkers.Rows.Add(new object[] { step.Days, step.Label, step.Icon });
                                }
                            }
                            else
                            {
                                dtEventMarkers.Rows.Add(new object[] { step.Days, step.Label, step.Icon });
                            }
                            break;
                        }
                        for (int i = 0; i < dtEventMarkers.Rows.Count; i++)
                            {
                                sbStepMarkers.Append(dtEventMarkers.Rows[i]["Days"] + ";" + dtEventMarkers.Rows[i]["OverLabel"] + ";" + dtEventMarkers.Rows[i]["BottomLabel"] + "|");
                            }
                            string sStepMarkers = sbStepMarkers.ToString().TrimEnd('|');
                            int iCurrentStep = tools.GetNbDaysFromDate(DateTime.Now.ToString("dd/MM/yyyy"), sOriginDate);
                            alert.StepMarkers = sStepMarkers;
                            alert.CurrentStep = iCurrentStep;
                            
                            listeAlertTracking.Add(alert);


                       
                        }
                    }
                
            }
        }
        catch (Exception e)
        {

        }
        finally
        {
        }

        return listeAlertTracking;
    }
}