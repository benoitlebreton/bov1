﻿using System;

/// <summary>
/// Description résumée de beneficiary
/// </summary>

[Serializable]
public class Beneficiary
{
    public int Id { get; set; }
    public string Reference { get; set; }
    public string Label { get; set; }
    public string Name { get; set; }
    public string Bic { get; set; }
    public string Iban { get; set; }
    public string BankName { get; set; }
    public string Email { get; set; }
    public bool IsChecked { get; set; }
    public DateTime CheckedDate { get; set; }
    public DateTime DeletedDate { get; set; }

    public string BeneficiaryAndBankName
    {
        get
        {
            return this.Name + " (" + this.BankName + ")";
        }
    }

   

    public BeneficiaryStatutEnum Statut
    {
        get
        {
            BeneficiaryStatutEnum statut;

            // Si il a été validé
            if (IsChecked)
            {
                statut = BeneficiaryStatutEnum.Ok;
                // Puis supprimé
                if (DeletedDate != DateTime.MinValue)
                    statut = BeneficiaryStatutEnum.OkThenDelete;
            }
            else
            {
                statut = BeneficiaryStatutEnum.Error;
                // Puis supprimé
                if (DeletedDate != DateTime.MinValue)
                    statut = BeneficiaryStatutEnum.ErrorThenDelete;
            }


            return statut;
        }
    }



}