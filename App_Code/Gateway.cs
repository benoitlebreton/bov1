﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using XMLMethodLibrary;

/// <summary>
/// Description résumée de Gateway
/// </summary>
/// 
[Serializable]
public class Gateway
{
    protected static DataTable getGatewayLastTransactionsFromXML(string sXML)
    {
        DataTable dt = new DataTable();

        dt.Columns.Add("DateRemontee");
        dt.Columns.Add("Montant");
        dt.Columns.Add("Carte");
        dt.Columns.Add("MessageType");
        dt.Columns.Add("CodeReponse");
        dt.Columns.Add("Champ54");
        dt.Columns.Add("Commerce");
        dt.Columns.Add("Mode");
        dt.Columns.Add("AlertStatus");

        List<string> lsTRX = CommonMethod.GetTags(sXML, "ALL_XML_OUT/Gateway/TRX");

        List<string> lsDateRemontee = new List<string>();
        List<string> lsMontant = new List<string>();
        List<string> lsCarte = new List<string>();
        List<string> lsMessageType = new List<string>();
        List<string> lsCodeReponse = new List<string>();
        List<string> lsChamp54 = new List<string>();
        List<string> lsCommerce = new List<string>();
        List<string> lsMode = new List<string>();
        string sDateRemontee = "";
        string sMontant = "";
        string sCarte = "";
        string sMessageType = "";
        string sCodeReponse = "";
        string sChamp54 = "";
        string sCommerce = "";
        string sMode = "";
        int iNb120 = 0;
        int iRowIndexEnd = dt.Rows.Count;
        int iRowIndexBegin = 0;

        /*** BEGIN TEST ***/
        //int iForce120 = 10;
        //int iRow = 1;
        /*** END TEST ***/

        foreach (string sTRX in lsTRX)
        {
            lsDateRemontee = CommonMethod.GetAttributeValues(sTRX, "TRX", "DateRemontee");
            lsMontant = CommonMethod.GetAttributeValues(sTRX, "TRX", "Montant");
            lsCarte = CommonMethod.GetAttributeValues(sTRX, "TRX", "Carte");
            lsMessageType = CommonMethod.GetAttributeValues(sTRX, "TRX", "MessageType");
            lsCodeReponse = CommonMethod.GetAttributeValues(sTRX, "TRX", "CodeReponse");
            lsChamp54 = CommonMethod.GetAttributeValues(sTRX, "TRX", "Champ54");
            lsCommerce = CommonMethod.GetAttributeValues(sTRX, "TRX", "Commerce");
            lsMode = CommonMethod.GetAttributeValues(sTRX, "TRX", "GatewayMode");
            sDateRemontee = "";
            sMontant = "";
            sCarte = "";
            sMessageType = "";
            sCodeReponse = "";
            sChamp54 = "";
            sCommerce = "";
            sMode = "";

            if (lsDateRemontee != null && lsDateRemontee.Count > 0)
                sDateRemontee = lsDateRemontee[0].ToString();
            if (lsMontant != null && lsMontant.Count > 0)
                sMontant = lsMontant[0].ToString();
            if (lsCarte != null && lsCarte.Count > 0)
                sCarte = lsCarte[0].ToString();
            if (lsMessageType != null && lsMessageType.Count > 0)
                sMessageType = lsMessageType[0].ToString();//sMessageType = "0120";
            if (lsCodeReponse != null && lsCodeReponse.Count > 0)
                sCodeReponse = lsCodeReponse[0].ToString();
            if (lsChamp54 != null && lsChamp54.Count > 0)
                sChamp54 = lsChamp54[0].ToString();
            if (lsCommerce != null && lsCommerce.Count > 0)
                sCommerce = lsCommerce[0].ToString();
            if (lsMode != null && lsMode.Count > 0)
                sMode = lsMode[0].ToString();

            /*** BEGIN TEST ***/
            /*if(iForce120 > 0 && iRow > 0)
            {
                iForce120--;
                sMessageType = "0120";
            }
            iRow++;*/
            /*** END TEST ***/

            if (sMessageType.Trim() == "0120")
                iNb120++;
            else iNb120 = 0;

            if(iNb120>=5)
            {
                iRowIndexEnd = dt.Rows.Count-1;
                iRowIndexBegin = iRowIndexEnd - iNb120;
                for(int i = iRowIndexEnd; i > iRowIndexBegin+1; i--)
                    dt.Rows[i]["AlertStatus"] = "1";

                dt.AcceptChanges();
                dt.Rows.Add(new object[] { sDateRemontee, sMontant, sCarte, sMessageType, sCodeReponse, sChamp54, sCommerce, sMode, "1" });
            }
            else
                dt.Rows.Add(new object[] { sDateRemontee, sMontant, sCarte, sMessageType, sCodeReponse, sChamp54, sCommerce, sMode, "0" });
        }

        return dt;
    }

    public static bool getGatewayLastTransactions(out int iTotalTrx, out int iLast10Trx, out DataTable dtGatewayLastTransactions)
    {
        bool isOK = false; iTotalTrx = -1; iLast10Trx = -1;
        dtGatewayLastTransactions = new DataTable();
        SqlConnection conn = null;
        authentication auth = authentication.GetCurrent();

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("WEB.P_GetGatewayOperation", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@ReturnSelect", SqlDbType.Bit);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                                new XElement("Gateway",
                                                    new XAttribute("CashierToken", auth.sToken),
                                                    new XAttribute("CultureID", auth.sCulture))).ToString();
            cmd.Parameters["@ReturnSelect"].Value = false;

            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXml = cmd.Parameters["@OUT"].Value.ToString();

            List<string> lsRC = CommonMethod.GetAttributeValues(sXml, "ALL_XML_OUT/Gateway", "RC");
            List<string> lsNbTrxTot = CommonMethod.GetAttributeValues(sXml, "ALL_XML_OUT/Gateway", "NbTrxTot");
            List<string> lsTrxLast10Min = CommonMethod.GetAttributeValues(sXml, "ALL_XML_OUT/Gateway", "NbTrxLast10Minutes");

            if (lsRC != null && lsRC.Count > 0 && lsRC[0] == "0")
                isOK = true;

            if (isOK)
            {
                if (!(lsNbTrxTot != null && lsNbTrxTot.Count > 0 && int.TryParse(lsNbTrxTot[0].ToString(), out iTotalTrx)))
                    isOK = false;

                if (!(lsTrxLast10Min != null && lsTrxLast10Min.Count > 0 && int.TryParse(lsTrxLast10Min[0].ToString(), out iLast10Trx)))
                    isOK = false;

                dtGatewayLastTransactions = getGatewayLastTransactionsFromXML(sXml);
                //rptGatewayMonitoring.DataSource = getGatewayLastTransactionsFromXML(sXml);
                //rptGatewayMonitoring.DataBind();
            }

        }
        catch (Exception e)
        {
            isOK = false;
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return isOK;
    }

    [Serializable]
    public class GatewayStatus
    {
        public string Status { get; set; }
        public string StatusUntil { get; set; }
        public string CurrentMode { get; set; }
        public string RequestedMode { get;set;}
        public string NbTrxToInjectInSab { get; set; }
        public string NbAccounts { get; set; }
        public string NbPositiveAccounts { get; set; }
        public string LastBalanceRefreshDate { get; set; }
    }
    public static bool getGatewayStatus(out GatewayStatus status, out DataTable dtDetails)
    {
        bool isOK = true;
        SqlConnection conn = null;
        authentication auth = authentication.GetCurrent();
        status = new GatewayStatus();
        dtDetails = new DataTable();

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            /*
            DECLARE @IN VARCHAR(MAX),@OUT VARCHAR(MAX),@RC INT
                        
            SET @IN = '<ALL_XML_IN><Gateway CashierToken="HMB" CultureID="fr-FR"/></ALL_XML_IN>'
                                                                                
            EXEC @RC = [Web].[P_GetGatewayStatus] @IN, @OUT OUTPUT,1
            SELECT @RC,@OUT
            */

            SqlCommand cmd = new SqlCommand("WEB.P_GetGatewayStatus", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@ReturnSelect", SqlDbType.Bit);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);
            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                                new XElement("Gateway",
                                                    new XAttribute("CashierToken", auth.sToken),
                                                    new XAttribute("CultureID", auth.sCulture))).ToString();
            cmd.Parameters["@ReturnSelect"].Value = false;
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
            cmd.ExecuteNonQuery();

            string sXml = cmd.Parameters["@OUT"].Value.ToString();
            //<ALL_XML_OUT><Gateway GatewayMode="D" NbTrx="0" NbAccounts="0" NbPositiveAccounts="0" LastBalanceRefreshDate="11/04/2016 12:48:56" GatewayModeRequested="D"/></ALL_XML_OUT>

            List<string> lsRC = CommonMethod.GetAttributeValues(sXml, "ALL_XML_OUT/Gateway", "RC");
            List<string> lsRequestedMode = CommonMethod.GetAttributeValues(sXml, "ALL_XML_OUT/Gateway", "GatewayModeRequested");
            List<string> lsCurrentMode = CommonMethod.GetAttributeValues(sXml, "ALL_XML_OUT/Gateway", "GatewayMode");
            List<string> lsNbTrxToInjectInSab = CommonMethod.GetAttributeValues(sXml, "ALL_XML_OUT/Gateway", "NbTrx");
            List<string> lsNbAccounts = CommonMethod.GetAttributeValues(sXml, "ALL_XML_OUT/Gateway", "NbAccounts");
            List<string> lsNbPositiveAccounts = CommonMethod.GetAttributeValues(sXml, "ALL_XML_OUT/Gateway", "NbPositiveAccounts");
            List<string> lsLastBalanceRefreshDate = CommonMethod.GetAttributeValues(sXml, "ALL_XML_OUT/Gateway", "LastBalanceRefreshDate");
            List<string> lsStatus = CommonMethod.GetAttributeValues(sXml, "ALL_XML_OUT/Gateway", "GatewayStatus");
            List<string> lsStatusUntil = CommonMethod.GetAttributeValues(sXml, "ALL_XML_OUT/Gateway", "StatusUntil");
            List<string> lsAlertsInfo = CommonMethod.GetTags(sXml, "ALL_XML_OUT/Gateway/AlertsInfo");

            //if (lsRC != null && lsRC.Count > 0 && lsRC[0] == "0")
            //isOK = true;

            //if (isOK)
            //{

            if (lsStatus.Count > 0 && lsStatus[0].ToString().Trim().Length > 0)
                status.Status = lsStatus[0].ToString();
            else
            {
                isOK = false;
                status.Status = "NON RECUP&Eacute;R&Eacute;";
            }

            if (lsRequestedMode.Count > 0 && lsRequestedMode[0].ToString().Trim().Length > 0)
                status.RequestedMode = lsRequestedMode[0].ToString();
            else
            {
                isOK = false;
                status.RequestedMode = "NON RECUP&Eacute;R&Eacute;";
            }

            if (lsCurrentMode.Count > 0 && lsCurrentMode[0].ToString().Trim().Length > 0)
                status.CurrentMode = lsCurrentMode[0].ToString();
            else
            {
                isOK = false;
                status.CurrentMode = "NON RECUP&Eacute;R&Eacute;";
            }

            int iNbTrxToInjectInSab = 0;
            if ((lsRequestedMode.Count > 0 && int.TryParse(lsNbTrxToInjectInSab[0].ToString(), out iNbTrxToInjectInSab)))
                status.NbTrxToInjectInSab = iNbTrxToInjectInSab.ToString();
            else
            {
                isOK = false;
                status.NbTrxToInjectInSab = "NON RECUP&Eacute;R&Eacute;";
            }

            int iNbAccount = 0;
            if ((lsNbAccounts.Count > 0 && int.TryParse(lsNbAccounts[0].ToString(), out iNbAccount)))
                status.NbAccounts = iNbAccount.ToString();
            else
            {
                isOK = false;
                status.NbAccounts = "NON RECUP&Eacute;R&Eacute;";
            }

            int iNbPositiveAccounts = 0;
            if ((lsNbPositiveAccounts.Count > 0 && int.TryParse(lsNbPositiveAccounts[0].ToString(), out iNbPositiveAccounts)))
                status.NbPositiveAccounts = iNbPositiveAccounts.ToString();
            else
            {
                status.NbPositiveAccounts = "NON RECUP&Eacute;R&Eacute;";
                isOK = false;
            }

            DateTime dtLastBalanceRefreshDate = new DateTime();
            if ((lsLastBalanceRefreshDate.Count > 0 && DateTime.TryParse(lsLastBalanceRefreshDate[0].ToString(), out dtLastBalanceRefreshDate)))
                status.LastBalanceRefreshDate = dtLastBalanceRefreshDate.ToString("dd/MM/yyyy HH:mm:ss");
            else
            {
                status.LastBalanceRefreshDate = "NON RECUP&Eacute;R&Eacute;";
            }

            DateTime dtStatusUntil = new DateTime();
            if ((lsStatusUntil.Count > 0 && DateTime.TryParse(lsStatusUntil[0].ToString(), out dtStatusUntil)))
                status.StatusUntil = dtStatusUntil.ToString("dd/MM/yyyy HH:mm:ss");
            else
            {
                status.StatusUntil = "NON RECUP&Eacute;R&Eacute;";
                isOK = false;
            }

             getGatewayStatusDetails(lsAlertsInfo, out dtDetails);

            //}

        }
        catch (Exception e)
        {
            isOK = false;
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return isOK;
    }
    protected static bool getGatewayStatusDetails(List<string> AlertsInfoXml, out DataTable dtDetails)
    {
        bool isOk = true;
        dtDetails = new DataTable();
        dtDetails.Columns.Add("Row");
        dtDetails.Columns.Add("Date");
        dtDetails.Columns.Add("NbThread");
        dtDetails.Columns.Add("Nb100");
        dtDetails.Columns.Add("Nb110");
        dtDetails.Columns.Add("Nb120");
        dtDetails.Columns.Add("Nb120Accepte");
        dtDetails.Columns.Add("NbRow");

        if(AlertsInfoXml.Count > 0)
        {
            List<string> lsAlertInfo = CommonMethod.GetTags(AlertsInfoXml[0], "AlertsInfo/AlertInfo");

            List<string> lsDateInfo = new List<string>();
            List<string> lsNbThread = new List<string>();
            List<string> lsNb100 = new List<string>();
            List<string> lsNb110 = new List<string>();
            List<string> lsNb120 = new List<string>();
            List<string> lsNb120Accepte = new List<string>();
            List<string> lsNbRow = new List<string>();
            for (int i = 0; i < lsAlertInfo.Count; i++)
            {
                lsDateInfo = CommonMethod.GetAttributeValues(lsAlertInfo[i], "AlertInfo", "DateInfo");
                lsNbThread = CommonMethod.GetAttributeValues(lsAlertInfo[i], "AlertInfo", "NbThread");
                lsNb100 = CommonMethod.GetAttributeValues(lsAlertInfo[i], "AlertInfo", "Nb100");
                lsNb110 = CommonMethod.GetAttributeValues(lsAlertInfo[i], "AlertInfo", "Nb110");
                lsNb120 = CommonMethod.GetAttributeValues(lsAlertInfo[i], "AlertInfo", "Nb120");
                lsNb120Accepte = CommonMethod.GetAttributeValues(lsAlertInfo[i], "AlertInfo", "Nb120Accepte");
                lsNbRow = CommonMethod.GetAttributeValues(lsAlertInfo[i], "AlertInfo", "NbRow");

                if (lsDateInfo.Count > 0 && lsNbThread.Count > 0 && lsNb100.Count > 0 && lsNb110.Count > 0 && lsNb120.Count > 0 && lsNb120Accepte.Count > 0 && lsNbRow.Count > 0)
                    dtDetails.Rows.Add(new object[] { i.ToString() , lsDateInfo[0], lsNbThread[0], lsNb100[0], lsNb110[0], lsNb120[0], lsNb120Accepte[0], lsNbRow[0] });
                else
                {
                    dtDetails.Rows.Add(new object[] { i.ToString(), "Erreur", "", "", "", "", "", "" });
                    isOk = false;
                }
            }
        }

        return isOk;
    }

    public static bool setGatewayStatus(string sStatusRequested)
    {
        bool isOK = false;
        /*
            DECLARE @IN VARCHAR(MAX),@OUT VARCHAR(MAX),@RC INT
            SET @IN = '<ALL_XML_IN><Gateway CashierToken="HMB" Culture="fr-FR" Mode="T"/></ALL_XML_IN>'
            EXEC @RC = [Web].[P_ChangeGatewayMode] @IN,@OUT OUTPUT
                                
            SELECT @RC,@OUT
        */
        SqlConnection conn = null;
        authentication auth = authentication.GetCurrent();

        if (sStatusRequested.Trim().Length > 0)
        {
            try
            {
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
                conn.Open();

                /*
                DECLARE @IN VARCHAR(MAX),@OUT VARCHAR(MAX),@RC INT

                SET @IN = '<ALL_XML_IN><Gateway CashierToken="HMB" CultureID="fr-FR"/></ALL_XML_IN>'

                EXEC @RC = [Web].[P_GetGatewayStatus] @IN, @OUT OUTPUT,1
                SELECT @RC,@OUT
                */

                SqlCommand cmd = new SqlCommand("WEB.P_ChangeGatewayMode", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
                cmd.Parameters.Add("@RC", SqlDbType.Int);
                cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);
                cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                                    new XElement("Gateway",
                                                        new XAttribute("CashierToken", auth.sToken),
                                                        new XAttribute("CultureID", auth.sCulture),
                                                        new XAttribute("Mode", sStatusRequested))).ToString();
                cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
                cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;
                cmd.ExecuteNonQuery();

                string sXml = cmd.Parameters["@OUT"].Value.ToString();

                List<string> lsRC = CommonMethod.GetAttributeValues(sXml, "ALL_XML_OUT/GATEWAY", "RC");

                if (cmd.Parameters["@RC"].Value.ToString().Trim() == "0" && lsRC != null && lsRC.Count > 0 && lsRC[0] == "0")
                    isOK = true;
            }
            catch (Exception e)
            {
                isOK = false;
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        return isOK;
    }

    public static string getCssFromStatus(string sMessageType, string sReponseCode, string sAlert)
    {
        string sColor = "";

        if (sAlert.Trim() == "1")
            sColor = "color:red";
        else
        {
            if (sMessageType.Trim() == "0120" && sReponseCode.Trim() == "05") { sColor = "color:red"; }
            else if (sReponseCode.Trim() != "00") { sColor = "color:orange"; }
        }

        return sColor;
    }

    public static string getModeLabel(string sMode)
    {
        switch(sMode.Trim().ToUpper())
        {
            case "T":
                return "Transparent";
            case "D":
                return "Délégation";
            case "H":
                return "Hybride";
            case "I":
                return "Inconnu";
            default: return "Inconnu (" + sMode + ")";
        }
    }
    public static string getModeValue(string sModeLabel)
    {
        string sValue = "";

        switch (sModeLabel.Trim().ToUpper())
        {
            case "TRANSPARENT":
                sValue = "T";
                break;
            case "DÉLÉGATION":
                sValue = "D";
                break;
            case "HYBRIDE":
                sValue = "H";
                break;
        }

        return sValue;
    }

    public static string getStatusLabel(string sStatus, string sDateUntil)
    {
        string sLabel = "";

        string sUntil = "";
        if (sDateUntil != null && sDateUntil.Trim().Length > 0)
            sUntil = " depuis le " + sDateUntil;

        if (sStatus.Trim().ToUpper() == "ON")
            sLabel = "<span class=\"bold\" style=\"color:green\">Activée</span>" + sUntil;
        else
            sLabel = "<span class=\"bold\" style=\"color:red\">Désactivée</span>" + sUntil;
        return sLabel;
    }

    public static DataTable getLastImportedSabFiles()
    {
        DataTable dt = new DataTable();

        SqlConnection conn = null;
        authentication auth = authentication.GetCurrent();

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Nickel"].ConnectionString);
            conn.Open();
            SqlCommand cmd = new SqlCommand("Nickel.P_GetLastImportedSabFiles", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }

    public static string getSabFileNameFromPath(string sPath)
    {
        return sPath.Replace("D:\\FICHIERS\\EXPORTS\\DWH\\","");
    }

    public static bool importSabFiles()
    {
        bool isOK = false;
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Nickel"].ConnectionString);
            conn.Open();
            SqlCommand cmd = new SqlCommand("[Tools].StartJob", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@JobName", SqlDbType.NVarChar, 128);
            cmd.Parameters.Add("@ReturnCode", SqlDbType.TinyInt);
            cmd.Parameters["@JobName"].Value = "GTW.Delegation";
            cmd.ExecuteNonQuery();

            int rc = -1;
            if (int.TryParse(cmd.Parameters["@ReturnCode"].ToString(), out rc) && rc == 0)
                isOK = true;
        }
        catch (Exception e)
        {
            isOK = false;
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return isOK;
    }

}