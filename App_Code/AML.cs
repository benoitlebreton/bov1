﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml.Linq;
using XMLMethodLibrary;

/// <summary>
/// Description résumée de AML
/// </summary>
/// 
[Serializable]
public class AML
{
    public static DataTable getClientAlertList(string xmlIn, out string xmlOut)
    {
        DataTable dt = new DataTable();

        SqlConnection conn = null;
        xmlOut = "";

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("AML.P_SearchAlert", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@ReturnSelect", SqlDbType.Bit);

            cmd.Parameters["@IN"].Value = xmlIn;
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
            cmd.Parameters["@ReturnSelect"].Value = 1;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;

            da.Fill(dt);

            xmlOut = cmd.Parameters["@OUT"].Value.ToString();

        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }
    public static bool hideClientAlert(string token, int refCustomer, string alertTAG, string endDate)
    {
        bool isOK = false;

        SqlConnection conn = null;
        string sXmlOut = "";

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("AML.P_HideCustomerAlert", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@RC", SqlDbType.Int);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                                new XElement("Alerts",
                                                    new XAttribute(new XAttribute("CashierToken", token)),
                                                    new XElement("Alert",
                                                        new XAttribute("RefCustomer", refCustomer),
                                                        new XAttribute("AlertTAG", alertTAG),
                                                        new XAttribute("EndDate", endDate)))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
            cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;
            cmd.ExecuteNonQuery();

            sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            //<ALL_XML_OUT><Result RC="0" Inserted="1"  /></ALL_XML_OUT>
            List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Result", "RC");
            if (listRC.Count > 0 && listRC[0].ToString().Trim() == "0")
                isOK = true;
        }
        catch (Exception e)
        {
            isOK = false;
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return isOK;
    }
    public static bool changeClientAlertStatus(string sXmlIN)
    {
        bool isOK = false;

        SqlConnection conn = null;
        string sXmlOut = "";

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("AML.P_ChangeAlertStatus", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@RC", SqlDbType.Int);

            cmd.Parameters["@IN"].Value = sXmlIN;
            //cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
            //                                    new XElement("Alerts",
            //                                        new XAttribute("CashierToken", token),
            //                                        new XElement("Alert",
            //                                        new XAttribute("RefAlert", refAlert),
            //                                        new XAttribute("RefAlertStatus", refAlertStatus)))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
            cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;
            cmd.ExecuteNonQuery();

            sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            //<ALL_XML_OUT><Result RC="0" Inserted="1"  /></ALL_XML_OUT>
            List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Result", "RC");
            if (listRC.Count > 0 && listRC[0].ToString().Trim() == "0")
                isOK = true;
        }
        catch (Exception e)
        {
            isOK = false;
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return isOK;
    }
    public static DataTable getClientHiddenAlert(string sToken, int iRefCustomer)
    {
        DataTable dt = new DataTable();

        SqlConnection conn = null;
        string xmlOut = "";

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("AML.P_GetHideCustomerAlert", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@ReturnSelect", SqlDbType.Bit);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN", 
                                            new XElement("HideAlert",
                                                new XAttribute("CashierToken", sToken),
                                                new XAttribute("RefCustomer", iRefCustomer.ToString()))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
            cmd.Parameters["@ReturnSelect"].Value = 1;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;

            da.Fill(dt);

            xmlOut = cmd.Parameters["@OUT"].Value.ToString();

        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
     }
    public static string getAlertDetails(string sRefAlert)
    {
        string sXmlOut = "";

        SqlConnection conn = null;
        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("AML.P_GetAlertDetailsInXML", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@RefAlert", SqlDbType.Int);
            cmd.Parameters.Add("@AlertDetails", SqlDbType.Xml);

            cmd.Parameters["@RefAlert"].Value = sRefAlert;
            cmd.Parameters["@AlertDetails"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            sXmlOut = cmd.Parameters["@AlertDetails"].Value.ToString();

        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return sXmlOut;
    }

    public static DataTable GetAlertGeneralInfo(int iRefAlert)
    {
        DataTable dt = new DataTable();
        dt.TableName = "dtGetAlertGeneralInfo";
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("AML.P_GetAlertGeneralInfo", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@RefAlert", SqlDbType.Int);
            cmd.Parameters["@RefAlert"].Value = iRefAlert;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
        }
        catch(Exception e)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }

    public static DataTable getAlertTypeList()
    {
        DataTable dt = new DataTable();
        dt.TableName = "dtgetAlertTypeList";
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("AML.P_GetAlertCategoryList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;
            da.Fill(dt);
        }
        catch (Exception e)
        {
            dt.Columns.Add("TypeAlerte");
            dt.Rows.Add(new object[] { "Operation" });
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }
    public static DataTable getAlertTagList()
    {
        DataTable dt = new DataTable();
        dt.TableName = "dtgetAlertTagList";
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("AML.P_GetAlertDefinitionList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;
            da.Fill(dt);
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }

    public static DataTable getGroupAlertTypeList()
    {
        DataTable dtTmp = new DataTable();
        dtTmp.TableName = "dtgetGroupAlertTypeListTmp";

        DataTable dt = new DataTable();
        dtTmp.TableName = "dtgetGroupAlertTypeList";
        dt.Columns.Add("GroupTAG");
        dt.Columns.Add("GroupName");
        dt.Columns.Add("GroupReference");
        dt.Columns.Add("GroupDescription");
        dt.Columns.Add("GroupParamsNames", typeof(DataTable));

        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("AML.P_GetAlertGroupDefinitionList", conn);

            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;
            da.Fill(dtTmp);

            for (int i = 0; i < dtTmp.Rows.Count; i++)
            {
                string sParams = dtTmp.Rows[i]["GroupParamsNames"].ToString();
                sParams = "<ALL>" + sParams + "</ALL>";
                List<string> listParams = CommonMethod.GetTags(sParams, "ALL/P");

                DataTable dtParams = new DataTable("dtParams");
                dtParams.Columns.Add("N");
                dtParams.Columns.Add("V");

                for(int j = 0; j < listParams.Count; j++)
                {
                    List<string> listN = CommonMethod.GetAttributeValues(listParams[j], "P", "N");
                    List<string> listV = CommonMethod.GetAttributeValues(listParams[j], "P", "V");
                    dtParams.Rows.Add(new object[] { listN[0], listV[0] });
                }

                dt.Rows.Add(new object[] { dtTmp.Rows[i]["GroupTAG"], dtTmp.Rows[i]["GroupName"], dtTmp.Rows[i]["GroupReference"], dtTmp.Rows[i]["GroupDescription"], dtParams });
            }
        }
        catch (Exception e)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }

    public static DataTable getProfiles()
    {
        DataTable dt = new DataTable("dtGetProfiles");

        SqlConnection conn = null;
        string sXmlOut = "";

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("[AML].[P_GetProfileList]", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;
            da.Fill(dt);

        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }
    public static DataTable getProfiles(List<string> lsFilterProfileValues)
    {
        DataTable dt = new DataTable("dtGetProfiles");
        DataTable dtFiltered = new DataTable();
        dtFiltered.TableName = "dtgetProfilesFiltered";
        dtFiltered.Columns.Add("Profil");
        dtFiltered.Columns.Add("NiveauRisque");
        dtFiltered.Columns.Add("Description");

        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("[AML].[P_GetProfileList]", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;
            da.Fill(dt);

            for(int i=0; i < lsFilterProfileValues.Count; i++)
            {
                foreach(DataRow dr in dt.Rows)
                {
                    if (dr["Profil"].ToString().Trim().ToUpper() == lsFilterProfileValues[i].Trim().ToUpper())
                        dtFiltered.Rows.Add(new object[] { dr["Profil"].ToString(), dr["NiveauRisque"].ToString(), dr["Description"].ToString() });
                }
            }

        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dtFiltered;
    }
    public static bool getClientProfile(int refCustomer, out DataTable dtClientProfile_History, out DataTable dtClientProfile_Current)
    {
        bool isOK = false;
        dtClientProfile_History = new DataTable("dtClientProfile_History");
        dtClientProfile_Current = new DataTable("dtClientProfile_Current");

        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("AML.P_GetCustomerProfile", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@IN", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);

            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                            new XElement("Profile",
                                                new XAttribute("RefCustomer", refCustomer.ToString()))).ToString();

            cmd.ExecuteNonQuery();
            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();
            dtClientProfile_History = getDtClientProfileHistoryFromXml(sXmlOut);
            dtClientProfile_Current = getDtClientProfileCurrentFromXml(sXmlOut);
            isOK = true;
        }
        catch (Exception e)
        {
            isOK = false;
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return isOK;
    }
    private static DataTable getDtClientProfileHistoryFromXml(string sXml)
    {
        DataTable dt = new DataTable("dtClientProfileHistoryFromXml");
        dt.Columns.Add("TAG");
        dt.Columns.Add("Date");
        dt.Columns.Add("ByUser");

        string sTag = "", sDate = "", sByUser = "";

        List<string> listHistory = CommonMethod.GetTags(sXml, "ALL_XML_OUT/Profile/History");
        List<string> listTag = new List<string>();
        List<string> listDate = new List<string>();
        List<string> listByUser = new List<string>();

        for (int i = 0; i < listHistory.Count; i++)
        {
            sTag = "";
            sDate = "";
            sByUser = "";

            listTag = CommonMethod.GetAttributeValues(listHistory[i], "History", "OldProfileTAG");
            listDate = CommonMethod.GetAttributeValues(listHistory[i], "History", "StartDate");
            listByUser = CommonMethod.GetAttributeValues(listHistory[i], "History", "StartUser");

            if (listTag.Count > 0)
                sTag = listTag[0];
            if (listDate.Count > 0)
                sDate = listDate[0];
            if (listByUser.Count > 0)
                sByUser = listByUser[0];

            if (sTag.Trim().Length > 0 || sDate.Trim().Length > 0 || sByUser.Trim().Length > 0)
                dt.Rows.Add(new object[] { sTag, sDate, sByUser });
        }

        return dt;
    }
    private static DataTable getDtClientProfileCurrentFromXml(string sXml)
    {
        DataTable dt = new DataTable("dtClientProfileCurrentFromXml");
        dt.Columns.Add("TAG");
        dt.Columns.Add("Date");
        dt.Columns.Add("ByUser");
        dt.Columns.Add("Reason");

        string sTag = "", sDate = "", sByUser = "", sReason = "";

        List<string> listTag = CommonMethod.GetAttributeValues(sXml, "ALL_XML_OUT/Profile", "ProfileTAG");
        List<string> listDate = CommonMethod.GetAttributeValues(sXml, "ALL_XML_OUT/Profile", "StartDate");
        List<string> listByUser = CommonMethod.GetAttributeValues(sXml, "ALL_XML_OUT/Profile", "ByUser");
        List<string> listReason = CommonMethod.GetAttributeValues(sXml, "ALL_XML_OUT/Profile", "Reason");
        if (listTag.Count > 0)
            sTag = listTag[0];
        if (listDate.Count > 0)
            sDate = listDate[0];
        if (listByUser.Count > 0)
            sByUser = listByUser[0];
        if (listReason.Count > 0)
            sReason = listReason[0];

        if (sTag.Trim().Length > 0 || sDate.Trim().Length > 0 || sByUser.Trim().Length > 0)
            dt.Rows.Add(new object[] { sTag, sDate, sByUser, sReason });

        return dt;
    }
    public static bool setClientProfile(string token, int refCustomer, string profile, string reason)
    {
        bool isOK = true;

        SqlConnection conn = null;
        string sXmlOut = "";

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("AML.P_ChangeCustomerProfile", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@RC", SqlDbType.Int);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                                new XElement("Customer",
                                                    new XAttribute("CashierToken", token),
                                                    new XAttribute("RefCustomer", refCustomer.ToString()),
                                                    new XAttribute("ProfileTAG", profile),
                                                    new XAttribute("Reason", reason))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
            cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;
            cmd.ExecuteNonQuery();

            sXmlOut = cmd.Parameters["@OUT"].Value.ToString();
            //<ALL_XML_OUT><Result RC="0" Updated="1"  /></ALL_XML_OUT>
            List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Result", "RC");
            if (listRC.Count > 0 && listRC[0].ToString().Trim() == "0")
                isOK = true;
        }
        catch (Exception e)
        {
            isOK = false;
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return isOK;
    }

    public static DataTable getSpecificityList()
    {
        DataTable dt = new DataTable("dtGetSpecificityList");
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("AML.P_GetSpecifityList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;
            da.Fill(dt);

        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }
    public static DataTable getClientSpecifities(int refCustomer, bool bShowDeleted)
    {
        DataTable dt = new DataTable("dtGetClientSpecifities");

        SqlConnection conn = null;
        string sXmlOut = "";

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("AML.P_GetCustomerSpecificities", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@ReturnSelect", SqlDbType.Bit);
            cmd.Parameters.Add("@RC", SqlDbType.Int);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                            new XElement("Specificity",
                                                new XAttribute("RefCustomer", refCustomer),
                                                new XAttribute("Criteria", "ALL"))).ToString();
            cmd.Parameters["@ReturnSelect"].Value = 1;
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
            cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;

            da.Fill(dt);

            if (!bShowDeleted)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["IsDeleted"].ToString().Trim().ToLower() == "true")
                    {
                        dt.Rows[i].Delete();
                        dt.AcceptChanges();
                    }
                }
            }

            sXmlOut = cmd.Parameters["@OUT"].Value.ToString();
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }
    public static bool addClientSpecificity(string token, int refCustomer, string refSpecificity)
    {
        bool isOK = false;
        string sXML = new XElement("ALL_XML_IN",
                        new XElement("Specificities",
                            new XAttribute("TOKEN", token),
                                new XElement("Specificity",
                                    new XAttribute("ActionType", "A"),
                                    new XAttribute("RefCustomer", refCustomer),
                                    new XAttribute("RefSpecificity", refSpecificity)))).ToString();

        isOK = addDelClientSpecificitiy(sXML);

        return isOK;
    }
    public static bool delClientSpecificity(string token, int refCustomer, string refSpecificity)
    {
        bool isOK = false;
        string sXML = new XElement("ALL_XML_IN",
                        new XElement("Specificities",
                            new XAttribute("TOKEN", token),
                                new XElement("Specificity",
                                    new XAttribute("ActionType", "D"),
                                    new XAttribute("RefCustomer", refCustomer),
                                    new XAttribute("RefSpecificity", refSpecificity)))).ToString();

        isOK = addDelClientSpecificitiy(sXML);

        return isOK;
    }
    private static bool addDelClientSpecificitiy(string xml)
    {
        bool isOK = false;

        SqlConnection conn = null;
        string sXmlOut = "";

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("AML.P_AddDelCustomerSpecificities", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@XMLin", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@XMLout", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@RC", SqlDbType.Int);

            cmd.Parameters["@XMLin"].Value = xml;
            cmd.Parameters["@XMLout"].Direction = ParameterDirection.Output;
            cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;
            cmd.ExecuteNonQuery();

            sXmlOut = cmd.Parameters["@XMLout"].Value.ToString();

            List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Specificities", "RC");
            if (listRC.Count > 0 && listRC[0].ToString().Trim() == "0")
                isOK = true;
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return isOK;

    }
    public static bool isClientHasSpecificity(int refCustomer, string refSpecificity)
    {
        bool isClientHasSpecificity = false;
        DataTable dt = new DataTable("dtGetClientSpecifities");
        dt = getClientSpecifities(refCustomer, false);

        foreach(DataRow dr in dt.Rows)
        {
            string sIsDeleted = dr["IsDeleted"].ToString().Trim();

            if (dr["RefSpecificity"].ToString() == refSpecificity && dr["IsDeleted"].ToString().Trim().ToLower() != "true")
            {
                isClientHasSpecificity = true;
            }
        }

        return isClientHasSpecificity;
    }

    public static DataTable getClientNote(int refCustomer)
    {
        DataTable dt = new DataTable("dtGetClientNote");
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("AML.P_GetCustomerNote", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@ReturnSelect", SqlDbType.Bit);
            cmd.Parameters.Add("@RC", SqlDbType.Int);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                            new XElement("Note",
                                                new XAttribute("RefCustomer", refCustomer.ToString()))).ToString();
            cmd.Parameters["@ReturnSelect"].Value = 1;
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
            cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;

            da.Fill(dt);

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }
    public static bool addClientNote(string token, int refCustomer, string note)
    {
        bool isOK = false;

        SqlConnection conn = null;
        string sXmlOut = "";

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("AML.P_AddCustomerNote", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@RC", SqlDbType.Int);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                                new XElement("Note",
                                                    new XAttribute("CashierToken", token),
                                                    new XAttribute("RefCustomer", refCustomer),
                                                    new XAttribute("Note", note))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
            cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;
            cmd.ExecuteNonQuery();

            sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Result", "RC");
            if (listRC.Count > 0 && listRC[0].ToString().Trim() == "0")
                isOK = true;
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return isOK;
    }

    public static DataTable getNoComplianceList()
    {
        DataTable dt = new DataTable("dtGetNoComplianceList");
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("AML.P_GetNoComplanceSuscriptionList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;

            da.Fill(dt);
        }

        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }

    public static bool getWatchStatus(string sWatchStatus)
    {
        bool bWatchStatus = false;

        bool.TryParse(sWatchStatus, out bWatchStatus);

        return bWatchStatus;
    }
    public static string getWatchDetails(string sWatchBy, string sWatchDate)
    {
        string sWatchDetails = "";

        if (sWatchBy.Trim().Length > 0)
        {
            sWatchDetails += "surveillé par " + sWatchBy.Trim();

            if (sWatchDate.Trim().Length > 0)
                sWatchDetails += " le " + sWatchDate.Trim();
        }

        return sWatchDetails;
    }
    public static bool watchClientAlert(int iRefSeller, int iRefCustomer)
    {
        bool isOK = false;

        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("AML.P_WatchedCustomerAlert", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@RefUID", SqlDbType.Int);
            cmd.Parameters.Add("@RefCustomer", SqlDbType.Int);
            cmd.Parameters.Add("@RC", SqlDbType.Int);

            cmd.Parameters["@RefUID"].Value = iRefSeller;
            cmd.Parameters["@RefCustomer"].Value = iRefCustomer;
            cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;
            cmd.ExecuteNonQuery();

            if (cmd.Parameters["@RC"].Value.ToString().Trim() == "0")
                isOK = true;

        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return isOK;
    }
    public static bool unwatchClientAlert(int iRefSeller, int iRefCustomer)
    {
        bool isOK = false;

        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("AML.P_UnwatchedCustomerAlert", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@RefUID", SqlDbType.Int);
            cmd.Parameters.Add("@RefCustomer", SqlDbType.Int);
            cmd.Parameters.Add("@RC", SqlDbType.Int);

            cmd.Parameters["@RefUID"].Value = iRefSeller;
            cmd.Parameters["@RefCustomer"].Value = iRefCustomer;
            cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;
            cmd.ExecuteNonQuery();

            if (cmd.Parameters["@RC"].Value.ToString().Trim() == "0")
                isOK = true;

        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return isOK;
    }

    public static bool setSurveillance(bool bActive, string sToken, int iRefCustomer)
    {
        bool bOk = false;
        SqlConnection conn = null;
        string sXmlIn = "", sXmlOut = "";
        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = null;
            
            if(bActive)
                cmd = new SqlCommand("Web.P_WatchCustomer", conn);
            else cmd = new SqlCommand("Web.P_UnwatchCustomer", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);

            sXmlIn = new XElement("ALL_XML_IN",
                                        new XElement("Customer",
                                            new XAttribute("CashierToken", sToken),
                                            new XAttribute("RefCustomer", iRefCustomer.ToString()))).ToString();
            cmd.Parameters["@IN"].Value = sXmlIn;
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if (sXmlOut.Length > 0)
            {
                List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Result", "RC");

                if (listRC.Count > 0 && listRC[0] == "0")
                    bOk = true;
                else
                    tools.LogToFile("setSurveillance", "XMLIN : " + sXmlIn + "|XMLOUT : " + sXmlOut);
            }
            else
                tools.LogToFile("setSurveillance", "XMLIN : " + sXmlIn + "|XMLOUT : " + sXmlOut);
        }
        catch (Exception e)
        {
            tools.LogToFile("setSurveillance", "XMLIN : "+ sXmlIn + "|XMLOUT : " + sXmlOut + "|EXCEPTION : " + e.ToString());
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return bOk;
    }

    public static DataTable getPendingTransferList(string sXmlIn, out string sXmlOut)
    {
        sXmlOut = "";
        DataTable dtPendingList = new DataTable("dtPendingList");
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_PendingTransfers", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@ReturnSelect", SqlDbType.Bit);

            cmd.Parameters["@IN"].Value = sXmlIn;
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
            cmd.Parameters["@ReturnSelect"].Value = 1;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;

            da.Fill(dtPendingList);

            sXmlOut = cmd.Parameters["@OUT"].Value.ToString();
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dtPendingList;
    }

    public static bool SetPendingTransferStatus(string sRefPendingTransfer, string sRefCustomer, string sCashierToken, string sAction, out string sErrorMessage)
    {
        sErrorMessage = "";
        bool bOk = false;
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_PendingTransferAction", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);

            XElement xelem = new XElement("Transfer",
                                                new XAttribute("RefPendingTransfer", sRefPendingTransfer),
                                                new XAttribute("CashierToken", sCashierToken),
                                                new XAttribute("Action", sAction));
            if (!String.IsNullOrWhiteSpace(sRefCustomer))
                xelem.Add(new XAttribute("RefCustomer", sRefCustomer));

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                                xelem
                                            ).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if (sXmlOut.Length > 0)
            {
                List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Transfer", "RC");
                List<string> listErrorLabel = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Transfer", "ErrorLabel");
                List<string> listSabErrorLabel = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Transfer", "SabErrorLabel");
                
                if (listRC.Count > 0 && listRC[0] == "0")
                    bOk = true;
                else
                {
                    if (listErrorLabel.Count > 0 && listErrorLabel[0].Length > 0)
                    {
                        StringBuilder sb = new StringBuilder();
                        sb.AppendLine(listErrorLabel[0]);

                        if (listSabErrorLabel.Count > 0 && listSabErrorLabel[0].Length > 0)
                            sb.AppendLine("Erreur SAB : " + listSabErrorLabel[0]);

                        sErrorMessage = sb.ToString().Trim();
                    }
                }
            }
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return bOk;
    }

    public static bool SetCustomerTransferSituation(string sRefCustomer, string sCashierToken, string sAction, string sLimitAmount)
    {
        bool bOk = false;
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_ConfigureCustomerTransfer", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                            new XElement("Transfer",
                                                new XAttribute("CashierToken", sCashierToken),
                                                new XAttribute("RefCustomer", sRefCustomer),
                                                new XAttribute("Action", sAction),
                                                new XAttribute("LimitAmount", sLimitAmount))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if (sXmlOut.Length > 0)
            {
                List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Result", "RC");
                if (listRC.Count > 0 && listRC[0] == "0")
                    bOk = true;
            }
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return bOk;
    }

    public static DataTable GetCustomerAMLAlerts(string sRefCustomer, string sToken, out bool bWatch, out string sPeriod, out string sWatchStart, out string sWatchEnd)
    {
        DataTable dt = new DataTable("dtGetCustomerAMLAlerts");
        dt.Columns.Add("TAG");
        dt.Columns.Add("MinAmount");
        dt.Columns.Add("Agent");
        dt.Columns.Add("Service");
        SqlConnection conn = null;
        bWatch = false;
        sPeriod = "";
        sWatchStart = "";
        sWatchEnd = "";

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("AML.P_AmlGetWatchAlerts", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                            new XElement("AML_ALERTS",
                                                new XAttribute("TOKEN", sToken),
                                                new XAttribute("RefCustomer", sRefCustomer))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if (sXmlOut.Length > 0)
            {
                List<string> listAlerts = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/AML_ALERTS/ALERT");
                List<string> listWatch = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/AML_ALERTS", "Watch");
                if (listWatch.Count > 0 && listWatch[0] == "1")
                    bWatch = true;
                List<string> listPeriod = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/AML_ALERTS", "Period");
                if (listPeriod.Count > 0 && !String.IsNullOrWhiteSpace(listPeriod[0]))
                    sPeriod = listPeriod[0];
                List<string> listWatchStart = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/AML_ALERTS", "WatchStartDate");
                if (listWatchStart.Count > 0 && !String.IsNullOrWhiteSpace(listWatchStart[0]))
                    sWatchStart = listWatchStart[0];
                List<string> listWatchEnd = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/AML_ALERTS", "WatchEndDate");
                if (listWatchEnd.Count > 0 && !String.IsNullOrWhiteSpace(listWatchEnd[0]))
                    sWatchEnd = listWatchEnd[0];

                for (int i = 0; i < listAlerts.Count; i++)
                {
                    List<string> listTag = CommonMethod.GetAttributeValues(listAlerts[i], "ALERT", "TAG");
                    List<string> listMinAmount = CommonMethod.GetAttributeValues(listAlerts[i], "ALERT", "MinAmount");
                    List<string> listAgent = CommonMethod.GetAttributeValues(listAlerts[i], "ALERT", "Agent");
                    List<string> listService = CommonMethod.GetAttributeValues(listAlerts[i], "ALERT", "ServiceConformite");
                    dt.Rows.Add(new object[] { listTag[0], listMinAmount[0], listAgent[0], listService[0] });
                }
            }
        }
        catch (Exception ex)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }
    public static bool SetCustomerAMLAlerts(string sXmlIn)
    {
        bool bOk = false;
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("AML.P_AmlSetWatchAlerts", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);

            cmd.Parameters["@IN"].Value = sXmlIn;
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if (sXmlOut.Length > 0)
            {
                List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Result", "RC");
                if (listRC.Count > 0 && listRC[0] == "0")
                    bOk = true;
            }
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return bOk;
    }

    public static bool HasPendingTransferRights(string sCashierToken, out bool bView, out bool bAction)
    {
        bool bHasRights = false;
        bView = false;
        bAction = false;
        string sXmlOut = tools.GetRights(authentication.GetCurrent().sToken, "SC_Customer");
        List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Page", "RC");

        if (listRC.Count > 0 && listRC[0] == "0")
        {
            List<string> listAction = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/Page/Action");

            for (int i = 0; i < listAction.Count; i++)
            {
                List<string> listTag = CommonMethod.GetAttributeValues(listAction[i], "Action", "TagAction");
                List<string> listActionAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ActionAllowed");
                List<string> listViewAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ViewAllowed");

                if (listTag.Count > 0 && listTag[0] == "PendingTransfer")
                {
                    if (listActionAllowed[0] == "1" && listViewAllowed[0] == "1")
                        bHasRights = true;
                    if (listActionAllowed[0] == "1")
                        bAction = true;
                    if (listViewAllowed[0] == "1")
                        bView = true;
                    break;
                }
            }
        }

        return bHasRights;
    }

    public static bool SetAlertTreatmentStatus(string sRefAlert, string sRefCustomer, string sCashierToken, string sAction, out string sErrorMessage)
    {
        bool isOK = false;
        sErrorMessage = "";

        return isOK;
    }
    
    public static bool SetClientAlertManagement(string sCashierToken, string sRefAlert, out string sErrorLabel)
    {
        bool isOK = false;
        sErrorLabel = "Erreur inconnue";

        int iRefAlert;
        if (int.TryParse(sRefAlert, out iRefAlert))
        {

            SqlConnection conn = null;

            try
            {
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
                conn.Open();

                SqlCommand cmd = new SqlCommand("AML.P_ManageAlertFirstAction", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
                cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);

                cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                                new XElement("Alert",
                                                    new XAttribute("CashierToken", sCashierToken),
                                                    new XAttribute("RefAlert", sRefAlert))).ToString();
                cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();

                string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

                if (sXmlOut.Length > 0)
                {
                    List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Alert", "RC");
                    List<string> listErrorLabel = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Alert", "ErrorLabel");

                    if (listRC.Count > 0 && listRC[0] != null && listRC[0].Trim() == "0")
                    {
                        sErrorLabel = "";
                        isOK = true;
                    }
                    else
                    {
                        if (listErrorLabel.Count > 0 && listErrorLabel[0] != null && listErrorLabel[0].Trim().Length > 0)
                            sErrorLabel = listErrorLabel[0].Trim();
                        else sErrorLabel = "Erreur inconnue";
                    }
                }
            }
            catch (Exception ex)
            {
                isOK = false;
                sErrorLabel = "Une erreur est survenue : " + ex.Message;
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }
        else sErrorLabel = "Référence alerte non valide";

        return isOK;   
    }
    public static bool GetAlertEvents(string sCashierToken, string sRefAlert, out string sErrorLabel, out string sXml)
    {
        sXml = "";
        bool isOK = false;

        int iRefAlert;
        sErrorLabel = "";
        if (int.TryParse(sRefAlert, out iRefAlert))
        {

            SqlConnection conn = null;

            try
            {
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
                conn.Open();

                SqlCommand cmd = new SqlCommand("AML.P_GetAlertEvents", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
                cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);

                cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                                new XElement("Alert",
                                                    new XAttribute("CashierToken", sCashierToken),
                                                    new XAttribute("RefAlert", sRefAlert))).ToString();
                cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

                cmd.ExecuteNonQuery();

                sXml = cmd.Parameters["@OUT"].Value.ToString();

                if (sXml.Length > 0)
                    isOK = true;
            }
            catch (Exception ex)
            {
                isOK = false;
                sErrorLabel = ex.Message;
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }
        else sErrorLabel = "Référence alerte non valide";

        return isOK;
    }

    public static bool SetAlertTreatment(string sXmlIn, out string sRefStatement)
    {
        sRefStatement = "";

        bool bOk = false;
        string sXmlOut = _SetAlertTreatment(sXmlIn);

        if (sXmlOut.Length > 0)
        {
            List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Result", "RC");

            if (listRC.Count > 0 && listRC[0] == "0")
            {
                bOk = true;

                List<string> listRefStatement = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Result", "RefStatement");
                if (listRefStatement.Count > 0 && !String.IsNullOrWhiteSpace(listRefStatement[0]))
                    sRefStatement = listRefStatement[0];
            }
        }

        return bOk;
    }
    public static bool SetAlertTreatment(string sXmlIn)
    {
        bool bOk = false;

        string sXmlOut = _SetAlertTreatment(sXmlIn);

        if (sXmlOut.Length > 0)
        {
            List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Result", "RC");

            if (listRC.Count > 0 && listRC[0] == "0")
                bOk = true;
        }

        return bOk;
    }
    private static string _SetAlertTreatment(string sXmlIn)
    {
        SqlConnection conn = null;
        string sXmlOut = "";

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("AML.P_SetAlertTreatment", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);

            cmd.Parameters["@IN"].Value = sXmlIn;
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            sXmlOut = cmd.Parameters["@OUT"].Value.ToString();
        }
        catch (Exception e)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return sXmlOut;
    }

    public static bool HasAccountClosingRights(string sCashierToken, out bool bView, out bool bAction)
    {
        bool bHasRights = false;
        bView = false;
        bAction = false;
        string sXmlOut = tools.GetRights(authentication.GetCurrent().sToken, "SC_SearchAccountClosing");
        List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Page", "RC");

        if (listRC.Count > 0 && listRC[0] == "0")
        {
            List<string> listAction = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/Page/Action");

            for (int i = 0; i < listAction.Count; i++)
            {
                List<string> listTag = CommonMethod.GetAttributeValues(listAction[i], "Action", "TagAction");
                List<string> listActionAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ActionAllowed");
                List<string> listViewAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ViewAllowed");

                if (listTag.Count > 0 && listTag[0] == "SearchAccountClosing")
                {
                    if (listActionAllowed[0] == "1" && listViewAllowed[0] == "1")
                        bHasRights = true;
                    if (listActionAllowed[0] == "1")
                        bAction = true;
                    if (listViewAllowed[0] == "1")
                        bView = true;
                    break;
                }
            }
        }

        return bHasRights;
    }

    public static string GetMacroAnswer(int iRefAlert, string sType)
    {
        SqlConnection conn = null;
        string sContent = "";

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("AML.P_GetMacroAnswer", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 200);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                            new XElement("MACRO",
                                                new XAttribute("RefAlert", (iRefAlert == 0) ? "" : iRefAlert.ToString()),
                                                new XAttribute("Type", sType))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if (sXmlOut.Length > 0)
            {
                if (iRefAlert == 0 && String.IsNullOrWhiteSpace(sType))
                    sContent = sXmlOut;
                else
                {
                    List<string> listContent = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/MACRO", "Content");
                    sContent = (listContent.Count > 0) ? listContent[0] : "";
                }
            }
        }
        catch (Exception e)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return sContent;
    }

    public static List<AgentNickel> GetAgentConformityList()
    {
        List<AgentNickel> listeAgent = new List<AgentNickel>() ;
        authentication auth = authentication.GetCurrent(false);
        string sXml = tools.GetUserList(auth.sToken);

        List<string> listBOUser = CommonMethod.GetTags(sXml, "ALL_XML_OUT/BOUser");
        
        for (int i = 0; i < listBOUser.Count; i++)
        {
            List<string> listRef = CommonMethod.GetAttributeValues(listBOUser[i], "BOUser", "RefUID");
            List<string> listLastName = CommonMethod.GetAttributeValues(listBOUser[i], "BOUser", "LastName");
            List<string> listFirstName = CommonMethod.GetAttributeValues(listBOUser[i], "BOUser", "FirstName");
            List<string> listTagProfil = CommonMethod.GetAttributeValues(listBOUser[i], "BOUser", "TAGProfile");
            List<string> listDisabled = CommonMethod.GetAttributeValues(listBOUser[i], "BOUser", "Disabled");

            if (listDisabled.Count > 0 && listDisabled[0] == "0" &&
                listTagProfil.Count > 0 &&
                (listTagProfil[0].ToLower().Contains("dcirc") ||
                (listTagProfil[0].ToLower().Contains("admin") && auth.sTagProfile.ToLower() == "admin"))
                )
                listeAgent.Add(new AgentNickel() { Id = Convert.ToInt32(listRef[0]), FirstName = listFirstName[0], LastName = listLastName[0] });
        }


        return listeAgent.OrderBy(ord => ord.CompleteName).ToList() ;
    }
}

