﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using XMLMethodLibrary;

/// <summary>
/// Description résumée de registration
/// </summary>
/// 
[Serializable]
public class registration
{
    public int iRefUserChecker { get; set; }
    public int iCurrentStep { get; set; }
    public string sCheckGeneralStatus { get; set; }
    public bool bCurrentStepIsAdult { get; set; }
    public string sRefCustomer { get; set; }
    public string sRegistrationCode { get; set; }
    public string sRegistrationDate { get; set; }
    public string sRefRegistration { get; set; }
    
    public bool bMinorAccount { get; set; }
    public bool bCheckParentId { get; set; }

    public string sParentalAuthorityProofDocumentType { get; set; }
    
    public registrationsheet ClientRegistrationSheet { get; set; }
    public registrationsheet ParentRegistrationSheet { get; set; }
    public clientsheet ClientSheet { get; set; }
    public clientsheet ParentSheet { get; set; }
    public ComplianceInfos ComplianceToApply { get; set; }
    [Serializable]
    public class ComplianceInfos
    {
        public string TAG { get; set; }
        public string Label { get; set; }
        public string sXML { get; set; }
        public bool isOK { get; set; }
    }
    [Serializable]
    public class registrationsheet
    {
        public string DocumentType { get; set; }
        public string DocumentCountry { get; set; }
        public string DocumentNumber { get; set; }
        public string DocumentExpirationDate { get; set; }
        public string LastName { get; set; }
        public string MaritalName { get; set; }
        public string UseName { get; set; }
        public string FirstName { get; set; }
        public string BirthDate { get; set; }
        public string BirthCountry { get; set; }
        public string BirthCity { get; set; }
        public string BirthDep { get; set; }
        public string Gender { get; set; }
    }
    [Serializable]
    public class clientsheet
    {
        public string RefDocument { get; set; }
        public string RefParentCustomer { get; set; }
        public bool isParentHasAccount { get; set; }
        public string DocumentCountry { get; set; }
        public string DocumentNumber { get; set; }
        public string DocumentType { get; set; }
        public string LastName { get; set; }
        public string MaritalName { get; set; }
        public string UseName { get; set; }
        public string FirstName { get; set; }
        public string BirthDate { get; set; }
        public string BirthCountry { get; set; }
        public string BirthDep { get; set; }
        public string BirthCity { get; set; }
        public string Gender { get; set; }
        public string ExpirationDate { get; set; }
    }

    public string sXmlCheckStepList { get; set; }
    public List<int> liStepInfo { get; set; }

    public static registration GetCurrent(bool redirect, string RegistrationCode)
    {
        registration registr = null;
        if (!string.IsNullOrWhiteSpace(RegistrationCode))
        { 
            registr = (registration)System.Web.HttpContext.Current.Session["RegistrationCheck_"+RegistrationCode];
        }

        if (registr != null)
            return registr;
        else if (redirect) System.Web.HttpContext.Current.Response.Redirect("default.aspx", true);

        return null;
    }
    public static void Save(registration registr, string RegistrationCode)
    {
        if (!string.IsNullOrWhiteSpace(RegistrationCode))
        {
            System.Web.HttpContext.Current.Session["RegistrationCheck_" + RegistrationCode] = registr;
        }
    }
    public static void Clear(string RegistrationCode)
    {
        if (!string.IsNullOrWhiteSpace(RegistrationCode))
        {
            System.Web.HttpContext.Current.Session.Remove("RegistrationCheck_" + RegistrationCode);
        }
    }
    [Serializable]
    public class ReportingFilter
    {
        public DateTime beginDate { get; set; }
        public DateTime endDate { get; set; }
    }
    [Serializable]
    public class GlobalCheckInfo
    {
        public bool bManager { get; set; }
        public int iLevel { get; set; }
        public int _ALL_NOT_CHECKED_ { get; set; }
        public int _L1_CHECKS_KO_L2_CHECKS_TO_START_ { get; set; }
        public int _L1_CHECKS_IN_PROGRESS_ { get; set; }
        public int _L1_CHECKS_OK_NO_CHANGES_ { get; set; }
        public int _L1_CHECKS_OK_WITH_CHANGES_ { get; set; }
        public int _L1_CHECKS_KO_NO_L2_ { get; set; }
        public int _L1_ALL_CHECKS_ { get; set; }
        public int _L1_CHECKS_KO_L2_CHECKS_IN_PROGRESS_ { get; set; }
        public int _L1_CHECKS_KO_L2_CHECKS_OK_ { get; set; }
        public int _L1_CHECKS_KO_L2_CHECKS_KO_ { get; set; }
        public int _L1_NO_CHECK_L2_CHECKS_IN_PROGRESS_ { get; set; }
        public int _L1_NO_CHECK_L2_CHECKS_OK_ { get; set; }
        public int _L1_NO_CHECK_L2_CHECKS_KO_ { get; set; }
        public int _L2_ALL_CHECKS_ { get; set; }
        public int _ONFIDO_CHECKS_IN_PROGRESS_ { get; set; }
    }

    public static DataTable GetReportingCheckRegistration(string sToken, ReportingFilter _reportingFilter)
    {
        string sXmlIn="", sXmlOut = "";

        XElement xUsersInfos = new XElement("UsersInfos",
                        new XAttribute("TOKEN", sToken));

        if (_reportingFilter.beginDate != null && _reportingFilter.beginDate != DateTime.MinValue)
            xUsersInfos.Add(new XAttribute("dtCheckDateStartFilter", _reportingFilter.beginDate.ToString("yyyy-MM-dd")));

        if (_reportingFilter.endDate != null && _reportingFilter.endDate != DateTime.MinValue)
            xUsersInfos.Add(new XAttribute("dtCheckDateEndFilter", _reportingFilter.endDate.ToString("yyyy-MM-dd")));


        sXmlIn = new XElement("ALL_XML_IN", xUsersInfos).ToString();

        if (GetUsersChecksInfos(sXmlIn, out sXmlOut) && !string.IsNullOrWhiteSpace(sXmlOut))
        {
            return GetUsersChecksInfosDatatableFromXml(sXmlOut);
        }

        return null;
    }
    public static GlobalCheckInfo GetGlobalCheckInfo(string sToken)
    {
        GlobalCheckInfo _globalCheckInfo = new GlobalCheckInfo();
        _globalCheckInfo.iLevel = 1;
        _globalCheckInfo.bManager = false;
        _globalCheckInfo._ALL_NOT_CHECKED_ = 0;
        _globalCheckInfo._L1_CHECKS_KO_L2_CHECKS_TO_START_ = 0;
        _globalCheckInfo._L1_CHECKS_IN_PROGRESS_ = 0;
        _globalCheckInfo._L1_CHECKS_OK_NO_CHANGES_ = 0;
        _globalCheckInfo._L1_CHECKS_OK_WITH_CHANGES_ = 0;
        _globalCheckInfo._L1_CHECKS_KO_NO_L2_ = 0;
        _globalCheckInfo._L1_ALL_CHECKS_ = 0;
        _globalCheckInfo._L1_CHECKS_KO_L2_CHECKS_IN_PROGRESS_ = 0;
        _globalCheckInfo._L1_CHECKS_KO_L2_CHECKS_OK_ = 0;
        _globalCheckInfo._L1_CHECKS_KO_L2_CHECKS_KO_ = 0;
        _globalCheckInfo._L1_NO_CHECK_L2_CHECKS_IN_PROGRESS_ = 0;
        _globalCheckInfo._L1_NO_CHECK_L2_CHECKS_OK_ = 0;
        _globalCheckInfo._L1_NO_CHECK_L2_CHECKS_KO_ = 0;
        _globalCheckInfo._L2_ALL_CHECKS_ = 0;
        _globalCheckInfo._ONFIDO_CHECKS_IN_PROGRESS_ = 0;

        string sXmlIn = "", sXmlOut = "";

        XElement xUsersInfos = new XElement("UsersInfos",
                        new XAttribute("TOKEN", sToken));

        sXmlIn = new XElement("ALL_XML_IN", xUsersInfos).ToString();
        if (GetUsersChecksInfos(sXmlIn, out sXmlOut) && !string.IsNullOrWhiteSpace(sXmlOut))
        {
            List<string> lsGlobalInfoCheckList = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/GlobalInfos/Check");
            List<string> lsUserGroupLevel = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/UsersInfos", "iCheckUserGroupLevel");
            List<string> lsUserManager = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/UsersInfos","bManager");
            List<string> lsRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/UsersInfos", "RC");
            List<string> lsUserCheck = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/UsersList/UserCheck");

            if (lsRC != null && lsRC.Count > 0 && lsRC[0] == "0")
            {
                _globalCheckInfo.bManager = (lsUserManager != null && lsUserManager.Count > 0 && lsUserManager[0] == "1") ? true : false;
                _globalCheckInfo.iLevel = (lsUserGroupLevel != null && lsUserGroupLevel.Count > 0 && lsUserGroupLevel[0] == "2") ? 2 : 1;

                for (int i = 0; i < lsGlobalInfoCheckList.Count; i++)
                {
                    /*
                       <ALL_XML_OUT>
                          <UsersInfos RC="0" iCheckUserGroupLevel="2" bManager="1" />
                          <GlobalInfos>
                            <Check sCheckGeneralStatus="_ALL_NOT_CHECKED_" iCount="9373" />
                            <Check sCheckGeneralStatus="_L1_CHECKS_KO_L2_CHECKS_TO_START_" iCount="0" />
                            <Check sCheckGeneralStatus="_L1_CHECKS_IN_PROGRESS_" iCount="0" />
                            <Check sCheckGeneralStatus="_L1_CHECKS_OK_NO_CHANGES_" iCount="0" />
                            <Check sCheckGeneralStatus="_L1_CHECKS_OK_WITH_CHANGES_" iCount="0" />
                            <Check sCheckGeneralStatus="_L1_CHECKS_KO_NO_L2_" iCount="0" />
                            <Check sCheckGeneralStatus="_L1_ALL_CHECKS_" iCount="0" />
                            <Check sCheckGeneralStatus="_L1_CHECKS_KO_L2_CHECKS_IN_PROGRESS_" iCount="0" />
                            <Check sCheckGeneralStatus="_L1_CHECKS_KO_L2_CHECKS_OK_" iCount="0" />
                            <Check sCheckGeneralStatus="_L1_CHECKS_KO_L2_CHECKS_KO_" iCount="0" />
                            <Check sCheckGeneralStatus="_L1_NO_CHECK_L2_CHECKS_IN_PROGRESS_" iCount="0" />
                            <Check sCheckGeneralStatus="_L1_NO_CHECK_L2_CHECKS_OK_" iCount="0" />
                            <Check sCheckGeneralStatus="_L1_NO_CHECK_L2_CHECKS_KO_" iCount="0" />
                            <Check sCheckGeneralStatus="_L2_ALL_CHECKS_" iCount="0" />
                          </GlobalInfos>
                          <UsersList />
                        </ALL_XML_OUT>
                    */
                    List<string> lsCheckGeneralStatus = CommonMethod.GetAttributeValues(lsGlobalInfoCheckList[i], "Check", "sCheckGeneralStatus");
                    List<string> lsCount = CommonMethod.GetAttributeValues(lsGlobalInfoCheckList[i], "Check", "iCount");

                    int iCount = 0;

                    if (lsCheckGeneralStatus != null && lsCheckGeneralStatus.Count > 0 && !string.IsNullOrWhiteSpace(lsCheckGeneralStatus[0]) &&
                        lsCount != null && lsCount.Count > 0 && int.TryParse(lsCount[0], out iCount))
                    {
                        switch (lsCheckGeneralStatus[0])
                        {
                            case "_ALL_NOT_CHECKED_":
                                _globalCheckInfo._ALL_NOT_CHECKED_ = iCount;
                                break;
                            case "_L1_CHECKS_KO_L2_CHECKS_TO_START_":
                                _globalCheckInfo._L1_CHECKS_KO_L2_CHECKS_TO_START_ = iCount;
                                break;
                            case "_L1_CHECKS_IN_PROGRESS_":
                                _globalCheckInfo._L1_CHECKS_IN_PROGRESS_ = iCount;
                                break;
                            case "_L1_CHECKS_OK_NO_CHANGES_":
                                _globalCheckInfo._L1_CHECKS_OK_NO_CHANGES_ = iCount;
                                break;
                            case "_L1_CHECKS_OK_WITH_CHANGES_":
                                _globalCheckInfo._L1_CHECKS_OK_WITH_CHANGES_ = iCount;
                                break;
                            case "_L1_CHECKS_KO_NO_L2_":
                                _globalCheckInfo._L1_CHECKS_KO_NO_L2_ = iCount;
                                break;
                            case "_L1_ALL_CHECKS_":
                                _globalCheckInfo._L1_ALL_CHECKS_ = iCount;
                                break;
                            case "_L1_CHECKS_KO_L2_CHECKS_IN_PROGRESS_":
                                _globalCheckInfo._L1_CHECKS_KO_L2_CHECKS_IN_PROGRESS_ = iCount;
                                break;
                            case "_L1_CHECKS_KO_L2_CHECKS_OK_":
                                _globalCheckInfo._L1_CHECKS_KO_L2_CHECKS_OK_ = iCount;
                                break;
                            case "_L1_CHECKS_KO_L2_CHECKS_KO_":
                                _globalCheckInfo._L1_CHECKS_KO_L2_CHECKS_KO_ = iCount;
                                break;
                            case "_L1_NO_CHECK_L2_CHECKS_IN_PROGRESS_":
                                _globalCheckInfo._L1_NO_CHECK_L2_CHECKS_IN_PROGRESS_ = iCount;
                                break;
                            case "_L1_NO_CHECK_L2_CHECKS_OK_":
                                _globalCheckInfo._L1_NO_CHECK_L2_CHECKS_OK_ = iCount;
                                break;
                            case "_L1_NO_CHECK_L2_CHECKS_KO_":
                                _globalCheckInfo._L1_NO_CHECK_L2_CHECKS_KO_ = iCount;
                                break;
                            case "_L2_ALL_CHECKS_":
                                _globalCheckInfo._L2_ALL_CHECKS_ = iCount;
                                break;
                        }
                    }
                }

                for (int i = 0; i < lsUserCheck.Count; i++)
                {
                    List<string> lsLastName = CommonMethod.GetAttributeValues(lsUserCheck[i], "UserCheck", "sLastName");
                    List<string> lsFirstName = CommonMethod.GetAttributeValues(lsUserCheck[i], "UserCheck", "sFirstName");
                    List<string> ls_L1_CHECKS_IN_PROGRESS_ = CommonMethod.GetAttributeValues(lsUserCheck[i], "UserCheck", "_L1_CHECKS_IN_PROGRESS_");
                    try
                    {
                        if (lsLastName[0].Trim().ToUpper() == "AUTO" && lsFirstName[0].Trim().ToUpper() == "ONFIDO")
                        {
                            _globalCheckInfo._ONFIDO_CHECKS_IN_PROGRESS_ = int.Parse(ls_L1_CHECKS_IN_PROGRESS_[0].ToString());
                        }
                    }
                    catch (Exception ex) { }
                }
            }
        }

        return _globalCheckInfo;
    }

    private static bool GetUsersChecksInfos(string sXmlIn, out string sXmlOut)
    {
        bool isOK = false;
        sXmlOut = "";

        SqlConnection conn = null;
        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["PARTNER"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("[Registration].[P_GetUsersChecksInfos]", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            //<ALL_XML_IN><UsersInfos TOKEN = "0X26F295DA3CD07A61AD906D542725BED103718F8B"/></ ALL_XML_IN >

            cmd.Parameters.Add("@sXmlIn", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@sXmlOut", SqlDbType.VarChar, -1);

            cmd.Parameters["@sXmlIn"].Value = sXmlIn;
            cmd.Parameters["@sXmlOut"].Direction = ParameterDirection.Output;
            cmd.ExecuteNonQuery();

            sXmlOut = cmd.Parameters["@sXmlOut"].Value.ToString();
            //<ALL_XML_OUT><UsersInfos RC = "0" iCheckUserGroupLevel = "2" bManager = "1" />

            List<string> lsRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/UsersInfos", "RC");
            if (lsRC != null && lsRC.Count > 0 && lsRC[0].Trim() == "0")
            {
                isOK = true;
            }
        }
        catch (Exception ex)
        {
            isOK = false;
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return isOK;
    }
    private static DataTable GetUsersChecksInfosDatatableFromXml(string sXml)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("LastName");
        dt.Columns.Add("FirstName");
        dt.Columns.Add("TreatmentHourAverage");
        dt.Columns.Add("_L1_CHECKS_IN_PROGRESS_");
        dt.Columns.Add("_L1_CHECKS_OK_NO_CHANGES_");
        dt.Columns.Add("_L1_CHECKS_OK_WITH_CHANGES_");
        dt.Columns.Add("_L1_CHECKS_KO_NO_L2_");
        dt.Columns.Add("_L1_CHECKS_KO_L2_CHECKS_TO_START_");
        dt.Columns.Add("_L1_CHECKS_KO_L2_CHECKS_IN_PROGRESS_");
        dt.Columns.Add("_L1_CHECKS_KO_L2_CHECKS_OK_");
        dt.Columns.Add("_L1_CHECKS_KO_L2_CHECKS_KO_");
        dt.Columns.Add("_L1_NO_CHECK_L2_CHECKS_IN_PROGRESS_");
        dt.Columns.Add("_L1_NO_CHECK_L2_CHECKS_OK_");
        dt.Columns.Add("_L1_NO_CHECK_L2_CHECKS_KO_");
        dt.Columns.Add("Level");
        

        List<string> lsUserCheckList = CommonMethod.GetTags(sXml, "ALL_XML_OUT/UsersList/UserCheck");
        for(int i = 0; i < lsUserCheckList.Count; i++)
        {
            /*
                <UserCheck sLastName="ANDRIAMAHARO" sFirstName="Gaby" sUserGroupTAG="OdityAgentATD" iCheckLevel="1" bManager="0" bSysAdmin="0"
                _L1_CHECKS_KO_L2_CHECKS_TO_START_ ="73" _L1_CHECKS_KO_NO_L2_="27" _L1_CHECKS_OK_NO_CHANGES_="1336" />
            */
            List<string> lsFirstName = CommonMethod.GetAttributeValues(lsUserCheckList[i], "UserCheck", "sFirstName");
            List<string> lsLastName = CommonMethod.GetAttributeValues(lsUserCheckList[i], "UserCheck", "sLastName");
            List<string> ls_L1_CHECKS_IN_PROGRESS_ = CommonMethod.GetAttributeValues(lsUserCheckList[i], "UserCheck", "_L1_CHECKS_IN_PROGRESS_");
            List<string> ls_L1_CHECKS_OK_NO_CHANGES_ = CommonMethod.GetAttributeValues(lsUserCheckList[i], "UserCheck", "_L1_CHECKS_OK_NO_CHANGES_");
            List<string> ls_L1_CHECKS_OK_WITH_CHANGES_ = CommonMethod.GetAttributeValues(lsUserCheckList[i], "UserCheck", "_L1_CHECKS_OK_WITH_CHANGES_");
            List<string> ls_L1_CHECKS_KO_NO_L2_ = CommonMethod.GetAttributeValues(lsUserCheckList[i], "UserCheck", "_L1_CHECKS_KO_NO_L2_");
            List<string> ls_L1_CHECKS_KO_L2_CHECKS_TO_START_ = CommonMethod.GetAttributeValues(lsUserCheckList[i], "UserCheck", "_L1_CHECKS_KO_L2_CHECKS_TO_START_");
            List<string> ls_L1_CHECKS_KO_L2_CHECKS_IN_PROGRESS_ = CommonMethod.GetAttributeValues(lsUserCheckList[i], "UserCheck", "_L1_CHECKS_KO_L2_CHECKS_IN_PROGRESS_");
            List<string> ls_L1_CHECKS_KO_L2_CHECKS_OK_ = CommonMethod.GetAttributeValues(lsUserCheckList[i], "UserCheck", "_L1_CHECKS_KO_L2_CHECKS_OK_");
            List<string> ls_L1_CHECKS_KO_L2_CHECKS_KO_ = CommonMethod.GetAttributeValues(lsUserCheckList[i], "UserCheck", "_L1_CHECKS_KO_L2_CHECKS_KO_");
            List<string> ls_L1_NO_CHECK_L2_CHECKS_IN_PROGRESS_ = CommonMethod.GetAttributeValues(lsUserCheckList[i], "UserCheck", "_L1_NO_CHECK_L2_CHECKS_IN_PROGRESS_");
            List<string> ls_L1_NO_CHECK_L2_CHECKS_OK_ = CommonMethod.GetAttributeValues(lsUserCheckList[i], "UserCheck", "_L1_NO_CHECK_L2_CHECKS_OK_");
            List<string> ls_L1_NO_CHECK_L2_CHECKS_KO_ = CommonMethod.GetAttributeValues(lsUserCheckList[i], "UserCheck", "_L1_NO_CHECK_L2_CHECKS_KO_");
            List<string> ls_TreatmentHourAverage = CommonMethod.GetAttributeValues(lsUserCheckList[i], "UserCheck", "iTreatmentHourAverage");

            List<string> lsUserGroupTAG = CommonMethod.GetAttributeValues(lsUserCheckList[i], "UserCheck", "sUserGroupTAG");
            List<string> lsCheckLevel = CommonMethod.GetAttributeValues(lsUserCheckList[i], "UserCheck", "iCheckLevel");
            List<string> lsManager = CommonMethod.GetAttributeValues(lsUserCheckList[i], "UserCheck", "bManager");
            List<string> lsSysAdmin = CommonMethod.GetAttributeValues(lsUserCheckList[i], "UserCheck", "bSysAdmin");

            string sFirstName = (lsFirstName != null && lsFirstName.Count > 0) ? lsFirstName[0] : "";
            string sLastName = (lsLastName != null && lsLastName.Count > 0) ? lsLastName[0] : "";
            string _L1_CHECKS_IN_PROGRESS_ = (ls_L1_CHECKS_IN_PROGRESS_ != null && ls_L1_CHECKS_IN_PROGRESS_.Count > 0 && ls_L1_CHECKS_IN_PROGRESS_[0].Trim().Length > 0) ? ls_L1_CHECKS_IN_PROGRESS_[0] : "0";
            string _L1_CHECKS_OK_NO_CHANGES_ = (ls_L1_CHECKS_OK_NO_CHANGES_ != null && ls_L1_CHECKS_OK_NO_CHANGES_.Count > 0 && ls_L1_CHECKS_OK_NO_CHANGES_[0].Trim().Length > 0) ? ls_L1_CHECKS_OK_NO_CHANGES_[0] : "0";
            string _L1_CHECKS_OK_WITH_CHANGES_ = (ls_L1_CHECKS_OK_WITH_CHANGES_ != null && ls_L1_CHECKS_OK_WITH_CHANGES_.Count > 0 && ls_L1_CHECKS_OK_WITH_CHANGES_[0].Trim().Length > 0) ? ls_L1_CHECKS_OK_WITH_CHANGES_[0] : "0";
            string _L1_CHECKS_KO_NO_L2_ = (ls_L1_CHECKS_KO_NO_L2_ != null && ls_L1_CHECKS_KO_NO_L2_.Count > 0 && ls_L1_CHECKS_KO_NO_L2_[0].Trim().Length > 0) ? ls_L1_CHECKS_KO_NO_L2_[0] : "0";
            string _L1_CHECKS_KO_L2_CHECKS_TO_START_ = (ls_L1_CHECKS_KO_L2_CHECKS_TO_START_ != null && ls_L1_CHECKS_KO_L2_CHECKS_TO_START_.Count > 0 && ls_L1_CHECKS_KO_L2_CHECKS_TO_START_[0].Trim().Length > 0) ? ls_L1_CHECKS_KO_L2_CHECKS_TO_START_[0] : "0";
            string _L1_CHECKS_KO_L2_CHECKS_IN_PROGRESS_ = (ls_L1_CHECKS_KO_L2_CHECKS_IN_PROGRESS_ != null && ls_L1_CHECKS_KO_L2_CHECKS_IN_PROGRESS_.Count > 0 && ls_L1_CHECKS_KO_L2_CHECKS_IN_PROGRESS_[0].Trim().Length > 0) ? ls_L1_CHECKS_KO_L2_CHECKS_IN_PROGRESS_[0] : "0";
            string _L1_CHECKS_KO_L2_CHECKS_OK_ = (ls_L1_CHECKS_KO_L2_CHECKS_OK_ != null && ls_L1_CHECKS_KO_L2_CHECKS_OK_.Count > 0 && ls_L1_CHECKS_KO_L2_CHECKS_OK_[0].Trim().Length > 0) ? ls_L1_CHECKS_KO_L2_CHECKS_OK_[0] : "0";
            string _L1_CHECKS_KO_L2_CHECKS_KO_ = (ls_L1_CHECKS_KO_L2_CHECKS_KO_ != null && ls_L1_CHECKS_KO_L2_CHECKS_KO_.Count > 0 && ls_L1_CHECKS_KO_L2_CHECKS_KO_[0].Trim().Length > 0) ? ls_L1_CHECKS_KO_L2_CHECKS_KO_[0] : "0";
            string _L1_NO_CHECK_L2_CHECKS_IN_PROGRESS_ = (ls_L1_NO_CHECK_L2_CHECKS_IN_PROGRESS_ != null && ls_L1_NO_CHECK_L2_CHECKS_IN_PROGRESS_.Count > 0 && ls_L1_NO_CHECK_L2_CHECKS_IN_PROGRESS_[0].Trim().Length > 0) ? ls_L1_NO_CHECK_L2_CHECKS_IN_PROGRESS_[0] : "0";
            string _L1_NO_CHECK_L2_CHECKS_OK_ = (ls_L1_NO_CHECK_L2_CHECKS_OK_ != null && ls_L1_NO_CHECK_L2_CHECKS_OK_.Count > 0 && ls_L1_NO_CHECK_L2_CHECKS_OK_[0].Trim().Length > 0) ? ls_L1_NO_CHECK_L2_CHECKS_OK_[0] : "0";
            string _L1_NO_CHECK_L2_CHECKS_KO_ = (ls_L1_NO_CHECK_L2_CHECKS_KO_ != null && ls_L1_NO_CHECK_L2_CHECKS_KO_.Count > 0 && ls_L1_NO_CHECK_L2_CHECKS_KO_[0].Trim().Length > 0) ? ls_L1_NO_CHECK_L2_CHECKS_KO_[0] : "0";
            string TreatmentHourAverage = (ls_TreatmentHourAverage != null && ls_TreatmentHourAverage.Count > 0 && ls_TreatmentHourAverage[0].Trim().Length > 0) ? ls_TreatmentHourAverage[0] : "0";
            //sUserGroupTAG="OdityAgentATD" iCheckLevel="1" bManager="0" bSysAdmin="0"
            bool isManager = (lsManager != null && lsManager.Count > 0 && lsManager[0].Trim() == "1") ? true : false;
            string CheckLevel = (lsCheckLevel != null && lsCheckLevel.Count > 0 && lsCheckLevel[0].Trim().Length > 0) ? lsCheckLevel[0]: "1";

            string sLevel = ((isManager) ? "Manager" : "Agent" ).ToString() + " N" + CheckLevel;

            dt.Rows.Add(new object[] { sLastName, sFirstName, TreatmentHourAverage, _L1_CHECKS_IN_PROGRESS_, _L1_CHECKS_OK_NO_CHANGES_, _L1_CHECKS_OK_WITH_CHANGES_, _L1_CHECKS_KO_NO_L2_,
                _L1_CHECKS_KO_L2_CHECKS_TO_START_, _L1_CHECKS_KO_L2_CHECKS_IN_PROGRESS_, _L1_CHECKS_KO_L2_CHECKS_OK_, _L1_CHECKS_KO_L2_CHECKS_KO_,
                _L1_NO_CHECK_L2_CHECKS_IN_PROGRESS_, _L1_NO_CHECK_L2_CHECKS_OK_, _L1_NO_CHECK_L2_CHECKS_KO_, sLevel });

        }

        return dt;
    }

    public static registration GetRegistrationCheckDetails(string sRegistrationCode, string sRefRegistration)
    {
        registration registr = registration.GetCurrent(false, sRegistrationCode);
        if(registr == null || registr.sRegistrationCode != sRegistrationCode || registr.sRefRegistration != sRefRegistration) { registr = new registration(); }

        try
        {
            registr.iCurrentStep = 1;
            registr.sRegistrationCode = sRegistrationCode;
            registr.sRefRegistration = sRefRegistration;
            string sXmlOUT = "";
            GetRegistrationCheckStepReasonsInfos(int.Parse(sRefRegistration), authentication.GetCurrent().sToken, out sXmlOUT);
            registr.sXmlCheckStepList = sXmlOUT;
            List<string> lsCheckAdult = CommonMethod.GetAttributeValues(sXmlOUT, "ALL_XML_OUT/CurrentRegistrationState", "bCheckStepAdult");
            List<string> lsRefUser = CommonMethod.GetAttributeValues(sXmlOUT, "ALL_XML_OUT/CurrentRegistrationState", "iCheckUser");
            List<string> lsGeneralCurrentStatus = CommonMethod.GetAttributeValues(sXmlOUT, "ALL_XML_OUT/CurrentRegistrationState", "sCheckGeneralStatus");

            registr.sCheckGeneralStatus = (lsGeneralCurrentStatus != null && lsGeneralCurrentStatus.Count > 0) ? lsGeneralCurrentStatus[0] : "";
            registr.bCurrentStepIsAdult = GetAdultValue(sRegistrationCode, (lsCheckAdult != null && lsCheckAdult.Count > 0)? lsCheckAdult[0] : "");
            registr.iRefUserChecker = (lsRefUser != null && lsRefUser.Count > 0) ? int.Parse(lsRefUser[0]) : 0;
        }
        catch (Exception ex) {  }

        return registr;
    }
    public static registration GetRegistrationCheckDetails(string sRegistrationCode, string sRefRegistration, string sRefCustomer)
    {
        registration registr = registration.GetCurrent(false, sRegistrationCode);
        if (registr == null || registr.sRegistrationCode != sRegistrationCode || registr.sRefRegistration != sRefRegistration) { registr = new registration(); }

        try
        {

            registr.sRegistrationCode = sRegistrationCode;
            registr.sRefRegistration = sRefRegistration;
            string sXmlOUT = "";
            GetRegistrationCheckStepReasonsInfos(int.Parse(sRefRegistration), authentication.GetCurrent().sToken, out sXmlOUT);
            registr.sXmlCheckStepList = sXmlOUT;
            List<string> lsCheckAdult = CommonMethod.GetAttributeValues(sXmlOUT, "ALL_XML_OUT/CurrentRegistrationState", "bCheckStepAdult");
            List<string> lsRefUser = CommonMethod.GetAttributeValues(sXmlOUT, "ALL_XML_OUT/CurrentRegistrationState", "iCheckUser");
            List<string> lsCurrentStep = CommonMethod.GetAttributeValues(sXmlOUT, "ALL_XML_OUT/CurrentRegistrationState", "iCurrentCheckStep");
            List<string> lsGeneralCurrentStatus = CommonMethod.GetAttributeValues(sXmlOUT, "ALL_XML_OUT/CurrentRegistrationState", "sCheckGeneralStatus");

            int iCurrentStep;
            iCurrentStep = (lsCurrentStep.Count > 0 && !string.IsNullOrWhiteSpace(lsCurrentStep[0]) && int.TryParse(lsCurrentStep[0], out iCurrentStep)) ? iCurrentStep : 1;

            registr.iCurrentStep = iCurrentStep;
            registr.sCheckGeneralStatus = (lsGeneralCurrentStatus != null && lsGeneralCurrentStatus.Count > 0) ? lsGeneralCurrentStatus[0] : "";
            registr.bCurrentStepIsAdult = GetAdultValue(sRegistrationCode, (lsCheckAdult != null && lsCheckAdult.Count > 0) ? lsCheckAdult[0] : "");
            registr.iRefUserChecker = (lsRefUser != null && lsRefUser.Count > 0) ? int.Parse(lsRefUser[0]) : 0;
            registr.ClientSheet =  GetClientSheetDetails(Client.GetCustomerInformations(int.Parse(sRefCustomer)));
            registr.sRefCustomer = sRefCustomer;
            registr.bMinorAccount = false;
            if (sRegistrationCode.Trim().Substring(0, 3) == "CNJ") { registr.bMinorAccount = true; }

            if (registr.bMinorAccount && !string.IsNullOrWhiteSpace(registr.ClientSheet.RefParentCustomer))
            {
                registr.ParentSheet = GetParentSheetDetails(Client.GetCustomerInformations(int.Parse(registr.ClientSheet.RefParentCustomer)));
            }
        }
        catch (Exception ex) { }

        return registr;
    }

    private static clientsheet GetClientSheetDetails(string sXMLCustomerInfos)
    {
        clientsheet _clientsheet = new clientsheet();

        if (!string.IsNullOrWhiteSpace(sXMLCustomerInfos))
        {

            List<string> listCivility = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "PolitenessTAG");
            List<string> listMaidenName = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "MaidenName");
            List<string> listLastName = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "LastName");
            List<string> listNewLastName = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "NewLastName");
            List<string> listMarriedLastName = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "MarriedLastName");
            List<string> listFirstName = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "FirstName");
            List<string> listBirthPlace = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "BirthPlace");
            List<string> listBirthDepartment = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "BirthDepartment");
            List<string> listBirthDate = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "BirthDate");
            List<string> listBirthCountry = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "BirthCountryISO2");

            _clientsheet.Gender = (listCivility != null && listCivility.Count > 0 && listCivility[0] != "M.") ? "M":"F";
            _clientsheet.LastName = (listLastName != null && listLastName.Count > 0) ? listLastName[0] : "";
            _clientsheet.MaritalName = (listMarriedLastName != null && listMarriedLastName.Count > 0) ? listMarriedLastName[0] : "";
            _clientsheet.UseName = (listNewLastName != null && listNewLastName.Count > 0) ? listNewLastName[0] : "";
            _clientsheet.FirstName = (listFirstName != null && listFirstName.Count > 0) ? listFirstName[0] : "";
            _clientsheet.BirthDate = (listBirthDate != null && listBirthDate.Count > 0) ? listBirthDate[0] : "";
            _clientsheet.BirthCountry = (listBirthCountry != null && listBirthCountry.Count > 0) ? listBirthCountry[0] : "";
            _clientsheet.BirthCity = (listBirthPlace != null && listBirthPlace.Count > 0) ? listBirthPlace[0] : "";
            _clientsheet.BirthDep = (listBirthDepartment != null && listBirthDepartment.Count > 0) ? listBirthDepartment[0] : "";

            List<string> listDocs = CommonMethod.GetTags(sXMLCustomerInfos, "ALL_XML_OUT/Customer/DOCS/DOC");

            for (int i = 0; i < listDocs.Count; i++)
            {
                List<string> listRef = CommonMethod.GetAttributeValues(listDocs[i], "DOC", "RefDocument");
                List<string> listCat = CommonMethod.GetAttributeValues(listDocs[i], "DOC", "CategoryTAG");
                List<string> listType = CommonMethod.GetAttributeValues(listDocs[i], "DOC", "DocumentTAG");
                List<string> listCountry = CommonMethod.GetAttributeValues(listDocs[i], "DOC", "IssuedCountry");
                List<string> listExpire = CommonMethod.GetAttributeValues(listDocs[i], "DOC", "ExpireDate");
                List<string> listDelivery = CommonMethod.GetAttributeValues(listDocs[i], "DOC", "DeliveryDate");
                List<string> listNumber = CommonMethod.GetAttributeValues(listDocs[i], "DOC", "DocumentNumber");

                if (listCat.Count > 0)
                {
                    switch (listCat[0])
                    {
                        case "ID":
                            //txtDeliveryDateID.Text = (listDelivery.Count > 0) ? listDelivery[0] : "";
                            _clientsheet.RefDocument = (listRef.Count > 0) ? listRef[0] : "";
                            _clientsheet.ExpirationDate = (listExpire.Count > 0) ? listExpire[0] : "";
                            _clientsheet.DocumentCountry = (listCountry.Count > 0) ? listCountry[0] : "";
                            _clientsheet.DocumentNumber = (listNumber.Count > 0) ? listNumber[0] : "";
                            _clientsheet.DocumentType = (listType.Count > 0) ? listType[0] : "";
                            //txtNumberID.Text = (listNumber.Count > 0) ? listNumber[0] : "";
                            //hdnRefDocID.Value = (listRef.Count > 0) ? listRef[0] : "";
                            break;
                    }
                }
            }

            //CHECK IF THERE IS PARENT CN ACCOUNT
            List<string> lsParentAccount = CommonMethod.GetTags(sXMLCustomerInfos, "ALL_XML_OUT/Customer/PARENT_ACCOUNTS/ACCOUNT");
            if (lsParentAccount.Count > 0 && !string.IsNullOrWhiteSpace(lsParentAccount[0]))
            {
                List<string> lsAccountKindTAG = CommonMethod.GetAttributeValues(lsParentAccount[0], "ACCOUNT", "AccountKindTAG");
                List<string> lsRefParentCustomer = CommonMethod.GetAttributeValues(lsParentAccount[0], "ACCOUNT", "RefCustomer");

                _clientsheet.isParentHasAccount = (lsAccountKindTAG.Count > 0 && !string.IsNullOrWhiteSpace(lsAccountKindTAG[0]) && lsAccountKindTAG[0] == "NOB") ? true : false;
                _clientsheet.RefParentCustomer = (lsRefParentCustomer.Count > 0 && !string.IsNullOrWhiteSpace(lsRefParentCustomer[0])) ? lsRefParentCustomer[0] : "";
            }
        }

        return _clientsheet;
    }
    private static clientsheet GetParentSheetDetails(string sXMLCustomerInfos)
    {
        clientsheet _clientsheet = new clientsheet();

        if (!string.IsNullOrWhiteSpace(sXMLCustomerInfos))
        {

            List<string> listCivility = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "PolitenessTAG");
            List<string> listMaidenName = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "MaidenName");
            List<string> listLastName = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "LastName");
            List<string> listNewLastName = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "NewLastName");
            List<string> listMarriedLastName = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "MarriedLastName");
            List<string> listFirstName = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "FirstName");
            List<string> listBirthPlace = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "BirthPlace");
            List<string> listBirthDepartment = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "BirthDepartment");
            List<string> listBirthDate = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "BirthDate");
            List<string> listBirthCountry = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "BirthCountryISO2");

            _clientsheet.Gender = (listCivility != null && listCivility.Count > 0 && listCivility[0] != "M.") ? "M" : "F";
            _clientsheet.LastName = (listLastName != null && listLastName.Count > 0) ? listLastName[0] : "";
            _clientsheet.MaritalName = (listMarriedLastName != null && listMarriedLastName.Count > 0) ? listMarriedLastName[0] : "";
            _clientsheet.UseName = (listNewLastName != null && listNewLastName.Count > 0) ? listNewLastName[0] : "";
            _clientsheet.FirstName = (listFirstName != null && listFirstName.Count > 0) ? listFirstName[0] : "";
            _clientsheet.BirthDate = (listBirthDate != null && listBirthDate.Count > 0) ? listBirthDate[0] : "";
            _clientsheet.BirthCountry = (listBirthCountry != null && listBirthCountry.Count > 0) ? listBirthCountry[0] : "";
            _clientsheet.BirthCity = (listBirthPlace != null && listBirthPlace.Count > 0) ? listBirthPlace[0] : "";
            _clientsheet.BirthDep = (listBirthDepartment != null && listBirthDepartment.Count > 0) ? listBirthDepartment[0] : "";

            List<string> listDocs = CommonMethod.GetTags(sXMLCustomerInfos, "ALL_XML_OUT/Customer/DOCS/DOC");

            for (int i = 0; i < listDocs.Count; i++)
            {
                List<string> listRef = CommonMethod.GetAttributeValues(listDocs[i], "DOC", "RefDocument");
                List<string> listCat = CommonMethod.GetAttributeValues(listDocs[i], "DOC", "CategoryTAG");
                List<string> listType = CommonMethod.GetAttributeValues(listDocs[i], "DOC", "DocumentTAG");
                List<string> listCountry = CommonMethod.GetAttributeValues(listDocs[i], "DOC", "IssuedCountry");
                List<string> listExpire = CommonMethod.GetAttributeValues(listDocs[i], "DOC", "ExpireDate");
                List<string> listDelivery = CommonMethod.GetAttributeValues(listDocs[i], "DOC", "DeliveryDate");
                List<string> listNumber = CommonMethod.GetAttributeValues(listDocs[i], "DOC", "DocumentNumber");

                if (listCat.Count > 0)
                {
                    switch (listCat[0])
                    {
                        case "ID":
                            //txtDeliveryDateID.Text = (listDelivery.Count > 0) ? listDelivery[0] : "";
                            _clientsheet.RefDocument = (listRef.Count > 0) ? listRef[0] : "";
                            _clientsheet.ExpirationDate = (listExpire.Count > 0) ? listExpire[0] : "";
                            _clientsheet.DocumentCountry = (listCountry.Count > 0) ? listCountry[0] : "";
                            _clientsheet.DocumentNumber = (listNumber.Count > 0) ? listNumber[0] : "";
                            _clientsheet.DocumentType = (listType.Count > 0) ? listType[0] : "";
                            //txtNumberID.Text = (listNumber.Count > 0) ? listNumber[0] : "";
                            //hdnRefDocID.Value = (listRef.Count > 0) ? listRef[0] : "";
                            break;
                    }
                }
            }
        }
        return _clientsheet;
    }

    public static string GetRegistrationCheckStepReasonsInfos(int iRefRegistration, string sToken, out string sXmlOut)
    {
        sXmlOut = "";

        /*
            DECLARE @sXmlIn VARCHAR(max)
	        DECLARE @sXmlOut VARCHAR(max)

	        SET @sXmlIn = '<ALL_XML_IN><RegistrationInfos iRefTransaction = "3" TOKEN = "DBTREATMENT" /></ALL_XML_IN>'

	        EXEC [Partner].[Registration].[P_GetRegistrationCheckStepsReasonsInfos] @sXmlIn, @sXmlOut OUTPUT
	        SELECT @sXmlOut, CAST(@sXmlOut AS XML)
         */
        SqlConnection conn = null;
        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["PARTNER"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("[Registration].[P_GetRegistrationCheckStepsReasonsInfosV3]", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@sXmlIn", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@sXmlOut", SqlDbType.VarChar, -1);

            cmd.Parameters["@sXmlIn"].Value = new XElement("ALL_XML_IN",
                                                new XElement("RegistrationInfos",
                                                    new XAttribute("iRefTransaction", iRefRegistration.ToString()),
                                                    new XAttribute("TOKEN", sToken)
                                                    )).ToString();
            cmd.Parameters["@sXmlOut"].Direction = ParameterDirection.Output;
            cmd.ExecuteNonQuery();

            sXmlOut = cmd.Parameters["@sXmlOut"].Value.ToString();
            //<ALL_XML_OUT><Benef RefBeneficiary="1" RefCustomer="23" BeneficiaryLabel="test" BeneficiaryName="nom prénom" BIC="12341234123" IBAN="FR121234123412341234123412341234" Checked="0"/></ALL_XML_OUT>
        }
        catch (Exception ex)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return sXmlOut;
    }

    public static bool CheckRegistrationStep(registration registr, int iStep, string sValue, out bool bEndCheck)
    {
        bool bShowNextStep = false;
        bEndCheck = false;
        bool bChanged = false;
        string sReason = "", sReasonLabel = "";
        try
        {
            if (!string.IsNullOrWhiteSpace(sValue))
            {
                DataTable dtNewValue = new DataTable();
                dtNewValue.Columns.Add("Label");
                dtNewValue.Columns.Add("Value");
                GetStepKOReason(registr, sValue, out sReason, out sReasonLabel);
                switch (iStep)
                {
                    case 1: //type document
                        if (UpdateCheckStep(registr)) { bShowNextStep = true; }
                        break;
                    case 2: //pays document
                        string sDocumentType = (registr.bMinorAccount && registr.bCurrentStepIsAdult) ? registr.ParentSheet.DocumentType : registr.ClientSheet.DocumentType;
                        if (!string.IsNullOrEmpty(sDocumentType.Trim()))
                        {
                            dtNewValue.Rows.Add(new object[] { "IssuedCountry", sValue });
                            if (!CheckIdCountry(sValue, sDocumentType)) { if (EndCheckStep(registr, false, sReason, sReasonLabel, GetCRFromValue(false, sValue), iStep, sValue) && SaveClientInformations(registr, iStep, sValue, out bChanged)) { bEndCheck = true; } }
                            else if (UpdateCheckStep(registr, iStep, dtNewValue, sValue) && SaveClientInformations(registr, iStep, sValue, out bChanged)) { bShowNextStep = true; }
                        }
                        break;
                    case 3: //contrôle photocopie
                    case 6: //titre de séjour étudiant algérien
                        if (sValue == "YES") { if (EndCheckStep(registr, false, sReason, sReasonLabel, GetCRFromValue(false, sValue))) { bEndCheck = true; } }
                        else if (UpdateCheckStep(registr)) { bShowNextStep = true; }
                        break;
                    case 4: //Recto verso présents
                    case 5: //Pièce présente et lisible
                    case 7: //contrôle MRZ
                    case 8: //contrôle hologramme
                    case 9: //contrôle RF
                    case 10: //contrôle puce verso titre de séjour
                    case 11: //contrôle police document
                    case 12: //contrôle emplacement numéro document
                    case 13: //contrôle photo document
                    case 14: //contrôle couleur document
                        if (sValue != "YES") { if (EndCheckStep(registr, false, sReason, sReasonLabel, GetCRFromValue(false, sValue))) { bEndCheck = true; } }
                        else if (UpdateCheckStep(registr)) { bShowNextStep = true; }
                        break;
                    case 15: //numéro du document
                        dtNewValue.Rows.Add(new object[] { "DocumentNumber", sValue });
                        if (!string.IsNullOrWhiteSpace(sValue) && UpdateCheckStep(registr, iStep, dtNewValue, sValue) && SaveClientInformations(registr, iStep, sValue, out bChanged)) { bShowNextStep = true; }
                        break;
                    case 17: //prénom
                        dtNewValue.Rows.Add(new object[] { "FirstName", sValue });
                        if (!string.IsNullOrWhiteSpace(sValue) && UpdateCheckStep(registr, iStep, dtNewValue, sValue) && SaveClientInformations(registr, iStep, sValue, out bChanged)) { bShowNextStep = true; }
                        break;
                    case 21: //sexe
                        dtNewValue.Rows.Add(new object[] { "PolitenessTAG", sValue[0] });
                        if ((sValue == "M" || sValue == "F") && UpdateCheckStep(registr, iStep, dtNewValue, sValue) && SaveClientInformations(registr, iStep, sValue, out bChanged)) { bShowNextStep = true; }
                        break;
                    case 18: //date de naissance
                        dtNewValue.Rows.Add(new object[] { "BirthDate", sValue });
                        if (tools.IsDateValid(sValue) && UpdateCheckStep(registr, iStep, dtNewValue, sValue) && SaveClientInformations(registr, iStep, sValue, out bChanged)) { bShowNextStep = true; }
                        break;
                    case 22: //date d'expiration document
                        dtNewValue.Rows.Add(new object[] { "ExpireDate", sValue });
                        if (tools.IsDateValid(sValue))
                        {
                            DateTime dtExpirationDate = DateTime.ParseExact(sValue, "dd/MM/yyyy", null);
                            DateTime dtRegistrationDate = DateTime.ParseExact(registr.sRegistrationDate.Trim().Substring(0, 10), "dd/MM/yyyy", null);
                            if (dtExpirationDate != DateTime.MinValue && dtRegistrationDate != DateTime.MinValue &&
                                dtExpirationDate > dtRegistrationDate && UpdateCheckStep(registr, iStep, dtNewValue, sValue) && SaveClientInformations(registr, iStep, sValue, out bChanged)) { bShowNextStep = true; }
                        }
                        break;
                    case 24: //contrôle signature
                        if (sValue != "YES") { if (EndCheckStep(registr, false, sReason, sReasonLabel, GetCRFromValue(false, sValue))) { bEndCheck = true; } }
                        //else if (registr.bMinorAccount && registr.ClientSheet.isParentHasAccount && (((registr.bCheckParentId && !registr.bCurrentStepIsAdult) || registr.bCurrentStepIsAdult) && UpdateCheckStep(registr))) { bShowNextStep = true; }
                        else if (registr.bMinorAccount && UpdateCheckStep(registr)) { bShowNextStep = true; }
                        else if (EndCheckStep(registr, true, "", "", GetCRFromValue(true, sValue))) { bEndCheck = true; }
                        break;
                    case 25: // type de document (livret de famille ou autre)
                        if (registr.bCurrentStepIsAdult)
                        {
                            if (sValue == "AU") { if (EndCheckStep(registr, false, sReason, sReasonLabel, GetCRFromValue(false, sValue))) { bEndCheck = true; } }
                            else if (UpdateCheckStep(registr)) { bShowNextStep = true; }
                        }
                        break;
                    case 26: // contrôle photocopie (livret de famille ou autre)
                        if (registr.bCurrentStepIsAdult)
                        {
                            if (sValue == "YES") { if (EndCheckStep(registr, false, sReason, sReasonLabel, GetCRFromValue(false, sValue))) { bEndCheck = true; } }
                            else if (UpdateCheckStep(registr)) { bShowNextStep = true; }
                        }
                        break;
                    case 27: // contrôle qualité (livret de famille ou autre)
                        if (registr.bCurrentStepIsAdult)
                        {
                            if (sValue != "YES") { if (EndCheckStep(registr, false, sReason, sReasonLabel, GetCRFromValue(false, sValue))) { bEndCheck = true; } }
                            else if (EndCheckStep(registr, true, "", "", GetCRFromValue(true, sValue))) { bEndCheck = true; }
                        }
                        break;
                        /*case 23: //contrôle photo faciale
                            if (sValue != "OK")
                            {
                                bEndCheck = true;
                            }
                            else
                            {
                                bShowNextStep = true;
                            }
                            break;*/
                }
            }
        }
        catch(Exception ex)
        {
            bShowNextStep = false;
        }

        return bShowNextStep;
    }
    public static bool CheckRegistrationStep(registration registr, int iStep, List<string> lsValue, out bool bEndCheck)
    {
        bool bShowNextStep = false;
        bEndCheck = false;
        string sReason = "", sReasonLabel = "";

        bool bChanged = false;

        if (lsValue != null && lsValue.Count > 0 && !string.IsNullOrWhiteSpace(lsValue[0]))
        {
            DataTable dtNewValue = new DataTable();
            dtNewValue.Columns.Add("Label");
            dtNewValue.Columns.Add("Value");

            GetStepReason(registr, out sReason, out sReasonLabel);
            switch (iStep)
            {
                case 1: //type document
                    if (UpdateCheckStep(registr)) { bShowNextStep = true; }
                    break;
                case 2: //pays document
                    string sDocumentType = (registr.bMinorAccount && registr.bCurrentStepIsAdult) ? registr.ParentSheet.DocumentType : registr.ClientSheet.DocumentType;
                    //if (!CheckIdCountry(lsValue[0], registr.DocumentType)) { if (EndCheckStep(registr, false, sReason, sReasonLabel, GetCRFromValue(false, lsValue[0]))) { bEndCheck = true; } }
                    if (!string.IsNullOrEmpty(sDocumentType.Trim()))
                    {
                        dtNewValue.Rows.Add(new object[] { "IssuedCountry", lsValue[0] });
                        if (!CheckIdCountry(lsValue[0], sDocumentType)) { if (EndCheckStep(registr, false, sReason, sReasonLabel, GetCRFromValue(false, lsValue[0]), iStep, lsValue[0]) && SaveClientInformations(registr, iStep, lsValue[0], out bChanged)) { bEndCheck = true; } }
                        else if (UpdateCheckStep(registr, iStep, dtNewValue, lsValue) && SaveClientInformations(registr, iStep, lsValue[0], out bChanged)) { bShowNextStep = true; }
                    }
                    break;
                case 3: //contrôle photocopie
                case 6: //titre de séjour étudiant algérien
                    if (lsValue[0] == "YES") { if (EndCheckStep(registr, false, sReason, sReasonLabel, GetCRFromValue(false, lsValue[0]))) { bEndCheck = true; } }
                    else if (UpdateCheckStep(registr)) { bShowNextStep = true; }
                    break;
                case 4: //Recto verso présents
                case 5: //Pièce présente et lisible
                case 7: //contrôle MRZ
                case 8: //contrôle hologramme
                case 9: //contrôle RF
                case 10: //contrôle puce verso titre de séjour
                case 11: //contrôle police document
                case 12: //contrôle emplacement numéro document
                case 13: //contrôle photo document
                case 14: //contrôle couleur document
                    if (lsValue[0] != "YES") { if (EndCheckStep(registr, false, sReason, sReasonLabel, GetCRFromValue(false, lsValue[0]))) { bEndCheck = true; } }
                    else if (UpdateCheckStep(registr)) { bShowNextStep = true; }
                    break;
                case 19: //lieu de naissance
                case 20: //pays de naissance
                    if (lsValue.Count == 3 && !string.IsNullOrWhiteSpace(lsValue[0]) && !string.IsNullOrWhiteSpace(lsValue[1]))
                    { 
                        dtNewValue.Rows.Add(new object[] { "BirthCountryISO2", lsValue[0] });
                        dtNewValue.Rows.Add(new object[] { "BirthPlace", lsValue[1] });
                        dtNewValue.Rows.Add(new object[] { "BirthDepartment", lsValue[2] });

                        if((CheckBirthDepartementNeeded(lsValue[0]) && !string.IsNullOrWhiteSpace(lsValue[2]) || !CheckBirthDepartementNeeded(lsValue[0])) &&
                             UpdateCheckStep(registr, iStep, dtNewValue, lsValue) && SaveClientInformations(registr, iStep, lsValue, out bChanged))
                            {
                                bShowNextStep = true;
                            }
                    }
                    break;
                case 16: //nom
                    if (lsValue.Count == 3 && !string.IsNullOrWhiteSpace(lsValue[0]))
                    {
                        dtNewValue.Rows.Add(new object[] { "LastName", lsValue[0] });
                        dtNewValue.Rows.Add(new object[] { "MarriedLastName", lsValue[1] });
                        dtNewValue.Rows.Add(new object[] { "NewLastName", lsValue[2] });

                        if (UpdateCheckStep(registr, iStep, dtNewValue, lsValue) && SaveClientInformations(registr, iStep, lsValue, out bChanged))
                        {
                            bShowNextStep = true;
                        }
                    }
                    break;
                case 24: //contrôle signature
                    if (lsValue[0] != "YES") { if (EndCheckStep(registr, false, sReason, sReasonLabel, GetCRFromValue(false, lsValue[0]))) { bEndCheck = true; } }
                    else if (EndCheckStep(registr, true, "", "", GetCRFromValue(true, lsValue[0]))) { bEndCheck = true; }
                    break;
                    /*case 23: //contrôle photo faciale
                        if (sValue != "OK")
                        {
                            bEndCheck = true;
                        }
                        else
                        {
                            bShowNextStep = true;
                        }
                        break;*/
            }
        }

        return bShowNextStep;
    }

    public static bool CheckRegistrationStep(registration registr, int iStep, string sValue, string sTAGInfo, out bool bEndCheck)
    {
        bool bShowNextStep = false;
        bEndCheck = false;
        bool bChanged = false;
        string sReason = "", sReasonLabel = "";
        try
        {
            if (!string.IsNullOrWhiteSpace(sValue))
            {
                DataTable dtNewValue = new DataTable();
                dtNewValue.Columns.Add("Label");
                dtNewValue.Columns.Add("Value");
                GetStepKOReason(registr, sValue, out sReason, out sReasonLabel);
                switch (iStep)
                {
                    case 17: //prénom
                        dtNewValue.Rows.Add(new object[] { "FirstName", sValue });
                        if (!string.IsNullOrWhiteSpace(sValue) && UpdateCheckStep(registr, iStep, dtNewValue, sValue, sTAGInfo) && SaveClientInformations(registr, iStep, sValue, out bChanged)) { bShowNextStep = true; }
                        break;
                }
            }
        }
        catch (Exception ex)
        {
            bShowNextStep = false;
        }

        return bShowNextStep;
    }
    public static bool CheckRegistrationStep(registration registr, int iStep, List<string> lsValue, string sTAGInfo, out bool bEndCheck)
    {
        bool bShowNextStep = false;
        bEndCheck = false;
        string sReason = "", sReasonLabel = "";

        bool bChanged = false;

        if (lsValue != null && lsValue.Count > 0 && !string.IsNullOrWhiteSpace(lsValue[0]))
        {
            DataTable dtNewValue = new DataTable();
            dtNewValue.Columns.Add("Label");
            dtNewValue.Columns.Add("Value");

            GetStepReason(registr, out sReason, out sReasonLabel);
            switch (iStep)
            {
                case 16: //nom
                    if (lsValue.Count == 3 && !string.IsNullOrWhiteSpace(lsValue[0]))
                    {
                        dtNewValue.Rows.Add(new object[] { "LastName", lsValue[0] });
                        dtNewValue.Rows.Add(new object[] { "MarriedLastName", lsValue[1] });
                        dtNewValue.Rows.Add(new object[] { "NewLastName", lsValue[2] });

                        if (UpdateCheckStep(registr, iStep, dtNewValue, lsValue, sTAGInfo) && SaveClientInformations(registr, iStep, lsValue, out bChanged))
                        {
                            bShowNextStep = true;
                        }
                    }
                    break;
            }
        }

        return bShowNextStep;
    }

    private static bool CheckIdCountry(string iso2, string idType)
    {
        bool isOK = false;
        SqlConnection conn = null;
        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            /*
             [BankSI].[P_CheckIDAllowedOrigine] @ISSUED_COUNTRY_ISO2 VARCHAR(MAX), @DOC_TYPE VARCHAR(MAX)
             */

            SqlCommand cmd = new SqlCommand("[BankSI].[P_CheckIDAllowedOrigine]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@RC", SqlDbType.Int);
            cmd.Parameters.Add("@ISSUED_COUNTRY_ISO2", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@DOC_TYPE", SqlDbType.VarChar, -1);
            cmd.Parameters["@ISSUED_COUNTRY_ISO2"].Value = iso2;
            cmd.Parameters["@DOC_TYPE"].Value = idType;
            cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;
            cmd.ExecuteNonQuery();

            string sRC = cmd.Parameters["@RC"].Value.ToString();

            if (sRC.Trim() == "0")
                isOK = true;

        }
        catch (Exception ex)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return isOK;
    }
    public static bool CheckBirthDepartementNeeded(string iso2)
    {
        bool isCheckNeeded = false;
        switch (iso2)
        {
            case "FR":
            case "GP":
            case "MQ":
            case "GF":
            case "RE":
            case "PM":
            case "YT":
                isCheckNeeded = true;
                break;
        }

        return isCheckNeeded;
    }

    public static bool CheckRegistrationAllocate(registration registr, out string sMessage)
    {
        sMessage = "";
        bool isOK = false;
        //<ALL_XML_IN><RegistrationInfos sCheckInfosUpdateType = "_CHECK_START_" iRefTransaction = "18" iRefUser = "55" TOKEN = "DBTREATMENT" /></ALL_XML_IN>
        string sXmlIN = new XElement("ALL_XML_IN", new XElement("RegistrationInfos",
                                new XAttribute("sCheckInfosUpdateType", "_CHECK_START_"),
                                new XAttribute("iRefTransaction", registr.sRefRegistration),
                                new XAttribute("TOKEN", authentication.GetCurrent().sToken))).ToString();
        string sXmlOUT = "";
        isOK = SetRegistrationCheckInfos(sXmlIN, out sXmlOUT);

        if (!isOK)
        {
            List<string> lsMessage = CommonMethod.GetAttributeValues(sXmlOUT, "ALL_XML_OUT/RegistrationInfos", "ERROR_MSG");
            List<string> lsRC = CommonMethod.GetAttributeValues(sXmlOUT, "ALL_XML_OUT/RegistrationInfos", "RC");
            sMessage = (lsMessage.Count > 0 && !string.IsNullOrWhiteSpace(lsMessage[0])) ? lsMessage[0] : "";
            sMessage += (lsRC.Count > 0 && !string.IsNullOrWhiteSpace(lsRC[0])) ? " (" + lsRC[0] + ")" : "";
        }

        return isOK;
    }
    public static bool CheckRegistrationDeallocate(registration registr)
    {
        bool isOK = false;
        string sXmlIN = new XElement("ALL_XML_IN", new XElement("RegistrationInfos",
                                new XAttribute("sCheckInfosUpdateType", "_CHECK_STOP_"),
                                new XAttribute("iRefTransaction", registr.sRefRegistration),
                                new XAttribute("TOKEN", authentication.GetCurrent().sToken),
                                new XAttribute("iCheckStep", registr.iCurrentStep))).ToString();
        string sXmlOUT = "";
        isOK = SetRegistrationCheckInfos(sXmlIN, out sXmlOUT);

        return isOK;
    }

    public static bool UpdateCheckStep(registration registr)
    {
        bool isOK = false;
        //<ALL_XML_IN><RegistrationInfos sCheckInfosUpdateType = "_CHECK_START_" iRefTransaction = "18" iRefUser = "55" TOKEN = "DBTREATMENT" /></ALL_XML_IN>
        string sXmlIN = new XElement("ALL_XML_IN", new XElement("RegistrationInfos",
                                new XAttribute("sCheckInfosUpdateType", "_CHECK_UPDATE_"),
                                new XAttribute("bCheckStepAdult", (registr.bCurrentStepIsAdult) ? "1" : "0"),
                                new XAttribute("iRefTransaction", registr.sRefRegistration),
                                new XAttribute("TOKEN", authentication.GetCurrent().sToken),
                                new XAttribute("iCheckStep", registr.iCurrentStep))).ToString();

        string sXmlOUT = "";
        isOK = SetRegistrationCheckInfos(sXmlIN, out sXmlOUT);

        return isOK;
    }
    public static bool UpdateCheckStep(registration registr, int iStep, DataTable dtNewValue, string sNewValue)
    {
        bool isOK = false;
        bool bChanged = CheckClientInfoChanged(registr, iStep, sNewValue);

        //<ALL_XML_IN><RegistrationInfos sCheckInfosUpdateType = "_CHECK_START_" iRefTransaction = "18" iRefUser = "55" TOKEN = "DBTREATMENT" /></ALL_XML_IN>
        XElement xRegistrationInfos = new XElement("RegistrationInfos",
                                new XAttribute("sCheckInfosUpdateType", "_CHECK_UPDATE_"),
                                new XAttribute("bCheckStepAdult", (registr.bCurrentStepIsAdult) ? "1" : "0"),
                                new XAttribute("bCustomerInfosChangedByUser", (bChanged) ? "1" : "0"),
                                new XAttribute("iRefTransaction", registr.sRefRegistration),
                                new XAttribute("TOKEN", authentication.GetCurrent().sToken),
                                new XAttribute("iCheckStep", registr.iCurrentStep));

        bool isNewValueListOK = true;
        if (bChanged && dtNewValue.Rows.Count > 0)
        {
            string sNewValueList = "";
            try
            {
                foreach (DataRow dr in dtNewValue.Rows)
                {
                    if (!string.IsNullOrWhiteSpace(dr[0].ToString()))
                    {
                        sNewValueList += dr[0].ToString() + "=" + dr[1].ToString() + ";";
                    }
                }

                if (!string.IsNullOrEmpty(sNewValueList))
                {
                    sNewValueList = sNewValueList.Substring(0, sNewValueList.Length - 1);
                    xRegistrationInfos.Add(new XAttribute("sCustomerNewInfo", sNewValueList));
                }
                else { isNewValueListOK = false; }
            }
            catch (Exception ex)
            {
                isNewValueListOK = false;
            }
        }

        if (isNewValueListOK)
        {
            string sXmlIN = new XElement("ALL_XML_IN", xRegistrationInfos).ToString();

            string sXmlOUT = "";
            isOK = SetRegistrationCheckInfos(sXmlIN, out sXmlOUT);
        }

        return isOK;
    }
    public static bool UpdateCheckStep(registration registr, int iStep, DataTable dtNewValue, List<string> lsNewValue)
    {
        bool isOK = false;
        dtNewValue = new DataTable();
        bool bChanged = CheckClientInfoChanged(registr, iStep, lsNewValue, out dtNewValue);

        //<ALL_XML_IN><RegistrationInfos sCheckInfosUpdateType = "_CHECK_START_" iRefTransaction = "18" iRefUser = "55" TOKEN = "DBTREATMENT" /></ALL_XML_IN>
        XElement xRegistrationInfos = new XElement("RegistrationInfos",
                                new XAttribute("sCheckInfosUpdateType", "_CHECK_UPDATE_"),
                                new XAttribute("bCheckStepAdult", (registr.bCurrentStepIsAdult) ? "1" : "0"),
                                new XAttribute("bCustomerInfosChangedByUser", (bChanged) ? "1" : "0"),
                                new XAttribute("iRefTransaction", registr.sRefRegistration),
                                new XAttribute("TOKEN", authentication.GetCurrent().sToken),
                                new XAttribute("iCheckStep", registr.iCurrentStep));

        bool isNewValueListOK = true;
        if (bChanged && dtNewValue.Rows.Count > 0)
        {
            string sNewValueList = "";
            try
            {
                foreach (DataRow dr in dtNewValue.Rows)
                {
                    if (!string.IsNullOrWhiteSpace(dr[0].ToString()))
                    {
                        sNewValueList += dr[0].ToString() + "=" + dr[1].ToString() + ";";
                    }
                }

                if (!string.IsNullOrEmpty(sNewValueList))
                {
                    sNewValueList = sNewValueList.Substring(0, sNewValueList.Length - 1);
                    xRegistrationInfos.Add(new XAttribute("sCustomerNewInfo", sNewValueList));
                }
                else { isNewValueListOK = false; }
            }
            catch(Exception ex)
            {
                isNewValueListOK = false;
            }
        }

        if (isNewValueListOK) {
            string sXmlIN = new XElement("ALL_XML_IN", xRegistrationInfos).ToString();

            string sXmlOUT = "";
            isOK = SetRegistrationCheckInfos(sXmlIN, out sXmlOUT);
        }

        return isOK;
    }

    public static bool UpdateCheckStep(registration registr, int iStep, DataTable dtNewValue, string sNewValue, string sTAGInfo)
    {
        bool isOK = false;
        bool bChanged = CheckClientInfoChanged(registr, iStep, sNewValue);

        if (!string.IsNullOrWhiteSpace(sTAGInfo))
        {
            //<ALL_XML_IN><RegistrationInfos sCheckInfosUpdateType = "_CHECK_START_" iRefTransaction = "18" iRefUser = "55" TOKEN = "DBTREATMENT" /></ALL_XML_IN>
            XElement xRegistrationInfos = new XElement("RegistrationInfos",
                                    new XAttribute("sCheckInfosUpdateType", "_CHECK_UPDATE_"),
                                    new XAttribute("bCheckStepAdult", (registr.bCurrentStepIsAdult) ? "1" : "0"),
                                    new XAttribute("bCustomerInfosChangedByUser", (bChanged) ? "1" : "0"),
                                    new XAttribute("sCheckTAGInfo", sTAGInfo),
                                    new XAttribute("iRefTransaction", registr.sRefRegistration),
                                    new XAttribute("TOKEN", authentication.GetCurrent().sToken),
                                    new XAttribute("iCheckStep", registr.iCurrentStep));

            bool isNewValueListOK = true;
            if (bChanged && dtNewValue.Rows.Count > 0)
            {
                string sNewValueList = "";
                try
                {
                    foreach (DataRow dr in dtNewValue.Rows)
                    {
                        if (!string.IsNullOrWhiteSpace(dr[0].ToString()))
                        {
                            sNewValueList += dr[0].ToString() + "=" + dr[1].ToString() + ";";
                        }
                    }

                    if (!string.IsNullOrEmpty(sNewValueList))
                    {
                        sNewValueList = sNewValueList.Substring(0, sNewValueList.Length - 1);
                        xRegistrationInfos.Add(new XAttribute("sCustomerNewInfo", sNewValueList));
                    }
                    else { isNewValueListOK = false; }
                }
                catch (Exception ex)
                {
                    isNewValueListOK = false;
                }
            }

            if (isNewValueListOK)
            {
                string sXmlIN = new XElement("ALL_XML_IN", xRegistrationInfos).ToString();

                string sXmlOUT = "";
                isOK = SetRegistrationCheckInfos(sXmlIN, out sXmlOUT);
            }
        }

        return isOK;
    }
    public static bool UpdateCheckStep(registration registr, int iStep, DataTable dtNewValue, List<string> lsNewValue, string sTAGInfo)
    {
        bool isOK = false;
        dtNewValue = new DataTable();
        bool bChanged = CheckClientInfoChanged(registr, iStep, lsNewValue, out dtNewValue);

        if (!string.IsNullOrWhiteSpace(sTAGInfo))
        {
            //<ALL_XML_IN><RegistrationInfos sCheckInfosUpdateType = "_CHECK_START_" iRefTransaction = "18" iRefUser = "55" TOKEN = "DBTREATMENT" /></ALL_XML_IN>
            XElement xRegistrationInfos = new XElement("RegistrationInfos",
                                new XAttribute("sCheckInfosUpdateType", "_CHECK_UPDATE_"),
                                new XAttribute("bCheckStepAdult", (registr.bCurrentStepIsAdult) ? "1" : "0"),
                                new XAttribute("bCustomerInfosChangedByUser", (bChanged) ? "1" : "0"),
                                new XAttribute("sCheckTAGInfo", sTAGInfo),
                                new XAttribute("iRefTransaction", registr.sRefRegistration),
                                new XAttribute("TOKEN", authentication.GetCurrent().sToken),
                                new XAttribute("iCheckStep", registr.iCurrentStep));

            bool isNewValueListOK = true;
            if (bChanged && dtNewValue.Rows.Count > 0)
            {
                string sNewValueList = "";
                try
                {
                    foreach (DataRow dr in dtNewValue.Rows)
                    {
                        if (!string.IsNullOrWhiteSpace(dr[0].ToString()))
                        {
                            sNewValueList += dr[0].ToString() + "=" + dr[1].ToString() + ";";
                        }
                    }

                    if (!string.IsNullOrEmpty(sNewValueList))
                    {
                        sNewValueList = sNewValueList.Substring(0, sNewValueList.Length - 1);
                        xRegistrationInfos.Add(new XAttribute("sCustomerNewInfo", sNewValueList));
                    }
                    else { isNewValueListOK = false; }
                }
                catch (Exception ex)
                {
                    isNewValueListOK = false;
                }
            }

            if (isNewValueListOK)
            {
                string sXmlIN = new XElement("ALL_XML_IN", xRegistrationInfos).ToString();

                string sXmlOUT = "";
                isOK = SetRegistrationCheckInfos(sXmlIN, out sXmlOUT);
            }
        }
        return isOK;
    }

    public static bool EndCheckStep(registration registr, bool bCheckStatus, string sReason, string sReasonLabel, string sCR)
    {
        bool isOK = true;

        registr.ComplianceToApply = new ComplianceInfos();

        string sXmlIN = "";
        if (!bCheckStatus && !string.IsNullOrWhiteSpace(sReason))
        {
            sXmlIN = new XElement("ALL_XML_IN", new XElement("RegistrationInfos",
                                    new XAttribute("sCheckInfosUpdateType", "_CHECK_END_"),
                                    new XAttribute("iRefTransaction", registr.sRefRegistration),
                                    new XAttribute("TOKEN", authentication.GetCurrent().sToken),
                                    new XAttribute("bCheckStepAdult", (registr.bCurrentStepIsAdult) ? "1" : "0"),
                                    new XAttribute("sCheckKOTAGReason", sReason),
                                    new XAttribute("iCheckCR", sCR),
                                    new XAttribute("iCheckStep", registr.iCurrentStep))).ToString();

            
            registr.ComplianceToApply.TAG = sReason;
            registr.ComplianceToApply.Label = sReasonLabel;
            registr.ComplianceToApply.isOK = false;
        }
        else {
            sXmlIN = new XElement("ALL_XML_IN", new XElement("RegistrationInfos",
                                    new XAttribute("sCheckInfosUpdateType", "_CHECK_END_"),
                                    new XAttribute("iRefTransaction", registr.sRefRegistration),
                                    new XAttribute("TOKEN", authentication.GetCurrent().sToken),
                                    new XAttribute("bCheckStepAdult", (registr.bCurrentStepIsAdult) ? "1" : "0"),
                                    (bCheckStatus && !string.IsNullOrWhiteSpace(sReason)) ? new XAttribute("sCheckKOTAGReason", sReason) : null,
                                    new XAttribute("iCheckCR", (bCheckStatus) ? "0" : sCR),
                                    new XAttribute("iCheckStep", registr.iCurrentStep))).ToString();

            registr.ComplianceToApply.TAG = "";
            registr.ComplianceToApply.Label = "";
            registr.ComplianceToApply.isOK = bCheckStatus;
        }

        //string sXmlOUT = "";
        //isOK = SetRegistrationCheckInfos(sXmlIN, out sXmlOUT);
        registr.ComplianceToApply.sXML = sXmlIN;

        return isOK;
    }
    public static bool EndCheckStep(registration registr, bool bCheckStatus, string sReason, string sReasonLabel, string sCR, int iStep, string sNewValue)
    {
        bool isOK = true;
        registr.ComplianceToApply = new ComplianceInfos();
        bool bChanged = CheckClientInfoChanged(registr, iStep, sNewValue);

        XElement xRegistrationInfos = null;
        string sXmlIN = "";

        if (!bCheckStatus && !string.IsNullOrWhiteSpace(sReason))
        {

            xRegistrationInfos = new XElement("RegistrationInfos",
                                    new XAttribute("sCheckInfosUpdateType", "_CHECK_END_"),
                                    new XAttribute("iRefTransaction", registr.sRefRegistration),
                                    new XAttribute("TOKEN", authentication.GetCurrent().sToken),
                                    new XAttribute("bCheckStepAdult", (registr.bCurrentStepIsAdult) ? "1" : "0"),
                                    new XAttribute("bCustomerInfosChangedByUser", (bChanged) ? "1" : "0"),
                                    new XAttribute("sCheckKOTAGReason", sReason),
                                    new XAttribute("iCheckCR", sCR),
                                    new XAttribute("iCheckStep", registr.iCurrentStep));

            if (bChanged && !string.IsNullOrWhiteSpace(sNewValue))
            {
                xRegistrationInfos.Add(new XAttribute("sCustomerNewInfo", sNewValue));
            }

            sXmlIN = new XElement("ALL_XML_IN", xRegistrationInfos).ToString();

            registr.ComplianceToApply.TAG = sReason;
            registr.ComplianceToApply.Label = sReasonLabel;
            registr.ComplianceToApply.isOK = false;
        }
        else
        {
            xRegistrationInfos = new XElement("RegistrationInfos",
                                    new XAttribute("sCheckInfosUpdateType", "_CHECK_END_"),
                                    new XAttribute("iRefTransaction", registr.sRefRegistration),
                                    new XAttribute("TOKEN", authentication.GetCurrent().sToken),
                                    new XAttribute("bCheckStepAdult", (registr.bCurrentStepIsAdult) ? "1" : "0"),
                                    new XAttribute("bCustomerInfosChangedByUser", (bChanged) ? "1" : "0"),
                                    (bCheckStatus && !string.IsNullOrWhiteSpace(sReason)) ? new XAttribute("sCheckKOTAGReason", sReason) : null,
                                    new XAttribute("iCheckCR", (bCheckStatus) ? "0" : sCR),
                                    new XAttribute("iCheckStep", registr.iCurrentStep));

            if (bChanged && !string.IsNullOrWhiteSpace(sNewValue))
            {
                xRegistrationInfos.Add(new XAttribute("sCustomerNewInfo", sNewValue));
            }

            sXmlIN = new XElement("ALL_XML_IN", xRegistrationInfos).ToString();

            registr.ComplianceToApply.TAG = "";
            registr.ComplianceToApply.Label = "";
            registr.ComplianceToApply.isOK = bCheckStatus;
        }

        //string sXmlOUT = "";
        //isOK = SetRegistrationCheckInfos(sXmlIN, out sXmlOUT);
        registr.ComplianceToApply.sXML = sXmlIN;

        return isOK;
    }

    public static bool ValidateEndCheckStep(registration registr, out string sXmlOUT)
    {
        sXmlOUT = "";
        bool isOK = false;

        if(registr.ComplianceToApply != null && !string.IsNullOrEmpty(registr.ComplianceToApply.sXML.Trim()))
        {
            isOK = SetRegistrationCheckInfos(registr.ComplianceToApply.sXML.Trim(), out sXmlOUT);
        }

        return isOK;
    }

    private static string GetCRFromValue(bool bCheckStatus, string sValue)
    {
        string sCR = "99";

        if (bCheckStatus)
        {
            sCR = "0";
        }
        else
        {
            switch (sValue)
            {
                default:
                case "YES":
                case "NO":
                    sCR = "1";
                    break;
                case "2":
                    sCR = "2";
                    break;
                case "AU":
                    sCR = "3";
                    break;
            }
        }


        return sCR;
    }

    private static bool GetStepReason(registration registr, out string sReason, out string sReasonLabel)
    {
        sReason = "";
        sReasonLabel = "";
        bool isOK = true;
        string sDocumentType = (registr.bMinorAccount && registr.bCurrentStepIsAdult) ? registr.ParentSheet.DocumentType : registr.ClientSheet.DocumentType;

        try
        {
            List<string> lsStepReasonList = CommonMethod.GetTags(registr.sXmlCheckStepList, "ALL_XML_OUT/CheckStepsReasons/StepsReasons");
            for (int i = 0; i < lsStepReasonList.Count; i++)
            {
                List<string> lsStep = CommonMethod.GetAttributeValues(lsStepReasonList[i], "StepsReasons/Step", "iStep");

                if (int.Parse(lsStep[0]) == registr.iCurrentStep)
                {
                    List<string> lsReasonList = CommonMethod.GetTags(lsStepReasonList[i], "StepsReasons/Reason");
                    for (int j = 0; j < lsReasonList.Count; j++)
                    {
                        List<string> lsAdultDocumentReason = CommonMethod.GetAttributeValues(lsReasonList[j], "Reason", "bAdultDocumentReason");
                        List<string> lsDocumentTypeCode = CommonMethod.GetAttributeValues(lsReasonList[j], "Reason", "sDocumentTypeCode");
                        List<string> lsTAGReason = CommonMethod.GetAttributeValues(lsReasonList[j], "Reason", "sTAGReason");
                        List<string> lsCheckKOReasonType = CommonMethod.GetAttributeValues(lsReasonList[j], "Reason", "iCheckKOReasonType");

                        if (lsTAGReason != null && lsTAGReason.Count > 0 && 
                            lsCheckKOReasonType != null && lsCheckKOReasonType.Count > 0 && lsCheckKOReasonType[0].Trim() == "1")
                        {

                            bool bIsAdultCheck = (lsAdultDocumentReason != null && lsAdultDocumentReason.Count > 0 && !string.IsNullOrWhiteSpace(lsAdultDocumentReason[0])) ? true : false;
                            bool bIsDocumentTypeCheck = (lsDocumentTypeCode != null && lsDocumentTypeCode.Count > 0 && !string.IsNullOrWhiteSpace(lsDocumentTypeCode[0])) ? true : false;
                            bool bAdultMatch = true;
                            bool bDocumentTypeMatch = true;
                            //string sDocumentType = "";

                            if (bIsAdultCheck && !GetCheckAdultReasonValue(registr, lsAdultDocumentReason[0]))
                            {
                                bAdultMatch = false;
                            }

                            if (bIsDocumentTypeCheck && ((lsDocumentTypeCode != null && lsDocumentTypeCode.Count > 0) ? lsDocumentTypeCode[0] : sDocumentType).ToString() != sDocumentType)
                            {
                                bDocumentTypeMatch = false;
                            }

                            if (bAdultMatch && bDocumentTypeMatch)
                            {
                                sReason = lsTAGReason[0];
                            }
                        }
                    }
                }
            }
        }
        catch(Exception ex)
        {
            isOK = false;
        }

        return isOK;
    }
    private static bool GetStepKOReason(registration registr, string sValue, out string sReason, out string sReasonLabel)
    {
        sReason = "";
        sReasonLabel = "";
        bool isOK = true;

        string sCheckKOReasonType = "1";

        switch (sValue)
        {
            default:
            case "YES":
            case "NO":
                sCheckKOReasonType = "1";
                break;
            case "2":
                sCheckKOReasonType = "2";
                break;
            case "AU":
                sCheckKOReasonType = "3";
                break;
        }

        try
        {
            string sDocumentType = (registr.bMinorAccount && registr.bCurrentStepIsAdult) ? registr.ParentSheet.DocumentType : registr.ClientSheet.DocumentType;

            string sParentalAuthorityProofDocumentType = (!string.IsNullOrWhiteSpace(registr.sParentalAuthorityProofDocumentType)) ? registr.sParentalAuthorityProofDocumentType : "";
            
            List <string> lsStepReasonList = CommonMethod.GetTags(registr.sXmlCheckStepList, "ALL_XML_OUT/CheckStepsReasons/StepsReasons");
            for (int i = 0; i < lsStepReasonList.Count; i++)
            {
                List<string> lsStep = CommonMethod.GetAttributeValues(lsStepReasonList[i], "StepsReasons/Step", "iStep");

                if (int.Parse(lsStep[0]) == registr.iCurrentStep)
                {
                    List<string> lsReasonList = CommonMethod.GetTags(lsStepReasonList[i], "StepsReasons/Reason");
                    for (int j = 0; j < lsReasonList.Count; j++)
                    {
                        List<string> lsAdultDocumentReason = CommonMethod.GetAttributeValues(lsReasonList[j], "Reason", "bAdultDocumentReason");
                        List<string> lsDocumentTypeCode = CommonMethod.GetAttributeValues(lsReasonList[j], "Reason", "sDocumentTypeCode");
                        List<string> lsTAGReason = CommonMethod.GetAttributeValues(lsReasonList[j], "Reason", "sTAGReason");
                        List<string> lsLabel = CommonMethod.GetAttributeValues(lsReasonList[j], "Reason", "sLabel");
                        List<string> lsCheckKOReasonType = CommonMethod.GetAttributeValues(lsReasonList[j], "Reason", "iCheckKOReasonType");

                        if (lsTAGReason != null && lsTAGReason.Count > 0 &&
                            lsCheckKOReasonType != null && lsCheckKOReasonType.Count > 0 && lsCheckKOReasonType[0].Trim() == sCheckKOReasonType)
                        {

                            bool bIsAdultCheck = (lsAdultDocumentReason != null && lsAdultDocumentReason.Count > 0 && !string.IsNullOrWhiteSpace(lsAdultDocumentReason[0])) ? true : false;
                            bool bIsDocumentTypeCheck = (lsDocumentTypeCode != null && lsDocumentTypeCode.Count > 0 && !string.IsNullOrWhiteSpace(lsDocumentTypeCode[0])) ? true : false;
                            bool bAdultMatch = true;
                            bool bDocumentTypeMatch = true;

                            string sCheckDocumentType = (lsDocumentTypeCode != null && lsDocumentTypeCode.Count > 0 && !string.IsNullOrWhiteSpace(lsDocumentTypeCode[0])) ? lsDocumentTypeCode[0] : "";
                            
                            //string sDocumentType = "";

                            if (bIsAdultCheck && !GetCheckAdultReasonValue(registr, lsAdultDocumentReason[0]))
                            {
                                bAdultMatch = false;
                            }

                            if (bIsDocumentTypeCheck && ((!string.IsNullOrWhiteSpace(sCheckDocumentType))? sCheckDocumentType: sDocumentType).ToString() != sDocumentType)
                            {
                                bDocumentTypeMatch = false;
                            }

                            if (bAdultMatch && bDocumentTypeMatch)
                            {
                                sReason = lsTAGReason[0];
                                sReasonLabel = lsLabel[0];
                            }
                            else if (sCheckDocumentType == "FB" && registr.bCurrentStepIsAdult && registr.iCurrentStep == 26) //check family book copy
                            {
                                sReason = lsTAGReason[0];
                                sReasonLabel = lsLabel[0];
                            }
                            else if(sCheckDocumentType == sParentalAuthorityProofDocumentType && registr.bCurrentStepIsAdult && registr.iCurrentStep == 27)
                            {
                                sReason = lsTAGReason[0];
                                sReasonLabel = lsLabel[0];
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            isOK = false;
        }

        return isOK;
    }
    private static bool GetCheckKOReasonType(registration registr, string sTAGReason, out string sCR)
    {
        sCR = "1";
        bool isOK = true;
        string sDocumentType = (registr.bMinorAccount && registr.bCurrentStepIsAdult) ? registr.ParentSheet.DocumentType : registr.ClientSheet.DocumentType;

        try
        {
            List<string> lsStepReasonList = CommonMethod.GetTags(registr.sXmlCheckStepList, "ALL_XML_OUT/CheckStepsReasons/StepsReasons");
            for (int i = 0; i < lsStepReasonList.Count; i++)
            {
                List<string> lsStep = CommonMethod.GetAttributeValues(lsStepReasonList[i], "StepsReasons/Step", "iStep");
                if (int.Parse(lsStep[0]) == registr.iCurrentStep)
                {
                    List<string> lsReasonList = CommonMethod.GetTags(lsStepReasonList[i], "StepsReasons/Reason");
                    for (int j = 0; j < lsReasonList.Count; j++)
                    {
                        List<string> lsAdultDocumentReason = CommonMethod.GetAttributeValues(lsReasonList[j], "Reason", "bAdultDocumentReason");
                        List<string> lsDocumentTypeCode = CommonMethod.GetAttributeValues(lsReasonList[j], "Reason", "sDocumentTypeCode");
                        List<string> lsTAGReason = CommonMethod.GetAttributeValues(lsReasonList[j], "Reason", "sTAGReason");
                        List<string> lsCheckKoReasonType = CommonMethod.GetAttributeValues(lsReasonList[j], "Reason", "iCheckKOReasonType");

                        if (lsTAGReason != null && lsTAGReason.Count > 0 && 
                            lsCheckKoReasonType != null && lsCheckKoReasonType.Count > 0 && !string.IsNullOrWhiteSpace(lsCheckKoReasonType[0]) && 
                            sTAGReason == lsTAGReason[0])
                        {

                            bool bIsAdultCheck = (lsAdultDocumentReason != null && lsAdultDocumentReason.Count > 0 && !string.IsNullOrWhiteSpace(lsAdultDocumentReason[0])) ? true : false;
                            bool bIsDocumentTypeCheck = (lsDocumentTypeCode != null && lsDocumentTypeCode.Count > 0 && !string.IsNullOrWhiteSpace(lsDocumentTypeCode[0])) ? true : false;
                            bool bAdultMatch = true;
                            bool bDocumentTypeMatch = true;
                            //string sDocumentType = "";

                            if (bIsAdultCheck && !GetCheckAdultReasonValue(registr, lsAdultDocumentReason[0]))
                            {
                                bAdultMatch = false;
                            }

                            if (bIsDocumentTypeCheck && ((lsDocumentTypeCode != null && lsDocumentTypeCode.Count > 0) ? lsDocumentTypeCode[0] :sDocumentType).ToString() != sDocumentType)
                            {
                                bDocumentTypeMatch = false;
                            }

                            if (bAdultMatch && bDocumentTypeMatch)
                            {
                                sCR = lsCheckKoReasonType[0];
                            }
                        }
                    }
                }
            }
        }
        catch(Exception ex)
        {
            isOK = false;
        }

        return isOK;
    }

    public static bool GetStepInfoList(registration registr, int iStep, out DataTable dtTagInfoList)
    {
        bool isTagInfo = false;

        dtTagInfoList = new DataTable();
        dtTagInfoList.Columns.Add("TAG");
        dtTagInfoList.Columns.Add("Label");

        //registr.liStepInfo = new List<int>();

        if (!string.IsNullOrWhiteSpace(registr.sXmlCheckStepList))
        {
            List<string> lsStepReasons = CommonMethod.GetTags(registr.sXmlCheckStepList, "ALL_XML_OUT/CheckStepsReasons/StepsReasons");
            for (int i = 0; i < lsStepReasons.Count; i++)
            {
                List<string> lsStep = CommonMethod.GetAttributeValues(lsStepReasons[i], "StepsReasons/Step", "iStep");
                List<string> lsInfoLabel = CommonMethod.GetAttributeValues(lsStepReasons[i], "StepsReasons/Info", "sLabel");
                List<string> lsInfoTAG = CommonMethod.GetAttributeValues(lsStepReasons[i], "StepsReasons/Info", "sTAGInfo");
                if (lsStep.Count > 0 && lsStep[0] == iStep.ToString() &&
                    lsInfoLabel.Count > 0 && lsInfoTAG.Count > 0 &&
                    lsInfoLabel.Count == lsInfoTAG.Count)
                {
                    if(registr.liStepInfo == null || registr.liStepInfo.Count == 0)
                    {
                        registr.liStepInfo = new List<int>();
                        registr.liStepInfo.Add(iStep);
                    }
                    else if (!registr.liStepInfo.Contains(iStep)) { registr.liStepInfo.Add(iStep); }

                    isTagInfo = true;
                    for (int j = 0; j < lsInfoTAG.Count; j++)
                    {
                        dtTagInfoList.Rows.Add(new object[] { lsInfoTAG[j], lsInfoLabel[j] });
                    }
                }

            }

        }

        return isTagInfo;
    }

    private static bool SetRegistrationCheckInfos(string sXmlIN, out string sXmlOUT)
    {
        bool isOK = false;
        sXmlOUT = "";
        //EXEC [Partner].[Registration].[P_SetRegistrationCheckInfos] @sXmlIn, @sXmlOut OUTPUT

        SqlConnection conn = null;
        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["PARTNER"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("Registration.P_SetRegistrationCheckInfosV3", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@sXmlIn", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@sXmlOut", SqlDbType.VarChar, -1);

            cmd.Parameters["@sXmlIn"].Value = sXmlIN;
            cmd.Parameters["@sXmlOut"].Direction = ParameterDirection.Output;
            cmd.ExecuteNonQuery();

            sXmlOUT = cmd.Parameters["@sXmlOut"].Value.ToString();
            //<ALL_XML_OUT><Benef RefBeneficiary="1" RefCustomer="23" BeneficiaryLabel="test" BeneficiaryName="nom prénom" BIC="12341234123" IBAN="FR121234123412341234123412341234" Checked="0"/></ALL_XML_OUT>

            //isOK
            //<ALL_XML_OUT><RegistrationInfos RC="0"/></ALL_XML_OUT>
            List<string> lsRC = CommonMethod.GetAttributeValues(sXmlOUT, "ALL_XML_OUT/RegistrationInfos", "RC");
            if (lsRC.Count > 0 && lsRC[0].Trim() == "0") { isOK = true; }

        }
        catch (Exception ex)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return isOK;
    }

    private static bool GetAdultValue(string sRegistrationCode, string sAdultValue)
    {
        bool isAdult = true;

        if (sRegistrationCode.Trim().Substring(0, 3) == "CNJ")
        {
            if (string.IsNullOrWhiteSpace(sAdultValue) || sAdultValue.Trim() != "1") { isAdult = false; }
        }

        return isAdult;
    }
    private static bool GetCheckAdultReasonValue(registration registr, string sAdultValue)
    {
        bool isMatch = false;
        bool isAdultCheck = (!string.IsNullOrWhiteSpace(sAdultValue) && sAdultValue == "1") ? true : false;

        if(isAdultCheck && !registr.bMinorAccount)
        {
            isMatch = true;
        }
        else if (registr.bMinorAccount && isAdultCheck == registr.bCurrentStepIsAdult)
        {
            isMatch = true;
        }

        return isMatch;
    }

    private static bool CheckClientInfoChanged(registration registr, int iStep, string sValue)
    {
        bool bChanged = false;

        try
        {
            string sRefDocument = registr.ClientSheet.RefDocument;
            string sRefCustomer = registr.sRefCustomer;

            if (registr.bMinorAccount && registr.bCurrentStepIsAdult && !registr.ClientSheet.isParentHasAccount)
            {
                sRefDocument = registr.ParentSheet.RefDocument;
                sRefCustomer = registr.ClientSheet.RefParentCustomer;
            }

            XElement xmlIn = new XElement("Customer",
                                        new XAttribute("CashierToken", authentication.GetCurrent().sToken),
                                        new XAttribute("RefCustomer", sRefCustomer));

            switch (iStep)
            {
                case 2: // pays du document
                    string sDocumentCountry = (registr.bMinorAccount && registr.bCurrentStepIsAdult && !registr.ClientSheet.isParentHasAccount) ? registr.ParentSheet.DocumentCountry : registr.ClientSheet.DocumentCountry;
                    if (sValue != sDocumentCountry)
                    {
                        xmlIn.Add(new XElement("DOCS",
                                            new XElement("DOC",
                                                new XAttribute("RefDocument", registr.ClientSheet.RefDocument),
                                                new XAttribute("IssuedCountry", sValue))));
                        bChanged = true;
                    }
                    break;
                case 15: //numéro du document
                    string sDocumentNumber = (registr.bMinorAccount && registr.bCurrentStepIsAdult && !registr.ClientSheet.isParentHasAccount) ? registr.ParentSheet.DocumentNumber : registr.ClientSheet.DocumentNumber;
                    if (sValue != sDocumentNumber)
                    {
                        xmlIn.Add(new XElement("DOCS",
                                            new XElement("DOC",
                                                new XAttribute("RefDocument", registr.ClientSheet.RefDocument),
                                                new XAttribute("DocumentNumber", sValue))));
                        bChanged = true;
                    }
                    break;
                case 17: //prénom
                    string sFirstName = (registr.bMinorAccount && registr.bCurrentStepIsAdult && !registr.ClientSheet.isParentHasAccount) ? registr.ParentSheet.FirstName : registr.ClientSheet.FirstName;
                    if (sValue != sFirstName)
                    {
                        xmlIn.Add(new XAttribute("FirstName", sValue));
                        bChanged = true;
                    }
                    break;
                case 19: //lieu de naissance
                    string sBirthCity = (registr.bMinorAccount && registr.bCurrentStepIsAdult && !registr.ClientSheet.isParentHasAccount) ? registr.ParentSheet.BirthCity : registr.ClientSheet.BirthCity;
                    if (sValue != sBirthCity)
                    {
                        xmlIn.Add(new XAttribute("BirthPlace", sValue));
                        bChanged = true;
                    }
                    break;
                case 20: //pays de naissance
                    string sBirthCountry = (registr.bMinorAccount && registr.bCurrentStepIsAdult && !registr.ClientSheet.isParentHasAccount) ? registr.ParentSheet.BirthCountry : registr.ClientSheet.BirthCountry;
                    if (sValue != sBirthCountry)
                    {
                        xmlIn.Add(new XAttribute("BirthCountryISO2", sValue));
                        bChanged = true;
                    }
                    break;
                case 21: //sexe
                    string sGender = (registr.bMinorAccount && registr.bCurrentStepIsAdult && !registr.ClientSheet.isParentHasAccount) ? registr.ParentSheet.Gender : registr.ClientSheet.Gender;
                    if (sValue != sGender)
                    {
                        xmlIn.Add(new XAttribute("PolitenessTAG", sValue));
                        bChanged = true;
                    }
                    break;
                case 18: //date de naissance
                    string sBirthDate = (registr.bMinorAccount && registr.bCurrentStepIsAdult && !registr.ClientSheet.isParentHasAccount) ? registr.ParentSheet.BirthDate : registr.ClientSheet.BirthDate;
                    if (sValue != sBirthDate)
                    {
                        xmlIn.Add(new XAttribute("BirthDate", sValue));
                        bChanged = true;
                    }
                    break;
                case 22: //date d'expiration document
                    string sExpirationDate = (registr.bMinorAccount && registr.bCurrentStepIsAdult && !registr.ClientSheet.isParentHasAccount) ? registr.ParentSheet.ExpirationDate : registr.ClientSheet.ExpirationDate;
                    if (sValue != sExpirationDate)
                    {
                        xmlIn.Add(new XElement("DOCS",
                                              new XElement("DOC",
                                                  new XAttribute("RefDocument", sRefDocument),
                                                  new XAttribute("ExpireDate", sValue))));
                        bChanged = true;
                    }
                    break;
            }

        }
        catch (Exception ex)
        { }

        return bChanged;
    }
    private static bool CheckClientInfoChanged(registration registr, int iStep, List<string> sValue, out DataTable dtNewValue)
    {
        bool bChanged = false;
        dtNewValue = new DataTable();
        dtNewValue.Columns.Add("Label");
        dtNewValue.Columns.Add("Value");

        try
        {
            string sRefDocument = registr.ClientSheet.RefDocument;
            string sRefCustomer = registr.sRefCustomer;

            if (registr.bMinorAccount && registr.bCurrentStepIsAdult && !registr.ClientSheet.isParentHasAccount)
            {
                sRefDocument = registr.ParentSheet.RefDocument;
                sRefCustomer = registr.ClientSheet.RefParentCustomer;
            }

            switch (iStep)
            {
                case 2: // pays du document
                    string sDocumentCountry = (registr.bMinorAccount && registr.bCurrentStepIsAdult && !registr.ClientSheet.isParentHasAccount) ? registr.ParentSheet.DocumentCountry : registr.ClientSheet.DocumentCountry;
                    if (sValue[0] != sDocumentCountry)
                    {
                        dtNewValue.Rows.Add(new object[] { "IssuedCountry", sValue[0] });
                        bChanged = true;
                    }
                    break;
                case 15: //numéro du document
                    string sDocumentNumber = (registr.bMinorAccount && registr.bCurrentStepIsAdult && !registr.ClientSheet.isParentHasAccount) ? registr.ParentSheet.DocumentNumber : registr.ClientSheet.DocumentNumber;
                    if (sValue[0] != sDocumentNumber)
                    {
                        dtNewValue.Rows.Add(new object[] { "DocumentNumber", sValue[0] });
                        bChanged = true;
                    }
                    break;
                case 16: //nom
                    string sLastName = (registr.bMinorAccount && registr.bCurrentStepIsAdult && !registr.ClientSheet.isParentHasAccount) ? registr.ParentSheet.LastName : registr.ClientSheet.LastName;
                    string sMaritalName = (registr.bMinorAccount && registr.bCurrentStepIsAdult && !registr.ClientSheet.isParentHasAccount) ? registr.ParentSheet.MaritalName : registr.ClientSheet.MaritalName;
                    string sUseName = (registr.bMinorAccount && registr.bCurrentStepIsAdult && !registr.ClientSheet.isParentHasAccount) ? registr.ParentSheet.UseName : registr.ClientSheet.UseName;

                    if (sValue[0] != sLastName)
                    {
                        dtNewValue.Rows.Add(new object[] { "LastName", sValue[0] });
                        bChanged = true;
                    }
                    if (sValue[1] != sMaritalName)
                    {
                        dtNewValue.Rows.Add(new object[] { "MarriedLastName", sValue[1] });
                        bChanged = true;
                    }
                    if (sValue[2] != sUseName)
                    {
                        dtNewValue.Rows.Add(new object[] { "NewLastName", sValue[2] });
                        bChanged = true;
                    }
                    break;
                case 17: //prénom
                    string sFirstName = (registr.bMinorAccount && registr.bCurrentStepIsAdult && !registr.ClientSheet.isParentHasAccount) ? registr.ParentSheet.FirstName : registr.ClientSheet.FirstName;
                    if (sValue[0] != sFirstName)
                    {
                        dtNewValue.Rows.Add(new object[] { "FirstName", sValue[0] });
                        bChanged = true;
                    }
                    break;
                case 19: //lieu de naissance
                case 20: //pays de naissance
                    string sBirthCountry = (registr.bMinorAccount && registr.bCurrentStepIsAdult && !registr.ClientSheet.isParentHasAccount) ? registr.ParentSheet.BirthCountry : registr.ClientSheet.BirthCountry;
                    if (sValue[0] != sBirthCountry)
                    {
                        dtNewValue.Rows.Add(new object[] { "BirthCountryISO2", sValue[0] });
                        bChanged = true;
                    }
                    string sBirthCity = (registr.bMinorAccount && registr.bCurrentStepIsAdult && !registr.ClientSheet.isParentHasAccount) ? registr.ParentSheet.BirthCity : registr.ClientSheet.BirthCity;
                    if (sValue[1] != sBirthCity)
                    {
                        dtNewValue.Rows.Add(new object[] { "BirthPlace", sValue[1] });
                        bChanged = true;
                    }

                    string sBirthDepartment = (registr.bMinorAccount && registr.bCurrentStepIsAdult && !registr.ClientSheet.isParentHasAccount) ? registr.ParentSheet.BirthDep : registr.ClientSheet.BirthDep;
                    if (sValue[2] != sBirthDepartment)
                    {
                        dtNewValue.Rows.Add(new object[] { "BirthDepartment", sValue[2] });
                        bChanged = true;
                    }
                    break;
                case 21: //sexe
                    string sGender = (registr.bMinorAccount && registr.bCurrentStepIsAdult && !registr.ClientSheet.isParentHasAccount) ? registr.ParentSheet.Gender : registr.ClientSheet.Gender;
                    if (sValue[0] != sGender)
                    {
                        dtNewValue.Rows.Add(new object[] { "PolitenessTAG", sValue[0] });
                        bChanged = true;
                    }
                    break;
                case 18: //date de naissance
                    string sBirthDate = (registr.bMinorAccount && registr.bCurrentStepIsAdult && !registr.ClientSheet.isParentHasAccount) ? registr.ParentSheet.BirthDate : registr.ClientSheet.BirthDate;
                    if (sValue[0] != registr.ClientSheet.BirthDate)
                    {
                        dtNewValue.Rows.Add(new object[] { "BirthDate", sValue[0] });
                        bChanged = true;
                    }
                    break;
                case 22: //date d'expiration document
                    string sExpirationDate = (registr.bMinorAccount && registr.bCurrentStepIsAdult && !registr.ClientSheet.isParentHasAccount) ? registr.ParentSheet.ExpirationDate : registr.ClientSheet.ExpirationDate;
                    if (sValue[0] != sExpirationDate)
                    {
                        dtNewValue.Rows.Add(new object[] { "ExpireDate", sValue[0] });
                        bChanged = true;
                    }
                    break;
            }
        }
        catch (Exception ex)
        { }

        return bChanged;
    }

    private static bool SaveClientInformations(registration registr, int iStep, List<string> sValue, out bool bChanged)
    {
        bool isOK = false;
        bool isNeedSave = false;
        bChanged = false;

        try
        {
            string sRefDocument = registr.ClientSheet.RefDocument;
            string sRefCustomer = registr.sRefCustomer;

            if (registr.bMinorAccount && registr.bCurrentStepIsAdult && !registr.ClientSheet.isParentHasAccount)
            {
                sRefDocument = registr.ParentSheet.RefDocument;
                sRefCustomer = registr.ClientSheet.RefParentCustomer;
            }

            XElement xmlIn = new XElement("Customer",
                                        new XAttribute("CashierToken", authentication.GetCurrent().sToken),
                                        new XAttribute("RefCustomer", sRefCustomer));

            switch (iStep)
            {
                case 2: // pays du document
                    string sDocumentCountry = (registr.bMinorAccount && registr.bCurrentStepIsAdult && !registr.ClientSheet.isParentHasAccount) ? registr.ParentSheet.DocumentCountry : registr.ClientSheet.DocumentCountry;
                    if (sValue[0] != sDocumentCountry)
                    {
                        xmlIn.Add(new XElement("DOCS",
                                            new XElement("DOC",
                                                new XAttribute("RefDocument", registr.ClientSheet.RefDocument),
                                                new XAttribute("IssuedCountry", sValue))));
                        isNeedSave = true;
                    }
                    break;
                case 15: //numéro du document
                    string sDocumentNumber = (registr.bMinorAccount && registr.bCurrentStepIsAdult && !registr.ClientSheet.isParentHasAccount) ? registr.ParentSheet.DocumentNumber : registr.ClientSheet.DocumentNumber;
                    if (sValue[0] != sDocumentNumber)
                    {
                        xmlIn.Add(new XElement("DOCS",
                                            new XElement("DOC",
                                                new XAttribute("RefDocument", sRefDocument),
                                                new XAttribute("DocumentNumber", sValue))));
                        isNeedSave = true;
                    }
                    break;
                case 16: //nom
                    string sLastName = (registr.bMinorAccount && registr.bCurrentStepIsAdult && !registr.ClientSheet.isParentHasAccount) ? registr.ParentSheet.LastName : registr.ClientSheet.LastName;
                    string sMaritalName = (registr.bMinorAccount && registr.bCurrentStepIsAdult && !registr.ClientSheet.isParentHasAccount) ? registr.ParentSheet.MaritalName : registr.ClientSheet.MaritalName;
                    string sUseName = (registr.bMinorAccount && registr.bCurrentStepIsAdult && !registr.ClientSheet.isParentHasAccount) ? registr.ParentSheet.UseName : registr.ClientSheet.UseName;

                    if (sValue[0] != sLastName)
                    {
                        xmlIn.Add(new XAttribute("LastName", sValue[0]));
                        isNeedSave = true;
                    }
                    if (sValue[1] != sMaritalName)
                    {
                        xmlIn.Add(new XAttribute("MarriedLastName", sValue[1]));
                        isNeedSave = true;
                    }
                    if (sValue[2] != sUseName)
                    {
                        xmlIn.Add(new XAttribute("NewLastName", sValue[2]));
                        isNeedSave = true;
                    }
                    break;
                case 17: //prénom
                    string sFirstName = (registr.bMinorAccount && registr.bCurrentStepIsAdult && !registr.ClientSheet.isParentHasAccount) ? registr.ParentSheet.FirstName : registr.ClientSheet.FirstName;
                    if (sValue[0] != sFirstName)
                    {
                        xmlIn.Add(new XAttribute("FirstName", sValue[0]));
                        isNeedSave = true;
                    }
                    break;
                case 19: //lieu de naissance
                case 20: //pays de naissance
                    string sBirthCountry = (registr.bMinorAccount && registr.bCurrentStepIsAdult && !registr.ClientSheet.isParentHasAccount) ? registr.ParentSheet.BirthCountry : registr.ClientSheet.BirthCountry;
                    if (sValue[0] != sBirthCountry)
                    {
                        xmlIn.Add(new XAttribute("BirthCountryISO2", sValue[0]));
                        isNeedSave = true;
                    }
                    string sBirthCity = (registr.bMinorAccount && registr.bCurrentStepIsAdult && !registr.ClientSheet.isParentHasAccount) ? registr.ParentSheet.BirthCity : registr.ClientSheet.BirthCity;
                    if (sValue[1] != sBirthCity)
                    {
                        xmlIn.Add(new XAttribute("BirthPlace", sValue[1]));
                        isNeedSave = true;
                    }

                    string sBirthDepartment = (registr.bMinorAccount && registr.bCurrentStepIsAdult && !registr.ClientSheet.isParentHasAccount) ? registr.ParentSheet.BirthDep : registr.ClientSheet.BirthDep;
                    if (sValue[2] != sBirthDepartment)
                    {
                        xmlIn.Add(new XAttribute("BirthDepartment", sValue[2]));
                        isNeedSave = true;
                    }
                    break;
                case 21: //sexe
                    string sGender = (registr.bMinorAccount && registr.bCurrentStepIsAdult && !registr.ClientSheet.isParentHasAccount) ? registr.ParentSheet.Gender : registr.ClientSheet.Gender;
                    if (sValue[0] != sGender)
                    {
                        xmlIn.Add(new XAttribute("PolitenessTAG", sValue[0]));
                        isNeedSave = true;
                    }
                    break;
                case 18: //date de naissance
                    string sBirthDate = (registr.bMinorAccount && registr.bCurrentStepIsAdult && !registr.ClientSheet.isParentHasAccount) ? registr.ParentSheet.BirthDate : registr.ClientSheet.BirthDate;
                    if (sValue[0] != registr.ClientSheet.BirthDate)
                    {
                        xmlIn.Add(new XAttribute("BirthDate", sValue[0]));
                        isNeedSave = true;
                    }
                    break;
                case 22: //date d'expiration document
                    string sExpirationDate = (registr.bMinorAccount && registr.bCurrentStepIsAdult && !registr.ClientSheet.isParentHasAccount) ? registr.ParentSheet.ExpirationDate : registr.ClientSheet.ExpirationDate;
                    if (sValue[0] != sExpirationDate)
                    {
                        xmlIn.Add(new XElement("DOCS",
                                              new XElement("DOC",
                                                  new XAttribute("RefDocument", sRefDocument),
                                                  new XAttribute("ExpireDate", sValue))));
                        isNeedSave = true;
                    }
                    break;
            }

            if (isNeedSave)
            {
                string sXmlIN = new XElement("ALL_XML_IN", xmlIn).ToString();
                isOK = Client.SaveInformations(sXmlIN);
                bChanged = true;
            }
            else
            {
                isOK = true;
            }
        }
        catch (Exception ex)
        { }

        return isOK;
    }
    private static bool SaveClientInformations(registration registr, int iStep, string sValue, out bool bChanged)
    {
        bool isOK = false;
        bool isNeedSave = false;
        bChanged = false;

        try
        {
            string sRefDocument = registr.ClientSheet.RefDocument;
            string sRefCustomer = registr.sRefCustomer;

            if (registr.bMinorAccount && registr.bCurrentStepIsAdult && !registr.ClientSheet.isParentHasAccount)
            {
                sRefDocument = registr.ParentSheet.RefDocument;
                sRefCustomer = registr.ClientSheet.RefParentCustomer;
            }

            XElement xmlIn = new XElement("Customer",
                                        new XAttribute("CashierToken", authentication.GetCurrent().sToken),
                                        new XAttribute("RefCustomer", sRefCustomer));

            switch (iStep)
            {
                case 2: // pays du document
                    string sDocumentCountry = (registr.bMinorAccount && registr.bCurrentStepIsAdult && !registr.ClientSheet.isParentHasAccount) ? registr.ParentSheet.DocumentCountry: registr.ClientSheet.DocumentCountry;
                    if (sValue != sDocumentCountry)
                    {
                        xmlIn.Add(new XElement("DOCS",
                                            new XElement("DOC",
                                                new XAttribute("RefDocument", registr.ClientSheet.RefDocument),
                                                new XAttribute("IssuedCountry", sValue))));
                        isNeedSave = true;
                    }
                    break;
                case 15: //numéro du document
                    string sDocumentNumber = (registr.bMinorAccount && registr.bCurrentStepIsAdult && !registr.ClientSheet.isParentHasAccount) ? registr.ParentSheet.DocumentNumber : registr.ClientSheet.DocumentNumber;
                    if (sValue != sDocumentNumber)
                    {
                        xmlIn.Add(new XElement("DOCS",
                                            new XElement("DOC",
                                                new XAttribute("RefDocument", registr.ClientSheet.RefDocument),
                                                new XAttribute("DocumentNumber", sValue))));
                        isNeedSave = true;
                    }
                    break;
                case 17: //prénom
                    string sFirstName = (registr.bMinorAccount && registr.bCurrentStepIsAdult && !registr.ClientSheet.isParentHasAccount) ? registr.ParentSheet.FirstName : registr.ClientSheet.FirstName;
                    if (sValue != sFirstName)
                    {
                        xmlIn.Add(new XAttribute("FirstName", sValue));
                        isNeedSave = true;
                    }
                    break;
                case 19: //lieu de naissance
                    string sBirthCity = (registr.bMinorAccount && registr.bCurrentStepIsAdult && !registr.ClientSheet.isParentHasAccount) ? registr.ParentSheet.BirthCity : registr.ClientSheet.BirthCity;
                    if (sValue != sBirthCity)
                    {
                        xmlIn.Add(new XAttribute("BirthPlace", sValue));
                        isNeedSave = true;
                    }
                    break;
                case 20: //pays de naissance
                    string sBirthCountry = (registr.bMinorAccount && registr.bCurrentStepIsAdult && !registr.ClientSheet.isParentHasAccount) ? registr.ParentSheet.BirthCountry : registr.ClientSheet.BirthCountry;
                    if (sValue != sBirthCountry)
                    {
                        xmlIn.Add(new XAttribute("BirthCountryISO2", sValue));
                        isNeedSave = true;
                    }
                    break;
                case 21: //sexe
                    string sGender = (registr.bMinorAccount && registr.bCurrentStepIsAdult && !registr.ClientSheet.isParentHasAccount) ? registr.ParentSheet.Gender : registr.ClientSheet.Gender;
                    if (sValue != sGender)
                    {
                        xmlIn.Add(new XAttribute("PolitenessTAG", sValue));
                        isNeedSave = true;
                    }
                    break;
                case 18: //date de naissance
                    string sBirthDate = (registr.bMinorAccount && registr.bCurrentStepIsAdult && !registr.ClientSheet.isParentHasAccount) ? registr.ParentSheet.BirthDate : registr.ClientSheet.BirthDate;
                    if (sValue != sBirthDate)
                    {
                        xmlIn.Add(new XAttribute("BirthDate", sValue));
                        isNeedSave = true;
                    }
                    break;
                case 22: //date d'expiration document
                    string sExpirationDate = (registr.bMinorAccount && registr.bCurrentStepIsAdult && !registr.ClientSheet.isParentHasAccount) ? registr.ParentSheet.ExpirationDate : registr.ClientSheet.ExpirationDate;
                    if (sValue != sExpirationDate)
                    {
                        xmlIn.Add(new XElement("DOCS",
                                              new XElement("DOC",
                                                  new XAttribute("RefDocument", sRefDocument),
                                                  new XAttribute("ExpireDate", sValue))));
                        isNeedSave = true;
                    }
                    break;
            }

            if (isNeedSave)
            {
                string sXmlIN = new XElement("ALL_XML_IN", xmlIn).ToString();
                isOK = Client.SaveInformations(sXmlIN);
                bChanged = true;
            }
            else
            {
                isOK = true;
            }
        }
        catch (Exception ex)
        { }

        return isOK;
    }

    public static string GetRegistrationCodeFromRefCustomer(int iRefCustomer)
    {
        string sRegistrationCode = "";

        
        SqlConnection conn = null;
        DataTable dt = new DataTable();
        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();
            //[BankSI].[F_GetNOBFromRefcustomer] (@RefCustomer INT)
            SqlCommand cmd = new SqlCommand("SELECT [BankSI].[F_GetNOBFromRefcustomer](" + iRefCustomer.ToString() + ")", conn);
            cmd.CommandType = CommandType.Text;
            cmd.ExecuteNonQuery();

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);

            sRegistrationCode = dt.Rows[0][0].ToString();
        }
        catch (Exception ex)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return sRegistrationCode;
    }

    public static string GetLabelFromGeneralStatusValue(string tagValue)
    {
        string sLabel = tagValue;
        switch (tagValue)
        {
            case "_CHECK_NOT_STARTED_":
                sLabel = "<label style=\"color:orange;\">A traiter</label>";
                break;
            case "_L1_CHECKS_KO_L2_CHECKS_TO_START_":
                sLabel = "<label style=\"color:orange;\">En attente N2 (KO N1)</label>";
                break;
            case "_L1_CHECKS_IN_PROGRESS_":
                sLabel = "<label style=\"\">En cours N1</label>";
                break;
            case "_L1_CHECKS_OK_NO_CHANGES_":
                sLabel = "<label style=\"\">Traité OK (N1)</label>";
                break;
            case "_L1_CHECKS_OK_WITH_CHANGES_":
                sLabel = "<label style=\"\">Traité OK (N1)</label>";
                break;
            case "_L1_CHECKS_KO_NO_L2_":
                sLabel = "<label style=\"\">Traité KO (N1)</label>";
                break;
            case "_L1_CHECKS_KO_L2_CHECKS_IN_PROGRESS_":
                sLabel = "<label style=\"\">En cours N2 (KO N1)</label>";
                break;
            case "_L1_CHECKS_KO_L2_CHECKS_OK_":
                sLabel = "<label style=\"\">Traité OK N2 (KO N1)</label>";
                break;
            case "_L1_CHECKS_KO_L2_CHECKS_KO_":
                sLabel = "<label style=\"\">Traité KO N2 (KO N1)</label>";
                break;
            case "_L1_NO_CHECK_L2_CHECKS_IN_PROGRESS_":
                sLabel = "<label style=\"\">En cours N2</label>";
                break;
            case "_L1_NO_CHECK_L2_CHECKS_OK_":
                sLabel = "<label style=\"\">Traité OK N2</label>";
                break;
            case "_L1_NO_CHECK_L2_CHECKS_KO_":
                sLabel = "<label style=\"\">Traité KO N2</label>";
                break;
        }

        return sLabel;
    }

    public static DataTable GetAutoCheckDetails(string sToken, int iRefTransaction)
    {
        DataTable dt = null;
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Partner"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("[Registration].[P_GetRegistrationAutoReportsDetails]", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@sXmlIn", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@sXmlOut", SqlDbType.VarChar, -1);

            cmd.Parameters["@sXmlIn"].Value = new XElement("ALL_XML_IN",
                                                new XElement("RegistrationAutoCheckInfos",
                                                    new XAttribute("TOKEN", sToken),
                                                    new XAttribute("iRefTransaction", iRefTransaction.ToString()))).ToString();
            cmd.Parameters["@sXmlOut"].Direction = ParameterDirection.Output;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            dt = new DataTable();
            da.Fill(dt);
        }
        catch (Exception e)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }
}