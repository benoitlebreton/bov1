﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using XMLMethodLibrary;

[Serializable]
public class Requisition
{
    protected static DataTable dtMotifRequisitionList { get { return (HttpContext.Current.Session["dtMotifRequisitionList"] != null) ? (DataTable)HttpContext.Current.Session["dtMotifRequisitionList"] : null; } set { HttpContext.Current.Session["dtMotifRequisitionList"] = value; } }

    public Requisition()
    {

    }

    public static DataTable GetMotifRequisitionList()
    {
        if (dtMotifRequisitionList != null)
            return dtMotifRequisitionList;

        DataTable dt = new DataTable();
        dt.TableName = "dtGetMotifRequisitionList";
        dt.Columns.Add("Value");
        dt.Columns.Add("Text");

        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_GetRequisitionReasonList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN", new XElement("RequisitionReason")).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if (sXmlOut.Length > 0)
            {
                List<string> listReqReason = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/RequisitionReason");

                for (int i = 0; i < listReqReason.Count; i++)
                {
                    List<string> listRef = CommonMethod.GetAttributeValues(listReqReason[i], "RequisitionReason", "RefReason");
                    List<string> listDesc = CommonMethod.GetAttributeValues(listReqReason[i], "RequisitionReason", "ReasonDescription");

                    dt.Rows.Add(new object[] { listRef[0], listDesc[0] });
                }
            }
        }
        catch (Exception e)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }

    public static DataTable GetRJDCList(int iRefRJDC, int iRefCustomer)
    {
        DataTable dt = new DataTable();
        dt.TableName = "dtGetRJDCList";
        dt.Columns.Add("Ref");
        dt.Columns.Add("Description");
        dt.Columns.Add("Date");

        string sXmlOut = GetRJDCInfos(iRefRJDC, iRefCustomer);

        if (!String.IsNullOrWhiteSpace(sXmlOut))
        {
            List<string> listRJDC = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/RJ_DC");

            for (int i = 0; i < listRJDC.Count; i++)
            {
                List<string> listRef = CommonMethod.GetAttributeValues(listRJDC[i], "RJ_DC", "RefRJDC");
                List<string> listType = CommonMethod.GetAttributeValues(listRJDC[i], "RJ_DC/DOC", "DocType");
                List<string> listTreatmentDate = CommonMethod.GetAttributeValues(listRJDC[i], "RJ_DC/INFOS", "TreatmentDate");

                string sDesc = "";
                if (listType[0] == "REQ")
                {
                    List<string> listReasonsNoSplit = CommonMethod.GetAttributeValues(listRJDC[i], "RJ_DC/INFOS", "Reasons");
                    List<string> listReasons = listReasonsNoSplit[0].Split(';').ToList();

                    DataTable dtReason = new DataTable("dtReason");
                    dtReason = GetMotifRequisitionList();

                    for (int j = 0; j < dtReason.Rows.Count; j++)
                    {
                        if (listReasons.Contains(dtReason.Rows[j]["Value"].ToString()))
                            sDesc += dtReason.Rows[j]["Text"] + ", ";
                    }

                    sDesc = sDesc.Trim();
                    sDesc = sDesc.Substring(0, sDesc.Length - 1);
                }
                else sDesc = "Droit de communication";

                dt.Rows.Add(new object[] { listRef[0], sDesc, listTreatmentDate[0] });
            }
        }

        return dt;
    }

    public static string GetRJDCInfos(int iRefRJDC, int iRefCustomer)
    {
        string sXmlOut = null;
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_GetRequisitionInfosDetails", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);

            XElement xml = new XElement("RJ_DC",
                                new XAttribute("Token", authentication.GetCurrent().sToken));
            if (iRefRJDC != 0)
                xml.Add(new XAttribute("RefRJDC", iRefRJDC.ToString()));
            if (iRefCustomer != 0)
                xml.Add(new XAttribute("RefCustomer", iRefCustomer.ToString()));

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN", xml).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            sXmlOut = cmd.Parameters["@OUT"].Value.ToString();
        }
        catch (Exception e)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return sXmlOut;
    }
}