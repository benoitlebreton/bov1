﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using XMLMethodLibrary;

/// <summary>
/// Description résumée de Client_ID
/// </summary>
public class Client_ID
{
    public static DataTable GetHistoryChangeID(int iRefCustomer)
    {
        SqlConnection conn = null;
        DataTable dt = new DataTable();
        dt.Columns.Add("Date");
        dt.Columns.Add("Kind");
        dt.Columns.Add("OldValue");
        dt.Columns.Add("NewValue");
        dt.Columns.Add("Channel");
        dt.Columns.Add("BOUser");

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_GetCustomerChangeInfo", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@ReturnSelect", SqlDbType.Bit);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN", new XElement("ChangeInfo", new XAttribute("RefCustomer", iRefCustomer.ToString()))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
            cmd.Parameters["@ReturnSelect"].Direction = 0;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if (sXmlOut.Length > 0)
            {
                List<string> listChanges = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/ChangeInfo");

                for (int i = 0; i < listChanges.Count; i++)
                {
                    List<string> listChangeDate = CommonMethod.GetAttributeValues(listChanges[i], "ChangeInfo", "ChangeDate");
                    List<string> listChannel = CommonMethod.GetAttributeValues(listChanges[i], "ChangeInfo", "Channel");
                    List<string> listKind = CommonMethod.GetAttributeValues(listChanges[i], "ChangeInfo", "Kind");
                    List<string> listOldValue = CommonMethod.GetAttributeValues(listChanges[i], "ChangeInfo", "OldValue");
                    List<string> listNewValue = CommonMethod.GetAttributeValues(listChanges[i], "ChangeInfo", "NewValue");
                    List<string> listBOUser = CommonMethod.GetAttributeValues(listChanges[i], "ChangeInfo", "BOUser");

                    dt.Rows.Add(new object[] { listChangeDate[0], listKind[0], (listOldValue[0].Length > 0) ? listOldValue[0] : "N.D.", listNewValue[0], listChannel[0], listBOUser[0] });
                }
            }
        }
        catch (Exception e)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }
        return dt;
    }

    public static bool ResetWEBPassword(int iRefCustomer, out string sMessage)
    {
        SqlConnection conn = null;
        bool bReseted = false;
        sMessage = "";

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_ResetPassword", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@RefCustomer", SqlDbType.Int);
            cmd.Parameters.Add("@RC", SqlDbType.Int);

            cmd.Parameters["@RefCustomer"].Value = iRefCustomer;
            cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;

            cmd.ExecuteNonQuery();

            int iRC = int.Parse(cmd.Parameters["@RC"].Value.ToString());

            if (iRC == 0)
                bReseted = true;
            else if (iRC == 71008)
                sMessage = "Session terminée. Veuillez vous reconnecter.";
            else sMessage = "Une erreur est survenue.";
        }
        catch (Exception e)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return bReseted;
    }

    public static bool SendWEBID(string sXmlIN, out string sMessage)
    {
        SqlConnection conn = null;
        bool bSent = false;
        sMessage = "";
        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("[web].[P_SentWebIdentifier]", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@RC", SqlDbType.Int);
            cmd.Parameters["@IN"].Value = sXmlIN;
            cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();
            int iRC = int.Parse(cmd.Parameters["@RC"].Value.ToString());

            if (iRC == 0)
            {
                if (sXmlOut.Length > 0)
                {
                    List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Result", "RC");
                    if (listRC.Count > 0 && listRC[0].Trim() == "0") { bSent = true; }
                }
            }
            else if (iRC == 71008)
                sMessage = "Session terminée. Veuillez vous reconnecter.";
            else sMessage = "Une erreur est survenue.";
        }
        catch (Exception e)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return bSent;
    }
}