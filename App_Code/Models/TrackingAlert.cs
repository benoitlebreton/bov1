﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

/// <summary>
/// Description résumée de TrackingAlert
/// </summary>
public class TrackingAlert
{

    private int _daysCriticalAlert;

    public int Id { get; set; }
    public string BOUserRef { get; set; }
    public string BOUserName { get; set; }
    

    public List<Alert> AlertList { get; set; }

    private List<Alert> _criticalAlertList;
    /// <summary>
    /// Retourne la liste des alertes considérées critiques en fonction du nombre de jour
    /// restant pour les traiter
    /// </summary>
    public List<Alert> CriticalAlertList
    {
        get
        {
            if (_criticalAlertList == null)
            {
                _criticalAlertList = new List<Alert>();
                foreach (var idx in AlertList)
                {
                    var result = idx.ListeStep.OrderBy(p => p.Date).FirstOrDefault(m => DateTime.Today.Subtract(m.Date).TotalDays > _daysCriticalAlert);
                    if (result != null)
                    {
                        _criticalAlertList.Add(idx);
                    }
                }
            }

            return _criticalAlertList.OrderByDescending(o=>o.CurrentStep).ToList();
        }
        
    }

    public int NumberCriticalAlert
    {
        get
        {
            return CriticalAlertList.Count;
        }
    }

    public int NumberAlert
    {
        get
        {
            return AlertList.Count;
        }
    }


    public TrackingAlert(int daysCriticalAlert)
    {
        //
        // TODO: ajoutez ici la logique du constructeur
        //
        AlertList = new List<Alert>();
        _daysCriticalAlert = daysCriticalAlert;
        
    }


    
}