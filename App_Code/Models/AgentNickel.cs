﻿using System;

/// <summary>
/// Description résumée de AgentNickel
/// </summary>
public class AgentNickel
{
    public int Id { get; set; }
    public string LastName { get; set; }
    public string FirstName { get; set; }
    public string CompleteName
    {
        get
        {
            return String.Concat(FirstName, " ", LastName);
        }
    }

    public AgentNickel()
    {
        Id = 0;
        LastName = string.Empty;
        FirstName = string.Empty;
    }
}