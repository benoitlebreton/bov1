﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Description résumée de Alert
/// </summary>
public class Alert
{
    public string RefBOUser { get; set; }
    public string RefAlert { get; set; }
    public string AlertLabel { get; set; }
    public string CustomerName { get; set; }
    public string Title
    {
        get
        {
            return string.Concat(CustomerName, " - ", AlertLabel);
        }
    }

    public List<Step> ListeStep { get; set; }
    /// <summary>
    /// Défini si l'alert a été réattribué
    /// </summary>
    public bool IsReassigned { get; set; }
    public DateTime ReassignedDate { get; set; }
    public int CurrentStep { get; set; }
    public string StepMarkers { get; set; }
    

    public DateTime OuvertLe { get
        {
            if (!ListeStep.Any()) return DateTime.MinValue;
            return ListeStep.OrderBy(p => p.Date).First().Date;
        }
    }


    public Alert()
    {
        //
        // TODO: ajoutez ici la logique du constructeur
        //
        ListeStep = new List<Step>();
    }
}