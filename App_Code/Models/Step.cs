﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Description résumée de Step
/// </summary>
public class Step
{
    public DateTime Date { get; set; }
    public string Days { get; set; }
    public string Label { get; set; }
    public string Icon { get; set; }
    public string BOUser { get; set; }

    

    public Step()
    {
        //
        // TODO: ajoutez ici la logique du constructeur
        //
    }
}