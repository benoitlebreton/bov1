﻿using System;

/// <summary>
/// Description résumée de BeneficiaryView
/// Classe utilisé au plus près de la  vue
/// </summary>
[Serializable]
public class BeneficiaryView : Beneficiary
{

    #region Constructeurs
    public BeneficiaryView()
    {
        VisibilityImageStatut1 = "style='display: none'";
        VisibilityImageStatut2 = "style='display: none'";

    }
    #endregion Constructeurs

    #region Property
    /// <summary>
    /// Date de confirmation 
    /// </summary>
    public string ConfirmeLe
    {
        get
        {
            if (this.CheckedDate != DateTime.MinValue)
                return CheckedDate.ToString("dd/MM/yyyy HH:mm:ss");
            return string.Empty;
        }
    }

    /// <summary>
    /// Url de l'image du statut 1 
    /// </summary>
    public string UrlImageStatut1 { get; set; }

    /// <summary>
    /// Url de l'image du statut 2
    /// </summary>
    public string UrlImageStatut2 { get; set; }


    /// <summary>
    /// Visibilité de l'image du statut 1 
    /// </summary>
    public string VisibilityImageStatut1 { get; set; }

    /// <summary>
    /// Visibilité de l'image du statut 1 
    /// </summary>
    public string VisibilityImageStatut2 { get; set; }
    

    /// <summary>
    /// Retourne si le beneficiaire est valide
    /// </summary>
    public bool IsBeneficiaryValidate
    {
        get
        {
            return (Statut == BeneficiaryStatutEnum.Ok);
        }
    }

    /// <summary>
    /// Description du statut
    /// - validés (ok)
    /// - validés puis supprimés
    /// - ajoutés mais echec
    /// </summary>
    public string StatutDescription
    {
        get
        {
            string statutDescription = string.Empty;
            switch (Statut)
            {
                case BeneficiaryStatutEnum.Ok:
                    statutDescription = string.Format(@"Validé le {0}", this.CheckedDate.ToString("dd/MM/yyyy"));
                    break;
                case BeneficiaryStatutEnum.OkThenDelete:
                    statutDescription = string.Format(@"Validé le {0} puis supprimé le {1}", this.CheckedDate.ToString("dd/MM/yyyy"), this.DeletedDate.ToString("dd/MM/yyyy"));
                    break;
                case BeneficiaryStatutEnum.Error:
                    if (this.CheckedDate != DateTime.MinValue)
                        statutDescription = string.Format(@"Echec de l'ajout le {0}", this.CheckedDate.ToString("dd/MM/yyyy"));
                    else
                        statutDescription = "Echec de l'ajout";
                    break;

                case BeneficiaryStatutEnum.ErrorThenDelete:
                    if (this.CheckedDate != DateTime.MinValue)
                        statutDescription = string.Format(@"Echec de l'ajout le {0} puis supprimé le {1}", this.CheckedDate.ToString("dd/MM/yyyy"), this.DeletedDate.ToString("dd/MM/yyyy"));
                    else
                        statutDescription = string.Format(@"Echec de l'ajout puis supprimé le {0}", this.DeletedDate.ToString("dd/MM/yyyy"));
                    break;
                default:
                    return string.Empty;
            }
            return statutDescription.Replace("'", "&apos;");
        }
    }
    #endregion Property
}