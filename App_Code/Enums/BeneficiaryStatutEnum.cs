﻿/// <summary>
/// Description résumée de BeneficiaryStatutEnum
/// </summary>
public enum BeneficiaryStatutEnum
{
    Ok = 1,
    Error= 2,
    Delete = 4,
    OkThenDelete = Ok + Delete,
    ErrorThenDelete = Error + Delete
}
