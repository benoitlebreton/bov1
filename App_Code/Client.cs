﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Xml;
using System.Xml.Linq;
using XMLMethodLibrary;

/// <summary>
/// Description résumée de Client
/// </summary>

[Serializable]
public class Client
{
    public static DataTable GetClientList(string sBarcode, string sLastName, string sFirstName, string sAddressStatus, string sRegistrationCode, string sToken)
    {
        DataTable dt = new DataTable();

        string sXml = new XElement("ALL_XML_IN", new XElement("C",
                        new XAttribute("Barcode", sBarcode),
                        new XAttribute("LastName", sLastName),
                        new XAttribute("FirstName", sFirstName),
                        new XAttribute("AddressCheckStatus", sAddressStatus),
                        new XAttribute("RegistrationCode", sRegistrationCode),
                        new XAttribute("TOKEN", sToken))).ToString();

        dt = getClientListFromXML(sXml);


        return dt;
    }
    public static DataTable GetClientList(string sBarcode, string sLastName, string sFirstName, string sAddressStatus,string sRegistrationCode, string sUnknownText, string sToken)
    {
        DataTable dt = new DataTable();

        string sXml = new XElement("ALL_XML_IN", new XElement("C",
                        new XAttribute("UnknownText", sUnknownText),
                        new XAttribute("Barcode", sBarcode),
                        new XAttribute("LastName", sLastName),
                        new XAttribute("FirstName", sFirstName),
                        new XAttribute("AddressCheckStatus", sAddressStatus),
                        new XAttribute("RegistrationCode", sRegistrationCode),
                        new XAttribute("TOKEN", sToken))).ToString();

        dt = getClientListFromXML(sXml);
        return dt;
    }
    public static DataTable GetClientList(string sAccountNumber, string sToken)
    {
        DataTable dt = new DataTable();

        string sXml = new XElement("ALL_XML_IN", new XElement("C",
                        new XAttribute("AccountNumber", sAccountNumber),
                        new XAttribute("TOKEN", sToken))).ToString();

        dt = getClientListFromXML(sXml);


        return dt;
    }
    public static DataTable GetClientList(bool bWatched, string sToken)
    {
        string sXml = new XElement("ALL_XML_IN", new XElement("C",
                        new XAttribute("Watched", (bWatched) ? "1" : "0"),
                        new XAttribute("TOKEN", sToken))).ToString();

        return getClientListFromXML(sXml);
    }

    protected static DataTable getClientListFromXML(string sXml)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("RefCustomer");
        dt.Columns.Add("LastName");
        dt.Columns.Add("FirstName");
        dt.Columns.Add("BirthDate");
        dt.Columns.Add("AccountNumber");
        dt.Columns.Add("AccountKind");

        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_SearchCustomer", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@ReturnSelect", SqlDbType.Bit);

            cmd.Parameters["@IN"].Value = sXml;

            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
            cmd.Parameters["@ReturnSelect"].Value = 0;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if (sXmlOut.Length > 0)
            {
                List<string> listCustomer = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/C");

                for (int i = 0; i < listCustomer.Count; i++)
                {
                    List<string> listRef = CommonMethod.GetAttributeValues(listCustomer[i], "C", "RefCustomer");
                    List<string> listLastName = CommonMethod.GetAttributeValues(listCustomer[i], "C", "LastName");
                    List<string> listFirstName = CommonMethod.GetAttributeValues(listCustomer[i], "C", "FirstName");
                    List<string> listBirthDate = CommonMethod.GetAttributeValues(listCustomer[i], "C", "BirthDate");
                    List<string> listAccountNumber = CommonMethod.GetAttributeValues(listCustomer[i], "C", "AccountNumber");
                    List<string> listAccountKind = CommonMethod.GetAttributeValues(listCustomer[i], "C", "AccountKind");

                    DataRow row = dt.NewRow();
                    row["RefCustomer"] = (listRef.Count > 0) ? listRef[0] : "";
                    row["LastName"] = (listLastName.Count > 0) ? listLastName[0] : "";
                    row["FirstName"] = (listFirstName.Count > 0) ? listFirstName[0] : "";
                    row["BirthDate"] = (listBirthDate.Count > 0) ? listBirthDate[0] : "";
                    row["AccountNumber"] = (listAccountNumber.Count > 0) ? listAccountNumber[0] : "";
                    row["AccountKind"] = (listAccountKind.Count > 0) ? listAccountKind[0] : "";

                    dt.Rows.Add(row);
                }
            }
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }

    public static DataTable getIdenticalAddressList(string sRegistrationCode)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("RefCustomer");
        dt.Columns.Add("RegistrationCode");
        dt.Columns.Add("CreationDate");
        dt.Columns.Add("FirstName");
        dt.Columns.Add("LastName");
        dt.Columns.Add("AMLProfile");

        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("BankSI.P_GetIdenticalAddressList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@ReturnSelect", SqlDbType.Bit);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                            new XElement("Customer",
                                                new XAttribute("RegistrationCode", sRegistrationCode),
                                                new XAttribute("RefCustomer",""))).ToString();

            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
            cmd.Parameters["@ReturnSelect"].Value = 0;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if (sXmlOut.Length > 0)
            {
                List<string> listCustomer = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/Customer");

                for (int i = 0; i < listCustomer.Count; i++)
                {
                    List<string> listRef = CommonMethod.GetAttributeValues(listCustomer[i], "Customer", "RefCustomer");
                    List<string> listRegistrationCode = CommonMethod.GetAttributeValues(listCustomer[i], "Customer", "RegistrationCode");
                    List<string> listCreationDate = CommonMethod.GetAttributeValues(listCustomer[i], "Customer", "CreationDate");
                    List<string> listFirstName = CommonMethod.GetAttributeValues(listCustomer[i], "Customer", "FirstName");
                    List<string> listLastName = CommonMethod.GetAttributeValues(listCustomer[i], "Customer", "LastName");
                    List<string> listAMLProfile = CommonMethod.GetAttributeValues(listCustomer[i], "Customer", "AMLProfile");

                    DataRow row = dt.NewRow();
                    row["RefCustomer"] = (listRef.Count > 0) ? listRef[0] : "";
                    row["RegistrationCode"] = (listRegistrationCode.Count > 0) ? listRegistrationCode[0] : "";
                    row["CreationDate"] = (listCreationDate.Count > 0) ? listCreationDate[0] : "";
                    row["FirstName"] = (listFirstName.Count > 0) ? listFirstName[0] : "";
                    row["LastName"] = (listLastName.Count > 0) ? listLastName[0] : "";
                    row["AMLProfile"] = (listAMLProfile.Count > 0) ? listAMLProfile[0] : "";

                    dt.Rows.Add(row);
                }
            }
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }

    public static DataTable GetCustomerTransferDebitRefusal(bool bFirstRefusal, bool bFifthRefusal)
    {
        DataTable dt = new DataTable();
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("WEB.P_GetCustomerTransferDebitRefusal", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);

            XElement xmlTransfer = new XElement("TransferDebit");
            if (bFirstRefusal)
                xmlTransfer.Add(new XAttribute("NotificationFirstRefusal", "1"));
            if (bFifthRefusal)
                xmlTransfer.Add(new XAttribute("Notification5Refusals", "1"));

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN", xmlTransfer).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }

    public static string GetCustomerInformations(int iRefCustomer)
    {
        SqlConnection conn = null;
        string sXmlOut = "";
        string sToken = authentication.GetCurrent(false).sToken;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_GetCustomerInformation", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@ReturnSelect", SqlDbType.Bit);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN", new XElement("Customer", new XAttribute("RefCustomer", iRefCustomer), new XAttribute("TOKEN", sToken))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
            cmd.Parameters["@ReturnSelect"].Value = 0;

            cmd.ExecuteNonQuery();

            sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            //<ALL_XML_OUT><Customer RC="77001" RetMessage="L'accès au compte est restreint. Veuillez contacter la DCRCI"/></ALL_XML_OUT>

            List<string> lsRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Customer", "RC");
            List<string> lsRetMessage = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Customer", "RetMessage");
            string sRC = (lsRC.Count > 0 && !string.IsNullOrWhiteSpace(lsRC[0])) ? lsRC[0].ToString().Trim() : "";
            string sMessage = (lsRetMessage.Count > 0 && !string.IsNullOrWhiteSpace(lsRetMessage[0])) ? lsRetMessage[0].ToString().Trim() : "";
            string sCurrentPage = "ClientSearch.aspx";//HttpContext.Current.Request.RawUrl.Replace("/","");

            if(sRC == "77001")
            {
                tools.CheckUnauthorizedAccess(sRC, sMessage, sCurrentPage);
            }

        }
        catch (Exception ex)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return sXmlOut;
    }

    public static string GetKYCChoices()
    {
        SqlConnection conn = null;
        string sXmlOut = "";

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_GetCustomersKYCLists", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@ReturnSelect", SqlDbType.Bit);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN").ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
            cmd.Parameters["@ReturnSelect"].Value = 0;

            cmd.ExecuteNonQuery();

            sXmlOut = cmd.Parameters["@OUT"].Value.ToString();
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return sXmlOut;
    }

    public static DataTable GetBankStatementList(int iRefCustomer, int iInitialRefCustomer)
    {
        SqlConnection conn = null;
        DataTable dt = new DataTable();
        dt.Columns.Add("Date");
        dt.Columns.Add("File");

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK2"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_GetCustomerReleves", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN", new XElement("Releve", new XAttribute("RefCustomer", iRefCustomer), new XAttribute("InitialRefCustomer", iInitialRefCustomer))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            List<string> listReleves = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/Releve");
            List<string> listFraisAnnuel = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/FraisAnnuel");
            List<string> listCombined = new List<string>();
            listCombined.AddRange(listReleves);
            listCombined.AddRange(listFraisAnnuel);

            for (int i = 0; i < listCombined.Count; i++)
            {
                List<string> listDate = CommonMethod.GetAttributeValues(listCombined[i], "Releve", "YYYYMM");
                List<string> listYear = CommonMethod.GetAttributeValues(listCombined[i], "FraisAnnuel", "YYYY");
                List<string> listFile = CommonMethod.GetAttributeValues(listCombined[i], "Releve", "Filename");
                if (listFile.Count == 0)
                    listFile = CommonMethod.GetAttributeValues(listCombined[i], "FraisAnnuel", "Filename");

                if ((listDate.Count > 0 || listYear.Count > 0) && listFile.Count > 0)
                {
                    DataRow row = dt.NewRow();
                    row["Date"] = (listDate.Count > 0) ? listDate[0] : listYear[0] + "99";
                    string sPath = ConfigurationManager.AppSettings["NickelDocPath"].ToString();
                    string sTmp = HostingEnvironment.MapPath("~" + sPath + listFile[0].Replace("\\", "/"));
                    //Uri uriCurrentUrl = new Uri(Request.Url.AbsoluteUri);
                    //if (File.Exists(HostingEnvironment.MapPath("~" + sPath + listFile[0].Replace("\\", "/"))))
                    row["File"] = "." + sPath + listFile[0].Replace("\\", "/");
                    //row["File"] = uriCurrentUrl.Scheme + Uri.SchemeDelimiter + uriCurrentUrl.Host + ":" + uriCurrentUrl.Port + sPath + listFile[0].Replace("\\", "/");
                    //else row["File"] = "";
                    dt.Rows.Add(row);
                }
            }
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return SortBankStatementList(dt);
    }
    protected static DataTable SortBankStatementList(DataTable dt)
    {
        DataView dv = new DataView(dt);
        dv.Sort = "Date DESC";
        return dv.ToTable();
    }
    public static DataTable FormatDateBankStatementList(DataTable dt)
    {
        DataTable dt2 = dt.Clone();

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (!dt.Rows[i][0].ToString().EndsWith("99"))
            {
                DateTime date = DateTime.ParseExact(dt.Rows[i][0].ToString(), "yyyyMM", CultureInfo.InvariantCulture);
                dt.Rows[i][0] = date.ToString("MMMM yyyy");
            }
            else
            {
                dt.Rows[i][0] = "Relevé des frais " + dt.Rows[i][0].ToString().Replace("99", "");
            }

            // Limit list to the 15 first results
            if (i < 15)
                dt2.ImportRow(dt.Rows[i]);
        }

        return dt2;
    }

    public static bool SaveInformations(string sXmlIN)
    {
        bool bSaved = false;
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_SetCustomer", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);

            cmd.Parameters["@IN"].Value = sXmlIN;
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOUT = cmd.Parameters["@OUT"].Value.ToString();

            if (sXmlOUT.Length > 0)
            {
                List<string> listRC = CommonMethod.GetAttributeValues(sXmlOUT, "ALL_XML_OUT/Result", "RC");
                List<string> listUpdated = CommonMethod.GetAttributeValues(sXmlOUT, "ALL_XML_OUT/Result", "Updated");

                if (listRC.Count > 0)
                {
                    if (listRC[0] == "0")
                    {
                        if (listUpdated.Count > 0 && listUpdated[0] == "1")
                            bSaved = true;
                    }
                    //else if (listRC[0] == "71008")
                    //{
                        //UpdateMessage("Session terminée. Veuillez vous reconnecter.");
                    //}
                    //else UpdateMessage("Une erreur est survenue.");
                }
            }
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return bSaved;
    }

    public static bool ReopenClientAccount(int iRefCustomer, string sCashierToken, out string sMessage)
    {
        /*
            DECLARE 	 @IN VARCHAR(MAX),@OUT VARCHAR(MAX),@RC INT
			
            SET @IN = '<ALL_XML_IN>
              <Customer RefCustomer="497225" 
	            CashierToken="0x0011EA463FD7F9FE8B299228AF7F7DA56E655DC8"  />
            </ALL_XML_IN>'			
										
            EXEC @RC = [BankSI].[P_ReopenCustomer]
										 @IN 
										 ,@OUT  OUTPUT
				
            SELECT @RC,CAST(@OUT AS XML)
         */

        bool bIsOK = false;
        sMessage = "Une erreur est survenue.";
        SqlConnection conn = null;

        if (iRefCustomer > 0 && !string.IsNullOrWhiteSpace(sCashierToken))
        {

            try
            {
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
                conn.Open();

                SqlCommand cmd = new SqlCommand("[BankSI].[P_ReopenCustomer]", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@IN", SqlDbType.VarChar, -1);
                cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);
                cmd.Parameters.Add("@RC", SqlDbType.Int);

                cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;
                cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                                new XElement("Customer",
                                                    new XAttribute("RefCustomer", iRefCustomer.ToString()),
                                                    new XAttribute("CashierToken", sCashierToken))).ToString();
                cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                string sXmlOUT = cmd.Parameters["@OUT"].Value.ToString();

                if (sXmlOUT.Length > 0) {

                    List<string> listRC = CommonMethod.GetAttributeValues(sXmlOUT, "ALL_XML_OUT/Result", "RC");
                    List<string> listMessage = CommonMethod.GetTagValues(sXmlOUT, "ALL_XML_OUT/Message");

                    XmlDocument xmlout = new XmlDocument();
                    xmlout.LoadXml(sXmlOUT);

                    try
                    {
                        XmlCDataSection cDataNode = (XmlCDataSection)(xmlout.SelectSingleNode("ALL_XML_OUT/Result/Message").ChildNodes[0]);
                        sMessage = cDataNode.Data;
                    }
                    catch(Exception ex) { sMessage = "Une erreur est survenue."; }

                    if (cmd.Parameters["@RC"].Value.ToString().Trim() == "0")
                    {
                        if (listRC.Count > 0)
                        {
                            if (listRC[0] == "0") { bIsOK = true; }
                        }
                    }
                }
            }
            catch (Exception e)
            {
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        return bIsOK;
    }
}