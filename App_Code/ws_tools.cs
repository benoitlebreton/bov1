﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Hosting;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.SessionState;
using System.Xml.Linq;
using XMLMethodLibrary;
using Newtonsoft.Json;
using System.Net;

/// <summary>
/// Description résumée de ws_tools
/// </summary>
/// 
[Serializable]
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// Pour autoriser l'appel de ce service Web depuis un script à l'aide d'ASP.NET AJAX, supprimez les marques de commentaire de la ligne suivante. 
[System.Web.Script.Services.ScriptService]
public class ws_tools : System.Web.Services.WebService
{

    [WebMethod]
    public List<string> GetCityList(string prefix, string dep)
    {
        string culture = "fr-FR", order = "ASC";
        DataTable dt = tools.getCityList(dep, prefix, order, culture);

        int max = 10;
        if (dt.Rows.Count < max)
            max = dt.Rows.Count;

        List<string> cityList = new List<string>();
        for (int i = 0; i < max; i++)
        {
            cityList.Add(dt.Rows[i][2].ToString());
        }

        //cityList.Reverse();

        return cityList.ToList();
    }

    [WebMethod]
    public List<string> GetDepartmentList(string prefix)
    {
        string culture = "fr-FR", order = "ASC";

        DataTable dt = tools.getDepList(prefix, order, culture);

        int max = 5;
        if (dt.Rows.Count < max)
            max = dt.Rows.Count;

        List<string> departmentList = new List<string>();
        for (int i = 0; i < max; i++)
        {
            departmentList.Add(dt.Rows[i]["DepartementFull2"].ToString());
        }

        departmentList.Reverse();

        return departmentList.ToList();
    }

    [Serializable]
    private class NbNonEvaluateSubscription
    {
        public int GlobalCounter { get; set; }
        public int N1Counter { get; set; }
        public int N2Counter { get; set; }
        public int OnfidoCounter { get; set; }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string getNbNonEvaluateSubscription()
    {
        string json = "";

        SqlConnection conn = null;
        NbNonEvaluateSubscription _NbNonEvaluateSubscription = new NbNonEvaluateSubscription();
        
        try
        {
            authentication auth = authentication.GetCurrent();
            if (authentication.CheckForCounterRefresh("NbNonEvaluateSubscription", auth))
            {
                registration.GlobalCheckInfo _globalCheckInfo = registration.GetGlobalCheckInfo(authentication.GetCurrent(false).sToken);
                _NbNonEvaluateSubscription.N2Counter = _globalCheckInfo._L1_CHECKS_KO_L2_CHECKS_IN_PROGRESS_ + _globalCheckInfo._L1_CHECKS_KO_L2_CHECKS_TO_START_ + _globalCheckInfo._L1_NO_CHECK_L2_CHECKS_IN_PROGRESS_;
                _NbNonEvaluateSubscription.N1Counter = _globalCheckInfo._ALL_NOT_CHECKED_ - (_globalCheckInfo._L1_NO_CHECK_L2_CHECKS_IN_PROGRESS_+ _globalCheckInfo._L1_CHECKS_KO_L2_CHECKS_IN_PROGRESS_ + _globalCheckInfo._L1_CHECKS_KO_L2_CHECKS_TO_START_ + _globalCheckInfo._L1_NO_CHECK_L2_CHECKS_IN_PROGRESS_);
                _NbNonEvaluateSubscription.GlobalCounter = _globalCheckInfo._ALL_NOT_CHECKED_;
                _NbNonEvaluateSubscription.OnfidoCounter = _globalCheckInfo._ONFIDO_CHECKS_IN_PROGRESS_;

                auth.clrCountersLastRefresh.iPendingRegistrationToCheckN2 = _NbNonEvaluateSubscription.N2Counter;
                auth.clrCountersLastRefresh.iPendingRegistrationToCheckN1 = _NbNonEvaluateSubscription.N1Counter;
                auth.clrCountersLastRefresh.iNbNonEvaluateSubscription = _NbNonEvaluateSubscription.GlobalCounter;
                auth.clrCountersLastRefresh.iInProgressOnfido = _NbNonEvaluateSubscription.OnfidoCounter;
                auth.clrCountersLastRefresh.dtLastNbNonEvaluateSubscription = DateTime.Now;
            }
            else
            {
                _NbNonEvaluateSubscription.N2Counter = auth.clrCountersLastRefresh.iPendingRegistrationToCheckN2;
                _NbNonEvaluateSubscription.N1Counter = auth.clrCountersLastRefresh.iPendingRegistrationToCheckN1;
                _NbNonEvaluateSubscription.GlobalCounter = auth.clrCountersLastRefresh.iNbNonEvaluateSubscription;
                _NbNonEvaluateSubscription.OnfidoCounter = auth.clrCountersLastRefresh.iInProgressOnfido;
            }

            json = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(_NbNonEvaluateSubscription);
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return json;
    }
    //public int getNbNonEvaluateSubscription()
    //{
    //    int iNbNonEvaluateSubscription = 0;

    //    SqlConnection conn = null;

    //    string sXML = new XElement("ALL_XML_IN",
    //                    new XElement("Stats",
    //                        new XAttribute("Kind", "UNCHECKED")
    //                    )
    //                ).ToString();

    //    try
    //    {
    //        authentication auth = authentication.GetCurrent();
    //        if (authentication.CheckForCounterRefresh("NbNonEvaluateSubscription", auth))
    //        {

    //            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
    //            conn.Open();

    //            SqlCommand cmd = new SqlCommand("WEB.P_Statistics", conn);
    //            cmd.CommandType = CommandType.StoredProcedure;
    //            cmd.Parameters.Add("@IN", SqlDbType.VarChar, -1);
    //            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);
    //            cmd.Parameters.Add("@ReturnSelect", SqlDbType.Int);
    //            cmd.Parameters["@IN"].Value = sXML;
    //            cmd.Parameters["@ReturnSelect"].Value = false;
    //            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
    //            cmd.ExecuteNonQuery();

    //            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();
    //            string sRowCount = tools.GetValueFromXml(sXmlOut, "ALL_XML_OUT/Stats", "NbUncheckedCustomers");

    //            if (sRowCount.Trim().Length > 0)
    //            {
    //                iNbNonEvaluateSubscription = int.Parse(sRowCount);
    //                auth.clrCountersLastRefresh.dtLastNbNonEvaluateSubscription = DateTime.Now;
    //                auth.clrCountersLastRefresh.iNbNonEvaluateSubscription = iNbNonEvaluateSubscription;
    //            }
    //        }
    //        else
    //        {
    //            iNbNonEvaluateSubscription = auth.clrCountersLastRefresh.iNbNonEvaluateSubscription;
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //    }

    //    return iNbNonEvaluateSubscription;
    //}

    [WebMethod]
    public string getAmlAlertDetails(string sRefAlert)
    {
        string sAlertDetails = "";
        string sAlert = AML.getAlertDetails(sRefAlert);

        List<string> lDetails = CommonMethod.GetTags(sAlert, "ALERTE/DETAIL/table");

        if (lDetails.Count > 0)
            sAlertDetails = lDetails[0];


        return sAlertDetails;
    }

    [WebMethod]
    public int getPendingTransferCount()
    {
        int iPendingTransferCount = 0;
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.F_GetNbPendingTransfers", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@NbPendingTransfer", SqlDbType.Int);
            cmd.Parameters["@NbPendingTransfer"].Direction = ParameterDirection.ReturnValue;
            cmd.ExecuteNonQuery();

            iPendingTransferCount = int.Parse(cmd.Parameters["@NbPendingTransfer"].Value.ToString());
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }


        return iPendingTransferCount;
    }

    [WebMethod]
    public int getPendingTransferReservedCount()
    {
        int iPendingTransferCount = 0;
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.F_GetNbPendingTransfersReserved", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@NbPendingTransfer", SqlDbType.Int);
            cmd.Parameters["@NbPendingTransfer"].Direction = ParameterDirection.ReturnValue;
            cmd.ExecuteNonQuery();

            iPendingTransferCount = int.Parse(cmd.Parameters["@NbPendingTransfer"].Value.ToString());
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }


        return iPendingTransferCount;
    }

    [WebMethod]
    public int getGarnishmentPendingTransferCount()
    {
        int iPendingTransferCount = 0;
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.F_GetNbPendingGarnishmentTransfer", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@NbPendingTransfer", SqlDbType.Int);
            cmd.Parameters["@NbPendingTransfer"].Direction = ParameterDirection.ReturnValue;
            cmd.ExecuteNonQuery();

            iPendingTransferCount = int.Parse(cmd.Parameters["@NbPendingTransfer"].Value.ToString());
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }


        return iPendingTransferCount;
    }

    [WebMethod]
    public void GetClientDocForDownload(string sFileName, string sRefCustomer)
    {
        bool isOK = false;

        SqlConnection conn = null;
        try
        {
            if (sFileName.Trim().Length > 0)
            {
                isOK = tools.GetClientDocForDownload(sRefCustomer, sRefCustomer, sFileName);
            }
        }
        catch (Exception ex)
        {
            isOK = false;
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        //return isOK;
        Context.Response.Write(isOK);
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string GetClientOpeningFile(string sRegistrationCode, string sFileName)
    {
        CheckIdentity();
        string sPathFile = "";
        try
        {
            authentication auth = authentication.GetCurrent(false);
            int iRefCustomer = tools.GetRefCustomerFromRegistrationCode(sRegistrationCode);
            if (auth != null && sFileName.Trim().Length > 0 && sRegistrationCode.Trim().Length > 0 && iRefCustomer > 0)
            {
                string sDocFilePath = ConfigurationManager.AppSettings["NickelDocPath"].ToString();
                string sWebDirPath = tools.GetCustomerWebDirectoryName(sRegistrationCode) + "/";

                if (File.Exists(HostingEnvironment.MapPath("~" + sDocFilePath + sWebDirPath + "Dossier_ouverture_" + sFileName + ".pdf")))
                    sPathFile = "." + sDocFilePath + sWebDirPath + "Dossier_ouverture_" + sFileName + ".pdf";
                else if (File.Exists(HostingEnvironment.MapPath("~" + sDocFilePath + sWebDirPath + sFileName + ".pdf")))
                    sPathFile = "." + sDocFilePath + sWebDirPath + sFileName + ".pdf";
                else if (tools.GetClientOpeningFile(iRefCustomer.ToString()))
                {
                    Thread.Sleep(2000);
                    if (File.Exists(HostingEnvironment.MapPath("~" + sDocFilePath + sWebDirPath + "Dossier_ouverture_" + sFileName + ".pdf")))
                        sPathFile = "." + sDocFilePath + sWebDirPath + "Dossier_ouverture_" + sFileName + ".pdf";
                    else if (File.Exists(HostingEnvironment.MapPath("~" + sDocFilePath + sWebDirPath + sFileName + ".pdf")))
                        sPathFile = "." + sDocFilePath + sWebDirPath + sFileName + ".pdf";

                    if (sPathFile.Trim() == "")
                    {
                        sPathFile = "." + sDocFilePath + sWebDirPath + "Dossier_ouverture_" + sFileName + ".pdf";
                        sPathFile += ";." + sDocFilePath + sWebDirPath + "Dossier_ouverture_" + sFileName + ".pdf";
                    }
                }

            }
        }
        catch (Exception ex)
        {
            sPathFile = "";
        }

        return sPathFile;
    }

    private void CheckIdentity()
    {
        if (HttpContext.Current.Session == null)
            throw new ApplicationException("Access denied");
        else
        {
            HttpSessionState SessionList = HttpContext.Current.Session;

            if (Session["Authentication"] == null)
                throw new InvalidOperationException("Authentication required");
        }
    }

    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string GetClientOperationsStats(string sRegistrationCode)
    {
        CheckIdentity();
        string json = "";
        try
        {
            authentication auth = authentication.GetCurrent(false);
            int iRefCustomer = tools.GetRefCustomerFromRegistrationCode(sRegistrationCode);
            if (auth != null && sRegistrationCode.Trim().Length > 0 && iRefCustomer > 0)
            {
                List<ClientOperationsStatsResult> resultRows = new List<ClientOperationsStatsResult>();

                resultRows.Add(new ClientOperationsStatsResult { value = "20", category = "DAB" });
                resultRows.Add(new ClientOperationsStatsResult { value = "40", category = "VIREMENTS" });
                resultRows.Add(new ClientOperationsStatsResult { value = "40", category = "HA" });

                json = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(new { ClientOperationsStatsResult = resultRows });
            }
        }
        catch (Exception ex)
        {
            json = "";
        }
        finally
        {

        }

        return json;
    }
    [Serializable]
    private class ClientOperationsStatsResult
    {
        public string value { get; set; }
        public string category { get; set; }
    }

    [WebMethod]
    public List<string> GetBicList(string prefix)
    {
        List<string> bicList = new List<string>();
        if (prefix.Trim().Length > 0)
        {

            string culture = "fr-FR", order = "ASC";

            DataTable dt = new DataTable();

            /*
             * DECLARE @BICRequest VARCHAR(max)
                DECLARE @BICList VARCHAR(max)
    
                SET @BICRequest = '<BIC Culture="fr-FR" Prefix="F" Order="ASC" />'
        
                EXEC [Other].[P_GetBICList] @BICRequest
             */
            SqlConnection conn = null;

            try
            {
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NICKEL"].ConnectionString);
                conn.Open();

                SqlCommand cmd = new SqlCommand("NOBANK.Other.P_GetBICList", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@BICRequest", SqlDbType.VarChar, -1);
                //cmd.Parameters.Add("@BICList", SqlDbType.VarChar, -1);
                //cmd.Parameters.Add("@RC", SqlDbType.Int);

                cmd.Parameters["@BICRequest"].Value = new XElement("BIC",
                                                        new XAttribute("Culture", culture),
                                                        new XAttribute("Prefix", prefix),
                                                        new XAttribute("Order", order)).ToString();
                //cmd.Parameters["@BICList"].Direction = ParameterDirection.Output;
                //cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;

                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                da.Fill(dt);

                //string sXmlOut = cmd.Parameters["@BICList"].Value.ToString();

            }
            catch (Exception ex)
            {
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }

            int max = 4;
            if (dt.Rows.Count < max)
                max = dt.Rows.Count;

            for (int i = 0; i < max; i++)
            {
                bicList.Add(dt.Rows[i][0].ToString() + "|" + dt.Rows[i][1].ToString() + "|" + dt.Rows[i][2].ToString() + "|" + dt.Rows[i][4].ToString());
            }
        }
        else
        {
            string sTest = prefix;
        }

        //bicList.Reverse();

        return bicList.ToList();
    }
    [WebMethod]
    public string GetBicFromIban(string iban)
    {
        string sBic = "";
        if (iban.Trim().Length > 0)
        {
            SqlConnection conn = null;

            try
            {
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
                conn.Open();

                SqlCommand cmd = new SqlCommand("Other.F_GetBicFromIban", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@IBAN", SqlDbType.VarChar, 32);
                cmd.Parameters.Add("@BIC", SqlDbType.VarChar, 11);

                cmd.Parameters["@IBAN"].Value = iban.Replace(" ", "");
                cmd.Parameters["@BIC"].Direction = ParameterDirection.ReturnValue;

                cmd.ExecuteNonQuery();

                sBic = cmd.Parameters["@BIC"].Value.ToString();
            }
            catch (Exception ex)
            {
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        return sBic;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string GarnishmentPendingModifyIban(string sRefCustomer, string sRefPendingTransfer, string sIban)
    {
        bool bOk = false;
        authentication auth = authentication.GetCurrent(false);
        string sErrorMessage = "";

        if (auth != null)
        {
            if (tools.IsIbanChecksumValid(sIban.Trim()))
            {

                sErrorMessage = "";
                SqlConnection conn = null;

                try
                {
                    conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
                    conn.Open();

                    SqlCommand cmd = new SqlCommand("web.P_PendingTransferAction", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
                    cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);

                    XElement xelem = new XElement("Transfer",
                                                        new XAttribute("RefPendingTransfer", sRefPendingTransfer),
                                                        new XAttribute("CashierToken", auth.sToken),
                                                        new XAttribute("Action", "SET"),
                                                        new XAttribute("IBAN", sIban.Trim()));
                    if (!String.IsNullOrWhiteSpace(sRefCustomer))
                        xelem.Add(new XAttribute("RefCustomer", sRefCustomer));

                    cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                                        xelem
                                                    ).ToString();
                    cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
                    cmd.ExecuteNonQuery();

                    string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

                    if (sXmlOut.Length > 0)
                    {
                        List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Transfer", "RC");
                        List<string> listErrorLabel = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Transfer", "ErrorLabel");
                        List<string> listSabErrorLabel = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Transfer", "SabErrorLabel");

                        if (listRC.Count > 0 && listRC[0] == "0")
                            bOk = true;
                        else
                        {
                            if (listErrorLabel.Count > 0 && listErrorLabel[0].Length > 0)
                            {
                                StringBuilder sb = new StringBuilder();
                                sb.AppendLine(listErrorLabel[0]);

                                if (listSabErrorLabel.Count > 0 && listSabErrorLabel[0].Length > 0)
                                    sb.AppendLine("Erreur SAB : " + listSabErrorLabel[0]);

                                sErrorMessage = sb.ToString().Trim();
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                }
                finally
                {
                    if (conn != null)
                        conn.Close();
                }
            }
        }

        return bOk.ToString();
    }
    [Serializable]
    class JsonAmlAlert
    {
        public bool manager;
        public bool bCreationRight;
        public bool bValidationRight;
        public bool bApprovalRight;
        public int iNbToValidate;
        public int iNbToApprove;
        public int iNbPastTwentyDays;
        public List<ManagedAlert> alerts;
    }
    [Serializable]
    class ManagedAlert
    {
        public bool bIsUserAlert { get; set; }
        public int iRefUser { get; set; }
        public string sUserName { get; set; }
        public int iNbAlert { get; set; }
        public int iNbNotification { get; set; }
        public int iNbStatementApproved { get; set; }
        public int iNbStatementPastTwentyDays { get; set; }
        public ManagedAlert(bool bIsUserAlert, int iRefUser, string sUserName, int iNbAlert, int iNbNotification)
        {
            this.bIsUserAlert = bIsUserAlert;
            this.iRefUser = iRefUser;
            this.sUserName = sUserName;
            this.iNbAlert = iNbAlert;
            this.iNbNotification = iNbNotification;
        }
        public ManagedAlert(bool bIsUserAlert, int iRefUser, string sUserName, int iNbAlert, int iNbNotification, int iNbStatementApproved, int iNbStatementPastTwentyDays)
        {
            this.bIsUserAlert = bIsUserAlert;
            this.iRefUser = iRefUser;
            this.sUserName = sUserName;
            this.iNbAlert = iNbAlert;
            this.iNbNotification = iNbNotification;
            this.iNbStatementApproved = iNbStatementApproved;
            this.iNbStatementPastTwentyDays = iNbStatementPastTwentyDays;
        }
    }
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string getNbAlertAML()
    {
        authentication auth = authentication.GetCurrent();
        string JsonOut = "";
        bool bCounterAgent = false;
        //bool bCounterAll = false;

        if (Session["AlertCounterAgent"] == null || Session["AlertCounterAll"] == null)
        {
            string sXmlRights = tools.GetRights(auth.sToken, "SC_AlertSearch");

            if (sXmlRights.Length > 0)
            {
                List<string> listAction = CommonMethod.GetTags(sXmlRights, "ALL_XML_OUT/Page/Action");

                for (int i = 0; i < listAction.Count; i++)
                {
                    List<string> listTag = CommonMethod.GetAttributeValues(listAction[i], "Action", "TagAction");
                    List<string> listActionAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ActionAllowed");
                    List<string> listViewAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ViewAllowed");

                    switch (listTag[0])
                    {
                        case "AlertCounter":
                            if (listViewAllowed[0] == "1" && listActionAllowed[0] == "1")
                                bCounterAgent = true;
                            break;
                        //case "AlertCounterAllAgent":
                        //    if (listViewAllowed[0] == "1" && listActionAllowed[0] == "1")
                        //        bCounterAll = true;
                        //    break;
                    }
                }
            }

            Session["AlertCounterAgent"] = bCounterAgent;
            Session["AlertCounterAll"] = true;
        }
        else
        {
            if (Session["AlertCounterAgent"] != null)
                bCounterAgent = bool.Parse(Session["AlertCounterAgent"].ToString());
            //if (Session["AlertCounterAll"] != null)
            //    bCounterAll = bool.Parse(Session["AlertCounterAll"].ToString());
        }

        //if (bCounterAgent || bCounterAll)
        if (bCounterAgent)
        {
            SqlConnection conn = null;

            try
            {
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
                conn.Open();

                SqlCommand cmd = new SqlCommand("AML.P_GetManagedAlertsInformation", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
                cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);

                cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                                new XElement("ManagedAlerts",
                                                    new XAttribute("CashierToken", auth.sToken)
                                                    //,new XAttribute("RefBoUser", (!bCounterAll) ? auth.sRef : "")
                                                    )).ToString();
                cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

                cmd.ExecuteNonQuery();

                string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

                if (sXmlOut.Length > 0)
                {
                    List<string> listManagedAlerts = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/ManagedAlerts");
                    if (listManagedAlerts.Count > 0)
                    {
                        List<ManagedAlert> list = new List<ManagedAlert>();
                        for (int i = 0; i < listManagedAlerts.Count; i++)
                        {
                            List<string> listRefBoUser = CommonMethod.GetAttributeValues(listManagedAlerts[i], "ManagedAlerts", "RefBOUser");
                            List<string> listBoUserName = CommonMethod.GetAttributeValues(listManagedAlerts[i], "ManagedAlerts", "BoUserName");
                            List<string> listNbAlert = CommonMethod.GetAttributeValues(listManagedAlerts[i], "ManagedAlerts", "NbAlertNotTreated");
                            List<string> listNbNotification = CommonMethod.GetAttributeValues(listManagedAlerts[i], "ManagedAlerts", "PendingNotification");

                            list.Add(new ManagedAlert((listRefBoUser[0] == auth.sRef) ? true : false, int.Parse(listRefBoUser[0]), listBoUserName[0], int.Parse(listNbAlert[0]), int.Parse(listNbNotification[0])));
                        }

                        StatementStatus stmtSts = GetStatementStatus(auth.sToken);

                        for (int i = 0; i < stmtSts.dtAgents.Rows.Count; i++)
                        {
                            bool bFound = false;
                            for (int j = 0; j < list.Count; j++)
                            {
                                if ((int)stmtSts.dtAgents.Rows[i]["RefBOUser"] == list[j].iRefUser)
                                {
                                    bFound = true;
                                    list[j].iNbStatementApproved = (int)stmtSts.dtAgents.Rows[i]["NbStatementApproved"];
                                    list[j].iNbStatementPastTwentyDays = (int)stmtSts.dtAgents.Rows[i]["NbStatementPastTwentyDays"];
                                    break;
                                }
                            }
                            if (!bFound)
                            {
                                list.Add(new ManagedAlert((stmtSts.dtAgents.Rows[i]["RefBOUser"].ToString() == auth.sRef) ? true : false, (int)stmtSts.dtAgents.Rows[i]["RefBOUser"], stmtSts.dtAgents.Rows[i]["BoUserName"].ToString(), 0, 0, (int)stmtSts.dtAgents.Rows[i]["NbStatementApproved"], (int)stmtSts.dtAgents.Rows[i]["NbStatementPastTwentyDays"]));
                            }
                        }

                        JsonAmlAlert json = new JsonAmlAlert();
                        //json.manager = bCounterAll;
                        json.alerts = list;
                        json.bCreationRight = stmtSts.bCreationRight;
                        json.bValidationRight = stmtSts.bValidationRight;
                        json.bApprovalRight = stmtSts.bApprovalRight;
                        json.iNbToValidate = stmtSts.iNbToValidate;
                        json.iNbToApprove = stmtSts.iNbToApprove;
                        json.iNbPastTwentyDays = stmtSts.iNbPastTwentyDays;
                        JsonOut = JsonConvert.SerializeObject(json);
                    }
                }
            }
            catch (Exception e)
            {

            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        return JsonOut;
    }

    [Serializable]
    protected class StatementStatus {
        public bool bCreationRight;
        public bool bValidationRight;
        public bool bApprovalRight;
        public int iNbToValidate;
        public int iNbToApprove;
        public int iNbPastTwentyDays;
        public DataTable dtAgents;
    }
    protected StatementStatus GetStatementStatus(string sToken)
    {
        if (Session["StatementStatus"] != null)
            return (StatementStatus)Session["StatementStatus"];

        StatementStatus stmtSts = new StatementStatus();
        DataTable dt = new DataTable();
        dt.Columns.Add("RefBOUser", typeof(int));
        dt.Columns.Add("BoUserName");
        dt.Columns.Add("NbStatementApproved", typeof(int));
        dt.Columns.Add("NbStatementPastTwentyDays", typeof(int));
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("TracFin.P_GetManagedStatementsInformation", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                            new XElement("ManagedStatements",
                                                new XAttribute("CashierToken", sToken)
                                            )
                                          ).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if(!String.IsNullOrWhiteSpace(sXmlOut))
            {
                List<string> listCreation = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/ManagedStatements", "RightCreation");
                List<string> listValidation = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/ManagedStatements", "RightValidation");
                List<string> listApproval = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/ManagedStatements", "RightApproval");
                List<string> listNbStatementsToValidate = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/ManagedStatements", "NbStatementsToValidate");
                List<string> listNbStatementToApprove = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/ManagedStatements", "NbStatementToApprove");
                List<string> listAgent = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/ManagedStatements/Agent");

                if (listCreation.Count > 0 && listCreation[0] == "1")
                    stmtSts.bCreationRight = true;
                if (listValidation.Count > 0 && listValidation[0] == "1")
                    stmtSts.bValidationRight = true;
                if (listApproval.Count > 0 && listApproval[0] == "1")
                    stmtSts.bApprovalRight = true;

                int iTmp = 0;
                if (listNbStatementsToValidate.Count > 0 && int.TryParse(listNbStatementsToValidate[0], out iTmp))
                    stmtSts.iNbToValidate = iTmp;
                iTmp = 0;
                if (listNbStatementToApprove.Count > 0 && int.TryParse(listNbStatementToApprove[0], out iTmp))
                    stmtSts.iNbToApprove = iTmp;

                int iNbPastTwentyDays = 0;
                for (int i = 0; i < listAgent.Count; i++)
                {
                    List<string> listRefBOUser = CommonMethod.GetAttributeValues(listAgent[i], "Agent", "RefBOUser");
                    List<string> listBoUserName = CommonMethod.GetAttributeValues(listAgent[i], "Agent", "BoUserName");
                    List<string> listNbStatementApproval = CommonMethod.GetAttributeValues(listAgent[i], "Agent", "NbStatementApproval");
                    List<string> listNbPendingStatement = CommonMethod.GetAttributeValues(listAgent[i], "Agent", "NbPendingStatement");

                    int iNbStatementApproval = 0;
                    if (listNbStatementApproval.Count > 0)
                        int.TryParse(listNbStatementApproval[0], out iNbStatementApproval);
                    int iNbPendingStatement = 0;
                    if (listNbPendingStatement.Count > 0)
                        int.TryParse(listNbPendingStatement[0], out iNbPendingStatement);
                    iNbPastTwentyDays += iNbPendingStatement;

                    dt.Rows.Add(new object[] { listRefBOUser[0], listBoUserName[0], iNbStatementApproval, iNbPendingStatement });
                }

                stmtSts.iNbPastTwentyDays = iNbPastTwentyDays;
                stmtSts.dtAgents = dt;

                Session["StatementStatus"] = stmtSts;
            }
        }
        catch(Exception e)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }
        
        return stmtSts;
    }
    [Serializable]
    class IPAddressLocation
    {
        public string Country { get; set; }
    }
    [Serializable]
    class IPAddressLocationAPIResp
    {
        public string ip { get; set; }
        public string type { get; set; }
        public string continent_code { get; set; }
        public string continent_name { get; set; }
        public string country_code { get; set; }
        public string country_name { get; set; }
        public string region_code { get; set; }
        public string region_name { get; set; }
        public string city { get; set; }
        public string zip { get; set; }
        public double? latitude { get; set; }
        public double? longitude { get; set; }
        public Location location { get; set; }
        public TimeZone time_zone { get; set; }
        public Currency currency { get; set; }
        public Connection connection { get; set; }
    }
    public class Language
    {
        public string code { get; set; }
        public string name { get; set; }
        public string native { get; set; }
    }

    public class Location
    {
        public int? geoname_id { get; set; }
        public string capital { get; set; }
        public List<Language> languages { get; set; }
        public string country_flag { get; set; }
        public string country_flag_emoji { get; set; }
        public string country_flag_emoji_unicode { get; set; }
        public string calling_code { get; set; }
        public bool is_eu { get; set; }
    }

    public class TimeZone
    {
        public string id { get; set; }
        public DateTime current_time { get; set; }
        public int? gmt_offset { get; set; }
        public string code { get; set; }
        public bool is_daylight_saving { get; set; }
    }

    public class Currency
    {
        public string code { get; set; }
        public string name { get; set; }
        public string plural { get; set; }
        public string symbol { get; set; }
        public string symbol_native { get; set; }
    }

    public class Connection
    {
        public int? asn { get; set; }
        public string isp { get; set; }
    }
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string getIPAddressLocation(string sIPAddress)
    {
        IPAddressLocation location = new IPAddressLocation();

        if (tools.ValidateIPAddress(sIPAddress))
        {
            // Recherche session si adresse déjà localisée
            if (Session["IPAddressLocationRequests"] != null)
            {
                DataTable dt = (DataTable)Session["IPAddressLocationRequests"];

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["IPAddress"].ToString() == sIPAddress)
                    {
                        location.Country = dt.Rows[i]["Country"].ToString();
                        break;
                    }
                }
            }

            // Valeur introuvable appel API
            if (String.IsNullOrWhiteSpace(location.Country))
            {
                string sURL = String.Format("https://api.ipstack.com/{0}?access_key={1}", sIPAddress, ConfigurationManager.AppSettings["Ipstack_api_key"].ToString());
                //string sURL = String.Format("http://freegeoip.net/json/{0}", sIPAddress);
                //string sURL = String.Format("http://ip-api.com/json/{0}", sIPAddress);

                try
                {
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(sURL);
                    using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                    using (Stream stream = response.GetResponseStream())
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        string sResponse = reader.ReadToEnd();

                        try
                        {
                            IPAddressLocationAPIResp resp = JsonConvert.DeserializeObject<IPAddressLocationAPIResp>(sResponse);

                            //if (resp.status == "success")
                            if (!string.IsNullOrWhiteSpace(resp.ip))
                            {
                                DataTable dtRequests = null;

                                if (Session["IPAddressLocationRequests"] != null)
                                    dtRequests = (DataTable)Session["IPAddressLocationRequests"];
                                else
                                {
                                    dtRequests = new DataTable();

                                    dtRequests.Columns.Add("IPAddress");
                                    dtRequests.Columns.Add("Country");
                                }

                                //dtRequests.Rows.Add(new object[] { resp.query, resp.country });
                                dtRequests.Rows.Add(new object[] { resp.ip, resp.country_name });
                                Session["IPAddressLocationRequests"] = dtRequests;

                                location.Country = resp.country_name;
                            }
                        }
                        catch (Exception ex2)
                        {
                        }
                    }
                }
                catch (Exception ex)
                { }
            }
        }

        return JsonConvert.SerializeObject(location);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetCounterpartEmailList(string prefix)
    {
        List<string> listEmail = new List<string>();
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("AML.P_GetCounterpartEmail", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN", new XElement("EMAIL_COUNTERPART", new XAttribute("Search", prefix))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if(sXmlOut.Length > 0)
            {
                listEmail = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/EMAIL_COUNTERPART", "EMAIL");
            }
        }
        catch(Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return listEmail.ToArray();
    }

    [WebMethod(EnableSession = true)]
    public int getPendingCustomerCloseCount()
    {
        int iPendingCustomerCloseCount = 0;
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_GetNbPendingCustomerClose", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@CashierToken", SqlDbType.VarChar, 200);
            cmd.Parameters.Add("@NbPendingCustomerClose", SqlDbType.Int);

            cmd.Parameters["@CashierToken"].Value = authentication.GetCurrent().sToken;
            cmd.Parameters["@NbPendingCustomerClose"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            iPendingCustomerCloseCount = int.Parse(cmd.Parameters["@NbPendingCustomerClose"].Value.ToString());
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }


        return iPendingCustomerCloseCount;
    }

    [WebMethod(EnableSession = true)]
    public int getPendingReturnFundsCount()
    {
        int iPendingReturnFundsCount = 0;
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_GetNbPendingReturnFunds", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@CashierToken", SqlDbType.VarChar, 200);
            cmd.Parameters.Add("@NbPendingReturnFunds", SqlDbType.Int);

            cmd.Parameters["@CashierToken"].Value = authentication.GetCurrent().sToken;
            cmd.Parameters["@NbPendingReturnFunds"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            iPendingReturnFundsCount = int.Parse(cmd.Parameters["@NbPendingReturnFunds"].Value.ToString());
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }


        return iPendingReturnFundsCount;
    }

}