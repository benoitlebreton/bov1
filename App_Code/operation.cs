﻿using ApiCobalt;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Web;
using System.Xml.Linq;
using XMLMethodLibrary;

/// <summary>
/// Description résumée de operation
/// </summary>
/// 
[Serializable]
public class operation
{
    [Serializable()]
    public class SearchCriteria : ISerializable
    {
        public int iPageSize { get; set; }
        public int iPageStep { get; set; }
        public string sDateFrom { get; set; }
        public string sDateTo { get; set; }
        public bool bForceRefresh { get; set; }
        public string sOperationType { get; set; }
        public string sCategoryTag { get; set; }
        public string sAccountNumber { get; set; }
        public string sStatus { get; set; }
        public string sDirection { get; set; }
        public string sOutputFormat { get; set; }
        public string sIBAN { get; set; }
        public bool bIsCobalt { get; set; }

        public SearchCriteria(int iPageSize, int iPageStep, string sDateFrom, string sDateTo, string sAccountNumber) { this.iPageSize = iPageSize; this.iPageStep = iPageStep; this.sDateFrom = sDateFrom; this.sDateTo = sDateTo; this.sAccountNumber = sAccountNumber; this.bForceRefresh = false; }
        public SearchCriteria(int iPageSize, int iPageStep, string sDateFrom, string sDateTo, string sAccountNumber, bool bForceRefresh) { this.iPageSize = iPageSize; this.iPageStep = iPageStep; this.sDateFrom = sDateFrom; this.sDateTo = sDateTo; this.sAccountNumber = sAccountNumber; this.bForceRefresh = bForceRefresh; }
        public SearchCriteria(int iPageSize, int iPageStep, string sDateFrom, string sDateTo, string sAccountNumber, string sOperationType, bool bForceRefresh) { this.iPageSize = iPageSize; this.iPageStep = iPageStep; this.sDateFrom = sDateFrom; this.sDateTo = sDateTo; this.sAccountNumber = sAccountNumber; this.sOperationType = sOperationType; this.bForceRefresh = bForceRefresh; }
        public SearchCriteria(int iPageSize, int iPageStep, string sDateFrom, string sDateTo, string sAccountNumber, string sOperationType, string sDirection, string sStatus, bool bForceRefresh)
        {
            this.iPageSize = iPageSize;
            this.iPageStep = iPageStep;
            this.sDateFrom = sDateFrom;
            this.sDateTo = sDateTo;
            this.sAccountNumber = sAccountNumber;
            this.sOperationType = sOperationType;
            this.sStatus = sStatus;
            this.sDirection = sDirection;
            this.bForceRefresh = bForceRefresh;
        }

        public SearchCriteria(int iPageSize, int iPageStep, string sDateFrom, string sDateTo, string sAccountNumber, string sOperationType, string sDirection, string sStatus, bool bForceRefresh, string sOututFormat)
        {
            this.iPageSize = iPageSize;
            this.iPageStep = iPageStep;
            this.sDateFrom = sDateFrom;
            this.sDateTo = sDateTo;
            this.sAccountNumber = sAccountNumber;
            this.sOperationType = sOperationType;
            this.sStatus = sStatus;
            this.sDirection = sDirection;
            this.bForceRefresh = bForceRefresh;
            this.sOutputFormat = sOutputFormat;
        }

        //Deserialization constructor.
        public SearchCriteria(SerializationInfo info, StreamingContext ctxt)
        {
            //Get the values from info and assign them to the appropriate properties
            iPageSize = (int)info.GetValue("PageSize", typeof(int));
            iPageStep = (int)info.GetValue("PageStep", typeof(int));
            sDateFrom = (string)info.GetValue("DateFrom", typeof(string));
            sDateTo = (string)info.GetValue("DateTo", typeof(string));
            sOperationType = (string)info.GetValue("OperationType", typeof(string));
            bForceRefresh = (bool)info.GetValue("ForceRefresh", typeof(bool));
            sAccountNumber = (string)info.GetValue("AccountNumber", typeof(string));
            sStatus = (string)info.GetValue("Status", typeof(string));
            sDirection = (string)info.GetValue("Direction", typeof(string));
            sOutputFormat = (string)info.GetValue("OutputFormat", typeof(string));
        }

        //Serialization function.
        public void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            //You can use any custom name for your name-value pair. But make sure you
            // read the values with the same name. For ex:- If you write EmpId as "EmployeeId"
            // then you should read the same with "EmployeeId"
            info.AddValue("PageSize", iPageSize);
            info.AddValue("PageStep", iPageStep);
            info.AddValue("DateFrom", sDateFrom);
            info.AddValue("DateTo", sDateTo);
            info.AddValue("OperationType", sOperationType);
            info.AddValue("ForceRefresh", bForceRefresh);
            info.AddValue("AccountNumber", sAccountNumber);
            info.AddValue("Status", sStatus);
            info.AddValue("Direction", sDirection);
            info.AddValue("OutputFormat", sOutputFormat);
        }
    }

    [Serializable]
    public class Category
    {
        public string sTag { get; set; }
        public string sLabel { get; set; }
        public string sImg { get; set; }
        public bool bUpdate { get; set; }
        public bool bSearch { get; set; }
        public Category(string sTag, string sLabel, string sImg, bool bUpdate, bool bSearch)
        {
            this.sTag = sTag;
            this.sLabel = sLabel;
            this.sImg = sImg;
            this.bUpdate = bUpdate;
            this.bSearch = bSearch;
        }
        public static List<Category> GetCategories()
        {
            List<Category> list = new List<Category>();
            list.Add(new Category("VOY", "Voyage", "picto-voyage.png", true, true));
            list.Add(new Category("ALI", "Alimentation", "picto-alimentation.png", true, true));
            list.Add(new Category("IMM", "Immobilier", "picto-immobilier.png", true, true));
            list.Add(new Category("TRA", "Transport", "picto-transport.png", true, true));
            list.Add(new Category("LOI", "Loisirs", "picto-loisirs.png", true, true));
            list.Add(new Category("IMP", "Impôts", "picto-impots.png", true, true));
            list.Add(new Category("ADB", "Achat de biens", "picto-commerce.png", true, true));
            list.Add(new Category("VES", "Vestimentaire", "picto-vestimentaire.png", true, true));
            list.Add(new Category("FNI", "Frais Nickel", "picto-fraisnickel.png", false, false));
            list.Add(new Category("VEM", "Virement émis", "picto-virementsortant.png", false, false));
            list.Add(new Category("VRE", "Virement reçu", "picto-viremententrant.png", false, false));
            list.Add(new Category("RET", "Retrait cash", "picto-retrait.png", false, false));
            list.Add(new Category("DEP", "Dépôt cash", "picto-depot.png", false, false));
            list.Add(new Category("", "N.C.", "picto-default.png", false, false));
            return list;
        }
    }

    public bool bCheckOpeResult(DataTable dt)
    {
        bool bResultOK = true;

        if (dt.Columns.IndexOf("OPE_DATE") == -1)
            bResultOK = false;

        return bResultOK;
    }

    public DataTable getOperations(SearchCriteria criteria, out int iRowCount)
    {
        iRowCount = 0;

        if (criteria.bIsCobalt)
        {
            // GET COBALT OPE HERE
            return getOperationsFromCobalt(criteria);
        }
        else
        {
            return getOperationsFromDB(criteria, out iRowCount);
        }
    }
    protected string isNull(string sValue, string sReplaceValue)
    {
        string sReturnValue = sValue;
        if (sValue == null)
            sReturnValue = sReplaceValue;

        return sReturnValue;
    }

    protected DataTable getOperationsFromDB(SearchCriteria criteria, out int iRowCount)
    {
        iRowCount = 0;
        DataTable dt = new DataTable();
        authentication auth = authentication.GetCurrent();
        bool isOK = false;

        SqlConnection conn = null;

        try
        {
            var sscsb = new SqlConnectionStringBuilder(ConfigurationManager.ConnectionStrings["Nickel"].ConnectionString);
            //sscsb.CommTimeout = 80;
            conn = new SqlConnection(sscsb.ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("Nickel.P_GetUserOperationsV2", conn);
            cmd.CommandTimeout = 80;
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@sXmlIn", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@sXmlOut", SqlDbType.VarChar, -1);

            cmd.Parameters.Add("@RC", SqlDbType.Int);

            cmd.Parameters["@sXmlIn"].Value = new XElement("ALL_XML_IN",
                                                new XElement("UserOperations",
                                                    new XAttribute("Culture", "fr-FR"),
                                                    new XAttribute("iReturnType", "1"),
                                                    new XAttribute("OutputFormat", (criteria.sOutputFormat == null) ? "" : criteria.sOutputFormat),
                                                    new XAttribute("DateFrom", criteria.sDateFrom),
                                                    new XAttribute("DateTo", criteria.sDateTo),
                                                    new XAttribute("PageIndex", "1"),
                                                    new XAttribute("PageSize", criteria.iPageSize.ToString()),
                                                    new XAttribute("ForceRefresh", (criteria.bForceRefresh) ? "1" : "0"),
                                                    new XAttribute("OpeCodeNickelList", (criteria.sOperationType == null) ? "" : criteria.sOperationType),
                                                    new XAttribute("TagCategoryFilterList", (criteria.sCategoryTag == null) ? "" : criteria.sCategoryTag),
                                                    new XAttribute("UserBankAccount", criteria.sAccountNumber),
                                                    new XAttribute("TOKEN", auth.sToken))).ToString();
            cmd.Parameters["@sXmlOut"].Direction = ParameterDirection.Output;
            cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;

            da.Fill(dt);

            dt.Columns.Add("ADDITIONAL_INFO_HTML");

            string sXmlOut = cmd.Parameters["@sXmlOut"].Value.ToString();

            string sRowCount = "0";
            GetValueFromXml(sXmlOut, "ALL_XML_OUT/UserOperations", "NbResult", out sRowCount);

            iRowCount = int.Parse(sRowCount);

            if (cmd.Parameters["@RC"].Value.ToString() == "0")
                isOK = true;

        }
        catch (Exception ex)
        {
            isOK = false;
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }

    protected DataTable getOperationsFromCobalt(SearchCriteria criteria)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("row");
        dt.Columns.Add("OPE_STATUS");
        dt.Columns.Add("OPE_DATE", typeof(DateTime));
        dt.Columns.Add("OPE_TIME");
        dt.Columns.Add("OPE_CODE");
        dt.Columns.Add("CODE_MCC");
        dt.Columns.Add("OPE_CODE_LABEL");
        dt.Columns.Add("OPE_LABEL");
        dt.Columns.Add("OPE_LABEL_DET1");
        dt.Columns.Add("OPE_LABEL_DET2");
        dt.Columns.Add("MERC_NB");
        dt.Columns.Add("MERC_NAME");
        dt.Columns.Add("MERC_ADD");
        dt.Columns.Add("MERC_ZIP");
        dt.Columns.Add("MERC_CITY");
        dt.Columns.Add("AMOUNT", typeof(Decimal));
        dt.Columns.Add("CUR_CODE");
        dt.Columns.Add("COUNTRY");
        dt.Columns.Add("AUTO_NUM");
        dt.Columns.Add("CATEGORY");
        dt.Columns.Add("TAGCATEGORY");
        dt.Columns.Add("TRX_MODE");
        dt.Columns.Add("CH_AUTH_METH");
        dt.Columns.Add("OPE_ENV");
        dt.Columns.Add("CATEGORY_CHG");
        dt.Columns.Add("ADDITIONAL_INFO_HTML");
        dt.Columns.Add("OPE_ID");

        CobaltApi api = new CobaltApi(ConfigurationManager.AppSettings["Cobalt_Core_URL"].ToString(), ConfigurationManager.AppSettings["Cobalt_Transfer_URL"].ToString(), null, ConfigurationManager.AppSettings["Cobalt_Expiration_URL"].ToString());
        ApiCobalt.Accounts.Account account = api.Account.GetAccount(criteria.sIBAN);

        DateTime? dtFrom = null, dtTo = null;
        DateTime dtTmp;
        if (!String.IsNullOrWhiteSpace(criteria.sDateFrom))
            if (DateTime.TryParseExact(criteria.sDateFrom, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dtTmp))
                dtFrom = dtTmp;
        if (!String.IsNullOrWhiteSpace(criteria.sDateTo))
            if (DateTime.TryParseExact(criteria.sDateTo, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dtTmp))
                dtTo = dtTmp;

        if (dtFrom == null)
            if (criteria.bForceRefresh)
            {
                if (account.Category == ApiCobalt.Accounts.AccountKind.PRO_CATEGORY)
                    dtFrom = DateTime.Now.AddDays(-7);
                else dtFrom = DateTime.Now.AddMonths(-3);
            }
            else dtFrom = new DateTime(2000, 1, 1);
        
        if (dtTo == null)
            dtTo = DateTime.Now;

        ApiCobalt.Transactions.TransactionParams transactionParams = new ApiCobalt.Transactions.TransactionParams();
        transactionParams.StartDate = dtFrom;
        transactionParams.EndDate = dtTo;
        if (criteria.sOperationType != null)
        {
            List<string> listExtOpeTypeTmp = criteria.sOperationType.Split(',').ToList();
            List<ApiCobalt.Transactions.TransactionParams.ExternalOperationType> listExtOpeType = new List<ApiCobalt.Transactions.TransactionParams.ExternalOperationType>();

            for (int i = 0; i < listExtOpeTypeTmp.Count; i++)
            {
                ApiCobalt.Transactions.TransactionParams.ExternalOperationType extOpe;
                string sTypeTag;
                if (!String.IsNullOrWhiteSpace(listExtOpeTypeTmp[i])
                    && OperationType.CobaltOperationTypesOther.TryGetValue(int.Parse(listExtOpeTypeTmp[i]), out sTypeTag))
                    if (System.Enum.TryParse(sTypeTag, out extOpe))
                    {
                        listExtOpeType.Add(extOpe);
                    }
            }
            transactionParams.ExternalOperationTypes = listExtOpeType;
        }
        
        List<ApiCobalt.Transactions.Transaction> listTransaction = api.Transaction.GetTransactions(criteria.sIBAN, transactionParams);

        for (int i = 0; i < listTransaction.Count; i++)
        {
            try
            {
                List<string> listRejectStatus = new List<string>() { "2", "12", "14" };
                //(OPE_STATUS_REJECTED, 2)->rejet comptable
                //(OPE_STATUS_KO, 12)->KO général
                //(OPE_STATUS_REJECTED_TECH, 14)->rejet technique

                if (!listRejectStatus.Contains(listTransaction[i].OpeStatus.Value))
                {
                    string sOpeStatus = "";
                    if (listTransaction[i].OpeStatus.Value == "3")
                        sOpeStatus = "2";

                    string sOpeCode = "";
                    if (!String.IsNullOrWhiteSpace(listTransaction[i].MerchantAddress))
                        sOpeCode = "1";
                    else sOpeCode = (listTransaction[i].OpeCode != null) ? listTransaction[i].OpeCode.Value : "";

                    string sOpeCodeLabel = "";
                    if (listTransaction[i].ExternalOpeCode != null)
                        OperationType.CobaltOperationTypes.TryGetValue(int.Parse(listTransaction[i].ExternalOpeCode.Value), out sOpeCodeLabel);

                    string sOpeLabel = "";
                    sOpeLabel = (!string.IsNullOrWhiteSpace(listTransaction[i].MerchantName)) ? listTransaction[i].MerchantName : sOpeLabel;
                    sOpeLabel = (!string.IsNullOrWhiteSpace(listTransaction[i].Description)) ? listTransaction[i].Description : sOpeLabel;
                    if (!string.IsNullOrWhiteSpace(listTransaction[i].Label1))
                        sOpeLabel = listTransaction[i].Label1;

                    string sOpeLabel2 = (!string.IsNullOrWhiteSpace(listTransaction[i].Label2)) ? listTransaction[i].Label2 : "";

                    double dAmount = listTransaction[i].Amount / 100d;
                    double dComAmount = 0;
                    int iComCode = 0;
                    string sOpeComCodeLabel;

                    // COM AMOUNT CALCULATION
                    if (listTransaction[i].Commissions != null)
                        for (int j = 0; j < listTransaction[i].Commissions.Count; j++)
                        {
                            if (listTransaction[i].Commissions[j].Amount != 0)
                            {
                                double dComAmountTmp = (double)listTransaction[i].Commissions[j].Amount / 100d;
                                if (dComAmountTmp < 0)
                                    dComAmountTmp *= -1;
                                dComAmount += dComAmountTmp;
                            }
                        }

                    bool bComAmountRecalculated = false;
                    try
                    {
                        if (listTransaction[i].OpeCode.Value == "302" && dComAmount == 0)
                        {
                            bComAmountRecalculated = true;
                            if (listTransaction[i].LocalAmount > 0 && listTransaction[i].Amount > 0)
                            {
                                dComAmount = (listTransaction[i].LocalAmount - listTransaction[i].Amount) / 100d;
                            }
                        }
                    }
                    catch(Exception ex)
                    {
                        tools.LogToFile(ex.Message, "calcul commission depot cash initial");
                        throw new Exception(ex.Message);
                    }

                    if (listTransaction[i].OpeDirection.ToLower() == "credit")
                        dAmount = dAmount + dComAmount;
                    else dAmount = dAmount - dComAmount;

                    if (dComAmount > 0 && !bComAmountRecalculated) { dComAmount *= -1; }

                    DateTime dateOpe = (listTransaction[i].LocalTransactionDate != null) ? listTransaction[i].LocalTransactionDate : (listTransaction[i].OpeDate != null) ? listTransaction[i].OpeDate : DateTime.Now;
                    string sOpeHour = (listTransaction[i].LocalTransactionDate != null) ? listTransaction[i].LocalTransactionDate.ToString("HH:mm:ss") : "";
                    string sCatCodeKey = (listTransaction[i].CatCode != null) ? listTransaction[i].CatCode.Key : "";
                    string sMerchantName = (listTransaction[i].MerchantName != null) ? listTransaction[i].MerchantName : "";
                    string sMerchantAddress = (listTransaction[i].MerchantAddress != null) ? listTransaction[i].MerchantAddress : "";
                    string sMerchantZipcode = (listTransaction[i].MerchantZipcode != null) ? listTransaction[i].MerchantZipcode : "";
                    string sMerchantCity = (listTransaction[i].MerchantCity != null) ? listTransaction[i].MerchantCity : "";
                    string sCurrencyCode = (listTransaction[i].CurrencyCode != null) ? listTransaction[i].CurrencyCode : "";
                    string sAuthNumber = (listTransaction[i].AuthorizationNumber != null) ? listTransaction[i].AuthorizationNumber : "";
                    string sCatCodeValue = "";
                    if (listTransaction[i].CatCode != null && !string.IsNullOrWhiteSpace(listTransaction[i].CatCode.Value))
                    {
                        OperationType.OperationCategory ope;
                        if (OperationType.OperationCategories.TryGetValue(int.Parse(listTransaction[i].CatCode.Value), out ope))
                            sCatCodeValue = ope.sCatTag;
                    }
                    switch (listTransaction[i].ExternalOpeCode.Value)
                    {
                        case "700": //retrait cash
                        case "200": //retrait cash dab
                            iComCode = 604; //commission retrait cash
                            break;
                        case "300": //depot cash
                            iComCode = 600; //commission depot cash
                            break;
                        case "350": //depot cash initial
                            iComCode = 601; //commission depot cash initial
                            break;
                        case "400":
                            sCatCodeValue = "VRE";
                            if (listTransaction[i].AccountToAccountDetails != null)
                            {
                                sOpeLabel2 = sOpeLabel;
                                sOpeLabel = String.Format("Virement de {0}", listTransaction[i].AccountToAccountDetails.CounterPartName);
                            }
                            break;
                        case "500":
                            sCatCodeValue = "VEM";
                            if (listTransaction[i].AccountToAccountDetails != null)
                            {
                                sOpeLabel2 = sOpeLabel;
                                sOpeLabel = String.Format("Virement à {0}", listTransaction[i].AccountToAccountDetails.CounterPartName);
                            }
                            break;
                        case "610":
                            sCatCodeValue = "FNI";
                            break;
                        case "1000":
                            if (listTransaction[i].ExternalOpeCode != null)
                                int.TryParse(listTransaction[i].ExternalOpeCode.Value, out iComCode);
                            if (listTransaction[i].AccountToAccountDetails != null)
                            {
                                sOpeLabel2 = sOpeLabel;
                                sOpeLabel = String.Format("Prélèvement de {0}", listTransaction[i].AccountToAccountDetails.CounterPartName);
                            }
                            break;
                        default:
                            if (listTransaction[i].ExternalOpeCode != null)
                                int.TryParse(listTransaction[i].ExternalOpeCode.Value, out iComCode);
                            break;
                    }
                    OperationType.CobaltOperationTypes.TryGetValue(iComCode, out sOpeComCodeLabel);

                    string sOpeComLabel = sOpeComCodeLabel;
                    switch (listTransaction[i].OpeCode.Value)
                    {
                        case "527": // credit compte par CB
                            sOpeComLabel = "Frais recharge CB";
                            break;
                    }

                    StringBuilder sbHTML = new StringBuilder();
                    sbHTML.AppendLine("<div class=\"cobalt-ope-details\">");
                        sbHTML.AppendLine("<div><div>Solde après opération</div><div>" + (listTransaction[i].NewBalance / 100d) + " &euro;</div></div>");
                    if(!string.IsNullOrWhiteSpace(listTransaction[i].AuthorizationNumber))
                        sbHTML.AppendLine("<div><div>N° d'autorisation</div><div>" + listTransaction[i].AuthorizationNumber + "</div></div>");
                    if (!string.IsNullOrWhiteSpace(listTransaction[i].SepaNumber))
                        sbHTML.AppendLine("<div><div>N° d'opération SEPA</div><div>" + listTransaction[i].SepaNumber + "</div></div>");
                    if (listTransaction[i].ProvisionDate != null)
                        sbHTML.AppendLine("<div><div>Date d'autorisation</div><div>" + listTransaction[i].ProvisionDate + "</div></div>");
                    if (listTransaction[i].AccountingDate != null)
                        sbHTML.AppendLine("<div><div>Date compensation</div><div>" + listTransaction[i].AccountingDate + "</div></div>");
                    if (listTransaction[i].AccountToAccountDetails != null)
                    {
                        if (listTransaction[i].AccountToAccountDetails.CounterPartBic == "FPELFR21")
                        {
                            sbHTML.AppendLine("<div><div>IBAN</div><div><a target=\"_blank\" href=\"ClientDetails.aspx?iban=" + listTransaction[i].AccountToAccountDetails.CounterPartIban + "\">" + listTransaction[i].AccountToAccountDetails.CounterPartIban + "</a></div></div>");
                            sOpeLabel = String.Format("<span style=\"color:Orange\">{0}</span>", sOpeLabel);
                        }
                        else sbHTML.AppendLine("<div><div>IBAN</div><div>" + listTransaction[i].AccountToAccountDetails.CounterPartIban + "</div></div>");
                        sbHTML.AppendLine("<div><div>BIC</div><div>" + listTransaction[i].AccountToAccountDetails.CounterPartBic + "</div></div>");
                    }
                    sbHTML.AppendLine("</div>");

					string sOpeId = (listTransaction[i].OpeId != null) ? listTransaction[i].OpeId : "";

                    // CAS DES OPÉRATIONS BURALISTE
                    if (account.Category == ApiCobalt.Accounts.AccountKind.PRO_CATEGORY)
                    {
                        if (listTransaction[i].OpeCode.Key == "OPE_CODE_ON_US_PAYMENT")
                        {
                            if (string.IsNullOrWhiteSpace(sOpeLabel))
                                sOpeLabel = "ACHAT CLIENT NICKEL";
                            if (string.IsNullOrWhiteSpace(sCatCodeValue))
                                sCatCodeValue = "ADB";
                        }

                        if ((listTransaction[i].OpeCode.Key == "OPE_CODE_ON_US_DEPOSIT"
                        || listTransaction[i].OpeCode.Key == "OPE_CODE_ON_US_WITHDRAWAL")
                        || listTransaction[i].OpeCode.Key == "OPE_CODE_ON_US_SUBSCRIPTION_DEPOSIT")
                        {
                            dAmount = listTransaction[i].LocalAmount / 100;
                            switch (listTransaction[i].OpeCode.Key)
                            {
                                case "OPE_CODE_ON_US_DEPOSIT":
                                    dComAmount = (listTransaction[i].LocalAmount - listTransaction[i].Amount) / 100;
                                    if (string.IsNullOrWhiteSpace(sOpeLabel))
                                        sOpeLabel = "DEPOT CASH CLIENT";
                                    if (string.IsNullOrWhiteSpace(sCatCodeValue))
                                        sCatCodeValue = "DEP";
                                    break;
                                case "OPE_CODE_ON_US_WITHDRAWAL":
                                    dComAmount = (listTransaction[i].Amount - listTransaction[i].LocalAmount) / 100;
                                    if (string.IsNullOrWhiteSpace(sOpeLabel))
                                        sOpeLabel = "RETRAIT CASH CLIENT";
                                    if (string.IsNullOrWhiteSpace(sCatCodeValue))
                                        sCatCodeValue = "RET";
                                    break;
                                case "OPE_CODE_ON_US_SUBSCRIPTION_DEPOSIT":
                                    if (string.IsNullOrWhiteSpace(sOpeLabel))
                                        sOpeLabel = "DEPOT CASH INITIAL";
                                    if (string.IsNullOrWhiteSpace(sCatCodeValue))
                                        sCatCodeValue = "DEP_INIT";
                                    break;
                            }
                        }
                        else if (listTransaction[i].OpeCode.Key == "OPE_CODE_ANNUAL_SUBSCRIPTION")
                        {
                            sOpeLabel = "Cotisation annuelle client";
                        }
                        else if (listTransaction[i].OpeCode.Key == "OPE_CODE_ACCOUNT_OPENING" && listTransaction[i].Amount == 50)
                        {
                            sOpeLabel = "Commission supplémentaire sur ouverture de compte 12-18";
                        }

                    }

                    // CORRECTION HEURE OPE ON US
                    if(listTransaction[i].OpeCode.Key.Contains("_ON_US_"))  
                    {
                        if (listTransaction[i].OpeDate != null)
                        {
                            dateOpe = listTransaction[i].OpeDate;
                            if (dateOpe.ToString("HH:mm:ss") != "00:00:00")
                            {
                                DateTime dtToEuropeTZ = TimeZoneInfo.ConvertTimeFromUtc(dateOpe, TimeZoneInfo.FindSystemTimeZoneById("Central Europe Standard Time"));
                                sOpeHour = dtToEuropeTZ.ToString("HH:mm:ss");
                                dateOpe = dtToEuropeTZ;
                            }
                        }
                    }

                    // ADD OPE AMOUNT
                    dt.Rows.Add(new object[] { i,
                    sOpeStatus,
                    dateOpe,
                    sOpeHour,
                    sOpeCode,
                    sCatCodeKey,
                    sOpeCodeLabel, sOpeLabel, sOpeLabel2, "", "",
                    sMerchantName,
                    sMerchantAddress,
                    sMerchantZipcode,
                    sMerchantCity,
                    ((listTransaction[i].OpeDirection.ToLower() == "debit" && !listTransaction[i].Amount.ToString().StartsWith("-")) ? "-" : "") + dAmount.ToString() + (!dAmount.ToString().Replace('.', ',').Contains(",") ? ",00" : "00"),
                    sCurrencyCode,
                    "",
                    sAuthNumber,
                    "",
                    sCatCodeValue,
                    "", "", "", "",
                    sbHTML,
                    sOpeId });
        
                    // ADD OPE COM AMOUNT
                    if(dComAmount != 0)
                    dt.Rows.Add(new object[] { i,
                        sOpeStatus,
                        dateOpe,
                        sOpeHour,
                        sOpeCode,
                        "",
                        sOpeComCodeLabel,
                        sOpeComLabel, //"Commission", //sOpeLabel,
                        sOpeLabel2, "", "",
                        sMerchantName,
                        sMerchantAddress,
                        sMerchantZipcode,
                        sMerchantCity,
                        dComAmount.ToString() + (!dComAmount.ToString().Replace('.', ',').Contains(",") ? ",00" : "00"),
                        sCurrencyCode,
                        "",
                        sAuthNumber,
                        "",
                        "FNI", //sCatCodeValue,
                        "", "", "", "",
                        sbHTML,
                        sOpeId });
                }
            }
            catch(Exception exc)
            {
                tools.LogToFile(exc.Message, "getOperationsFromCobalt");
                throw new Exception(exc.Message);
            }
        }

        dt.DefaultView.Sort = "OPE_DATE DESC";

        return dt;
    }

    protected void GetValueFromXml(string sXml, string sNode, string sAttribute, out string sValue)
    {
        sValue = "";

        List<string> tagList = CommonMethod.GetAttributeValues(sXml.ToString(), sNode, sAttribute);

        if (tagList.Count > 0)
            sValue = tagList[0];
    }

    public static string getCategoryImage(string sTag, string sMerchantNumber, string sLabel, bool bAddTooltip, string sUpdatable)
    {
        string sImageName = "";
        string sTooltip = "";

        switch (sTag)
        {
            case "VOY":
                sImageName = "picto-voyage.png";
                sTooltip = "Voyage";
                break;
            case "ALI":
                sImageName = "picto-alimentation.png";
                sTooltip = "Alimentation";
                break;
            case "IMM":
                sImageName = "picto-immobilier.png";
                sTooltip = "Immobilier";
                break;
            case "TRA":
                sImageName = "picto-transport.png";
                sTooltip = "Transport";
                break;
            case "LOI":
                sImageName = "picto-loisirs.png";
                sTooltip = "Loisirs";
                break;
            case "IMP":
                sImageName = "picto-impots.png";
                sTooltip = "Impôts";
                break;
            case "ADB":
                sImageName = "picto-commerce.png";
                sTooltip = "Achat de biens";
                break;
            case "VES":
                sImageName = "picto-vestimentaire.png";
                sTooltip = "Vestimentaire";
                break;
            case "FNI":
                sImageName = "picto-fraisnickel.png";
                sTooltip = "Frais Nickel";
                break;
            case "VEM":
                sImageName = "picto-virementsortant.png";
                sTooltip = "Virement émis";
                break;
            case "VRE":
                sImageName = "picto-viremententrant.png";
                sTooltip = "Virement reçu";
                break;
            case "RET":
                sImageName = "picto-retrait.png";
                sTooltip = "Retrait cash";
                break;
            case "DEP":
                sImageName = "picto-depot.png";
                sTooltip = "Dépôt cash";
                break;
            default:
                sImageName = "picto-default.png";
                sTooltip = "N.C.";
                break;
        }

        string sReturn = "<span";
        if(bAddTooltip)
            sReturn += " class=\"tooltip\" tooltiptext=\"" + sTooltip + "\"";
        sReturn += " style=\"position:relative;top:-4px\"><img src=\"Styles/Img/" + sImageName + "\" style=\"height:25px\" alt=\"" + sTooltip + "\"";
        //if (sUpdatable.Length > 0 && sUpdatable == "1")
            //sReturn += " onclick=\"ShowChangeCategoryPopup(event, '" + sMerchantNumber + "', '" + sTag + "', '" + sLabel + "');\"";
        sReturn += "/></span>";

        return sReturn;
    }

    public static DataTable getUserAuthorizationList(SearchCriteria criteria, out int iRowCount)
    {

        SqlConnection conn = null;
        DataTable dt = new DataTable();
        iRowCount = 0;
        bool isOK = false;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Nickel"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("Nickel.P_GetUserAutorizations", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@sXmlIn", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@sXmlOut", SqlDbType.VarChar, -1);

            cmd.Parameters["@sXmlIn"].Value = new XElement("ALL_XML_IN", new XElement("UserAutorizations",
                                                new XAttribute("UserBankAccount", criteria.sAccountNumber),
                                                new XAttribute("PageIndex", "1"),
                                                new XAttribute("PageSize", criteria.iPageSize.ToString()),
                                                new XAttribute("DateFrom", criteria.sDateFrom.ToString()),
                                                new XAttribute("DateTo", criteria.sDateTo.ToString()),
                                                new XAttribute("sAutorizationType", (criteria.sOperationType != null && criteria.sOperationType.Trim().Length > 0) ? criteria.sOperationType.Trim() : "ALL"),
                                                new XAttribute("ForceRefresh", (criteria.bForceRefresh) ? "1" : "0"),
                                                new XAttribute("Culture", "fr-FR"))).ToString();
            cmd.Parameters["@sXmlOut"].Direction = ParameterDirection.Output;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;

            da.Fill(dt);

            string sXmlOut = cmd.Parameters["@sXmlOut"].Value.ToString();
            string sRowCount = tools.GetValueFromXml(sXmlOut, "ALL_XML_OUT/UserAutorizations", "NbResult");

            iRowCount = int.Parse(sRowCount);

            if (cmd.Parameters["@RC"].Value.ToString() == "0")
                isOK = true;

        }
        catch (Exception e)
        {
            isOK = false;
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }

    public static DataTable getUserTransferList(SearchCriteria criteria, out int iRowCount)
    {
        iRowCount = 0;

        if (criteria.bIsCobalt)
        {
            // GET COBALT OPE HERE
            return getUserTransferListFromCobalt(criteria);
        }
        else
        {
            return getUserTransferListFromDB(criteria, out iRowCount);
        }
    }

    public static DataTable getUserTransferListFromDB(SearchCriteria criteria, out int iRowCount)
    {
        SqlConnection conn = null;
        DataTable dt = new DataTable();
        iRowCount = 0;
        bool isOK = false;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Nickel"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("Nickel.P_GetUserTransfers", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@sXmlIn", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@sXmlOut", SqlDbType.VarChar, -1);

            cmd.Parameters["@sXmlIn"].Value = new XElement("ALL_XML_IN", new XElement("UserTransfers",
                                                new XAttribute("UserBankAccount", criteria.sAccountNumber),
                                                new XAttribute("PageIndex", "1"),
                                                new XAttribute("PageSize", criteria.iPageSize.ToString()),
                                                new XAttribute("DateFrom", criteria.sDateFrom.ToString()),
                                                new XAttribute("DateTo", criteria.sDateTo.ToString()),
                                                new XAttribute("sOperationType", (criteria.sOperationType != null && criteria.sOperationType.Trim().Length > 0) ? criteria.sOperationType.Trim() : "ALL"),
                                                new XAttribute("sOperationOkKo", (criteria.sStatus != null && criteria.sStatus.Trim().Length > 0) ? criteria.sStatus.Trim() : "ALL"),
                                                new XAttribute("sDirection", (criteria.sDirection != null && criteria.sDirection.Trim().Length > 0) ? criteria.sDirection.Trim() : "ALL"),
                                                new XAttribute("ForceRefresh", (criteria.bForceRefresh) ? "1" : "0"),
                                                new XAttribute("Culture", "fr-FR"))).ToString();
            cmd.Parameters["@sXmlOut"].Direction = ParameterDirection.Output;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;

            da.Fill(dt);

            string sXmlOut = cmd.Parameters["@sXmlOut"].Value.ToString();
            string sRowCount = tools.GetValueFromXml(sXmlOut, "ALL_XML_OUT/UserTransfers", "NbResult");

            iRowCount = int.Parse(sRowCount);
        }
        catch (Exception e)
        {
            isOK = false;
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }

    protected static DataTable getUserTransferListFromCobalt(SearchCriteria criteria)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("row");
        dt.Columns.Add("CodeOPE");
        dt.Columns.Add("DateCreation", typeof(DateTime));
        dt.Columns.Add("DateReglement", typeof(DateTime));
        dt.Columns.Add("MontantEchange", typeof(Decimal));
        dt.Columns.Add("CodeRejet");
        dt.Columns.Add("IBANDO");
        dt.Columns.Add("BICDO");
        dt.Columns.Add("sLibelleOpeNickel");
        dt.Columns.Add("Libelle");
        dt.Columns.Add("NumOPE");
        dt.Columns.Add("rumId");
        dt.Columns.Add("NomDO");

        dt.Columns.Add("ADDITIONAL_INFO_HTML");

        CobaltApi api = new CobaltApi(ConfigurationManager.AppSettings["Cobalt_Core_URL"].ToString(), ConfigurationManager.AppSettings["Cobalt_Transfer_URL"].ToString(), null, ConfigurationManager.AppSettings["Cobalt_Expiration_URL"].ToString());
        List<ApiCobalt.DirectDebits.DirectDebit> listTransaction = api.DirectDebit.GetDirectDebits(criteria.sIBAN);
        for (int i = 0; i < listTransaction.Count; i++ )
        {
            try
            {
                string sOpeCode = "R0P";
                DateTime dtDateCreation = listTransaction[i].CollectionDate;
                DateTime dtDateReglement = listTransaction[i].CollectionDate;
                double dAmount = listTransaction[i].Amount / 100d;
                string sCodeRejet = listTransaction[i].CancelReference;
                string sIbanDo = listTransaction[i].CounterPartIban;
                string sBicDo = listTransaction[i].CounterPartBic;
                string sOpeLabel = "";
                sOpeLabel = (listTransaction[i].CounterPartName != null) ? listTransaction[i].CounterPartName : sOpeLabel;
                sOpeLabel = (!string.IsNullOrWhiteSpace(listTransaction[i].Description)) ? listTransaction[i].Description : sOpeLabel;
                string sReference = listTransaction[i].Reference;
                string sRum = listTransaction[i].Rum;
                string sNomDO = listTransaction[i].CounterPartName;
                string sStatus = listTransaction[i].Status.ToString();
                string sbHTML = "";

                switch(sStatus)
                {
                    case "OpenDemandStatus":
                    case "CloseDemandStatus":
                        sCodeRejet = (sCodeRejet == null) ? "" : sCodeRejet;
                        break;
                    case "MigratedDemandStatus":
                        sCodeRejet = "MigratedDemandStatus";
                        break;
                    default:
                        sCodeRejet = (sCodeRejet == null) ? sStatus : sCodeRejet;
                        break;
                }

                dt.Rows.Add(new object[] { i,
                    sOpeCode,
                    dtDateCreation.ToString(),
                    dtDateReglement.ToString(),
                    dAmount.ToString() + (!dAmount.ToString().Replace('.', ',').Contains(",") ? ",00" : "00"),
                    sCodeRejet,
                    sIbanDo,
                    sBicDo,
                    sOpeLabel,
                    sOpeLabel,
                    sReference,
                    sRum,
                    sNomDO,
                    sbHTML });

            }
            catch(Exception ex)
            {

            }
        }
            
        dt.DefaultView.Sort = "DateReglement DESC";

        return dt;
    }

    public static DataTable getUserTransferListTreated(DataTable dtUserTransferListBrut, ref int iRowCount)
    {
        string sCodeOPE = "", sNumOpeOrigin="";
        DataTable dtTransferListTreated = dtUserTransferListBrut.Copy();
        DataTable _dtUserTransferListBrut = dtUserTransferListBrut.Copy();

        //Récuperer la liste des prélèvements refusés
        for (int i = 0; i < dtUserTransferListBrut.Rows.Count; i++)
        {
            sCodeOPE = dtUserTransferListBrut.Rows[i]["CodeOPE"].ToString().Trim().ToUpper();
            
            if (sCodeOPE == "A2P" || sCodeOPE == "A3P")
            {
                iRowCount--;
                sNumOpeOrigin = dtUserTransferListBrut.Rows[i]["NumOPEorigine"].ToString().Trim().ToUpper();

                //Recuperer les infos de la transaction annulée + suppression
                for (int j = 0; j < _dtUserTransferListBrut.Rows.Count; j++)
                {
                    if (_dtUserTransferListBrut.Rows[j]["NumOPE"].ToString().Trim() == sNumOpeOrigin)
                    {
                        if (dtTransferListTreated.Rows[i].RowState != DataRowState.Deleted)
                        {
                            dtTransferListTreated.Rows[i]["IBANDO"] = _dtUserTransferListBrut.Rows[j]["IBANDO"];
                            dtTransferListTreated.Rows[i]["BICDO"] = _dtUserTransferListBrut.Rows[j]["BICDO"];
                            dtTransferListTreated.Rows[i]["NomDO"] = _dtUserTransferListBrut.Rows[j]["NomDO"];
                            dtTransferListTreated.Rows[j].Delete();
                        }
                    }
                }
            }
        }

        dtTransferListTreated.AcceptChanges();

        return dtTransferListTreated;
    }

    public static string getTransferStatus(string sDateCreation, string sDateReglement, string sAmount, string sCodeRejet)
    {
        string sStatus = "";

        string _sDateCreation = "";
        string _sDateReglement = "";

        DateTime dtToday = DateTime.Parse(DateTime.Now.ToShortDateString());
        DateTime dtDateCreation; DateTime dtDateReglement;
        if (DateTime.TryParse(sDateCreation, out dtDateCreation))
            _sDateCreation = dtDateCreation.ToString("dd/MM/yyyy hh:mm");
        if (DateTime.TryParse(sDateReglement, out dtDateReglement))
            _sDateReglement = dtDateReglement.ToString("dd/MM/yyyy hh:mm");

        string sSigne = sAmount.Trim().Substring(0, 1);
        if (sSigne.Trim().Length == 0)
            sSigne = "+";

        if (sSigne == "+")
        {
            /*if (!String.IsNullOrWhiteSpace(sCodeRejet))
            {
                if (sCodeRejet == "MigratedDemandStatus")
                    sStatus = "<span style=\"color:red\">Contacter service exploitation</span>";
                else*/
            if (!String.IsNullOrWhiteSpace(sCodeRejet) && sCodeRejet != "MigratedDemandStatus")
            {
                sStatus = "<span style=\"color:red\">Refusé</span>";
            }
            else if (dtDateReglement != null && dtDateReglement <= dtToday)
                sStatus = "Prélevé";
            else
                sStatus = "A venir";
        }
        else
        {
            /*if (!String.IsNullOrWhiteSpace(sCodeRejet) || ((sDateReglement.Trim().Length == 0 || dtDateReglement == null) && dtDateCreation != null && dtDateCreation <= dtToday))
            {
                if (sCodeRejet == "MigratedDemandStatus")
                    sStatus = "<span style=\"color:red\">Contacter service exploitation</span>";
                else*/
            if ((!String.IsNullOrWhiteSpace(sCodeRejet) && sCodeRejet != "MigratedDemandStatus") || ((sDateReglement.Trim().Length == 0 || dtDateReglement == null) && dtDateCreation != null && dtDateCreation <= dtToday))
            {
                sStatus = "<span style=\"color:red\">Refusé</span>";
            }
            else if (sDateReglement.Trim().Length > 0 && dtDateReglement != null && dtDateReglement <= dtToday)
                sStatus = "Prélevé";
            else
                sStatus = "A venir";
        }

        return sStatus;
    }
    public static DateTime getTransferDate(string sDateCreation, string sDateReglement)
    {
        DateTime dtToday = DateTime.Parse(DateTime.Now.ToShortDateString());
        DateTime dtOperationDate = new DateTime();
        DateTime dtDateCreation; DateTime dtDateReglement;

        if(sDateReglement != null && DateTime.TryParse(sDateReglement, out dtDateReglement))
            dtOperationDate = dtDateReglement;
        else if(sDateCreation != null && DateTime.TryParse(sDateCreation, out dtDateCreation))
            dtOperationDate = dtDateCreation;

        return dtOperationDate;
    }
    public static string getFormattedTimeFromDate(string sDate)
    {
        string sFormattedTime = "";
        DateTime dt;
        DateTime.TryParse(sDate, out dt);
        //sFormattedDate = dt.ToString("dd MMMM yyyy");
        sFormattedTime = dt.ToString("HH:mm").Replace(':', 'h');
        return sFormattedTime;
    }

    public static string getFormattedDate(string sDate)
    {
        string sFormattedDate = "";
        DateTime dt;
        DateTime.TryParse(sDate, out dt);
        //sFormattedDate = dt.ToString("dd MMMM yyyy");
        sFormattedDate = dt.ToString("dd/MM/yyyy");
        return sFormattedDate;
    }
    public static string getTransferFormattedShortDate(string sDate, char cSeparation)
    {
        if (sDate.Trim().Length > 10)
            sDate = sDate.Trim().Substring(0, 10);

        string sFormattedDate = sDate;
        try
        {

            DateTime dtDate;
            char _cSeparation = '/';

            if (cSeparation.ToString().Trim().Length > 0)
                _cSeparation = cSeparation;

            if (DateTime.TryParse(sDate, out dtDate))
                sFormattedDate = dtDate.ToString("dd" + _cSeparation + "MM" + _cSeparation + "yyyy");
        }
        catch (Exception ex)
        {
        }

        return sFormattedDate;
    }
    public static string getTransferFormattedTime(string sTime, char cSeparation)
    {
        if (sTime.Trim().Length >= 16)
            sTime = sTime.Trim().Substring(11, 5);
        char _cSeparation = 'h';
        if (cSeparation.ToString().Trim().Length > 0)
            _cSeparation = cSeparation;

        if (sTime.Length == 0)
            sTime = "-- " + _cSeparation + " --";
        else if (sTime.Length > 5)
            sTime = sTime.Substring(0, 5).Replace(':', _cSeparation);

        sTime = sTime.Trim().Replace("00:00", "-- " + cSeparation + " --");

        return sTime;
    }
    public static string getTransferAmount(string sAmount, string sTransferType)
    {
        string sFormattedAmount = "";

        return sFormattedAmount;
    }
    public static string getTransferRowColor(string sTransferCodeOpe, string sClass)
    {
        string sClassToReturn = sClass;

        switch (sTransferCodeOpe.Trim().ToUpper())
        {
            case "A2P":
                sClassToReturn = sClass + "-red";
                break;
            case "A3P":
                sClassToReturn = sClass + "-red";
                break;
            default:
                sClassToReturn = sClass;
                break;
        }

        return sClassToReturn;
    }
    public static string getFormattedAmount(string sAmount)
    {
        string sLblAmount = "";
        string sAmountSign = (sAmount.StartsWith("-")) ? "-" : "+";

        // Remove last two zeros
        sAmount = sAmount.Substring(0, sAmount.Length - 2);

        // Add space every 3 numbers
        try
        {
            sAmount = sAmount.Replace("-", "");
            string[] arAmount = sAmount.Split(',');
            int iTmp = int.Parse(arAmount[0]);
            arAmount[0] = String.Format("{0:### ### ### ###}", iTmp).Trim();
            if (arAmount[0].Length == 0)
                arAmount[0] = "0";
            sAmount = (sAmount.StartsWith("-0,") ? "-" : "") + ((arAmount.Length > 1) ? arAmount[0] + "," + arAmount[1].PadRight(2, '0') : arAmount[0]);
        }
        catch (Exception e)
        {
        }

        // Add Euro symbol
        sAmount += " &euro;";

        // Add sign and corresponding colors
        sAmount = sAmountSign + sAmount;
        if (sAmountSign == "+")
        {
            sLblAmount = "<span class='font-green'>" + sAmount + "</span>";
        }
        else
        {
            sLblAmount = "<span class='font-red'>" + sAmount + "</span>";
        }

        return sLblAmount;
    }

    public static DataTable getUserOpeRejectList(SearchCriteria criteria, out int iRowCount)
    {
        SqlConnection conn = null;
        DataTable dt = new DataTable();
        iRowCount = 0;
        bool isOK = false;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Nickel"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("Nickel.P_GetUserRefusedAutorizations", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@sXmlIn", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@sXmlOut", SqlDbType.VarChar, -1);

            cmd.Parameters["@sXmlIn"].Value = new XElement("ALL_XML_IN", new XElement("UserAutorizations",
                                                new XAttribute("UserBankAccount", criteria.sAccountNumber),
                                                new XAttribute("sAuthorizationType", "ALL"),
                                                new XAttribute("PageIndex", "1"),
                                                new XAttribute("PageSize", criteria.iPageSize.ToString()),
                                                new XAttribute("DateFrom", criteria.sDateFrom.ToString()),
                                                new XAttribute("DateTo", criteria.sDateTo.ToString()),
                                                new XAttribute("sDirection", (criteria.sDirection != null && criteria.sDirection.Trim().Length > 0) ? criteria.sDirection.Trim() : "ALL"),
                                                new XAttribute("ForceRefresh", (criteria.bForceRefresh) ? "1" : "0"),
                                                new XAttribute("Culture", "fr-FR"))).ToString();
            cmd.Parameters["@sXmlOut"].Direction = ParameterDirection.Output;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;

            da.Fill(dt);

            string sXmlOut = cmd.Parameters["@sXmlOut"].Value.ToString();
            string sRowCount = tools.GetValueFromXml(sXmlOut, "ALL_XML_OUT/UserAutorizations", "NbResult");

            iRowCount = int.Parse(sRowCount);
        }
        catch (Exception e)
        {
            isOK = false;
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }
    public static DataTable getUserOpeRejectListTreated(DataTable dtUserOpeRejectListBrut, ref int iRowCount)
    {
        return dtUserOpeRejectListBrut;
    }

    public static bool ForceSubscriptionRenew(string sRefCustomer, string sCashierToken)
    {
        SqlConnection conn = null;
        bool bOk = false;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("BankSI.P_GetSubscriptionRenew", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                            new XElement("Fee",
                                                new XAttribute("RefCustomer", sRefCustomer),
                                                new XAttribute("CashierToken", sCashierToken))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if (sXmlOut.Length > 0)
            {
                List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Fee", "RC");

                if (listRC.Count > 0 && listRC[0] == "0")
                    bOk = true;
            }
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return bOk;
    }

    // gestion rejet prélèvement
    public static string getPrelevementImage(string sNumOpe, string sCodeRejet)
    {
        string sImageName = "";
        string sTooltip = "";
        string sOnClick = "";

        string sReturn = "<span ";

        /*if (!String.IsNullOrWhiteSpace(sCodeRejet))
        {
            if (sCodeRejet == "MigratedDemandStatus")
            {
                sTooltip = "migration en cours";
            }
            else
            {*/
        if (!String.IsNullOrWhiteSpace(sCodeRejet) && sCodeRejet != "MigratedDemandStatus")
        {
            { 
                sTooltip = "prélèvement rejeté";
            }
            sImageName = "deny.png";
            sOnClick = "";
        }
        else
        {
            sImageName = "cancel.png";
            sTooltip = "Rejeter le prélèvement";
            sOnClick = " onclick=\"ShowConfirmCancelPrelevement('" + sNumOpe + "');return false;\"";
        }
        sReturn += " class=\"tooltip\" tooltiptext=\"";
        sReturn += sTooltip;
        sReturn += "\" style=\"position:relative;top:-4px\"><img src=\"Styles/Img/";
        sReturn += sImageName;
        sReturn += "\" style=\"height:25px\" alt=\"" + sTooltip + "\"";
        sReturn += " style=\"cursor: pointer\"";
        sReturn += sOnClick;
        sReturn += "/></span>";
/*
        sReturn = "<asp:ImageButton ID=\"imgPrelevement";
        sReturn += sNumOpe;
        sReturn += "\" runat=\"server\" CssClass=\"image-tooltip\" ToolTip=\"";
        sReturn += sTooltip;
        sReturn += "\" src=\"../Style/Img/";
        sReturn += sImageName;
        sReturn += " style=\"cursor: pointer\"";
        sReturn += sOnClick;
        sReturn += " />";
        */
        return sReturn;
    }

    public static bool doCancelPrelevement(string sRefCustomer, string sNumOpe, string sCodeOpe, string sCodeRejet, out string sError, out string sRC)
    {
        bool isOK = false; sError = ""; sRC = "";

        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_CancelPrelevementMoney", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = 80;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@RC", SqlDbType.Int);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                                new XElement("CancelTransfer",
                                                    new XAttribute("RefCustomer", sRefCustomer),
                                                    new XAttribute("InitialRefCustomer", sRefCustomer),
                                                    new XAttribute("NumOpe", sNumOpe),
                                                    new XAttribute("CodeOpe", sCodeOpe),
                                                    new XAttribute("CodeRej", sCodeRejet),
                                                    new XAttribute("Source", "BO")
                                                    )).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
            cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();
            List<string> listString = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/CancelTransfer", "RC");

            if (listString.Count > 0)
                sRC = listString[0];

            if (sRC == "0")
                isOK = true;
            else
            {
                listString = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/CancelTransfer", "ErrorLabel");
                if (listString.Count > 0)
                    sError = listString[0];
                else
                    sError = "Operation échouée";
            }
        }
        catch (Exception ex)
        {
            isOK = false;
            //TEST
            //sRC = "1976";
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return isOK;
    }

    public static bool doCancelPrelevementCobalt(string sRefCustomer, string sIban, string sNumOpe, out string sError, out string sRC)
    {
        bool isOK = false;
        sError = "";
        sRC = "0";
        try
        {
            CobaltApi api = new CobaltApi(ConfigurationManager.AppSettings["Cobalt_Core_URL"].ToString(), ConfigurationManager.AppSettings["Cobalt_Transfer_URL"].ToString(), null, ConfigurationManager.AppSettings["Cobalt_Expiration_URL"].ToString());
            isOK = api.DirectDebit.DeleteDirectDebit(sIban, sNumOpe);

            if (isOK == false)
                sError = "Rejet échoué";
            else
                sError = "Rejet Réussi";
        }
        catch (Exception ex)
        {
            isOK = false;
            //TEST
            //sRC = "1976";
            //sError = ex.Message;
			sError = "Contacter service exploitation<br/>Numéro opération : " + sNumOpe;
        }

        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_CancelPrelevementMoney_Cobalt", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = 80;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@RC", SqlDbType.Int);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                                new XElement("CancelTransfer",
                                                    new XAttribute("RefCustomer", sRefCustomer),
                                                    new XAttribute("NumOpe", sNumOpe),
                                                    new XAttribute("CrCobalt", (isOK) ? "0" : "1977"),
                                                    new XAttribute("Source", "BO")
                                                    )).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
            cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();
            List<string> listString = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/CancelTransfer", "RC");

            if (listString.Count > 0)
                sRC = listString[0];

            if (sRC != "0")
            {
                listString = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/CancelTransfer", "ErrorLabel");
                sError += "<br/>Operation SQL échouée ";
                if (listString.Count > 0)
                    sError += listString[0];
                else
                    sError = " erreur SQL inconnue";
            }
        }
        catch (Exception ex)
        {
            sError += "<br/>Exception SQL " + ex.Message;
            //TEST
            //sRC = "1976";
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }
		
        return isOK;
    }

    // gestion des mandats
    public static DataTable getUserMandateListCobalt(SearchCriteria criteria, out int iRowCount)
    {
        string sIban = criteria.sIBAN;
        DataTable dt = new DataTable();
        dt.Columns.Add("row");
        dt.Columns.Add("rumId");
        dt.Columns.Add("sepaRumId");
        dt.Columns.Add("creditorName");
        dt.Columns.Add("creditorIban");
        dt.Columns.Add("creditorBic");
        dt.Columns.Add("signatureDate", typeof(DateTime));
        dt.Columns.Add("ics");
        dt.Columns.Add("activationdateDate", typeof(DateTime));
        dt.Columns.Add("deactivationdateDate", typeof(DateTime));
        dt.Columns.Add("rejectDate", typeof(DateTime));
        dt.Columns.Add("MontantEchange", typeof(Decimal));
        dt.Columns.Add("dateDernierPrelevement", typeof(DateTime));
        dt.Columns.Add("rumStatus");

        CobaltApi api = new CobaltApi(ConfigurationManager.AppSettings["Cobalt_Core_URL"].ToString(), ConfigurationManager.AppSettings["Cobalt_Transfer_URL"].ToString(), null, ConfigurationManager.AppSettings["Cobalt_Expiration_URL"].ToString());
        List<ApiCobalt.Rums.Rum> listTransaction = api.Rum.GetRums(api.Account.GetAccount(sIban).ClientId, sIban);
        List<ApiCobalt.DirectDebits.DirectDebit> listPrelevement = api.DirectDebit.GetDirectDebits(sIban);
        listPrelevement.Sort(delegate(ApiCobalt.DirectDebits.DirectDebit x, ApiCobalt.DirectDebits.DirectDebit y)
        {
            int iResultCompare = x.Rum.CompareTo(y.Rum);
            if (iResultCompare == 0)
            {
                iResultCompare = y.CollectionDate.CompareTo(x.CollectionDate);
            }
            return iResultCompare;
        });
        for (int i = 0; i < listTransaction.Count; i++)
        {
            double dAmount = 0.00;
            DateTime dtDateDernierPrelevement = new DateTime(2000, 1, 1);

            for (int idxPrelevement = 0; idxPrelevement < listPrelevement.Count; idxPrelevement++)
            {
                if (listPrelevement[idxPrelevement].Rum.CompareTo(listTransaction[i].RumId) == 0)
                {
                    if (listPrelevement[idxPrelevement].CollectionDate.CompareTo(dtDateDernierPrelevement) > 0)
                    {
                        dtDateDernierPrelevement = listPrelevement[idxPrelevement].CollectionDate;
                        dAmount = listPrelevement[idxPrelevement].Amount / 100d;
                        break;
                    }
                }
            }

            ApiCobalt.Rums.Rum.StatusRecipient xStatus = listTransaction[i].Status;


            dt.Rows.Add(new object[] { i,
                listTransaction[i].RumId,
                listTransaction[i].SepaRumId,
                listTransaction[i].CreditorName,
                listTransaction[i].CreditorIban,
                listTransaction[i].CreditorName,
                listTransaction[i].SignatureDate,
                listTransaction[i].Ics,
                listTransaction[i].ActivationDate,
                listTransaction[i].DeactivationDate,
                listTransaction[i].RejectDate,
                dAmount.ToString() + (!dAmount.ToString().Replace('.', ',').Contains(",") ? ",00" : "00"),
                dtDateDernierPrelevement.CompareTo(listTransaction[i].SignatureDate) < 0 ? listTransaction[i].SignatureDate : dtDateDernierPrelevement,
                xStatus.Key
            });
        }

        iRowCount = dt.Rows.Count;
        
        return dt;
    }

    public static bool doActivateMandate(string sRefCustomer, string sIbanClient, string sRumId, string sIbanDo, out string sError, out string sRC)
    {
        bool isOK = false; sError = ""; sRC = "";

        CobaltApi api = new CobaltApi(ConfigurationManager.AppSettings["Cobalt_Core_URL"].ToString(), ConfigurationManager.AppSettings["Cobalt_Transfer_URL"].ToString(), null, ConfigurationManager.AppSettings["Cobalt_Expiration_URL"].ToString());

        try
        {
            isOK = api.Rum.ActivateRum(api.Account.GetAccount(sIbanClient).ClientId, sIbanClient, sRumId);
            if (isOK)
                sError = "Activation du mandat réussie";
            else
                sError = "Activation du mandat échouée";
        }
        catch (Exception ex)
        {
            isOK = false;
            //sError = ex.Message;
			sError = "Contacter service exploitation<br/>Numéro opération : " + sRumId;
        }

        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_MandateTreatmentCobalt", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@RC", SqlDbType.Int);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                                new XElement("MandateTreatment",
                                                    new XAttribute("RefCustomer", sRefCustomer),
                                                    new XAttribute("RumId", sRumId),
                                                    new XAttribute("CodeOpe", "Activation mandat"),
                                                    new XAttribute("idCreancier", sIbanDo),
                                                    new XAttribute("CrCobalt", (isOK) ? "0" : "1977"),
                                                    new XAttribute("Source", "BO")
                                                    )).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
            cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();
            List<String> sListRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/MandateTreatment", "RC");

            if (sListRC.Count > 0 && sListRC[0] != "0")
            {
                List<string> sListErrorLabel = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/MandateTreatment", "ErrorLabel");
                sError += "<br/>Enregistrement SQL KO " + ((sListErrorLabel.Count > 0) ? sListErrorLabel[0] : "erreur inconnue");
            }
        }
        catch (Exception ex)
        {
            //TEST
            //sRC = "1976";
            sError += "<br/>Exception enregistrement SQL KO " + ex.Message;
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return isOK;
    }

    public static bool doSuspendMandate(string sRefCustomer, string sIbanClient, string sRumId, string sIbanDo, out string sError, out string sRC)
    {
        bool isOK = false; sError = ""; sRC = "";

        CobaltApi api = new CobaltApi(ConfigurationManager.AppSettings["Cobalt_Core_URL"].ToString(), ConfigurationManager.AppSettings["Cobalt_Transfer_URL"].ToString(), null, ConfigurationManager.AppSettings["Cobalt_Expiration_URL"].ToString());

        try
        {
            isOK = api.Rum.DeactivateRum(api.Account.GetAccount(sIbanClient).ClientId, sIbanClient, sRumId);
            if (isOK)
                sError = "Suspension du mandat réussie";
            else
                sError = "Suspension du mandat échouée";
        }
        catch (Exception ex)
        {
            isOK = false;
            //sError = ex.Message;
			sError = "Contacter service exploitation<br/>Numéro opération : " + sRumId;
        }

        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_MandateTreatment", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@RC", SqlDbType.Int);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                                new XElement("P_MandateTreatmentCobalt",
                                                    new XAttribute("RefCustomer", sRefCustomer),
                                                    new XAttribute("RumId", sRumId),
                                                    new XAttribute("CodeOpe", "Suspension mandat"),
                                                    new XAttribute("idCreancier", sIbanDo),
                                                    new XAttribute("CrCobalt", (isOK) ? "0" : "1977"),
                                                    new XAttribute("Source", "BO")
                                                    )).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
            cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();
            List<String> sListRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/MandateTreatment", "RC");

            if (sListRC.Count > 0 && sListRC[0] != "0")
            {
                List<string> sListErrorLabel = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/MandateTreatment", "ErrorLabel");
                sError += "<br/>Enregistrement SQL KO " + ((sListErrorLabel.Count > 0) ? sListErrorLabel[0] : "erreur inconnue");
            }
        }
        catch (Exception ex)
        {
            //TEST
            //sRC = "1976";
            sError += "<br/>Exception enregistrement SQL KO " + ex.Message;
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return isOK;
    }

    public static bool doBlockMandate(string sRefCustomer, string sIbanClient, string sRumId, string sIbanDo, out string sError, out string sRC)
    {
        bool isOK = false; sError = ""; sRC = "";

        CobaltApi api = new CobaltApi(ConfigurationManager.AppSettings["Cobalt_Core_URL"].ToString(), ConfigurationManager.AppSettings["Cobalt_Transfer_URL"].ToString(), null, ConfigurationManager.AppSettings["Cobalt_Expiration_URL"].ToString());

        try
        {
            isOK = api.Rum.RejectRum(api.Account.GetAccount(sIbanClient).ClientId, sIbanClient, sRumId);
            if (isOK)
                sError = "Blocage du mandat réussi";
            else
                sError = "Blocage du mandat échoué";
        }
        catch (Exception ex)
        {
            isOK = false;
            //sError = ex.Message;
			sError = "Contacter service exploitation<br/>Numéro opération : " + sRumId;
        }

        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_MandateTreatmentCobalt", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@RC", SqlDbType.Int);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                                new XElement("MandateTreatment",
                                                    new XAttribute("RefCustomer", sRefCustomer),
                                                    new XAttribute("RumId", sRumId),
                                                    new XAttribute("CodeOpe", "Blocage mandat"),
                                                    new XAttribute("idCreancier", sIbanDo),
                                                    new XAttribute("CrCobalt", (isOK) ? "0" : "1977"),
                                                    new XAttribute("Source", "BO")
                                                    )).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
            cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();
            List<String> sListRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/MandateTreatment", "RC");

            if (sListRC.Count > 0 && sListRC[0] != "0")
            {
                List<string> sListErrorLabel = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/MandateTreatment", "ErrorLabel");
                sError += "<br/>Enregistrement SQL KO " + ((sListErrorLabel.Count > 0) ? sListErrorLabel[0] : "erreur inconnue");
            }
        }
        catch (Exception ex)
        {
            //TEST
            //sRC = "1976";
            sError += "<br/>Exception enregistrement SQL KO " + ex.Message;
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return isOK;
    }

    public bool bCancelMinuteOpe(string sIban, string sNumOpe, string sDateOpe, string sError)
    {
        bool bOk = false;
        CobaltApi api = new CobaltApi(ConfigurationManager.AppSettings["Cobalt_Core_URL"].ToString(), ConfigurationManager.AppSettings["Cobalt_Transfer_URL"].ToString(), null, ConfigurationManager.AppSettings["Cobalt_Expiration_URL"].ToString());
        DateTime dtDateOpe = DateTime.Now;
        sError = "";
        try
        {
            dtDateOpe = DateTime.ParseExact(sDateOpe, "yyyy-MM-ddTHH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
            //    api.Transaction
            bOk = api.Expiration.ExpireOperation(sIban, dtDateOpe, sNumOpe);
        }
        catch (Exception e)
        {
            bOk = false;
            //sError = e.Message;
			sError = "Contacter service exploitation<br/>Numéro opération : " + sNumOpe;
        }

        // enregsitrement en base
        SqlConnection conn = null;
        authentication auth = authentication.GetCurrent();

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_AddLogAnnulOpMinute", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = 80;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@RC", SqlDbType.Int);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                                new XElement("ExpirationOp",
                                                    new XAttribute("IBAN", sIban),
                                                    new XAttribute("NumOpe", sNumOpe),
                                                    new XAttribute("DateOperation", dtDateOpe.ToString()),
                                                    new XAttribute("bEnvoiCobalt", (bOk == true) ? "1" : "0"),
                                                    new XAttribute("TOKEN", auth.sToken))).ToString(); ;
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
            cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();
            List<string> listString = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Result", "RC");

            if (listString.Count > 0)
            {
                if (listString[0] == "0")
                {
                    bOk = true;
                }
                else
                {
                    listString = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Result", "ErrorLabel");
                    if (listString.Count > 0)
                        sError += listString[0];
                    else
                        sError += " Operation échouée";
                }
            }
        }
        catch (Exception ex)
        {
            bOk = false;
            sError += " SQL exception" + ex.Message;
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }


        return bOk;
    }

}
