﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Xml.Linq;


[Serializable]
public class Tracfin
{
    public Tracfin()
    {
        
    }

    public static string SearchStatement(int iRefCustomer, int iRefCashier, string sCashierToken, string sReference, string sDateFrom, string sDateTo)
    {
        string sXmlOut = "";
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("TracFin.P_GetStatement", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);

            XElement xml = new XElement("TRACFIN_STATEMENT",
                                new XAttribute("CashierToken", sCashierToken));
            if (iRefCustomer != 0)
                xml.Add(new XAttribute("RefCustomer", iRefCustomer));
            if (iRefCashier != 0)
                xml.Add(new XAttribute("RefCashierTarget", iRefCashier));
            if (!String.IsNullOrWhiteSpace(sDateFrom))
                xml.Add(new XAttribute("DateFrom", sDateFrom));
            if (!String.IsNullOrWhiteSpace(sDateTo))
                xml.Add(new XAttribute("DateTo", sDateTo));
            if (!String.IsNullOrWhiteSpace(sReference))
                xml.Add(new XAttribute("RefStatementInt", sReference));
            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN", xml).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            sXmlOut = cmd.Parameters["@OUT"].Value.ToString();
        }
        catch (Exception e)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return sXmlOut;
    }

    public static string SearchStatement(int iRefCustomer, int iRefCashier, string sCashierToken)
    {
        return SearchStatement(iRefCustomer, iRefCashier, sCashierToken, null, null, null);
    }

    public static string GetStatementDetails(int iRefStatement)
    {
        string sXmlOut = "";
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("TracFin.P_GetStatementDetails", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                                new XElement("TRACFIN_STATEMENT",
                                                    new XAttribute("RefStatement", iRefStatement),
                                                    new XAttribute("CashierToken", authentication.GetCurrent().sToken))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            sXmlOut = cmd.Parameters["@OUT"].Value.ToString();
        }
        catch (Exception e)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return sXmlOut;
    }
}