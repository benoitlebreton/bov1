﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Hosting;
using System.Xml.Linq;
using XMLMethodLibrary;
using libphonenumber;
using System.Globalization;
using System.Web;
using ApiCobalt;
using System.Web.UI.WebControls;
using System.Linq;
using System.Web.Security;


[Serializable]
public class tools
{
    public static string GetMimeType(string extension)
    {
        if (extension == null)
            throw new ArgumentNullException("extension");

        if (extension.StartsWith("."))
            extension = extension.Substring(1);


        switch (extension.ToLower())
        {
            #region Big freaking list of mime types
            case "323": return "text/h323";
            case "3g2": return "video/3gpp2";
            case "3gp": return "video/3gpp";
            case "3gp2": return "video/3gpp2";
            case "3gpp": return "video/3gpp";
            case "7z": return "application/x-7z-compressed";
            case "aa": return "audio/audible";
            case "aac": return "audio/aac";
            case "aaf": return "application/octet-stream";
            case "aax": return "audio/vnd.audible.aax";
            case "ac3": return "audio/ac3";
            case "aca": return "application/octet-stream";
            case "accda": return "application/msaccess.addin";
            case "accdb": return "application/msaccess";
            case "accdc": return "application/msaccess.cab";
            case "accde": return "application/msaccess";
            case "accdr": return "application/msaccess.runtime";
            case "accdt": return "application/msaccess";
            case "accdw": return "application/msaccess.webapplication";
            case "accft": return "application/msaccess.ftemplate";
            case "acx": return "application/internet-property-stream";
            case "addin": return "text/xml";
            case "ade": return "application/msaccess";
            case "adobebridge": return "application/x-bridge-url";
            case "adp": return "application/msaccess";
            case "adt": return "audio/vnd.dlna.adts";
            case "adts": return "audio/aac";
            case "afm": return "application/octet-stream";
            case "ai": return "application/postscript";
            case "aif": return "audio/x-aiff";
            case "aifc": return "audio/aiff";
            case "aiff": return "audio/aiff";
            case "air": return "application/vnd.adobe.air-application-installer-package+zip";
            case "amc": return "application/x-mpeg";
            case "application": return "application/x-ms-application";
            case "art": return "image/x-jg";
            case "asa": return "application/xml";
            case "asax": return "application/xml";
            case "ascx": return "application/xml";
            case "asd": return "application/octet-stream";
            case "asf": return "video/x-ms-asf";
            case "ashx": return "application/xml";
            case "asi": return "application/octet-stream";
            case "asm": return "text/plain";
            case "asmx": return "application/xml";
            case "aspx": return "application/xml";
            case "asr": return "video/x-ms-asf";
            case "asx": return "video/x-ms-asf";
            case "atom": return "application/atom+xml";
            case "au": return "audio/basic";
            case "avi": return "video/x-msvideo";
            case "axs": return "application/olescript";
            case "bas": return "text/plain";
            case "bcpio": return "application/x-bcpio";
            case "bin": return "application/octet-stream";
            case "bmp": return "image/bmp";
            case "c": return "text/plain";
            case "cab": return "application/octet-stream";
            case "caf": return "audio/x-caf";
            case "calx": return "application/vnd.ms-office.calx";
            case "cat": return "application/vnd.ms-pki.seccat";
            case "cc": return "text/plain";
            case "cd": return "text/plain";
            case "cdda": return "audio/aiff";
            case "cdf": return "application/x-cdf";
            case "cer": return "application/x-x509-ca-cert";
            case "chm": return "application/octet-stream";
            case "class": return "application/x-java-applet";
            case "clp": return "application/x-msclip";
            case "cmx": return "image/x-cmx";
            case "cnf": return "text/plain";
            case "cod": return "image/cis-cod";
            case "config": return "application/xml";
            case "contact": return "text/x-ms-contact";
            case "coverage": return "application/xml";
            case "cpio": return "application/x-cpio";
            case "cpp": return "text/plain";
            case "crd": return "application/x-mscardfile";
            case "crl": return "application/pkix-crl";
            case "crt": return "application/x-x509-ca-cert";
            case "cs": return "text/plain";
            case "csdproj": return "text/plain";
            case "csh": return "application/x-csh";
            case "csproj": return "text/plain";
            case "css": return "text/css";
            case "csv": return "text/csv";
            case "cur": return "application/octet-stream";
            case "cxx": return "text/plain";
            case "dat": return "application/octet-stream";
            case "datasource": return "application/xml";
            case "dbproj": return "text/plain";
            case "dcr": return "application/x-director";
            case "def": return "text/plain";
            case "deploy": return "application/octet-stream";
            case "der": return "application/x-x509-ca-cert";
            case "dgml": return "application/xml";
            case "dib": return "image/bmp";
            case "dif": return "video/x-dv";
            case "dir": return "application/x-director";
            case "disco": return "text/xml";
            case "dll": return "application/x-msdownload";
            case "dll.config": return "text/xml";
            case "dlm": return "text/dlm";
            case "doc": return "application/msword";
            case "docm": return "application/vnd.ms-word.document.macroenabled.12";
            case "docx": return "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
            case "dot": return "application/msword";
            case "dotm": return "application/vnd.ms-word.template.macroenabled.12";
            case "dotx": return "application/vnd.openxmlformats-officedocument.wordprocessingml.template";
            case "dsp": return "application/octet-stream";
            case "dsw": return "text/plain";
            case "dtd": return "text/xml";
            case "dtsconfig": return "text/xml";
            case "dv": return "video/x-dv";
            case "dvi": return "application/x-dvi";
            case "dwf": return "drawing/x-dwf";
            case "dwp": return "application/octet-stream";
            case "dxr": return "application/x-director";
            case "eml": return "message/rfc822";
            case "emz": return "application/octet-stream";
            case "eot": return "application/octet-stream";
            case "eps": return "application/postscript";
            case "etl": return "application/etl";
            case "etx": return "text/x-setext";
            case "evy": return "application/envoy";
            case "exe": return "application/octet-stream";
            case "exe.config": return "text/xml";
            case "fdf": return "application/vnd.fdf";
            case "fif": return "application/fractals";
            case "filters": return "application/xml";
            case "fla": return "application/octet-stream";
            case "flr": return "x-world/x-vrml";
            case "flv": return "video/x-flv";
            case "fsscript": return "application/fsharp-script";
            case "fsx": return "application/fsharp-script";
            case "generictest": return "application/xml";
            case "gif": return "image/gif";
            case "group": return "text/x-ms-group";
            case "gsm": return "audio/x-gsm";
            case "gtar": return "application/x-gtar";
            case "gz": return "application/x-gzip";
            case "h": return "text/plain";
            case "hdf": return "application/x-hdf";
            case "hdml": return "text/x-hdml";
            case "hhc": return "application/x-oleobject";
            case "hhk": return "application/octet-stream";
            case "hhp": return "application/octet-stream";
            case "hlp": return "application/winhlp";
            case "hpp": return "text/plain";
            case "hqx": return "application/mac-binhex40";
            case "hta": return "application/hta";
            case "htc": return "text/x-component";
            case "htm": return "text/html";
            case "html": return "text/html";
            case "htt": return "text/webviewhtml";
            case "hxa": return "application/xml";
            case "hxc": return "application/xml";
            case "hxd": return "application/octet-stream";
            case "hxe": return "application/xml";
            case "hxf": return "application/xml";
            case "hxh": return "application/octet-stream";
            case "hxi": return "application/octet-stream";
            case "hxk": return "application/xml";
            case "hxq": return "application/octet-stream";
            case "hxr": return "application/octet-stream";
            case "hxs": return "application/octet-stream";
            case "hxt": return "text/html";
            case "hxv": return "application/xml";
            case "hxw": return "application/octet-stream";
            case "hxx": return "text/plain";
            case "i": return "text/plain";
            case "ico": return "image/x-icon";
            case "ics": return "application/octet-stream";
            case "idl": return "text/plain";
            case "ief": return "image/ief";
            case "iii": return "application/x-iphone";
            case "inc": return "text/plain";
            case "inf": return "application/octet-stream";
            case "inl": return "text/plain";
            case "ins": return "application/x-internet-signup";
            case "ipa": return "application/x-itunes-ipa";
            case "ipg": return "application/x-itunes-ipg";
            case "ipproj": return "text/plain";
            case "ipsw": return "application/x-itunes-ipsw";
            case "iqy": return "text/x-ms-iqy";
            case "isp": return "application/x-internet-signup";
            case "ite": return "application/x-itunes-ite";
            case "itlp": return "application/x-itunes-itlp";
            case "itms": return "application/x-itunes-itms";
            case "itpc": return "application/x-itunes-itpc";
            case "ivf": return "video/x-ivf";
            case "jar": return "application/java-archive";
            case "java": return "application/octet-stream";
            case "jck": return "application/liquidmotion";
            case "jcz": return "application/liquidmotion";
            case "jfif": return "image/pjpeg";
            case "jnlp": return "application/x-java-jnlp-file";
            case "jpb": return "application/octet-stream";
            case "jpe": return "image/jpeg";
            case "jpeg": return "image/jpeg";
            case "jpg": return "image/jpeg";
            case "js": return "application/x-javascript";
            case "jsx": return "text/jscript";
            case "jsxbin": return "text/plain";
            case "latex": return "application/x-latex";
            case "library-ms": return "application/windows-library+xml";
            case "lit": return "application/x-ms-reader";
            case "loadtest": return "application/xml";
            case "lpk": return "application/octet-stream";
            case "lsf": return "video/x-la-asf";
            case "lst": return "text/plain";
            case "lsx": return "video/x-la-asf";
            case "lzh": return "application/octet-stream";
            case "m13": return "application/x-msmediaview";
            case "m14": return "application/x-msmediaview";
            case "m1v": return "video/mpeg";
            case "m2t": return "video/vnd.dlna.mpeg-tts";
            case "m2ts": return "video/vnd.dlna.mpeg-tts";
            case "m2v": return "video/mpeg";
            case "m3u": return "audio/x-mpegurl";
            case "m3u8": return "audio/x-mpegurl";
            case "m4a": return "audio/m4a";
            case "m4b": return "audio/m4b";
            case "m4p": return "audio/m4p";
            case "m4r": return "audio/x-m4r";
            case "m4v": return "video/x-m4v";
            case "mac": return "image/x-macpaint";
            case "mak": return "text/plain";
            case "man": return "application/x-troff-man";
            case "manifest": return "application/x-ms-manifest";
            case "map": return "text/plain";
            case "master": return "application/xml";
            case "mda": return "application/msaccess";
            case "mdb": return "application/x-msaccess";
            case "mde": return "application/msaccess";
            case "mdp": return "application/octet-stream";
            case "me": return "application/x-troff-me";
            case "mfp": return "application/x-shockwave-flash";
            case "mht": return "message/rfc822";
            case "mhtml": return "message/rfc822";
            case "mid": return "audio/mid";
            case "midi": return "audio/mid";
            case "mix": return "application/octet-stream";
            case "mk": return "text/plain";
            case "mmf": return "application/x-smaf";
            case "mno": return "text/xml";
            case "mny": return "application/x-msmoney";
            case "mod": return "video/mpeg";
            case "mov": return "video/quicktime";
            case "movie": return "video/x-sgi-movie";
            case "mp2": return "video/mpeg";
            case "mp2v": return "video/mpeg";
            case "mp3": return "audio/mpeg";
            case "mp4": return "video/mp4";
            case "mp4v": return "video/mp4";
            case "mpa": return "video/mpeg";
            case "mpe": return "video/mpeg";
            case "mpeg": return "video/mpeg";
            case "mpf": return "application/vnd.ms-mediapackage";
            case "mpg": return "video/mpeg";
            case "mpp": return "application/vnd.ms-project";
            case "mpv2": return "video/mpeg";
            case "mqv": return "video/quicktime";
            case "ms": return "application/x-troff-ms";
            case "msi": return "application/octet-stream";
            case "mso": return "application/octet-stream";
            case "mts": return "video/vnd.dlna.mpeg-tts";
            case "mtx": return "application/xml";
            case "mvb": return "application/x-msmediaview";
            case "mvc": return "application/x-miva-compiled";
            case "mxp": return "application/x-mmxp";
            case "nc": return "application/x-netcdf";
            case "nsc": return "video/x-ms-asf";
            case "nws": return "message/rfc822";
            case "ocx": return "application/octet-stream";
            case "oda": return "application/oda";
            case "odc": return "text/x-ms-odc";
            case "odh": return "text/plain";
            case "odl": return "text/plain";
            case "odp": return "application/vnd.oasis.opendocument.presentation";
            case "ods": return "application/oleobject";
            case "odt": return "application/vnd.oasis.opendocument.text";
            case "one": return "application/onenote";
            case "onea": return "application/onenote";
            case "onepkg": return "application/onenote";
            case "onetmp": return "application/onenote";
            case "onetoc": return "application/onenote";
            case "onetoc2": return "application/onenote";
            case "orderedtest": return "application/xml";
            case "osdx": return "application/opensearchdescription+xml";
            case "p10": return "application/pkcs10";
            case "p12": return "application/x-pkcs12";
            case "p7b": return "application/x-pkcs7-certificates";
            case "p7c": return "application/pkcs7-mime";
            case "p7m": return "application/pkcs7-mime";
            case "p7r": return "application/x-pkcs7-certreqresp";
            case "p7s": return "application/pkcs7-signature";
            case "pbm": return "image/x-portable-bitmap";
            case "pcast": return "application/x-podcast";
            case "pct": return "image/pict";
            case "pcx": return "application/octet-stream";
            case "pcz": return "application/octet-stream";
            case "pdf": return "application/pdf";
            case "pfb": return "application/octet-stream";
            case "pfm": return "application/octet-stream";
            case "pfx": return "application/x-pkcs12";
            case "pgm": return "image/x-portable-graymap";
            case "pic": return "image/pict";
            case "pict": return "image/pict";
            case "pkgdef": return "text/plain";
            case "pkgundef": return "text/plain";
            case "pko": return "application/vnd.ms-pki.pko";
            case "pls": return "audio/scpls";
            case "pma": return "application/x-perfmon";
            case "pmc": return "application/x-perfmon";
            case "pml": return "application/x-perfmon";
            case "pmr": return "application/x-perfmon";
            case "pmw": return "application/x-perfmon";
            case "png": return "image/png";
            case "pnm": return "image/x-portable-anymap";
            case "pnt": return "image/x-macpaint";
            case "pntg": return "image/x-macpaint";
            case "pnz": return "image/png";
            case "pot": return "application/vnd.ms-powerpoint";
            case "potm": return "application/vnd.ms-powerpoint.template.macroenabled.12";
            case "potx": return "application/vnd.openxmlformats-officedocument.presentationml.template";
            case "ppa": return "application/vnd.ms-powerpoint";
            case "ppam": return "application/vnd.ms-powerpoint.addin.macroenabled.12";
            case "ppm": return "image/x-portable-pixmap";
            case "pps": return "application/vnd.ms-powerpoint";
            case "ppsm": return "application/vnd.ms-powerpoint.slideshow.macroenabled.12";
            case "ppsx": return "application/vnd.openxmlformats-officedocument.presentationml.slideshow";
            case "ppt": return "application/vnd.ms-powerpoint";
            case "pptm": return "application/vnd.ms-powerpoint.presentation.macroenabled.12";
            case "pptx": return "application/vnd.openxmlformats-officedocument.presentationml.presentation";
            case "prf": return "application/pics-rules";
            case "prm": return "application/octet-stream";
            case "prx": return "application/octet-stream";
            case "ps": return "application/postscript";
            case "psc1": return "application/powershell";
            case "psd": return "application/octet-stream";
            case "psess": return "application/xml";
            case "psm": return "application/octet-stream";
            case "psp": return "application/octet-stream";
            case "pub": return "application/x-mspublisher";
            case "pwz": return "application/vnd.ms-powerpoint";
            case "qht": return "text/x-html-insertion";
            case "qhtm": return "text/x-html-insertion";
            case "qt": return "video/quicktime";
            case "qti": return "image/x-quicktime";
            case "qtif": return "image/x-quicktime";
            case "qtl": return "application/x-quicktimeplayer";
            case "qxd": return "application/octet-stream";
            case "ra": return "audio/x-pn-realaudio";
            case "ram": return "audio/x-pn-realaudio";
            case "rar": return "application/octet-stream";
            case "ras": return "image/x-cmu-raster";
            case "rat": return "application/rat-file";
            case "rc": return "text/plain";
            case "rc2": return "text/plain";
            case "rct": return "text/plain";
            case "rdlc": return "application/xml";
            case "resx": return "application/xml";
            case "rf": return "image/vnd.rn-realflash";
            case "rgb": return "image/x-rgb";
            case "rgs": return "text/plain";
            case "rm": return "application/vnd.rn-realmedia";
            case "rmi": return "audio/mid";
            case "rmp": return "application/vnd.rn-rn_music_package";
            case "roff": return "application/x-troff";
            case "rpm": return "audio/x-pn-realaudio-plugin";
            case "rqy": return "text/x-ms-rqy";
            case "rtf": return "application/rtf";
            case "rtx": return "text/richtext";
            case "ruleset": return "application/xml";
            case "s": return "text/plain";
            case "safariextz": return "application/x-safari-safariextz";
            case "scd": return "application/x-msschedule";
            case "sct": return "text/scriptlet";
            case "sd2": return "audio/x-sd2";
            case "sdp": return "application/sdp";
            case "sea": return "application/octet-stream";
            case "searchconnector-ms": return "application/windows-search-connector+xml";
            case "setpay": return "application/set-payment-initiation";
            case "setreg": return "application/set-registration-initiation";
            case "settings": return "application/xml";
            case "sgimb": return "application/x-sgimb";
            case "sgml": return "text/sgml";
            case "sh": return "application/x-sh";
            case "shar": return "application/x-shar";
            case "shtml": return "text/html";
            case "sit": return "application/x-stuffit";
            case "sitemap": return "application/xml";
            case "skin": return "application/xml";
            case "sldm": return "application/vnd.ms-powerpoint.slide.macroenabled.12";
            case "sldx": return "application/vnd.openxmlformats-officedocument.presentationml.slide";
            case "slk": return "application/vnd.ms-excel";
            case "sln": return "text/plain";
            case "slupkg-ms": return "application/x-ms-license";
            case "smd": return "audio/x-smd";
            case "smi": return "application/octet-stream";
            case "smx": return "audio/x-smd";
            case "smz": return "audio/x-smd";
            case "snd": return "audio/basic";
            case "snippet": return "application/xml";
            case "snp": return "application/octet-stream";
            case "sol": return "text/plain";
            case "sor": return "text/plain";
            case "spc": return "application/x-pkcs7-certificates";
            case "spl": return "application/futuresplash";
            case "src": return "application/x-wais-source";
            case "srf": return "text/plain";
            case "ssisdeploymentmanifest": return "text/xml";
            case "ssm": return "application/streamingmedia";
            case "sst": return "application/vnd.ms-pki.certstore";
            case "stl": return "application/vnd.ms-pki.stl";
            case "sv4cpio": return "application/x-sv4cpio";
            case "sv4crc": return "application/x-sv4crc";
            case "svc": return "application/xml";
            case "swf": return "application/x-shockwave-flash";
            case "t": return "application/x-troff";
            case "tar": return "application/x-tar";
            case "tcl": return "application/x-tcl";
            case "testrunconfig": return "application/xml";
            case "testsettings": return "application/xml";
            case "tex": return "application/x-tex";
            case "texi": return "application/x-texinfo";
            case "texinfo": return "application/x-texinfo";
            case "tgz": return "application/x-compressed";
            case "thmx": return "application/vnd.ms-officetheme";
            case "thn": return "application/octet-stream";
            case "tif": return "image/tiff";
            case "tiff": return "image/tiff";
            case "tlh": return "text/plain";
            case "tli": return "text/plain";
            case "toc": return "application/octet-stream";
            case "tr": return "application/x-troff";
            case "trm": return "application/x-msterminal";
            case "trx": return "application/xml";
            case "ts": return "video/vnd.dlna.mpeg-tts";
            case "tsv": return "text/tab-separated-values";
            case "ttf": return "application/octet-stream";
            case "tts": return "video/vnd.dlna.mpeg-tts";
            case "txt": return "text/plain";
            case "u32": return "application/octet-stream";
            case "uls": return "text/iuls";
            case "user": return "text/plain";
            case "ustar": return "application/x-ustar";
            case "vb": return "text/plain";
            case "vbdproj": return "text/plain";
            case "vbk": return "video/mpeg";
            case "vbproj": return "text/plain";
            case "vbs": return "text/vbscript";
            case "vcf": return "text/x-vcard";
            case "vcproj": return "application/xml";
            case "vcs": return "text/plain";
            case "vcxproj": return "application/xml";
            case "vddproj": return "text/plain";
            case "vdp": return "text/plain";
            case "vdproj": return "text/plain";
            case "vdx": return "application/vnd.ms-visio.viewer";
            case "vml": return "text/xml";
            case "vscontent": return "application/xml";
            case "vsct": return "text/xml";
            case "vsd": return "application/vnd.visio";
            case "vsi": return "application/ms-vsi";
            case "vsix": return "application/vsix";
            case "vsixlangpack": return "text/xml";
            case "vsixmanifest": return "text/xml";
            case "vsmdi": return "application/xml";
            case "vspscc": return "text/plain";
            case "vss": return "application/vnd.visio";
            case "vsscc": return "text/plain";
            case "vssettings": return "text/xml";
            case "vssscc": return "text/plain";
            case "vst": return "application/vnd.visio";
            case "vstemplate": return "text/xml";
            case "vsto": return "application/x-ms-vsto";
            case "vsw": return "application/vnd.visio";
            case "vsx": return "application/vnd.visio";
            case "vtx": return "application/vnd.visio";
            case "wav": return "audio/wav";
            case "wave": return "audio/wav";
            case "wax": return "audio/x-ms-wax";
            case "wbk": return "application/msword";
            case "wbmp": return "image/vnd.wap.wbmp";
            case "wcm": return "application/vnd.ms-works";
            case "wdb": return "application/vnd.ms-works";
            case "wdp": return "image/vnd.ms-photo";
            case "webarchive": return "application/x-safari-webarchive";
            case "webtest": return "application/xml";
            case "wiq": return "application/xml";
            case "wiz": return "application/msword";
            case "wks": return "application/vnd.ms-works";
            case "wlmp": return "application/wlmoviemaker";
            case "wlpginstall": return "application/x-wlpg-detect";
            case "wlpginstall3": return "application/x-wlpg3-detect";
            case "wm": return "video/x-ms-wm";
            case "wma": return "audio/x-ms-wma";
            case "wmd": return "application/x-ms-wmd";
            case "wmf": return "application/x-msmetafile";
            case "wml": return "text/vnd.wap.wml";
            case "wmlc": return "application/vnd.wap.wmlc";
            case "wmls": return "text/vnd.wap.wmlscript";
            case "wmlsc": return "application/vnd.wap.wmlscriptc";
            case "wmp": return "video/x-ms-wmp";
            case "wmv": return "video/x-ms-wmv";
            case "wmx": return "video/x-ms-wmx";
            case "wmz": return "application/x-ms-wmz";
            case "wpl": return "application/vnd.ms-wpl";
            case "wps": return "application/vnd.ms-works";
            case "wri": return "application/x-mswrite";
            case "wrl": return "x-world/x-vrml";
            case "wrz": return "x-world/x-vrml";
            case "wsc": return "text/scriptlet";
            case "wsdl": return "text/xml";
            case "wvx": return "video/x-ms-wvx";
            case "x": return "application/directx";
            case "xaf": return "x-world/x-vrml";
            case "xaml": return "application/xaml+xml";
            case "xap": return "application/x-silverlight-app";
            case "xbap": return "application/x-ms-xbap";
            case "xbm": return "image/x-xbitmap";
            case "xdr": return "text/plain";
            case "xht": return "application/xhtml+xml";
            case "xhtml": return "application/xhtml+xml";
            case "xla": return "application/vnd.ms-excel";
            case "xlam": return "application/vnd.ms-excel.addin.macroenabled.12";
            case "xlc": return "application/vnd.ms-excel";
            case "xld": return "application/vnd.ms-excel";
            case "xlk": return "application/vnd.ms-excel";
            case "xll": return "application/vnd.ms-excel";
            case "xlm": return "application/vnd.ms-excel";
            case "xls": return "application/vnd.ms-excel";
            case "xlsb": return "application/vnd.ms-excel.sheet.binary.macroenabled.12";
            case "xlsm": return "application/vnd.ms-excel.sheet.macroenabled.12";
            case "xlsx": return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            case "xlt": return "application/vnd.ms-excel";
            case "xltm": return "application/vnd.ms-excel.template.macroenabled.12";
            case "xltx": return "application/vnd.openxmlformats-officedocument.spreadsheetml.template";
            case "xlw": return "application/vnd.ms-excel";
            case "xml": return "text/xml";
            case "xmta": return "application/xml";
            case "xof": return "x-world/x-vrml";
            case "xoml": return "text/plain";
            case "xpm": return "image/x-xpixmap";
            case "xps": return "application/vnd.ms-xpsdocument";
            case "xrm-ms": return "text/xml";
            case "xsc": return "application/xml";
            case "xsd": return "text/xml";
            case "xsf": return "text/xml";
            case "xsl": return "text/xml";
            case "xslt": return "text/xml";
            case "xsn": return "application/octet-stream";
            case "xss": return "application/xml";
            case "xtp": return "application/octet-stream";
            case "xwd": return "image/x-xwindowdump";
            case "z": return "application/x-compress";
            case "zip": return "application/x-zip-compressed";
            #endregion
            default: return "application/octet-stream";
        }
    }

    public static string getSubscriptionDirectory(string sRegistrationCode)
    {
        string directory = "";

        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_GetCustomerWebDirectory", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@ReturnSelect", SqlDbType.Bit);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN", new XElement("Web",
                                                            new XAttribute("RegistrationCode", sRegistrationCode))).ToString();

            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
            cmd.Parameters["@ReturnSelect"].Value = 0;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if (sXmlOut.Length > 0)
            {
                List<string> listDirectory = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Web", "Directory");
                if (listDirectory.Count > 0 && listDirectory[0].Trim().Length > 0)
                    directory = listDirectory[0];
            }
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return directory;
    }

    public static string getHash(string value)
    {
        string hashValue = "";

        string encode = value;
        ASCIIEncoding AE = new ASCIIEncoding();
        byte[] MessageBytes = AE.GetBytes(encode);
        SHA1Managed SHhash = new SHA1Managed();

        string encode2 = "TOTIG?NDGG?";
        byte[] MessageBytes2 = AE.GetBytes(encode2);

        byte[] MessageBytes3 = BytesCombine(SHhash.ComputeHash(MessageBytes), SHhash.ComputeHash(MessageBytes2));

        string encode3 = BitConverter.ToString(SHhash.ComputeHash(MessageBytes)).Replace("-", "") + BitConverter.ToString(SHhash.ComputeHash(MessageBytes2)).Replace("-", "");
        //byte[] MessageBytes3 = AE3.GetBytes(encode3);
        hashValue = BitConverter.ToString(SHhash.ComputeHash(MessageBytes3)).Replace("-", "");

        return hashValue;
    }

    protected static byte[] BytesCombine(byte[] first, byte[] second)
    {
        byte[] ret = new byte[first.Length + second.Length];
        Buffer.BlockCopy(first, 0, ret, 0, first.Length);
        Buffer.BlockCopy(second, 0, ret, first.Length, second.Length);
        return ret;
    }

    public static string GetValueFromXml(string sXml, string sNode, string sAttribute)
    {
        string sValue = "";

        List<string> tagList = CommonMethod.GetAttributeValues(sXml.ToString(), sNode, sAttribute);

        if (tagList.Count > 0)
            sValue = tagList[0];

        return sValue;
    }

    protected static bool isPasswordSecured(string sPassword)
    {
        bool bPasswordSecured = false;

        Regex regExNumber = new Regex(@"[\d]");
        Regex regExCapital = new Regex(@"[A-Z]");
        Regex regExChar = new Regex(@"[A-z]");
        Regex regExSpecialChar = new Regex(@"[!$%^&*()_+|~\-={}\[\]:"";'<>?,.\/@]");

        bool bCapital = regExCapital.IsMatch(sPassword);
        bool bNumber = regExNumber.IsMatch(sPassword);
        bool bChar = regExChar.IsMatch(sPassword);
        bool bSpecialChar = regExSpecialChar.IsMatch(sPassword);

        if (sPassword.Trim().Length >= 8 && bNumber && bChar && bSpecialChar && bCapital)
        {
            bPasswordSecured = true;
        }

        return bPasswordSecured;
    }
    protected static string EncodePassword(string password)
    {
        ASCIIEncoding AE = new ASCIIEncoding();
        SHA1Managed SHhash = new SHA1Managed();

        byte[] PassBytes = SHhash.ComputeHash(AE.GetBytes(password));
        byte[] PPBytes = SHhash.ComputeHash(AE.GetBytes("TOTIG?NDGG?"));

        byte[] TempBytes = new byte[PassBytes.Length + PPBytes.Length];

        for (int x = 0; x < TempBytes.Length; x++)
        {
            for (int y = 0; y < PassBytes.Length; y++)
            {
                TempBytes[x] = PassBytes[y];
                x++;
            }
            for (int y = 0; y < PPBytes.Length; y++)
            {
                TempBytes[x] = PPBytes[y];
                x++;
            }
        }

        return BitConverter.ToString(SHhash.ComputeHash(TempBytes)).Replace("-", "");
    }
    public static void ChangeUserPassword(string refUID, string oldPassword, string newPassword, out string sMessage, out System.Drawing.Color color)
    {
        if (isPasswordSecured(newPassword))
        {
            string newPasswordEncoded = EncodePassword(newPassword);
            string oldPasswordEncoded = EncodePassword(oldPassword);
            SqlConnection conn = null;

            try
            {
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
                conn.Open();

                SqlCommand cmd = new SqlCommand("ServiceClient.P_ChangePassword", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@IN", SqlDbType.VarChar, -1);
                cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);

                cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                                    new XElement("User",
                                                        new XAttribute("RefUID", refUID),
                                                        new XAttribute("OldPass", oldPasswordEncoded),
                                                        new XAttribute("NewPass", newPasswordEncoded))).ToString();

                cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

                cmd.ExecuteNonQuery();

                string sXml = cmd.Parameters["@OUT"].Value.ToString();

                if (sXml.Length > 0)
                {
                    List<string> infoResult = CommonMethod.GetAttributeValues(sXml, "ALL_XML_OUT/Result", "Updated");
                    if (infoResult.Count > 0)
                    {
                        if (int.Parse(infoResult[0]) > 0)
                        {
                            sMessage = Resources.res.OperationSuccessful;
                            color = System.Drawing.Color.Green;
                        }
                        else
                        {
                            sMessage = Resources.res.OperationFailed;
                            color = System.Drawing.Color.Red;
                        }
                    }
                    else
                    {
                        sMessage = Resources.res.OperationFailed + " (Bad XML Out)";
                        color = System.Drawing.Color.Red;
                    }
                }
                else
                {
                    sMessage = Resources.res.OperationFailed + " (Empty XML Out)";
                    color = System.Drawing.Color.Red;
                }
            }
            catch (Exception e)
            {
                sMessage = e.Message;
                color = System.Drawing.Color.Red;
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }
        else
        {
            sMessage = "Nouveau mot de passe non valide !";
            color = System.Drawing.Color.Red;
        }
    }
    public static bool ForgotPassword(string sEmail, out string sMessage)
    {
        sMessage = "";
        bool isOK = false;
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("ServiceClient.P_ReinitPassword", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@Email", SqlDbType.VarChar, 500);
            cmd.Parameters.Add("@RC", SqlDbType.Int);

            cmd.Parameters["@Email"].Value = sEmail;
            cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;

            cmd.ExecuteNonQuery();

            int iRC = -1;
            string sRC = cmd.Parameters["@RC"].Value.ToString();

            if (int.TryParse(sRC, out iRC))
            {
                if(iRC == 0)
                {
                    isOK = true;
                    sMessage = "Un nouveau mot de passe vient de vous être envoyé sur " + sEmail;
                }
                //Email inconnu
                else if(iRC == 75080 )
                    sMessage = "Cet e-mail n&apos;existe pas";
                else
                    sMessage = "Une erreur est survenue (" + sRC + ")";
            }
            else
                sMessage = "Une erreur est survenue (" + sRC + ")";
        }
        catch (Exception e)
        {
            sMessage = "Une erreur est survenue (" + e.Message + ")";
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return isOK;
    }

    //public static string GetFormattedAccountBalance(string sAccountNumber, string sIBAN, bool bIsCobalt)
    //{
    //    decimal dAccountBalance = -1;
    //    if (GetAccountBalance(sAccountNumber, sIBAN, bIsCobalt, out dAccountBalance))
    //        return dAccountBalance.ToString().Replace('.', ',') + " &euro;";
    //    else return "";
    //}
    public static bool GetAccountBalance(int iRefCustomer, out decimal dAccountBalance, out string sFormattedAccountBalance)
    {
        string sXmlOut = Client.GetCustomerInformations(iRefCustomer);
        List<string> listAccountNumber = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Customer", "AccountNumber");
        List<string> listIsCobalt = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Customer", "IsCobalt");
        List<string> listIBAN = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Customer", "IBAN");
        bool bIsCobalt = (listIsCobalt.Count > 0 && listIsCobalt[0] == "1") ? true : false;

        dAccountBalance = -1;
        sFormattedAccountBalance = "";
        bool bBalanceOk = GetAccountBalance(listAccountNumber[0], listIBAN[0], bIsCobalt, out dAccountBalance);

        if (bBalanceOk)
            sFormattedAccountBalance = String.Format("{0:0.00}", dAccountBalance).Replace('.', ',') + " &euro;";

        return bBalanceOk;
    }
    public static bool GetAccountBalance(string sAccountNumber, string sIBAN, bool bIsCobalt, out decimal dAccountBalance)
    {
        return GetAccountBalance(sAccountNumber, sIBAN, bIsCobalt, false, out dAccountBalance);
    }
    public static bool GetAccountBalance(string sAccountNumber, string sIBAN, bool bIsCobalt, bool bERP, out decimal dAccountBalance)
    {
        bool bOk = false;
        dAccountBalance = 0;

        if (bIsCobalt)
        {
            try
            {
                CobaltApi api = new CobaltApi(ConfigurationManager.AppSettings["Cobalt_Core_URL"].ToString(), ConfigurationManager.AppSettings["Cobalt_Transfer_URL"].ToString(), null, ConfigurationManager.AppSettings["Cobalt_Expiration_URL"].ToString());
                dAccountBalance = decimal.Parse((bERP)?api.Account.GetAccount(sIBAN).BalanceErp.ToString() : api.Account.GetAccount(sIBAN).Balance.ToString());
                dAccountBalance /= 100; // convert from cents
                bOk = true;
            }
            catch(Exception e)
            {

            }
        }
        else
        {
            dAccountBalance = -1;
            WS_SAB.SabNbkClient ws = new WS_SAB.SabNbkClient();
            WS_SAB.GetSoldesRequest wsReq = new WS_SAB.GetSoldesRequest();
            WS_SAB.GetSoldesResponse wsRes = new WS_SAB.GetSoldesResponse();
            wsReq.AccountReference = "";
            wsReq.AccountNumber = sAccountNumber;

            try
            {
                wsRes = ws.GetSoldes(wsReq);

                dAccountBalance = decimal.Parse(wsRes.RealTimeAccountBalance);
                bOk = true;
            }
            catch (Exception ex)
            {
            }
        }

        return bOk;
    }
    public static string GetRights(string sToken, string sPageTAG)
    {
        SqlConnection conn = null;
        string sXmlOut = "";

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("GenericWeb.P_GetPageRights", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                            new XElement("Rights",
                                                new XAttribute("TAGPage", sPageTAG),
                                                new XAttribute("UserToken", sToken))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Page", "RC");
            if(listRC.Count > 0 && listRC[0] == "78000")
                System.Web.HttpContext.Current.Session.RemoveAll();
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return sXmlOut;
    }
    public static bool GetTagActionRight(string sXmlRights, string sTagAction, out bool bAction, out bool bView)
    {
        bool isOK = false;
        bAction = false;
        bView = false;

        List<string> listRC = CommonMethod.GetAttributeValues(sXmlRights, "ALL_XML_OUT/Page", "RC");

        if (listRC.Count > 0 && listRC[0] == "0")
        {
            List<string> listAction = CommonMethod.GetTags(sXmlRights, "ALL_XML_OUT/Page/Action");

            for (int i = 0; i < listAction.Count; i++)
            {
                List<string> listTag = CommonMethod.GetAttributeValues(listAction[i], "Action", "TagAction");
                List<string> listActionAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ActionAllowed");
                List<string> listViewAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ViewAllowed");

                if (listTag.Count > 0 && listTag[0] == sTagAction)
                {
                    isOK = true;
                    if (listViewAllowed.Count == 0 || listViewAllowed[0] == "1")
                        bView = true;

                    if (listActionAllowed.Count == 0 || listActionAllowed[0] == "1")
                        bAction = true;

                    else { return false; }
                }
            }
        }

        return isOK;
    }
    public static void LogErrorDB(string sWebsite, string sServerName, int iRefCustomer, string sMessage, string sStackTrace)
    {
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_AddExceptionGeneric", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@WebSite", SqlDbType.VarChar, 50);
            cmd.Parameters.Add("@ServerName", SqlDbType.VarChar, 15);
            cmd.Parameters.Add("@RefCustomer", SqlDbType.Int);
            cmd.Parameters.Add("@ExceptionMessage", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@StackTrace", SqlDbType.VarChar, -1);

            cmd.Parameters["@WebSite"].Value = sWebsite;
            cmd.Parameters["@ServerName"].Value = sServerName;
            cmd.Parameters["@RefCustomer"].Value = iRefCustomer;
            cmd.Parameters["@ExceptionMessage"].Value = sMessage;
            cmd.Parameters["@StackTrace"].Value = sStackTrace;

            cmd.ExecuteNonQuery();
        }
        catch (Exception ex)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }
    }

    public static DataTable GetAgencyList()
    {
        SqlConnection conn = null;
        DataTable dt = new DataTable();

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("BankSI.P_GetAgencyList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;

            da.Fill(dt);
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }
    public static DataTable GetSellerList(string AgencyID, string sRefAgency, string sRefHW, string sRefSeller)
    {
        SqlConnection conn = null;
        DataTable dt = new DataTable();

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Partner"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("Partner.P_GetNickelAgencySellerList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@XMLIN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@XMLOUT", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@ReturnSelect", SqlDbType.Bit);

            cmd.Parameters["@XMLOUT"].Direction = ParameterDirection.Output;
            cmd.Parameters["@XMLIN"].Value = new XElement("ALL_XML_IN",
                                            new XElement("A",
                                                new XAttribute("RefAgency", sRefAgency),
                                                new XAttribute("RefHardware", sRefHW),
                                                new XAttribute("RefUser", sRefSeller),
                                                new XAttribute("SelectedAgencyID", AgencyID))).ToString();

            cmd.Parameters["@ReturnSelect"].Value = true;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;

            da.Fill(dt);
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }

    public static string GetXMLCustomerInformations(int iRefCustomer)
    {
        SqlConnection conn = null;
        string sXmlOut = "";

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_GetCustomerInformation", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@ReturnSelect", SqlDbType.Bit);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN", new XElement("Customer", new XAttribute("RefCustomer", iRefCustomer))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
            cmd.Parameters["@ReturnSelect"].Value = 0;

            cmd.ExecuteNonQuery();

            sXmlOut = cmd.Parameters["@OUT"].Value.ToString();
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return sXmlOut;
    }
    public static WS_SAB.GetRibResponse GetBankDetails(string sAccountNumber, int refCustomer)
    {
        WS_SAB.SabNbkClient ws = new WS_SAB.SabNbkClient();
        WS_SAB.GetRibRequest wsReq = new WS_SAB.GetRibRequest();
        WS_SAB.GetRibResponse wsRes = new WS_SAB.GetRibResponse();
        wsReq.AccountNumber = sAccountNumber;

        wsRes = ws.GetRib(wsReq);

        string sXMLCustomerInfos = tools.GetXMLCustomerInformations(refCustomer);
        //string sXMLCustomerInfos = Tools.GetXMLCustomerInformations(1541);
        List<string> listLastName = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "LastName");
        List<string> listFirstName = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "FirstName");
        List<string> listAddress1 = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "Address1");
        List<string> listAddress2 = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "Address2");
        List<string> listAddress3 = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "Address3");
        List<string> listAddress4 = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "Address4");
        List<string> listZipcode = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "Zipcode");
        List<string> listCity = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "City");

        wsRes.CustomerLastName = (listLastName.Count > 0 && listLastName[0].Length > 0) ? listLastName[0] : wsRes.CustomerLastName;
        wsRes.CustomerFirstName = (listFirstName.Count > 0 && listFirstName[0].Length > 0) ? listFirstName[0] : wsRes.CustomerFirstName;
        wsRes.CustomerAddress1 = (listAddress1.Count > 0 && listAddress1[0].Length > 0) ? listAddress1[0] : wsRes.CustomerAddress1;
        wsRes.CustomerAddress2 = (listAddress2.Count > 0) ? listAddress2[0] : wsRes.CustomerAddress2;
        wsRes.CustomerAddress3 = (listAddress3.Count > 0) ? listAddress3[0] : wsRes.CustomerAddress3;
        wsRes.CustomerAddress4 = (listAddress4.Count > 0) ? listAddress4[0] : wsRes.CustomerAddress4;
        wsRes.CustomerZipCode = (listZipcode.Count > 0 && listZipcode[0].Length > 0) ? listZipcode[0] : wsRes.CustomerZipCode;
        wsRes.CustomerCity = (listCity.Count > 0 && listCity[0].Length > 0) ? listCity[0] : wsRes.CustomerCity;

        return wsRes;
    }
    public static void ReplaceTag(ref string sContent, string sTag, string sValue)
    {
        sContent = sContent.Replace(sTag, sValue);
    }

    public static string getFormattedMoney(string sMoney)
    {
        string sFormattedMoney = sMoney;
        decimal dMoney;

        if (decimal.TryParse(sMoney, out dMoney))
            sFormattedMoney = dMoney.ToString("0.##") + " &euro;";

        return sFormattedMoney;
    }
    public static string getFormattedDate(string sDate)
    {
        string sFormattedDate = sDate;
        DateTime dtDate;

        if (DateTime.TryParse(sDate, out dtDate))
            sFormattedDate = dtDate.ToString("dd/MM/yyyy HH:mm");

        return sFormattedDate;
    }
    public static string getFormattedShortDate(string sDate, char cSeparation)
    {
        if (sDate.Trim().Length > 10)
            sDate = sDate.Trim().Substring(0, 10);

        string sFormattedDate = sDate;
        try
        {
            DateTime dtDate;
            char _cSeparation = '/';

            if (cSeparation.ToString().Trim().Length > 0)
                _cSeparation = cSeparation;

            if (DateTime.TryParse(sDate, out dtDate))
                sFormattedDate = dtDate.ToString("dd" + _cSeparation + "MM" + _cSeparation + "yyyy");
        }
        catch(Exception ex)
        {
        }

        return sFormattedDate;
    }
    public static string getFormattedTime(string sTime, char cSeparation)
    {
        if (sTime.Trim().Length >= 16)
            sTime = sTime.Trim().Substring(11, 5);
        char _cSeparation = 'h';
        if (cSeparation.ToString().Trim().Length > 0)
            _cSeparation = cSeparation;

        if (sTime.Length == 0)
            sTime = "-- "+ _cSeparation +" --";
        else if (sTime.Length > 5)
            sTime = sTime.Substring(0, 5).Replace(':', _cSeparation);

        sTime = sTime.Trim().Replace("00:00","-- " + cSeparation + " --");

        return sTime;
    }
    public static string getFormattedAmount(string sAmount, bool bHtmlFormat)
    {
        string sLblAmount = "";

        try {
            string sAmountSign = (sAmount.StartsWith("-")) ? "-" : "+";

            // Remove last two zeros
            sAmount = sAmount.Substring(0, sAmount.Length - 2);

            // Add space every 3 numbers
            sAmount = sAmount.Replace("-", "");
            string[] arAmount = sAmount.Replace('.', ',').Split(',');
            int iTmp = int.Parse(arAmount[0]);
            arAmount[0] = String.Format("{0:### ### ### ###}", iTmp).Trim();
            if (arAmount[0].Length == 0)
                arAmount[0] = "0";
            sAmount = (arAmount.Length > 1) ? arAmount[0] + "," + arAmount[1] : arAmount[0];

            // Add Euro symbol
            sAmount += " &euro;";

            // Add sign and corresponding colors
            sAmount = sAmountSign + sAmount;
            if (!bHtmlFormat)
                sLblAmount = sAmount;
            else
            {
                if (sAmountSign == "+")
                    sLblAmount = "<span class='font-green'>" + sAmount + "</span>";
                else
                    sLblAmount = "<span class='font-red'>" + sAmount + "</span>";
            }
        }
        catch(Exception ex)
        {

        }

        return sLblAmount;
    }
    public static string getFormattedAmount(string sAmount)
    {
        string sLblAmount = "";
        string sAmountSign = (sAmount.StartsWith("-")) ? "-" : "+";

        // Remove last two zeros
        sAmount = sAmount.Substring(0, sAmount.Length - 2);

        // Add space every 3 numbers
        sAmount = sAmount.Replace("-", "");
        string[] arAmount = sAmount.Replace('.', ',').Split(',');
        int iTmp = int.Parse(arAmount[0]);
        arAmount[0] = String.Format("{0:### ### ### ###}", iTmp).Trim();
        if (arAmount[0].Length == 0)
            arAmount[0] = "0";
        sAmount = (arAmount.Length > 1) ? arAmount[0] + "," + arAmount[1] : arAmount[0];

        // Add Euro symbol
        sAmount += " &euro;";

        // Add sign and corresponding colors
        sAmount = sAmountSign + sAmount;

        if (sAmountSign == "+")
            sLblAmount = "<span class='font-green'>" + sAmount + "</span>";
        else
            sLblAmount = "<span class='font-red'>" + sAmount + "</span>";

        return sLblAmount;
    }
    public static string getAuthorFormattedAmount(string sAmount, string sLocalAmount)
    {
        string sLblAmount = "";
        string sAmountSign = (sLocalAmount.StartsWith("-")) ? "-" : "+";

        sAmount = sAmountSign + sAmount.Replace('.', ',') + " &euro;";
        if (sAmountSign == "+")
        {
            sLblAmount = "<span class='font-green'>" + sAmount + "</span>";
        }
        else
        {
            sLblAmount = "<span class='font-red'>" + sAmount + "</span>";
        }

        return sLblAmount;
    }
    public static string convertStringWhitoutAccent(string libelle)
    {
    if ((libelle != null)&&(libelle != string.Empty))
    {
    char[] oldChar = { 'À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'à', 'á', 'â', 'ã', 'ä', 'å', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'È', 'É', 'Ê', 'Ë', 'è', 'é', 'ê', 'ë', 'Ì', 'Í', 'Î', 'Ï', 'ì', 'í', 'î', 'ï', 'Ù', 'Ú', 'Û', 'Ü', 'ù', 'ú', 'û', 'ü', 'ÿ', 'Ñ', 'ñ', 'Ç', 'ç', '°' };
    char[] newChar = { 'A', 'A', 'A', 'A', 'A', 'A', 'a', 'a', 'a', 'a', 'a', 'a', 'O', 'O', 'O', 'O', 'O', 'O', 'o', 'o', 'o', 'o', 'o', 'o', 'E', 'E', 'E', 'E', 'e', 'e', 'e', 'e', 'I', 'I', 'I', 'I', 'i', 'i', 'i', 'i', 'U', 'U', 'U', 'U', 'u', 'u', 'u', 'u', 'y', 'N', 'n', 'C', 'c', ' ' };
    for (int i = 0; i < oldChar.Length; i++)
    {
    libelle = libelle.Replace(oldChar[i], newChar[i]);
    }
       }
       return libelle;
    }

    public static string getHighlightedSearchValue(string sValue, string sSearchValue)
    {
        string sHighlightedValue = sValue;

        if (sSearchValue.Trim().Length > 0)
            sHighlightedValue = sValue.ToUpper().Replace(sSearchValue.ToUpper(), "<span class=\"searchHighlighted\">" + sSearchValue.ToUpper() + "</span>");

        return sHighlightedValue;
    }
    public static bool IsDateValid(string sDate)
    {
        bool bValid = true;

        try
        {
            DateTime.ParseExact(sDate, "dd/MM/yyyy", null);
        }
        catch (Exception e) { bValid = false; }

        return bValid;
    }
    //public static bool IsPhoneValid(string sPhone)
    //{
    //    //Regex reg = new Regex(@"^\+{0,1}[0-9]{10,12}$");
    //    //Regex reg = new Regex(@"^\+[1-9]{1}[0-9]{1,14}$");
    //    Regex reg = new Regex(@"(0|(\\+33)|(0033))[1-9][0-9]{8}");
    //    Match match = reg.Match(sPhone);
    //    return match.Success;
    //}
    public static bool IsPhoneFixedFrenchValid(string sPhone)
    {
        Regex reg = new Regex(@"^((\+|00)33\s?|0)[1-5]|9(\s?\d{2}){4}$");
        Match match = reg.Match(sPhone);
        return match.Success;

    }

    protected static List<int> getDomTomCountryCode()
    {
        List<int> lsDomTomCountryCode = new List<int>();
        lsDomTomCountryCode.Add(262); // reunion/mayotte
        lsDomTomCountryCode.Add(594); // guyane
        lsDomTomCountryCode.Add(590); // guadeloupe
        lsDomTomCountryCode.Add(596); // martinique

        return lsDomTomCountryCode;
    }
    public static bool IsPhoneValid(string sPhone, bool isMobile)
    {
        bool bValid = false;
        //0156291627
        try
        {
            PhoneNumberUtil phoneUtil = PhoneNumberUtil.Instance;
            PhoneNumber phoneNumber = phoneUtil.Parse(sPhone, "FR"); //+33690580100 //06 96 70 60 50 //05 96 76 06 34

            bValid = phoneNumber.IsValidNumber;
            int iCountryCode = phoneNumber.CountryCode.GetValueOrDefault(-1);
            //string InternationnalNumber = phoneNumber.Format(PhoneNumberUtil.PhoneNumberFormat.INTERNATIONAL);
            if (phoneNumber.IsPossibleNumber)
            {
                if (isMobile)
                {
                    //string sCountryCode = phoneNumber.CountryCode.Value;
                    if (iCountryCode != -1 && getDomTomCountryCode().Contains(iCountryCode) && !bValid) //Exception Mobile Guadeloupe, Martinique, Réunion, Guyane
                    {
                        PhoneNumber phoneNumberAntillesToFR = phoneUtil.Parse("+33" + phoneNumber.NationalNumber.ToString().Replace(" ","").Replace("-",""), "FR");
                        if (phoneNumberAntillesToFR.IsValidNumber && phoneNumberAntillesToFR.NumberType == PhoneNumberUtil.PhoneNumberType.MOBILE)
                            bValid = true;
                    }
                    else if (iCountryCode != 33 && (phoneNumber.NumberType == PhoneNumberUtil.PhoneNumberType.MOBILE || phoneNumber.NumberType == PhoneNumberUtil.PhoneNumberType.FIXED_LINE_OR_MOBILE || phoneNumber.NumberType == PhoneNumberUtil.PhoneNumberType.UNKNOWN))
                        bValid = true;
                    else if (!(phoneNumber.NumberType == PhoneNumberUtil.PhoneNumberType.MOBILE || phoneNumber.NumberType == PhoneNumberUtil.PhoneNumberType.FIXED_LINE_OR_MOBILE))
                        bValid = false;
                }
                else if (iCountryCode != 33 && (phoneNumber.NumberType == PhoneNumberUtil.PhoneNumberType.MOBILE || phoneNumber.NumberType == PhoneNumberUtil.PhoneNumberType.FIXED_LINE_OR_MOBILE || phoneNumber.NumberType == PhoneNumberUtil.PhoneNumberType.UNKNOWN))
                    bValid = true;
                else if(!(phoneNumber.NumberType == PhoneNumberUtil.PhoneNumberType.FIXED_LINE || phoneNumber.NumberType == PhoneNumberUtil.PhoneNumberType.FIXED_LINE_OR_MOBILE))
                    bValid = false;
            }
        }
        catch (Exception e)
        {
            bValid = false;
        }

        return bValid;
    }
    public static bool IsEmailValid(string sEmail)
    {
        Regex reg = new Regex(@"^(([^<>()[\]\\.,;:\s@\""]+(\.[^<>()[\]\\.,;:\s@\""]+)*)|(\"".+\""))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$");
        Match match = reg.Match(sEmail);
        return match.Success;
    }
    public static bool IsValid(string sValue, string sPattern)
    {
        Regex reg = new Regex(sPattern);
        Match match = reg.Match(sValue);
        return match.Success;
    }

    public static bool IsIbanChecksumValid(string iban)
    {
        try
        {
            if (iban.Length < 4 || iban[0] == ' ' || iban[1] == ' ' || iban[2] == ' ' || iban[3] == ' ') throw new InvalidOperationException();

            var checksum = 0;
            var ibanLength = iban.Length;
            for (int charIndex = 0; charIndex < ibanLength; charIndex++)
            {
                if (iban[charIndex] == ' ') continue;

                int value;
                var c = iban[(charIndex + 4) % ibanLength];
                if ((c >= '0') && (c <= '9'))
                {
                    value = c - '0';
                }
                else if ((c >= 'A') && (c <= 'Z'))
                {
                    value = c - 'A';
                    checksum = (checksum * 10 + (value / 10 + 1)) % 97;
                    value %= 10;
                }
                else if ((c >= 'a') && (c <= 'z'))
                {
                    value = c - 'a';
                    checksum = (checksum * 10 + (value / 10 + 1)) % 97;
                    value %= 10;
                }
                else throw new InvalidOperationException();

                checksum = (checksum * 10 + value) % 97;
            }
            return checksum == 1;
        }
        catch (Exception e)
        {
            return false;
        }
    }

    public static string GetAttributeValueFromXml(string sXml, string sNode, string sAttribute)
    {
        string sValue = "";

        List<string> tagList = CommonMethod.GetAttributeValues(sXml.ToString(), sNode, sAttribute);

        if (tagList.Count > 0)
            sValue = tagList[0];

        return sValue;
    }
    public static DataTable GetCountryList()
    {
        DataTable dt = new DataTable();
        SqlConnection conn = null;
        bool isCountryListSession = false;

        DataTable dtSession = new DataTable();
        try
        {
            if (HttpContext.Current.Session["__CountryList"] != null)
            {
                dtSession = (DataTable)HttpContext.Current.Session["__CountryList"];
                if(dtSession.Rows.Count > 0) { isCountryListSession = true; }
            }
        }
        catch(Exception ex)
        {
            isCountryListSession = false;
        }

        if (isCountryListSession)
        {
            dt = dtSession;
        }
        else
        {
            try
            {
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["UniversalID"].ConnectionString);
                conn.Open();

                SqlCommand cmd = new SqlCommand("Other.P_GetCountryList", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@CountryRequest", SqlDbType.VarChar, -1);
                cmd.Parameters.Add("@CountryList", SqlDbType.VarChar, -1);
                cmd.Parameters.Add("@ReturnSelect", SqlDbType.Bit);

                cmd.Parameters["@CountryRequest"].Value = new XElement("CountryRequest", new XAttribute("Culture", "fr-FR"), new XAttribute("Order", "ASC")).ToString();
                cmd.Parameters["@CountryList"].Direction = ParameterDirection.Output;
                cmd.Parameters["@ReturnSelect"].Value = 1;

                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;

                da.Fill(dt);

                HttpContext.Current.Session["__CountryList"] = dt;
            }
            catch (Exception e)
            {
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        return dt;
    }

    public static string GetCountryNameFromISO2(string ISO2, DataTable dtCountryList)
    {
        string sCountryName = "";

        try
        {
            foreach (DataRow dr in dtCountryList.Rows)
            {
                if (dr["ISO2"].ToString() == ISO2)
                {
                    sCountryName = dr["CountryName"].ToString();
                }
            }
        }
        catch (Exception ex) { sCountryName = ""; }

        return sCountryName;
    }
    public static string GetAccountTypeImg(string sServiceTag)
    {
        string sURL = "";

        switch (sServiceTag)
        {
            case "NOB":
            case "1":
                sURL = "./Styles/Img/logo-nickel.png";
                break;
            case "CNJ":
            case "4":
                sURL = "./Styles/Img/logo_nickel_12-18.png";
                break;
            case "PRO":
                sURL = "./Styles/Img/bura-orange.png";
                break;
            case "8":
            case "FCN":
                sURL = "./Styles/Img/logo-nickel-specimen.png";
                break;
        }

        return sURL;
    }
    public static bool isTobaccoShopAccount(string sRegistrationCode)
    {
        bool isTobaccoShopAccount = false;

        try
        {
            if (sRegistrationCode.Substring(0, 3) == "PRO")
                isTobaccoShopAccount = true;
        }
        catch(Exception ex){}

        return isTobaccoShopAccount;
    }
    public static bool GetClientDocForDownload(string sRefCustomer, string sInitialRefCustomer, string sFileName)
    {
        bool isOK = false;

        SqlConnection conn = null;
        try
        {
            if (sRefCustomer.Trim().Length > 0 && sFileName.Trim().Length > 0)
            {
                /***********/
                /*
                    Exemple Xml IN :
                    SET @IN = '
                    <ALL_XML_IN>
                        <Document RefCustomer="2" Name="131007RM00001180001.pdf" />
                        <Document RefCustomer="2" Name="ABFD5B0F2DA95E7A9E993A9F331188ED433E0FB5\140801RM00001180001.pdf" />
                    </ALL_XML_IN>'

                    Resultat :
                    <ALL_XML_OUT>
                        <Document RefCustomer="2" Name="131007RM00001180001.pdf" Rc="0" />
                        <Document RefCustomer="2" Name="ABFD5B0F2DA95E7A9E993A9F331188ED433E0FB5\140801RM00001180001.pdf" Rc="0" />
                    </ALL_XML_OUT>
                */
                /***********/
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK2"].ConnectionString);
                conn.Open();

                SqlCommand cmd = new SqlCommand("[Web].[P_DownloadCustomerDocument]", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@IN", SqlDbType.VarChar, -1);
                cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);
                cmd.Parameters.Add("@RC", SqlDbType.Int);

                cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                                new XElement("Document",
                                                    new XAttribute("RefCustomer", sRefCustomer),
                                                    new XAttribute("InitialRefCustomer", sInitialRefCustomer),
                                                    new XAttribute("Name", sFileName))).ToString();
                cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
                cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;

                cmd.ExecuteNonQuery();

                string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

                if (sXmlOut.Trim().Length > 0)
                {
                    List<string> listDocument = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/Document");

                    for (int i = 0; i < listDocument.Count; i++)
                    {
                        List<string> listName = CommonMethod.GetAttributeValues(listDocument[i], "Document", "Name");
                        List<string> listRefCustomer = CommonMethod.GetAttributeValues(listDocument[i], "Document", "RefCustomer");
                        List<string> listRc = CommonMethod.GetAttributeValues(listDocument[i], "Document", "Rc");

                        if (listName != null && listRefCustomer != null && listRc != null &&
                            listName.Count > 0 && listRefCustomer.Count > 0 && listRc.Count > 0 &&
                            listName[0].Trim() == sFileName &&
                            listRefCustomer[0].Trim() == sRefCustomer.Trim() &&
                            listRc[0].Trim() == "0")
                        {
                            isOK = true;
                        }

                    }
                }
            }
        }
        catch (Exception ex)
        {
            isOK = false;
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return isOK;
    }
    public static string GetCustomerWebDirectoryName(string sRegistrationCode)
    {
        return GetCustomerWebDirectoryNameDB(new XElement("ALL_XML_IN",
                                                new XElement("Web",
                                                    new XAttribute("RegistrationCode", sRegistrationCode))).ToString());
    }
    public static string GetCustomerWebDirectoryName(int iRefCustomer)
    {
        return GetCustomerWebDirectoryNameDB(new XElement("ALL_XML_IN",
                                                new XElement("Web",
                                                    new XAttribute("RefCustomer", iRefCustomer))).ToString());
    }
    public static string GetCustomerWebDirectoryNameDB(string sXmlIN)
    {
        string sDirectoryName = "";
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("Web.P_GetCustomerWebDirectory", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);

            cmd.Parameters["@IN"].Value = sXmlIN;
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if (sXmlOut.Length > 0)
            {
                List<string> listDirectory = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Web", "Directory");
                if (listDirectory.Count > 0)
                    sDirectoryName = listDirectory[0];
            }
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }


        return sDirectoryName;
    }
    public static bool GetClientOpeningFile(string sRefCustomer)
    {
        bool isOK = false;

        SqlConnection conn = null;
        try
        {
            if (sRefCustomer.Trim().Length > 0)
            {
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
                conn.Open();

                SqlCommand cmd = new SqlCommand("[Google].[P_ManageDownloadOpenDocument]", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@RefCustomer", SqlDbType.Int);
                cmd.Parameters.Add("@RC", SqlDbType.Int);

                cmd.Parameters["@RefCustomer"].Value = sRefCustomer;
                cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;

                cmd.ExecuteNonQuery();

                int iRc = -1;
                string sRc = cmd.Parameters["@RC"].Value.ToString();
                if (int.TryParse(sRc, out iRc) && iRc == 0)
                {
                    isOK = true;
                }
            }
        }
        catch (Exception ex)
        {
            isOK = false;
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return isOK;
    }
    public static int GetRefCustomerFromRegistrationCode(string sRegistrationCode)
    {
        int iRefCustomer = -1;

        SqlConnection conn = null;
        DataTable dt = new DataTable();
        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("[Tools].[F_GetRefCustomerFromRegistrationCode]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@RegistrationCode", SqlDbType.VarChar, 20);
            cmd.Parameters.Add("@RefCustomer", SqlDbType.Int);
            cmd.Parameters["@RegistrationCode"].Value = sRegistrationCode;
            cmd.Parameters["@RefCustomer"].Direction = ParameterDirection.ReturnValue;

            cmd.ExecuteNonQuery();

            iRefCustomer = int.Parse(cmd.Parameters["@RefCustomer"].Value.ToString());
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return iRefCustomer;
    }
    public static string GetBankNameFromBic(string sBic)
    {
        string sBankName = "";

        SqlConnection conn = null;
        DataTable dt = new DataTable();
        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();
            SqlCommand cmd = new SqlCommand("[Other].[F_GetBankName]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@BIC", SqlDbType.VarChar, 11);
            cmd.Parameters.Add("@TYPE", SqlDbType.VarChar, 300);
            cmd.Parameters.Add("@BankName", SqlDbType.VarChar, 300);
            cmd.Parameters["@BIC"].Value = sBic;
            cmd.Parameters["@TYPE"].Value = "";
            cmd.Parameters["@BankName"].Direction = ParameterDirection.ReturnValue;

            cmd.ExecuteNonQuery();

            sBankName = cmd.Parameters["@BankName"].Value.ToString();
        }
        catch (Exception e)
        {
            sBankName = "";
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return sBankName;
    }

    public static DataTable getCityList(string dep, string prefix, string order, string culture)
    {
        DataTable dt = new DataTable();

        SqlConnection connProcIN = null;
        connProcIN = new SqlConnection(ConfigurationManager.ConnectionStrings["UniversalID"].ConnectionString);

        try
        {
            connProcIN.Open();
            SqlCommand cmdSpIn = new SqlCommand("Other.P_GetCityList", connProcIN);

            cmdSpIn.CommandType = CommandType.StoredProcedure;
            cmdSpIn.Parameters.Add("@CityRequest", System.Data.SqlDbType.VarChar, -1);
            cmdSpIn.Parameters.Add("@CityList", System.Data.SqlDbType.VarChar, -1);
            cmdSpIn.Parameters.Add("@RC", SqlDbType.Int);
            cmdSpIn.Parameters.Add("@ReturnSelect", SqlDbType.Bit);

            cmdSpIn.Parameters["@CityList"].Direction = ParameterDirection.Output;
            cmdSpIn.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;

            cmdSpIn.Parameters["@CityRequest"].Value = new XElement("CityRequest",
                                                        new XAttribute("Prefix", prefix),
                                                        new XAttribute("Dep", dep),
                                                        new XAttribute("Culture", (culture != null) ? culture : "fr-FR"),
                                                        new XAttribute("Order", (order != null) ? order : "ASC")).ToString();
            cmdSpIn.Parameters["@ReturnSelect"].Value = 1;

            SqlDataAdapter adapt = new SqlDataAdapter();
            adapt.SelectCommand = cmdSpIn;
            adapt.Fill(dt);
        }
        catch (Exception ex)
        {
        }
        finally
        {
            if (connProcIN != null)
                connProcIN.Close();
        }

        return dt;
    }
    public static DataTable getDepList(string prefix, string order, string culture)
    {
        DataTable dt = new DataTable();

        SqlConnection connProcIN = null;
        connProcIN = new SqlConnection(ConfigurationManager.ConnectionStrings["UniversalID"].ConnectionString);

        try
        {
            connProcIN.Open();
            SqlCommand cmdSpIn = new SqlCommand("Other.P_GetDepartementList", connProcIN);

            cmdSpIn.CommandType = CommandType.StoredProcedure;
            cmdSpIn.Parameters.Add("@DepartementRequest", System.Data.SqlDbType.VarChar, -1);
            cmdSpIn.Parameters.Add("@DepartementList", System.Data.SqlDbType.VarChar, -1);
            cmdSpIn.Parameters.Add("@RC", SqlDbType.Int);
            cmdSpIn.Parameters.Add("@ReturnSelect", SqlDbType.Bit);

            cmdSpIn.Parameters["@DepartementList"].Direction = ParameterDirection.Output;
            cmdSpIn.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;

            cmdSpIn.Parameters["@DepartementRequest"].Value = new XElement("DepartementRequest",
                                                        new XAttribute("DepID", prefix),
                                                        new XAttribute("Culture", (culture != null) ? culture : "fr-FR"),
                                                        new XAttribute("Order", (order != null) ? order : "ASC")).ToString();
            cmdSpIn.Parameters["@ReturnSelect"].Value = 1;

            SqlDataAdapter adapt = new SqlDataAdapter();
            adapt.SelectCommand = cmdSpIn;
            adapt.Fill(dt);
        }
        catch (Exception ex)
        {
        }
        finally
        {
            if (connProcIN != null)
                connProcIN.Close();
        }

        return dt;
    }
    public static bool checkBirthDep(string dep)
    {
        bool isOK = false;
        DataTable dt = getDepList(dep, "ASC", "fr-FR");

        foreach(DataRow dr in dt.Rows)
        {
            if (dr["DepartementID"].ToString().Trim() == dep)
                isOK = true;
        }

        return isOK;
    }
    public static bool checkBirthCity(string dep, string city)
    {
        bool isOK = false;

        DataTable dt = getCityList(dep, city, "ASC", "fr-FR");

        foreach (DataRow dr in dt.Rows)
        {
            if (dr["City"].ToString().Trim() == city)
                isOK = true;
        }

        return isOK;
    }
    public static void LogToFile(string errorMessage, string className)
    {
        DateTime nowDate = DateTime.Now;
        string shortDate = String.Format("{0:yyyy-MM-dd}", nowDate);
        string filename = string.Format("{0}.log", shortDate);
        //création du nom de fichier .log
        filename = filename.Replace("/", "-");
        //récupérer le path complet, application PC
        string rootPath = HostingEnvironment.ApplicationPhysicalPath + "Log\\";
        //ou le path pour une application serveur
        string fullFilename = string.Format(@"{0}{1}", rootPath, filename);
        //vérifier les dossiers Data & Log
        if (!Directory.Exists(rootPath))
        {
            //création du dossier
            Directory.CreateDirectory(rootPath);
        }
        //vérifier le fichier
        if (!System.IO.File.Exists(fullFilename))
        {
            //création du fichier log du jour
            System.IO.FileStream f = System.IO.File.Create(fullFilename);
            f.Close();
        }

        using (StreamWriter writer = new StreamWriter(fullFilename, true))
        {
            //écriture dans le fichier log du jour
            writer.WriteLine(string.Format(
                                    "[{0} ON {1}] : {2}",
                                    DateTime.Now,
                                    className,
                                    errorMessage));
        }
    }

    public static void SetBackUrl(string sUrl, string sButtonText, string sTargetPage)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("URL");
        dt.Columns.Add("ButtonText");
        dt.Columns.Add("TargetPage");
        dt.Rows.Add(new object[] { sUrl, sButtonText, sTargetPage });
        System.Web.HttpContext.Current.Session["BackUrl"] = dt;
    }
    public static bool GetBackUrl(out string sUrl, out string sButtonText)
    {
        bool isOK = false;
        sUrl = "";
        sButtonText = "";
        DataTable dt = new DataTable();

        if (System.Web.HttpContext.Current.Session["BackUrl"] != null)
        {
            try
            {
                dt = (DataTable)System.Web.HttpContext.Current.Session["BackUrl"];
                sUrl = dt.Rows[0]["URL"].ToString();
                sButtonText = dt.Rows[0]["ButtonText"].ToString();

                if (sUrl.Trim().Length > 0 && sButtonText.Trim().Length > 0)
                    isOK = true;
            }
            catch (Exception ex)
            {
            }
        }

        return isOK;
    }
    public static void DeleteBackUrl()
    {
        System.Web.HttpContext.Current.Session.Remove("BackUrl");
    }
    public static bool CheckBackUrl()
    {
        bool isOK = false;

        if (System.Web.HttpContext.Current.Session["BackUrl"] != null)
        {
            DataTable dt = new DataTable();
            try
            {
                dt = (DataTable)System.Web.HttpContext.Current.Session["BackUrl"];
                if (dt.Rows.Count > 0)
                    isOK = true;
            }
            catch (Exception ex)
            {
            }
        }

        return isOK;
    }
    public static void CheckBackUrlPage(string sActivePage)
    {
        DataTable dt = new DataTable();

        if (System.Web.HttpContext.Current.Session["BackUrl"] != null)
        {
            try
            {
                dt = (DataTable)System.Web.HttpContext.Current.Session["BackUrl"];
                string sTargetPage = dt.Rows[0]["TargetPage"].ToString();

                if (sTargetPage != sActivePage)
                    DeleteBackUrl();
            }
            catch (Exception ex)
            {
            }
        }
    }

    public static int GetNbDaysFromDate(string sDateTo, string sDateFrom)
    {
        int iNbDays;

        DateTime dateTo = new DateTime();
        try
        {
            dateTo = DateTime.ParseExact(sDateTo.Substring(0, 10), "dd/MM/yyyy", CultureInfo.InvariantCulture);
        }
        catch (Exception e) { }

        DateTime dateFrom = new DateTime();
        try
        {
            if (String.IsNullOrWhiteSpace(sDateFrom))
                dateFrom = DateTime.Now;
            else dateFrom = DateTime.ParseExact(sDateFrom.Substring(0, 10), "dd/MM/yyyy", CultureInfo.InvariantCulture);
        }
        catch(Exception e) { }

        iNbDays = (int)Math.Truncate((dateTo - dateFrom).TotalDays);

        //if (dateFrom > dateTo)
        //    iNbDays *= -1;

        return iNbDays;
    }

    public static bool ValidateIPAddress(string sIpAddress)
    {
        bool isIPAddress = false;

        string ipPattern = @"\b(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b";
        Regex regex = new Regex(ipPattern);

        isIPAddress = regex.IsMatch(sIpAddress);

        return isIPAddress;
    }

    public static string GetUserList(string sToken)
    {
        string sXmlOut = "";
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("ServiceClient.P_GetUsers", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN", new XElement("BOUser", new XAttribute("BOUserToken", sToken), new XAttribute("RefUID", ""), new XAttribute("Email", ""))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            sXmlOut = cmd.Parameters["@OUT"].Value.ToString();
        }
        catch (Exception e)
        { }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return sXmlOut;
    }

    [Serializable]
    public class UnauthorizedAccess
    {
        public bool Activated { get; set; }
        public string Message { get; set; }
        public string PreviousPage { get; set; }
    }

    public static void CheckUnauthorizedAccess(string sRC, string sMessage, string sCurrentPage)
    {
        ClearUnauthorizedAccess();

        List<string> lsRC = new List<string>();
        lsRC.Add("77001");
        lsRC.Add("77002");
        lsRC.Add("77003");

        if (lsRC.Contains(sRC.Trim()))
        {
            UnauthorizedAccess _UnauthorizedAccess = new UnauthorizedAccess();
            _UnauthorizedAccess.Activated = true;
            _UnauthorizedAccess.Message = (string.IsNullOrWhiteSpace(sMessage)) ? "" : sMessage;
            _UnauthorizedAccess.PreviousPage = sCurrentPage;

            HttpContext.Current.Session["UnauthorizedAccess"] = _UnauthorizedAccess;
            HttpContext.Current.Response.Redirect("UnauthorizedAccess.aspx");
        }
    }
    public static void ClearUnauthorizedAccess()
    {
        HttpContext.Current.Session.Remove("UnauthorizedAccess");
    }
    public static UnauthorizedAccess getUnautthorizedAccess()
    {
        UnauthorizedAccess _UnauthorizedAccess = new UnauthorizedAccess();
        _UnauthorizedAccess.Activated = false;
        _UnauthorizedAccess.Message = "";
        try
        {
            if (HttpContext.Current.Session["UnauthorizedAccess"] != null)
            {
                _UnauthorizedAccess = (UnauthorizedAccess)HttpContext.Current.Session["UnauthorizedAccess"];
            }
        }
        catch(Exception ex)
        {
            _UnauthorizedAccess.Activated = false;
            _UnauthorizedAccess.Message = "";
        }

        return _UnauthorizedAccess;
    }

    [Serializable]
    public class AssetInfos
    {
        public string template { get; set; }
        public bool isRevival { get; set; }
        public bool isAlreadyTreated { get; set; }
        public List<Asset> assets { get; set; }
    }
    [Serializable]
    public class Asset
    {
        public string type { get; set; }
        public string content { get; set; }
        public decimal top { get; set; }
        public decimal left { get; set; }
        public int deg { get; set; }
        public string color { get; set; }

        public Asset(string sType, string sContent, decimal iTop, decimal iLeft, int iDeg, string sColor)
        {
            this.type = sType;
            this.content = sContent;
            this.top = iTop;
            this.left = iLeft;
            this.deg = iDeg;
            this.color = sColor;
        }
    }
    public static string ImageFileToBase64(string sPath)
    {
        using (System.Drawing.Image image = System.Drawing.Image.FromFile(sPath))
        {
            using (MemoryStream m = new MemoryStream())
            {
                image.Save(m, image.RawFormat);
                byte[] imageBytes = m.ToArray();

                // Convert byte[] to Base64 String
                return Convert.ToBase64String(imageBytes);
            }
        }
    }

    public static void ReorderAlphabetized(DropDownList ddl)
    {
        List<ListItem> listCopy = new List<ListItem>();
        foreach (ListItem item in ddl.Items)
            listCopy.Add(item);
        ddl.Items.Clear();
        foreach (ListItem item in listCopy.OrderBy(item => item.Text))
            ddl.Items.Add(item);
    }

    public static void SetRedirectCookie(string sURL)
    {
        string sXml = new XElement("URL", sURL).ToString();
        FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1, "THE-COOKIE", DateTime.Now, DateTime.Now.AddMinutes(5), false, sXml);
        string encryptedText = FormsAuthentication.Encrypt(ticket);
        HttpContext.Current.Response.Cookies.Add(new HttpCookie("__THE-COOKIE__", encryptedText));
    }
    public static void GetRedirectCookieValues(out string sURL)
    {
        sURL = "";

        if (HttpContext.Current.Request.Cookies["__THE-COOKIE__"] != null && HttpContext.Current.Request.Cookies["__THE-COOKIE__"].Value.Length > 0)
        {
            FormsAuthenticationTicket ticket = null;

            try
            {
                ticket = FormsAuthentication.Decrypt(HttpContext.Current.Request.Cookies["__THE-COOKIE__"].Value);

                if (ticket != null && ticket.Expired == false)
                {
                    List<string> listURL = CommonMethod.GetTagValues(ticket.UserData, "URL");
                    sURL = (listURL.Count > 0) ? listURL[0] : "";
                    RemoveTheCookie();
                }
            }
            catch (Exception ex)
            {
            }
        }
    }
    private static void RemoveTheCookie()
    {
        HttpContext.Current.Response.Cookies.Add(new HttpCookie("__THE-COOKIE__", ""));
    }

    public static bool GetCommunicationModel(string sXmlIn, out string sXmlOut)
    {
        bool isOK = false;
        sXmlOut = "";

        SqlConnection conn = null;
        DataTable dt = new DataTable();
        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("[Web].[P_GetModels]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@ALL_XML_IN", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@ALL_XML_OUT", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@RC", SqlDbType.Int);
            cmd.Parameters["@ALL_XML_IN"].Value = sXmlIn;
            cmd.Parameters["@ALL_XML_OUT"].Direction = ParameterDirection.Output;
            cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;

            cmd.ExecuteNonQuery();

            int iRC = int.Parse(cmd.Parameters["@RC"].Value.ToString());
            sXmlOut = cmd.Parameters["@ALL_XML_OUT"].Value.ToString();
            if(iRC == 0) { isOK = true; }
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return isOK;
    }
    public static bool SendSMSToShop(string sText, string sGroupTarget)
    {
        bool isOK = false;

        SqlConnection conn = null;
        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("PDV.P_SendSMSToShop", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@Texte", SqlDbType.VarChar, 500);
            cmd.Parameters.Add("@ShopTarget", SqlDbType.VarChar, 50);
            cmd.Parameters["@Texte"].Value = sText;
            cmd.Parameters["@ShopTarget"].Value = sGroupTarget;

            cmd.ExecuteNonQuery();
            isOK = true;
        }
        catch (Exception e)
        {
            isOK = false;
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return isOK;
    }
}
