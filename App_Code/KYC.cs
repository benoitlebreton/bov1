﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using XMLMethodLibrary;

/// <summary>
/// Description résumée de KYC
/// </summary>
/// 
[Serializable]
public class KYC
{
    public static List<string> UE_Countries_ISO2()
    {
        List<string> lsCountries = new List<string>();
        
        lsCountries.Add("DE	");
        lsCountries.Add("AT	");
        lsCountries.Add("BE	");
        lsCountries.Add("BG	");
        lsCountries.Add("CY	");
        lsCountries.Add("HR	");
        lsCountries.Add("DK	");
        lsCountries.Add("ES	");
        lsCountries.Add("EE	");
        lsCountries.Add("FI	");
        lsCountries.Add("FR	");
        lsCountries.Add("GR	");
        lsCountries.Add("NL	");
        lsCountries.Add("HU	");
        lsCountries.Add("IE	");
        lsCountries.Add("IT	");
        lsCountries.Add("LV");
        lsCountries.Add("LT");
        lsCountries.Add("LU");
        lsCountries.Add("MT");
        lsCountries.Add("PL");
        lsCountries.Add("PT");
        lsCountries.Add("CZ");
        lsCountries.Add("RO");
        lsCountries.Add("GB");
        lsCountries.Add("SK");
        lsCountries.Add("SI");
        lsCountries.Add("SE");


        return lsCountries;
    }
    public static List<string> AELE_Countries_ISO2()
    {
        List<string> lsCountries = new List<string>();

        lsCountries.Add("IS");
        lsCountries.Add("NO");
        lsCountries.Add("LI");
        lsCountries.Add("CH");

        return lsCountries;
    }
    public static List<string> UE_Countries()
    {
        List<string> lsCountries = new List<string>();
        
        lsCountries.Add("DE");
        lsCountries.Add("AT");
        lsCountries.Add("BE");
        lsCountries.Add("BG");
        lsCountries.Add("CY");
        lsCountries.Add("HR");
        lsCountries.Add("DK");
        lsCountries.Add("ES");
        lsCountries.Add("EE");
        lsCountries.Add("FI");
        lsCountries.Add("FR");
        lsCountries.Add("GR");
        lsCountries.Add("NL");
        lsCountries.Add("HU");
        lsCountries.Add("IE");
        lsCountries.Add("IT");
        lsCountries.Add("LV");
        lsCountries.Add("LT");
        lsCountries.Add("LU");
        lsCountries.Add("MT");
        lsCountries.Add("PL");
        lsCountries.Add("PT");
        lsCountries.Add("CZ");
        lsCountries.Add("RO");
        lsCountries.Add("GB");
        lsCountries.Add("SK");
        lsCountries.Add("SI");
        lsCountries.Add("SE");


        return lsCountries;
    }
    public static List<string> AELE_Countries()
    {
        List<string> lsCountries = new List<string>();

        lsCountries.Add("IS");
        lsCountries.Add("NO");
        lsCountries.Add("LI");
        lsCountries.Add("CH");

        return lsCountries;
    }

    [Serializable]
    public class AnswerList
    {
        public string MaritalStatus { get; set; }
        public string Nationality { get; set; }
        public string ProfessionalStatus { get; set; }
        public string Profession { get; set; }
        public string OldProfession { get; set; }
        public string ProfessionPlus { get; set; }
        public string CNUsagePro { get; set; }
        public string Income { get; set; }
        public string PropertyStatus { get; set; }
        public string At { get; set; }
        public string PropertyValue { get; set; }
        public string CNUsage { get; set; }
        public string AccountUsage { get; set; }
        //public string TaxResidence { get; set; }
        public bool Certificate { get; set; }
        public bool SaveKYC { get; set; }
        
        public bool FatcaAlert { get; set; }
        public bool? TaxResidentFrOnly { get; set; }
        public DataTable TaxResidenceList { get; set; }
        public bool? IsAeleOrUe { get; set; }
        public bool? USResidentOrBorn { get; set; }
        public bool? USResident { get; set; }
        public bool? USBorn { get; set; }
        public string W9Filename { get; set; }
        public string W8Filename { get; set; }
        
        public bool TaxResidenceCertificate { get; set; }
        public bool ClientDataUseAuthorized { get; set; }

        public bool SaveFatcaAeoiValues { get; set; }
    }

    public static List<string> getKycProA()
    {
        List<string> lsProA = new List<string>();
        lsProA.Add("659"); //Cadre
        lsProA.Add("012"); //Employé(e)

        return lsProA;
    }
    public static List<string> getKycProB()
    {
        List<string> lsProB = new List<string>();
        lsProB.Add("052"); //Retraité(e)
        return lsProB;
    }
    public static List<string> getKycProC()
    {
        List<string> lsProC = new List<string>();
        lsProC.Add("014"); //Auto Entrepreneur(se)
        lsProC.Add("649"); //Commercant(e)
        lsProC.Add("070"); //Profession libérale
        lsProC.Add("071"); //Artisan
        lsProC.Add("015"); //Entrepreneur(se)
        return lsProC;
    }

    public static List<string> getForbiddenProC()
    {
        List<string> lsForbiddenProC = new List<string>();
        lsForbiddenProC.Add("PRO"); //Auto Entrepreneur(se) : usage professionnel
        return lsForbiddenProC;
    }

    public static DataTable getKycForProA()
    {
        return getKycList("PROF_PLUS", getKycLists(false));
    }
    public static DataTable getKycForProB()
    {
        return getKycList("ANC_PROF", getKycLists(false));
    }
    public static DataTable getKycPlusForProB()
    {
        return getKycList("PROF_PLUS", getKycLists(false));
    }

    public static DataTable getKycForProC()
    {
        return getKycList("MONCNAUTO", getKycLists(false));
    }

    public static DataTable getKycForProC(bool isMobile)
    {
        return getKycList("MONCNAUTO", getKycLists(false), false, isMobile);
    }

    public static string getKycProPlusLabel(string sKycProValue)
    {
        string sProPlusLabel = "secteur professionnel";

        if(getKycProA().Contains(sKycProValue))
            sProPlusLabel = "Votre secteur professionnel";
        else if(getKycProB().Contains(sKycProValue))
            sProPlusLabel = "Votre secteur professionnel";
        else if (getKycProC().Contains(sKycProValue))
            sProPlusLabel = "Utilisation pour une activité pro.";

        return sProPlusLabel;
    }

    public static List<string> getKycPropertyPlusStatus()
    {
        List<string> lsKycPropertyPlusStatus = new List<string>();
        lsKycPropertyPlusStatus.Add("GR"); //Hébergé(e)
        return lsKycPropertyPlusStatus;
    }
    public static List<string> getKycPropertyStatusForValueReq()
    {
        List<string> lsKycPropertyStatusForValueReq = new List<string>();
        lsKycPropertyStatusForValueReq.Add("PR"); //Propriétaire
        return lsKycPropertyStatusForValueReq;
    }

    public static DataTable getKycList(string Tag, DataTable dtKycLists)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("Label");
        dt.Columns.Add("Value");

        foreach (DataRow dr in dtKycLists.Rows)
        {
            if (dr[0].ToString().Trim() == Tag && dr[1].ToString().Trim().Length > 0)
            {
               switch (Tag)
                {
                    //case "PATRIMOINE":
                    //case "REVENUS":
                        //dt.Rows.Add(new object[] { dr[1].ToString().Trim().Replace("de ", "de <span style=\"white-space:nowrap;\">").Replace("entre ", "entre <span style=\"white-space:nowrap;\">").Replace(" et ", "</span> et <span style=\"white-space:nowrap;\">") + "</span>", dr[2] });
                        //break;
                    case "MONCN":
                        dt.Rows.Add(new object[] { dr[1].ToString().Trim().ToLower().Replace("est mon compte ", ""), dr[2] });
                        break;
                    case "MONCNAUTO":
                        dt.Rows.Add(new object[] { dr[1].ToString().Trim().ToLower().Replace("mon compte nickel est à ", ""), dr[2] });
                        break;
                    default:
                        dt.Rows.Add(new object[] { dr[1], dr[2] });
                        break;
                }

            }
        }


        return dt;
    }
    public static DataTable getKycList(string Tag, DataTable dtKycLists, bool bHtmlValue)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("Label");
        dt.Columns.Add("Value");

        foreach (DataRow dr in dtKycLists.Rows)
        {
            if (dr[0].ToString().Trim() == Tag && dr[1].ToString().Trim().Length > 0)
            {
                switch (Tag)
                {
                    case "PATRIMOINE":
                    case "REVENUS":
                        if(bHtmlValue)
                            dt.Rows.Add(new object[] { dr[1].ToString().Trim().Replace("de ", "de <span style=\"white-space:nowrap;\">").Replace("entre ", "entre <span style=\"white-space:nowrap;\">").Replace(" et ", "</span> et <span style=\"white-space:nowrap;\">") + "</span>", dr[2] });
                        else
                            dt.Rows.Add(new object[] { dr[1], dr[2] });
                        break;
                    case "MONCN":
                        dt.Rows.Add(new object[] { dr[1].ToString().Trim().ToLower().Replace("est mon compte ", ""), dr[2] });
                        break;
                    case "MONCNAUTO":
                        dt.Rows.Add(new object[] { dr[1].ToString().Trim().ToLower().Replace("mon compte nickel est à ", ""), dr[2] });
                        break;
                    default:
                        dt.Rows.Add(new object[] { dr[1], dr[2] });
                        break;
                }

            }
        }


        return dt;
    }

    public static DataTable getKycList(string Tag, DataTable dtKycLists, bool bHtmlValue, bool bIsMobile)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("Label");
        dt.Columns.Add("Value");

        foreach (DataRow dr in dtKycLists.Rows)
        {
            if (dr[0].ToString().Trim() == Tag && dr[1].ToString().Trim().Length > 0)
            {
                switch (Tag)
                {
                    case "PATRIMOINE":
                    case "REVENUS":
                        if (bHtmlValue)
                            dt.Rows.Add(new object[] { dr[1].ToString().Trim().Replace("de ", "de <span style=\"white-space:nowrap;\">").Replace("entre ", "entre <span style=\"white-space:nowrap;\">").Replace(" et ", "</span> et <span style=\"white-space:nowrap;\">") + "</span>", dr[2] });
                        else
                            dt.Rows.Add(new object[] { dr[1], dr[2] });
                        break;
                    case "MONCN":
                        dt.Rows.Add(new object[] { dr[1].ToString().Trim().ToLower().Replace("est mon compte ", ""), dr[2] });
                        break;
                    case "MONCNAUTO":
                        if (bIsMobile) { dt.Rows.Add(new object[] { dr[1].ToString().Trim().ToLower().Replace("non", "usage privé uniquement").Replace("oui", "usage professionnel"), dr[2] }); }
                        else { dt.Rows.Add(new object[] { dr[1].ToString().Trim(), dr[2] }); }
                        break;
                    default:
                        dt.Rows.Add(new object[] { dr[1], dr[2] });
                        break;
                }

            }
        }

        if(bIsMobile && Tag == "MONCNAUTO")
        {
            dt.DefaultView.Sort = "Label asc";
        }


        return dt;
    }
    public static DataTable getKycLists(bool bRefresh)
    {
        DataTable dtKycList = new DataTable();

        if (bRefresh) {
            dtKycList = getKycListsFromDB();
            return dtKycList;
        }
        else if (HttpContext.Current.Session["KYCList"] != null)
        {
            try
            {
                dtKycList = (DataTable)HttpContext.Current.Session["KYCList"];
            }
            catch (Exception ex)
            {
                dtKycList = getKycListsFromDB();
            }
        }
        else
        {
            dtKycList = getKycListsFromDB();
        }

        return dtKycList;
    }
    protected static DataTable getKycListsFromDB()
    {
        SqlConnection conn = null;
        DataTable dt = new DataTable();
        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();
            SqlCommand cmd = new SqlCommand("[Web].[P_GetCustomersKYCLists]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@IN", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@ReturnSelect", SqlDbType.Bit);
            cmd.Parameters["@IN"].Value = "";
            cmd.Parameters["@ReturnSelect"].Value = true;
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;
            da.Fill(dt);
            HttpContext.Current.Session["KYCList"] = dt;
        }
        catch (Exception ex)
        { }

        return dt;
    }

    public static bool checkKYC(AnswerList _answerList, out string sMessage)
    {
        bool isOK = true;
        sMessage = "Les informations suivantes sont manquantes :<ul>";

        bool isKycOK = true;
        bool isFatcaAeoiOK = true;
       
        if (sMessage != "Une erreur est survenue" && getStringValue(_answerList.Nationality).Trim().Length == 0)
        {
            isKycOK = false;
            sMessage += "<li>Nationalité</li>";
        }
        if (getStringValue(_answerList.MaritalStatus).Trim().Length == 0)
        {
            isKycOK = false;
            sMessage += "<li>Situation maritale</li>";
        }
        //if (cbClientLastNameChanged.Checked && txtClientNewLastName.Text.Trim().Length == 0)
        //{
        //    isOK = false;
        //    sMessage += "<li>saisissez votre nouveau nom de famille</li>";
        //}
        if (getStringValue(_answerList.ProfessionalStatus).Trim().Length == 0)
        {
            isKycOK = false;
            sMessage += "<li>Situation professionelle</li>";
        }
        else
        {
            if (getKycProA().Contains(getStringValue(_answerList.ProfessionalStatus)) && getStringValue(_answerList.ProfessionPlus).Trim().Length == 0)
            {
                isKycOK = false;
                sMessage += "<li>Secteur professionel</li>";
            }
            if (getKycProA().Contains(getStringValue(_answerList.ProfessionalStatus)) && getStringValue(_answerList.Profession).Trim().Length == 0)
            {
                isKycOK = false;
                sMessage += "<li>Profession</li>";
            }
            if (getKycProB().Contains(getStringValue(_answerList.ProfessionalStatus)) && getStringValue(_answerList.OldProfession).Trim().Length == 0)
            {
                isKycOK = false;
                sMessage += "<li>Ancienne situation professionnelle</li>";
            }
            if (getKycProB().Contains(getStringValue(_answerList.ProfessionalStatus)) && getStringValue(_answerList.OldProfession).Trim().Length == 0)
            {
                isKycOK = false;
                sMessage += "<li>Ancien secteur professionnel</li>";
            }
            if (getKycProC().Contains(getStringValue(_answerList.ProfessionalStatus)) && getStringValue(_answerList.CNUsagePro).Trim().Length == 0)
            {
                isKycOK = false;
                sMessage += "<li>Usage professionel</li>";
            }
        }

        if (getStringValue(_answerList.Income).Trim().Length == 0)
        {
            isKycOK = false;
            sMessage += "<li>Revenus mensuels</li>";
        }
        if (getStringValue(_answerList.PropertyStatus).Trim().Length == 0)
        {
            isKycOK = false;
            sMessage += "<li>Situation patrimoniale</li>";
        }
        else if (getKycPropertyPlusStatus().Contains(_answerList.PropertyStatus.Trim()) && _answerList.At.Trim().Length == 0)
        {
            isKycOK = false;
            sMessage += "<li>indiquez le nom et prénom de votre hébergeur</li>";
        }
        if (getStringValue(_answerList.PropertyValue).Trim().Length == 0)
        {
            isKycOK = false;
            sMessage += "<li>la valeur du patrimoine</li>";
        }
        if (getStringValue(_answerList.AccountUsage).Trim().Length == 0)
        {
            isKycOK = false;
            sMessage += "<li>Sélectionner au moins un usage</li>";
        }
        if (getStringValue(_answerList.CNUsage).Trim().Length == 0)
        {
            isKycOK = false;
            sMessage += "<li>Indiquez s&apos;il s&apos;agit du compte principal ou secondaire</li>";
        }

        if (!_answerList.Certificate)
        {
            isKycOK = false;
            sMessage += "<li>Attester les informations</li>";
        }

        if(_answerList.TaxResidentFrOnly == null)
        {
            isFatcaAeoiOK = false;
            sMessage += "<li>Indiquez si vous êtes résident français uniquement</li>";
        }

        if (_answerList.USResidentOrBorn == null)
        {
            isFatcaAeoiOK = false;
            sMessage += "<li>Indiquez si vous êtes citoyen(ne) américain(e), résident(e) américain(e) ou né(e) aux Etats-Unis</li>";
        }

        string sCheckTaxResidenceListMessage = "";
        if (!(CheckTaxResidenceList(_answerList, out sCheckTaxResidenceListMessage)))
        {
            isFatcaAeoiOK = false;
            sMessage += (!string.IsNullOrWhiteSpace(sCheckTaxResidenceListMessage)) ? sCheckTaxResidenceListMessage.Trim() : "<li>Liste de résidence fiscale non valide</li>";
        }

        string sCheckUSMessage = "";
        if (!CheckUsResidentOrBorn(_answerList, out sCheckUSMessage))
        {
            isFatcaAeoiOK = false;
            sMessage += (!string.IsNullOrWhiteSpace(sCheckUSMessage)) ? sCheckUSMessage.Trim() :  "<li>Envoyer les formulaires nécessaires (W9, W8, ...)</li>";
        }

        if (!_answerList.TaxResidenceCertificate)
        {
            isFatcaAeoiOK = false;
            sMessage += "<li>Certifier les informations liées à votre résidence fiscale</li>";
        }

        if(!isFatcaAeoiOK && !isKycOK) { isOK = false; }
        if(_answerList.SaveFatcaAeoiValues && !isFatcaAeoiOK) { isOK = false; }
        if (_answerList.SaveKYC && !isKycOK) { isOK = false; }

        if (isOK) { sMessage = ""; }
        else if (!isOK && sMessage != "Une erreur est survenue") { sMessage += "</ul>"; }

        return isOK;
    }
    public static bool saveKYC(int iRefCustomer, string sToken, AnswerList _answerList, out string sMessage)
    {
        bool isOK = false;
        sMessage = "";

        if(checkKYC(_answerList, out sMessage))
        {
            isOK = false;
            XElement xCust = new XElement("Customer", new XAttribute("RefCustomer", iRefCustomer), new XAttribute("CashierToken", sToken));

            if (_answerList.SaveKYC)
            {
                xCust.Add(new XAttribute("NationalityISO2", _answerList.Nationality));
                //xCust.Add(new XAttribute("TaxResidenceISO2", _answerList.TaxResidence));
                xCust.Add(new XAttribute("sitFam", _answerList.MaritalStatus));
                //if (cbClientLastNameChanged.Checked && panelClientLastNameChanged.Visible) { xCust.Add(new XAttribute("NewLastName", txtClientNewLastName.Text)); };
                xCust.Add(new XAttribute("codPro", _answerList.ProfessionalStatus));
                if (getKycProA().Contains(_answerList.ProfessionalStatus))
                {
                    xCust.Add(new XAttribute("ProfessionPlusTAG", _answerList.ProfessionPlus),
                                new XAttribute("Profession", _answerList.Profession));
                }
                else if (getKycProB().Contains(_answerList.ProfessionalStatus)) { xCust.Add(new XAttribute("OldProfessionTAG", _answerList.OldProfession)); }
                else if (getKycProC().Contains(_answerList.ProfessionalStatus)) { xCust.Add(new XAttribute("CNUsagePro", _answerList.CNUsagePro)); }
                xCust.Add(new XAttribute("IncomeTAG", _answerList.Income));
                xCust.Add(new XAttribute("modHab", _answerList.PropertyStatus));
                if (getKycPropertyPlusStatus().Contains(_answerList.PropertyStatus)) { xCust.Add(new XAttribute("At", _answerList.At)); }
                if (getStringValue(_answerList.PropertyValue).Trim().Length > 0) { xCust.Add(new XAttribute("Estate", _answerList.PropertyValue)); }
                xCust.Add(new XAttribute("AccountUsageTAGList", _answerList.AccountUsage));
                xCust.Add(new XAttribute("CNUsage", _answerList.CNUsage));
            }

            if (_answerList.SaveFatcaAeoiValues)
            {
                xCust.Add(new XAttribute("FrenchTaxOnly", (_answerList.TaxResidentFrOnly == true) ? "1" : "0"));
                xCust.Add(new XAttribute("USResidentOrBorn", (_answerList.USResidentOrBorn == true) ? "1" : "0"));
                if (_answerList.USResidentOrBorn == true) { xCust.Add(new XAttribute("USBorn", (_answerList.USBorn == true) ? "1" : "0")); }
                if (_answerList.USResidentOrBorn == true) { xCust.Add(new XAttribute("USResident", (_answerList.USResident == true) ? "1" : "0")); }
                xCust.Add(new XAttribute("ClientDataUseAuthorized", (_answerList.ClientDataUseAuthorized) ? "1" : "0"));

                if (_answerList.TaxResidentFrOnly == false)
                {
                    XElement xTaxs = new XElement("Taxs");
                    if (_answerList.TaxResidenceList != null && _answerList.TaxResidenceList.Rows.Count > 0)
                    {

                        foreach (DataRow dr in _answerList.TaxResidenceList.Rows)
                        {
                            xTaxs.Add(new XElement("Tax", new XAttribute("Action", "A"), new XAttribute("ISO2", dr["ISO2"].ToString()), new XAttribute("TaxId", dr["TaxID"].ToString()), new XAttribute("ReasonNoTax", dr["Reason"])));
                        }

                        xCust.Add(xTaxs);
                    }
                }
            }

            bool isKycProPlus = (getKycProA().Contains(_answerList.ProfessionalStatus) || getKycProB().Contains(_answerList.ProfessionalStatus) || getKycProC().Contains(_answerList.ProfessionalStatus)) ? true : false;
            bool isAtOK = (String.IsNullOrEmpty(_answerList.At) && getKycPropertyPlusStatus().Contains(_answerList.PropertyStatus)) ? false : true;

            if (!String.IsNullOrWhiteSpace(_answerList.ProfessionalStatus) && (!isKycProPlus ||
                    (((getKycProA().Contains(_answerList.ProfessionalStatus) && !String.IsNullOrWhiteSpace(_answerList.ProfessionPlus) && !String.IsNullOrWhiteSpace(_answerList.Profession))
                    || (getKycProB().Contains(_answerList.ProfessionalStatus) && !String.IsNullOrWhiteSpace(_answerList.OldProfession))
                    || (getKycProC().Contains(_answerList.ProfessionalStatus) && !String.IsNullOrWhiteSpace(_answerList.CNUsagePro)))))
                    && !String.IsNullOrWhiteSpace(_answerList.Income)
                    && !String.IsNullOrWhiteSpace(_answerList.PropertyStatus)
                    && isAtOK
                    && !String.IsNullOrWhiteSpace(_answerList.PropertyValue)
                    && !String.IsNullOrWhiteSpace(_answerList.AccountUsage)
                    && !String.IsNullOrWhiteSpace(_answerList.CNUsage))
                xCust.Add(new XAttribute("AllKYCCompleted", "1"));
            //&& !String.IsNullOrWhiteSpace(_answerList.TaxResidence))
            //isOK = true;
            isOK = SetXMLCustomerInformations(new XElement("ALL_XML_IN", xCust).ToString());

            if (!isOK) { sMessage = "Les KYC n'ont pas pu être sauvées."; }
            //else
            //{
            //    if (_answerList.USResidentOrBorn && isUSResident)
            //    {
            //        sMessage = (!string.IsNullOrEmpty(auth.KycW9FileUpdated)) ? US_DocUploaded.Replace("'", "&apos;") : US_DocNotUploaded.Replace("'", "&apos;");
            //        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "KYC_SaveTaxResidence", "ShowDialogMessage('', '" + sMessage + "', '600', 'Default.aspx');", true);
            //    }
            //    else if (panelTaxResidenceList.Visible && !isUeOrAele)
            //    {
            //        sMessage = AEOI_NotUeOrAele.Replace("@@CLOSEDATE", ((!string.IsNullOrEmpty(CloseDate)) ? "le " + CloseDate : "")).Replace("'", "&apos;");
            //        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "KYC_SaveTaxResidence", "ShowDialogMessage('', '" + sMessage + "', '600', 'Default.aspx');", true);
            //    }
            //}
        }
        else
        {
            sMessage = "<b>Les KYC ne sont pas complets.</b><br/>" + sMessage;
        }

        return isOK;
    }

    public static string getStringValue(string sValue)
    {
        string sStringValue = "";

        if(sValue != null && sValue.Trim().Length > 0)
        {
            sStringValue = sValue;
        }

        return sStringValue;
    }

    public static bool SetXMLCustomerInformations(string sXmlIN)
    {
        bool bSaved = false;
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_SetCustomer", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);

            cmd.Parameters["@IN"].Value = sXmlIN;
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOUT = cmd.Parameters["@OUT"].Value.ToString();

            if (sXmlOUT.Length > 0)
            {
                List<string> listRC = CommonMethod.GetAttributeValues(sXmlOUT, "ALL_XML_OUT/Result", "RC");
                List<string> listUpdated = CommonMethod.GetAttributeValues(sXmlOUT, "ALL_XML_OUT/Result", "Updated");

                if (listRC.Count > 0)
                {
                    if (listRC[0] == "0")
                    {
                        if (listUpdated.Count > 0 && listUpdated[0] == "1")
                            bSaved = true;
                    }
                    else if (listRC[0] == "71008")
                    {
                        //UpdateMessage("Session terminée. Veuillez vous reconnecter.");
                    }
                    //else UpdateMessage("<span class=\"font-red\">Une erreur est survenue.</span>");
                }
            }
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return bSaved;
    }

    public static bool SetKycMandatory(int RefCustomer, int InitialRefCustomer)
    {
        bool isOK = false;

        XElement xCust = new XElement("Customer", new XAttribute("RefCustomer", RefCustomer.ToString()), new XAttribute("InitialRefCustomer", InitialRefCustomer.ToString()), new XAttribute("CashierToken", "HMB"));
        xCust.Add(new XAttribute("Mandatory", "1"));

        isOK = SetXMLCustomerInformations(new XElement("ALL_XML_IN", xCust).ToString());

        return isOK;
    }

    public static DataTable getResidenceTaxValues()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("ISO2");
        dt.Columns.Add("CountryName");

        dt.Rows.Add(new object[] { "FR", "France uniquement" });
        dt.Rows.Add(new object[] { "XF", "France plus autre(s) pays" });
        dt.Rows.Add(new object[] { "XX", "Autre(s) pays uniquement" });

        return dt;
    }

    public static DataTable GetAeoiCountryList()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("ISO2");
        dt.Columns.Add("CountryName");

        SqlConnection conn = null;
        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK2"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("[web].[P_GetAEOICountryList]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;
            da.Fill(dt);
        }
        catch (Exception ex)
        {

        }

        //TEST
        //dt.Rows.Add(new object[] { "PT","PORTUGAL","01/01/2017" });

        return dt;
    }

    public static string GetAeoiCountryListToString(DataTable dtAEOICountryList)
    {
        string sCountryList = "";

        foreach (DataRow dr in dtAEOICountryList.Rows)
        {
            sCountryList += dr["CountryName"] + ";";
        }

        if (!string.IsNullOrWhiteSpace(sCountryList)) { sCountryList = sCountryList.Substring(0, (sCountryList.LastIndexOf(';'))); }

        return sCountryList;
    }

    private static bool  CheckTaxResidenceList(AnswerList _answerList, out string sMessage)
    {
        bool isOK = false;
        sMessage = "";

        if(_answerList != null)
        {
            try
            {
                //Vérification des pays autres que France et US
                if (_answerList.TaxResidentFrOnly != null)
                {
                    if (_answerList.TaxResidentFrOnly == false)
                    {
                        if (_answerList.TaxResidenceList.Rows.Count > 0)
                        {
                            List<string> lsTaxResidenceForbidden = new List<string>(new string[] { "US" });

                            if (_answerList.TaxResidenceList.Rows.Count == 1)
                            {
                                foreach (DataRow dr in _answerList.TaxResidenceList.Rows)
                                {
                                    if (!lsTaxResidenceForbidden.Contains(_answerList.TaxResidenceList.Rows[0]["ISO2"].ToString().ToUpper().Trim()) &&
                                        _answerList.TaxResidenceList.Rows[0]["ISO2"].ToString().ToUpper().Trim() != "FR")
                                    {
                                        isOK = true;
                                    }
                                }
                            }
                            else if (_answerList.TaxResidenceList.Rows.Count > 1)
                            {
                                bool bIsForbiddenCountry = false;
                                foreach (DataRow dr in _answerList.TaxResidenceList.Rows)
                                {
                                    if (lsTaxResidenceForbidden.Contains(_answerList.TaxResidenceList.Rows[0]["ISO2"].ToString().ToUpper().Trim()))
                                    {
                                        bIsForbiddenCountry = true;
                                    }
                                }

                                if (!bIsForbiddenCountry) { isOK = true; }
                            }
                            else
                            {
                                isOK = false;
                                sMessage += "<li>Liste de résidence fiscale non valide</li>";
                            }
                        }
                        else
                        {
                            isOK = false;
                            sMessage += "<li>Liste de résidence fiscale non valide</li>";
                        }
                    }
                    else
                    {
                        isOK = true;
                    }
                }
                else
                {
                    isOK = false;
                    sMessage += "<li>Indiquez si résident français uniquement</li>";
                }

            }
            catch(Exception ex)
            {
                isOK = false;
            }
        }

        if(!isOK && _answerList.USResident == true) { isOK = true; }

        return isOK;
    }

    private static bool CheckUsResidentOrBorn(AnswerList _answerList, out string sMessage)
    {
        bool isOK = true;
        sMessage = "";

        if (_answerList.USResidentOrBorn != null)
        {
            if (_answerList.USResidentOrBorn == true)
            {
                if (_answerList.USBorn != null && _answerList.USResident != null)
                {
                    if (_answerList.USBorn == true && string.IsNullOrWhiteSpace(_answerList.W9Filename))
                    {
                        isOK = false;
                        sMessage += "<li>Envoyez un formulaire W9 ou tout autre justificatif</li>";
                    }
                }
                else
                {
                    isOK = false;
                    sMessage += "<li>Indiquez si résident ou né US</li>";
                }
            }
            else if (_answerList.FatcaAlert && _answerList.USResidentOrBorn == false && string.IsNullOrWhiteSpace(_answerList.W8Filename))
            {
                isOK = false;
                sMessage += "<li>Envoyez un formulaire W8</li>";
            }
        }
        else
        {
            isOK = false;
            sMessage += "<li>Indiquez si citoyen, résident ou né US</li>";
        }

        return isOK;
    }

    public static bool isAeoiCountry(DataTable dtAeoiList, string sISO2)
    {
        bool isOK = false;

        foreach (DataRow dr in dtAeoiList.Rows)
        {
            if(dr["ISO2"].ToString().Trim().ToUpper() == sISO2.Trim().ToUpper())
            {
                isOK = true;
            }
        }

        return isOK;
    }
}