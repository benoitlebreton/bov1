﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using XMLMethodLibrary;

public partial class Tools_SaveClientFile : System.Web.UI.Page
{
    protected class SaveFileResponse
    {
        public int rc;
        public string errormessage;
        public string savedfilename;
        public string originalfilename;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        string sOriginalFileName = "", sFileTmpName = "", sContentType = "", sErrorMessage = "";
        int iRC = 0;

        authentication auth = authentication.GetCurrent(false);
        if (auth != null && !String.IsNullOrEmpty(authentication.GetCurrent(false).sToken))
        {
            string sUploadPath = HttpContext.Current.Server.MapPath("~/UploadTmp");

            // Remove untreated files
            ClientFile.RemoveUntreatedDocs(sUploadPath, 15);

            if (HttpContext.Current.Request.Params["RefCustomer"] != null
                && HttpContext.Current.Request.Files["Files"] != null
                && HttpContext.Current.Request.Params["DocCategoryTAG"] != null)
            {
                int iRefCustomer = int.Parse(HttpContext.Current.Request.Params["RefCustomer"].ToString());
                HttpPostedFile files = HttpContext.Current.Request.Files["Files"];
                string sDocCategoryTAG = HttpContext.Current.Request.Params["DocCategoryTAG"].ToString();
                string sDescription = HttpContext.Current.Request.Params["Description"].ToString();
                bool bOverwrite = (HttpContext.Current.Request.Params["Overwrite"] != null) ? bool.Parse(HttpContext.Current.Request.Params["Overwrite"].ToString()) : false;

                if (ClientFile.SaveFileTmp(files, out sOriginalFileName, out sFileTmpName, out sContentType, out iRC, out sErrorMessage))
                {
                    if(ClientFile.SaveFileCustomerDir(iRefCustomer, "/UploadTmp/", sFileTmpName, sOriginalFileName, bOverwrite, true, out iRC, out sErrorMessage))
                    {
                        if (!ClientFile.SaveFileDB(auth.sToken, iRefCustomer, sContentType, sOriginalFileName, sDocCategoryTAG, sDescription))
                        {
                            iRC = 9994;
                            sErrorMessage = "Erreur d'enregistrement";
                        }
                    }
                }
            }
            else
            {
                iRC = 9997;
                sErrorMessage = "Paramètres manquants";
            }
        }
        else
        {
            iRC = 9996;
            sErrorMessage = "Echec authentification";
        }

        SaveFileResponse resp = new SaveFileResponse();
        resp.rc = iRC;
        resp.errormessage = sErrorMessage;
        resp.savedfilename = sFileTmpName;
        resp.originalfilename = sOriginalFileName;

        Response.Clear();
        Response.ContentType = "application/json; charset=utf-8";
        Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(resp));
        Response.End();
    }

}