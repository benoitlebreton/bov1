﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Tools_Upload_FATCA_AEOI : System.Web.UI.Page
{
    [Serializable]
    protected class SaveFileResponse
    {
        public int rc;
        /*
        public string errormessage;
        public string savedfilename;
        public string originalfilename;
        */
        public string W9FileName;
        public string W8FileName;
        public string LossUSNationalityCertificate;
        public string FatcaAutoCertificationFileName;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        var httpPostedFile = HttpContext.Current.Request.Files["UploadedImage"];
        if (httpPostedFile != null)
        {
            if (!string.IsNullOrWhiteSpace(Request.Params.Get("type")) && !string.IsNullOrWhiteSpace(Request.Params.Get("ref")))
            {
                string sType = Request.Params.Get("type").Trim();
                string sRefCustomer = Request.Params.Get("ref").Trim();

                int iRefCustomer;

                if (int.TryParse(sRefCustomer, out iRefCustomer) && iRefCustomer > 0)
                {

                    string filename = sType + "_" + DateTime.Now.ToString("yyyyMMdd") + Path.GetExtension(httpPostedFile.FileName);
                    string uploadTmpPath = "/UploadTmp/";
                    string uploadTmpServerPath = HttpContext.Current.Server.MapPath("~/" + uploadTmpPath);

                    var fileSavePath = Path.Combine(uploadTmpServerPath, filename);

                    // Save the uploaded file to "UploadedFiles" folder
                    httpPostedFile.SaveAs(fileSavePath);

                    List<string> lsFatcaTypeFile = new List<string>(new string[] { "image/jpg", "image/jpeg", "application/pdf" });
                    int iMaxSize = 2100000; // 2Mo en bytes

                    int rcSaveFile = -1;
                    string messSaveFile = "";
                    string W9FileName = "";
                    string W8FileName = "";
                    string FatcaAutoCertificationFileName = "";
                    string LossUSNationalityCertificate = "";

                    bool isTypeOK = true;
                    string sDBFileType = "";
                    switch (sType.Trim().ToLower())
                    {
                        case "w9":
                            sDBFileType = "W9";
                            W9FileName = filename;
                            break;
                        case "lossusnationalitycertificate":
                            sDBFileType = "LOSSUSNATI";
                            LossUSNationalityCertificate = filename;
                            break;
                        case "w8":
                            W8FileName = filename;
                            sDBFileType = "W8";
                            break;
                        case "fatcaautocertification":
                            FatcaAutoCertificationFileName = filename;
                            sDBFileType = "FATCAACERT";
                            break;
                        default:
                            isTypeOK = false;
                            W8FileName = "";
                            W9FileName = "";
                            FatcaAutoCertificationFileName = "";
                            LossUSNationalityCertificate = "";
                            break;
                    }

                    if (!(isTypeOK &&
                        lsFatcaTypeFile.Contains(httpPostedFile.ContentType) && httpPostedFile.ContentLength <= iMaxSize &&
                        ClientFile.SaveFileCustomerDir(iRefCustomer, uploadTmpPath, filename, filename, true, true, out rcSaveFile, out messSaveFile) && rcSaveFile == 0 &&
                        ClientFile.SaveFileDB("HMB", iRefCustomer, httpPostedFile.ContentType, filename, sDBFileType, "")))
                    {
                        tools.LogToFile("Message = " + messSaveFile + '\n' + " RC = " + rcSaveFile.ToString(), "ClientFile.SaveFileCustomerDir");
                        tools.LogToFile("Message = " + messSaveFile + '\n' + " RC = " + rcSaveFile.ToString(), "ClientFile.SaveFileDB");

                        W8FileName = "";
                        W9FileName = "";
                        FatcaAutoCertificationFileName = "";
                        LossUSNationalityCertificate = "";
                        ReturnError();
                    }
                    else
                    {
                        SaveFileResponse resp = new SaveFileResponse();
                        resp.rc = 0;
                        resp.W8FileName = W8FileName;
                        resp.W9FileName = W9FileName;
                        resp.FatcaAutoCertificationFileName = FatcaAutoCertificationFileName;
                        resp.LossUSNationalityCertificate = LossUSNationalityCertificate;

                        Response.Clear();
                        Response.ContentType = "application/json; charset=utf-8";
                        Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(resp));
                        Response.End();
                    }
                }
                else ReturnError();
            }
            else ReturnError();
        }
        else ReturnError();
    }

    protected void ReturnError()
    {
        Response.Clear();
        Response.ContentType = "application/json; charset=utf-8";
        Response.StatusCode = 400;
        Response.Write("");
        Response.End();
    }
}