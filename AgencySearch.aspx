﻿<%@ Page Title="Recherche agence - BO Compte-Nickel" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="AgencySearch.aspx.cs" Inherits="AgencySearch" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContent" Runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $('#<%=txtSearch.ClientID%>').focus().watermark("ID agence, nom agence, nom/prénom gérant, S/N borne, S/N TPE, ...");
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" Runat="Server">
    <asp:Panel ID="panelSearch" runat="server" DefaultButton="btnSearch">
        <h2 style="color:#344b56; margin-bottom:10px;text-transform:uppercase;">
            Recherche agence
        </h2>
        <div style="margin: 20px auto; width: 90%">
            <div>
                <div class="table" style="width:100%">
                    <div class="row">
                        <div class="cell" style="padding-left:0; vertical-align:middle">
                            <asp:TextBox ID="txtSearch" runat="server" autocomplete="off" MaxLength="200" style="width:100%;height:30px;"></asp:TextBox>
                        </div>
                        <div class="cell" style="width:1px;padding-left:10px;vertical-align:middle; padding-top:7px;">
                            <asp:Button ID="btnSearch" runat="server" CssClass="button" Text="Rechercher" OnClick="btnSearch_Click" />
                        </div>
                    </div>
                </div>
            </div>
            <asp:UpdateProgress ID="upSearchResultLoading" runat="server" AssociatedUpdatePanelID="upSearchResult">
                <ProgressTemplate>
                    <div style="text-align:center; position:absolute;left:45%;top:50%">
                        <img src="Styles/Img/loading.gif" alt="loading..." />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:UpdatePanel ID="upSearchResult" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Panel ID="panelSearchResultNb" runat="server" Visible="false" style="margin-top:10px;">
                        <asp:Label ID="lblSearchResultNb" runat="server" style="font-weight:bold"></asp:Label>
                    </asp:Panel>
                    <asp:Repeater ID="rptSearchResult" runat="server">
                        <ItemTemplate>
                            <div style="margin-top:10px;" class="searchResultBox"
                                onclick="document.location.href='AgencyDetails.aspx?ref=<%#Eval("A_REF") %>'">
                                <div style="display:flex;flex-direction:row;align-items:center;justify-content:space-between;">
                                    <div>
                                        <h3 style="font-family:Arial;font-size:1.2em;font-weight:lighter;color:#ff6a00; margin:0; padding:0;"><%# tools.getHighlightedSearchValue(Eval("A_ID").ToString(), txtSearch.Text) %> - 
                                            <%# tools.getHighlightedSearchValue(Eval("A_Name").ToString(), txtSearch.Text) %>
                                            <br />
                                            <span style="position:relative;top:-4px"><%# tools.getHighlightedSearchValue(Eval("A_Name2").ToString(), txtSearch.Text) %></span>
                                        </h3>
                                        <div style="font-size:1em;">
                                            <%# tools.getHighlightedSearchValue(Eval("A_ManLastName").ToString(), txtSearch.Text) %>
                                            <%# tools.getHighlightedSearchValue(Eval("A_ManFirstName").ToString(), txtSearch.Text) %>
                                            <br />
                                            Région commerciale : <%# tools.getHighlightedSearchValue(Agency.getSearchValueFromMPAD(Eval("A_Area").ToString(), txtSearch.Text), txtSearch.Text) %>
                                            <br />
                                            TPE S/N : <%# tools.getHighlightedSearchValue(Agency.getSearchValueFromPOS(Eval("POS_SerialList").ToString(), txtSearch.Text), txtSearch.Text) %>
                                            <br />
                                            MPAD S/N : <%# tools.getHighlightedSearchValue(Agency.getSearchValueFromMPAD(Eval("MP_SerialList").ToString(), txtSearch.Text), txtSearch.Text) %>                                            
                                        </div>
                                    </div>
                                    <div style="justify-content:center;text-transform:uppercase;color:#f00;font-weight:bold;font-size:20px;text-align:right;padding-right:20px;width:230px;">
                                        <asp:Panel ID="panelIncidentsKYC" runat="server" Visible='<%#(Eval("NBIncidentsKYC").ToString() != "0") ? true : false%>'>
                                            <div style="display:flex;flex-direction:row;align-items:center;">
                                                <div>
                                                    <%#Eval("NBIncidentsKYC") %>
                                                    alerte<asp:Literal ID="litS" runat="server" Visible='<%#(Eval("NBIncidentsKYC").ToString() != "1" && Eval("NBIncidentsKYC").ToString() != "0") ? true : false%>'>s</asp:Literal>
                                                    KYC en cours
                                                </div>
                                                <div style="padding-left:15px">
                                                    <img src="Styles/Img/important-check.png" height="45px" />
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="click"/>
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </asp:Panel>
</asp:Content>

