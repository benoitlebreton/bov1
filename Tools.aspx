﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Tools.aspx.cs" Inherits="Tools" %>

<%@ Register Src="~/API/MessageWithCharCounter.ascx" TagName="MessageWithCharCounter" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">

    <script type="text/javascript">
        $(document).ready(function () {
            $(".tools-tabs").tabs({
                activate: function (event, ui) {
                    $('#<%=hdnActiveTab.ClientID%>').val($(".tools-tabs").tabs("option", "active"));

                        // SERVICE LOCK
                        //if ($('#<%=panelServiceLock.ClientID%>') != null && $('#<%=panelServiceLock.ClientID%>').is(':visible')) {
                        //InitLimitsBar();
                        //}
                    }
            });

            $('#popup-edit').dialog({
                autoOpen: false,
                draggable: false,
                resizable: false,
                width: 'auto',
                minWidth: 500,
                dialogClass: "no-close",
                modal: true,
                title: 'Modification',
                buttons: {
                    Ok: function () {
                        $('#<%=btnSaveChanges.ClientID%>').click();
                        $(this).dialog("close");
                    },
                    Annuler: function () {
                        $(this).dialog("close");
                    }
                }
            });
            $('#popup-edit').parent().appendTo(jQuery("form:first"));

            var datepickerOptions = {
                defaultDate: "+0d",
                dateFormat: "dd/mm/yy",
                dayNamesMin: ["Di", "Lu", "Ma", "Me", "Je", "Ve", "Sa"],
                monthNames: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"],
                firstDay: 1,
                //changeMonth: true,
                numberOfMonths: 1,
                minDate: "-0d",
                beforeShow: function (input, inst) {
                    setTimeout(function () {
                        inst.dpDiv.position({ my: 'left top', at: 'left bottom', of: input });
                    }, 0);
                }
            };

            $('#<%=txtDateFromProd.ClientID%>').datepicker(datepickerOptions);
            $('#<%=txtDateFromProd.ClientID%>').datepicker('option', 'onClose', function (selectedDate) {
                if(selectedDate.length > 0)
                    $("#<%=txtDateToProd.ClientID %>").datepicker("option", "minDate", selectedDate);
            });
            $('#<%=txtDateToProd.ClientID%>').datepicker(datepickerOptions);
            $("#<%=txtDateToProd.ClientID %>").datepicker("option", "minDate", "+0d");
            $('#<%=txtDateFromTest.ClientID%>').datepicker(datepickerOptions);
            $('#<%=txtDateFromTest.ClientID%>').datepicker('option', 'onClose', function (selectedDate) {
                if (selectedDate.length > 0)
                    $("#<%=txtDateToTest.ClientID %>").datepicker("option", "minDate", selectedDate);
            });
            $('#<%=txtDateToTest.ClientID%>').datepicker(datepickerOptions);
            $("#<%=txtDateToTest.ClientID %>").datepicker("option", "minDate", "+0d");

            $('input:radio[name="ctl00$MainContent$rblAvailableTest"], input:radio[name="ctl00$MainContent$rblAvailableProd"]').bind('click', function () {
                if ($('input:radio[name="ctl00$MainContent$rblAvailableTest"]').filter('[value="0"]').is(':checked'))
                    $('.div-test .div-delay').show();
                else $('.div-test .div-delay').hide();
                if ($('input:radio[name="ctl00$MainContent$rblAvailableProd"]').filter('[value="0"]').is(':checked'))
                    $('.div-prod .div-delay').show();
                else $('.div-prod .div-delay').hide();
            });

            $('.div-delay input[type=checkbox]').bind('click', function () {
                //console.log($(this).is(':checked'));
                if ($(this).is(':checked')) {
                    $(this).parents('.div-delay').find('input[type=text], select').attr('disabled', false);
                }
                else {
                    $(this).parents('.div-delay').find('input[type=text]').val('');
                    $(this).parents('.div-delay').find('select option').attr('selected', false);
                    $(this).parents('.div-delay').find('select option:first').attr('selected', true);
                    $(this).parents('.div-delay').find('input[type=text], select').attr('disabled', true);
                }
            });

            if ($('#<%=txtIdPRO.ClientID%>') != null) {
                $('#<%=txtIdPRO.ClientID%>').mask("999-999-9");
            }

            $('.RadioButtonList').buttonset();
        });

        function ToolsAlertMessage() {
            hideLoading();
            $('#popup-ToolsMessage').dialog({
                autoOpen: true,
                draggable: false,
                resizable: false,
                width: 'auto',
                minWidth: 500,
                dialogClass: "no-close",
                modal: true,
                title: 'GATEWAY - DEMANDE DE CHANGEMENT DE MODE',
                buttons: {
                    OK: function () {
                        $(this).dialog("close");
                        $(this).dialog("destroy");
                    }
                }
            });
        }

        function MessageToHTML(title, val) {
            if (title == null)
                title = $('#<%=txtTitle.ClientID%>').val();
            if (val == null)
                val = $('#<%=txtMessage.ClientID%>').val();
            if (val.length > 0) {
                val = val.replace(/\\'/g, "'");
                var result = XBBCODE.process({
                    text: val,
                    removeMisalignedTags: false,
                    addInLineBreaks: true
                });

                $('#message-to-html').empty().append('<div class="message-title">' + title + '</div>' + result.html);

                $('#message-to-html').dialog({
                    draggable: false,
                    resizable: false,
                    width: 800,
                    dialogClass: "no-close",
                    modal: true,
                    title: 'Aperçu',
                    buttons: {
                        Ok: function () {
                            $(this).dialog("close");
                        }
                    }
                });
            }
        }
        function AddTag(tagStart, tagEnd) {
            var textTmp = "Votre texte ici";
            var caretPos = doGetCaretPosition($('#<%=txtMessage.ClientID%>')[0]);
            //console.log(caretPos);

            var oldVal = $('#<%=txtMessage.ClientID%>').val();
            var insertText = tagStart + textTmp + tagEnd;
            var newVal = [oldVal.slice(0, caretPos), insertText, oldVal.slice(caretPos)].join('');

            $('#<%=txtMessage.ClientID%>').val(newVal);
            var indexStart = caretPos + tagStart.length;
            $('#<%=txtMessage.ClientID%>').selectRange(indexStart, indexStart + textTmp.length);
        }

        function Edit_Click(ref) {
            $('#<%=hfEditRef.ClientID%>').val(ref);
            $('#<%=hfEditType.ClientID%>').val($('#service-' + ref + ' .hdnType').val());
            $('#<%=btnEditUpdate.ClientID%>').click();
        }
        function ShowEditPopup(ref) {
            $('.RadioButtonList').buttonset();

            var type = $('#service-' + ref + ' .hdnType').val();
            var lbl = $('#service-' + ref + ' .hdnLabel').val();
            var prod = $('#service-' + ref + ' .hdnProdSts').val();
            var test = $('#service-' + ref + ' .hdnTestSts').val();
            var ttl = $('#service-' + ref + ' .hdnTitle').val();
            var msg = $('#service-' + ref + ' .hdnMessage').val();

            //console.log(type + "-" + prod.toLowerCase() + "-" + test.toLowerCase() + "-" + msg);
            //console.log($('input:radio[name="ctl00$MainContent$rblAvailableProd"]').filter('[value="1"]'));

            $('#<%=hdnType.ClientID%>').val(type);

            $('input:radio[name="ctl00$MainContent$rblAvailableProd"]').prop('checked', true);
            try { $('input:radio[name="ctl00$MainContent$rblAvailableProd"]').filter('[value="' + prod + '"]').prop('checked', true); }
            catch(ex) { $('input:radio[name="ctl00$MainContent$rblAvailableProd"]').filter('[value="0"]').prop('checked', true); }

            $('input:radio[name="ctl00$MainContent$rblAvailableTest"]').prop('checked', true);
            try { $('input:radio[name="ctl00$MainContent$rblAvailableTest"]').filter('[value="' + test + '"]').prop('checked', true); }
            catch (ex) { $('input:radio[name="ctl00$MainContent$rblAvailableTest"]').filter('[value="0"]').prop('checked', true); }

            $('#<%=txtTitle.ClientID%>').val(ttl);
            $('#<%=txtMessage.ClientID%>').val(msg);

            $('#popup-edit').dialog({ title: lbl });
            $('#popup-edit').dialog('open');

            $('.RadioButtonList').buttonset('refresh');

        }
        function HideEditPopup() {
            $('#popup-edit').dialog('close');
        }

        function doGetCaretPosition(ctrl) {
            var CaretPos = 0;
            // IE Support
            if (document.selection) {
                ctrl.focus();
                var Sel = document.selection.createRange();
                Sel.moveStart('character', -ctrl.value.length);
                CaretPos = Sel.text.length;
            }
                // Firefox support
            else if (ctrl.selectionStart || ctrl.selectionStart == '0')
                CaretPos = ctrl.selectionStart;
            return (CaretPos);
        }

        function ToggleOperationDetails(row) {
            if (!$(row).next('tr').find('.row-details').is(':visible')) {
                $(row).find('.detail-arrow').attr('src', $(row).find('.detail-arrow').attr('src').replace(/up/g, 'down'));
                $(row).next('tr').find('.row-details').slideDown(400);
            }
            else {
                $(row).next('tr').find('.row-details').slideUp(400);
                $(row).find('.detail-arrow').attr('src', $(row).find('.detail-arrow').attr('src').replace(/down/g, 'up'));
            }
        }
    </script>
    <script type="text/javascript">
        function onChangeGatewayMode() {
            if ($('#<%=rblGatewayMode.ClientID%>').find(":checked").val().trim().length > 0) {
                //console.log($('#<%=rblGatewayMode.ClientID%>').find(":checked").next().html());
                $('#lblGatewayModeRequestedConfirmation').html($('#<%=rblGatewayMode.ClientID%>').find(":checked").next().html());
                $('#popup-gatewayModeChange').dialog({
                    autoOpen: true,
                    draggable: false,
                    resizable: false,
                    width: 'auto',
                    minWidth: 500,
                    dialogClass: "no-close",
                    modal: true,
                    title: 'GATEWAY - DEMANDE DE CHANGEMENT DE MODE',
                    buttons: {
                        Confirmer: function () {
                            $('#<%=btnSaveGatewayModeChange.ClientID%>').click();
                            showLoading();
                            $(this).dialog("close");
                            $(this).dialog("destroy");
                        },
                        Annuler: function () {
                            $('#<%=rblGatewayMode.ClientID%> input[type=radio]').prop('checked', false);
                            $('#<%=rblGatewayMode.ClientID %>').find("input[value='"+$('#<%=hfGatewayCurrentMode.ClientID%>').val()+"']").attr("checked", "checked");
                            $('.RadioButtonList').buttonset();
                            $(this).dialog("close");
                            $(this).dialog("destroy");
                        }
                    }
                });
                $('#popup-gatewayModeChange').parent().appendTo(jQuery("form:first"));
            }
        }

        function showMoreGatewayStatusDetails() {
            $('#btnShowMoreGatewayStatusDetails').hide();
            $('#divGatewayStatusDetails table tr.notVisible').show();
        }
    </script>

    <style type="text/css">

        .rpt-service {
            text-transform:initial;
            width:48%;
            margin:0 1%;
            float:left;

            box-shadow: 1px 2px 5px #C9C9C9;
            -moz-box-shadow: 1px 2px 5px #C9C9C9;
            -webkit-box-shadow: 1px 2px 5px #C9C9C9;
            -o-box-shadow: 1px 2px 5px #C9C9C9;
            /*border:1px solid #ccc;
            border-radius:4px;*/
            background-color:#fff;
            border:0;
        }
        .rpt-service-content {
            margin:10px;
            position:relative;
        }
        .service-edit {
            position:absolute;
            right:0px;
            top:0px;
            height:30px;
            cursor:pointer;
        }
        .service-type {
            margin:10px 0 5px 0;
            border-bottom:1px solid #ddd;
        }
        .service-type span {
            background-color:#fff;
            position:relative;
            top:8px;
            padding:0 5px;
        }
        .service-label {
            text-align:center;
            text-transform:uppercase;
            font-size:1.2em;
            font-weight:bold;
        }
        .service-active, .service-inactive, .service-active-without-operation, .service-active-without-SAB,.service-inactive-without-BDD {
            text-transform:uppercase;
            font-weight:bold;
        }
        .service-active {
            color:green;
        }
        .service-inactive,.service-inactive-without-BDD {
            color:red;
        }

        .service-active-without-operation {
            color:rgb(255, 203, 0);
            /*text-shadow: 0 0 2px #000;
            -moz-text-shadow: 0 0 2px #000;
            -webkit-text-shadow: 0 0 2px #000;*/
        }

        .service-active-without-SAB {
            color:rgb(255, 101, 0);
            /*text-shadow: 0 0 2px #000;
            -moz-text-shadow: 0 0 2px #000;
            -webkit-text-shadow: 0 0 2px #000;*/
        }

        .service-message {
            text-align:center;
            margin-top:15px;
        }
        .service-message span {
            cursor:pointer;
            color:blue;
        }
        .service-message span:hover {
            text-decoration:underline;
        }

        #ar-loading {
            background:url('Styles/img/loading2.gif') no-repeat center;
            background-size: 35px 27px;
            display:none;
            position:absolute;
            background-color:rgba(255, 255, 255, 0.5);
            z-index:10000;
        }

        .format input{
            font-family:Arial;
            font-size: 12px;
        }

        .format input[type=button].big-size {
            font-size:20px;
        }

        .format input[type=button].small-size {
            font-size: 10px;
        }

        .ErrorValues span{
            color:red;
            font-weight:bold;
        }
    </style>

    <script type="text/javascript">
        function onChangeSmsWay() {
            if ($('#<%=rblSmsProvider.ClientID%>').find(":checked").val().trim().length > 0) {
                $('#<%=lblSmsCurrentWayConfirmation.ClientID%>').html($('#<%=lblSmsCurrentProvider.ClientID%>').html());
                $('#lblSmsWayRequestedConfirmation').html($('#<%=rblSmsProvider.ClientID%>').find(":checked").next().html());
                //console.log($('#<%=rblSmsProvider.ClientID%>').find(":checked").next().html());
                $('#popup-SmsWayChange').dialog({
                    autoOpen: true,
                    draggable: false,
                    resizable: false,
                    width: 'auto',
                    minWidth: 500,
                    dialogClass: "no-close",
                    modal: true,
                    title: 'SMS - DEMANDE DE CHANGEMENT D&apos;ENVOI',
                    buttons: {
                        Confirmer: function () {
                            $('#<%=btnSaveSmsWayChange.ClientID%>').click();
                            showLoading();
                            $(this).dialog("close");
                            $(this).dialog("destroy");
                        },
                        Annuler: function () {
                            $('#<%=rblSmsProvider.ClientID%> input[type=radio]').prop('checked', false);
                            $('#<%=rblSmsProvider.ClientID %>').find("input[value='"+$('#<%=lblSmsCurrentProvider.ClientID%>').html()+"']").attr("checked", "checked");
                            $('.RadioButtonList').buttonset();
                            $(this).dialog("close");
                            $(this).dialog("destroy");
                        }
                    }
                });
                $('#popup-SmsWayChange').parent().appendTo(jQuery("form:first"));

                return false;
            }
        }
    </script>

    <script type="text/javascript">
        function onChangeOnUsStatus() {
            if ($('#<%=rblOnUsStatus.ClientID%>').find(":checked").val().trim().length > 0) {
                
                $('#lblOnUsStatusRequestedConfirmation').html($('#<%=rblOnUsStatus.ClientID%>').find(":checked").next().html());
                $('#popup-OnUsStatusChange').dialog({
                    autoOpen: true,
                        draggable: false,
                        resizable: false,
                        width: 'auto',
                        minWidth: 500,
                        dialogClass: "no-close",
                        modal: true,
                        title: 'ON US - DEMANDE DE CHANGEMENT DE STATUT',
                        buttons: {
                    Confirmer: function () {
                    $('#<%=btnSaveOnUsStatusChange.ClientID%>').click();
                            showLoading();
                                $(this).dialog("close");
                                $(this).dialog("destroy");
                            },
                            Annuler: function () {
                                $('#<%=rblOnUsStatus.ClientID%> input[type=radio]').prop('checked', false);
                                $('#<%=rblOnUsStatus.ClientID %>').find("input[value='"+$('#<%=hfOnUsCurrentStatus.ClientID%>').val()+"']").attr("checked", "checked");
                                $('.RadioButtonList').buttonset();
                                $(this).dialog("close");
                                $(this).dialog("destroy");
                            }
                        }
                    });
                $('#popup-OnUsStatusChange').parent().appendTo(jQuery("form:first"));
            }
        }

        function OnUsSmsClick() {
            $('.RadioButtonList').buttonset();
            $('#popup-OnUsSms').dialog({
                autoOpen: true,
                draggable: false,
                resizable: false,
                width: 'auto',
                minWidth: 500,
                dialogClass: "no-close",
                modal: true,
                title: 'ON US - ENVOI SMS AUX BURALISTES',
                buttons: {
                    Confirmer: function () {
                                showLoading();
                                $('#<%=btnOnUsSendSMS.ClientID%>').click();
                                $(this).dialog("close");
                                $(this).dialog("destroy");
                            },
                            Annuler: function () {
                                $(this).dialog("close");
                                $(this).dialog("destroy");
                            }
                }
            });
            $('#popup-OnUsSms').parent().appendTo(jQuery("form:first"));
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

    <div id="ar-loading"></div>

    <h2 style="color: #344b56;text-transform:uppercase; margin-bottom: 10px; margin-top:10px; white-space:nowrap">Outils</h2>

    <div style="border: 3px solid #344b56; border-radius: 8px; width: 100%; margin: auto;margin-bottom:20px; text-transform:uppercase" class="tools-tabs">

        <asp:Repeater ID="rptTabs" runat="server">
            <HeaderTemplate>
                <ul>
            </HeaderTemplate>
            <ItemTemplate>
                <%# Eval("tab") %>
            </ItemTemplate>
            <FooterTemplate>
                </ul>
            </FooterTemplate>
        </asp:Repeater>

        <asp:Panel ID="panelServiceLock" runat="server" Visible="false">
            <asp:UpdatePanel ID="upServiceLock" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Repeater ID="rptServices" runat="server">
                        <HeaderTemplate>
                            <div class="rpt-services">
                        </HeaderTemplate>
                        <ItemTemplate>
                                <div id="service-<%#Container.ItemIndex + 1%>" class="rpt-service">
                                    <input type="hidden" class="hdnType" value='<%#Eval("Type") %>' />
                                    <input type="hidden" class="hdnLabel" value='<%#Eval("Label") %>' />
                                    <input type="hidden" class="hdnProdSts" value='<%#Eval("AvailableProd") %>' />
                                    <input type="hidden" class="hdnTestSts" value='<%#Eval("AvailableTest") %>' />
                                    <input type="hidden" id='title-<%#Container.ItemIndex + 1%>' class="hdnTitle" value='<%#Eval("Title") %>' />
                                    <input type="hidden" id='message-<%#Container.ItemIndex + 1%>' class="hdnMessage" value='<%#Eval("Message").ToString().Replace("'","&apos;")%>' />
                                    <div class="rpt-service-content">
                                        <img class="service-edit" alt="Modifier" src="Styles/Img/modify.png" onclick="Edit_Click(<%#Container.ItemIndex + 1%>);" />
                                        <div class="service-label">
                                            <%#Eval("Label") %>
                                        </div>
                                        <div class="service-type">
                                            <div style="float:left;width:50%;">
                                                <span>Production</span>
                                            </div>
                                            <div style="float:right;width:50%;text-align:right">
                                                <%#GetStatusLabel(int.Parse(Eval("AvailableProd").ToString()))%>
                                            </div>
                                            <div style="clear:both"></div>
                                        </div>
                                        <div class="service-type">
                                            <div style="float:left;width:50%;">
                                                <span>Test</span>
                                            </div>
                                            <div style="float:right;width:50%;text-align:right">
                                                <%#GetStatusLabel(int.Parse(Eval("AvailableTest").ToString()))%>
                                            </div>
                                            <div style="clear:both"></div>
                                        </div>
                                        <div class="service-message">
                                            <%#GetMessageLink((Container.ItemIndex + 1).ToString())%>
                                        </div>
                                    </div>
                                </div>
                        </ItemTemplate>
                        <FooterTemplate>
                                <div style="clear:both"></div>
                            </div>
                        </FooterTemplate>
                    </asp:Repeater>
                    <asp:Button ID="btnEditUpdate" runat="server" style="display:none" OnClick="upEdit_Update"/>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnEditUpdate" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>

            <asp:HiddenField ID="hfEditRef" runat="server" />
            <asp:HiddenField ID="hfEditType" runat="server" />

            <div id="message-to-html" style="display:none"></div>
            <div id="popup-edit" style="display:none">
                <asp:UpdatePanel ID="upEdit" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:HiddenField ID="hdnType" runat="server" />
                        <div class="div-prod" style="border-bottom:1px solid #ddd;">
                            <b style="color:#000;position:relative;top:15px">Production</b>
                            <asp:RadioButtonList ID="rblAvailableProd" runat="server" CssClass="RadioButtonList" RepeatDirection="Horizontal" style="float:right; margin-left:50px">
                                <asp:ListItem Value="1" Text="Actif"></asp:ListItem>
                                <asp:ListItem Value="0" Text="Inactif"></asp:ListItem>
                            </asp:RadioButtonList>
                            <div style="clear:both"></div>
                            <asp:Panel ID="panelDelayProd" runat="server" CssClass="div-delay" style="display:none">
                                <div style="margin-top:5px">
                                    <asp:Label ID="lblDelayProd" runat="server" AssociatedControlID="ckbDelayProd" ForeColor="Black" style="position:relative;top:-3px">
                                        Programmer
                                    </asp:Label>
                                    <asp:CheckBox ID="ckbDelayProd" runat="server" />
                                </div>
                                <div class="table">
                                    <div class="table-row">
                                        <div class="table-cell" style="padding:6px 0 5px 0"><span style="color:black">du</span>&nbsp;</div>
                                        <div class="table-cell"><asp:TextBox ID="txtDateFromProd" runat="server" placeholder="JJ/MM/AAAA" Enabled="false" MaxLength="10"></asp:TextBox></div>
                                        <div class="table-cell">
                                            &nbsp;à&nbsp;
                                            <asp:DropDownList ID="ddlHourFromProd" runat="server" DataTextField="time" DataValueField="time" Enabled="false">
                                            </asp:DropDownList>
                                            h
                                            <asp:DropDownList ID="ddlMinFromProd" runat="server" DataTextField="time" DataValueField="time" Enabled="false">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="table-row">
                                        <div class="table-cell" style="padding:5px 0 8px 0"><span style="color:black">au</span>&nbsp;</div>
                                        <div class="table-cell" style="text-align:center"><asp:TextBox ID="txtDateToProd" runat="server" placeholder="JJ/MM/AAAA" Enabled="false" MaxLength="10"></asp:TextBox></div>
                                        <div class="table-cell" style="text-align:center">
                                            &nbsp;à&nbsp;
                                            <asp:DropDownList ID="ddlHourToProd" runat="server" DataTextField="time" DataValueField="time" Enabled="false">
                                            </asp:DropDownList>
                                            h
                                            <asp:DropDownList ID="ddlMinToProd" runat="server" DataTextField="time" DataValueField="time" Enabled="false">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                        <div class="div-test" style="border-bottom:1px solid #ddd;">
                            <b style="color:#000;position:relative;top:15px">Test</b>
                            <asp:RadioButtonList ID="rblAvailableTest" runat="server" CssClass="RadioButtonList" RepeatDirection="Horizontal" style="float:right; margin-left:50px">
                                <asp:ListItem Value="1" Text="Actif"></asp:ListItem>
                                <asp:ListItem Value="0" Text="Inactif"></asp:ListItem>
                            </asp:RadioButtonList>
                            <div style="clear:both"></div>
                            <asp:Panel ID="panelDelayTest" runat="server" CssClass="div-delay" style="display:none">
                                <div style="margin-top:5px">
                                    <asp:Label ID="lblDelayTest" runat="server" AssociatedControlID="ckbDelayTest" ForeColor="Black" style="position:relative;top:-3px">
                                        Programmer
                                    </asp:Label>
                                    <asp:CheckBox ID="ckbDelayTest" runat="server" />
                                </div>
                                <div class="table">
                                    <div class="table-row">
                                        <div class="table-cell" style="padding:6px 0 5px 0"><span style="color:black">du</span>&nbsp;</div>
                                        <div class="table-cell"><asp:TextBox ID="txtDateFromTest" runat="server" placeholder="JJ/MM/AAAA" Enabled="false" MaxLength="10"></asp:TextBox></div>
                                        <div class="table-cell">
                                            &nbsp;à&nbsp;
                                            <asp:DropDownList ID="ddlHourFromTest" runat="server" DataTextField="time" DataValueField="time" Enabled="false">
                                            </asp:DropDownList>
                                            h
                                            <asp:DropDownList ID="ddlMinFromTest" runat="server" DataTextField="time" DataValueField="time" Enabled="false">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="table-row">
                                        <div class="table-cell" style="padding:5px 0 8px 0"><span style="color:black">au</span>&nbsp;</div>
                                        <div class="table-cell" style="text-align:center"><asp:TextBox ID="txtDateToTest" runat="server" placeholder="JJ/MM/AAAA" Enabled="false" MaxLength="10"></asp:TextBox></div>
                                        <div class="table-cell" style="text-align:center">
                                            &nbsp;à&nbsp;
                                            <asp:DropDownList ID="ddlHourToTest" runat="server" DataTextField="time" DataValueField="time" Enabled="false">
                                            </asp:DropDownList>
                                            h
                                            <asp:DropDownList ID="ddlMinToTest" runat="server" DataTextField="time" DataValueField="time" Enabled="false">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                        <div style="margin-top:20px">
                            <div style="margin-bottom:5px"><b style="color:#000">Titre</b></div>
                            <asp:TextBox ID="txtTitle" runat="server" placeholder="MAINTENANCE, ERREUR, etc." style="width:100%"></asp:TextBox>
                        </div>
                        <div style="margin-top:20px">
                            <div style="margin-bottom:5px"><b style="color:#000">Message</b></div>
                            <asp:TextBox ID="txtMessage" runat="server" TextMode="MultiLine" Rows="5" placeholder="Message d'information" style="width:100%"></asp:TextBox>
                        </div>
                        <div>
                            <div class="format" style="float:left">
                                <div>
                                    <input class="big-size" type="button" value="A" onclick="AddTag('[size=40]', '[/size]');" />
                                    <input class="small-size" type="button" value="A" onclick="AddTag('[size=10]', '[/size]');" />
                                    <input type="button" value="Gras" style="font-weight:bold" onclick="AddTag('[b]', '[/b]');" />
                                    <input type="button" value="Italic" style="font-style:italic" onclick="AddTag('[i]', '[/i]');" />
                                    <input type="button" value="Souligné" style="text-decoration:underline" onclick="AddTag('[u]', '[/u]');" />
                                    <input type="button" value="Orange" class="font-orange" onclick="AddTag('[o]', '[/o]');" />
                                </div>
                                <div style="margin-top:5px">
                                    <input class="tab-icon" type="button" value="Tab" onclick="AddTag('[tab]', '[/tab]');" />
                                    <input class="align-left-icon" type="button" value="Gauche" onclick="AddTag('[tal]', '[/tal]');" />
                                    <input class="align-center-icon" type="button" value="Centre" onclick="AddTag('[tac]', '[/tac]');" />
                                    <input class="align-right-icon" type="button" value="Droite" onclick="AddTag('[tar]', '[/tar]');" />
                                </div>
                                <div style="margin-top:3px">
                                    <input class="link-icon" type="button" value="Lien" onclick="AddTag('[url=Votre lien ici]', '[/url]');" />
                                    <input class="image-icon" type="button" value="Image" onclick="AddTag('[img]', '[/img]');" />
                                </div>
                            </div>
                            <div style="clear:both"></div>
                            <div style="margin-top:15px;text-align:center">
                                <input type="button" value="Aperçu du message" onclick="MessageToHTML();" />
                            </div>
                            <div style="clear:both"></div>
                        </div>
                        <asp:Button ID="btnSaveChanges" runat="server" style="display:none" OnClick="btnSaveChanges_Click" />
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSaveChanges" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
            <div id="popup-ChangeGatewayStatus" style="display:none">
                <%--<div class="table" style="width:95%; margin:10px auto">
                    <div class="row">
                        <div class="cell" style="vertical-align:middle; text-align:center">
                            <asp:Label ID="lblGatewayCurrentStatusConfirmation" runat="server"></asp:Label>
                        </div>
                        <div class="cell" style="vertical-align:middle; text-align:center">
                            ->
                        </div>
                        <div style="vertical-align:middle; text-align:center">
                            <asp:Label ID="lblGatewayStatusRequestedConfirmation" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
                <asp:Panel ID="panelGatewayDelegationChangement" runat="server" Visible="false" CssClass="bold italic">
                    Attention! Le changement ne sera effectif que lorsqu'il n'y aura plus de transaction à injecter dans SAB.
                </asp:Panel>
                <div style="margin-top:20px">
                    Confirmez-vous le changement de mode de la Gateway ?
                </div>--%>
            </div>

        </asp:Panel>
        <asp:Panel ID="panelLimits" runat="server" Visible="false">
            <asp:UpdatePanel ID="upLimits" runat="server">
                <ContentTemplate>
                    <asp:Button ID="btnApproveChangeLimits" runat="server" CssClass="button" Text="Accepter les changements de plafond" OnClientClick="showBigLoading('TRAITEMENT EN COURS');" OnClick="btnApproveChangeLimits_Click"
                        style="height:40px" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
        <asp:Panel ID="panelGateway" runat="server" Visible="false">
            <asp:UpdatePanel ID="upGateway" runat="server">
                <ContentTemplate>
                    <div style="height: 40px;" class="table">
                        <div class="row">
                            <div class="cell" style="vertical-align:middle">
                                <asp:Button ID="btnGatewayRefreshMonitoring" runat="server" CssClass="button" Text="Rafraîchir" OnClick="btnGatewayRefreshMonitoring_Click" style="height:40px" />
                            </div>
                            <div class="cell" style="padding-left:10px; vertical-align:middle">
                                <asp:UpdateProgress ID="updateProgressGateway" runat="server" AssociatedUpdatePanelID="upGateway">
                                    <ProgressTemplate>
                                        <img src="Styles/Img/loading.gif" alt="loading" width="25px" height="25px" />
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </div>
                        </div>
                    </div>
                    <asp:Panel ID="panelGatewayStatus" runat="server" Visible="false">
                        <div class="tabSubtitle" style="margin:10px 0;" >Statut</div>
                        <div style="margin:10px 0 20px 0;">
                            <asp:Label ID="lblGatewayStatus" runat="server" style="font-size:1.4em"></asp:Label>
                        </div>
                        <div class="table" style="width:100%; margin-top:10px">
                            <div class="row">
                                <div class="cell" style="width:50%; height:80px">
                                    <div class="bold font-orange">Mode actuel</div>
                                    <div><asp:Label ID="lblGatewayCurrentMode" runat="server"></asp:Label></div>
                                </div>
                                <div>
                                    <asp:Panel ID="panelGatewayChangeMode" runat="server" CssClass="cell" style="height:80px">
                                        <div class="bold font-orange">Demande de changement de mode</div>
                                        <asp:RadioButtonList ID="rblGatewayMode" runat="server" RepeatDirection="Horizontal" CssClass="RadioButtonList" onchange="onChangeGatewayMode();" AutoPostBack="false">
                                            <asp:ListItem Text="Transparent" Value="T"></asp:ListItem>
                                            <asp:ListItem Text="Délégation" Value="D"></asp:ListItem>
                                            <asp:ListItem Text="Hybride" Value="H" Enabled="false"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </asp:Panel>
                                    <asp:HiddenField ID="hfGatewayCurrentMode" runat="server" />
                                    <asp:HiddenField ID="hfGatewayRequestedMode" runat="server" />
                                </div>
                            </div>
                        </div>
                        <div id="divGatewayStatusDetails">
                            <div class="bold font-orange" style="margin-top:10px;">Fonctionnement lors des dernières minutes</div>
                            <asp:Repeater ID="rptGatewayStatusDetails" runat="server">
                                <HeaderTemplate>
                                    <table class="defaultTable3 noBorderDetails" style="width:100%;" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <th>Date</th>
                                        <th>Nb VOIES</th>
                                        <th>Nb AUTHOR</th>
                                        <th>Nb réponses AUTHOR</th>
                                        <th>Nb AVIS</th>
                                        <th>Nb réponses AVIS</th>
                                        <th>Total</th>
                                    </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                <tr class="<%# getGatewayStatusDetailsVisibility(Eval("Row").ToString()) %>">
                                    <td align="left">
                                        <%# Eval("Date") %>
                                    </td>
                                    <td align="center"><%# Eval("NbThread") %></td>
                                    <td align="center"><%# Eval("Nb100") %></td>
                                    <td align="center"><%# Eval("Nb110") %></td>
                                    <td align="center"><%# Eval("Nb120") %></td>
                                    <td align="center"><%# Eval("Nb120Accepte") %></td>
                                    <td align="center"><%# Eval("NbRow") %></td>
                                </tr>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <tr class="alternate-row <%# getGatewayStatusDetailsVisibility(Eval("Row").ToString()) %>">
                                    <td align="left">
                                        <%# Eval("Date") %>
                                    </td>
                                    <td align="center"><%# Eval("NbThread") %></td>
                                    <td align="center"><%# Eval("Nb100") %></td>
                                    <td align="center"><%# Eval("Nb110") %></td>
                                    <td align="center"><%# Eval("Nb120") %></td>
                                    <td align="center"><%# Eval("Nb120Accepte") %></td>
                                    <td align="center"><%# Eval("NbRow") %></td>
                                </tr>
                            </AlternatingItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                            <div style="text-align:center; margin-top:5px;">
                                <input id="btnShowMoreGatewayStatusDetails" type="button" class="button" value="Afficher plus" onclick="showMoreGatewayStatusDetails();" />
                            </div>
                        </div>

                        <div class="bold font-orange" style="margin-top:10px;">Détails</div>
                        <asp:Panel ID="panelGatewayDetails" runat="server" style="border:1px solid rgb(255, 101, 0); border-left:0; border-right:0; padding:5px;">
                            <div class="table" style="width:100%; font-size:0.9em">
                                <asp:Panel ID="panelGatewayNbTrxToInjectInSab" runat="server" CssClass="row">
                                    <div class="cell bold" style="width:50%;">Nombre de transactions à injecter dans SAB :</div>
                                    <div class="cell"><asp:Label ID="lblNbTrxToInjectInSab" runat="server"></asp:Label></div>
                                    <div class="cell"></div>
                                </asp:Panel>
                                <asp:Panel ID="panelGatewayLastBalanceRefreshDate" runat="server" CssClass="row">
                                    <div class="cell bold" style="width:50%;">Dernière actualisation des soldes :</div>
                                    <div class="cell"><asp:Label ID="lblGatewayLastBalanceRefreshDate" runat="server"></asp:Label></div>
                                    <div class="cell"><input type="button" class="MiniButton" value="Mettre à jour" onclick="showGetSabFiles();" style="font-family:Arial; font-size:12px" /></div>
                                </asp:Panel>
                                <asp:Panel ID="panelGatewayNbAccounts" runat="server" CssClass="row">
                                    <div class="cell bold" style="width:50%;">Nombre de comptes :</div>
                                    <div class="cell"><asp:Label ID="lblGatewayNbAccounts" runat="server"></asp:Label></div>
                                    <div class="cell"></div>
                                </asp:Panel>
                                <asp:Panel ID="PanelGatewayNbPositiveAccounts" runat="server" CssClass="row">
                                    <div class="cell bold" style="width:50%;">Nombre de comptes créditeurs :</div>
                                    <div class="cell"><asp:Label ID="lblGatewayNbPositiveAccounts" runat="server"></asp:Label></div>
                                    <div class="cell"></div>
                                </asp:Panel>
                            </div>
                        </asp:Panel>

                    </asp:Panel>
                    <asp:Panel ID="panelGatewayTransactions" runat="server" Visible="false" style="margin-top:10px">
                        <div class="tabSubtitle" style="margin:10px 0;" >Transactions</div>
                        <div class="table" style="width:100%; margin:10px 0">
                            <div class="row">
                                <div class="cell" style="width:50%">
                                    <div class="bold font-orange">Nombre total de transactions</div>
                                    <div><asp:Label ID="lblNbTrxTotal" runat="server"></asp:Label></div>
                                </div>
                                <div class="cell">
                                    <div class="bold font-orange">Nombre de transactions les 10 dernières minutes</div>
                                    <div><asp:Label ID="lblTrxLast10Minutes" runat="server"></asp:Label></div>
                                </div>
                            </div>
                        </div>
                        <asp:Repeater ID="rptGatewayMonitoring" runat="server">
                            <HeaderTemplate>
                                <table class="defaultTable2 noBorderDetails" style="width:100%;" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <th>Date remontée</th>
                                        <th>Type message</th>
                                        <th>Montant</th>
                                        <th>Code réponse</th>
                                        <th>Mode</th>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr onclick="ToggleOperationDetails(this);" style="<%# Gateway.getCssFromStatus(Eval("MessageType").ToString(),Eval("CodeReponse").ToString(), Eval("AlertStatus").ToString()) %>">
                                    <td align="left">
                                        <img class="detail-arrow" alt="" src="Styles/Img/row-detail-arrow-up.png">
                                        <%# Eval("DateRemontee") %>
                                    </td>
                                    <td align="center"><%# Eval("MessageType") %></td>
                                    <td align="right"><%# Eval("Montant").ToString().Replace('.',',') + " &euro;" %></td>
                                    <td align="center"><%# Eval("CodeReponse") %></td>
                                    <td align="center"><%# Gateway.getModeLabel(Eval("Mode").ToString()) %></td>
                                </tr>
                                <tr style="cursor:auto;border:0">
                                    <td class="tdDetails" colspan="5" style="font-size:0.8em; border:0;">
                                        <div class="row-details" style="display:none; padding:5px 0; border:0;">
                                            <div>
                                                <div style="display:inline-block" class="bold">Carte</div>
                                                <div style="display:inline-block"><%# Eval("Carte") %></div>
                                            </div>
                                            <div>
                                                <div style="display:inline-block" class="bold">Champ 54</div>
                                                <div style="display:inline-block"><%# Eval("Champ54") %></div>
                                            </div>
                                            <div>
                                                <div style="display:inline-block" class="bold">Commerce</div>
                                                <div style="display:inline-block"><%# Eval("Commerce") %></div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <tr class="alternate-row" onclick="ToggleOperationDetails(this);" style="<%# Gateway.getCssFromStatus(Eval("MessageType").ToString(),Eval("CodeReponse").ToString(), Eval("AlertStatus").ToString()) %>">
                                    <td align="left">
                                        <img class="detail-arrow" alt="" src="Styles/Img/row-detail-arrow-up.png">
                                        <%# Eval("DateRemontee") %>
                                    </td>
                                    <td align="center"><%# Eval("MessageType") %></td>
                                    <td align="right"><%# Eval("Montant").ToString().Replace('.',',') + " &euro;" %></td>
                                    <td align="center"><%# Eval("CodeReponse") %></td>
                                    <td align="center"><%# Gateway.getModeLabel(Eval("Mode").ToString()) %></td>
                                </tr>
                                <tr class="alternate-row" style="cursor:auto;border:0">
                                    <td  class="tdDetails" colspan="5" style="font-size:0.8em; border:0;">
                                        <div class="row-details" style="display:none; padding:5px 0;">
                                            <div>
                                                <div style="display:inline-block" class="bold">Carte</div>
                                                <div style="display:inline-block"><%# Eval("Carte") %></div>
                                            </div>
                                            <div>
                                                <div style="display:inline-block" class="bold">Champ 54</div>
                                                <div style="display:inline-block"><%# Eval("Champ54") %></div>
                                            </div>
                                            <div>
                                                <div style="display:inline-block" class="bold">Commerce</div>
                                                <div style="display:inline-block"><%# Eval("Commerce") %></div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </AlternatingItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </asp:Panel>
                    <asp:Panel ID="panelGatewayError" runat="server" Visible="false" style="text-align:center; color:red">
                        Une erreur est survenue
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div id="popup-gatewayModeChange" style="display:none">
                <asp:UpdatePanel ID="upGatewatModeChange" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="table" style="margin:10px auto; text-transform:uppercase">
                            <div class="row">
                                <div class="cell bold" style="text-align:center">
                                    <asp:Label ID="lblGatewayCurrentModeConfirmation" runat="server"></asp:Label>
                                </div>
                                <div class="cell" style="vertical-align:middle; text-align:center; padding:0 10px">
                                    <img alt="" src="Styles/Img/right.png" />
                                </div>
                                <div class="cell bold" style="text-align:center">
                                    <span id="lblGatewayModeRequestedConfirmation"></span>
                                </div>
                            </div>
                        </div>
                        <asp:Panel ID="panelGatewayDelegationChangement" runat="server" Visible="false" CssClass="italic">
                            <span class="bold">Attention!</span><br />
                            Le changement ne sera effectif que lorsqu'il n'y aura plus de<br />transaction à injecter dans SAB.
                        </asp:Panel>
                        <div style="margin-top:20px;text-transform:uppercase" class="bold">
                            Confirmez-vous le changement de mode de la Gateway ?
                        </div>
                        <asp:Button ID="btnSaveGatewayModeChange" runat="server" style="display:none" OnClick="btnSaveGatewayModeChange_Click" />
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSaveGatewayModeChange" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>

            <div id="popup-ToolsMessage" style="display:none">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
            </div>

            <div id="popup-LastImportedSabFiles" style="display:none">
                <asp:UpdatePanel ID="upLastImportedSabFiles" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <h2 style="margin:0; font-size:1em;">DERNIERS IMPORTS</h2>
                        <div style="margin:5px 0 0 0; height: 40px;" class="table">
                            <div class="row">
                                <div class="cell" style="vertical-align:middle">
                                    <asp:Button ID="btnRefreshLastImportedSabFiles" runat="server" CssClass="MiniButton" style="font-family:Arial; font-size:12px" Text="Actualiser" OnClick="btnRefreshLastImportedSabFiles_Click" />
                                </div>
                                <div class="cell" style="padding-left:10px; vertical-align:middle">
                                    <asp:UpdateProgress ID="uprogressLastImportedSabFiles" runat="server" AssociatedUpdatePanelID="upLastImportedSabFiles">
                                        <ProgressTemplate>
                                            <img src="Styles/Img/loading.gif" alt="loading" width="15px" height="15px" style="margin-top:5px;" />
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                                </div>
                            </div>
                        </div>
                        <asp:Repeater ID="rptLastImportedSabFiles" runat="server">
                            <HeaderTemplate>
                                <table class="defaultTable3 noBorderDetails">
                                    <tr>
                                        <th>
                                            Date import
                                        </th>
                                        <th>
                                            Nom fichier
                                        </th>
                                        <th>
                                            Nb lignes
                                        </th>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td><%# Eval("DateImport") %></td>
                                    <td><%# Gateway.getSabFileNameFromPath(Eval("FileName").ToString()) %></td>
                                    <td><%# Eval("InsertedRows") %></td>
                                </tr>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <tr class="alternate-row">
                                    <td><%# Eval("DateImport") %></td>
                                    <td><%# Gateway.getSabFileNameFromPath(Eval("FileName").ToString()) %></td>
                                    <td><%# Eval("InsertedRows") %></td>
                                </tr>
                            </AlternatingItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                        <div style="margin-top:20px;width:100%; text-align:center">
                            <asp:Button ID="btnForceSabFileImport" runat="server" CssClass="buttonHigher" Text="Forcer l'import" OnClick="btnForceSabFileImport_Click" OnClientClick="showLoading();" />
                            <div style="font-size:0.8em">
                                Force l'import des fichiers solde SAB s'ils sont présents sur le serveur.
                            </div>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnRefreshLastImportedSabFiles" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="btnForceSabFileImport" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>

            <script type="text/javascript">
                function showGetSabFiles() {
                    $('#popup-LastImportedSabFiles').dialog({
                        autoOpen: true,
                        draggable: false,
                        resizable: false,
                        width: 'auto',
                        minWidth: 500,
                        modal: true,
                        title: 'GATEWAY - ACTUALISATION DU SOLDE'
                    });
                    $('#popup-LastImportedSabFiles').parent().appendTo(jQuery("form:first"));
                    refreshSabImportedFiles();
                }

                function refreshSabImportedFiles() {
                    $('#<%=btnRefreshLastImportedSabFiles.ClientID%>').click();
                }
            </script>
        </asp:Panel>
        <asp:Panel ID="panel3DS" runat="server" Visible="false">
            <asp:UpdatePanel ID="up3DS" runat="server">
                <ContentTemplate>
                    <div style="height: 40px;" class="table">
                        <div class="row">
                            <div class="cell" style="vertical-align:middle">
                                <asp:Button ID="btn3DSRefreshMonitoring" runat="server" CssClass="button" Text="Rafraîchir" OnClick="btn3DSRefreshMonitoring_Click" style="height:40px" />
                            </div>
                            <div class="cell" style="padding-left:10px; vertical-align:middle">
                                <asp:UpdateProgress ID="updateProgress3DS" runat="server" AssociatedUpdatePanelID="up3DS">
                                    <ProgressTemplate>
                                        <img src="Styles/Img/loading.gif" alt="loading" width="25px" height="25px" />
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </div>
                        </div>
                    </div>
                    <div id="div3DSStatusDetails">
                        <div class="bold font-orange" style="margin-top:10px;">Derniers échanges</div>
                        <asp:Repeater ID="rpt3DSLastExchanges" runat="server">
                            <HeaderTemplate>
                                <table class="defaultTable3 noBorderDetails" style="width:100%;" cellpadding="0" cellspacing="0">
                                <tr>
                                    <th>Date demande</th>
                                    <th>Date envoi</th>
                                    <th>MONEXT date</th>
                                    <th>RC envoi</th>
                                    <th>Message erreur</th>
                                </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                            <tr style="<%# get3DSCssStatus(Eval("RCEnvoi").ToString()) %>">
                                <td align="center"><%# Eval("DateDemande") %></td>
                                <td align="center"><%# Eval("DateEnvoi") %></td>
                                <td align="center"><%# Eval("MonextDate") %> <%# Eval("MonextTime") %></td>
                                <td align="center"><%# Eval("RCEnvoi") %></td>
                                <td align="center"><%# Eval("ErrorMessage") %></td>
                            </tr>
                        </ItemTemplate>
                        <AlternatingItemTemplate>
                            <tr class="alternate-row" style="<%# get3DSCssStatus(Eval("RCEnvoi").ToString()) %>">
                                <td align="center"><%# Eval("DateDemande") %></td>
                                <td align="center"><%# Eval("DateEnvoi") %></td>
                                <td align="center"><%# Eval("MonextDate") %> <%# Eval("MonextTime") %></td>
                                <td align="center"><%# Eval("RCEnvoi") %></td>
                                <td align="center"><%# Eval("ErrorMessage") %></td>
                            </tr>
                        </AlternatingItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
        </asp:Panel>
        <asp:Panel ID="panelSMS" runat="server" Visible="false">
            <asp:UpdatePanel ID="upSMS" runat="server">
                <ContentTemplate>
                    <div style="height: 40px;" class="table">
                        <div class="row">
                            <div class="cell" style="vertical-align:middle">
                                <asp:Button ID="btnSMSRefreshMonitoring" runat="server" CssClass="button" Text="Rafraîchir" OnClick="btnSMSRefreshMonitoring_Click" style="height:40px" />
                            </div>
                            <div class="cell" style="padding-left:10px; vertical-align:middle">
                                <asp:UpdateProgress ID="updateProgressSMS" runat="server" AssociatedUpdatePanelID="up3DS">
                                    <ProgressTemplate>
                                        <img src="Styles/Img/loading.gif" alt="loading" width="25px" height="25px" />
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </div>
                        </div>
                    </div>
                    <div>
                        Mode actuel : <asp:Label ID="lblSmsCurrentProvider" runat="server" style="font-weight:bold"></asp:Label>
                    </div>
                    <asp:RadioButtonList ID="rblSmsProvider" runat="server" RepeatDirection="Horizontal" CssClass="RadioButtonList" Visible="false" onchange="return onChangeSmsWay();">
                    </asp:RadioButtonList>
                </ContentTemplate>
            </asp:UpdatePanel>

            <div id="popup-SmsWayChange" style="display:none">
                <asp:UpdatePanel ID="upSmsWayCHange" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="table" style="margin:10px auto; text-transform:uppercase">
                            <div class="row">
                                <div class="cell bold" style="text-align:center">
                                    <asp:Label ID="lblSmsCurrentWayConfirmation" runat="server"></asp:Label>
                                </div>
                                <div class="cell" style="vertical-align:middle; text-align:center; padding:0 10px">
                                    <img alt="" src="Styles/Img/right.png" />
                                </div>
                                <div class="cell bold" style="text-align:center">
                                    <span id="lblSmsWayRequestedConfirmation"></span>
                                </div>
                            </div>
                        </div>
                        <div style="margin-top:20px;text-transform:uppercase" class="bold">
                            Confirmez-vous le changement de mode d'envoi SMS ?
                        </div>
                        <asp:Button ID="btnSaveSmsWayChange" runat="server" style="display:none" OnClick="btnSaveSmsWayChange_Click" />
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSaveSmsWayChange" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </asp:Panel>
        <asp:Panel ID="panelOnUs" runat="server" Visible="false">
            <asp:UpdatePanel ID="upOnUs" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div style="width:100%" class="table">
                        <div class="row">
                            <div class="cell" style="vertical-align:middle">
                                <div class="table" style="height: 40px;">
                                    <div class="row">
                                        <div class="cell" style="vertical-align:top">
                                            <asp:Button ID="btnOnUsRefreshMonitoring" runat="server" OnClick="btnOnUsRefreshMonitoring_Click" CssClass="button" Text="Rafraîchir" style="height:40px" />
                                        </div>
                                        <div class="cell" style="padding-left:10px; vertical-align:middle">
                                            <asp:UpdateProgress ID="updateProgressOnUs" runat="server" AssociatedUpdatePanelID="upOnUs">
                                                <ProgressTemplate>
                                                    <img src="Styles/Img/loading.gif" alt="loading" width="25px" height="25px" class="ImageButton" />
                                                </ProgressTemplate>
                                            </asp:UpdateProgress>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="cell" style="text-align:right">
                                <img alt="Envoyer SMS aux buralistes" src="Styles/Img/mobile-sms.png" onclick="OnUsSmsClick();" />
                            </div>
                        </div>
                    </div>

                    <asp:Panel ID="panelOnUsStatus" runat="server" Visible="false">
                        <asp:RadioButtonList ID="rblOnUsStatus" runat="server" RepeatDirection="Horizontal" CssClass="RadioButtonList" Visible="false" onchange="return onChangeOnUsStatus();">
                            <asp:ListItem Value="START"><span style="font-size:1.1em;font-weight:bold; color:green;-webkit-text-fill-color: green; -webkit-text-stroke-color: white; -webkit-text-stroke-width: 0.6px; ">ON</span></asp:ListItem>
                            <asp:ListItem Value="STOP"><span style="font-size:1.1em;font-weight:bold; color:red;-webkit-text-fill-color: red; -webkit-text-stroke-color: white; -webkit-text-stroke-width: 0.6px; ">OFF</span></asp:ListItem>
                        </asp:RadioButtonList>
                        <asp:HiddenField ID="hfOnUsCurrentStatus" runat="server" />
                        <asp:HiddenField ID="hfOnUsStatusRequested" runat="server" />
                        <div id="divOnUsStatusDetails">
                            <div class="bold font-orange" style="margin-top:10px;">Dernières transactions</div>
                            <asp:Repeater ID="rptOnUsMonitoring" runat="server">
                                <HeaderTemplate>
                                    <table class="defaultTable3 noBorderDetails" style="width:100%;" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <th style="width:200px">Date transaction</th>
                                        <th>Type opération</th>
                                        <th>Durée (ms)</th>
                                        <th>Etat</th>
                                    </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr onclick="ToggleOperationDetails(this);">
                                        <td align="left">
                                            <img class="detail-arrow" alt="" src="Styles/Img/row-detail-arrow-up.png">
                                            <%# Eval("TransactionDate") %>
                                        </td>
                                        <td align="center"><%# Eval("OpeType") %></td>
                                        <td align="center"><%# Eval("Duration") %></td>
                                        <td align="center"><%# Eval("Erreur") %></td>
                                    </tr>
                                    <tr style="cursor:auto;border:0">
                                        <td class="tdDetails" colspan="4" style="font-size:0.8em; border:0;">
                                            <div class="row-details" style="display:none; padding:5px 0;">
                                                <div>
                                                    <div style="display:inline-block" class="bold">Montant</div>
                                                    <div style="display:inline-block"><%# Eval("Amount") %></div>
                                                </div>
                                                <div>
                                                    <div style="display:inline-block" class="bold">HCard</div>
                                                    <div style="display:inline-block"><%# Eval("HCard") %></div>
                                                </div>
                                                <div>
                                                    <div style="display:inline-block" class="bold">Date demande</div>
                                                    <div style="display:inline-block"><%# Eval("RequestDate") %></div>
                                                </div>
                                                <div>
                                                    <div style="display:inline-block" class="bold">Date réponse</div>
                                                    <div style="display:inline-block"><%# Eval("AnswerDate") %></div>
                                                </div>
                                                <div>
                                                    <div style="display:inline-block" class="bold">IdCOM</div>
                                                    <div style="display:inline-block"><%# Eval("IDCom") %></div>
                                                </div>
                                                <div>
                                                    <div style="display:inline-block" class="bold">Transaction FPE</div>
                                                    <div style="display:inline-block"><%# Eval("FPETransaction") %></div>
                                                </div>
                                                <div>
                                                    <div style="display:inline-block" class="bold">RC Afsol</div>
                                                    <div style="display:inline-block"><%# Eval("AFSOLRC") %></div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>  
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <tr onclick="ToggleOperationDetails(this);" class="alternate-row">
                                        <td align="left">
                                            <img class="detail-arrow" alt="" src="Styles/Img/row-detail-arrow-up.png">
                                            <%# Eval("TransactionDate") %>
                                        </td>
                                        <td align="center"><%# Eval("OpeType") %></td>
                                        <td align="center"><%# Eval("Duration") %></td>
                                        <td align="center"><%# Eval("Erreur") %></td>
                                    </tr>
                                    <tr class="alternate-row" style="cursor:auto;border:0">
                                        <td class="tdDetails" colspan="4" style="font-size:0.8em; border:0;">
                                            <div class="row-details" style="display:none; padding:5px 0;">
                                            <div>
                                                <div style="display:inline-block" class="bold">Montant</div>
                                                <div style="display:inline-block"><%# Eval("Amount") %></div>
                                            </div>
                                            <div>
                                                <div style="display:inline-block" class="bold">HCard</div>
                                                <div style="display:inline-block"><%# Eval("HCard") %></div>
                                            </div>
                                            <div>
                                                <div style="display:inline-block" class="bold">Date demande</div>
                                                <div style="display:inline-block"><%# Eval("RequestDate") %></div>
                                            </div>
                                            <div>
                                                <div style="display:inline-block" class="bold">Date réponse</div>
                                                <div style="display:inline-block"><%# Eval("AnswerDate") %></div>
                                            </div>
                                            <div>
                                                <div style="display:inline-block" class="bold">IdCOM</div>
                                                <div style="display:inline-block"><%# Eval("IDCom") %></div>
                                            </div>
                                            <div>
                                                <div style="display:inline-block" class="bold">Transaction FPE</div>
                                                <div style="display:inline-block"><%# Eval("FPETransaction") %></div>
                                            </div>
                                            <div>
                                                <div style="display:inline-block" class="bold">RC Afsol</div>
                                                <div style="display:inline-block"><%# Eval("AFSOLRC") %></div>
                                            </div>
                                        </div>
                                        </td>
                                    </tr>
                                </AlternatingItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div id="popup-OnUsStatusChange" style="display:none">
                <asp:UpdatePanel ID="upOnUsStatusChange" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="table" style="margin:10px auto; text-transform:uppercase">
                            <div class="row">
                                <div class="cell bold" style="text-align:center">
                                    <asp:Label ID="lblOnUsCurrentStatusConfirmation" runat="server"></asp:Label>
                                </div>
                                <div class="cell" style="vertical-align:middle; text-align:center; padding:0 10px">
                                    <img alt="" src="Styles/Img/right.png" />
                                </div>
                                <div class="cell bold" style="text-align:center">
                                    <span id="lblOnUsStatusRequestedConfirmation"></span>
                                </div>
                            </div>
                        </div>
                        <div style="margin-top:20px;text-transform:uppercase" class="bold">
                            Confirmez-vous le changement du statut On Us ?
                        </div>
                        <asp:Button ID="btnSaveOnUsStatusChange" runat="server" style="display:none" OnClick="btnSaveOnUsStatusChange_Click" />
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSaveOnUsStatusChange" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
            <div id="popup-OnUsSms" style="display:none">
                <asp:UpdatePanel ID="upOnUsSms" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Panel ID="panelOnUsSms" runat="server">
                            <div style="width:100%; text-align:left">
                                <label class="label">Destinataires</label>
                            </div>
                            <div style="width:100%; margin:5px 0 20px 0; text-align:left">
                                <asp:RadioButtonList ID="rblOnUsSmsRecipientGroup" runat="server" CssClass="RadioButtonList" RepeatDirection="Horizontal">
                                    <asp:ListItem Value="TEST" Selected="True">TEST</asp:ListItem>
                                    <asp:ListItem Value="ALL">Tous</asp:ListItem>
                                    <asp:ListItem Value="METROPOLE">Métropole</asp:ListItem>
                                    <asp:ListItem Value="DOMTOM">DOM-TOM</asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                            <div style="width:100%; text-align:left">
                                <label class="label">SMS prédéfini</label>
                            </div>
                            <div style="width:100%; text-align:center">
                                <asp:DropDownList ID="ddlCommunicationModelList" runat="server" style="width:100%"></asp:DropDownList>
                            </div>
                            <div style="margin:5px 0 20px 0; width:100%; text-align:center">
                                <asp:Button ID="btnOnUsSmsApplyCommunicationModel" runat="server" CssClass="MiniButton" style="width:200px;" Text="Appliquer" OnClick="btnOnUsSmsApplyCommunicationModel_Click" />
                            </div>
                            <asp:MessageWithCharCounter ID="mwccOnUsSms" runat="server" MaxLength="-1" Splitted="true" SplitLength="160" SplittedKindMessage="SMS" SplittedKindMessagePlural="false" Height="300" Label="Contenu du SMS" ReadOnly="false" />

                            <asp:Button ID="btnOnUsSendSMS" runat="server" style="display:none" OnClick="btnOnUsSendSMS_Click" />
                        </asp:Panel>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnOnUsSmsApplyCommunicationModel" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="btnOnUsSendSMS" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </asp:Panel>
        <asp:Panel ID="panelCobalt" runat="server" Visible="false">
            <asp:UpdatePanel ID="upCobalt" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Panel ID="panelAddCobaltPDV" runat="server" DefaultButton="btnAddCobaltPointOfSale">
                        <div class="font-orange">
                            Ajout point de vente COBALT via Identifiant PRO
                        </div>
                        <div class="table">
                            <div class="table-row">
                                <div class="table-cell">
                                    PRO
                                </div>
                                <div class="table-cell" style="padding-left:5px">
                                    <asp:TextBox ID="txtIdPRO" runat="server" MaxLength="7"></asp:TextBox>
                                </div>
                                <div class="table-cell" style="padding-left:10px;">
                                    <asp:Button ID="btnAddCobaltPointOfSale" runat="server" OnClick="btnAddCobaltPointOfSale_Click" Text="AJOUTER" CssClass="button" />
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnAddCobaltPointOfSale" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </asp:Panel>
    </div>

    <asp:HiddenField ID="hdnActiveTab" runat="server" Value="" />
    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        var sRequestControlID = "";
        function BeginRequestHandler(sender, args) {
            pbControl = args.get_postBackElement();
            var panel = '';

            //console.log('loading (' + pbControl.id + ')');
            switch(pbControl.id)
            {
                case "<%=btnSaveChanges.ClientID%>":
                    panel = '#<%=panelServiceLock.ClientID %>';
                    break;
                case "<%=btnOnUsSmsApplyCommunicationModel.ClientID%>":
                    panel = '#<%=panelOnUsSms.ClientID%>';
                case "<%=btnAddCobaltPointOfSale.ClientID%>":
                    panel = '#<%=panelAddCobaltPDV.ClientID%>';
                        break;
            }

            console.log(panel);

            if (panel.length > 0) {
                var width = $(panel).outerWidth();
                var height = $(panel).outerHeight();
                $('#ar-loading').css('width', width);
                $('#ar-loading').css('height', height);
                $('#ar-loading').show();
                $('#ar-loading').position({ my: "left top", at: "left top", of: panel });
            }

            sRequestControlID = pbControl.id;
        }
        function EndRequestHandler(sender, args) {
            $('#ar-loading').hide();
            hideLoading();
            $('.RadioButtonList').buttonset();

            if ($('#<%=txtIdPRO.ClientID%>') != null) {
                $('#<%=txtIdPRO.ClientID%>').mask("999-999-9");
            }
        }
    </script>
</asp:Content>

