﻿<%@ Page Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="LogistiqueCommande_Details.aspx.cs" Inherits="LogistiqueCommande_Details" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script type="text/javascript" src="Scripts/tools.js"></script>
    <script type="text/javascript">
        //        function CheckEnvoi(refLot) {
        function CheckEnvoi() {
//            $('#<%=hdnNumeroLot.ClientID%>').val(refLot);
            var maxRestant = parseInt($('#<%=hdnMaxAttendu.ClientID%>').val());
            var quantityDemande = parseInt($('#<%=txtQuantity.ClientID%>').val());

            if (isNaN(quantityDemande) || quantityDemande == 0) {
                showAlertError('Veuillez remplir le nombre attendu');
                return false;
            }
            
            if (quantityDemande > maxRestant) {
                showAlertError('Nombre attendu supérieur au maximum possible');
                return false;
            }
            //txtDateLivraison
            var parts = ($('#<%=txtDateLivraison.ClientID%>').val()).split('/');
            if (parts.length < 3 || parts[0].length == 0 || parts[1].length == 0 || parts[2].length == 0) {
                showAlertError('Veuillez remplir la date de récpetion estimée');
                return false;
            }
            var dateLivraison = new Date(parts[2], parts[1], parts[0]);
            parts = ($('#<%=lblDateCommand.ClientID%>').text()).split('/');
            var dateCommande = new Date(parts[2], parts[1], parts[0]);
            if (dateLivraison <= dateCommande) {
                showAlertError('La date de livraison doit être postérieur à la date de commande');
                return false;
            }

            return true;
        }

        function DownloadFile(FileNameFull) {
            console.log(FileNameFull);
            window.open(FileNameFull);
            
        }


    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:HiddenField ID="hdnNumeroLot" runat="server" />
    <asp:HiddenField ID="hdnMaxAttendu" runat="server" />
    <div style="position: relative;">
        <asp:Button ID="btnPrev" runat="server" Text="Recherche commande" PostBackUrl="~/LogistiqueCommande.aspx" CssClass="button" />
    </div>
    <div id="dInfoCommande">
        <h2 style="color: #344b56">
            <asp:Label ID="lblInfoCommand" runat="server" />
        </h2>
        <table>
            <tr>
                <td><span><b>Date de commande : </b>
                    <asp:Label ID="lblDateCommand" runat="server" /></span></td>
            </tr>
            <tr>
                <td><span><b>Type de visuel : </b>
                    <asp:Label ID="lblVisuelType" runat="server" /></span></td>
            </tr>
            <tr>
                <td><span><b>Quantité totale : </b>
                    <asp:Label ID="lblQuantity" runat="server" /></span></td>
            </tr>
            <tr>
                <td><span><b>Prix unitaire : </b>
                    <asp:Label ID="lblPrice" runat="server" /></span></td>
            </tr>
        </table>
        <asp:UpdatePanel ID="upReceptionLot" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Repeater ID="rptLotList" runat="server" Visible="false">
                    <HeaderTemplate>
                        <table id="tReceptionLot" style="width: 70%" class="operation-table">
                            <thead></thead>
                            <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td style="width: 1px; white-space: nowrap; text-align: left">
                                <div>
                                    <%# getLibelleLot(Eval("NumeroLot").ToString(), Eval("QuantityLot").ToString(), Eval("LivraisonDate").ToString()) %>
                                </div>
                            </td>
                            <td>
                                <asp:Button ID="btnExtraction" runat="server" Text="Extraction des n° CSV" 
                                    OnClick="btnExtraction_Click" CommandArgument='<%#Eval("NumeroLot") %>' 
                                    OnClientClick="showLoading();"
                                    CssClass="button" Style="position: relative; top: 5px;" />
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </tbody>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
                <asp:HyperLink ID="lnkExport" runat="server" Visible="false" NavigateUrl="www.google.com" Target="_blank"></asp:HyperLink>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div id="dBonReception">
        <h2 style="color: #344b56">Bon de reception
        </h2>
        <asp:UpdatePanel ID="upBonReception" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Repeater ID="rptBonReception" runat="server" Visible="false">
                    <HeaderTemplate>
                        <table id="tReceptionBon" style="width: 100%" class="operation-table">
                            <thead></thead>
                            <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td style="width: 1px; white-space: nowrap; text-align: left">
                                <div>
                                    <%# getLibelleBon(Eval("NumeroLot").ToString(), Eval("BonReceptionDate").ToString()) %>
                                </div>
                            </td>
                            <td>
                                <asp:Button ID="btnRenvoyer" runat="server" Text="Renvoyer" OnClick="btnRenvoyer_Click" CommandArgument='<%# String.Format("{0},{1},{2}", Eval("NumeroLot"), Eval("QuantityLot"), Eval("LivraisonDate")) %>' CssClass="button" Style="position: relative; top: 5px;" />
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </tbody>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:Panel ID="pEnvoiBon" runat="server" Visible="false">
            <div class="table" style="width: 100%">
                <div class="row">
                    <div class="cell" style="width: 50%">
                        <div class="table">
                            <div class="row">
                                <div class="cell" style="width: 35%">
                                    <asp:Label ID="lblDateEstime" runat="server">Date de livraison estimée (jj/mm/aaaa)</asp:Label>

                                </div>
                                <div class="cell" style="width: 35%">
                                    <asp:TextBox ID="txtDateLivraison" runat="server" />
                                    <asp:ImageButton ID="ImgBntCalc1" runat="server" ImageUrl="~/Img/calendar.png" CausesValidation="False"
                                        AlternateText="Cal" ToolTip="<%=Resources.res.OpenCalendar %>" Width="22px" Style="vertical-align: middle" />
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtDateLivraison" 
                                        PopupButtonID="ImgBntCalc1" Format="dd/MM/yyyy" />
                                    <%--
                                    <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender1" runat="server" TargetControlID="txtDateLivraison"
                                        Mask="99/99/9999" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                                        OnInvalidCssClass="MaskedEditError" MaskType="Date" ErrorTooltipEnabled="True" />
                                    <ajaxToolkit:MaskedEditValidator ID="MaskedEditValidator1" runat="server" ControlExtender="MaskedEditExtender1"
                                        ControlToValidate="txtDateLivraison" InvalidValueMessage="Date invalide" Display="Dynamic" ForeColor="Red"
                                        EmptyValueBlurredText="*" InvalidValueBlurredMessage="Date invalide" ValidationGroup="MKE" /> 
                                       <asp:RequiredFieldValidator runat="server" ID="reqBeginDate" ControlToValidate="txtDateLivraison"
                                        CssClass="reqStarStyle" ValidationGroup="MKE">*</asp:RequiredFieldValidator>--%>
                                </div>
                            </div>
                            <div class="row">
                                <div class="cell" style="width: 35%">
                                    <asp:Label ID="lblQuantityAttendue" runat="server">Quantité attendue</asp:Label>
                                </div>
                                <div class="cell" style="width: 35%">
                                    <asp:TextBox ID="txtQuantity" runat="server" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="cell" style="width: 50%">
                        <asp:Button ID="btnEnvoyer" runat="server" Text="Envoyer à Mobiltron" OnClick="btnEnvoyer_Click" OnClientClick="return CheckEnvoi();" CssClass="button" Style="position: relative; top: 5px;" />
                    </div>
                </div>
            </div>
        </asp:Panel>
    </div>

    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        var sRequestControlID = "";
        function BeginRequestHandler(sender, args) {
        }
        function EndRequestHandler(sender, args) {
            hideLoading();
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">
</asp:Content>
