﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using XMLMethodLibrary;

public partial class Site2 : System.Web.UI.MasterPage
{
    protected void Page_Init(object sender, EventArgs e)
    {
        Thread.CurrentThread.CurrentUICulture = new CultureInfo("fr-FR");
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("fr-FR");

        string sPageName = Request.Url.AbsoluteUri.Substring(Request.Url.AbsoluteUri.LastIndexOf('/') + 1);

        if (!pageNotAuthenticated(sPageName))
        {
            authentication auth = authentication.GetCurrent();
            hfSessionStarted.Value = "true";
        }
        else
        {
            hfSessionStarted.Value = "false";
            Session.Clear();
            Session["SessionClosed"] = false;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        int iTimeout;
        if (int.TryParse(ConfigurationManager.AppSettings["SessionTimeout"].ToString(), out iTimeout))
        {
            iTimeout *= 60000;
            hfSessionTimeout.Value = iTimeout.ToString();
        }
        if (Session["SessionClosed"] != null && (bool)Session["SessionClosed"] == true)
        {
            hfShowTimeout.Value = "true";
            Session["SessionClosed"] = false;
        }

        if (IsPostBack)
        {
            //string controlName = Request.Params.Get("__EVENTTARGET");
            //string argument = Request.Params.Get("__EVENTARGUMENT");
            //if (controlName == "SessionTimeout")
            //{
            //    Session.Clear();
            //    Session["SessionClosed"] = true;
            //    Response.Redirect("TimeoutSession.aspx");
            //}
        }
        else
        {
            //getNbNonEvaluateSubscription();
        }
    }

    protected bool pageNotAuthenticated(string pageName)
    {
        bool isAuthorized = false;

        string pageList = (ConfigurationManager.AppSettings["NotAuthenticatedPageList"] != null) ? ConfigurationManager.AppSettings["NotAuthenticatedPageList"].ToString() : "";
        string[] tPageList = pageList.Split(',');

        for (int i = 0; i < tPageList.Length; i++)
        {
            if (tPageList[i].ToString().ToLower().Trim() == pageName.ToLower().Trim())
                isAuthorized = true;
        }

        return isAuthorized;
    }

    

    //protected void NbNonEvaluateSubscription_Tick(object sender, EventArgs e)
    //{
    //    getNbNonEvaluateSubscription();
    //}

    //protected void getNbNonEvaluateSubscription()
    //{
    //    try
    //    {
    //        int result = ws_tools.getNbNonEvaluateSubscription();

    //        if (result == 0)
    //        {
    //            panelNbNonEvaluateSubscription.Visible = false;
    //        }
    //        else if (result == 1)
    //        {
    //            panelNbNonEvaluateSubscription.Visible = true;
    //            lblNbNonEvaluateSubscription.Text = "<b>" + result.ToString() + "</b> inscription non vérifiée";
    //        }
    //        else
    //        {
    //            panelNbNonEvaluateSubscription.Visible = true;
    //            lblNbNonEvaluateSubscription.Text = "<b>" + result.ToString() + "</b> inscriptions non vérifiées";
    //        }

    //    }
    //    catch (Exception ex)
    //    {
    //    }
    //}
}
