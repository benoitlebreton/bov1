﻿<%@ Page Title="Page d'accueil - BO Compte-Nickel" Language="C#" MasterPageFile="~/mobile/SiteMobile.master" AutoEventWireup="true"
    CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    
    <script type="text/javascript">
        function menuChoice(page) {
            $('#<%=hfPageRequest.ClientID%>').val(page);
            $('#<%=btnMenuChoosen.ClientID%>').click();
        }
    </script>

    <asp:UpdatePanel ID="upMenu" runat="server">
        <ContentTemplate>
            <asp:Repeater ID="rptMenu" runat="server">
                <HeaderTemplate>
                    <div style="margin:20px auto; border:2px solid #ff6a00; display:table; width:100%">
                </HeaderTemplate>
                <ItemTemplate>
                    <div style="display:table-row">
                        <div style="display:table-cell;height:40px; vertical-align:middle; text-align:center;" class="classMenu" onclick='menuChoice("<%#Eval("Value") %>");'>
                            <%#Eval("Label") %>
                        </div>
                    </div>
                </ItemTemplate>
                <FooterTemplate>
                    </div>
                </FooterTemplate>
            </asp:Repeater>
            <asp:Panel ID="panelMenuEmpty" runat="server" Visible="false" style="margin:20px auto; text-align:center">
                Votre compte utilisateur ne dispose pas de droits suffisants pour accéder au menu.
            </asp:Panel>

            <asp:Button ID="btnMenuChoosen" runat="server" OnClick="btnMenuChoosen_Click" style="display:none" />
            <asp:UpdateProgress ID="upMenuLoading" runat="server" AssociatedUpdatePanelID="upMenu">
                <ProgressTemplate>
                    <div style="text-align: center; position: absolute; left: 45%; top: 50%">
                        <img src="../Styles/Img/loading.gif" alt="loading..." />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnMenuChoosen" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    
    <asp:HiddenField ID="hfPageRequest" runat="server" />
</asp:Content>
<script runat="server">
    protected override void OnPreInit(EventArgs e)
    {
         base.OnPreInit(e);
     
         if (Request.UserAgent != null &&Request.UserAgent.IndexOf("AppleWebKit", StringComparison.CurrentCultureIgnoreCase) > -1)
         {
              this.ClientTarget = "uplevel";
         }
    }
</script>