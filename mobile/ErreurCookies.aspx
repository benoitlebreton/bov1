﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ErreurCookies.aspx.cs" Inherits="ErreurCookies" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Erreur cookies</title>

    <meta name="HandheldFriendly" content="True" />
    <meta name="MobileOptimized" content="320" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta http-equiv="cleartype" content="on" />
    <meta name="viewport" content="width=320,maximum-scale=1.0" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <meta name="format-detection" content="telephone=no" />

    <link rel="shortcut icon" href="favicon57.png" />
    <link rel="apple-touch-icon" href="favicon57.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="favicon72.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="favicon114.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="favicon144.png" />

    <link rel="stylesheet" href="../Styles/Site.css" />
    <link rel="stylesheet" href="Styles/SiteMobile.css" />
    <link rel="stylesheet" href="../Styles/UI/jquery-ui-1.9.2.custom.css" />

    <script src="../Scripts/jquery-1.8.3.js"></script>
    <script src="../Scripts/jquery-ui-1.9.2.custom.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.watermark.min.js"></script>
    <!--<script src="Scripts/jquery.jpanelmenu.min.js" type="text/javascript"></script>-->
    <script src="Scripts/jquery.jpanelmenu.js" type="text/javascript"></script>

    <script src="Scripts/link.js" type="text/javascript"></script>

    <script type="text/javascript">
        var jPM_isOpened = false;
        var Header_isFixed = false;

        $(document).ready(function () {
            if (are_cookies_enabled() && $('#<%=hfCookieSupported.ClientID%>').val() == "supported") {
                document.location.href = "Default.aspx";
            }
        });

        function are_cookies_enabled() {
            var cookieEnabled = (navigator.cookieEnabled) ? true : false;

            if (typeof navigator.cookieEnabled == "undefined" && !cookieEnabled) {
                document.cookie = "testcookie";
                cookieEnabled = (document.cookie.indexOf("testcookie") != -1) ? true : false;
            }
            return (cookieEnabled);
        }

        $(function () {
            var jPM = $.jPanelMenu({
                menu: '#menu',
                trigger: '.menu-trigger',
                openDuration: 300,
                closeDuration: 200,
                beforeOpen: function () {
                    if (jPM.isOpen()) {
                        jPM.close();
                        jPM_isOpened = true;
                    }
                    else {
                        $('#header_disabled').show();
                        jPM_isOpened = true;
                    }
                },
                afterClose: function () {
                    setTimeout(function () { $('#header_disabled').hide(); }, 500);
                    jPM_isOpened = false;
                    FixHeaderOnScroll();
                }
            });
            jPM.on();

            // Close on orientation change
            $(window).resize(function () {
                jPM.close();
                jPM_isOpened = false;
                FixHeaderOnScroll();
            });

            $(".page-mobile").css("min-height", $(window).height().toString() + "px");
        });

        document.addEventListener("scroll", Scroll, false);
        document.addEventListener("touchmove", ScrollStart, false);

        var KBisOpen = false;
        function Blur() {
            KBisOpen = false;
            setTimeout(function () {
                FixHeaderOnScroll();
            }, 100);
            setTimeout(function () {
                FixHeaderOnScroll();
            }, 200);
            setTimeout(function () {
                FixHeaderOnScroll();
            }, 300);
            setTimeout(function () {
                FixHeaderOnScroll();
            }, 400);
            setTimeout(function () {
                FixHeaderOnScroll();
            }, 500);
        }
        function Focus() {
            KBisOpen = true;
        }

        function ScrollStart() {
            FixHeaderOnScroll();
        }

        function Scroll() {
            FixHeaderOnScroll();
        }

        function FixHeaderOnScroll() {
            if (!$('#<%=panelHeaderNotAuth.ClientID%>').is(':visible')) {
                var scrOfY = Math.round(window.pageYOffset);

                //Fixer le header avec le solde minute lorsqu'on arrive en haut de la page
                if (scrOfY >= 60 && KBisOpen == false) {
                    $('#headerConnectionStatus_replacement').show();
                    $('.headerConnectionStatus').addClass("headerfix");
                    Header_isFixed = true;
                    if (jPM_isOpened) {
                        var jPM_width = $('#jPanelMenu-menu').width();
                        $('.headerConnectionStatus').css("left", jPM_width.toString() + "px");
                    }
                    else {
                        $('.headerConnectionStatus').css("left", "0");
                    }

                }
                else {
                    DisabledHeaderFix();
                }
            }
        }
        function DisabledHeaderFix() {
            $('#headerConnectionStatus_replacement').hide();
            $('.headerConnectionStatus').removeClass("headerfix").css("left", "0");
            Header_isFixed = false;
        }

        (function () {

            jQuery(window).bind('scrollstart', function () {
                FixHeaderOnScroll();
            });

            jQuery(window).bind('scrollstop', function (e) {
                FixHeaderOnScroll();
            });

        })();

        function ShowMenuLoading() {
            var width = $('#jPanelMenu-menu').width();
            var height = $('#jPanelMenu-menu').height();
            $('#ar-loading-menu').css('width', width);
            $('#ar-loading-menu').css('height', height);

            $('#ar-loading-menu').show();
            $('#ar-loading-menu').position({ my: 'center', at: 'center', of: $('#jPanelMenu-menu') });
        }
        function HideMenuLoading() {
            $('#ar-loading-menu').hide();
        }

        function ChangePage(url) {
            ShowMenuLoading();
            document.location.href = url;
        }

        function AlertMessage(message) {
            //console.log(message);
            $('.alert-popup').empty().append(message);
            $('.alert-bg').fadeIn();
            $('.alert-bg').unbind('click').bind('click', function () { $(this).fadeOut(); });
            //if (window.navigator.msPointerEnabled) {
            //$('.alert-bg').unbind('MSPointerDown').bind('MSPointerDown', function () { $(this).fadeOut(); });
            //}
            //else {
            //$('.alert-bg').unbind('touchStart').bind('touchstart', function () { $(this).fadeOut(); });
            //}
            $(window).bind('resize', function () { if ($('.alert-bg').is(':visible')) AlertMessage(message); });

            var viewportWidth = $(window).width();
            var viewportHeight = $(window).height();

            //console.log(viewportWidth + ":" + viewportHeight);
            $('.alert-bg').css('width', viewportWidth);
            $('.alert-bg').css('height', viewportHeight);

            $('.alert-popup').position({ my: 'center', at: 'center', of: $(window) });
        }

        function ShowLoadingPage() {
            var width = $(window).width();
            var height = $(window).height();
            $('#ar-loading-page').css('width', width);
            $('#ar-loading-page').css('height', height);

            $('#ar-loading-page').show();
            $('#ar-loading-page').position({ my: 'center', at: 'center', of: $(window) });
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <%--<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>--%>
        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" EnablePartialRendering="true" EnableScriptLocalization="true"
            EnableScriptGlobalization="true" EnablePageMethods="true">
        </asp:ToolkitScriptManager>

        <div class="page-mobile">
            <div id="header_disabled" style="display: none; height: 60px; position: absolute; top: 0; left: 0; width: 100%; z-index: 202;">
                &nbsp;
            </div>
            <asp:Panel ID="panelHeaderNotAuth" runat="server" Visible="true" CssClass="header" style="z-index:201">
                <div class="table" style="width: 100%; height: 30px;">
                    <div class="table-row">
                        <div class="table-cell" style="width: 30%; padding-left:10px; vertical-align: middle;">
                            <a href="authentication.aspx" style="text-decoration:none;">
                                <div class="table" style="margin-top:10px;">
                                    <div class="table-row">
                                        <div class="table-cell">
                                            <img src="Styles/Img/arrow-back.png" alt="" height="30" />
                                        </div>
                                        <div class="table-cell font-VAGRoundedLight" style="vertical-align:middle; padding-left:5px; font-size:1.2em; color:#808080">
                                            retour
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="table-cell" style="text-align: center; vertical-align: middle">
                            <div style="margin-top:10px;">
                                <img src="Styles/Img/bo-nickel-header-logo.png" alt="" height="40" />
                            </div>
                        </div>
                        <div class="table-cell" style="text-align: right; width: 30%; padding-right: 20px; vertical-align: middle">
                            
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <div id="headerConnectionStatus_replacement" style="display:none;position:relative; height:45px; width:100%">&nbsp;</div>
            <div class="content">
                <div class="main">
                    <div id="content">
                        <div style="width:100%; margin:auto; text-align:center">
                            <h1 class="font-orange" style="font-size:2em; padding:0; margin:10px 0">OUPS...</h1>
                            <div style="font-size:1em;">
                                <div style="margin-top:5px; border-top:1px solid #808080; padding-top:20px;font-weight:bold">
                                    Il semble que vos cookies soient désactivés dans votre navigateur.
                                </div>
                                <div style="margin-top:10px;">
                                    Pour pouvoir profiter en toute tranquilité de votre espace client Mon Compte-Nickel, veuillez activer vos cookies
                                </div>
                                <div style="margin-top:10px;">ou</div>
                                <div style="margin-top:10px;">essayer d'y accéder depuis un autre navigateur.</div>
                                <div style="margin:auto; margin-top:20px;">
                                    <img src="Styles/Img/cookies.png" alt="" style="max-width:45%;" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <asp:HiddenField ID="hfCookieSupported" runat="server" Value="" />
    </form>
</body>
</html>
