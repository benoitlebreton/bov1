﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using XMLMethodLibrary;

public partial class mobile_SiteMobile : System.Web.UI.MasterPage
{
    protected void Page_Init(object sender, EventArgs e)
    {
        string sPageName = Request.Url.AbsoluteUri.Substring(Request.Url.AbsoluteUri.LastIndexOf('/') + 1);

        panelDisconnect.Visible = false;
        panelMenu.Visible = false;
        authentication auth = authentication.GetCurrent(false);

        bool isPageAuth = false;

        if (auth != null)
        {
            panelMenu.Visible = true;
            panelDisconnect.Visible = true;
            if (!IsPostBack)
            {
                //<a href="AgencySearch.aspx">Recherche agence</a></div>
                string sXmlOut = tools.GetRights(authentication.GetCurrent().sToken, "SC_Mobile_Menu");

                List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Page", "RC");

                if (listRC.Count > 0 && listRC[0] == "0")
                {
                    List<string> listAction = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/Page/Action");
                    DataTable dtMenu = new DataTable();
                    dtMenu.Columns.Add("Link");

                    for (int i = 0; i < listAction.Count; i++)
                    {
                        List<string> listTagAction = CommonMethod.GetAttributeValues(listAction[i], "Action", "TagAction");
                        List<string> listViewAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ViewAllowed");

                        if (listTagAction.Count > 0 && listViewAllowed.Count > 0 && listViewAllowed[0] == "1")
                        {
                            switch (listTagAction[0])
                            {
                                case "AgencySearch":
                                    if (sPageName.ToLower() == "agencysearch.aspx")
                                        isPageAuth = true;
                                    dtMenu.Rows.Add(new object[] { "<a href=\"AgencySearch.aspx\">Recherche agence</a>" });
                                    break;
                            }
                        }
                    }

                    rptMenu.DataSource = dtMenu;
                    rptMenu.DataBind();

                    if (!isPageAuth && sPageName.ToLower() != "default.aspx")
                        Response.Redirect("Default.aspx");
                }
            }
        }
        else if (sPageName.Trim().ToLower() != "authentication.aspx")
        {
            //if (sPageName.Trim().ToLower() == "erreurcookies.aspx" && hfNoCookies.Value.Trim() == "true")
            //    Response.Redirect("./ErreurCookies.aspx");
            //else
            Response.Redirect("./Authentication.aspx");
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        checkNoCookies();
    }

    protected void clickDisconnectMe(object sender, EventArgs e)
    {
        Session.RemoveAll();
        Response.Redirect("./Authentication.aspx");
    }

    protected void checkNoCookies() 
    {
        hfNoCookies.Value = "";
        bool bCookieSupported = true;

        if (!Request.Browser.Cookies)
        {
            bCookieSupported = false;
        }

        if (Request.Cookies["AspxAutoDetectCookieSupport"] == null)
        {
            bCookieSupported = false;
        }

        if (Request.Headers["Cookie"] == null || Request.Headers["Cookie"].ToString() == "")
        {
            bCookieSupported = false;
        }

        if (!bCookieSupported)
        {
            hfNoCookies.Value = "true";
            Response.Redirect("ErreurCookies.aspx");
        }
    }
}

