﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class mobile_saveToFile : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string sFilePath = "";
        try
        {
            HttpFileCollection files = HttpContext.Current.Request.Files;
            for (int index = 0; index < files.Count; index++)
            {
                HttpPostedFile uploadfile = files[index];
                // You must create “upload” sub folder under the wwwroot.
                //uploadfile.SaveAs(Server.MapPath(".") + "\\upload\\" + uploadfile.FileName);
                sFilePath = System.Web.HttpContext.Current.Request.MapPath(".") + "/Upload/" + uploadfile.FileName;
                uploadfile.SaveAs(sFilePath);
                //sfiles += Server.MapPath(".") + "\\upload\\" + uploadfile.FileName;
            }
            HttpContext.Current.Response.Write("Upload successfully!");
        }
        catch (Exception ex)
        {
        }
    }
}