﻿<%@ Page Title="Authentification - BO Compte-Nickel" Language="C#" MasterPageFile="~/mobile/SiteMobile.master" AutoEventWireup="true" CodeFile="Authentication.aspx.cs" Inherits="mobile_Authentication" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <script type="text/javascript">
        $(function () {
            init();
        });

        function init() {
            $('#<%=btnConnection.ClientID%>').show();
            if ($('#<%=LoginTxt.ClientID%>') != null && $('#<%=LoginTxt.ClientID%>').val() != null) {
                $('#<%=LoginTxt.ClientID%>').watermark("<%=Resources.res.LoginID %>").click(function () {
                    $(this).removeClass("reqBorder");
                });
            }
            $('#<%=PassTxt.ClientID%>').watermark("<%=Resources.res.Password %>").click(function () {
                $(this).removeClass("reqBorder");
            });
        }

        function changeLogin() {
            __doPostBack("deleteLoginCookie", "");
        }

        function forgotPassword() {
            showForgotPassword();
        }

        function showForgotPassword() {
            $('#emailError_pwdForgotten').html('');
            $('#div_pwdForgotten').show();
            $('#dialog-message').dialog({
                resizable: false,
                draggable: false,
                modal: true,
                title: "Mot de passe oublié",
                buttons: [
                    { text: 'Annuler', click: function () { $(this).dialog('close'); } },
                    {
                        text: 'Valider', click: function () {
                            $('#emailError_pwdForgotten').html('');
                            if (checkEmail_pwdForgotten()) {
                                $(this).dialog('close');
                                __doPostBack("forgotPassword", $('#<%= txtEmail_pwdForgotten.ClientID %>').val());
                            }
                            else
                                $('#emailError_pwdForgotten').html('e-mail non valide');
                        }
                    }
                ]
            });
            }

            function showMessage(title, message, redirect) {
                var sTitle = "Message";
                if (message != null && message.trim().length > 0)
                    $('#<%=lblInfoMessage.ClientID%>').html(message.trim());
            if (title != null && title.trim().length > 0)
                sTitle = title;

            $('#dialog-message').dialog({
                resizable: false,
                draggable: false,
                modal: true,
                title: sTitle,
                buttons: [{ text: 'Fermer', click: function () { $(this).dialog('close'); if (redirect == "true") { document.location.href = "Default.aspx"; } } }]
            });

        }

        function checkEmail_pwdForgotten() {
            var isOK = false;
            var email = $('#<%= txtEmail_pwdForgotten.ClientID %>').val().trim();

            if (email.length > 0) {
                var email_regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
                isOK = email_regex.test(email);
            }

            return isOK;
        }

        function btnConnectionClick() {
            var isOK = true;

            $('#<%=PassTxt.ClientID%>').attr('disabled', 'disabled');
            $('#<%=btnConnection.ClientID%>').hide();

            $('#<%=PassTxt.ClientID%>').removeAttr("reqBorder");
            
            if ($('#<%=PassTxt.ClientID%>').val().trim().length == 0) {
                isOK = false;
                setTimeout(function () { $('#<%=PassTxt.ClientID%>').addClass("reqBorder"); }, 100);
            }

            if ($('#<%=LoginTxt.ClientID%>') != null && $('#<%=LoginTxt.ClientID%>').val() != null) {
                $('#<%=LoginTxt.ClientID%>').attr('disabled', 'disabled');
                $('#<%=LoginTxt.ClientID%>').removeAttr("reqBorder");

                if ($('#<%=LoginTxt.ClientID%>').val().trim().length == 0) {
                    isOK = false;
                    setTimeout(function () { $('#<%=LoginTxt.ClientID%>').addClass("reqBorder"); }, 100);
                }
            }

            if (!isOK) {
                $('#<%=btnConnection.ClientID%>').show();
                $('#<%=LoginTxt.ClientID%>').removeAttr('disabled');
                $('#<%=PassTxt.ClientID%>').removeAttr('disabled');
            }

            return isOK;
        }

    </script>
    <asp:UpdatePanel ID="UpdatePanelSend" runat="server" width="100%" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Panel ID="panelAuthentication" runat="server" CssClass="authentication" DefaultButton="btnConnection" style="position:relative; width:100%">
                <div style="margin:auto; width:100%">
                    <div style="padding-bottom:5px; margin:auto; width:100%">
                        <h2 style="color:#344b56; margin:5px 0;text-transform:uppercase; text-align:center">
                            Authentification
                        </h2>
                        <div style="border: 2px solid #f57527;border-radius:8px; padding: 0;">
                            <div style="margin:auto; padding:10px">
                                <asp:Panel ID="panelLoginNotSaved" runat="server" style="margin-top:5px">
                                    <div class="table" style="width:98%">
                                        <div class="table-row">
                                            <div class="table-cell" style="text-align:left; padding-left:0;width:100%;">
                                                <asp:TextBox ID="LoginTxt" runat="server" autocomplete="off" style="width:100%" CssClass="textbox"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div style="margin-top:5px">
                                        <asp:CheckBox ID="cbSaveClientID" runat="server" /><label for="<%=cbSaveClientID.ClientID%>" style="font-size:0.9em"> Enregistrer mon identifiant</label>
                                    </div>
                                </asp:Panel>
                                <asp:Panel ID="panelLoginSaved" runat="server" Visible="false">
                                    <asp:Label ID="LoginTxtLbl" runat="server" style="font-family:Arial; color:#f57527"></asp:Label>
                                    <div style="margin-top:2px">
                                        <a class="linkNoStyle" style="font-size:14px; " onclick="changeLogin();">se connecter avec un autre identifiant ?</a>
                                    </div>
                                </asp:Panel>
                            </div>
                            <div style="margin:auto; padding:10px">
                                <div class="table" style="width:98%">
                                    <div class="table-row">
                                        <div class="table-cell" style="text-align:left; padding-left:0;width:100%">
                                            <asp:TextBox ID="PassTxt" runat="server" TextMode="Password" autocomplete="off" style="width:100%" ></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div style="margin-top:2px">
                                    <a class="linkNoStyle" style="font-size:14px;display:none" onclick="forgotPassword();">mot de passe oublié ?</a>
                                </div>
                            </div>
                        </div>
                        <div style="width:100%; text-align:right;padding:0; margin:0">
                            <div style="margin-top:10px">
                                <asp:Button CssClass="button" style="width:100%" ID="btnConnection" runat="server" Text="Connexion"
                                    OnClick="btnConnection_Click" OnClientClick="return btnConnectionClick();" />
                            </div>
                        </div>
                    </div>
                    
                    <asp:UpdateProgress ID="UpdateProgressSend" runat="server" AssociatedUpdatePanelID="UpdatePanelSend">
                        <ProgressTemplate>
                            <div style="position:relative;width:100%;">
                                <div style="width: 100%; height: 100%; text-align: center;position:absolute; ">
                                    <img src="../Styles/Img/loading.gif" alt="loading" style="width:30px;height:30px" />
                                </div>
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </div>
            </asp:Panel>
            <asp:Label ID="lblAuthenticationError" runat="server" ForeColor="Red"></asp:Label>

            <div id="dialog-message" style="display:none">
                <asp:Label ID="lblInfoMessage" runat="server" style="font-weight:bold"></asp:Label>
                <div id="div_pwdForgotten" style="display:none">
                    <div>
                        Veuillez saisir votre e-mail d'identification
                    </div>
                    <asp:TextBox ID="txtEmail_pwdForgotten" runat="server" MaxLength="500" style="width:400px"></asp:TextBox>
                    <div style="padding:10px 0; height:20px">
                        <span id="emailError_pwdForgotten" style="color:red; font-weight:bold"></span>
                    </div>
                </div>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>
<script runat="server">
    protected override void OnPreInit(EventArgs e)
    {
         base.OnPreInit(e);
     
         if (Request.UserAgent != null &&Request.UserAgent.IndexOf("AppleWebKit", StringComparison.CurrentCultureIgnoreCase) > -1)
         {
              this.ClientTarget = "uplevel";
         }
    }
</script>