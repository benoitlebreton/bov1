﻿// Listen for ALL links at the top level of the document. For
// testing purposes, we're not going to worry about LOCAL vs.
// EXTERNAL links - we'll just demonstrate the feature.
$(document).on(
	"click",
	"a",
	function (event) {
	    // Manually change the location of the page to stay in
	    // "Standalone" mode and change the URL at the same time.
	    var href = $(event.target).attr("href");
	    var target = $(event.target).attr("target");
	    if (href != undefined && target != "_blank") {
	        // Stop the default behavior of the browser, which
	        // is to change the URL of the page.
	        event.preventDefault();
	        //console.log(href);
	        location.href = href;
	    }
	}
);