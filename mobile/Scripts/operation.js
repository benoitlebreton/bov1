﻿function ToggleOperationDetails(row) {
    if ($(row).next('tr').find('.operation-detail, .operation-detail-alternate').is(':hidden')) {
        $(row).next('tr').find('td').css('background-color', $(row).find('td').css('background-color'));
        //$(row).next('tr').find('td').css('padding-bottom', '5px');
        $(row).next('tr').find('.operation-detail, .operation-detail-alternate').slideDown(400);
        //$(row).find('td').css('border-bottom-right-radius', '0').css('border-bottom-left-radius', '0');

        //$(row).find('.detail-arrow').attr('src', $(row).find('.detail-arrow').attr('src').replace(/down/g, 'up'));
    }
    else {
        $(row).next('tr').find('.operation-detail, .operation-detail-alternate').slideUp(400, function () {
            //$(row).find('td:first-of-type').css('border-top-left-radius', '8px').css('border-bottom-left-radius', '8px');
            //$(row).find('td:last-of-type').css('border-top-right-radius', '8px').css('border-bottom-right-radius', '8px');
            //$(row).next('tr').find('td').animate({ paddingBottom: 0 }, 0);
        });

        //$(row).find('.detail-arrow').attr('src', $(row).find('.detail-arrow').attr('src').replace(/up/g, 'down'));
    }
}