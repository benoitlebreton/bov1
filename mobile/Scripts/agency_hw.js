﻿var hfMpadReinitCode_ID;
var hfReload_ID
var btnRefreshAgencyDetails_ID;
var panelSearch_ID;
var btnSearch_ID;
var txtSearch_ID;
var panelAgencyDetails_ID;
var panelSearchAgencyToAllocate_ID;
var panelSearchAgencyToDeallocate_ID;
var panelAgencyDetails_AgencyToDeallocate_ID;
var hfPanelActive_ID;
var btnSearchAgencyToAllocate_ID;
var hfHardwareAllocateDirection_ID;
var panelHardwareReallocate_ID;
var panelHardwareReallocateComment_ID;
var panelHardwareReallocateConfirm_ID;
var hfHardwareType_selected_ID;
var btnConfirmHardwareAllocate_ID;
var btnSearchAgencyToDeallocate_ID;
var btnRefreshAgencyDetails_AgencyToDeallocate_ID;
var hfRefPartner_AgencyToDeallocate_selected_ID;
var hfRefAgency_AgencyToDeallocate_selected_ID;
var hfAgencyID_AgencyToDeallocate_selected_ID;
var hfAgencyName_AgencyToDeallocate_selected_ID;
var hfManagerLastName_AgencyToDeallocate_selected_ID;
var hfManagerFirstName_AgencyToDeallocate_selected_ID;
var hfHardwareSerialList_AgencyToDeallocate_ID;
var txtHardwareToAddSearch_ID;
var btnHardwareToAddSearch_ID;
var btnMpadSearch_ID;
var txtMpadSearch_ID;
var panelMpadSearchAdd_ID;
var panelPosSearchAdd_ID;
var hfPosSerialList_ID;
var hfPosSerialList_temp_ID;

var hfTabletSerialList_temp_ID;
var hfTabletSupportSerialList_temp_ID;
var hfScannerSerialList_temp_ID;
var hfPinpadSerialList_temp_ID;
var hfTabletSerialList_ID;
var hfScannerSerialList_ID;
var hfPinpadSerialList_ID;

var panelTabletSearchAdd_ID;
var panelTabletSupportSearchAdd_ID;
var panelScannerSearchAdd_ID;
var panelPinpadSearchAdd_ID;

var txtPosSearch_ID;
var btnPosSearch_ID;
var txtTabletSearch_ID;
var btnTabletSearch_ID;
var txtTabletSupportSearch_ID;
var btnTabletSupportSearch_ID;
var txtScannerSearch_ID;
var btnScannerSearch_ID;
var txtPinpadSearch_ID;
var btnPinpadSearch_ID;
var hfMpadSerialList_temp_ID;
var hfMpadSerialList_ID;
var hfRefPartner_selected_ID;
var hfRefAgency_selected_ID;
var hfAgencyID_selected_ID;
var hfAgencyName_selected_ID;
var hfManagerLastName_selected_ID;
var hfManagerFirstName_selected_ID;
var hfAgencyAddress_selected_ID;
var hfHardwareSerial_selected_ID;
var panelMpadDetails_ID;
var panelMpadReinitCodeResult_ID;
var hfAgencyID_destination_ID;
var hfRefPartner_destination_ID;
var panelHardwareReallocateResult_ID;
var panelHardwareReallocateLoading_ID;

function initDialogs() {
    $('#divMpadDetail-popup').dialog({
        autoOpen: false,
        resizable: false,
        draggable: false,
        modal: true,
        title: "Détail borne",
        open: function (event, ui) {
            $('.ui-widget-overlay').bind('click', function () { $("#divMpadDetail-popup").dialog('close'); });
            $('.ui-dialog-titlebar').bind('click', function () { $("#divMpadDetail-popup").dialog('close'); });
        },
        close: function (event, ui) {
            if ($('#'+hfMpadReinitCode_ID).val().trim() == "true") {
                $('#'+btnRefreshAgencyDetails_ID).click();
            }
        }
    });
    $('#divMpadDetail-popup').parent().appendTo(jQuery("form:first"));

    $('#divHardwareReallocate-popup').dialog({
        autoOpen: false,
        resizable: false,
        draggable: false,
        modal: true,
        title: "Affectation borne",
        open: function (event, ui) {
            $('.ui-widget-overlay').bind('click', function () { $("#divHardwareReallocate-popup").dialog('close'); });
            $('.ui-dialog-titlebar').bind('click', function () { $("#divHardwareReallocate-popup").dialog('close'); });
        },
        close: function (event, ui) {
            if ($('#'+hfReload_ID).val().trim() == "true")
                window.location = "AgencySearch.aspx";
        }
    });
    $('#divHardwareReallocate-popup').parent().appendTo(jQuery("form:first"));

    $('#divPosDetail-popup').dialog({
        autoOpen: false,
        resizable: false,
        draggable: false,
        modal: true,
        title: "Détail TPE",
        open: function (event, ui) {
            $('.ui-widget-overlay').bind('click', function () { $("#divPosDetail-popup").dialog('close'); });
            $('.ui-dialog-titlebar').bind('click', function () { $("#divPosDetail-popup").dialog('close'); });
        }
    });
    $('#divPosDetail-popup').parent().appendTo(jQuery("form:first"));

    $('#divTabletDetail-popup').dialog({
        autoOpen: false,
        resizable: false,
        draggable: false,
        modal: true,
        title: "Détail tablette",
        open: function (event, ui) {
            $('.ui-widget-overlay').bind('click', function () { $("#divTabletDetail-popup").dialog('close'); });
            $('.ui-dialog-titlebar').bind('click', function () { $("#divTabletDetail-popup").dialog('close'); });
        },
        close: function (event, ui) {
        }
    });
    $('#divTabletDetail-popup').parent().appendTo(jQuery("form:first"));

    $('#divTabletSupportDetail-popup').dialog({
        autoOpen: false,
        resizable: false,
        draggable: false,
        modal: true,
        title: "Détail support tablette",
        open: function (event, ui) {
            $('.ui-widget-overlay').bind('click', function () { $("#divTabletSupportDetail-popup").dialog('close'); });
            $('.ui-dialog-titlebar').bind('click', function () { $("#divTabletSupportDetail-popup").dialog('close'); });
        },
        close: function (event, ui) {
        }
    });
    $('#divTabletSupportDetail-popup').parent().appendTo(jQuery("form:first"));

    $('#divScannerDetail-popup').dialog({
        autoOpen: false,
        resizable: false,
        draggable: false,
        modal: true,
        title: "Détail scanner",
        open: function (event, ui) {
            $('.ui-widget-overlay').bind('click', function () { $("#divScannerDetail-popup").dialog('close'); });
            $('.ui-dialog-titlebar').bind('click', function () { $("#divScannerDetail-popup").dialog('close'); });
        },
        close: function (event, ui) {
        }
    });
    $('#divScannerDetail-popup').parent().appendTo(jQuery("form:first"));

    $('#divPinpadDetail-popup').dialog({
        autoOpen: false,
        resizable: false,
        draggable: false,
        modal: true,
        title: "Détail pinpad",
        open: function (event, ui) {
            $('.ui-widget-overlay').bind('click', function () { $("#divPinpadDetail-popup").dialog('close'); });
            $('.ui-dialog-titlebar').bind('click', function () { $("#divPinpadDetail-popup").dialog('close'); });
        },
        close: function (event, ui) {
        }
    });
    $('#divPinpadDetail-popup').parent().appendTo(jQuery("form:first"));
}

function initPanel() {
    $('#'+panelSearch_ID).hide();
    $('#'+panelAgencyDetails_ID).hide();
    $('#'+panelSearchAgencyToAllocate_ID).hide();
    $('#'+panelSearchAgencyToDeallocate_ID).hide();
    $('#'+panelAgencyDetails_AgencyToDeallocate_ID).hide();

    switch ($('#' + hfPanelActive_ID).val()) {
        case "":
        case "search":
            $('#' + panelSearch_ID).show();
            if ($('#' + txtSearch_ID).val().trim().length > 0)
                $('#' + btnSearch_ID).click();
            break;
        case "details":
            $('#'+panelAgencyDetails_ID).show();
            $('#'+btnRefreshAgencyDetails_ID).click();
            break;
        case "allocate":
            $('#'+panelSearchAgencyToAllocate_ID).show();
            $('#'+btnSearchAgencyToAllocate_ID).click();
            break;
        case "deallocate":
            $('#'+panelSearchAgencyToDeallocate_ID).show();
            $('#'+btnSearchAgencyToDeallocate_ID).click();
            break;
        case "deallocate_details":
            $('#'+panelAgencyDetails_AgencyToDeallocate_ID).show();
            $('#'+btnRefreshAgencyDetails_AgencyToDeallocate_ID).click();
            break;
    }
}

function HardwareReallocateCancel() {
    $('#divHardwareReallocate-popup').dialog('close');
}
function HardwareReallocate() {
    if ($('#'+ hfHardwareAllocateDirection_ID).val().trim() != 'add') {
        $('#' + panelHardwareReallocate_ID).hide();
        $('#' + panelHardwareReallocateComment_ID).hide();
        $('#' + panelHardwareReallocateConfirm_ID).show();

        $('#divHardwareReallocateWarning').hide();
        var sWarningMessage = "";
        var sConfirmMessage = "Voulez-vous vraiment désaffecter ce matériel ?";

        //console.log($('#' + hfHardwareType_selected_ID).val());

        switch($('#' + hfHardwareType_selected_ID).val()){
            case "MPD":
                $('#divHardwareReallocateWarning').show();
                sConfirmMessage = "Voulez-vous vraiment désaffecter cette borne ?";
                sWarningMessage = "Attention! Cette action va rendre la borne inutilisable tant que le nouveau code ne sera pas saisi sur la borne.";
                break;
            case "POS":
                sConfirmMessage = "Voulez-vous vraiment désaffecter ce terminal ?";
                break;
            case "TAB":
                sConfirmMessage = "Voulez-vous vraiment désaffecter cette tablette ?";
                break;
            case "SCN":
                sConfirmMessage = "Voulez-vous vraiment désaffecter ce scanner ?";
                break;
            case "PPD":
                sConfirmMessage = "Voulez-vous vraiment désaffecter ce pinpad ?";
                break;
            case "PDB":
                sConfirmMessage = "Voulez-vous vraiment désaffecter ce support tablette ?";
                break;
            default:
                sConfirmMessage = "Voulez-vous vraiment désaffecter ce matériel ?";
                break;
        }

        $('#divHardwareReallocateConfirmMessage').html(sConfirmMessage);
        //if ($('#' + hfHardwareType_selected_ID).val() == "POS") {
            //$('#divHardwareReallocateConfirmMessage').html("Voulez-vous vraiment désaffecter ce terminal ?");
            //$('#divMpadReallocateWarning').hide();
        //}
        //else {
            //$('#divHardwareReallocateConfirmMessage').html("Voulez-vous vraiment désaffecter cette borne ?");
            //$('#divMpadReallocateWarning').show();
        //}
    }
    else {
        $('#'+btnConfirmHardwareAllocate_ID).click();
    }

}
function HardwareReallocateComment() {
    $('#' + panelHardwareReallocate_ID).hide();
    $('#' + panelHardwareReallocateConfirm_ID).hide();
    $('#' + panelHardwareReallocateComment_ID).show();
}

function CancelHardwareAllocate() {
    $('#' + panelHardwareReallocateConfirm_ID).hide();
    $('#' + panelHardwareReallocate_ID).show();
}

function showConfirmReinitCode() {
    $('#divMpadDetails').hide();
    $('#divMpadDetail-popup').dialog("option", "title", "Réinitialiser code");
    $('#divMpadReinitCode').show();
}
function hideConfirmReinitCode() {
    $('#divMpadReinitCode').hide();
    $('#divMpadDetail-popup').dialog("option", "title", "Détail borne");
    $('#divMpadDetails').show();
}

function agencySearchAlert(sTitle, sMessage) {
    $('#divAlert-message').html(sMessage);
    $('#divAlert-popup').dialog({
        autoOpen: true,
        resizable: false,
        draggable: false,
        modal: true,
        title: sTitle,
        open: function (event, ui) {
            $('.ui-widget-overlay').bind('click', function () { $("#divAlert-popup").dialog('close'); });
            $('.ui-dialog-titlebar').bind('click', function () { $("#divAlert-popup").dialog('close'); });
        }
    });
}


/*** AJOUT HARDWARE ***/
function showSearchAgencyToDeallocate(sHardwareType) {
    if (sHardwareType == null)
        sHardwareType = "MPD";

    $('#' + hfHardwareType_selected_ID).val(sHardwareType);
    switch (sHardwareType) {
        default:
        case "MPD":
            $('#divMpadDetail-popup').dialog('close');
            break;
        case "POS":
            $('#divPosDetail-popup').dialog('close');
            break;
        case "TAB":
            $('#divTabletDetail-popup').dialog('close');
            break;
        case "SCN":
            $('#divScannerDetail-popup').dialog('close');
            break;
        case "PPD":
            $('#divPinpadDetail-popup').dialog('close');
            break;
        case "PDB":
            $('#divTabletSupportDetail-popup').dialog('close');
            break;
    }

    $('#' + btnSearchAgencyToDeallocate_ID).click();
    $('#' + panelAgencyDetails_ID).fadeOut()
    setTimeout(function () {
        $('#' + panelSearchAgencyToDeallocate_ID).show("slide", { direction: "up" }, 500)
    }, 500);

    $('#' + hfPanelActive_ID).val("deallocate");

    return false;
}
function hideSearchAgencyToDeallocate() {
    $('#'+panelSearchAgencyToDeallocate_ID).hide("slide", { direction: "up" }, 500, function () {
        $('#'+panelAgencyDetails_ID).fadeIn();
    });

    $('#' + hfPanelActive_ID).val("details");

    return false;
}

function showAgencyToDeallocateDetails(refPartner, refAgency, agencyID, agencyName, managerLastName, managerFirstName) {
    $('#'+panelSearchAgencyToDeallocate_ID).hide("slide", { direction: "left" }, 500, function () {
        $('#'+hfRefPartner_AgencyToDeallocate_selected_ID).val(refPartner);
        $('#'+hfRefAgency_AgencyToDeallocate_selected_ID).val(refAgency);
        $('#'+hfAgencyID_AgencyToDeallocate_selected_ID).val(agencyID);
        $('#'+hfAgencyName_AgencyToDeallocate_selected_ID).val(agencyName);
        $('#'+hfManagerLastName_AgencyToDeallocate_selected_ID).val(managerLastName)
        $('#'+hfManagerFirstName_AgencyToDeallocate_selected_ID).val(managerFirstName);
        $('#'+btnRefreshAgencyDetails_AgencyToDeallocate_ID).click();
        $('#'+panelAgencyDetails_AgencyToDeallocate_ID).fadeIn();

    });
    $('#'+hfPanelActive_ID).val("deallocate_details");
}

function backToAgencyToDeallocate() {
    $('#' + panelAgencyDetails_AgencyToDeallocate_ID).fadeOut(200, "swing", function () {
        $('#' + panelSearchAgencyToDeallocate_ID).show("slide", { direction: "left" }, 500);
        $('#' + btnSearchAgencyToDeallocate_ID).click();
    });

    $('#' + hfPanelActive_ID).val("deallocate");
}

var arHardwareToAddSearchResult;
function autocomplete_HardwareToAddSearch(showAlert) {
    //if(showAlert != 'false')
    //AlertMessage("Cette agence contient trop de bornes pour les afficher. <br/>Veuillez rechercher par numéro de série.");
    if (txtHardwareToAddSearch_ID != null && $('#'+txtHardwareToAddSearch_ID) != null && $('#'+hfHardwareSerialList_AgencyToDeallocate_ID).val().trim().length > 0) {
        $('#'+txtHardwareToAddSearch_ID).watermark("Recherche matériel par numéro série").autocomplete({
            source: function (request, response) {
                var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
                var hardwareList = $('#'+hfHardwareSerialList_AgencyToDeallocate_ID).val().trim().split(';');
                arHardwareToAddSearchResult = new Array();
                arHardwareToAddSearchResult = $.grep(hardwareList, function (item) { return matcher.test(item); });

                //LIMITE A 3 RESULTATS MAX
                arSerialListToShow = new Array();
                if (arHardwareToAddSearchResult.length > 3) {
                    arSerialListToShow.push(arHardwareToAddSearchResult[0]);
                    arSerialListToShow.push(arHardwareToAddSearchResult[1]);
                    arSerialListToShow.push(arHardwareToAddSearchResult[2]);
                }
                else
                    arSerialListToShow = arHardwareToAddSearchResult;

                response($.grep(arSerialListToShow, function (item) {
                    return matcher.test(item);
                }));
            },
            position: { my: "left bottom", at: "left top", collision: "flip" },
            max: 3,
            select: function (event, ui) {
                setTimeout(function () { HardwareToAddSearchClick(); }, 500);
            }
        });

    }
}

function HardwareToAddSearchClick() {
    $('#'+btnHardwareToAddSearch_ID).click();
}
function checkHardwareToAddSearch() {
    isOK = false;
    var sHardwareType = "";

    //console.log($('#' + txtHardwareToAddSearch_ID).val());
    //console.log(arHardwareToAddSearchResult);

    //alert($(<%=txtHardwareToAddSearch.ClientID%>).val());

    switch ($('#'+hfHardwareType_selected_ID).val().trim()) {
        case "POS":
            sHardwareType = "terminal";
            break;
        case "TAB":
            sHardwareType = "tablette";
            break;
        case "SCN":
            sHardwareType = "scanner";
            break;
        case "PPD":
            sHardwareType = "pinpad";
            break;
        case "PDB":
            sHardwareType = "support tablette";
        case "MPD":
        default:
            sHardwareType = "borne";
            break;

    }

    //if ($('#<%=hfHardwareType_selected.ClientID%>').val().trim() == "POS")
    //sHardwareType = "terminal";

    if ($('#'+txtHardwareToAddSearch_ID).val().trim().length > 0) {
        if (arHardwareToAddSearchResult.length > 0) {
            for (var i = 0; i < arHardwareToAddSearchResult.length; i++) {
                if ($('#'+txtHardwareToAddSearch_ID).val().trim() == arHardwareToAddSearchResult[i].trim())
                    isOK = true;
            }
        }

        if (!isOK)
            AlertMessage("Ce numéro de série de " + sHardwareType + " n'existe pas dans cette agence");
    }
    else
        AlertMessage("Veuillez saisir un numéro de série de " + sHardwareType);
    return isOK;
}

function showAgencyAddress() {
    if ($('#divAgencyAddress').is(':visible')) {
        $('#divAgencyAddress').slideUp();
        $('#divShowAgencyAddress').html("afficher l'adresse");
    }
    else {
        $('#divAgencyAddress').slideDown();
        $('#divShowAgencyAddress').html("masquer l'adresse");
    }
}

var arMpadSearchResult;
function autocomplete_MpadSearch(showAlert) {
    if ($('#' + hfMpadSerialList_temp_ID) != null && $('#' + hfMpadSerialList_temp_ID).val() != null && $('#' + hfMpadSerialList_temp_ID).val().trim().length > 0)
        $('#'+hfMpadSerialList_ID).val($('#'+ hfMpadSerialList_temp_ID).val().trim());

    //if(showAlert != 'false')
    //AlertMessage("Cette agence contient trop de bornes pour les afficher. <br/>Veuillez rechercher par numéro de série.");
    if (txtMpadSearch_ID != null && $('#'+txtMpadSearch_ID) != null && $('#'+hfMpadSerialList_ID).val().trim().length > 0) {
        $('#'+txtMpadSearch_ID).watermark("Recherche par numéro série").autocomplete({
            source: function (request, response) {
                var hwList = $('#' + hfMpadSerialList_ID).val().trim().split(';');
                arMpadSearchResult = new Array();

                var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
                arMpadSearchResult = $.grep(hwList, function (item) { return matcher.test(item); });

                //LIMITE A 3 RESULTATS MAX
                var arSerialListToShow = new Array();
                if (arMpadSearchResult.length > 3) {
                    arSerialListToShow.push(arMpadSearchResult[0]);
                    arSerialListToShow.push(arMpadSearchResult[1]);
                    arSerialListToShow.push(arMpadSearchResult[2]);
                }
                else
                    arSerialListToShow = arMpadSearchResult;

                response($.grep(arSerialListToShow, function (item) {
                    return matcher.test(item);
                }));
            },
            position: { my: "left bottom", at: "left top", collision: "flip" },
            max: 3,
            select: function (event, ui) {
                $('#'+ panelMpadSearchAdd_ID).fadeOut(function () {
                    MpadSearchClick();
                });

            }
        })
    }
}

function MpadSearchClick() {
    $('#' + btnMpadSearch_ID).click();
}
function TabletSearchClick() {
    $('#' + btnTabletSearch_ID).click();
}
function ScannerSearchClick() {
    $('#' + btnScannerSearch_ID).click();
}
function PinpadSearchClick() {
    $('#' + btnPinpadSearch_ID).click();
}
function TabletSupportSearchClick() {
    $('#' + btnTabletSupportSearch_ID).click();
}

function checkMpadSearch() {
    isOK = false;

    //console.log($('#' + txtMpadSearch_ID).val());
    //console.log(arMpadSearchResult);
    //console.log();

    if ($('#'+txtMpadSearch_ID).val().trim().length > 0) {
        if (arMpadSearchResult.length > 0) {
            for (var i = 0; i < arMpadSearchResult.length; i++) {
                if ($('#'+txtMpadSearch_ID).val().trim() == arMpadSearchResult[i].trim())
                    isOK = true;
            }
        }

        if (!isOK)
            AlertMessage("Ce numéro de série de borne n'existe pas dans cette agence");
    }
    else
        AlertMessage("Veuillez saisir un numéro de série de borne");

    return isOK;
}

var arPosSearchResult;
function autocomplete_PosSearch(showAlert) {
    if ($('#' + hfPosSerialList_temp_ID) != null && $('#' + hfPosSerialList_temp_ID).val() != null && $('#' + hfPosSerialList_temp_ID).val().trim().length > 0)
        $('#' + hfPosSerialList_ID).val($('#'+hfPosSerialList_temp_ID).val().trim());


    if ($('#' + txtPosSearch_ID) != null && $('#' + txtPosSearch_ID).val() != null && $('#' + hfPosSerialList_ID).val().trim().length > 0) {
        $('#'+txtPosSearch_ID).watermark("Recherche par numéro série").autocomplete({
            source: function (request, response) {
                var hwList = $('#' + hfPosSerialList_ID).val().trim().split(';');
                arPosSearchResult = new Array();

                var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
                arPosSearchResult = $.grep(hwList, function (item) { return matcher.test(item); });

                //LIMITE A 3 RESULTATS MAX
                var arSerialListToShow = new Array();
                if (arPosSearchResult.length > 3) {
                    arSerialListToShow.push(arPosSearchResult[0]);
                    arSerialListToShow.push(arPosSearchResult[1]);
                    arSerialListToShow.push(arPosSearchResult[2]);
                }
                else
                    arSerialListToShow = arPosSearchResult;

                response($.grep(arSerialListToShow, function (item) {
                    return matcher.test(item);
                }));
            },
            position: { my: "left bottom", at: "left top", collision: "flip" },
            max: 3,
            select: function (event, ui) {
                $('#'+panelPosSearchAdd_ID).fadeOut(function () {
                    PosSearchClick();
                });
            }
        })
    }
}

var arTabletSearchResult;
function autocomplete_TabletSearch(showAlert) {
    if ($('#' + hfTabletSerialList_temp_ID) != null && $('#' + hfTabletSerialList_temp_ID).val() != null && $('#' + hfTabletSerialList_temp_ID).val().trim().length > 0)
        $('#' + hfTabletSerialList_ID).val($('#' + hfTabletSerialList_temp_ID).val().trim());


    if ($('#' + txtTabletSearch_ID) != null && $('#' + txtTabletSearch_ID).val() != null && $('#' + hfTabletSerialList_ID).val().trim().length > 0) {
        $('#' + txtTabletSearch_ID).watermark("Recherche par numéro série").autocomplete({
            source: function (request, response) {
                var hwList = $('#' + hfTabletSerialList_ID).val().trim().split(';');
                arTabletSearchResult = new Array();

                var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
                arTabletSearchResult = $.grep(hwList, function (item) { return matcher.test(item); });

                //LIMITE A 3 RESULTATS MAX
                var arSerialListToShow = new Array();
                if (arTabletSearchResult.length > 3) {
                    arSerialListToShow.push(arTabletSearchResult[0]);
                    arSerialListToShow.push(arTabletSearchResult[1]);
                    arSerialListToShow.push(arTabletSearchResult[2]);
                }
                else
                    arSerialListToShow = arTabletSearchResult;

                response($.grep(arSerialListToShow, function (item) {
                    return matcher.test(item);
                }));
            },
            position: { my: "left bottom", at: "left top", collision: "flip" },
            max: 3,
            select: function (event, ui) {
                $('#' + panelTabletSearchAdd_ID).fadeOut(function () {
                    TabletSearchClick();
                });
            }
        })
    }
}

var arTabletSupportSearchResult;
function autocomplete_TabletSupportSearch(showAlert) {
    if ($('#' + hfTabletSupportSerialList_temp_ID) != null && $('#' + hfTabletSupportSerialList_temp_ID).val() != null && $('#' + hfTabletSupportSerialList_temp_ID).val().trim().length > 0)
        $('#' + hfTabletSupportSerialList_ID).val($('#' + hfTabletSupportSerialList_temp_ID).val().trim());


    if ($('#' + txtTabletSupportSearch_ID) != null && $('#' + txtTabletSupportSearch_ID).val() != null && $('#' + hfTabletSupportSerialList_ID).val().trim().length > 0) {
        $('#' + txtTabletSupportSearch_ID).watermark("Recherche par numéro série").autocomplete({
            source: function (request, response) {
                var hwList = $('#' + hfTabletSupportSerialList_ID).val().trim().split(';');
                arTabletSupportSearchResult = new Array();

                var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
                arTabletSupportSearchResult = $.grep(hwList, function (item) { return matcher.test(item); });

                //LIMITE A 3 RESULTATS MAX
                var arSerialListToShow = new Array();
                if (arTabletSupportSearchResult.length > 3) {
                    arSerialListToShow.push(arTabletSupportSearchResult[0]);
                    arSerialListToShow.push(arTabletSupportSearchResult[1]);
                    arSerialListToShow.push(arTabletSupportSearchResult[2]);
                }
                else
                    arSerialListToShow = arTabletSupportSearchResult;

                response($.grep(arSerialListToShow, function (item) {
                    return matcher.test(item);
                }));
            },
            position: { my: "left bottom", at: "left top", collision: "flip" },
            max: 3,
            select: function (event, ui) {
                $('#' + panelTabletSupportSearchAdd_ID).fadeOut(function () {
                    TabletSupportSearchClick();
                });
            }
        })
    }
}

var arScannerSearchResult;
function autocomplete_ScannerSearch(showAlert) {
    if ($('#' + hfScannerSerialList_temp_ID) != null && $('#' + hfScannerSerialList_temp_ID).val() != null && $('#' + hfScannerSerialList_temp_ID).val().trim().length > 0)
        $('#' + hfScannerSerialList_ID).val($('#' + hfScannerSerialList_temp_ID).val().trim());

    if ($('#' + txtScannerSearch_ID) != null && $('#' + txtScannerSearch_ID).val() != null && $('#' + hfScannerSerialList_ID).val().trim().length > 0) {
        $('#' + txtScannerSearch_ID).watermark("Recherche par numéro série").autocomplete({
            source: function (request, response) {
                var hwList = $('#' + hfScannerSerialList_ID).val().trim().split(';');
                arScannerSearchResult = new Array();

                var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
                arScannerSearchResult = $.grep(hwList, function (item) { return matcher.test(item); });

                //LIMITE A 3 RESULTATS MAX
                var arSerialListToShow = new Array();
                if (arScannerSearchResult.length > 3) {
                    arSerialListToShow.push(arScannerSearchResult[0]);
                    arSerialListToShow.push(arScannerSearchResult[1]);
                    arSerialListToShow.push(arScannerSearchResult[2]);
                }
                else
                    arSerialListToShow = arScannerSearchResult;

                response($.grep(arSerialListToShow, function (item) {
                    return matcher.test(item);
                }));
            },
            position: { my: "left bottom", at: "left top", collision: "flip" },
            max: 3,
            select: function (event, ui) {
                $('#' + panelScannerSearchAdd_ID).fadeOut(function () {
                    ScannerSearchClick();
                });
            }
        })
    }
}

var arPinpadSearchResult;
function autocomplete_PinpadSearch(showAlert) {
    if ($('#' + hfPinpadSerialList_temp_ID) != null && $('#' + hfPinpadSerialList_temp_ID).val() != null && $('#' + hfPinpadSerialList_temp_ID).val().trim().length > 0)
        $('#' + hfPinpadSerialList_ID).val($('#' + hfPinpadSerialList_temp_ID).val().trim());


    if ($('#' + txtPinpadSearch_ID) != null && $('#' + txtPinpadSearch_ID).val() != null && $('#' + hfPinpadSerialList_ID).val().trim().length > 0) {
        $('#' + txtPinpadSearch_ID).watermark("Recherche par numéro série").autocomplete({
            source: function (request, response) {
                var hwList = $('#' + hfPinpadSerialList_ID).val().trim().split(';');
                arPinpadSearchResult = new Array();

                var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
                arPinpadSearchResult = $.grep(hwList, function (item) { return matcher.test(item); });

                //LIMITE A 3 RESULTATS MAX
                var arSerialListToShow = new Array();
                if (arPinpadSearchResult.length > 3) {
                    arSerialListToShow.push(arPinpadSearchResult[0]);
                    arSerialListToShow.push(arPinpadSearchResult[1]);
                    arSerialListToShow.push(arPinpadSearchResult[2]);
                }
                else
                    arSerialListToShow = arPinpadSearchResult;

                response($.grep(arSerialListToShow, function (item) {
                    return matcher.test(item);
                }));
            },
            position: { my: "left bottom", at: "left top", collision: "flip" },
            max: 3,
            select: function (event, ui) {
                $('#' + panelPinpadSearchAdd_ID).fadeOut(function () {
                    PinpadSearchClick();
                });
            }
        })
    }
}

function PosSearchClick() {
    $('#'+btnPosSearch_ID).click();
}
function checkPosSearch() {
    isOK = false;
    if ($('#'+txtPosSearch_ID).val().trim().length > 0) {
        if (arPosSearchResult.length > 0) {
            for (var i = 0; i < arPosSearchResult.length; i++) {
                if ($('#'+txtPosSearch_ID).val().trim() == arPosSearchResult[i].trim())
                    isOK = true;
            }
        }

        if (!isOK)
            AlertMessage("Ce numéro de série de terminal n'existe pas dans cette agence");
    }
    else
        AlertMessage("Veuillez saisir un numéro de série de terminal");
    return isOK;
}
function checkTabletSearch() {
    isOK = false;
    if ($('#' + txtTabletSearch_ID).val().trim().length > 0) {
        if (arTabletSearchResult.length > 0) {
            for (var i = 0; i < arTabletSearchResult.length; i++) {
                if ($('#' + txtTabletSearch_ID).val().trim() == arTabletSearchResult[i].trim())
                    isOK = true;
            }
        }

        if (!isOK)
            AlertMessage("Ce numéro de série de tablette n'existe pas dans cette agence");
    }
    else
        AlertMessage("Veuillez saisir un numéro de série de tablette");
    return isOK;
}
function checkTabletSupportSearch() {
    isOK = false;
    if ($('#' + txtTabletSupportSearch_ID).val().trim().length > 0) {
        if (arTabletSupportSearchResult.length > 0) {
            for (var i = 0; i < arTabletSupportSearchResult.length; i++) {
                if ($('#' + txtTabletSupportSearch_ID).val().trim() == arTabletSupportSearchResult[i].trim())
                    isOK = true;
            }
        }

        if (!isOK)
            AlertMessage("Ce numéro de série de support tablette n'existe pas dans cette agence");
    }
    else
        AlertMessage("Veuillez saisir un numéro de série de support tablette");
    return isOK;
}
function checkScannerSearch() {
    //console.log('checkScannerSearch');

    isOK = false;
    if ($('#' + txtScannerSearch_ID).val().trim().length > 0) {
        if (arScannerSearchResult.length > 0) {
            for (var i = 0; i < arScannerSearchResult.length; i++) {
                if ($('#' + txtScannerSearch_ID).val().trim() == arScannerSearchResult[i].trim())
                    isOK = true;
            }
        }

        if (!isOK)
            AlertMessage("Ce numéro de série de scanner n'existe pas dans cette agence");
    }
    else
        AlertMessage("Veuillez saisir un numéro de série de scanner");
    return isOK;
}
function checkPinpadSearch() {
    isOK = false;
    if ($('#' + txtPinpadSearch_ID).val().trim().length > 0) {
        if (arPinpadSearchResult.length > 0) {
            for (var i = 0; i < arPinpadSearchResult.length; i++) {
                if ($('#' + txtPinpadSearch_ID).val().trim() == arPinpadSearchResult[i].trim())
                    isOK = true;
            }
        }

        if (!isOK)
            AlertMessage("Ce numéro de série de pinpad n'existe pas dans cette agence");
    }
    else
        AlertMessage("Veuillez saisir un numéro de série de pinpad");
    return isOK;
}


function checkAgencySearch() {
    var isOK = true;
    $('#'+txtSearch_ID).attr('disabled');

    if ($('#'+txtSearch_ID).val().trim().length == 0) {
        isOK = false;
        $('#divAgencySearch').addClass("reqBorder").removeAttr('disabled');
    }

    return isOK;
}

function search() {
    $('#'+btnSearch_ID).click();
}
function backToSearch() {
    $('#'+panelAgencyDetails_ID).fadeOut(200, "swing", function () {
        $('#'+panelSearch_ID).show("slide", { direction: "left" }, 500);
    });

    $('#'+hfPanelActive_ID).val("search");
}
function showAgencyDetails(refPartner, refAgency, agencyID, agencyName, managerLastName, managerFirstName, agencyAddress) {
    $('#'+panelSearchAgencyToAllocate_ID).hide();
    $('#'+panelSearch_ID).hide("slide", { direction: "left" }, 500, function () {
        $('#'+hfRefPartner_selected_ID).val(refPartner);
        $('#'+hfRefAgency_selected_ID).val(refAgency);
        $('#'+hfAgencyID_selected_ID).val(agencyID);
        $('#'+hfAgencyName_selected_ID).val(agencyName);
        $('#'+hfManagerLastName_selected_ID).val(managerLastName)
        $('#'+hfManagerFirstName_selected_ID).val(managerFirstName);
        $('#'+hfAgencyAddress_selected_ID).val(agencyAddress);
        $('#'+btnRefreshAgencyDetails_ID).click();
        $('#'+panelAgencyDetails_ID).fadeIn();
    });
    $('#'+hfPanelActive_ID).val("details");
}

function showMpadDetail(sSerial, isLocked, sMpadRelease, sAbmSoftRelease, sInitStatusText, sCode, sCodeCreationDate, sLastCnxDate, sRemoteAccessReq) {
    var sStatus = sInitStatusText;

    $('#'+hfHardwareType_selected_ID).val("MPD");
    $('#'+hfHardwareAllocateDirection_ID).val("del");
    $('#'+hfHardwareSerial_selected_ID).val(sSerial);
    $('#sMpadSerial').html(sSerial);
    $('#sMpadStatus').html(sStatus);
    $('#sMpadRelease').html("v" + sMpadRelease.trim());
    $('#sAbmSoftRelease').html("v" + sAbmSoftRelease.trim());

    if (sCode != null && sCode.trim().length > 0) {
        $('#divMpadCode').show();
        $('#sMpadCode').html(sCode.trim());
        if (sCodeCreationDate.trim().length > 0)
            $('#sMpadCodeDate').html("(généré le <span class=\"font-orange\" style=\"font-weight:bold\">" + sCodeCreationDate.trim().substr(0, 10) + "</span>)");
    }
    else
        $('#divMpadCode').hide();

    if (sLastCnxDate != null)
        $('#sMpadLastCnxDate').html(sLastCnxDate.trim());

    if (sRemoteAccessReq != null)
        $('#sMpadRemoteAccessRequested').html(sRemoteAccessReq.trim());

    $('#divMpadReinitCode').hide();
    $('#divMpadDetails').show();

    $('#'+hfMpadReinitCode_ID).val('');
    $('#'+panelMpadReinitCodeResult_ID).hide();
    $('#'+panelMpadDetails_ID).show();
    $('#divMpadDetail-popup').dialog('open');
}
function showPosDetail(sSerial, isLocked, sSoftRelease, sLastCnxDate, sInitStatusText, sPosType) {
    var sStatus = sInitStatusText;

    $('#'+hfHardwareType_selected_ID).val("POS");
    $('#'+hfHardwareAllocateDirection_ID).val("del");
    $('#'+hfHardwareSerial_selected_ID).val(sSerial);
    $('#sPosType').html(sPosType);
    $('#sPosSerial').html(sSerial);
    $('#sPosStatus').html(sStatus);
    if (sSoftRelease.trim().length > 0)
        $('#sPosRelease').html(sSoftRelease.trim());
    else
        $('#sPosRelease').removeClass("font-orange").html('<span style="font-style:italic; color:#888; font-weight:normal;">INCONNU</span>');
    if (sLastCnxDate != null && sLastCnxDate.trim().length > 0)
        $('#sPosLastCnxDate').html(sLastCnxDate.trim());
    else
        $('#sPosLastCnxDate').removeClass("font-orange").html('<span style="font-style:italic; color:#888; font-weight:normal;">INCONNU</span>');

    if (sPosType != null && sPosType.trim().length > 0)
        $('#imgPosDetails').attr("src", "Styles/Img/" + sPosType + ".png");

    $('#divPosDetail-popup').dialog('open');
}
function showTabletDetail(sSerial, sFreeLabel) {
    $('#' + hfHardwareType_selected_ID).val("TAB");
    $('#' + hfHardwareAllocateDirection_ID).val("del");
    $('#' + hfHardwareSerial_selected_ID).val(sSerial);
    //$('#sTabType').html(sTabType);
    $('#sTabSerial').html(sSerial);
    //$('#sTabStatus').html(sStatus);
    //if (sSoftRelease.trim().length > 0)
        //$('#sTabRelease').html(sSoftRelease.trim());
    //else
        //$('#sTabRelease').removeClass("font-orange").html('<span style="font-style:italic; color:#888; font-weight:normal;">INCONNU</span>');
    //if (sLastCnxDate != null && sLastCnxDate.trim().length > 0)
        //$('#sTabLastCnxDate').html(sLastCnxDate.trim());
    //else
        //$('#sTabLastCnxDate').removeClass("font-orange").html('<span style="font-style:italic; color:#888; font-weight:normal;">INCONNU</span>');

    //if (sTabType != null && sTabType.trim().length > 0)
        //$('#imgTabDetails').attr("src", "Styles/Img/" + sTabType + ".png");

    $('#divTabletDetail-popup').dialog('open');
}
function showTabletSupportDetail(sSerial, sFreeLabel, sModel, sModelImage) {
    $('#' + hfHardwareType_selected_ID).val("PDB");
    $('#' + hfHardwareAllocateDirection_ID).val("del");
    $('#' + hfHardwareSerial_selected_ID).val(sSerial);
    $('#sTabSupportSerial').html(sSerial);
    if (sModel != null && sModel.trim().length > 0)
        $('#sTabSupportType').html(sModel);
    if (sModelImage != null && sModelImage.trim().length > 0)
        $('#imgTabSupportDetails').attr("src", "Styles/Img/" + sModelImage + ".png");

    $('#divTabletSupportDetail-popup').dialog('open');
}
function showScannerDetail(sSerial, sFreeLabel, sModel, sModelImage) {
    $('#' + hfHardwareType_selected_ID).val("SCN");
    $('#' + hfHardwareAllocateDirection_ID).val("del");
    $('#' + hfHardwareSerial_selected_ID).val(sSerial);

    $('#sScnType').html(sModel);
    if (sModel != null && sModel.trim().length > 0) {
        $('#sScnSerial').html(sSerial);
        if (sModelImage != null && sModelImage.trim().length > 0)
        $('#imgScnDetails').attr("src", "Styles/Img/" + sModelImage + ".png");
    }
    $('#divScannerDetail-popup').dialog('open');
}
function showPinpadDetail(sSerial, sFreeLabel) {
    $('#' + hfHardwareType_selected_ID).val("PPD");
    $('#' + hfHardwareAllocateDirection_ID).val("del");
    $('#' + hfHardwareSerial_selected_ID).val(sSerial);
    $('#sPpdSerial').html(sSerial);
    $('#divPinpadDetail-popup').dialog('open');
}

function showSearchAgencyToAllocate(sHardwareType) {

    if (sHardwareType == null)
        sHardwareType = "MPD";

    $('#' + hfHardwareType_selected_ID).val(sHardwareType);

    switch (sHardwareType) {
        case "MPD":
            $('#divMpadDetail-popup').dialog('close');
            break;
        case "POS":
            $('#divPosDetail-popup').dialog('close');
            break;
        case "TAB":
            $('#divTabletDetail-popup').dialog('close');
            break;
        case "SCN":
            $('#divScannerDetail-popup').dialog('close');
            break;
        case "PPD":
            $('#divPinpadDetail-popup').dialog('close');
            break;
        case "PDB":
            $('#divTabletSupportDetail-popup').dialog('close');
            break;
    }

    $('#' + panelSearch_ID).hide();
    $('#' + btnSearchAgencyToAllocate_ID).click();
    $('#' + panelAgencyDetails_ID).fadeOut();
    setTimeout(function () {
        $('#'+panelSearchAgencyToAllocate_ID).show("slide", { direction: "up" }, 500)
    }, 500);

    $('#' + hfPanelActive_ID).val("allocate");

    return false;
}
function hideSearchAgencyToAllocate() {
    $('#' + panelSearchAgencyToAllocate_ID).hide("slide", { direction: "up" }, 500, function () {
        $('#'+panelAgencyDetails_ID).fadeIn(function () {
            $('#'+panelMpadReinitCodeResult_ID).hide();
            $('#'+panelMpadDetails_ID).show();

            var sHardwareType = $('#' + hfHardwareType_selected_ID).val().trim();

            switch (sHardwareType) {
                case "POS":
                    $('#divPosDetail-popup').dialog('open');
                    break;
                case "TAB":
                    $('#divTabletDetail-popup').dialog('open');
                    break;
                case "SCN":
                    $('#divScannerDetail-popup').dialog('open');
                    break;
                case "PPD":
                    $('#divPinpadDetail-popup').dialog('open');
                    break;
                case "PDB":
                    $('#divTabletSupportDetail-popup').dialog('open');
                    break;
                default:
                case "MPD":
                    $('#divMpadDetail-popup').dialog('open');
                    break;
            }
        });
    });

    $('#'+hfPanelActive_ID).val("details");

    return false;
}

function showConfirmHardwareReallocate(hardwareSerial, agencyID_source, agencyName_source, agencyName_destination, agencyID_destination, refPartner_destination, direction, hardwareModelName) {
    if (direction == null || direction.trim().length == 0)
        $('#'+hfHardwareAllocateDirection_ID).val("del");
    else
        $('#'+hfHardwareAllocateDirection_ID).val(direction);

    var sTitle = "Affectation borne"
    var sHardwareType = $('#'+hfHardwareType_selected_ID).val().trim();
    var sHardwareModelName = (hardwareModelName != null && hardwareModelName.trim().length > 0) ? hardwareModelName.toLowerCase().trim() : "ingenico";

    if (sHardwareType == null || sHardwareType.trim().length == 0)
        sHardwareType = "MPD";

    switch (sHardwareType) {
        case "POS":
            $('#imgHardwareReallocate').attr('src', 'Styles/Img/' + sHardwareModelName + '.png');
            sTitle = "Affectation terminal";
            break;
        case "TAB":
            $('#imgHardwareReallocate').attr('src', 'Styles/Img/Tablet.png');
            sTitle = "Affectation tablette";
            break;
        case "SCN":
            $('#imgHardwareReallocate').attr('src', 'Styles/Img/Scanner.png');
            sTitle = "Affectation scanner";
            break;
        case "PPD":
            $('#imgHardwareReallocate').attr('src', 'Styles/Img/pinpad_ipp310.png');
            sTitle = "Affectation pinpad";
            break;
        default:
        case "MPD":
            $('#imgHardwareReallocate').attr('src', 'Styles/Img/Mpad.png');
            sTitle = "Affectation borne";
            break;
        case "PDB":
            $('#imgHardwareReallocate').attr('src', 'Styles/Img/TabletSupportLight.png');
            sTitle = "Affectation support tablette";
            break;
    }

    $('#'+panelHardwareReallocateResult_ID).hide();
    $('#'+panelHardwareReallocateConfirm_ID).hide();
    $('#'+panelHardwareReallocateComment_ID).hide();
    $('#'+panelHardwareReallocateLoading_ID).hide();
    $('#'+panelHardwareReallocate_ID).show();

    $('#'+hfHardwareSerial_selected_ID).val(hardwareSerial);
    $('#'+hfAgencyID_destination_ID).val(agencyID_destination);
    $('#'+hfRefPartner_destination_ID).val(refPartner_destination);
    $('#sHardwareAgencySource').html("<b>" + agencyID_source + "</b><br/>" + agencyName_source);
    $('#sHardwareAgencyDestination').html("<b>" + agencyID_destination + "</b><br/>" + agencyName_destination);
    $('#sHardwareReallocateSerial').html(hardwareSerial);

    $('#divHardwareReallocate-popup').dialog('open');
    $('#divHardwareReallocate-popup').dialog("option", "title", sTitle);
}