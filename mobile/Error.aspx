﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Error.aspx.cs" Inherits="mobile_erreur" EnableSessionState="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>

    <%--<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1, maximum-scale=1, target-densityDpi=device-dpi, user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <meta name="HandheldFriendly" content="True" />
    <meta name="apple-touch-fullscreen" content="yes" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">--%>
    <meta name="HandheldFriendly" content="True" />
    <meta name="MobileOptimized" content="320" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta http-equiv="cleartype" content="on" />
    <meta name="viewport" content="width=320,maximum-scale=1.0" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />

    <link rel="shortcut icon" href="favicon57.png" />
    <link rel="apple-touch-icon" href="favicon57.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="favicon72.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="favicon114.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="favicon144.png" />

    <link rel="stylesheet" href="../Styles/Site.css" />
    <link rel="stylesheet" href="Styles/SiteMobile.css" />
    <link rel="stylesheet" href="../Styles/UI/jquery-ui-1.9.2.custom.css" />

    <script src="../Scripts/jquery.js"></script>
    <script src="../Scripts/jqueryui.min.js" type="text/javascript"></script>
    <!--<script src="Scripts/jquery.jpanelmenu.min.js" type="text/javascript"></script>-->
    <script src="Scripts/jquery.jpanelmenu.js" type="text/javascript"></script>

    <script src="Scripts/link.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            setCookie("ClientGmtOffset", GetGmtOffset(), 365);
        });
        function setCookie(cname, cvalue, exdays) {
            //alert("set cookie" + cname + "-" + cvalue + "-" + exdays);
            var d = new Date();
            d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
            var expires = "expires=" + d.toGMTString();
            document.cookie = cname + "=" + cvalue + "; " + expires;
        }
        function GetGmtOffset() {
            var rightNow = new Date();
            var jan1 = new Date(rightNow.getFullYear(), 0, 1, 0, 0, 0, 0);
            var temp = jan1.toGMTString();
            var jan2 = new Date(temp.substring(0, temp.lastIndexOf(" ") - 1));
            var std_time_offset = (jan1 - jan2) / (1000 * 60 * 60);

            return std_time_offset;
        }

        var jPM_isOpened = false;
        var Header_isFixed = false;
        $(function () {
            var jPM = $.jPanelMenu({
                menu: '#menu',
                trigger: '.menu-trigger',
                openDuration: 300,
                closeDuration: 200,
                beforeOpen: function () {
                    if (jPM.isOpen()) {
                        jPM.close();
                        jPM_isOpened = true;
                    }
                    else {
                        $('#header_disabled').show();
                        jPM_isOpened = true;
                    }
                },
                afterClose: function () {
                    setTimeout(function () { $('#header_disabled').hide(); }, 500);
                    jPM_isOpened = false;
                }
            });
            jPM.on();

            // Close on orientation change
            $(window).resize(function () {
                jPM.close();
                jPM_isOpened = false;
            });

            $(".page-mobile").css("min-height", $(window).height().toString() + "px");
        });

        function disconnectMe() {
            if (confirm('Voulez-vous vous déconnecter ?')) {
                $('#<%=btnDisconnectMe.ClientID%>').click();
        }
    }

    function ShowMenuLoading() {
        var width = $('#jPanelMenu-menu').width();
        var height = $('#jPanelMenu-menu').height();
        $('#ar-loading-menu').css('width', width);
        $('#ar-loading-menu').css('height', height);

        $('#ar-loading-menu').show();
        $('#ar-loading-menu').position({ my: 'center', at: 'center', of: $('#jPanelMenu-menu') });
    }
    function HideMenuLoading() {
        $('#ar-loading-menu').hide();
    }

    function ChangePage(url) {
        ShowMenuLoading();
        document.location.href = url;
    }

    function AlertMessage(message) {
        //console.log(message);
        $('.alert-popup').empty().append(message);
        $('.alert-bg').fadeIn();
        if (window.navigator.msPointerEnabled) {
            $('.alert-bg').unbind('MSPointerDown').bind('MSPointerDown', function () { $(this).fadeOut(); });
        }
        else {
            $('.alert-bg').unbind('touchStart').bind('touchstart', function () { $(this).fadeOut(); });
        }
        $(window).bind('resize', function () { if ($('.alert-bg').is(':visible')) AlertMessage(message); });

        var viewportWidth = $(window).width();
        var viewportHeight = $(window).height();

        //console.log(viewportWidth + ":" + viewportHeight);
        $('.alert-bg').css('width', viewportWidth);
        $('.alert-bg').css('height', viewportHeight);

        $('.alert-popup').position({ my: 'center', at: 'center', of: $(window) });
    }

    </script>

    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-42035525-6', 'compte-nickel.fr');
        ga('send', 'pageview');
    </script>

    <style type="text/css">
        
        #ar-loading-menu {
            background:url('../Styles/img/loading.gif') no-repeat center;
            background-size: 35px 27px;
        }

    </style>
</head>
<body>
    <form id="form1" runat="server" novalidate>

        <div class="alert-bg">
            <div class="alert-popup"></div>
        </div>

        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true" EnableScriptLocalization="true"
            EnableScriptGlobalization="true" EnablePageMethods="true" ScriptMode="Release">
        </asp:ScriptManager>

        <div id="ar-loading-menu" style="display:none;position:absolute;background-color:rgba(249, 249, 249, 0.5);z-index:100"></div>

        <div class="page-mobile">
            <div id="header_disabled" style="display: none; height: 60px; position: absolute; top: 0; left: 0; width: 100%; z-index: 202;">
                &nbsp;
            </div>
            <asp:Panel ID="panelHeader" runat="server" CssClass="header" style="z-index:201">
                <div class="table" style="width: 100%; height: 30px">
                    <div class="table-row">
                        <div class="table-cell" style="width: 25%; padding-left: 20px; vertical-align: middle">
                            <div style="margin-top: 10px">
                                <a href="#menu" class="menu-trigger" onclick="HideMenuLoading();">
                                    <img src="Styles/Img/menu-bars.png" alt="" height="30" />
                                </a>
                            </div>
                        </div>
                        <div class="table-cell" style="text-align: center; vertical-align: middle">
                            <div style="margin-top: 10px">
                                <img src="Styles/Img/compte-nickel-header-logo.png" alt="" height="40" />
                            </div>
                        </div>
                        <div class="table-cell" style="text-align: right; width: 25%; padding-right: 20px; vertical-align: middle">
                            <div style="margin-top: 10px">
                                <a style="padding: 0;" onmousedown="disconnectMe();">
                                    <img src="Styles/Img/btn_logoff.png" alt="" height="30" />
                                </a>
                                <asp:Button ID="btnDisconnectMe" runat="server" Style="display: none" OnClick="clickDisconnectMe" />
                            </div>
                        </div>
                    </div>

                </div>
            </asp:Panel>
            <asp:Panel ID="panelHeaderNotAuth" runat="server" Visible="false" CssClass="header" style="z-index:201">
                <div class="table" style="width: 100%; height: 30px">
                    <div class="table-row">
                        <div class="table-cell" style="width: 25%; padding-left: 20px; vertical-align: middle">
                            <div style="margin-top: 10px">
                                <a href="identifiez-vous.aspx">
                                    <img src="Styles/Img/arrow-back.png" alt="" height="30" />
                                </a>
                            </div>
                        </div>
                        <div class="table-cell" style="text-align: center; vertical-align: middle">
                            <div style="margin-top: 10px">
                                <img src="Styles/Img/compte-nickel-header-logo.png" alt="" height="40" />
                            </div>
                        </div>
                        <div class="table-cell" style="text-align: right; width: 25%; padding-right: 20px; vertical-align: middle">
                            
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <div class="content" style="width: 100%; position: relative; padding: 5px 0 10px 0;">
                <div class="main">
                    <div class="uppercase font-bold font-orange" style="text-align:center;font-size:1.2em;margin-top:15px;padding-bottom:15px;border-bottom:1px solid #b7b7b7;">
                        <asp:Label ID="lblErrorTitle" runat="server"></asp:Label>
                    </div>
                    <div style="margin-top:15px">
                        <asp:Label ID="lblErrorMessage" runat="server"></asp:Label>
                    </div>
                </div>
            </div>

        </div>

        <asp:HiddenField ID="hfShowTimeout" runat="server" />
        <asp:HiddenField ID="hfSessionTimeout" runat="server" />
        <asp:HiddenField ID="hfSessionStarted" runat="server" Value="false" />
        <asp:HiddenField ID="hfRegistrationFilePath" runat="server" />
        <asp:HiddenField ID="hfBtnDisconnectVisible" runat="server" Value="false" />
    </form>
    <div id="jPanelMenu-menu" style="overflow: hidden;overflow-y:auto; width: 250px; z-index: -1; display: none;">
        <div style="width: 97%; margin: 10px auto 0 auto;">
            <div class="font-AracneRegular" style="text-transform: uppercase; font-size: 1.5em;">
                <div class="table" style="width: 100%">
                    <div class="table-row">
                        <div class="table-cell" style="width: 120px; padding-right: 10px; padding-left: 5px">
                            Tout <span class="font-VAGRoundedBold">savoir</span>
                        </div>
                        <div class="table-cell">
                            <div style="width: 100%; border-bottom: 2px solid #000;"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="linkLeftMenu" style="text-transform: uppercase; text-align: left; width:100%">
                <div style="margin-top: 10px;"><a onclick="ShowMenuLoading();" href="vos-operations.aspx">vos dernières opérations</a></div>
                <div style="margin-top:5px; border-top:1px solid #b7b7b7"><a onclick="ShowMenuLoading();" href="votre-calendrier.aspx">votre calendrier d'opérations</a></div>
                <div style="margin-top:5px; border-top:1px solid #b7b7b7"><a onclick="ShowMenuLoading();" href="vos-alertes.aspx">vos alertes personnalisables</a></div>
            </div>

            <div class="font-AracneRegular" style="text-transform: uppercase; margin-top: 20px; font-size: 1.5em">
                <div class="table" style="width: 100%">
                    <div class="table-row">
                        <div class="table-cell" style="width: 100px; padding-right: 10px; padding-left: 5px">
                            Tout <span class="font-VAGRoundedBold">faire</span>
                        </div>
                        <div class="table-cell">
                            <div style="width: 100%; border-bottom: 2px solid #000;"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="linkLeftMenu" style="text-transform: uppercase; text-align: left">
                <div style="margin-top: 10px;"><a onclick="ShowMenuLoading();" href="faire-un-virement.aspx">faire un virement</a></div>
                <div style="margin-top: 5px; border-top:1px solid #b7b7b7"><a onclick="ShowMenuLoading();" href="alimenter-votre-compte.aspx">alimenter votre compte</a></div>
                <div style="margin-top: 5px; border-top:1px solid #b7b7b7"><a onclick="ShowMenuLoading();" href="vos-limites.aspx">gérer vos limites</a></div>
            </div>

            <div class="font-AracneRegular" style="text-transform: uppercase; margin-top: 20px; font-size: 1.5em;">
                <div class="table" style="width: 100%">
                    <div class="table-row">
                        <div class="table-cell" style="width: 130px; padding-right: 10px; padding-left: 5px">
                            Tout <span class="font-VAGRoundedBold">obtenir</span>
                        </div>
                        <div class="table-cell">
                            <div style="width: 100%; border-bottom: 2px solid #000;"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="linkLeftMenu" style="text-transform: uppercase; text-align: left">
                <div style="margin-top: 10px;"><a onclick="ShowMenuLoading();" href="votre-rib.aspx">votre RIB</a></div>
                <div style="margin-top:5px; border-top:1px solid #b7b7b7"><a href="http://www.compte-nickel.fr/mobile/ou-le-prendre" target="_blank">le buraliste le plus proche</a></div>
            </div>
        </div>
        <div class="table linkBottomMenu" style="margin-top:20px;width:100%; height:40px; border-collapse:collapse;"><!--position:absolute;bottom:0;left:-2px-->
	        <div class="table-row">
                <div class="table-cell" style="text-align:left;vertical-align:middle; background-color:#b6b6b1; line-height:35px;border-left:0;border-bottom:5px solid #fff">
                    <a href="https://faq.compte-nickel.fr/home" target="_blank">
                        <img src="Styles/Img/questions.png" style="height:20px;position:relative;top:5px;margin-right:2px" />
                        Des questions ?
                    </a>
                </div>
            </div>
            <div class="table-row">
                <div class="table-cell" style="text-align:left;vertical-align:middle; background-color:#b6b6b1; line-height:35px;border-left:0;border-bottom:5px solid #fff">
                    <a onclick="ShowMenuLoading();" href="votre-compte.aspx">
                        <img src="Styles/Img/client_blanc.png" style="height:20px;position:relative;top:5px;margin-right:2px" />
                        Votre compte
                    </a>
                </div>
            </div>
            <div class="table-row">
                <div class="table-cell" style="text-align:left;vertical-align:middle; background-color:#b6b6b1; line-height:35px">
                    <a onclick="ShowMenuLoading();" href="sos.aspx">
                        <img src="Styles/Img/bouee.png" style="height:20px;position:relative;top:5px;margin-right:2px" />
                        SOS Compte-Nickel
                    </a>
                </div>
            </div>
        </div>
    </div>
</body>
</html>