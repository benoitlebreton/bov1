﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class mobile_erreur : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            authentication auth = null;
            try { auth = authentication.GetCurrent(false); }
            catch (Exception exAuth) { }
            if (auth == null)
            {
                panelHeader.Visible = false;
                panelHeaderNotAuth.Visible = true;
            }

            Exception ex = Server.GetLastError();
            if (ex != null)
            {
                if (ex.GetBaseException() != null)
                    ex = ex.GetBaseException();

                try
                {
                    HttpException httpEx = (HttpException)ex;

                    if (httpEx != null && httpEx.GetHttpCode() == 404)
                    {
                        lblErrorTitle.Text = "404";

                        string sPath = HttpContext.Current.Request.Url.AbsolutePath;
                        string[] ar = sPath.Split('/');
                        string sPageName = ar[ar.Length - 1];

                        lblErrorMessage.Text = "La page " + sPageName + " n'existe pas.";
                    }
                    else throw new Exception();
                }
                catch (Exception ex2)
                {
                    lblErrorTitle.Text = "Oups !";
                    lblErrorMessage.Text = "Une erreur s'est produite.<br/><br/>Notre équipe technique a été informée du problème et se charge de le régler au plus vite.";
                }
            }
        }
    }

    protected void clickDisconnectMe(object sender, EventArgs e)
    {
        Session.Clear();
        Response.Redirect("identifiez-vous.aspx");
    }
}