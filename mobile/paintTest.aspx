﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="paintTest.aspx.cs" Inherits="mobile_paintTest" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link rel="stylesheet" href="../Styles/Site.css" />
    <link rel="stylesheet" href="Styles/SiteMobile.css" />
    <link rel="stylesheet" href="../Styles/UI/jquery-ui-1.9.2.custom.css" />

    <script src="../Scripts/jquery-1.8.3.js"></script>
    <script src="../Scripts/jquery-ui-1.9.2.custom.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.watermark.min.js"></script>
    <!--<script src="Scripts/jquery.jpanelmenu.min.js" type="text/javascript"></script>-->
    <script src="Scripts/jquery.jpanelmenu.js" type="text/javascript"></script>

    <script src="Scripts/link.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            //paint();
        });

        // get the canvas element and its context
        var canvas;
        var context;
        window.addEventListener('load', function () {
            canvas = document.getElementById('canvas');
            context = canvas.getContext('2d');

            // create a drawer which tracks touch movements
            var drawer = {
                isDrawing: false,
                touchstart: function (coors) {
                    context.beginPath();
                    context.moveTo(coors.x, coors.y);
                    this.isDrawing = true;
                },
                touchmove: function (coors) {
                    if (this.isDrawing) {
                        context.lineTo(coors.x, coors.y);
                        context.lineWidth = 5;
                        context.stroke();
                    }
                },
                touchend: function (coors) {
                    if (this.isDrawing) {
                        this.touchmove(coors);
                        this.isDrawing = false;
                    }
                }
            };
            // create a function to pass touch events and coordinates to drawer
            function draw(event) { 
                var type = null;
                // map mouse events to touch events
                switch(event.type){
                    case "mousedown":
                        event.touches = [];
                        event.touches[0] = { 
                            pageX: event.pageX,
                            pageY: event.pageY
                        };
                        type = "touchstart";                  
                        break;
                    case "mousemove":                
                        event.touches = [];
                        event.touches[0] = { 
                            pageX: event.pageX,
                            pageY: event.pageY
                        };
                        type = "touchmove";                
                        break;
                    case "mouseup":                
                        event.touches = [];
                        event.touches[0] = { 
                            pageX: event.pageX,
                            pageY: event.pageY
                        };
                        type = "touchend";
                        break;
                }        

                // touchend clear the touches[0], so we need to use changedTouches[0]
                var coors;
                if(event.type === "touchend") {
                    coors = {
                        x: event.changedTouches[0].pageX,
                        y: event.changedTouches[0].pageY
                    };
                }
                else {
                    // get the touch coordinates
                    coors = {
                        x: event.touches[0].pageX,
                        y: event.touches[0].pageY
                    };
                }
                type = type || event.type
                // pass the coordinates to the appropriate handler
                drawer[type](coors);
            }

            $("#reset").click(function () {
                // Clear canvas :
                context.clearRect(0, 0, canvas.width, canvas.height);
            });

            // Bouton Save :
            $("#save").click(function () {
                var canvas_tmp = document.getElementById("canvas");	// Ca merde en pernant le selecteur jQuery
                window.location = canvas_tmp.toDataURL("image/png");
            });

            // detect touch capabilities
            var touchAvailable = ('createTouch' in document) || ('ontouchstart' in window);

            // attach the touchstart, touchmove, touchend event listeners.
            if(touchAvailable){
                canvas.addEventListener('touchstart', draw, false);
                canvas.addEventListener('touchmove', draw, false);
                canvas.addEventListener('touchend', draw, false);        
            }    
                // attach the mousedown, mousemove, mouseup event listeners.
            else {
                canvas.addEventListener('mousedown', draw, false);
                canvas.addEventListener('mousemove', draw, false);
                canvas.addEventListener('mouseup', draw, false);
            }

            // prevent elastic scrolling
            document.body.addEventListener('touchmove', function (event) {
                event.preventDefault();
            }, false); // end body.onTouchMove

        }, false); // end window.onLoad

        function paint (){
            // Variables :
            var color = "#000";
            var painting = false;
            var started = false;
            var width_brush = 20;
            var canvas = $("#canvas");
            var cursorX, cursorY;
            var restoreCanvasArray = [];
            var restoreCanvasIndex = 0;

            var context = canvas[0].getContext('2d');

            // Trait arrondi :
            context.lineJoin = 'round';
            context.lineCap = 'round';

            // Click souris enfoncÃ© sur le canvas, je dessine :
            canvas.mouse(function (e) {
                painting = true;

                // CoordonnÃ©es de la souris :
                cursorX = (e.pageX - this.offsetLeft);
                cursorY = (e.pageY - this.offsetTop);
            });

            // Relachement du Click sur tout le document, j'arrÃªte de dessiner :
            $(this).mouseup(function () {
                painting = false;
                started = false;
            });

            // Mouvement de la souris sur le canvas :
            canvas.mousemove(function (e) {
                // Si je suis en train de dessiner (click souris enfoncÃ©) :
                if (painting) {
                    // Set CoordonnÃ©es de la souris :
                    cursorX = (e.pageX - this.offsetLeft) - 10; // 10 = dÃ©calage du curseur
                    cursorY = (e.pageY - this.offsetTop) - 10;

                    // Dessine une ligne :
                    drawLine();
                }
            });

            // Fonction qui dessine une ligne :
            function drawLine() {
                // Si c'est le dÃ©but, j'initialise
                if (!started) {
                    // Je place mon curseur pour la premiÃ¨re fois :
                    context.beginPath();
                    context.moveTo(cursorX, cursorY);
                    started = true;
                }
                    // Sinon je dessine
                else {
                    context.lineTo(cursorX, cursorY);
                    context.strokeStyle = "#000000";
                    context.lineWidth = 20;
                    context.stroke();
                }
            }

            // Clear du Canvas :
            function clear_canvas() {
                context.clearRect(0, 0, canvas.width(), canvas.height());
            }

            // Bouton Reset :
            $("#reset").click(function () {
                // Clear canvas :
                clear_canvas();

                // Valeurs par dÃ©faut :
                width_brush = 20;
            });

            // Bouton Save :
            $("#save").click(function () {
                var canvas_tmp = document.getElementById("canvas");	// Ca merde en pernant le selecteur jQuery
                window.location = canvas_tmp.toDataURL("image/png");
            });
        }
    </script>
</head>
<body>

    <div style="padding:10px;">
        <canvas id="canvas" width="800" height="400" style="border:2px solid #000"></canvas>
        <form id="form1">
		    <input type="reset" id="reset" value="Réinitialiser" />
		    <input type="button" id="save" value="Sauvergarder mon image" />
	    </form>
    </div>
	
</body>
</html>
