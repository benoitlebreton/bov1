﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mobile/SiteMobile.master" AutoEventWireup="true" CodeFile="AgencySearch.aspx.cs" Inherits="mobile_AgencySearch" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="./Scripts/agency_hw.js"></script>
    <script type="text/javascript">
        $.extend($.ui.autocomplete.prototype, {
            _renderItem: function (ul, item) {
                var term = this.element.val(),
                    html = item.label.replace(term, "<b>$&</b>");
                return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append($("<a></a>").html(html))
                    .appendTo(ul);
            }
        });
        $(document).ready(function () {

            initVariables();

            $('#<%=txtSearch.ClientID%>').watermark("<%=Resources.res.Search%>").click(function () {
                $('#divAgencySearch').removeClass('reqBorder');
            });

            initDialogs();
            initPanel();
        });

        function initVariables() {
            hfHardwareAllocateDirection_ID = '<%=hfHardwareAllocateDirection.ClientID%>';
            panelHardwareReallocate_ID = '<%=panelHardwareReallocate.ClientID%>';
            panelHardwareReallocateComment_ID = '<%=panelHardwareReallocateComment.ClientID%>';
            panelHardwareReallocateConfirm_ID = '<%=panelHardwareReallocateConfirm.ClientID%>';
            hfHardwareType_selected_ID = '<%=hfHardwareType_selected.ClientID%>';
            btnConfirmHardwareAllocate_ID = '<%=btnConfirmHardwareAllocate.ClientID%>';
            btnSearchAgencyToDeallocate_ID = '<%=btnSearchAgencyToDeallocate.ClientID%>';
            btnRefreshAgencyDetails_AgencyToDeallocate_ID = '<%=btnRefreshAgencyDetails_AgencyToDeallocate.ClientID%>';
            hfMpadReinitCode_ID = '<%=hfMpadReinitCode.ClientID%>';
            hfReload_ID = '<%=hfReload.ClientID%>';
            btnRefreshAgencyDetails_ID = '<%=btnRefreshAgencyDetails.ClientID%>';
            panelSearch_ID = '<%=panelSearch.ClientID%>';
            btnSearch_ID = '<%=btnSearch.ClientID%>';
            txtSearch_ID = '<%=txtSearch.ClientID%>';
            panelAgencyDetails_ID = '<%=panelAgencyDetails.ClientID%>';
            panelSearchAgencyToAllocate_ID = '<%=panelSearchAgencyToAllocate.ClientID%>';
            panelSearchAgencyToDeallocate_ID = '<%=panelSearchAgencyToDeallocate.ClientID%>';
            panelAgencyDetails_AgencyToDeallocate_ID = '<%=panelAgencyDetails_AgencyToDeallocate.ClientID%>';
            hfPanelActive_ID = '<%=hfPanelActive.ClientID%>';
            btnSearchAgencyToAllocate_ID = '<%=btnSearchAgencyToAllocate.ClientID%>';
            hfRefPartner_AgencyToDeallocate_selected_ID = '<%=hfRefPartner_AgencyToDeallocate_selected.ClientID%>';
            hfRefAgency_AgencyToDeallocate_selected_ID = '<%=hfRefAgency_AgencyToDeallocate_selected.ClientID%>';
            hfAgencyID_AgencyToDeallocate_selected_ID = '<%=hfAgencyID_AgencyToDeallocate_selected.ClientID%>';
            hfAgencyName_AgencyToDeallocate_selected_ID = '<%=hfAgencyName_AgencyToDeallocate_selected.ClientID%>';
            hfManagerLastName_AgencyToDeallocate_selected_ID = '<%=hfManagerLastName_AgencyToDeallocate_selected.ClientID%>';
            hfManagerFirstName_AgencyToDeallocate_selected_ID = '<%=hfManagerFirstName_AgencyToDeallocate_selected.ClientID%>';
            hfHardwareSerialList_AgencyToDeallocate_ID = '<%=hfHardwareSerialList_AgencyToDeallocate.ClientID%>';
            txtHardwareToAddSearch_ID = '<%=txtHardwareToAddSearch.ClientID%>';
            btnHardwareToAddSearch_ID = '<%=btnHardwareToAddSearch.ClientID%>';
            btnMpadSearch_ID = '<%=btnMpadSearch.ClientID%>';
            txtMpadSearch_ID = '<%=txtMpadSearch.ClientID%>';
            txtTabletSearch_ID = '<%=txtTabletSearch.ClientID%>';
            txtTabletSupportSearch_ID = '<%=txtTabletSupportSearch.ClientID%>';
            txtScannerSearch_ID = '<%=txtScannerSearch.ClientID%>';
            txtPinpadSearch_ID = '<%=txtPinpadSearch.ClientID%>';
            btnTabletSearch_ID = '<%=btnTabletSearch.ClientID%>';
            btnTabletSupportSearch_ID = '<%=btnTabletSupportSearch.ClientID%>';
            btnScannerSearch_ID = '<%=btnScannerSearch.ClientID%>';
            btnPinpadSearch_ID = '<%=btnPinpadSearch.ClientID%>';
            panelMpadSearchAdd_ID = '<%=panelMpadSearchAdd.ClientID%>';
            panelPosSearchAdd_ID = '<%=panelPosSearchAdd.ClientID%>';
            panelTabletSearchAdd_ID = '<%=panelTabletSearchAdd.ClientID%>';
            panelTabletSupportSearchAdd_ID = '<%=panelTabletSupportSearchAdd.ClientID%>';
            panelScannerSearchAdd_ID = '<%=panelScannerSearchAdd.ClientID%>'
            panelPinpadSearchAdd_ID = '<%=panelPinpadSearchAdd.ClientID%>';
            hfPosSerialList_ID = '<%=hfPosSerialList.ClientID%>';
            hfPosSerialList_temp_ID = '<%=hfPosSerialList_temp.ClientID%>';
            txtPosSearch_ID = '<%=txtPosSearch.ClientID%>';
            btnPosSearch_ID = '<%=btnPosSearch.ClientID%>';
            hfMpadSerialList_temp_ID = '<%=hfMpadSerialList_temp.ClientID%>';
            hfMpadSerialList_ID = '<%=hfMpadSerialList.ClientID%>';
            hfTabletSerialList_temp_ID = '<%=hfTabletSerialList_temp.ClientID%>';
            hfTabletSerialList_ID = '<%=hfTabletSerialList.ClientID%>';
            hfTabletSupportSerialList_temp_ID = '<%=hfTabletSupportSerialList_temp.ClientID%>';
            hfTabletSupportSerialList_ID = '<%=hfTabletSupportSerialList.ClientID%>';
            hfScannerSerialList_ID = '<%=hfScannerSerialList.ClientID%>';
            hfPinpadSerialList_ID = '<%=hfPinpadSerialList.ClientID%>';
            hfScannerSerialList_temp_ID = '<%=hfScannerSerialList_temp.ClientID%>';
            hfPinpadSerialList_temp_ID = '<%=hfPinpadSerialList_temp.ClientID%>';
            hfRefPartner_selected_ID = '<%=hfRefPartner_selected.ClientID%>';
            hfRefAgency_selected_ID = '<%=hfRefAgency_selected.ClientID%>';
            hfAgencyID_selected_ID = '<%=hfAgencyID_selected.ClientID%>';
            hfAgencyName_selected_ID = '<%=hfAgencyName_selected.ClientID%>';
            hfManagerLastName_selected_ID = '<%=hfManagerLastName_selected.ClientID%>';
            hfManagerFirstName_selected_ID = '<%=hfManagerFirstName_selected.ClientID%>';
            hfAgencyAddress_selected_ID = '<%=hfAgencyAddress_selected.ClientID%>';
            hfHardwareSerial_selected_ID = '<%=hfHardwareSerial_selected.ClientID%>';
            panelMpadDetails_ID = '<%=panelMpadDetails.ClientID%>';
            panelMpadReinitCodeResult_ID = '<%=panelMpadReinitCodeResult.ClientID%>';
            hfAgencyID_destination_ID = '<%=hfAgencyID_destination.ClientID%>';
            hfRefPartner_destination_ID = '<%=hfRefPartner_destination.ClientID%>';
            panelHardwareReallocateResult_ID = '<%=panelHardwareReallocateResult.ClientID%>';
            panelHardwareReallocateLoading_ID = '<%=panelHardwareReallocateLoading.ClientID%>';
        }
    </script>
        <style type="text/css">
        .ui-dialog-title {
            font-family: Arial;
        }

        .ui-dialog-content {
            font-family: Arial;
        }

        .ui-menu {
	        font-family:Arial;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="Server">
    <asp:Panel ID="panelSearch" runat="server" DefaultButton="btnSearch">
        <asp:UpdatePanel ID="upSearchResult" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <h2 style="color: #344b56; margin: 5px 0; text-transform: uppercase; text-align: center">Recherche agence
                </h2>
                <div id="divAgencySearch" class="table" style="width: 100%;">
                    <div class="table-row">
                        <div class="table-cell" style="padding: 0; text-align: left; vertical-align: top;">
                            <asp:TextBox ID="txtSearch" runat="server" Style="margin: 0; padding: 0 2px; border-right: 0; height: 30px; width: 100%" CssClass="textbox" autocomplete="off"></asp:TextBox>
                        </div>
                        <div class="table-cell" style="width: 30px; padding: 0; text-align: right; vertical-align: top">
                            <div style="background-color: #fff; padding: 0; height: 30px;" onclick="search();">
                                <img src="Styles/Img/loupe.png" alt="loupe" style="padding: 0; height: 30px; border: 1px solid #bababa; border-left: 0;" />
                            </div>
                            <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" OnClientClick="return checkAgencySearch();" Style="display: none" />
                        </div>
                    </div>
                </div>
                <asp:UpdateProgress ID="upSearchResultLoading" runat="server" AssociatedUpdatePanelID="upSearchResult">
                    <ProgressTemplate>
                        <div style="text-align: center; position: absolute; left: 45%; top: 50%">
                            <img src="../Styles/Img/loading.gif" alt="loading..." />
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
                <asp:Panel ID="panelSearchResultNb" runat="server" Visible="false" Style="margin-top: 10px;">
                    <asp:Label ID="lblSearchResultNb" runat="server" Style="font-weight: bold"></asp:Label>
                </asp:Panel>
                <asp:Repeater ID="rptSearchResult" runat="server">
                    <HeaderTemplate>
                        <div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <div style="margin-top: 10px; background-color: #fff; padding: 10px 5px; box-shadow: 0px 1px 2px #888;"
                            onclick="showAgencyDetails('<%# Eval("P_Ref") %>','<%# Eval("A_Ref") %>', '<%# Eval("A_ID")%>','<%# Eval("A_Name")%>','<%# Eval("A_ManLastName")%>','<%# Eval("A_ManFirstName")%>', '<%# Eval("A_Add").ToString() + "|" + Eval("A_City").ToString()+ "|" + Eval("A_Zip").ToString() %>')">
                            <h3 style="font-family: Arial; font-size: 1.2em; font-weight: lighter; color: #ff6a00"><%# tools.getHighlightedSearchValue(Eval("A_ID").ToString(), txtSearch.Text) %> -
                                <%# tools.getHighlightedSearchValue(Eval("A_Name").ToString(), txtSearch.Text) %></h3>
                            <div style="font-size: 1em;">
                                <div style='<%# (Eval("A_Name2").ToString().Trim().Length > 0) ? "": "display:none" %>; font-size: 0.8em; font-weight: bold;' class="font-orange">
                                    <%# tools.getHighlightedSearchValue(Eval("A_Name2").ToString(), txtSearch.Text) %>
                                </div>
                                <div>
                                    <%# tools.getHighlightedSearchValue(Eval("A_ManLastName").ToString(), txtSearch.Text) %>
                                    <%# tools.getHighlightedSearchValue(Eval("A_ManFirstName").ToString(), txtSearch.Text) %>
                                </div>
                                <div style="background-color:#ff6a00; height:1px;margin:10px auto;"></div>
                                <div style="font-size:0.9em;">
                                    <span style="font-weight:bold">TPE S/N : </span><%# Agency.getSearchForEmptyValue(tools.getHighlightedSearchValue(Agency.getSearchValueFromPOS(Eval("POS_SerialList").ToString(), txtSearch.Text), txtSearch.Text)) %>
                                    <br />
                                    <span style="font-weight:bold">MPAD S/N : </span><%# Agency.getSearchForEmptyValue(tools.getHighlightedSearchValue(Agency.getSearchValueFromMPAD(Eval("MP_SerialList").ToString(), txtSearch.Text), txtSearch.Text)) %>
                                    <br />
                                    <span style="font-weight:bold">TABLETTE S/N : </span><%# Agency.getSearchForEmptyValue(tools.getHighlightedSearchValue(Agency.getSearchValueFromHW(Eval("TAB_SerialList").ToString() , "TAB" , txtSearch.Text), txtSearch.Text)) %>
                                    <br />
                                    <span style="font-weight:bold">SUPPORT TABLETTE S/N : </span><%# Agency.getSearchForEmptyValue(tools.getHighlightedSearchValue(Agency.getSearchValueFromHW(Eval("PDB_SerialList").ToString() , "PDB" , txtSearch.Text), txtSearch.Text)) %>
                                    <br />
                                    <span style="font-weight:bold">SCANNER S/N : </span><%# Agency.getSearchForEmptyValue(tools.getHighlightedSearchValue(Agency.getSearchValueFromHW(Eval("SCN_SerialList").ToString(), "SCN", txtSearch.Text), txtSearch.Text)) %>
                                    <br />
                                    <span style="font-weight:bold">PINPAD S/N : </span><%# Agency.getSearchForEmptyValue(tools.getHighlightedSearchValue(Agency.getSearchValueFromHW(Eval("PPD_SerialList").ToString(), "PPD", txtSearch.Text), txtSearch.Text)) %>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                    <FooterTemplate>
                        </div>
                    </FooterTemplate>
                </asp:Repeater>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="click" />
            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>
    <asp:Panel ID="panelAgencyDetails" runat="server" Style="display: none">
        <asp:UpdatePanel ID="upAgencyDetails" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="table" style="width: 100%">
                    <div class="table-row">
                        <div class="table-cell" style="padding: 0; vertical-align: top; width: 30px;">
                            <div class="font-VAGRoundedBold font-orange" style="font-size: 3em;" onclick="backToSearch();"><</div>
                        </div>
                        <div class="table-cell" style="text-align: center; padding: 7px 0 0 10px; vertical-align: middle">
                            <h2 style="margin-right: 40px;">Détail agence</h2>
                        </div>
                    </div>
                </div>
                <div style="font-weight: bold; text-transform: uppercase;" class="font-orange">
                    <asp:Label ID="lblAgencyID" runat="server"></asp:Label>
                    -
                    <asp:Label ID="lblAgencyName" runat="server"></asp:Label>
                </div>
                <div>
                    <asp:Label ID="lblManagerName" runat="server"></asp:Label>
                </div>
                <div id="divShowAgencyAddress" onclick="showAgencyAddress();" class="font-orange" style="text-decoration:underline">
                    afficher l'adresse
                </div>
                <div id="divAgencyAddress"style="display:none">
                    <asp:Label ID="lblAgencyAddress" runat="server"></asp:Label>
                </div>

                <div style="border-top: 1px solid #ff6a00; border-bottom: 1px solid #ff6a00; padding: 2px 0; margin: 20px 0 5px 0; text-transform: uppercase; font-weight: bold">
                    Liste borne(s)
                </div>
                <asp:Panel ID="panelMpadSearch" runat="server" Visible="false" DefaultButton="btnMpadSearch">
                    <div style="background-color: #ff6a00; color: #fff; font-size: 1em; padding: 5px; border-radius: 2px;">
                        Trop de bornes à afficher.<br />
                        Veuillez rechercher par numéro de série.
                    </div>
                    <div class="table" style="width: 100%; margin-top: 5px;">
                        <div class="table-row">
                            <div class="table-cell" style="padding: 0; text-align: left; vertical-align: top;">
                                <asp:TextBox ID="txtMpadSearch" runat="server" Style="margin: 0; padding: 0 2px; border-right: 0; height: 30px; width: 100%" CssClass="textbox" autocomplete="off"></asp:TextBox>
                            </div>
                            <div class="table-cell" style="width: 30px; padding: 0; text-align: right; vertical-align: top">
                                <div style="background-color: #fff; padding: 0; height: 30px;" onclick="MpadSearchClick();">
                                    <img src="Styles/Img/loupe.png" alt="loupe" style="padding: 0; height: 30px; border: 1px solid #bababa; border-left: 0;" />
                                </div>
                                <asp:Button ID="btnMpadSearch" runat="server" OnClientClick="return checkMpadSearch();" OnClick="btnMpadSearch_Click" Style="display: none" />
                            </div>
                        </div>
                    </div>
                    <asp:Panel ID="panelMpadSearchAdd" runat="server" style="margin-top:10px;">
                        <table style="background-color: #fff; width: 100%; box-shadow: 0px 1px 2px #888; padding: 5px 5px 2px 5px; margin-top: -5px; border-spacing: 0;">
                        <thead>
                            <tr onclick="showSearchAgencyToDeallocate('MPD');">
                                <th style="vertical-align: middle; padding: 5px 0 2px 0">
                                    <img src="Styles/Img/Mpad.png" alt="" style="max-height: 50px;max-width:50px" />
                                </th>
                                <th style="width: 100%; text-align: left; padding: 0 0 3px 2px; vertical-align: top;">
                                    <img alt="+" src="../Styles/Img/plus.png" style="height: 20px" />
                                </th>
                            </tr>
                        </thead>
                    </table>
                    </asp:Panel>
                </asp:Panel>
                <asp:Panel ID="panelMpadList" runat="server" Style="margin-top: 10px;">
                    <asp:Repeater ID="rptMpadList" runat="server">
                        <HeaderTemplate>
                            <table style="background-color: #fff; width: 100%; box-shadow: 0px 1px 2px #888; padding: 5px; margin-top: -5px; border-spacing: 0;">
                                <thead>
                                    <tr>
                                        <th>
                                            <div onclick="showSearchAgencyToDeallocate('MPD');">
                                                <img alt="+" src="../Styles/Img/plus.png" style="height: 20px" />
                                            </div>
                                        </th>
                                        <th>N&deg; série</th>
                                        <th>Statut</th>
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr class='<%# (Container.ItemIndex+1).ToString() == hfMpadListLength.Value ? "tr-no-border" : "tr-border" %>'
                                onclick="showMpadDetail('<%# Eval("MP_Serial") %>','<%# Eval("A0_LCK") %>', '<%# Eval("MP_MainSoftRelease") %>', '<%# Eval("MP_AbmSoftRelease") %>','<%# Agency.getMpadStatus(Eval("MP_sInitStatus").ToString(), Eval("MP_sInitStatusColor").ToString()).ToString().Replace("\"", "&quot;") %>','<%# Eval("MP_Token") %>', '<%#Eval("MP_TokenCreationDate")%>', '<%# Eval("MP_LastCnxDate") %>', '<%# Agency.getRemoteAccessStatus(Eval("A2_RA").ToString()) %>');">
                                <td style="text-align: center; vertical-align: middle; padding: 5px; width: 30px;">
                                    <asp:HiddenField ID="hfMpadRef" runat="server" Value='<%# Eval("MP_Ref") %>' />
                                    <img src="Styles/Img/Mpad.png" alt="" style="max-height: 50px;max-width:50px" />
                                </td>
                                <td style="vertical-align: middle; padding: 5px; text-align: center;">
                                    <%# Eval("MP_Serial") %>
                                </td>
                                <td style="vertical-align: middle; padding: 5px; text-align: center; width: 100px">
                                    <%# Agency.getMpadStatus(Eval("MP_sInitStatus").ToString(),Eval("MP_sInitStatusColor").ToString()) %>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <asp:HiddenField ID="hfMpadListLength" runat="server" Value="0" />
                    <asp:Panel ID="panelMpadListEmpty" runat="server" Visible="false">
                        <table style="background-color: #fff; width: 100%; box-shadow: 0px 1px 2px #888; padding: 5px; margin-top: -5px; border-spacing: 0;">
                            <thead>
                                <tr onclick="showSearchAgencyToDeallocate('MPD');">
                                    <th style="vertical-align: middle; padding: 5px 0">
                                        <img src="Styles/Img/Mpad.png" alt="" style="max-height: 50px;max-width:50px" />
                                    </th>
                                    <th style="width: 100%; text-align: left; padding: 0 0 5px 2px; vertical-align: top;">
                                        <img alt="+" src="../Styles/Img/plus.png" style="height: 20px" />
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </asp:Panel>
                </asp:Panel>

                <div style="border-top: 1px solid #ff6a00; border-bottom: 1px solid #ff6a00; padding: 2px 0; margin: 20px 0 5px 0; text-transform: uppercase; font-weight: bold">
                    Liste tablette(s)
                </div>
                <asp:Panel ID="panelTabletSearch" runat="server" Visible="false" DefaultButton="btnTabletSearch">
                     <div style="background-color: #ff6a00; color: #fff; font-size: 1em; padding: 5px; border-radius: 2px;">
                        Trop de tablettes à afficher.<br />
                        Veuillez rechercher par numéro de série.
                    </div>
                    <div class="table" style="width: 100%; margin-top: 5px;">
                        <div class="table-row">
                            <div class="table-cell" style="padding: 0; text-align: left; vertical-align: top;">
                                <asp:TextBox ID="txtTabletSearch" runat="server" Style="margin: 0; padding: 0 2px; border-right: 0; height: 30px; width: 100%" CssClass="textbox" autocomplete="off"></asp:TextBox>
                            </div>
                            <div class="table-cell" style="width: 30px; padding: 0; text-align: right; vertical-align: top">
                                <div style="background-color: #fff; padding: 0; height: 30px;" onclick="TabletSearchClick();">
                                    <img src="Styles/Img/loupe.png" alt="loupe" style="padding: 0; height: 30px; border: 1px solid #bababa; border-left: 0;" />
                                </div>
                                <asp:Button ID="btnTabletSearch" runat="server" OnClientClick="return checkTabletSearch();" OnClick="btnTabletSearch_Click" Style="display: none" />
                            </div>
                        </div>
                    </div>
                    <asp:Panel ID="panelTabletSearchAdd" runat="server" style="margin-top:10px;">
                        <table style="background-color: #fff; width: 100%; box-shadow: 0px 1px 2px #888; padding: 5px 5px 2px 5px; margin-top: -5px; border-spacing: 0;">
                        <thead>
                            <tr onclick="showSearchAgencyToDeallocate('TAB');">
                                <th style="vertical-align: middle; padding: 5px 0 2px 0">
                                    <img src="Styles/Img/Tablet.png" alt="" style="height: 25px;" />
                                </th>
                                <th style="width: 100%; text-align: left; padding: 0 0 3px 2px; vertical-align: top;">
                                    <img alt="+" src="../Styles/Img/plus.png" style="height: 20px" />
                                </th>
                            </tr>
                        </thead>
                    </table>
                    </asp:Panel>
                </asp:Panel>
                <asp:Panel ID="panelTabletList" runat="server" Style="margin-top: 10px;">
                    <asp:Repeater ID="rptTabletList" runat="server">
                        <HeaderTemplate>
                            <table style="background-color: #fff; width: 100%; box-shadow: 0px 1px 2px #888; padding: 5px; margin-top: -5px; border-spacing: 0;">
                                <thead>
                                    <tr>
                                        <th>
                                            <div onclick="showSearchAgencyToDeallocate('TAB');">
                                                <img alt="+" src="../Styles/Img/plus.png" style="height: 20px" />
                                            </div>
                                        </th>
                                        <th>N&deg; série</th>
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr class='<%# (Container.ItemIndex+1).ToString() == hfTabletListLength.Value ? "tr-no-border" : "tr-border" %>'
                                onclick="showTabletDetail('<%# Eval("TAB_Serial") %>','<%# Eval("TAB_FreeLabel") %>');">
                                <td style="text-align: center; vertical-align: middle; padding: 5px; width: 30px;">
                                    <asp:HiddenField ID="hfTabletRef" runat="server" Value='<%# Eval("TAB_Ref") %>' />
                                    <img src="Styles/Img/Tablet.png" alt="" style="max-height: 50px;max-width:50px" />
                                </td>
                                <td style="vertical-align: middle; padding: 5px; text-align: center;">
                                    <%# Eval("TAB_Serial") %>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <asp:HiddenField ID="hfTabletListLength" runat="server" Value="0" />
                    <asp:Panel ID="panelTabletListEmpty" runat="server" Visible="false">
                        <table style="background-color: #fff; width: 100%; box-shadow: 0px 1px 2px #888; padding: 5px; margin-top: -5px; border-spacing: 0;">
                            <thead>
                                <tr onclick="showSearchAgencyToDeallocate('TAB');">
                                    <th style="vertical-align: middle; padding: 5px 0">
                                        <img src="Styles/Img/Tablet.png" alt="" style="max-height: 50px;max-width:50px" />
                                    </th>
                                    <th style="width: 100%; text-align: left; padding: 0 0 5px 2px; vertical-align: top;">
                                        <img alt="+" src="../Styles/Img/plus.png" style="height: 20px" />
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </asp:Panel>
                </asp:Panel>

                <div style="border-top: 1px solid #ff6a00; border-bottom: 1px solid #ff6a00; padding: 2px 0; margin: 20px 0 5px 0; text-transform: uppercase; font-weight: bold">
                    Liste support(s) borne
                </div>
                <asp:Panel ID="panelTabletSupportSearch" runat="server" Visible="false" DefaultButton="btnTabletSupportSearch">
                     <div style="background-color: #ff6a00; color: #fff; font-size: 1em; padding: 5px; border-radius: 2px;">
                        Trop de supports borne à afficher.<br />
                        Veuillez rechercher par numéro de série.
                    </div>
                    <div class="table" style="width: 100%; margin-top: 5px;">
                        <div class="table-row">
                            <div class="table-cell" style="padding: 0; text-align: left; vertical-align: top;">
                                <asp:TextBox ID="txtTabletSupportSearch" runat="server" Style="margin: 0; padding: 0 2px; border-right: 0; height: 30px; width: 100%" CssClass="textbox" autocomplete="off"></asp:TextBox>
                            </div>
                            <div class="table-cell" style="width: 30px; padding: 0; text-align: right; vertical-align: top">
                                <div style="background-color: #fff; padding: 0; height: 30px;" onclick="TabletSupportSearchClick();">
                                    <img src="Styles/Img/loupe.png" alt="loupe" style="padding: 0; height: 30px; border: 1px solid #bababa; border-left: 0;" />
                                </div>
                                <asp:Button ID="btnTabletSupportSearch" runat="server" OnClientClick="return checkTabletSupportSearch();" OnClick="btnTabletSupportSearch_Click" Style="display: none" />
                            </div>
                        </div>
                    </div>
                    <asp:Panel ID="panelTabletSupportSearchAdd" runat="server" style="margin-top:10px;">
                        <table style="background-color: #fff; width: 100%; box-shadow: 0px 1px 2px #888; padding: 5px 5px 2px 5px; margin-top: -5px; border-spacing: 0;">
                        <thead>
                            <tr onclick="showSearchAgencyToDeallocate('PDB');">
                                <th style="vertical-align: middle; padding: 5px 0 2px 0">
                                    <img src='Styles/Img/TabletSupport.png' alt="" style="height: 50px;" />
                                </th>
                                <th style="width: 100%; text-align: left; padding: 0 0 3px 2px; vertical-align: top;">
                                    <img alt="+" src="../Styles/Img/plus.png" style="height: 20px" />
                                </th>
                            </tr>
                        </thead>
                    </table>
                    </asp:Panel>
                </asp:Panel>
                <asp:Panel ID="panelTabletSupportList" runat="server" Style="margin-top: 10px;">
                    <asp:Repeater ID="rptTabletSupportList" runat="server">
                        <HeaderTemplate>
                            <table style="background-color: #fff; width: 100%; box-shadow: 0px 1px 2px #888; padding: 5px; margin-top: -5px; border-spacing: 0;">
                                <thead>
                                    <tr>
                                        <th>
                                            <div onclick="showSearchAgencyToDeallocate('PDB');">
                                                <img alt="+" src="../Styles/Img/plus.png" style="height: 20px" />
                                            </div>
                                        </th>
                                        <th>N&deg; série</th>
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr class='<%# (Container.ItemIndex+1).ToString() == hfTabletSupportListLength.Value ? "tr-no-border" : "tr-border" %>'
                                onclick="showTabletSupportDetail('<%# Eval("PDB_Serial") %>','<%# Eval("PDB_FreeLabel") %>', '<%# Agency.getTabletSupportModelName(Eval("PDB_Serial").ToString())%>', '<%# Agency.getTabletSupportImageName(Agency.getTabletSupportModelName(Eval("PDB_Serial").ToString()))%>');">
                                <td style="text-align: center; vertical-align: middle; padding: 5px; width: 30px;">
                                    <asp:HiddenField ID="hfTabletSupportRef" runat="server" Value='<%# Eval("PDB_Ref") %>' />
                                    <img src='Styles/Img/<%# Agency.getTabletSupportImageName(Agency.getTabletSupportModelName(Eval("PDB_Serial").ToString())) %>.png' alt=""
                                        style="max-height: 50px;max-width:50px" />
                                </td>
                                <td style="vertical-align: middle; padding: 5px; text-align: center;">
                                    <%# Eval("PDB_Serial") %>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <asp:HiddenField ID="hfTabletSupportListLength" runat="server" Value="0" />
                    <asp:Panel ID="panelTabletSupportListEmpty" runat="server" Visible="false">
                        <table style="background-color: #fff; width: 100%; box-shadow: 0px 1px 2px #888; padding: 5px; margin-top: -5px; border-spacing: 0;">
                            <thead>
                                <tr onclick="showSearchAgencyToDeallocate('PDB');">
                                    <th style="vertical-align: middle; padding: 5px 0">
                                        <img src="Styles/Img/TabletSupport.png" alt="" style="max-height: 50px;max-width:50px" />
                                    </th>
                                    <th style="width: 100%; text-align: left; padding: 0 0 5px 2px; vertical-align: top;">
                                        <img alt="+" src="../Styles/Img/plus.png" style="height: 20px" />
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </asp:Panel>
                </asp:Panel>

                <div style="border-top: 1px solid #ff6a00; border-bottom: 1px solid #ff6a00; padding: 2px 0; margin: 20px 0 5px 0; text-transform: uppercase; font-weight: bold">
                    Liste scanner(s)
                </div>
                <asp:Panel ID="panelScannerSearch" runat="server" Visible="false" DefaultButton="btnScannerSearch">
                     <div style="background-color: #ff6a00; color: #fff; font-size: 1em; padding: 5px; border-radius: 2px;">
                        Trop de scanners à afficher.<br />
                        Veuillez rechercher par numéro de série.
                    </div>
                    <div class="table" style="width: 100%; margin-top: 5px;">
                        <div class="table-row">
                            <div class="table-cell" style="padding: 0; text-align: left; vertical-align: top;">
                                <asp:TextBox ID="txtScannerSearch" runat="server" Style="margin: 0; padding: 0 2px; border-right: 0; height: 30px; width: 100%" CssClass="textbox" autocomplete="off"></asp:TextBox>
                            </div>
                            <div class="table-cell" style="width: 30px; padding: 0; text-align: right; vertical-align: top">
                                <div style="background-color: #fff; padding: 0; height: 30px;" onclick="ScannerSearchClick();">
                                    <img src="Styles/Img/loupe.png" alt="loupe" style="padding: 0; height: 30px; border: 1px solid #bababa; border-left: 0;" />
                                </div>
                                <asp:Button ID="btnScannerSearch" runat="server" OnClientClick="return checkScannerSearch();" OnClick="btnScannerSearch_Click" Style="display: none" />
                            </div>
                        </div>
                    </div>
                    <asp:Panel ID="panelScannerSearchAdd" runat="server" style="margin-top:10px;">
                        <table style="background-color: #fff; width: 100%; box-shadow: 0px 1px 2px #888; padding: 5px 5px 2px 5px; margin-top: -5px; border-spacing: 0;">
                        <thead>
                            <tr onclick="showSearchAgencyToDeallocate('SCN');">
                                <th style="vertical-align: middle; padding: 5px 0 2px 0">
                                    <img src="Styles/Img/Scanner.png" alt="" style="height: 25px;" />
                                </th>
                                <th style="width: 100%; text-align: left; padding: 0 0 3px 2px; vertical-align: top;">
                                    <img alt="+" src="../Styles/Img/plus.png" style="height: 20px" />
                                </th>
                            </tr>
                        </thead>
                    </table>
                    </asp:Panel>
                </asp:Panel>
                <asp:Panel ID="panelScannerList" runat="server" Style="margin-top: 10px;">
                    <asp:Repeater ID="rptScannerList" runat="server">
                        <HeaderTemplate>
                            <table style="background-color: #fff; width: 100%; box-shadow: 0px 1px 2px #888; padding: 5px; margin-top: -5px; border-spacing: 0;">
                                <thead>
                                    <tr>
                                        <th>
                                            <div onclick="showSearchAgencyToDeallocate('SCN');">
                                                <img alt="+" src="../Styles/Img/plus.png" style="height: 20px" />
                                            </div>
                                        </th>
                                        <th>N&deg; série</th>
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr class='<%# (Container.ItemIndex+1).ToString() == hfScannerListLength.Value ? "tr-no-border" : "tr-border" %>'
                                onclick="showScannerDetail('<%# Eval("SCN_Serial") %>','<%# Eval("SCN_FreeLabel") %>', '<%# Agency.getScannerModelName(Eval("SCN_FreeLabel").ToString())%>', '<%# Agency.getScannerImageName(Agency.getScannerModelName(Eval("SCN_FreeLabel").ToString()))%>');">
                                <td style="text-align: center; vertical-align: middle; padding: 5px; width: 30px;">
                                    <asp:HiddenField ID="hfScannerRef" runat="server" Value='<%# Eval("SCN_Ref") %>' />
                                    <img src='Styles/Img/<%# Agency.getScannerImageName(Agency.getScannerModelName(Eval("SCN_FreeLabel").ToString())) %>.png' alt=""
                                        style="max-height: 50px;max-width:50px" />
                                </td>
                                <td style="vertical-align: middle; padding: 5px; text-align: center;">
                                    <%# Eval("SCN_Serial") %>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <asp:HiddenField ID="hfScannerListLength" runat="server" Value="0" />
                    <asp:Panel ID="panelScannerListEmpty" runat="server" Visible="false">
                        <table style="background-color: #fff; width: 100%; box-shadow: 0px 1px 2px #888; padding: 5px; margin-top: -5px; border-spacing: 0;">
                            <thead>
                                <tr onclick="showSearchAgencyToDeallocate('SCN');">
                                    <th style="vertical-align: middle; padding: 5px 0">
                                        <img src="Styles/Img/Scanner.png" alt="" style="max-height: 50px;max-width:50px" />
                                    </th>
                                    <th style="width: 100%; text-align: left; padding: 0 0 5px 2px; vertical-align: top;">
                                        <img alt="+" src="../Styles/Img/plus.png" style="height: 20px" />
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </asp:Panel>
                </asp:Panel>

                <div style="border-top: 1px solid #ff6a00; border-bottom: 1px solid #ff6a00; padding: 2px 0; margin: 20px 0 5px 0; text-transform: uppercase; font-weight: bold">
                    Liste TPE(s)
                </div>
                <asp:Panel ID="panelPosSearch" runat="server" Visible="false" DefaultButton="btnPosSearch">
                    <div style="background-color: #ff6a00; color: #fff; font-size: 1em; padding: 5px; border-radius: 2px;">
                        Trop de terminaux à afficher.<br />
                        Veuillez rechercher par numéro de série.
                    </div>
                    <div class="table" style="width: 100%; margin-top: 5px;">
                        <div class="table-row">
                            <div class="table-cell" style="padding: 0; text-align: left; vertical-align: top;">
                                <asp:TextBox ID="txtPosSearch" runat="server" Style="margin: 0; padding: 0 2px; border-right: 0; height: 30px; width: 100%" CssClass="textbox" autocomplete="off"></asp:TextBox>
                            </div>
                            <div class="table-cell" style="width: 30px; padding: 0; text-align: right; vertical-align: top">
                                <div style="background-color: #fff; padding: 0; height: 30px;" onclick="PosSearchClick();">
                                    <img src="Styles/Img/loupe.png" alt="loupe" style="padding: 0; height: 30px; border: 1px solid #bababa; border-left: 0;" />
                                </div>
                                <asp:Button ID="btnPosSearch" runat="server" OnClientClick="return checkPosSearch();" OnClick="btnPosSearch_Click" Style="display: none" />
                            </div>
                        </div>
                    </div>
                    <asp:Panel ID="panelPosSearchAdd" runat="server" style="margin-top:10px;">
                        <table style="background-color: #fff; width: 100%; box-shadow: 0px 1px 2px #888; padding: 5px 5px 2px 5px; margin-top: -5px; border-spacing: 0;">
                            <thead>
                                <tr onclick="showSearchAgencyToDeallocate('POS');">
                                    <th style="vertical-align: middle; padding: 5px 0 2px 0">
                                        <img src="Styles/Img/ingenico.png" alt="" style="height: 25px;" />
                                    </th>
                                    <th style="width: 100%; text-align: left; padding: 0 0 3px 2px; vertical-align: top;">
                                        <img alt="+" src="../Styles/Img/plus.png" style="height: 20px" />
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </asp:Panel>
                </asp:Panel>
                <asp:Panel ID="panelPosList" runat="server" style="margin-top:10px;">
                    <asp:Repeater ID="rptPosList" runat="server">
                        <HeaderTemplate>
                            <table style="background-color: #fff; width: 100%; box-shadow: 0px 1px 2px #888; padding: 5px; border-spacing: 0;">
                                <thead>
                                    <tr>
                                        <th>
                                            <div onclick="showSearchAgencyToDeallocate('POS');">
                                                <img alt="+" src="../Styles/Img/plus.png" style="height: 20px" />
                                            </div>
                                        </th>
                                        <th>N&deg; série</th>
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr class='<%# (Container.ItemIndex+1).ToString() == hfPosListLength.Value ? "tr-no-border" : "tr-border" %>'
                                onclick="showPosDetail('<%# Eval("PS_Serial") %>','<%# Eval("PS_IsLocked") %>','<%# Eval("PS_SoftRelease") %>','<%# Eval("PS_LastCnxDate") %>','<%# Agency.getPosStatus(Eval("PS_IsLocked").ToString()).Replace("\"", "&quot;") %>','<%# Agency.getPosImageName(Agency.getPosModelName(Eval("PS_Serial").ToString()))%>');">
                                <td style="text-align: center; padding: 5px; vertical-align: middle; width: 30px;">
                                    <asp:HiddenField ID="hfPosRef" runat="server" Value='<%# Eval("PS_Ref") %>' />
                                    <img src='Styles/Img/<%# Agency.getPosImageName(Agency.getPosModelName(Eval("PS_Serial").ToString())) %>.png' alt=""
                                        style="max-height: 50px;max-width:50px" />
                                </td>
                                <td style="padding: 5px; text-align: center; vertical-align: middle">
                                    <%# Eval("PS_Serial") %>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <asp:HiddenField ID="hfPosListLength" runat="server" Value="0" />
                    <asp:Panel ID="panelPosListEmpty" runat="server" Visible="false">
                        <table style="background-color: #fff; width: 100%; box-shadow: 0px 1px 2px #888; padding: 5px; margin-top: -5px; border-spacing: 0;">
                            <thead>
                                <tr onclick="showSearchAgencyToDeallocate('POS');">
                                    <th style="vertical-align: middle; padding: 5px 0">
                                        <img src="Styles/Img/ingenico.png" alt="" style="max-height: 50px;max-width:50px" />
                                    </th>
                                    <th style="width: 100%; text-align: left; padding: 0 0 5px 2px; vertical-align: top;">
                                        <img alt="+" src="../Styles/Img/plus.png" style="height: 20px" />
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </asp:Panel>
                </asp:Panel>

                <div style="border-top: 1px solid #ff6a00; border-bottom: 1px solid #ff6a00; padding: 2px 0; margin: 20px 0 5px 0; text-transform: uppercase; font-weight: bold">
                    Liste PINPAD(s)
                </div>
                <asp:Panel ID="panelPinpadSearch" runat="server" Visible="false" DefaultButton="btnPinpadSearch">
                    <div style="background-color: #ff6a00; color: #fff; font-size: 1em; padding: 5px; border-radius: 2px;">
                        Trop de pinpads à afficher.<br />
                        Veuillez rechercher par numéro de série.
                    </div>
                    <div class="table" style="width: 100%; margin-top: 5px;">
                        <div class="table-row">
                            <div class="table-cell" style="padding: 0; text-align: left; vertical-align: top;">
                                <asp:TextBox ID="txtPinpadSearch" runat="server" Style="margin: 0; padding: 0 2px; border-right: 0; height: 30px; width: 100%" CssClass="textbox" autocomplete="off"></asp:TextBox>
                            </div>
                            <div class="table-cell" style="width: 30px; padding: 0; text-align: right; vertical-align: top">
                                <div style="background-color: #fff; padding: 0; height: 30px;" onclick="PosSearchClick();">
                                    <img src="Styles/Img/loupe.png" alt="loupe" style="padding: 0; height: 30px; border: 1px solid #bababa; border-left: 0;" />
                                </div>
                                <asp:Button ID="btnPinpadSearch" runat="server" OnClientClick="return checkPinpadSearch();" OnClick="btnPinpadSearch_Click" Style="display: none" />
                            </div>
                        </div>
                    </div>
                    <asp:Panel ID="panelPinpadSearchAdd" runat="server" style="margin-top:10px;">
                        <table style="background-color: #fff; width: 100%; box-shadow: 0px 1px 2px #888; padding: 5px 5px 2px 5px; margin-top: -5px; border-spacing: 0;">
                            <thead>
                                <tr onclick="showSearchAgencyToDeallocate('PPD');">
                                    <th style="vertical-align: middle; padding: 5px 0 2px 0">
                                        <img src="Styles/Img/pinpad_ipp310.png" alt="" style="height: 25px;" />
                                    </th>
                                    <th style="width: 100%; text-align: left; padding: 0 0 3px 2px; vertical-align: top;">
                                        <img alt="+" src="../Styles/Img/plus.png" style="height: 20px" />
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </asp:Panel>
                </asp:Panel>
                <asp:Panel ID="panelPinpadList" runat="server" style="margin-top:10px;">
                    <asp:Repeater ID="rptPinpadList" runat="server">
                        <HeaderTemplate>
                            <table style="background-color: #fff; width: 100%; box-shadow: 0px 1px 2px #888; padding: 5px; border-spacing: 0;">
                                <thead>
                                    <tr>
                                        <th>
                                            <div onclick="showSearchAgencyToDeallocate('PPD');">
                                                <img alt="+" src="../Styles/Img/plus.png" style="height: 20px" />
                                            </div>
                                        </th>
                                        <th>N&deg; série</th>
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr class='<%# (Container.ItemIndex+1).ToString() == hfPinpadListLength.Value ? "tr-no-border" : "tr-border" %>'
                                onclick="showPinpadDetail('<%# Eval("PPD_Serial") %>','<%# Eval("PPD_FreeLabel") %>')">
                                <td style="text-align: center; padding: 5px; vertical-align: middle; width: 30px;">
                                    <asp:HiddenField ID="hfPosRef" runat="server" Value='<%# Eval("PPD_Ref") %>' />
                                    <img src='Styles/Img/pinpad_ipp310.png' alt=""
                                        style="height: 25px;" />
                                </td>
                                <td style="padding: 5px; text-align: center; vertical-align: middle">
                                    <%# Eval("PPD_Serial") %>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <asp:HiddenField ID="hfPinpadListLength" runat="server" Value="0" />
                    <asp:Panel ID="panelPinpadListEmpty" runat="server" Visible="false">
                        <table style="background-color: #fff; width: 100%; box-shadow: 0px 1px 2px #888; padding: 5px; margin-top: -5px; border-spacing: 0;">
                            <thead>
                                <tr onclick="showSearchAgencyToDeallocate('PPD');">
                                    <th style="vertical-align: middle; padding: 5px 0">
                                        <img src="Styles/Img/pinpad_ipp310.png" alt="" style="max-height: 50px;max-width:50px" />
                                    </th>
                                    <th style="width: 100%; text-align: left; padding: 0 0 5px 2px; vertical-align: top;">
                                        <img alt="+" src="../Styles/Img/plus.png" style="height: 20px" />
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </asp:Panel>
                </asp:Panel>

                <asp:HiddenField ID="hfPosSerialList_temp" runat="server" Value="" />
                <asp:HiddenField ID="hfMpadSerialList_temp" runat="server" Value="" />
                <asp:HiddenField ID="hfTabletSerialList_temp" runat="server" Value="" />
                <asp:HiddenField ID="hfTabletSupportSerialList_temp" runat="server" Value="" />
                <asp:HiddenField ID="hfScannerSerialList_temp" runat="server" Value="" />
                <asp:HiddenField ID="hfPinpadSerialList_temp" runat="server" Value="" />

                <asp:Button ID="btnRefreshAgencyDetails" runat="server" OnClick="btnRefreshAgencyDetails_Click" Style="display: none" />
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnRefreshAgencyDetails" EventName="click" />
                <asp:AsyncPostBackTrigger ControlID="btnMpadSearch" EventName="click"/>
                <asp:AsyncPostBackTrigger ControlID="btnPosSearch" EventName="click"/>
                <asp:AsyncPostBackTrigger ControlID="btnScannerSearch" EventName="click"/>
                <asp:AsyncPostBackTrigger ControlID="btnTabletSearch" EventName="click"/>
                <asp:AsyncPostBackTrigger ControlID="btnTabletSupportSearch" EventName="click"/>
                <asp:AsyncPostBackTrigger ControlID="btnPinpadSearch" EventName="click"/>
            </Triggers>
        </asp:UpdatePanel>
        <asp:UpdateProgress ID="upAgencyDetailsLoading" runat="server" AssociatedUpdatePanelID="upAgencyDetails">
            <ProgressTemplate>
                <div style="text-align: center; position: absolute; left: 45%; top: 50%">
                    <img src="../Styles/Img/loading.gif" alt="loading..." />
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </asp:Panel>

    <asp:Panel ID="panelSearchAgencyToAllocate" runat="server" Style="display: none; padding-top: 10px;">
        <div>
            <input type="button" value="annuler" class="button" onclick="hideSearchAgencyToAllocate();" />
        </div>
        <h2 style="color: #344b56; margin: 0; text-transform: uppercase; text-align: center; font-size: 1.4em;">Veuillez sélectionner l'agence de destination</h2>
        <div style="display: none">
            <asp:TextBox ID="txtSearchAgencyToAllocate" runat="server" Style="margin: 0; padding: 0 2px; border-right: 0; height: 30px; width: 100%" autocomplete="off"></asp:TextBox>
            <asp:Button ID="btnSearchAgencyToAllocate" runat="server" OnClick="btnSearchAgencyToAllocate_Click" Style="display: none" />
        </div>
        <asp:UpdateProgress ID="upSearchAgencyToAllocateLoading" runat="server" AssociatedUpdatePanelID="upSearchAgencyToAllocate">
            <ProgressTemplate>
                <div style="text-align: center; position: absolute; left: 45%; top: 50%">
                    <img src="../Styles/Img/loading.gif" alt="loading..." />
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:UpdatePanel ID="upSearchAgencyToAllocate" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="panelSearchAgencyToAllocateResultNb" runat="server" Visible="false" Style="margin-top: 10px;">
                    <asp:Label ID="lblSearchAgencyToAllocateResultNb" runat="server" Style="font-weight: bold"></asp:Label>
                </asp:Panel>
                <asp:Repeater ID="rptSearchAgencyToAllocateResult" runat="server">
                    <HeaderTemplate>
                        <div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <div style="margin-top: 10px; background-color: #fff; padding: 10px 5px; box-shadow: 0px 1px 2px #888;"
                            onclick="showConfirmHardwareReallocate('<%=hfHardwareSerial_selected.Value %>', '<%= hfAgencyID_selected.Value %>','<%= hfAgencyName_selected.Value %>','<%# Eval("A_Name") %>', '<%# Eval("A_ID")%>', '<%# Eval("P_Ref")%>')">
                            <h3 style="font-family: Arial; font-size: 1.2em; font-weight: lighter; color: #ff6a00"><%# Eval("A_ID").ToString() %> -
                                <%# Eval("A_Name").ToString() %></h3>
                            <div style="font-size: 1em;">
                                <%# Eval("A_ManLastName").ToString() %>
                                <%# Eval("A_ManFirstName").ToString() %>
                                <br />
                                TPE S/N : <%# Agency.getSearchForEmptyValue(Agency.getSearchValueFromPOS(Eval("POS_SerialList").ToString(), "")) %>
                                <br />
                                MPAD S/N : <%# Agency.getSearchForEmptyValue(Agency.getSearchValueFromMPAD(Eval("MP_SerialList").ToString(), "")) %>
                                <br />
                                TABLETTE S/N : <%# Agency.getSearchForEmptyValue(Agency.getSearchValueFromHW(Eval("TAB_SerialList").ToString() , "TAB" , "")) %>
                                <br />
                                SUPPORT TABLETTE S/N : <%# Agency.getSearchForEmptyValue(Agency.getSearchValueFromHW(Eval("PDB_SerialList").ToString() , "PDB" , "")) %>
                                <br />
                                SCANNER S/N : <%# Agency.getSearchForEmptyValue(Agency.getSearchValueFromHW(Eval("SCN_SerialList").ToString(), "SCN", "")) %>
                                <br />
                                PINPAD S/N : <%# Agency.getSearchForEmptyValue(Agency.getSearchValueFromHW(Eval("PPD_SerialList").ToString(), "PPD", "")) %>
                            </div>
                        </div>
                    </ItemTemplate>
                    <FooterTemplate>
                        </div>
                    </FooterTemplate>
                </asp:Repeater>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnSearchAgencyToAllocate" EventName="click" />
            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>

    <asp:Panel ID="panelSearchAgencyToDeallocate" runat="server" Style="display: none; padding-top: 10px;">
        <div>
            <input type="button" value="annuler" class="button" onclick="hideSearchAgencyToDeallocate();" />
        </div>
        <h2 style="color: #344b56; margin: 0; text-transform: uppercase; text-align: center; font-size: 1.4em;">Veuillez sélectionner l'agence source</h2>
        <div style="display: none">
            <asp:TextBox ID="txtSearchAgencyToDeallocate" runat="server" Style="margin: 0; padding: 0 2px; border-right: 0; height: 30px; width: 100%" autocomplete="off"></asp:TextBox>
            <asp:Button ID="btnSearchAgencyToDeallocate" runat="server" OnClick="btnSearchAgencyToDeallocate_Click" Style="display: none" />
        </div>

        <asp:UpdateProgress ID="upSearchAgencyToDeallocateLoading" runat="server" AssociatedUpdatePanelID="upSearchAgencyToDeallocate">
            <ProgressTemplate>
                <div style="text-align: center; position: absolute; left: 45%; top: 50%">
                    <img src="../Styles/Img/loading.gif" alt="loading..." />
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:UpdatePanel ID="upSearchAgencyToDeallocate" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="panelSearchAgencyToDeallocateResultNb" runat="server" Visible="false" Style="margin-top: 10px;">
                    <asp:Label ID="lblSearchAgencyToDeallocateResultNb" runat="server" Style="font-weight: bold"></asp:Label>
                </asp:Panel>
                <asp:Repeater ID="rptSearchAgencyToDeallocateResult" runat="server">
                    <HeaderTemplate>
                        <div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <div style="margin-top: 10px; background-color: #fff; padding: 10px 5px; box-shadow: 0px 1px 2px #888;"
                            onclick="showAgencyToDeallocateDetails('<%# Eval("P_Ref") %>','<%# Eval("A_Ref") %>', '<%# Eval("A_ID")%>','<%# Eval("A_Name")%>','<%# Eval("A_ManLastName")%>','<%# Eval("A_ManFirstName")%>')">
                            <h3 style="font-family: Arial; font-size: 1.2em; font-weight: lighter; color: #ff6a00"><%# Eval("A_ID").ToString() %> -
                                <%# Eval("A_Name") %></h3>
                            <div style="font-size: 1em;">
                                <div style='<%# (Eval("A_Name2").ToString().Trim().Length > 0) ? "": "display:none" %>; font-size: 0.8em; font-weight: bold;' class="font-orange">
                                    <%# tools.getHighlightedSearchValue(Eval("A_Name2").ToString(), txtSearch.Text) %>
                                </div>
                                <%# Eval("A_ManLastName") %>
                                <%# Eval("A_ManFirstName") %>
                                <br />
                                TPE S/N : <%# Agency.getSearchForEmptyValue(Agency.getSearchValueFromHW(Eval("POS_SerialList").ToString(), "POS" ,"")) %>
                                <br />
                                MPAD S/N : <%# Agency.getSearchForEmptyValue(Agency.getSearchValueFromHW(Eval("MP_SerialList").ToString(), "MPD", "")) %>
                                <br />
                                TABLETTE S/N : <%# Agency.getSearchForEmptyValue(Agency.getSearchValueFromHW(Eval("TAB_SerialList").ToString() , "TAB" , "")) %>
                                <br />
                                SUPPORT TABLETTE S/N : <%# Agency.getSearchForEmptyValue(Agency.getSearchValueFromHW(Eval("PDB_SerialList").ToString() , "PDB" , "")) %>
                                <br />
                                SCANNER S/N : <%# Agency.getSearchForEmptyValue(Agency.getSearchValueFromHW(Eval("SCN_SerialList").ToString(), "SCN", "")) %>
                                <br />
                                PINPAD S/N : <%# Agency.getSearchForEmptyValue(Agency.getSearchValueFromHW(Eval("PPD_SerialList").ToString(), "PPD", "")) %>
                            </div>
                        </div>
                    </ItemTemplate>
                    <FooterTemplate>
                        </div>
                    </FooterTemplate>
                </asp:Repeater>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnSearchAgencyToDeallocate" EventName="click" />
            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>
    <asp:Panel ID="panelAgencyDetails_AgencyToDeallocate" runat="server" Style="display: none; padding-top: 10px;">
        <asp:UpdatePanel ID="upAgencyDetails_AgencyToDeallocate" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="table" style="width: 100%">
                    <div class="table-row">
                        <div class="table-cell" style="padding: 0; vertical-align: top; width: 30px;">
                            <div class="font-VAGRoundedBold font-orange" style="font-size: 3em;" onclick="backToAgencyToDeallocate();"><</div>
                        </div>
                        <div class="table-cell" style="text-align: center; padding: 7px 0 0 10px; vertical-align: middle">
                            <h2 style="margin-right: 40px; font-size: 1.4em;">
                                <asp:Label ID="lblAgencyToDeallocateTitle" runat="server"></asp:Label>
                            </h2>
                        </div>
                    </div>
                </div>
                <div style="font-weight: bold; text-transform: uppercase; margin-top: 5px;" class="font-orange">
                    <asp:Label ID="lblAgencyID_AgencyToDeallocate" runat="server"></asp:Label>
                    -
                    <asp:Label ID="lblAgencyName_AgencyToDeallocate" runat="server"></asp:Label>
                </div>
                <div>
                    <asp:Label ID="lblManagerName_AgencyToDeallocate" runat="server"></asp:Label>
                </div>

                <div style="border-top: 1px solid #ff6a00; border-bottom: 1px solid #ff6a00; padding: 2px 0; margin: 20px 0 5px 0; text-transform: uppercase; font-weight: bold">
                    <asp:Label ID="lblHardwareToAddTitle" runat="server"></asp:Label>
                </div>
                <asp:Panel ID="panelHardwareToAddSearch" runat="server" Visible="false" DefaultButton="btnHardwareToAddSearch">
                    <div style="background-color: #ff6a00; color: #fff; font-size: 1em; padding: 5px; border-radius: 2px;">
                        <asp:Label ID="lblHardwareToAddSearch" runat="server"></asp:Label>
                    </div>
                    <div class="table" style="width: 100%; margin-top: 5px;">
                        <div class="table-row">
                            <div class="table-cell" style="padding: 0; text-align: left; vertical-align: top;">
                                <asp:TextBox ID="txtHardwareToAddSearch" runat="server" Style="margin: 0; padding: 0 2px; border-right: 0; height: 30px; width: 100%" CssClass="textbox" autocomplete="off"></asp:TextBox>
                            </div>
                            <div class="table-cell" style="width: 30px; padding: 0; text-align: right; vertical-align: top">
                                <div style="background-color: #fff; padding: 0; height: 30px;" onclick="HardwareToAddSearchClick();">
                                    <img src="Styles/Img/loupe.png" alt="loupe" style="padding: 0; height: 30px; border: 1px solid #bababa; border-left: 0;" />
                                </div>
                                <asp:Button ID="btnHardwareToAddSearch" runat="server" OnClick="btnHardwareToAddSearch_Click" OnClientClick="return checkHardwareToAddSearch();" Style="display: none" />
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="panelHardwareList_AgencyToDeallocate" runat="server" Style="margin-top: 10px;">
                    <asp:Repeater ID="rptMpadList_AgencyToDeallocate" runat="server">
                        <HeaderTemplate>
                            <table style="background-color: #fff; width: 100%; box-shadow: 0px 1px 2px #888; padding: 5px; border-spacing: 0;">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>N&deg; série</th>
                                        <th>Statut</th>
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr class='<%# (Container.ItemIndex+1).ToString() == hfHardwareListLength_AgencyToDeallocate.Value ? "tr-no-border" : "tr-border" %>'
                                onclick="showConfirmHardwareReallocate('<%# Eval("MP_Serial") %>', '<%= hfAgencyID_AgencyToDeallocate_selected.Value %>','<%= hfAgencyName_AgencyToDeallocate_selected.Value %>', '<%= hfAgencyName_selected.Value %>','<%= hfAgencyID_selected.Value %>', '<%= hfRefPartner_selected.Value %>', 'add')">
                                <td style="text-align: center; vertical-align: middle; padding: 5px; width: 30px;">
                                    <asp:HiddenField ID="hfMpadRef" runat="server" Value='<%# Eval("MP_Ref") %>' />
                                    <img src="Styles/Img/Mpad.png" alt="" style="height: 25px;" />
                                </td>
                                <td style="vertical-align: middle; padding: 5px; text-align: center;">
                                    <%# Eval("MP_Serial") %>
                                </td>
                                <td style="vertical-align: middle; padding: 5px; text-align: center; width: 100px">
                                    <%# Agency.getMpadStatus(Eval("MP_sInitStatus").ToString(),Eval("MP_sInitStatusColor").ToString()) %>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <asp:Repeater ID="rptPosList_AgencyToDeallocate" runat="server">
                        <HeaderTemplate>
                            <table style="background-color: #fff; width: 100%; box-shadow: 0px 1px 2px #888; padding: 5px; border-spacing: 0;">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>N&deg; série</th>
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr class='<%# (Container.ItemIndex+1).ToString() == hfHardwareListLength_AgencyToDeallocate.Value ? "tr-no-border" : "tr-border" %>'
                                onclick="showConfirmHardwareReallocate('<%# Eval("PS_Serial") %>', '<%= hfAgencyID_AgencyToDeallocate_selected.Value %>','<%= hfAgencyName_AgencyToDeallocate_selected.Value %>', '<%= hfAgencyName_selected.Value %>','<%= hfAgencyID_selected.Value %>', '<%= hfRefPartner_selected.Value %>', 'add')">
                                <td style="text-align: center; vertical-align: middle; padding: 5px; width: 30px;">
                                    <asp:HiddenField ID="hfPosRef" runat="server" Value='<%# Eval("PS_Ref") %>' />
                                    <img src='Styles/Img/<%# Agency.getPosImageName(Agency.getPosModelName(Eval("PS_Serial").ToString())) %>.png' alt="" style="height: 25px;" />
                                </td>
                                <td style="vertical-align: middle; padding: 5px; text-align: center;">
                                    <%# Eval("PS_Serial") %>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <asp:Repeater ID="rptTabletList_AgencyToDeallocate" runat="server">
                        <HeaderTemplate>
                            <table style="background-color: #fff; width: 100%; box-shadow: 0px 1px 2px #888; padding: 5px; border-spacing: 0;">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>N&deg; série</th>
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr class='<%# (Container.ItemIndex+1).ToString() == hfHardwareListLength_AgencyToDeallocate.Value ? "tr-no-border" : "tr-border" %>'
                                onclick="showConfirmHardwareReallocate('<%# Eval("TAB_Serial") %>', '<%= hfAgencyID_AgencyToDeallocate_selected.Value %>','<%= hfAgencyName_AgencyToDeallocate_selected.Value %>', '<%= hfAgencyName_selected.Value %>','<%= hfAgencyID_selected.Value %>', '<%= hfRefPartner_selected.Value %>', 'add')">
                                <td style="text-align: center; vertical-align: middle; padding: 5px; width: 30px;">
                                    <asp:HiddenField ID="hfTabRef" runat="server" Value='<%# Eval("TAB_Ref") %>' />
                                    <img src='Styles/Img/Tablet.png' alt="" style="height: 25px;" />
                                </td>
                                <td style="vertical-align: middle; padding: 5px; text-align: center;">
                                    <%# Eval("TAB_Serial") %>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <asp:Repeater ID="rptTabletSupportList_AgencyToDeallocate" runat="server">
                        <HeaderTemplate>
                            <table style="background-color: #fff; width: 100%; box-shadow: 0px 1px 2px #888; padding: 5px; border-spacing: 0;">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>N&deg; série</th>
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr class='<%# (Container.ItemIndex+1).ToString() == hfHardwareListLength_AgencyToDeallocate.Value ? "tr-no-border" : "tr-border" %>'
                                onclick="showConfirmHardwareReallocate('<%# Eval("PDB_Serial") %>', '<%= hfAgencyID_AgencyToDeallocate_selected.Value %>','<%= hfAgencyName_AgencyToDeallocate_selected.Value %>', '<%= hfAgencyName_selected.Value %>','<%= hfAgencyID_selected.Value %>', '<%= hfRefPartner_selected.Value %>', 'add')">
                                <td style="text-align: center; vertical-align: middle; padding: 5px; width: 30px;">
                                    <asp:HiddenField ID="hfTabSupportRef" runat="server" Value='<%# Eval("PDB_Ref") %>' />
                                    <img src='Styles/Img/<%# Agency.getTabletSupportImageName(Agency.getTabletSupportModelName(Eval("PDB_Serial").ToString())) %>.png' alt="" style="height: 50px;" />
                                </td>
                                <td style="vertical-align: middle; padding: 5px; text-align: center;">
                                    <%# Eval("PDB_Serial") %>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <asp:Repeater ID="rptScannerList_AgencyToDeallocate" runat="server">
                        <HeaderTemplate>
                            <table style="background-color: #fff; width: 100%; box-shadow: 0px 1px 2px #888; padding: 5px; border-spacing: 0;">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>N&deg; série</th>
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr class='<%# (Container.ItemIndex+1).ToString() == hfHardwareListLength_AgencyToDeallocate.Value ? "tr-no-border" : "tr-border" %>'
                                onclick="showConfirmHardwareReallocate('<%# Eval("SCN_Serial") %>', '<%= hfAgencyID_AgencyToDeallocate_selected.Value %>','<%= hfAgencyName_AgencyToDeallocate_selected.Value %>', '<%= hfAgencyName_selected.Value %>','<%= hfAgencyID_selected.Value %>', '<%= hfRefPartner_selected.Value %>', 'add')">
                                <td style="text-align: center; vertical-align: middle; padding: 5px; width: 30px;">
                                    <asp:HiddenField ID="hfScannerRef" runat="server" Value='<%# Eval("SCN_Ref") %>' />
                                    <img src='Styles/Img/<%# Agency.getScannerImageName(Agency.getScannerModelName(Eval("SCN_FreeLabel").ToString())) %>.png' alt="" style="height: 25px;" />
                                </td>
                                <td style="vertical-align: middle; padding: 5px; text-align: center;">
                                    <%# Eval("SCN_Serial") %>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <asp:Repeater ID="rptPinpadList_AgencyToDeallocate" runat="server">
                        <HeaderTemplate>
                            <table style="background-color: #fff; width: 100%; box-shadow: 0px 1px 2px #888; padding: 5px; border-spacing: 0;">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>N&deg; série</th>
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr class='<%# (Container.ItemIndex+1).ToString() == hfHardwareListLength_AgencyToDeallocate.Value ? "tr-no-border" : "tr-border" %>'
                                onclick="showConfirmHardwareReallocate('<%# Eval("PPD_Serial") %>', '<%= hfAgencyID_AgencyToDeallocate_selected.Value %>','<%= hfAgencyName_AgencyToDeallocate_selected.Value %>', '<%= hfAgencyName_selected.Value %>','<%= hfAgencyID_selected.Value %>', '<%= hfRefPartner_selected.Value %>', 'add')">
                                <td style="text-align: center; vertical-align: middle; padding: 5px; width: 30px;">
                                    <asp:HiddenField ID="hfPinpadRef" runat="server" Value='<%# Eval("PPD_Ref") %>' />
                                    <img src='Styles/Img/pinpad_ipp310.png' alt="" style="height: 25px;" />
                                </td>
                                <td style="vertical-align: middle; padding: 5px; text-align: center;">
                                    <%# Eval("PPD_Serial") %>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <asp:HiddenField ID="hfHardwareListLength_AgencyToDeallocate" runat="server" Value="0" />
                    <asp:HiddenField ID="hfHardwareSerialList_AgencyToDeallocate" runat="server" Value="" />
                    <asp:Panel ID="panelHardwareListEmpty_AgencyToDeallocate" runat="server" Visible="false">
                        <asp:Label ID="lblHardwareListEmpty_AgencyToDeallocate" runat="server">Aucune borne affectée.</asp:Label>
                    </asp:Panel>
                </asp:Panel>
                <asp:Button ID="btnRefreshAgencyDetails_AgencyToDeallocate" runat="server" OnClick="btnRefreshAgencyDetails_AgencyToDeallocate_Click" Style="display: none" />
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnRefreshAgencyDetails_AgencyToDeallocate" EventName="click" />
                <asp:AsyncPostBackTrigger ControlID="btnHardwareToAddSearch" EventName="click" />
            </Triggers>
        </asp:UpdatePanel>
        <asp:UpdateProgress ID="upAgencyDetails_AgencyToDeallocateLoading" runat="server" AssociatedUpdatePanelID="upAgencyDetails_AgencyToDeallocate">
            <ProgressTemplate>
                <div style="text-align: center; position: absolute; left: 45%; top: 50%">
                    <img src="../Styles/Img/loading.gif" alt="loading..." />
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </asp:Panel>

    <div id="divMpadDetail-popup" style="display: none">
        <asp:UpdatePanel ID="upMpadDetail" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <script>
                    function ShowMpadReinitCodeLoading() {
                        $('#<%=panelMpadDetails.ClientID%>').hide();
                        $('#<%=panelMpadReinitCodeResult.ClientID%>').hide();
                        $('#<%=panelMpadReinitCodeLoading.ClientID%>').show();

                        return true;
                    }
                    function HideMpadReinitCodeLoading() {
                        $('#<%=panelMpadReinitCodeLoading.ClientID%>').hide();
                    }
                </script>
                <asp:Panel ID="panelMpadDetails" runat="server">
                    <div id="divMpadDetails">
                        <div class="table">
                            <div class="table-row">
                                <div class="table-cell" style="vertical-align: middle;">
                                    <img src="Styles/Img/Mpad.png" style="max-height: 70px; max-width: 70px;" />
                                </div>
                                <div class="table-cell" style="padding-left: 10px; vertical-align: middle">
                                    <div id="divMpadSerial">
                                        S/N :
                                        <span id="sMpadSerial" class="font-orange" style="font-weight: bold"></span>
                                    </div>
                                    <div id="divMpadStatus" style="margin-top: 5px;">
                                        Statut :
                                        <span id="sMpadStatus" style="font-weight: bold"></span>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div id="divMpadRelease">
                            Logiciel principal :
                            <span id="sMpadRelease" class="font-orange" style="font-weight: bold"></span>
                        </div>
                        <div id="divAbmSoftRelease">
                            Logiciel associé :
                            <span id="sAbmSoftRelease" class="font-orange" style="font-weight: bold"></span>
                        </div>
                        <div id="divMpadInitStatus" style="display: none"></div>
                        <div id="divMpadCode" style="margin-top: 10px;">
                            <div>
                                Code initialisation :
                                <span id="sMpadCode" class="font-orange" style="font-weight: bold"></span>
                            </div>
                            <span id="sMpadCodeDate"></span>
                        </div>
                        <div id="divMpadLastCnxDate" style="margin-top: 5px;">
                            <div>Dernière connexion :</div>
                            <span id="sMpadLastCnxDate" class="font-orange" style="font-weight: bold"></span>
                        </div>
                        <div id="divMpadRemoteAccessRequested" style="margin-top: 5px; display: none">
                            <div>Remote access demandé :</div>
                            <span id="sMpadRemoteAccessRequested" class="font-orange" style="font-weight: bold"></span>
                        </div>
                        <div style="margin-top: 20px; text-align: center;">
                            <div style="display:inline-block">
                                <div class="table">
                                    <div class="table-row">
                                        <div class="table-cell" style="width:50%;text-align:right;padding-right:10px">
                                            <asp:Button ID="btnInitCode" runat="server" OnClientClick="showConfirmReinitCode(); return false;" Text="Réinit. code"
                                                CssClass="button" Style="margin-bottom: 0; font-family: Arial; font-size: 0.8em" />
                                        </div>
                                        <div class="table-cell" style="width:50%;text-align:left;padding-left:10px">
                                            <asp:Button ID="btnMpadDeallocate" runat="server" Text="Désaffecter" CssClass="button"
                                                OnClientClick="showSearchAgencyToAllocate(); return false;" Style="margin-bottom: 0; font-family: Arial; font-size: 0.8em" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div id="divMpadReinitCode" style="display: none">
                        <div>Voulez-vous vraiment générer un nouveau code d'initialisation pour cette borne ?</div>
                        <div style="color: red; font-size: 0.8em; font-weight: bold; margin-top: 5px;">Attention! Cette action va rendre la borne inutilisable tant que le nouveau code ne sera pas saisi sur la borne.</div>
                        <div style="margin-top: 20px; text-align: center;">
                        <div style="display:inline-block">
                            <div class="table" >
                                <div class="table-row">
                                    <div class="table-cell" style="width:50%;text-align:right;padding-right:10px">
                                        <input type="button" value="Annuler" class="button" onclick="hideConfirmReinitCode();"
                                            style="margin-bottom: 0; font-family: Arial; font-size: 0.8em" />
                                    </div>
                                    <div class="table-cell" style="width:50%;text-align:left;padding-left:10px">
                                        <asp:Button ID="btnConfirmInitCode" runat="server" OnClientClick="return ShowMpadReinitCodeLoading();" OnClick="btnInitCode_Click" Text="Valider" CssClass="button"
                                            Style="margin-bottom: 0; font-family: Arial; font-size: 0.8em" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="panelMpadReinitCodeResult" runat="server">
                    <div>
                        <asp:Label ID="lblMpadReinitCodeResult" runat="server"></asp:Label>
                    </div>
                    <asp:Panel ID="panelMpadReinitCodeResult_Code" runat="server" Visible="false" Style="margin-top: 10px">
                        Veuillez saisir le code suivant sur la borne :
                        <div style="text-align: center; font-weight: bold; font-size: 1.4em;">
                            <asp:Label ID="lblMpadReinitCodeResult_Code" runat="server"></asp:Label>
                        </div>
                    </asp:Panel>
                </asp:Panel>
                <asp:Panel ID="panelMpadReinitCodeLoading" runat="server" style="display:none; margin-top:20px;">
                    <div style="text-align: center; vertical-align:middle ">
                        <img src="../Styles/Img/loading.gif" alt="loading..." />
                    </div>
                </asp:Panel>
                <asp:HiddenField ID="hfMpadReinitCode" runat="server" />
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnConfirmInitCode" EventName="click" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <div id="divHardwareReallocate-popup" style="display: none">
        <asp:UpdatePanel ID="upHardwareReallocate" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <script>
                    function ShowHardwareReallocateLoading() {
                        $('#<%=panelHardwareReallocate.ClientID%>').hide();
                        $('#<%=panelHardwareReallocateComment.ClientID%>').hide();
                        $('#<%=panelHardwareReallocateConfirm.ClientID%>').hide();
                        $('#<%=panelHardwareReallocateResult.ClientID%>').hide();

                        $('#<%=panelHardwareReallocateLoading.ClientID%>').show();
                    }
                    function HideHardwareReallocateLoading() {
                        $('#<%=panelHardwareReallocateLoading.ClientID%>').hide();
                    }
                </script>

                <div style="font-family: Arial">
                    <asp:Panel ID="panelHardwareReallocate" runat="server">
                        <div style="text-align: left; width: 100%; margin: 10px 0; padding-bottom: 5px; font-size: 0.8em;">
                            S/N : <span id="sHardwareReallocateSerial" style="font-weight: bold;"></span>
                        </div>
                        <div class="table" style="width: 100%">
                            <div class="table-row" style="border-bottom: 1px solid #ff6a00; border-top: 1px solid #ff6a00">
                                <div class="table-cell" style="font-size: 0.8em; font-weight: bold; text-align: center; padding:5px 0; width: 45%">
                                    Agence de
                                    <br />
                                    départ
                                </div>
                                <div class="table-cell" style="width: 15%">
                                </div>
                                <div class="table-cell" style="font-size: 0.8em; font-weight: bold; text-align: center; padding-bottom:5px 0; width: 45%;">
                                    Agence de
                                    <br />
                                    destination
                                </div>
                            </div>
                            <div class="table-row">
                                <div class="table-cell" style="vertical-align: middle; text-align: left; font-size: 0.8em; width: 45%">
                                    <span id="sHardwareAgencySource"></span>
                                </div>
                                <div class="table-cell" style="padding: 10px; vertical-align: middle; text-align: center; width: 15%">
                                    <div>
                                        <div>
                                            <img id="imgHardwareReallocate" src="Styles/Img/Mpad.png" style="max-height: 30px; max-width: 30px;" />
                                        </div>
                                        <div>
                                            <img src="Styles/Img/right_arrow.png" style="max-height: 30px; max-width: 30px;" />
                                        </div>
                                    </div>
                                </div>

                                <div class="table-cell" style="padding-left: 10px; vertical-align: middle; font-size: 0.8em; text-align: right; width: 45%">
                                    <span id="sHardwareAgencyDestination"></span>
                                </div>
                            </div>
                        </div>
                         <div style="margin-top: 20px; text-align: center;">
                            <div style="display:inline-block">
                                <div class="table">
                                    <div class="table-row">
                                        <div class="table-cell" style="text-align:right;padding-right:10px">
                                            <input type="button" id="btnHardwareReallocateCancel" class="button" style="margin-bottom: 0; font-family: Arial; font-size: 0.8em" value="Annuler" onclick="HardwareReallocateCancel();" />
                                        </div>
                                        <div class="table-cell" style="text-align:left;padding-left:10px">
                                            <input type="button" id="btnHardwareReallocate" class="button" style="margin-bottom: 0; font-family: Arial; font-size: 0.8em" value="Continuer" onclick="HardwareReallocateComment();" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="panelHardwareReallocateComment" runat="server" style="display:none">
                        <div>Voulez-vous commenter ce changement d'affectation ?</div>
                        <div class="table" style="margin:5px 0 0 0;width:100%; padding:0;">
                            <div class="table-row">
                                <div class="table-cell" style="width:100%; vertical-align:bottom">
                                    Commentaire(s) <label id="lblNbChar">0</label>/
                                    <asp:Label ID="lblNbMaxCheckComment" runat="server" Text="100"></asp:Label> :
                                </div>
                                <div class="table-cell" style="text-align:right; vertical-align:bottom">
                                    <input type="button" class="button" value="effacer" onclick="clearComment();" style="font-size:12px;padding:2px 5px 0 5px; margin:0; height:20px; font-family:Arial" />
                                </div>
                            </div>
                        </div>
                        <div>
                            <script>
                                function MaxLength(text, maxlength) {
                                    //asp.net textarea maxlength doesnt work; do it by hand
                                    //var maxlength = 2000; //set your value here (or add a parm and pass it in)
                                    if(maxlength == null) {
                                        maxlength = $('#<%=lblNbMaxCheckComment.ClientID%>').html();
                                    }

                                    var object = document.getElementById(text.id)  //get your object
                                    if (object.value.length > maxlength) {
                                        object.focus(); //set focus to prevent jumping
                                        object.value = text.value.substring(0, maxlength); //truncate the value
                                        object.scrollTop = object.scrollHeight; //scroll to the end to prevent jumping
                                        $('#lblNbChar').css('color', '#FF0000');
                                        return false;
                                    }
                                    else if (object.value.length == maxlength) {
                                        $('#lblNbChar').css('color', '#FF0000');
                                    }
                                    else {
                                        $('#lblNbChar').css('color', '#344B56');
                                    }

                                    $('#lblNbChar').html(object.value.length);
                                    return true;
                                }

                                function clearComment() {
                                    $('#<%=txtHardwareReallocateComment.ClientID%>').val("");
                                    $('#lblNbChar').html("0");
                                }
                            </script>
                            <asp:TextBox ID="txtHardwareReallocateComment" MaxLength="100" runat="server" TextMode="MultiLine" autocomplete="off"
                                onKeyUp="javascript:MaxLength(this);" onChange="javascript:MaxLength(this);"
                                style="max-width:100%; min-width:100%; min-height:100px; max-height:100px"></asp:TextBox>
                        </div>
                        <div style="margin-top: 20px; text-align: center;">
                            <div style="display:inline-block">
                                <div class="table">
                                    <div class="table-row">
                                        <div class="table-cell" style="text-align:right;padding-right:10px">
                                            <input type="button" id="btnHardwareReallocateCommentCancel" class="button" style="margin-bottom: 0; font-family: Arial; font-size: 0.8em" value="Annuler" onclick="HardwareReallocateCancel();" />
                                        </div>
                                        <div class="table-cell" style="text-align:left;padding-left:10px;">
                                            <input type="button" id="btnHardwareReallocateComment" class="button" style="margin-bottom: 0; font-family: Arial; font-size: 0.8em" value="Continuer" onclick="HardwareReallocate();" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="panelHardwareReallocateConfirm" runat="server" Style="display: none">
                        <div id="divHardwareReallocateConfirmMessage">Voulez-vous vraiment désaffecter ce matériel ?</div>
                        <div id="divHardwareReallocateWarning" style="color: red; font-size: 0.8em; font-weight: bold; margin-top: 5px;">Attention! Cette action va rendre la borne inutilisable tant que le nouveau code ne sera pas saisi sur la borne.</div>
                        <div style="margin-top: 20px; text-align: center;">
                            <div style="display:inline-block">
                                <div class="table">
                                    <div class="table-row">
                                        <div class="table-cell" style="text-align:right;padding-right:10px">
                                            <input type="button" id="btnCancelHardwareAllocate" class="button" style="margin-bottom: 0; font-family: Arial; font-size: 0.8em" value="Annuler" onclick="CancelHardwareAllocate();" />
                                        </div>
                                        <div class="table-cell" style="text-align:left;padding-left:10px;">
                                            <asp:Button ID="btnConfirmHardwareAllocate" runat="server" OnClientClick="ShowHardwareReallocateLoading();" OnClick="btnConfirmHardwareAllocate_Click" Text="Valider"
                                                Style="margin-bottom: 0; font-family: Arial; font-size: 0.8em" CssClass="button" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="panelHardwareReallocateResult" runat="server" Style="display: none">
                        <div>
                            <asp:Label ID="lblHardwareReallocateResult" runat="server"></asp:Label>
                        </div>
                        <asp:Panel ID="panelMpadReallocate_InitCode" runat="server" Visible="false" Style="margin-top: 10px;">
                            Veuillez saisir le code suivant sur la borne :
                            <div style="text-align: center; font-weight: bold; font-size: 1.4em;">
                                <asp:Label ID="lblMpadReallocate_InitCode" runat="server"></asp:Label>
                            </div>

                        </asp:Panel>
                        <asp:HiddenField ID="hfReload" runat="server" />
                    </asp:Panel>
                    <asp:Panel ID="panelHardwareReallocateLoading" runat="server" style="display:none; margin-top:20px;">
                        <div style="text-align: center; vertical-align:middle ">
                            <img src="../Styles/Img/loading.gif" alt="loading..." />
                        </div>
                    </asp:Panel>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnConfirmHardwareAllocate" EventName="click" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <div id="divTabletDetail-popup" style="display: none">
        <div id="divTabDetails">
            <div class="table">
                <div class="table-row">
                    <div class="table-cell" style="vertical-align: middle;">
                        <img id="imgTabDetails" src="Styles/Img/Tablet.png" alt="" style="max-height: 70px; max-width: 50px;" />
                    </div>
                    <div class="table-cell" style="padding-left: 10px; vertical-align: top">
                        <div id="divTabType">
                            Marque :
                            <span id="sTabType" class="font-orange" style="font-weight: bold; text-transform: uppercase">ACER</span>
                        </div>
                        <div id="divTabModel">
                            Modèle :
                            <span id="sTabModel" class="font-orange" style="font-weight: bold; text-transform: uppercase; font-size:0.7em">Aspire Switch 10 E</span>
                        </div>
                        <%--<div id="divTabSerial">
                            S/N :
                            <span id="sTabSerial" class="font-orange" style="font-weight: bold; font-size:0.8em;"></span>
                        </div>--%>
                        <%--<div id="divTabRelease">
                            Version logiciel :
                            <span id="sTabRelease" class="font-orange" style="font-weight: bold"></span>
                        </div>
                        <div id="divTabStatus" style="display: none">
                            Statut :
                            <span id="sTabStatus" class="font-orange" style="font-weight: bold"></span>
                        </div>--%>
                    </div>
                </div>
            </div>
            <div id="divTabSerial" style="margin-top: 5px;">
                S/N :
                <span id="sTabSerial" class="font-orange" style="font-weight: bold; font-size:0.9em;"></span>
            </div>
            <%--<div id="divTabLastCnxDate" style="margin-top: 5px;">
                <div>Dernière connexion :</div>
                <span id="sTabLastCnxDate" class="font-orange" style="font-weight: bold"></span>
            </div>--%>
            <div style="width: 100%; text-align: center; margin-top: 20px; margin-bottom: 0;">
                <asp:Button ID="btnTabDeallocate" runat="server" Text="Désaffecter" CssClass="button"
                    OnClientClick="showSearchAgencyToAllocate('TAB'); return false;" Style="margin-bottom: 0; font-family: Arial; font-size: 0.8em" />
            </div>
        </div>
    </div>
    <div id="divTabletSupportDetail-popup" style="display: none">
        <div id="divTabSupportDetails">
            <div class="table">
                <div class="table-row">
                    <div class="table-cell" style="vertical-align: top;">
                        <img id="imgTabSupportDetails" src="Styles/Img/TabletSupportLight.png" alt="" style="max-height: 70px; max-width: 50px;" />
                    </div>
                    <div class="table-cell" style="padding-left: 10px; vertical-align: top">
                        <div id="divTabSupportType">
                            Type :
                            <span id="sTabSupportType" class="font-orange" style="font-weight: bold; text-transform: uppercase">AVEC PIED</span>
                        </div>
                        <div id="divTabSupportSerial" style="margin-top: 5px;">
                            S/N :
                            <span id="sTabSupportSerial" class="font-orange" style="font-weight: bold; font-size:0.9em;"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div style="width: 100%; text-align: center; margin-top: 20px; margin-bottom: 0;">
                <asp:Button ID="btnTabSupportDeallocate" runat="server" Text="Désaffecter" CssClass="button"
                    OnClientClick="showSearchAgencyToAllocate('PDB'); return false;" Style="margin-bottom: 0; font-family: Arial; font-size: 0.8em" />
            </div>
        </div>
    </div>
    <div id="divScannerDetail-popup" style="display: none">
        <div id="divScnDetails">
            <div class="table">
                <div class="table-row">
                    <div class="table-cell" style="vertical-align: middle;">
                        <img id="imgScnDetails" src="Styles/Img/Scanner.png" alt="" style="max-height: 70px; max-width: 70px;" />
                    </div>
                    <div class="table-cell" style="padding-left: 10px; vertical-align: top">
                        <div id="divScnType">
                            Marque :
                            <span id="sScnType" class="font-orange" style="font-weight: bold; text-transform: uppercase">AVA5 Plus</span>
                        </div>
                        <div id="divScnSerial">
                            S/N :
                            <span id="sScnSerial" class="font-orange" style="font-weight: bold; font-size:0.9em;"></span>
                        </div>
                        <%--<div id="divScnRelease">
                            Version logiciel :
                            <span id="sScnRelease" class="font-orange" style="font-weight: bold"></span>
                        </div>
                        <div id="divScnStatus" style="display: none">
                            Statut :
                            <span id="sScnStatus" class="font-orange" style="font-weight: bold"></span>
                        </div>--%>
                    </div>
                </div>
            </div>
            <%--<div id="divScnSerial" style="margin-top: 5px;">
                S/N :
                <span id="sScnSerial" class="font-orange" style="font-weight: bold"></span>
            </div>--%>
            <%--<div id="divScntLastCnxDate" style="margin-top: 5px;">
                <div>Dernière connexion :</div>
                <span id="sScnLastCnxDate" class="font-orange" style="font-weight: bold"></span>
            </div>--%>
            <div style="width: 100%; text-align: center; margin-top: 20px; margin-bottom: 0;">
                <asp:Button ID="btnScnDeallocate" runat="server" Text="Désaffecter" CssClass="button"
                    OnClientClick="showSearchAgencyToAllocate('SCN'); return false;" Style="margin-bottom: 0; font-family: Arial; font-size: 0.8em" />
            </div>
        </div>
    </div>
    <div id="divPinpadDetail-popup" style="display: none">
        <div id="divPpdDetails">
            <div class="table">
                <div class="table-row">
                    <div class="table-cell" style="vertical-align: middle;">
                        <img id="imgPpdDetails" src="Styles/Img/pinpad_ipp310.png" alt="" style="max-height: 70px; max-width: 70px;" />
                    </div>
                    <div class="table-cell" style="padding-left: 10px; vertical-align: top">
                        <div id="divPpdType">
                            Marque :
                            <span id="sPpdType" class="font-orange" style="font-weight: bold; text-transform: uppercase">Ingenico</span>
                        </div>

                        <%--<div id="divPpdRelease">
                            Version logiciel :
                            <span id="sPpdRelease" class="font-orange" style="font-weight: bold"></span>
                        </div>
                        <div id="divPpdStatus" style="display: none">
                            Statut :
                            <span id="sPpdStatus" class="font-orange" style="font-weight: bold"></span>
                        </div>--%>
                    </div>
                </div>
            </div>
            <div id="divPpdSerial" style="margin-top:5px;">
                S/N :
                <span id="sPpdSerial" class="font-orange" style="font-weight: bold"></span>
            </div>
            <%--<div id="divPpdtLastCnxDate" style="margin-top: 5px;">
                <div>Dernière connexion :</div>
                <span id="sPpdLastCnxDate" class="font-orange" style="font-weight: bold"></span>
            </div>--%>
            <div style="width: 100%; text-align: center; margin-top: 20px; margin-bottom: 0;">
                <asp:Button ID="btnPpdDeallocate" runat="server" Text="Désaffecter" CssClass="button"
                    OnClientClick="showSearchAgencyToAllocate('PPD'); return false;" Style="margin-bottom: 0; font-family: Arial; font-size: 0.8em" />
            </div>
        </div>
    </div>
    <div id="divPosDetail-popup" style="display: none">
        <div id="divPosDetails">
            <div class="table">
                <div class="table-row">
                    <div class="table-cell" style="vertical-align: middle;">
                        <img id="imgPosDetails" src="Styles/Img/Ingenico.png" alt="" style="max-height: 70px; max-width: 70px;" />
                    </div>
                    <div class="table-cell" style="padding-left: 10px; vertical-align: top">
                        <div id="divPosType">
                            Marque :
                            <span id="sPosType" class="font-orange" style="font-weight: bold; text-transform: uppercase"></span>
                        </div>
                        <div id="divPosSerial">
                            S/N :
                            <span id="sPosSerial" class="font-orange" style="font-weight: bold"></span>
                        </div>
                        <div id="divPosRelease">
                            Version logiciel :
                            <span id="sPosRelease" class="font-orange" style="font-weight: bold"></span>
                        </div>
                        <div id="divPosStatus" style="display: none">
                            Statut :
                            <span id="sPosStatus" class="font-orange" style="font-weight: bold"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div id="divPostLastCnxDate" style="margin-top: 5px;">
                <div>Dernière connexion :</div>
                <span id="sPosLastCnxDate" class="font-orange" style="font-weight: bold"></span>
            </div>
            <div style="width: 100%; text-align: center; margin-top: 20px; margin-bottom: 0;">
                <asp:Button ID="btnPosDeallocate" runat="server" Text="Désaffecter" CssClass="button"
                    OnClientClick="showSearchAgencyToAllocate('POS'); return false;" Style="margin-bottom: 0; font-family: Arial; font-size: 0.8em" />
            </div>
        </div>
    </div>
    <div id="divAlert-popup">
        <div id="divAlert-message"></div>
    </div>

    <asp:HiddenField ID="hfPanelActive" runat="server" />
    <asp:HiddenField ID="hfRefPartner_selected" runat="server" />
    <asp:HiddenField ID="hfRefAgency_selected" runat="server" />
    <asp:HiddenField ID="hfAgencyID_selected" runat="server" />
    <asp:HiddenField ID="hfAgencyName_selected" runat="server" />
    <asp:HiddenField ID="hfManagerLastName_selected" runat="server" />
    <asp:HiddenField ID="hfManagerFirstName_selected" runat="server" />
    <asp:HiddenField ID="hfAgencyAddress_selected" runat="server" />
    <asp:HiddenField ID="hfRefPartner_AgencyToDeallocate_selected" runat="server" />
    <asp:HiddenField ID="hfRefAgency_AgencyToDeallocate_selected" runat="server" />
    <asp:HiddenField ID="hfAgencyID_AgencyToDeallocate_selected" runat="server" />
    <asp:HiddenField ID="hfAgencyName_AgencyToDeallocate_selected" runat="server" />
    <asp:HiddenField ID="hfManagerLastName_AgencyToDeallocate_selected" runat="server" />
    <asp:HiddenField ID="hfManagerFirstName_AgencyToDeallocate_selected" runat="server" />
    <asp:HiddenField ID="hfHardwareAllocateDirection" runat="server" />
    <asp:HiddenField ID="hfHardwareSerial_selected" runat="server" />
    <asp:HiddenField ID="hfHardwareType_selected" runat="server" />
    <asp:HiddenField ID="hfRefPartner_destination" runat="server" />
    <asp:HiddenField ID="hfAgencyID_destination" runat="server" />
    <asp:HiddenField ID="hfAgencyID_source" runat="server" />
    <asp:HiddenField ID="hfMpadSerialList" runat="server" Value="" />
    <asp:HiddenField ID="hfPosSerialList" runat="server" Value="" />
    <asp:HiddenField ID="hfTabletSerialList" runat="server" Value="" />
    <asp:HiddenField ID="hfTabletSupportSerialList" runat="server" Value="" />
    <asp:HiddenField ID="hfScannerSerialList" runat="server" Value="" />
    <asp:HiddenField ID="hfPinpadSerialList" runat="server" Value="" />
</asp:Content>
<script runat="server">
    protected override void OnPreInit(EventArgs e)
    {
         base.OnPreInit(e);

         if (Request.UserAgent != null &&Request.UserAgent.IndexOf("AppleWebKit", StringComparison.CurrentCultureIgnoreCase) > -1)
         {
              this.ClientTarget = "uplevel";
         }
    }
</script>
