﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class mobile_AgencySearch : System.Web.UI.Page
{
    int iMaxResult = 20;

    protected void Page_Load(object sender, EventArgs e)
    {
        System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("fr-FR");
        System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("fr-FR");

        txtSearchAgencyToAllocate.Text = "agence fpe";
        txtSearchAgencyToDeallocate.Text = "agence fpe";

        panelSearchResultNb.Visible = false;

        if (!IsPostBack)
        {
            hfReload.Value = "";
        }

        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "autocomplete_MpadSearch();autocomplete_PosSearch();autocomplete_TabletSearch();autocomplete_ScannerSearch();autocomplete_PinpadSearch();", true);
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        string sXmlOut = "";

        authentication auth = authentication.GetCurrent();
        string sSearchValue = txtSearch.Text;
        DataTable dtSearchResult = Agency.getAgencySearchResult(sSearchValue, iMaxResult, auth.sToken, out sXmlOut);

        rptSearchResult.DataSource = replaceValueInDatatable(dtSearchResult, "'", "&rsquo;");
        rptSearchResult.DataBind();

        string sNbResult = tools.GetValueFromXml(sXmlOut, "ALL_XML_OUT/H", "NbResult");
        int iSearchResultNb = 0;
        int.TryParse(sNbResult, out iSearchResultNb);

        if (iSearchResultNb == 0)
            lblSearchResultNb.Text = "Aucune agence trouvée.";
        else
        {
            if (iSearchResultNb > 1)
                lblSearchResultNb.Text = iSearchResultNb.ToString() + " agences trouvées.";
            else if (iSearchResultNb == -1) { lblSearchResultNb.Text = "Plus de 100 agences trouvées."; }
            else { lblSearchResultNb.Text = iSearchResultNb.ToString() + " agence trouvée."; }
        }
        panelSearchResultNb.Visible = true;

        if (iSearchResultNb > iMaxResult || iSearchResultNb == -1)
        {
            string sMessage = "Votre recherche a retourné trop de résultats.<br/> Seuls les " + iMaxResult.ToString() + " premiers sont affichés.";
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "agencySearchAlert('Veuillez affiner votre recherche','" + sMessage + "');", true);
        }
    }
    protected void btnRefreshAgencyDetails_Click(object sender, EventArgs e)
    {
        rptMpadList.Visible = true;
        rptPosList.Visible = true;
        rptScannerList.Visible = true;
        rptTabletList.Visible = true;
        rptPinpadList.Visible = true;
        panelMpadSearch.Visible = false;
        panelPosSearch.Visible = false;
        panelScannerSearch.Visible = false;
        panelTabletSearch.Visible = false;
        panelPinpadSearch.Visible = false;
        panelMpadListEmpty.Visible = false;
        panelPosListEmpty.Visible = false;
        panelScannerListEmpty.Visible = false;
        panelTabletListEmpty.Visible = false;
        panelPinpadListEmpty.Visible = false;
        hfMpadListLength.Value = "0";
        hfPosListLength.Value = "0";
        hfScannerListLength.Value="0";
        hfTabletListLength.Value = "0";
        hfPinpadListLength.Value = "0";

        lblAgencyID.Text = hfAgencyID_selected.Value;
        lblAgencyName.Text = hfAgencyName_selected.Value;
        lblManagerName.Text = hfManagerLastName_selected.Value + " " + hfManagerFirstName_selected.Value;

        string sZipCode = "", sCity = "", sAddress = "";
        if(hfAgencyAddress_selected.Value.Trim().Length > 0 && hfAgencyAddress_selected.Value.Trim().Split('|').Length >= 3)
        {
            sAddress = hfAgencyAddress_selected.Value.Trim().Split('|')[0].Trim();
            sCity = hfAgencyAddress_selected.Value.Trim().Split('|')[1].Trim();
            sZipCode = hfAgencyAddress_selected.Value.Trim().Split('|')[2].Trim();
        }
        lblAgencyAddress.Text = sCity + " " + sZipCode + "<br/>" + sAddress;

        //MPAD LIST
        setMpadList();

        //POS LIST
        setPosList();

        //SCANNER LIST
        setScannerList();

        //TABLET LIST
        setTabletList();

        //SUPPORT TABLET LIST
        setTabletSupportList();

        //PINPAD LIST
        setPinpadList();
    }
    protected void btnMpadSearch_Click(object sender, EventArgs e)
    {
        if (txtMpadSearch.Text.Trim().Length == 12)
        {

            panelMpadSearch.Visible = true;
            rptMpadList.Visible = true;
            panelMpadListEmpty.Visible = false;
            hfMpadListLength.Value = "0";

            lblAgencyID.Text = hfAgencyID_selected.Value;
            lblAgencyName.Text = hfAgencyName_selected.Value;
            lblManagerName.Text = hfManagerLastName_selected.Value + " " + hfManagerFirstName_selected.Value;

            authentication auth = authentication.GetCurrent();
            string sXmlOut = "";
            DataTable dtMpadList = Agency.getAgencyHwDetails("MPD",hfRefAgency_selected.Value, txtMpadSearch.Text, auth.sToken, out sXmlOut);

            int iNbResult = 0;
            int.TryParse(tools.GetValueFromXml(sXmlOut, "ALL_XML_OUT/H", "NbResult"), out iNbResult);

            if (iNbResult > 0)
            {
                panelMpadSearchAdd.Visible = false;
                hfMpadListLength.Value = dtMpadList.Rows.Count.ToString();
                rptMpadList.DataSource = dtMpadList;
                rptMpadList.DataBind();
            }
            else
            {
                panelMpadSearchAdd.Visible = true;
                rptMpadList.Visible = false;
                panelMpadListEmpty.Visible = true;
            }
        }
    }
    protected void btnPosSearch_Click(object sender, EventArgs e)
    {
        if (txtPosSearch.Text.Trim().Length >= 8)
        {

            panelPosSearch.Visible = true;
            rptPosList.Visible = true;
            panelPosListEmpty.Visible = false;
            hfPosListLength.Value = "0";

            lblAgencyID.Text = hfAgencyID_selected.Value;
            lblAgencyName.Text = hfAgencyName_selected.Value;
            lblManagerName.Text = hfManagerLastName_selected.Value + " " + hfManagerFirstName_selected.Value;

            authentication auth = authentication.GetCurrent();
            string sXmlOut = "";
            DataTable dtPosList = Agency.getAgencyHwDetails("POS",hfRefAgency_selected.Value, txtPosSearch.Text, auth.sToken, out sXmlOut);

            int iNbResult = 0;
            int.TryParse(tools.GetValueFromXml(sXmlOut, "ALL_XML_OUT/H", "NbResult"), out iNbResult);

            if (iNbResult > 0)
            {
                panelPosSearchAdd.Visible = false;
                hfPosListLength.Value = dtPosList.Rows.Count.ToString();
                rptPosList.DataSource = dtPosList;
                rptPosList.DataBind();
            }
            else
            {
                panelPosSearchAdd.Visible = true;
                rptPosList.Visible = false;
                panelPosListEmpty.Visible = true;
            }
        }
    }
    protected void btnScannerSearch_Click(object sender, EventArgs e)
    {
        if (txtScannerSearch.Text.Trim().Length > 0)
        {

            panelScannerSearch.Visible = true;
            rptScannerList.Visible = true;
            panelScannerListEmpty.Visible = false;
            hfScannerListLength.Value = "0";

            lblAgencyID.Text = hfAgencyID_selected.Value;
            lblAgencyName.Text = hfAgencyName_selected.Value;
            lblManagerName.Text = hfManagerLastName_selected.Value + " " + hfManagerFirstName_selected.Value;

            authentication auth = authentication.GetCurrent();
            string sXmlOut = "";
            DataTable dtScannerList = Agency.getAgencyHwDetails("SCN", hfRefAgency_selected.Value, txtScannerSearch.Text, auth.sToken, out sXmlOut);

            int iNbResult = 0;
            int.TryParse(tools.GetValueFromXml(sXmlOut, "ALL_XML_OUT/H", "NbResult"), out iNbResult);

            if (iNbResult > 0)
            {
                panelScannerSearchAdd.Visible = false;
                hfScannerListLength.Value = dtScannerList.Rows.Count.ToString();
                rptScannerList.DataSource = dtScannerList;
                rptScannerList.DataBind();
            }
            else
            {
                panelScannerSearchAdd.Visible = true;
                rptScannerList.Visible = false;
                panelScannerListEmpty.Visible = true;
            }
        }
    }
    protected void btnTabletSearch_Click(object sender, EventArgs e)
    {
        if (txtTabletSearch.Text.Trim().Length > 0)
        {

            panelTabletSearch.Visible = true;
            rptTabletList.Visible = true;
            panelTabletListEmpty.Visible = false;
            hfTabletListLength.Value = "0";

            lblAgencyID.Text = hfAgencyID_selected.Value;
            lblAgencyName.Text = hfAgencyName_selected.Value;
            lblManagerName.Text = hfManagerLastName_selected.Value + " " + hfManagerFirstName_selected.Value;

            authentication auth = authentication.GetCurrent();
            string sXmlOut = "";
            DataTable dtTabletList = Agency.getAgencyHwDetails("TAB", hfRefAgency_selected.Value, txtTabletSearch.Text, auth.sToken, out sXmlOut);

            int iNbResult = 0;
            int.TryParse(tools.GetValueFromXml(sXmlOut, "ALL_XML_OUT/H", "NbResult"), out iNbResult);

            if (iNbResult > 0)
            {
                panelTabletSearchAdd.Visible = false;
                hfTabletListLength.Value = dtTabletList.Rows.Count.ToString();
                rptTabletList.DataSource = dtTabletList;
                rptTabletList.DataBind();
            }
            else
            {
                panelTabletSearchAdd.Visible = true;
                rptTabletList.Visible = false;
                panelTabletListEmpty.Visible = true;
            }
        }
    }
    protected void btnPinpadSearch_Click(object sender, EventArgs e)
    {
        if (txtPinpadSearch.Text.Trim().Length > 0)
        {

            panelPinpadSearch.Visible = true;
            rptPinpadList.Visible = true;
            panelPinpadListEmpty.Visible = false;
            hfPinpadListLength.Value = "0";

            lblAgencyID.Text = hfAgencyID_selected.Value;
            lblAgencyName.Text = hfAgencyName_selected.Value;
            lblManagerName.Text = hfManagerLastName_selected.Value + " " + hfManagerFirstName_selected.Value;

            authentication auth = authentication.GetCurrent();
            string sXmlOut = "";
            DataTable dtPinpadList = Agency.getAgencyHwDetails("PPD", hfRefAgency_selected.Value, txtPinpadSearch.Text, auth.sToken, out sXmlOut);

            int iNbResult = 0;
            int.TryParse(tools.GetValueFromXml(sXmlOut, "ALL_XML_OUT/H", "NbResult"), out iNbResult);

            if (iNbResult > 0)
            {
                panelPinpadSearchAdd.Visible = false;
                hfPinpadListLength.Value = dtPinpadList.Rows.Count.ToString();
                rptPinpadList.DataSource = dtPinpadList;
                rptPinpadList.DataBind();
            }
            else
            {
                panelPinpadSearchAdd.Visible = true;
                rptPinpadList.Visible = false;
                panelPinpadListEmpty.Visible = true;
            }
        }
    }

    protected void btnSearchAgencyToAllocate_Click(object sender, EventArgs e)
    {
        string sXmlOut = "";
        authentication auth = authentication.GetCurrent();
        string sSearchValue = txtSearchAgencyToAllocate.Text;
        DataTable dtSearchResult = Agency.getAgencySearchResult(sSearchValue, iMaxResult, auth.sToken, out sXmlOut);
        rptSearchAgencyToAllocateResult.DataSource = dtSearchResult;
        rptSearchAgencyToAllocateResult.DataBind();

        int iSearchResultNb = dtSearchResult.Rows.Count;
        if (iSearchResultNb == 0)
            lblSearchAgencyToAllocateResultNb.Text = "Aucune agence trouvée.";

        panelSearchAgencyToAllocateResultNb.Visible = true;
    }
    protected void btnSearchAgencyToDeallocate_Click(object sender, EventArgs e)
    {
        string sXmlOut = "";
        authentication auth = authentication.GetCurrent();
        string sSearchValue = txtSearchAgencyToDeallocate.Text;
        DataTable dtSearchResult = Agency.getAgencySearchResult(sSearchValue, iMaxResult, auth.sToken, out sXmlOut);
        rptSearchAgencyToDeallocateResult.DataSource = dtSearchResult;
        rptSearchAgencyToDeallocateResult.DataBind();

        int iSearchResultNb = dtSearchResult.Rows.Count;
        if (iSearchResultNb == 0)
            lblSearchAgencyToDeallocateResultNb.Text = "Aucune agence trouvée.";

        panelSearchAgencyToDeallocateResultNb.Visible = true;

    }
    protected void btnRefreshAgencyDetails_AgencyToDeallocate_Click(object sender, EventArgs e)
    {
        panelHardwareToAddSearch.Visible = false;
        panelHardwareListEmpty_AgencyToDeallocate.Visible = false;
        hfHardwareListLength_AgencyToDeallocate.Value = "0";

        lblAgencyID_AgencyToDeallocate.Text = hfAgencyID_AgencyToDeallocate_selected.Value;
        lblAgencyName_AgencyToDeallocate.Text = hfAgencyName_AgencyToDeallocate_selected.Value;
        lblManagerName_AgencyToDeallocate.Text = hfManagerLastName_AgencyToDeallocate_selected.Value + " " + hfManagerFirstName_AgencyToDeallocate_selected.Value;

        authentication auth = authentication.GetCurrent();
        string sXmlOut = "";
        int iNbResult = 0;

        rptMpadList_AgencyToDeallocate.Visible = false;
        rptPosList_AgencyToDeallocate.Visible = false;
        rptTabletList_AgencyToDeallocate.Visible = false;
        rptScannerList_AgencyToDeallocate.Visible = false;
        rptPinpadList_AgencyToDeallocate.Visible = false;

        DataTable dtHwList = new DataTable();
        try
        {
            dtHwList = Agency.getAgencyHwList(hfHardwareType_selected.Value, hfRefAgency_AgencyToDeallocate_selected.Value, auth.sToken, iMaxResult, out sXmlOut);
            int.TryParse(tools.GetValueFromXml(sXmlOut, "ALL_XML_OUT/H", "NbResult"), out iNbResult);
        }
        catch (Exception ex) { }
        string sHardwareName = "";
        string sHardwaresName = "";

        switch (hfHardwareType_selected.Value)
        {
            case "POS":
                sHardwareName = "terminal";
                sHardwaresName = "terminaux";
                rptPosList_AgencyToDeallocate.Visible = true;
                lblHardwareToAddTitle.Text = "LISTE TPE(S)";
                lblAgencyToDeallocateTitle.Text = "Veuillez sélectionner le terminal à ajouter";
                lblHardwareListEmpty_AgencyToDeallocate.Text = "Aucun terminal affecté.";

                if (iNbResult > 0)
                {
                    hfHardwareListLength_AgencyToDeallocate.Value = dtHwList.Rows.Count.ToString();
                    rptPosList_AgencyToDeallocate.DataSource = dtHwList;
                    rptPosList_AgencyToDeallocate.DataBind();
                }
                else if (iNbResult < 0)
                {
                    lblHardwareToAddSearch.Text = "Trop de " + sHardwaresName + " à afficher.<br />Veuillez rechercher par numéro de série.";
                    rptPosList_AgencyToDeallocate.Visible = false;
                    panelHardwareListEmpty_AgencyToDeallocate.Visible = false;
                    hfHardwareSerialList_AgencyToDeallocate.Value = getHwSerialList(hfHardwareType_selected.Value, hfAgencyID_AgencyToDeallocate_selected.Value.Trim(), txtSearchAgencyToDeallocate.Text);
                    panelHardwareToAddSearch.Visible = true;
                    txtHardwareToAddSearch.Text = "";
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "autocomplete_HardwareToAddSearch();", true);
                }
                else
                {
                    rptPosList_AgencyToDeallocate.Visible = false;
                    panelHardwareListEmpty_AgencyToDeallocate.Visible = true;
                }


                break;
            case "MPD":
                sHardwareName = "borne";
                sHardwaresName = "bornes";
                rptMpadList_AgencyToDeallocate.Visible = true;
                lblHardwareToAddTitle.Text = "LISTE BORNE(S)";
                lblAgencyToDeallocateTitle.Text = "Veuillez sélectionner la borne à ajouter";
                lblHardwareListEmpty_AgencyToDeallocate.Text = "Aucune borne affectée.";

                if (iNbResult > 0)
                {
                    hfHardwareListLength_AgencyToDeallocate.Value = dtHwList.Rows.Count.ToString();
                    rptMpadList_AgencyToDeallocate.DataSource = dtHwList;
                    rptMpadList_AgencyToDeallocate.DataBind();
                }
                else if (iNbResult < 0)
                {
                    lblHardwareToAddSearch.Text = "Trop de " + sHardwaresName + " à afficher.<br />Veuillez rechercher par numéro de série.";
                    rptMpadList_AgencyToDeallocate.Visible = false;
                    panelHardwareListEmpty_AgencyToDeallocate.Visible = false;
                    hfHardwareSerialList_AgencyToDeallocate.Value = getHwSerialList(hfHardwareType_selected.Value, hfAgencyID_AgencyToDeallocate_selected.Value.Trim(), txtSearchAgencyToDeallocate.Text);
                    panelHardwareToAddSearch.Visible = true;
                    txtHardwareToAddSearch.Text = "";
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "autocomplete_HardwareToAddSearch();", true);
                }
                else
                {
                    rptMpadList_AgencyToDeallocate.Visible = false;
                    panelHardwareListEmpty_AgencyToDeallocate.Visible = true;
                }
                break;
            case "TAB":
                sHardwareName = "tablette";
                sHardwaresName = "tablettes";
                rptTabletList_AgencyToDeallocate.Visible = true;
                lblHardwareToAddTitle.Text = "LISTE TABLETTE(S)";
                lblAgencyToDeallocateTitle.Text = "Veuillez sélectionner la tablette à ajouter";
                lblHardwareListEmpty_AgencyToDeallocate.Text = "Aucune tablette affectée.";

                if (iNbResult > 0)
                {
                    hfHardwareListLength_AgencyToDeallocate.Value = dtHwList.Rows.Count.ToString();
                    rptTabletList_AgencyToDeallocate.DataSource = dtHwList;
                    rptTabletList_AgencyToDeallocate.DataBind();
                }
                else if (iNbResult < 0)
                {
                    lblHardwareToAddSearch.Text = "Trop de " + sHardwaresName + " à afficher.<br />Veuillez rechercher par numéro de série.";
                    rptTabletList_AgencyToDeallocate.Visible = false;
                    panelHardwareListEmpty_AgencyToDeallocate.Visible = false;
                    hfHardwareSerialList_AgencyToDeallocate.Value = getHwSerialList(hfHardwareType_selected.Value, hfAgencyID_AgencyToDeallocate_selected.Value.Trim(), txtSearchAgencyToDeallocate.Text);
                    panelHardwareToAddSearch.Visible = true;
                    txtHardwareToAddSearch.Text = "";
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "autocomplete_HardwareToAddSearch();", true);
                }
                else
                {
                    rptTabletList_AgencyToDeallocate.Visible = false;
                    panelHardwareListEmpty_AgencyToDeallocate.Visible = true;
                }
                break;
            case "SCN":
                sHardwareName = "scanner";
                sHardwaresName = "scanners";
                rptScannerList_AgencyToDeallocate.Visible = true;
                lblHardwareToAddTitle.Text = "LISTE SCANNER(S)";
                lblAgencyToDeallocateTitle.Text = "Veuillez sélectionner le scanner à ajouter";
                lblHardwareListEmpty_AgencyToDeallocate.Text = "Aucun scanner affecté.";

                if (iNbResult > 0)
                {
                    hfHardwareListLength_AgencyToDeallocate.Value = dtHwList.Rows.Count.ToString();
                    rptScannerList_AgencyToDeallocate.DataSource = dtHwList;
                    rptScannerList_AgencyToDeallocate.DataBind();
                }
                else if (iNbResult < 0)
                {
                    lblHardwareToAddSearch.Text = "Trop de " + sHardwaresName + " à afficher.<br />Veuillez rechercher par numéro de série.";
                    rptScannerList_AgencyToDeallocate.Visible = false;
                    panelHardwareListEmpty_AgencyToDeallocate.Visible = false;
                    hfHardwareSerialList_AgencyToDeallocate.Value = getHwSerialList(hfHardwareType_selected.Value, hfAgencyID_AgencyToDeallocate_selected.Value.Trim(), txtSearchAgencyToDeallocate.Text);
                    panelHardwareToAddSearch.Visible = true;
                    txtHardwareToAddSearch.Text = "";
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "autocomplete_HardwareToAddSearch();", true);
                }
                else
                {
                    rptScannerList_AgencyToDeallocate.Visible = false;
                    panelHardwareListEmpty_AgencyToDeallocate.Visible = true;
                }
                break;
            case "PPD":
                sHardwareName = "pinpad";
                sHardwaresName = "pinpads";
                rptPinpadList_AgencyToDeallocate.Visible = true;
                lblHardwareToAddTitle.Text = "LISTE PINPAD(S)";
                lblAgencyToDeallocateTitle.Text = "Veuillez sélectionner le pinpad à ajouter";
                lblHardwareListEmpty_AgencyToDeallocate.Text = "Aucun pinpad affecté.";

                if (iNbResult > 0)
                {
                    hfHardwareListLength_AgencyToDeallocate.Value = dtHwList.Rows.Count.ToString();
                    rptPinpadList_AgencyToDeallocate.DataSource = dtHwList;
                    rptPinpadList_AgencyToDeallocate.DataBind();
                }
                else if (iNbResult < 0)
                {
                    lblHardwareToAddSearch.Text = "Trop de " + sHardwaresName + " à afficher.<br />Veuillez rechercher par numéro de série.";
                    rptPinpadList_AgencyToDeallocate.Visible = false;
                    panelHardwareListEmpty_AgencyToDeallocate.Visible = false;
                    hfHardwareSerialList_AgencyToDeallocate.Value = getHwSerialList(hfHardwareType_selected.Value, hfAgencyID_AgencyToDeallocate_selected.Value.Trim(), txtSearchAgencyToDeallocate.Text);
                    panelHardwareToAddSearch.Visible = true;
                    txtHardwareToAddSearch.Text = "";
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "autocomplete_HardwareToAddSearch();", true);
                }
                else
                {
                    rptPinpadList_AgencyToDeallocate.Visible = false;
                    panelHardwareListEmpty_AgencyToDeallocate.Visible = true;
                }
                break;
            case "PDB":
                sHardwareName = "support borne";
                sHardwaresName = "supports borne";
                rptScannerList_AgencyToDeallocate.Visible = true;
                lblHardwareToAddTitle.Text = "LISTE SUPPORT(S) BORNE";
                lblAgencyToDeallocateTitle.Text = "Veuillez sélectionner le support borne à ajouter";
                lblHardwareListEmpty_AgencyToDeallocate.Text = "Aucun support borne affecté.";

                if (iNbResult > 0)
                {
                    hfHardwareListLength_AgencyToDeallocate.Value = dtHwList.Rows.Count.ToString();
                    rptScannerList_AgencyToDeallocate.DataSource = dtHwList;
                    rptScannerList_AgencyToDeallocate.DataBind();
                }
                else if (iNbResult < 0)
                {
                    lblHardwareToAddSearch.Text = "Trop de " + sHardwaresName + " à afficher.<br />Veuillez rechercher par numéro de série.";
                    rptScannerList_AgencyToDeallocate.Visible = false;
                    panelHardwareListEmpty_AgencyToDeallocate.Visible = false;
                    hfHardwareSerialList_AgencyToDeallocate.Value = getHwSerialList(hfHardwareType_selected.Value, hfAgencyID_AgencyToDeallocate_selected.Value.Trim(), txtSearchAgencyToDeallocate.Text);
                    panelHardwareToAddSearch.Visible = true;
                    txtHardwareToAddSearch.Text = "";
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "autocomplete_HardwareToAddSearch();", true);
                }
                else
                {
                    rptScannerList_AgencyToDeallocate.Visible = false;
                    panelHardwareListEmpty_AgencyToDeallocate.Visible = true;
                }
                break;
        }


    }

    //protected string getPosSerialList(string sAgencyID, string sAgencySearchValue)
    //{
    //    string sPosSerialList = "";
    //    authentication auth = authentication.GetCurrent();
    //    string sXmlOut = "";
    //    string sAgencyIDFound = "";
    //    DataTable dt = Agency.getAgencySearchResult(sAgencySearchValue, 50, auth.sToken, out sXmlOut);

    //    for (int i = 0; i < dt.Rows.Count; i++)
    //    {
    //        sAgencyIDFound = dt.Rows[i]["A_ID"].ToString().Trim();
    //        if (sAgencyIDFound == sAgencyID.Trim())
    //            sPosSerialList = dt.Rows[i]["POS_SerialList"].ToString().Trim();
    //    }

    //    return sPosSerialList;
    //}
    //protected string getMpadSerialList(string sAgencyID, string sAgencySearchValue)
    //{
    //    string sMpadSerialList = "";
    //    authentication auth = authentication.GetCurrent();
    //    string sXmlOut = "";
    //    string sAgencyIDFound = "";
    //    DataTable dt = Agency.getAgencySearchResult(sAgencySearchValue, 50, auth.sToken, out sXmlOut);

    //    for (int i = 0; i < dt.Rows.Count; i++)
    //    {
    //        sAgencyIDFound = dt.Rows[i]["A_ID"].ToString().Trim();
    //        if (sAgencyIDFound == sAgencyID.Trim())
    //            sMpadSerialList = dt.Rows[i]["MP_SerialList"].ToString().Trim();
    //    }

    //    return sMpadSerialList;
    //}
    protected string getHwSerialList(string sHwType, string sAgencyID, string sAgencySearchValue)
    {
        string sHwSerialList = "";
        authentication auth = authentication.GetCurrent();
        string sXmlOut = "";
        string sAgencyIDFound = "";
        DataTable dt = Agency.getAgencySearchResult(sAgencySearchValue, 50, auth.sToken, out sXmlOut);

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            sAgencyIDFound = dt.Rows[i]["A_ID"].ToString().Trim();
            if (sAgencyIDFound == sAgencyID.Trim())
            {
                switch (sHwType)
                {
                    case "MPD":
                        sHwSerialList = dt.Rows[i]["MP_SerialList"].ToString().Trim();
                        break;
                    case "POS":
                        sHwSerialList = dt.Rows[i]["POS_SerialList"].ToString().Trim();
                        break;
                    case "SCN":
                        sHwSerialList = dt.Rows[i]["SCN_SerialList"].ToString().Trim();
                        break;
                    case "TAB":
                        sHwSerialList = dt.Rows[i]["TAB_SerialList"].ToString().Trim();
                        break;
                    case "PPD":
                        sHwSerialList = dt.Rows[i]["PPD_SerialList"].ToString().Trim();
                        break;
                    case "PDB":
                        sHwSerialList = dt.Rows[i]["PDB_SerialList"].ToString().Trim();
                        break;
                }
            }
        }

        return sHwSerialList;
    }

    protected void btnHardwareToAddSearch_Click(object sender, EventArgs e)
    {
        authentication auth = authentication.GetCurrent();
        string sXmlOut = "";
        int iNbResult = 0;

        panelHardwareListEmpty_AgencyToDeallocate.Visible = false;
        panelHardwareToAddSearch.Visible = true;
        hfHardwareListLength_AgencyToDeallocate.Value = "0";
        lblAgencyID_AgencyToDeallocate.Text = hfAgencyID_AgencyToDeallocate_selected.Value;
        lblAgencyName_AgencyToDeallocate.Text = hfAgencyName_AgencyToDeallocate_selected.Value;
        lblManagerName_AgencyToDeallocate.Text = hfManagerLastName_AgencyToDeallocate_selected.Value + " " + hfManagerFirstName_AgencyToDeallocate_selected.Value;

        rptMpadList_AgencyToDeallocate.Visible = false;
        rptPosList_AgencyToDeallocate.Visible = false;
        //rptTabletList_AgencyToDeallocate.Visible = false;
        //rptScannerList_AgencyToDeallocate.Visible = false;
        //rptPinpadList_AgencyToDeallocate.Visible = false;

        DataTable dtHwList = new DataTable();

        switch(hfHardwareType_selected.Value.Trim())
        {
            case "POS":
                if(txtHardwareToAddSearch.Text.Trim().Length >= 8)
                {
                    rptPosList_AgencyToDeallocate.Visible = true;
                    dtHwList = Agency.getAgencyHwDetails(hfHardwareType_selected.Value.Trim(), hfRefAgency_AgencyToDeallocate_selected.Value, txtHardwareToAddSearch.Text, auth.sToken, out sXmlOut);

                    int.TryParse(tools.GetValueFromXml(sXmlOut, "ALL_XML_OUT/H", "NbResult"), out iNbResult);

                    if (iNbResult > 0)
                    {
                        hfHardwareListLength_AgencyToDeallocate.Value = dtHwList.Rows.Count.ToString();

                        rptPosList_AgencyToDeallocate.DataSource = dtHwList;
                        rptPosList_AgencyToDeallocate.DataBind();
                    }
                    else
                    {
                        rptPosList_AgencyToDeallocate.Visible = false;
                        panelHardwareListEmpty_AgencyToDeallocate.Visible = true;
                    }
                }
                break;
            case "MPD":
                if (txtHardwareToAddSearch.Text.Trim().Length == 12)
                {
                    rptMpadList_AgencyToDeallocate.Visible = true;
                    dtHwList = Agency.getAgencyHwDetails(hfHardwareType_selected.Value.Trim(), hfRefAgency_AgencyToDeallocate_selected.Value, txtHardwareToAddSearch.Text, auth.sToken, out sXmlOut);

                    int.TryParse(tools.GetValueFromXml(sXmlOut, "ALL_XML_OUT/H", "NbResult"), out iNbResult);

                    if (iNbResult > 0)
                    {
                        hfHardwareListLength_AgencyToDeallocate.Value = dtHwList.Rows.Count.ToString();
                        rptMpadList_AgencyToDeallocate.DataSource = dtHwList;
                        rptMpadList_AgencyToDeallocate.DataBind();
                    }
                    else
                    {
                        rptMpadList_AgencyToDeallocate.Visible = false;
                        panelHardwareListEmpty_AgencyToDeallocate.Visible = true;
                    }
                }
                break;
            case "TAB":
                if (txtHardwareToAddSearch.Text.Trim().Length > 0)
                {
                    rptTabletList_AgencyToDeallocate.Visible = true;
                    dtHwList = Agency.getAgencyHwDetails(hfHardwareType_selected.Value.Trim(), hfRefAgency_AgencyToDeallocate_selected.Value, txtHardwareToAddSearch.Text, auth.sToken, out sXmlOut);

                    int.TryParse(tools.GetValueFromXml(sXmlOut, "ALL_XML_OUT/H", "NbResult"), out iNbResult);

                    if (iNbResult > 0)
                    {
                        hfHardwareListLength_AgencyToDeallocate.Value = dtHwList.Rows.Count.ToString();
                        rptTabletList_AgencyToDeallocate.DataSource = dtHwList;
                        rptTabletList_AgencyToDeallocate.DataBind();
                    }
                    else
                    {
                        rptTabletList_AgencyToDeallocate.Visible = false;
                        panelHardwareListEmpty_AgencyToDeallocate.Visible = true;
                    }
                }
                break;
            case "SCN":
                if (txtHardwareToAddSearch.Text.Trim().Length > 0)
                {
                    rptScannerList_AgencyToDeallocate.Visible = true;
                    dtHwList = Agency.getAgencyHwDetails(hfHardwareType_selected.Value.Trim(), hfRefAgency_AgencyToDeallocate_selected.Value, txtHardwareToAddSearch.Text, auth.sToken, out sXmlOut);

                    int.TryParse(tools.GetValueFromXml(sXmlOut, "ALL_XML_OUT/H", "NbResult"), out iNbResult);

                    if (iNbResult > 0)
                    {
                        hfHardwareListLength_AgencyToDeallocate.Value = dtHwList.Rows.Count.ToString();
                        rptScannerList_AgencyToDeallocate.DataSource = dtHwList;
                        rptScannerList_AgencyToDeallocate.DataBind();
                    }
                    else
                    {
                        rptScannerList_AgencyToDeallocate.Visible = false;
                        panelHardwareListEmpty_AgencyToDeallocate.Visible = true;
                    }
                }
                break;
            case "PPD":
                if (txtHardwareToAddSearch.Text.Trim().Length > 0)
                {
                    rptPinpadList_AgencyToDeallocate.Visible = true;
                    dtHwList = Agency.getAgencyHwDetails(hfHardwareType_selected.Value.Trim(), hfRefAgency_AgencyToDeallocate_selected.Value, txtHardwareToAddSearch.Text, auth.sToken, out sXmlOut);

                    int.TryParse(tools.GetValueFromXml(sXmlOut, "ALL_XML_OUT/H", "NbResult"), out iNbResult);

                    if (iNbResult > 0)
                    {
                        hfHardwareListLength_AgencyToDeallocate.Value = dtHwList.Rows.Count.ToString();
                        rptPinpadList_AgencyToDeallocate.DataSource = dtHwList;
                        rptPinpadList_AgencyToDeallocate.DataBind();
                    }
                    else
                    {
                        rptPinpadList_AgencyToDeallocate.Visible = false;
                        panelHardwareListEmpty_AgencyToDeallocate.Visible = true;
                    }
                }
                break;
            case "PDB":
                if (txtHardwareToAddSearch.Text.Trim().Length > 0)
                {
                    rptScannerList_AgencyToDeallocate.Visible = true;
                    dtHwList = Agency.getAgencyHwDetails(hfHardwareType_selected.Value.Trim(), hfRefAgency_AgencyToDeallocate_selected.Value, txtHardwareToAddSearch.Text, auth.sToken, out sXmlOut);

                    int.TryParse(tools.GetValueFromXml(sXmlOut, "ALL_XML_OUT/H", "NbResult"), out iNbResult);

                    if (iNbResult > 0)
                    {
                        hfHardwareListLength_AgencyToDeallocate.Value = dtHwList.Rows.Count.ToString();
                        rptTabletSupportList_AgencyToDeallocate.DataSource = dtHwList;
                        rptTabletSupportList_AgencyToDeallocate.DataBind();
                    }
                    else
                    {
                        rptTabletSupportList_AgencyToDeallocate.Visible = false;
                        panelHardwareListEmpty_AgencyToDeallocate.Visible = true;
                    }
                }
                break;
        }

        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "autocomplete_HardwareToAddSearch('false');", true);
    }

    protected void btnInitCode_Click(object sender, EventArgs e)
    {
        authentication auth = authentication.GetCurrent();
        panelMpadDetails.Attributes.Add("style", "display:none");

        string sCode = "", sError = "";
        if (Agency.MpadReinitCode(auth.sToken, hfHardwareSerial_selected.Value, hfAgencyID_selected.Value, hfRefPartner_selected.Value, out sCode, out sError))
        {
            lblMpadReinitCodeResult.ForeColor = System.Drawing.Color.Green;
            lblMpadReinitCodeResult.Text = "<b>L'opération a réussi.</b>";
            lblMpadReinitCodeResult_Code.Text = sCode;
            panelMpadReinitCodeResult_Code.Visible = true;
            hfMpadReinitCode.Value = "true";
            hfReload.Value = "true";
        }
        else
        {
            lblMpadReinitCodeResult.ForeColor = System.Drawing.Color.Red;
            lblMpadReinitCodeResult.Text = "<b>L'opération a échoué.</b><br/> ("+ sError.Trim() +")";
            panelMpadReinitCodeResult_Code.Visible = false;
        }
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "HideMpadReinitCodeLoading();", true);
    }
    protected void btnConfirmHardwareAllocate_Click(object sender, EventArgs e)
    {
        panelMpadReallocate_InitCode.Visible = false;
        authentication auth = authentication.GetCurrent();
        string sCode = "", sError = "", sComment = "", sHardwareType = "MPD";
        //if (rblHardwareReallocateComment.SelectedValue == "Y")
        sComment = txtHardwareReallocateComment.Text;
        if(hfHardwareType_selected.Value.Trim().Length > 0)
            sHardwareType = hfHardwareType_selected.Value.Trim();

        //if (Agency.addMpadToAgency(auth.sToken, hfMpadSerial_selected.Value, hfAgencyID_destination.Value, hfRefPartner_destination.Value,
            //sComment, out sCode, out sError))

        if(Agency.addHardwareToAgency(auth.sToken, hfHardwareType_selected.Value, hfHardwareSerial_selected.Value, hfAgencyID_destination.Value, hfRefPartner_destination.Value,
            sComment, out sCode, out sError))
        {
            lblHardwareReallocateResult.ForeColor = System.Drawing.Color.Green;
            lblHardwareReallocateResult.Text = "<b>L'opération a réussi.</b>";
            panelHardwareReallocate.Attributes.Add("style", "display:none");
            panelHardwareReallocateResult.Attributes.Remove("style");
            hfReload.Value = "true";

            if (sHardwareType != "MPD")
                panelMpadReallocate_InitCode.Visible = false;
            else
            {
                panelMpadReallocate_InitCode.Visible = true;
                lblMpadReallocate_InitCode.Text = sCode;
            }

        }
        else
        {
            lblHardwareReallocateResult.ForeColor = System.Drawing.Color.Red;
            lblHardwareReallocateResult.Text = "<b>L'opération a échoué.</b><br/> (" + sError.Trim() + ")";
            panelMpadReallocate_InitCode.Visible = false;
            panelHardwareReallocate.Attributes.Add("style", "display:none");
            panelHardwareReallocateResult.Attributes.Remove("style");
        }

        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "HideHardwareReallocateLoading();", true);
    }

    protected DataTable replaceValueInDatatable(DataTable dt, string sValueToReplace, string sValue)
    {
        DataTable dtNew = new DataTable();

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            for (int j = 0; j < dt.Columns.Count; j++)
            {
                dt.Rows[i][j] = dt.Rows[i][j].ToString().Replace(sValueToReplace, sValue);
            }
        }

        dt.AcceptChanges();
        dtNew = dt;

        return dtNew;
    }

    protected void setMpadList()
    {
        //MPAD LIST
        authentication auth = authentication.GetCurrent();
        string sMpadList_XmlOut = "";
        DataTable dtMpadList = Agency.getAgencyMpadList(hfRefAgency_selected.Value, auth.sToken, iMaxResult, out sMpadList_XmlOut);
        int iNbMpadResult = 0;
        int.TryParse(tools.GetValueFromXml(sMpadList_XmlOut, "ALL_XML_OUT/H", "NbResult"), out iNbMpadResult);

        if (iNbMpadResult > 0)
        {
            hfMpadListLength.Value = dtMpadList.Rows.Count.ToString();
            rptMpadList.DataSource = dtMpadList;
            rptMpadList.DataBind();
        }
        else if (iNbMpadResult < 0)
        {
            rptMpadList.Visible = false;
            panelMpadListEmpty.Visible = false;
            hfMpadSerialList_temp.Value = getHwSerialList("MPD",hfAgencyID_selected.Value.Trim(), txtSearch.Text);
            panelMpadSearch.Visible = true;
            panelMpadSearchAdd.Visible = true;
            txtMpadSearch.Text = "";
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "autocomplete_MpadSearch();", true);
        }
        else
        {
            rptMpadList.Visible = false;
            panelMpadListEmpty.Visible = true;
        }
    }
    protected void setPosList()
    {
        //POS LIST
        authentication auth = authentication.GetCurrent();
        string sPosList_XmlOut = "";
        DataTable dtPosList = Agency.getAgencyPosList(hfRefAgency_selected.Value, auth.sToken, iMaxResult, out sPosList_XmlOut);
        int iNbPosResult = 0;
        int.TryParse(tools.GetValueFromXml(sPosList_XmlOut, "ALL_XML_OUT/H", "NbResult"), out iNbPosResult);
        if (iNbPosResult > 0)
        {
            hfPosListLength.Value = dtPosList.Rows.Count.ToString();
            rptPosList.DataSource = dtPosList;
            rptPosList.DataBind();
        }
        else if (iNbPosResult < 0)
        {
            rptPosList.Visible = false;
            panelPosListEmpty.Visible = false;
            hfPosSerialList_temp.Value = getHwSerialList("POS",hfAgencyID_selected.Value.Trim(), txtSearch.Text);
            panelPosSearch.Visible = true;
            panelPosSearchAdd.Visible = true;
            txtPosSearch.Text = "";
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "autocomplete_PosSearch();", true);
        }
        else
        {
            rptPosList.Visible = false;
            panelPosListEmpty.Visible = true;
        }
    }
    protected void setScannerList()
    {
        //SCANNER LIST
        authentication auth = authentication.GetCurrent();
        string sScannerList_XmlOut = "";
        DataTable dtScannerList = Agency.getAgencyHwList("SCN",hfRefAgency_selected.Value, auth.sToken, iMaxResult, out sScannerList_XmlOut);
        int iNbScannerResult = 0;
        int.TryParse(tools.GetValueFromXml(sScannerList_XmlOut, "ALL_XML_OUT/H", "NbResult"), out iNbScannerResult);

        if (iNbScannerResult > 0)
        {
            hfScannerListLength.Value = dtScannerList.Rows.Count.ToString();
            rptScannerList.DataSource = dtScannerList;
            rptScannerList.DataBind();
        }
        else if (iNbScannerResult < 0)
        {
            rptScannerList.Visible = false;
            panelScannerListEmpty.Visible = false;
            hfScannerSerialList_temp.Value = getHwSerialList("SCN",hfAgencyID_selected.Value.Trim(), txtSearch.Text);
            panelScannerSearch.Visible = true;
            panelScannerSearchAdd.Visible = true;
            txtScannerSearch.Text = "";
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "autocomplete_ScannerSearch();", true);
        }
        else
        {
            rptScannerList.Visible = false;
            panelScannerListEmpty.Visible = true;
        }
    }
    protected void setTabletList()
    {
        //TABLET LIST
        authentication auth = authentication.GetCurrent();
        string sTabletList_XmlOut = "";
        DataTable dtTabletList = Agency.getAgencyHwList("TAB", hfRefAgency_selected.Value, auth.sToken, iMaxResult, out sTabletList_XmlOut);
        int iNbTabletResult = 0;
        int.TryParse(tools.GetValueFromXml(sTabletList_XmlOut, "ALL_XML_OUT/H", "NbResult"), out iNbTabletResult);

        if (iNbTabletResult > 0)
        {
            hfTabletListLength.Value = dtTabletList.Rows.Count.ToString();
            rptTabletList.DataSource = dtTabletList;
            rptTabletList.DataBind();
        }
        else if (iNbTabletResult < 0)
        {
            rptTabletList.Visible = false;
            panelTabletListEmpty.Visible = false;
            hfTabletSerialList_temp.Value = getHwSerialList("TAB", hfAgencyID_selected.Value.Trim(), txtSearch.Text);
            panelTabletSearch.Visible = true;
            panelTabletSearchAdd.Visible = true;
            txtTabletSearch.Text = "";
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "autocomplete_TabletSearch();", true);
        }
        else
        {
            rptTabletList.Visible = false;
            panelTabletListEmpty.Visible = true;
        }
    }
    protected void setPinpadList()
    {
        //PINPAD LIST
        authentication auth = authentication.GetCurrent();
        string sHwList_XmlOut = "";
        DataTable dtHwList = Agency.getAgencyHwList("PPD", hfRefAgency_selected.Value, auth.sToken, iMaxResult, out sHwList_XmlOut);
        int iNbPinpadResult = 0;
        int.TryParse(tools.GetValueFromXml(sHwList_XmlOut, "ALL_XML_OUT/H", "NbResult"), out iNbPinpadResult);

        if (iNbPinpadResult > 0)
        {
            hfPinpadListLength.Value = dtHwList.Rows.Count.ToString();
            rptPinpadList.DataSource = dtHwList;
            rptPinpadList.DataBind();
        }
        else if (iNbPinpadResult < 0)
        {
            rptPinpadList.Visible = false;
            panelPinpadListEmpty.Visible = false;
            hfPinpadSerialList_temp.Value = getHwSerialList("PPD", hfAgencyID_selected.Value.Trim(), txtSearch.Text);
            panelPinpadSearch.Visible = true;
            panelPinpadSearchAdd.Visible = true;
            txtPinpadSearch.Text = "";
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "autocomplete_PinpadSearch();", true);
        }
        else
        {
            rptPinpadList.Visible = false;
            panelPinpadListEmpty.Visible = true;
        }
    }
    protected void setTabletSupportList()
    {
        //TABLET SUPPORT LIST
        authentication auth = authentication.GetCurrent();
        string sTabletSupportList_XmlOut = "";
        DataTable dtTabletSupportList = Agency.getAgencyHwList("PDB", hfRefAgency_selected.Value, auth.sToken, iMaxResult, out sTabletSupportList_XmlOut);
        int iNbTabletSupportResult = 0;
        int.TryParse(tools.GetValueFromXml(sTabletSupportList_XmlOut, "ALL_XML_OUT/H", "NbResult"), out iNbTabletSupportResult);

        if (iNbTabletSupportResult > 0)
        {
            hfTabletSupportListLength.Value = dtTabletSupportList.Rows.Count.ToString();
            rptTabletSupportList.DataSource = dtTabletSupportList;
            rptTabletSupportList.DataBind();
        }
        else if (iNbTabletSupportResult < 0)
        {
            rptTabletSupportList.Visible = false;
            panelTabletSupportListEmpty.Visible = false;
            hfTabletSupportSerialList_temp.Value = getHwSerialList("PDB", hfAgencyID_selected.Value.Trim(), txtSearch.Text);
            panelTabletSupportSearch.Visible = true;
            panelTabletSupportSearchAdd.Visible = true;
            txtTabletSupportSearch.Text = "";
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "autocomplete_TabletSupportSearch();", true);
        }
        else
        {
            rptTabletSupportList.Visible = false;
            panelTabletSupportListEmpty.Visible = true;
        }
    }

    protected void btnTabletSupportSearch_Click(object sender, EventArgs e)
    {
        if (txtTabletSupportSearch.Text.Trim().Length > 0)
        {

            panelTabletSupportSearch.Visible = true;
            rptTabletSupportList.Visible = true;
            panelTabletSupportListEmpty.Visible = false;
            hfTabletSupportListLength.Value = "0";

            lblAgencyID.Text = hfAgencyID_selected.Value;
            lblAgencyName.Text = hfAgencyName_selected.Value;
            lblManagerName.Text = hfManagerLastName_selected.Value + " " + hfManagerFirstName_selected.Value;

            authentication auth = authentication.GetCurrent();
            string sXmlOut = "";
            DataTable dtTabletSupportList = Agency.getAgencyHwDetails("PDB", hfRefAgency_selected.Value, txtTabletSupportSearch.Text, auth.sToken, out sXmlOut);

            int iNbResult = 0;
            int.TryParse(tools.GetValueFromXml(sXmlOut, "ALL_XML_OUT/H", "NbResult"), out iNbResult);

            if (iNbResult > 0)
            {
                panelTabletSupportSearchAdd.Visible = false;
                hfTabletSupportListLength.Value = dtTabletSupportList.Rows.Count.ToString();
                rptTabletSupportList.DataSource = dtTabletSupportList;
                rptTabletSupportList.DataBind();
            }
            else
            {
                panelTabletSupportSearchAdd.Visible = true;
                rptTabletSupportList.Visible = false;
                panelTabletSupportListEmpty.Visible = true;
            }
        }
        }
}