﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using XMLMethodLibrary;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!IsPostBack)
        //{
        //    BindMenu();
        //    hfPageRequest.Value = "";
        //}
        Response.Redirect("AgencySearch.aspx");
    }

    protected void BindMenu()
    {
        string sXmlOut = tools.GetRights(authentication.GetCurrent().sToken, "SC_Mobile_Menu");

        List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Page", "RC");

        if (listRC.Count > 0 && listRC[0] == "0")
        {
            List<string> listAction = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/Page/Action");
            DataTable dtMenu = new DataTable();
            dtMenu.Columns.Add("Label");
            dtMenu.Columns.Add("Value");

            for (int i = 0; i < listAction.Count; i++)
            {
                List<string> listTagAction = CommonMethod.GetAttributeValues(listAction[i], "Action", "TagAction");
                List<string> listViewAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ViewAllowed");

                if (listTagAction.Count > 0 && listViewAllowed.Count > 0 && listViewAllowed[0] == "1")
                {
                    switch (listTagAction[0])
                    {
                        case "AgencySearch":
                            dtMenu.Rows.Add(new object[] { "RECHERCHE AGENCE", "AgencySearch.aspx" });
                            break;
                    }
                }
            }

            rptMenu.DataSource = dtMenu;
            rptMenu.DataBind();

            if (rptMenu.Items.Count == 0)
            {
                rptMenu.Visible = false;
                panelMenuEmpty.Visible = true;
            }
        }
    }
    protected void btnMenuChoosen_Click(object sender, EventArgs e)
    {
        if (hfPageRequest.Value.Trim().Length > 0)
        {
            Response.Redirect(hfPageRequest.Value, true);
        }
    }
}
