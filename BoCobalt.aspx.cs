﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using XMLMethodLibrary;

public partial class BoCobalt : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindMenu();
        }
    }

    protected void BindMenu()
    {
        string sXmlOut = tools.GetRights(authentication.GetCurrent().sToken, "SC_BoCobalt_Menu");

        List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Page", "RC");

        if (listRC.Count > 0 && listRC[0] == "0")
        {
            List<string> listAction = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/Page/Action");
            DataTable dtMenu = new DataTable();
            dtMenu.Columns.Add("Ref");
            dtMenu.Columns.Add("Label");
            dtMenu.Columns.Add("Value");

            /*
            listAction.Add("<Action TagAction=\"OpDiverses\" ActionAllowed=\"1\" ViewAllowed=\"1\" />");
            listAction.Add("<Action TagAction=\"Monitoring\" ActionAllowed=\"1\" ViewAllowed=\"1\" />");
            listAction.Add("<Action TagAction=\"ApiAccount\" ActionAllowed=\"1\" ViewAllowed=\"1\" />");
            listAction.Add("<Action TagAction=\"ApiHomeBanking\" ActionAllowed=\"1\" ViewAllowed=\"1\" />");
            listAction.Add("<Action TagAction=\"Requetteur\" ActionAllowed=\"1\" ViewAllowed=\"1\" />");
             * INSERT INTO GenericWeb.PagesRights (TagPage, TagAction, ActionDescription) VALUES ('SC_BoCobalt_Menu', 'OpDiverses', 'gestion des opérations diverses')
INSERT INTO GenericWeb.PagesRights (TagPage, TagAction, ActionDescription) VALUES ('SC_BoCobalt_Menu', 'Monitoring', 'Affichage de Grafana')
INSERT INTO GenericWeb.PagesRights (TagPage, TagAction, ActionDescription) VALUES ('SC_BoCobalt_Menu', 'ApiAccount', 'API Cobalt Core Banking')
INSERT INTO GenericWeb.PagesRights (TagPage, TagAction, ActionDescription) VALUES ('SC_BoCobalt_Menu', 'ApiHomeBanking', 'API Cobalt Home Banking')
INSERT INTO GenericWeb.PagesRights (TagPage, TagAction, ActionDescription) VALUES ('SC_BoCobalt_Menu', 'Requetteur', 'API Cobalt Home Banking')
INSERT INTO GenericWeb.PagesRights (TagPage, TagAction, ActionDescription) VALUES ('SC_BoCobalt_OpDiverse', 'SaisieOp', 'Saisie Operation diverse')
INSERT INTO GenericWeb.PagesRights (TagPage, TagAction, ActionDescription) VALUES ('SC_BoCobalt_OpDiverse', 'ValidationOp', 'Saisie Operation diverse')
INSERT INTO GenericWeb.PagesRights (TagPage, TagAction, ActionDescription) VALUES ('SC_Menu', 'BoCobalt', 'lien vers la page BoCobalt')
             * INSERT INTO GenericWeb.ProfileDefinition (TAGProfile, RefPageRight, ViewAllowed, ActionAllowed) SELECT 'ADMIN', PR.RefPageRight, 1, 1 FROM GenericWeb.PagesRights PR WITH(READUNCOMMITTED) where TagPage like '%cobalt%'
            */

            for (int i = 0; i < listAction.Count; i++)
            {
                List<string> listTagAction = CommonMethod.GetAttributeValues(listAction[i], "Action", "TagAction");
                List<string> listViewAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ViewAllowed");

                if (listTagAction.Count > 0 && listViewAllowed.Count > 0 && listViewAllowed[0] == "1")
                {
                    switch (listTagAction[0])
                    {
                        case "OpDiverses":
                            dtMenu.Rows.Add(new object[] { "01", "Opérations diverses", "BoCobalt_OpDiverses.aspx" });
                            
                            break;
                        case "Monitoring":
                            dtMenu.Rows.Add(new object[] { "02", "Monitoring", "BoCobalt_Monitoring.aspx" });
                            break;
                        case "ApiAccount":
                            dtMenu.Rows.Add(new object[] { "03", "API Account", "BoCobalt_ApiAccount.aspx" });
                            break;
                        case "ApiHomeBanking":
                            dtMenu.Rows.Add(new object[] { "04", "API HomeBanking", "BoCobalt_ApiHomeBanking.aspx" });
                            break;
                        case "Requetteur":
                            dtMenu.Rows.Add(new object[] { "05", "Requetteur", "BoCobalt_Requetteur.aspx" });
                            break;
                    }
                }
            }

            DataView dv = dtMenu.DefaultView;
            dv.Sort = "Ref asc";

            rptMenu.DataSource = dv.ToTable();
            rptMenu.DataBind();
        }
    }
}
