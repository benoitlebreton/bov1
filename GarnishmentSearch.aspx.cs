﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using XMLMethodLibrary;

public partial class GarnishmentSearch : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CheckRights();

        if (!IsPostBack)
        {
            if (Request.Params["status"] != null)
                GarnishmentList1.Status = Request.Params["status"].ToString();
            else
                GarnishmentList1.Status = "Untreated";
        }
    }

    protected void CheckRights()
    {
        string sXmlOut = tools.GetRights(authentication.GetCurrent().sToken, "SC_GarnishmentSearch");

        List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Page", "RC");

        if (listRC.Count == 0 || listRC[0] != "0")
        {
            Response.Redirect("Default.aspx", true);
        }
    }
}