﻿<%@ Page Title="Vos relevés - mon Compte-Nickel" Language="C#" AutoEventWireup="true" CodeFile="vos-releves_Frame.aspx.cs" Inherits="mes_releves" MaintainScrollPositionOnPostback="true" %>
<!--
    <xhtmlConformance mode="Transitional"/>
-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Vos relevés - mon Compte-Nickel</title>
    <script src="Scripts/jquery-1.8.3.js" type="text/javascript"></script>
    <script src="Scripts/jquery-ui-1.9.2.custom.min.js" type="text/javascript"></script>
    <link type="text/css" href="Styles/UI/jquery-ui-1.9.2.custom.css" rel="stylesheet" />
    <link type="text/css" href="Styles/Site.css" rel="stylesheet" />

    <%--<script type="text/javascript" src="Scripts/jquery.fileDownload.js"></script>--%>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#popup-statement-file').dialog({
                autoOpen: false,
                resizable: false,
                draggable: false,
                modal: true,
                width: 850,
                close: function (event, ui) { $('#pdfFrame').attr('src', ''); }
            });
            $('#popup-statement-file-not-available').dialog({
                autoOpen: false,
                resizable: false,
                draggable: false,
                modal: true,
                width: 400
            });
            $('#popup-search').dialog({
                autoOpen: false,
                resizable: false,
                draggable: false,
                modal: true,
                width: 300,
                title: "Rechercher un relevé",
                buttons: [{ text: $('#<%=btnSearch.ClientID%>').val(), click: function () { $("#<%=btnSearch.ClientID %>").click(); $(this).dialog('close'); } }]
            });
            $("#popup-search").parent().appendTo(jQuery("form:first"));
        });

        function ShowSearch() {
            $('#popup-search').dialog('open');
        }

        function DownLoadFile(event, control) {
            ShowLoading();
            event = event || window.event //For IE
            event.stopPropagation();
        }

        function execDownloadFile(sFile) {
            //console.log(sFile);
            $('#<%=hfFileToDownload.ClientID%>').val(sFile);
            $('#<%=btnFileToDownload.ClientID%>').click();
        }

        var nbTryFindFile;
        function ShowFile(title, file)
        {
            console.log(file);
            nbTryFindFile = 5;
            ShowLoading();
            $.ajax({
                url: file, 
                success: function (data) {
                    console.log(file + " found");
                    $('#pdfFrame').attr('src', file);
                    $('#popup-statement-file').dialog({ title: title });
                    $('#popup-statement-file').dialog('open');
                    HideLoading();
                },
                error: function (data) {
                    console.log('get from cloud');
                    $.ajax({
                        url: "./ws_tools.asmx/GetClientDocForDownload",
                        data: '{ "sFileName": "' + file + '", "sRefCustomer": "<%=hfRefCustomer.Value%>"}', //, culture: 'fr-FR', order: 'ASC' 
                        dataType: "json",
                        type: "POST",
                        cache: false,
                        contentType: "application/json; charset=utf-8",
                        dataFilter: function (data) { return data; },
                        success: function (data) {
                            var BoolData = (data == "True") ? true : false;
                            if (BoolData) {
                                ShowFileGetted(title, file);
                            }
                            else {
                                $('#popup-statement-file-not-available').dialog({ title: title });
                                $('#popup-statement-file-not-available').dialog('open');
                                HideLoading();
                            }
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            ShowFileGetted(title,file);
                        }
                    });
                }
            });
        }
        
        function ShowFileGetted(title, file) {
            $.get(file).done(function () {
                $('#pdfFrame').attr('src', file);
                $('#popup-statement-file').dialog({ title: title });
                $('#popup-statement-file').dialog('open');
                HideLoading();
            }).fail(function () {
                nbTryFindFile--;
                if (nbTryFindFile > 0) {
                    setTimeout(function () { ShowFileGetted(title, file); }, 2000);
                }
                else {
                    $('#popup-statement-file-not-available').dialog({ title: title });
                    $('#popup-statement-file-not-available').dialog('open');
                    HideLoading();
                }
            });
        }

        function InitDropDocList() {
            $('.block-files-title').bind('click', function () {
                var blockFiles = $(this).parents('.block-files').find('.block-files-list')[0];
                //console.log($(blockFiles).is(':visible'));
                if ($(blockFiles).is(':visible'))
                    $(blockFiles).slideUp('400');
                else $(blockFiles).slideDown('400');
            });
        }

        function ShowLoading() {
            var width = $('#<%=panelStatementList.ClientID%>').width();
            var height = $('#<%=panelStatementList.ClientID%>').height();
            $('#ar-loading').css('width', width);
            $('#ar-loading').css('height', height);

            $('#ar-loading').show();

            $('#ar-loading').position({ my: "left top", at: "left top", of: '#<%=panelStatementList.ClientID%>' });
        }
        function HideLoading() {
            $('#ar-loading').hide();
        }
    </script>

    <%--<style type="text/css">
        
        .annual-fees-row {
            color:#fff;
            font-family:"VAG Rounded";
        }
        .annual-fees-row td {
            background-color:#BEBDBD;
        }

        .block-files-title {
            width:100%;
            padding:15px 0;
            cursor:pointer;
        }
        .block-files-list {
            display:none;
        }
        .block-file {
            width:50%;            
            margin:auto;
            padding-top:10px;
            cursor:pointer;
        }
        .statement-table-alternate-row .table-cell .block-file {
            border-top:1px dashed #ddd;
        }
        .statement-table-row .table-cell .block-file {
            border-top:1px dashed #ccc;
        }
        .block-file-content {
            
        }
        
        .statement-list {
            width:100%;
            border-collapse:collapse;
            font-family:'VAG Rounded SSi'
        }
        .statement-list .table-head {
            text-align:center;
            font-family:'Aracne Regular';
            font-size:1.2em;
            background-color: #f57527;
            color: #fff;
            padding:15px 0;
            font-weight:bold;
        }
        .statement-table-row .table-cell {
            background-color: #ebecec;
        }
        .statement-table-alternate-row .table-cell {
            background-color:#fff;
        }
        #ar-loading {
            display: none;
            position: absolute;
            background-color: rgba(249, 249, 249, 0.5);
            background-image:url('Styles/Img/loading.gif');
            background-size: 35px 27px;
            background-repeat:no-repeat;
            background-position:center;
            z-index: 100;
        }
    </style>--%>

    <style type="text/css">
        html, body {
            background-color:#fff;
            font-family: Arial, Verdana;
            text-transform:uppercase;
        }
        body {
            padding:20px;
        }

        .page-title {
            background-color:#f57527;
            color:#fff;
            font-weight:bold;
            text-transform:uppercase;
            padding:10px 0;
            font-size:1.2em;
            text-align:center;
        }
        .operation-table {
            border-collapse:collapse;
        }

        .operation-table thead {
            background-color: #f57527;
            color: #fff;
        }

        .operation-table thead tr th{
            background-color:#f57527;
            padding:0;
            height:50px;
            color:#fff;
            text-align:center;
        }
        .operation-table thead tr th div {
            border-right:1px solid #d7d7d7;
            border-left:1px solid #c2c3c3;
            padding:15px 5px;
            height:15px;
        }
        .operation-table thead tr th:nth-of-type(4) div {
            padding-top:6px;
            height:24px;
        }

        .operation-table td {
            border:0;
        }

        .operation-table-row td div, .operation-table-alternate-row td div, .operation-table-row-red td div, .operation-table-alternate-row-red td div {
            border-right:1px solid #d7d7d7;
            border-left:1px solid #c2c3c3;
            padding:7px 5px 4px 5px;
            height:20px;
        }

        .operation-table-row
        {
    
        }
        .operation-table-row td, .operation-table-alternate-row td, .operation-table-row-red td, .operation-table-alternate-row-red td {
            padding:4px 0;
            margin:0;
        }
        /*.operation-table-alternate-row td {
            padding:3px 0;
        }*/
        .operation-table-row td{
            background-color:#ebecec;
        }

        .operation-table-alternate-row, .operation-table-row, .operation-table-row-red, .operation-table-alternate-row-red {
            /*border-radius:8px;*/
            background-color:transparent;
            cursor:pointer;
        }
        .operation-table-alternate-row td:first-of-type, .operation-table-row td:first-of-type
        {
            /*border-bottom-left-radius:8px;
            border-top-left-radius:8px;*/
        }
        .operation-table-alternate-row td:last-of-type, .operation-table-row td:last-of-type
        {
            /*border-bottom-right-radius:8px;
            border-top-right-radius:8px;*/
        }
        .operation-table-alternate-row td:last-of-type .tooltip, .operation-table-row td:last-of-type .tooltip, .operation-table-row-red td:last-of-type .tooltip, .operation-table-row-red td:last-of-type .tooltip {
            position:relative;
            top:-2px;
        }

        .operation-table-alternate-row
        {
    
        }
        .operation-table-alternate-row td{
            background-color:#fff;
        }

        .operation-table-row-red td, .operation-table-alternate-row-red td {
            /*background-color:#f36868;*/
            color:#f36868;
        }
        .operation-table-alternate-row-red td {
            /*background-color:#f79797;*/
        }

        .operation-table-row td:last-of-type div, .operation-table-alternate-row td:last-of-type div, .operation-table thead tr th:last-of-type div, operation-table-row-red td:last-of-type div, .operation-table-row-red td:last-of-type div {
            border-right:0;
        }
        .operation-table-row td:first-of-type div, .operation-table-alternate-row td:first-of-type div, .operation-table thead tr th:first-of-type div, operation-table-row-red td:first-of-type div, .operation-table-row-red td:first-of-type div {
            border-left:0;
        }

        .annual-fees-row {
            color:#fff;
            font-weight:bold;
        }
        .annual-fees-row td {
            background-color:#BEBDBD;
        }

        .block-files-title {
            width:100%;
            padding:15px 0;
            cursor:pointer;
        }
        .block-files-list {
            display:none;
        }
        .block-file {
            width:50%;            
            margin:auto;
            padding-top:10px;
            cursor:pointer;
        }
        .statement-table-alternate-row .table-cell .block-file {
            border-top:1px dashed #ddd;
        }
        .statement-table-row .table-cell .block-file {
            border-top:1px dashed #ccc;
        }
        .block-file-content {
            
        }
        
        .statement-list {
            width:100%;
            border-collapse:collapse;
        }
        .statement-list .table-head {
            text-align:center;
            font-size:1.2em;
            background-color: #f57527;
            color: #fff;
            padding:15px 0;
            font-weight:bold;
        }
        .statement-table-row .table-cell {
            background-color: #ebecec;
        }
        .statement-table-alternate-row .table-cell {
            background-color:#fff;
        }
        .result-div-orange {
            text-align:left;
            text-transform:none;
            background-color:#f57527;
            padding:10px;
            color:#fff;
        }
        .uppercase {
            text-transform:uppercase;
        }
        
    </style>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true" EnableScriptLocalization="true"
            EnableScriptGlobalization="true" EnablePageMethods="true" ScriptMode="Release" LoadScriptsBeforeUI="false"></asp:ScriptManager>
    <div class="page-title">
        Consultez en ligne <asp:Label ID="lblYouTitle" runat="server">vos</asp:Label> relevés ou téléchargez les
    </div>
    <div class="page-content">
        <div style="margin:30px 0 30px 0">
        <div style="float:left;width:40%;text-align:left;">
            <img src="Styles/Img/releve.png" style="width:350px;position:relative;left:-20px" />
        </div>
        <div style="float:right;width:60%">
            <div id="ar-loading" style="display:none;position:absolute;background-color:rgba(249, 249, 249, 0.5);z-index:100"></div>
            <asp:Panel ID="panelStatementList" runat="server">
                <asp:Repeater ID="rptStatement" runat="server">
                    <HeaderTemplate>
                        <table cellpadding="0" cellspacing="0" style="width:100%" class="table-releve operation-table">
                            <%--<thead>
		                        <tr>
			                        <th style="text-align:center;font-size:1.2em" class="font-AracneRegular">Consultez en ligne vos relevés ou téléchargez les</th>
		                        </tr>
	                        </thead>--%>
                            <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr class='<%# Container.ItemIndex % 2 == 0 ? "operation-table-row" : "operation-table-alternate-row" %> <%# GetAnnualFeesStyle(Eval("Date").ToString()) %>' onclick="ShowFile('Relevé de compte <%# Eval("Date") %>', '<%# Eval("File") %>');">
                            <td class="uppercase" style="text-align:center;height:30px;padding-bottom:5px;">
                                <div style="width:50%;margin:auto">
                                    <div style="width:80%;float:left;">
                                        <%# Eval("Date") %>
                                    </div>
                                    <div style="width:10%;float:right;text-align:center;">
                                        <span class="tooltip font-normal" tooltiptext="Télécharger" style="position:relative;top:-5px;">
                                            <asp:ImageButton ID="btnDownload" runat="server" ImageUrl="Styles/Img/telecharger.png" OnClientClick="DownLoadFile(event);" OnClick="btnDownload_Click" style="position:relative;left:-3px" />
                                            <asp:HiddenField ID="hdnFilePath" runat="server" Value='<%# Eval("File") %>' />
                                        </span>
                                    </div>
                                    <div style="clear:both"></div>
                                </div>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                            </tbody>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>

                <asp:Repeater ID="rptStatementPRO" runat="server" OnItemDataBound="rptStatementPRO_ItemDataBound">
                    <HeaderTemplate>
                        <!--<table cellpadding="0" cellspacing="0" style="width:100%" class="operation-table">
                            <thead>
		                        <tr>
			                        <th style="text-align:center;font-size:1.2em" class="font-AracneRegular">Consultez en ligne vos relevés ou téléchargez les</th>
		                        </tr>
	                        </thead>
                            <tbody>-->
                        <div class="table-releve table statement-list">
                            <div class="table-row">
                                <div class="table-cell table-head">
                                    Consultez en ligne vos relevés ou téléchargez les
                                </div>
                            </div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <div class='table-row <%# Container.ItemIndex % 2 == 0 ? "statement-table-row" : "statement-table-alternate-row" %>'>
                            <div class="table-cell uppercase" style="text-align:center;">
                                <div class="block-files">
                                    <div class="block-files-title">
                                        <b><asp:Label ID="lblTitle" runat="server" Text='<%# Eval("Title") %>'></asp:Label></b>
                                    </div>
                                    <div class="block-files-list">
                                        <asp:Repeater ID="rptStatementPROFileList" runat="server">
                                            <ItemTemplate>
                                                <div class="block-file" onclick="ShowFile('<%# HttpUtility.HtmlEncode(Eval("FileName")) %> <%# Eval("Date") %>', '<%# Eval("File") %>');">
                                                    <div class="block-file-content">
                                                        <div style="width:80%;float:left;">
                                                            <%# Eval("FileName") %>
                                                        </div>
                                                        <div style="width:10%;float:right;text-align:center;">
                                                            <span class="tooltip font-normal" tooltiptext="Télécharger" style="position:relative;top:-5px;">
                                                                <asp:ImageButton ID="btnDownload" runat="server" ImageUrl="Styles/Img/telecharger.png" OnClientClick="DownLoadFile(event);" OnClick="btnDownload_Click" style="position:relative;left:-3px" />
                                                                <asp:HiddenField ID="hdnFilePath" runat="server" Value='<%# Eval("File") %>' />
                                                            </span>
                                                        </div>
                                                        <div style="clear:both"></div>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                    <FooterTemplate>
                        </div>    
                        <!--</tbody>
                        </table>-->
                    </FooterTemplate>
                </asp:Repeater>

                <div style="text-align:center;">
                    <asp:Panel ID="panelResult" runat="server" CssClass="result-div-orange" Visible="false" style="margin-top:10px">
                        <asp:Label ID="lblResult" runat="server"></asp:Label>
                    </asp:Panel>
                    <div style="margin-top:40px;">
                        <input type="button" value="Rechercher un relevé" class="button" onclick="ShowSearch();" />
                    </div>
                </div>
            </asp:Panel>

            <asp:Panel ID="panelStatementListEmpty" runat="server" Visible="false">
                <div class="font-bold uppercase" style="text-align:center">
                    Vous n'avez aucun relevé disponible.
                </div>
            </asp:Panel>

            <asp:HiddenField ID="hdnDLRowIndex" runat="server" />
        </div>

        <div style="clear:both"></div>
    </div>
    </div>

    <div id="popup-search" style="display:none">
        <div style="width:180px;float:left;">
            <div class="font-bold uppercase" style="margin:5px 0">
                Mois
            </div>
            <div>
                <asp:DropDownList ID="ddlMonths" runat="server" CssClass="uppercase" style="width:180px" AppendDataBoundItems="true">
                    <asp:ListItem Text="" Value="" Selected="True"></asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div style="width:80px;float:right;">
            <div class="font-bold uppercase" style="margin:5px 0">
                Année
            </div>
            <div>
                <asp:DropDownList ID="ddlYears" runat="server" style="width:80px" CssClass="uppercase" AppendDataBoundItems="true">
                    <asp:ListItem Text="" Value="" Selected="True"></asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div>
            <asp:Button ID="btnSearch" runat="server" Text="Rechercher" OnClick="btnSearch_Click" CssClass="orange-big-button" style="display:none" />
        </div>
    </div>

    <div id="popup-statement-file" style="display:none;text-align:center">
        <iframe id="pdfFrame" width="800" height="400"></iframe>
    </div>

    <div id="popup-statement-file-not-available" style="display:none;text-align:center;padding-top:25px">
        Momentanément indisponible.<br />Veuillez réessayer ulterieurement.
    </div>

    <script type="text/javascript">
        Sys.Application.add_init(function(){ 
            Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
        });
        Sys.Application.add_init(function () {
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        });
        var sRequestControlID = "";
        function BeginRequestHandler(sender, args) {
            pbControl = args.get_postBackElement();
            
            $('#<%=panelResult.ClientID%>').slideUp();
            //console.log(pbControl.id);
            if (pbControl.id == '<%=btnSearch.ClientID %>') {
                var width = $('.table-releve').width();
                var height = $('.table-releve').height();
                $('#ar-loading').css('width', width);
                $('#ar-loading').css('height', height);

                $('#ar-loading').show();

                $('#ar-loading').position({ my: "left top", at: "left top", of: ".table-releve" });
            }
            sRequestControlID = pbControl.id;
        }
        function EndRequestHandler(sender, args) {
            $('#ar-loading').hide();
        }
    </script>
    
    <asp:HiddenField ID="hfRefCustomer" runat="server" Value="" />
    <asp:HiddenField ID="hfFileToDownload" runat="server" Value="" />
    <asp:Button ID="btnFileToDownload" runat="server" OnClick="btnFileToDownload_Click" style="display:none"/>
    </form>
</body>
</html>