﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Xml.Linq;
using XMLMethodLibrary;

public partial class RegistrationSearch : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack)
        {
            string[] eventArgs = Page.Request["__EVENTARGUMENT"].ToString().Split(';');
            if (eventArgs[0].ToString().ToLower() == "showregistration" && eventArgs.Length > 1)
            {
                //registrationDetails.RegistrationNumber = eventArgs[1];
                //divRegistrationDetails.Visible = true;
                divRegistrationSearch.Visible = false;

                //registrationDetails.Visible = true;
                panelRegistrationSearch.Visible = false;

                btnPrevious.Visible = true;
                btnPrevious_2.Visible = false;
                btnValidate.Visible = true;

                Response.Redirect("RegistrationDetails.aspx?No=" + eventArgs[1]);
            }
            else if (eventArgs[0].ToString().ToLower() == "birthdatepicker" && eventArgs.Length > 1)
            {
                hfClientBirthDate.Value = eventArgs[1];
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "InitAccordion(); initTooltip();FilterStatusDisplayManagement(); clickRegistrationSearch(1);", true);
            }
            else if (eventArgs[0].ToString().ToLower() == "getregistrationsearchfilter" && eventArgs.Length > 1)
            {
                Session["RegistrationSearchFilter"] = eventArgs[1];
                divGridView.Visible = true;
                divGridViewInfo.Visible = true;
                gvRegistrationSearchResult.DataSource = GetRegistrationSearch(CreateXmlStringFromFilter(eventArgs[1]));
                gvRegistrationSearchResult.DataBind();

                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "InitAccordion();initTooltip();FilterStatusDisplayManagement();", true);
            }
        }
        else
        {
            CheckRights();

            divRegistrationSearch.Visible = true;
            //divRegistrationDetails.Visible = false;

            //registrationDetails.Visible = false;
            panelRegistrationSearch.Visible = true;
            btnPrevious.Visible = false;
            btnPrevious_2.Visible = false;
            btnValidate.Visible = false;

            try
            {
                ddlAgencyID.Items.Clear();
                DataTable dt = tools.GetAgencyList();
                DataRow dr = dt.NewRow();
                dr.ItemArray = new object[] { "TOUS", " " };
                dt.Rows.InsertAt(dr, 0);
                //ddlAgencyID.DataSource = GetAgencyList();
                ddlAgencyID.DataSource = dt;
                ddlAgencyID.DataBind();
            }
            catch (Exception ex)
            {
            }

            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "InitAccordion();initTooltip();FilterStatusDisplayManagement();", true);
            hfBeginDate.Value = DateTime.Now.ToString("dd/MM/yyyy");
            hfEndDate.Value = DateTime.Now.ToString("dd/MM/yyyy");
            hfPeriod.Value = "4";

            if (Request.Params.Count > 0)
            {
                if (Request.Params["NotChecked"] != null && Request.Params["NotChecked"].ToString().Length > 0
                    && Request.Params["NotChecked"].ToString().Trim().ToUpper() == "Y")
                {
                    string sParams = "RefStatus!15,StatusCheck!W,Period!3";
                    Session["RegistrationSearchFilter"] = sParams;
                    divGridView.Visible = true;
                    divGridViewInfo.Visible = true;
                    gvRegistrationSearchResult.DataSource = GetRegistrationSearch(CreateXmlStringFromFilter(sParams));
                    gvRegistrationSearchResult.DataBind();

                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "InitAccordion();initTooltip();FilterStatusDisplayManagement();", true);
                }
                else
                    getRegistrationSearchFilter();
            }
            else
                getRegistrationSearchFilter();
            
        }
        RegistrationSearchByAgencyPeriod.PeriodChanged += new EventHandler(RegistrationSearchByAgencyPeriod_PeriodChanged);
        //registrationDetails.PreviousClicked += new EventHandler(onClickPreviousButton);
        
    }

    protected void CheckRights()
    {
        string sXmlOut = tools.GetRights(authentication.GetCurrent().sToken, "SC_RegistrationSearch");

        List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Page", "RC");

        if (listRC.Count == 0 || listRC[0] != "0")
        {
            Response.Redirect("Default.aspx", true);
        }
    }

    protected void onClickPreviousButton(object sender, EventArgs e)
    {
        //if (registrationDetails.Visible && !searchFilter.Visible)
        //{
        //    btnPrevious.Visible = false;
        //    btnPrevious_2.Visible = false;
        //    btnValidate.Visible = false;
        //    registrationDetails.Visible = false;
        //    divRegistrationDetails.Visible = false;
        //    divRegistrationSearch.Visible = true;
        //    searchFilter.Visible = true;
        //    registrationDetails.RegistrationNumber = "";

        //    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "InitAccordion();onChangePageNumber();", true);
        //}

        //Page_Load(null, null);
    }

    protected void onClickValidate(object sender, EventArgs e)
    {
        
    }

    protected void getRegistrationSearchFilter()
    {
        if (Session["RegistrationSearchFilter"] != null && Session["RegistrationSearchFilter"].ToString().Length > 0)
        {
            divGridView.Visible = true;
            divGridViewInfo.Visible = true;
            gvRegistrationSearchResult.DataSource = GetRegistrationSearch(CreateXmlStringFromFilter(Session["RegistrationSearchFilter"].ToString()));
            gvRegistrationSearchResult.DataBind();

            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "InitAccordion();initTooltip();FilterStatusDisplayManagement();", true);
        }
    }

    protected void RegistrationSearchByAgencyPeriod_PeriodChanged(object sender, EventArgs e)
    {
        //hfBeginDate.Value = RegistrationSearchByAgencyPeriod.sBeginDate;
        //hfEndDate.Value = RegistrationSearchByAgencyPeriod.sEndDate;
        //hfPeriod.Value = RegistrationSearchByAgencyPeriod.sPeriod;

        //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "InitAccordion(); clickRegistrationSearch(1);", true);
        string sValues = "";
        sValues = "AgencyID!" + ddlAgencyID.SelectedValue;
        sValues += ",BeginDate!" + RegistrationSearchByAgencyPeriod.sBeginDate;
        sValues += ",EndDate!" + RegistrationSearchByAgencyPeriod.sEndDate;
        sValues += ",Period!" + RegistrationSearchByAgencyPeriod.sPeriod;

        sValues += ",RefStatus!" + rblStatus.SelectedValue;
        sValues += ",StatusCheck!" + rblStatusCheck.SelectedValue;
        sValues += ",PageIndex!1";

        Session["RegistrationSearchFilter"] = sValues;
        divGridView.Visible = true;
        divGridViewInfo.Visible = true;
        gvRegistrationSearchResult.DataSource = GetRegistrationSearch(CreateXmlStringFromFilter(sValues));
        gvRegistrationSearchResult.DataBind();
        upGridView.Update();

        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "InitAccordion();initTooltip();FilterStatusDisplayManagement();hideLoading();", true);
    }

    protected string getCheckStatus(string sValue, string sType)
    {
        string sReturnValue = "";

        if (sType == "AlternateText")
            sReturnValue = getCheckStatusLabel(sValue);
        else if (sType == "ImageUrl")
            sReturnValue = getCheckStatusImageUrlColor(sValue);
        else if (sType == "Tooltip")
        {
            if (sValue.Split(';').Length > 2)
            {
                if (sValue.Split(';')[0].Trim() == "W")
                {
                    sReturnValue = "<span style=\"color:" + getCheckStatusColor(sValue.Split(';')[0]) + "\">" + getCheckStatusLabel(sValue.Split(';')[0]) + "</span>";
                }
                else
                {
                    sReturnValue = "évaluée <span style=\"color:" + getCheckStatusColor(sValue.Split(';')[0]) + "\">" + getCheckStatusLabel(sValue.Split(';')[0]) +
                        "</span> par " + sValue.Split(';')[2] + " le " + sValue.Split(';')[1];
                    //sReturnValue = "évalué "+ getCheckStatusLabel(sValue.Split(';')[0]) + " par " + sValue.Split(';')[2] + " le " + sValue.Split(';')[1];
                }
            }
        }

        return sReturnValue;
    }

    protected string getCheckStatusLabel(string sStatus)
    {
        string sReturnValue = "";
        switch (sStatus)
        {

            case "G":
                sReturnValue = "Conforme";
                break;
            case "O":
                sReturnValue = "Suspecte";
                break;
            case "R":
                sReturnValue = "Non conforme";
                break;
            case "W":
                sReturnValue = "Non vérifiée";
                break;
        }

        return sReturnValue;
    }

    protected string getCheckStatusImageUrlColor(string sStatus)
    {
        string sImageUrl = "";

        switch (sStatus)
        {
            case "G":
                sImageUrl = "./Styles/Img/green_status.png";
                break;
            case "O":
                sImageUrl = "./Styles/Img/yellow_status.png";
                break;
            case "R":
                sImageUrl = "./Styles/Img/red_status.png";
                break;
            case "W":
                sImageUrl = "./Styles/Img/gray_status.png";
                break;
            default:
                sImageUrl = "";
                break;
        }

        return sImageUrl;
    }

    protected string getCheckStatusColor(string sStatus)
    {
        string sColor = "";

        switch (sStatus)
        {
            case "G":
                sColor = "green";
                break;
            case "O":
                sColor = "orange";
                break;
            case "R":
                sColor = "red";
                break;
            default:
                sColor = "";
                break;
        }

        return sColor;
    }

    protected string CreateXmlStringFromFilter(string sParameters)
    {
        string sXML = "";
        authentication auth = authentication.GetCurrent();

        try
        {

            int iPageSize = int.Parse(ddlNbResult.SelectedValue);
            string PackNumber = GetParam(sParameters, ',', '!', "PackNumber");
            string RegistrationCode = GetParam(sParameters, ',', '!', "RegistrationNumber");
            string ClientLastName = GetParam(sParameters, ',', '!', "ClientLastName");
            string ClientFirstName = GetParam(sParameters, ',', '!', "ClientFirstName");
            string ClientBirthDate = GetParam(sParameters, ',', '!', "ClientBirthDate");
            string PhoneNumber = GetParam(sParameters, ',', '!', "PhoneNumber");
            string BeginDate = GetParam(sParameters, ',', '!', "BeginDate");
            string EndDate = GetParam(sParameters, ',', '!', "EndDate");
            string AgencyID = GetParam(sParameters, ',', '!', "AgencyID");
            string CashierCheck = GetParam(sParameters, ',', '!', "CashierCheck");
            string StatusCheck = GetParam(sParameters, ',', '!', "StatusCheck");
            string RefStatus = GetParam(sParameters, ',', '!', "RefStatus");
            string Period = GetParam(sParameters, ',', '!', "Period");
            string ServiceTAG = GetParam(sParameters, ',', '!', "ServiceTAG");

            int iPageIndex;
            if (!int.TryParse(GetParam(sParameters, ',', '!', "PageIndex"), out iPageIndex))
                iPageIndex = 1;

            if (Period.Trim() != "3")
            {
                RegistrationSearchByAgencyPeriod.getDateFromPeriodValue(Period.Trim(), out BeginDate, out EndDate);
            }

            sXML = new XElement("ALL_XML_IN",
                        new XElement("Registration",
                            new XAttribute("PageSize", iPageSize.ToString()),
                            new XAttribute("PageIndex", iPageIndex.ToString()),
                            new XAttribute("CashierToken", auth.sToken),
                            new XAttribute("RegistrationCode", RegistrationCode),
                            new XAttribute("PackNumber", PackNumber),
                            new XAttribute("LastName", ClientLastName),
                            new XAttribute("FirstName", ClientFirstName),
                            new XAttribute("BirthDate", ClientBirthDate),
                            new XAttribute("PhoneNumber", PhoneNumber),
                            new XAttribute("RefStatus", RefStatus),
                            new XAttribute("DateFrom", BeginDate),
                            new XAttribute("DateTo", EndDate),
                            new XAttribute("NbMinutesLastChange", ""),
                            new XAttribute("PartnerID", ""),
                            new XAttribute("PartnerAgencyID", ""),
                            new XAttribute("StatusCheck", StatusCheck),
                            new XAttribute("AgencyID", AgencyID),
                            new XAttribute("Period", ((Period.Trim().Length > 0) ? Period : "4").ToString()),
                //new XAttribute((CashierCheck == "Y")? "CashierFinalCheck":"CustomerFinalCheck", "1"),
                //new XAttribute("CashierFinalCheck", "1"),
                //new XAttribute("CustomerFinalCheck", "1"),
                            new XAttribute("Culture", "fr-FR"),
                            new XAttribute("ServiceTAG", ServiceTAG)
                        )
                    ).ToString();

            initFieldsFromXML(sXML);
        }
        catch (Exception ex)
        {
        }

        return sXML;
    }

    protected void initFieldsFromXML(string sXml)
    {
        //Par défaut date personnalisée
        RegistrationSearchByAgencyPeriod.PredifinedPeriodChoice.SelectedValue = "3";
        hfPanelActive.Value = "";

        if (tools.GetValueFromXml(sXml, "ALL_XML_IN/Registration", "PageSize").Length > 0)
            if (ddlNbResult.Items.FindByValue(tools.GetValueFromXml(sXml, "ALL_XML_IN/Registration", "PageSize")) != null)
                ddlNbResult.SelectedValue = tools.GetValueFromXml(sXml, "ALL_XML_IN/Registration", "PageSize");

        if (tools.GetValueFromXml(sXml, "ALL_XML_IN/Registration", "PageNumber").Length > 0)
            if (ddlPage.Items.FindByValue(tools.GetValueFromXml(sXml, "ALL_XML_IN/Registration", "PageNumber")) != null)
                ddlPage.SelectedValue = tools.GetValueFromXml(sXml, "ALL_XML_IN/Registration", "PageNumber");

        if (tools.GetValueFromXml(sXml, "ALL_XML_IN/Registration", "RegistrationCode").Length > 0)
        {
            txtRegistrationNumber.Text = tools.GetValueFromXml(sXml, "ALL_XML_IN/Registration", "RegistrationCode");
            hfPanelActive.Value = "1";
        }

        if (tools.GetValueFromXml(sXml, "ALL_XML_IN/Registration", "PackNumber").Length > 0)
        {
            txtPackNumber.Text = tools.GetValueFromXml(sXml, "ALL_XML_IN/Registration", "PackNumber");
            hfPanelActive.Value = "0";
        }

        if (tools.GetValueFromXml(sXml, "ALL_XML_IN/Registration", "LastName").Length > 0)
        {
            txtClientLastName.Text = tools.GetValueFromXml(sXml, "ALL_XML_IN/Registration", "LastName");
            hfPanelActive.Value = "3";
        }

        if (tools.GetValueFromXml(sXml, "ALL_XML_IN/Registration", "FirstName").Length > 0)
        {
            txtClientFirstName.Text = tools.GetValueFromXml(sXml, "ALL_XML_IN/Registration", "FirstName");
            hfPanelActive.Value = "3";
        }

        if (tools.GetValueFromXml(sXml, "ALL_XML_IN/Registration", "BirthDate").Length > 0)
        {
            txtClientBirthDate.Text = tools.GetValueFromXml(sXml, "ALL_XML_IN/Registration", "BirthDate");
            hfPanelActive.Value = "3";
        }

        if (tools.GetValueFromXml(sXml, "ALL_XML_IN/Registration", "PhoneNumber").Length > 0)
        {
            txtPhoneNumber.Text = tools.GetValueFromXml(sXml, "ALL_XML_IN/Registration", "PhoneNumber");
            hfPanelActive.Value = "2";
        }

        if (tools.GetValueFromXml(sXml, "ALL_XML_IN/Registration", "Period").Trim().Length > 0)
        {
            if (RegistrationSearchByAgencyPeriod.PredifinedPeriodChoice.Items.FindByValue(tools.GetValueFromXml(sXml, "ALL_XML_IN/Registration", "Period")) != null)
            {
                RegistrationSearchByAgencyPeriod.PredifinedPeriodChoice.SelectedValue = tools.GetValueFromXml(sXml, "ALL_XML_IN/Registration", "Period");
                hfPeriod.Value = tools.GetValueFromXml(sXml, "ALL_XML_IN/Registration", "Period");
            }
        }

        if (tools.GetValueFromXml(sXml, "ALL_XML_IN/Registration", "Period").Trim().Length == 0 && RegistrationSearchByAgencyPeriod.PredifinedPeriodChoice.SelectedValue == "3")
        {
            if (tools.GetValueFromXml(sXml, "ALL_XML_IN/Registration", "DateFrom").Length > 0)
            {
                RegistrationSearchByAgencyPeriod.sBeginDate = tools.GetValueFromXml(sXml, "ALL_XML_IN/Registration", "DateFrom");
                hfBeginDate.Value = tools.GetValueFromXml(sXml, "ALL_XML_IN/Registration", "DateFrom");
            }

            if (tools.GetValueFromXml(sXml, "ALL_XML_IN/Registration", "DateTo").Length > 0)
            {
                RegistrationSearchByAgencyPeriod.sEndDate = tools.GetValueFromXml(sXml, "ALL_XML_IN/Registration", "DateTo");
                hfBeginDate.Value = tools.GetValueFromXml(sXml, "ALL_XML_IN/Registration", "DateFrom");
            }
        }

        if (tools.GetValueFromXml(sXml, "ALL_XML_IN/Registration", "RefStatus").Length > 0)
        {
            if (rblStatus.Items.FindByValue(tools.GetValueFromXml(sXml, "ALL_XML_IN/Registration", "RefStatus")) != null)
                rblStatus.SelectedValue = tools.GetValueFromXml(sXml, "ALL_XML_IN/Registration", "RefStatus");
        }
        else
            rblStatus.SelectedValue = "";

        if (tools.GetValueFromXml(sXml, "ALL_XML_IN/Registration", "StatusCheck").Length > 0)
        {
            if (rblStatusCheck.Items.FindByValue(tools.GetValueFromXml(sXml, "ALL_XML_IN/Registration", "StatusCheck")) != null)
                rblStatusCheck.SelectedValue = tools.GetValueFromXml(sXml, "ALL_XML_IN/Registration", "StatusCheck");
        }
        else
            rblStatusCheck.SelectedValue = "";

        if (tools.GetValueFromXml(sXml, "ALL_XML_IN/Registration", "AgencyID").Length > 0)
        {
            if (ddlAgencyID.Items.FindByValue(tools.GetValueFromXml(sXml, "ALL_XML_IN/Registration", "AgencyID")) != null)
            {
                ddlAgencyID.SelectedValue = tools.GetValueFromXml(sXml, "ALL_XML_IN/Registration", "AgencyID");
                hfPanelActive.Value = "4";
            }
        }
    }

    protected DataTable GetRegistrationSearch(string sXML)
    {
        SqlConnection conn = null;
        DataTable dt = new DataTable();

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Partner"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("Registration.P_SearchRegistration", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@ReturnSelect", SqlDbType.Bit);

            cmd.Parameters["@IN"].Value = sXML;
            cmd.Parameters["@ReturnSelect"].Value = 1;

            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;
            da.Fill(dt);

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            /*<ALL_XML_OUT> 
				<RESULT RC="71008" ErrorLabel="Session Terminee" />
			</ALL_XML_OUT>*/

            string rc = "";
            GetValueFromXml(sXmlOut, "ALL_XML_OUT/RESULT", "RC", out rc);

            if (rc != null && rc != "" && rc != "0")
            {
                lblNbOfResults.Text = "0";
                ddlPage.Items.Clear();
                btnNextPage.Enabled = false;
                btnPreviousPage.Enabled = false;
                divPagination.Visible = false;

                if (rc == "71008")
                {
                    Session.Remove("Authentication");
                    Response.Redirect("RegistrationSearch.aspx");
                }

            }
            else
            {

                string sRowCount = "0";
                GetValueFromXml(sXmlOut, "ALL_XML_OUT/Registration", "NbResult", out sRowCount);
                lblNbOfResults.Text = sRowCount;

                if (int.Parse(sRowCount) > int.Parse(ddlNbResult.SelectedValue.ToString()))
                {

                    int selectedPage = ddlPage.SelectedIndex;

                    DataTable dtPages = getPages(CalculateTotalPages(int.Parse(sRowCount), int.Parse(ddlNbResult.SelectedValue)));
                    ddlPage.DataSource = dtPages;
                    ddlPage.DataBind();

                    string sPageIndex = ""; int iPageIndex = 0;
                    GetValueFromXml(sXML, "ALL_XML_IN/Registration", "PageIndex", out sPageIndex);
                    if (sPageIndex.Trim().Length > 0 && int.TryParse(sPageIndex, out iPageIndex))
                    {
                        if (ddlPage.Items.Count > 0 && ddlPage.Items.FindByValue(iPageIndex.ToString()) != null)
                            ddlPage.Items.FindByValue(iPageIndex.ToString()).Selected = true;
                    }
                    else if (Session["RegistrationPageIndex"] != null)
                    {
                        if (ddlPage.Items.Count > 0)
                            ddlPage.Items.FindByValue(Session["RegistrationPageIndex"].ToString()).Selected = true;
                    }
                    else if (ddlPage.Items.Count > selectedPage)
                        ddlPage.SelectedIndex = selectedPage;

                    if (int.Parse(ddlPage.SelectedValue) == ddlPage.Items.Count)
                        btnNextPage.Enabled = false;
                    else
                        btnNextPage.Enabled = true;

                    if (int.Parse(ddlPage.SelectedValue) == 1)
                        btnPreviousPage.Enabled = false;
                    else
                        btnPreviousPage.Enabled = true;

                    if (ddlPage.Items.Count > 1)
                        divPagination.Visible = true;
                    else
                        divPagination.Visible = false;

                    lblTotalPage.Text = " / " + ddlPage.Items.Count.ToString();
                }
                else
                {
                    divPagination.Visible = false;
                }
            }
        }
        catch (Exception ex)
        {
        }

        return dt;
    }

    protected void GetValueFromXml(string sXml, string sNode, string sAttribute, out string sValue)
    {
        sValue = "";

        List<string> tagList = CommonMethod.GetAttributeValues(sXml.ToString(), sNode, sAttribute);

        if (tagList.Count > 0)
            sValue = tagList[0];
    }

    protected string GetParam(string ListOfParam, char ParamSeparator, char ValueSeparator, string ParamName)
    {
        string Value = "";

        string[] paramList = ListOfParam.Split(ParamSeparator);

        for (int i = 0; i < paramList.Length; i++)
        {
            if (paramList[i].Split(ValueSeparator)[0] == ParamName && paramList[i].Split(ValueSeparator).Length > 1)
                Value = paramList[i].Split(ValueSeparator)[1];
        }

        return Value;
    }

    protected DataTable getPages(int nbPages)
    {
        DataTable dt = new DataTable();

        dt.Columns.Add("L");
        dt.Columns.Add("V");

        for (int i = 1; i <= nbPages; i++)
        {
            dt.Rows.Add(i.ToString(), i.ToString());
        }

        return dt;
    }

    protected int CalculateTotalPages(double totalRows, int pageSize)
    {
        int totalPages = (int)Math.Ceiling(totalRows / pageSize);

        return totalPages;
    }

    bool isFirstTime = true;
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (this.isFirstTime)
            {
                System.Data.DataView dv = (e.Row.DataItem as
                System.Data.DataRowView).DataView;
                //NbOfResults.Text = dv.Count.ToString();
                this.isFirstTime = false;
            }

            Label lblRegistrationNumber = (Label)e.Row.FindControl("lblRegistrationNumber");
            //Label bDeleteField = (Label)e.Row.FindControl("bDelete");

            if (lblRegistrationNumber != null)
            {
                e.Row.Cells[0].Attributes.Add("onclick", "showRegistration( '" + lblRegistrationNumber.Text + "')");
                e.Row.Cells[1].Attributes.Add("onclick", "showRegistration( '" + lblRegistrationNumber.Text + "')");
                e.Row.Cells[2].Attributes.Add("onclick", "showRegistration( '" + lblRegistrationNumber.Text + "')");
                e.Row.Cells[3].Attributes.Add("onclick", "showRegistration( '" + lblRegistrationNumber.Text + "')");
                e.Row.Cells[4].Attributes.Add("onclick", "showRegistration( '" + lblRegistrationNumber.Text + "')");
                e.Row.Cells[5].Attributes.Add("onclick", "showRegistration( '" + lblRegistrationNumber.Text + "')");
                e.Row.Cells[6].Attributes.Add("onclick", "showRegistration( '" + lblRegistrationNumber.Text + "')");
                e.Row.Cells[7].Attributes.Add("onclick", "showRegistration( '" + lblRegistrationNumber.Text + "')");
            }

            HiddenField hdn = (HiddenField)e.Row.FindControl("hdnServiceTag");
            if(hdn != null && hdn.Value == "CNJ")
                e.Row.Attributes.Add("onmouseover", "this.style.cursor = 'pointer';this.style.backgroundColor = '#1dc39c'");
            else e.Row.Attributes.Add("onmouseover", "this.style.cursor = 'pointer';this.style.backgroundColor = '#ff9966'");
            e.Row.Attributes.Add("onmouseout", "this.style.cursor = 'auto';this.style.backgroundColor = '#fff'");
        }
    }

    protected string ConvertToLocalDateTime(string UTCDate)
    {
        DateTime convertedDate = DateTime.SpecifyKind(
                    DateTime.Parse(UTCDate),
                    DateTimeKind.Utc);

        DateTime dt = convertedDate.ToLocalTime();

        return dt.ToString().Substring(0, 16);
    }

    protected System.Drawing.Color ColorStatusLabel(string Color)
    {
        System.Drawing.Color color;

        color = System.Drawing.Color.Black;

        try
        {
            color = System.Drawing.Color.FromName(Color);
        }
        catch (Exception ex)
        {
        }

        return color;
    }
    protected void btnRegistrationSearch_Click(object sender, EventArgs e)
    {
        string sValues = "";
        switch (hfPanelActive.Value) 
        {
            case "0":
                sValues = "PackNumber!" + txtPackNumber.Text.Trim();
                sValues += ",BeginDate!";
                sValues += ",EndDate!";
                sValues += ",Period!3";
                break;
            case "1":
                sValues = "RegistrationNumber!" + txtRegistrationNumber.Text.Trim();
                sValues += ",BeginDate!";
                sValues += ",EndDate!";
                sValues += ",Period!3";
                break;
            case "2":
                sValues = "PhoneNumber!" + txtPhoneNumber.Text.Trim();
                sValues += ",BeginDate!";
                sValues += ",EndDate!";
                sValues += ",Period!3";
                break;
            case "3":
                sValues = "ClientLastName!" + txtClientLastName.Text.Trim();
                sValues += ",ClientFirstName!" + txtClientFirstName.Text.Trim();
                sValues += ",ClientBirthDate!" + hfClientBirthDate.Value.Trim();
                sValues += ",BeginDate!";
                sValues += ",EndDate!";
                sValues += ",Period!3";
                break;
            default:
            case "4":
                sValues = "AgencyID!" + ddlAgencyID.SelectedValue;
                sValues += ",BeginDate!" + RegistrationSearchByAgencyPeriod.sBeginDate;
                sValues += ",EndDate!" + RegistrationSearchByAgencyPeriod.sEndDate;
                sValues += ",Period!" + RegistrationSearchByAgencyPeriod.sPeriod;

                sValues += ",RefStatus!" + rblStatus.SelectedValue;
                sValues += ",StatusCheck!" + rblStatusCheck.SelectedValue;
                break;
        }

        sValues += ",ServiceTAG!" + ddlAccountType.SelectedValue;

        int iPageSelected;
        if (hfPageSelected.Value.Trim().Length != 0 && int.TryParse(hfPageSelected.Value.Trim(), out iPageSelected))
             sValues += ",PageIndex!" + iPageSelected.ToString();
        else sValues += ",PageIndex!1";

        Session["RegistrationSearchFilter"] = sValues;
        divGridView.Visible = true;
        divGridViewInfo.Visible = true;
        gvRegistrationSearchResult.DataSource = GetRegistrationSearch(CreateXmlStringFromFilter(sValues));
        gvRegistrationSearchResult.DataBind();

        upFilter.Update();
        upFilterStatus.Update();

        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "InitAccordion();initTooltip();FilterStatusDisplayManagement();hideLoading();", true);
    }
    protected void btnDeleteFilter_Click(object sender, EventArgs e)
    {
        string sValues = "";
        sValues += "AgencyID!,RefStatus!15,StatusCheck!,Period!4";

        txtPackNumber.Text = "";
        txtRegistrationNumber.Text = "";
        txtPhoneNumber.Text = "";
        txtClientLastName.Text = "";
        txtClientFirstName.Text = "";
        txtClientBirthDate.Text = "";
        ddlAgencyID.SelectedItem.Selected = false;
        ddlAgencyID.Items[0].Selected = true;
        rblStatus.SelectedItem.Selected = false;
        rblStatus.Items[0].Selected = true;
        rblStatusCheck.SelectedItem.Selected = false;
        rblStatusCheck.Items[0].Selected = false;

        RegistrationSearchByAgencyPeriod.PredifinedPeriodChoice.SelectedValue = "3";
        hfPanelActive.Value = "";

        upFilter.Update();
        upFilterStatus.Update();

        Session["RegistrationSearchFilter"] = sValues;
        divGridView.Visible = true;
        divGridViewInfo.Visible = true;
        gvRegistrationSearchResult.DataSource = GetRegistrationSearch(CreateXmlStringFromFilter(sValues));
        gvRegistrationSearchResult.DataBind();

        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "InitAccordion(4);initTooltip();FilterStatusDisplayManagement();hideLoading();", true);
    }
}