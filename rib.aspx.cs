﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using XMLMethodLibrary;

public partial class rib : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        string sAccountNumber = ""; string sRefCustomer = "";
        int refCustomer;

        if (Request.Params["ref"] != null && Request.Params["ref"].Length > 0)
        {
            sRefCustomer = Request.Params["ref"].ToString();
        }
        if (Request.Params["accNb"] != null && Request.Params["accNb"].Length > 0)
        {
            sAccountNumber = Request.Params["accNb"].ToString();
        }

        if (sRefCustomer.Trim().Length > 0 && sAccountNumber.Trim().Length > 0 && int.TryParse(sRefCustomer, out refCustomer))
        {
            rRib.DataSource = getRibFromWs(refCustomer, sAccountNumber);
            rRib.DataBind();
        }



    }

    protected DataTable getRibFromWs(int refCustomer, string accountNumber)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("AgencyName");
        dt.Columns.Add("AgencyAddress1");
        dt.Columns.Add("AgencyAddress2");
        dt.Columns.Add("AgencyAddress3");
        dt.Columns.Add("AgencyZipcode");
        dt.Columns.Add("AgencyCity");
        dt.Columns.Add("ClientCivility");
        dt.Columns.Add("ClientLastName");
        dt.Columns.Add("ClientFirstName");
        dt.Columns.Add("ClientAddress1");
        dt.Columns.Add("ClientAddress2");
        dt.Columns.Add("ClientAddress3");
        dt.Columns.Add("ClientAddress4");
        dt.Columns.Add("ClientZipcode");
        dt.Columns.Add("ClientCity");
        dt.Columns.Add("ClientCountry");
        dt.Columns.Add("AccountNumber");
        dt.Columns.Add("BankCode");
        dt.Columns.Add("BranchCode");
        dt.Columns.Add("Key");
        dt.Columns.Add("Iban");
        dt.Columns.Add("Bic");

        try
        {
            if (accountNumber != null && accountNumber.Trim().Length > 0)
            {
                WS_SAB.GetRibResponse wsRes = tools.GetBankDetails(accountNumber, refCustomer);
                //wsSAB.GetRibResponse wsRes = Tools.GetBankDetails("00018260001");

                if (wsRes != null)
                {

                    string sXMLCustomerInfos = tools.GetXMLCustomerInformations(refCustomer);
                    //string sXMLCustomerInfos = Tools.GetXMLCustomerInformations(1541);

                    List<string> listCivility = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "Politeness");

                    int iNbRib = 4;
                    int.TryParse(hfNbRib.Value, out iNbRib);
                    for (int i = 0; i < iNbRib; i++)
                    {

                        dt.Rows.Add(new object[]{   wsRes.AgencyName, wsRes.AgencyAddress1, wsRes.AgencyAddress2, wsRes.AgencyAddress3 , wsRes.AgencyZipCode, wsRes.AgencyCity,
                                    (listCivility.Count > 0) ? listCivility[0] : "", wsRes.CustomerLastName, wsRes.CustomerFirstName, wsRes.CustomerAddress1, wsRes.CustomerAddress2, wsRes.CustomerAddress3, wsRes.CustomerAddress4, wsRes.CustomerZipCode, wsRes.CustomerCity, wsRes.OfficeCode,
                                    wsRes.AccountNumber, wsRes.BankCode, wsRes.OfficeCode, wsRes.RibKey, wsRes.Iban.Replace("IBAN","").Trim(), "FPELFR21" });
                    }
                }
                else
                {
                    throw new System.ArgumentException("Null Response from Web Service SAB", "NullException_wsSAB ");
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }

        return dt;
    }
}