﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CobaltTransfer.aspx.cs" Inherits="CobaltTransfer" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Virement Cobalt</title>

    <link href="Styles/UI/jquery-ui-1.9.2.custom.css" rel="Stylesheet" type="text/css" />
    <link href="~/Styles/Site.css" rel="stylesheet" type="text/css" />
    <link href="Styles/Operations.css?v=20180314" type="text/css" rel="Stylesheet" />

    <style>
        .operation-table-row, .operation-table-alternate-row { cursor:auto }
    </style>

    <script src="Scripts/jquery-1.8.3.js" type="text/javascript"></script>
    <script src="Scripts/jquery-ui-1.9.2.custom.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        function ShowCancelConfirmDialog(ref, receveur, montant) {
            console.log(ref + ' ' + receveur + ' ' + montant);

            $('#<%=hdnRefTransferToCancel.ClientID%>').val(ref);
            $('#montant-dialog').empty().append(montant);
            $('#receveur-dialog').empty().append(receveur);

            $('#dialog-confirm-cancel').dialog({
                autoOpen: true,
                resizable: false,
                draggable: false,
                modal: true,
                width: 300,
                title: "Confirmation annulation",
                buttons: [{ text: "Confirmer", click: function () { $("#<%=btnCancel.ClientID %>").click(); $(this).dialog('destroy'); } },
                { text: "Annuler", click: function () { $(this).dialog('destroy'); } }]
            });
            $('#dialog-confirm-cancel').parent().appendTo(jQuery("form:first"));
        }

        function AlertMessage(title, message, redirect, focusID) {
            $('#<%=lblAlertMessage.ClientID%>').html(message);

            $("#panelAlertMessage").dialog({
                draggable: false,
                resizable: false,
                width: 500,
                dialogClass: "no-close",
                modal: true,
                title: title,
                buttons: {
                    Ok: function () {
                        $(this).dialog("close");
                        if (redirect != null && redirect.trim().length > 0) {
                            window.location = redirect;
                        }
                        else if (focusID != null && focusID.trim().length > 0 && $('#' + focusID).length > 0) {
                            $('#' + focusID).focus();
                        }
                    }
                }
            });
        }

        function CloseAlertMessage() {
            $("#panelAlertMessage").dialog('close');
            $("#panelAlertMessage").dialog('destroy');
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" EnablePartialRendering="true" EnableScriptLocalization="true"
            EnableScriptGlobalization="true" EnablePageMethods="true">
        </asp:ToolkitScriptManager>

        <div>
            <asp:UpdatePanel ID="upCobaltTransfer" runat="server">
                <ContentTemplate>
                    <asp:Repeater ID="rptCobaltTransfer" runat="server">
                        <HeaderTemplate>
                            <table id="cobalt-transfer-list" class="operation-table" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>IBAN/BIC</th>
                                        <th>Nom</th>
                                        <th>Montant</th>
                                        <th>Libellé</th>
                                        <th>Statut</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                                    <tr class='<%# Container.ItemIndex % 2 == 0 ? "operation-table-row" : "operation-table-alternate-row" %>'>
                                        <td><%# Eval("IBAN_RECEVEUR") %><br /><%# Eval("BIC_RECEVEUR") %></td>
                                        <td><%# Eval("NOM_RECEVEUR") %></td>
                                        <td><%# tools.getFormattedAmount(Eval("MONTANT").ToString()) %></td>
                                        <td><%# Eval("LABEL") %></td>
                                        <td><%# Eval("STATUT") %></td>
                                        <td>
                                            <img src="Styles/Img/cancel.png" width="20" alt="Annuler" onclick='<%# String.Format("ShowCancelConfirmDialog(\"{0}\",\"{1}\",\"{2}\");", Eval("REFERENCE").ToString(), Eval("NOM_RECEVEUR").ToString(), tools.getFormattedAmount(Eval("MONTANT").ToString(), false)) %>' />
                                        </td>
                                    </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                                </tbody>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>

        <div id="dialog-confirm-cancel" style="display:none">
            <span class="ui-helper-hidden-accessible"><input type="text"/></span>
            Etes-vous sûr de vouloir annuler le virement de <b id="montant-dialog"></b> à destination de <b id="receveur-dialog"></b> ?
            <asp:HiddenField ID="hdnRefTransferToCancel" runat="server" />
            <asp:Button ID="btnCancel" runat="server" OnClick="btnCancel_Click" style="display:none" />
        </div>

        <div id="ar-loading" class="loading"></div>

        <div id="panelAlertMessage" style="display: none;padding-top:10px">
            <asp:Label ID="lblAlertMessage" runat="server" Style="color: red"></asp:Label>
        </div>

        <script type="text/javascript">
            Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
            var sRequestControlID = "";
            function BeginRequestHandler(sender, args) {
                pbControl = args.get_postBackElement();
                if (pbControl.id == '<%=btnCancel.ClientID %>') {
                    var width = $('#cobalt-transfer-list').width();
                    var height = $('#cobalt-transfer-list').height();
                    $('#ar-loading').css('width', width);
                    $('#ar-loading').css('height', height);

                    $('#ar-loading').show();

                    $('#ar-loading').position({ my: "left top", at: "left top", of: "#cobalt-transfer-list" });
                }
                sRequestControlID = pbControl.id;
            }
            function EndRequestHandler(sender, args) {
                $('#ar-loading').hide();
            }
        </script>
    </form>
</body>
</html>