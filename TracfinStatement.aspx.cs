﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Hosting;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using XMLMethodLibrary;

public partial class TracfinStatement : System.Web.UI.Page
{
    private int _iArticle
    {
        get { return (ViewState["Article"] != null) ? int.Parse(ViewState["Article"].ToString()) : 0; }
        set { ViewState["Article"] = value; }
    }
    private string _sAlertType
    {
        get { return (Session["TracfinAlertType"] != null) ? Session["TracfinAlertType"].ToString() : ""; }
        set { Session["TracfinAlertType"] = value; }
    }
    private int _RefStatement
    {
        get { return (ViewState["RefStatement"] != null) ? int.Parse(ViewState["RefStatement"].ToString()) : 0; }
        set { ViewState["RefStatement"] = value; }
    }
    protected DataTable _dtClient {
        get {
            if (_RefStatement != 0)
            {
                string sRefStatement = _RefStatement.ToString();
                if (Session["dtClient_" + sRefStatement] == null)
                {
                    DataTable dt = new DataTable();
                    dt.Columns.Add("RefCustomer");
                    dt.Columns.Add("Politeness");
                    dt.Columns.Add("LastName");
                    dt.Columns.Add("FirstName");
                    dt.Columns.Add("BirthDate");
                    dt.Columns.Add("BirthPlace");
                    dt.Columns.Add("BirthCountry");
                    dt.Columns.Add("Nationality");
                    dt.Columns.Add("AccountNumber");
                    dt.Columns.Add("IBAN");
                    dt.Columns.Add("RefCloseAction");
                    dt.Columns.Add("Selected", typeof(bool));
                    dt.Columns.Add("FS", typeof(bool));
                    dt.Columns.Add("NewLastName");
                    dt.Columns.Add("Address", typeof(Address));
                    dt.Columns.Add("IDDocument", typeof(IDDocument));
                    dt.Columns.Add("KYC", typeof(KYC));
                    dt.Columns.Add("Patrimoine");
                    _dtClient = dt;
                }

                return (DataTable)Session["dtClient_" + sRefStatement];
            }
            else return null;
        }
        set
        {
            if (_RefStatement != 0)
            {
                string sRefStatement = _RefStatement.ToString();
                Session["dtClient_" + sRefStatement] = value;
            }
        }
    }
    private string _sHashForm
    {
        get { return (ViewState["HashForm"] != null) ? ViewState["HashForm"].ToString() : ""; }
        set { ViewState["HashForm"] = value; }
    }
    protected int _iRefMainCustomer
    {
        get { return (ViewState["RefMainCustomer"] != null) ? int.Parse(ViewState["RefMainCustomer"].ToString()) : 0; }
        set { ViewState["RefMainCustomer"] = value; }
    }

    [Serializable]
    public class Address
    {
        public int iNumeroVoie;
        public string sTypeVoie;
        public string sComplementVoie;
        public string sNomVoie;
        public string sCodePostal;
        public string sVille;
        public string sCodePays;
        public string sPhoneNumber;
        public string sEmail;

        public Address() { }
        public Address(int iNum, string sType, string sComplementVoie, string sNom, string sCodePostal, string sVille, string sCodePays, string sPhoneNumber, string sEmail)
        {
            this.iNumeroVoie = iNum;
            this.sTypeVoie = sType.Trim();
            this.sComplementVoie = sComplementVoie.Trim();
            this.sNomVoie = sNom.Trim();
            this.sCodePostal = sCodePostal;
            this.sVille = sVille;
            this.sCodePays = sCodePays;
            this.sPhoneNumber = sPhoneNumber;
            this.sEmail = sEmail;
        }
    }
    [Serializable]
    public class IDDocument
    {
        public string sType;
        public string sNum;
        public string sPays;
        public string sDateDebut;
        public string sDateFin;

        public IDDocument() { }
        public IDDocument(string sType, string sNum, string sPays, string sDateDebut, string sDateFin)
        {
            this.sType = sType;
            this.sNum = sNum;
            this.sPays = sPays;
            this.sDateDebut = sDateDebut;
            this.sDateFin = sDateFin;
        }
    }
    [Serializable]
    public class KYC
    {
        public string sSitFam;

        public KYC() { }
        public KYC(string sSitFam)
        {
            this.sSitFam = sSitFam;
        }
    }
    protected DataTable _dtOpeOffUs
    {
        get
        {
            if (Session["dtOpeOffUs"] == null)
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("sIdOpe");
                dt.Columns.Add("Date");
                dt.Columns.Add("Time");
                dt.Columns.Add("Place");
                dt.Columns.Add("Object");
                dt.Columns.Add("Amount", typeof(decimal));
                dt.Columns.Add("Category");
                dt.Columns.Add("Selected", typeof(bool));
                dt.Columns.Add("CustomerName");
                dt.Columns.Add("DateTime", typeof(DateTime));
                _dtOpeOffUs = dt;
            }

            return (DataTable)Session["dtOpeOffUs"];
        }
        set { Session["dtOpeOffUs"] = value; }
    }
    protected DataTable _dtStatementFiles
    {
        get
        {
            if (Session["dtCustomerFiles"] == null)
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("FileName");
                dt.Columns.Add("CategoryTAG");
                dt.Columns.Add("CategoryLabel");
                dt.Columns.Add("ContentType");
                dt.Columns.Add("LocalStorage", typeof(bool));
                dt.Columns.Add("URL");
                dt.Columns.Add("Description");
                dt.Columns.Add("LastUploadDate");
                dt.Columns.Add("StorePath");
                dt.Columns.Add("CheckFile");
                dt.Columns.Add("RefCustomer");
                _dtStatementFiles = dt;
            }

            return (DataTable)Session["dtCustomerFiles"];
        }
        set { Session["dtCustomerFiles"] = value; }
    }
    private bool _CreationStarted
    {
        get { return (ViewState["CreationStarted"] != null) ? bool.Parse(ViewState["CreationStarted"].ToString()) : false; }
        set { ViewState["CreationStarted"] = value; }
    }
    private bool _Editable {
        get { return (ViewState["FormEditable"] != null) ? bool.Parse(ViewState["FormEditable"].ToString()) : false; }
        set { ViewState["FormEditable"] = value; }
    }
    private bool _RightsCreation { get; set; }
    private bool _RightsValidation { get; set; }
    private bool _RightsCorrection { get; set; }
    private bool _RightsApproval { get; set; }
    private string _sLastAction { get; set; }

    private Dictionary<string, string> dicSituation = new Dictionary<string, string>
    {
        { "CE", "SFA1" },
        { "DI", "SFA2" },
        { "MA", "SFA3" },
        //{ "key1", "SFA4" },
        //{ "key1", "SFA5" },
        { "VE", "SFA6" },
        { "CO", "SFA7" },
        { "", "SFA8" }
    };
    private Dictionary<string, string> dicTypeVoie = new Dictionary<string, string>
    {
        { "Allée", "TVO1" },
        { "Arcades", "TVO2" },
        { "Avenue", "TVO3" },
        { "Bois", "TVO4" },
        { "Boulevard", "TVO5" },
        { "Butte", "TVO6" },
        { "Carré", "TVO7" },
        { "Carrefour", "TVO8" },
        { "Chaussée", "TVO9" },
        { "Cité", "TVO10" },
        { "Clos", "TVO11" },
        { "Coteau", "TVO12" },
        { "Cour", "TVO13" },
        { "Cours", "TVO14" },
        { "Domaine", "TVO15" },
        { "Esplanade", "TVO16" },
        { "Faubourg", "TVO17" },
        { "Fosse", "TVO18" },
        { "Hameau", "TVO19" },
        { "Impasse", "TVO20" },
        { "Jardin", "TVO21" },
        { "Lieu Dit", "TVO22" },
        { "Lotissement", "TVO23" },
        { "Mail", "TVO24" },
        { "Parc", "TVO25" },
        { "Parvis", "TVO26" },
        { "Passage", "TVO27" },
        { "Passerelle", "TVO28" },
        { "Patio", "TVO29" },
        { "Pavillon", "TVO30" },
        { "Place", "TVO31" },
        { "Porte", "TVO32" },
        { "Poterne", "TVO33" },
        { "Promenade", "TVO34" },
        { "Quai", "TVO35" },
        { "Quartier", "TVO36" },
        { "Résidence", "TVO37" },
        { "Rond Point", "TVO38" },
        { "Route", "TVO39" },
        { "Rue", "TVO40" },
        { "Ruelle", "TVO41" },
        { "Sentier", "TVO42" },
        { "Square", "TVO43" },
        { "Terrasse", "TVO44" },
        { "Venelle", "TVO45" },
        { "Villa", "TVO46" },
        { "Voie", "TVO47" },
        { "Autre", "TVO48" },
        { "Chemin", "TVO49" }
    };
    private Dictionary<string, string> dicPieceIdentite = new Dictionary<string, string>
    {
        { "I", "TID2" },
        { "P", "TID3" },
        { "DL", "TID4" },
        { "DA", "TID5" },
        { "T", "TID7" }
    };
    private Dictionary<string, string> dicComplementNumero = new Dictionary<string, string>
    {
        {"B", "CNV1" },
        {"T", "CNV2" },
        {"Q", "CNV3" }
    };

    protected void Page_Init(object sender, EventArgs e)
    {
        clientSearch1.ClientFound += new EventHandler(clientFound);
        multiReason.choiceAdded += new EventHandler(multiReasonChoiceAddedRemoved);
        multiReason.choiceRemoved += new EventHandler(multiReasonChoiceAddedRemoved);
        clientFileUpload1.ClientFileUploaded += new EventHandler(fileUploaded);
        clientFileUpload1.ClientFileUploadError += new EventHandler(fileUploadFailed);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            CheckRights();

            _Editable = true;
            ClearSessions();

            int iTmp;
            if (Request.Params["ref"] != null && int.TryParse(Request.Params["ref"].ToString(), out iTmp))
                _RefStatement = iTmp;

            BindStatementReasons();
            BindStatementDetails();
            RefreshClientMetaData();
            BindStatementFiles();
            //BindFSFiles();

            CheckPreviousButtonUrl();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "InitControls();", true);
        }
    }

    protected void CheckRights()
    {
        string sXmlOut = tools.GetRights(authentication.GetCurrent().sToken, "SC_TracfinStatement");

        List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Page", "RC");

        if (listRC.Count == 0 || listRC[0] != "0")
        {
            Response.Redirect("Default.aspx", true);
        }
        else
        {
            List<string> listAction = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/Page/Action");

            for (int i = 0; i < listAction.Count; i++)
            {
                List<string> listTag = CommonMethod.GetAttributeValues(listAction[i], "Action", "TagAction");
                List<string> listActionAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ActionAllowed");
                List<string> listViewAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ViewAllowed");

                if (listTag.Count > 0)
                {
                    switch (listTag[0])
                    {
                        case "Creation":
                            if (listActionAllowed.Count > 0 && listActionAllowed[0] == "1")
                                _RightsCreation = true;
                            break;
                        case "Validation":
                            if (listActionAllowed.Count > 0 && listActionAllowed[0] == "1")
                                _RightsValidation = true;
                            break;
                        case "Correction":
                            if (listActionAllowed.Count > 0 && listActionAllowed[0] == "1")
                                _RightsCorrection = true;
                            break;
                        case "Approval":
                            if (listActionAllowed.Count > 0 && listActionAllowed[0] == "1")
                                _RightsApproval = true;
                            break;
                    }
                }
            }
        }
    }

    protected void CheckPreviousButtonUrl()
    {
        string sUrl = "";
        string sButtonText = "";
        if (tools.GetBackUrl(out sUrl, out sButtonText))
        {
            btnPrev.Text = sButtonText;
            btnPrev.PostBackUrl = sUrl;

            Session.Remove("BackUrl");
        }
    }

    protected void ClearSessions()
    {
        _dtClient = null;
        _dtOpeOffUs = null;
        _dtStatementFiles = null;
        _RefStatement = 0;
    }

    protected void AlertMessage(string sTitle, string sMessage)
    {
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('" + sTitle.Replace("'", "&#39;") + "', '" + sMessage.Replace("'", "&#39;") + "');", true);
    }

    protected void BindStatementDetails()
    {
        string sStatementDetails = Tracfin.GetStatementDetails(_RefStatement);

        // PARSING
        List<string> listRC = CommonMethod.GetAttributeValues(sStatementDetails, "ALL_XML_OUT/TRACFIN_STATEMENT", "RC");
        List<string> listErrorMessage = CommonMethod.GetAttributeValues(sStatementDetails, "ALL_XML_OUT/TRACFIN_STATEMENT", "ErrorMessage");

        if (listRC.Count > 0 && listRC[0] == "0")
        {
            List<string> listCreationDate = CommonMethod.GetAttributeValues(sStatementDetails, "ALL_XML_OUT/TRACFIN_STATEMENT", "CreationDate");
            List<string> listRefStatementExt = CommonMethod.GetAttributeValues(sStatementDetails, "ALL_XML_OUT/TRACFIN_STATEMENT", "RefStatementExt");
            List<string> listRefStatementInt = CommonMethod.GetAttributeValues(sStatementDetails, "ALL_XML_OUT/TRACFIN_STATEMENT", "RefStatementInt");
            List<string> listRefInitStmtInt = CommonMethod.GetAttributeValues(sStatementDetails, "ALL_XML_OUT/TRACFIN_STATEMENT", "RefInitStmtInt");
            List<string> listRefInitStmtExt = CommonMethod.GetAttributeValues(sStatementDetails, "ALL_XML_OUT/TRACFIN_STATEMENT", "RefInitStmtExt");
            List<string> listOpeOriginDRDate = CommonMethod.GetAttributeValues(sStatementDetails, "ALL_XML_OUT/TRACFIN_STATEMENT", "OpeOriginDRDate");
            List<string> listDRStartDate = CommonMethod.GetAttributeValues(sStatementDetails, "ALL_XML_OUT/TRACFIN_STATEMENT", "DRStartDate");
            List<string> listDREndDate = CommonMethod.GetAttributeValues(sStatementDetails, "ALL_XML_OUT/TRACFIN_STATEMENT", "DREndDate");
            List<string> listRefAlert = CommonMethod.GetAttributeValues(sStatementDetails, "ALL_XML_OUT/TRACFIN_STATEMENT", "RefAlert");
            List<string> listAlertKindManual = CommonMethod.GetTagValues(sStatementDetails, "ALL_XML_OUT/TRACFIN_STATEMENT/AlertKindManual");
            List<string> listOpeCount = CommonMethod.GetAttributeValues(sStatementDetails, "ALL_XML_OUT/TRACFIN_STATEMENT", "OpeCount");
            List<string> listOpeAmount = CommonMethod.GetAttributeValues(sStatementDetails, "ALL_XML_OUT/TRACFIN_STATEMENT", "OpeAmount");
            List<string> listMainCustomer = CommonMethod.GetAttributeValues(sStatementDetails, "ALL_XML_OUT/TRACFIN_STATEMENT", "MainCustomer");

            if (listMainCustomer.Count > 0)
            {
                int iTmp;
                int.TryParse(listMainCustomer[0], out iTmp);
                _iRefMainCustomer = iTmp;
            }

            List<string> listAtypicalFundsCirculationIN = CommonMethod.GetTagValues(sStatementDetails, "ALL_XML_OUT/TRACFIN_STATEMENT/ATYPICAL_FUNDS_CIRCULATION_IN");
            List<string> listAtypicalFundsCirculationOUT = CommonMethod.GetTagValues(sStatementDetails, "ALL_XML_OUT/TRACFIN_STATEMENT/ATYPICAL_FUNDS_CIRCULATION_OUT");
            List<string> listAtypicalFundsCirculationOTHER = CommonMethod.GetTagValues(sStatementDetails, "ALL_XML_OUT/TRACFIN_STATEMENT/ATYPICAL_FUNDS_CIRCULATION_OTHER");

            List<string> listCustomers = CommonMethod.GetTags(sStatementDetails, "ALL_XML_OUT/TRACFIN_STATEMENT/CUSTOMERS/CUSTOMER");

            List<string> listFiles = CommonMethod.GetTags(sStatementDetails, "ALL_XML_OUT/TRACFIN_STATEMENT/FILES/FILE");

            List<string> listReasons = CommonMethod.GetTags(sStatementDetails, "ALL_XML_OUT/TRACFIN_STATEMENT/REASONS/REASON");

            List<string> listActions = CommonMethod.GetTags(sStatementDetails, "ALL_XML_OUT/TRACFIN_STATEMENT/ACTIONS/ACTION");

            List<string> listOpeCashSelect = CommonMethod.GetTags(sStatementDetails, "ALL_XML_OUT/TRACFIN_STATEMENT/OPE_CASH_SELECT/OPE_CASH");


            #region // BIND DATA
            BindActions(listActions);

            txtAccountOpenDate.Text = (!string.IsNullOrWhiteSpace(listCreationDate[0])) ? listCreationDate[0].Substring(0, 10) : "";
            if (!string.IsNullOrWhiteSpace(listRefStatementExt[0]))
            {
                panelTRACFIN.Visible = true;
                txtRefStatementExt.Text = listRefStatementExt[0];
            }
            else txtRefStatementExt.Text = "";
            if (!string.IsNullOrWhiteSpace(listRefStatementInt[0]))
                txtRefStatementInt.Text = listRefStatementInt[0];
            else panelRefStatementInt.Visible = false;
            txtRefInitStatementInt.Text = (!string.IsNullOrWhiteSpace(listRefInitStmtInt[0])) ? listRefInitStmtInt[0] : "";
            txtRefInitStatementExt.Text = (!string.IsNullOrWhiteSpace(listRefInitStmtExt[0])) ? listRefInitStmtExt[0] : "";
            txtOpeOriginDRDate.Text = (!string.IsNullOrWhiteSpace(listOpeOriginDRDate[0])) ? listOpeOriginDRDate[0].Substring(0,10) : "";
            txtDRStartDate.Text = (!string.IsNullOrWhiteSpace(listDRStartDate[0])) ? listDRStartDate[0].Substring(0, 10) : "";
            if(String.IsNullOrWhiteSpace(txtDREndDate.Text))
                txtDREndDate.Text = (!string.IsNullOrWhiteSpace(listDREndDate[0])) ? listDREndDate[0].Substring(0, 10) : "";
            if(String.IsNullOrWhiteSpace(txtPeriodeEndDate.Text))
                txtPeriodeEndDate.Text = (!string.IsNullOrWhiteSpace(listDREndDate[0])) ? listDREndDate[0].Substring(0, 10) : "";

            int iRefAlert = 0;
            if (listRefAlert.Count > 0 && !String.IsNullOrWhiteSpace(listRefAlert[0]) && int.TryParse(listRefAlert[0], out iRefAlert))
                BindAlertDetails(iRefAlert);

            if(listAlertKindManual != null && listAlertKindManual.Count > 0 && !String.IsNullOrWhiteSpace(listAlertKindManual[0]))
            {
                rblAlertKind.SelectedIndex = 1;
                txtAlertKind.Text = listAlertKindManual[0];
                _sAlertType = listAlertKindManual[0];
                rblAlertKind_SelectedIndexChanged(new object(), new EventArgs());
            }

            txtAtypicalFundsCirculationIN.Text = (listAtypicalFundsCirculationIN.Count > 0 && !string.IsNullOrWhiteSpace(listAtypicalFundsCirculationIN[0])) ? listAtypicalFundsCirculationIN[0] : "";
            txtAtypicalFundsCirculationOUT.Text = (listAtypicalFundsCirculationOUT.Count > 0 && !string.IsNullOrWhiteSpace(listAtypicalFundsCirculationOUT[0])) ? listAtypicalFundsCirculationOUT[0] : "";
            txtAtypicalFundsCirculationOTHER.Text = (listAtypicalFundsCirculationOTHER.Count > 0 && !string.IsNullOrWhiteSpace(listAtypicalFundsCirculationOTHER[0])) ? listAtypicalFundsCirculationOTHER[0] : "";

            _dtClient = null;
            for (int i = 0; i < listCustomers.Count; i++)
            {
                List<string> listRefCustomer = CommonMethod.GetAttributeValues(listCustomers[i], "CUSTOMER", "RefCustomer");
                List<string> listSex = CommonMethod.GetAttributeValues(listCustomers[i], "CUSTOMER", "Sex");
                List<string> listLastName = CommonMethod.GetAttributeValues(listCustomers[i], "CUSTOMER", "LastName");
                List<string> listNewLastName = CommonMethod.GetAttributeValues(listCustomers[i], "CUSTOMER", "NewLastName");
                List<string> listFirstName = CommonMethod.GetAttributeValues(listCustomers[i], "CUSTOMER", "FirstName");
                List<string> listBirthDate = CommonMethod.GetAttributeValues(listCustomers[i], "CUSTOMER", "BirthDate");
                List<string> listBirthPlace = CommonMethod.GetAttributeValues(listCustomers[i], "CUSTOMER", "BirthPlace");
                List<string> listBirthCountry = CommonMethod.GetAttributeValues(listCustomers[i], "CUSTOMER", "BirthCountry");
                List<string> listNationality = CommonMethod.GetAttributeValues(listCustomers[i], "CUSTOMER", "Nationality");
                List<string> listAccountNumber = CommonMethod.GetAttributeValues(listCustomers[i], "CUSTOMER", "AccountNumber");
                List<string> listIBAN = CommonMethod.GetAttributeValues(listCustomers[i], "CUSTOMER", "IBAN");
                List<string> listRefCloseAction = CommonMethod.GetAttributeValues(listCustomers[i], "CUSTOMER", "RefCloseAction");
                List<string> listFS = CommonMethod.GetAttributeValues(listCustomers[i], "CUSTOMER", "FS");

                List<string> listPhoneNumber = CommonMethod.GetAttributeValues(listCustomers[i], "CUSTOMER", "PhoneNumber");
                List<string> listEmail = CommonMethod.GetAttributeValues(listCustomers[i], "CUSTOMER", "Email");
                List<string> listDocType = CommonMethod.GetAttributeValues(listCustomers[i], "CUSTOMER", "DocType");
                List<string> listDocNumber = CommonMethod.GetAttributeValues(listCustomers[i], "CUSTOMER", "DocNumber");
                List<string> listDocDeliveryDate = CommonMethod.GetAttributeValues(listCustomers[i], "CUSTOMER", "DocDeliveryDate");
                List<string> listDocExpireDate = CommonMethod.GetAttributeValues(listCustomers[i], "CUSTOMER", "DocExpireDate");
                List<string> listDocIssuedCountry = CommonMethod.GetAttributeValues(listCustomers[i], "CUSTOMER", "DocIssuedCountry");
                List<string> listAddStreetNumber = CommonMethod.GetAttributeValues(listCustomers[i], "CUSTOMER", "AddStreetNumber");
                int iAddStreetNumber = 0;
                string sComplementVoie = "";
                ParseStreetNumberInfos(listAddStreetNumber[0], out iAddStreetNumber, out sComplementVoie);
                List<string> listAddStreetKind = CommonMethod.GetAttributeValues(listCustomers[i], "CUSTOMER", "AddStreetKind");
                List<string> listAddStreetName = CommonMethod.GetAttributeValues(listCustomers[i], "CUSTOMER", "AddStreetName");
                List<string> listAddZipCode = CommonMethod.GetAttributeValues(listCustomers[i], "CUSTOMER", "AddZipCode");
                List<string> listAddCity = CommonMethod.GetAttributeValues(listCustomers[i], "CUSTOMER", "AddCity");
                List<string> listAddCountry = CommonMethod.GetAttributeValues(listCustomers[i], "CUSTOMER", "AddCountry");
                List<string> listSitFam = CommonMethod.GetAttributeValues(listCustomers[i], "CUSTOMER", "SitFam");
                List<string> listPatrimoine = CommonMethod.GetAttributeValues(listCustomers[i], "CUSTOMER", "Patrimoine");

                Address addr = new Address(iAddStreetNumber, listAddStreetKind[0], sComplementVoie, listAddStreetName[0], listAddZipCode[0], listAddCity[0], listAddCountry[0], listPhoneNumber[0], listEmail[0]);
                IDDocument doc = new IDDocument(listDocType[0], listDocNumber[0], listDocIssuedCountry[0], listDocDeliveryDate[0], listDocExpireDate[0]);
                KYC kyc = new KYC(listSitFam[0]);

                _dtClient.Rows.Add(new object[] {
                    listRefCustomer[0], listSex[0] == "M" ? "M." : "Mme", listLastName[0], listFirstName[0], listBirthDate[0], listBirthPlace[0], listBirthCountry[0], listNationality[0], listAccountNumber[0],
                    listIBAN[0], listRefCloseAction[0], true, (listFS[0] == "1") ? true : false, listNewLastName[0], addr, doc, kyc, listPatrimoine[0] });
            }
            BindClientList();

            _dtOpeOffUs = GetOpeList(false);
            for (int i = 0; i < _dtOpeOffUs.Rows.Count; i++)
            {
                if (listOpeCashSelect.Count == 0)
                    for (int j = 0; j < _dtOpeOffUs.Rows.Count; j++)
                    {
                        _dtOpeOffUs.Rows[j]["Selected"] = true;
                    }
                else
                    for (int j = 0; j < listOpeCashSelect.Count; j++)
                    {
                        List<string> listIdOpe = CommonMethod.GetAttributeValues(listOpeCashSelect[j], "OPE_CASH", "sIdOpe");
                        if (_dtOpeOffUs.Rows[i]["sIdOpe"].ToString() == listIdOpe[0].ToString())
                        {
                            _dtOpeOffUs.Rows[i]["Selected"] = true;
                            break;
                        }
                    }
            }
            BindOpeList(false);

            if (listOpeCount.Count > 0 && !String.IsNullOrWhiteSpace(listOpeCount[0]))
                txtOpeCountSum.Text = listOpeCount[0];
            if (listOpeAmount.Count > 0 && !String.IsNullOrWhiteSpace(listOpeAmount[0]))
                txtOpeAmountSum.Text = listOpeAmount[0].Replace('.', ',') + " €";
            if (_RightsCorrection)
            {
                txtOpeCountSum.ReadOnly = false;
                txtOpeCountSum.CssClass = "";
                txtOpeCountSum.Attributes["onclick"] = "";
                imgOpe1.Visible = true;
                txtOpeAmountSum.ReadOnly = false;
                txtOpeAmountSum.CssClass = "";
                txtOpeAmountSum.Attributes["onclick"] = "";
                imgOpe2.Visible = true;
                txtPeriodeStartDate.Enabled = true;
            }

            if (listReasons.Count > 0)
            {
                DataTable dtReasons = new DataTable();
                dtReasons.Columns.Add("Value");
                dtReasons.Columns.Add("Text");

                for (int i = 0; i < listReasons.Count; i++)
                {
                    List<string> listRefStatementReason = CommonMethod.GetAttributeValues(listReasons[i], "REASON", "RefStmtReason");
                    List<string> listStmtReasonLabel = CommonMethod.GetAttributeValues(listReasons[i], "REASON", "StmtReasonLabel");
                    List<string> listRefArticle = CommonMethod.GetAttributeValues(listReasons[i], "REASON", "RefArticle");

                    dtReasons.Rows.Add(new object[] { String.Format("{0}{1}", listRefArticle[0], (!string.IsNullOrWhiteSpace(listRefStatementReason[0])) ? "|" + listRefStatementReason[0] : ""), String.Format("{0} Art {1}", listStmtReasonLabel[0], listRefArticle[0]) });
                }

                multiReason.ListChoicesSelected = dtReasons;
                multiReason.BindListChoiceSelected();
                UpdateMainArticle();
            }

            string sCustomerDirPath = ConfigurationManager.AppSettings["NickelDocPath"].ToString();
            for (int i = 0; i < listFiles.Count; i++)
            {
                List<string> listStoreFileName = CommonMethod.GetTagValues(listFiles[i], "FILE/StoreFileName");
                List<string> listStorePath = CommonMethod.GetTagValues(listFiles[i], "FILE/StorePath");
                List<string> listRefCustomer = CommonMethod.GetTagValues(listFiles[i], "FILE/RefCustomer");
                List<string> listType = CommonMethod.GetTagValues(listFiles[i], "FILE/Type");
                List<string> listContentType = CommonMethod.GetTagValues(listFiles[i], "FILE/ContentType");
                List<string> listDate = CommonMethod.GetTagValues(listFiles[i], "FILE/Date");
                List<string> listFileDescription = CommonMethod.GetTagValues(listFiles[i], "FILE/FileDescription");
                List<string> listDocCatTAG = CommonMethod.GetTagValues(listFiles[i], "FILE/DocCategoryTAG");
               
                string sCustomerWebDirName = tools.GetCustomerWebDirectoryName(int.Parse(listRefCustomer[0]));
                string sClientDirPath = "~" + sCustomerDirPath + "/" + sCustomerWebDirName;

                bool bLocalStorage = File.Exists(HostingEnvironment.MapPath(sClientDirPath + "/" + listStoreFileName[0]));

                _dtStatementFiles.Rows.Add(new object[] { listStoreFileName[0], listDocCatTAG[0], listType[0], (listContentType.Count > 0) ? listContentType[0] : "", bLocalStorage, sClientDirPath + "/" + listStoreFileName[0], (listFileDescription.Count > 0) ? listFileDescription[0] : "", listDate[0], listStorePath[0], "", listRefCustomer[0] });
            }

            if (_sLastAction == "VALIDATE")
                if(string.IsNullOrWhiteSpace(txtOpeCountSum.Text) || txtOpeCountSum.Text.Trim() == "0")
                    AlertMessage("Erreur", "Une erreur est survenue lors de la récupération des opérations en jeu.<br/><br/>Veuillez recharger la page afin de pouvoir télécharger la déclaration au format XML.");
                else btnDownloadXML.Visible = true;

            // ENABLE AUTOSAVE FOR ANALYSTS
            if (_RightsCreation && (_sLastAction == "STARTCREATION" || _sLastAction == "CREATIONPENDING" || _sLastAction == "CANCELVALIDATION"))
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "EnableAutoSave();", true);

            #endregion
        }
    }

    private void ParseStreetNumberInfos(string sStreetNumber, out int iAddStreetNumber, out string sComplementVoie)
    {
        iAddStreetNumber = 0;
        sComplementVoie = "";
        if(!String.IsNullOrWhiteSpace(sStreetNumber) && !int.TryParse(sStreetNumber, out iAddStreetNumber))
        {
            string sNumString = Regex.Match(sStreetNumber, @"\d+").Value;
            int.TryParse(sNumString, out iAddStreetNumber);

            if (sStreetNumber.Contains("B"))
                sComplementVoie = "B";
            if (sStreetNumber.Contains("T"))
                sComplementVoie = "T";
            if (sStreetNumber.Contains("Q"))
                sComplementVoie = "Q";
        }
    }

    protected void BindActions(List<string> listActions)
    {
        for (int i = 0; i < listActions.Count; i++)
        {
            List<string> listRefBoUser = CommonMethod.GetAttributeValues(listActions[i], "ACTION", "RefBoUser");
            List<string> listBoUserLastName = CommonMethod.GetAttributeValues(listActions[i], "ACTION", "BoUserLastName");
            List<string> listBoUserFirstName = CommonMethod.GetAttributeValues(listActions[i], "ACTION", "BoUserFirstName");
            List<string> listAction = CommonMethod.GetAttributeValues(listActions[i], "ACTION", "Action");
            List<string> listActionDate = CommonMethod.GetAttributeValues(listActions[i], "ACTION", "ActionDate");
            List<string> listComment = CommonMethod.GetTagValues(listActions[i], "ACTION/COMMENT");

            switch (listAction[0])
            {
                case "STARTCREATION":
                case "CREATIONPENDING":
                    if (listAction[0] == "STARTCREATION")
                        _CreationStarted = true;
                    txtBoUserOwner.Text = listBoUserFirstName[0] + " " + listBoUserLastName[0];
                    txtDREndDate.Text = listActionDate[0].Substring(0, 10);
                    txtPeriodeEndDate.Text = listActionDate[0].Substring(0, 10);
                    break;
                case "ENDCREATION":
                    txtBoUserOwner.Text = listBoUserFirstName[0] + " " + listBoUserLastName[0];
                    txtDateToValidation.Text = listActionDate[0].Substring(0, 10);
                    panelDateToValidation.Visible = true;
                    break;
                case "VALIDATION":
                //case "EDITVALIDATION":
                    txtBoUserValidator.Text = listBoUserFirstName[0] + " " + listBoUserLastName[0];
                    panelBoUserValidator.Visible = true;
                    break;
            }

            // LAST ACTION
            if (i == listActions.Count - 1)
            {
                BindLastActionState(listAction[0]);

                if(listComment.Count > 0 && !String.IsNullOrWhiteSpace(listComment[0]))
                    AlertMessage(String.Format("Commentaire de {0} {1}", listBoUserFirstName[0], listBoUserLastName[0]), String.Format("<span style=\"color:black\">{0}</span>", listComment[0]));
            }
        }
    }

    protected void BindLastActionState(string sLastAction)
    {
        _sLastAction = sLastAction;
        btnOtherAction.Click += btnAction_Click;

        switch (sLastAction)
        {
            case "STARTCREATION":
            case "CREATIONPENDING":
            case "CANCELVALIDATION":
                if (!_RightsCreation)
                    panelSave.Visible = false;
                break;
            case "ENDCREATION":
            case "CANCELAPPROVAL":
            case "EDITVALIDATION":
                if (_RightsValidation)
                {
                    btnSave.Visible = false;
                    btnAction.Visible = true;
                    btnAction.CommandArgument = "VALIDATION";
                    btnAction.Text = "Valider";

                    btnOtherAction.Text = "Enregistrer les modifications";
                    btnOtherAction.CommandArgument = "EDITVALIDATION";

                    btnOtherAction2.Text = "Revoir";
                    btnOtherAction2.OnClientClick = "ShowCommentDialog();return false;";
                    btnOtherActionComment.CommandArgument = "CANCELVALIDATION";
                    btnOtherAction2.Visible = true;
                }
                else if(_RightsCorrection)
                {
                    btnSave.Visible = false;
                    btnAction.Visible = true;
                    btnAction.Text = "Enregistrer les modifications";
                    btnAction.CommandArgument = "EDITVALIDATION";

                    btnOtherAction.Text = "Revoir";
                    btnOtherAction.OnClientClick = "ShowCommentDialog();return false;";
                    btnOtherActionComment.CommandArgument = "CANCELVALIDATION";
                }
                else panelSave.Visible = false;
                break;
            case "VALIDATION":
                _Editable = false;
                SetControlsReadOnly();
                if (_RightsApproval)
                {
                    btnSave.Visible = false;
                    btnAction.Visible = true;
                    btnAction.CommandArgument = "APPROVAL";
                    btnAction.Text = "Approuver";

                    btnOtherAction.Text = "Annuler";
                    btnOtherAction.OnClientClick = "ShowCommentDialog();return false;";
                    btnOtherActionComment.CommandArgument = "CANCELAPPROVAL";
                }
                else panelSave.Visible = false;
                break;
            case "APPROVAL":
            case "STARTERMESINPUT":
                _Editable = false;
                SetControlsReadOnly();
                if (_RightsCreation)
                {
                    btnSave.Visible = false;
                    btnAction.Visible = true;
                    btnAction.CommandArgument = "ENDERMESINPUT";
                    btnAction.Text = "J'ai fini ma saisie ERMES";

                    btnOtherAction.Visible = false;
                    btnDownloadXML.Visible = true;

                    //panelMoreOptions.Visible = false;
                    //btnMoreOptions.Visible = false;
                }
                else panelSave.Visible = false;
                break;
            case "ENDERMESINPUT":
                _Editable = false;
                SetControlsReadOnly();
                panelTRACFIN.Visible = true;
                if (_RightsCreation)
                {
                    btnSave.Visible = false;
                    btnAction.Visible = true;
                    btnAction.CommandArgument = "COMPLETION";
                    btnAction.Text = "Enregistrer la référence TRACFIN";

                    panelMoreOptions.Visible = false;
                    btnMoreOptions.Visible = false;
                }
                else panelSave.Visible = false;
                break;
            case "COMPLETION":
                panelSave.Visible = false;
                _Editable = false;
                SetControlsReadOnly();
                break;
        }
    }

    protected void BindClientList()
    {
        rptClient.DataSource = _dtClient;
        rptClient.DataBind();
        upClient.Update();
    }
    protected void clientFound(object sender, EventArgs e)
    {
        int iRefCustomerFound = clientSearch1.RefCustomer;

        string sXml = Client.GetCustomerInformations(iRefCustomerFound);

        if(!String.IsNullOrWhiteSpace(sXml))
        {
            List<string> listPoliteness = CommonMethod.GetAttributeValues(sXml, "ALL_XML_OUT/Customer", "Politeness");
            List<string> listLastName = CommonMethod.GetAttributeValues(sXml, "ALL_XML_OUT/Customer", "LastName");
            List<string> listFirstName = CommonMethod.GetAttributeValues(sXml, "ALL_XML_OUT/Customer", "FirstName");
            List<string> listAccountNumber = CommonMethod.GetAttributeValues(sXml, "ALL_XML_OUT/Customer", "AccountNumber");
            List<string> listBirthDate = CommonMethod.GetAttributeValues(sXml, "ALL_XML_OUT/Customer", "BirthDate");
            List<string> listIBAN = CommonMethod.GetAttributeValues(sXml, "ALL_XML_OUT/Customer", "IBAN");
            List<string> listBankStatus = CommonMethod.GetAttributeValues(sXml, "ALL_XML_OUT/Customer", "BankStatus");
            List<string> listClosingInProgress = CommonMethod.GetAttributeValues(sXml, "ALL_XML_OUT/Customer", "ClosingInProgress");

            bool bExists = false;
            for (int i = 0; i < _dtClient.Rows.Count; i++)
            {
                if(_dtClient.Rows[i]["RefCustomer"].ToString() == iRefCustomerFound.ToString())
                {
                    bExists = true;
                    break;
                }
            }
            if (!bExists)
            {
                int iRefCloseAction = 0;
                if (listBankStatus[0] == "4")
                    iRefCloseAction = 5;
                else if (listClosingInProgress[0] == "1")
                    iRefCloseAction = 4;

                _dtClient.Rows.Add(new object[] { iRefCustomerFound.ToString(), listPoliteness[0], listLastName[0], listFirstName[0], listBirthDate[0], null, null, null,
                    listAccountNumber[0], listIBAN[0], iRefCloseAction.ToString(), true, true });

                BindClientList();
                RefreshClientMetaData();
                BindOpeList(true);
            }
        }
    }
    protected void rptClient_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item ||
       e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hdn = (HiddenField)e.Item.FindControl("hdnCloseAction");
            DropDownList ddl = (DropDownList)e.Item.FindControl("ddlCloseAction");

            if (hdn != null && ddl != null)
            {
                ListItem item = ddl.Items.FindByValue(hdn.Value);
                if (item != null)
                {
                    item.Selected = true;

                    //if (_Editable)
                    //    item.Enabled = true;
                    //else item.Enabled = false;

                    //if (ddl.SelectedValue == "3" || ddl.SelectedValue == "4")
                    if (ddl.SelectedValue == "3")
                        ddl.Enabled = false;
                }
            }
        }
    }
    protected void ckbCheckClient_CheckedChanged(object sender, EventArgs e)
    {
        RefreshClientMetaData();
    }
    protected void UpdateClientCheckState()
    {
        // CUSTOMER SELECT
        foreach (RepeaterItem item in rptClient.Items)
        {
            if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
            {
                HiddenField hdn = (HiddenField)item.FindControl("hdnRefCustomer");
                DropDownList ddl = (DropDownList)item.FindControl("ddlCloseAction");
                CheckBox ckb = (CheckBox)item.FindControl("ckbCheck");

                if (hdn != null && ckb != null)
                {
                    for (int i = 0; i < _dtClient.Rows.Count; i++)
                    {
                        if (_dtClient.Rows[i]["RefCustomer"].ToString() == hdn.Value)
                        {
                            _dtClient.Rows[i]["RefCloseAction"] = ddl.SelectedValue;
                            _dtClient.Rows[i]["Selected"] = ckb.Checked;
                            break;
                        }
                    }
                }
            }
        }

        // FS
        foreach (RepeaterItem item in rptCustomerFS.Items)
        {
            if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
            {
                HiddenField hdn = (HiddenField)item.FindControl("hdnRefCustomer");
                CheckBox ckb = (CheckBox)item.FindControl("ckbCheck");

                if (hdn != null && ckb != null)
                {
                    for (int i = 0; i < _dtClient.Rows.Count; i++)
                    {
                        if (_dtClient.Rows[i]["RefCustomer"].ToString() == hdn.Value)
                        {
                            _dtClient.Rows[i]["FS"] = ckb.Checked;
                            break;
                        }
                    }
                }
            }
        }
    }


    protected void BindStatementReasons()
    {
        multiReason.ListChoices = GetStatementReasons();
    }
    protected DataTable GetStatementReasons()
    {
        SqlConnection conn = null;
        DataTable dtReasons = new DataTable();
        dtReasons.Columns.Add("Text");
        dtReasons.Columns.Add("Value");

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("TracFin.P_GetReason", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN", new XElement("REASONS", new XAttribute("CashierToken", authentication.GetCurrent().sToken))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if(sXmlOut.Length > 0)
            {
                List<string> listReason = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/REASONS/REASON");

                for (int i = 0; i < listReason.Count; i++)
                {
                    List<string> listRefArt = CommonMethod.GetAttributeValues(listReason[i], "REASON", "RefArticle");
                    List<string> listLabel = CommonMethod.GetAttributeValues(listReason[i], "REASON", "StmtReasonLabel");
                    List<string> listRef = CommonMethod.GetAttributeValues(listReason[i], "REASON", "RefStmtReason");

                    dtReasons.Rows.Add(new object[] { String.Format("{0} Art {1}", listLabel[0], listRefArt[0]), String.Format("{0}|{1}", listRefArt[0], listRef[0]) });
                }
            }
        }
        catch(Exception e)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dtReasons;
    }
    protected void multiReasonChoiceAddedRemoved(object sender, EventArgs e)
    {
        UpdateMainArticle();
    }
    protected void btnAddOtherReason_Click(object sender, EventArgs e)
    {
        if (!String.IsNullOrWhiteSpace(txtOtherReasonLabel.Text)
            && !String.IsNullOrWhiteSpace(ddlOtherReasonArticle.SelectedValue))
        {
            multiReason.AddListChoice(
                txtOtherReasonLabel.Text + " " + ddlOtherReasonArticle.SelectedItem.Text,
                ddlOtherReasonArticle.SelectedValue+"|noref", true);

            txtOtherReasonLabel.Text = "";
            ddlOtherReasonArticle.SelectedIndex = 0;
        }
        else AlertMessage("Erreur", "Autre motif : champs incomplets");
    }
    protected void UpdateMainArticle()
    {
        int iArt = 0;

        DataTable dtReasons = multiReason.GetSelectedChoices();
        for (int i = 0; i < dtReasons.Rows.Count; i++)
        {
            string[] arValue = dtReasons.Rows[i]["Value"].ToString().Split('|');

            if (iArt == 0)
                iArt = int.Parse(arValue[0]);
            else if (iArt != int.Parse(arValue[0]))
            {
                iArt = 3;
                break;
            }
        }

        _iArticle = iArt;
        if (iArt > 0)
            litArticle.Text = "Article " + ((iArt == 3) ? "1 et 2" : iArt.ToString());
        else litArticle.Text = "";
    }


    protected void BindOpeList(bool bRefresh)
    {
        if(bRefresh)
            _dtOpeOffUs = GetOpeList(true);

        rptOpeOffUs.DataSource = _dtOpeOffUs;
        rptOpeOffUs.DataBind();

        lblNbOpeTotal.Text = _dtOpeOffUs.Rows.Count.ToString();

        upOpeOffUs.Update();

        RefreshOpeSum();
    }
    protected DataTable GetOpeList(bool bAutoSelect)
    {
        SqlConnection conn = null;
        DataTable dt = new DataTable();
        dt.Columns.Add("sIdOpe");
        dt.Columns.Add("Date");
        dt.Columns.Add("Time");
        dt.Columns.Add("Place");
        dt.Columns.Add("Object");
        dt.Columns.Add("Amount", typeof(decimal));
        dt.Columns.Add("Category");
        dt.Columns.Add("Selected", typeof(bool));
        dt.Columns.Add("CustomerName");
        dt.Columns.Add("DateTime", typeof(DateTime));

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("TracFin.P_GetOpeCashIn", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);

            XElement xmlCust = new XElement("CUSTOMERS");
            for (int i = 0; i < _dtClient.Rows.Count; i++)
                xmlCust.Add(new XElement("CUSTOMER",
                                new XAttribute("Ref",_dtClient.Rows[i]["RefCustomer"])));

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                            new XElement("OPE_CASH_IN",
                                                new XAttribute("CashierToken", authentication.GetCurrent().sToken),
                                                //new XAttribute("DateFrom", txtPeriodeStartDate.Text),
                                                new XAttribute("DateTo", txtPeriodeEndDate.Text),
                                                xmlCust)).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();
            
            if (sXmlOut.Length > 0)
            {
                List<string> listOpe = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/OPE_CASH_IN/OPE_DETAILS");

                for (int i = 0; i < listOpe.Count; i++)
                {
                    List<string> listIdOpe = CommonMethod.GetAttributeValues(listOpe[i], "OPE_DETAILS", "sIdOpe");
                    List<string> listDateTime = CommonMethod.GetAttributeValues(listOpe[i], "OPE_DETAILS", "Datetime");
                    List<string> listObject = CommonMethod.GetAttributeValues(listOpe[i], "OPE_DETAILS", "Object");
                    List<string> listAmount = CommonMethod.GetAttributeValues(listOpe[i], "OPE_DETAILS", "Amount");
                    List<string> listCategory = CommonMethod.GetAttributeValues(listOpe[i], "OPE_DETAILS", "Category");
                    List<string> listLastName = CommonMethod.GetAttributeValues(listOpe[i], "OPE_DETAILS", "LastName");
                    List<string> listFirstName = CommonMethod.GetAttributeValues(listOpe[i], "OPE_DETAILS", "FirstName");

                    DateTime date;
                    bool bDateOk = false;
                    bDateOk = DateTime.TryParseExact(listDateTime[0], "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out date);

                    decimal dAmount;

                    if (decimal.TryParse(listAmount[0].ToString().Replace(".", ","), NumberStyles.Currency, new CultureInfo("fr-FR"), out dAmount))
                        dt.Rows.Add(listIdOpe[0],
                            (bDateOk) ? date.ToString("dd/MM/yyyy") : "N.C.",
                            (bDateOk) ? date.ToString("HH'h'mm") : "N.C.",
                            "",
                            listObject[0],
                            dAmount,
                            listCategory[0],
                            bAutoSelect,
                            listLastName[0] + " " + listFirstName[0],
                            date);
                }
            }
        }
        catch(Exception e)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }
    protected void RefreshOpeSum()
    {
        int iNbOpe = 0;
        decimal dOpeAmount = 0, dTmp = 0;
        DateTime minDate = DateTime.Now.AddDays(1);

        for (int i = 0; i < _dtOpeOffUs.Rows.Count; i++)
        {
            if (bool.Parse(_dtOpeOffUs.Rows[i]["Selected"].ToString()) == true)
            {
                dTmp = 0;
                if (Decimal.TryParse(_dtOpeOffUs.Rows[i]["Amount"].ToString().Replace(".", ","), NumberStyles.Currency, new CultureInfo("fr-FR"), out dTmp))
                {
                    dOpeAmount += dTmp;
                    iNbOpe++;

                    DateTime dateOpe;
                    if(DateTime.TryParseExact(_dtOpeOffUs.Rows[i]["Date"].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateOpe))
                    {
                        if (DateTime.Compare(minDate, dateOpe) > 0)
                            minDate = dateOpe;
                    }
                }
            }
        }

        txtOpeCountSum.Text = iNbOpe.ToString();
        lblNbOpeSelect.Text = iNbOpe.ToString();
        txtOpeAmountSum.Text = dOpeAmount.ToString() + " €";        

        DateTime datePeriodeEnd;
        if(DateTime.TryParseExact(txtPeriodeEndDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out datePeriodeEnd))
            if (DateTime.Compare(minDate, datePeriodeEnd) > 0)
                minDate = datePeriodeEnd;
        if (DateTime.Compare(minDate, DateTime.Now) <= 0)
            txtPeriodeStartDate.Text = minDate.ToString("dd/MM/yyyy");

        upOpeSum.Update();
    }
    protected void btnUpdateOpeDetails_Click(object sender, EventArgs e)
    {
        foreach (RepeaterItem item in rptOpeOffUs.Items)
        {
            if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
            {
                CheckBox ckb = (CheckBox)item.FindControl("ckbCheck");
                HiddenField hdn = (HiddenField)item.FindControl("hdnIdOpe");

                if (ckb != null && hdn != null)
                {
                    for (int i = 0; i < _dtOpeOffUs.Rows.Count; i++)
                    {
                        if (_dtOpeOffUs.Rows[i]["sIdOpe"].ToString() == hdn.Value)
                        {
                            _dtOpeOffUs.Rows[i]["Selected"] = ckb.Checked;
                            break;
                        }
                    }
                }
            }
        }

        RefreshOpeSum();
    }


    protected void BindAlertDetails(int iRefAlert)
    {
        DataTable dt = AML.GetAlertGeneralInfo(iRefAlert);

        if (dt.Rows.Count > 0)
        {
            DataTable dtInfos = new DataTable();
            dtInfos.Columns.Add("Label");
            dtInfos.Columns.Add("Value");

            for (int i = 0; i < dt.Columns.Count; i++)
            {
                if (i == 0)
                {
                    _sAlertType = dt.Rows[0][i].ToString();
                    dtInfos.Rows.Add(new object[] { "Type", _sAlertType });
                }
                else if (dt.Columns[i].Caption.Contains("Label") && !String.IsNullOrWhiteSpace(dt.Rows[0][i].ToString()))
                {
                    if (i + 1 < dt.Columns.Count)
                    {
                        bool bShow = true;
                        string sLabel, sValue;
                        sLabel = dt.Rows[0][i].ToString();
                        sValue = dt.Rows[0][i + 1].ToString();

                        if (dt.Rows[0][0].ToString().ToLower().Contains("réquisition"))
                        {
                            if (i == 1)
                                bShow = false;
                            if (i == 5)
                            {
                                int iCloseAccount;
                                int.TryParse(sValue.Split('.')[0], out iCloseAccount);

                                sValue = (iCloseAccount == 0) ? "Non" : "Oui";
                            }
                            if (i == 7)
                            {
                                DataTable dtReq = Requisition.GetMotifRequisitionList();
                                String[] arMotif = sValue.Split(';');
                                if (dtReq != null && dtReq.Rows.Count > 0 && arMotif.Length > 0)
                                {
                                    StringBuilder sb = new StringBuilder();

                                    for (int j = 0; j < arMotif.Length; j++)
                                    {
                                        for (int k = 0; k < dtReq.Rows.Count; k++)
                                        {
                                            if (arMotif[j] == dtReq.Rows[k]["Value"].ToString())
                                            {
                                                sb.AppendLine(dtReq.Rows[k]["Text"].ToString());
                                                break;
                                            }
                                        }
                                    }

                                    sValue = sb.ToString().Replace(Environment.NewLine, "<br/>");
                                }
                            }
                        }

                        if (sLabel == "Scoring" || sLabel == "Scoring Details")
                            bShow = false;

                        if (bShow)
                            dtInfos.Rows.Add(new object[] { sLabel, sValue });
                    }
                }
            }

            rptAlertInfos.DataSource = dtInfos;
            rptAlertInfos.DataBind();

            string sAlert = AML.getAlertDetails(iRefAlert.ToString());
            if (!String.IsNullOrWhiteSpace(sAlert))
            {
                List<string> listDetails = CommonMethod.GetTags(sAlert, "ALERTE/DETAIL/table");

                if (listDetails.Count > 0)
                {
                    litAlertDetailsTable.Text = listDetails[0];
                    panelAlertDetailsTable.Visible = true;
                }
            }
        }
        else panelAlertDetails.Visible = false;
    }


    protected void RefreshClientMetaData()
    {
        // FILES
        UpdateClientCheckState();

        BindFSFiles();

        DataTable dtClientNew = _dtClient.Clone();
        DataTable dtStatementFilesNew = _dtStatementFiles.Clone();

        DataTable dtClientFile = new DataTable();
        dtClientFile.Columns.Add("Text");
        dtClientFile.Columns.Add("Value");

        for (int i = 0; i < _dtClient.Rows.Count; i++)
        {
            if ((bool)_dtClient.Rows[i]["Selected"])
            {
                dtClientFile.Rows.Add(new object[] { _dtClient.Rows[i]["LastName"] + " " + _dtClient.Rows[i]["FirstName"], _dtClient.Rows[i]["RefCustomer"] });
                dtClientNew.Rows.Add(_dtClient.Rows[i].ItemArray);
            }

            // REMOVE STATEMENT FILES OF REMOVED CLIENT
            for (int x = 0; x < _dtStatementFiles.Rows.Count; x++)
            {
                if (_dtStatementFiles.Rows[x]["RefCustomer"].ToString() == _dtClient.Rows[i]["RefCustomer"].ToString())
                    dtStatementFilesNew.Rows.Add(_dtStatementFiles.Rows[x].ItemArray);
            }
        }

        if (dtClientNew.Rows.Count != _dtClient.Rows.Count) {
            _dtClient = dtClientNew;
            BindClientList();
            BindOpeList(true);
        }
        if (dtStatementFilesNew.Rows.Count != _dtStatementFiles.Rows.Count)
        {
            _dtStatementFiles = dtStatementFilesNew;
            BindStatementFiles();
        }

        panelFileClientList.Visible = false;
        panelCustomerFiles.Visible = false;

        if (dtClientFile.Rows.Count > 0)
        {
            if(_Editable)
                panelCustomerFiles.Visible = true;
            ddlFileClientList.DataSource = dtClientFile;
            ddlFileClientList.DataBind();

            tools.ReorderAlphabetized(ddlFileClientList);

            BindFileControls();

            if (dtClientFile.Rows.Count > 1)
                panelFileClientList.Visible = true;
        }
        upFile.Update();

        // WHO STATEMENT
        if (_dtClient.Rows.Count > 0)
        {
            panelWhoStatement.Visible = true;

            string sOurClient = "";

            if (_dtClient.Rows.Count > 1)
                sOurClient = "nos clients";
            else
            {
                if (String.IsNullOrWhiteSpace(_dtClient.Rows[0]["Politeness"].ToString())
                    || _dtClient.Rows[0]["Politeness"].ToString() == "M.")
                    sOurClient = "notre client";
                else sOurClient = "notre cliente";
            }

            litFPEDeclare.Text = String.Format("FPE décide conséquemment de déclarer {0}.", sOurClient);
        }
        else
        {
            panelWhoStatement.Visible = false;
        }
        upWhoStatement.Update();
    }
    protected void BindFileControls()
    {
        int iRefClientFile = int.Parse(ddlFileClientList.SelectedValue);
        clientFileManager1.RefCustomer = iRefClientFile;
        clientFileManager1.UpdateClientFiles();
        clientFileUpload1.RefCustomer = iRefClientFile;
    }
    protected void ddlFileClientList_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindFileControls();
        upFile.Update();
    }
    protected void btnAddFileToStatement_Click(object sender, EventArgs e)
    {
        DataTable dt = clientFileManager1.GetSelectedFiles();
        bool bExists = false;

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            bExists = false;

            for (int j = 0; j < _dtStatementFiles.Rows.Count; j++)
            {
                if(dt.Rows[i]["RefCustomer"].ToString() == _dtStatementFiles.Rows[j]["RefCustomer"].ToString()
                    && dt.Rows[i]["FileName"].ToString() == _dtStatementFiles.Rows[j]["FileName"].ToString()
                    && dt.Rows[i]["StorePath"].ToString() == _dtStatementFiles.Rows[j]["StorePath"].ToString())
                {
                    bExists = true;
                    break;
                }
            }

            if(!bExists)
            {
                _dtStatementFiles.Rows.Add(dt.Rows[i].ItemArray);
            }
        }

        BindStatementFiles();
    }
    protected void BindStatementFiles()
    {
        rptStatementFiles.Visible = false;
        panelStatementFilesEmpty.Visible = false;
        btnRemoveFileToStatement.Visible = false;

        if (_dtStatementFiles.Rows.Count > 0)
        {
            rptStatementFiles.DataSource = _dtStatementFiles;
            rptStatementFiles.DataBind();
            rptStatementFiles.Visible = true;
            btnRemoveFileToStatement.Visible = true;
        }
        else
        {
            panelStatementFilesEmpty.Visible = true;
        }
    }
    protected void BindFSFiles()
    {
        rptCustomerFS.Visible = false;
        panelCustomerFSEmpty.Visible = false;

        DataTable dt = _dtClient.Clone();

        if (_dtClient.Rows.Count > 0)
        {
            for (int i = 0; i < _dtClient.Rows.Count; i++)
            {
                if ((bool)_dtClient.Rows[i]["Selected"])
                    dt.Rows.Add(_dtClient.Rows[i].ItemArray);
            }

            if (dt.Rows.Count > 0)
            {
                rptCustomerFS.DataSource = dt;
                rptCustomerFS.DataBind();
                rptCustomerFS.Visible = true;
            }
            else panelCustomerFSEmpty.Visible = true;
        }
        else panelCustomerFSEmpty.Visible = true;
    }
    protected string GetCustomerName(string sRefCustomer)
    {
        return ddlFileClientList.Items.FindByValue(sRefCustomer).Text;
    }
    protected void btnRemoveFileToStatement_Click(object sender, EventArgs e)
    {
        DataTable dt = _dtStatementFiles.Clone();

        if (_dtStatementFiles != null && _dtStatementFiles.Rows.Count > 0)
        {
            foreach (RepeaterItem item in rptStatementFiles.Items)
            {
                if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                {
                    CheckBox ckb = item.FindControl("ckbSelect") as CheckBox;

                    if (ckb != null && !ckb.Checked)
                    {
                        if (_dtStatementFiles.Rows.Count > item.ItemIndex)
                            dt.Rows.Add(_dtStatementFiles.Rows[item.ItemIndex].ItemArray);
                    }
                }
            }
        }

        _dtStatementFiles = dt;
        BindStatementFiles();
    }


    protected void btnSave_Click(object sender, EventArgs e)
    {
        string sErrorMessage;
        if (CheckForm(false, out sErrorMessage))
        {
            if (SaveStatement(null))
            {
                if (!_CreationStarted)
                {
                    if (SetAction("STARTCREATION", null))
                        _CreationStarted = true;
                }
            }
            else AlertMessage("Erreur", "Une erreur est survenue lors de l'enregistrement.");
        }
        else AlertMessage("Erreur", sErrorMessage);
    }
    protected bool SaveStatement(string sXmlIn)
    {
        bool bSaved = false;
        if(sXmlIn == null)
            sXmlIn = FormToXML();
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("TracFin.P_SetStatementDetails", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@XMLIn", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@XMLOut", SqlDbType.VarChar, 8000);

            cmd.Parameters["@XMLIn"].Value = sXmlIn;
            cmd.Parameters["@XMLOut"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@XMLOut"].Value.ToString();

            if(sXmlOut.Length > 0)
            {
                List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/TRACFIN_STATEMENT", "RC");
                List<string> listRefStatement = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/TRACFIN_STATEMENT", "RefStatment");

                if (listRC.Count > 0 && listRC[0] == "0")
                {
                    bSaved = true;

                    if (_RefStatement == 0 && listRefStatement.Count > 0 && !String.IsNullOrWhiteSpace(listRefStatement[0]))
                        _RefStatement = int.Parse(listRefStatement[0]);
                }
            }
        }
        catch(Exception e)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return bSaved;
    }
    protected string FormToXML()
    {
        Decimal dOpeAmount;
        Decimal.TryParse(txtOpeAmountSum.Text.Replace("€", "").Replace(".", ",").Trim(), NumberStyles.Currency, new CultureInfo("fr-FR"), out dOpeAmount);

        XElement xml = 
            new XElement("TRACFIN_STATEMENT",
                    new XAttribute("CashierToken", authentication.GetCurrent(true).sToken),
                    new XAttribute("RefStatementExt", txtRefStatementExt.Text),
                    new XAttribute("RefInitStmtInt", txtRefInitStatementInt.Text),
                    new XAttribute("RefInitStmtExt", txtRefInitStatementExt.Text),
                    new XAttribute("OpeOriginDRDate", txtOpeOriginDRDate.Text),
                    new XAttribute("DRStartDate", txtDRStartDate.Text),
                    new XAttribute("DREndDate", txtDREndDate.Text),
                    new XAttribute("OpeCount", txtOpeCountSum.Text),
                    new XAttribute("OpeAmount", dOpeAmount),
                    new XElement("ATYPICAL_FUNDS_CIRCULATION_IN", txtAtypicalFundsCirculationIN.Text),
                    new XElement("ATYPICAL_FUNDS_CIRCULATION_OUT", txtAtypicalFundsCirculationOUT.Text),
                    new XElement("ATYPICAL_FUNDS_CIRCULATION_OTHER", txtAtypicalFundsCirculationOTHER.Text)
            );

        if (rblAlertKind.SelectedValue == "Manual")
            xml.Add(new XElement("AlertKindManual", txtAlertKind.Text));

        UpdateClientCheckState();
        XElement xmlCustomers = new XElement("CUSTOMERS");
        for (int i = 0; i < _dtClient.Rows.Count; i++)
        {
            if ((bool)_dtClient.Rows[i]["Selected"])
            {
                xmlCustomers.Add(
                    new XElement("CUSTOMER",
                        new XAttribute("RefCustomer", _dtClient.Rows[i]["RefCustomer"]),
                        new XAttribute("RefCloseAction", _dtClient.Rows[i]["RefCloseAction"]),
                        new XAttribute("FS", _dtClient.Rows[i]["FS"]))
                    );
            }
        }
        xml.Add(xmlCustomers);

        XElement xmlOpe = new XElement("OPE_CASH_SELECT");
        for (int i = 0; i < _dtOpeOffUs.Rows.Count; i++)
        {
            if ((bool)_dtOpeOffUs.Rows[i]["Selected"])
                xmlOpe.Add(new XElement("OPE_CASH", new XAttribute("sIdOpe", _dtOpeOffUs.Rows[i]["sIdOpe"])));
        }
        xml.Add(xmlOpe);

        XElement xmlReasons = new XElement("REASONS");
        DataTable dtReasons = multiReason.GetSelectedChoices();
        for (int i = 0; i < dtReasons.Rows.Count; i++)
        {
            string[] arValue = dtReasons.Rows[i]["Value"].ToString().Split('|');
            xmlReasons.Add(new XElement("REASON",
                                new XAttribute("RefStatementReason", (arValue.Length > 1 && arValue[1] != "noref") ? arValue[1] : ""),
                                new XAttribute("StmtReasonLabel", dtReasons.Rows[i]["Text"].ToString().Substring(0, dtReasons.Rows[i]["Text"].ToString().LastIndexOf(" Art "))),
                                new XAttribute("RefArticle", arValue[0])
                                ));
        }
        xml.Add(xmlReasons);

        XElement xmlFiles = new XElement("FILES");
        if (_dtStatementFiles.Rows.Count > 0)
        {
            for (int i = 0; i < _dtStatementFiles.Rows.Count; i++)
            {
                xmlFiles.Add(new XElement("FILE",
                                new XAttribute("RefCustomer", _dtStatementFiles.Rows[i]["RefCustomer"]),
                                new XAttribute("StorePath", _dtStatementFiles.Rows[i]["StorePath"]),
                                new XAttribute("StoreFileName", _dtStatementFiles.Rows[i]["FileName"])
                                ));
            }
        }
        else xmlFiles.Add(new XAttribute("Del", "1"));
        xml.Add(xmlFiles);

        if (_RefStatement != 0)
            xml.Add(new XAttribute("RefStatement", _RefStatement));

        return new XElement("ALL_XML_IN", xml).ToString();
    }

    protected bool CheckForm(bool bFullCheck, out string sErrorMessage)
    {
        sErrorMessage = "";
        bool bOk = true;
        StringBuilder sb = new StringBuilder();

        //if (_dtClient.Rows.Count == 0)
        //    sb.AppendLine("Identification : champ requis");
        //if(String.IsNullOrWhiteSpace(txtOpeOriginDRDate.Text))
        //    sb.AppendLine("Date de l’opération à l’origine de la diligence renforcée : champ requis");
        //if (String.IsNullOrWhiteSpace(txtDRStartDate.Text))
        //    sb.AppendLine("Date de la diligence renforcée : champ requis");
        //if (String.IsNullOrWhiteSpace(txtDRStartDate.Text))
        //    sb.AppendLine("Date de la décision de déclaration : champ requis");
        //if(_dtOpeOffUs.Rows.Count == 0)
        //    sb.AppendLine("Montants et nombre d'opérations : champ requis");
        if ((rblAlertKind.SelectedValue == "Auto" && _sAlertType.ToLower().Contains("alerte manuelle"))
            || (rblAlertKind.SelectedValue == "Manual" && txtAlertKind.Text.ToLower().Contains("alerte manuelle")))
            sb.AppendLine("Type d'alerte : champ non valide");

        if(bFullCheck)
        {
            if (multiReason.GetSelectedChoices().Rows.Count == 0)
                sb.AppendLine("Motif : champ requis");

            if (String.IsNullOrWhiteSpace(txtAtypicalFundsCirculationIN.Text))
                sb.AppendLine("Circulation atypique des fonds, Encaissements : champ requis");
            if (String.IsNullOrWhiteSpace(txtAtypicalFundsCirculationOUT.Text))
                sb.AppendLine("Circulation atypique des fonds, Décaissements : champ requis");
            if (String.IsNullOrWhiteSpace(txtAtypicalFundsCirculationOTHER.Text))
                sb.AppendLine("Circulation atypique des fonds, Autres éléments : champ requis");
        }

        int iOpeCount = 0;
        if(!String.IsNullOrWhiteSpace(txtOpeCountSum.Text) && !int.TryParse(txtOpeCountSum.Text, out iOpeCount))
            sb.AppendLine("Nombre d’opérations en jeu : format incorrect");
        Decimal dOpeAmount;
        if (!String.IsNullOrWhiteSpace(txtOpeAmountSum.Text) && !Decimal.TryParse(txtOpeAmountSum.Text.Replace("€", "").Replace(".", ",").Trim(), NumberStyles.Currency, new CultureInfo("fr-FR"), out dOpeAmount))
            sb.AppendLine("Montants en jeu : format incorrect");

        sErrorMessage = sb.ToString().Replace(Environment.NewLine, "<br/>");

        if (!String.IsNullOrWhiteSpace(sErrorMessage))
            bOk = false;

        return bOk;
    }

    protected bool SetAction(string sAction, string sComment)
    {
        string sXmlIn =
            new XElement("ALL_XML_IN",
                new XElement("TRACFIN_STATEMENT",
                    new XAttribute("CashierToken", authentication.GetCurrent(true).sToken),
                    new XAttribute("RefStatement", _RefStatement),
                    new XElement("ACTIONS",
                        new XElement("ACTION",
                            new XAttribute("Action", sAction),
                            sComment)))).ToString();

        return SaveStatement(sXmlIn);
    }
    protected void btnAction_Click(object sender, EventArgs e)
    {
        Button btn = (Button)sender;

        if (btn.CommandArgument != null && !String.IsNullOrWhiteSpace(btn.CommandArgument))
        {
            string sComment = (!String.IsNullOrWhiteSpace(txtComment.Text)) ? txtComment.Text : null;

            bool bContinue = true;
            if (btn.CommandArgument == "COMPLETION")
                if (!String.IsNullOrWhiteSpace(txtRefStatementExt.Text))
                {
                    bContinue = SaveStatement(new XElement("ALL_XML_IN", new XElement("TRACFIN_STATEMENT",
                                                new XAttribute("CashierToken", authentication.GetCurrent(true).sToken),
                                                new XAttribute("RefStatement", _RefStatement),
                                                new XAttribute("RefStatementExt", txtRefStatementExt.Text))).ToString());
                }
                else
                {
                    AlertMessage("Erreur", "Référence TRACFIN : champ requis");
                    bContinue = false;
                }

            if (btn.CommandArgument == "ENDCREATION")
            {
                if (!_CreationStarted)
                    if (SetAction("STARTCREATION", null))
                        _CreationStarted = true;

                string sErrorMessage = "";
                if (!CheckForm(true, out sErrorMessage))
                {
                    bContinue = false;
                    AlertMessage("Erreur", sErrorMessage);
                }
            }

            if (bContinue)
            {

                if (SetAction(btn.CommandArgument, sComment))
                {
                    switch (btn.CommandArgument)
                    {
                        case "EDITVALIDATION":
                            if(!SaveStatement(null))
                                AlertMessage("Erreur", "Une erreur est survenue lors de l'enregistrement.");
                            break;
                        case "ENDCREATION":
                        case "ENDERMESINPUT":
                            Response.Redirect("StatusTrackingAccount.aspx", true);
                            break;
                        case "VALIDATION":
                        case "CANCELVALIDATION":
                            Response.Redirect("StatusTrackingAccount.aspx?view=validate", true);
                            break;
                        case "APPROVAL":
                        case "CANCELAPPROVAL":
                            Response.Redirect("StatusTrackingAccount.aspx?view=approve", true);
                            break;
                    }

                    BindLastActionState(btn.CommandArgument);
                }
                else AlertMessage("Erreur", "Une erreur est survenue lors de l'enregistrement.");
            }
        }
    }

    protected void SetControlsReadOnly()
    {
        hdnEditable.Value = "0";
        txtRefInitStatementInt.ReadOnly = true;
        txtRefInitStatementExt.ReadOnly = true;

        clientSearch1.Visible = false;

        multiReason.HideEdit = true;
        panelAddOtherReason.Visible = false;

        txtOpeOriginDRDate.ReadOnly = true;
        txtDRStartDate.ReadOnly = true;
        txtDREndDate.ReadOnly = true;

        txtAtypicalFundsCirculationIN.ReadOnly = true;
        txtAtypicalFundsCirculationOUT.ReadOnly = true;
        txtAtypicalFundsCirculationOTHER.ReadOnly = true;

        rblAlertKind.Visible = false;
        panelFakeLegend.CssClass = "fake-legend no-button";
        txtAlertKind.ReadOnly = true;

        panelCustomerFiles.Visible = false;
        panelRemoveFileToStatement.Visible = false;
        clientFileManager1.AutoLoad = false;
    }

    protected void GetStatementToXml_Click(object sender, EventArgs e)
    {
        SetAction("STARTERMESINPUT", null);

        Response.Clear();
        Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "declaration.xml"));
        Response.ContentType = "application/octet-stream";
        Response.ContentEncoding = Encoding.UTF8;

        try
        {
            XElement xDS = new XElement("ds",
                new XAttribute(XNamespace.Xmlns + "xsd", "http://www.w3.org/2001/XMLSchema"),
                new XAttribute(XNamespace.Xmlns + "xsi", "http://www.w3.org/2001/XMLSchema-instance"));

            DataTable dtFilesToSend = _dtStatementFiles.Clone();

            string sReasons = "";
            DataTable dt = multiReason.GetSelectedChoices();
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    sReasons += dt.Rows[i]["Text"] + ", ";
                }
                sReasons = sReasons.Substring(0, sReasons.LastIndexOf(", "));
                if (sReasons.Length > 50)
                    sReasons = sReasons.Substring(0, 47) + "...";
            }
            else throw new Exception("Motif de la déclaration manquant");

            StringBuilder sbAnalyse = new StringBuilder();
            sbAnalyse.AppendLine(String.Format("Alerte ayant révélé le cas : {0}", _sAlertType));
            sbAnalyse.AppendLine("La présente déclaration résulte des constats suivants :");
            sbAnalyse.AppendLine("● Circulation atypique des fonds :");
            sbAnalyse.AppendLine(String.Format("- Encaissements : {0}", CleanTextForErmes(txtAtypicalFundsCirculationIN.Text)));
            sbAnalyse.AppendLine(String.Format("- Décaissements : {0}", CleanTextForErmes(txtAtypicalFundsCirculationOUT.Text)));
            sbAnalyse.AppendLine("● Autres éléments atypiques à la base de cette déclaration :");
            sbAnalyse.AppendLine(CleanTextForErmes(txtAtypicalFundsCirculationOTHER.Text));
            //sbAnalyse.AppendLine(litFPEDeclare.Text);

            if(sbAnalyse.ToString().Length > 11500)
                throw new Exception("Analyse trop longue (> 11500 caractères) :" + Environment.NewLine + sbAnalyse.ToString());

            XElement xGeneral = new XElement("general",
                                    new XElement("date", DateTime.Now.ToString("dd/MM/yyyy")),
                                    new XElement("refInterne", GetFriendlyInternalRef(txtRefStatementInt.Text)),
                                    new XElement("ar", "0"),
                                    new XElement("declaration_complementaire", (String.IsNullOrWhiteSpace(txtRefInitStatementExt.Text) ? "1" : "2"))
                                );

            if (!String.IsNullOrWhiteSpace(txtRefInitStatementExt.Text))
                xGeneral.Add(new XElement("complement", txtRefInitStatementExt.Text));

            xGeneral.Add(new XElement("type", "DET" + _iArticle.ToString()),
                         new XElement("pj_presence", HasFile() ? "1" : "0"));

            StringBuilder sbCloture = new StringBuilder();
            sbCloture.Append("Cloture :");

            List<XElement> listXmlClient = new List<XElement>();
            for (int i = 0; i < _dtClient.Rows.Count; i++)
            {
                if ((bool)_dtClient.Rows[i]["Selected"])
                {
                    string sSituation = null, sComplementNumero = null, sTypeVoie = null, sPieceIdentite = null;
                    Address addr = (Address)_dtClient.Rows[i]["Address"];
                    IDDocument doc = (IDDocument)_dtClient.Rows[i]["IDDocument"];
                    dicSituation.TryGetValue(((KYC)_dtClient.Rows[i]["KYC"]).sSitFam, out sSituation);
                    dicComplementNumero.TryGetValue(addr.sComplementVoie, out sComplementNumero);
                    dicTypeVoie.TryGetValue(addr.sTypeVoie, out sTypeVoie);
                    dicPieceIdentite.TryGetValue(doc.sType, out sPieceIdentite);

                    if (addr.iNumeroVoie == 0)
                    {
                        addr.iNumeroVoie = 999999;
                        //throw new Exception("Adresse postale incorrecte");
                    }

                    XElement xmlAddress = new XElement("adresse",
                                new XElement("type", "TAD3"),
                                new XElement("typeVoie", sTypeVoie),
                                new XElement("numero", addr.iNumeroVoie),
                                new XElement("nomVoie", addr.sNomVoie),
                                new XElement("codePostal", addr.sCodePostal),
                                new XElement("ville", addr.sVille),
                                new XElement("pays", addr.sCodePays),
                                new XElement("mobile", addr.sPhoneNumber),
                                new XElement("mail", addr.sEmail)
                            );
                    //if (!String.IsNullOrWhiteSpace(sComplementNumero))
                    //    xmlAddress.Add(new XElement("complementNumero", sComplementNumero));

                    listXmlClient.Add(
                        new XElement("pp",
                            new XElement("etatCivil",
                                new XElement("nomNaissance", _dtClient.Rows[i]["LastName"]),
                                new XElement("nomUsuel", _dtClient.Rows[i]["NewLastName"]),
                                new XElement("prenoms", _dtClient.Rows[i]["FirstName"]),
                                new XElement("dateNaissance", _dtClient.Rows[i]["BirthDate"]),
                                new XElement("lieuNaissance", _dtClient.Rows[i]["BirthPlace"]),
                                new XElement("paysNaissance", _dtClient.Rows[i]["BirthCountry"]),
                                new XElement("sexe", (_dtClient.Rows[i]["Politeness"].ToString() == "M.") ? "SEX1" : "SEX2"),
                                new XElement("nationalite", _dtClient.Rows[i]["Nationality"]),
                                new XElement("situation", sSituation)
                            ),
                            new XElement("secteur", null),
                            new XElement("activite", null),
                            new XElement("ppe", "0"),
                            xmlAddress,
                            new XElement("piece_identite",
                                new XElement("type", sPieceIdentite),
                                new XElement("num", doc.sNum),
                                new XElement("autorite", ""),
                                new XElement("pays", doc.sPays),
                                new XElement("date_debut", (!String.IsNullOrEmpty(doc.sDateDebut) && doc.sDateDebut.Length >= 0) ? doc.sDateDebut.Substring(0, 10) : ""),
                                new XElement("date_fin", (!String.IsNullOrEmpty(doc.sDateFin) && doc.sDateFin.Length >= 0) ? doc.sDateFin.Substring(0, 10) : "")
                            ),
                            new XElement("patrimoine", _dtClient.Rows[i]["Patrimoine"]),
                            new XElement("support",
                                new XElement("type", "TEF3"),
                                new XElement("iban", _dtClient.Rows[i]["IBAN"]),
                                new XElement("droits", "TLI1"))
                        )
                    );

                    if ((bool)_dtClient.Rows[i]["FS"])
                    {
                        dtFilesToSend.Rows.Add(new object[] { "Fiche_Synthese_" + _dtClient.Rows[i]["LastName"] + "_" + _dtClient.Rows[i]["AccountNumber"] + ".pdf", "", "Fiche de synthèse", "", true, "",
                        "Fiche de synthèse " + _dtClient.Rows[i]["LastName"] + " " + _dtClient.Rows[i]["FirstName"], "", "", null, _dtClient.Rows[i]["RefCustomer"] });
                    }
                }
            }

            #region Fichiers
            string sCustomerDirPath = ConfigurationManager.AppSettings["NickelDocPath"].ToString(),
                sClientDirPath, 
                sCustomerWebDirName, 
                sFullFilePath;
            int iRefCustomer = 0;

            for (int i = 0; i < _dtStatementFiles.Rows.Count; i++)
            {
                iRefCustomer = int.Parse(_dtStatementFiles.Rows[i]["RefCustomer"].ToString());
                sCustomerWebDirName = tools.GetCustomerWebDirectoryName(iRefCustomer);
                sClientDirPath = "~" + sCustomerDirPath + "/" + sCustomerWebDirName;
                sFullFilePath = HostingEnvironment.MapPath(sClientDirPath + "/" + _dtStatementFiles.Rows[i]["FileName"].ToString());

                if (!File.Exists(sFullFilePath))
                {
                    ClientFile.GetFileFromCloudStorage(authentication.GetCurrent().sToken, iRefCustomer,
                        sClientDirPath, _dtStatementFiles.Rows[i]["FileName"].ToString()
                        );

                    int iCpt = 0;
                    while (iCpt < 3)
                    {
                        if (File.Exists(sFullFilePath))
                            break;
                        else System.Threading.Thread.Sleep(1000);
                        iCpt++;
                    }

                    if (!File.Exists(sFullFilePath))
                        break;
                    else
                    {
                        dtFilesToSend.Rows.Add(_dtStatementFiles.Rows[i].ItemArray);
                    }
                }
                else
                {
                    dtFilesToSend.Rows.Add(_dtStatementFiles.Rows[i].ItemArray);
                }
            }

            for (int i = 0; i < dtFilesToSend.Rows.Count; i++)
            {
                iRefCustomer = int.Parse(dtFilesToSend.Rows[i]["RefCustomer"].ToString());
                sCustomerWebDirName = tools.GetCustomerWebDirectoryName(iRefCustomer);
                sClientDirPath = "~" + sCustomerDirPath + "/" + sCustomerWebDirName;

                sFullFilePath = HostingEnvironment.MapPath(sClientDirPath + "/" + dtFilesToSend.Rows[i]["FileName"].ToString());

                if (File.Exists(sFullFilePath))
                {
                    string sDate;

                    if (String.IsNullOrWhiteSpace(dtFilesToSend.Rows[i]["LastUploadDate"].ToString()))
                        sDate = DateTime.Now.ToString("dd/MM/yyyy");
                    else sDate = (dtFilesToSend.Rows[i]["LastUploadDate"].ToString().Length > 10) ? dtFilesToSend.Rows[i]["LastUploadDate"].ToString().Substring(0, 10) : "";

                    xGeneral.Add(new XElement("piece",
                                        new XElement("nom", dtFilesToSend.Rows[i]["FileName"]),
                                        new XElement("type", "TDO9"),
                                        new XElement("date", sDate),
                                        new XElement("libelle", (dtFilesToSend.Rows[i]["Description"].ToString().Length > 0) ? dtFilesToSend.Rows[i]["Description"] : "aucun"),
                                        new XElement("type_mime", tools.GetMimeType(dtFilesToSend.Rows[i]["FileName"].ToString().Substring(dtFilesToSend.Rows[i]["FileName"].ToString().LastIndexOf('.')))),
                                        new XElement("contenu", Convert.ToBase64String(File.ReadAllBytes(sFullFilePath)))
                                    )
                        );
                }
            }
            #endregion

            if (!_sAlertType.Contains(" WC ") && (String.IsNullOrWhiteSpace(txtOpeCountSum.Text) || txtOpeCountSum.Text == "0"))
                throw new Exception("Opérations manquantes");

            xDS.Add(new XElement("organisme",
                        new XElement("profession", "TPI44"),
                        new XElement("libelle", "Financière des Paiements Electroniques"),
                        new XElement("numIdPro", "16598R"),
                        new XElement("adresse",
                            new XElement("typeVoie", "TVO3"),
                            new XElement("numero", "18"),
                            new XElement("nomVoie", "Winston Churchill"),
                            new XElement("codePostal", "94220"),
                            new XElement("ville", "Charenton Le Pont"),
                            new XElement("pays", "FR"),
                            new XElement("tel", "0175435007"),
                            new XElement("fax", "0957376690"),
                            new XElement("mail", "christelle.janssens@compte-nickel.fr")
                        )
                    ));

            if(txtBoUserValidator.Text.ToLower().Contains("hassaine"))
                xDS.Add(new XElement("declarant",
                    new XElement("declarant_teleDS", "1"),
                    new XElement("numeroTeleDS", "10156"),
                    new XElement("coordonnees",
                        new XElement("nom", "HASSAINE"),
                        new XElement("prenom", "Sabrina"),
                        new XElement("tel", "0180916077"),
                        new XElement("fax", "0143754388"),
                        new XElement("mail", "sabrina.hassaine@compte-nickel.fr")
                    )
                ));
            else xDS.Add(new XElement("declarant",
                    new XElement("declarant_teleDS", "1"),
                    new XElement("numeroTeleDS", "11974"),
                    new XElement("coordonnees",
                        new XElement("nom", "ROGER"),
                        new XElement("prenom", "Benoît"),
                        new XElement("tel", "0665720212"),
                        new XElement("fax", "0143754388"),
                        new XElement("mail", "benoit.roger@compte-nickel.fr")
                    )
                ));

            XElement xSynthese = new XElement("synthese",
                    new XElement("motif", sReasons),
                    new XElement("debut", txtPeriodeStartDate.Text),
                    new XElement("fin", txtPeriodeEndDate.Text));

            string sAmountSum = txtOpeAmountSum.Text.Replace("€", "").Trim();
            Decimal dAmountSum;
            if (!String.IsNullOrWhiteSpace(sAmountSum) && sAmountSum != "0")
                if (Decimal.TryParse(sAmountSum.Replace('.', ','), NumberStyles.Currency, new CultureInfo("fr-FR"), out dAmountSum))
                {
                    xSynthese.Add(new XElement("montant", Math.Ceiling(dAmountSum).ToString()));
                }

            xSynthese.Add(new XElement("instrument", "TOP13"));

            if (!String.IsNullOrWhiteSpace(txtOpeCountSum.Text) && txtOpeCountSum.Text != "0")
                xSynthese.Add(new XElement("nb_operations", txtOpeCountSum.Text));

            xSynthese.Add(new XElement("statut_operations", "SOP3"),
                    new XElement("pj_presence_date_heure", "0"),
                    //new XElement("date_heure_operations", null),
                    new XElement("analyse", sbAnalyse.ToString())
                );

            xDS.Add(new XElement("contact_presence", "0"),
                new XElement("contact", null),
                xGeneral,
                xSynthese    
            );

            for (int i = 0; i < listXmlClient.Count; i++)
            {
                xDS.Add(listXmlClient[i]);
            }

            XDocument xdoc = new XDocument(new XElement("lot", xDS));
            xdoc.Save(Response.Output);
        }
        catch (Exception ex)
        {
            Response.Headers["content-disposition"] = string.Format("attachment; filename={0}", "error.xml");
            XDocument xdoc = new XDocument(new XElement("error", ex.Message));
            xdoc.Save(Response.Output);
        }

        Response.End();
    }

    protected string GetFriendlyInternalRef(string sIntRef)
    {
        string[] arIntRef = sIntRef.Split('-');
        arIntRef[2] = arIntRef[2].Split('/')[0];
        return String.Join("-", arIntRef);
    }

    protected bool HasFile()
    {
        bool bHasFile = false;
        if (_dtStatementFiles != null && _dtStatementFiles.Rows.Count > 0)
            bHasFile = true;
        else
            for(int i=0;i<_dtClient.Rows.Count;i++)
            {
                if(bool.Parse(_dtClient.Rows[i]["FS"].ToString()) == true)
                {
                    bHasFile = true;
                    break;
                }
            }

        return bHasFile;
    }

    protected void rblAlertKind_SelectedIndexChanged(object sender, EventArgs e)
    {
        panelAlertAuto.Visible = false;
        panelAlertManual.Visible = false;

        switch (rblAlertKind.SelectedValue)
        {
            case "Manual":
                panelAlertManual.Visible = true;
                break;
            default:
                panelAlertAuto.Visible = true;
                break;
        }
    }

    protected string CleanTextForErmes(string sText)
    {
        return sText.Replace("°", "");
    }

    protected void fileUploaded(object sender, EventArgs e)
    {
        BindFileControls();
    }
    protected void fileUploadFailed(object sender, EventArgs e)
    {
        AlertMessage("Erreur", "Une erreur est survenue lors de l'enregistrement du fichier.");
    }

    protected void btnOrderOpe_Click(object sender, EventArgs e)
    {
        string sOrderBy = hdnOrderByOpe.Value;
        string sOrderDirection = hdnOrderDirectionOpe.Value;
        string sSortBy = "";

        switch(sOrderBy)
        {
            case "date":
                sSortBy = string.Format("DateTime {0}", sOrderDirection);
                break;
            case "amount":
                sSortBy = string.Format("Amount {0}", sOrderDirection);
                break;
            case "client":
                sSortBy = string.Format("CustomerName {0}", sOrderDirection);
                break;
        }

        DataView dv = _dtOpeOffUs.DefaultView;
        dv.Sort = sSortBy;

        _dtOpeOffUs = dv.ToTable();

        rptOpeOffUs.DataSource = _dtOpeOffUs;
        rptOpeOffUs.DataBind();
    }

    protected void btnAutoSave_Click(object sender, EventArgs e)
    {
        string sXmlIn = FormToXML();
        bool bSaved;
        string sNewHash = CalculateMD5Hash(sXmlIn);

        if (sNewHash != _sHashForm)
        {
            bSaved = SaveStatement(sXmlIn);
            _sHashForm = sNewHash;

            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "SetAutoSaveStatus(" + bSaved.ToString().ToLower() + ");", true);

            panelAutoSave.CssClass = "saving-status visible";
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "HideAutoSaveStatus();", true);
            panelAutoSave.CssClass = "saving-status";
        }
    }

    protected string CalculateMD5Hash(string input)
    {
        // step 1, calculate MD5 hash from input
        MD5 md5 = System.Security.Cryptography.MD5.Create();
        byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
        byte[] hash = md5.ComputeHash(inputBytes);

        // step 2, convert byte array to hex string
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < hash.Length; i++)
        {
            sb.Append(hash[i].ToString("X2"));
        }
        return sb.ToString();
    }
}