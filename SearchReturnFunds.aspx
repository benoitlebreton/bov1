﻿<%@ Page Title="Recherche retour de fonds" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="SearchReturnFunds.aspx.cs" Inherits="SearchReturnFunds" %>
<%@ Register Src="~/API/AccountClosingReason.ascx" TagName="ClosingReason" TagPrefix="asp" %>
<%@ Register Src="~/API/ReturnFundsList.ascx" TagName="ReturnFundsList" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">

    <link rel="Stylesheet" href="Styles/ReturnFundsList.css" />
    <script type="text/javascript" src="Scripts/jquery.sticky.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

    <h2 style="color: #344b56;text-transform:uppercase">Recherche retour de fonds</h2>

    <div style="margin:20px 0 15px 0;">
        <asp:ReturnFundsList ID="returnFundsList1" runat="server" />
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" Runat="Server">

</asp:Content>

