﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using XMLMethodLibrary;

public partial class StatusTrackingAccount : System.Web.UI.Page
{
    // Session remplie par ws_tools
    private bool _bCounterAll { get { return (Session["AlertCounterAll"] != null) ? bool.Parse(Session["AlertCounterAll"].ToString()) : false; } }
    private string _BoUser { get { return (_bCounterAll && Request.Params["ra"] != null) ? Request.Params["ra"].ToString() : authentication.GetCurrent().sRef; } }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SetAlertProgressList();

            tools.SetBackUrl("StatusTrackingAccount.aspx", "Etat de suivi des alertes", "AlertTreatmentSheet");
            //DataTable dt = new DataTable();
            //dt.Columns.Add("URL");
            //dt.Columns.Add("ButtonText");
            //dt.Rows.Add(new object[] { "StatusTrackingAccount.aspx", "Etat de suivi des comptes/alertes" });
            //Session["BackUrl"] = dt;

            if (Request.Params["na"] != null)
                lblAgent.Text = "de " + Request.Params["na"].ToString();
            else lblAgent.Text = "";

            SetUserList();

            if (Request.Params["view"] != null && Request.Params["view"].Trim().Length > 0)
                ForceShowTab(Request.Params["view"]);
        }
    }

    protected void SetAlertProgressList()
    {
        DataTable dt = GetAlertProgressList();

        rptAlertProgressList.DataSource = dt;
        rptAlertProgressList.DataBind();

        if (dt == null || dt.Rows.Count == 0)
            panelEmpty.Visible = true;
        else panelEmpty.Visible = false;

        upAlertProgress.Update();

        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "initProgressBarList();getNbAlertAML();", true);
    }

    protected DataTable GetAlertProgressList()
    {
        SqlConnection conn = null;
        DataTable dtAlert = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("AML.P_GetAlertEventsV2", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);

            XElement xml = new XElement("Alert",
                                new XAttribute("CashierToken", authentication.GetCurrent().sToken),
                                new XAttribute("Treated", "0"));
            //if (!ckbViewAll.Checked)
                xml.Add(new XAttribute("RefBOUser", _BoUser));

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN", xml).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if (sXmlOut.Length > 0)
            {
                List<string> listAlert = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/Alert");

                if (listAlert.Count > 0)
                {
                    dtAlert = new DataTable();
                    dtAlert.Columns.Add("RefAlert");
                    dtAlert.Columns.Add("AlertLabel");
                    dtAlert.Columns.Add("RefCustomer");
                    dtAlert.Columns.Add("CustomerName");
                    dtAlert.Columns.Add("Steps", typeof(DataTable));
                    dtAlert.Columns.Add("StepMarkers");
                    dtAlert.Columns.Add("CurrentStep");
                    dtAlert.Columns.Add("StatusClass");
                    dtAlert.Columns.Add("Notification");
                    dtAlert.Columns.Add("NotificationIcon");
                    dtAlert.Columns.Add("NotificationLabel");
                    dtAlert.Columns.Add("BlockedLabel");
                    dtAlert.Columns.Add("BlockedDate");

                    for (int x = 0; x < listAlert.Count; x++)
                    {
                        List<string> listRefAlert = CommonMethod.GetAttributeValues(listAlert[x], "Alert", "refAlert");
                        List<string> listAlertLabel = CommonMethod.GetAttributeValues(listAlert[x], "Alert", "AlertLabel");
                        List<string> listRefCustomer = CommonMethod.GetAttributeValues(listAlert[x], "Alert", "RefCustomer");
                        List<string> listCustomerPoliteness = CommonMethod.GetAttributeValues(listAlert[x], "Alert", "Politeness");
                        List<string> listCustomerLastName = CommonMethod.GetAttributeValues(listAlert[x], "Alert", "CustomerLastName");
                        List<string> listCustomerFirstName = CommonMethod.GetAttributeValues(listAlert[x], "Alert", "CustomerFirstName");
                        List<string> listNotification = CommonMethod.GetAttributeValues(listAlert[x], "Alert", "AlertNotification");
                        List<string> listNotificationKind = CommonMethod.GetAttributeValues(listAlert[x], "Alert", "TagAlertNotificationKind");
                        List<string> listNotificationLabel = CommonMethod.GetAttributeValues(listAlert[x], "Alert", "AlertNotificationLabel");
                        List<string> listBlocked = CommonMethod.GetAttributeValues(listAlert[x], "Alert", "IsLocked");
                        List<string> listBlockedDate = CommonMethod.GetAttributeValues(listAlert[x], "Alert", "LockedDate");

                        List<string> listEvents = CommonMethod.GetTags(listAlert[x], "Alert/Events/AlertEvent");

                        string sCustomerFullName = listCustomerPoliteness[0] + " " + listCustomerFirstName[0] + " " + listCustomerLastName[0];

                        StringBuilder sbStepMarkers = new StringBuilder();
                        DataTable dtEvents = new DataTable();
                        dtEvents.Columns.Add("Date");
                        dtEvents.Columns.Add("Days");
                        dtEvents.Columns.Add("Label");
                        dtEvents.Columns.Add("Icon");
                        dtEvents.Columns.Add("BOUser");

                        // Recuperation date ORIGINE ZERO
                        string sOriginDate = "";
                        for (int y = 0; y < listEvents.Count; y++)
                        {
                            List<string> listDate = CommonMethod.GetAttributeValues(listEvents[y], "AlertEvent", "Date");
                            List<string> listTag = CommonMethod.GetAttributeValues(listEvents[y], "AlertEvent", "Tag");

                            string sDate = (listDate.Count > 0) ? listDate[0] : "";

                            //if(listTag[0] == "FIRSTCHECK")
                            if(listTag[0] == "COUNTERPART"
                                || listTag[0] == "FUNDSOURCE"
                                || listTag[0] == "LOCKACCOUNTINFO"
                                || listTag[0] == "SELFEMPLOYEDSPLIT"
                                )
                            {
                                sOriginDate = sDate;
                                break;
                            }
                        }

                        // Parsing EVENTS
                        for (int y = 0; y < listEvents.Count; y++)
                        {
                            List<string> listDate = CommonMethod.GetAttributeValues(listEvents[y], "AlertEvent", "Date");
                            List<string> listLabel = CommonMethod.GetAttributeValues(listEvents[y], "AlertEvent", "Label");
                            List<string> listBOUser = CommonMethod.GetAttributeValues(listEvents[y], "AlertEvent", "BOUser");
                            List<string> listTag = CommonMethod.GetAttributeValues(listEvents[y], "AlertEvent", "Tag");

                            string sDate = (listDate.Count > 0) ? listDate[0] : "";
                            string sNbDays = "";
                            if (!String.IsNullOrWhiteSpace(sDate))
                                sNbDays = tools.GetNbDaysFromDate(sDate, sOriginDate).ToString();

                            string sIcon = "";
                            switch(listTag[0])
                            {
                                case "GENERATION":
                                    sIcon = "alarm";
                                    break;
                                case "FIRSTCHECK":
                                    sIcon = "visibility";
                                    break;
                                case "COUNTERPART":
                                case "FUNDSOURCE":
                                case "LOCKACCOUNTINFO":
                                case "SELFEMPLOYEDSPLIT":
                                    sIcon = "mail_outline";
                                    break;
                                case "NoContact":
                                    sIcon = "mic_off";
                                    break;
                                case "ContactCounterpartAgain":
                                case "RELANCEJ+7":
                                case "RELANCEJ+14":
                                case "RELANCEJ+30":
                                    sIcon = "email";
                                    break;
                                case "UnfreezeAccount":
                                    sIcon = "lock_open";
                                    break;
                                case "CONTACTCLIENTPHONE":
                                case "CallCounterpart":
                                    sIcon = "phone";
                                    break;
                                case "AlertResponseOK":
                                    sIcon = "sentiment_satisfied";
                                    break;
                                case "AlertResponseKO":
                                    sIcon = "sentiment_dissatisfied";
                                    break;
                                case "AlertResponseNotSatisfactory":
                                    sIcon = "sentiment_neutral";
                                    break;
                                case "CounterpartAnswer":
                                case "CallCounterpartAnswerOk":
                                    sIcon = "thumb_up";
                                    break;
                                case "CounterpartNoAnwser":
                                    sIcon = "thumb_down";
                                    break;
                                case "CallCounterpartAnswerPending":
                                    sIcon = "more_horiz";
                                    break;
                                case "REASSIGN":
                                    sIcon = "supervisor_account";
                                    break;
                                default:
                                    sIcon = "fiber_manual_record";
                                    break;
                            }

                            dtEvents.Rows.Add(new object[] { sDate, sNbDays, listLabel[0], String.Format("<i class=\"material-icons\" tooltip=\"{1}\">{0}</i>", sIcon, listLabel[0]), (listBOUser.Count > 0) ? listBOUser[0] : "" });
                        }

                        // Creation markers
                        DataTable dtEventMarkers = new DataTable();
                        dtEventMarkers.Columns.Add("Days");
                        dtEventMarkers.Columns.Add("OverLabel");
                        dtEventMarkers.Columns.Add("BottomLabel");
                        for (int i = 0; i < dtEvents.Rows.Count; i++)
                        {
                            if(dtEventMarkers.Rows.Count > 0)
                            {
                                // Même jour marker précédent
                                if(dtEvents.Rows[i]["Days"].ToString() == dtEventMarkers.Rows[dtEventMarkers.Rows.Count-1]["Days"].ToString())
                                {
                                    dtEventMarkers.Rows[dtEventMarkers.Rows.Count - 1]["OverLabel"] += "<br/>" + dtEvents.Rows[i]["Label"];
                                    dtEventMarkers.Rows[dtEventMarkers.Rows.Count - 1]["BottomLabel"] += dtEvents.Rows[i]["Icon"].ToString();
                                }
                                else
                                {
                                    dtEventMarkers.Rows.Add(new object[] { dtEvents.Rows[i]["Days"], dtEvents.Rows[i]["Label"], dtEvents.Rows[i]["Icon"] });
                                }
                            }
                            else
                            {
                                dtEventMarkers.Rows.Add(new object[] { dtEvents.Rows[i]["Days"], dtEvents.Rows[i]["Label"], dtEvents.Rows[i]["Icon"] });
                            }
                        }
                        for(int i=0;i<dtEventMarkers.Rows.Count;i++)
                        {
                            sbStepMarkers.Append(dtEventMarkers.Rows[i]["Days"] + ";" + dtEventMarkers.Rows[i]["OverLabel"] + ";" + dtEventMarkers.Rows[i]["BottomLabel"] + "|");
                        }
                        string sStepMarkers = sbStepMarkers.ToString().TrimEnd('|');
                        int iCurrentStep = tools.GetNbDaysFromDate(DateTime.Now.ToString("dd/MM/yyyy"), sOriginDate);

                        string blockedLabel = string.Empty;
                        DateTime blockedDate = DateTime.MinValue;
                        string slockedDate = string.Empty;
                        if (listBlockedDate.Count > 0)
                            DateTime.TryParse(listBlockedDate[0], out blockedDate);
                        if (blockedDate != DateTime.MinValue)
                        {
                            int nbreJour = (int)(DateTime.Today - blockedDate).TotalDays;
                            blockedLabel = string.Format(" Bloqué depuis {0} jour{1}", nbreJour, (nbreJour <= 1) ? "":"s");
                            slockedDate = blockedDate.ToString("dd/MM/yyyy");
                        }

                        dtAlert.Rows.Add(new object[] {
                            listRefAlert[0],
                            listAlertLabel[0],
                            listRefCustomer[0],
                            sCustomerFullName,
                            dtEvents,
                            sStepMarkers,
                            iCurrentStep.ToString(),
                            (iCurrentStep >= 15) ? "step-progressbar-bar-red" : "",
                            (listNotification.Count > 0) ? listNotification[0] : "0",
                            String.Format("<i class=\"material-icons\" tooltip=\"{1}\">{0}</i>", "priority_high", listNotificationLabel[0]),
                            listNotificationLabel[0],
                            blockedLabel,
                            slockedDate
                        });
                    }
                }
            }                                
        }
        catch(Exception e)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dtAlert;
    }

    protected void rptAlertProgressList_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        HyperLink hlShowAlertProgressDetails = (HyperLink)e.Item.FindControl("hlShowAlertProgressDetails");
        Panel AlertProgressDetails = (Panel)e.Item.FindControl("AlertProgressDetails");

        if (hlShowAlertProgressDetails != null && AlertProgressDetails != null)
            hlShowAlertProgressDetails.Attributes.Add("onclick", "showAlertDetails(this,'"+ AlertProgressDetails.ClientID +"')");
    }

    protected void hlGoToFastAnalysisSheet_Click(object sender, EventArgs e)
    {
        tools.SetBackUrl(Request.Url.ToString(), "Etat de suivi des alertes", "FastAnalysisSheet");
        Button btn = (Button)sender;
        Response.Redirect(btn.CommandArgument, true);
    }

    protected void hlGoToAlert_Click(object sender, EventArgs e)
    {
        tools.SetBackUrl(Request.Url.ToString(), "Etat de suivi des alertes", "AlertTreatmentSheet");
        Button btn = (Button)sender;
        Response.Redirect(btn.CommandArgument, true);
    }

    protected void AlertMessage(string sTitle, string sMessage)
    {
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('" + sTitle + "', \"" + sMessage + "\");", true);
    }

    protected void btnChangeAlertAttribution_Click(object sender, EventArgs e)
    {
        XElement xml = new XElement("ALL_XML_IN",
                                new XElement("ALERT_TREATMENT",
                                    new XAttribute("CashierToken", authentication.GetCurrent().sToken),
                                    new XAttribute("RefAlert", hdnRefAlert.Value),
                                    new XAttribute("RefBOUserReassign", ddlBOUsers.SelectedValue),
                                    new XAttribute("ReserveAlert", "1")
                                    ));
        if (AML.SetAlertTreatment(xml.ToString()))
        {
            AlertMessage("Réattribution réussie", "<span style='color:green'>L'alerte a été réattribuée.</span>");
            SetAlertProgressList();
        }
        else AlertMessage("Erreur", "Une erreur est survenue lors de la réattribution.");
    }

    protected void SetUserList()
    {
        ddlBOUsers.DataSource = AML.GetAgentConformityList();
        ddlBOUsers.DataBind();
    }

    protected void RemoveNotification(string sRefAlert)
    {
        XElement xml = new XElement("ALL_XML_IN",
                                new XElement("ALERT_TREATMENT",
                                    new XAttribute("CashierToken", authentication.GetCurrent().sToken),
                                    new XAttribute("RefAlert", sRefAlert),
                                    new XAttribute("AlertNotification", "0")
                                    ));
        if (AML.SetAlertTreatment(xml.ToString()))
        {
            //AlertMessage("Réattribution réussie", "<span style='color:green'>L'alerte a été réattribuée.</span>");
            SetAlertProgressList();
        }
        //else AlertMessage("Erreur", "Une erreur est survenue lors de la réattribution.");
    }

    protected void btnRemoveNotification_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton btn = (ImageButton)sender;

        if (btn != null)
        {
            RemoveNotification(btn.CommandArgument);
        }
    }

    protected void BindTracfinStatement(string sRefTable)
    {
        string sXmlOut = Tracfin.SearchStatement(0, int.Parse(_BoUser), authentication.GetCurrent().sToken);
        List<string> listStatement = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/TRACFIN_STATEMENT");

        Dictionary<string, string> dicStatementStatus = new Dictionary<string, string>()
        {
            { "CREATIONPENDING","En attente de déclaration" }
            ,{ "STARTCREATION","En cours de déclaration" }
            ,{ "ENDCREATION","&Agrave; corriger" }
            ,{ "VALIDATION","&Agrave; approuver" }
            ,{ "EDITVALIDATION","&Agrave; valider<br/>Corrigée par @@BOUSER" }
            ,{ "CANCELVALIDATION","Annulée par le valideur" }
            ,{ "APPROVAL","Approuvée" }
            ,{ "CANCELAPPROVAL","Annulée par l'approbateur" }
            ,{ "ENDERMESINPUT","Saisie ERMES terminée" }
            ,{ "STARTERMESINPUT","En cours de saisie par @@BOUSER" }
        };

        DataTable dt1 = new DataTable();
        dt1.Columns.Add("RefStatement");
        dt1.Columns.Add("RefAlert");
        dt1.Columns.Add("AlertTAG");
        dt1.Columns.Add("RefCustomer");
        dt1.Columns.Add("LastName");
        dt1.Columns.Add("FirstName");
        dt1.Columns.Add("AccountNumber");
        dt1.Columns.Add("LastAction");
        dt1.Columns.Add("RefStatementExt");
        dt1.Columns.Add("RefStatementInt");
        dt1.Columns.Add("NbDaysSinceDR");
        dt1.Columns.Add("Porteur");
        DataTable dt2 = dt1.Clone();
        DataTable dt3 = dt1.Clone();
        DataTable dt4 = dt1.Clone();
        DataTable dtToAddRow = dt1.Clone();

        for (int i = 0; i < listStatement.Count; i++)
        {
            try
            {
                List<string> listRefAlert = CommonMethod.GetAttributeValues(listStatement[i], "TRACFIN_STATEMENT", "RefAlert");
                List<string> listAlertTAG = CommonMethod.GetAttributeValues(listStatement[i], "TRACFIN_STATEMENT", "AlertTAG");
                List<string> listRefStatement = CommonMethod.GetAttributeValues(listStatement[i], "TRACFIN_STATEMENT", "RefStatement");
                List<string> listRefStatementInt = CommonMethod.GetAttributeValues(listStatement[i], "TRACFIN_STATEMENT", "RefStatementInt");
                List<string> listRefStatementExt = CommonMethod.GetAttributeValues(listStatement[i], "TRACFIN_STATEMENT", "RefStatementExt");
                List<string> listRefCustomer = CommonMethod.GetAttributeValues(listStatement[i], "TRACFIN_STATEMENT", "RefCustomer");
                List<string> listLastName = CommonMethod.GetAttributeValues(listStatement[i], "TRACFIN_STATEMENT", "LastName");
                List<string> listFirstName = CommonMethod.GetAttributeValues(listStatement[i], "TRACFIN_STATEMENT", "FirstName");
                List<string> listAccountNumber = CommonMethod.GetAttributeValues(listStatement[i], "TRACFIN_STATEMENT", "AccountNumber");
                List<string> listActions = CommonMethod.GetTags(listStatement[i], "TRACFIN_STATEMENT/ACTIONS/ACTION");
                List<string> listDRStartDate = CommonMethod.GetAttributeValues(listStatement[i], "TRACFIN_STATEMENT", "DRStartDate");

                int? iNbDaysSinceDR = null;
                if(listDRStartDate.Count > 0 && !String.IsNullOrWhiteSpace(listDRStartDate[0]))
                {
                    DateTime date;
                    if(DateTime.TryParseExact(listDRStartDate[0], "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                        iNbDaysSinceDR = (DateTime.Now - date).Days;
                }

                string sPorteur = "";
                if (listActions.Count > 0)
                {
                    int j = listActions.Count - 1;
                    List<string> listAction = CommonMethod.GetAttributeValues(listActions[j], "ACTION", "Action");
                    List<string> listActionDate = CommonMethod.GetAttributeValues(listActions[j], "ACTION", "ActionDate");
                    List<string> listComment = CommonMethod.GetTagValues(listActions[j], "ACTION/COMMENT");
                    List<string> listRefBoUser = CommonMethod.GetAttributeValues(listActions[j], "ACTION", "RefBoUser");
                    List<string> listBoUserLastName = CommonMethod.GetAttributeValues(listActions[j], "ACTION", "BoUserLastName");
                    List<string> listBoUserFirstName = CommonMethod.GetAttributeValues(listActions[j], "ACTION", "BoUserFirstName");                    

                    switch (listAction[0])
                    {
                        // DOSSIER A COMPLETER
                        case "CREATIONPENDING":
                        case "STARTCREATION":
                        case "CANCELVALIDATION":
                            dtToAddRow = dt1;
                            break;
                        // DOSSIER A VALIDER
                        case "ENDCREATION":
                        case "EDITVALIDATION":
                        case "CANCELAPPROVAL":
                            dtToAddRow = dt2;
                            break;
                        // DOSSIER A APPROUVER
                        case "VALIDATION":
                            dtToAddRow = dt2;
                            break;
                        // DOSSIER A SAISIR ERMES
                        case "APPROVAL":
                        // DOSSIER A COMPLETER OU TERMINE
                        case "ENDERMESINPUT":
                        case "STARTERMESINPUT":
                            dtToAddRow = dt3;
                            break;
                        case "COMPLETION":
                            dtToAddRow = null;
                            break;
                    }

                    // GET PORTEUR
                    for (int k = 0; k < listActions.Count; k++)
                    {
                        List<string> listAction2 = CommonMethod.GetAttributeValues(listActions[k], "ACTION", "Action");
                        List<string> listBoUserLastName2 = CommonMethod.GetAttributeValues(listActions[k], "ACTION", "BoUserLastName");
                        List<string> listBoUserFirstName2 = CommonMethod.GetAttributeValues(listActions[k], "ACTION", "BoUserFirstName");

                        if (listAction2[0] == "STARTCREATION" || listAction2[0] == "CREATIONPENDING")
                        {
                            sPorteur = listBoUserFirstName2[0] + " " + listBoUserLastName2[0];
                            break;
                        }
                    }

                    if (dtToAddRow != null)
                    {
                        string sActionLabel = "";
                        dicStatementStatus.TryGetValue(listAction[0], out sActionLabel);

                        sActionLabel = sActionLabel.Replace("@@BOUSER", string.Format("{0} {1}", listBoUserFirstName[0], listBoUserLastName[0]));

                        dtToAddRow.Rows.Add(new object[] { listRefStatement[0], listRefAlert[0], listAlertTAG[0], listRefCustomer[0],
                            listLastName[0], listFirstName[0], listAccountNumber[0], sActionLabel,
                            (listRefStatementExt.Count > 0) ? listRefStatementExt[0] : "", (listRefStatementInt.Count > 0) ? listRefStatementInt[0] : "", (iNbDaysSinceDR != null) ? iNbDaysSinceDR.ToString() : "N.C.", sPorteur });
                    }
                }
            }
            catch(Exception ex)
            { }

        }

        panelNoStatement.Visible = false;
        rptTracfinStatement.Visible = false;
        switch(sRefTable)
        {
            case "1":
                if (dt1.Rows.Count > 0)
                    rptTracfinStatement.DataSource = dt1;
                break;
            case "2":
                if (dt2.Rows.Count > 0)
                    rptTracfinStatement.DataSource = dt2;
                break;
            case "3":
                if (dt3.Rows.Count > 0)
                    rptTracfinStatement.DataSource = dt3;
                break;
        }

        DataTable dtTmp = (DataTable)rptTracfinStatement.DataSource;
        if (dtTmp != null && dtTmp.Rows.Count > 0)
            rptTracfinStatement.Visible = true;
        else panelNoStatement.Visible = true;

        rptTracfinStatement.DataBind();
    }

    protected void btnRefreshTracfinStatement_Click(object sender, EventArgs e)
    {
        List<string> listTab = new List<string>() { "1", "2", "3" };
        if (listTab.Contains(hdnActiveTab.Value))
            BindTracfinStatement(hdnActiveTab.Value);
    }

    protected DataTable GetTracfinStatementCloseAccount()
    {
        DataTable dt = new DataTable();
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("TracFin.P_GetStatementAccountToClose", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            sda.Fill(dt);
        }
        catch(Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }
    protected void BindTracfinStatementCloseAccount()
    {
        DataTable dt = GetTracfinStatementCloseAccount();

        panelNoAccountClose.Visible = false;
        rptAccountClose.Visible = false;
        if (dt.Rows.Count > 0)
        {
            rptAccountClose.DataSource = dt;
            rptAccountClose.DataBind();
            rptAccountClose.Visible = true;

            amlComment1.BindCommentLengthCheck();
        }
        else panelNoAccountClose.Visible = true;
    }

    protected void btnRefreshAccountClose_Click(object sender, EventArgs e)
    {
        if (hdnActiveTab.Value == "4")
            BindTracfinStatementCloseAccount();
    }

    protected string GetCloseActionLabel(string sRefCloseAction)
    {
        switch(sRefCloseAction)
        {
            case "1":
                return "Immédiate";
            case "2":
                return "Avec préavis";
            default:
                return "";
        }
    }

    protected void btnGoToCloseAccount_Click(object sender, EventArgs e)
    {
        bool bOk = true;
        int iRefCustomer;

        if (int.TryParse(hdnRefCustomerToClose.Value, out iRefCustomer))
        {
            if (!String.IsNullOrWhiteSpace(rblDRKO.SelectedValue))
                bOk = AML.addClientSpecificity(authentication.GetCurrent().sToken, iRefCustomer, rblDRKO.SelectedValue);
            if (bOk)
                bOk = AML.addClientNote(authentication.GetCurrent().sToken, iRefCustomer, amlComment1.Commentary);
        }
        else bOk = false;

        if (bOk)
            Response.Redirect("AccountClosing.aspx?ref=" + iRefCustomer.ToString() + "&type=" + hdnTypeToClose.Value);
        else AlertMessage("Erreur", "Une erreur est survenue.");
    }

    protected void ForceShowTab(string sTabName)
    {
        if(!String.IsNullOrWhiteSpace(sTabName))
        {
            switch (sTabName)
            {
                case "input":
                    BindTracfinStatement("3");
                    hdnActiveTab.Value = "3";
                    break;
                case "validate":
                case "approve":
                    BindTracfinStatement("2");
                    hdnActiveTab.Value = "2";
                    break;
                case "close":
                    hdnForceShowTab.Value = "account-close-tab";
                    BindTracfinStatementCloseAccount();
                    break;
            }
        }
    }

    protected string GetDSLineColor(string sNbDays, string sAlertTAG)
    {
        bool bIsOverdue = IsAlertOverdue(sNbDays);
        bool bIsImportant = IsAlertKindImportant(sAlertTAG);
        int i;
        int.TryParse(sNbDays, out i);

        if (bIsImportant)
            if (i >= 1)
                return "background: repeating-linear-gradient(45deg,rgba(255, 0, 0, 0.3),rgba(255, 0, 0, 0.3) 10px,rgba(255, 255, 0, 0.3) 10px,rgba(255, 255, 0, 0.3) 20px)";
            else return "background-color: rgba(255, 255, 0, 0.5)";
        else if (bIsOverdue)
        {
            return "background-color: rgba(255, 0, 0, 0.5)";
        }

        return "";
    }

    protected bool IsAlertOverdue(string sNbDays)
    {
        bool bOverdue = false;
        int i;
        if (int.TryParse(sNbDays, out i))
            if (i >= 25)
                bOverdue = true;
        return bOverdue;
    }

    protected bool IsAlertKindImportant(string sAlertTAG)
    {
        bool bImportant = false;

        if (sAlertTAG.StartsWith("WC_"))
            bImportant = true;

        return bImportant;
    }
}