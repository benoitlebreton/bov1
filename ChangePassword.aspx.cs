﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using XMLMethodLibrary;

public partial class ChangePassword : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        authentication auth = authentication.GetCurrent();
        lblInfoMessage.Text = "";

        if (IsPostBack)
        {
            string[] eventArgs = Page.Request["__EVENTARGUMENT"].ToString().Split(';');
            if (eventArgs[0].ToString().ToLower() == "changepassword")
            {
                string sMessage = "";
                System.Drawing.Color color;
                tools.ChangeUserPassword(auth.sRef, OldPswd.Text, pswdConfirm.Text, out sMessage, out color);
                UpdateInfoMessage(sMessage, color);

                string sRedirect = "false";
                if (color.Name != null && color.Name.Length > 0 && color.Name == "Green")
                {
                    sRedirect = "true";
                    auth.bChangePassword = false;
                }
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "showMessage('','','" + sRedirect + "');", true);
            }
        }
        else
        {
            if (auth.bChangePassword)
            {
                lblInfoMessage.Text = "Pour des raisons de sécurité, <br/>veuillez changer votre mot de passe.";
                panelOldPassword.Visible = false;
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "showMessage('Changez votre mot de passe','','false');", true);
            }
        }

        
    }

    protected void UpdateInfoMessage(string message, System.Drawing.Color color)
    {
        lblInfoMessage.Text = message;
        lblInfoMessage.ForeColor = color;
    }

    protected void Button_PreRender(object sender, EventArgs e)
    {
        Button btn = (Button)sender;

        if (btn != null)
        {
            string btnText = "";

            switch (btn.ID)
            {
                case "btnChangePassword":
                    btnText = "Changer le mot de passe";
                    break;
            }

            btn.Text = btnText;
        }
    }

    public DataTable GetAgencyList()
    {
        SqlConnection conn = null;
        DataTable dt = new DataTable();

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MoneyGram"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("P_MG_WEB_GetTagMgAgentList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;

            da.Fill(dt);
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }
}