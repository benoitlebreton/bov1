﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using XMLMethodLibrary;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;
using System.Globalization;

public partial class ClientSearch : System.Web.UI.Page
{
    private bool _bExportsActionAllowed { get { return (ViewState["ExportsActionAllowed"] != null) ? bool.Parse(ViewState["ExportsActionAllowed"].ToString()) : false; } set { ViewState["ExportsActionAllowed"] = value; } }

    [Serializable]
    protected class ClientSearchFilters
    {
        public string sLastName { get; set; }
        public string sFirstName { get; set; }
        public string sAddressStatus { get; set; }
        public string sRegistrationNumber { get; set; }
        public string sCardBarcode { get; set; }
        public string sGlobalSearch { get; set; }
        
        public ClientSearchFilters()
        {
        }
        public ClientSearchFilters(string sLastName, string sFirstName, string sAddressStatus, string sRegistrationNumber, string sCardBarcode, string sGlobalSearch)
        {
            this.sLastName = sLastName;
            this.sFirstName = sFirstName;
            this.sAddressStatus = sAddressStatus;
            this.sRegistrationNumber = sRegistrationNumber;
            this.sCardBarcode = sCardBarcode;
            this.sGlobalSearch = sGlobalSearch;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            CheckRights();
            CheckForSearchFilters();
            txtClientLastName.Focus();

            try
            {
                InitExportExcel();
            }
            catch(Exception ex)
            {
                divFilters2.Visible = false;
            }

            // CHECK FOR AUTO OPEN ZENDESK
            if (Request.Params["zendesk"] != null)
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "$(window).load(function(){OpenZendeskTab();});", true);
            if (Request.Params["svi-auth"] != null && !String.IsNullOrWhiteSpace(Request.Params["svi-auth"]))
            {
                panelSVIAuthenticated.Visible = true;
                if (Request.Params["svi-auth"] == "1")
                {
                    litSVIauth.Text = "Client authentifié sur SVI";
                    panelSVIAuthenticated.CssClass = "svi-headband svi-auth";
                }
                else
                {
                    litSVIauth.Text = "Client non authentifié sur SVI";
                    panelSVIAuthenticated.CssClass = "svi-headband svi-not-auth";
                }

                //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "CheckSVIheadbandPosition();", true);
            }
        }
    }

    protected void CheckRights()
    {
        string sXmlOut = tools.GetRights(authentication.GetCurrent().sToken, "SC_CustomerSearch");

        List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Page", "RC");

        if (listRC.Count == 0 || listRC[0] != "0")
        {
            Response.Redirect("Default.aspx", true);
        }
        else
        {
            List<string> listAction = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/Action");
            for (int i = 0; i < listAction.Count; i++)
            {
                List<string> listViewAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ViewAllowed");
                List<string> listActionAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ActionAllowed");

                if (listViewAllowed.Count == 0 || listViewAllowed[0] != "1")
                {
                    divFilters2.Visible = false;
                }
                if (listActionAllowed.Count > 0 && listActionAllowed[0] == "1")
                {
                    _bExportsActionAllowed = true;
                }
            }
        }
    }

    protected void CheckForSearchFilters()
    {

        try
        {
            if (Session["ClientSearchFilters"] != null)
            {
                ClientSearchFilters searchFilters = null;
                try { searchFilters = (ClientSearchFilters)Session["ClientSearchFilters"]; } catch (Exception e) { }
                if (searchFilters != null)
                {
                    txtClientLastName.Text = (searchFilters.sLastName != null) ? searchFilters.sLastName : "";
                    txtClientFirstName.Text = (searchFilters.sFirstName != null) ? searchFilters.sFirstName : "";
                    rblAddressStatus.SelectedValue = (searchFilters.sAddressStatus != null) ? searchFilters.sAddressStatus : "";
                    txtBarcode.Text = (searchFilters.sCardBarcode != null) ? searchFilters.sCardBarcode : "";
                    txtRegistrationNumber.Text = (searchFilters.sRegistrationNumber != null) ? searchFilters.sRegistrationNumber : "";
                    txtGlobalSearch.Text = (searchFilters.sGlobalSearch != null) ? searchFilters.sGlobalSearch : "";

                    //btnSearch_Click(new object(), new EventArgs());
                }
            }
        }
        catch(Exception ex)
        {
            Session.Remove("ClientSearchFilters");
        }
    }

    protected void BindClientList(DataTable dt)
    {
        gvClientSearchResult.DataSource = dt;
        gvClientSearchResult.DataBind();
        lblRowCount.Text = dt.Rows.Count.ToString();

        panelGrid.Visible = true;
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        bool isOK = false;
        int iGlobalSearch;
        int minGlobalSearchAlpha = 3, minGlobalSearchNum = 6;
        //CHECK GLOBAL SEARCH LENGTH VALUE
        if (txtGlobalSearch.Text.Trim().Length == 0 ||
            (int.TryParse(txtGlobalSearch.Text.Trim(), out iGlobalSearch) && txtGlobalSearch.Text.Trim().Length >= 6) ||
            (!int.TryParse(txtGlobalSearch.Text.Trim(), out iGlobalSearch) && txtGlobalSearch.Text.Trim().Length >= 3))
        {
            string sToken = authentication.GetCurrent(false).sToken;
            Session["ClientSearchFilters"] = new ClientSearchFilters(txtClientLastName.Text, txtClientFirstName.Text, rblAddressStatus.SelectedValue, txtRegistrationNumber.Text, txtBarcode.Text, txtGlobalSearch.Text);
            BindClientList(Client.GetClientList(txtBarcode.Text, txtClientLastName.Text, txtClientFirstName.Text, rblAddressStatus.SelectedValue, txtRegistrationNumber.Text, txtGlobalSearch.Text, sToken));
        }
        else
        {
            string sErrorText = "La recherche globale doit contenir au minimum " + minGlobalSearchNum + " caractères numériques ou " + minGlobalSearchAlpha + " caractères alphanumériques.";
            string jsScript = "$('#sFiltersError').html(\""+sErrorText+"\");$('#divFiltersErrorPopup').dialog('open');";
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "SearchValueError", jsScript, true);
        }
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HiddenField hdnRefCustomer = (HiddenField)e.Row.FindControl("hdnRefCustomer");
            Label lblAccountNumber = (Label)e.Row.FindControl("lblAccountNumber");
            //Label bDeleteField = (Label)e.Row.FindControl("bDelete");

            if (hdnRefCustomer != null)
            {
                e.Row.Attributes.Add("onclick", "ShowClientDetails('" + hdnRefCustomer.Value + "')");
                HiddenField hdn = (HiddenField)e.Row.FindControl("hdnServiceTag");
                if (hdn != null && hdn.Value == "4")
                    e.Row.Attributes.Add("onmouseover", "this.style.cursor = 'pointer';this.style.backgroundColor = '#1dc39c'");
                else e.Row.Attributes.Add("onmouseover", "this.style.cursor = 'pointer';this.style.backgroundColor = '#ff9966'");
                e.Row.Attributes.Add("onmouseout", "this.style.cursor = 'auto';this.style.backgroundColor = '#fff'");
            }
        }
    }

    protected void btnSearchSurveillance_Click(object sender, EventArgs e)
    {
        BindClientList(Client.GetClientList(true, authentication.GetCurrent().sToken));
    }


    // EXPORTS EXCELS
    protected void InitExportExcel()
    {
        DataTable dtExports = new DataTable();
        dtExports.Columns.Add("Label");
        dtExports.Columns.Add("Filename");
        dtExports.Columns.Add("Link");
        dtExports.Columns.Add("Date");
        dtExports.Columns.Add("Tag");
        dtExports.Rows.Add(new object[] { "Etat des prélèvements de tous les clients", "", "", "-", (authentication.GetCurrent().sTagProfile.ToUpper() == "ADMIN") ? "RefusedTransferOutAll" : "" });
        dtExports.Rows.Add(new object[] { "Etat des prélèvements des clients avertis pour 1 rejet sans dépôt", "", "", "-", (_bExportsActionAllowed) ? "RefusedTransferOut1" : "" });
        dtExports.Rows.Add(new object[] { "Etat des prélèvements des clients avertis pour 5 rejets sans dépôt", "", "", "-", (_bExportsActionAllowed) ? "RefusedTransferOut5" : "" });

        string sExportsExcelPath = ConfigurationManager.AppSettings["ExportsExcelFilePath"].ToString();
        DirectoryInfo di = new DirectoryInfo(Request.MapPath("~" + sExportsExcelPath));
        FileInfo[] Files = di.GetFiles("*.xlsx");

        foreach (FileInfo file in Files)
        {
            string sFileName = file.Name.Replace(file.Extension, "");
            string[] arSplit = sFileName.Split('_');

            DateTime date = new DateTime();
            if (DateTime.TryParseExact(arSplit[arSplit.Length - 1], "ddMMyyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
            {
                DataRow row = null;
                if (sFileName.StartsWith("Rejets_prelevements_tous_clients"))
                    row = dtExports.Rows[0];
                else if (sFileName.StartsWith("Rejets_prelevements_clients_1_avertissement"))
                    row = dtExports.Rows[1];
                else if (sFileName.StartsWith("Rejets_prelevements_clients_5_avertissements"))
                    row = dtExports.Rows[2];

                if (row != null)
                {
                    row["Filename"] = sFileName;
                    row["Link"] = "." + sExportsExcelPath + sFileName + ".xlsx";
                    row["Date"] = date.ToString("dd/MM/yyyy");
                }
            }
        }

        rptExportsExcel.DataSource = dtExports;
        rptExportsExcel.DataBind();
    }

    protected bool CreateExportExcelFile(DataTable dt, string sFilename, string sWorkbookLabel)
    {
        bool bGenerated = false;

        if (dt.Rows.Count > 0)
        {
            string sExportsExcelFilePath = ConfigurationManager.AppSettings["ExportsExcelFilePath"].ToString();

            // Create the file using the FileInfo object
            var file = new FileInfo(Server.MapPath("~/" + sExportsExcelFilePath + sFilename + ".xlsx"));

            // Create the package and make sure you wrap it in a using statement
            using (var package = new ExcelPackage(file))
            {
                // add a new worksheet to the empty workbook
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add(sWorkbookLabel);

                // Add some formatting to the worksheet
                worksheet.TabColor = Color.Blue;
                worksheet.DefaultRowHeight = 20;
                worksheet.HeaderFooter.FirstFooter.LeftAlignedText = string.Format("Generated: {0}", DateTime.Now.ToShortDateString());

                worksheet.Row(1).Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Row(1).Style.Fill.BackgroundColor.SetColor(Color.FromArgb(245, 117, 39));
                worksheet.Row(1).Style.Font.Color.SetColor(Color.White);
                worksheet.Row(1).Style.Font.Bold = true;
                worksheet.Row(1).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Row(1).Height = 30;
                worksheet.Cells[1, 1].Value = "Nom";
                worksheet.Cells[1, 2].Value = "Prénom";
                worksheet.Cells[1, 3].Value = "N° Compte";
                worksheet.Cells[1, 4].Value = "N° Inscription";
                worksheet.Cells[1, 5].Value = "Date dernière MAJ";
                worksheet.Cells[1, 6].Value = "Nb rejets de prélèvements";
                worksheet.Cells[1, 7].Value = "Montant total des rejets";
                worksheet.Cells[1, 8].Value = "Nb prélèvements acceptés";
                worksheet.Cells[1, 9].Value = "Montant total des prélèvements acceptés";
                worksheet.Cells[1, 10].Value = "Notification client pour 1er rejet sans dépôt";
                worksheet.Cells[1, 11].Value = "Date notification client pour 1er rejet sans dépôt";
                worksheet.Cells[1, 12].Value = "Notification client pour 2ème rejet sans dépôt";
                worksheet.Cells[1, 13].Value = "Date notification client pour 2ème rejet sans dépôt";
                worksheet.Cells[1, 14].Value = "Ticket Zendesk pour clôture (en raison d'un 2ème rejet sans dépôt)";
                worksheet.Cells[1, 15].Value = "Date ticket Zendesk pour clôture (en raison d'un second rejet sans dépôt)";
                worksheet.Cells[1, 16].Value = "Notification client pour 5 rejets sans dépôt";
                worksheet.Cells[1, 17].Value = "Date notification client pour 5 rejets sans dépôt";
                worksheet.Cells[1, 18].Value = "Notification client pour problème non résolu au bout d'un mois";
                worksheet.Cells[1, 19].Value = "Date notification client pour problème non résolu au bout d'un mois";
                worksheet.Cells[1, 20].Value = "Ticket Zendesk pour clôture (en raison d'un mauvais comportement sur plusieurs mois)";
                worksheet.Cells[1, 21].Value = "Date ticket Zendesk pour clôture (en raison d'un mauvais comportement sur plusieurs mois)";

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    worksheet.Cells[i + 2, 1].Value = dt.Rows[i]["LastName"];
                    worksheet.Cells[i + 2, 2].Value = dt.Rows[i]["FirstName"];
                    worksheet.Cells[i + 2, 3].Value = dt.Rows[i]["BankAccountNumber"];
                    worksheet.Cells[i + 2, 4].Value = dt.Rows[i]["RegistrationCode"];
                    worksheet.Cells[i + 2, 5].Value = dt.Rows[i]["RefreshDate"];
                    worksheet.Cells[i + 2, 6].Value = dt.Rows[i]["NbRefusals"];
                    worksheet.Cells[i + 2, 7].Value = dt.Rows[i]["RefusalAmount"];
                    worksheet.Cells[i + 2, 8].Value = dt.Rows[i]["NbOK"];
                    worksheet.Cells[i + 2, 9].Value = dt.Rows[i]["OKAmount"];
                    worksheet.Cells[i + 2, 10].Value = dt.Rows[i]["NotificationFirstRefusal"];
                    worksheet.Cells[i + 2, 11].Value = dt.Rows[i]["NotificationFirstRefusalDate"];
                    worksheet.Cells[i + 2, 12].Value = dt.Rows[i]["NotificationClosedSecondRefusal"];
                    worksheet.Cells[i + 2, 13].Value = dt.Rows[i]["NotificationClosedSecondRefusalDate"];
                    worksheet.Cells[i + 2, 14].Value = dt.Rows[i]["ZendeskClosedSecondRefusal"];
                    worksheet.Cells[i + 2, 15].Value = dt.Rows[i]["ZendeskClosedSecondRefusalDate"];
                    worksheet.Cells[i + 2, 16].Value = dt.Rows[i]["Notification5Refusals"];
                    worksheet.Cells[i + 2, 17].Value = dt.Rows[i]["Notification5RefusalsDate"];
                    worksheet.Cells[i + 2, 18].Value = dt.Rows[i]["NotificationM1"];
                    worksheet.Cells[i + 2, 19].Value = dt.Rows[i]["NotificationM1Date"];
                    worksheet.Cells[i + 2, 20].Value = dt.Rows[i]["ZendeskClosedM2"];
                    worksheet.Cells[i + 2, 21].Value = dt.Rows[i]["ZendeskClosedM2Date"];
                }

                worksheet.Cells[worksheet.Dimension.Address].AutoFitColumns();

                package.Save();

                bGenerated = true;
            }
        }

        return bGenerated;
    }

    protected void btnRefreshExport_Click(object sender, EventArgs e)
    {
        Button btn = (Button)sender;

        if (btn != null)
        {
            DataTable dt = new DataTable();
            string sFilename = "";
            string sFileTAG = "";
            string sWorkbookLabel = "";

            switch (btn.CommandArgument)
            {
                case "RefusedTransferOutAll":
                    dt = Client.GetCustomerTransferDebitRefusal(false, false);
                    sFileTAG = "Rejets_prelevements_tous_clients";
                    sFilename = sFileTAG + "_" + DateTime.Now.ToString("ddMMyyyy");
                    sWorkbookLabel = "Etat des prélèvements de tous les clients";
                    break;
                case "RefusedTransferOut1":
                    dt = Client.GetCustomerTransferDebitRefusal(true, false);
                    sFileTAG = "Rejets_prelevements_clients_1_avertissement";
                    sFilename = sFileTAG + "_" + DateTime.Now.ToString("ddMMyyyy");
                    sWorkbookLabel = "Etat des prélèvements des clients avertis pour 1 rejet sans dépôt";
                    break;
                case "RefusedTransferOut5":
                    dt = Client.GetCustomerTransferDebitRefusal(false, true);
                    sFileTAG = "Rejets_prelevements_clients_5_avertissements";
                    sFilename = sFileTAG + "_" + DateTime.Now.ToString("ddMMyyyy");
                    sWorkbookLabel = "Etat des prélèvements des clients avertis pour 5 rejets sans dépôt";
                    break;
            }

            if (CreateExportExcelFile(dt, sFilename, sWorkbookLabel))
            {
                string sExportsExcelPath = ConfigurationManager.AppSettings["ExportsExcelFilePath"].ToString();
                DirectoryInfo di = new DirectoryInfo(Request.MapPath("~" + sExportsExcelPath));
                FileInfo[] Files = di.GetFiles("*.xlsx");

                foreach (FileInfo file in Files)
                {
                    if (file.Name.StartsWith(sFileTAG) && file.Name.Replace(file.Extension, "") != sFilename)
                        File.Delete(file.FullName);
                }

                InitExportExcel();

                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "ShowExportsExcelTabKey", "ShowExportsExcelTab();", true);
            }
        }
    }
}