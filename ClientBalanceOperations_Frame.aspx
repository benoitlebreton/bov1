﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ClientBalanceOperations_Frame.aspx.cs" Inherits="ClientBalanceOperations_Frame" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Solde / Opérations</title>

    <link href="Styles/tipTip.css" rel="Stylesheet" type="text/css" />
    <link href="Styles/UI/jquery-ui-1.9.2.custom.css" rel="Stylesheet" type="text/css" />
    <link href="~/Styles/Site.css" rel="stylesheet" type="text/css" />
    <link href="Styles/Operations.css?v=20180314" type="text/css" rel="Stylesheet" />

    <style>
        .PaymentMethodFilter{
            
        }
        .PaymentMethodFilter:hover{
            cursor:pointer;
            text-decoration:line-through;
        }
    </style>

    <script src="Scripts/jquery-1.8.3.js" type="text/javascript"></script>
    <script src="Scripts/jquery.tipTip.minified.js" type="text/javascript"></script>
    <script src="Scripts/jquery-ui-1.9.2.custom.min.js" type="text/javascript"></script>
    <script src="Scripts/jQuery_mousewheel_plugin.js" type="text/javascript"></script>
    <script src="Scripts/jquery.jloupe.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="Scripts/mapbox.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery.watermark.min.js" type="text/javascript"></script>
    <%--<script src="Scripts/jquery.pnotify.min.js" type="text/javascript"></script>--%>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#search-popup').dialog({
                autoOpen: false,
                resizable: false,
                draggable: false,
                modal: true,
                minWidth: 310,
                position: { my: "center top", at: "center top", of: window },
                title: "Critères de recherche",
                buttons: [{ text: $('#<%=btnSearch.ClientID%>').val(), click: function () { $("#<%=btnSearch.ClientID %>").click(); $(this).dialog('close'); } }]
            });
            $("#search-popup").parent().appendTo(jQuery("form:first"));

            $('#change-category-popup').dialog({
                autoOpen: false,
                resizable: false,
                draggable: false,
                modal: true,
                width: 245,
                title: "Catégorie",
                buttons: [{ text: $('#<%=btnChangeCategory.ClientID%>').val(), click: function () { $("#<%=btnChangeCategory.ClientID %>").click(); $(this).dialog('close'); } }]
            });
            $("#change-category-popup").parent().appendTo(jQuery("form:first"));

            $("#<%=txtDateFrom.ClientID %>").datepicker({
                defaultDate: "-1w",
                dateFormat: "dd/mm/yy",
                dayNamesMin: ["Di", "Lu", "Ma", "Me", "Je", "Ve", "Sa"],
                monthNames: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"],
                firstDay: 1,
                //changeMonth: true,
                numberOfMonths: 1,
                onClose: function (selectedDate) {
                    $("#<%=txtDateTo.ClientID %>").datepicker("option", "minDate", selectedDate);
               },
               beforeShow: function (input, inst) {
                   setTimeout(function () {
                       inst.dpDiv.position({ my: 'right top', at: 'right bottom', of: input });
                   }, 0);
               }
            });
            $("#<%=txtDateTo.ClientID %>").datepicker({
                defaultDate: "0",
                dateFormat: "dd/mm/yy",
                dayNamesMin: ["Di", "Lu", "Ma", "Me", "Je", "Ve", "Sa"],
                monthNames: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"],
                firstDay: 1,
                maxDate: "0",
                //changeMonth: true,
                numberOfMonths: 1,
                onClose: function (selectedDate) {
                    $("#<%=txtDateFrom.ClientID %>").datepicker("option", "maxDate", selectedDate);
                }
            });

            $('.lblClientTime').text(GetClientTime());

            initPaymentMethodChoice();
        });

        function initPaymentMethodChoice() {
            $('#divPaymentMethod').empty();
            initPaymentChoiceSelect();
            
        }
        function initPaymentChoiceSelect() {
            $('#ddlPaymentMethod').show();
            $('#ddlPaymentMethod').empty();
            if ($('#<%=cbPaymentMethod.ClientID%> input[type="checkbox"]:not(:checked)').length > 0) {
                $('#ddlPaymentMethod').append('<option value=""></option>');
                $('#<%=cbPaymentMethod.ClientID%> input[type="checkbox"]:not(:checked)').each(function (index) {
                    if ($(this).val() != null && $(this).val().trim().length > 0)
                        $('#ddlPaymentMethod').append('<option value="' + $(this).val() + '">' + $('label[for="' + $(this).attr('id') + '"]').html() + '</option>');
                });
                $('#ddlPaymentMethod').unbind('change').bind('change', function () { selectPaymentMethod(); });
            }
            else { $('#ddlPaymentMethod').hide(); }
        }
        function selectPaymentMethod() {
            if ($('#ddlPaymentMethod option:selected').val().trim().length > 0) {
                $('#<%=cbPaymentMethod.ClientID%> input[value="' + $('#ddlPaymentMethod option:selected').val() + '"]').removeAttr('checked').attr('checked', true);
                showPaymentMethodFilters();
                initPaymentChoiceSelect();
            }
        }
        function deletePaymenMethod(paymentMethodValue) {
            $('#<%=cbPaymentMethod.ClientID%> input[value="' + paymentMethodValue + '"]').removeAttr('checked');
            initPaymentChoiceSelect();
            showPaymentMethodFilters();
        }
        function showPaymentMethodFilters() {
            $('#divPaymentMethod').empty();
            $('#<%=cbPaymentMethod.ClientID%> input[type="checkbox"]:checked').each(function (index) {
                if ($('#divPaymentMethod').html().trim().length > 0)
                    $('#divPaymentMethod').append(', <span class="PaymentMethodFilter" onclick="deletePaymenMethod(' + $(this).val() + ')">' + $('label[for="' + $(this).attr('id') + '"]').html() + '</span>');
                else
                    $('#divPaymentMethod').append('<span style="font-weight:bold;">Filtre(s) : </span><span class="PaymentMethodFilter" onclick="deletePaymenMethod(' + $(this).val() + ')">' + $('label[for="' + $(this).attr('id') + '"]').html() + '</span>');
            });
        }

        function ShowSearchPopup() {
            $('#search-popup .picto-list img').unbind('click').bind('click', function () {
                var sTag = $(this).attr('id').replace('img-', '');
                SelectSearchCategory(sTag, this);
            });

            $('#search-popup').dialog('open');
        }
        function HideSearchPopup() {
            $('#search-popup').dialog('close');
        }

        function SelectSearchCategory(sTag, img) {
            if (!$(img).hasClass('selected')) {
                $('#<%=hdnSearchCategory.ClientID%>').val($('#<%=hdnSearchCategory.ClientID%>').val() + sTag + ",");
                $(img).addClass('selected');
            }
            else {
                $('#<%=hdnSearchCategory.ClientID%>').val($('#<%=hdnSearchCategory.ClientID%>').val().replace(sTag + ",", ""));
                $(img).removeClass('selected');
            }
        }
        function ChangeSearchCategory(sTag, img) {
            if (!$(img).hasClass('selected')) {
                $('#<%=hdnSearchCategory.ClientID%>').val(sTag);
                $('#search-popup .picto-list img.selected').removeClass('selected');
                $(img).addClass('selected');
            }
            else {
                $('#<%=hdnSearchCategory.ClientID%>').val('');
                $(img).removeClass('selected');
            }
        }

        function ShowChangeCategoryPopup(event, sMerchantNumber, sTag, sLabel) {
            event = event || window.event //For IE

            $('#<%=hdnMerchantNumber.ClientID%>').val(sMerchantNumber);
            $('#<%=txtCategoryLabel.ClientID%>').val(sLabel);
            $('#change-category-popup .picto-list img.selected').removeClass('selected');

            if ($('#change-category-popup .picto-list #img-' + sTag).length > 0) {
                $('#change-category-popup #img-' + sTag).addClass('selected');
                $('#<%=hdnCategoryTag.ClientID%>').val(sTag);
            }
            else {
                $('#change-category-popup .picto-list img').first().addClass('selected');
                $('#<%=hdnCategoryTag.ClientID%>').val($('#change-category-popup .picto-list img').first().attr('id').replace('img-', ''));
            }

            $('#change-category-popup .picto-list img').unbind('click').bind('click', function () {
                var sTag = $(this).attr('id').replace('img-', '');
                ChooseNewCategory(sTag, this)
            });

            $('#change-category-popup').dialog('open');
            event.stopPropagation();
        }
        function HideChangeCategoryPopup() {
            $('#change-category-popup').dialog('close');
        }
        function ChooseNewCategory(sTag, img) {
            $('#<%=hdnCategoryTag.ClientID%>').val(sTag);
            $('#change-category-popup .picto-list img.selected').removeClass('selected');
            $(img).addClass('selected');
        }

        function ToggleOperationDetails(row) {
            if ($(row).next('tr').find('.operation-detail, .operation-detail-alternate').is(':hidden')) {
                $(row).next('tr').find('td').css('background-color', $(row).find('td').css('background-color'));
                //$(row).next('tr').find('td').css('padding-bottom', '5px');
                $(row).next('tr').find('.operation-detail, .operation-detail-alternate').slideDown(400);
                //$(row).find('td').css('border-bottom-right-radius', '0').css('border-bottom-left-radius', '0');

                $(row).find('.detail-arrow').attr('src', $(row).find('.detail-arrow').attr('src').replace(/down/g, 'up'));
            }
            else {
                $(row).next('tr').find('.operation-detail, .operation-detail-alternate').slideUp(400, function () {
                    //$(row).find('td:first-of-type').css('border-top-left-radius', '8px').css('border-bottom-left-radius', '8px');
                    //$(row).find('td:last-of-type').css('border-top-right-radius', '8px').css('border-bottom-right-radius', '8px');
                    //$(row).next('tr').find('td').animate({ paddingBottom: 0 }, 0);
                });

                $(row).find('.detail-arrow').attr('src', $(row).find('.detail-arrow').attr('src').replace(/up/g, 'down'));
            }
        }

        function GetClientTime() {
            var date = new Date();
            var sHour = date.getHours().toString();
            var sMinute = date.getMinutes().toString();
            if (sMinute.length == 1)
                sMinute = '0' + sMinute;
            return sHour + ':' + sMinute;
        }

        function ToggleHelp() {
            if ($('.operation-help').is(':visible'))
                HideHelp();
            else ShowHelp();
        }
        function ShowHelp() {
            $('.operation-help').fadeIn();
            $('.operation-help').position({ my: 'center bottom', at: 'center bottom-20px', of: '.operation-table' });
            setTimeout(function () { $(window).bind('click', function () { HideHelp(); $(window).unbind('click'); }); }, 200);
        }
        function HideHelp() {
            $('.operation-help').fadeOut();
        }
        
        function ShowConfirmCancelMinuteOperation(row) {
            $('#<%=hdnRowNumber.ClientID%>').val(row);

            $('#cancelMinuteOpe').dialog({
                autoOpen: true,
                resizable: false,
                width: 400,
                dialogClass: "no-close",
                modal: true,
                title: "Annulation de l'opération",
                buttons: [{ text: "Annulation de l'opération", click: function () { $("#<%=btnCancelMinuteOpe.ClientID %>").click(); $(this).dialog('close'); } },
                    { text: "non", click: function () { $(this).dialog('close'); } }]

            });
            $(".ui-dialog-titlebar").hide();
            $("#cancelMinuteOpe").parent().appendTo(jQuery("form:first"));
        }

    </script>
</head>
<body style="background-color: #fff">
    <form id="form1" runat="server">
        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" EnablePartialRendering="true" EnableScriptLocalization="true"
            EnableScriptGlobalization="true" EnablePageMethods="true">
        </asp:ToolkitScriptManager>
        <div style="width: 100%;">
            <div style="margin-top: 0px">
                <asp:Panel ID="panelOperationListTable" runat="server" Visible="false">
                    <asp:UpdatePanel ID="upOpe" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>

                            <!--<div style="float:left;margin-bottom:5px;">
                        <a href="votre-calendrier.aspx" class="view-link">
                            <img src="Styles/Img/calendrier.png" style="height:30px;" />
                            &nbsp;<span style="position:relative;top:-8px">Vue calendrier</span>
                        </a>
                    </div>-->

                            <h2 style="padding-left: 2px; color: #344b56; margin-bottom: 10px">SOLDE
                            </h2>
                            <div style="padding-left: 2px;">
                                <asp:Label ID="lblClientBalance" runat="server" CssClass="label" Style="font-weight: normal; font-size: 1.2em"></asp:Label>
                            </div>

                            <h2 style="padding-left: 2px; color: #344b56; margin-bottom: 10px">OPERATIONS
                            </h2>

                            <asp:Panel ID="panelNoFilters" runat="server">
                                <div style="padding: 0 2px; text-align: left; font-size: 1.2em; text-transform: uppercase">
                                    au
                                    <asp:Label ID="lblDateNow" runat="server" Style="font-weight: bold; text-transform: uppercase"></asp:Label>, à <span class="lblClientTime" style="font-weight: bold"></span>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="panelFilters" runat="server" Visible="false">
                                <div style="padding: 0 2px; text-align: left; font-size: 1.2em;" class="font-AracneRegular">
                                    <div style="width: 90%; float: left">
                                        <asp:Literal ID="ltlFilters" runat="server"></asp:Literal>
                                    </div>
                                    <div style="width: 10%; float: right; text-align: right">
                                        <asp:Literal ID="ltlNbResults" runat="server"></asp:Literal>
                                    </div>
                                    <div style="clear: both"></div>
                                </div>
                            </asp:Panel>

                            <div id="ar-loading" style="display: none; position: absolute; background-color: rgba(255, 255, 255, 0.5); z-index: 100"></div>
                            <table id="tOperationList" style="width: 100%; margin-bottom: 10px;" class="operation-table">
                                <thead>
                                    <tr>
                                        <th style="white-space: nowrap">
                                            <div>Date d'opération</div>
                                        </th>
                                        <th>
                                            <div>Heure</div>
                                        </th>
                                        <th>
                                            <div>Lieu</div>
                                        </th>
                                        <th>
                                            <div>
                                                Type<br />
                                                d'opération
                                            </div>
                                        </th>
                                        <th>
                                            <div>Objet</div>
                                        </th>
                                        <th colspan="2">
                                            <div>Montant</div>
                                        </th>
                                        <th>
                                            <div>Catégorie</div>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="rptOperationList" runat="server" OnItemDataBound="rptOperationList_ItemDataBound">
                                        <HeaderTemplate>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr class='<%# Container.ItemIndex % 2 == 0 ? "operation-table-row" : "operation-table-alternate-row" %>' onclick="ToggleOperationDetails(this);">
                                                <%--<%# Eval("OPE_STATUS").ToString() == "2" ? " no-operation-detail" : "" %>--%>
                                                <td style="width: 1px; white-space: nowrap; text-align: center">
                                                    <div>
                                                        <span style="position: relative; left: -6px;">
                                                            <img class="detail-arrow" src="Styles/Img/row-detail-arrow-down.png" /></span>
                                                        <%# tools.getFormattedShortDate(Eval("OPE_DATE").ToString(), '.') %>
                                                    </div>
                                                </td>
                                                <td style="width: 1px; white-space: nowrap; text-align: center">
                                                    <div><%# tools.getFormattedTime(Eval("OPE_TIME").ToString(), 'h')%></div>
                                                </td>
                                                <td style="width: 100px">
                                                    <div class="ellipsis" style="width: 100px"><%# Eval("MERC_CITY")%></div>
                                                </td>
                                                <td style="width: 100px; text-align: center" class='<%# getOpeTypeClass("", Eval("OPE_CODE").ToString())%>'>
                                                    <div class="ellipsis" style="width: 100px"><%# Eval("OPE_CODE_LABEL")%></div>
                                                </td>
                                                <td style="text-align: left;" class='<%# getOpeTypeClass("", Eval("OPE_CODE").ToString())%>'>
                                                    <div><%# Eval("OPE_LABEL")%></div>
                                                </td>
                                                <td style="width: 1px; white-space: nowrap; text-align: right" class='<%# getOpeTypeClass("", Eval("OPE_CODE").ToString())%>'>
                                                    <div style="border-right: 0"><%# getFormattedAmount(Eval("AMOUNT").ToString())%></div>
                                                </td>
                                                <td style="width: 1px; white-space: nowrap; text-align: center">
                                                    <div style="width: 20px; border-left: 0"><%# getStatusImage(Eval("OPE_STATUS").ToString(), Eval("row").ToString())%></div>
                                                </td>
                                                <td style="width: 1px; white-space: nowrap; text-align: center">
                                                    <div>
                                                        <%# getCategoryImage(Eval("TAGCATEGORY").ToString(), Eval("MERC_NB").ToString(), Eval("CATEGORY").ToString(), Eval("CATEGORY_CHG").ToString())%>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr style="cursor: auto; border: 0">
                                                <td id="tdDetails" runat="server" colspan="8" style="padding: 0">
                                                    <!--style="border-bottom-left-radius:8px;border-bottom-right-radius:8px;"-->
                                                    <div class='<%# Container.ItemIndex % 2 == 0 ? "operation-detail" : "operation-detail-alternate" %>' style="display: none; position: relative; z-index: 0">
                                                        <div class="separator"></div>
                                                        <div class="detail-content" style="min-height: 15px;">
                                                            <asp:HiddenField ID="hdnOpeCode" runat="server" Value='<%# Eval("OPE_CODE")%>' />
                                                            <asp:HiddenField ID="hdnOpeStatus" runat="server" Value='<%# Eval("OPE_STATUS")%>' />
                                                            <div class="ellipsis" style="width: 46%;"><%# ShowOpeDetail(Eval("OPE_LABEL_DET1").ToString(), Eval("OPE_CODE").ToString()) %></div>
                                                            <div class="ellipsis" style="width: 46%;"><%# ShowOpeDetail(Eval("OPE_LABEL_DET2").ToString(), Eval("OPE_CODE").ToString()) %></div>
                                                            <asp:Panel ID="panelPurchaseDetails" runat="server" Visible="false" Style="min-height: 45px">
                                                                <div style="width: 41%; float: left">
                                                                    <div><%--<%# Eval("TRX_MODE")%>--%>&nbsp;</div>
                                                                </div>
                                                                <div class="ope-details-address" style="width: 25%; float: right; position: absolute; top: 2px;">
                                                                    <div style="text-align: left">
                                                                        <div><%# Eval("MERC_ADD")%></div>
                                                                        <div><%# Eval("MERC_ZIP")%> <%# Eval("MERC_CITY")%></div>
                                                                        <div><%# Eval("COUNTRY")%></div>
                                                                    </div>
                                                                </div>
                                                                <div style="clear: both"></div>
                                                            </asp:Panel>
                                                            <div class="ellipsis" style="width: 25%; text-align: right; position: absolute; right: 20px; top: 2px;"><%# Eval("CATEGORY")%></div>
                                                        </div>

                                                        <div>
                                                            <%# Eval("ADDITIONAL_INFO_HTML")%>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td style="background-color: #f57527;">
                                            <asp:Label ID="lblTotal" runat="server" Style="display: block; color: #fff; text-align: right; white-space: nowrap; padding: 8px 5px"></asp:Label>
                                        </td>
                                        <td style="background-color: #f57527;"></td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>

                            <div style="text-align: right; float: right; position: relative; top: 3px;">
                                <div class="operation-help-link" onclick="ToggleHelp();">
                                    <img src="Styles/Img/interrogation-small.png" style="position: relative; top: 5px" />
                                    <span style="position: relative; top: -1px">aide</span>
                                </div>
                                <div class="operation-help">
                                    <div class="font-bold uppercase">Légende</div>
                                    <div>
                                        <div>
                                            - Le montant d'une transaction en <span class="font-red">rouge</span> représente un débit sur votre compte.
                                        </div>
                                        <div>
                                            - Le montant d'une transaction en <span class="font-green">vert</span> représente un crédit sur votre compte.
                                        </div>
                                        <div>
                                            - Une opération en <span class="font-orange">orange</span> représente des frais Compte-Nickel (retraits, dépôts, etc.).
                                        </div>
                                        <div>
                                            - Une
                                            <img src="Styles/Img/clock.png" alt="Horloge" height="20px" />
                                            indique que l'opération est en attente de confirmation de la banque du commerçant.
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <asp:Panel ID="panelNoSearchResult" runat="server" Visible="false" Style="margin-top: 10px">
                                Votre recherche n'a retourné aucun résultat.
                            </asp:Panel>

                            <div style="text-align: center; margin: 3px 0 50px 0">
                                <asp:Button ID="btnShowMore" runat="server" CssClass="button" Text="Afficher plus d'opérations" OnClick="btnShowMore_Click" Style="padding-left: 50px; padding-right: 50px; margin-right: 1px" />
                                <input runat="server" id="btnSearchTmp" type="button" value="Rechercher une opération" class="button" onclick="ShowSearchPopup();" style="padding-left: 50px; padding-right: 50px;" />
                                <asp:Button ID="btnExcelExport" runat="server" CssClass="button excel-button" Text="Export" OnClick="btnExportExcel_Click" />
                            </div>

                            <div style="clear: both"></div>

                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnChangeCategory" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnCancelMinuteOpe" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>

                    <div id="search-popup" style="display: none">
                        <span class="ui-helper-hidden-accessible">
                            <input type="text" /></span>
                        <div>
                            <div class="font-bold uppercase" style="margin: 5px 0">
                                Date d'opération
                            </div>
                            <div style="white-space: nowrap">
                                <asp:Label ID="lblDateFrom" runat="server" AssociatedControlID="txtDateFrom" CssClass="font-AracneRegular font-orange" Font-Bold="true">Du</asp:Label>&nbsp;
                            <asp:TextBox ID="txtDateFrom" runat="server" Style="width: 100px"></asp:TextBox>&nbsp;
                            <asp:Label ID="lblDateTo" runat="server" AssociatedControlID="txtDateTo" CssClass="font-AracneRegular font-orange" Font-Bold="true">Au</asp:Label>&nbsp;
                            <asp:TextBox ID="txtDateTo" runat="server" Style="width: 100px"></asp:TextBox>
                            </div>
                        </div>
                        <div>
                            <div class="font-bold uppercase" style="margin: 10px 0 5px 0">
                                Type d'opération
                            </div>
                            <div>
                                <asp:CheckBoxList ID="cbPaymentMethod" runat="server" DataTextField="Text" DataValueField="Value" CssClass="uppercase" style="display:none">
                                </asp:CheckBoxList>
                                <select id="ddlPaymentMethod" class="uppercase" style="width: 280px"></select>

                                <%--<asp:DropDownList ID="ddlPaymentMethod" runat="server" DataTextField="Text" DataValueField="Value" AppendDataBoundItems="true" CssClass="uppercase" Style="width: 280px">
                                    <asp:ListItem Text="" Value="" Selected="True"></asp:ListItem>
                                </asp:DropDownList>--%>
                            </div>
                            <div id="divPaymentMethod">
                            </div>
                        </div>
                        <asp:Panel ID="panelCategoryChoice" runat="server">
                            <div class="font-bold uppercase" style="margin: 10px 0 5px 0">
                                Catégorie
                            </div>
                            <div>
                                <asp:HiddenField ID="hdnSearchCategory" runat="server" />
                                <asp:Repeater ID="rptCategory2" runat="server">
                                    <HeaderTemplate>
                                        <div class="picto-list">
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <img id='img-<%# Eval("TAG") %>' src='Styles/Img/<%# Eval("IMG") %>' alt='<%# Eval("LBL") %>' />
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </div>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </asp:Panel>
                        <div style="text-align: center">
                            <asp:Button ID="btnSearch" runat="server" Text="Rechercher" OnClick="btnSearch_Click" CssClass="button" Style="display: none" />

                        </div>
                    </div>

                    <div id="change-category-popup" style="display: none">
                        <span class="ui-helper-hidden-accessible">
                            <input type="text" /></span>
                        <asp:HiddenField ID="hdnMerchantNumber" runat="server" />
                        <div class="font-bold uppercase" style="margin: 5px 0">
                            Libellé
                        </div>
                        <div>
                            <asp:TextBox ID="txtCategoryLabel" runat="server" Style="width: 95%" />
                        </div>
                        <div class="font-bold uppercase" style="margin: 10px 0 5px 0">
                            Pictogramme
                        </div>
                        <div>
                            <asp:HiddenField ID="hdnCategoryTag" runat="server" />
                            <asp:Repeater ID="rptCategory1" runat="server">
                                <HeaderTemplate>
                                    <div class="picto-list">
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <img id='img-<%# Eval("TAG") %>' src='Styles/Img/<%# Eval("IMG") %>' alt='<%# Eval("LBL") %>' />
                                </ItemTemplate>
                                <FooterTemplate>
                                    </div>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                        <div style="text-align: center">
                            <asp:Button ID="btnChangeCategory" runat="server" Text="Modifier la catégorie" OnClick="btnChangeCategory_Click" CssClass="button" Style="display: none" />
                        </div>
                    </div>

                    <div id="cancelMinuteOpe" style="display: none">
                        <asp:HiddenField ID="hdnRowNumber" runat="server" />
                        <div class="font-bold uppercase" style="margin: 10px 0 5px 0">
                            Vous allez annuler l'opération
                        </div>
                        <div style="text-align: center">
                            <asp:Button ID="btnCancelMinuteOpe" runat="server" Text="annulation de l'opération" OnClick="btnCancelMinuteOpe_Click" CssClass="button" Style="display: none" />
                        </div>
                    </div>

                </asp:Panel>

                <asp:Panel ID="panelOperationListEmpty" runat="server" Visible="false">
                    <div class="font-bold uppercase" style="margin-bottom: 10px; text-align: center">
                        Vous n'avez effectué aucune opération à ce jour.
                    </div>
                </asp:Panel>
                <asp:Panel ID="panelOperationError" runat="server" Visible="false" Style="color: red; font-weight: bold; text-align: center; padding: 10px;">
                    <div style="margin: auto; height: 50px; width: 90%;">
                        Une erreur s'est produite.
                        <br />
                        Veuillez réessayer ultérieurement.
                    </div>
                </asp:Panel>
            </div>
        </div>

        <div id="panelAlertMessage" style="display: none">
            <asp:Label ID="lblAlertMessage" runat="server" Style="color: red"></asp:Label>
        </div>

        <script type="text/javascript">
            Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
            var sRequestControlID = "";
            function BeginRequestHandler(sender, args) {
                pbControl = args.get_postBackElement();
                if (pbControl.id == '<%=btnShowMore.ClientID %>' || pbControl.id == '<%=btnSearch.ClientID %>' || pbControl.id == '<%=btnChangeCategory.ClientID %>') {
                    var width = $('#tOperationList').width();
                    var height = $('#tOperationList').height();
                    $('#ar-loading').css('width', width);
                    $('#ar-loading').css('height', height);

                    $('#ar-loading').show();

                    $('#ar-loading').position({ my: "left top", at: "left top", of: "#tOperationList" });
                }
                sRequestControlID = pbControl.id;
            }
            function EndRequestHandler(sender, args) {
                $('#ar-loading').hide();

                if (sRequestControlID == '<%=btnSearch.ClientID %>') {
                    $(document).scrollTop(0);
                }

                $('.lblClientTime').text(GetClientTime());
            }
        </script>
    </form>
</body>
</html>
