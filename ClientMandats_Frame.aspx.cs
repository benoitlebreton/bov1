﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class ClientMandats_Frame : System.Web.UI.Page
{
    private string _sClientBankAccount
    {
        get { return ViewState["ClientBankAccount"].ToString(); }
        set { ViewState["ClientBankAccount"] = value; }
    }

    private string _sRefCustomer
    {
        get { return ViewState["RefCustomer"].ToString(); }
        set { ViewState["RefCustomer"] = value; }
    }
    private string _sIBAN
    {
        get { return (ViewState["IBAN"] != null) ? ViewState["IBAN"].ToString() : ""; }
        set { ViewState["IBAN"] = value; }
    }
    private bool _bIsCobalt
    {
        get { return (ViewState["IsCobalt"] != null) ? bool.Parse(ViewState["IsCobalt"].ToString()) : false; }
        set { ViewState["IsCobalt"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("fr-FR");
        Thread.CurrentThread.CurrentUICulture = new CultureInfo("fr-FR");
        ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
        //scriptManager.RegisterPostBackControl(this.btnExcelExport);

        panelTransferListTable.Visible = true;
        lblDateNow.Text = DateTime.Now.ToString("dd MMMM yyyy");

        if (!IsPostBack)
        {
            if (Request.Params["ref"] != null && Request.Params["ref"].Length > 0)
            {
                _sRefCustomer = Request.Params["ref"].ToString();
            }

            if (Request.Params["account"] != null && Request.Params["account"].Length > 0)
            {
                _sClientBankAccount = Request.Params["account"].ToString();
                //rptAuthorizationList.DataSource = operation.getUserAuthorizationList(_sClientBankAccount.Trim(), 1, 100, "", "", "ALL");
                //rptAuthorizationList.DataBind();
                if (Request.Params["isCobalt"] != null && Request.Params["isCobalt"].ToString() == "1")
                    _bIsCobalt = true;

                if (Request.Params["iban"] != null && Request.Params["iban"].Length > 0)
                    _sIBAN = Request.Params["iban"].ToString();

                operation ope = new operation();
                int iSize = 15;
                int iStep = 15;
                operation.SearchCriteria searchCriteria = new operation.SearchCriteria(iSize, iStep, "", "", _sClientBankAccount, "2", "ALL", "ALL", true);
                searchCriteria.bIsCobalt = _bIsCobalt;
                searchCriteria.sIBAN = _sIBAN;

                UpdateOperationList(searchCriteria);
                searchCriteria.bForceRefresh = false;
                ViewState["SearchCriteria"] = searchCriteria;

                if (rptTransferList.Items.Count > 0)
                {
                    panelNoSearchResult.Visible = false;
                    rptTransferList.Visible = true;
                }
                else
                {
                    panelNoSearchResult.Visible = true;
                    rptTransferList.Visible = false;
                }
            }
            else
            {
                panelTransferListTable.Visible = false;
            }

        }
    }

    protected void UpdateNbPages(int iPageSize, int iNbTotal)
    {
        ltlNbResults.Text = "<span class=\"font-bold\">" + iPageSize.ToString() + "</span>/<span class=\"font-bold\">" + iNbTotal.ToString() + "</span>";
    }

    protected void UpdateOperationList(operation.SearchCriteria searchCriteria)
    {
        operation ope = new operation();
        int iRowCount;

        searchCriteria.sOperationType = "2";

        DataTable dt = operation.getUserMandateListCobalt(searchCriteria, out iRowCount);

        if (iRowCount == dt.Rows.Count)
        {
            //btnShowMore.Visible = false;
            UpdateNbPages(iRowCount, iRowCount);
        }
        else
        {
            //btnShowMore.Visible = true;
            UpdateNbPages(dt.Rows.Count, iRowCount);
        }

        if (dt.Rows.Count > 0)
        {
            rptTransferList.Visible = true;
            panelNoSearchResult.Visible = false;
            rptTransferList.DataSource = dt;
            rptTransferList.DataBind();
        }
        else
        {
            rptTransferList.Visible = false;
            panelNoSearchResult.Visible = true;
            rptTransferList.DataSource = null;
            rptTransferList.DataBind();
        }
    }

    protected void btnActivateMandate_Click(object sender, EventArgs e)
    {
        Button btn = (Button)sender;
        int iRowCount;
        operation.SearchCriteria searchCriteria = (operation.SearchCriteria) ViewState["SearchCriteria"];

        searchCriteria.sOperationType = "2";
        searchCriteria.bIsCobalt = _bIsCobalt;
        searchCriteria.sIBAN = _sIBAN;
        DataTable dt = operation.getUserMandateListCobalt(searchCriteria, out iRowCount);
        if (hdnActivateMandate.Value.Length > 0)
        {
            int currentRow = 0;
            string sChampNumOPE = "";

            for (currentRow = 0; currentRow < dt.Rows.Count; currentRow++)
            {
                sChampNumOPE = dt.Rows[currentRow].Field<string>("rumId");
                if (sChampNumOPE.Equals(hdnActivateMandate.Value.ToString()) == true)
                {
                    string sError = "", sRC = "";
                    string sIbanDonneurOrdre = dt.Rows[currentRow].Field<string>("creditorIban");

                    //string sRomain = "NumOpe = " + sChampNumOPE + " créée le " + dtChampDateCreation.ToString();
                    if (operation.doActivateMandate(_sRefCustomer, _sIBAN, sChampNumOPE, sIbanDonneurOrdre, out sError, out sRC))
                    {
                        UpdateOperationList(searchCriteria);
                    }
                    else
                    {
                        ShowAlertMessage((sError.Length > 0) ? sError : "Activation mandat échouée");
                    }

                    break;
                }
            }
        }
    }

    protected void btnSuspendMandate_Click(object sender, EventArgs e)
    {
        Button btn = (Button)sender;
        operation.SearchCriteria searchCriteria = (operation.SearchCriteria)ViewState["SearchCriteria"];
        int iRowCount;

        searchCriteria.sOperationType = "2";
        searchCriteria.bIsCobalt = _bIsCobalt;
        searchCriteria.sIBAN = _sIBAN;
        DataTable dt = operation.getUserMandateListCobalt(searchCriteria, out iRowCount);
        if (hdnSuspendMandate.Value.Length > 0)
        {
            int currentRow = 0;
            string sChampNumOPE = "";

            for (currentRow = 0; currentRow < dt.Rows.Count; currentRow++)
            {
                sChampNumOPE = dt.Rows[currentRow].Field<string>("rumId");
                if (sChampNumOPE.Equals(hdnSuspendMandate.Value.ToString()) == true)
                {
                    string sError = "", sRC = "";
                    string sIbanDonneurOrdre = dt.Rows[currentRow].Field<string>("creditorIban");

                    //string sRomain = "NumOpe = " + sChampNumOPE + " créée le " + dtChampDateCreation.ToString();
                    if (operation.doSuspendMandate(_sRefCustomer, _sIBAN, sChampNumOPE, sIbanDonneurOrdre, out sError, out sRC))
                    {
                        UpdateOperationList(searchCriteria);
                    }
                    else
                    {
                        ShowAlertMessage((sError.Length > 0) ? sError : "Suspension mandat échouée");
                    }

                    break;
                }
            }
        }
    }


    protected void btnBlockMandate_Click(object sender, EventArgs e)
    {
        Button btn = (Button)sender;
        int iRowCount;
        operation.SearchCriteria searchCriteria = (operation.SearchCriteria)ViewState["SearchCriteria"];

        searchCriteria.sOperationType = "2";
        searchCriteria.bIsCobalt = _bIsCobalt;
        searchCriteria.sIBAN = _sIBAN;
        DataTable dt = operation.getUserMandateListCobalt(searchCriteria, out iRowCount);
        if (hdnBlockMandate.Value.Length > 0)
        {
            int currentRow = 0;
            string sChampNumOPE = "";

            for (currentRow = 0; currentRow < dt.Rows.Count; currentRow++)
            {
                sChampNumOPE = dt.Rows[currentRow].Field<string>("rumId");
                if (sChampNumOPE.Equals(hdnBlockMandate.Value.ToString()) == true)
                {
                    string sError = "", sRC = "";
                    string sIbanDonneurOrdre = dt.Rows[currentRow].Field<string>("creditorIban");

                    //string sRomain = "NumOpe = " + sChampNumOPE + " créée le " + dtChampDateCreation.ToString();

                    if (operation.doBlockMandate(_sRefCustomer, _sIBAN, sChampNumOPE, sIbanDonneurOrdre, out sError, out sRC))
                    {
                        UpdateOperationList(searchCriteria);
                    }
                    else
                    {
                        ShowAlertMessage((sError.Length > 0) ? sError : "Blocage mandat échoué");
                    }

                    break;
                }
            }
        }
    }

    protected string sGetMandateMenu(string sRumId, string sMandateStatus)
    {
        string sReturn = "";
        switch (sMandateStatus)
        {
            case "RUM_STATUS_CODE_TO_CONFIRM":
                sReturn = "<span onclick=\"ShowConfirmActivateMandate('" + sRumId + "')\">activer le mandat</span>";
                sReturn += "<span onclick=\"ShowConfirmSuspendMandate('" + sRumId + "')\">Suspendre le mandat</span>";
                sReturn += "<span onclick=\"ShowConfirmBlockMandate('" + sRumId + "')\">Bloquer définitivement le mandat</span>";
                break;
            case "RUM_STATUS_CODE_ACTIVATED":
                sReturn = "<span onclick=\"ShowConfirmSuspendMandate('" + sRumId + "','Suspendre')\">Suspendre le mandat</span>";
                sReturn += "<span onclick=\"ShowConfirmBlockMandate('" + sRumId + "')\">Bloquer définitivement le mandat</span>";
                break;
            //case "RUM_STATUS_CODE_REJECTED":
            case "RUM_STATUS_CODE_DEACTIVATED":
                sReturn = "<span onclick=\"ShowConfirmActivateMandate('" + sRumId + "')\">Ré-activer le mandat</span>";
                sReturn += "<span onclick=\"ShowConfirmBlockMandate('" + sRumId + "')\">Bloquer définitivement le mandat</span>";
                break;
        }

        /*
         *   <span onclick="<%# string.Format("ShowConfirmSuspendCreditor('{0}', '{1}');", Eval("NumOPE"), operation.getCreditorMenu(Eval("sMandateRejet").ToString())) %>"><%# operation.getCreditorMenu(Eval("sMandateRejet").ToString()) %></span>
             <span onclick="<%# string.Format("ShowConfirmBlockCreditor('{0}');", Eval("NumOPE")) %>">Bloquer définitivement le créancier</span>

         * */
        return sReturn;
    }

    protected static string getMandateStatus(string sMandateStatus)
    {
        string sStatus = "";

        switch (sMandateStatus)
        {
            case "RUM_STATUS_CODE_TO_CONFIRM":
                sStatus = "A CONFIRMER";
                break;
            case "RUM_STATUS_CODE_ACTIVATED":
                sStatus = "ACTIF";
                break;
            case "RUM_STATUS_CODE_REJECTED":
                sStatus = "BLOQUE";
                break;
            case "RUM_STATUS_CODE_DEACTIVATED":
                sStatus = "SUSPENDU";
                break;
        }

        return sStatus;
    }

    protected void rptTransferList_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
    }

    protected void ShowAlertMessage(string sTitle, string message)
    {
        ClientScriptManager cs = Page.ClientScript;
        cs.RegisterStartupScript(this.Page.GetType(), "alertMessageVirementKey", "alertMessageVirement(\"" + sTitle + "\",\"" + message + "\");", true);
    }
    protected void ShowAlertMessage(string message)
    {
        ClientScriptManager cs = Page.ClientScript;
        cs.RegisterStartupScript(this.Page.GetType(), "alertMessageVirementKey", "alertMessageVirement('Erreur',\"" + message + "\");", true);
    }
}