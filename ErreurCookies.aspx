﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ErreurCookies.aspx.cs" Inherits="ErreurCookies" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Erreur cookies</title>

    <link href="Styles/tipTip.css" rel="Stylesheet" type="text/css" />
    <link href="Styles/UI/jquery-ui-1.9.2.custom.css" rel="Stylesheet" type="text/css" />
    <link href="~/Styles/Site.css" rel="stylesheet" type="text/css" />

    <script src="Scripts/jquery-1.8.3.js" type="text/javascript"></script>
    <script src="Scripts/jquery.tipTip.minified.js" type="text/javascript"></script>
    <script src="Scripts/jquery-ui-1.9.2.custom.min.js" type="text/javascript"></script>
    <script src="Scripts/jQuery_mousewheel_plugin.js" type="text/javascript"></script>
    <script src="Scripts/jquery.jloupe.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="Scripts/mapbox.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery.watermark.min.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
        <%--<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>--%>
        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" EnablePartialRendering="true" EnableScriptLocalization="true"
            EnableScriptGlobalization="true" EnablePageMethods="true">
        </asp:ToolkitScriptManager>
        <div class="page">
            <div class="header">
                <div class="title">
                    <div style="display: table; width: 100%">
                        <div style="display: table-row">
                            <div style="display: table-cell; width: 70%">
                                <div style="margin: 5px; display: table;">
                                    <div style="display: table-row">
                                        <div style="display: table-cell; vertical-align: middle">
                                            <img src="Styles/Img/LogoNickel.png" alt="NICKEL Backoffice" height="110px" /></div>
                                        <div style="display: table-cell; padding-left: 20px; padding-top: 5px; vertical-align: middle; font-size: 42px; color: #1A171B; font-weight: bold; font-style: normal; text-transform: uppercase; font-family: dinBold, Verdana">Backoffice</div>
                                    </div>
                                </div>
                            </div>
                            <div style="display: table-cell; vertical-align: middle; padding-right: 10px; width: 30%">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clear hideSkiplink">
                    <div class="table" style="width: 100%">
                        <div class="row">
                            <div class="cell">
                                <div class="menu">
                                    <asp:Button ID="btnHome" runat="server" Text="Accueil" PostBackUrl="~/Default.aspx" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="main">
                <div style="width:100%; margin:auto; text-align:center">
                    <h1 class="font-orange" style="font-size:2.5em; padding:0; margin:0">OUPS...</h1>
                    <div style="font-size:1.2em;">
                        <div style="margin-top:5px; border-top:1px solid #808080; padding-top:20px;font-weight:bold">
                            Il semble que vos cookies soient désactivés dans votre navigateur.
                        </div>
                        <div style="margin-top:10px;">
                            Pour pouvoir accéder au site, veuillez activer vos cookies
                        </div>
                        <div style="margin-top:10px;">ou</div>
                        <div style="margin-top:10px;">essayer d'y accéder depuis un autre navigateur.</div>
                        <div style="margin:auto; margin:20px;">
                            <img src="Styles/Img/cookies.png" alt="" style="max-width:25%;" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
