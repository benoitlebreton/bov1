﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using XMLMethodLibrary;
using System.Globalization;

public partial class AvisCSDetails : System.Web.UI.Page
{
    private string _Prev { get { return (ViewState["Prev"] != null) ? ViewState["Prev"].ToString() : ""; } set { ViewState["Prev"] = value; } }
    private bool _Editable { get { return (Session["AvisCSEditable"] != null) ? bool.Parse(Session["AvisCSEditable"].ToString()) : false; } set { Session["AvisCSEditable"] = value; } }

    private string _Page1 { get { return (ViewState["Page1"] != null) ? ViewState["Page1"].ToString() : ""; } set { ViewState["Page1"] = value; } }
    private string _Page2 { get { return (ViewState["Page2"] != null) ? ViewState["Page2"].ToString() : ""; } set { ViewState["Page2"] = value; } }

    private string _OriginalFile { get { return (ViewState["OriginalFile"] != null) ? ViewState["OriginalFile"].ToString() : ""; } set { ViewState["OriginalFile"] = value; } }
    private string _ResponseFile { get { return (ViewState["ResponseFile"] != null) ? ViewState["ResponseFile"].ToString() : ""; } set { ViewState["ResponseFile"] = value; } }
    private bool _FileRetrieved { get { return (ViewState["FileRetrieved"] != null) ? bool.Parse(ViewState["FileRetrieved"].ToString()) : false; } set { ViewState["FileRetrieved"] = value; } }
    private string _RefAvisCS { get { return (ViewState["RefAvisCS"] != null) ? ViewState["RefAvisCS"].ToString() : ""; } set { ViewState["RefAvisCS"] = value; } }
    private bool _Certified { get { return (ViewState["Treated"] != null) ? bool.Parse(ViewState["Treated"].ToString()) : false; } set { ViewState["Treated"] = value; } }
    private string _DocType { get { return (ViewState["DocType"] != null) ? ViewState["DocType"].ToString() : ""; } set { ViewState["DocType"] = value; } }
    private bool _IsRevival { get { return (ViewState["IsRevival"] != null) ? bool.Parse(ViewState["IsRevival"].ToString()) : false; } set { ViewState["IsRevival"] = value; } }
    private bool _IsAlreadyTreated { get { return (ViewState["IsAlreadyTreated"] != null) ? bool.Parse(ViewState["IsAlreadyTreated"].ToString()) : false; } set { ViewState["IsAlreadyTreated"] = value; } }
    private string _OriginTreatmentDate { get { return (ViewState["OriginTreatmentDate"] != null) ? ViewState["OriginTreatmentDate"].ToString() : ""; } set { ViewState["OriginTreatmentDate"] = value; } }
    private string _OriginTreatmentDetails { get { return (ViewState["OriginTreatmentDetails"] != null) ? ViewState["OriginTreatmentDetails"].ToString() : ""; } set { ViewState["OriginTreatmentDetails"] = value; } }

    private string _RefCustomer { get { return (ViewState["RefCustomer"] != null) ? ViewState["RefCustomer"].ToString() : ""; } set { ViewState["RefCustomer"] = value; } }
    private string _AccountNumber { get { return (ViewState["AccountNumber"] != null) ? ViewState["AccountNumber"].ToString() : ""; } set { ViewState["AccountNumber"] = value; } }
    private string _BankStatus { get { return (ViewState["BankStatus"] != null) ? ViewState["BankStatus"].ToString() : ""; } set { ViewState["BankStatus"] = value; } }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.Params["prev"] != null)
            {
               _Prev = Request.Params["prev"].ToString();
            }

            CheckRights();
            BindAvisCSDetails();

            if (Session["AvisCSSaved"] != null && Session["AvisCSSaved"].ToString() == "1")
            {
                Session.Remove("AvisCSSaved");
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Avis traité', \"<span style='color:green'>Le traitement de l'avis est terminé.</span>\");", true);
            }
        }
    }

    protected void CheckRights()
    {
        string sXmlOut = tools.GetRights(authentication.GetCurrent().sToken, "SC_GarnishmentDetails");

        List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Page", "RC");

        if (listRC.Count == 0 || listRC[0] != "0")
        {
            Response.Redirect("Default.aspx", true);
        }
        else
        {
            List<string> listAction = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/Page/Action");

            for (int i = 0; i < listAction.Count; i++)
            {
                List<string> listTag = CommonMethod.GetAttributeValues(listAction[i], "Action", "TagAction");
                List<string> listActionAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ActionAllowed");
                List<string> listViewAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ViewAllowed");

                if (listTag.Count > 0)
                {
                    switch (listTag[0])
                    {
                        case "GarnishmentRejected":
                            if (listActionAllowed.Count > 0 && listActionAllowed[0] == "1")
                                panelReject.Visible = true;
                            break;
                        case "GarnishmentRejectedFinal":
                            if (listActionAllowed.Count > 0 && listActionAllowed[0] == "1")
                                panelRejectFinal.Visible = true;
                            break;
                        case "GarnishmentCancelRejection":
                            if (listActionAllowed.Count > 0 && listActionAllowed[0] == "1")
                                panelCancelRejection.Visible = true;
                            break;
                        case "GarnishmentRetreat":
                            panelRetreat.Visible = true;
                            break;
                        case "GarnishmentModifyTreatment":
                            if (listActionAllowed.Count > 0 && listActionAllowed[0] == "1")
                                _Editable = true;
                            break;
                    }
                }
            }
        }
    }

    protected void BindAvisCSDetails()
    {
        if (Request.Params["ref"] != null && !String.IsNullOrWhiteSpace(Request.Params["ref"].ToString()))
        {
            _RefAvisCS = Request.Params["ref"].ToString();
            string sXmlOut = AvisCS.GetAvisCSDetails(_RefAvisCS, authentication.GetCurrent().sToken);

            //sXmlOut = "<ALL_XML_OUT><AvisCS BOUserReserved=\"David BOULY\" SBI=\"524.68\" Amount=\"21802.62\" IBAN=\"FR693000100286364C000000015\" AccountNumber=\"00049720001\" AccountFirstName=\"GUILLAUME\" AccountLastName=\"NORMAND\" Date=\"09/06/2016 19:28:08\" ExternalRef=\"1617078020124 075013\" DocType=\"ATD\" Ref=\"7\"><Documents><Document FileIndex=\"0\" Filename=\"Test_David_ATD.pdf\"/><Document FileIndex=\"1\" Filename=\"1_Test_David_ATD.JPG\"/><Document FileIndex=\"2\" Filename=\"2_Test_David_ATD.JPG\"/></Documents></AvisCS></ALL_XML_OUT>"; // TEST
            //sXmlOut = "<ALL_XML_OUT><AvisCS BOUserReserved=\"David BOULY\" SBI=\"524.68\" Amount=\"21802.62\" IBAN=\"FR693000100286364C000000015\" AccountNumber=\"00049720001\" AccountFirstName=\"GUILLAUME\" AccountLastName=\"NORMAND\" Date=\"09/06/2016 19:28:08\" ExternalRef=\"1617078020124 075013\" DocType=\"AO\" Ref=\"7\"><Documents><Document FileIndex=\"0\" Filename=\"Test_David_AO.pdf\"/><Document FileIndex=\"1\" Filename=\"1_Test_David_AO.JPG\"/><Document FileIndex=\"2\" Filename=\"2_Test_David_AO.JPG\"/></Documents></AvisCS></ALL_XML_OUT>"; // TEST
            //sXmlOut = "<ALL_XML_OUT><AvisCS BOUserReserved=\"David BOULY\" SBI=\"524.68\" Amount=\"21802.62\" IBAN=\"FR693000100286364C000000015\" AccountNumber=\"00049720001\" AccountFirstName=\"GUILLAUME\" AccountLastName=\"NORMAND\" Date=\"09/06/2016 19:28:08\" ExternalRef=\"1617078020124 075013\" DocType=\"OA1\" Ref=\"7\"><Documents><Document FileIndex=\"0\" Filename=\"Test_David_OA1.pdf\"/><Document FileIndex=\"1\" Filename=\"1_Test_David_OA1.JPG\"/><Document FileIndex=\"2\" Filename=\"2_Test_David_OA1.JPG\"/></Documents></AvisCS></ALL_XML_OUT>"; // TEST
            //sXmlOut = "<ALL_XML_OUT><AvisCS BOUserReserved=\"David BOULY\" SBI=\"524.68\" Amount=\"21802.62\" IBAN=\"FR693000100286364C000000015\" AccountNumber=\"00049720001\" AccountFirstName=\"GUILLAUME\" AccountLastName=\"NORMAND\" Date=\"09/06/2016 19:28:08\" ExternalRef=\"1617078020124 075013\" DocType=\"OA2\" Ref=\"7\"><Documents><Document FileIndex=\"0\" Filename=\"Test_David_OA2.pdf\"/><Document FileIndex=\"1\" Filename=\"1_Test_David_OA2.JPG\"/><Document FileIndex=\"2\" Filename=\"2_Test_David_OA2.JPG\"/></Documents></AvisCS></ALL_XML_OUT>"; // TEST
/*            sXmlOut = "<ALL_XML_OUT><AvisCS BOUserReserved=\"David BOULY\" AccountFirstName=\"GUILLAUME\" AccountLastName=\"NORMAND\" Date=\"09/06/2016 19:28:08\" AccountNumber=\"00049720001\" "
                + "DocType=\"CSR\" DocNumber=\"1617078020124 075013\" "
                + "GestionnaireBDF=\"GestionnaireBDF\" "
                + "GestionnaireBDFTel=\"06xxxxxx\" "
                + "GestionnaireBDFMail=\"romain.vigan@compte-nickel.fr\" "
                + "DateCourrier=\"04/09/1976\" "
                + "DateRecevabilite=\"05/09/2018\" "
                + "RefCustomer=\"31\" "
                + "ADD_Street=\"18 avenue Winston Churchil\" "
                + "ADD_Zip=\"94140\" "
                + "ADD_City=\"Charenton le pont\" "
                + "AccountBirthDate=\"04/09/2018\" "
                + "Ref=\"7\"><Documents><Document FileIndex=\"0\" Filename=\"testAvisCS.pdf\"/><Document FileIndex=\"1\" Filename=\"1_testAvisCS.JPG\"/><Document FileIndex=\"2\" Filename=\"2_testAvisCS.JPG\"/></Documents></AvisCS></ALL_XML_OUT>"; // TEST
*/
            if (sXmlOut.Length > 0)
            {
                List<string> listDocType = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/AvisCS", "DocType");
                List<string> listDocNumber = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/AvisCS", "DocNumber");
                List<string> listGestionnaireBDF = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/AvisCS", "GestionnaireBDF");
                List<string> listGestionnaireBDFTel = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/AvisCS", "GestionnaireBDFTel");
                List<string> listGestionnaireBDFMail = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/AvisCS", "GestionnaireBDFMail");
                List<string> listDateCourrier = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/AvisCS", "DateCourrier");
                List<string> listDateRecevabilite = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/AvisCS", "DateRecevabilite");
                List<string> listRefCustomer = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/AvisCS", "RefCustomer");
                List<string> listAccountNumber = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/AvisCS", "AccountNumber");
                List<string> listAddress_Street = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/AvisCS", "ADD_Street");
                List<string> listAddress_Zip = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/AvisCS", "ADD_Zip");
                List<string> listAddress_City = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/AvisCS", "ADD_City");
                List<string> listDocuments = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/AvisCS/Documents/Document");

                List<string> listStatus = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/AvisCS", "StatusTAG");
                List<string> listDate = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/AvisCS", "Date");
                List<string> listBOUserCertified = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/AvisCS", "BOUserCertified");
                List<string> listRejectedBy = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/AvisCS", "RejectedBy");
                List<string> listRejectedFinalBy = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/AvisCS", "RejectedFinalBy");
                List<string> listRejectedReason = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/AvisCS", "RejectedReason");

                List<string> listIsRevival = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/AvisCS", "IsRevival");
                List<string> listIsAlreadyTreated = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/AvisCS", "IsAlreadyTreated");
                List<string> listOriginTreatmentDate = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/AvisCS", "OriginTreatmentDate");
                List<string> listOriginTreatmentDetails = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/AvisCS", "OriginTreatmentDetails");


                // 
                txtFileNumber.Text = listDocNumber[0];
                txtGestionnaireBDF.Text = listGestionnaireBDF[0];
                txtGestionnaireBDFTel.Text = listGestionnaireBDFTel[0];
                txtGestionnaireBDFMail.Text = listGestionnaireBDFMail[0];
                txtDateCourrier.Text = listDateCourrier[0];
                txtDateRecevabilite.Text = listDateRecevabilite[0];

                _AccountNumber = (listAccountNumber.Count > 0) ? listAccountNumber[0] : "";
                txtAccountNumber.Text = _AccountNumber;
                txtAddress_Street.Text = (listAddress_Street.Count > 0) ? listAddress_Street[0] : "";
                txtAddress_Zipcode.Text = (listAddress_Zip.Count > 0) ? listAddress_Zip[0] : "";
                txtAddress_City.Text = (listAddress_City.Count > 0) ? listAddress_City[0] : "";

                lblDate.Text = listDate[0];
                _Certified = (listStatus.Count > 0 && (listStatus[0] == "Certified" || listStatus[0] == "Treated")) ? true : false;
                if (_Certified)
                    hdnStatus.Value = "treated";

                /*** TEST ***-/
                _Certified = false;
                listStatus[0] = "";
                hdnStatus.Value = "";
                /*** FIN TEST ***/
                /*
                if (listStatus[0] != "Treated" && listStatus[0] != "Certified" && listStatus[0] != "TreatedError" && listStatus[0] != "RejectedFinal")
                {
                    panelRetreat.Visible = false;
                }*/

                if (listRefCustomer.Count > 0 && listRefCustomer[0].Length > 0)
                {
                    List<string> listLastName = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/AvisCS", "AccountLastName");
                    List<string> listFirstName = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/AvisCS", "AccountFirstName");
                    List<string> listBirthDate = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/AvisCS", "AccountBirthDate");

                    UpdateClientPanel(listRefCustomer[0], listLastName[0], listFirstName[0], listBirthDate[0]);

                    if (_Prev == "fc")
                    {
                        btnPrev.Text = "Fiche client";
                        btnPrev.PostBackUrl = "ClientDetails.aspx?ref=" + listRefCustomer[0];
                    }
                }
                //else panelClientDetails.Visible = false;

                // DOCS
                //if (listDocuments.Count == 3)
                {
                    _DocType = (listDocType.Count > 0 && listDocType[0].Length > 0) ? listDocType[0] : "";
                    ListItem item = ddlTypeCourrier.Items.FindByValue(_DocType);
                    if (item != null)
                        item.Selected = true;
                    else ddlTypeCourrier.Items[0].Selected = true;

                    for (int i = 0; i < listDocuments.Count; i++)
                    {
                        List<string> listIndex = CommonMethod.GetAttributeValues(listDocuments[i], "Document", "FileIndex");
                        List<string> listFileName = CommonMethod.GetAttributeValues(listDocuments[i], "Document", "Filename");

                        if (listIndex.Count > 0)
                            switch (listIndex[0])
                            {
                                case "0":
                                    _OriginalFile = listFileName[0];
                                    break;
                                case "1":
                                    _Page1 = listFileName[0].ToLower();
                                    break;
                                case "2":
                                    _Page2 = listFileName[0].ToLower();
                                    break;
                            }
                    }
                }
                //else ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Erreur', \"Nombre de document insuffisant.\");", true);

                /* test rvn 17/10/2018
                if (String.IsNullOrWhiteSpace(_Page1))
                    _Page1 = _OriginalFile;
                if (String.IsNullOrWhiteSpace(_Page2))
                    _Page2 = _OriginalFile;
                // fin test */
                _Page2 = _Page1;
                string sFileName = "";
                if (!String.IsNullOrWhiteSpace(_Page2))
                {
                    sFileName = _Page2.Substring(0, _Page2.LastIndexOf('.')).Trim().Replace(' ', '_');
                    _ResponseFile = sFileName + ".pdf";
                }
                
                
                TransferDocFromGoogle();

                if (!listStatus[0].ToUpper().Contains("REJECTED"))
                    UpdateZoomInfos(_Page1, _DocType, 1, true);

                if (_Certified)
                {
                    panelReject.Visible = false;
                //    panelClientSearch.Visible = false;
                    panelFormBG.Visible = false;
                    panelFormButton.Visible = false;
                    ddlTypeCourrier.Enabled = false;
                    txtAddress_Street.Enabled = false;
                    txtAddress_Zipcode.Enabled = false;
                    txtAddress_City.Enabled = false;
                    panelAllForm.CssClass = "active";
                    
                    panelPDFEditor.Visible = false;
                    panelPDFResponse.Visible = true;
                    //panelResponse.Visible = true;
                    string sAvisCSResponsePath = (ConfigurationManager.AppSettings["AvisCSResponsePath"] != null) ? ConfigurationManager.AppSettings["AvisCSResponsePath"].ToString() : "";

                    if (File.Exists(System.Web.HttpContext.Current.Server.MapPath("~" + sAvisCSResponsePath + sFileName + ".pdf")))
                        frameResponse.Attributes["src"] = sAvisCSResponsePath + sFileName + ".pdf#zoom=150";
                    else ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Erreur', \"Document réponse introuvable.\");", true);

                    panelStatus.Visible = true;
                    lblStatus.Text = "Certifié par " + listBOUserCertified[0];

                    if (_Editable)
                    {
                        ddlTypeCourrier.Enabled = true;
                        panelSaveModificationPostTreatment.Visible = true;
                    }
                }
                else if (listStatus[0].ToUpper().Contains("REJECTED"))
                {
                    panelForm.Visible = false;
                    panelReject.Visible = false;
                    panelRejectDetails.Visible = true;

                    lblRejectedBy.Text = (listRejectedBy.Count > 0) ? listRejectedBy[0] : "";
                    lblRejectedReason.Text = (listRejectedReason.Count > 0) ? listRejectedReason[0] : "";
                    string sAvisCSOriginalPath = (ConfigurationManager.AppSettings["AvisCSOriginalPath"] != null) ? ConfigurationManager.AppSettings["AvisCSOriginalPath"].ToString() : "";
                    if (File.Exists(System.Web.HttpContext.Current.Server.MapPath("~" + sAvisCSOriginalPath + _OriginalFile)))
                        frameOriginalFile.Attributes["src"] = sAvisCSOriginalPath + _OriginalFile;
                    else ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Erreur', \"Document original introuvable.<br/>" + System.Web.HttpContext.Current.Server.MapPath("~" + sAvisCSOriginalPath + _OriginalFile).Replace("\\", "/") + "\");", true);

                    if (listStatus[0].ToUpper() == "REJECTEDFINAL")
                    {
                        panelCancelRejection.Visible = false;
                        panelRejectFinal.Visible = false;
                        panelRejectFinalDetails.Visible = true;
                        lblRejectedFinalBy.Text = (listRejectedFinalBy.Count > 0) ? listRejectedFinalBy[0] : "";
                    }
                }
            }
            else Response.Redirect("GarnishmentSearch.aspx", true);
        }
        else Response.Redirect("GarnishmentSearch.aspx", true);
    }

    protected void UpdateClientPanel(string sRefCustomer, string sLastName, string sFirstName, string sBirthDate)
    {
        lblName.Text = sLastName + " " + sFirstName;
        lblBirthDate.Text = sBirthDate;
        panelClientInfos.Visible = true;
        _RefCustomer = sRefCustomer;
        //btnShowClient.Attributes.Add("onclick", "window.open('ClientDetails.aspx?ref=" + sRefCustomer + "');");
        //panelClientDetails.Visible = true;
    }

    protected void TransferDocFromGoogle()
    {
        string sAvisCSOriginalPath = (ConfigurationManager.AppSettings["AvisCSOriginalPath"] != null) ? ConfigurationManager.AppSettings["AvisCSOriginalPath"].ToString() : "";
        string sAvisCSResponsePath = (ConfigurationManager.AppSettings["AvisCSResponsePath"] != null) ? ConfigurationManager.AppSettings["AvisCSResponsePath"].ToString() : "";
        if ((!File.Exists(System.Web.HttpContext.Current.Server.MapPath("~" + sAvisCSOriginalPath + _OriginalFile))
            || (!File.Exists(System.Web.HttpContext.Current.Server.MapPath("~" + sAvisCSResponsePath + _ResponseFile)) && _Certified))
            && !_FileRetrieved)
        {
            AvisCS.GetAvisCSFileFromGoogle(_RefCustomer);
            _FileRetrieved = true;
            BindAvisCSDetails();
        }
    }

    protected void btnSearchClient_Click(object sender, EventArgs e)
    {
        if (txtName.Text.Length > 0)
        {
            string sToken = authentication.GetCurrent(false).sToken;
            DataTable dt = Client.GetClientList("", "", "", "", "", txtName.Text, sToken);

            if (dt.Rows.Count > 0)
            {
                if (dt.Rows.Count == 1)
                {
                    txtAccountNumber.Text = dt.Rows[0]["AccountNumber"].ToString();
                    UpdateClientPanel(dt.Rows[0]["RefCustomer"].ToString(), dt.Rows[0]["LastName"].ToString(), dt.Rows[0]["FirstName"].ToString(), dt.Rows[0]["BirthDate"].ToString());
                }
                else
                {
                    rptSearchResult.DataSource = dt;
                    rptSearchResult.DataBind();
                    UpdateClientPanel("", "", "", "");
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "ShowSearchResult();", true);
                }
            }
            else ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Aucun client trouvé', \"Votre recherche ne retourne aucun résultat.<br/>Veuillez vérifier la saisie et réessayer.\", null, null);", true);

            txtName.Text = "";
        }
        else ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Erreur', \"Recherche : champ requis\", null, null);", true);

        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "InitBlur();", true);
    }

    protected bool SearchCustomer(string sAccountNumber, string sToken, out string sRefCustomer, out string sLastName, out string sFirstName, out string sBirthDate)
    {
        bool bOk = false;
        sFirstName = "";
        sLastName = "";
        sRefCustomer = "";
        sBirthDate = "";

        DataTable dt = Client.GetClientList(sAccountNumber, sToken);

        if (dt.Rows.Count > 0)
        {
            bOk = true;
            sLastName = dt.Rows[0]["LastName"].ToString();
            sFirstName = dt.Rows[0]["FirstName"].ToString();
            sRefCustomer = dt.Rows[0]["RefCustomer"].ToString();
            sBirthDate = dt.Rows[0]["BirthDate"].ToString();
        }

        return bOk;
    }

    protected void btnSaveForm_Click(object sender, EventArgs e)
    {
        Button btn = (Button)sender;

        if (btn != null)
        {
            int iStep = int.Parse(btn.CommandArgument);
            string sErrorMessage = "";
            string sToken = authentication.GetCurrent().sToken;
            decimal dAccountBalance = -1;
            string sFormattedAccountBalance = "";

            if (CheckForm(iStep, out sErrorMessage))
            {
                if (SaveInformation(iStep))
                {
                    switch (iStep)
                    {
                        case 5:
                            string sRefCustomer = "", sLastName = "", sFirstName = "", sBirthDate = "";
                            if (SearchCustomer(txtAccountNumber.Text, sToken, out sRefCustomer, out sLastName, out sFirstName, out sBirthDate))
                            {
                                _AccountNumber = txtAccountNumber.Text;
                                UpdateClientPanel(sRefCustomer, sLastName, sFirstName, sBirthDate);

                                if (_RefCustomer.Length > 0)
                                {
                                    string sXMLCustomerInfos = tools.GetXMLCustomerInformations(int.Parse(sRefCustomer));
                                    List<string> listBankStatus = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "BankStatus");

                                    if (listBankStatus.Count > 0)
                                    {
                                        _BankStatus = listBankStatus[0];
                                        //1:Bloque debit
                                        //2:Bloque credit
                                        //3:Bloque debit/credit
                                        //4:Cloture
                                    }

                                    //_BankStatus = "4"; // TEST
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Erreur', \"Client inconnu.\");", true);
                                iStep = iStep - 1;
                            }
                            break;
                        case 6:
                            InitResponsePanel(0, 0);
                            iStep = 5;
                            break;
                    }


                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Erreur', \"" + sErrorMessage + "\");", true);
                iStep = iStep - 1;
            }
            iStep = iStep + 1;
            btnSaveForm.CommandArgument = iStep.ToString();
            Panel panel = (Panel)panelForm.FindControl("panelForm" + iStep);
            if (panel != null)
                panel.CssClass = "row active";

            UpdateZoomInfos(_Page1, _DocType, iStep, false);
        }
    }

    protected bool CheckForm(int iStep, out string sErrorMessage)
    {
        StringBuilder sb = new StringBuilder();

        if ((iStep == 0 || iStep == 2) && String.IsNullOrWhiteSpace(txtFileNumber.Text))
            sb.AppendLine("Numéro de dossier : champ requis");

        if (iStep == 0 || iStep == 3)
        {
            if (String.IsNullOrWhiteSpace(txtGestionnaireBDF.Text))
                sb.AppendLine("Nom du Gestionnaire BDF : champ requis");

            if (String.IsNullOrWhiteSpace(txtGestionnaireBDFTel.Text))
                sb.AppendLine("Téléphone du Gestionnaire BDF : champ requis");

            if (!String.IsNullOrWhiteSpace(txtGestionnaireBDFMail.Text) && !tools.IsEmailValid(txtGestionnaireBDFMail.Text.Trim()))
                sb.AppendLine("Courriel du Gestionnaire BDF : format invalide");
        }

        if ((iStep == 0 || iStep == 4) && !tools.IsDateValid(txtDateCourrier.Text.Trim()))
            sb.AppendLine("date du courrier : format invalide");

        if ((iStep == 0 || iStep == 5) && String.IsNullOrWhiteSpace(txtAccountNumber.Text))
            sb.AppendLine("Numéro compte : champ requis");

        if ((iStep == 0 || iStep == 6) && !tools.IsDateValid(txtDateRecevabilite.Text.Trim()))
            sb.AppendLine("date de recevabilité : format invalide");

        sErrorMessage = sb.ToString().Replace("\r\n", "<br/>");
        
        return (sb.ToString().Length == 0);
    }

    protected bool SaveInformation(int iStep)
    {
        XElement xelem = new XElement("AvisCS");
        xelem.Add(new XAttribute("Ref", _RefAvisCS),
            new XAttribute("CashierToken", authentication.GetCurrent().sToken));

        if (iStep == 0 || iStep == 1)
            xelem.Add(new XAttribute("DocType", ddlTypeCourrier.SelectedValue));
        if (iStep == 0 || iStep == 2)
            xelem.Add(new XAttribute("DocNumber", txtFileNumber.Text.Trim()));
        if (iStep == 0 || iStep == 3)
        {
            //if (txtGestionnaireBDF.Text.Trim().Length > 0)
                xelem.Add(new XAttribute("GestName", txtGestionnaireBDF.Text.Trim()));

            //if (txtGestionnaireBDFMail.Text.Trim().Length > 0)
                xelem.Add(new XAttribute("GestMail", txtGestionnaireBDFMail.Text.Trim()));

            //if (txtGestionnaireBDFTel.Text.Trim().Length > 0)
                xelem.Add(new XAttribute("GestTel", txtGestionnaireBDFTel.Text.Trim()));
        }
        if (iStep == 0 || iStep == 4)
        {
            string sDateCourrier = txtDateCourrier.Text.Trim();
            string sMonth = "Janvier";
            switch(sDateCourrier.Substring(3, 2))
            {
                case "01":
                    sMonth = "Janvier";
                    break;
                case "02":
                    sMonth = "Fevrier";
                    break;
                case "03":
                    sMonth = "Mars";
                    break;
                case "04":
                    sMonth = "Avril";
                    break;
                case "05":
                    sMonth = "Mai";
                    break;
                case "06":
                    sMonth = "Juin";
                    break;
                case "07":
                    sMonth = "Juillet";
                    break;
                case "08":
                    sMonth = "Aout";
                    break;
                case "09":
                    sMonth = "Septembre";
                    break;
                case "10":
                    sMonth = "Octobre";
                    break;
                case "11":
                    sMonth = "Novembre";
                    break;
                case "12":
                    sMonth = "Decembre";
                    break;
            }

            xelem.Add(new XAttribute("DocDate", sDateCourrier.Substring(0,2) + " " + sMonth + " " + sDateCourrier.Substring(6, 4)));
        }

        if (iStep == 0 || iStep == 5)
        {
            xelem.Add(new XAttribute("CustBankAccount", txtAccountNumber.Text.Trim()));

            if (txtAddress_Street.Text.Trim().Length > 0)
                xelem.Add(new XAttribute("ADD_Street", txtAddress_Street.Text.Trim()));

            if (txtAddress_Zipcode.Text.Trim().Length > 0)
                xelem.Add(new XAttribute("ADD_Zip", txtAddress_Zipcode.Text.Trim()));

            if (txtAddress_City.Text.Trim().Length > 0)
                xelem.Add(new XAttribute("ADD_City", txtAddress_City.Text.Trim()));
        }

        if (iStep == 0 || iStep == 6)
            xelem.Add(new XAttribute("DateRecevabilite", txtDateRecevabilite.Text.Trim()));

        return AvisCS.SetAvisCSDetails(new XElement("ALL_XML_IN", xelem).ToString());
    }

    protected void InitResponsePanel(decimal dTransferAmount, decimal dAccountBalance)
    {
        //_Page2 = "testAvisCS.jpg";
        string sAvisCSPath = (ConfigurationManager.AppSettings["AvisCSPath"] != null) ? ConfigurationManager.AppSettings["AvisCSPath"].ToString() : "";
        string sAvisCSOriginalPath = (ConfigurationManager.AppSettings["AvisCSOriginalPath"] != null) ? ConfigurationManager.AppSettings["AvisCSOriginalPath"].ToString() : "";
        bool isFileResponsePresent = File.Exists(System.Web.HttpContext.Current.Server.MapPath("~" + sAvisCSPath + _Page2));
        bool isFileRequestPresent = File.Exists(System.Web.HttpContext.Current.Server.MapPath("~" + sAvisCSPath + _Page1));

        if (isFileResponsePresent || (isFileRequestPresent && _IsRevival))
        {
            /*
            panelFormButton.Visible = false;
            panelResponse.Visible = true;
            upResponse.Update();

            panelReject.Visible = false;
            upRejectBtn.Update();

            tools.AssetInfos assetInfos = new tools.AssetInfos();
            assetInfos.template = sAvisCSPath + _Page2;
            if (_IsRevival) { assetInfos.template = sAvisCSPath + _Page1; }

            List<tools.Asset> listAsset = new List<tools.Asset>();
            tools.Asset assetCity = new tools.Asset("text", "Charenton-le-Pont", 0, 0, 0, "");
            tools.Asset assetDate = new tools.Asset("text", DateTime.Now.ToString("dd/MM/yyyy"), 0, 0, 0, "");
            //tools.Asset assetStamp = new tools.Asset("text", "FINANCIERE DES PAIEMENTS ELECTRONIQUES\r\n18 avenue Winston Churchill\r\n94220 CHARENTON LE PONT\r\nTél. 01 75 43 50 00 - Fax : 09 78 16 78 38\r\nSiren 753 886 092 - RCS CRETEIL\r\nEtablissement de Paiement agréé N°16598R", 0, 0, 0, "#AAAAFF");
            tools.Asset assetStamp = new tools.Asset("img", "data:image/gif;base64," + tools.ImageFileToBase64(System.Web.HttpContext.Current.Server.MapPath("~/Styles/Img/FPE_tampon.png")), 0, 0, 0, "");
            tools.Asset assetSign = new tools.Asset("img", "data:image/gif;base64," + tools.ImageFileToBase64(System.Web.HttpContext.Current.Server.MapPath("~/Styles/Img/signature.png")), 0, 0, 0, "");

            if (dTransferAmount == 0)
            {
                tools.Asset assetSolde = _BankStatus != "4"
                    ? new tools.Asset("text", "Solde de " + dAccountBalance.ToString(new CultureInfo("fr-FR")).Replace('.', ',') + " &euro; inférieur au SBI", 0, 0, 0, "")
                    : new tools.Asset("text", "Compte clôturé", 0, 0, 0, "");
                tools.Asset assetX = new tools.Asset("text", "X", 0, 0, 0, "");
                switch (_DocType)
                {
                    case "SDT":
                    case "CSR":
                        assetCity.top = 95M;
                        assetCity.left = 10M;
                        assetCity.deg = 0;
                        listAsset.Add(assetCity);
                        assetDate.top = 95M;
                        assetDate.left = 40M;
                        listAsset.Add(assetDate);
                        assetStamp.top = 15M;
                        assetStamp.left = 5M;
                        listAsset.Add(assetStamp);
                        assetSign.top = 89.02M;
                        assetSign.left = 57.34M;
                        assetSign.deg = 12;
                        listAsset.Add(assetSign);
                        break;
                    
                }
            }
            else
            {
                switch (_DocType)
                {
                    case "SDT":
                    case "CSR":
                        assetCity.top = 68.55M;
                        assetCity.left = 56.89M;
                        listAsset.Add(assetCity);
                        assetDate.top = 68.55M;
                        assetDate.left = 75.55M;
                        listAsset.Add(assetDate);
                        assetStamp.top = 72.32M;
                        assetStamp.left = 56.22M;
                        listAsset.Add(assetStamp);
                        assetSign.top = 70.44M;
                        assetSign.left = 58.67M;
                        assetSign.deg = 12;
                        listAsset.Add(assetSign);
                        break;
                    
                }
            }

            assetInfos.isRevival = false;
            assetInfos.isAlreadyTreated = false;
            if (_IsRevival)
            {
                assetInfos.template = sAvisCSPath + _Page1;
                assetInfos.isRevival = true;

                if (_IsAlreadyTreated)
                {
                    assetInfos.isAlreadyTreated = true;
                    tools.Asset assetOriginTreatment = new tools.Asset("text", "Déjà traité le " + _OriginTreatmentDate + " :<br/>" + _OriginTreatmentDetails, 60.16M, 53.33M, 0, "");
                    listAsset.Add(assetOriginTreatment);
                }
            }

            assetInfos.assets = listAsset;

            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "InitAssets(" + JsonConvert.SerializeObject(assetInfos) + ");", true);
            */
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "ShowDialogCheckResponse('" + sAvisCSOriginalPath + _OriginalFile + "');", true);
        }
        else ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Erreur', \"Document détouré introuvable.<br/>Impossible de générer le document de réponse.\");", true);
    }

    protected void btnSaveModificationPostTreatment_Click(object sender, EventArgs e)
    {

    }

    protected void btnGenerateResponse_Click(object sender, EventArgs e)
    {
        string sFileName = "";
        if (_IsRevival) { sFileName = _Page1.Substring(0, _Page1.LastIndexOf('.')).Trim().Replace(" ", "_"); }
        else { sFileName = _Page2.Substring(0, _Page2.LastIndexOf('.')).Trim().Replace(" ", "_"); }

        //tools.LogToFile(sFileName, "btnGenerateResponse_Click|sFileName");

        if (!string.IsNullOrWhiteSpace(sFileName) && GenerateResponseFile(sFileName))
        {
            string sAvisCSResponsePath = (ConfigurationManager.AppSettings["AvisCSResponsePath"] != null) ? ConfigurationManager.AppSettings["AvisCSResponsePath"].ToString() : "";
            if (File.Exists(System.Web.HttpContext.Current.Server.MapPath("~" + sAvisCSResponsePath + sFileName + ".pdf")))
            {
                //tools.LogToFile(sAvisCSResponsePath + sFileName + ".pdf", "btnGenerateResponse_Click|File exists");
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "ShowDialogCheckResponse('" + sAvisCSResponsePath + sFileName + ".pdf" + "');", true);
            }
            else
            {
                tools.LogToFile(sAvisCSResponsePath + sFileName + ".pdf", "btnGenerateResponse_Click|File not exists");
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Erreur', \"Document introuvable.\");", true);
            }
        }
        else
        {
            tools.LogToFile("Generation du fichier KO", "btnGenerateResponse_Click|GenerateResponseFile KO");
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Erreur', \"Une erreur est survenue lors de la génération du fichier.<br/>" + sFileName + ".<br/>Veuillez réessayer.\");", true);
        }
        
    }

    protected bool GenerateResponseFile(string sFileName)
    {
        return true;
        bool bOk = false;
        string sJSONData = hfAssets.Value;
        string sXmlOut = "";

        tools.AssetInfos infos = JsonConvert.DeserializeObject<tools.AssetInfos>(sJSONData);
        XElement xeAssets = new XElement("ASSETS");

        for (int i = 0; i < infos.assets.Count; i++)
        {
            xeAssets.Add(new XElement("ASSET",
                            new XAttribute("type", infos.assets[i].type),
                            new XAttribute("content", infos.assets[i].content),
                            new XAttribute("top", infos.assets[i].top),
                            new XAttribute("left", infos.assets[i].left),
                            new XAttribute("deg", infos.assets[i].deg)
                            ));
        }

        string sTemplate = (_IsRevival) ? _Page1 : _Page2;

        string sXmlIn = new XElement("ALL_XML_IN",
                            new XElement("GARNISHMENT",
                                new XElement("TEMPLATE", sTemplate),
                                new XElement("FILENAME", sFileName),
                                xeAssets)).ToString();

        try
        {
            WS_PDFCreator.PDFCreatorClient ws = new WS_PDFCreator.PDFCreatorClient();
            sXmlOut = ws.CreateGarnishmentResponse(sXmlIn);
            //string sXmlOut = "<ALL_XML_OUT><GARNISHMENT RC=\"0\" /></ALL_XML_OUT>"; // TEST

            List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/GARNISHMENT", "RC");
            if (listRC.Count > 0 && listRC[0] == "0")
                bOk = true;
            else
            {
                tools.LogToFile(sXmlOut, "GenerateResponseFile|" + sFileName);
                tools.LogToFile("XMLIN: " + sXmlIn + "|XMLOUT : " + sXmlOut, "GenerateResponseFile|" + sFileName);
            }
        }
        catch(Exception ex)
        {
            tools.LogToFile(sXmlOut, "GenerateResponseFile|"+sFileName);
            tools.LogToFile(ex.Message, "GenerateResponseFile|" + sFileName);
            bOk = false;
        }

        return bOk;
    }
    protected void btnReject_Click(object sender, EventArgs e)
    {
        string sReason = (ddlRejectReason.SelectedValue.Length > 0) ? ddlRejectReason.SelectedValue : txtReason.Text;

        if (AvisCS.SetAvisCSStatus(_RefAvisCS, authentication.GetCurrent().sToken, "REJECTED", sReason))
        {
            Response.Redirect("GarnishmentSearch.aspx?status=Rejected");
        }
        else ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Erreur', \"Une erreur est survenue lors de l'enregistrement.<br/>Veuillez réessayer.\"); InitBlur();", true);
    }

    protected void btnTerminate_Click(object sender, EventArgs e)
    {
        XElement xelem = new XElement("AvisCS");
        xelem.Add(new XAttribute("Ref", _RefAvisCS),
                new XAttribute("CashierToken", authentication.GetCurrent().sToken));

        /*if (rblResponseMethod.SelectedValue == "email")
        {
            xelem.Add(new XAttribute("MailBDF", txtEmail.Text));
        }*/

        if (AvisCS.SetAvisCSDetails(new XElement("ALL_XML_IN", xelem).ToString()))
        {
            if (AvisCS.SetAvisCSStatus(_RefAvisCS, authentication.GetCurrent().sToken, "CERTIFIED", ""))
            {
                Session["AvisCSSaved"] = "1";
                Response.Redirect("AvisCSDetails.aspx?ref=" + _RefAvisCS);
            }
            else ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Erreur', \"Une erreur est survenue lors de l'enregistrement des informations.<br/>Veuillez réessayer.\");", true);
        }
        else ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Erreur', \"Une erreur est survenue lors de l'enregistrement des informations.<br/>Veuillez réessayer.\");", true);
    }

    protected void btnRejectFinal_Click(object sender, EventArgs e)
    {
        if (AvisCS.SetAvisCSStatus(_RefAvisCS, authentication.GetCurrent().sToken, "REJECTEDFINAL", ""))
        {
            Response.Redirect("AvisCSDetails.aspx?ref=" + _RefAvisCS);
        }
        else
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Erreur', \"Une erreur est survenue lors de l'enregistrement.<br/>Veuillez réessayer.\"); InitBlur();", true);
    }

    protected void btnCancelRejection_Click(object sender, EventArgs e)
    {

    }

    protected void btnRetreat_Click(object sender, EventArgs e)
    {
        if (AvisCS.SetAvisCSStatus(_RefAvisCS, authentication.GetCurrent().sToken, "TORETREAT", ""))
        {
            Response.Redirect("GarnishmentSearch.aspx");
        }
        else
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Erreur', \"Une erreur est survenue lors de l'enregistrement.<br/>Veuillez réessayer.\"); InitBlur();", true);
    }

    [Serializable]
    protected class ZoomInfos
    {
        public string template { get; set; }
        public List<Zoom> zooms { get; set; }
        public int zoomIndex { get; set; }
    }
    [Serializable]
    protected class Zoom
    {
        public int x { get; set; }
        public int y { get; set; }
        public int level { get; set; }

        public Zoom(int x, int y, int level)
        {
            this.x = x;
            this.y = y;
            this.level = level;
        }
    }
    protected void UpdateZoomInfos(string sFileName, string sDocType, int iStep, bool bAlert)
    {
        //UpdateZoomInfos(_Page1, _DocType, (iStep != 6) ? iStep - 1 : 0, false);
        string sAvisCSPath = (ConfigurationManager.AppSettings["AvisCSPath"] != null) ? ConfigurationManager.AppSettings["AvisCSPath"].ToString() : "";

        if (File.Exists(System.Web.HttpContext.Current.Server.MapPath("~" + sAvisCSPath + _Page1)))
        {
            ZoomInfos zoomInfos = new ZoomInfos();
            zoomInfos.template = sAvisCSPath + _Page1;
            List<Zoom> listZoom = new List<Zoom>();

            switch (sDocType)
            {   
                case "SDT":
                case "CSR":
                    listZoom.Add(new Zoom(0, 0, 0));
                    listZoom.Add(new Zoom(300, 500, 2));//numero de dossier
                    listZoom.Add(new Zoom(300, 500, 2));// gestionnaire BDF
                    listZoom.Add(new Zoom(300, 500, 2));// gestionnaire BDF tel
                    listZoom.Add(new Zoom(300, 500, 2));// gestionnaire BDF mail
                    listZoom.Add(new Zoom(700, 500, 4)); // date courrier
                    listZoom.Add(new Zoom(300, 750, 4)); // numero de compte
                    listZoom.Add(new Zoom(0, 0, 0)); // not used but needed
                    listZoom.Add(new Zoom(300, 800, 4)); // date recevabilité
                    break;
            }

            zoomInfos.zooms = listZoom;

            int iZoomIndex = iStep;
            if (iStep == 6)
                iZoomIndex = 9;
            // l'étape 3 inclus les zooms 3, 4 et 5 
            else if (iStep >= 4)
                iZoomIndex += 2;
            
            zoomInfos.zoomIndex = iZoomIndex;

            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "InitDocReader(" + JsonConvert.SerializeObject(zoomInfos) + ");", true);
        }
        else InitOriginalFrame(bAlert);
        /*
        if (!_Certified)
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "InitBlur();", true);*/
    }

    protected void InitOriginalFrame(bool bAlertError)
    {
        panelCroppedImg.Visible = false;
        string sAvisCSOriginalPath = (ConfigurationManager.AppSettings["AvisCSOriginalPath"] != null) ? ConfigurationManager.AppSettings["AvisCSOriginalPath"].ToString() : "";

        if (File.Exists(System.Web.HttpContext.Current.Server.MapPath("~" + sAvisCSOriginalPath + _OriginalFile)))
        {
            if (bAlertError)
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Erreur', \"Document détouré introuvable.<br/>Le document PDF original sera affiché à la place.\");", true);

            frameOriginalSmall.Attributes["src"] = sAvisCSOriginalPath + _OriginalFile;
            panelOriginalFile.Visible = true;
            upFile1.Update();
        }
        else ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Erreur', \"Document original introuvable.<br/>" + System.Web.HttpContext.Current.Server.MapPath("~" + sAvisCSOriginalPath + _OriginalFile).Replace("\\", "/") + "\");", true);

    }
}