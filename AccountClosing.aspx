﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="AccountClosing.aspx.cs" Inherits="AccountClosing" %>

<%@ Register Src="~/API/AmlComment.ascx" TagName="AmlComment" TagPrefix="asp" %>
<%@ Register Src="~/API/AccountClosingReason.ascx" TagName="AccountClosingReason" TagPrefix="asp" %>
<%@ Register Src="~/API/AccountClosingFee.ascx" TagName="AccountClosingFee" TagPrefix="asp" %>
<%@ Register Src="~/API/ReturnFunds.ascx" TagName="ReturnFunds" TagPrefix="asp" %>
<%@ Register Src="~/API/AmlSurveillance.ascx" TagName="AmlSurveillance" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">

    <link type="text/css" rel="Stylesheet" href="Styles/AccountClosingControls.css" />
    <link type="text/css" rel="Stylesheet" href="Styles/ReturnFunds.css" />
    <link type="text/css" rel="Stylesheet" href="Styles/DropdownListMultiSelect.css" />

    <script type="text/javascript">
        $(document).ready(function () {
            init();
        });

        function init() {
            $('.radioButtonList').buttonset();
            date_picker();
            //InitIBANinputs();
            //beneficiaryInit();
            //$('.money').maskMoney({ thousands: ' ', decimal: ',', allowZero: true, suffix: ' €' });
            //beneficiaryInit();
        }
    </script>
    <style type="text/css">
        h3 {
            margin: 0 0 5px 0;
        }

        h4 {
            font-size: 1em;
            margin: 0;
        }

        #ar-loading {
            background: url('Styles/img/loading2.gif') no-repeat center;
            background-size: 35px 27px;
            display: none;
            position: absolute;
            background-color: rgba(255, 255, 255, 0.5);
            z-index: 100000;
        }

        .cadreOrange {
            margin: auto;
            border: 1px solid #ff6a00;
            padding: 5px;
            margin: 10px 0;
        }

        .formField {
            padding:5px 0;
        }

        .main {
            min-height:unset;
        }

        .ui-button-text-only .ui-button-text {
            padding: .1em 1em;
        }

        .hidden {
            display:none;
        }

        .dual-choice .ui-buttonset {
            width:100%;
            border-spacing:0;
        }
        .dual-choice .ui-buttonset td {
            width:50%;
        }
        .dual-choice .ui-buttonset .ui-button {
            width:100%;
            text-transform:uppercase;
        }
    </style>
    <script type="text/javascript" src="Scripts/AccountClosing.js"></script>
    <script type="text/javascript" src="Scripts/beneficiary.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">

    <h2 style="color: #344b56;text-transform:uppercase;">
        Clôture de compte
        <asp:Label ID="lblAccountClosingType" runat="server"></asp:Label>
    </h2>

    <div>
        <div class="table" style="margin:10px 0 20px 0;width:100%;">
            <div class="row">
                <div class="cell" style="vertical-align:top;width:250px">
                    <div style="font-weight:bold; color:#f57527">Titulaire principal</div>
                    <div><asp:Label ID="lblClientName" runat="server"></asp:Label></div>
                </div>
                <div class="cell" style="vertical-align:top;padding-left:20px;width:110px">
                    <div style="font-weight:bold; color:#f57527">N&deg; Carte</div>
                    <div><asp:Label ID="lblClientCardNumber" runat="server"></asp:Label></div>
                </div>
                <div class="cell" style="padding-left:20px;vertical-align:top;">
                    <div style="font-weight:bold; color:#f57527">N&deg; Compte</div>
                    <div><asp:Label ID="lblClientAccountNumber" runat="server"></asp:Label></div>
                </div>
                <div class="cell" style="padding-left:20px;vertical-align:top;visibility:hidden">
                    <div style="font-weight:bold; color:#f57527">N&deg; Inscription</div>
                    <div><asp:Label ID="lblClientRegistrationNumber" runat="server"></asp:Label></div>
                </div>
                <div class="cell" style="text-align:right">
                    <input id="btnClientDetails" runat="server" type="button" class="MiniButton" value="accéder à la fiche du client" style="width:200px;position:relative;right:0;top:10px;" />
                </div>
            </div>
        </div>
    </div>

    <asp:UpdatePanel ID="upAccountClosing" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            
            <asp:Panel ID="panelAccountClosing" runat="server">
                <div>
                    <div class="label">
                        Raison
                    </div>
                    <asp:AccountClosingReason runat="server" ID="AccountClosingReason" />
                </div>
                
                <div class="dual-choice" style="margin-top: 10px;">
                    <asp:RadioButtonList ID="rblAccountClosingTime" runat="server" CssClass="radioButtonList" RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="rblAccountClosingTime_SelectedIndexChanged">
                        <asp:ListItem Text="Avec préavis" Value="later"></asp:ListItem>
                        <asp:ListItem Text="Immédiate" Value="now"></asp:ListItem>
                    </asp:RadioButtonList>
                </div>

                <asp:Panel ID="panelClosingTimeLater" runat="server" CssClass="hidden" style="padding-top:10px">
                    <div class="label">
                        Date fin préavis
                    </div>
                    <div>
                        <asp:TextBox ID="txtAccountClosingLaterDate" runat="server" CssClass="datePicker" minDate="0" maxDate="1y" Width="80"></asp:TextBox>
                    </div>
                    <div style="margin-top:10px">
                        <input type="checkbox" id="ckbCACF" runat="server" class="csscheckbox" />
                        <label for="<%= ckbCACF.ClientID %>" class="csscheckbox-label">CACF</label>
                    </div>
                    <asp:Panel ID="panelSurveillance" runat="server" style="margin-top:10px">
                        <asp:AmlSurveillance ID="amlSurveillance1" runat="server" ActionAllowed="true" DisplayMode="AlertTreatment" />
                    </asp:Panel>
                </asp:Panel>

                <asp:Panel ID="panelRfTypeChoice" runat="server" style="padding-top:10px" CssClass="dual-choice">
                    <asp:RadioButtonList ID="rblAccountClosingRfType" runat="server" CssClass="radioButtonList" RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="rblAccountClosingRfType_SelectedIndexChanged">
                        <asp:ListItem Text="Avec Retour de fonds" Value="WithRF"></asp:ListItem>
                        <asp:ListItem Text="Sans Retour de fonds" Value="WithoutRF"></asp:ListItem>
                    </asp:RadioButtonList>
                </asp:Panel>

                <asp:Panel ID="panelWithRF" runat="server" Visible="false" style="padding-top:10px;">
                    
                    <asp:ReturnFunds ID="ReturnFunds" runat="server" />

                </asp:Panel>

                <asp:Panel ID="panelFees" runat="server" style="padding-top:10px">
                    <div class="label">
                        Frais
                    </div>
                    <asp:AccountClosingFee ID="AccountClosingFee" runat="server" />
                </asp:Panel>

                <asp:UpdatePanel ID="upCalculation" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div style="margin-top:24px">
                            <div style="text-align:center;position:relative;width:50%;margin:auto;padding:10px 0;background-color:#eee">
                                <asp:ImageButton ID="imgReload" runat="server" ImageUrl="~/Styles/Img/refresh.png" OnClick="UpdateCalculation" Height="15" style="position:absolute;right:5px;top:5px;" />
                                <div style="display:inline-block">
                                    <div id="calc-sum" class="table">
                                        <div class="table-row">
                                            <div class="table-cell">
                                
                                            </div>
                                            <div class="table-cell">
                                                <asp:Literal ID="litAccountBalance" runat="server">0 €</asp:Literal>
                                            </div>
                                            <div class="table-cell">
                                                <span class="label">Solde du compte</span>
                                            </div>
                                        </div>
                                        <div class="table-row">
                                            <div class="table-cell">
                                                <span class="label">-</span>  
                                            </div>
                                            <div class="table-cell">
                                                <asp:Literal ID="litReturnFundsSum" runat="server">0 €</asp:Literal>
                                            </div>
                                            <div class="table-cell">
                                                <span class="label">Retour de fonds</span>
                                            </div>
                                        </div>
                                        <div class="table-row">
                                            <div class="table-cell">
                                                <span class="label">-</span>  
                                            </div>
                                            <div class="table-cell">
                                                <asp:Literal ID="litFeesSum" runat="server">0 €</asp:Literal>
                                            </div>
                                            <div class="table-cell">
                                                <span class="label">Frais</span>
                                            </div>
                                        </div>
                                        <asp:Panel ID="panelSum" runat="server" CssClass="table-row">
                                            <div class="table-cell">
                                                <span class="label">=</span>  
                                            </div>
                                            <div class="table-cell">
                                                <asp:Literal ID="litSumLeft" runat="server">0 €</asp:Literal>
                                            </div>
                                            <div class="table-cell">
                                                <span class="label">Restant</span>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                </div>
                                <asp:Panel ID="panelSumNeg" runat="server" Visible="false" style="text-align:center;margin-top: 5px;">
                                    <b style="color:red;text-transform: uppercase;">Attention : le solde du compte est insuffisant</b>
                                </asp:Panel>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>

                <asp:Panel ID="panelCommentAccountClosing" runat="server" style="margin-top:10px">
                    <asp:AmlComment ID="amlCommentAccountClosing" runat="server" MaxLength="400" />
                </asp:Panel>
                <div style="margin-top:20px">
                    <asp:Button ID="btnValidate" runat="server" CssClass="button" Text="Clôturer" OnClick="btnValidate_Click" style="width:100%" />
                    <asp:Button ID="btnUpdate" runat="server" CssClass="button" Text="Enregistrer" Visible="false" OnClick="btnUpdate_Click" style="width:100%" />
                </div>
            </asp:Panel>
            <asp:Panel ID="panelAccountClosed" runat="server" Visible="false">
                <div style="margin:20px 0 30px 0;font-weight: bold;font-size: 1.3em;text-align: center;">
                    Procédure de clôture terminée
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="rblAccountClosingRfType" EventName="SelectedIndexChanged" />
        </Triggers>
    </asp:UpdatePanel>
    <div id="ar-loading"></div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">
    <%--<script type="text/javascript" src="Scripts/jquery.maskMoney.js"></script>
    <script type="text/javascript" src="Scripts/beneficiary.js"></script>--%>
    <script type="text/javascript">
        var panelToLoad = "";

        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            var panel = '#<%=panelAccountClosing.ClientID%>';
            pbControl = args.get_postBackElement();

            if (panel.length > 0) {
                var width = $(panel).outerWidth();
                var height = $(panel).outerHeight();
                $('#ar-loading').css('width', width);
                $('#ar-loading').css('height', height);
                $('#ar-loading').show();
                $('#ar-loading').position({ my: "left top", at: "left top", of: panel });
            }
        }
        function EndRequestHandler(sender, args) {
            $('#ar-loading').hide();
            init();
        }
    </script>
    
</asp:Content>
