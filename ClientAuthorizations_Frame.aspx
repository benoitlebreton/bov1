﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ClientAuthorizations_Frame.aspx.cs" Inherits="ClientAuthorizations_Frame" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Autorisations / Refus</title>
    
    <link href="Styles/tipTip.css" rel="Stylesheet" type="text/css" />
    <link href="Styles/UI/jquery-ui-1.9.2.custom.css" rel="Stylesheet" type="text/css" />
    <link href="~/Styles/Site.css" rel="stylesheet" type="text/css" />
    <link href="Styles/Operations.css" type="text/css" rel="Stylesheet" />

    <script src="Scripts/jquery-1.8.3.js" type="text/javascript"></script>
    <script src="Scripts/jquery.tipTip.minified.js" type="text/javascript"></script>
    <script src="Scripts/jquery-ui-1.9.2.custom.min.js" type="text/javascript"></script>
    <script src="Scripts/jQuery_mousewheel_plugin.js" type="text/javascript"></script>
    <script src="Scripts/jquery.jloupe.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="Scripts/mapbox.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery.watermark.min.js" type="text/javascript"></script>
    <%--<script src="Scripts/jquery.pnotify.min.js" type="text/javascript"></script>--%>
   
    <script type="text/javascript">
        $(document).ready(function () {
            $('#search-popup').dialog({
                autoOpen: false,
                resizable: false,
                draggable: false,
                modal: true,
                minWidth: 310,
                title: "Critères de recherche",
                buttons: [{ text: $('#<%=btnSearch.ClientID%>').val(), click: function () { $("#<%=btnSearch.ClientID %>").click(); $(this).dialog('close'); } }]
            });
            $("#search-popup").parent().appendTo(jQuery("form:first"));

            $("#<%=txtDateFrom.ClientID %>").datepicker({
                defaultDate: "-1w",
                dateFormat: "dd/mm/yy",
                dayNamesMin: ["Di", "Lu", "Ma", "Me", "Je", "Ve", "Sa"],
                monthNames: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"],
                firstDay: 1,
                //changeMonth: true,
                numberOfMonths: 1,
                onClose: function (selectedDate) {
                    $("#<%=txtDateTo.ClientID %>").datepicker("option", "minDate", selectedDate);
               },
               beforeShow: function (input, inst) {
                   setTimeout(function () {
                       inst.dpDiv.position({ my: 'right top', at: 'right bottom', of: input });
                   }, 0);
               }
            });
            $("#<%=txtDateTo.ClientID %>").datepicker({
                defaultDate: "0",
                dateFormat: "dd/mm/yy",
                dayNamesMin: ["Di", "Lu", "Ma", "Me", "Je", "Ve", "Sa"],
                monthNames: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"],
                firstDay: 1,
                maxDate: "0",
                //changeMonth: true,
                numberOfMonths: 1,
                onClose: function (selectedDate) {
                    $("#<%=txtDateFrom.ClientID %>").datepicker("option", "maxDate", selectedDate);
               }
            });

            $('.lblClientTime').text(GetClientTime());
        });

        function GetClientTime() {
            var date = new Date();
            var sHour = date.getHours().toString();
            var sMinute = date.getMinutes().toString();
            if (sMinute.length == 1)
                sMinute = '0' + sMinute;
            return sHour + ':' + sMinute;
        }

        function ToggleOperationDetails(row) {
            if ($(row).next('tr').find('.operation-detail, .operation-detail-alternate').is(':hidden')) {
                $(row).next('tr').find('td').css('background-color', $(row).find('td').css('background-color'));
                //$(row).next('tr').find('td').css('padding-bottom', '5px');
                $(row).next('tr').find('.operation-detail, .operation-detail-alternate').slideDown(400);
                //$(row).find('td').css('border-bottom-right-radius', '0').css('border-bottom-left-radius', '0');

                $(row).find('.detail-arrow').attr('src', $(row).find('.detail-arrow').attr('src').replace(/down/g, 'up'));
            }
            else {
                $(row).next('tr').find('.operation-detail, .operation-detail-alternate').slideUp(400, function () {
                    //$(row).find('td:first-of-type').css('border-top-left-radius', '8px').css('border-bottom-left-radius', '8px');
                    //$(row).find('td:last-of-type').css('border-top-right-radius', '8px').css('border-bottom-right-radius', '8px');
                    //$(row).next('tr').find('td').animate({ paddingBottom: 0 }, 0);
                });

                $(row).find('.detail-arrow').attr('src', $(row).find('.detail-arrow').attr('src').replace(/up/g, 'down'));
            }
        }

        function ShowSearchPopup() {
            $("#search-popup").dialog('open');
        }
    </script>

</head>
<body style="background-color:#fff">
    <form id="form1" runat="server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" EnablePartialRendering="true" EnableScriptLocalization="true"
        EnableScriptGlobalization="true" EnablePageMethods="true">
    </asp:ToolkitScriptManager>
    <div style="width:100%;">
        <div style="margin-top:0px">
            <asp:Panel ID="panelAuthorizationListTable" runat="server">
                <asp:UpdatePanel ID="upOpe" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Panel ID="panelNoFilters" runat="server">
                            <div style="margin:10px 0 5px 0;padding:0 5px;text-align:left;font-size:1.2em;text-transform:uppercase">
                                au <asp:Label ID="lblDateNow" runat="server" style="font-weight:bold;text-transform:uppercase"></asp:Label>, à <span class="lblClientTime" style="font-weight:bold"></span>
                            </div>
                        </asp:Panel>
                        <asp:Panel ID="panelFilters" runat="server" Visible="false">
                            <div style="margin:10px 0 5px 0;padding:0 5px;text-align:left;font-size:1.2em;" class="font-AracneRegular">
                                <div style="width:90%;float:left">
                                    <asp:Literal ID="ltlFilters" runat="server"></asp:Literal>
                                </div>
                                <div style="width:10%;float:right;text-align:right">
                                    <asp:Literal ID="ltlNbResults" runat="server"></asp:Literal>
                                </div>
                                <div style="clear:both"></div>
                            </div>
                        </asp:Panel>

                        <div id="ar-loading" style="display:none;position:absolute;background-color:rgba(255, 255, 255, 0.5);z-index:100"></div>
                        <asp:Repeater ID="rptAuthorizationList" runat="server" OnItemDataBound="rptAuthorizationList_ItemDataBound">
                            <HeaderTemplate>
                                <table id="tOperationList" style="width: 100%" class="operation-table">
                                    <thead>
                                        <tr>
                                            <th style="white-space: nowrap">
                                                Date d'opération
                                            </th>
                                            <th>
                                                Heure
                                            </th>
                                            <th style="white-space: nowrap">
                                                &Eacute;tat
                                            </th>
                                            <th style="width:200px">
                                                Commerçant
                                            </th>
                                            <th>
                                                Montant
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr class='<%# Container.ItemIndex % 2 == 0 ? "operation-table-row" : "operation-table-alternate-row" %>' onclick="ToggleOperationDetails(this);">
                                    <td style="width:140px;white-space: nowrap; text-align: center">
                                        <div>
                                            <span style="position:relative;left:-6px;"><img class="detail-arrow" src="Styles/Img/row-detail-arrow-down.png" /></span>
                                            <%# tools.getFormattedShortDate(Eval("DateLocale").ToString(),'.') %>
                                        </div>
                                    </td>
                                    <td style="width:70px;white-space: nowrap; text-align: center">
                                        <div><%# tools.getFormattedTime(Eval("DateLocale").ToString(), 'h')%></div>
                                    </td>
                                    <td style="width: 200px">
                                        <div class="ellipsis" style="width: 200px">
                                            <%# Eval("ReponseAutorisation") %>
                                        </div>
                                    </td>
                                    <td style="width:400px;">
                                        <div class="ellipsis" style="width:400px;"><%# Eval("LocalisationAccepteur")%></div>
                                    </td>
                                    <td style="white-space: nowrap; text-align: right">
                                        <div style="border-right: 0"><%# tools.getAuthorFormattedAmount(Eval("MontantEnCentimes").ToString(), Eval("MontantTransaction").ToString())%></div>
                                    </td>
                                </tr>
                                <tr style="cursor:auto;border:0">
                                    <td id="tdDetails" runat="server" colspan="8" style="padding:0"><!--style="border-bottom-left-radius:8px;border-bottom-right-radius:8px;"-->
                                        <div class='<%# Container.ItemIndex % 2 == 0 ? "operation-detail" : "operation-detail-alternate" %>' style="display:none;position:relative;z-index:0">
                                            <div class="separator"></div>
                                            <div class="detail-content" style="min-height:15px;">
                                                <div style="font-weight:bold"><%# Eval("ReponseAutorisation") %></div>
                                                <div>Code réponse : <%# Eval("CodeReponseAutorisation") %> - 
                                                    N&deg; autorisation : <%# Eval("NumAutorisation") %>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody>
                            </table>
                            <div style="text-align: right; float: right; position: relative; top: 3px;">
                            </div>
                            </FooterTemplate>
                        </asp:Repeater>
                        <div style="clear:both"></div>
                        <asp:Panel ID="panelNoSearchResult" runat="server" Visible="false" Style="margin:10px 0; text-align:center">
                            Votre recherche n'a retourné aucun résultat.
                        </asp:Panel>

                        <div style="text-align:center;margin:3px 0 50px 0">
                            <asp:Button ID="btnShowMore" runat="server" CssClass="button" Text="Afficher plus d'opérations" style="padding-left:50px;padding-right:50px;margin-right:1px" OnClick="btnShowMore_Click" />
                            <input type="button" value="Rechercher une opération" class="button" onclick="ShowSearchPopup();" style="padding-left:50px;padding-right:50px;" />
                            <asp:Button ID="btnExcelExport" runat="server" CssClass="button excel-button" Text="Export" OnClick="btnExportExcel_Click" />
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>

                <div id="search-popup" style="display:none">
                    <span class="ui-helper-hidden-accessible"><input type="text"/></span>
                    <div>
                        <div class="font-bold uppercase" style="margin:5px 0">
                            Période
                        </div>
                        <div style="white-space:nowrap">
                            <asp:Label ID="lblDateFrom" runat="server" AssociatedControlID="txtDateFrom" CssClass="font-AracneRegular font-orange" Font-Bold="true">Du</asp:Label>&nbsp;
                            <asp:TextBox ID="txtDateFrom" runat="server" style="width:100px"></asp:TextBox>&nbsp;
                            <asp:Label ID="lblDateTo" runat="server" AssociatedControlID="txtDateTo" CssClass="font-AracneRegular font-orange" Font-Bold="true">Au</asp:Label>&nbsp;
                            <asp:TextBox ID="txtDateTo" runat="server" style="width:100px"></asp:TextBox>
                            
                        </div>
                        <div class="font-bold uppercase" style="margin:10px 0 5px 0">
                            Type d'autorisation
                        </div>
                        <div style="white-space:nowrap">
                            <asp:DropDownList ID="ddlAuthorizationType" runat="server" AppendDataBoundItems="true">
                                <asp:ListItem Value="ALL">TOUS</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div style="text-align:center">
                        <asp:Button ID="btnSearch" runat="server" Text="Rechercher" OnClick="btnSearch_Click" CssClass="button" style="display:none" />
                    </div>
                </div>
            </asp:Panel>

        </div>
    </div>
    
    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        var sRequestControlID = "";
        function BeginRequestHandler(sender, args) {
            pbControl = args.get_postBackElement();
            if (pbControl.id == '<%=btnShowMore.ClientID %>' || pbControl.id == '<%=btnSearch.ClientID %>') {
                var width = $('#tOperationList').width();
                var height = $('#tOperationList').height();
                $('#ar-loading').css('width', width);
                $('#ar-loading').css('height', height);

                $('#ar-loading').show();

                $('#ar-loading').position({ my: "left top", at: "left top", of: "#tOperationList" });
            }
            sRequestControlID = pbControl.id;
        }
        function EndRequestHandler(sender, args) {
            $('#ar-loading').hide();

            if (sRequestControlID == '<%=btnSearch.ClientID %>') {
                $(document).scrollTop(0);
            }

            $('.lblClientTime').text(GetClientTime());
        }
    </script>
    </form>
</body>
</html>
