﻿using System;
using System.Threading;
using System.Globalization;
using System.IO;
using System.Configuration;
using XMLMethodLibrary;
using System.Collections.Generic;

public partial class SiteMaster : System.Web.UI.MasterPage
{
    protected void Page_Init(object sender, EventArgs e)
    {
        Thread.CurrentThread.CurrentUICulture = new CultureInfo("fr-FR");
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("fr-FR");

        string sPageName = Request.Url.AbsoluteUri.Substring(Request.Url.AbsoluteUri.LastIndexOf('/') + 1);

        if(tools.CheckBackUrl())
            try
            {
                string sPage = sPageName.Split('.')[0];
                tools.CheckBackUrlPage(sPageName.Split('.')[0]);
            } catch (Exception ex) { }

        if (!pageNotAuthenticated(sPageName))
        {
            authentication auth = authentication.GetCurrent();
            hfSessionStarted.Value = "true";
        }
        else
        {
            hfSessionStarted.Value = "false";
            Session.Clear();
            Session["SessionClosed"] = false;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        int iTimeout;
        if (int.TryParse(ConfigurationManager.AppSettings["SessionTimeout"].ToString(), out iTimeout))
        {
            iTimeout *= 60000;
            hfSessionTimeout.Value = iTimeout.ToString();
        }
        if (Session["SessionClosed"] != null && (bool)Session["SessionClosed"] == true)
        {
            hfShowTimeout.Value = "true";
            Session["SessionClosed"] = false;
        }

        if (IsPostBack)
        {
            //string controlName = Request.Params.Get("__EVENTTARGET");
            //string argument = Request.Params.Get("__EVENTARGUMENT");
            //if (controlName == "SessionTimeout")
            //{
            //    Session.Clear();
            //    Session["SessionClosed"] = true;
            //    Response.Redirect("TimeoutSession.aspx");
            //}
        }
        else
        {
            lblServerName.Text = ConfigurationManager.AppSettings["ServerName"].ToString();
            //getNbNonEvaluateSubscription();
            CheckRights();
            CheckPendingTransferRights();
        }
    }

    protected void DeleteXmlFiles()
    {

        string date = DateTime.Now.ToString("yyyyMMdd");
        string path = "";
        string filename = "";

        try
        {
            string[] fileList = Directory.GetFiles(System.Web.HttpContext.Current.Request.MapPath(".") + "\\XmlTmp");

            for (int i = 0; i < fileList.Length; i++)
            {
                try
                {
                    string[] pathSplit = fileList[i].Split('\\');
                    filename = fileList[i].Split('\\')[pathSplit.Length-1];

                    if (filename.Split('_').Length != 3)
                        File.Delete(@fileList[i]);
                    else if (filename.Split('_')[2] != date + ".xml")
                        File.Delete(@fileList[i]);
                }
                catch (Exception ex)
                {
                }

            }
        }
        catch (Exception ex)
        {
        }
    }

    protected bool pageNotAuthenticated(string pageName)
    {
        bool isAuthorized = false;

        string pageList = (ConfigurationManager.AppSettings["NotAuthenticatedPageList"] != null) ? ConfigurationManager.AppSettings["NotAuthenticatedPageList"].ToString() : "";
        string[] tPageList = pageList.Split(',');

        for (int i = 0; i < tPageList.Length; i++)
        {
            if (tPageList[i].ToString().ToLower().Trim() == pageName.ToLower().Trim())
                isAuthorized = true;
        }

        return isAuthorized;
    }

    //protected void NbNonEvaluateSubscription_Tick(object sender, EventArgs e)
    //{
    //    getNbNonEvaluateSubscription();
    //}

    //protected void getNbNonEvaluateSubscription()
    //{
    //    try
    //    {
    //        int result = ws_tools.getNbNonEvaluateSubscription();

    //        if (result == 0)
    //        {
    //            panelNbNonEvaluateSubscription.Visible = false;
    //        }
    //        else if (result == 1)
    //        {
    //            panelNbNonEvaluateSubscription.Visible = true;
    //            lblNbNonEvaluateSubscription.Text = "<b>" + result.ToString() + "</b> inscription non vérifiée";
    //        }
    //        else
    //        {
    //            panelNbNonEvaluateSubscription.Visible = true;
    //            lblNbNonEvaluateSubscription.Text = "<b>" + result.ToString() + "</b> inscriptions non vérifiées";
    //        }

    //    }
    //    catch (Exception ex)
    //    {
    //    }
    //}

    protected void CheckPendingTransferRights()
    {
        authentication auth = authentication.GetCurrent(false);
        if(auth != null)
        {
            bool bView, bAction;
            AML.HasPendingTransferRights(auth.sToken, out bView, out bAction);
            if (bView)
            {
                panelBlockedTransfer.Visible = true;
                panelBlockedTransferReserved.Visible = true;
                panelGarnishmentPendingTransfer.Visible = true;
            }
            else
            {
                panelBlockedTransfer.Visible = false;
                panelBlockedTransferReserved.Visible = false;
                panelGarnishmentPendingTransfer.Visible = false;
            }

            AML.HasAccountClosingRights(auth.sToken, out bView, out bAction);
            if(bView)
            {
                panelPendingCustomerCloseRF.Visible = true;
            }
        }
    }

    protected void CheckRights()
    {
        panelNbNonEvaluateSubscription.Visible = false;
        try
        {
            string sXmlOut = tools.GetRights(authentication.GetCurrent(false).sToken, "SC_Header");

            List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Page", "RC");
            List<string> listAction = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/Page/Action");

            if (listRC.Count > 0 && listRC[0] == "0")
            {
                for (int i = 0; i < listAction.Count; i++)
                {
                    List<string> listTag = CommonMethod.GetAttributeValues(listAction[i], "Action", "TagAction");
                    List<string> listActionAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ActionAllowed");
                    List<string> listViewAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ViewAllowed");

                    if (listTag.Count > 0 && listTag[0] == "NbNonEvaluateSubscription")
                    {
                        if (listViewAllowed[0] == "1")
                        {
                            panelNbNonEvaluateSubscription.Visible = true;
                        }
                    }
                }
            }
        }
        catch(Exception ex)
        {

        }
    }
}
