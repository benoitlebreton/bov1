﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Xml.Linq;
using XMLMethodLibrary;
using System.IO;
using OfficeOpenXml;
using System.Drawing;
using OfficeOpenXml.Style;

public partial class _StoreLocator : System.Web.UI.Page
{
    protected bool bActionAllowed { get { return (ViewState["ActionAllowed"] != null) ? bool.Parse(ViewState["ActionAllowed"].ToString()) : false; } set { ViewState["ActionAllowed"] = value; } }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            CheckRights();
            BindGridView();
            //if (gvStores.FooterRow != null)
            //{
            //    TextBox txt = (TextBox)gvStores.FooterRow.FindControl("txtFreeLabel");
            //    if (txt != null)
            //        txt.Text = "[b]Horaires d'ouverture :[/b]" + Environment.NewLine + Environment.NewLine + "[b]Téléphone :[/b]";
            //}
        }
    }

    protected void CheckRights()
    {
        string sXmlOut = tools.GetRights(authentication.GetCurrent().sToken, "SC_StoreLocator");

        List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Page", "RC");

        if (listRC.Count == 0 || listRC[0] != "0")
        {
            Response.Redirect("Default.aspx", true);
        }
        else
        {
            List<string> listAction = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/Page/Action");

            for (int i = 0; i < listAction.Count; i++)
            {
                List<string> listTag = CommonMethod.GetAttributeValues(listAction[i], "Action", "TagAction");
                List<string> listActionAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ActionAllowed");
                List<string> listViewAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ViewAllowed");

                if (listTag.Count > 0 && listTag[0] == "StoreLocator")
                {
                    if (listActionAllowed[0] == "1")
                        bActionAllowed = true;
                    break;
                }
            }
        }
    }

    protected void BindGridView()
    {
        try
        {
            DataTable dt = GetStores();

            //if (dt.Rows.Count > 0)
            //{
                DataTable dtTmp = dt.Clone();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    bool bAdd = false;

                    if (txtFilter.Text.Length > 0)
                    {
                        string sFilter = txtFilter.Text.ToLower();
                        if (FilterText(sFilter, dt.Rows[i]) && FilterVisible(ckbFilterVisible.Checked, dt.Rows[i]["Visible"].ToString().ToLower()))
                            bAdd = true;
                    }
                    else {
                        if(FilterVisible(ckbFilterVisible.Checked, dt.Rows[i]["Visible"].ToString().ToLower()))
                            bAdd = true;
                    }

                    

                    if(bAdd)
                        dtTmp.Rows.Add(dt.Rows[i].ItemArray);
                }

                // Fill empty free label with default value
                for (int i = 0; i < dtTmp.Rows.Count; i++)
                {
                    if (dtTmp.Rows[i]["FreeLabel1"].ToString().Length == 0)
                        dtTmp.Rows[i]["FreeLabel1"] = "<b>Horaires d'ouverture :</b>" + Environment.NewLine + Environment.NewLine + "<b>Téléphone :</b>";
                }

                ViewState["gvStoresContent"] = dtTmp;
                gvStores.DataSource = dtTmp;
                gvStores.DataBind();
            //}
        }
        catch (Exception e)
        {
        }
    }
    protected DataTable GetStores()
    {
        SqlConnection conn = null;
        DataTable dt = new DataTable();

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_GetPOSInformationStoreLocation_2", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            using (SqlDataAdapter da = new SqlDataAdapter(cmd))
            {
                da.Fill(dt);
            }
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }

    protected bool FilterText(string sFilter, DataRow row)
    {
        if (row["IdePDVSAB"].ToString().ToLower().Contains(sFilter) || row["AgencyName"].ToString().ToLower().Contains(sFilter) || row["Zip"].ToString().ToLower().Contains(sFilter) || row["City"].ToString().ToLower().Contains(sFilter))
            return true;
        else return false;
    }
    protected bool FilterVisible(bool bChecked, string sVisible)
    {
        if ((bChecked == true && sVisible == "true") || (bChecked == false && sVisible == "false"))
            return true;
        else return false;
    }

    protected void EditStore(object sender, GridViewEditEventArgs e)
    {
        gvStores.EditIndex = e.NewEditIndex;
        BindGridView();
    }
    protected void CancelEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvStores.EditIndex = -1;
        BindGridView();
    }
    protected void UpdateStore(object sender, GridViewUpdateEventArgs e)
    {
        string sRef = ((Label)gvStores.Rows[e.RowIndex].FindControl("lblRef")).Text;
        string sIdePDVSAB = ((TextBox)gvStores.Rows[e.RowIndex].FindControl("txtIdePDVSAB")).Text;
        string sAgencyName = ((TextBox)gvStores.Rows[e.RowIndex].FindControl("txtName")).Text;
        string sLabel1 = ((TextBox)gvStores.Rows[e.RowIndex].FindControl("txtName2")).Text;
        string sAddress = ((TextBox)gvStores.Rows[e.RowIndex].FindControl("txtAddress")).Text;
        string sZip = ((TextBox)gvStores.Rows[e.RowIndex].FindControl("txtZip")).Text;
        string sCity = ((TextBox)gvStores.Rows[e.RowIndex].FindControl("txtCity")).Text;
        string sNbCitizens = ((TextBox)gvStores.Rows[e.RowIndex].FindControl("txtNbCitizens")).Text;
        //string sArea = ((TextBox)gvStores.Rows[e.RowIndex].FindControl("txtArea")).Text;
        string sLatitude = ((TextBox)gvStores.Rows[e.RowIndex].FindControl("txtLatitude")).Text;
        string sLongitude = ((TextBox)gvStores.Rows[e.RowIndex].FindControl("txtLongitude")).Text;
        string sFreeLabel1 = ((TextBox)gvStores.Rows[e.RowIndex].FindControl("txtFreeLabel")).Text.Replace("[", "<").Replace("]", ">");
        string sIsMPAD = (((CheckBox)gvStores.Rows[e.RowIndex].FindControl("ckMPAD")).Checked) ? "1" : "0";
        string sCertified = (((CheckBox)gvStores.Rows[e.RowIndex].FindControl("ckCertified")).Checked) ? "1" : "0";
        string sVisible = (((CheckBox)gvStores.Rows[e.RowIndex].FindControl("ckVisible")).Checked) ? "1" : "0";
        string sPhoneNumber = ((TextBox)gvStores.Rows[e.RowIndex].FindControl("txtPhoneNumber")).Text;
        string sOpeningHours = ((TextBox)gvStores.Rows[e.RowIndex].FindControl("txtOpeningHours")).Text;

        string sXmlIn = new XElement("ALL_XML_IN",
                            new XElement("P",
                                new XAttribute("ref", sRef),
                                new XAttribute("IdePDVSAB", sIdePDVSAB),
                                new XAttribute("AgencyName", sAgencyName),
                                new XAttribute("Label1", sLabel1),
                                new XAttribute("Address", sAddress),
                                new XAttribute("Zip", sZip),
                                new XAttribute("City", sCity),
                                new XAttribute("NbCitizens", sNbCitizens),
            //new XAttribute("Area", sArea),
                                new XAttribute("Latitude", sLatitude),
                                new XAttribute("Longitude", sLongitude),
                                new XAttribute("FreeLabel1", sFreeLabel1),
                                new XAttribute("IsMPAD", sIsMPAD),
                                new XAttribute("Certified", sCertified),
                                new XAttribute("Visible", sVisible),
                                new XAttribute("PhoneNumber", sPhoneNumber),
                                new XAttribute("OpeningHours", sOpeningHours),
                                new XAttribute("ActionType", "U"))).ToString();

        if(AddDelSetStore(sXmlIn))
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "AlertMessageKey", "AlertMessage('Message', '<span style=\"color:green\">Agence modifiée</span>', null, null);", true);
        else ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "AlertMessageKey", "AlertMessage('Message', 'Une erreur est survenue', null, null);", true);

        gvStores.EditIndex = -1;
        BindGridView();
    }

    protected void AddNewStore(object sender, EventArgs e)
    {
        string sIdePDVSAB = "";
        string sAgencyName = "";
        string sLabel1 = "";
        string sAddress = "";
        string sZip = "";
        string sCity = "";
        string sNbCitizens = "";
        //string sArea = "";
        string sLatitude = "";
        string sLongitude = "";
        string sFreeLabel1 = "";
        string sIsMPAD = "";
        string sCertified = "";
        string sVisible = "";
        string sPhoneNumber = "";
        string sOpeningHours = "";

        Button btn = (Button)sender;
        sIdePDVSAB = ((TextBox)btn.NamingContainer.FindControl("txtIdePDVSAB")).Text;
        sAgencyName = ((TextBox)btn.NamingContainer.FindControl("txtName")).Text;
        sLabel1 = ((TextBox)btn.NamingContainer.FindControl("txtName2")).Text;
        sAddress = ((TextBox)btn.NamingContainer.FindControl("txtAddress")).Text;
        sZip = ((TextBox)btn.NamingContainer.FindControl("txtZip")).Text;
        sCity = ((TextBox)btn.NamingContainer.FindControl("txtCity")).Text;
        sNbCitizens = ((TextBox)btn.NamingContainer.FindControl("txtNbCitizens")).Text;
        //sArea = ((TextBox)btn.NamingContainer.FindControl("txtArea")).Text;
        sLatitude = ((TextBox)btn.NamingContainer.FindControl("txtLatitude")).Text;
        sLongitude = ((TextBox)btn.NamingContainer.FindControl("txtLongitude")).Text;
        sFreeLabel1 = ((TextBox)btn.NamingContainer.FindControl("txtFreeLabel")).Text.Replace("[", "<").Replace("]", ">");
        sPhoneNumber = ((TextBox)btn.NamingContainer.FindControl("txtPhoneNumber")).Text;
        sOpeningHours = ((TextBox)btn.NamingContainer.FindControl("txtOpeningHours")).Text;
        sIsMPAD = (((CheckBox)btn.NamingContainer.FindControl("ckMPAD")).Checked) ? "1" : "0";
        sCertified = (((CheckBox)btn.NamingContainer.FindControl("ckCertified")).Checked) ? "1" : "0";
        sVisible = (((CheckBox)btn.NamingContainer.FindControl("ckVisible")).Checked) ? "1" : "0";

        string sXmlIn = new XElement("ALL_XML_IN",
                            new XElement("P",
                                new XAttribute("IdePDVSAB", sIdePDVSAB),
                                new XAttribute("AgencyName", sAgencyName),
                                new XAttribute("Label1", sLabel1),
                                new XAttribute("Address", sAddress),
                                new XAttribute("Zip", sZip),
                                new XAttribute("City", sCity),
                                new XAttribute("NbCitizens", sNbCitizens),
            //new XAttribute("Area", sArea),
                                new XAttribute("Latitude", sLatitude),
                                new XAttribute("Longitude", sLongitude),
                                new XAttribute("FreeLabel1", sFreeLabel1),
                                new XAttribute("IsMPAD", sIsMPAD),
                                new XAttribute("Certified", sCertified),
                                new XAttribute("Visible", sVisible),
                                new XAttribute("PhoneNumber", sPhoneNumber),
                                new XAttribute("OpeningHours", sOpeningHours),
                                new XAttribute("ActionType", "A"))).ToString();

        if(AddDelSetStore(sXmlIn))
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "AlertMessageKey", "AlertMessage('Message', '<span style=\"color:green\">Agence ajoutée</span>', null, null);", true);
        else ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "AlertMessageKey", "AlertMessage('Message', 'Une erreur est survenue', null, null);", true);
        
        BindGridView();
    }

    protected void DeleteStore(object sender, EventArgs e)
    {
        Button btn = (Button)sender;

        string sXmlIn = new XElement("ALL_XML_IN",
                            new XElement("P",
                                new XAttribute("ref", btn.CommandArgument),
                                new XAttribute("ActionType", "D"))).ToString();
        if (AddDelSetStore(sXmlIn))
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "AlertMessageKey", "AlertMessage('Message', '<span style=\"color:green\">Agence supprimée</span>', null, null);", true);
        else ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "AlertMessageKey", "AlertMessage('Message', 'Une erreur est survenue', null, null);", true);

        BindGridView();
    }

    protected bool AddDelSetStore(string sXmlIn)
    {
        SqlConnection conn = null;
        string sXmlOut = "";
        bool bOk = false;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_AddDelSetStoreLocatorPOS_2", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);

            cmd.Parameters["@IN"].Value = sXmlIn;
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            List<string> listAdd = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Result", "Inserted");
            List<string> listDel = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Result", "Deleted");
            List<string> listSet = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Result", "Updated");

            if ((listAdd.Count > 0 && listAdd[0] == "1") || (listDel.Count > 0 && listDel[0] == "1") || (listSet.Count > 0 && listSet[0] == "1"))
                bOk = true;
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return bOk;
    }

    protected bool ConvertBitToBool(string sValue)
    {
        return (sValue == "1") ? true : false;
    }

    protected string CheckboxToLabel(string sValue)
    {
        sValue = sValue.ToLower();

        if (sValue == "1" || sValue == "true")
            return "Oui";
        else return "Non";
    }

    protected string NewLineToBR(string sContent)
    {
        return sContent.Replace("\n", "<br/>");
    }

    protected string HTMLToText(string sValue)
    {
        return sValue.Replace("<", "[").Replace(">", "]");
    }

    private const string ASCENDING = " ASC";
    private const string DESCENDING = " DESC";

    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    protected void gvStores_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortExpression = e.SortExpression;

        if (GridViewSortDirection == SortDirection.Ascending)
        {
            GridViewSortDirection = SortDirection.Descending;
            SortGridView(sortExpression, DESCENDING);
        }
        else
        {
            GridViewSortDirection = SortDirection.Ascending;
            SortGridView(sortExpression, ASCENDING);
        }

    }

    private void SortGridView(string sortExpression, string direction)
    {
        //  You can cache the DataTable for improving performance
        DataTable dt = (DataTable)ViewState["gvStoresContent"];

        DataView dv = new DataView(dt);
        dv.Sort = sortExpression + direction;

        gvStores.DataSource = dv;
        gvStores.DataBind();
    }
    protected void btnRefresh_Click(object sender, EventArgs e)
    {
        BindGridView();
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        string sFilePath = "";

        if (CreateExportExcelFile(GetStores(), "carte_points_de_vente", "Carte points de vente " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss"), out sFilePath))
            Response.Redirect(sFilePath);

        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "ExportKey", "$('#ar-loading').hide();", true);
    }

    protected bool CreateExportExcelFile(DataTable dt, string sFilename, string sWorkbookLabel, out string sFilePath)
    {
        bool bGenerated = false;
        sFilePath = "";

        if (dt.Rows.Count > 0)
        {
            string sExportsExcelFilePath = ConfigurationManager.AppSettings["ExportsExcelFilePath"].ToString();
            sFilePath = "~/" + sExportsExcelFilePath + sFilename + ".xlsx";

            // Create the file using the FileInfo object
            var file = new FileInfo(Server.MapPath("~/" + sExportsExcelFilePath + sFilename + ".xlsx"));

            // Delete file if exists
            if (file.Exists)
                file.Delete();

            // Create the package and make sure you wrap it in a using statement
            using (var package = new ExcelPackage(file))
            {
                // add a new worksheet to the empty workbook
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add(sWorkbookLabel);

                // Add some formatting to the worksheet
                worksheet.TabColor = Color.Blue;
                worksheet.DefaultRowHeight = 20;
                worksheet.HeaderFooter.FirstFooter.LeftAlignedText = string.Format("Generated: {0}", DateTime.Now.ToShortDateString());

                worksheet.Row(1).Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Row(1).Style.Fill.BackgroundColor.SetColor(Color.FromArgb(245, 117, 39));
                worksheet.Row(1).Style.Font.Color.SetColor(Color.White);
                worksheet.Row(1).Style.Font.Bold = true;
                worksheet.Row(1).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Row(1).Height = 30;

                worksheet.Cells[1, 1].Value = "Ref";
                worksheet.Cells[1, 2].Value = "ID SAB";
                worksheet.Cells[1, 3].Value = "SIRET";
                worksheet.Cells[1, 4].Value = "Nom gérant(e)";
                worksheet.Cells[1, 5].Value = "Prénom gérant(e)";
                worksheet.Cells[1, 6].Value = "Email";
                worksheet.Cells[1, 7].Value = "Nom";
                worksheet.Cells[1, 8].Value = "Adresse";
                worksheet.Cells[1, 9].Value = "Code postal";
                worksheet.Cells[1, 10].Value = "Ville";
                worksheet.Cells[1, 11].Value = "Nb Habitants";
                worksheet.Cells[1, 12].Value = "Latitude";
                worksheet.Cells[1, 13].Value = "Longitude";
                worksheet.Cells[1, 14].Value = "Horaires";
                worksheet.Cells[1, 15].Value = "Certifiée";
                worksheet.Cells[1, 16].Value = "Visible";

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    worksheet.Cells[i + 2, 1].Value = dt.Rows[i]["refAgency"];
                    worksheet.Cells[i + 2, 2].Value = dt.Rows[i]["IdePDVSAB"];
                    worksheet.Cells[i + 2, 3].Value = dt.Rows[i]["SIRET"];
                    worksheet.Cells[i + 2, 4].Value = dt.Rows[i]["ManagerLastName"];
                    worksheet.Cells[i + 2, 5].Value = dt.Rows[i]["ManagerFirstName"];
                    worksheet.Cells[i + 2, 6].Value = dt.Rows[i]["Email"];
                    worksheet.Cells[i + 2, 7].Value = dt.Rows[i]["AgencyName"];
                    worksheet.Cells[i + 2, 8].Value = dt.Rows[i]["Address"];
                    worksheet.Cells[i + 2, 9].Value = dt.Rows[i]["Zip"];
                    worksheet.Cells[i + 2, 10].Value = dt.Rows[i]["City"];
                    worksheet.Cells[i + 2, 11].Value = dt.Rows[i]["NbCitizens"];
                    worksheet.Cells[i + 2, 12].Value = dt.Rows[i]["Latitude"];
                    worksheet.Cells[i + 2, 13].Value = dt.Rows[i]["Longitude"];
                    worksheet.Cells[i + 2, 14].Value = dt.Rows[i]["OpeningHours"];
                    worksheet.Cells[i + 2, 15].Value = dt.Rows[i]["Certified"];
                    worksheet.Cells[i + 2, 16].Value = dt.Rows[i]["Visible"];
                }

                worksheet.Cells[worksheet.Dimension.Address].AutoFitColumns();

                package.Save();

                bGenerated = true;
            }
        }

        return bGenerated;
    }
}
