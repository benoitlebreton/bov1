﻿using ApiCobalt;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class CobaltTransfer : System.Web.UI.Page
{
    protected string sIBAN { get { return (ViewState["sIBAN"] != null) ? ViewState["sIBAN"].ToString() : null; } set { ViewState["sIBAN"] = value; } }
    protected string sClientID { get { return (ViewState["sClientID"] != null) ? ViewState["sClientID"].ToString() : null; } set { ViewState["sClientID"] = value; } }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.Params["iban"] != null && !String.IsNullOrWhiteSpace(Request.Params["iban"]))
                sIBAN = Request.Params["iban"].ToString().Replace(" ", "").ToUpper();
            else Response.Redirect("Default.aspx", true);

            LoadTransfer(sIBAN);
        }
    }

    protected void LoadTransfer(string sIban)
    {
        List<ApiCobalt.CreditTransfers.CreditTransfer> listCreditTransfers = null;

        try
        {
            CobaltApi api = new CobaltApi(ConfigurationManager.AppSettings["Cobalt_Core_URL"].ToString(), ConfigurationManager.AppSettings["Cobalt_Transfer_URL"].ToString(), null, ConfigurationManager.AppSettings["Cobalt_Expiration_URL"].ToString());
            ApiCobalt.Accounts.Account account = api.Account.GetAccount(sIBAN);
            sClientID = account.ClientId;
            listCreditTransfers = api.CreditTransfer.GetCreditTransfers(sClientID, sIBAN);

#if DEBUG
            ApiCobalt.CreditTransfers.CreditTransfer ct = new ApiCobalt.CreditTransfers.CreditTransfer();
            ct.Amount = -1000;
            ct.CounterpartIban = "FR7616598000010715366000122";
            ct.CounterpartBic = "FPELFR21";
            ct.CounterpartName = "Gregory POINTAUX";
            ct.Description = "Test virement annulable";
            ct.Reference = "123456";
            ct.CollectionDate = "13/09/2018";
            ct.Status = "en_cours";
            listCreditTransfers.Add(ct);
            ct = new ApiCobalt.CreditTransfers.CreditTransfer();
            ct.Amount = -1500;
            ct.CounterpartIban = "FR7616598000010715366000122";
            ct.CounterpartBic = "FPELFR21";
            ct.CounterpartName = "Gregory POINTAUX";
            ct.Description = "Test virement annulable 2";
            ct.Reference = "123457";
            ct.CollectionDate = "12/09/2018";
            ct.Status = "en_cours";
            listCreditTransfers.Add(ct);
#endif
        }
        catch (Exception e)
        {

        }

        if (listCreditTransfers != null && listCreditTransfers.Count > 0)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("IBAN_RECEVEUR");
            dt.Columns.Add("BIC_RECEVEUR");
            dt.Columns.Add("NOM_RECEVEUR");
            dt.Columns.Add("MONTANT");
            dt.Columns.Add("DATE_COLLECTION");
            dt.Columns.Add("DATE_CREATION");
            dt.Columns.Add("DATE_FIN");
            dt.Columns.Add("LABEL");
            dt.Columns.Add("REFERENCE");
            dt.Columns.Add("STATUT");

            for (int i = 0; i < listCreditTransfers.Count; i++)
            {
                dt.Rows.Add(new object[] {
                    listCreditTransfers[i].CounterpartIban,
                    listCreditTransfers[i].CounterpartBic,
                    listCreditTransfers[i].CounterpartName,
                    listCreditTransfers[i].Amount,
                    listCreditTransfers[i].CollectionDate,
                    listCreditTransfers[i].CreationDate,
                    listCreditTransfers[i].EndDate,
                    listCreditTransfers[i].Description,
                    listCreditTransfers[i].Reference,
                    listCreditTransfers[i].Status
                });
            }

            rptCobaltTransfer.DataSource = dt;
            rptCobaltTransfer.DataBind();
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        string sReference = hdnRefTransferToCancel.Value;

        CobaltApi api = new CobaltApi(ConfigurationManager.AppSettings["Cobalt_Core_URL"].ToString(), ConfigurationManager.AppSettings["Cobalt_Transfer_URL"].ToString(), null, ConfigurationManager.AppSettings["Cobalt_Expiration_URL"].ToString());

        if (!api.CreditTransfer.DeleteCreditTransfer(sClientID, sIBAN, sReference))
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage(\"Erreur\",\"Le virement n'a pas pu être annulé.\");", true);
        else LoadTransfer(sIBAN);
    }
}