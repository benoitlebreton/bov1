﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using XMLMethodLibrary;
using System.Text;
using System.Xml;
using System.IO;
using System.Globalization;
using System.Web.Hosting;
using CuteWebUI;

public partial class RegistrationDetails : System.Web.UI.Page
{
    protected DataTable dtDocs { get { return (ViewState["dtDocs"] != null) ? (DataTable)ViewState["dtDocs"] : null; } set { ViewState["dtDocs"] = value; } }

    protected bool bActionAllowed { get { return (ViewState["ActionAllowed"] != null) ? bool.Parse(ViewState["ActionAllowed"].ToString()) : false; } set { ViewState["ActionAllowed"] = value; } }
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            DeleteOldXml();

            if (Request.Params.Count > 0 && Request.Params["No"] != null && Request.Params["No"].Trim().Length > 0)
            {
                lblRegistrationSelected.Text = Request.Params["No"].Trim();
                divRegistrationDetails.Visible = true;
                if (lblRegistrationSelected.Text.Trim() != "")
                    refresh();
            }
            else
            {
                Response.Redirect("RegistrationSearch.aspx");
            }

            CheckRights();
        }
    }
    protected void CheckRights()
    {
        string sXmlOut = tools.GetRights(authentication.GetCurrent().sToken, "SC_Registration");

        List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Page", "RC");

        if (listRC.Count == 0 || listRC[0] != "0")
        {
            Response.Redirect("Default.aspx", true);
        }
        else
        {
            List<string> listAction = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/Page/Action");

            for (int i = 0; i < listAction.Count; i++)
            {
                List<string> listTag = CommonMethod.GetAttributeValues(listAction[i], "Action", "TagAction");
                List<string> listActionAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ActionAllowed");
                List<string> listViewAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ViewAllowed");

                if (listTag.Count > 0 && listTag[0] == "RegistrationInfo")
                {
                    if (listActionAllowed[0] != "1")
                    {
                        btnForceSubscription.Visible = false;
                        btnValidate.Visible = false;
                        btnRectified.Visible = false;
                        btnModify.Visible = false;
                        btnModifyValidation.Visible = false;
                    }
                    else
                    {
                        bActionAllowed = true;
                    }
                    if (listViewAllowed[0] == "0")
                    {
                    }
                    break;
                }
            }
        }
    }

    protected void RegistrationChecked(string sStatus, string sStatusComment, string sNoComplianceList)
    {
        SqlConnection conn = null;
        authentication auth = authentication.GetCurrent();

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Partner"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("Registration.P_BackOffice_RegistrationAnalyse", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);

            string sXmlIN = new XElement("ALL_XML_IN",
                                                new XElement("DOC",
                                                    new XAttribute("RegistrationCode", lblRegistrationSelected.Text),
                                                    new XAttribute("CashierToken", auth.sToken),
                                                    new XAttribute("BackOfficeOpinion", sStatus),
                                                    new XAttribute("BackOfficeRemark", sStatusComment),
                                                    new XAttribute("BackOfficeNoCompliance", sNoComplianceList.Trim()),
                                                    new XAttribute("CultureID", (Session["CultureID"] != null) ? Session["CultureID"].ToString() : "Fr-fr")
                                                )
                                            ).ToString();

            cmd.Parameters["@IN"].Value = sXmlIN;
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sOut = cmd.Parameters["@OUT"].Value.ToString();

            if (sOut.Length > 0)
            {
                refresh();
                if (tools.GetValueFromXml(sOut, "ALL_XML_OUT/User", "Zendesk") == "0")
                {
                    string sMessage = "L'évaluation de l'inscription a bien été enregistrée dans le Backoffice mais n'as pas été enregistrée dans Zendesk.";
                    string sTitle = "Enregistrement évaluation Zendesk échoué";
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "alertMessage('"+sTitle.Replace("'","&apos;")+"','"+sMessage.Replace("'","&apos;")+"');", true);
                }
            }
            else
            {
            }
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

    }

    protected void onClickPreviousButton(object sender, EventArgs e)
    {
        Response.Redirect("RegistrationSearch.aspx");
    }

    protected void onClickSaveClientModification(object sender, EventArgs e)
    {
        lblRegistrationSelected.Text = Request.Params["No"].Trim();
        string sError = "";
        string sMessage = "";

        if (checkClientInformations(out sError))
        {
            if (updateRegistration(getXmlFromClientInformations(), out sError))
                sMessage = "Les modifications ont bien été enregistrées.";
            else
                sMessage = "Les modifications n'ont pas été enregistrées.<br/>" + sError;

            refresh();
        }
        else
        {
            sError = (sError.Length > 0) ? "Veuillez corriger le(s) champs suivant(s) :" + sError : "";
            sMessage = "Les modifications n'ont pas été enregistrées.<br/>" + sError;
        }

        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "alertMessage('Enregistrement','"+sMessage.Replace("'","&apos;").Trim()+"');", true);
    }

    /*** details function ***/
    protected string getDurationFormat(string sDate)
    {
        string sDuration = "";

        if (sDate.Trim().Length > 0 && sDate.Split(':').Length >= 3)
        {
            string[] tsDuration = sDate.Split(':');

            int iDurationTimeHH; int iDurationTimeMM; int iDurationTimeSS;

            if (int.TryParse(tsDuration[0].Trim(), out iDurationTimeHH) &&
                int.TryParse(tsDuration[1].Trim(), out iDurationTimeMM) &&
                int.TryParse(tsDuration[2].Trim(), out iDurationTimeSS))
            {
                string sDurationTimeHH = (iDurationTimeHH == 0) ? "" : iDurationTimeHH.ToString() + "h";
                string sDurationTimeMM = (iDurationTimeMM == 0) ? "" : iDurationTimeMM.ToString() + "min";
                string sDurationTimeSS = (iDurationTimeSS == 0) ? "" : iDurationTimeSS.ToString() + "s";
                sDuration = sDurationTimeHH + sDurationTimeMM + sDurationTimeSS;
            }
            else
                sDuration = sDate;
        }
        else
            sDuration = sDate;

        return sDuration;
    }

    protected void initAllFields()
    {
        txtCardNumber.Text = "";
        txtDepositAmount.Text = "";
        txtPackSaleWithActivation.Text = "";

        txtBirthDate.Text = "";
        lblBirthDate.Text = "";
        txtBirthCity.Text = "";
        lblBirthCity.Text = "";
        ddlBirthCountry.SelectedIndex = 0;
        lblBirthCountry.Text = ddlBirthCountry.SelectedItem.Text;
        if(lblBirthCountry.Text.Trim().Length == 0)
            lblBirthCountry.Text = "<span style=\"font-style:italic; text-transform:lowercase\">Non renseigné</span>";
        lblDocumentCountry.Text = ddlDocumentCountry.SelectedItem.Text;
        txtDocumentNumber.Text = "";
        lblDocumentCountry.Text = "";
        txtExpirationDate.Text = "";
        lblExpirationDate.Text = "";
        //rblGender.SelectedIndex = 0;
        //lblGender.Text = rblGender.SelectedValue;
        txtIDFirstName.Text = "";
        lblIDFirstName.Text = "";
        txtIDLastName.Text = "";
        lblIDLastName.Text = "";
        txtIssuedID.Text = "";
        lblIssuedID.Text = "";

        txtCity.Text = "";
        lblCity.Text = "";
        txtTo.Text = "";
        lblTo.Text = "";
        txtZipCode.Text = "";
        lblZipCode.Text = "";
        txtAddress1.Text = "";
        lblAddress1.Text = "";
        txtAddress2.Text = "";
        lblAddress2.Text = "";
        txtBiller.Text = "";
        lblBiller.Text = "";
        txtIssuedFact.Text = "";
        lblIssuedFact.Text = "";

        /*txtAddress2.ReadOnly = true;
        txtBiller.ReadOnly = true;
        txtTo.ReadOnly = true;
        txtIssuedFact.ReadOnly = true;
        txtIDMarriedLastName.ReadOnly = true;
        */

        rptSetNoComplianceList.DataSource = AML.getNoComplianceList();
        rptSetNoComplianceList.DataBind();
    }

    protected void getRegistrationDetails(string sRegistrationNumber)
    {
        string sXML = "";
        /*<ALL_XML_IN>
                <Enrolment RefTransaction=""
                    RegistrationCode="NOB205852968790"
                    CashierToken="0x9310D6EDA176B8C83C1FFD5E5E9230C1512562CC"
                    CultureID="fr-FR"
                />
        </ALL_XML_IN>*/

        SqlConnection conn = null;
        DataTable dt = new DataTable();
        authentication auth = authentication.GetCurrent();

        try
        {
            sXML = new XElement("ALL_XML_IN",
                        new XElement("Enrolment",
                            new XAttribute("RegistrationCode", sRegistrationNumber),
                            new XAttribute("CashierToken", auth.sToken),
                            new XAttribute("CultureID", "fr-FR")
                        )
                    ).ToString();

            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Partner"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("Registration.P_GetRegistrationTransactionDetailsWS", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@ALL_XML_IN", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@ALL_XML_OUT", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@RC", SqlDbType.Bit);

            cmd.Parameters["@ALL_XML_IN"].Value = sXML;

            cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;
            cmd.Parameters["@ALL_XML_OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            lblError.Text = "";

            string sXmlOut = cmd.Parameters["@ALL_XML_OUT"].Value.ToString();

            string rc = "", rc2 = "";
            GetValueFromXml(sXmlOut, "ALL_XML_OUT/Enrolment", "RC", out rc2);
            GetValueFromXml(sXmlOut, "ALL_XML_OUT/RESULT", "RC", out rc);

            if (rc != null && rc != "" && rc != "0")
            {
                if (rc == "71008")
                {
                    Session.Remove("Authentication");
                    //Response.Redirect("RegistrationSearch.aspx");
                }
                string sError = "";
                GetValueFromXml(sXmlOut, "ALL_XML_OUT/RESULT", "ErrorLabel", out sError);
                lblError.Text = sError;
            }
            else if (rc2 != null && rc2 != "" && rc2 != "0")
            {
                if (rc2 == "77002")
                {
                    List<string> lsRetMessage = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Enrolment", "RetMessage");
                    string sMessage = (lsRetMessage.Count > 0 && !string.IsNullOrWhiteSpace(lsRetMessage[0])) ? lsRetMessage[0].ToString().Trim() : "";
                    string sCurrentPage = "RegistrationSearch.aspx";//HttpContext.Current.Request.RawUrl.Replace("/","");

                    tools.CheckUnauthorizedAccess(rc2, sMessage, sCurrentPage);
                }
                else
                {
                    string sError = "";
                    GetValueFromXml(sXmlOut, "ALL_XML_OUT/Enrolment", "ErrorLabel", out sError);
                    lblError.Text = sError;
                }
            }
            else
            {
                List<string> listRefTransaction = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Enrolment", "RefTransaction");
                if(listRefTransaction.Count > 0 && !string.IsNullOrWhiteSpace(listRefTransaction[0]))
                {
                    OnfidoResult1.RefTransaction = int.Parse(listRefTransaction[0]);
                    OnfidoResult1.RegistrationCode = sRegistrationNumber;
                }

                getDocumentsDetails(sXmlOut);

                string sFaceCapturePath = "";
                divFC.Visible = false;
                divParentFC.Visible = false;
                OcrFiles.getCaptureFacePath(lblRegistrationSelected.Text, out sFaceCapturePath);
                if (sFaceCapturePath.Trim().Length > 0)
                {

                    hfParentFaceCapturePath.Value = sFaceCapturePath;
                    if (divIDParent.Visible)
                    {
                        divParentFC.Visible = true;
                        hfParentFaceCapturePath.Value = sFaceCapturePath;
                    }
                    else
                    {
                        divFC.Visible = true;
                        hfFaceCapturePath.Value = sFaceCapturePath;
                    }
                }

                rRegistrationAttempts.DataSource = getRegistrationAttempts(sRegistrationNumber);
                rRegistrationAttempts.DataBind();
            }
        }
        catch (Exception ex)
        {
            lblError.Text = ex.ToString();
        }
        finally
        {
            if (lblError.Text.Trim().Length > 0)
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "showError();", true);
        }

    }

    protected void getDocumentResume(string RegistrationClosed, string sXML)
    {
        panelSubscriptionAgency.Visible = false;
        panelSubscriptionCashier.Visible = false;
        panelSubscriptionDate.Visible = false;
        panelSubscriptionMpadTime.Visible = false;
        panelSubscriptionTpeTime.Visible = false;
        panelSubscriptionPhoneNumber.Visible = false;

        string sRegistrationDate = ""; string sRegistrationPhoneNumber = "";
        string sMpadTime = ""; string sTpeTime = "";
        string sAgencyName = ""; string sAgencyCity = ""; string sCashierName = "";
        string sIsPreSubscription = "";

        GetValueFromXml(sXML, "ALL_XML_OUT/Enrolment", "PhoneNumber", out sRegistrationPhoneNumber);
        lblRegistrationPhoneNumber.Text = sRegistrationPhoneNumber;

        panelBtnShowClient.Visible = false;

        if (RegistrationClosed == "Y")
        {
            panelSubscriptionPhoneNumber.Visible = true;
            panelBtnShowClient.Visible = true;
        }
        else if (sRegistrationPhoneNumber.Trim().Length > 0)
            panelSubscriptionPhoneNumber.Visible = true;

        GetValueFromXml(sXML, "ALL_XML_OUT/Enrolment", "ElapsedTimeMPAD", out sMpadTime);
        GetValueFromXml(sXML, "ALL_XML_OUT/Enrolment", "ElapsedTimePOS", out sTpeTime);
        GetValueFromXml(sXML, "ALL_XML_OUT/Enrolment", "ClosedDate", out sRegistrationDate);
        GetValueFromXml(sXML, "ALL_XML_OUT/Enrolment", "AgencyName", out sAgencyName);
        GetValueFromXml(sXML, "ALL_XML_OUT/Enrolment", "AgencyCity", out sAgencyCity);
        GetValueFromXml(sXML, "ALL_XML_OUT/Enrolment", "CashierName", out sCashierName);

        GetValueFromXml(sXML, "ALL_XML_OUT/Enrolment", "IsPreSubscription", out sIsPreSubscription);

        imgRegistrationHW.AlternateText = "Souscription BORNE";
        imgRegistrationHW.ToolTip = "Souscription BORNE";
        lblRegistrationHW.Text = "BORNE";
        imgRegistrationHW.ImageUrl = @"~/Styles/Img/borne.png";
        if (sIsPreSubscription.Trim() == "1")
        {
            imgRegistrationHW.AlternateText = "Souscription WEB";
            imgRegistrationHW.ToolTip = "Souscription WEB";
            imgRegistrationHW.ImageUrl = @"~/Styles/Img/web-responsive.png";
            lblRegistrationHW.Text = "WEB";
        }

        if (sRegistrationDate.Length > 0)
        {
            panelSubscriptionDate.Visible = true;
            lblRegistrationDate.Text = sRegistrationDate;
        }

        if (sMpadTime.Length > 0 && sMpadTime != "00:00:00")
        {
            panelSubscriptionMpadTime.Visible = true;
            lblRegistrationMpadTime.Text = getDurationFormat(sMpadTime);
        }

        if (sTpeTime.Length > 0)
        {
            panelSubscriptionTpeTime.Visible = true;
            lblRegistrationTpeTime.Text = getDurationFormat(sTpeTime);
        }

        if (sAgencyName.Length > 0 || sAgencyCity.Length > 0)
        {
            panelSubscriptionAgency.Visible = true;
            lblRegistrationAgencyName.Text = sAgencyName;
            lblRegistrationAgencyCity.Text = sAgencyCity;
        }

        if (sCashierName.Length > 0)
        {
            panelSubscriptionCashier.Visible = true;
            lblRegistrationCashierName.Text = sCashierName;
        }
    }

    protected void getDocumentsDetails(string sXml)
    {
        int iNbDoc = 0;
        string sNbDoc = "";

        GetValueFromXml(sXml, "ALL_XML_OUT/Enrolment/DOCS", "Number", out sNbDoc);
        List<string> docList = CommonMethod.GetTags(sXml, "ALL_XML_OUT/Enrolment/DOCS/DOC");

        string sRegistrationClosed = "";
        GetValueFromXml(sXml, "ALL_XML_OUT/Enrolment", "Closed", out sRegistrationClosed);

        if (sRegistrationClosed.Trim() == "Y")
            hfRegistrationClosed.Value = "Y";
        else
            hfRegistrationClosed.Value = "N";
        // TEST
        //hfRegistrationClosed.Value = "N";

        getDocumentResume(sRegistrationClosed.Trim(), sXml);

        hfRegistrationChecked.Value = "N";
        string sCheckedStatus = ""; string sCheckedComment = ""; string sAgentCheck = ""; string sCheckedDate = "";
        GetValueFromXml(sXml, "ALL_XML_OUT/Enrolment", "BackOfficeOpinion", out sCheckedStatus);
        GetValueFromXml(sXml, "ALL_XML_OUT/Enrolment", "BackOfficeRemark", out sCheckedComment);
        GetValueFromXml(sXml, "ALL_XML_OUT/Enrolment", "BackOfficeUser", out sAgentCheck);
        GetValueFromXml(sXml, "ALL_XML_OUT/Enrolment", "BackOfficeOpinionDate", out sCheckedDate);

        string sIsPreSubscription = "";
        GetValueFromXml(sXml, "ALL_XML_OUT/Enrolment", "IsPreSubscription", out sIsPreSubscription);

        if(sIsPreSubscription == "1") { imgRegistrationHW.ImageUrl = "~/Styles/Img/web-responsive.png"; }

        lblRegistrationAgentCheck.Text = sAgentCheck;
        lblRegistrationCheckedDate.Text = sCheckedDate;
        panelRegistrationCheckedStatus.Visible = false;
        panelRegistrationAgentCheck.Visible = false;
        panelRegistrationAgentCheckDate.Visible = false;
        hfRegistrationCheckStatus.Value = sCheckedStatus;
        panelRegistrationNoCompliance.Visible = false;

        switch (sCheckedStatus)
        {
            case "G":
                panelRegistrationChecked.Visible = true;
                lblRegistrationChecked.Text = "OUI";
                lblRegistrationCheckedStatus.Text = "Conforme";
                lblRegistrationCheckedStatus.ForeColor = System.Drawing.Color.Green;
                imgRegistrationCheckedStatus.ImageUrl = "./Styles/Img/green_status.png"; ;
                imgRegistrationCheckedStatus.AlternateText = "(Conforme)";
                hfRegistrationChecked.Value = "Y";
                panelRegistrationCheckedStatus.Visible = true;
                panelRegistrationAgentCheck.Visible = true;
                panelRegistrationAgentCheckDate.Visible = true;
                txtRegistrationCheckedComment.Visible = false;
                lblRegistrationCheckedComment.Visible = true;
                break;
            case "O":
                panelRegistrationChecked.Visible = true;
                lblRegistrationChecked.Text = "OUI";
                lblRegistrationCheckedStatus.Text = "Suspecte";
                lblRegistrationCheckedStatus.ForeColor = System.Drawing.Color.Orange;
                imgRegistrationCheckedStatus.ImageUrl = "./Styles/Img/yellow_status.png"; ;
                imgRegistrationCheckedStatus.AlternateText = "(Suspecte)";
                hfRegistrationChecked.Value = "Y";
                panelRegistrationCheckedStatus.Visible = true;
                panelRegistrationAgentCheck.Visible = true;
                panelRegistrationAgentCheckDate.Visible = true;
                txtRegistrationCheckedComment.Visible = false;
                lblRegistrationCheckedComment.Visible = true;
                break;
            case "R":
                panelRegistrationChecked.Visible = true;
                lblRegistrationChecked.Text = "OUI";
                lblRegistrationCheckedStatus.Text = "Non conforme";
                lblRegistrationCheckedStatus.ForeColor = System.Drawing.Color.Red;
                imgRegistrationCheckedStatus.ImageUrl = "./Styles/Img/red_status.png"; ;
                imgRegistrationCheckedStatus.AlternateText = "(Non conforme)";
                hfRegistrationChecked.Value = "Y";
                panelRegistrationCheckedStatus.Visible = true;
                panelRegistrationAgentCheck.Visible = true;
                panelRegistrationAgentCheckDate.Visible = true;
                txtRegistrationCheckedComment.Visible = false;
                lblRegistrationCheckedComment.Visible = true;
                panelRegistrationNoCompliance.Visible = true;
                break;
            case "W":
                panelRegistrationChecked.Visible = false;
                lblRegistrationChecked.Text = "NON";
                lblRegistrationCheckedStatus.Text = "Non vérifiée";
                lblRegistrationCheckedStatus.ForeColor = System.Drawing.Color.Gray;
                imgRegistrationCheckedStatus.ImageUrl = "./Styles/Img/gray_status.png"; ;
                imgRegistrationCheckedStatus.AlternateText = "(Non vérifié)";
                hfRegistrationChecked.Value = "N";
                panelRegistrationCheckedStatus.Visible = true;
                panelRegistrationAgentCheck.Visible = false;
                panelRegistrationAgentCheckDate.Visible = false;
                txtRegistrationCheckedComment.Visible = true;
                lblRegistrationCheckedComment.Visible = false;
                break;
            default:
                panelRegistrationChecked.Visible = false;
                panelRegistrationAgentCheck.Visible = false;
                lblRegistrationChecked.Text = "NON";
                hfRegistrationChecked.Value = "N";
                panelRegistrationCheckedStatus.Visible = false;
                imgRegistrationCheckedStatus.ImageUrl = "./Styles/Img/black_status.png"; ;
                imgRegistrationCheckedStatus.AlternateText = "(Non vérifié)";
                txtRegistrationCheckedComment.Visible = true;
                lblRegistrationCheckedComment.Visible = false;
                break;
        }

        if (sCheckedComment.Trim().Length > 0)
        {
            panelRegistrationCheckedComment.Visible = true;
            lblRegistrationCheckedComment.Text = sCheckedComment.Trim();
        }
        else
            panelRegistrationCheckedComment.Visible = false;

        string sNoComplianceList = "";
        GetValueFromXml(sXml, "ALL_XML_OUT/Enrolment", "BackOfficeNoCompliance", out sNoComplianceList);
        hfNoComplianceSelectedList.Value = sNoComplianceList;
        rptGetNoComplianceList.DataSource = AML.getNoComplianceList();
        rptGetNoComplianceList.DataBind();

        if (int.TryParse(sNbDoc, out iNbDoc))
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("index");
            dt.Columns.Add("label");
            dt.Columns.Add("xml");

            for (int i = 0; i < iNbDoc; i++)
            {
                List<string> DocType = CommonMethod.GetAttributeValues(docList[i].ToString(), "DOC", "DocumentCategory");
                if (DocType.Count > 0)
                {
                    switch (DocType[0].Trim().ToUpper())
                    {
                        case "ID":
                            lblIDDocumentType.Text = CommonMethod.GetAttributeValues(docList[i].ToString(), "DOC", "DocumentTypeLabel")[0];
                            List<string> listTarget = CommonMethod.GetAttributeValues(docList[i], "DOC", "DocumentTarget");

                            if (listTarget.Count > 0 && listTarget[0] == "PARENT")
                            {
                                divIDParent.Visible = true;
                                lblIDParentDocumentType.Text = CommonMethod.GetAttributeValues(docList[i].ToString(), "DOC", "DocumentTypeLabel")[0] + " représentant légal";
                                FillIdDocumentParent(docList[i]);
                            }
                            else
                            {
                                FillIdDocument(docList[i]);

                                dt.Rows.Add(new object[] {
                                    CommonMethod.GetAttributeValues(docList[i].ToString(), "DOC", "IndexDoc")[0],
                                    string.Format("Document n°{0} - {1}",
                                    CommonMethod.GetAttributeValues(docList[i].ToString(), "DOC", "IndexDoc")[0],
                                    CommonMethod.GetAttributeValues(docList[i].ToString(), "DOC", "DocumentTypeLabel")[0]),
                                    docList[i]
                                });
                            }
                            break;
                        case "HO":
                            string sHoDocumentType = CommonMethod.GetAttributeValues(docList[i].ToString(), "DOC", "DocumentTypeLabel")[0];
                            lblHoDocumentType.Text = (sHoDocumentType.Trim().Length == 0) ? "Saisie manuelle (ADRESSE)" : sHoDocumentType;
                            divHoFactInfo.Visible = false;
                            FillHoDocument(docList[i]);
                            break;
                        case "FB":
                            divFB.Visible = true;
                            string sFileName = CommonMethod.GetAttributeValues(docList[i].ToString(), "DOC", "FileName")[0];
                            string sFamilyBookPath = ConfigurationManager.AppSettings["FamilyBookPath"].ToString();
                            if (sFileName.EndsWith(".pdf"))
                            {
                                if (File.Exists(System.Web.HttpContext.Current.Server.MapPath("~" + sFamilyBookPath + sFileName)))
                                {
                                    panelFBpdf.Visible = true;
                                    frameFBpdf.Attributes["src"] = sFamilyBookPath + sFileName;
                                }
                                else panelFBmissing.Visible = true;
                            }
                            else
                            {
                                string sFilePath = System.Web.HttpContext.Current.Server.MapPath("~" + sFamilyBookPath + sFileName);

                                if (File.Exists(sFilePath))
                                {
                                    panelFBjpg.Visible = true;

                                    if (sFileName.StartsWith("FB_01"))
                                        hfFB1Path.Value = sFamilyBookPath + sFileName;
                                    else hfFB2Path.Value = sFamilyBookPath + sFileName;
                                }
                                else panelFBmissing.Visible = true;
                            }
                            break;
                    }

                }
            }

            if(dt.Rows.Count > 1)
            {
                ddlDocs.Visible = true;
                ddlDocs.DataSource = ReverseRowsInDataTable(dt);
                ddlDocs.DataBind();
                dtDocs = dt;
            }
        }

        List<string> listParent = CommonMethod.GetTags(sXml, "ALL_XML_OUT/Enrolment/Parents/Parent");
        if (listParent.Count > 0)
        {
            divIDParent.Visible = true;
            panelDocInfos.Visible = false;
            panelClientInfos.Visible = true;

            lblIDParentDocumentType.Text = "Représentant légal";

            List<string> listRefCustomer = CommonMethod.GetAttributeValues(listParent[0], "Parent", "RefCustomer");
            List<string> listParentLastName = CommonMethod.GetAttributeValues(listParent[0], "Parent", "LastName");
            List<string> listParentFirstName = CommonMethod.GetAttributeValues(listParent[0], "Parent", "FirstName");
            List<string> listParentGender = CommonMethod.GetAttributeValues(listParent[0], "Parent", "Sex");
            List<string> listParentBirthDate = CommonMethod.GetAttributeValues(listParent[0], "Parent", "BirthDate");

            if (listParentLastName.Count > 0)
                lblParentClientLastName.Text = listParentLastName[0];
            if (listParentFirstName.Count > 0)
                lblParentClientFirstName.Text = listParentFirstName[0];
            if (listParentGender.Count > 0)
                lblParentClientGender.Text = listParentGender[0];
            if (listParentBirthDate.Count > 0)
                lblParentClientBirthDate.Text = listParentBirthDate[0];
        }

        List<string> kycList = CommonMethod.GetTags(sXml, "ALL_XML_OUT/Enrolment/KYC");
        if (kycList.Count > 0)
        {
            FillKycDocument(kycList[0]);
        }

        List<string> infoSupList = CommonMethod.GetTags(sXml, "ALL_XML_OUT/Enrolment/ADDITIONALINFO");
        if (infoSupList.Count > 0)
        {
            FillInfoSupDocument(infoSupList[0]);
        }
    }

    protected DataTable ReverseRowsInDataTable(DataTable inputTable)
    {
        DataTable outputTable = inputTable.Clone();

        for (int i = inputTable.Rows.Count - 1; i >= 0; i--)
            outputTable.ImportRow(inputTable.Rows[i]);

        return outputTable;
    }

    protected void ddlDocs_SelectedIndexChanged(object sender, EventArgs e)
    {
        for (int i = 0; i < dtDocs.Rows.Count; i++)
        {
            if(dtDocs.Rows[i]["index"].ToString() == ddlDocs.SelectedValue)
            {
                FillIdDocument(dtDocs.Rows[i]["xml"].ToString());
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), new Guid().ToString(), "mapIdObjects();", true);
                upDocDetails.Update();
                break;
            }
        }
    }

    protected void GetValueFromXml(string sXml, string sNode, string sAttribute, out string sValue)
    {
        sValue = "";

        List<string> tagList = CommonMethod.GetAttributeValues(sXml.ToString(), sNode, sAttribute);

        if (tagList.Count > 0)
            sValue = tagList[0];
    }

    protected void VisualCheck(WebControl ctrl, string check, string visualCheck, string modified)
    {
        try
        {
            if (modified.Trim().ToUpper() == "Y")
                ctrl.CssClass = "ClientModification ";
            else if (visualCheck.Trim().ToUpper() == "Y" || check.Trim().ToUpper() == "Y")
                ctrl.CssClass = "OcrChecked ";

            if (visualCheck.Trim().ToUpper() == "Y")
                ctrl.CssClass += "VisualChecked ";
            else
                ctrl.CssClass += "VisualNotChecked ";

            if (check.Trim().ToUpper() == "Y")
                ctrl.CssClass += "MrzChecked ";
            else
                ctrl.CssClass += "MrzNotCheked ";
        }
        catch (Exception ex)
        {
        }

    }

    protected void FillIdDocument(string IdDocXml)
    {
        lblIDDocumentType.Text = CommonMethod.GetAttributeValues(IdDocXml, "DOC", "DocumentTypeLabel")[0];

        WebControl ctrl = null;
        List<WebControl> ctrlToDisableList = new List<WebControl>();

        List<string> tagList = CommonMethod.GetTags(IdDocXml, "DOC/O");

        List<string> tagFilename = CommonMethod.GetAttributeValues(IdDocXml, "DOC", "FileName");
        List<string> tagDocType = CommonMethod.GetAttributeValues(IdDocXml, "DOC", "DocumentType");
        List<string> tagDocCategory = CommonMethod.GetAttributeValues(IdDocXml, "DOC", "DocumentCategory");
        List<string> tagNbPage = CommonMethod.GetAttributeValues(IdDocXml, "DOC", "");

        DataTable dt = GetCountryList();

        int iNbPages = 1;

        foreach (string tag in tagList)
        {
            List<string> infoList = CommonMethod.GetAttributeValues(tag, "O", "N");
            if (infoList.Count == 0)
                infoList = CommonMethod.GetAttributeValues(tag, "INFO", "N");
            List<string> valueList = CommonMethod.GetAttributeValues(tag, "O", "V");
            if (valueList.Count == 0)
                valueList = CommonMethod.GetAttributeValues(tag, "INFO", "V");
            List<string> modifiedList = CommonMethod.GetAttributeValues(tag, "O", "Modified");
            if (modifiedList.Count == 0)
                modifiedList = CommonMethod.GetAttributeValues(tag, "INFO", "Modified");
            List<string> checksumList = CommonMethod.GetAttributeValues(tag, "O", "C");
            if (checksumList.Count == 0)
                checksumList = CommonMethod.GetAttributeValues(tag, "INFO", "C ");
            List<string> visualcheckList = CommonMethod.GetAttributeValues(tag, "O", "VisualC");
            if (visualcheckList.Count == 0)
                visualcheckList = CommonMethod.GetAttributeValues(tag, "INFO", "VisualC");

            if (infoList.Count > 0 && infoList.Count == valueList.Count)
            {
                ctrl = null;

                switch (infoList[0])
                {
                    case "MZ1":
                        lblMRZ1.Text = valueList[0].Replace("_", "&lt;");
                        break;
                    case "MZ2":
                        lblMRZ2.Text = valueList[0].Replace("_", "&lt;");
                        break;
                    case "MZ3":
                        lblMRZ3.Text = valueList[0].Replace("_", "&lt;");
                        break;
                    case "MLN":
                        panelMarriedLastName.Visible = true;
                        txtIDMarriedLastName.Text = valueList[0];
                        txtIDMarriedLastName.Style.Add("display", "none");
                        lblIDMarriedLastName.Text = valueList[0];
                        lblIDMarriedLastName.ToolTip = valueList[0];
                        ctrl = lblIDMarriedLastName;
                        break;
                    case "LNA":
                        txtIDLastName.Style.Add("display", "none");
                        txtIDLastName.Text = valueList[0];
                        lblIDLastName.Text = valueList[0];
                        lblIDLastName.ToolTip = valueList[0];
                        ctrl = lblIDLastName;
                        break;
                    case "FNA":
                        txtIDFirstName.Text = valueList[0];
                        lblIDFirstName.Text = valueList[0];
                        lblIDFirstName.ToolTip = valueList[0];
                        txtIDFirstName.Style.Add("display", "none");
                        ctrl = lblIDFirstName;
                        break;
                    case "SEX":
                        rblGender.SelectedValue = valueList[0];
                        lblGender.Text = valueList[0];
                        rblGender.Style.Add("display", "none");
                        ctrl = lblGender;
                        break;
                    case "BDA":
                        txtBirthDate.Text = valueList[0];
                        lblBirthDate.Text = valueList[0];
                        txtBirthDate.Style.Add("display", "none");
                        ctrl = lblBirthDate;
                        break;
                    case "BPL":
                        txtBirthCity.Text = valueList[0];
                        lblBirthCity.Text = valueList[0];
                        lblBirthCity.ToolTip = valueList[0];
                        txtBirthCity.Style.Add("display", "none");
                        ctrl = lblBirthCity;
                        break;
                    case "BI2":
                        ddlBirthCountry.DataSource = dt;
                        ddlBirthCountry.DataBind();
                        if (ddlBirthCountry.Items.FindByValue(valueList[0]) != null)
                        {
                            ddlBirthCountry.SelectedValue = valueList[0];
                            lblBirthCountry.Text = ddlBirthCountry.SelectedItem.Text;
                            if (lblBirthCountry.Text.Trim().Length == 0)
                                lblBirthCountry.Text = "<span style=\"font-style:italic; text-transform:lowercase\">Non renseigné</span>";
                            lblBirthCountry.ToolTip = lblBirthCountry.Text;
                            ddlBirthCountry.Style.Add("display", "none");
                        }
                        ctrl = lblBirthCountry;
                        break;
                    case "DTN":
                        txtDocumentNumber.Text = valueList[0];
                        lblDocumentNumber.Text = valueList[0];
                        lblDocumentNumber.ToolTip = valueList[0];
                        txtDocumentNumber.Style.Add("display", "none");
                        ctrl = lblDocumentNumber;
                        break;
                    case "EDT":
                        txtExpirationDate.Text = valueList[0];
                        lblExpirationDate.Text = valueList[0];
                        txtExpirationDate.Style.Add("display", "none");
                        ctrl = lblExpirationDate;
                        break;
                    case "IDT":
                        txtIssuedID.Text = valueList[0];
                        lblIssuedID.Text = valueList[0];
                        txtIssuedID.Style.Add("display", "none");
                        ctrl = lblIssuedID;
                        break;
                    case "ICT":
                        ddlDocumentCountry.DataSource = dt;
                        ddlDocumentCountry.DataBind();
                        if (ddlDocumentCountry.Items.FindByValue(valueList[0]) != null)
                        {
                            ddlDocumentCountry.SelectedValue = valueList[0];
                            lblDocumentCountry.Text = ddlDocumentCountry.SelectedItem.Text;
                            lblDocumentCountry.ToolTip = lblDocumentCountry.Text;
                            ddlDocumentCountry.Style.Add("display", "none");
                        }
                        //ddlDocumentCountry.CssClass = "OcrChecked";
                        //ctrl = ddlDocumentCountry;
                        ctrl = lblDocumentCountry;
                        break;
                    case "IST":
                        //if (ddlDocumentState.Items.FindByValue(valueList[0]) != null)
                        //ddlDocumentState.SelectedValue = valueList[0];
                        //ctrl = ddlDocumentState;
                        break;
                    case "HLD":
                        //txtHolder.Text = valueList[0];
                        //ctrl = txtHolder;
                        break;

                    case "NBP":
                        if (!int.TryParse(valueList[0], out iNbPages))
                            iNbPages = 1;
                        break;

                    //RIB - NON UTILISE
                    #region
                    //case "BIC":
                    //txtBIC.Text = valueList[0];
                    //if (checkList[0] == "Y")
                    //{
                    //    ctrlToDisableList.Add(txtBIC);
                    //}
                    //string sBankName = "";
                    //string sCountry = "";
                    //string sPlace = "";

                    //int iRC = GetBICInfo(valueList[0], out sBankName, out sCountry, out sPlace);

                    //    if (iRC == 0)
                    //    {
                    //        if (sBankName.Length > 0)
                    //            lblBankName.Text = sBankName;
                    //        else panelBankName.Visible = false;

                    //        if (sCountry.Length > 0)
                    //            lblBankCountry.Text = sCountry;
                    //        else panelBankCountry.Visible = false;

                    //        if (sPlace.Length > 0)
                    //            lblBankPlace.Text = sPlace;
                    //        else panelBankPlace.Visible = false;
                    //    }
                    //    else
                    //    {
                    //        panelBICInfo.Visible = false;
                    //        panelBIC.CssClass = "";
                    //        panelBIC.Attributes.Remove("style");
                    //        lblCheckBIC.Text = "N";
                    //    }

                    //    ctrl = txtBIC;
                    //    break;
                    //case "IBA":
                    //    txtIBAN.Text = valueList[0];
                    //    //if (checkList[0] == "Y")
                    //    //{
                    //    //    ctrlToDisableList.Add(txtIBAN);
                    //    //}
                    //    ctrl = txtIBAN;
                    //    break;
                    //
                    #endregion
                }

                VisualCheck(ctrl, checksumList[0], visualcheckList[0], modifiedList[0]);
            }
        }

        string RectoFilePath = "", VersoFilePath = "", OriginalFilePath = "";
        bool isOriginalFilePresent = false, isCroppedFile1Present = false, isCroppedFile2Present = false;
        if (tagFilename.Count > 0 && tagDocType.Count > 0)
        {
            btnShowOriginalImage.Visible = true;
            btnGetArchivedImage.Visible = false;
            getFilePath(tagFilename[0], tagDocCategory[0], iNbPages, out RectoFilePath, out VersoFilePath,
                out OriginalFilePath, out isOriginalFilePresent, out isCroppedFile1Present, out isCroppedFile2Present);

            if (!isOriginalFilePresent)
                btnShowOriginalImage.Visible = false;
            if (!isCroppedFile1Present)
                btnGetArchivedImage.Visible = true;
        }

        if (iNbPages == 1)
            panelIdVerso.Visible = false;
        else
            panelIdVerso.Visible = true;

        hfIdRectoPath.Value = RectoFilePath;
        hfIdVersoPath.Value = VersoFilePath;
        btnShowOriginalImage.OnClientClick = "clickShowOriginalImage('" + "." + OriginalFilePath + "','Image originale', false); return false;";
    }
    protected void FillIdDocumentParent(string IdDocXml)
    {
        WebControl ctrl = null;
        List<WebControl> ctrlToDisableList = new List<WebControl>();

        List<string> tagList = CommonMethod.GetTags(IdDocXml, "DOC/O");

        List<string> tagFilename = CommonMethod.GetAttributeValues(IdDocXml, "DOC", "FileName");
        List<string> tagDocType = CommonMethod.GetAttributeValues(IdDocXml, "DOC", "DocumentType");
        List<string> tagDocCategory = CommonMethod.GetAttributeValues(IdDocXml, "DOC", "DocumentCategory");
        List<string> tagNbPage = CommonMethod.GetAttributeValues(IdDocXml, "DOC", "");

        DataTable dt = GetCountryList();

        int iNbPages = 1;

        foreach (string tag in tagList)
        {
            List<string> infoList = CommonMethod.GetAttributeValues(tag, "O", "N");
            if (infoList.Count == 0)
                infoList = CommonMethod.GetAttributeValues(tag, "INFO", "N");
            List<string> valueList = CommonMethod.GetAttributeValues(tag, "O", "V");
            if (valueList.Count == 0)
                valueList = CommonMethod.GetAttributeValues(tag, "INFO", "V");
            List<string> modifiedList = CommonMethod.GetAttributeValues(tag, "O", "Modified");
            if (modifiedList.Count == 0)
                modifiedList = CommonMethod.GetAttributeValues(tag, "INFO", "Modified");
            List<string> checksumList = CommonMethod.GetAttributeValues(tag, "O", "C");
            if (checksumList.Count == 0)
                checksumList = CommonMethod.GetAttributeValues(tag, "INFO", "C ");
            List<string> visualcheckList = CommonMethod.GetAttributeValues(tag, "O", "VisualC");
            if (visualcheckList.Count == 0)
                visualcheckList = CommonMethod.GetAttributeValues(tag, "INFO", "VisualC");

            if (infoList.Count > 0 && infoList.Count == valueList.Count)
            {
                ctrl = null;

                switch (infoList[0])
                {
                    case "MZ1":
                        lblMRZ1Parent.Text = valueList[0].Replace("_", "&lt;");
                        break;
                    case "MZ2":
                        lblMRZ2Parent.Text = valueList[0].Replace("_", "&lt;");
                        break;
                    case "MZ3":
                        lblMRZ3Parent.Text = valueList[0].Replace("_", "&lt;");
                        break;
                    case "MLN":
                        panelMarriedLastNameParent.Visible = true;
                        txtIDMarriedLastNameParent.Text = valueList[0];
                        txtIDMarriedLastNameParent.Style.Add("display", "none");
                        lblIDMarriedLastNameParent.Text = valueList[0];
                        lblIDMarriedLastNameParent.ToolTip = valueList[0];
                        ctrl = lblIDMarriedLastNameParent;
                        break;
                    case "LNA":
                        txtIDLastNameParent.Style.Add("display", "none");
                        txtIDLastNameParent.Text = valueList[0];
                        lblIDLastNameParent.Text = valueList[0];
                        lblIDLastNameParent.ToolTip = valueList[0];
                        ctrl = lblIDLastNameParent;
                        break;
                    case "FNA":
                        txtIDFirstNameParent.Text = valueList[0];
                        lblIDFirstNameParent.Text = valueList[0];
                        lblIDFirstNameParent.ToolTip = valueList[0];
                        txtIDFirstNameParent.Style.Add("display", "none");
                        ctrl = lblIDFirstNameParent;
                        break;
                    case "SEX":
                        rblGenderParent.SelectedValue = valueList[0];
                        lblGenderParent.Text = valueList[0];
                        rblGenderParent.Style.Add("display", "none");
                        ctrl = lblGenderParent;
                        break;
                    case "BDA":
                        txtBirthDateParent.Text = valueList[0];
                        lblBirthDateParent.Text = valueList[0];
                        txtBirthDateParent.Style.Add("display", "none");
                        ctrl = lblBirthDateParent;
                        break;
                    case "BPL":
                        txtBirthCityParent.Text = valueList[0];
                        lblBirthCityParent.Text = valueList[0];
                        lblBirthCityParent.ToolTip = valueList[0];
                        txtBirthCityParent.Style.Add("display", "none");
                        ctrl = lblBirthCityParent;
                        break;
                    case "BI2":
                        ddlBirthCountryParent.DataSource = dt;
                        ddlBirthCountryParent.DataBind();
                        if (ddlBirthCountryParent.Items.FindByValue(valueList[0]) != null)
                        {
                            ddlBirthCountryParent.SelectedValue = valueList[0];
                            lblBirthCountryParent.Text = ddlBirthCountryParent.SelectedItem.Text;
                            if (lblBirthCountryParent.Text.Trim().Length == 0)
                                lblBirthCountryParent.Text = "<span style=\"font-style:italic; text-transform:lowercase\">Non renseigné</span>";
                            lblBirthCountryParent.ToolTip = lblBirthCountryParent.Text;
                            ddlBirthCountryParent.Style.Add("display", "none");
                        }
                        ctrl = lblBirthCountryParent;
                        break;
                    case "DTN":
                        txtDocumentNumberParent.Text = valueList[0];
                        lblDocumentNumberParent.Text = valueList[0];
                        lblDocumentNumberParent.ToolTip = valueList[0];
                        txtDocumentNumberParent.Style.Add("display", "none");
                        ctrl = lblDocumentNumberParent;
                        break;
                    case "EDT":
                        txtExpirationDateParent.Text = valueList[0];
                        lblExpirationDateParent.Text = valueList[0];
                        txtExpirationDateParent.Style.Add("display", "none");
                        ctrl = lblExpirationDateParent;
                        break;
                    case "IDT":
                        txtIssuedIDParent.Text = valueList[0];
                        lblIssuedIDParent.Text = valueList[0];
                        txtIssuedIDParent.Style.Add("display", "none");
                        ctrl = lblIssuedIDParent;
                        break;
                    case "ICT":
                        ddlDocumentCountryParent.DataSource = dt;
                        ddlDocumentCountryParent.DataBind();
                        if (ddlDocumentCountryParent.Items.FindByValue(valueList[0]) != null)
                        {
                            ddlDocumentCountryParent.SelectedValue = valueList[0];
                            lblDocumentCountryParent.Text = ddlDocumentCountryParent.SelectedItem.Text;
                            lblDocumentCountryParent.ToolTip = lblDocumentCountryParent.Text;
                            ddlDocumentCountryParent.Style.Add("display", "none");
                        }
                        //ddlDocumentCountry.CssClass = "OcrChecked";
                        //ctrl = ddlDocumentCountry;
                        ctrl = lblDocumentCountryParent;
                        break;
                    case "IST":
                        //if (ddlDocumentState.Items.FindByValue(valueList[0]) != null)
                        //ddlDocumentState.SelectedValue = valueList[0];
                        //ctrl = ddlDocumentState;
                        break;
                    case "HLD":
                        //txtHolder.Text = valueList[0];
                        //ctrl = txtHolder;
                        break;

                    case "NBP":
                        if (!int.TryParse(valueList[0], out iNbPages))
                            iNbPages = 1;
                        break;

                    //RIB - NON UTILISE
                    #region
                    //case "BIC":
                    //txtBIC.Text = valueList[0];
                    //if (checkList[0] == "Y")
                    //{
                    //    ctrlToDisableList.Add(txtBIC);
                    //}
                    //string sBankName = "";
                    //string sCountry = "";
                    //string sPlace = "";

                    //int iRC = GetBICInfo(valueList[0], out sBankName, out sCountry, out sPlace);

                    //    if (iRC == 0)
                    //    {
                    //        if (sBankName.Length > 0)
                    //            lblBankName.Text = sBankName;
                    //        else panelBankName.Visible = false;

                    //        if (sCountry.Length > 0)
                    //            lblBankCountry.Text = sCountry;
                    //        else panelBankCountry.Visible = false;

                    //        if (sPlace.Length > 0)
                    //            lblBankPlace.Text = sPlace;
                    //        else panelBankPlace.Visible = false;
                    //    }
                    //    else
                    //    {
                    //        panelBICInfo.Visible = false;
                    //        panelBIC.CssClass = "";
                    //        panelBIC.Attributes.Remove("style");
                    //        lblCheckBIC.Text = "N";
                    //    }

                    //    ctrl = txtBIC;
                    //    break;
                    //case "IBA":
                    //    txtIBAN.Text = valueList[0];
                    //    //if (checkList[0] == "Y")
                    //    //{
                    //    //    ctrlToDisableList.Add(txtIBAN);
                    //    //}
                    //    ctrl = txtIBAN;
                    //    break;
                    //
                    #endregion


                }

                VisualCheck(ctrl, checksumList[0], visualcheckList[0], modifiedList[0]);
            }
        }

        string RectoFilePath = "", VersoFilePath = "", OriginalFilePath = "";
        bool isOriginalFilePresent = false, isCroppedFile1Present = false, isCroppedFile2Present = false;
        if (tagFilename.Count > 0 && tagDocType.Count > 0)
        {
            btnShowOriginalImageParent.Visible = true;
            btnGetArchivedImage.Visible = false;
            getFilePath(tagFilename[0], tagDocCategory[0], iNbPages, out RectoFilePath, out VersoFilePath,
                out OriginalFilePath, out isOriginalFilePresent, out isCroppedFile1Present, out isCroppedFile2Present);

            if (!isOriginalFilePresent)
                btnShowOriginalImageParent.Visible = false;
            if (!isCroppedFile1Present)
                btnGetArchivedImageParent.Visible = true;
        }

        if (iNbPages == 1)
            panelIdParentVerso.Visible = false;
        else
            panelIdParentVerso.Visible = true;

        hfIdParentRectoPath.Value = RectoFilePath;
        hfIdParentVersoPath.Value = VersoFilePath;
        btnShowOriginalImageParent.OnClientClick = "clickShowOriginalImage('" + "." + OriginalFilePath + "','Image originale', false); return false;";
    }
    protected void FillHoDocument(string HoDocXml)
    {
        WebControl ctrl = null;
        List<WebControl> ctrlToDisableList = new List<WebControl>();

        List<string> tagList = CommonMethod.GetTags(HoDocXml, "DOC/O");

        List<string> Filename = CommonMethod.GetAttributeValues(HoDocXml, "DOC", "FileName");

        List<string> tagFilename = CommonMethod.GetAttributeValues(HoDocXml, "DOC", "FileName");
        List<string> tagDocType = CommonMethod.GetAttributeValues(HoDocXml, "DOC", "DocumentType");
        List<string> tagDocCategory = CommonMethod.GetAttributeValues(HoDocXml, "DOC", "DocumentCategory");
        List<string> tagNbPage = CommonMethod.GetAttributeValues(HoDocXml, "DOC", "");

        int iNbPages = 1;

        foreach (string tag in tagList)
        {
            List<string> infoList = CommonMethod.GetAttributeValues(tag, "O", "N");
            if (infoList.Count == 0)
                infoList = CommonMethod.GetAttributeValues(tag, "INFO", "N");
            List<string> valueList = CommonMethod.GetAttributeValues(tag, "O", "V");
            if (valueList.Count == 0)
                valueList = CommonMethod.GetAttributeValues(tag, "INFO", "V");
            List<string> modifiedList = CommonMethod.GetAttributeValues(tag, "O", "Modified");
            if (modifiedList.Count == 0)
                modifiedList = CommonMethod.GetAttributeValues(tag, "INFO", "Modified");
            List<string> checksumList = CommonMethod.GetAttributeValues(tag, "O", "C");
            if (checksumList.Count == 0)
                checksumList = CommonMethod.GetAttributeValues(tag, "INFO", "C ");
            List<string> visualcheckList = CommonMethod.GetAttributeValues(tag, "O", "VisualC");
            if (visualcheckList.Count == 0)
                visualcheckList = CommonMethod.GetAttributeValues(tag, "INFO", "VisualC");

            if (infoList.Count > 0 && infoList.Count == valueList.Count)
            {
                ctrl = null;

                switch (infoList[0])
                {
                    case "SDR":
                        txtBiller.Text = valueList[0];
                        lblBiller.Text = valueList[0];
                        lblBiller.ToolTip = valueList[0];
                        txtBiller.Style.Add("display", "none");
                        ctrl = lblBiller;
                        break;
                    case "TO":
                        txtTo.Text = valueList[0];
                        txtTo.Style.Add("display","none");
                        lblTo.Text = valueList[0];
                        lblTo.ToolTip = valueList[0];
                        ctrl = lblTo;
                        break;
                    case "IDT":
                        txtIssuedFact.Text = valueList[0];
                        lblIssuedFact.Text = valueList[0];
                        lblIssuedFact.ToolTip = valueList[0];
                        txtIssuedFact.Style.Add("display", "none");
                        ctrl = lblIssuedFact;
                        break;
                    case "AD1":
                        txtAddress1.Text = valueList[0];
                        lblAddress1.Text = valueList[0];
                        lblAddress1.ToolTip = valueList[0];
                        txtAddress1.Style.Add("display", "none");
                        ctrl = lblAddress1;
                        break;
                    case "AD2":
                        txtAddress2.Text = valueList[0];
                        lblAddress2.Text = valueList[0];
                        lblAddress2.ToolTip = valueList[0];
                        txtAddress2.Style.Add("display", "none");
                        ctrl = lblAddress2;
                        break;
                    case "ZIP":
                        txtZipCode.Text = valueList[0];
                        lblZipCode.Text = valueList[0];
                        txtZipCode.Style.Add("display", "none");
                        ctrl = lblZipCode;
                        break;
                    case "CIT":
                        txtCity.Text = valueList[0];
                        lblCity.Text = valueList[0];
                        lblCity.ToolTip = valueList[0];
                        txtCity.Style.Add("display", "none");
                        ctrl = lblCity;
                        break;
                    case "RTS":
                        //if (ddlResidentStatus.Items.FindByValue(valueList[0]) != null)
                        //ddlResidentStatus.SelectedValue = valueList[0];
                        break;
                    case "NBP":
                        if (!int.TryParse(valueList[0], out iNbPages))
                            iNbPages = 1;
                        break;
                }

                VisualCheck(ctrl, checksumList[0], visualcheckList[0], modifiedList[0]);
            }
        }

        string RectoFilePath = "", VersoFilePath = "", OriginalFilePath = "";
        bool isOriginalFilePresent = false, isCroppedFile1Present = false, isCroppedFile2Present = false;
        if (tagFilename.Count > 0 && tagDocType.Count > 0)
        {
            //btnShowOriginalImage.Visible = true;
            //btnGetArchivedImage.Visible = false;
            getFilePath(tagFilename[0], tagDocCategory[0], iNbPages, out RectoFilePath, out VersoFilePath,
                out OriginalFilePath, out isOriginalFilePresent, out isCroppedFile1Present, out isCroppedFile2Present);
        }

        hfHoPath.Value = RectoFilePath;
    }
    protected void FillKycDocument(string KycDocXml)
    {
        int iNbColumnToFill = 3;
        int iColumnToFill = 1;

        List<string> listKycAnswerLabel = CommonMethod.GetAttributeValues(KycDocXml, "KYC/I", "AnswerLabel");
        List<string> listKycAnswerValue = CommonMethod.GetAttributeValues(KycDocXml, "KYC/I", "AnswerValue");
        List<string> listKycQuestions = CommonMethod.GetAttributeValues(KycDocXml, "KYC/I", "QuestionLabel");
        List<string> listKycRefQuestions = CommonMethod.GetAttributeValues(KycDocXml, "KYC/I", "RefQuestion");

        string sRefQuestion = "";
        string sLabel = "";
        string sAnswer = "";

        TextBox txtTextbox = null;
        Label lblLabel = null;

        for (int i = 0; i < listKycQuestions.Count; i++)
        {
            sRefQuestion = listKycRefQuestions[i];
            sLabel = listKycQuestions[i];

            if (listKycAnswerLabel[i] != null && listKycAnswerLabel[i] != "")
                sAnswer = listKycAnswerLabel[i];
            else
                sAnswer = listKycAnswerValue[i];

            txtTextbox = new TextBox();
            string sControlID = "txtQuestion" + sRefQuestion;
            txtTextbox.ID = sControlID;
            txtTextbox.ReadOnly = true;
            txtTextbox.Text = sAnswer;

            lblLabel = new Label();
            lblLabel.ID = "lblQuestion" + sRefQuestion;
            lblLabel.AssociatedControlID = sControlID;
            lblLabel.Text = sLabel;
            lblLabel.CssClass = "label";

            Panel panelQuestion = new Panel();
            Panel panelLabel = new Panel();

            panelLabel.Controls.Add(lblLabel);
            panelQuestion.Controls.Add(txtTextbox);

            switch (iColumnToFill)
            {
                case 1:
                    iColumnToFill = updateColumnToFill(iColumnToFill, iNbColumnToFill);
                    panelKYCHolderLeft.Controls.Add(panelLabel);
                    panelKYCHolderLeft.Controls.Add(panelQuestion);

                    break;
                case 2:
                    iColumnToFill = updateColumnToFill(iColumnToFill, iNbColumnToFill);
                    panelKYCHolderMiddle.Controls.Add(panelLabel);
                    panelKYCHolderMiddle.Controls.Add(panelQuestion);
                    break;
                case 3:
                    iColumnToFill = updateColumnToFill(iColumnToFill, iNbColumnToFill);
                    panelKYCHolderRight.Controls.Add(panelLabel);
                    panelKYCHolderRight.Controls.Add(panelQuestion);
                    break;
                default:
                    panelKYCHolderLeft.Controls.Add(panelLabel);
                    panelKYCHolderLeft.Controls.Add(panelQuestion);
                    iColumnToFill = 1;
                    break;
            }
        }
    }
    protected void FillInfoSupDocument(string InfoSupXml)
    {
        List<string> tagList = CommonMethod.GetTags(InfoSupXml, "ADDITIONALINFO/I");

        foreach (string tag in tagList)
        {
            List<string> infoList = CommonMethod.GetAttributeValues(tag, "I", "N");
            List<string> valueList = CommonMethod.GetAttributeValues(tag, "I", "V");

            if (infoList.Count > 0 && infoList.Count == valueList.Count)
            {
                switch (infoList[0])
                {
                    case "AMOUNT":
                        txtDepositAmount.Text = valueList[0] + " EUR";
                        break;
                    case "PACK_SALE":
                        if (valueList[0].Trim().ToUpper() == "Y")
                            txtPackSaleWithActivation.Text = "OUI";
                        else
                            txtPackSaleWithActivation.Text = "NON";
                        break;
                    case "CARD":
                        txtCardNumber.Text = valueList[0];
                        lblPackNumberSelected.Text = valueList[0];
                        break;

                }
            }
        }
    }

    protected int updateColumnToFill(int iColumnToFill, int iNbColumnToFill)
    {
        if (iColumnToFill < iNbColumnToFill)
            iColumnToFill++;
        else iColumnToFill = 1;

        return iColumnToFill;
    }
    protected DataTable GetCountryList()
    {
        SqlConnection conn = null;
        DataTable dt = new DataTable();

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["UniversalID"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("Other.P_GetCountryList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@CountryRequest", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@CountryList", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@ReturnSelect", SqlDbType.Bit);

            cmd.Parameters["@CountryRequest"].Value = new XElement("CountryRequest",
                                                            new XAttribute("Order", "DESC"),
                                                            new XAttribute("ByNbWords", "1"),
                                                            new XAttribute("Culture", (Session["Culture"] != null) ? Session["Culture"].ToString() : "fr-FR")).ToString();
            cmd.Parameters["@ReturnSelect"].Value = 1;
            cmd.Parameters["@CountryList"].Direction = ParameterDirection.Output;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;

            da.Fill(dt);
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }
    protected void getFilePath(string OriginalFileName, string DocType, int NbPages, out string FilePath1, out string FilePath2, out string OriginalFilePath,
        out bool isOriginalFilePresent, out bool isCroppedFile1Present, out bool isCroppedFile2Present)
    {
        FilePath1 = "";
        FilePath2 = "";
        OriginalFilePath = "";
        isOriginalFilePresent = false;
        isCroppedFile1Present = false;
        isCroppedFile2Present = false;

        OcrFiles _ocrFiles = new OcrFiles();
        _ocrFiles.ABBYYOriginalPath = ConfigurationManager.AppSettings["ABBYYOriginalPath"].ToString();
        _ocrFiles.ABBYYCroppedPath = ConfigurationManager.AppSettings["ABBYYCroppedPath"].ToString();
        _ocrFiles.ICARCroppedPath = ConfigurationManager.AppSettings["ICARCroppedPath"].ToString();
        _ocrFiles.ABBYYFilePath = ConfigurationManager.AppSettings["ABBYYFilePath"].ToString();
        _ocrFiles.ErrorFilePath = ConfigurationManager.AppSettings["ErrorPath"].ToString();
        _ocrFiles.ICAROriginalPath = ConfigurationManager.AppSettings["ICAROriginalPath"].ToString();
        _ocrFiles.OnfidoFilePath = ConfigurationManager.AppSettings["OnfidoFilePath"].ToString();
        _ocrFiles.NbPages = NbPages;
        _ocrFiles.DocType = DocType;
        _ocrFiles.OriginalFileName = OriginalFileName;
        _ocrFiles.getCroppedFilePath(out FilePath1, out FilePath2, out isCroppedFile1Present, out isCroppedFile2Present);

        string FileNameWithoutExtension = OriginalFileName.Split('.')[0];

        OriginalFilePath = _ocrFiles.ICAROriginalPath + FileNameWithoutExtension + ".jpg";
        if (File.Exists(System.Web.HttpContext.Current.Server.MapPath("~" + _ocrFiles.ICAROriginalPath + FileNameWithoutExtension + ".jpg")))
            isOriginalFilePresent = true;

        if (NbPages > 1)
        {
            if (File.Exists(System.Web.HttpContext.Current.Server.MapPath("~" + _ocrFiles.ABBYYOriginalPath + FileNameWithoutExtension + ".pdf")))
            {
                isOriginalFilePresent = true;
                OriginalFilePath = _ocrFiles.ABBYYOriginalPath + OriginalFileName;
            }
            else if (File.Exists(System.Web.HttpContext.Current.Server.MapPath("~" + _ocrFiles.ErrorFilePath + FileNameWithoutExtension + ".pdf")))
            {
                isOriginalFilePresent = true;
                OriginalFilePath = _ocrFiles.ErrorFilePath + OriginalFileName;
            }
        }
        else
        {
            if (File.Exists(System.Web.HttpContext.Current.Server.MapPath("~" + _ocrFiles.ABBYYOriginalPath + FileNameWithoutExtension + ".jpg")))
            {
                isOriginalFilePresent = true;
                OriginalFilePath = _ocrFiles.ABBYYOriginalPath + OriginalFileName;
            }
            else if (File.Exists(System.Web.HttpContext.Current.Server.MapPath("~" + _ocrFiles.ErrorFilePath + FileNameWithoutExtension + ".jpg")))
            {
                isOriginalFilePresent = true;
                OriginalFilePath = _ocrFiles.ErrorFilePath + OriginalFileName;
            }
        }
    }
    protected void GetCGV(string sRegistrationCode, out string sFilename)
    {
        SqlConnection conn = null;
        sFilename = "";

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["PARTNER"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("Registration.P_GetCGV", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN", new XElement("Info", new XAttribute("RegistrationCode", sRegistrationCode))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sOut = cmd.Parameters["@OUT"].Value.ToString();

            if (sOut.Length > 0)
            {
                List<string> listFilename = CommonMethod.GetAttributeValues(sOut, "ALL_XML_OUT/I", "FileName");
                if (listFilename.Count > 0)
                    sFilename = listFilename[0];
            }
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }
    }
    protected void getPinBySmsInformation(string sRegistrationCode)
    {
        SqlConnection conn = null;
        authentication auth = authentication.GetCurrent();

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("BankSI.P_GetPinBySMSInformation", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@FindPinBySMSCriteria", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@PinBySMSInformation", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@ReturnSelect", SqlDbType.Bit);
            cmd.Parameters.Add("@RC", SqlDbType.Int);

            string sXmlIN = new XElement("ALL_XML_IN",
                                                new XElement("PinBySMS",
                                                    new XAttribute("RegistrationCode", sRegistrationCode),
                                                    new XAttribute("CashierToken", auth.sToken),
                                                    new XAttribute("CultureID", (Session["CultureID"] != null) ? Session["CultureID"].ToString() : "Fr-fr")
                                                )
                                            ).ToString();

            cmd.Parameters["@FindPinBySMSCriteria"].Value = sXmlIN;
            cmd.Parameters["@ReturnSelect"].Value = false;

            cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;
            cmd.Parameters["@PinBySMSInformation"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sOut = cmd.Parameters["@PinBySMSInformation"].Value.ToString();

            if (sOut.Length > 0)
            {
                getPinBySmsInformationFromXml(sOut);
            }
            else
            {
                divPinBySms.Visible = false;
            }
        }
        catch (Exception e)
        {
            ImgPinBySmsStatus.AlternateText = "Inconnu";
            ImgPinBySmsStatus.ImageUrl = "./Styles/Img/black_status.png";
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

    }
    protected void getPinBySmsInformationFromXml(string sXml)
    {
        List<string> listExceptingPhoneNumber = CommonMethod.GetAttributeValues(sXml, "ALL_XML_OUT/PinBySMS", "ExpectingPhoneNumber");
        if (listExceptingPhoneNumber.Count > 0)
            txtExpectingPhoneNumber.Text = listExceptingPhoneNumber[0];

        List<string> listSuccessfullCheckDate = CommonMethod.GetAttributeValues(sXml, "ALL_XML_OUT/PinBySMS", "SuccessfullCheckDate");
        if (listSuccessfullCheckDate.Count > 0)
            txtSuccessfullCheckDate.Text = listSuccessfullCheckDate[0];

        List<string> listSMSElapsedTime = CommonMethod.GetAttributeValues(sXml, "ALL_XML_OUT/PinBySMS", "ElapsedTimeSMS");
        if (listSMSElapsedTime.Count > 0 && listSMSElapsedTime[0].Trim().Length > 0)
        {
            string[] SmsElapsedTime = listSMSElapsedTime[0].Split(':');

            string SmsElapsedTimeHH = (SmsElapsedTime[0].Trim() == "00") ? "" : SmsElapsedTime[0].Trim() + "h";
            string SmsElapsedTimeMM = (SmsElapsedTime[1].Trim() == "00") ? "" : SmsElapsedTime[1].Trim() + "min";
            txtSmsElapsedTime.Text = SmsElapsedTimeHH + SmsElapsedTimeMM + SmsElapsedTime[2] + "s";
            panelSmsElapsedTime.Visible = true;
        }
        else
        {
            panelSmsElapsedTime.Visible = false;
        }

        List<string> listSMSSent = CommonMethod.GetAttributeValues(sXml, "ALL_XML_OUT/PinBySMS", "SMSSent");
        if (listSMSSent.Count > 0)
        {
            if (listSMSSent[0] == "1")
            {
                txtSmsSent.Text = "OUI";
                ImgPinBySmsStatus.ImageUrl = "./Styles/Img/green_status.png";
            }
            else
            {
                txtSmsSent.Text = "NON";
                ImgPinBySmsStatus.ImageUrl = "./Styles/Img/red_status.png";
            }
        }
        else
        {
            txtSmsSent.Text = "NON";
            ImgPinBySmsStatus.ImageUrl = "./Styles/Img/red_status.png";
        }
        ImgPinBySmsStatus.AlternateText = txtSmsSent.Text;

        List<string> listNbAttempts = CommonMethod.GetAttributeValues(sXml, "ALL_XML_OUT/PinBySMS/Attempts", "NbAttempts");
        if (listNbAttempts.Count > 0)
            txtNbAttempts.Text = listNbAttempts[0];

        int nbAttempts = 0;
        DataTable dtAttemps = new DataTable();
        dtAttemps.Columns.Add("PhoneNumber");
        dtAttemps.Columns.Add("ChallengeReceived");
        dtAttemps.Columns.Add("Date");
        dtAttemps.Columns.Add("Status");
        dtAttemps.Columns.Add("StatusColor");

        if (int.TryParse(listNbAttempts[0], out nbAttempts) && nbAttempts > 0)
        {
            string sAttemptPhoneNumber = "", sAttemptChallengeReceived = "", sAttemptsDate = "", sAttemptError = "", sAttemptStatusColor = "";
            List<string> listAttemptPhoneNumber = new List<string>();
            List<string> listAttemptChallengeReceived = new List<string>();
            List<string> listAttemptDate = new List<string>();
            List<string> listAttemptRC = new List<string>();

            List<string> listAttempts = CommonMethod.GetTags(sXml, "ALL_XML_OUT/PinBySMS/Attempts/Attempt");
            for (int i = 0; i < listAttempts.Count; i++)
            {
                listAttemptPhoneNumber = CommonMethod.GetAttributeValues(listAttempts[i], "Attempt", "PhoneNumber");
                listAttemptChallengeReceived = CommonMethod.GetAttributeValues(listAttempts[i], "Attempt", "ChallengeReceived");
                listAttemptDate = CommonMethod.GetAttributeValues(listAttempts[i], "Attempt", "AttemptDate");
                listAttemptRC = CommonMethod.GetAttributeValues(listAttempts[i], "Attempt", "ResultCode");

                if (listAttemptPhoneNumber.Count > 0)
                    sAttemptPhoneNumber = listAttemptPhoneNumber[0];

                if (listAttemptChallengeReceived.Count > 0)
                    sAttemptChallengeReceived = listAttemptChallengeReceived[0];

                if (listAttemptDate.Count > 0)
                    sAttemptsDate = listAttemptDate[0];

                if (listAttemptRC.Count > 0 && listAttemptRC[0].Trim() != "0")
                {
                    List<string> listAttemptError = CommonMethod.GetAttributeValues(listAttempts[i], "Attempt", "ResultSignification");
                    if (listAttemptError.Count > 0)
                    {
                        sAttemptError = listAttemptError[0] + " (" + listAttemptRC[0] + ")";

                        sAttemptStatusColor = "Red";
                    }
                }
                else
                {
                    sAttemptError = "OK";
                    sAttemptStatusColor = "Green";
                }

                dtAttemps.Rows.Add(new object[] { sAttemptPhoneNumber, sAttemptChallengeReceived, sAttemptsDate, sAttemptError, sAttemptStatusColor });
            }


            //divBtnAttemptDetails.Visible = true;
            btnAttemptDetails.Visible = true;
        }
        else
        {
            //divBtnAttemptDetails.Visible = false;
            btnAttemptDetails.Visible = false;
        }

        gvAttemptDetails.DataSource = dtAttemps;
        gvAttemptDetails.DataBind();
    }
    protected System.Drawing.Color getColor(string color)
    {
        System.Drawing.Color dColor;
        dColor = System.Drawing.Color.FromName(color);
        return dColor;
    }
    protected void getWebPasswordBySmsInformation(string sRegistrationCode)
    {
        SqlConnection conn = null;
        authentication auth = authentication.GetCurrent();

        try
        {

            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("BankSI.P_GetWebPasswordSentInformation", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@FindWebPasswordCriteria", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@WebPasswordInformation", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@ReturnSelect", SqlDbType.Bit);
            cmd.Parameters.Add("@RC", SqlDbType.Int);

            string sXmlIN = new XElement("ALL_XML_IN",
                                                new XElement("WebPassword",
                                                    new XAttribute("RegistrationCode", sRegistrationCode),
                                                    new XAttribute("CashierToken", auth.sToken),
                                                    new XAttribute("CultureID", (Session["CultureID"] != null) ? Session["CultureID"].ToString() : "Fr-fr")
                                                )
                                            ).ToString();

            cmd.Parameters["@FindWebPasswordCriteria"].Value = sXmlIN;
            cmd.Parameters["@ReturnSelect"].Value = false;

            cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;
            cmd.Parameters["@WebPasswordInformation"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sOut = cmd.Parameters["@WebPasswordInformation"].Value.ToString();

            if (sOut.Length > 0)
            {
                getWebPasswordBySmsInformationFromXml(sOut);
            }
            else
            {
                divWebPassword.Visible = false;
            }
        }
        catch (Exception e)
        {
            ImgWebPassBySmsStatus.AlternateText = "Inconnu";
            ImgWebPassBySmsStatus.ImageUrl = "./Styles/Img/black_status.png";
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }
    }
    protected void getWebPasswordBySmsInformationFromXml(string sXml)
    {
        List<string> listCustomerPhoneNumber = CommonMethod.GetAttributeValues(sXml, "ALL_XML_OUT/WebPassword", "CustomerPhoneNumber");
        if (listCustomerPhoneNumber.Count > 0)
            txtWebPasswordPhoneNumber.Text = listCustomerPhoneNumber[0];

        List<string> listSmsSent = CommonMethod.GetAttributeValues(sXml, "ALL_XML_OUT/WebPassword", "SMSSent");
        if (listSmsSent.Count > 0 && listSmsSent[0].Trim() != "")
        {
            txtWebPasswordSmsSent.Text = (listSmsSent[0] == "1") ? "OUI" : "NON";

            if (listSmsSent[0] == "1")
            {
                txtWebPasswordSmsSent.Text = "OUI";
                ImgWebPassBySmsStatus.ImageUrl = "./Styles/Img/green_status.png";
            }
            else
            {
                txtWebPasswordSmsSent.Text = "NON";
                ImgWebPassBySmsStatus.ImageUrl = "./Styles/Img/red_status.png";
            }

            List<string> listLastSentDate = CommonMethod.GetAttributeValues(sXml, "ALL_XML_OUT/WebPassword", "LastSentDate");
            if (listLastSentDate.Count > 0)
                txtWebPasswordLastSentDate.Text = listLastSentDate[0];

            List<string> listLastReceiveDate = CommonMethod.GetAttributeValues(sXml, "ALL_XML_OUT/WebPassword", "LastReceiveDate");
            if (listLastReceiveDate.Count > 0 && listLastReceiveDate[0].Trim() != "")
            {
                txtWebPasswordLastReceiveDate.Text = listLastReceiveDate[0];
                txtWebPasswordLastReceiveDate.Style.Remove("font-style");
                txtWebPasswordLastReceiveDate.Style.Remove("color");
            }
            else
            {
                txtWebPasswordLastReceiveDate.Text = "INCONNU";
                txtWebPasswordLastReceiveDate.Style.Add("font-style", "italic");
                txtWebPasswordLastReceiveDate.Style.Add("color", "#AAA");
            }
        }
        else
        {
            txtWebPasswordSmsSent.Text = "NON";
            txtWebPasswordLastSentDate.Text = "";
            txtWebPasswordLastReceiveDate.Text = "";
            ImgWebPassBySmsStatus.ImageUrl = "./Styles/Img/red_status.png";
        }

        ImgWebPassBySmsStatus.AlternateText = txtWebPasswordSmsSent.Text;
    }
    protected DataTable getRegistrationAttempts(string sRegistrationCode)
    {
        SqlConnection conn = null;
        DataTable dt = new DataTable();

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("BankSI.P_GetRegistrationAttempt", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@registrationCode", SqlDbType.VarChar, 20);
            cmd.Parameters["@registrationCode"].Value = sRegistrationCode;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;

            da.Fill(dt);
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }

    protected string getXmlDate(string sDate)
    {
        string sDateToShow = "";
        DateTime dtDate;

        if (DateTime.TryParse(sDate, out dtDate))
            sDateToShow = DateTime.Parse(sDate).ToString("dd/MM/yyyy HH:mm:ss");
        else
            sDateToShow = "<i>date inconnue</i>";

        return sDateToShow;
    }
    protected string getXmlFormat(string xml, string date, string xmlType)
    {
        return FormatXml(xml, date, xmlType);
    }
    protected void DeleteOldXml()
    {
        //Supprime tous les fichiers du dossier XmlTmp ne datant pas du jour
        string sXmlTmpPath = @System.Web.HttpContext.Current.Request.MapPath(".") + "\\XmlTmp\\";
        string[] tXmlTmpFiles = Directory.GetFiles(sXmlTmpPath);
        string sXmlTmpFilePath = ""; string sXmlTmpFileDate = "";
        string sDateToday = DateTime.Now.ToString("yyyyMMdd");
        for (int i = 0; i < tXmlTmpFiles.Length; i++)
        {
            try
            {
                sXmlTmpFilePath = tXmlTmpFiles[i];
                sXmlTmpFileDate = sXmlTmpFilePath.Split('.')[0].Split('\\')[sXmlTmpFilePath.Split('.')[0].Split('\\').Length - 1].Split('_')[2];

                if (sXmlTmpFileDate != sDateToday)
                {
                    File.Delete(sXmlTmpFilePath);
                }

            }
            catch(Exception ex)
            {
            }
        }
    }

    protected string getStatusButton(string xml)
    {
        string status = "";

        if (xml.Trim().Length == 0)
            status = "disabled='disabled'";

        return status;
    }

    protected string FormatXml(string sXml, string sDate, string sXmlType)
    {
        StringBuilder sb = new StringBuilder();
        DateTime dtDate;
        string urlPathFile = "";
        string pathFile = "";
        string filename = "";
        string filenameDate = "";
        string title = "";

        if (sXml.Trim().Length > 0)
        {
            if (DateTime.TryParse(sDate, out dtDate))
            {
                filenameDate = DateTime.Parse(sDate).ToString("yyyyMMddHHmmss");
                title = sXmlType + " DU " + sDate;
            }
            else
            {
                filenameDate = "inconnue";
                title = sXmlType + " DU <i>date inconnue</i>";
            }

            filename = "xml_" + filenameDate + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xml";
            urlPathFile = "\"" + @Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.LastIndexOf('/') + 1) + "XmlTmp/" + filename + "\", \"" + title + "\"";
            pathFile = @System.Web.HttpContext.Current.Request.MapPath(".") + "\\XmlTmp\\" + filename;

            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(sXml);

                StringWriter sw = new StringWriter(sb);
                XmlTextWriter xmlTextWriter = new XmlTextWriter(sw);
                xmlTextWriter.Formatting = Formatting.Indented;
                xmlDoc.WriteTo(xmlTextWriter);
                xmlDoc.Save(pathFile);
            }
            catch (Exception ex)
            { }
        }
        else
        {
            urlPathFile = "\"\", \"" + sXmlType + " DU <i>date inconnue</i>" + "\"";
        }

        //return sb.ToString();
        return urlPathFile;
    }
    protected XmlDocument getXML(string sXML)
    {

        XmlDocument xd = new XmlDocument();

        try
        {
            xd.LoadXml(sXML);
        }
        catch (Exception ex)
        {
        }

        return xd;
    }

    protected void getPdfSubscriptionInformation(string sRegistrationCode)
    {
        SqlConnection conn = null;
        authentication auth = authentication.GetCurrent();

        try
        {

            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("BankSI.P_GetPDFSubscriptionSentInformation", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@FindPDFSubscriptionCriteria", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@PDFSubscriptionInformation", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@ReturnSelect", SqlDbType.Bit);
            cmd.Parameters.Add("@RC", SqlDbType.Int);

            string sXmlIN = new XElement("ALL_XML_IN",
                                                new XElement("PDFSubscription",
                                                    new XAttribute("RegistrationCode", sRegistrationCode),
                                                    new XAttribute("CashierToken", auth.sToken),
                                                    new XAttribute("CultureID", (Session["CultureID"] != null) ? Session["CultureID"].ToString() : "Fr-fr")
                                                )
                                            ).ToString();

            cmd.Parameters["@FindPDFSubscriptionCriteria"].Value = sXmlIN;
            cmd.Parameters["@ReturnSelect"].Value = false;

            cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;
            cmd.Parameters["@PDFSubscriptionInformation"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sOut = cmd.Parameters["@PDFSubscriptionInformation"].Value.ToString();

            if (sOut.Length > 0)
            {
                getPdfSubscriptionInformationFromXml(sOut);
            }
            else
            {
                divPdf.Visible = false;
            }
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

    }
    protected void getPdfSubscriptionInformationFromXml(string sXml)
    {
        txtPdfSent.Text = "NON";
        List<string> listPdfSent = CommonMethod.GetAttributeValues(sXml, "ALL_XML_OUT/PDFSubscription", "Sent");

        if (listPdfSent.Count > 0 && listPdfSent[0].Trim() != "")
        {
            txtPdfSent.Text = (listPdfSent[0] == "1") ? "OUI" : "NON";

            if (listPdfSent[0] == "1")
            {
                txtPdfSent.Text = "OUI";
                //btnShowSubscriptionPdf.Visible = true;

                List<string> listPdfSent_Date = CommonMethod.GetAttributeValues(sXml, "ALL_XML_OUT/PDFSubscription", "SendDate");
                if (listPdfSent_Date.Count > 0)
                {
                    DateTime dtPdfSent_Date;
                    if (DateTime.TryParse(listPdfSent_Date[0], out dtPdfSent_Date))
                        //txtPdfSent_Date.Text = dtPdfSent_Date.ToString("dd/MM/yyyy hh:mm:ss");
                        txtPdfSent_Date.Text = listPdfSent_Date[0];
                    else
                        txtPdfSent_Date.Text = listPdfSent_Date[0];

                    panelPdfSent_Date.Visible = true;
                }

                string sRegistrationNumber = lblRegistrationSelected.Text;

                //string filepath = @"." + ConfigurationManager.AppSettings["SubscriptionPdfPath"].ToString() + lblRegistrationSelected.Text + ".pdf";
                string dynamicSubDirectory = tools.getSubscriptionDirectory(sRegistrationNumber);

                string filepath = "";
                if (File.Exists(System.Web.HttpContext.Current.Server.MapPath("~" + ConfigurationManager.AppSettings["SubscriptionPdfPath"].ToString() + dynamicSubDirectory + "/" + sRegistrationNumber + ".pdf")))
                    filepath = "." + ConfigurationManager.AppSettings["SubscriptionPdfPath"].ToString() + dynamicSubDirectory + "/" + sRegistrationNumber + ".pdf";
                else
                    filepath = "." + ConfigurationManager.AppSettings["SubscriptionPdfPath"].ToString() + dynamicSubDirectory + "/Dossier_ouverture_" + sRegistrationNumber + ".pdf";
                string title = "Dossier inscription " + sRegistrationNumber;
                //btnShowSubscriptionPdf.OnClientClick = "clickShowPdf('" + filepath + "','" + title + "');return false;";
                //hlTestPdf.NavigateUrl = filepath;
                //hlTestPdf.Target = "_blank";
                //hlTestPdf.Visible = false;

                string sDocFilePath = ConfigurationManager.AppSettings["NickelDocPath"].ToString();

                if (File.Exists(HostingEnvironment.MapPath("~" + sDocFilePath + dynamicSubDirectory + "Dossier_ouverture_" + sRegistrationNumber + ".pdf")))
                    hdnRegistrationFilePath.Value = "." + sDocFilePath + dynamicSubDirectory + "Dossier_ouverture_" + sRegistrationNumber + ".pdf";
                else if (File.Exists(HostingEnvironment.MapPath("~" + sDocFilePath + dynamicSubDirectory + sRegistrationNumber + ".pdf")))
                    hdnRegistrationFilePath.Value = "." + sDocFilePath + dynamicSubDirectory + sRegistrationNumber + ".pdf";
                else
                    hdnRegistrationFilePath.Value = "";
            }
            else
            {
                txtPdfSent.Text = "NON";
                panelPdfSent_Date.Visible = false;
                //btnShowSubscriptionPdf.Visible = false;
            }
        }
        else
        {
            txtPdfSent.Text = "NON";
            panelPdfSent_Date.Visible = false;
            //btnShowSubscriptionPdf.Visible = false;
        }
    }

    protected DataTable getMpadStepsDetails(string sRegistrationCode)
    {
        SqlConnection conn = null;
        DataTable dt = new DataTable();

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Partner"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("Registration.P_MPAD_Steps_History", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@RegistrationCode", SqlDbType.VarChar, 20);
            cmd.Parameters["@RegistrationCode"].Value = sRegistrationCode;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;

            da.Fill(dt);
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }

    protected bool updateRegistration(string sXML, out string sError)
    {
        SqlConnection conn = null;

        sError = "";
        bool isOK = false;
        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["PARTNER"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("Registration.P_BackOfficeCorrections", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);
            cmd.Parameters["@IN"].Value = sXML;
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sOut = cmd.Parameters["@OUT"].Value.ToString();

            string sRC = "";
            GetValueFromXml(sOut, "ALL_XML_OUT/User", "RC", out sRC);
            GetValueFromXml(sOut, "ALL_XML_OUT/User", "ErrorLabel", out sError);

            if (sOut.Length > 0 && sRC.Trim() == "0")
                isOK = true;
            else
            {
                isOK = false;
                sError += "(" + sRC + ")";
            }
        }
        catch (Exception e)
        {
            isOK = false;
            sError = "Une erreur inattendue est survenue";
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return isOK;
    }

    protected bool checkClientInformations(out string sErrorList)
    {
        bool isOK = true;
        sErrorList = "";
        string sError = "";

        DateTime dt;
        int num;

        if (panelMarriedLastName.Visible && txtIDMarriedLastName.Text.Trim().Length == 0)
            sError += "<li>Nom marital</li>";

        if (txtIDLastName.Text.Trim().Length == 0)
            sError += "<li>Nom</li>";

        if (txtIDFirstName.Text.Trim().Length == 0)
            sError += "<li>Prénom</li>";

        if (txtBirthDate.Text.Trim().Length == 0 || !DateTime.TryParseExact(txtBirthDate.Text, "dd/MM/yyyy", new CultureInfo("fr-FR"), DateTimeStyles.None, out dt))
            sError += "<li>Date de naissance</li>";

        if (txtBirthCity.Text.Trim().Length == 0)
            sError += "<li>Ville de naissance</li>";

        if (ddlBirthCountry.SelectedValue.Trim().Length == 0)
            sError += "<li>Pays de naissance</li>";

        if (txtDocumentNumber.Text.Trim().Length == 0)
            sError += "<li>Numéro de document</li>";

        if (txtExpirationDate.Text.Trim().Length == 0 || !DateTime.TryParseExact(txtExpirationDate.Text, "dd/MM/yyyy", new CultureInfo("fr-FR"), DateTimeStyles.None, out dt))
            sError += "<li>Date d'expiration</li>";

        //if (txtIssuedID.Text.Trim().Length == 0 || !DateTime.TryParseExact(txtIssuedID.Text, "dd/MM/yyyy", new CultureInfo("fr-FR"), DateTimeStyles.None, out dt))
            //sError += "<li>Date d'émission</li>";

        if (ddlDocumentCountry.SelectedValue.Trim().Length == 0)
            sError += "<li>Pays du document</li>";

        if (txtAddress1.Text.Trim().Length == 0)
            sError += "<li>Adresse</li>";

        if (txtZipCode.Text.Trim().Length != 5 || !int.TryParse(txtZipCode.Text, out num))
            sError += "<li>Code postal</li>";

        if (txtCity.Text.Trim().Length == 0)
            sError += "<li>Ville</li>";

        if (sError.Trim().Length > 0)
        {
            isOK = false;
            sErrorList = "<ul>"+sError+"</ul>";
        }

        return isOK;
    }
    protected string getXmlFromClientInformations()
    {
        string sXML = "";
        authentication auth = authentication.GetCurrent();

        try
        {

            XElement xml = new XElement("Registration",
                                new XAttribute("CashierToken", auth.sToken),
                                new XAttribute("RegistrationCode", lblRegistrationSelected.Text),
                                new XAttribute("Culture", (Session["Culture"] != null) ? Session["Culture"].ToString() : "fr-FR"));

            //ID DOCUMENT
            //if (!txtIDMarriedLastName.ReadOnly)
                xml.Add(new XElement("INFO",
                    new XAttribute("N", "MLN"),
                    new XAttribute("V", txtIDMarriedLastName.Text.ToUpper())));
            //if (!txtIDLastName.ReadOnly)
                xml.Add(new XElement("INFO",
                    new XAttribute("N", "LNA"),
                    new XAttribute("V", txtIDLastName.Text.ToUpper())));
            //if (!txtIDFirstName.ReadOnly)
                xml.Add(new XElement("INFO",
                    new XAttribute("N", "FNA"),
                    new XAttribute("V", txtIDFirstName.Text.ToUpper())));
                //if (!txtGender.ReadOnly)
                xml.Add(new XElement("INFO",
                    new XAttribute("N", "SEX"),
                    new XAttribute("V", rblGender.SelectedValue.ToUpper())));
                //if (!txtBirthDate.ReadOnly)
                xml.Add(new XElement("INFO",
                    new XAttribute("N", "BDA"),
                    new XAttribute("V", txtBirthDate.Text.ToUpper())));
                //if (!txtBirthCity.ReadOnly)
                xml.Add(new XElement("INFO",
                    new XAttribute("N", "BPL"),
                    new XAttribute("V", txtBirthCity.Text.ToUpper())));
                //if (!txtBirthCountry.ReadOnly)
                xml.Add(new XElement("INFO",
                    new XAttribute("N", "BI2"),
                    new XAttribute("V", ddlBirthCountry.SelectedValue.ToUpper())));
                //if (!txtDocumentNumber.ReadOnly)
                xml.Add(new XElement("INFO",
                    new XAttribute("N", "DTN"),
                    new XAttribute("V", txtDocumentNumber.Text.ToUpper())));
                //if (!txtExpirationDate.ReadOnly)
                xml.Add(new XElement("INFO",
                    new XAttribute("N", "EDT"),
                    new XAttribute("V", txtExpirationDate.Text.ToUpper())));
                //if (!txtIssuedID.ReadOnly)
                xml.Add(new XElement("INFO",
                    new XAttribute("N", "IDT"),
                    new XAttribute("V", txtIssuedID.Text.ToUpper())));
                //if (!txtDocumentCountry.ReadOnly)
                xml.Add(new XElement("INFO",
                    new XAttribute("N", "ICT"),
                    new XAttribute("V", ddlDocumentCountry.SelectedValue.ToUpper())));

            //HO DOCUMENT
                //if (!txtBiller.ReadOnly)
                xml.Add(new XElement("INFO",
                    new XAttribute("N", "SDR"),
                    new XAttribute("V", txtBiller.Text.ToUpper())));
                //if (!txtTo.ReadOnly)
                xml.Add(new XElement("INFO",
                    new XAttribute("N", "TO"),
                    new XAttribute("V", txtTo.Text.ToUpper())));
                //if (!txtIssuedFact.ReadOnly)
                xml.Add(new XElement("INFO",
                    new XAttribute("N", "IDT"),
                    new XAttribute("V", txtIssuedFact.Text.ToUpper())));
                //if (!txtAddress1.ReadOnly)
                xml.Add(new XElement("INFO",
                    new XAttribute("N", "AD1"),
                    new XAttribute("V", txtAddress1.Text.ToUpper())));
                //if (!txtAddress2.ReadOnly)
                xml.Add(new XElement("INFO",
                    new XAttribute("N", "AD2"),
                    new XAttribute("V", txtAddress2.Text.ToUpper())));
                //if (!txtZipCode.ReadOnly)
                xml.Add(new XElement("INFO",
                    new XAttribute("N", "ZIP"),
                    new XAttribute("V", txtZipCode.Text.ToUpper())));
                //if (!txtCity.ReadOnly)
                xml.Add(new XElement("INFO",
                    new XAttribute("N", "CIT"),
                    new XAttribute("V", txtCity.Text.ToUpper())));

            sXML = new XElement("ALL_XML_IN", xml).ToString();
        }
        catch (Exception ex)
        { }

        return sXML;
    }

    protected void refresh()
    {
        if (lblRegistrationSelected.Text.Trim() != "")
        {
            initAllFields();

            getRegistrationDetails(lblRegistrationSelected.Text);
            getWebPasswordBySmsInformation(lblRegistrationSelected.Text);
            getPdfSubscriptionInformation(lblRegistrationSelected.Text);

            string sSignatureFilename = "";
            GetCGV(lblRegistrationSelected.Text, out sSignatureFilename);
            if (sSignatureFilename.Trim().Length == 0)
                sSignatureFilename = "CGV_" + lblRegistrationSelected.Text + ".jpg";

            string sFilePath = ConfigurationManager.AppSettings["SignaturePath"].ToString();
            if (File.Exists(System.Web.HttpContext.Current.Server.MapPath("~" + sFilePath + sSignatureFilename)))
                imgSignatureCgv.ImageUrl = "." + sFilePath + sSignatureFilename;

            getPinBySmsInformation(lblRegistrationSelected.Text);

            rMpadStepsDetails.DataSource = getMpadStepsDetails(lblRegistrationSelected.Text);
            rMpadStepsDetails.DataBind();

            btnForceSubscription.Visible = false;
            if(hfRegistrationClosed.Value != "Y")
                btnForceSubscription.Visible = true;

            btnRectified.Visible = false;
            btnValidate.Visible = false;
            btnModify.Visible = false;
            btnModifyValidation.Visible = false;
            if (hfRegistrationChecked.Value == "N" && hfRegistrationClosed.Value == "Y")
            {
                btnValidate.Visible = true;
            }
            else if (hfRegistrationChecked.Value == "Y" && hfRegistrationClosed.Value == "Y" && (hfRegistrationCheckStatus.Value == "O" || hfRegistrationCheckStatus.Value == "R"))
            {
                //btnRectified.Visible = true;
                btnValidate.Visible = true;
                lblSubscriptionCheckCommentHistory.Text = lblRegistrationCheckedComment.Text.Trim();
                lblNbMaxCheckComment.Text = (198 - lblSubscriptionCheckCommentHistory.Text.Length).ToString();
            }

            if (hfRegistrationClosed.Value != "Y")
            {
                btnModify.Visible = true;
                btnModifyValidation.Visible = true;
            }

            panelRedMail.Visible = false;
            rptRedMail.DataSource = Client.getIdenticalAddressList(lblRegistrationSelected.Text);
            rptRedMail.DataBind();

            if (rptRedMail.Items.Count > 0)
                panelRedMail.Visible = true;

            btnShowClient.Attributes.Add("onclick", "GoToClient('" + lblRegistrationSelected.Text + "')");
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "showOcrCheck(\"MrzChecked\", \"mrz-ok.jpg\",'MRZ OK'); showOcrCheck(\"MrzNotChecked\", \"mrz-ko.jpg\", 'MRZ KO'); showOcrCheck(\"VisualChecked\", \"green-eye.png\",'VISUAL CHECK OK'); showOcrCheck(\"VisualNotChecked\", \"red-eye.png\",'VISUAL CHECK KO');initTooltip();", true);

        }
    }

    protected void clickForceSubscription(object sender, EventArgs e)
    {
        authentication auth = authentication.GetCurrent();
        string sError = ""; string sMessage = "";
        string accountNumber = txtForceSubscriptionAccountNumber.Text;
        string trackingNumber = txtForceSubscriptionTrackingNumber.Text;
        txtForceSubscriptionAccountNumber.Text = "";
        txtForceSubscriptionTrackingNumber.Text = "";
        string sXML = new XElement("ALL_XML_IN",
                        new XElement("SABAnswer",
                            new XAttribute("CashierToken", auth.sToken),
                            new XAttribute("TrackingNumber", trackingNumber),
                            new XAttribute("AccountNumber", accountNumber),
                            new XAttribute("RegistrationCode", lblRegistrationSelected.Text.Trim()))).ToString();

        if (ForceSubscription(sXML, out sError))
        {
            sMessage = "<span style=\"color:green\">L'opération a réussi.</span>";
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "alertMessage('Forcer inscription','" + sMessage.Replace("'", "&apos;").Trim() + "');", true);
        }
        else
        {
            sMessage = "<span style=\"color:red\">L'opération a échoué.<br/>"+ sError +"</span>";
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "alertMessage('Forcer inscription','" + sMessage.Replace("'", "&apos;").Trim() + "');", true);
        }
    }
    protected bool ForceSubscription(string sXML, out string sError)
    {
        sError = "";
        bool isOK = false;

        SqlConnection conn = null;
        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("Web.P_PushSABSubscriptionAnswer", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);
            cmd.Parameters["@IN"].Value = sXML;
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sOut = cmd.Parameters["@OUT"].Value.ToString();

            string sRC = "";
            GetValueFromXml(sOut, "ALL_XML_OUT/User", "RC", out sRC);

            if (sOut.Length > 0 && sRC.Trim() == "0")
                isOK = true;
            else
            {
                isOK = false;
                sError += "(" + sRC + ")";
            }
        }
        catch (Exception e)
        {
            isOK = false;
            sError = "Une erreur inattendue est survenue";
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return isOK;
    }

    protected void btnRegistrationChecked_click(object sender, EventArgs e)
    {
        string sNoComplianceList = "";
        for (int i = 0; i < rptSetNoComplianceList.Items.Count; i++)
        {
            try
            {
                HiddenField hfNoComplianceSelected = null;
                CheckBox cbNoCompliance = null;

                cbNoCompliance = (CheckBox)rptSetNoComplianceList.Items[i].FindControl("cbNoCompliance");
                if (cbNoCompliance != null && cbNoCompliance.Checked)
                {
                    hfNoComplianceSelected = (HiddenField)rptSetNoComplianceList.Items[i].FindControl("hfComplianceValue");
                    sNoComplianceList += hfNoComplianceSelected.Value + "+";
                }
            }
            catch (Exception ex)
            { }

        }

        if (sNoComplianceList.Trim().Length > 0)
            sNoComplianceList = sNoComplianceList.Substring(0, sNoComplianceList.Length - 1);

        RegistrationChecked(ddlSubscriptionCheckStatus.SelectedValue, txtSubscriptionCheckComment.Text, sNoComplianceList);
    }
    protected void btnRegistrationRectified_click(object sender, EventArgs e)
    {
        RegistrationChecked("G", txtSubscriptionCheckComment.Text, "");
    }
    protected void rptGetNoComplianceList_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (hfNoComplianceSelectedList.Value.Trim().Length > 0)
        {
            string[] sNoComplianceSelectedList = hfNoComplianceSelectedList.Value.Trim().Split('+');

            if (sNoComplianceSelectedList.Length > 0)
            {
                try
                {
                    CheckBox cbNoCompliance = null;
                    HiddenField hfComplianceValue = null;
                    string sNoComplianceSelectedValue = "";

                    sNoComplianceSelectedValue = ((HiddenField)e.Item.FindControl("hfComplianceValue")).Value.Trim();

                    for (int i = 0; i < sNoComplianceSelectedList.Length; i++)
                    {
                        if (sNoComplianceSelectedValue == sNoComplianceSelectedList[i].Trim())
                        {
                            ((CheckBox)e.Item.FindControl("cbNoCompliance")).Checked = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                }
            }
        }
    }

    protected bool getArchivedIdScan()
    {
        bool isOK = false;
        SqlConnection conn = null;
        DataTable dt = new DataTable();

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("[Google].[P_GetIDScanFiles]", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@RegistrationCode", SqlDbType.VarChar, 50);
            cmd.Parameters.Add("@RefCustomer", SqlDbType.Int);
            cmd.Parameters.Add("@RC", SqlDbType.Int);

            cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;
            cmd.Parameters["@RegistrationCode"].Value = lblRegistrationSelected.Text;
            cmd.Parameters["@RefCustomer"].Value = 0;
            cmd.ExecuteNonQuery();

            if(cmd.Parameters["@RC"].Value.ToString().Trim() == "0")
                isOK = true;
        }
        catch (Exception e)
        {
            isOK = false;
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return isOK;

        //EXEC[Google].[P_GetIDScanFiles] @RegistrationCode VARCHAR(50), @RefCustomer INT
    }

    protected void btnGetArchivedImageParent_Click(object sender, EventArgs e)
    {
        if (getArchivedIdScan())
            refresh();
    }

    protected void btnGetArchivedImage_Click(object sender, EventArgs e)
    {
        if (getArchivedIdScan())
            refresh();
    }

    //protected void Uploader_FileUploaded(object sender, UploaderEventArgs args)
    //{
    //    string sTmpPath = "";
    //    switch (hfUploadFace.Value.Trim().ToLower())
    //    {
    //        case "recto":
    //            sTmpPath = System.Web.HttpContext.Current.Server.MapPath("~/UploadTmp/" + hfIdRectoPath.Value.Substring(hfIdRectoPath.Value.LastIndexOf('/')));
    //            break;
    //        case "verso":
    //            sTmpPath = System.Web.HttpContext.Current.Server.MapPath("~/UploadTmp/" + hfIdVersoPath.Value.Substring(hfIdVersoPath.Value.LastIndexOf('/')));
    //            break;
    //    }

    //    //Copys the uploaded file to a new location.
    //    args.CopyTo(sTmpPath);
    //    //You can also open the uploaded file's data stream.
    //    //System.IO.Stream data = args.OpenStream();
    //}

    //protected void btnUpload_Click(object sender, EventArgs e)
    //{
    //    string sUploadClick = "";

    //}



    //protected void UploadAttachments1_UploadCompleted(object sender, UploaderEventArgs[] args)
    //{
    //    UploadAttachments1.DeleteAllAttachments();
    //}

    protected void btnGoogleUpload_Click(object sender, EventArgs e)
    {
        bool isOK = false;
        SqlConnection conn = null;
        DataTable dt = new DataTable();
        if (hfUploadFileName.Value.Trim().Length > 0) {
            //<ALL_XML_IN><Google RefCustomer="2" RegistrationCode="NOB208436988440" KindTAG="ID" Encrypt="0" StoreFileName="_1_ID_I_02_NOB208436988440_20130903120554024_42371248-e25a-4516-b583-afc63a973598.JPG" StorePath="\\339741-OCR2\DOCS_ICAR\OUT\CroppedImages"/></ALL_XML_IN>
            string sXmlIn = new XElement("ALL_XML_IN",
                                new XElement("Google",
                                    //new XAttribute("RefCustomer", _iref),
                                    new XAttribute("RegistrationCode", lblRegistrationSelected.Text),
                                    new XAttribute("KindTAG", "ID"),
                                    new XAttribute("Encrypt", "0"),
                                    new XAttribute("StoreFileName", hfUploadFileName.Value),
                                    new XAttribute("StorePath", @"\\339741-OCR2\DOCS_ICAR\OUT\CroppedImages"))).ToString();


            try
            {
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
                conn.Open();

                SqlCommand cmd = new SqlCommand("[Google].P_UploadCustomerFile", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@IN", SqlDbType.VarChar, -1);
                cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);
                cmd.Parameters.Add("@RC", SqlDbType.Int);

                cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;
                cmd.Parameters["@IN"].Value = sXmlIn;
                cmd.Parameters["@OUT"].Value = "";

                //cmd.ExecuteNonQuery();

                if (cmd.Parameters["@RC"].Value.ToString().Trim() == "0")
                {
                    isOK = true;
                    Response.Redirect(Request.RawUrl);
                }
            }
            catch (Exception ex)
            {
                isOK = false;
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        if(!isOK)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "initfileupload('"+ hfUploadFileName.Value + "');closeUploadImagesDialog();showUploadImagesDialog('" + hfUploadFace.Value + "'); ", true);
        }

        //return isOK;
    }
}
