﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UnauthorizedAccess : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        tools.UnauthorizedAccess _UnauthorizedAccess = tools.getUnautthorizedAccess();
        if (_UnauthorizedAccess.Activated)
        {
            lblMessageDetails.Text = _UnauthorizedAccess.Message;
        }
        else
        {
            Response.Redirect("Default.aspx");
        }

    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        tools.UnauthorizedAccess _UnauthorizedAccess = tools.getUnautthorizedAccess();
        if (!string.IsNullOrWhiteSpace(_UnauthorizedAccess.PreviousPage))
        {
            tools.ClearUnauthorizedAccess();
            Response.Redirect(_UnauthorizedAccess.PreviousPage);
        }
        else
        {
            Response.Redirect("Default.aspx");
        }
    }
}