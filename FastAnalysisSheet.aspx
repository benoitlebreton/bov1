﻿<%@ Page Title="Fiche d'analyse rapide" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="FastAnalysisSheet.aspx.cs" Inherits="FastAnalysisSheet" %>

<%@ Register Src="~/API/BeneficiaryList.ascx" TagPrefix="asp" TagName="BeneficiaryList" %>
<%@ Register Src="~/API/BlockingHistory.ascx" TagPrefix="asp" TagName="BlockingHistory" %>
<%@ Register Src="~/API/SpecificityHistory.ascx" TagPrefix="asp" TagName="SpecificityHistory" %>
<%@ Register Src="~/API/CardsHistory.ascx" TagPrefix="asp" TagName="CardsHistory" %>
<%@ Register Src="~/API/ProfileHistory.ascx" TagPrefix="asp" TagName="ProfileHistory" %>
<%@ Register Src="~/API/IPConnectionList.ascx" TagPrefix="asp" TagName="IPConnectionList" %>
<%@ Register Src="~/API/AmlComment.ascx" TagPrefix="asp" TagName="AmlComment" %>
<%@ Register Src="~/API/LightLock.ascx" TagPrefix="asp" TagName="LightLock" %>
<%@ Register Src="~/API/ProNotificationCounter.ascx" TagName="ProNotificationCounter" TagPrefix="asp" %>
<%@ Register Src="~/API/Scoring.ascx" TagName="Scoring" TagPrefix="asp" %>
<%@ Register Src="~/API/ClientRelation.ascx" TagPrefix="asp" TagName="ClientRelation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <link type="text/css" rel="Stylesheet" href="Styles/ProNotificationCounter.css" />
    <link type="text/css" rel="Stylesheet" href="Styles/Scoring.css?v=20171004" />
    <script type="text/javascript" src="Scripts/Scoring.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <%--<link href="Styles/jquery.stepProgressBar.css" rel="Stylesheet" type="text/css"/>--%>

    <script type="text/javascript" src="Scripts/ClientRelation.js"></script>
    <script type="text/javascript" src="Scripts/tableHeadFixer.js"></script>
    <link rel="Stylesheet" href="Styles/ClientRelation.css" />

     <style type="text/css">
        h3 {
            margin:0 0 5px 0;
            text-align:center;
        }

        h4 {
            font-size:1em;
            margin:0;
        }

        #ar-loading {
            background:url('Styles/img/loading2.gif') no-repeat center;
            background-size: 35px 27px;
            display:none;
            position:absolute;
            background-color:rgba(255, 255, 255, 0.5);
            z-index:10000;
        }

        .cadreOrange {
             margin:auto;
             border:1px solid #ff6a00; 
             padding:5px; 
             margin:10px 0 0 0;
        }

        .ui-widget{
            font-weight:bold;
        }

        /*.data-array {
             width:50%;
             float:right;
        }*/
        .ar-container {
            /*max-width:450px;*/
            max-height:200px;
            overflow-y:auto;
            font-size:0.8em;
        }
        /*.data-graph {
            width:50%;
            float:left;
        }*/
         .data-graph .cadreOrange {
            margin-right:10px;
            width:95.56%!important;
         }

         .chart-container {
             /*min-width: 250px;*/
             width:100%;
             height: 400px;
             /*max-width:300px;*/
             margin: 10px auto;
         }

         .ui-buttonset .ui-button.ui-state-default .ui-button-text {
             font-size:0.9em;
         }

         .label {
             font-size:1em;
         }

         .highcharts-tooltip span:first-of-type {
             margin-top:-8px !important;
         }

         .client-infos {
             margin-bottom:10px;
         }
         .client-infos .info {
            display: inline-block;
            margin: 0 10px;
            vertical-align: top;
            min-width: 25%;
         }

         .highlighted td {
             background-color:#ff9966;
         }

         .flex-container {
             display:flex;
             flex-direction:row;
             justify-content:space-between;
         }
        .flex-container .cadreOrange {
            width:48.2%;
        }

        .hc-tooltip-table {
            border-collapse:collapse;
            width:100%;
            margin-top:10px;
        }
        .hc-tooltip-table tr th {
            background-color:#344b56;
            color:#fff;
        }
        .hc-tooltip-table tr td:nth-of-type(2)
        {
            text-align:center;
        }
        .hc-tooltip-table tr td:nth-of-type(3)
        {
            text-align:right;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="Styles/FastAnalysisSheet_print.css" media="print" />

    <script type="text/javascript">
        $(document).ready(function () {
            InitTooltip();
        });

        function InitTooltip() {
            $('.image-tooltip').tooltip({
                position: {
                    my: "center bottom-10",
                    at: "center top",
                    using: function (position, feedback) {
                        $(this).css(position);
                        $("<div>")
                            .addClass("arrow")
                            .addClass(feedback.vertical)
                            .addClass(feedback.horizontal)
                            .appendTo(this);
                    }
                }
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <div class="no-print">
        <asp:Button ID="btnPrev" runat="server" Text="RECHERCHE ALERTE" CssClass="button" OnClientClick="showLoading();" />
    </div>
    <h2 style="color: #344b56;text-transform:uppercase;">Fiche d'analyse rapide</h2>
    <div>
        <div class="table" style="margin:5px 0 0 0;float:left;">
            <div class="row">
                <div class="cell" style="vertical-align:middle;">
                    <div style="font-weight:bold; color:#f57527">Titulaire principal</div>
                    <div><asp:Label ID="lblClientName" runat="server"></asp:Label></div>
                </div>
                <div class="cell" style="vertical-align:middle;padding-left:20px;">
                    <div style="font-weight:bold; color:#f57527">N&deg; Carte</div>
                    <div><asp:Label ID="lblClientCardNumber" runat="server"></asp:Label></div>
                </div>
                <div class="cell" style="padding-left:20px;vertical-align:middle;">
                    <div style="font-weight:bold; color:#f57527">N&deg; Compte</div>
                    <div><asp:Label ID="lblClientAccountNumber" runat="server"></asp:Label></div>
                </div>
                <%--<div class="cell" style="padding-left:20px;vertical-align:top;visibility:hidden">
                    <div style="font-weight:bold; color:#f57527">N&deg; Inscription</div>
                    <div><asp:Label ID="lblClientRegistrationNumber" runat="server"></asp:Label></div>
                </div>--%>
                <div class="cell" style="padding-left:20px;vertical-align:middle">
                    <asp:Scoring ID="scoring1" runat="server" Label="Scoring client" Type="client" />
                </div>
                <asp:Panel ID="panelScoring2" runat="server" CssClass="cell" style="padding-left:20px;vertical-align:middle">
                    <asp:Scoring ID="scoring2" runat="server" Label="Scoring opération" Type="alert" />
                </asp:Panel>
                <asp:Panel ID="panelScoring3" runat="server" CssClass="cell" style="padding-left:20px;vertical-align:middle">
                    <asp:Scoring ID="scoring3" runat="server" Label="Scoring alerte" Type="global" />
                </asp:Panel>
                <div class="cell" style="padding-left:20px;vertical-align:top">
                    <asp:LightLock ID="LightLock1" runat="server" ActionAllowed="true" />
                </div>
                <div class="cell" style="padding-left:20px;vertical-align:middle">
                    <asp:ProNotificationCounter ID="proNotificationCounter1" runat="server" />
                </div>
            </div>
        </div>
        <div class="no-print" style="float:right;text-align:right;position:relative;top:10px">
            <img src="Styles/Img/printer-icon.png" onclick="PrintPage()" style="cursor:pointer;width: 35px;" />
        </div>
        <div style="clear:both"></div>
    </div>

    <asp:Panel ID="panelAlertDetails" runat="server" CssClass="cadreOrange alert-details" style="margin-top:5px">
        <h3 class="font-bleuMarine">Détails de l'alerte</h3>
        <div>
        <asp:Repeater ID="rptAlertInfos" runat="server">
            <ItemTemplate>
                <div class="alert-infos">
                    <div class="label">
                        <asp:Label ID="lblAlertInfoLabel" runat="server"><%#Eval("Label") %></asp:Label>
                    </div>
                    <div>
                        <asp:Label ID="lblAlertInfoValue" runat="server"><%#Eval("Value") %></asp:Label>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
        </div>
        <asp:Panel ID="panelAlertDetailsTable" runat="server" Visible="false" CssClass="alert-details">
            <div style="margin-top:5px;overflow:auto;max-height:300px">
                <asp:Literal ID="litAlertDetailsTable" runat="server"></asp:Literal>
            </div>
        </asp:Panel>
    </asp:Panel>
    <div>
        <div class="flex-container">
        <div class="cadreOrange">
            <asp:UpdatePanel ID="upClientFlow" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="divClientFlow">
                            <h3 class="font-bleuMarine">Flux entrants et sortants</h3>
                            <asp:Label ID="lblClientFlow" runat="server"></asp:Label>
                            <asp:RadioButtonList ID="rblPeriodFlow" runat="server" RepeatDirection="Horizontal" CssClass="radioButtonList" AutoPostBack="true" OnSelectedIndexChanged="rblPeriodFlow_SelectedIndexChanged">
                                <asp:ListItem Value="1">1 mois</asp:ListItem>
                                <asp:ListItem Value="3">3 mois</asp:ListItem>
                                <asp:ListItem Value="6">6 mois</asp:ListItem>
                                <asp:ListItem Value="12">1 an</asp:ListItem>
                                <asp:ListItem Selected="True" Value="-1">Depuis ouverture</asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                    </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="rblPeriodFlow" EventName="SelectedIndexChanged" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
        <div class="cadreOrange">
            <asp:UpdatePanel ID="upClientCashInOut" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="divClientCashInOut">
                        <h3 class="font-bleuMarine">Dépôts et retraits cash</h3>
                        <asp:Label ID="lblClientCashInOut" runat="server"></asp:Label>
                        <asp:RadioButtonList ID="rblPeriodCashInOut" runat="server" RepeatDirection="Horizontal" CssClass="radioButtonList" AutoPostBack="true" OnSelectedIndexChanged="rblPeriodCashInOut_SelectedIndexChanged">
                            <asp:ListItem Value="1">1 mois</asp:ListItem>
                            <asp:ListItem Value="3">3 mois</asp:ListItem>
                            <asp:ListItem Value="6">6 mois</asp:ListItem>
                            <asp:ListItem Value="12">1 an</asp:ListItem>
                            <asp:ListItem Selected="True" Value="-1">Depuis ouverture</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="rblPeriodCashInOut" EventName="SelectedIndexChanged" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
        </div>

        <div class="flex-container">
        <div class="data-graph">
            <asp:Panel ID="panelRelation" runat="server" CssClass="cadreOrange">
                <h3 class="font-bleuMarine">Relations</h3>
                <div style="width:462px;margin-top:-25px">
                    <asp:ClientRelation ID="clientRelation1" runat="server" />
                </div>
            </asp:Panel>
            <div class="cadreOrange">
                <asp:UpdatePanel ID="upClientOperationByCategory" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="divClientOperationByCategory" style="text-align:center">
                            <h3 class="font-bleuMarine">
                                Analyse de la répartition des opérations
                            </h3>
                            <asp:RadioButtonList ID="rblOperationByCategoryPeriod" runat="server" RepeatDirection="Horizontal" CssClass="radioButtonList" AutoPostBack="true" OnSelectedIndexChanged="rblOperationByCategoryPeriod_SelectedIndexChanged">
                                <asp:ListItem Value="1">1 mois</asp:ListItem>
                                <asp:ListItem Value="3">3 mois</asp:ListItem>
                                <asp:ListItem Value="6">6 mois</asp:ListItem>
                                <asp:ListItem Value="12">1 an</asp:ListItem>
                                <asp:ListItem Selected="True" Value="-1">Depuis ouverture</asp:ListItem>
                            </asp:RadioButtonList>
                            <div style="width:462px">
                                <div id="containerClientOperationsIN" class="chart-container"></div>
                                <div id="containerClientOperationsOUT" class="chart-container"></div>
                                <div class="clear"></div>
                            </div>
                            <pre id="ClientOperationsIN_XML" runat="server" style="display:none"></pre>
                            <pre id="ClientOperationsOUT_XML" runat="server" style="display:none"></pre>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="rblOperationByCategoryPeriod" EventName="SelectedIndexChanged" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
        <div class="data-array cadreOrange">
            <div class="client-infos">
                <div class="info">
                    <div class="label">
                        Age
                    </div>
                    <div>
                        <asp:Literal ID="litAge" runat="server"></asp:Literal>
                    </div>
                </div>
                <div class="info">
                    <div class="label">
                        Nationalité
                    </div>
                    <div>
                        <asp:Literal ID="litNationalite" runat="server"></asp:Literal>
                    </div>
                </div>
                <div class="info">
                    <div class="label">
                        Sit. prof.
                    </div>
                    <div>
                        <asp:Literal ID="litCSP" runat="server"></asp:Literal>
                    </div>
                </div>
                <div class="info">
                    <div class="label">
                        Sit. patrim.
                    </div>
                    <div>
                        <asp:Literal ID="litHab" runat="server"></asp:Literal>
                    </div>
                </div>
                <div class="info">
                    <div class="label">
                        Revenus
                    </div>
                    <div>
                        <asp:Literal ID="litRevenus" runat="server">N.C.</asp:Literal>
                    </div>
                </div>
                <div class="info">
                    <div class="label">
                        Patrimoine
                    </div>
                    <div>
                        <asp:Literal ID="litPatrimoine" runat="server">N.C.</asp:Literal>
                    </div>
                </div>
                <div class="info">
                    <div class="label">
                        Email
                    </div>
                    <div>
                        <asp:Literal ID="litEmail" runat="server"></asp:Literal>
                    </div>
                </div>
                <div class="info">
                    <div class="label">
                        Code postal
                    </div>
                    <div>
                        <asp:Literal ID="litZipCode" runat="server"></asp:Literal>
                    </div>
                </div>
                <div class="info">
                    <div class="label">
                        Inscrit le
                    </div>
                    <div>
                        <asp:Literal ID="litInscriptionDate" runat="server"></asp:Literal>
                    </div>
                </div>
                <div class="info">
                    <div class="label">
                        A
                    </div>
                    <div>
                        <asp:Literal ID="litPDVName" runat="server"></asp:Literal>, <asp:Literal ID="litPDVCity" runat="server"></asp:Literal><br />
                        <asp:Literal ID="litIDPDV" runat="server"></asp:Literal>
                    </div>
                </div>
            </div>

            <h4>Adresses IP de connexion</h4>
            <div class="ar-container" style="overflow:hidden">
                <asp:IPConnectionList ID="IPConnectinoList1" runat="server" />
            </div>
            <div style="margin-top:10px;">
                <h4>Liste des bénéficiaires</h4>
                <div class="ar-container">
                    <asp:BeneficiaryList ID="BeneficiaryList1" runat="server"/>
                </div>
            </div>
            <div style="margin-top:10px;">
                <h4>Historique des profils</h4>
                <div class="ar-container">
                    <asp:ProfileHistory ID="ProfileHistory1" runat="server" />
                </div>
            </div>

            <div style="margin-top:10px;">
                <h4>Historique des spécificités</h4>
                <div class="ar-container">
                    <asp:SpecificityHistory ID="SpecificityHistory1" runat="server" />
                </div>

                <div style="margin-top:10px;">
                    <h4>Historique des blocages</h4>
                    <div class="ar-container">
                        <asp:BlockingHistory ID="BlockingHistory1" runat="server" />
                    </div>
                </div>
                        
                <div style="margin-top:10px;">
                    <h4>Historique des cartes</h4>
                    <div class="ar-container">
                        <asp:CardsHistory ID="CardsHistory1" runat="server" />
                    </div>
                </div>
            </div>
        </div>
        </div>

    </div>

    <div id="divComment" class="no-print" style="margin-top:15px">
        <asp:UpdatePanel ID="upAmlComment" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="panelPrevAmlComment" runat="server" CssClass="hidden">
                    <div id="previous-aml-comment">
                        <asp:Repeater ID="rptAmlComment" runat="server">
                            <ItemTemplate>
                                <div class='<%# Container.ItemIndex % 2 == 0 ? "row-even" : "row-odd" %>'>
                                    <asp:Literal ID="litAmlComment" runat="server" Text='<%#Eval("Note") %>'></asp:Literal>
                                    <div style="font-weight:bold;font-size:0.8em;margin-top:5px;">
                                        <asp:Literal ID="litBOUser" runat="server" Text='<%#Eval("ByUser") %>'></asp:Literal> le <asp:Literal ID="litCommentDate" runat="server" Text='<%#Eval("NoteDate") %>'></asp:Literal>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                    <div id="show-previous-aml-comment">
                        Afficher les commentaires précédents
                    </div>
                </asp:Panel>
                <div style="display:flex;align-items:flex-end;width:100%;">
                    <div style="flex:1">
                        <asp:AmlComment ID="AmlComment" runat="server" MaxLength="400" Height="50" />
                    </div>
                    <div style="margin-left:10px;font-size:0;">
                        <asp:Button ID="btnAddComment" runat="server" Text="Ajouter" OnClick="btnAddComment_Click" CssClass="button" style="margin-bottom:3px" />
                        <div class="more-actions">
                            <img alt="Plus" src="Styles/Img/more_vert.png" />
                            <div class="actions">
                                <asp:Button ID="btnAddKYCComment" runat="server" Text="KYC" CssClass="button" OnClick="btnAddKYCComment_Click" />  
                                <asp:Button ID="btnAddFluxComment" runat="server" Text="Flux" CssClass="button" OnClick="btnAddFluxComment_Click"  />  
                                <asp:TextBox ID="txtFluxEntrant" runat="server" Text="" Visible="false" />  
                                <asp:TextBox ID="txtFluxSortant" runat="server" Text="" Visible="false" />  
                            </div>
                               
                        </div>
                    </div>
                    
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <div class="no-print" style="margin-top:20px;">
        <div class="table" style="width:100%;">
            <div class="row">
                <div class="cell" style="text-align:left; width:33%">
                    <asp:Button ID="btnAccountStatement" runat="server" CssClass="buttonHigher" Text="Extrait de compte" style="width:99%" OnClick="btnAccountStatement_Click" />
                </div>
                <div class="cell" style="text-align:center">
                    <asp:Button ID="btnAlertTreatmentSheet" runat="server" CssClass="buttonHigher" Text="Fiche de traitement alerte" style="width:99%" OnClientClick="document.forms[0].target ='_blank';" OnClick="btnAlertTreatmentSheet_Click" />
                </div>
                <div class="cell" style="text-align:right; width:33%">
                    <asp:Button ID="btnAML" runat="server" CssClass="buttonHigher" Text="Fiche AML" style="width:99%" OnClick="btnAML_Click" />
                </div>
            </div>
        </div>
    </div>

    <div id="ar-loading"></div>

    <asp:HiddenField ID="hfRegistrationCode" runat="server" />
    <script type="text/javascript" src="https://code.highcharts.com/highcharts.js"></script>
    <script type="text/javascript" src="https://code.highcharts.com/modules/exporting.js"></script>

    <%--<script type="text/javascript" src="Scripts/jquery.stepProgressBar.js"></script>--%>

    <script type="text/javascript">
        $(document).ready(function () {
            init();
        });

        var chartIN;
        var chartOUT;
        function init() {
            $('.radioButtonList').buttonset();

            $('.alert-details td').unbind('click').bind('click', function () {
                var parent = $(this).parent('tr');

                if (!$(parent).hasClass('highlighted'))
                    $(parent).addClass('highlighted');
                else $(parent).removeClass('highlighted');
            }).css;

            InitAmlMultiComment();
        }

        function getNbOperation($xml, name) {
            //console.log($xml);
            //console.log(name);
            var nb = 0;
            $xml.find('series').each(function (i, series) {
                if ($(series).find('name').text() == name)
                    nb = parseInt($(series).find('nb').text());
            });

            return nb;
        }

        function PrintPage() {
            var width = chartIN.chartWidth;
            var height = chartIN.chartHeight;
            //console.log(width);
            //console.log(height);

            chartIN.setSize(335, 280, false);
            chartOUT.setSize(335, 280, false);
            window.print();
            setTimeout(function () {
                chartIN.setSize(width, height, false);
                chartOUT.setSize(width, height, false);
            }, 100);
        }

        function InitGraphs(options) {
            //console.log(options);
            InitSingleGraph('containerClientOperationsIN', options[0]);
            InitSingleGraph('containerClientOperationsOUT', options[1]);
        }
        function InitSingleGraph(containerID, options)
        {
            //console.log(options.title);
            //console.log(options.categories);
            //console.log(JSON.stringify(options.series));

            var options = {
                chart: {
                    type: 'column',
                    renderTo: containerID
                },
                title: {
                    text: options.title
                },
                xAxis: {
                    //categories: options.categories
                    categories: ['']
                },
                yAxis: {
                    min: 0,
                    max: 100,
                    title: {
                        text: ''
                    }
                },
                tooltip: { //#f57527
                    pointFormat: '<span style="font-size:1.2em;font-weight:bold;color:{series.color}">{series.name}</span><br/>Pourcentage : <b>{point.y:.1f}%</b><br/>Quantité : <b>{series.options.qty}</b><br/>Montant : <b>{series.options.amnt}</b>{series.options.details}' //<br/>Nb : </span><b>{series.options.qty}</b>
                    , shared: false
                    , useHTML: true
                    //, positioner: function (labelWidth, labelHeight, point) {
                    //    return {
                    //        x: 0,
                    //        y: 0
                    //    };
                    //}
                },
                plotOptions: {
                    column: {
                        //stacking: 'percent'
                        stacking: 'normal'
                    }
                },
                series: options.series
            };

            var chart = new Highcharts.Chart(options);

            if (containerID.indexOf("IN") != -1)
                chartIN = chart;
            else chartOUT = chart;
        }

    </script>

    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            pbControl = args.get_postBackElement();
            //console.log(pbControl.id);
            if (pbControl != null) {
                var panel = '';
                if (pbControl.id.indexOf('<%=rblPeriodFlow.ClientID %>') != -1)
                    panel = '#divClientFlow';
                else if (pbControl.id.indexOf('<%=rblPeriodCashInOut.ClientID %>') != -1)
                    panel = '#divClientCashInOut'
                else if (pbControl.id.indexOf('<%=rblOperationByCategoryPeriod.ClientID %>') != -1)
                    panel = '#divClientOperationByCategory'
                else if (pbControl.id.indexOf('<%=btnAddComment.ClientID%>') != -1 || pbControl.id.indexOf('<%=btnAddKYCComment.ClientID%>') != -1)
                    panel = '#divComment';
                //else if (pbControl.id == '<=btnLoadDashboard.ClientID%>')
                    //panel = '#<=panelAgencyDashboard.ClientID %>';

                //console.log(panel);
                if (panel.length > 0) {
                    var width = $(panel).outerWidth();
                    var height = $(panel).outerHeight();
                    $('#ar-loading').css('width', width);
                    $('#ar-loading').css('height', height);

                    $('#ar-loading').show();

                    $('#ar-loading').position({ my: "left top", at: "left top", of: panel });
                }
            }
        }
        function EndRequestHandler(sender, args) {
            $('#ar-loading').hide();
            $('.radioButtonList').buttonset();
        }
    </script>


</asp:Content>
