﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using XMLMethodLibrary;

public partial class AgencyDetails : System.Web.UI.Page
{
    protected bool bInfosActionAllowed { get { return (ViewState["InfosActionAllowed"] != null) ? bool.Parse(ViewState["InfosActionAllowed"].ToString()) : false; } set { ViewState["InfosActionAllowed"] = value; } }
    private int _iRefAgency
    {
        get { return int.Parse(ViewState["RefAgency"].ToString()); }
        set { ViewState["RefAgency"] = value; }
    }
    private string _sAccountNumber
    {
        get { return (ViewState["AccountNumber"] != null) ? ViewState["AccountNumber"].ToString() : ""; }
        set { ViewState["AccountNumber"] = value; }
    }
    private DataTable _dtMPAD { get { return (ViewState["dtMPAD"] != null) ? (DataTable)ViewState["dtMPAD"] : null; } set { ViewState["dtMPAD"] = value; } }
    private DataTable _dtPOS { get { return (ViewState["dtPOS"] != null) ? (DataTable)ViewState["dtPOS"] : null; } set { ViewState["dtPOS"] = value; } }
    private bool _bDashboardInitialized { get { return (ViewState["DashboardInitialized"] != null) ? bool.Parse(ViewState["DashboardInitialized"].ToString()) : false; } set { ViewState["DashboardInitialized"] = value; } }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            CheckRights();

            string sToken = authentication.GetCurrent().sToken;

            string sRef = "";
            if (Request.Params["ref"] != null && Request.Params["ref"].Length > 0)
                sRef = Request.Params["ref"].ToString();
            else if (Request.Params["id"] != null && Request.Params["id"].Length > 0)
            {
                string sXmlOut;
                DataTable dt = Agency.getAgencySearchResult(Request.Params["id"], 1, sToken, out sXmlOut);
                if (dt.Rows.Count > 0)
                    sRef = dt.Rows[0]["A_REF"].ToString();
            }

            int iRef = 0;
            if (int.TryParse(sRef, out iRef))
            {
                _iRefAgency = iRef;
                SetAgencyInfos(iRef, sToken);
                SetAgencyEquipmentList(iRef, sToken);
                kycAlert.RefAgency = lblID.Text;

                // CHECK FOR AUTO OPEN ZENDESK
                if (Request.Params["zendesk"] != null && !String.IsNullOrWhiteSpace(Request.Params["zendesk"]))
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "$(window).load(function(){OpenZendeskTab('" + Request.Params["zendesk"] + "');});", true);
            }

            if (Request.Params["tab"] != null)
            {
                string sTab = "";

                switch (Request.Params["tab"].ToString())
                {
                    case "alerts":
                        sTab = panelAgencyAlerts.ClientID;
                        break;
                }

                if (sTab.Length > 0)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "GoToTabKEY", "setTimeout(function(){GoToTab('" + sTab + "');},100);", true);
                }
            }

            int iNbAlert = kycAlert.GetAgencyNbKYCAlert(_iRefAgency);

            if (iNbAlert > 0)
            {
                hdnNbAlert.Value = iNbAlert.ToString();
                kycAlert.ShowUntreatedOnly();
            }
        }

        //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "InitPostBackKey", "InitPostBack();", true);
    }

    protected void CheckRights()
    {
        string sToken = authentication.GetCurrent().sToken;

        string sXmlOut = tools.GetRights(sToken, "SC_Agency");

        List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Page", "RC");
        List<string> listView = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Page/Action", "ViewAllowed");

        if (listRC.Count == 0 || listRC[0] != "0" || listView.Count == 0 || listView[0] != "1")
        {
            Response.Redirect("Default.aspx", true);
        }
        else
        {
            DataTable dtTabs = new DataTable();
            dtTabs.Columns.Add("tab");

            dtTabs.Rows.Add(new object[] { "<li><a href=\"#" + panelAgencyGeneralInfo.ClientID + "\">Fiche Agence</a></li>" });
            dtTabs.Rows.Add(new object[] { "<li><a href=\"#" + panelAgencyAddress.ClientID + "\">Localisation</a></li>" });
            dtTabs.Rows.Add(new object[] { "<li><a href=\"#" + panelAgencyEquipment.ClientID + "\">Equipement</a></li>" });
            dtTabs.Rows.Add(new object[] { "<li><a href=\"#" + panelAgencyActivity.ClientID + "\">Activité</a></li>" });
            dtTabs.Rows.Add(new object[] { "<li><a href=\"#" + panelAgencyDashboard.ClientID + "\">Dashboard</a></li>" });
            dtTabs.Rows.Add(new object[] { "<li><a href=\"#" + panelAgencySettings.ClientID + "\">Réglages</a></li>" });

            List<string> listAction = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/Page/Action");

            for (int i = 0; i < listAction.Count; i++)
            {
                List<string> listTag = CommonMethod.GetAttributeValues(listAction[i], "Action", "TagAction");
                List<string> listActionAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ActionAllowed");
                List<string> listViewAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ViewAllowed");

                if (listTag.Count > 0 && listTag[0] == "AgencyInformation")
                {
                    if (listActionAllowed[0] != "1")
                    {
                        btnEditAgencySettings.Visible = false;
                        btnSendContractToMe.Visible = false;
                        btnSendContractToTobacco.Visible = false;
                    }
                    else bInfosActionAllowed = true;
                    break;
                }
            }

            bool bAction = false;
            bool bView = false;
            if (Agency.HasAlertRights("SC_Agency", sToken, out bView, out bAction))
            {
                dtTabs.Rows.Add(new object[] { "<li><a href=\"#" + panelAgencyAlerts.ClientID + "\">Incident KYC</a></li>" });
                kycAlert.ViewRights = bView;
                kycAlert.ActionRights = bAction;
            }
            else panelAgencyAlerts.Visible = false;

            dtTabs.Rows.Add(new object[] { "<li><a href=\"#" + panelFormation.ClientID + "\">Formation</a></li>" });
            dtTabs.Rows.Add(new object[] { "<li><a href=\"#" + panelRisks.ClientID + "\">Risques</a></li>" });

            rptTabs.DataSource = dtTabs;
            rptTabs.DataBind();
        }
    }

    protected void SetAgencyInfos(int iRefAgency, string sToken)
    {
        string sXmlOUT = Agency.GetAgencyDetails(iRefAgency, sToken);

        if (sXmlOUT.Length > 0)
        {
            List<string> listID = CommonMethod.GetAttributeValues(sXmlOUT, "ALL_XML_OUT/Shop", "AgencyID");
            List<string> listIDPDVSAB = CommonMethod.GetAttributeValues(sXmlOUT, "ALL_XML_OUT/Shop", "idePDVSAB");
            List<string> listIDBimedia = CommonMethod.GetAttributeValues(sXmlOUT, "ALL_XML_OUT/Shop", "ExternalID");
            List<string> listFirstActivation = CommonMethod.GetAttributeValues(sXmlOUT, "ALL_XML_OUT/Shop", "FirstActivation");
            List<string> listSiret = CommonMethod.GetAttributeValues(sXmlOUT, "ALL_XML_OUT/Shop", "Siret");
            List<string> listRaisonSocial = CommonMethod.GetAttributeValues(sXmlOUT, "ALL_XML_OUT/Shop", "AgencyName2");
            List<string> listNomCommercial = CommonMethod.GetAttributeValues(sXmlOUT, "ALL_XML_OUT/Shop", "AgencyName1");
            List<string> listNomCommercial2 = CommonMethod.GetAttributeValues(sXmlOUT, "ALL_XML_OUT/Shop/StoreLocator", "AgencyName");
            List<string> listManagerLastName = CommonMethod.GetAttributeValues(sXmlOUT, "ALL_XML_OUT/Shop", "ManagerLastName");
            List<string> listManagerFirstName = CommonMethod.GetAttributeValues(sXmlOUT, "ALL_XML_OUT/Shop", "ManagerFirstName");
            List<string> listPhoneNumber = CommonMethod.GetAttributeValues(sXmlOUT, "ALL_XML_OUT/Shop", "PhoneNumber");
            List<string> listMobilePhone = CommonMethod.GetAttributeValues(sXmlOUT, "ALL_XML_OUT/Shop", "Mobile");
            List<string> listEmail = CommonMethod.GetAttributeValues(sXmlOUT, "ALL_XML_OUT/Shop", "Email");
            List<string> listAddress = CommonMethod.GetAttributeValues(sXmlOUT, "ALL_XML_OUT/Shop", "Address");
            List<string> listZipcode = CommonMethod.GetAttributeValues(sXmlOUT, "ALL_XML_OUT/Shop", "Zip");
            List<string> listCity = CommonMethod.GetAttributeValues(sXmlOUT, "ALL_XML_OUT/Shop", "City");
            List<string> listLatitude = CommonMethod.GetAttributeValues(sXmlOUT, "ALL_XML_OUT/Shop/StoreLocator", "Latitude");
            List<string> listLongitude = CommonMethod.GetAttributeValues(sXmlOUT, "ALL_XML_OUT/Shop/StoreLocator", "Longitude");
            List<string> listAccountNumber = CommonMethod.GetAttributeValues(sXmlOUT, "ALL_XML_OUT/Shop", "AccountNumber");
            List<string> listBarCode = CommonMethod.GetAttributeValues(sXmlOUT, "ALL_XML_OUT/Shop", "BarCode");
            List<string> listRegionCommerciale = CommonMethod.GetAttributeValues(sXmlOUT, "ALL_XML_OUT/Shop", "RegionCommerciale");

            // SCORING
            List<string> listPreventiveScore = CommonMethod.GetAttributeValues(sXmlOUT, "ALL_XML_OUT/Shop", "PreventiveScore");
            List<string> listCurativeScore = CommonMethod.GetAttributeValues(sXmlOUT, "ALL_XML_OUT/Shop", "CurativeScore");
            List<string> listScoreDetails = CommonMethod.GetAttributeValues(sXmlOUT, "ALL_XML_OUT/Shop", "ScoreDetails");
            List<string> listScoreLastChangeDate = CommonMethod.GetAttributeValues(sXmlOUT, "ALL_XML_OUT/Shop", "ScoreLastChangeDate");
            SetAgencyScoreInfos(listPreventiveScore[0], listCurativeScore[0], listScoreDetails[0], listScoreLastChangeDate[0]);

            if (listAccountNumber.Count > 0 && listAccountNumber[0].Length > 0)
                _sAccountNumber = listAccountNumber[0];
            if (listID.Count > 0 && listID[0].Length > 0)
                lblID.Text = listID[0];
            if (listIDPDVSAB.Count > 0 && listIDPDVSAB[0].Length > 0)
                lblIDPDVSAB.Text = listIDPDVSAB[0];
            if (listBarCode.Count > 0 && listBarCode[0].Length > 0)
                staffManagement.IdPro = listBarCode[0];
            else if (listID.Count > 0 && listID[0].Length > 0)
                staffManagement.IdPro = listID[0];
            if (listIDBimedia.Count > 0 && listIDBimedia[0].Length > 0)
                lblIDBimedia.Text = listIDBimedia[0];
            if (listSiret.Count > 0 && listSiret[0].Length > 0)
                txtSiret.Text = listSiret[0];
            if (listRaisonSocial.Count > 0 && listRaisonSocial[0].Length > 0)
                txtRaisonSociale.Text = listRaisonSocial[0];
            if (listNomCommercial2.Count > 0 && listNomCommercial2[0].Length > 0)
                txtNomCommercial.Text = listNomCommercial2[0];
            else
            {
                if (listNomCommercial.Count > 0 && listNomCommercial[0].Length > 0)
                    txtNomCommercial.Text = listNomCommercial[0];
            }

            if (listFirstActivation.Count > 0 && listFirstActivation[0].Length > 0) {
                DateTime dtFirstActivation;
                if(DateTime.TryParse(listFirstActivation[0], out dtFirstActivation))
                    lblFirstActivation.Text = dtFirstActivation.ToString("dd/MM/yyyy HH:mm");
                else
                    lblFirstActivation.Text = "<span style=\"font-style:italic;\">inconnue</span>";
            }

            if (listRegionCommerciale.Count > 0 && listRegionCommerciale[0].Length > 0)
            {
                lblRegionCommerciale.Text = listRegionCommerciale[0];
            }

            if (listAddress.Count > 0 && listAddress[0].Length > 0)
                txtAddress.Text = listAddress[0];
            if (listZipcode.Count > 0 && listZipcode[0].Length > 0)
                txtZip.Text = listZipcode[0];
            if (listCity.Count > 0 && listCity[0].Length > 0)
                txtCity.Text = listCity[0];

            if (listManagerFirstName.Count > 0 && listManagerFirstName[0].Length > 0)
                txtManagerFirstName.Text = listManagerFirstName[0];
            if (listManagerLastName.Count > 0 && listManagerLastName[0].Length > 0)
                txtManagerLastName.Text = listManagerLastName[0];
            if (listPhoneNumber.Count > 0 && listPhoneNumber[0].Length > 0)
                txtPhoneNumber.Text = listPhoneNumber[0];
            if (listMobilePhone.Count > 0 && listMobilePhone[0].Length > 0)
                txtMobilePhone.Text = listMobilePhone[0];
            if (listEmail.Count > 0 && listEmail[0].Length > 0)
                txtEmail.Text = listEmail[0];

            if (listLatitude.Count > 0 && listLatitude[0].Length > 0)
                hdnLatitude.Value = listLatitude[0];
            if (listLongitude.Count > 0 && listLongitude[0].Length > 0)
                hdnLongitude.Value = listLongitude[0];
        }
    }

    protected void SetAgencyScoreInfos(string sPreventiveScore, string sCurativeScore, string sScoreDetails, string sScoreLastChangeDate)
    {
        litScoreLastChangeDate.Text = sScoreLastChangeDate;

        DataTable dtScores = new DataTable();
        dtScores.Columns.Add("ScoreType");
        dtScores.Columns.Add("ScoreValue");
        dtScores.Columns.Add("ScoreDetails", typeof(DataTable));

        DataTable dtPrev = new DataTable();
        dtPrev.Columns.Add("Label");
        dtPrev.Columns.Add("Value");
        DataTable dtCura = dtPrev.Clone();

        string[] arAllScores = sScoreDetails.Split(';');
        for (int i = 0; i < arAllScores.Length; i++)
        {
            string[] arScoreLblValue = arAllScores[i].Split(':');
            if (arScoreLblValue.Length == 2)
            {
                object[] obj = new object[] { arScoreLblValue[0].Substring(arScoreLblValue[0].ToString().IndexOf('-') + 1).Trim(), arScoreLblValue[1] };
                if (arScoreLblValue[0].Trim().StartsWith("PREV"))
                    dtPrev.Rows.Add(obj);
                else if (arScoreLblValue[0].Trim().StartsWith("CURA"))
                    dtCura.Rows.Add(obj);
            }
        }

        dtScores.Rows.Add(new object[] { "Score préventif", sPreventiveScore, dtPrev });
        dtScores.Rows.Add(new object[] { "Score curatif", sCurativeScore, dtCura });

        rptAgentScore.DataSource = dtScores;
        rptAgentScore.DataBind();
    }

    protected void SetAgencyEquipmentList(int iRefAgency, string sToken)
    {
        SetAgencyMPADList(iRefAgency, sToken);
        SetAgencyPOSList(iRefAgency, sToken);
    }
    protected void SetAgencyMPADList(int iRefAgency, string sToken)
    {
        DataTable dt = Agency.getAgencyMpadList(iRefAgency.ToString(), sToken, 1000);

        if (dt.Rows.Count == 0)
            panelMPADListEmpty.Visible = true;
        else
        {
            rptMPAD.DataSource = dt;
            rptMPAD.DataBind();
        }
        
        _dtMPAD = dt;
    }
    protected void SetAgencyPOSList(int iRefAgency, string sToken)
    {
        DataTable dt = Agency.getAgencyPosList(iRefAgency.ToString(), sToken, 1000);

        if (dt.Rows.Count == 0)
            panelPosListEmpty.Visible = true;
        else
        {
            rptPOS.DataSource = dt;
            rptPOS.DataBind();
        }
        
        _dtPOS = dt;
    }

    protected void SetAgencyActivity(int iRefAgency, string sMode, string sToken)
    {
        string sXmlOUT = Agency.GetAgencyActivity(iRefAgency, sMode, sToken);

        if (sXmlOUT.Length > 0)
        {
            List<string> listRC = CommonMethod.GetAttributeValues(sXmlOUT, "ALL_XML_OUT/H", "RC");
            List<string> listErrorMessage = CommonMethod.GetAttributeValues(sXmlOUT, "ALL_XML_OUT/H", "ERROR_MSG");

            if (listRC.Count > 0 && listRC[0] == "0")
            {
                List<string> listBalance = CommonMethod.GetAttributeValues(sXmlOUT, "ALL_XML_OUT/MerchantOperationsReportInfos/MerchantOperationsInfos/BalanceInfos", "mBalance");
                List<string> listNbPack = CommonMethod.GetAttributeValues(sXmlOUT, "ALL_XML_OUT/MerchantOperationsReportInfos/MerchantOperationsInfos/PackInfos", "iNbPack");
                List<string> listPackAmount = CommonMethod.GetAttributeValues(sXmlOUT, "ALL_XML_OUT/MerchantOperationsReportInfos/MerchantOperationsInfos/PackInfos", "mPackSaleAmount");
                List<string> listOperations = CommonMethod.GetTags(sXmlOUT, "ALL_XML_OUT/MerchantOperationsReportInfos/MerchantOperationsInfos/OperationsInfos/MerchantOperationsCount");

                lblPackQty.Text = (listNbPack.Count > 0) ? listNbPack[0] : "";
                lblPackTotalAmnt.Text = (listPackAmount.Count > 0) ? listPackAmount[0].Replace('.', ',').Replace("-", "") : "";

                for (int i = 0; i < listOperations.Count; i++)
                {
                    List<string> listOPE = CommonMethod.GetAttributeValues(listOperations[i], "MerchantOperationsCount", "OPE");
                    List<string> listNB_OPE = CommonMethod.GetAttributeValues(listOperations[i], "MerchantOperationsCount", "NB_OPE");
                    List<string> listOPE_TOT_AMNT = CommonMethod.GetAttributeValues(listOperations[i], "MerchantOperationsCount", "OPE_TOT_AMNT");

                    if (listOPE.Count > 0)
                    {
                        switch (listOPE[0])
                        {
                            case "ACTIVATION COM":
                                lblActivationQty.Text = (listNB_OPE.Count > 0) ? listNB_OPE[0] : "0";
                                break;
                            case "DEPOT INIT":
                                lblCashDepositOpenQty.Text = (listNB_OPE.Count > 0) ? listNB_OPE[0] : "0";
                                lblCashDepositOpenTotalAmnt.Text = (listOPE_TOT_AMNT.Count > 0 && listOPE_TOT_AMNT[0].Length > 0) ? listOPE_TOT_AMNT[0].Replace('.', ',').Replace("-", "") : "0,00";
                                break;
                            case "DEPOT":
                                lblCashDepositQty.Text = (listNB_OPE.Count > 0) ? listNB_OPE[0] : "0";
                                lblCashDepositTotalAmnt.Text = (listOPE_TOT_AMNT.Count > 0 && listOPE_TOT_AMNT[0].Length > 0) ? listOPE_TOT_AMNT[0].Replace('.', ',').Replace("-", "") : "0,00";
                                break;
                            case "RETRAIT":
                                lblCashWithdrawQty.Text = (listNB_OPE.Count > 0) ? listNB_OPE[0] : "";
                                lblCashWithdrawTotalAmnt.Text = (listOPE_TOT_AMNT.Count > 0 && listOPE_TOT_AMNT[0].Length > 0) ? listOPE_TOT_AMNT[0].Replace('.', ',').Replace("-", "") : "0,00";
                                break;
                            case "PAIEMENT ONUS":
                                lblPaymentOnUsQty.Text = (listNB_OPE.Count > 0) ? listNB_OPE[0] : "";
                                lblPaymentOnUsTotalAmnt.Text = (listOPE_TOT_AMNT.Count > 0 && listOPE_TOT_AMNT[0].Length > 0) ? listOPE_TOT_AMNT[0].Replace('.', ',').Replace("-", "") : "0,00";
                                break;
                        }
                    }
                }
            }
            else{
                string sMessage = (listErrorMessage.Count > 0 && listErrorMessage[0].Length > 0) ? listErrorMessage[0] : "Une erreur est survenue";
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "AlertMessageKey", "AlertMessage('Erreur', \"" + sMessage + "\", null, null);", true);
            }
        }
    }
    protected void rblPeriod_SelectedIndexChanged(object sender, EventArgs e)
    {
        string sToken = authentication.GetCurrent().sToken;
        SetAgencyActivity(_iRefAgency, rblPeriod.SelectedValue, sToken);
        panelActivityDetails.Visible = true;
    }
    protected void btnLoadFirstActivity_Click(object sender, EventArgs e)
    {
        if (rblPeriod.SelectedIndex != 0)
        {
            string sToken = authentication.GetCurrent().sToken;
            SetAgencyActivity(_iRefAgency, rblPeriod.SelectedValue, sToken);
            panelActivityDetails.Visible = true;
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "RefreshPeriodControlKey", "RefreshPeriodControl();", true);
        }
    }

    protected void btnMaintenance_Click(object sender, EventArgs e)
    {
        ImageButton btn = (ImageButton)sender;

        if (btn != null)
        {
            SetAgencyMPADList(_iRefAgency, authentication.GetCurrent().sToken);
            string sRefMP = btn.CommandArgument;

            if (sRefMP.Length > 0)
            {
                btnRefresh.CommandArgument = sRefMP;

                for (int i = 0; i < _dtMPAD.Rows.Count; i++)
                {
                    if (_dtMPAD.Rows[i]["MP_Ref"].ToString() == sRefMP)
                    {
                        hdnRefMPAD.Value = sRefMP;
                        if (_dtMPAD.Rows[i]["A2_RA"].ToString().ToLower() == "true")
                        {
                            if (_dtMPAD.Rows[i]["NET_VPN_IP"].ToString().Length > 0)
                            {
                                panelAccessRequestSent.Visible = false;
                                lnkAccessVPN.Visible = true;
                                lnkAccessVPN.Text = "- Accéder à la borne (Ethernet / " + _dtMPAD.Rows[i]["NET_VPN_IP"].ToString() + ")";
                                lnkAccessVPN.NavigateUrl = "http://" + _dtMPAD.Rows[i]["NET_VPN_IP"].ToString();
                            }
                            else
                            {
                                panelAccessRequestSent.Visible = true;
                                lnkAccessVPN.Visible = false;
                            }

                            rblRemoteAccess.SelectedIndex = 0;
                        }
                        else
                        {
                            panelAccessRequestSent.Visible = false;
                            lnkAccessVPN.Visible = false;
                            rblRemoteAccess.SelectedIndex = 1;
                        }

                        if (_dtMPAD.Rows[i]["NET_ETH_IP"].ToString().Length > 0)
                        {
                            lnkLocalEth.Text = "- Accéder à la borne (Ethernet / " + _dtMPAD.Rows[i]["NET_ETH_IP"].ToString() + ")";
                            lnkLocalEth.NavigateUrl = "http://" + _dtMPAD.Rows[i]["NET_ETH_IP"].ToString();
                            lnkLocalEth.Visible = true;
                        }
                        else lnkLocalEth.Visible = false;

                        if (_dtMPAD.Rows[i]["NET_WLAN_IP"].ToString().Length > 0)
                        {
                            lnkLocalWifi.Text = "- Accéder à la borne (Wifi / " + _dtMPAD.Rows[i]["NET_WLAN_IP"].ToString() + ")";
                            lnkLocalWifi.NavigateUrl = "http://" + _dtMPAD.Rows[i]["NET_WLAN_IP"].ToString();
                            lnkLocalWifi.Visible = true;
                        }
                        else lnkLocalWifi.Visible = false;

                        if (lnkLocalEth.Visible == false && lnkLocalWifi.Visible == false)
                            panelLocalAccess.Visible = false;
                        else panelLocalAccess.Visible = true;
                    }
                }

                upMaintenance.Update();
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "ShowMaintenancePopupKey", "ShowMaintenancePopup();", true);
            }
        }

        upAgencyEquipment.Update();
    }
    protected void rblRemoteAccess_SelectedIndexChanged(object sender, EventArgs e)
    {
        string sValue = rblRemoteAccess.SelectedValue;

        if (SetMPADInfo(new XElement("ALL_XML_IN", new XElement("H", new XAttribute("ID", hdnRefMPAD.Value), new XAttribute("A2_RA", sValue))).ToString()))
        {
            if (sValue == "1")
                panelAccessRequestSent.Visible = true;
            else panelAccessRequestSent.Visible = false;
        }
        else
        {
            if(sValue == "1")
                rblRemoteAccess.SelectedIndex = 0;
            else rblRemoteAccess.SelectedIndex = 1;
        }

        if(sender.GetType() == typeof(RadioButtonList))
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "ShowMaintenancePopupKey", "ShowMaintenancePopup();", true);
    }

    protected bool SetMPADInfo(string sMPADInfo)
    {
        SqlConnection conn = null;
        string sXmlOut = "";
        bool bOk = false;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["PARTNER"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("SY_HW.P_SetMPADInformation", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@sXmlIn", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@sXmlOut", SqlDbType.VarChar, 8000);

            cmd.Parameters["@sXmlIn"].Value = sMPADInfo;

            cmd.Parameters["@sXmlOut"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            sXmlOut = cmd.Parameters["@sXmlOut"].Value.ToString();

            if (sXmlOut.Length > 0)
            {
                List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/H", "RC");
                if (listRC.Count > 0 && listRC[0] == "0")
                    bOk = true;
            }
        }
        catch (Exception ex)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return bOk;
    }
    protected void btnForceCloseMaintenance_Click(object sender, EventArgs e)
    {
        rblRemoteAccess.SelectedIndex = 1;

        rblRemoteAccess_SelectedIndexChanged(sender, e);
        upMaintenance.Update();

        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "CloseAllDialogsKey", "CloseAllDialogs();", true);
    }

    protected void btnLoadDashboard_Click(object sender, EventArgs e)
    {
        if (!_bDashboardInitialized)
        {
            panelStatsPRO.Attributes["style"] = "display:initial";
            _bDashboardInitialized = true;
            StatsPRO1.AccountNumber = _sAccountNumber;
            StatsPRO1.RefreshDashboard();
        }
    }

    protected void btnLoadAgencySettings_Click(object sender, EventArgs e)
    {
        panelReadSettings.Visible = true;
        panelEditSettings.Visible = false;
        authentication _auth = authentication.GetCurrent();
        float fAmount;

        if (!getPosBalanceAlert(lblIDPDVSAB.Text, out fAmount))
        {
            txtSmsAlertAmount.Text = fAmount.ToString().Replace('.', ',');
            lblSmsAlertAmount.Text = "- &euro;";
        }
        else
        {
            txtSmsAlertAmount.Text = fAmount.ToString().Replace('.', ',');
            lblSmsAlertAmount.Text = fAmount.ToString().Replace('.', ',') + " &euro;";
        }

    }
    protected void btnCancelEditAgencySettings_Click(object sender, EventArgs e)
    {
        panelReadSettings.Visible = true;
        panelEditSettings.Visible = false;
    }
    protected void btnSaveAgencySettings_Click(object sender, EventArgs e)
    {
        lblSmsAlertAmountError.Text = "";
        authentication _auth = authentication.GetCurrent();
        string sAmount = txtSmsAlertAmount.Text;
        float fAmount;

        if (float.TryParse(sAmount.Replace('.', ','), NumberStyles.AllowDecimalPoint, CultureInfo.CreateSpecificCulture("fr-FR"), out fAmount))
            if(savePosBalanceAlert(lblIDPDVSAB.Text, fAmount))
            {
                lblSmsAlertAmount.Text = fAmount.ToString().Replace('.', ',') + " &euro;";
                panelEditSettings.Visible = false;
                panelReadSettings.Visible = true;
            }
        else
            lblSmsAlertAmountError.Text = "Montant non valide";
    }
    protected void btnEditAgencySettings_Click(object sender, EventArgs e)
    {
        lblSmsAlertAmountError.Text = "";
        panelReadSettings.Visible = false;
        panelEditSettings.Visible = true;
    }

    protected bool getPosBalanceAlert(string sIdPDV, out float fAmount)
    {
        SqlConnection conn = null;
        string sXmlOut = "";
        bool bOk = false;
        string sAmount = "";
        fAmount = 0;
        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_GETPOSBalanceAlert", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IDPDV", SqlDbType.VarChar, 20);
            cmd.Parameters.Add("@DefineSMSAlertAmount", SqlDbType.VarChar, 500);

            cmd.Parameters["@IDPDV"].Value = sIdPDV;
            cmd.Parameters["@DefineSMSAlertAmount"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            sAmount = cmd.Parameters["@DefineSMSAlertAmount"].Value.ToString();

            if (float.TryParse(sAmount.Replace('.', ','), NumberStyles.AllowDecimalPoint, CultureInfo.CreateSpecificCulture("fr-FR"), out fAmount))
                bOk = true;

            /*
             * sXmlOut = cmd.Parameters["@sXmlOut"].Value.ToString();

            if (sXmlOut.Length > 0)
            {
                List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/H", "RC");
                if (listRC.Count > 0 && listRC[0] == "0")
                    bOk = true;
            }*/
        }
        catch (Exception ex)
        {
            bOk = false;
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return bOk;
    }
    protected bool savePosBalanceAlert(string sIdPDV, float fAmount) 
    {
        SqlConnection conn = null;
        string sXmlOut = "";
        bool bOk = false;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_SetPOSBalanceAlert", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IDPDV", SqlDbType.VarChar, 20);
            cmd.Parameters.Add("@DefineSMSAlertAmount", SqlDbType.VarChar, 500);
            cmd.Parameters.Add("@RC", SqlDbType.Int);

            cmd.Parameters["@IDPDV"].Value = sIdPDV;
            cmd.Parameters["@DefineSMSAlertAmount"].Value = fAmount.ToString().Replace('.', ',');
            cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;

            cmd.ExecuteNonQuery();

            string sRc = cmd.Parameters["@RC"].Value.ToString();
            if (sRc.Trim() == "0")
                bOk = true;
        }
        catch (Exception ex)
        {
            bOk = false;
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return bOk;
    }

    protected bool sendContractByMail(string sEmail)
    {
        bool isOK = false;
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("[NoBank].[PDV].[P_SendContractFromZendesk]", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@Siren", SqlDbType.VarChar, 50);
            cmd.Parameters.Add("@EmailDest", SqlDbType.VarChar, 500);
            cmd.Parameters.Add("@RC", SqlDbType.Int);

            cmd.Parameters["@Siren"].Value = txtSiret.Text.Substring(0,9);
            cmd.Parameters["@EmailDest"].Value = sEmail;

            cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;

            cmd.ExecuteNonQuery();

            string sRc = cmd.Parameters["@RC"].Value.ToString();

            if (sRc.Trim() == "0")
                isOK = true;
        }
        catch (Exception ex)
        {
            isOK = false;
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return isOK;
    }
    protected void btnSendContractToMe_Click(object sender, EventArgs e)
    {
        authentication auth = authentication.GetCurrent();
        if (auth.sEmail.Trim().Length > 0)
            ShowContractResult(sendContractByMail(auth.sEmail));
    }
    protected void btnSendContractToTobacco_Click(object sender, EventArgs e)
    {
        ShowContractResult(sendContractByMail(""));
    }

    protected void ShowContractResult(bool isOK)
    {
        string sMessage = "";
        if (isOK)
            sMessage = "<span style='color:green'>Le contrat a bien été envoyé</span>";
        else
            sMessage = "<span style='color:red'>Le contrat n'a pas pu être envoyé car une erreur est survenue.</span>";

        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "AlertMessageSendContract", "AlertMessage('Envoi du contrat buraliste', \"" + sMessage + "\", null, null);", true);
    }

    protected bool SaveAgencyInformation(string sXmlIn)
    {
        bool isOK = false;
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("[Partner].[Partner].[P_NickelSetAgencies]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@Action", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@ActionResult", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@RC", SqlDbType.Int);
            cmd.Parameters["@Action"].Value = sXmlIn;
            cmd.Parameters["@ActionResult"].Direction = ParameterDirection.Output;
            cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;
            cmd.ExecuteNonQuery();

            string sRc = cmd.Parameters["@RC"].Value.ToString();
            string sXmlOut = cmd.Parameters["@ActionResult"].Value.ToString();

            //<ALL_XML_OUT><Result RC="229" SIRET="75388609200026"  /></ALL_XML_OUT>

            if (sRc.Trim() == "0" && CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Result", "RC").Count > 0 && 
                CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Result", "RC")[0].Trim() == "0")
            {
                isOK = true;
            }
        }
        catch (Exception ex)
        {
            isOK = false;
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return isOK;
    }
    protected void btnSaveAgencyInformation_Click(object sender, EventArgs e)
    {
        string sMessage = "";
        authentication auth = authentication.GetCurrent();

        if (_iRefAgency > 0)
        {
            if (checkAgencyInformation())
            {

                string sXml = new XElement("ALL_XML_IN", new XElement("UPDATE",
                                    new XAttribute("refBoUser", auth.sRef),
                                    new XAttribute("refAgency", _iRefAgency.ToString()),
                                    new XAttribute("SIRET", txtSiret.Text.Trim()),
                                    new XAttribute("AgencyName", txtNomCommercial.Text.Trim()),
                                    new XAttribute("AgencyName2", txtRaisonSociale.Text.Trim()),
                                    new XAttribute("ManagerFirstName", txtManagerFirstName.Text.Trim()),
                                    new XAttribute("ManagerLastName", txtManagerLastName.Text.Trim()),
                                    new XAttribute("PhoneNumber", txtPhoneNumber.Text),
                                    new XAttribute("Mobile", txtMobilePhone.Text.Trim()),
                                    new XAttribute("Email", txtEmail.Text.Trim()))).ToString();


                if (SaveAgencyInformation(sXml))
                    sMessage = "Les informations ont bien été enregistrées.";
                else
                    sMessage = "Une erreur est survenue.";

                SetAgencyInfos(_iRefAgency, auth.sToken);
                upAgencyGeneralInfo.Update();
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "ShowMessageKey", "ShowMessage('Enregistrement informations agence', \"" + sMessage + "\");", true);
            }
        }
        else
            Response.Redirect("Default.aspx", true);
    }
    protected bool checkAgencyInformation()
    {
        bool isOK = true;
        string sMessage = "Veuillez corriger les champs suivants : <ul>";

        if(txtNomCommercial.Text.Trim().Length == 0)
        {
            isOK = false;
            sMessage += "<li>Nom commercial requis</li>";
        }
        if (txtRaisonSociale.Text.Trim().Length == 0)
        {
            isOK = false;
            sMessage += "<li>Raison sociale requise</li>";
        }

        if(txtSiret.Text.Trim().Length == 0)
        {
            isOK = false;
            sMessage += "<li>Siret requis</li>";
        }
        else if(txtSiret.Text.Trim().Length != 14)
        {
            isOK = false;
            sMessage += "<li>Siret non valide</li>";
        }

        if (txtManagerFirstName.Text.Trim().Length == 0)
        {
            isOK = false;
            sMessage += "<li>Nom gérant requis</li>";
        }

        if (txtManagerFirstName.Text.Trim().Length == 0)
        {
            isOK = false;
            sMessage += "<li>Prénom gérant requis</li>";
        }
        
        if (txtMobilePhone.Text.Length == 0)
        {
            isOK = false;
            sMessage += "<li>N° de mobile requis</li>";
        }
        else if (!tools.IsPhoneValid(txtMobilePhone.Text, true))
        {
            isOK = false;
            sMessage += "<li>N° de mobile non valide</li>";
        }

        if (txtPhoneNumber.Text.Length == 0)
        {
            isOK = false;
            sMessage += "<li>N° de fixe requis</li>";
        }
        else if (!tools.IsPhoneFixedFrenchValid(txtPhoneNumber.Text))
        {
            isOK = false;
            sMessage += "<li>N° de fixe non valide</li>";
        }

        if (txtEmail.Text.Length > 0 && !tools.IsEmailValid(txtEmail.Text))
        {
            isOK = false;
            sMessage += "<li>Adresse email non valide</li>";
        }

        sMessage += "</ul>";
        if(!isOK)
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "ShowMessageKey", "ShowMessage('Erreur', \"" + sMessage + "\");", true);

        return isOK;
    }
    protected void btnSaveAgencyAddress_Click(object sender, EventArgs e)
    {
        string sMessage = "";
        authentication auth = authentication.GetCurrent();

        if (_iRefAgency > 0)
        {
            if (checkAgencyAddress())
            {
                string sXml = new XElement("ALL_XML_IN", new XElement("UPDATE",
                                    new XAttribute("refBoUser", auth.sRef),
                                    new XAttribute("refAgency", _iRefAgency.ToString()),
                                    new XAttribute("Address", txtAddress.Text.Trim()),
                                    new XAttribute("City", txtCity.Text.Trim()),
                                    new XAttribute("Zip", txtZip.Text.Trim()))).ToString();

                if (SaveAgencyInformation(sXml))
                    sMessage = "Les informations ont bien été enregistrées.";
                else
                    sMessage = "Une erreur est survenue.";

                SetAgencyInfos(_iRefAgency, auth.sToken);
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "ShowMessageKey", "ShowMessage('Enregistrement adresse agence', \"" + sMessage + "\");InitGMap();", true);
            }
        }
        else
            Response.Redirect("Default.aspx", true);
    }
    protected bool checkAgencyAddress()
    {
        bool isOK = true;
        string sMessage = "Veuillez corriger les champs suivants : <ul>";

        if (txtZip.Text.Length == 0)
        {
            isOK = false;
            sMessage += "<li>Code postal requis</li>";
        }
        else if (!tools.IsValid(txtZip.Text, @"^[0-9]{5}$"))
        {
            isOK = false;
            sMessage += "<li>Code postal non valide</li>";
        }

        if(txtCity.Text.Trim().Length == 0)
        {
            isOK = false;
            sMessage += "<li>Ville requise</li>";
        }

        if (txtAddress.Text.Trim().Length == 0)
        {
            isOK = false;
            sMessage += "<li>Adresse requise</li>";
        }

        sMessage += "</ul>";
        if (!isOK)
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "ShowMessageKey", "ShowMessage('Erreur', \"" + sMessage + "\");", true);

        return isOK;
    }
}