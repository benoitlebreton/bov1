﻿<%@ Page Title="Réinitialisation du code PIN vendeur - BO Compte-Nickel" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="PincodeReset.aspx.cs" Inherits="PincodeReset" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <style type="text/css">
        .ui-combobox {
        position: relative;
        display: inline-block;
        }
        .ui-combobox-toggle {
        position: absolute;
        top: 0;
        bottom: 0;
        margin-left: -1px;
        padding: 0;
        /* support: IE7 */
        *height: 1.7em;
        *top: 0.1em;
        }
        .ui-combobox-input {
        margin: 0;
        padding: 0.3em;
        width:300px;
        height:30px;
        }

        .ui-autocomplete {
            max-width:305px;
        }

    </style>
    <script type="text/javascript">
        (function ($) {
            $.widget("ui.combobox", {
                _create: function () {
                    var input,
                            that = this,
                            wasOpen = false,
                            select = this.element.hide(),
                            selected = select.children(":selected"),
                            value = selected.val() ? selected.text() : "",
                            wrapper = this.wrapper = $("<span>")
                            .addClass("ui-combobox")
                            .insertAfter(select);
                    function removeIfInvalid(element) {
                        var value = $(element).val(),
                                matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex(value) + "$", "i"),
                                valid = false;
                        select.children("option").each(function () {
                            if ($(this).text().match(matcher)) {
                                this.selected = valid = true;
                                return false;
                            }
                        });
                        if (!valid) {
                            // remove invalid value, as it didn't match anything
                            $(element)
                                    .val("")
                                    .attr("title", value + " n'existe pas")
                                    .tooltip("open");
                            select.val("");
                            setTimeout(function () {
                                input.tooltip("close").attr("title", "");
                            }, 2500);
                            input.data("ui-autocomplete").term = "";
                        }
                    }
                    input = $("<input>")
                            .appendTo(wrapper)
                            .val(value)
                            .attr("title", "")
                            .addClass("ui-state-default ui-combobox-input")
                            .autocomplete({
                                delay: 0,
                                minLength: 0,
                                source: function (request, response) {
                                    var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
                                    response(select.children("option").map(function () {
                                        var text = $(this).text();
                                        if (this.value && (!request.term || matcher.test(text)))
                                            return {
                                                label: text.replace(
                                                    new RegExp(
                                                    "(?![^&;]+;)(?!<[^<>]*)(" +
                                                    $.ui.autocomplete.escapeRegex(request.term) +
                                                    ")(?![^<>]*>)(?![^&;]+;)", "gi"
                                                    ), "<strong>$1</strong>"),
                                                value: text,
                                                option: this
                                            };
                                    }));
                                },
                                select: function (event, ui) {
                                    ui.item.option.selected = true;
                                    that._trigger("selected", event, {
                                        item: ui.item.option
                                    });
                                    __doPostBack('__Page', 'onAgencyIdChanged');
                                },
                                change: function (event, ui) {
                                    if (!ui.item) {
                                        removeIfInvalid(this);
                                        __doPostBack('__Page', 'onAgencyIdChanged');
                                    }
                                }
                            })
                            .addClass("ui-widget ui-widget-content ui-corner-left");
                    input.data("ui-autocomplete")._renderItem = function (ul, item) {
                        return $("<li>")
                            .append("<a>" + item.label + "</a>")
                            .appendTo(ul);
                    };
                    $("<a>")
                            .attr("tabIndex", -1)
                            .attr("title", "Afficher tout")
                            .tooltip()
                            .appendTo(wrapper)
                            .button(
                                {
                                    icons: {
                                        primary: "ui-icon-triangle-1-s"
                                    },
                                    text: false
                                }
                            )
                            .removeClass("ui-corner-all")
                            .addClass("ui-corner-right ui-combobox-toggle")
                            .mousedown(function () {
                                wasOpen = input.autocomplete("widget").is(":visible");
                            })
                            .click(function () {
                                input.focus();
                                // close if already visible
                                if (wasOpen) {
                                    return;
                                }
                                // pass empty string as value to search for, displaying all results
                                input.autocomplete("search", "");
                            });
                    input.tooltip({
                        tooltipClass: "ui-state-highlight"
                    });
                },
                _destroy: function () {
                    this.wrapper.remove();
                    this.element.show();
                }
            });
        })(jQuery);

        $(function () {
            init();
        });

        function init() {
            $("#<%= ddlAgencyID.ClientID %>").combobox();
            if ($("#<%= ddlAgencyID.ClientID %>").val().trim().length > 0) {
                $('#divSellerChoice').css('visibility', 'visible');
                $('#<%=btnResetPincodeSeller.ClientID %>').css('visibility', 'visible');
                $("#<%= ddlSeller.ClientID %>").combobox();
            }
            else {
                $('#divSellerChoice').css('visibility', 'hidden');
                $('#<%=btnResetPincodeSeller.ClientID %>').css('visibility', 'hidden');
            }
            
            $('#<%=ddlAgencyID.ClientID%>').next().find('input').focus();
        }

        function ConfirmResetPincode() {
            $('#<%=txtCustomPincode.ClientID%>').val("")
            setTimeout(function () { $('#<%=txtCustomPincode.ClientID%>').focus(); }, 500);
            $("#divConfirmPincodeReset").dialog({
                resizable: false,
                width: 600,
                modal: true,
                title: "Réinitialisation code PIN vendeur",
                buttons: {
                    "Annuler": function () { $(this).dialog("close"); },
                    "Valider": function () {
                        if (Page_IsValid) {
                            $('#<%=btnResetPincode.ClientID%>').click();
                            $(this).dialog("close");
                        }
                    }
                }
            });
            $("#divConfirmPincodeReset").parent().appendTo(jQuery("form:first"));
        }

        function PincodeChanged() {
            $("#divPincodeResetResult").dialog({
                resizable: false,
                width: 600,
                modal: true,
                title: "Réinitialisation code PIN vendeur",
                buttons: {
                    "Fermer": function () { $(this).dialog("close"); }
                }
            });
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <h2 style="color: #344b56; margin-bottom: 10px">
        Réinitialisation du code PIN vendeur
    </h2>
    <asp:Panel ID="panelSellerType" runat="server" DefaultButton="btnResetPincodeSeller">
        <div style="margin:auto; width:400px;">
            <div style="display: table">
                <div style="display: table-row">
                    <div style="display: table-cell">
                        <label for="comboBoxAgencyID">
                            Agence :</label>
                    </div>
                    <div style="display: table-cell">
                        <span id="REQ-comboBoxAgencyID" class="reqStarStyle" style="visibility: hidden">*</span>
                    </div>
                </div>
            </div>
            <div style="padding-left: 5px;">
                <div class="ui-widget">
                    <asp:DropDownList ID="ddlAgencyID" runat="server" DataTextField="AgencyFullName" DataValueField="AgencyID"
                        AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="onAgencyIdChanged"
                        Width="400px" Height="30px">
                        <asp:ListItem Value="" Selected="True"></asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
                
            <div id="divSellerChoice" style="visibility: hidden; margin-top:20px">
                <div style="display: table;">
                    <div style="display: table-row">
                        <div style="display: table-cell;">
                            <label for="comboBoxAgencyID">
                                Vendeur :</label>
                        </div>
                        <div style="display: table-cell">
                            <asp:RequiredFieldValidator ID="reqSeller" runat="server" ControlToValidate="ddlSeller"
                                CssClass="reqStarStyle" ValidationGroup="vgResetPincodeSeller">*</asp:RequiredFieldValidator>
                        </div>
                    </div>
                </div>
                <div style="padding-left: 5px;">
                    <div class="ui-widget">
                        <asp:DropDownList ID="ddlSeller" runat="server" DataTextField="FULLNAME" DataValueField="RefUser"
                            Width="400px" AppendDataBoundItems="true" Height="30px" AutoPostBack="false">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div style="margin-top:50px; width:100%; text-align:center">
                <asp:Button ID="btnResetPincodeSeller" runat="server" CssClass="button" Text="MODIFIER PINCODE" ValidationGroup="vgResetPincodeSeller"
                    style="padding:10px 20px; height:40px; font-weight:bold" OnClick="btnResetSellerPinCodeClick"/>
            </div>
            <asp:HiddenField ID="hfAgencyIdSelected" runat="server" />
            <asp:HiddenField ID="hfAgencySearchType" runat="server" />
        </div>
    </asp:Panel>

    <div id="divConfirmPincodeReset" style="display: none">
        <asp:Panel ID="panelConfirmPincodeReset" runat="server" DefaultButton="btnResetPincode">
            <div>
                <asp:Label ID="lblAlertMessage" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
            </div>
            <div style="margin-top: 10px">
                Vous pouvez personnaliser le code PIN en remplissant le champs ci-dessous.</div>
            <div>
                Si vous voulez un code PIN choisi aléatoirement, laissez ce champs vide et validez.</div>
            <div style="margin-top: 10px; text-align: center;">
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtCustomPincode"
                    ValidationExpression="[0-9]{4}" Font-Bold="true" ForeColor="Red" ErrorMessage="Le code PIN doit contenir 4 chiffres"></asp:RegularExpressionValidator>
            </div>
            <div style="margin-top: 10px; text-align: center; font-size: 16px">
                code PIN
                <asp:TextBox ID="txtCustomPincode" runat="server" MaxLength="4" Width="50px" autocomplete="off"></asp:TextBox>
            </div>
            <asp:Button ID="btnResetPincode" runat="server" style="display:none" OnClick="clickResetPincode" />
        </asp:Panel>
    </div>

    <div id="divPincodeResetResult" style="text-align: center; margin: 30px 0 20px 0;">
        <asp:Label ID="lblResetResult" runat="server" Style="font-size: 1.4em; line-height: 40px"></asp:Label>
    </div>
</asp:Content>

