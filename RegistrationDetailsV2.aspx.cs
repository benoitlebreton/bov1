﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using XMLMethodLibrary;
using System.Text;
using System.Xml;
using System.IO;
using System.Globalization;
using System.Web.Hosting;
using CuteWebUI;

public partial class RegistrationDetailsV2 : System.Web.UI.Page
{
    public string sRefTransaction { get; set; }
    public string sRefCustomer { get; set; }
    protected bool bActionAllowed { get { return (ViewState["ActionAllowed"] != null) ? bool.Parse(ViewState["ActionAllowed"].ToString()) : false; } set { ViewState["ActionAllowed"] = value; } }
    protected DataTable dtDocs { get { return (ViewState["dtDocs"] != null) ? (DataTable)ViewState["dtDocs"] : null; } set { ViewState["dtDocs"] = value; } }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DeleteOldXml();

            if (Request.Params.Count > 0 && Request.Params["No"] != null && Request.Params["No"].Trim().Length > 0)
            {
                lblRegistrationSelected.Text = Request.Params["No"].Trim();
                divRegistrationDetails.Visible = true;

                registration registr = registration.GetCurrent(false, lblRegistrationSelected.Text);

                if (lblRegistrationSelected.Text.Trim() != "")
                    refresh(true);
            }
            else
            {
                registration.Clear(lblRegistrationSelected.Text);
                Response.Redirect("RegistrationSearchV2.aspx");
            }

            CheckRights();
        }
    }

    protected void CheckRights()
    {
        string sXmlOut = tools.GetRights(authentication.GetCurrent().sToken, "SC_RegistrationControl");

        List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Page", "RC");

        if (listRC.Count == 0 || listRC[0] != "0")
        {
            registration.Clear(lblRegistrationSelected.Text);
            Response.Redirect("Default.aspx", true);
        }
        else
        {
            List<string> listAction = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/Page/Action");

            for (int i = 0; i < listAction.Count; i++)
            {
                List<string> listTag = CommonMethod.GetAttributeValues(listAction[i], "Action", "TagAction");
                List<string> listActionAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ActionAllowed");
                List<string> listViewAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ViewAllowed");

                if (listTag.Count > 0 && listTag[0] == "RegistrationInfo")
                {
                    if (listActionAllowed[0] != "1")
                    {
                        btnForceSubscription.Visible = false;
                        btnValidate.Visible = false;
                        btnRectified.Visible = false;
                        //btnModify.Visible = false;
                        //btnModifyValidation.Visible = false;
                    }
                    else
                    {
                        bActionAllowed = true;
                    }
                    if (listViewAllowed[0] == "0")
                    {
                    }
                    break;
                }
            }
        }
    }
    protected void RegistrationChecked(string sStatus, string sStatusComment, string sNoComplianceList)
    {
        SqlConnection conn = null;
        authentication auth = authentication.GetCurrent();

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Partner"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("Registration.P_BackOffice_RegistrationAnalyse", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);

            string sXmlIN = new XElement("ALL_XML_IN",
                                                new XElement("DOC",
                                                    new XAttribute("RegistrationCode", lblRegistrationSelected.Text),
                                                    new XAttribute("CashierToken", auth.sToken),
                                                    new XAttribute("BackOfficeOpinion", sStatus),
                                                    new XAttribute("BackOfficeRemark", sStatusComment),
                                                    new XAttribute("BackOfficeNoCompliance", sNoComplianceList.Trim()),
                                                    new XAttribute("CultureID", (Session["CultureID"] != null) ? Session["CultureID"].ToString() : "Fr-fr")
                                                )
                                            ).ToString();

            cmd.Parameters["@IN"].Value = sXmlIN;
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sOut = cmd.Parameters["@OUT"].Value.ToString();

            if (sOut.Length > 0)
            {
                refresh();
                if (tools.GetValueFromXml(sOut, "ALL_XML_OUT/User", "Zendesk") == "0")
                {
                    string sMessage = "L'évaluation de l'inscription a bien été enregistrée dans le Backoffice mais n'as pas été enregistrée dans Zendesk.";
                    string sTitle = "Enregistrement évaluation Zendesk échoué";
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "alertMessage('" + sTitle.Replace("'", "&apos;") + "','" + sMessage.Replace("'", "&apos;") + "');", true);
                }
            }
            else
            {
            }
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

    }
    protected void onClickPreviousButton(object sender, EventArgs e)
    {
        registration.Clear(lblRegistrationSelected.Text);
        Response.Redirect("RegistrationSearchV2.aspx");
    }
    protected void onClickSaveClientModification(object sender, EventArgs e)
    {
        lblRegistrationSelected.Text = Request.Params["No"].Trim();
        string sError = "";
        string sMessage = "";

        if (checkClientInformations(out sError))
        {
            if (updateRegistration(getXmlFromClientInformations(), out sError))
                sMessage = "Les modifications ont bien été enregistrées.";
            else
                sMessage = "Les modifications n'ont pas été enregistrées.<br/>" + sError;

            refresh();
        }
        else
        {
            sError = (sError.Length > 0) ? "Veuillez corriger le(s) champs suivant(s) :" + sError : "";
            sMessage = "Les modifications n'ont pas été enregistrées.<br/>" + sError;
        }

        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "alertMessage('Enregistrement','" + sMessage.Replace("'", "&apos;").Trim() + "');", true);
    }

    /*** details function ***/
    protected string getDurationFormat(string sDate)
    {
        string sDuration = "";

        if (sDate.Trim().Length > 0 && sDate.Split(':').Length >= 3)
        {
            string[] tsDuration = sDate.Split(':');

            int iDurationTimeHH; int iDurationTimeMM; int iDurationTimeSS;

            if (int.TryParse(tsDuration[0].Trim(), out iDurationTimeHH) &&
                int.TryParse(tsDuration[1].Trim(), out iDurationTimeMM) &&
                int.TryParse(tsDuration[2].Trim(), out iDurationTimeSS))
            {
                string sDurationTimeHH = (iDurationTimeHH == 0) ? "" : iDurationTimeHH.ToString() + "h";
                string sDurationTimeMM = (iDurationTimeMM == 0) ? "" : iDurationTimeMM.ToString() + "min";
                string sDurationTimeSS = (iDurationTimeSS == 0) ? "" : iDurationTimeSS.ToString() + "s";
                sDuration = sDurationTimeHH + sDurationTimeMM + sDurationTimeSS;
            }
            else
                sDuration = sDate;
        }
        else
            sDuration = sDate;

        return sDuration;
    }

    protected void initAllFields()
    {
        txtCardNumber.Text = "";
        txtDepositAmount.Text = "";
        txtPackSaleWithActivation.Text = "";

        txtBirthDate.Text = "";
        lblBirthDate.Text = "";
        txtBirthCity.Text = "";
        lblBirthCity.Text = "";
        ddlBirthCountry.SelectedIndex = 0;
        lblBirthCountry.Text = ddlBirthCountry.SelectedItem.Text;
        if (lblBirthCountry.Text.Trim().Length == 0)
            lblBirthCountry.Text = "<span style=\"font-style:italic; text-transform:lowercase\">Non renseigné</span>";
        lblDocumentCountry.Text = ddlDocumentCountry.SelectedItem.Text;
        txtDocumentNumber.Text = "";
        lblDocumentCountry.Text = "";
        txtExpirationDate.Text = "";
        lblExpirationDate.Text = "";
        //rblGender.SelectedIndex = 0;
        //lblGender.Text = rblGender.SelectedValue;
        txtIDFirstName.Text = "";
        lblIDFirstName.Text = "";
        txtIDLastName.Text = "";
        lblIDLastName.Text = "";
        txtIssuedID.Text = "";
        lblIssuedID.Text = "";

        txtCity.Text = "";
        lblCity.Text = "";
        txtTo.Text = "";
        lblTo.Text = "";
        txtZipCode.Text = "";
        lblZipCode.Text = "";
        txtAddress1.Text = "";
        lblAddress1.Text = "";
        txtAddress2.Text = "";
        lblAddress2.Text = "";
        txtBiller.Text = "";
        lblBiller.Text = "";
        txtIssuedFact.Text = "";
        lblIssuedFact.Text = "";

        /*txtAddress2.ReadOnly = true;
        txtBiller.ReadOnly = true;
        txtTo.ReadOnly = true;
        txtIssuedFact.ReadOnly = true;
        txtIDMarriedLastName.ReadOnly = true;
        */

        rptSetNoComplianceList.DataSource = AML.getNoComplianceList();
        rptSetNoComplianceList.DataBind();

        ddlIDDocumentCountry.Items.Clear();
        ddlIDDocumentCountry.AppendDataBoundItems = true;
        ddlIDDocumentCountry.DataTextField = "CountryName";
        ddlIDDocumentCountry.DataValueField = "ISO2";
        ddlIDDocumentCountry.Items.Add(new ListItem("", ""));
        ddlIDDocumentCountry.DataSource = GetCountryList("ASC");
        ddlIDDocumentCountry.DataBind();

        ddlParentIDDocumentCountry.Items.Clear();
        ddlParentIDDocumentCountry.AppendDataBoundItems = true;
        ddlParentIDDocumentCountry.DataTextField = "CountryName";
        ddlParentIDDocumentCountry.DataValueField = "ISO2";
        ddlParentIDDocumentCountry.Items.Add(new ListItem("", ""));
        ddlParentIDDocumentCountry.DataSource = GetCountryList("ASC");
        ddlParentIDDocumentCountry.DataBind();

        authentication auth = authentication.GetCurrent();

        if (auth.cuCheckUser != null && auth.cuCheckUser.iLevel == 2)
        {
            ddlIDDocumentHolo.Items.Add(new ListItem("Douteux", "2"));
            ddlIDDocumentRF.Items.Add(new ListItem("Douteux", "2"));
            ddlIDDocumentPolice.Items.Add(new ListItem("Douteux", "2"));
            ddlIDDocumentPhoto.Items.Add(new ListItem("Douteux", "2"));
            ddlIDDocumentColor.Items.Add(new ListItem("Douteux", "2"));

            ddlParentIDDocumentHolo.Items.Add(new ListItem("Douteux", "2"));
            ddlParentIDDocumentRF.Items.Add(new ListItem("Douteux", "2"));
            ddlParentIDDocumentPolice.Items.Add(new ListItem("Douteux", "2"));
            ddlParentIDDocumentPhoto.Items.Add(new ListItem("Douteux", "2"));
            ddlParentIDDocumentColor.Items.Add(new ListItem("Douteux", "2"));
        }

        ddlBirthPlaceCountry.Items.Clear();
        ddlBirthPlaceCountry.DataSource = tools.GetCountryList();
        ddlBirthPlaceCountry.DataBind();

        ddlParentBirthPlaceCountry.Items.Clear();
        ddlParentBirthPlaceCountry.DataSource = tools.GetCountryList();
        ddlParentBirthPlaceCountry.DataBind();

        
    }

    protected void InitAllFieldsInfo(registration registr)
    {
        //LASTNAME
        DataTable dtLNTagInfoList;
        if (registration.GetStepInfoList(registr, 16, out dtLNTagInfoList) && dtLNTagInfoList.Rows.Count > 0)
        {
            ddlIDLastNameModificationReason.AppendDataBoundItems = true;
            ddlIDLastNameModificationReason.Items.Clear();
            ddlIDLastNameModificationReason.Items.Add(new ListItem("", ""));
            ddlIDLastNameModificationReason.DataTextField = "Label";
            ddlIDLastNameModificationReason.DataValueField = "TAG";
            ddlIDLastNameModificationReason.DataSource = dtLNTagInfoList;
            ddlIDLastNameModificationReason.DataBind();
        }
        //FIRSTNAME
        DataTable dtFNTagInfoList;
        if (registration.GetStepInfoList(registr, 17, out dtFNTagInfoList) && dtFNTagInfoList.Rows.Count > 0)
        {
            ddlIDFirstNameModificationReason.AppendDataBoundItems = true;
            ddlIDFirstNameModificationReason.Items.Clear();
            ddlIDFirstNameModificationReason.Items.Add(new ListItem("", ""));
            ddlIDFirstNameModificationReason.DataTextField = "Label";
            ddlIDFirstNameModificationReason.DataValueField = "TAG";
            ddlIDFirstNameModificationReason.DataSource = dtFNTagInfoList;
            ddlIDFirstNameModificationReason.DataBind();
        }

        //PARENT LASTNAME
        DataTable dtPLNTagInfoList;
        if (registration.GetStepInfoList(registr, 16, out dtPLNTagInfoList) && dtPLNTagInfoList.Rows.Count > 0)
        {
            ddlParentIDFirstNameModificationReason.AppendDataBoundItems = true;
            ddlParentIDFirstNameModificationReason.Items.Clear();
            ddlParentIDFirstNameModificationReason.Items.Add(new ListItem("", ""));
            ddlParentIDLastNameModificationReason.DataTextField = "Label";
            ddlParentIDLastNameModificationReason.DataValueField = "TAG";
            ddlParentIDLastNameModificationReason.DataSource = dtPLNTagInfoList;
            ddlParentIDLastNameModificationReason.DataBind();
        }
        //PARENT FIRSTNAME
        DataTable dtPFNTagInfoList;
        if (registration.GetStepInfoList(registr, 17, out dtPFNTagInfoList) && dtPFNTagInfoList.Rows.Count > 0)
        {
            ddlParentIDFirstNameModificationReason.AppendDataBoundItems = true;
            ddlParentIDFirstNameModificationReason.Items.Clear();
            ddlParentIDFirstNameModificationReason.Items.Add(new ListItem("", ""));
            ddlParentIDFirstNameModificationReason.DataTextField = "Label";
            ddlParentIDFirstNameModificationReason.DataValueField = "TAG";
            ddlParentIDFirstNameModificationReason.DataSource = dtPFNTagInfoList;
            ddlParentIDFirstNameModificationReason.DataBind();
        }
    }

    protected void getRegistrationDetails(string sRegistrationNumber)
    {
        string sXML = "";
        /*<ALL_XML_IN>
                <Enrolment RefTransaction=""
                    RegistrationCode="NOB205852968790"
                    CashierToken="0x9310D6EDA176B8C83C1FFD5E5E9230C1512562CC"
                    CultureID="fr-FR"
                />
        </ALL_XML_IN>*/

        SqlConnection conn = null;
        DataTable dt = new DataTable();
        authentication auth = authentication.GetCurrent();

        try
        {
            sXML = new XElement("ALL_XML_IN",
                        new XElement("Enrolment",
                            new XAttribute("RegistrationCode", sRegistrationNumber),
                            new XAttribute("CashierToken", auth.sToken),
                            new XAttribute("CultureID", "fr-FR")
                        )
                    ).ToString();

            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Partner"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("Registration.P_GetRegistrationTransactionDetailsWS", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@ALL_XML_IN", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@ALL_XML_OUT", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@RC", SqlDbType.Bit);

            cmd.Parameters["@ALL_XML_IN"].Value = sXML;

            cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;
            cmd.Parameters["@ALL_XML_OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            lblError.Text = "";

            string sXmlOut = cmd.Parameters["@ALL_XML_OUT"].Value.ToString();

            string rc = "", rc2 = "";
            GetValueFromXml(sXmlOut, "ALL_XML_OUT/Enrolment", "RC", out rc2);
            GetValueFromXml(sXmlOut, "ALL_XML_OUT/RESULT", "RC", out rc);

            if (rc != null && rc != "" && rc != "0")
            {
                //<ALL_XML_OUT><Customer RC="77001" RetMessage="L'accès au compte est restreint. Veuillez contacter la DCRCI"/></ALL_XML_OUT>


                if (rc == "71008")
                {
                    Session.Remove("Authentication");
                    //Response.Redirect("RegistrationSearch.aspx");
                }
                string sError = "";
                GetValueFromXml(sXmlOut, "ALL_XML_OUT/RESULT", "ErrorLabel", out sError);
                lblError.Text = sError;

            }
            else if (rc2 != null && rc2 != "" && rc2 != "0")
            {
                if (rc2 == "77002")
                {
                    List<string> lsRetMessage = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Enrolment", "RetMessage");
                    string sMessage = (lsRetMessage.Count > 0 && !string.IsNullOrWhiteSpace(lsRetMessage[0])) ? lsRetMessage[0].ToString().Trim() : "";
                    string sCurrentPage = "RegistrationSearchV2.aspx";//HttpContext.Current.Request.RawUrl.Replace("/","");

                    tools.CheckUnauthorizedAccess(rc2, sMessage, sCurrentPage);
                }
                else
                {
                    string sError = "";
                    GetValueFromXml(sXmlOut, "ALL_XML_OUT/Enrolment", "ErrorLabel", out sError);
                    lblError.Text = sError;
                }
            }
            else
            {
                registration registr = registration.GetCurrent(false, lblRegistrationSelected.Text);

                if (!string.IsNullOrWhiteSpace(registr.sRefRegistration))
                {
                    OnfidoResult1.RefTransaction = int.Parse(registr.sRefRegistration);
                    OnfidoResult1.RegistrationCode = sRegistrationNumber;
                }

                getDocumentsDetails(sXmlOut);

                string sFaceCapturePath = "";
                divFC.Visible = false;
                divParentFC.Visible = false;
                OcrFiles.getCaptureFacePath(lblRegistrationSelected.Text, out sFaceCapturePath);
                if (sFaceCapturePath.Trim().Length > 0)
                {

                    hfParentFaceCapturePath.Value = sFaceCapturePath;
                    if (divIDParent.Visible)
                    {
                        divParentFC.Visible = true;
                        hfParentFaceCapturePath.Value = sFaceCapturePath;
                    }
                    else
                    {
                        divFC.Visible = true;
                        hfFaceCapturePath.Value = sFaceCapturePath;
                    }
                }

                rRegistrationAttempts.DataSource = getRegistrationAttempts(sRegistrationNumber);
                rRegistrationAttempts.DataBind();
            }
        }
        catch (Exception ex)
        {
            lblError.Text = ex.ToString();
        }
        finally
        {
            if (lblError.Text.Trim().Length > 0)
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "showError();", true);
        }

    }

    protected void getDocumentResume(string RegistrationClosed, string sXML)
    {
        registration registr = registration.GetCurrent(true, lblRegistrationSelected.Text);
        panelSubscriptionAgency.Visible = false;
        panelSubscriptionCashier.Visible = false;
        panelSubscriptionDate.Visible = false;
        panelSubscriptionMpadTime.Visible = false;
        panelSubscriptionTpeTime.Visible = false;
        panelSubscriptionPhoneNumber.Visible = false;

        string sRegistrationDate = ""; string sRegistrationPhoneNumber = "";
        string sMpadTime = ""; string sTpeTime = "";
        string sAgencyName = ""; string sAgencyCity = ""; string sCashierName = "";
        string sIsPreSubscription = "";

        GetValueFromXml(sXML, "ALL_XML_OUT/Enrolment", "PhoneNumber", out sRegistrationPhoneNumber);
        lblRegistrationPhoneNumber.Text = sRegistrationPhoneNumber;

        //panelBtnShowClient.Visible = false;

        if (RegistrationClosed == "Y")
        {
            panelSubscriptionPhoneNumber.Visible = true;
            //panelBtnShowClient.Visible = true;
        }
        else if (sRegistrationPhoneNumber.Trim().Length > 0)
            panelSubscriptionPhoneNumber.Visible = true;

        GetValueFromXml(sXML, "ALL_XML_OUT/Enrolment", "ElapsedTimeMPAD", out sMpadTime);
        GetValueFromXml(sXML, "ALL_XML_OUT/Enrolment", "ElapsedTimePOS", out sTpeTime);
        GetValueFromXml(sXML, "ALL_XML_OUT/Enrolment", "ClosedDate", out sRegistrationDate);
        GetValueFromXml(sXML, "ALL_XML_OUT/Enrolment", "AgencyName", out sAgencyName);
        GetValueFromXml(sXML, "ALL_XML_OUT/Enrolment", "AgencyCity", out sAgencyCity);
        GetValueFromXml(sXML, "ALL_XML_OUT/Enrolment", "CashierName", out sCashierName);

        GetValueFromXml(sXML, "ALL_XML_OUT/Enrolment", "IsPreSubscription", out sIsPreSubscription);

        imgRegistrationHW.AlternateText = "Souscription BORNE";
        imgRegistrationHW.ToolTip = "Souscription BORNE";
        lblRegistrationHW.Text = "BORNE";
        imgRegistrationHW.ImageUrl = @"~/Styles/Img/borne.png";
        if (sIsPreSubscription.Trim() == "1")
        {
            imgRegistrationHW.AlternateText = "Souscription WEB";
            imgRegistrationHW.ToolTip = "Souscription WEB";
            imgRegistrationHW.ImageUrl = @"~/Styles/Img/web-responsive.png";
            lblRegistrationHW.Text = "WEB";
        }

        string reftransaction = "";
        GetValueFromXml(sXML, "ALL_XML_OUT/Enrolment", "RefTransaction", out reftransaction);
        sRefTransaction = (!string.IsNullOrWhiteSpace(reftransaction)) ? reftransaction : (!string.IsNullOrWhiteSpace(registr.sRefRegistration))? registr.sRefRegistration : "";

        string refcustomer = "";
        GetValueFromXml(sXML, "ALL_XML_OUT/Enrolment", "Refcustomer", out refcustomer);
        sRefCustomer = (!string.IsNullOrWhiteSpace(refcustomer)) ? refcustomer : (!string.IsNullOrWhiteSpace(registr.sRefCustomer)) ? registr.sRefCustomer : "";

        if (sRegistrationDate.Length > 0)
        {
            panelSubscriptionDate.Visible = true;
            lblRegistrationDate.Text = sRegistrationDate;
            registr.sRegistrationDate = sRegistrationDate;
        }

        if (sMpadTime.Length > 0 && sMpadTime != "00:00:00")
        {
            panelSubscriptionMpadTime.Visible = true;
            lblRegistrationMpadTime.Text = getDurationFormat(sMpadTime);
        }

        if (sTpeTime.Length > 0)
        {
            panelSubscriptionTpeTime.Visible = true;
            lblRegistrationTpeTime.Text = getDurationFormat(sTpeTime);
        }

        if (sAgencyName.Length > 0 || sAgencyCity.Length > 0)
        {
            panelSubscriptionAgency.Visible = true;
            lblRegistrationAgencyName.Text = sAgencyName;
            lblRegistrationAgencyCity.Text = sAgencyCity;
        }

        if (sCashierName.Length > 0)
        {
            panelSubscriptionCashier.Visible = true;
            lblRegistrationCashierName.Text = sCashierName;
        }
    }

    protected void getDocumentsDetails(string sXml)
    {
        int iNbDoc = 0;
        string sNbDoc = "";

        GetValueFromXml(sXml, "ALL_XML_OUT/Enrolment/DOCS", "Number", out sNbDoc);
        List<string> docList = CommonMethod.GetTags(sXml, "ALL_XML_OUT/Enrolment/DOCS/DOC");

        string sRegistrationClosed = "";
        GetValueFromXml(sXml, "ALL_XML_OUT/Enrolment", "Closed", out sRegistrationClosed);

        if (sRegistrationClosed.Trim() == "Y")
            hfRegistrationClosed.Value = "Y";
        else
            hfRegistrationClosed.Value = "N";
        // TEST
        //hfRegistrationClosed.Value = "N";

        getDocumentResume(sRegistrationClosed.Trim(), sXml);

        hfRegistrationChecked.Value = "N";
        string sCheckedStatus = ""; string sCheckedComment = ""; string sAgentCheck = ""; string sCheckedDate = "";
        GetValueFromXml(sXml, "ALL_XML_OUT/Enrolment", "BackOfficeOpinion", out sCheckedStatus);
        GetValueFromXml(sXml, "ALL_XML_OUT/Enrolment", "BackOfficeRemark", out sCheckedComment);
        GetValueFromXml(sXml, "ALL_XML_OUT/Enrolment", "BackOfficeUser", out sAgentCheck);
        GetValueFromXml(sXml, "ALL_XML_OUT/Enrolment", "BackOfficeOpinionDate", out sCheckedDate);

        lblRegistrationAgentCheck.Text = sAgentCheck;
        lblRegistrationCheckedDate.Text = sCheckedDate;
        panelRegistrationCheckedStatus.Visible = false;
        panelRegistrationAgentCheck.Visible = false;
        panelRegistrationAgentCheckDate.Visible = false;
        hfRegistrationCheckStatus.Value = sCheckedStatus;
        panelRegistrationNoCompliance.Visible = false;

        switch (sCheckedStatus)
        {
            case "G":
                panelRegistrationChecked.Visible = true;
                lblRegistrationChecked.Text = "OUI";
                lblRegistrationCheckedStatus.Text = "Conforme";
                lblRegistrationCheckedStatus.ForeColor = System.Drawing.Color.Green;
                imgRegistrationCheckedStatus.ImageUrl = "./Styles/Img/green_status.png"; ;
                imgRegistrationCheckedStatus.AlternateText = "(Conforme)";
                hfRegistrationChecked.Value = "Y";
                panelRegistrationCheckedStatus.Visible = true;
                panelRegistrationAgentCheck.Visible = true;
                panelRegistrationAgentCheckDate.Visible = true;
                txtRegistrationCheckedComment.Visible = false;
                lblRegistrationCheckedComment.Visible = true;
                registration.Clear(lblRegistrationSelected.Text);
                Response.Redirect("RegistrationSearchV2.aspx");
                break;
            case "O":
                panelRegistrationChecked.Visible = true;
                lblRegistrationChecked.Text = "OUI";
                lblRegistrationCheckedStatus.Text = "Suspecte";
                lblRegistrationCheckedStatus.ForeColor = System.Drawing.Color.Orange;
                imgRegistrationCheckedStatus.ImageUrl = "./Styles/Img/yellow_status.png"; ;
                imgRegistrationCheckedStatus.AlternateText = "(Suspecte)";
                hfRegistrationChecked.Value = "Y";
                panelRegistrationCheckedStatus.Visible = true;
                panelRegistrationAgentCheck.Visible = true;
                panelRegistrationAgentCheckDate.Visible = true;
                txtRegistrationCheckedComment.Visible = false;
                lblRegistrationCheckedComment.Visible = true;
                registration.Clear(lblRegistrationSelected.Text);
                Response.Redirect("RegistrationSearchV2.aspx");
                break;
            case "R":
                panelRegistrationChecked.Visible = true;
                lblRegistrationChecked.Text = "OUI";
                lblRegistrationCheckedStatus.Text = "Non conforme";
                lblRegistrationCheckedStatus.ForeColor = System.Drawing.Color.Red;
                imgRegistrationCheckedStatus.ImageUrl = "./Styles/Img/red_status.png"; ;
                imgRegistrationCheckedStatus.AlternateText = "(Non conforme)";
                hfRegistrationChecked.Value = "Y";
                panelRegistrationCheckedStatus.Visible = true;
                panelRegistrationAgentCheck.Visible = true;
                panelRegistrationAgentCheckDate.Visible = true;
                txtRegistrationCheckedComment.Visible = false;
                lblRegistrationCheckedComment.Visible = true;
                panelRegistrationNoCompliance.Visible = true;
                registration.Clear(lblRegistrationSelected.Text);
                Response.Redirect("RegistrationSearchV2.aspx");
                break;
            case "W":
                panelRegistrationChecked.Visible = false;
                lblRegistrationChecked.Text = "NON";
                lblRegistrationCheckedStatus.Text = "Non vérifiée";
                lblRegistrationCheckedStatus.ForeColor = System.Drawing.Color.Gray;
                imgRegistrationCheckedStatus.ImageUrl = "./Styles/Img/gray_status.png"; ;
                imgRegistrationCheckedStatus.AlternateText = "(Non vérifié)";
                hfRegistrationChecked.Value = "N";
                panelRegistrationCheckedStatus.Visible = true;
                panelRegistrationAgentCheck.Visible = false;
                panelRegistrationAgentCheckDate.Visible = false;
                txtRegistrationCheckedComment.Visible = true;
                lblRegistrationCheckedComment.Visible = false;
                break;
            default:
                panelRegistrationChecked.Visible = false;
                panelRegistrationAgentCheck.Visible = false;
                lblRegistrationChecked.Text = "NON";
                hfRegistrationChecked.Value = "N";
                panelRegistrationCheckedStatus.Visible = false;
                imgRegistrationCheckedStatus.ImageUrl = "./Styles/Img/black_status.png"; ;
                imgRegistrationCheckedStatus.AlternateText = "(Non vérifié)";
                txtRegistrationCheckedComment.Visible = true;
                lblRegistrationCheckedComment.Visible = false;
                break;
        }

        if (sCheckedComment.Trim().Length > 0)
        {
            panelRegistrationCheckedComment.Visible = true;
            lblRegistrationCheckedComment.Text = sCheckedComment.Trim();
        }
        else
            panelRegistrationCheckedComment.Visible = false;

        string sNoComplianceList = "";
        GetValueFromXml(sXml, "ALL_XML_OUT/Enrolment", "BackOfficeNoCompliance", out sNoComplianceList);
        hfNoComplianceSelectedList.Value = sNoComplianceList;
        rptGetNoComplianceList.DataSource = AML.getNoComplianceList();
        rptGetNoComplianceList.DataBind();

        registration registr = registration.GetCurrent(false, lblRegistrationSelected.Text);
        registr.bCheckParentId = false;
        registr.bCurrentStepIsAdult = true;
        registr.bMinorAccount = false;

        if (int.TryParse(sNbDoc, out iNbDoc))
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("index");
            dt.Columns.Add("label");
            dt.Columns.Add("xml");

            for (int i = 0; i < iNbDoc; i++)
            {
                List<string> DocType = CommonMethod.GetAttributeValues(docList[i].ToString(), "DOC", "DocumentCategory");
                if (DocType.Count > 0)
                {
                    switch (DocType[0].Trim().ToUpper())
                    {
                        case "ID":

                            List<string> listTarget = CommonMethod.GetAttributeValues(docList[i], "DOC", "DocumentTarget");

                            /*
                             * <DOC IndexDoc="2" DocumentCategory="ID" DocumentType="P" DocumentTypeLabel="Passeport" DocumentTarget="" FileName="ID_P_1_NOB2202994210980_20170607111830829_52ee2295-a9a3-49d8-989b-faee89385cff.pdf"><O N="MZ1" Label="MRZ ligne 1" V="P_GBRSINGH__HARMINDER_______________________" Modified="N" C="N" VisualC="N" /><O N="MZ2" Label="MRZ ligne 2" V="I113669341GBR8911110M2006189______________02" Modified="N" C="N" VisualC="N" /><O N="DTY" Label="Document Type" V="P" Modified="N" C="N" VisualC="N" /><O N="DTN" Label="Document Number" V="I11366934" Modified="N" C="Y" VisualC="Y" /><O N="EDT" Label="Date d'expiration" V="18/06/2020" Modified="N" C="N" VisualC="N" /><O N="CY2" Label="Code Pays ISO 2" V="GB" Modified="N" C="N" VisualC="N" /><O N="FNA" Label="Prénom" V="HARMINDER" Modified="N" C="N" VisualC="Y" /><O N="LNA" Label="Nom" V="SINGH" Modified="N" C="N" VisualC="Y" /><O N="NAT" Label="Nationalité" V="GB" Modified="N" C="N" VisualC="N" /><O N="ICT" Label="Pays d'emission" V="GB" Modified="N" C="N" VisualC="N" /><O N="IDT" Label="Date Emission" V="18/06/2010" Modified="N" C="N" VisualC="N" /><O N="BDA" Label="Date de Naissance" V="11/11/1989" Modified="N" C="N" VisualC="N" /><O N="SEX" Label="Sexe" V="M" Modified="N" C="N" VisualC="N" /><O N="NBP" Label="Nombre de pages" V="1" Modified="N" C="N" VisualC="N" /><O N="CHC" Label="Octet de control" V="0" Modified="N" C="N" VisualC="N" /><O N="BPL" Label="Ville de naissance" V="CHALANDRY" Modified="N" C="N" VisualC="N" /><O N="BDP" V="02" Modified="N" C="N" VisualC="N" /><O N="BI2" Label="Code ISO2 pays de naissance" V="FR" Modified="N" C="N" VisualC="N" /></DOC>
                             */
                            if (listTarget.Count > 0 && listTarget[0] == "ENFANT")
                            {
                                registr.bMinorAccount = true;
                                lblIDDocumentType.Text = CommonMethod.GetAttributeValues(docList[i].ToString(), "DOC", "DocumentTypeLabel")[0];
                                ddlIDDocumentType.SelectedValue = CommonMethod.GetAttributeValues(docList[i].ToString(), "DOC", "DocumentType")[0];
                                List<string> lsDocO = CommonMethod.GetTags(docList[i].ToString(), "DOC/O");
                                for (int j = 0; j < lsDocO.Count; j++)
                                {
                                    List<string> lsDocON = CommonMethod.GetAttributeValues(lsDocO[j].ToString(), "O", "Label");
                                    List<string> lsDocOV = CommonMethod.GetAttributeValues(lsDocO[j].ToString(), "O", "V");

                                    if (lsDocON[0] == "Pays d'emission")
                                    {
                                        ddlIDDocumentCountry.SelectedValue = lsDocOV[0];
                                    }
                                }
                                ddlIDDocumentCountry.DataBind();

                                FillIdDocument(docList[i]);
                            }
                            else if (listTarget.Count > 0 && listTarget[0] == "PARENT")
                            {
                                registr.bCheckParentId = true;
                                divIDParent.Visible = true;

                                ddlParentIDDocumentType.SelectedValue = CommonMethod.GetAttributeValues(docList[i].ToString(), "DOC", "DocumentType")[0];
                                lblIDParentDocumentType.Text = CommonMethod.GetAttributeValues(docList[i].ToString(), "DOC", "DocumentTypeLabel")[0] + " représentant légal";

                                List<string> lsDocO = CommonMethod.GetTags(docList[i].ToString(), "DOC/O");
                                for (int j = 0; j < lsDocO.Count; j++)
                                {
                                    List<string> lsDocON = CommonMethod.GetAttributeValues(lsDocO[j].ToString(), "O", "Label");
                                    List<string> lsDocOV = CommonMethod.GetAttributeValues(lsDocO[j].ToString(), "O", "V");

                                    if (lsDocON[0] == "Pays d'emission")
                                    {
                                        ddlParentIDDocumentCountry.SelectedValue = lsDocOV[0];
                                    }
                                }
                                ddlParentIDDocumentCountry.DataBind();

                                FillIdDocumentParent(docList[i]);
                            }
                            else
                            {
                                List<string> lsDocO = CommonMethod.GetTags(docList[i].ToString(), "DOC/O");
                                for (int j = 0; j < lsDocO.Count; j++)
                                {
                                    List<string> lsDocON = CommonMethod.GetAttributeValues(lsDocO[j].ToString(), "O", "Label");
                                    List<string> lsDocOV = CommonMethod.GetAttributeValues(lsDocO[j].ToString(), "O", "V");

                                    if (lsDocON[0] == "Pays d'emission")
                                    {
                                        ddlIDDocumentCountry.SelectedValue = lsDocOV[0];
                                    }
                                }
                                ddlIDDocumentCountry.DataBind();
                                lblIDDocumentType.Text = CommonMethod.GetAttributeValues(docList[i].ToString(), "DOC", "DocumentTypeLabel")[0];
                                ddlIDDocumentType.SelectedValue = CommonMethod.GetAttributeValues(docList[i].ToString(), "DOC", "DocumentType")[0];
                                FillIdDocument(docList[i]);

                                dt.Rows.Add(new object[] {
                                    CommonMethod.GetAttributeValues(docList[i].ToString(), "DOC", "IndexDoc")[0],
                                    string.Format("Document n°{0} - {1}",
                                    CommonMethod.GetAttributeValues(docList[i].ToString(), "DOC", "IndexDoc")[0],
                                    CommonMethod.GetAttributeValues(docList[i].ToString(), "DOC", "DocumentTypeLabel")[0]),
                                    docList[i]
                                });
                            }
                            break;
                        case "HO":
                            string sHoDocumentType = CommonMethod.GetAttributeValues(docList[i].ToString(), "DOC", "DocumentTypeLabel")[0];
                            lblHoDocumentType.Text = (sHoDocumentType.Trim().Length == 0) ? "Saisie manuelle (ADRESSE)" : sHoDocumentType;
                            divHoFactInfo.Visible = false;
                            FillHoDocument(docList[i]);
                            break;
                        case "FB":
                            divFB.Visible = true;
                            string sFileName = CommonMethod.GetAttributeValues(docList[i].ToString(), "DOC", "FileName")[0];
                            string sFamilyBookPath = ConfigurationManager.AppSettings["FamilyBookPath"].ToString();
                            if (sFileName.EndsWith(".pdf"))
                            {
                                if (File.Exists(System.Web.HttpContext.Current.Server.MapPath("~" + sFamilyBookPath + sFileName)))
                                {
                                    panelFBpdf.Visible = true;
                                    frameFBpdf.Attributes["src"] = sFamilyBookPath + sFileName;
                                }
                                else panelFBmissing.Visible = true;
                            }
                            else
                            {
                                string sFilePath = System.Web.HttpContext.Current.Server.MapPath("~" + sFamilyBookPath + sFileName);

                                if (File.Exists(sFilePath))
                                {
                                    panelFBjpg.Visible = true;

                                    if (sFileName.StartsWith("FB_01"))
                                        hfFB1Path.Value = sFamilyBookPath + sFileName;
                                    else hfFB2Path.Value = sFamilyBookPath + sFileName;
                                }
                                else panelFBmissing.Visible = true;
                            }
                            break;
                    }

                }
            }

            if (dt.Rows.Count > 1)
            {
                ddlDocs.Visible = true;
                ddlDocs.DataSource = ReverseRowsInDataTable(dt);
                ddlDocs.DataBind();
                dtDocs = dt;
            }
        }

        List<string> listParent = CommonMethod.GetTags(sXml, "ALL_XML_OUT/Enrolment/Parents/Parent");
        if (listParent.Count > 0)
        {
            divIDParent.Visible = true;
            panelDocInfos.Visible = false;
            //panelClientInfos.Visible = true;

            lblIDParentDocumentType.Text = "Représentant légal";

            List<string> listRefCustomer = CommonMethod.GetAttributeValues(listParent[0], "Parent", "RefCustomer");
            List<string> listParentLastName = CommonMethod.GetAttributeValues(listParent[0], "Parent", "LastName");
            List<string> listParentFirstName = CommonMethod.GetAttributeValues(listParent[0], "Parent", "FirstName");
            List<string> listParentGender = CommonMethod.GetAttributeValues(listParent[0], "Parent", "Sex");
            List<string> listParentBirthDate = CommonMethod.GetAttributeValues(listParent[0], "Parent", "BirthDate");

            if (listParentLastName.Count > 0)
                lblParentClientLastName.Text = listParentLastName[0];
            if (listParentFirstName.Count > 0)
                lblParentClientFirstName.Text = listParentFirstName[0];
            if (listParentGender.Count > 0)
                lblParentClientGender.Text = listParentGender[0];
            if (listParentBirthDate.Count > 0)
                lblParentClientBirthDate.Text = listParentBirthDate[0];
        }

        List<string> kycList = CommonMethod.GetTags(sXml, "ALL_XML_OUT/Enrolment/KYC");
        if (kycList.Count > 0)
        {
            FillKycDocument(kycList[0]);
        }

        List<string> infoSupList = CommonMethod.GetTags(sXml, "ALL_XML_OUT/Enrolment/ADDITIONALINFO");
        if (infoSupList.Count > 0)
        {
            FillInfoSupDocument(infoSupList[0]);
        }

        bool isCheckParentID = registr.bCheckParentId;
        registration.Save(registr, lblRegistrationSelected.Text);
    }

    protected DataTable ReverseRowsInDataTable(DataTable inputTable)
    {
        DataTable outputTable = inputTable.Clone();

        for (int i = inputTable.Rows.Count - 1; i >= 0; i--)
            outputTable.ImportRow(inputTable.Rows[i]);

        return outputTable;
    }

    protected void ddlDocs_SelectedIndexChanged(object sender, EventArgs e)
    {
        for (int i = 0; i < dtDocs.Rows.Count; i++)
        {
            if (dtDocs.Rows[i]["index"].ToString() == ddlDocs.SelectedValue)
            {
                FillIdDocument(dtDocs.Rows[i]["xml"].ToString());
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), new Guid().ToString(), "mapIdObjects();", true);
                upDocDetails.Update();
                break;
            }
        }
    }

    protected void GetValueFromXml(string sXml, string sNode, string sAttribute, out string sValue)
    {
        sValue = "";

        List<string> tagList = CommonMethod.GetAttributeValues(sXml.ToString(), sNode, sAttribute);

        if (tagList.Count > 0)
            sValue = tagList[0];
    }

    protected void VisualCheck(WebControl ctrl, string check, string visualCheck, string modified)
    {
        try
        {
            if (modified.Trim().ToUpper() == "Y")
                ctrl.CssClass = "ClientModification ";
            else if (visualCheck.Trim().ToUpper() == "Y" || check.Trim().ToUpper() == "Y")
                ctrl.CssClass = "OcrChecked ";

            if (visualCheck.Trim().ToUpper() == "Y")
                ctrl.CssClass += "VisualChecked ";
            else
                ctrl.CssClass += "VisualNotChecked ";

            if (check.Trim().ToUpper() == "Y")
                ctrl.CssClass += "MrzChecked ";
            else
                ctrl.CssClass += "MrzNotCheked ";
        }
        catch (Exception ex)
        {
        }

    }
    protected void AddClientSheetModificationVisualCheck(WebControl ctrl)
    {
        ctrl.CssClass = ctrl.CssClass.Replace("ClientModification ", "");
        ctrl.CssClass = ctrl.CssClass.Replace("OcrChecked ", "");
        ctrl.CssClass += "ClientSheetModification";
    }

    protected void FillIdDocument(string IdDocXml)
    {
        registration registr = registration.GetCurrent(true, lblRegistrationSelected.Text);
        registr.ClientRegistrationSheet = new registration.registrationsheet();
        registr.ClientRegistrationSheet.BirthDep = "";

        WebControl ctrl = null;
        List<WebControl> ctrlToDisableList = new List<WebControl>();

        List<string> tagList = CommonMethod.GetTags(IdDocXml, "DOC/O");

        List<string> tagFilename = CommonMethod.GetAttributeValues(IdDocXml, "DOC", "FileName");
        List<string> tagDocType = CommonMethod.GetAttributeValues(IdDocXml, "DOC", "DocumentType");
        List<string> tagDocCategory = CommonMethod.GetAttributeValues(IdDocXml, "DOC", "DocumentCategory");
        List<string> tagNbPage = CommonMethod.GetAttributeValues(IdDocXml, "DOC", "");

        DataTable dt = GetCountryList();

        int iNbPages = 1;

        bool isMaritalLastName = false;

        foreach (string tag in tagList)
        {
            List<string> infoList = CommonMethod.GetAttributeValues(tag, "O", "N");
            if (infoList.Count == 0)
                infoList = CommonMethod.GetAttributeValues(tag, "INFO", "N");
            List<string> valueList = CommonMethod.GetAttributeValues(tag, "O", "V");
            if (valueList.Count == 0)
                valueList = CommonMethod.GetAttributeValues(tag, "INFO", "V");
            List<string> modifiedList = CommonMethod.GetAttributeValues(tag, "O", "Modified");
            if (modifiedList.Count == 0)
                modifiedList = CommonMethod.GetAttributeValues(tag, "INFO", "Modified");
            List<string> checksumList = CommonMethod.GetAttributeValues(tag, "O", "C");
            if (checksumList.Count == 0)
                checksumList = CommonMethod.GetAttributeValues(tag, "INFO", "C ");
            List<string> visualcheckList = CommonMethod.GetAttributeValues(tag, "O", "VisualC");
            if (visualcheckList.Count == 0)
                visualcheckList = CommonMethod.GetAttributeValues(tag, "INFO", "VisualC");

            if (infoList.Count > 0 && infoList.Count == valueList.Count)
            {
                ctrl = null;

                switch (infoList[0])
                {
                    case "MZ1":
                        lblMRZ1.Text = valueList[0].Replace("_", "&lt;");
                        break;
                    case "MZ2":
                        lblMRZ2.Text = valueList[0].Replace("_", "&lt;");
                        break;
                    case "MZ3":
                        lblMRZ3.Text = valueList[0].Replace("_", "&lt;");
                        break;
                    case "MLN":
                        //panelMarriedLastName.Visible = true;
                        txtIDMarriedLastName.Text = valueList[0];
                        txtIDMarriedLastName.Style.Add("display", "none");
                        lblIDMarriedLastName.Text = valueList[0];
                        lblIDMarriedLastName.ToolTip = valueList[0];
                        ctrl = lblIDMarriedLastName;
                        isMaritalLastName = true;
                        registr.ClientRegistrationSheet.MaritalName = valueList[0];
                        break;
                    case "LNA":
                        txtIDLastName.Style.Add("display", "none");
                        txtIDLastName.Text = valueList[0];
                        lblIDLastName.Text = valueList[0];
                        lblIDLastName.ToolTip = valueList[0];
                        ctrl = lblIDLastName;
                        registr.ClientRegistrationSheet.LastName = valueList[0];
                        break;
                    case "FNA":
                        txtIDFirstName.Text = valueList[0];
                        lblIDFirstName.Text = valueList[0];
                        lblIDFirstName.ToolTip = valueList[0];
                        txtIDFirstName.Style.Add("display", "none");
                        ctrl = lblIDFirstName;
                        registr.ClientRegistrationSheet.FirstName = valueList[0];
                        break;
                    case "SEX":
                        rblGender.SelectedValue = valueList[0];
                        lblGender.Text = valueList[0];
                        rblGender.Style.Add("display", "none");
                        ctrl = lblGender;
                        registr.ClientRegistrationSheet.Gender = valueList[0];
                        break;
                    case "BDA":
                        txtBirthDate.Text = valueList[0];
                        lblBirthDate.Text = valueList[0];
                        txtBirthDate.Style.Add("display", "none");
                        ctrl = lblBirthDate;
                        registr.ClientRegistrationSheet.BirthDate = valueList[0];
                        break;
                    case "BPL":
                        txtBirthCity.Text = valueList[0];
                        lblBirthCity.Text = valueList[0];
                        lblBirthCity.ToolTip = valueList[0];
                        txtBirthCity.Style.Add("display", "none");
                        ctrl = lblBirthCity;
                        registr.ClientRegistrationSheet.BirthCity = valueList[0];
                        break;
                    case "BI2":
                        ddlBirthCountry.DataSource = dt;
                        ddlBirthCountry.DataBind();
                        if (ddlBirthCountry.Items.FindByValue(valueList[0]) != null)
                        {
                            ddlBirthCountry.SelectedValue = valueList[0];
                            lblBirthCountry.Text = ddlBirthCountry.SelectedItem.Text;
                            if (lblBirthCountry.Text.Trim().Length == 0)
                                lblBirthCountry.Text = "<span style=\"font-style:italic; text-transform:lowercase\">Non renseigné</span>";
                            lblBirthCountry.ToolTip = lblBirthCountry.Text;
                            ddlBirthCountry.Style.Add("display", "none");
                            registr.ClientRegistrationSheet.BirthCountry = valueList[0];
                        }
                        ctrl = lblBirthCountry;
                        
                        break;
                    case "DTN":
                        txtDocumentNumber.Text = valueList[0];
                        lblDocumentNumber.Text = valueList[0];
                        lblDocumentNumber.ToolTip = valueList[0];
                        txtDocumentNumber.Style.Add("display", "none");
                        ctrl = lblDocumentNumber;
                        registr.ClientRegistrationSheet.DocumentNumber = valueList[0];
                        break;
                    case "EDT":
                        txtExpirationDate.Text = valueList[0];
                        lblExpirationDate.Text = valueList[0];
                        txtExpirationDate.Style.Add("display", "none");
                        ctrl = lblExpirationDate;
                        registr.ClientRegistrationSheet.DocumentExpirationDate = valueList[0];
                        break;
                    case "IDT":
                        txtIssuedID.Text = valueList[0];
                        lblIssuedID.Text = valueList[0];
                        txtIssuedID.Style.Add("display", "none");
                        ctrl = lblIssuedID;
                        break;
                    case "ICT":
                        ddlDocumentCountry.DataSource = dt;
                        ddlDocumentCountry.DataBind();
                        if (ddlDocumentCountry.Items.FindByValue(valueList[0]) != null)
                        {
                            ddlDocumentCountry.SelectedValue = valueList[0];
                            lblDocumentCountry.Text = ddlDocumentCountry.SelectedItem.Text;
                            lblDocumentCountry.ToolTip = lblDocumentCountry.Text;
                            ddlDocumentCountry.Style.Add("display", "none");
                            registr.ClientRegistrationSheet.DocumentCountry = valueList[0];
                        }
                        //ddlDocumentCountry.CssClass = "OcrChecked";
                        //ctrl = ddlDocumentCountry;
                        ctrl = lblDocumentCountry;
                        break;
                    case "IST":
                        //if (ddlDocumentState.Items.FindByValue(valueList[0]) != null)
                        //ddlDocumentState.SelectedValue = valueList[0];
                        //ctrl = ddlDocumentState;
                        break;
                    case "HLD":
                        //txtHolder.Text = valueList[0];
                        //ctrl = txtHolder;
                        break;

                    case "NBP":
                        if (!int.TryParse(valueList[0], out iNbPages))
                            iNbPages = 1;
                        break;

                        //RIB - NON UTILISE
                        #region
                        //case "BIC":
                        //txtBIC.Text = valueList[0];
                        //if (checkList[0] == "Y")
                        //{
                        //    ctrlToDisableList.Add(txtBIC);
                        //}
                        //string sBankName = "";
                        //string sCountry = "";
                        //string sPlace = "";

                        //int iRC = GetBICInfo(valueList[0], out sBankName, out sCountry, out sPlace);

                        //    if (iRC == 0)
                        //    {
                        //        if (sBankName.Length > 0)
                        //            lblBankName.Text = sBankName;
                        //        else panelBankName.Visible = false;

                        //        if (sCountry.Length > 0)
                        //            lblBankCountry.Text = sCountry;
                        //        else panelBankCountry.Visible = false;

                        //        if (sPlace.Length > 0)
                        //            lblBankPlace.Text = sPlace;
                        //        else panelBankPlace.Visible = false;
                        //    }
                        //    else
                        //    {
                        //        panelBICInfo.Visible = false;
                        //        panelBIC.CssClass = "";
                        //        panelBIC.Attributes.Remove("style");
                        //        lblCheckBIC.Text = "N";
                        //    }

                        //    ctrl = txtBIC;
                        //    break;
                        //case "IBA":
                        //    txtIBAN.Text = valueList[0];
                        //    //if (checkList[0] == "Y")
                        //    //{
                        //    //    ctrlToDisableList.Add(txtIBAN);
                        //    //}
                        //    ctrl = txtIBAN;
                        //    break;
                        //
                        #endregion
                }

                VisualCheck(ctrl, checksumList[0], visualcheckList[0], modifiedList[0]);
            }
        }

        if (!isMaritalLastName)
        {
            txtIDMarriedLastName.Style.Add("display", "none");
            VisualCheck(lblIDMarriedLastName, "", "", "");
        }
        txtIDUsageLastName.Style.Add("display", "none");
        VisualCheck(lblIDUsageLastName, "", "", "");

        string RectoFilePath = "", VersoFilePath = "", OriginalFilePath = "";
        bool isOriginalFilePresent = false, isCroppedFile1Present = false, isCroppedFile2Present = false;
        if (tagFilename.Count > 0 && tagDocType.Count > 0)
        {
            btnShowOriginalImage.Visible = true;
            btnGetArchivedImage.Visible = false;
            getFilePath(tagFilename[0], tagDocCategory[0], iNbPages, out RectoFilePath, out VersoFilePath,
                out OriginalFilePath, out isOriginalFilePresent, out isCroppedFile1Present, out isCroppedFile2Present);

            if (!isOriginalFilePresent)
                btnShowOriginalImage.Visible = false;
            if (!isCroppedFile1Present)
                btnGetArchivedImage.Visible = true;
        }

        if (iNbPages == 1)
            panelIdVerso.Visible = false;
        else
            panelIdVerso.Visible = true;

        hfIdRectoPath.Value = RectoFilePath;
        hfIdVersoPath.Value = VersoFilePath;
        btnShowOriginalImage.OnClientClick = "clickShowOriginalImage('" + "." + OriginalFilePath + "','Image originale', false); return false;";
    }
    protected void FillIdDocumentParent(string IdDocXml)
    {
        registration registr = registration.GetCurrent(true, lblRegistrationSelected.Text);
        registr.ParentRegistrationSheet = new registration.registrationsheet();
        registr.ParentRegistrationSheet.BirthDep = "";

        WebControl ctrl = null;
        List<WebControl> ctrlToDisableList = new List<WebControl>();

        List<string> tagList = CommonMethod.GetTags(IdDocXml, "DOC/O");

        List<string> tagFilename = CommonMethod.GetAttributeValues(IdDocXml, "DOC", "FileName");
        List<string> tagDocType = CommonMethod.GetAttributeValues(IdDocXml, "DOC", "DocumentType");
        List<string> tagDocCategory = CommonMethod.GetAttributeValues(IdDocXml, "DOC", "DocumentCategory");
        List<string> tagNbPage = CommonMethod.GetAttributeValues(IdDocXml, "DOC", "");

        DataTable dt = GetCountryList();

        int iNbPages = 1;

        bool isMaritalLastName = false;

        foreach (string tag in tagList)
        {
            List<string> infoList = CommonMethod.GetAttributeValues(tag, "O", "N");
            if (infoList.Count == 0)
                infoList = CommonMethod.GetAttributeValues(tag, "INFO", "N");
            List<string> valueList = CommonMethod.GetAttributeValues(tag, "O", "V");
            if (valueList.Count == 0)
                valueList = CommonMethod.GetAttributeValues(tag, "INFO", "V");
            List<string> modifiedList = CommonMethod.GetAttributeValues(tag, "O", "Modified");
            if (modifiedList.Count == 0)
                modifiedList = CommonMethod.GetAttributeValues(tag, "INFO", "Modified");
            List<string> checksumList = CommonMethod.GetAttributeValues(tag, "O", "C");
            if (checksumList.Count == 0)
                checksumList = CommonMethod.GetAttributeValues(tag, "INFO", "C ");
            List<string> visualcheckList = CommonMethod.GetAttributeValues(tag, "O", "VisualC");
            if (visualcheckList.Count == 0)
                visualcheckList = CommonMethod.GetAttributeValues(tag, "INFO", "VisualC");

            if (infoList.Count > 0 && infoList.Count == valueList.Count)
            {
                ctrl = null;

                switch (infoList[0])
                {
                    case "MZ1":
                        lblMRZ1Parent.Text = valueList[0].Replace("_", "&lt;");
                        lblParentMRZ1.Text = valueList[0].Replace("_", "&lt;");
                        break;
                    case "MZ2":
                        lblMRZ2Parent.Text = valueList[0].Replace("_", "&lt;");
                        lblParentMRZ2.Text = valueList[0].Replace("_", "&lt;");
                        break;
                    case "MZ3":
                        lblMRZ3Parent.Text = valueList[0].Replace("_", "&lt;");
                        lblParentMRZ3.Text = valueList[0].Replace("_", "&lt;");
                        break;
                    case "MLN":
                        panelParentMarriedLastName.Visible = true;
                        txtParentIDMarriedLastName.Text = valueList[0];
                        txtParentIDMarriedLastName.Style.Remove("display");
                        txtParentIDMarriedLastName.Style.Add("display", "none");
                        lblParentIDMarriedLastName.Text = valueList[0];
                        lblParentIDMarriedLastName.ToolTip = valueList[0];
                        ctrl = lblParentIDMarriedLastName;
                        isMaritalLastName = true;
                        registr.ParentRegistrationSheet.MaritalName = valueList[0];
                        break;
                    case "LNA":
                        txtParentIDLastName.Style.Add("display", "none");
                        txtParentIDLastName.Text = valueList[0];
                        lblParentIDLastName.Text = valueList[0];
                        lblParentIDLastName.ToolTip = valueList[0];
                        ctrl = lblParentIDLastName;
                        registr.ParentRegistrationSheet.LastName = valueList[0];
                        break;
                    case "FNA":
                        txtParentIDFirstName.Text = valueList[0];
                        lblParentIDFirstName.Text = valueList[0];
                        lblParentIDFirstName.ToolTip = valueList[0];
                        txtParentIDFirstName.Style.Add("display", "none");
                        ctrl = lblParentIDFirstName;
                        registr.ParentRegistrationSheet.FirstName = valueList[0];
                        break;
                    case "SEX":
                        rblParentGender.SelectedValue = valueList[0];
                        lblParentGender.Text = valueList[0];
                        rblParentGender.Style.Add("display", "none");
                        ctrl = lblParentGender;
                        registr.ParentRegistrationSheet.Gender = valueList[0];
                        break;
                    case "BDA":
                        txtParentBirthDate.Text = valueList[0];
                        lblParentBirthDate.Text = valueList[0];
                        txtParentBirthDate.Style.Add("display", "none");
                        ctrl = lblParentBirthDate;
                        registr.ParentRegistrationSheet.BirthDate = valueList[0];
                        break;
                    case "BPL":
                        //txtBirthCityParent.Text = valueList[0];
                        //lblBirthCityParent.Text = valueList[0];
                        //lblBirthCityParent.ToolTip = valueList[0];
                        //txtBirthCityParent.Style.Add("display", "none");
                        //ctrl = lblBirthCityParent;
                        txtParentBirthCity.Text = valueList[0];
                        lblParentBirthCity.Text = valueList[0];
                        lblParentBirthCity.ToolTip = valueList[0];
                        txtParentBirthCity.Style.Add("display", "none");
                        ctrl = lblParentBirthCity;
                        registr.ParentRegistrationSheet.BirthCity = valueList[0];
                        break;
                    case "BI2":
                        ddlParentBirthCountry.DataSource = dt;
                        ddlParentBirthCountry.DataBind();
                        if (ddlParentBirthCountry.Items.FindByValue(valueList[0]) != null)
                        {
                            ddlParentBirthCountry.SelectedValue = valueList[0];
                            lblParentBirthCountry.Text = ddlParentBirthCountry.SelectedItem.Text;
                            if (lblParentBirthCountry.Text.Trim().Length == 0)
                                lblParentBirthCountry.Text = "<span style=\"font-style:italic; text-transform:lowercase\">Non renseigné</span>";
                            lblParentBirthCountry.ToolTip = lblParentBirthCountry.Text;
                            ddlParentBirthCountry.Style.Add("display", "none");
                            registr.ParentRegistrationSheet.BirthCountry = valueList[0];
                        }
                        ctrl = lblParentBirthCountry;
                        break;
                    case "DTN":
                        //txtDocumentNumberParent.Text = valueList[0];
                        //lblDocumentNumberParent.Text = valueList[0];
                        //lblDocumentNumberParent.ToolTip = valueList[0];
                        //txtDocumentNumberParent.Style.Add("display", "none");
                        //ctrl = lblDocumentNumberParent;

                        txtParentDocumentNumber.Text = valueList[0];
                        lblParentDocumentNumber.Text = valueList[0];
                        lblParentDocumentNumber.ToolTip = valueList[0];
                        txtParentDocumentNumber.Style.Add("display", "none");
                        ctrl = lblParentDocumentNumber;
                        registr.ParentRegistrationSheet.DocumentNumber = valueList[0];

                        break;
                    case "EDT":
                        //txtExpirationDateParent.Text = valueList[0];
                        //lblExpirationDateParent.Text = valueList[0];
                        //txtExpirationDateParent.Style.Add("display", "none");
                        //ctrl = lblExpirationDateParent;
                        txtParentExpirationDate.Text = valueList[0];
                        lblParentExpirationDate.Text = valueList[0];
                        txtParentExpirationDate.Style.Add("display", "none");
                        ctrl = lblParentExpirationDate;
                        registr.ParentRegistrationSheet.DocumentExpirationDate = valueList[0];
                        break;
                    case "IDT":
                        txtIssuedIDParent.Text = valueList[0];
                        lblIssuedIDParent.Text = valueList[0];
                        txtIssuedIDParent.Style.Add("display", "none");
                        ctrl = lblIssuedIDParent;
                        break;
                    case "ICT":
                        ddlDocumentCountryParent.DataSource = dt;
                        ddlDocumentCountryParent.DataBind();
                        if (ddlDocumentCountryParent.Items.FindByValue(valueList[0]) != null)
                        {
                            ddlDocumentCountryParent.SelectedValue = valueList[0];
                            lblDocumentCountryParent.Text = ddlDocumentCountryParent.SelectedItem.Text;
                            lblDocumentCountryParent.ToolTip = lblDocumentCountryParent.Text;
                            ddlDocumentCountryParent.Style.Add("display", "none");
                            registr.ParentRegistrationSheet.DocumentCountry = valueList[0];
                        }
                        //ddlDocumentCountry.CssClass = "OcrChecked";
                        //ctrl = ddlDocumentCountry;
                        ctrl = lblDocumentCountryParent;
                        break;
                    case "IST":
                        //if (ddlDocumentState.Items.FindByValue(valueList[0]) != null)
                        //ddlDocumentState.SelectedValue = valueList[0];
                        //ctrl = ddlDocumentState;
                        break;
                    case "HLD":
                        //txtHolder.Text = valueList[0];
                        //ctrl = txtHolder;
                        break;

                    case "NBP":
                        if (!int.TryParse(valueList[0], out iNbPages))
                            iNbPages = 1;
                        break;

                        //RIB - NON UTILISE
                        #region
                        //case "BIC":
                        //txtBIC.Text = valueList[0];
                        //if (checkList[0] == "Y")
                        //{
                        //    ctrlToDisableList.Add(txtBIC);
                        //}
                        //string sBankName = "";
                        //string sCountry = "";
                        //string sPlace = "";

                        //int iRC = GetBICInfo(valueList[0], out sBankName, out sCountry, out sPlace);

                        //    if (iRC == 0)
                        //    {
                        //        if (sBankName.Length > 0)
                        //            lblBankName.Text = sBankName;
                        //        else panelBankName.Visible = false;

                        //        if (sCountry.Length > 0)
                        //            lblBankCountry.Text = sCountry;
                        //        else panelBankCountry.Visible = false;

                        //        if (sPlace.Length > 0)
                        //            lblBankPlace.Text = sPlace;
                        //        else panelBankPlace.Visible = false;
                        //    }
                        //    else
                        //    {
                        //        panelBICInfo.Visible = false;
                        //        panelBIC.CssClass = "";
                        //        panelBIC.Attributes.Remove("style");
                        //        lblCheckBIC.Text = "N";
                        //    }

                        //    ctrl = txtBIC;
                        //    break;
                        //case "IBA":
                        //    txtIBAN.Text = valueList[0];
                        //    //if (checkList[0] == "Y")
                        //    //{
                        //    //    ctrlToDisableList.Add(txtIBAN);
                        //    //}
                        //    ctrl = txtIBAN;
                        //    break;
                        //
                        #endregion


                }

                VisualCheck(ctrl, checksumList[0], visualcheckList[0], modifiedList[0]);
            }
        }

        if (!isMaritalLastName)
        {
            txtIDMarriedLastNameParent.Style.Add("display", "none");
            VisualCheck(lblIDMarriedLastNameParent, "", "", "");
        }
        //VisualCheck(lblIDUsageLastNameParent, "", "", "");

        string RectoFilePath = "", VersoFilePath = "", OriginalFilePath = "";
        bool isOriginalFilePresent = false, isCroppedFile1Present = false, isCroppedFile2Present = false;
        if (tagFilename.Count > 0 && tagDocType.Count > 0)
        {
            btnShowOriginalImageParent.Visible = true;
            btnGetArchivedImage.Visible = false;
            getFilePath(tagFilename[0], tagDocCategory[0], iNbPages, out RectoFilePath, out VersoFilePath,
                out OriginalFilePath, out isOriginalFilePresent, out isCroppedFile1Present, out isCroppedFile2Present);

            if (!isOriginalFilePresent)
                btnShowOriginalImageParent.Visible = false;
            if (!isCroppedFile1Present)
                btnGetArchivedImageParent.Visible = true;
        }

        if (iNbPages == 1)
            panelIdParentVerso.Visible = false;
        else
            panelIdParentVerso.Visible = true;

        hfIdParentRectoPath.Value = RectoFilePath;
        hfIdParentVersoPath.Value = VersoFilePath;
        btnShowOriginalImageParent.OnClientClick = "clickShowOriginalImage('" + "." + OriginalFilePath + "','Image originale', false); return false;";
    }
    protected void FillHoDocument(string HoDocXml)
    {
        WebControl ctrl = null;
        List<WebControl> ctrlToDisableList = new List<WebControl>();

        List<string> tagList = CommonMethod.GetTags(HoDocXml, "DOC/O");

        List<string> Filename = CommonMethod.GetAttributeValues(HoDocXml, "DOC", "FileName");

        List<string> tagFilename = CommonMethod.GetAttributeValues(HoDocXml, "DOC", "FileName");
        List<string> tagDocType = CommonMethod.GetAttributeValues(HoDocXml, "DOC", "DocumentType");
        List<string> tagDocCategory = CommonMethod.GetAttributeValues(HoDocXml, "DOC", "DocumentCategory");
        List<string> tagNbPage = CommonMethod.GetAttributeValues(HoDocXml, "DOC", "");

        int iNbPages = 1;

        foreach (string tag in tagList)
        {
            List<string> infoList = CommonMethod.GetAttributeValues(tag, "O", "N");
            if (infoList.Count == 0)
                infoList = CommonMethod.GetAttributeValues(tag, "INFO", "N");
            List<string> valueList = CommonMethod.GetAttributeValues(tag, "O", "V");
            if (valueList.Count == 0)
                valueList = CommonMethod.GetAttributeValues(tag, "INFO", "V");
            List<string> modifiedList = CommonMethod.GetAttributeValues(tag, "O", "Modified");
            if (modifiedList.Count == 0)
                modifiedList = CommonMethod.GetAttributeValues(tag, "INFO", "Modified");
            List<string> checksumList = CommonMethod.GetAttributeValues(tag, "O", "C");
            if (checksumList.Count == 0)
                checksumList = CommonMethod.GetAttributeValues(tag, "INFO", "C ");
            List<string> visualcheckList = CommonMethod.GetAttributeValues(tag, "O", "VisualC");
            if (visualcheckList.Count == 0)
                visualcheckList = CommonMethod.GetAttributeValues(tag, "INFO", "VisualC");

            if (infoList.Count > 0 && infoList.Count == valueList.Count)
            {
                ctrl = null;

                switch (infoList[0])
                {
                    case "SDR":
                        txtBiller.Text = valueList[0];
                        lblBiller.Text = valueList[0];
                        lblBiller.ToolTip = valueList[0];
                        txtBiller.Style.Add("display", "none");
                        ctrl = lblBiller;
                        break;
                    case "TO":
                        txtTo.Text = valueList[0];
                        txtTo.Style.Add("display", "none");
                        lblTo.Text = valueList[0];
                        lblTo.ToolTip = valueList[0];
                        ctrl = lblTo;
                        break;
                    case "IDT":
                        txtIssuedFact.Text = valueList[0];
                        lblIssuedFact.Text = valueList[0];
                        lblIssuedFact.ToolTip = valueList[0];
                        txtIssuedFact.Style.Add("display", "none");
                        ctrl = lblIssuedFact;
                        break;
                    case "AD1":
                        txtAddress1.Text = valueList[0];
                        lblAddress1.Text = valueList[0];
                        lblAddress1.ToolTip = valueList[0];
                        txtAddress1.Style.Add("display", "none");
                        ctrl = lblAddress1;
                        break;
                    case "AD2":
                        txtAddress2.Text = valueList[0];
                        lblAddress2.Text = valueList[0];
                        lblAddress2.ToolTip = valueList[0];
                        txtAddress2.Style.Add("display", "none");
                        ctrl = lblAddress2;
                        break;
                    case "ZIP":
                        txtZipCode.Text = valueList[0];
                        lblZipCode.Text = valueList[0];
                        txtZipCode.Style.Add("display", "none");
                        ctrl = lblZipCode;
                        break;
                    case "CIT":
                        txtCity.Text = valueList[0];
                        lblCity.Text = valueList[0];
                        lblCity.ToolTip = valueList[0];
                        txtCity.Style.Add("display", "none");
                        ctrl = lblCity;
                        break;
                    case "RTS":
                        //if (ddlResidentStatus.Items.FindByValue(valueList[0]) != null)
                        //ddlResidentStatus.SelectedValue = valueList[0];
                        break;
                    case "NBP":
                        if (!int.TryParse(valueList[0], out iNbPages))
                            iNbPages = 1;
                        break;
                }

                VisualCheck(ctrl, checksumList[0], visualcheckList[0], modifiedList[0]);
            }
        }

        string RectoFilePath = "", VersoFilePath = "", OriginalFilePath = "";
        bool isOriginalFilePresent = false, isCroppedFile1Present = false, isCroppedFile2Present = false;
        if (tagFilename.Count > 0 && tagDocType.Count > 0)
        {
            btnShowOriginalImage.Visible = true;
            btnGetArchivedImage.Visible = false;
            getFilePath(tagFilename[0], tagDocCategory[0], iNbPages, out RectoFilePath, out VersoFilePath,
                out OriginalFilePath, out isOriginalFilePresent, out isCroppedFile1Present, out isCroppedFile2Present);
        }

        hfHoPath.Value = RectoFilePath;
    }
    protected void FillKycDocument(string KycDocXml)
    {
        int iNbColumnToFill = 3;
        int iColumnToFill = 1;

        List<string> listKycAnswerLabel = CommonMethod.GetAttributeValues(KycDocXml, "KYC/I", "AnswerLabel");
        List<string> listKycAnswerValue = CommonMethod.GetAttributeValues(KycDocXml, "KYC/I", "AnswerValue");
        List<string> listKycQuestions = CommonMethod.GetAttributeValues(KycDocXml, "KYC/I", "QuestionLabel");
        List<string> listKycRefQuestions = CommonMethod.GetAttributeValues(KycDocXml, "KYC/I", "RefQuestion");

        string sRefQuestion = "";
        string sLabel = "";
        string sAnswer = "";

        TextBox txtTextbox = null;
        Label lblLabel = null;

        for (int i = 0; i < listKycQuestions.Count; i++)
        {
            sRefQuestion = listKycRefQuestions[i];
            sLabel = listKycQuestions[i];

            if (listKycAnswerLabel[i] != null && listKycAnswerLabel[i] != "")
                sAnswer = listKycAnswerLabel[i];
            else
                sAnswer = listKycAnswerValue[i];

            txtTextbox = new TextBox();
            string sControlID = "txtQuestion" + sRefQuestion;
            txtTextbox.ID = sControlID;
            txtTextbox.ReadOnly = true;
            txtTextbox.Text = sAnswer;

            lblLabel = new Label();
            lblLabel.ID = "lblQuestion" + sRefQuestion;
            lblLabel.AssociatedControlID = sControlID;
            lblLabel.Text = sLabel;
            lblLabel.CssClass = "label";

            Panel panelQuestion = new Panel();
            Panel panelLabel = new Panel();

            panelLabel.Controls.Add(lblLabel);
            panelQuestion.Controls.Add(txtTextbox);

            switch (iColumnToFill)
            {
                case 1:
                    iColumnToFill = updateColumnToFill(iColumnToFill, iNbColumnToFill);
                    panelKYCHolderLeft.Controls.Add(panelLabel);
                    panelKYCHolderLeft.Controls.Add(panelQuestion);

                    break;
                case 2:
                    iColumnToFill = updateColumnToFill(iColumnToFill, iNbColumnToFill);
                    panelKYCHolderMiddle.Controls.Add(panelLabel);
                    panelKYCHolderMiddle.Controls.Add(panelQuestion);
                    break;
                case 3:
                    iColumnToFill = updateColumnToFill(iColumnToFill, iNbColumnToFill);
                    panelKYCHolderRight.Controls.Add(panelLabel);
                    panelKYCHolderRight.Controls.Add(panelQuestion);
                    break;
                default:
                    panelKYCHolderLeft.Controls.Add(panelLabel);
                    panelKYCHolderLeft.Controls.Add(panelQuestion);
                    iColumnToFill = 1;
                    break;
            }
        }
    }
    protected void FillInfoSupDocument(string InfoSupXml)
    {
        List<string> tagList = CommonMethod.GetTags(InfoSupXml, "ADDITIONALINFO/I");

        foreach (string tag in tagList)
        {
            List<string> infoList = CommonMethod.GetAttributeValues(tag, "I", "N");
            List<string> valueList = CommonMethod.GetAttributeValues(tag, "I", "V");

            if (infoList.Count > 0 && infoList.Count == valueList.Count)
            {
                switch (infoList[0])
                {
                    case "AMOUNT":
                        txtDepositAmount.Text = valueList[0] + " EUR";
                        break;
                    case "PACK_SALE":
                        if (valueList[0].Trim().ToUpper() == "Y")
                            txtPackSaleWithActivation.Text = "OUI";
                        else
                            txtPackSaleWithActivation.Text = "NON";
                        break;
                    case "CARD":
                        txtCardNumber.Text = valueList[0];
                        lblPackNumberSelected.Text = valueList[0];
                        break;
                }
            }
        }
    }

    protected int updateColumnToFill(int iColumnToFill, int iNbColumnToFill)
    {
        if (iColumnToFill < iNbColumnToFill)
            iColumnToFill++;
        else iColumnToFill = 1;

        return iColumnToFill;
    }

    protected DataTable GetCountryList()
    {
        string sXML =  new XElement("CountryRequest",
                            new XAttribute("Order", "DESC"),
                            new XAttribute("ByNbWords", "1"),
                            new XAttribute("Culture", (Session["Culture"] != null) ? Session["Culture"].ToString() : "fr-FR")).ToString();

        return GetCountryList(sXML);
    }
    protected DataTable GetCountryList(string order)
    {
        return GetCountryList(new XElement("CountryRequest",
            new XAttribute("Order", order),
            new XAttribute("ByNbWords", "1"),
            new XAttribute("Culture", (Session["Culture"] != null) ? Session["Culture"].ToString() : "fr-FR")));
    }

    protected DataTable GetCountryList(XElement xXmlIN)
    {
        SqlConnection conn = null;
        DataTable dt = new DataTable();

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["UniversalID"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("Other.P_GetCountryList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@CountryRequest", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@CountryList", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@ReturnSelect", SqlDbType.Bit);

            cmd.Parameters["@CountryRequest"].Value = xXmlIN.ToString();
            cmd.Parameters["@ReturnSelect"].Value = 1;
            cmd.Parameters["@CountryList"].Direction = ParameterDirection.Output;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;

            da.Fill(dt);
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }
    protected void getFilePath(string OriginalFileName, string DocType, int NbPages, out string FilePath1, out string FilePath2, out string OriginalFilePath,
        out bool isOriginalFilePresent, out bool isCroppedFile1Present, out bool isCroppedFile2Present)
    {
        FilePath1 = "";
        FilePath2 = "";
        OriginalFilePath = "";
        isOriginalFilePresent = false;
        isCroppedFile1Present = false;
        isCroppedFile2Present = false;

        OcrFiles _ocrFiles = new OcrFiles();
        _ocrFiles.ABBYYOriginalPath = ConfigurationManager.AppSettings["ABBYYOriginalPath"].ToString();
        _ocrFiles.ABBYYCroppedPath = ConfigurationManager.AppSettings["ABBYYCroppedPath"].ToString();
        _ocrFiles.ICARCroppedPath = ConfigurationManager.AppSettings["ICARCroppedPath"].ToString();
        _ocrFiles.ABBYYFilePath = ConfigurationManager.AppSettings["ABBYYFilePath"].ToString();
        _ocrFiles.ErrorFilePath = ConfigurationManager.AppSettings["ErrorPath"].ToString();
        _ocrFiles.ICAROriginalPath = ConfigurationManager.AppSettings["ICAROriginalPath"].ToString();
        _ocrFiles.OnfidoFilePath = ConfigurationManager.AppSettings["OnfidoFilePath"].ToString();
        _ocrFiles.NbPages = NbPages;
        _ocrFiles.DocType = DocType;
        _ocrFiles.OriginalFileName = OriginalFileName;
        _ocrFiles.getCroppedFilePath(out FilePath1, out FilePath2, out isCroppedFile1Present, out isCroppedFile2Present);

        string FileNameWithoutExtension = OriginalFileName.Split('.')[0];

        OriginalFilePath = _ocrFiles.ICAROriginalPath + FileNameWithoutExtension + ".jpg";
        if (File.Exists(System.Web.HttpContext.Current.Server.MapPath("~" + _ocrFiles.ICAROriginalPath + FileNameWithoutExtension + ".jpg")))
            isOriginalFilePresent = true;

        if (NbPages > 1)
        {
            if (File.Exists(System.Web.HttpContext.Current.Server.MapPath("~" + _ocrFiles.ABBYYOriginalPath + FileNameWithoutExtension + ".pdf")))
            {
                isOriginalFilePresent = true;
                OriginalFilePath = _ocrFiles.ABBYYOriginalPath + OriginalFileName;
            }
            else if (File.Exists(System.Web.HttpContext.Current.Server.MapPath("~" + _ocrFiles.ErrorFilePath + FileNameWithoutExtension + ".pdf")))
            {
                isOriginalFilePresent = true;
                OriginalFilePath = _ocrFiles.ErrorFilePath + OriginalFileName;
            }
        }
        else
        {
            if (File.Exists(System.Web.HttpContext.Current.Server.MapPath("~" + _ocrFiles.ABBYYOriginalPath + FileNameWithoutExtension + ".jpg")))
            {
                isOriginalFilePresent = true;
                OriginalFilePath = _ocrFiles.ABBYYOriginalPath + OriginalFileName;
            }
            else if (File.Exists(System.Web.HttpContext.Current.Server.MapPath("~" + _ocrFiles.ErrorFilePath + FileNameWithoutExtension + ".jpg")))
            {
                isOriginalFilePresent = true;
                OriginalFilePath = _ocrFiles.ErrorFilePath + OriginalFileName;
            }
        }
    }
    protected void GetCGV(string sRegistrationCode, out string sFilename)
    {
        SqlConnection conn = null;
        sFilename = "";

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["PARTNER"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("Registration.P_GetCGV", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN", new XElement("Info", new XAttribute("RegistrationCode", sRegistrationCode))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sOut = cmd.Parameters["@OUT"].Value.ToString();

            if (sOut.Length > 0)
            {
                List<string> listFilename = CommonMethod.GetAttributeValues(sOut, "ALL_XML_OUT/I", "FileName");
                if (listFilename.Count > 0)
                    sFilename = listFilename[0];
            }
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }
    }
    protected void getPinBySmsInformation(string sRegistrationCode)
    {
        SqlConnection conn = null;
        authentication auth = authentication.GetCurrent();

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("BankSI.P_GetPinBySMSInformation", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@FindPinBySMSCriteria", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@PinBySMSInformation", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@ReturnSelect", SqlDbType.Bit);
            cmd.Parameters.Add("@RC", SqlDbType.Int);

            string sXmlIN = new XElement("ALL_XML_IN",
                                                new XElement("PinBySMS",
                                                    new XAttribute("RegistrationCode", sRegistrationCode),
                                                    new XAttribute("CashierToken", auth.sToken),
                                                    new XAttribute("CultureID", (Session["CultureID"] != null) ? Session["CultureID"].ToString() : "Fr-fr")
                                                )
                                            ).ToString();

            cmd.Parameters["@FindPinBySMSCriteria"].Value = sXmlIN;
            cmd.Parameters["@ReturnSelect"].Value = false;

            cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;
            cmd.Parameters["@PinBySMSInformation"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sOut = cmd.Parameters["@PinBySMSInformation"].Value.ToString();

            if (sOut.Length > 0)
            {
                getPinBySmsInformationFromXml(sOut);
            }
            else
            {
                divPinBySms.Visible = false;
            }
        }
        catch (Exception e)
        {
            ImgPinBySmsStatus.AlternateText = "Inconnu";
            ImgPinBySmsStatus.ImageUrl = "./Styles/Img/black_status.png";
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

    }
    protected void getPinBySmsInformationFromXml(string sXml)
    {
        List<string> listExceptingPhoneNumber = CommonMethod.GetAttributeValues(sXml, "ALL_XML_OUT/PinBySMS", "ExpectingPhoneNumber");
        if (listExceptingPhoneNumber.Count > 0)
            txtExpectingPhoneNumber.Text = listExceptingPhoneNumber[0];

        List<string> listSuccessfullCheckDate = CommonMethod.GetAttributeValues(sXml, "ALL_XML_OUT/PinBySMS", "SuccessfullCheckDate");
        if (listSuccessfullCheckDate.Count > 0)
            txtSuccessfullCheckDate.Text = listSuccessfullCheckDate[0];

        List<string> listSMSElapsedTime = CommonMethod.GetAttributeValues(sXml, "ALL_XML_OUT/PinBySMS", "ElapsedTimeSMS");
        if (listSMSElapsedTime.Count > 0 && listSMSElapsedTime[0].Trim().Length > 0)
        {
            string[] SmsElapsedTime = listSMSElapsedTime[0].Split(':');

            string SmsElapsedTimeHH = (SmsElapsedTime[0].Trim() == "00") ? "" : SmsElapsedTime[0].Trim() + "h";
            string SmsElapsedTimeMM = (SmsElapsedTime[1].Trim() == "00") ? "" : SmsElapsedTime[1].Trim() + "min";
            txtSmsElapsedTime.Text = SmsElapsedTimeHH + SmsElapsedTimeMM + SmsElapsedTime[2] + "s";
            panelSmsElapsedTime.Visible = true;
        }
        else
        {
            panelSmsElapsedTime.Visible = false;
        }

        List<string> listSMSSent = CommonMethod.GetAttributeValues(sXml, "ALL_XML_OUT/PinBySMS", "SMSSent");
        if (listSMSSent.Count > 0)
        {
            if (listSMSSent[0] == "1")
            {
                txtSmsSent.Text = "OUI";
                ImgPinBySmsStatus.ImageUrl = "./Styles/Img/green_status.png";
            }
            else
            {
                txtSmsSent.Text = "NON";
                ImgPinBySmsStatus.ImageUrl = "./Styles/Img/red_status.png";
            }
        }
        else
        {
            txtSmsSent.Text = "NON";
            ImgPinBySmsStatus.ImageUrl = "./Styles/Img/red_status.png";
        }
        ImgPinBySmsStatus.AlternateText = txtSmsSent.Text;

        List<string> listNbAttempts = CommonMethod.GetAttributeValues(sXml, "ALL_XML_OUT/PinBySMS/Attempts", "NbAttempts");
        if (listNbAttempts.Count > 0)
            txtNbAttempts.Text = listNbAttempts[0];

        int nbAttempts = 0;
        DataTable dtAttemps = new DataTable();
        dtAttemps.Columns.Add("PhoneNumber");
        dtAttemps.Columns.Add("ChallengeReceived");
        dtAttemps.Columns.Add("Date");
        dtAttemps.Columns.Add("Status");
        dtAttemps.Columns.Add("StatusColor");

        if (int.TryParse(listNbAttempts[0], out nbAttempts) && nbAttempts > 0)
        {
            string sAttemptPhoneNumber = "", sAttemptChallengeReceived = "", sAttemptsDate = "", sAttemptError = "", sAttemptStatusColor = "";
            List<string> listAttemptPhoneNumber = new List<string>();
            List<string> listAttemptChallengeReceived = new List<string>();
            List<string> listAttemptDate = new List<string>();
            List<string> listAttemptRC = new List<string>();

            List<string> listAttempts = CommonMethod.GetTags(sXml, "ALL_XML_OUT/PinBySMS/Attempts/Attempt");
            for (int i = 0; i < listAttempts.Count; i++)
            {
                listAttemptPhoneNumber = CommonMethod.GetAttributeValues(listAttempts[i], "Attempt", "PhoneNumber");
                listAttemptChallengeReceived = CommonMethod.GetAttributeValues(listAttempts[i], "Attempt", "ChallengeReceived");
                listAttemptDate = CommonMethod.GetAttributeValues(listAttempts[i], "Attempt", "AttemptDate");
                listAttemptRC = CommonMethod.GetAttributeValues(listAttempts[i], "Attempt", "ResultCode");

                if (listAttemptPhoneNumber.Count > 0)
                    sAttemptPhoneNumber = listAttemptPhoneNumber[0];

                if (listAttemptChallengeReceived.Count > 0)
                    sAttemptChallengeReceived = listAttemptChallengeReceived[0];

                if (listAttemptDate.Count > 0)
                    sAttemptsDate = listAttemptDate[0];

                if (listAttemptRC.Count > 0 && listAttemptRC[0].Trim() != "0")
                {
                    List<string> listAttemptError = CommonMethod.GetAttributeValues(listAttempts[i], "Attempt", "ResultSignification");
                    if (listAttemptError.Count > 0)
                    {
                        sAttemptError = listAttemptError[0] + " (" + listAttemptRC[0] + ")";

                        sAttemptStatusColor = "Red";
                    }
                }
                else
                {
                    sAttemptError = "OK";
                    sAttemptStatusColor = "Green";
                }

                dtAttemps.Rows.Add(new object[] { sAttemptPhoneNumber, sAttemptChallengeReceived, sAttemptsDate, sAttemptError, sAttemptStatusColor });
            }


            //divBtnAttemptDetails.Visible = true;
            btnAttemptDetails.Visible = true;
        }
        else
        {
            //divBtnAttemptDetails.Visible = false;
            btnAttemptDetails.Visible = false;
        }

        gvAttemptDetails.DataSource = dtAttemps;
        gvAttemptDetails.DataBind();
    }
    protected System.Drawing.Color getColor(string color)
    {
        System.Drawing.Color dColor;
        dColor = System.Drawing.Color.FromName(color);
        return dColor;
    }
    protected void getWebPasswordBySmsInformation(string sRegistrationCode)
    {
        SqlConnection conn = null;
        authentication auth = authentication.GetCurrent();

        try
        {

            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("BankSI.P_GetWebPasswordSentInformation", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@FindWebPasswordCriteria", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@WebPasswordInformation", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@ReturnSelect", SqlDbType.Bit);
            cmd.Parameters.Add("@RC", SqlDbType.Int);

            string sXmlIN = new XElement("ALL_XML_IN",
                                                new XElement("WebPassword",
                                                    new XAttribute("RegistrationCode", sRegistrationCode),
                                                    new XAttribute("CashierToken", auth.sToken),
                                                    new XAttribute("CultureID", (Session["CultureID"] != null) ? Session["CultureID"].ToString() : "Fr-fr")
                                                )
                                            ).ToString();

            cmd.Parameters["@FindWebPasswordCriteria"].Value = sXmlIN;
            cmd.Parameters["@ReturnSelect"].Value = false;

            cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;
            cmd.Parameters["@WebPasswordInformation"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sOut = cmd.Parameters["@WebPasswordInformation"].Value.ToString();

            if (sOut.Length > 0)
            {
                getWebPasswordBySmsInformationFromXml(sOut);
            }
            else
            {
                divWebPassword.Visible = false;
            }
        }
        catch (Exception e)
        {
            ImgWebPassBySmsStatus.AlternateText = "Inconnu";
            ImgWebPassBySmsStatus.ImageUrl = "./Styles/Img/black_status.png";
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }
    }
    protected void getWebPasswordBySmsInformationFromXml(string sXml)
    {
        List<string> listCustomerPhoneNumber = CommonMethod.GetAttributeValues(sXml, "ALL_XML_OUT/WebPassword", "CustomerPhoneNumber");
        if (listCustomerPhoneNumber.Count > 0)
            txtWebPasswordPhoneNumber.Text = listCustomerPhoneNumber[0];

        List<string> listSmsSent = CommonMethod.GetAttributeValues(sXml, "ALL_XML_OUT/WebPassword", "SMSSent");
        if (listSmsSent.Count > 0 && listSmsSent[0].Trim() != "")
        {
            txtWebPasswordSmsSent.Text = (listSmsSent[0] == "1") ? "OUI" : "NON";

            if (listSmsSent[0] == "1")
            {
                txtWebPasswordSmsSent.Text = "OUI";
                ImgWebPassBySmsStatus.ImageUrl = "./Styles/Img/green_status.png";
            }
            else
            {
                txtWebPasswordSmsSent.Text = "NON";
                ImgWebPassBySmsStatus.ImageUrl = "./Styles/Img/red_status.png";
            }

            List<string> listLastSentDate = CommonMethod.GetAttributeValues(sXml, "ALL_XML_OUT/WebPassword", "LastSentDate");
            if (listLastSentDate.Count > 0)
                txtWebPasswordLastSentDate.Text = listLastSentDate[0];

            List<string> listLastReceiveDate = CommonMethod.GetAttributeValues(sXml, "ALL_XML_OUT/WebPassword", "LastReceiveDate");
            if (listLastReceiveDate.Count > 0 && listLastReceiveDate[0].Trim() != "")
            {
                txtWebPasswordLastReceiveDate.Text = listLastReceiveDate[0];
                txtWebPasswordLastReceiveDate.Style.Remove("font-style");
                txtWebPasswordLastReceiveDate.Style.Remove("color");
            }
            else
            {
                txtWebPasswordLastReceiveDate.Text = "INCONNU";
                txtWebPasswordLastReceiveDate.Style.Add("font-style", "italic");
                txtWebPasswordLastReceiveDate.Style.Add("color", "#AAA");
            }
        }
        else
        {
            txtWebPasswordSmsSent.Text = "NON";
            txtWebPasswordLastSentDate.Text = "";
            txtWebPasswordLastReceiveDate.Text = "";
            ImgWebPassBySmsStatus.ImageUrl = "./Styles/Img/red_status.png";
        }

        ImgWebPassBySmsStatus.AlternateText = txtWebPasswordSmsSent.Text;
    }
    protected DataTable getRegistrationAttempts(string sRegistrationCode)
    {
        SqlConnection conn = null;
        DataTable dt = new DataTable();

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("BankSI.P_GetRegistrationAttempt", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@registrationCode", SqlDbType.VarChar, 20);
            cmd.Parameters["@registrationCode"].Value = sRegistrationCode;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;

            da.Fill(dt);
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }

    protected string getXmlDate(string sDate)
    {
        string sDateToShow = "";
        DateTime dtDate;

        if (DateTime.TryParse(sDate, out dtDate))
            sDateToShow = DateTime.Parse(sDate).ToString("dd/MM/yyyy HH:mm:ss");
        else
            sDateToShow = "<i>date inconnue</i>";

        return sDateToShow;
    }
    protected string getXmlFormat(string xml, string date, string xmlType)
    {
        return FormatXml(xml, date, xmlType);
    }
    protected void DeleteOldXml()
    {
        //Supprime tous les fichiers du dossier XmlTmp ne datant pas du jour
        string sXmlTmpPath = @System.Web.HttpContext.Current.Request.MapPath(".") + "\\XmlTmp\\";
        string[] tXmlTmpFiles = Directory.GetFiles(sXmlTmpPath);
        string sXmlTmpFilePath = ""; string sXmlTmpFileDate = "";
        string sDateToday = DateTime.Now.ToString("yyyyMMdd");
        for (int i = 0; i < tXmlTmpFiles.Length; i++)
        {
            try
            {
                sXmlTmpFilePath = tXmlTmpFiles[i];
                sXmlTmpFileDate = sXmlTmpFilePath.Split('.')[0].Split('\\')[sXmlTmpFilePath.Split('.')[0].Split('\\').Length - 1].Split('_')[2];

                if (sXmlTmpFileDate != sDateToday)
                {
                    File.Delete(sXmlTmpFilePath);
                }

            }
            catch (Exception ex)
            {
            }
        }
    }

    protected string getStatusButton(string xml)
    {
        string status = "";

        if (xml.Trim().Length == 0)
            status = "disabled='disabled'";

        return status;
    }

    protected string FormatXml(string sXml, string sDate, string sXmlType)
    {
        StringBuilder sb = new StringBuilder();
        DateTime dtDate;
        string urlPathFile = "";
        string pathFile = "";
        string filename = "";
        string filenameDate = "";
        string title = "";

        if (sXml.Trim().Length > 0)
        {
            if (DateTime.TryParse(sDate, out dtDate))
            {
                filenameDate = DateTime.Parse(sDate).ToString("yyyyMMddHHmmss");
                title = sXmlType + " DU " + sDate;
            }
            else
            {
                filenameDate = "inconnue";
                title = sXmlType + " DU <i>date inconnue</i>";
            }

            filename = "xml_" + filenameDate + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xml";
            urlPathFile = "\"" + @Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.LastIndexOf('/') + 1) + "XmlTmp/" + filename + "\", \"" + title + "\"";
            pathFile = @System.Web.HttpContext.Current.Request.MapPath(".") + "\\XmlTmp\\" + filename;

            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(sXml);

                StringWriter sw = new StringWriter(sb);
                XmlTextWriter xmlTextWriter = new XmlTextWriter(sw);
                xmlTextWriter.Formatting = Formatting.Indented;
                xmlDoc.WriteTo(xmlTextWriter);
                xmlDoc.Save(pathFile);
            }
            catch (Exception ex)
            { }
        }
        else
        {
            urlPathFile = "\"\", \"" + sXmlType + " DU <i>date inconnue</i>" + "\"";
        }

        //return sb.ToString();
        return urlPathFile;
    }
    protected XmlDocument getXML(string sXML)
    {

        XmlDocument xd = new XmlDocument();

        try
        {
            xd.LoadXml(sXML);
        }
        catch (Exception ex)
        {
        }

        return xd;
    }

    protected void getPdfSubscriptionInformation(string sRegistrationCode)
    {
        SqlConnection conn = null;
        authentication auth = authentication.GetCurrent();

        try
        {

            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("BankSI.P_GetPDFSubscriptionSentInformation", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@FindPDFSubscriptionCriteria", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@PDFSubscriptionInformation", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@ReturnSelect", SqlDbType.Bit);
            cmd.Parameters.Add("@RC", SqlDbType.Int);

            string sXmlIN = new XElement("ALL_XML_IN",
                                                new XElement("PDFSubscription",
                                                    new XAttribute("RegistrationCode", sRegistrationCode),
                                                    new XAttribute("CashierToken", auth.sToken),
                                                    new XAttribute("CultureID", (Session["CultureID"] != null) ? Session["CultureID"].ToString() : "Fr-fr")
                                                )
                                            ).ToString();

            cmd.Parameters["@FindPDFSubscriptionCriteria"].Value = sXmlIN;
            cmd.Parameters["@ReturnSelect"].Value = false;

            cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;
            cmd.Parameters["@PDFSubscriptionInformation"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sOut = cmd.Parameters["@PDFSubscriptionInformation"].Value.ToString();

            if (sOut.Length > 0)
            {
                getPdfSubscriptionInformationFromXml(sOut);
            }
            else
            {
                divPdf.Visible = false;
            }
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

    }
    protected void getPdfSubscriptionInformationFromXml(string sXml)
    {
        txtPdfSent.Text = "NON";
        List<string> listPdfSent = CommonMethod.GetAttributeValues(sXml, "ALL_XML_OUT/PDFSubscription", "Sent");

        if (listPdfSent.Count > 0 && listPdfSent[0].Trim() != "")
        {
            txtPdfSent.Text = (listPdfSent[0] == "1") ? "OUI" : "NON";

            if (listPdfSent[0] == "1")
            {
                txtPdfSent.Text = "OUI";
                //btnShowSubscriptionPdf.Visible = true;

                List<string> listPdfSent_Date = CommonMethod.GetAttributeValues(sXml, "ALL_XML_OUT/PDFSubscription", "SendDate");
                if (listPdfSent_Date.Count > 0)
                {
                    DateTime dtPdfSent_Date;
                    if (DateTime.TryParse(listPdfSent_Date[0], out dtPdfSent_Date))
                        //txtPdfSent_Date.Text = dtPdfSent_Date.ToString("dd/MM/yyyy hh:mm:ss");
                        txtPdfSent_Date.Text = listPdfSent_Date[0];
                    else
                        txtPdfSent_Date.Text = listPdfSent_Date[0];

                    panelPdfSent_Date.Visible = true;
                }

                string sRegistrationNumber = lblRegistrationSelected.Text;

                //string filepath = @"." + ConfigurationManager.AppSettings["SubscriptionPdfPath"].ToString() + lblRegistrationSelected.Text + ".pdf";
                string dynamicSubDirectory = tools.getSubscriptionDirectory(sRegistrationNumber);

                string filepath = "";
                if (File.Exists(System.Web.HttpContext.Current.Server.MapPath("~" + ConfigurationManager.AppSettings["SubscriptionPdfPath"].ToString() + dynamicSubDirectory + "/" + sRegistrationNumber + ".pdf")))
                    filepath = "." + ConfigurationManager.AppSettings["SubscriptionPdfPath"].ToString() + dynamicSubDirectory + "/" + sRegistrationNumber + ".pdf";
                else
                    filepath = "." + ConfigurationManager.AppSettings["SubscriptionPdfPath"].ToString() + dynamicSubDirectory + "/Dossier_ouverture_" + sRegistrationNumber + ".pdf";
                string title = "Dossier inscription " + sRegistrationNumber;
                //btnShowSubscriptionPdf.OnClientClick = "clickShowPdf('" + filepath + "','" + title + "');return false;";
                //hlTestPdf.NavigateUrl = filepath;
                //hlTestPdf.Target = "_blank";
                //hlTestPdf.Visible = false;

                string sDocFilePath = ConfigurationManager.AppSettings["NickelDocPath"].ToString();

                if (File.Exists(HostingEnvironment.MapPath("~" + sDocFilePath + dynamicSubDirectory + "Dossier_ouverture_" + sRegistrationNumber + ".pdf")))
                    hdnRegistrationFilePath.Value = "." + sDocFilePath + dynamicSubDirectory + "Dossier_ouverture_" + sRegistrationNumber + ".pdf";
                else if (File.Exists(HostingEnvironment.MapPath("~" + sDocFilePath + dynamicSubDirectory + sRegistrationNumber + ".pdf")))
                    hdnRegistrationFilePath.Value = "." + sDocFilePath + dynamicSubDirectory + sRegistrationNumber + ".pdf";
                else
                    hdnRegistrationFilePath.Value = "";
            }
            else
            {
                txtPdfSent.Text = "NON";
                panelPdfSent_Date.Visible = false;
                //btnShowSubscriptionPdf.Visible = false;
            }
        }
        else
        {
            txtPdfSent.Text = "NON";
            panelPdfSent_Date.Visible = false;
            //btnShowSubscriptionPdf.Visible = false;
        }
    }

    protected DataTable getMpadStepsDetails(string sRegistrationCode)
    {
        SqlConnection conn = null;
        DataTable dt = new DataTable();

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Partner"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("Registration.P_MPAD_Steps_History", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@RegistrationCode", SqlDbType.VarChar, 20);
            cmd.Parameters["@RegistrationCode"].Value = sRegistrationCode;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;

            da.Fill(dt);
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }

    protected bool updateRegistration(string sXML, out string sError)
    {
        SqlConnection conn = null;

        sError = "";
        bool isOK = false;
        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["PARTNER"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("Registration.P_BackOfficeCorrections", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);
            cmd.Parameters["@IN"].Value = sXML;
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sOut = cmd.Parameters["@OUT"].Value.ToString();

            string sRC = "";
            GetValueFromXml(sOut, "ALL_XML_OUT/User", "RC", out sRC);
            GetValueFromXml(sOut, "ALL_XML_OUT/User", "ErrorLabel", out sError);

            if (sOut.Length > 0 && sRC.Trim() == "0")
                isOK = true;
            else
            {
                isOK = false;
                sError += "(" + sRC + ")";
            }
        }
        catch (Exception e)
        {
            isOK = false;
            sError = "Une erreur inattendue est survenue";
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return isOK;
    }

    protected bool checkClientInformations(out string sErrorList)
    {
        bool isOK = true;
        sErrorList = "";
        string sError = "";

        DateTime dt;
        int num;

        if (panelMarriedLastName.Visible && txtIDMarriedLastName.Text.Trim().Length == 0)
            sError += "<li>Nom marital</li>";

        if (txtIDLastName.Text.Trim().Length == 0)
            sError += "<li>Nom</li>";

        if (txtIDFirstName.Text.Trim().Length == 0)
            sError += "<li>Prénom</li>";

        if (txtBirthDate.Text.Trim().Length == 0 || !DateTime.TryParseExact(txtBirthDate.Text, "dd/MM/yyyy", new CultureInfo("fr-FR"), DateTimeStyles.None, out dt))
            sError += "<li>Date de naissance</li>";

        if (txtBirthCity.Text.Trim().Length == 0)
            sError += "<li>Ville de naissance</li>";

        if (ddlBirthCountry.SelectedValue.Trim().Length == 0)
            sError += "<li>Pays de naissance</li>";

        if (txtDocumentNumber.Text.Trim().Length == 0)
            sError += "<li>Numéro de document</li>";

        if (txtExpirationDate.Text.Trim().Length == 0 || !DateTime.TryParseExact(txtExpirationDate.Text, "dd/MM/yyyy", new CultureInfo("fr-FR"), DateTimeStyles.None, out dt))
            sError += "<li>Date d'expiration</li>";

        //if (txtIssuedID.Text.Trim().Length == 0 || !DateTime.TryParseExact(txtIssuedID.Text, "dd/MM/yyyy", new CultureInfo("fr-FR"), DateTimeStyles.None, out dt))
        //sError += "<li>Date d'émission</li>";

        if (ddlDocumentCountry.SelectedValue.Trim().Length == 0)
            sError += "<li>Pays du document</li>";

        if (txtAddress1.Text.Trim().Length == 0)
            sError += "<li>Adresse</li>";

        if (txtZipCode.Text.Trim().Length != 5 || !int.TryParse(txtZipCode.Text, out num))
            sError += "<li>Code postal</li>";

        if (txtCity.Text.Trim().Length == 0)
            sError += "<li>Ville</li>";

        if (sError.Trim().Length > 0)
        {
            isOK = false;
            sErrorList = "<ul>" + sError + "</ul>";
        }

        return isOK;
    }
    protected string getXmlFromClientInformations()
    {
        string sXML = "";
        authentication auth = authentication.GetCurrent();

        try
        {

            XElement xml = new XElement("Registration",
                                new XAttribute("CashierToken", auth.sToken),
                                new XAttribute("RegistrationCode", lblRegistrationSelected.Text),
                                new XAttribute("Culture", (Session["Culture"] != null) ? Session["Culture"].ToString() : "fr-FR"));

            //ID DOCUMENT
            //if (!txtIDMarriedLastName.ReadOnly)
            xml.Add(new XElement("INFO",
                new XAttribute("N", "MLN"),
                new XAttribute("V", txtIDMarriedLastName.Text.ToUpper())));
            //if (!txtIDLastName.ReadOnly)
            xml.Add(new XElement("INFO",
                new XAttribute("N", "LNA"),
                new XAttribute("V", txtIDLastName.Text.ToUpper())));
            //if (!txtIDFirstName.ReadOnly)
            xml.Add(new XElement("INFO",
                new XAttribute("N", "FNA"),
                new XAttribute("V", txtIDFirstName.Text.ToUpper())));
            //if (!txtGender.ReadOnly)
            xml.Add(new XElement("INFO",
                new XAttribute("N", "SEX"),
                new XAttribute("V", rblGender.SelectedValue.ToUpper())));
            //if (!txtBirthDate.ReadOnly)
            xml.Add(new XElement("INFO",
                new XAttribute("N", "BDA"),
                new XAttribute("V", txtBirthDate.Text.ToUpper())));
            //if (!txtBirthCity.ReadOnly)
            xml.Add(new XElement("INFO",
                new XAttribute("N", "BPL"),
                new XAttribute("V", txtBirthCity.Text.ToUpper())));
            //if (!txtBirthCountry.ReadOnly)
            xml.Add(new XElement("INFO",
                new XAttribute("N", "BI2"),
                new XAttribute("V", ddlBirthCountry.SelectedValue.ToUpper())));
            //if (!txtDocumentNumber.ReadOnly)
            xml.Add(new XElement("INFO",
                new XAttribute("N", "DTN"),
                new XAttribute("V", txtDocumentNumber.Text.ToUpper())));
            //if (!txtExpirationDate.ReadOnly)
            xml.Add(new XElement("INFO",
                new XAttribute("N", "EDT"),
                new XAttribute("V", txtExpirationDate.Text.ToUpper())));
            //if (!txtIssuedID.ReadOnly)
            xml.Add(new XElement("INFO",
                new XAttribute("N", "IDT"),
                new XAttribute("V", txtIssuedID.Text.ToUpper())));
            //if (!txtDocumentCountry.ReadOnly)
            xml.Add(new XElement("INFO",
                new XAttribute("N", "ICT"),
                new XAttribute("V", ddlDocumentCountry.SelectedValue.ToUpper())));

            //HO DOCUMENT
            //if (!txtBiller.ReadOnly)
            xml.Add(new XElement("INFO",
                new XAttribute("N", "SDR"),
                new XAttribute("V", txtBiller.Text.ToUpper())));
            //if (!txtTo.ReadOnly)
            xml.Add(new XElement("INFO",
                new XAttribute("N", "TO"),
                new XAttribute("V", txtTo.Text.ToUpper())));
            //if (!txtIssuedFact.ReadOnly)
            xml.Add(new XElement("INFO",
                new XAttribute("N", "IDT"),
                new XAttribute("V", txtIssuedFact.Text.ToUpper())));
            //if (!txtAddress1.ReadOnly)
            xml.Add(new XElement("INFO",
                new XAttribute("N", "AD1"),
                new XAttribute("V", txtAddress1.Text.ToUpper())));
            //if (!txtAddress2.ReadOnly)
            xml.Add(new XElement("INFO",
                new XAttribute("N", "AD2"),
                new XAttribute("V", txtAddress2.Text.ToUpper())));
            //if (!txtZipCode.ReadOnly)
            xml.Add(new XElement("INFO",
                new XAttribute("N", "ZIP"),
                new XAttribute("V", txtZipCode.Text.ToUpper())));
            //if (!txtCity.ReadOnly)
            xml.Add(new XElement("INFO",
                new XAttribute("N", "CIT"),
                new XAttribute("V", txtCity.Text.ToUpper())));

            sXML = new XElement("ALL_XML_IN", xml).ToString();
        }
        catch (Exception ex)
        { }

        return sXML;
    }

    protected void refresh()
    {
        if (lblRegistrationSelected.Text.Trim() != "")
        {
            initAllFields();
            getRegistrationDetails(lblRegistrationSelected.Text);
            registration registr = registration.GetRegistrationCheckDetails(lblRegistrationSelected.Text, sRefTransaction, sRefCustomer);
            authentication auth = authentication.GetCurrent();

            InitAllFieldsInfo(registr);

            MergeClientSheetWithSubscriptionSheet();

            if(registr.iRefUserChecker.ToString() != auth.sRef)
            {

                if (registr.iRefUserChecker != 0) //test
                {
                    registration.Clear(lblRegistrationSelected.Text);
                    Response.Redirect("RegistrationSearchV2.aspx", true);
                }
            }

            Session["RegistrationCheck"] = registr;
            getWebPasswordBySmsInformation(lblRegistrationSelected.Text);
            getPdfSubscriptionInformation(lblRegistrationSelected.Text);

            string sSignatureFilename = "";
            GetCGV(lblRegistrationSelected.Text, out sSignatureFilename);
            if (sSignatureFilename.Trim().Length == 0)
                sSignatureFilename = "CGV_" + lblRegistrationSelected.Text + ".jpg";

            string sFilePath = ConfigurationManager.AppSettings["SignaturePath"].ToString();
            if (File.Exists(System.Web.HttpContext.Current.Server.MapPath("~" + sFilePath + sSignatureFilename)))
                imgSignatureCgv.ImageUrl = "." + sFilePath + sSignatureFilename;

            imgStep22.ImageUrl = imgSignatureCgv.ImageUrl;

            getPinBySmsInformation(lblRegistrationSelected.Text);

            rMpadStepsDetails.DataSource = getMpadStepsDetails(lblRegistrationSelected.Text);
            rMpadStepsDetails.DataBind();

            btnForceSubscription.Visible = false;
            if (hfRegistrationClosed.Value != "Y")
                btnForceSubscription.Visible = true;

            btnRectified.Visible = false;
            btnValidate.Visible = false;
            //btnModify.Visible = false;
            //btnModifyValidation.Visible = false;
            if (hfRegistrationChecked.Value == "N" && hfRegistrationClosed.Value == "Y")
            {
                //btnValidate.Visible = true;
            }
            else if (hfRegistrationChecked.Value == "Y" && hfRegistrationClosed.Value == "Y" && (hfRegistrationCheckStatus.Value == "O" || hfRegistrationCheckStatus.Value == "R"))
            {
                //btnRectified.Visible = true;
                //btnValidate.Visible = true;
                lblSubscriptionCheckCommentHistory.Text = lblRegistrationCheckedComment.Text.Trim();
                lblNbMaxCheckComment.Text = (198 - lblSubscriptionCheckCommentHistory.Text.Length).ToString();
            }

            if (hfRegistrationClosed.Value != "Y")
            {
                //btnModify.Visible = true;
                //btnModifyValidation.Visible = true;
            }

            panelRedMail.Visible = false;
            //rptRedMail.DataSource = Client.getIdenticalAddressList(lblRegistrationSelected.Text);
            //rptRedMail.DataBind();

            //if (rptRedMail.Items.Count > 0)
            //panelRedMail.Visible = true;

            //btnShowClient.Attributes.Add("onclick", "GoToClient('" + lblRegistrationSelected.Text + "')");

            //panelCheckID.Visible = true;
            //panelCheckIDNotAvailable.Visible = false;
            //panelCheckIDParent.Visible = false;
            //panelCheckIDParentNotAvailable.Visible = true;
            //if (registr.bCheckParentId && registr.bCurrentStepIsAdult)
            //{
            //    panelCheckID.Visible = false;
            //    panelCheckIDNotAvailable.Visible = true;
            //    panelCheckIDParent.Visible = true;
            //    panelCheckIDParentNotAvailable.Visible = false;
            //}
            //else if (registr.bMinorAccount && !registr.bCheckParentId)
            //{
            //    panelCheckIDParent.Visible = false;
            //    panelCheckIDParentNotAvailable.Visible = false;
            //    panelDocInfos.Visible = false;
            //    panelClientInfos.Visible = true;
            //}

            InitBirthPlace(registr);

            InitCheckStep(registr);

            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "showOcrCheck(\"MrzChecked\", \"mrz-ok.jpg\",'MRZ OK'); showOcrCheck(\"MrzNotChecked\", \"mrz-ko.jpg\", 'MRZ KO'); showOcrCheck(\"VisualChecked\", \"green-eye.png\",'VISUAL CHECK OK'); showOcrCheck(\"VisualNotChecked\", \"red-eye.png\",'VISUAL CHECK KO');initTooltip();", true);
        }
    }

    protected void refresh(bool bInit)
    {
        if (lblRegistrationSelected.Text.Trim() != "")
        {
            initAllFields();
            getRegistrationDetails(lblRegistrationSelected.Text);
            registration registr = registration.GetRegistrationCheckDetails(lblRegistrationSelected.Text, sRefTransaction, sRefCustomer);
            authentication auth = authentication.GetCurrent();

            InitAllFieldsInfo(registr);

            panelClientInfos.Visible = false;
            if (registr.ClientSheet.isParentHasAccount)
            {
                panelClientInfos.Visible = true;
            }

            MergeClientSheetWithSubscriptionSheet();

            if (registr.iRefUserChecker.ToString() != auth.sRef)
            {

                if (registr.iRefUserChecker != 0) //test
                {
                    registration.Clear(lblRegistrationSelected.Text);
                    Response.Redirect("RegistrationSearchV2.aspx", true);
                }
            }

            Session["RegistrationCheck"] = registr;
            getWebPasswordBySmsInformation(lblRegistrationSelected.Text);
            getPdfSubscriptionInformation(lblRegistrationSelected.Text);

            string sSignatureFilename = "";
            GetCGV(lblRegistrationSelected.Text, out sSignatureFilename);
            if (sSignatureFilename.Trim().Length == 0)
                sSignatureFilename = "CGV_" + lblRegistrationSelected.Text + ".jpg";

            string sFilePath = ConfigurationManager.AppSettings["SignaturePath"].ToString();
            if (File.Exists(System.Web.HttpContext.Current.Server.MapPath("~" + sFilePath + sSignatureFilename)))
                imgSignatureCgv.ImageUrl = "." + sFilePath + sSignatureFilename;

            imgStep22.ImageUrl = imgSignatureCgv.ImageUrl;

            getPinBySmsInformation(lblRegistrationSelected.Text);

            rMpadStepsDetails.DataSource = getMpadStepsDetails(lblRegistrationSelected.Text);
            rMpadStepsDetails.DataBind();

            btnForceSubscription.Visible = false;
            if (hfRegistrationClosed.Value != "Y")
                btnForceSubscription.Visible = true;

            btnRectified.Visible = false;
            btnValidate.Visible = false;
            //btnModify.Visible = false;
            //btnModifyValidation.Visible = false;
            if (hfRegistrationChecked.Value == "N" && hfRegistrationClosed.Value == "Y")
            {
                //btnValidate.Visible = true;
            }
            else if (hfRegistrationChecked.Value == "Y" && hfRegistrationClosed.Value == "Y" && (hfRegistrationCheckStatus.Value == "O" || hfRegistrationCheckStatus.Value == "R"))
            {
                //btnRectified.Visible = true;
                //btnValidate.Visible = true;
                lblSubscriptionCheckCommentHistory.Text = lblRegistrationCheckedComment.Text.Trim();
                lblNbMaxCheckComment.Text = (198 - lblSubscriptionCheckCommentHistory.Text.Length).ToString();
            }

            if (hfRegistrationClosed.Value != "Y")
            {
                //btnModify.Visible = true;
                //btnModifyValidation.Visible = true;
            }

            panelRedMail.Visible = false;
            //rptRedMail.DataSource = Client.getIdenticalAddressList(lblRegistrationSelected.Text);
            //rptRedMail.DataBind();

            //if (rptRedMail.Items.Count > 0)
            //panelRedMail.Visible = true;

            //btnShowClient.Attributes.Add("onclick", "GoToClient('" + lblRegistrationSelected.Text + "')");

            //panelCheckID.Visible = true;
            //panelCheckIDNotAvailable.Visible = false;
            //panelCheckIDParent.Visible = false;
            //panelCheckIDParentNotAvailable.Visible = true;
            //if (registr.bCheckParentId && registr.bCurrentStepIsAdult)
            //{
            //    panelCheckID.Visible = false;
            //    panelCheckIDNotAvailable.Visible = true;
            //    panelCheckIDParent.Visible = true;
            //    panelCheckIDParentNotAvailable.Visible = false;
            //}
            //else if (registr.bMinorAccount && !registr.bCheckParentId)
            //{
            //    panelCheckIDParent.Visible = false;
            //    panelCheckIDParentNotAvailable.Visible = false;
            //    panelDocInfos.Visible = false;
            //    panelClientInfos.Visible = true;
            //}

            InitBirthPlace(registr);

            InitCheckStep(registr, bInit);

            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "showOcrCheck(\"MrzChecked\", \"mrz-ok.jpg\",'MRZ OK'); showOcrCheck(\"MrzNotChecked\", \"mrz-ko.jpg\", 'MRZ KO'); showOcrCheck(\"VisualChecked\", \"green-eye.png\",'VISUAL CHECK OK'); showOcrCheck(\"VisualNotChecked\", \"red-eye.png\",'VISUAL CHECK KO');initTooltip();", true);

        }
    }

    protected void MergeClientSheetWithSubscriptionSheet()
    {
        registration registr = registration.GetCurrent(true, lblRegistrationSelected.Text);

        if(ddlIDDocumentCountry.SelectedValue != registr.ClientSheet.DocumentCountry)
        {
            ddlIDDocumentCountry.SelectedItem.Selected = false;
            ddlIDDocumentCountry.Items.FindByValue(registr.ClientSheet.DocumentCountry).Selected = true;
        }

        if(lblIDFirstName.Text.Trim() != registr.ClientSheet.FirstName.Trim())
        {
            lblIDFirstName.Text = registr.ClientSheet.FirstName.Trim();
            txtIDFirstName.Text = registr.ClientSheet.FirstName.Trim();
            lblIDFirstName.ToolTip = registr.ClientSheet.FirstName.Trim();
            AddClientSheetModificationVisualCheck(lblIDFirstName);
        }

        if (lblIDLastName.Text.Trim() != registr.ClientSheet.LastName.Trim())
        {
            lblIDLastName.Text = registr.ClientSheet.LastName.Trim();
            txtIDLastName.Text = registr.ClientSheet.LastName.Trim();
            lblIDLastName.ToolTip = registr.ClientSheet.LastName.Trim();
            AddClientSheetModificationVisualCheck(lblIDLastName);
        }
        if (lblIDMarriedLastName.Text.Trim() != registr.ClientSheet.MaritalName.Trim())
        {
            lblIDMarriedLastName.Text = registr.ClientSheet.MaritalName.Trim();
            txtIDMarriedLastName.Text = registr.ClientSheet.MaritalName.Trim();
            lblIDMarriedLastName.ToolTip = registr.ClientSheet.MaritalName.Trim();
            AddClientSheetModificationVisualCheck(lblIDMarriedLastName);
        }
        if (lblIDUsageLastName.Text.Trim() != registr.ClientSheet.UseName.Trim())
        {
            lblIDUsageLastName.Text = registr.ClientSheet.UseName.Trim();
            txtIDUsageLastName.Text = registr.ClientSheet.UseName.Trim();
            lblIDUsageLastName.ToolTip = registr.ClientSheet.UseName.Trim();
            AddClientSheetModificationVisualCheck(lblIDUsageLastName);
        }

        if (lblBirthDate.Text.Trim() != registr.ClientSheet.BirthDate.Trim())
        {
            lblBirthDate.Text = registr.ClientSheet.BirthDate.Trim();
            txtBirthDate.Text = registr.ClientSheet.BirthDate.Trim();
            lblBirthDate.ToolTip = registr.ClientSheet.BirthDate.Trim();
            AddClientSheetModificationVisualCheck(lblBirthDate);
        }

        if(ddlBirthCountry.SelectedValue.Trim() != registr.ClientSheet.BirthCountry.Trim())
        {
            ddlBirthCountry.SelectedItem.Selected = false;
            if(ddlBirthCountry.Items.FindByValue(registr.ClientSheet.BirthCountry.Trim()) != null)
                ddlBirthCountry.Items.FindByValue(registr.ClientSheet.BirthCountry.Trim()).Selected = true;
            lblBirthCountry.Text = ddlBirthCountry.SelectedItem.Text.Trim();
            lblBirthCountry.ToolTip = registr.ClientSheet.BirthCountry.Trim();
            AddClientSheetModificationVisualCheck(lblBirthCountry);
        }
        if((registr.ClientRegistrationSheet.BirthDep.Trim() != registr.ClientSheet.BirthDep.Trim()) ||
            registr.ClientRegistrationSheet.BirthCity.Trim() != registr.ClientSheet.BirthCity.Trim())
        {
            if (registration.CheckBirthDepartementNeeded(registr.ClientSheet.BirthCountry.Trim()))
            {
                lblBirthCity.Text = registr.ClientSheet.BirthCity.Trim() + " (" + registr.ClientSheet.BirthDep.Trim() + ")";
                txtBirthPlaceDepartment.Text = registr.ClientSheet.BirthDep.Trim();
            }
            else
            {
                lblBirthCity.Text = registr.ClientSheet.BirthCity.Trim();
                txtBirthPlaceDepartment.Text = "";
            }
            txtBirthCity.Text = registr.ClientSheet.BirthCity.Trim();
            txtBirthPlaceCity.Text = registr.ClientSheet.BirthCity.Trim();
            lblBirthCity.ToolTip = lblBirthCity.Text;
            AddClientSheetModificationVisualCheck(lblBirthCity);
        }
    }

    protected void InitBirthPlace(registration registr)
    {
        try
        {
            ddlBirthPlaceCountry.SelectedValue = registr.ClientSheet.BirthCountry;
            txtBirthPlaceDepartment.Text = registr.ClientSheet.BirthDep;
            txtBirthPlaceCity.Text = registr.ClientSheet.BirthCity;

            if (registr.bMinorAccount && registr.bCheckParentId)
            {
                ddlParentBirthPlaceCountry.SelectedValue = registr.ParentSheet.BirthCountry;
                txtParentBirthPlaceDepartment.Text = registr.ParentSheet.BirthDep;
                txtParentBirthPlaceCity.Text = registr.ParentSheet.BirthCity;

                
            }
        }
        catch(Exception ex)
        {

        }
    }

    protected void clickForceSubscription(object sender, EventArgs e)
    {
        authentication auth = authentication.GetCurrent();
        string sError = ""; string sMessage = "";
        string accountNumber = txtForceSubscriptionAccountNumber.Text;
        string trackingNumber = txtForceSubscriptionTrackingNumber.Text;
        txtForceSubscriptionAccountNumber.Text = "";
        txtForceSubscriptionTrackingNumber.Text = "";
        string sXML = new XElement("ALL_XML_IN",
                        new XElement("SABAnswer",
                            new XAttribute("CashierToken", auth.sToken),
                            new XAttribute("TrackingNumber", trackingNumber),
                            new XAttribute("AccountNumber", accountNumber),
                            new XAttribute("RegistrationCode", lblRegistrationSelected.Text.Trim()))).ToString();

        if (ForceSubscription(sXML, out sError))
        {
            sMessage = "<span style=\"color:green\">L'opération a réussi.</span>";
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "alertMessage('Forcer inscription','" + sMessage.Replace("'", "&apos;").Trim() + "');", true);
        }
        else
        {
            sMessage = "<span style=\"color:red\">L'opération a échoué.<br/>" + sError + "</span>";
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "alertMessage('Forcer inscription','" + sMessage.Replace("'", "&apos;").Trim() + "');", true);
        }
    }
    protected bool ForceSubscription(string sXML, out string sError)
    {
        sError = "";
        bool isOK = false;

        SqlConnection conn = null;
        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("Web.P_PushSABSubscriptionAnswer", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);
            cmd.Parameters["@IN"].Value = sXML;
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sOut = cmd.Parameters["@OUT"].Value.ToString();

            string sRC = "";
            GetValueFromXml(sOut, "ALL_XML_OUT/User", "RC", out sRC);

            if (sOut.Length > 0 && sRC.Trim() == "0")
                isOK = true;
            else
            {
                isOK = false;
                sError += "(" + sRC + ")";
            }
        }
        catch (Exception e)
        {
            isOK = false;
            sError = "Une erreur inattendue est survenue";
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return isOK;
    }

    protected void btnRegistrationChecked_click(object sender, EventArgs e)
    {
        string sNoComplianceList = "";
        for (int i = 0; i < rptSetNoComplianceList.Items.Count; i++)
        {
            try
            {
                HiddenField hfNoComplianceSelected = null;
                CheckBox cbNoCompliance = null;

                cbNoCompliance = (CheckBox)rptSetNoComplianceList.Items[i].FindControl("cbNoCompliance");
                if (cbNoCompliance != null && cbNoCompliance.Checked)
                {
                    hfNoComplianceSelected = (HiddenField)rptSetNoComplianceList.Items[i].FindControl("hfComplianceValue");
                    sNoComplianceList += hfNoComplianceSelected.Value + "+";
                }
            }
            catch (Exception ex)
            { }

        }

        if (sNoComplianceList.Trim().Length > 0)
            sNoComplianceList = sNoComplianceList.Substring(0, sNoComplianceList.Length - 1);

        RegistrationChecked(ddlSubscriptionCheckStatus.SelectedValue, txtSubscriptionCheckComment.Text, sNoComplianceList);
    }
    protected void btnRegistrationRectified_click(object sender, EventArgs e)
    {
        RegistrationChecked("G", txtSubscriptionCheckComment.Text, "");
    }
    protected void rptGetNoComplianceList_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (hfNoComplianceSelectedList.Value.Trim().Length > 0)
        {
            string[] sNoComplianceSelectedList = hfNoComplianceSelectedList.Value.Trim().Split('+');

            if (sNoComplianceSelectedList.Length > 0)
            {
                try
                {
                    CheckBox cbNoCompliance = null;
                    HiddenField hfComplianceValue = null;
                    string sNoComplianceSelectedValue = "";

                    sNoComplianceSelectedValue = ((HiddenField)e.Item.FindControl("hfComplianceValue")).Value.Trim();

                    for (int i = 0; i < sNoComplianceSelectedList.Length; i++)
                    {
                        if (sNoComplianceSelectedValue == sNoComplianceSelectedList[i].Trim())
                        {
                            ((CheckBox)e.Item.FindControl("cbNoCompliance")).Checked = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                }
            }
        }
    }

    protected bool getArchivedIdScan()
    {
        bool isOK = false;
        SqlConnection conn = null;
        DataTable dt = new DataTable();

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("[Google].[P_GetIDScanFiles]", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@RegistrationCode", SqlDbType.VarChar, 50);
            cmd.Parameters.Add("@RefCustomer", SqlDbType.Int);
            cmd.Parameters.Add("@RC", SqlDbType.Int);

            cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;
            cmd.Parameters["@RegistrationCode"].Value = lblRegistrationSelected.Text;
            cmd.Parameters["@RefCustomer"].Value = 0;
            cmd.ExecuteNonQuery();

            if (cmd.Parameters["@RC"].Value.ToString().Trim() == "0")
                isOK = true;
        }
        catch (Exception e)
        {
            isOK = false;
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return isOK;

        //EXEC[Google].[P_GetIDScanFiles] @RegistrationCode VARCHAR(50), @RefCustomer INT
    }

    protected void btnGetArchivedImageParent_Click(object sender, EventArgs e)
    {
        if (getArchivedIdScan())
            refresh();
    }

    protected void btnGetArchivedImage_Click(object sender, EventArgs e)
    {
        if (getArchivedIdScan())
            refresh();
    }

    protected void btnGoogleUpload_Click(object sender, EventArgs e)
    {
        bool isOK = false;
        SqlConnection conn = null;
        DataTable dt = new DataTable();
        if (hfUploadFileName.Value.Trim().Length > 0)
        {
            //<ALL_XML_IN><Google RefCustomer="2" RegistrationCode="NOB208436988440" KindTAG="ID" Encrypt="0" StoreFileName="_1_ID_I_02_NOB208436988440_20130903120554024_42371248-e25a-4516-b583-afc63a973598.JPG" StorePath="\\339741-OCR2\DOCS_ICAR\OUT\CroppedImages"/></ALL_XML_IN>
            string sXmlIn = new XElement("ALL_XML_IN",
                                new XElement("Google",
                                    //new XAttribute("RefCustomer", _iref),
                                    new XAttribute("RegistrationCode", lblRegistrationSelected.Text),
                                    new XAttribute("KindTAG", "ID"),
                                    new XAttribute("Encrypt", "0"),
                                    new XAttribute("StoreFileName", hfUploadFileName.Value),
                                    new XAttribute("StorePath", @"\\339741-OCR2\DOCS_ICAR\OUT\CroppedImages"))).ToString();


            try
            {
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
                conn.Open();

                SqlCommand cmd = new SqlCommand("[Google].P_UploadCustomerFile", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@IN", SqlDbType.VarChar, -1);
                cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);
                cmd.Parameters.Add("@RC", SqlDbType.Int);

                cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;
                cmd.Parameters["@IN"].Value = sXmlIn;
                cmd.Parameters["@OUT"].Value = "";

                //cmd.ExecuteNonQuery();

                if (cmd.Parameters["@RC"].Value.ToString().Trim() == "0")
                {
                    isOK = true;
                    Response.Redirect(Request.RawUrl);
                }
            }
            catch (Exception ex)
            {
                isOK = false;
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        if (!isOK)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "initfileupload('" + hfUploadFileName.Value + "');closeUploadImagesDialog();showUploadImagesDialog('" + hfUploadFace.Value + "'); ", true);
        }

        //return isOK;
    }

    protected void InitCheckStep(registration registr)
    {
        panelCheckID.Visible = true;
        panelCheckIDNotAvailable.Visible = false;
        panelCheckIDParent.Visible = false;
        panelCheckIDParentNotAvailable.Visible = true;

        Panel panelCurrentStep = (Panel)upCheckIDDocument.FindControl("panelStep" + registr.iCurrentStep.ToString());
        panelCheckIDDocumentbuttons.Visible = true;

        lblCheckIDCurrentStep.Text = registr.iCurrentStep.ToString();

        if (registr.bMinorAccount && registr.bCurrentStepIsAdult)
        {
            panelCurrentStep = (Panel)upCheckIDDocument.FindControl("panelParentStep" + registr.iCurrentStep.ToString());
            panelCheckID.Visible = false;
            panelCheckIDNotAvailable.Visible = true;
            panelCheckIDParent.Visible = true;
            panelCheckIDParentNotAvailable.Visible = false;
            lblCheckParentIDCurrentStep.Text = registr.iCurrentStep.ToString();
        }
        else if (registr.bMinorAccount && registr.ClientSheet.isParentHasAccount)
        {
            panelCheckIDParentNotAvailable.Visible = false;
            panelClientInfos.Visible = true;
            lblCheckParentIDCurrentStep.Text = registr.iCurrentStep.ToString();
        }

        btnCheckIDPreviousStep.Visible = false;
        if (registr.iCurrentStep > 1) { btnCheckIDPreviousStep.Visible = true; }

        panelCurrentStep.Visible = true;
        upCheckIDDocument.Update();
        upCheckParentIDDocument.Update();
    }

    protected void InitCheckStep(registration registr, bool Init)
    {
        panelCheckID.Visible = true;
        panelCheckIDNotAvailable.Visible = false;
        panelCheckIDParent.Visible = false;
        panelCheckIDParentNotAvailable.Visible = true;

        if(registr.iCurrentStep > 25 && !registr.ClientSheet.isParentHasAccount && registr.bMinorAccount && registr.bCurrentStepIsAdult)
        {
            registr.iCurrentStep = 25; //Récupération du type de livret de famille car pas sauver pour l'instant
        }
        else if (registr.bMinorAccount && registr.ClientSheet.isParentHasAccount && registr.bCurrentStepIsAdult)
        {
            registr.iCurrentStep = 25; //reprise d'un contrôle pièce du parent pour un CNJ sans NOB
        }

        Panel panelCurrentStep = (Panel)upCheckIDDocument.FindControl("panelStep" + registr.iCurrentStep.ToString());
        panelCheckIDDocumentbuttons.Visible = true;

        lblCheckIDCurrentStep.Text = registr.iCurrentStep.ToString();

        if (registr.bMinorAccount && registr.bCurrentStepIsAdult)
        {
            panelCurrentStep = (Panel)upCheckIDDocument.FindControl("panelParentStep" + registr.iCurrentStep.ToString());
            panelCheckID.Visible = false;
            panelCheckIDNotAvailable.Visible = true;
            panelCheckIDParent.Visible = true;
            panelCheckIDParentNotAvailable.Visible = false;
            lblCheckParentIDCurrentStep.Text = registr.iCurrentStep.ToString();
        }
        else if (registr.bMinorAccount && registr.ClientSheet.isParentHasAccount && registr.bCurrentStepIsAdult)
        {
            panelCurrentStep = (Panel)upCheckIDDocument.FindControl("panelParentStep" + registr.iCurrentStep.ToString());
            panelCheckID.Visible = false;
            panelCheckIDNotAvailable.Visible = true;
            panelCheckIDParent.Visible = true;
            panelCheckIDParentNotAvailable.Visible = false;
            lblCheckParentIDCurrentStep.Text = registr.iCurrentStep.ToString();
        }

        btnCheckIDPreviousStep.Visible = false;
        if (registr.iCurrentStep > 1) { btnCheckIDPreviousStep.Visible = true; }

        panelCurrentStep.Visible = true;
        upCheckIDDocument.Update();
        upCheckParentIDDocument.Update();
    }

    protected void btnIDDocumentChecked_Click(object sender, EventArgs e)
    {
        try
        {
            bool bShowNextStep = false;
            bool bEndCheck = false;
            string JSToCall = "";

            panelCheckIDDocumentbuttons.Visible = false;
            registration registr = registration.GetCurrent(false, lblRegistrationSelected.Text);
            int iStep = registr.iCurrentStep;
            
            //registr.bCurrentStepIsAdult = false;

            Panel panelCurrentStep = null;
            Panel panelNextStep = null;

            if (iStep <= 24)
            {
                panelCurrentStep = (Panel)upCheckIDDocument.FindControl("panelStep" + iStep.ToString());
                panelNextStep = (Panel)upCheckIDDocument.FindControl("panelStep" + (iStep + 1).ToString());
                panelCheckIDDocumentbuttons.Visible = true;

                switch (iStep)
                {
                    case 1:
                        bShowNextStep = registration.CheckRegistrationStep(registr, iStep, ddlIDDocumentType.SelectedValue, out bEndCheck);
                        if (bShowNextStep && !bEndCheck) { registr.ClientRegistrationSheet.DocumentType = ddlIDDocumentType.SelectedValue; }
                        break;
                    case 2:
                        bShowNextStep = registration.CheckRegistrationStep(registr, iStep, ddlIDDocumentCountry.SelectedValue, out bEndCheck);
                        if (bShowNextStep && !bEndCheck) { registr.ClientRegistrationSheet.DocumentCountry = ddlIDDocumentCountry.SelectedValue; }
                        break;
                    case 3:
                        bShowNextStep = registration.CheckRegistrationStep(registr, iStep, ddlIDDocumentCopy.SelectedValue, out bEndCheck);
                        break;
                    case 4:
                        bShowNextStep = registration.CheckRegistrationStep(registr, iStep, ddlIDDocumentRectoVerso.SelectedValue, out bEndCheck);
                        break;
                    case 5:
                        bShowNextStep = registration.CheckRegistrationStep(registr, iStep, ddlIDDocumentQuality.SelectedValue, out bEndCheck);
                        break;
                    case 6:
                        bShowNextStep = registration.CheckRegistrationStep(registr, iStep, ddlIDDocumentAlgerianResidencePermit.SelectedValue, out bEndCheck);
                        break;
                    case 7:
                        bShowNextStep = registration.CheckRegistrationStep(registr, iStep, ddlIDDocumentMRZ.SelectedValue, out bEndCheck);
                        break;
                    case 8:
                        bShowNextStep = registration.CheckRegistrationStep(registr, iStep, ddlIDDocumentHolo.SelectedValue, out bEndCheck);
                        break;
                    case 9:
                        bShowNextStep = registration.CheckRegistrationStep(registr, iStep, ddlIDDocumentRF.SelectedValue, out bEndCheck);
                        break;
                    case 10:
                        bShowNextStep = registration.CheckRegistrationStep(registr, iStep, ddlIDDocumentPuce.SelectedValue, out bEndCheck);
                        break;
                    case 11:
                        bShowNextStep = registration.CheckRegistrationStep(registr, iStep, ddlIDDocumentPolice.SelectedValue, out bEndCheck);
                        break;
                    case 12:
                        bShowNextStep = registration.CheckRegistrationStep(registr, iStep, ddlIDDocumentNumberLocation.SelectedValue, out bEndCheck);
                        break;
                    case 13:
                        bShowNextStep = registration.CheckRegistrationStep(registr, iStep, ddlIDDocumentPhoto.SelectedValue, out bEndCheck);
                        break;
                    case 14:
                        bShowNextStep = registration.CheckRegistrationStep(registr, iStep, ddlIDDocumentColor.SelectedValue, out bEndCheck);
                        break;
                    case 15:
                        bShowNextStep = registration.CheckRegistrationStep(registr, iStep, txtDocumentNumber.Text, out bEndCheck);
                        if (!bShowNextStep)
                        {
                            JSToCall = "modifyClientField('#btnModify-txtDocumentNumber', '"+ lblDocumentNumber.ClientID + "','"+ txtDocumentNumber.ClientID + "');";
                            JSToCall += "AlertMessage('Une erreur est survenue', '" + HttpUtility.JavaScriptStringEncode("Numéro de document non valide") + "');";
                        }
                        break;
                    case 16:
                        List<string> lsLastName = new List<string>();
                        lsLastName.Add(txtIDLastName.Text);
                        lsLastName.Add(txtIDMarriedLastName.Text);
                        lsLastName.Add(txtIDUsageLastName.Text);

                        if (lblIDLastName.Text.Trim() != txtIDLastName.Text.Trim() && registr.liStepInfo.Contains(16))
                            bShowNextStep = registration.CheckRegistrationStep(registr, iStep, lsLastName, ddlIDLastNameModificationReason.SelectedValue, out bEndCheck);
                        else { bShowNextStep = registration.CheckRegistrationStep(registr, iStep, lsLastName, out bEndCheck); }

                        if (!bShowNextStep)
                        {
                            JSToCall = "showDialogModificationReason('dialog-IDLastNameModificationReason', '" + ddlIDLastNameModificationReason.ClientID + "','#btnModify-txtIDLastName', '" + lblIDLastName.ClientID + "','" + txtIDLastName.ClientID + "');";
                            if (ddlIDLastNameModificationReason.SelectedValue.Trim().Length == 0)
                                JSToCall += "AlertMessage('Une erreur est survenue', '" + HttpUtility.JavaScriptStringEncode("Motif de modification du nom requis") + "');";
                            else { JSToCall += "AlertMessage('Une erreur est survenue', '" + HttpUtility.JavaScriptStringEncode("Nom(s) non valide") + "');"; }
                        }
                        break;
                    case 17:
                        if (lblIDFirstName.Text.Trim() != txtIDFirstName.Text.Trim() && registr.liStepInfo.Contains(17))
                            bShowNextStep = registration.CheckRegistrationStep(registr, iStep, txtIDFirstName.Text, ddlIDFirstNameModificationReason.SelectedValue, out bEndCheck);
                        else { bShowNextStep = registration.CheckRegistrationStep(registr, iStep, txtIDFirstName.Text, out bEndCheck); }

                        if (!bShowNextStep)
                        {
                            JSToCall = "showDialogModificationReason('dialog-IDLastNameModificationReason', '" + ddlIDLastNameModificationReason.ClientID + "','#btnModify-txtIDLastName', '" + lblIDLastName.ClientID + "','" + txtIDLastName.ClientID + "');";
                            if (ddlIDLastNameModificationReason.SelectedValue.Trim().Length == 0)
                                JSToCall += "AlertMessage('Une erreur est survenue', '" + HttpUtility.JavaScriptStringEncode("Motif de modification du prénom requis") + "');";
                            else { JSToCall += "AlertMessage('Une erreur est survenue', '" + HttpUtility.JavaScriptStringEncode("Prénom(s) non valide(s)") + "');"; }
                        }

                        break;
                    case 18:
                        bShowNextStep = registration.CheckRegistrationStep(registr, iStep, txtBirthDate.Text, out bEndCheck);
                        if (!bShowNextStep) {
                            JSToCall = "modifyClientField('#btnModify-txtBirthDate', '" + lblBirthDate.ClientID + "','" + txtBirthDate.ClientID + "');";
                            JSToCall += "AlertMessage('Une erreur est survenue', '" + HttpUtility.JavaScriptStringEncode("Date de naissance non valide") + "');";
                        }
                        break;
                    case 19:
                        List<string> lsBirthPlace = new List<string>();
                        lsBirthPlace.Add(ddlBirthPlaceCountry.SelectedValue);
                        lsBirthPlace.Add(txtBirthPlaceCity.Text);
                        if (registration.CheckBirthDepartementNeeded(ddlBirthPlaceCountry.SelectedValue))
                        {
                            lsBirthPlace.Add(txtBirthPlaceDepartment.Text);
                        }
                        else { lsBirthPlace.Add(""); }
                        bShowNextStep = registration.CheckRegistrationStep(registr, iStep, lsBirthPlace, out bEndCheck);
                        if (!bShowNextStep) {
                            JSToCall = "modifyBirthPlace('divCheckBirthPlace','divModifyBirthPlace');";
                            JSToCall += "AlertMessage('Une erreur est survenue', '" + HttpUtility.JavaScriptStringEncode("Lieu de naissance non valide") + "');";
                        }
                        break;
                    case 20:
                        //bShowNextStep = registration.CheckRegistrationStep(registr, iStep, ddlBirthCountry.SelectedValue, out bEndCheck);
                        //check birth country in step 19
                        break;
                    case 21:
                        bShowNextStep = registration.CheckRegistrationStep(registr, iStep, rblGender.SelectedValue, out bEndCheck);
                        if (!bShowNextStep)
                        {
                            JSToCall = "modifyClientField('#btnModify-txtGender', '" + lblGender.ClientID + "','" + rblGender.ClientID + "');";
                            JSToCall += "AlertMessage('Une erreur est survenue', '" + HttpUtility.JavaScriptStringEncode("Civilité non valide") + "');";
                        }
                        break;
                    case 22:
                        bShowNextStep = registration.CheckRegistrationStep(registr, iStep, txtExpirationDate.Text, out bEndCheck);
                        if (!bShowNextStep)
                        {
                            JSToCall = "modifyClientField('#btnModify-txtExpirationDate', '" + lblExpirationDate.ClientID + "','" + txtExpirationDate.ClientID + "');";
                            JSToCall += "AlertMessage('Une erreur est survenue', '" + HttpUtility.JavaScriptStringEncode("Date d'expiration non valide") + "');";
                        }
                        break;
                    case 23:
                        //Capture faciale
                        //bShowNextStep = true;
                        break;
                    case 24:
                        bShowNextStep = registration.CheckRegistrationStep(registr, iStep, ddlIDDocumentSignature.SelectedValue, out bEndCheck);
                        break;
                }

                if (bEndCheck)
                {
                    //registration.Clear(lblRegistrationSelected.Text);
                    //Response.Redirect("RegistrationSearchV2.aspx");
                    lblConfirmEndCheck.Text = "Confirmez-vous la conformité de la souscription ?";
                    if (!registr.ComplianceToApply.isOK)
                    {
                        if (!string.IsNullOrEmpty(registr.ComplianceToApply.TAG.Trim()) && !string.IsNullOrEmpty(registr.ComplianceToApply.Label.Trim()))
                        {
                            lblConfirmEndCheck.Text = "Confirmez-vous la non conformité de la souscription ?<br/><br/>";
                            lblConfirmEndCheck.Text += "Raison : <b>" + HttpUtility.JavaScriptStringEncode(registr.ComplianceToApply.Label) + " (" + HttpUtility.JavaScriptStringEncode(registr.ComplianceToApply.TAG.Replace("_", " ").Trim()) + ") </b>";
                        }
                        else
                        {
                            lblConfirmEndCheck.Text = "Confirmez-vous la non conformité de la souscription ?<br/><br/>";
                            lblConfirmEndCheck.Text += "<span style=\"font-style:italic; font-weight:bold\">AUCUNE COMPLIANCE A APPLIQUER</span>";
                            //throw new Exception("NULL or EMPTY Compliance");
                        }
                    }

                    upConfirmEndCheck.Update();
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "ShowConfirmEndCheck();", true);
                }
                else if (bShowNextStep)
                {
                    btnCheckIDPreviousStep.Visible = true;
                    panelCheckID.Visible = true;
                    panelCheckIDNotAvailable.Visible = false;
                    panelCheckIDParent.Visible = false;
                    panelCheckIDParentNotAvailable.Visible = true;

                    if (registr.bMinorAccount && registr.bCurrentStepIsAdult)
                    {
                        panelCheckID.Visible = false;
                        panelCheckIDNotAvailable.Visible = true;
                        panelCheckIDParent.Visible = true;
                        panelCheckIDParentNotAvailable.Visible = false;

                    }

                    if (iStep == 24 && !registr.bCurrentStepIsAdult)
                    {
                        registr.bCurrentStepIsAdult = true;
                        btnCheckParentIDPreviousStep.Visible = true;
                        panelCheckParentIDDocumentbuttons.Visible = true;
                        panelCheckID.Visible = false;
                        panelCheckIDNotAvailable.Visible = true;
                        panelCheckIDParent.Visible = true;
                        panelCheckIDParentNotAvailable.Visible = false;

                        if (!registr.ClientSheet.isParentHasAccount)
                        {
                            registr.iCurrentStep = 1;
                            lblCheckParentIDCurrentStep.Text = "1";
                            panelNextStep = (Panel)upCheckParentIDDocument.FindControl("panelParentStep1");
                        }
                        else
                        {
                            registr.iCurrentStep = 25;
                            lblCheckParentIDCurrentStep.Text = "25";
                            panelNextStep = (Panel)upCheckParentIDDocument.FindControl("panelParentStep25");
                        }
                        panelNextStep.Visible = true;
                    }
                    else
                    {
                        panelCurrentStep.Visible = false;
                        panelNextStep.Visible = true;
                        //checkNextStepInfo(registr);
                        iStep++;
                        registr.iCurrentStep = iStep;
                        lblCheckIDCurrentStep.Text = registr.iCurrentStep.ToString();
                    }

                    lblCheckIDCurrentStep.Text = registr.iCurrentStep.ToString();
                    checkNextStep(registr);

                    upCheckIDDocument.Update();
                    upCheckParentIDDocument.Update();
                }
                else if(!bShowNextStep && !string.IsNullOrEmpty(JSToCall))
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), JSToCall, true);
                }
            }
            /*else if (iStep == 24)
            {
                panelCheckIDDocumentbuttons.Visible = true;
                if (registration.CheckRegistrationStep(registr, iStep, ddlIDDocumentSignature.SelectedValue, out bEndCheck) || bEndCheck)
                {
                    Response.Redirect("RegistrationSearchV2.aspx");
                }
            }*/
            else
            {
                registration.Clear(lblRegistrationSelected.Text);
                Response.Redirect("RegistrationSearchV2.aspx");
            }
        }
        catch(Exception ex)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Une erreur est survenue', '" + HttpUtility.JavaScriptStringEncode(ex.Message) + "', 'RegistrationSearchV2.aspx');", true);
        }
    }

    protected void checkNextStepInfo(registration registr)
    {
        DataTable dtTagInfoList;
        int iNextStep = registr.iCurrentStep + 1;

        switch (iNextStep)
        {
            case 16: //LASTNAME
                if (registration.GetStepInfoList(registr, iNextStep, out dtTagInfoList) && dtTagInfoList.Rows.Count > 0)
                {
                    ddlIDLastNameModificationReason.AppendDataBoundItems = true;
                    ddlIDLastNameModificationReason.Items.Clear();
                    ddlIDLastNameModificationReason.Items.Add(new ListItem("", ""));
                    ddlIDLastNameModificationReason.DataTextField = "Label";
                    ddlIDLastNameModificationReason.DataTextField = "TAG";
                    ddlIDLastNameModificationReason.DataSource = dtTagInfoList;
                    ddlIDLastNameModificationReason.DataBind();
                }
                break;
            case 17: //FIRSTNAME
                if (registration.GetStepInfoList(registr, iNextStep, out dtTagInfoList) && dtTagInfoList.Rows.Count > 0)
                {
                    ddlIDFirstNameModificationReason.AppendDataBoundItems = true;
                    ddlIDFirstNameModificationReason.Items.Clear();
                    ddlIDFirstNameModificationReason.Items.Add(new ListItem("", ""));
                    ddlIDFirstNameModificationReason.DataTextField = "Label";
                    ddlIDFirstNameModificationReason.DataTextField = "TAG";
                    ddlIDFirstNameModificationReason.DataSource = dtTagInfoList;
                    ddlIDFirstNameModificationReason.DataBind();
                }
                break;
        }
    }

    protected void checkNextStep(registration registr)
    {
        try
        {
            int iStep = registr.iCurrentStep;
            Panel panelNextStep = (Panel)upCheckIDDocument.FindControl("panelStep" + iStep.ToString());
            bool bCheckAgain = false;

            switch (iStep)
            {
                case 4: //check recto verso
                    if (ddlIDDocumentType.SelectedValue == "P")
                    {
                        iStep++;
                        panelNextStep.Visible = false;
                        panelNextStep = (Panel)upCheckIDDocument.FindControl("panelStep" + (iStep).ToString());
                        panelNextStep.Visible = true;
                        bCheckAgain = true;
                    }
                    break;
                case 6: //check titre de séjour étudiant algérien
                    if (ddlIDDocumentType.SelectedValue != "T")
                    {
                        iStep++;
                        panelNextStep.Visible = false;
                        panelNextStep = (Panel)upCheckIDDocument.FindControl("panelStep" + (iStep).ToString());
                        panelNextStep.Visible = true;
                        bCheckAgain = true;
                    }
                    break;
                case 9: //check RF document français
                    if (ddlIDDocumentCountry.SelectedValue != "FR")
                    {
                        iStep++;
                        panelNextStep.Visible = false;
                        panelNextStep = (Panel)upCheckIDDocument.FindControl("panelStep" + (iStep).ToString());
                        panelNextStep.Visible = true;
                        bCheckAgain = true;
                    }
                    break;
                case 10: //check puce titre de séjour
                    if (ddlIDDocumentType.SelectedValue != "T")
                    {
                        iStep++;
                        panelNextStep.Visible = false;
                        panelNextStep = (Panel)upCheckIDDocument.FindControl("panelStep" + (iStep).ToString());
                        panelNextStep.Visible = true;
                        bCheckAgain = true;
                    }
                    break;
                case 20: //check birth country
                    iStep++;
                    panelNextStep.Visible = false;
                    panelNextStep = (Panel)upCheckIDDocument.FindControl("panelStep" + (iStep).ToString());
                    panelNextStep.Visible = true;
                    bCheckAgain = true;
                    break;
                case 23: //face capture non applicable pour le moment
                    iStep = 24;
                    panelNextStep.Visible = false;
                    panelNextStep = (Panel)upCheckIDDocument.FindControl("panelStep" + (iStep).ToString());
                    panelNextStep.Visible = true;
                    bCheckAgain = true;
                    break;
            }

            registr.iCurrentStep = iStep;
            lblCheckIDCurrentStep.Text = iStep.ToString();

            if (bCheckAgain) { checkNextStep(registr); }

        }
        catch(Exception ex)
        {

        }
    }

    protected void btnParentIDDocumentChecked_Click(object sender, EventArgs e)
    {
        try
        {
            bool bShowNextStep = false;
            bool bEndCheck = false;
            string JSToCall = "";

            panelCheckParentIDDocumentbuttons.Visible = false;
            registration registr = registration.GetCurrent(false, lblRegistrationSelected.Text);
            int iStep = registr.iCurrentStep;

            registr.bCurrentStepIsAdult = true;

            Panel panelCurrentStep = null;
            Panel panelNextStep = null;

            if (iStep <= 27)
            {
                panelCurrentStep = (Panel)upCheckParentIDDocument.FindControl("panelParentStep" + iStep.ToString());
                panelNextStep = (Panel)upCheckParentIDDocument.FindControl("panelParentStep" + (iStep + 1).ToString());
                panelCheckParentIDDocumentbuttons.Visible = true;

                switch (iStep)
                {
                    case 1:
                        bShowNextStep = registration.CheckRegistrationStep(registr, iStep, ddlParentIDDocumentType.SelectedValue, out bEndCheck);
                        if (bShowNextStep && !bEndCheck) { registr.ParentRegistrationSheet.DocumentType = ddlIDDocumentType.SelectedValue; }
                        break;
                    case 2:
                        bShowNextStep = registration.CheckRegistrationStep(registr, iStep, ddlParentIDDocumentCountry.SelectedValue, out bEndCheck);
                        if (bShowNextStep && !bEndCheck) { registr.ParentRegistrationSheet.DocumentCountry = ddlParentIDDocumentCountry.SelectedValue; }
                        break;
                    case 3:
                        bShowNextStep = registration.CheckRegistrationStep(registr, iStep, ddlParentIDDocumentCopy.SelectedValue, out bEndCheck);
                        break;
                    case 4:
                        bShowNextStep = registration.CheckRegistrationStep(registr, iStep, ddlParentIDDocumentRectoVerso.SelectedValue, out bEndCheck);
                        break;
                    case 5:
                        bShowNextStep = registration.CheckRegistrationStep(registr, iStep, ddlParentIDDocumentQuality.SelectedValue, out bEndCheck);
                        break;
                    case 6:
                        bShowNextStep = registration.CheckRegistrationStep(registr, iStep, ddlParentIDDocumentAlgerianResidencePermit.SelectedValue, out bEndCheck);
                        break;
                    case 7:
                        bShowNextStep = registration.CheckRegistrationStep(registr, iStep, ddlParentIDDocumentMRZ.SelectedValue, out bEndCheck);
                        break;
                    case 8:
                        bShowNextStep = registration.CheckRegistrationStep(registr, iStep, ddlParentIDDocumentHolo.SelectedValue, out bEndCheck);
                        break;
                    case 9:
                        bShowNextStep = registration.CheckRegistrationStep(registr, iStep, ddlParentIDDocumentRF.SelectedValue, out bEndCheck);
                        break;
                    case 10:
                        bShowNextStep = registration.CheckRegistrationStep(registr, iStep, ddlParentIDDocumentPuce.SelectedValue, out bEndCheck);
                        break;
                    case 11:
                        bShowNextStep = registration.CheckRegistrationStep(registr, iStep, ddlParentIDDocumentPolice.SelectedValue, out bEndCheck);
                        break;
                    case 12:
                        bShowNextStep = registration.CheckRegistrationStep(registr, iStep, ddlParentIDDocumentNumberLocation.SelectedValue, out bEndCheck);
                        break;
                    case 13:
                        bShowNextStep = registration.CheckRegistrationStep(registr, iStep, ddlParentIDDocumentPhoto.SelectedValue, out bEndCheck);
                        break;
                    case 14:
                        bShowNextStep = registration.CheckRegistrationStep(registr, iStep, ddlParentIDDocumentColor.SelectedValue, out bEndCheck);
                        break;
                    case 15:
                        bShowNextStep = registration.CheckRegistrationStep(registr, iStep, txtParentDocumentNumber.Text, out bEndCheck);
                        if (!bShowNextStep)
                        {
                            JSToCall = "modifyClientField('#btnModify-txtParentDocumentNumber', '" + lblParentDocumentNumber.ClientID + "','" + txtParentDocumentNumber.ClientID + "');";
                            JSToCall += "AlertMessage('Une erreur est survenue', '" + HttpUtility.JavaScriptStringEncode("Numéro de document non valide") + "');";
                        }
                        break;
                    case 16:
                        List<string> lsLastName = new List<string>();
                        lsLastName.Add(txtParentIDLastName.Text);
                        lsLastName.Add(txtParentIDMarriedLastName.Text);
                        lsLastName.Add(txtParentIDUsageLastName.Text);

                        //bShowNextStep = registration.CheckRegistrationStep(registr, iStep, lsLastName, out bEndCheck);
                        //if (!bShowNextStep)
                        //{
                        //    JSToCall = "modifyClientField('#btnModify-txtParentIDLastName', '" + lblParentIDLastName.ClientID + "','" + txtParentIDLastName.ClientID + "');";
                        //    JSToCall += "AlertMessage('Une erreur est survenue', '" + HttpUtility.JavaScriptStringEncode("Nom(s) non valide") + "');";
                        //}
                        //break;

                        if (lblParentIDLastName.Text.Trim() != txtParentIDLastName.Text.Trim() && registr.liStepInfo.Contains(16))
                            bShowNextStep = registration.CheckRegistrationStep(registr, iStep, lsLastName, ddlParentIDLastNameModificationReason.SelectedValue, out bEndCheck);
                        else { bShowNextStep = registration.CheckRegistrationStep(registr, iStep, lsLastName, out bEndCheck); }

                        if (!bShowNextStep)
                        {
                            JSToCall = "showDialogModificationReason('dialog-ParentIDLastNameModificationReason', '" + ddlIDLastNameModificationReason.ClientID + "','#btnModify-txtParentIDLastName', '" + lblParentIDLastName.ClientID + "','" + txtParentIDLastName.ClientID + "');";
                            if (ddlParentIDLastNameModificationReason.SelectedValue.Trim().Length == 0)
                                JSToCall += "AlertMessage('Une erreur est survenue', '" + HttpUtility.JavaScriptStringEncode("Motif de modification du nom requis") + "');";
                            else { JSToCall += "AlertMessage('Une erreur est survenue', '" + HttpUtility.JavaScriptStringEncode("Nom(s) non valide") + "');"; }
                        }
                        break;

                    case 17:
                        bShowNextStep = registration.CheckRegistrationStep(registr, iStep, txtParentIDFirstName.Text, out bEndCheck);
                        //if (!bShowNextStep)
                        //{
                        //    JSToCall = "modifyClientField('#btnModify-txtParentIDFirstName', '" + lblParentIDFirstName.ClientID + "','" + txtParentIDFirstName.ClientID + "');";
                        //    JSToCall += "AlertMessage('Une erreur est survenue', '" + HttpUtility.JavaScriptStringEncode("Prénom(s) non valide") + "');";
                        //}
                        if (lblParentIDLastName.Text.Trim() != txtParentIDLastName.Text.Trim() && registr.liStepInfo.Contains(16))
                            bShowNextStep = registration.CheckRegistrationStep(registr, iStep, txtParentIDFirstName.Text, ddlParentIDFirstNameModificationReason.SelectedValue, out bEndCheck);
                        else { bShowNextStep = registration.CheckRegistrationStep(registr, iStep, txtParentIDFirstName.Text, out bEndCheck); }

                        if (!bShowNextStep)
                        {
                            JSToCall = "showDialogModificationReason('dialog-ParentIDFirstNameModificationReason', '" + ddlIDFirstNameModificationReason.ClientID + "','#btnModify-txtParentIDFirstName', '" + lblParentIDFirstName.ClientID + "','" + txtParentIDFirstName.ClientID + "');";
                            if (ddlParentIDFirstNameModificationReason.SelectedValue.Trim().Length == 0)
                                JSToCall += "AlertMessage('Une erreur est survenue', '" + HttpUtility.JavaScriptStringEncode("Motif de modification du prénom requis") + "');";
                            else { JSToCall += "AlertMessage('Une erreur est survenue', '" + HttpUtility.JavaScriptStringEncode("Prénom(s) non valide") + "');"; }
                        }
                        break;
                    case 18:
                        bShowNextStep = registration.CheckRegistrationStep(registr, iStep, txtParentBirthDate.Text, out bEndCheck);
                        if (!bShowNextStep)
                        {
                            JSToCall = "modifyClientField('#btnModify-txtParentBirthDate', '" + lblParentBirthDate.ClientID + "','" + txtParentBirthDate.ClientID + "');";
                            JSToCall += "AlertMessage('Une erreur est survenue', '" + HttpUtility.JavaScriptStringEncode("Date de naissance non valide") + "');";
                        }
                        break;
                    case 19:
                        List<string> lsBirthPlace = new List<string>();
                        lsBirthPlace.Add(ddlParentBirthPlaceCountry.SelectedValue);
                        lsBirthPlace.Add(txtParentBirthPlaceCity.Text);
                        if (registration.CheckBirthDepartementNeeded(ddlParentBirthPlaceCountry.SelectedValue))
                        {
                            lsBirthPlace.Add(txtParentBirthPlaceDepartment.Text);
                        }
                        else { lsBirthPlace.Add(""); }

                        bShowNextStep = registration.CheckRegistrationStep(registr, iStep, lsBirthPlace, out bEndCheck);
                        if (!bShowNextStep)
                        {
                            JSToCall = "modifyBirthPlace('divCheckParentBirthPlace', 'divModifyParentBirthPlace');";
                            JSToCall += "AlertMessage('Une erreur est survenue', '" + HttpUtility.JavaScriptStringEncode("Lieu de naissance non valide") + "');";
                        }
                        break;
                    case 20:
                        //bShowNextStep = registration.CheckRegistrationStep(registr, iStep, ddlParentBirthCountry.SelectedValue, out bEndCheck);
                        //check birth country in step 19
                        break;
                    case 21:
                        bShowNextStep = registration.CheckRegistrationStep(registr, iStep, rblParentGender.SelectedValue, out bEndCheck);
                        if (!bShowNextStep)
                        {
                            JSToCall = "modifyClientField('#btnModify-txtParentGender', '" + lblParentGender.ClientID + "','" + rblParentGender.ClientID + "');";
                            JSToCall += "AlertMessage('Une erreur est survenue', '" + HttpUtility.JavaScriptStringEncode("Civilité non valide") + "');";
                        }
                        break;
                    case 22:
                        bShowNextStep = registration.CheckRegistrationStep(registr, iStep, txtParentExpirationDate.Text, out bEndCheck);
                        if (!bShowNextStep)
                        {
                            JSToCall = "modifyClientField('#btnModify-txtParentExpirationDate', '" + lblParentExpirationDate.ClientID + "','" + txtParentExpirationDate.ClientID + "');";
                            JSToCall += "AlertMessage('Une erreur est survenue', '" + HttpUtility.JavaScriptStringEncode("Date d'expiration non valide") + "');";
                        }
                        break;
                    case 23:
                        //Capture faciale
                        //bShowNextStep = true;
                        break;
                    case 24:
                        bShowNextStep = registration.CheckRegistrationStep(registr, iStep, ddlParentIDDocumentSignature.SelectedValue, out bEndCheck);
                        break;
                    case 25:
                        registr.sParentalAuthorityProofDocumentType = ddlFamilyBookType.SelectedValue;
                        bShowNextStep = registration.CheckRegistrationStep(registr, iStep, ddlFamilyBookType.SelectedValue, out bEndCheck);
                        //if (bShowNextStep)
                        //{
                        //lblFamilyBookType_Quality.Text = "<b>" + ((ddlFamilyBookType.SelectedValue == "BA") ? "l'" : "le ").ToString() + ddlFamilyBookType.SelectedItem.Text.ToLower() + "</b>";
                        //lblFamilyBookType_Copy.Text = "<b>" + ((ddlFamilyBookType.SelectedValue == "BA") ? "de l'" : "du ").ToString() + ddlFamilyBookType.SelectedItem.Text.ToLower() + "</b>";
                        //}
                        break;
                    case 26:
                        bShowNextStep = registration.CheckRegistrationStep(registr, iStep, ddlFamilyBookCopy.SelectedValue, out bEndCheck);
                        break;
                    case 27:
                        bShowNextStep = registration.CheckRegistrationStep(registr, iStep, ddlFamilyBookQuality.SelectedValue, out bEndCheck);
                        break;
                }

                if (bEndCheck)
                {
                    //registration.Clear(lblRegistrationSelected.Text);
                    //Response.Redirect("RegistrationSearchV2.aspx");
                    lblConfirmEndCheck.Text = "Confirmez-vous la conformité de la souscription ?";
                    if (!registr.ComplianceToApply.isOK)
                    {
                        if (!string.IsNullOrEmpty(registr.ComplianceToApply.TAG.Trim()) && !string.IsNullOrEmpty(registr.ComplianceToApply.Label.Trim()))
                        {
                            lblConfirmEndCheck.Text = "Confirmez-vous la non conformité de la souscription ?<br/><br/>";
                            lblConfirmEndCheck.Text += "Raison : <b>" + HttpUtility.JavaScriptStringEncode(registr.ComplianceToApply.Label) + " (" + HttpUtility.JavaScriptStringEncode(registr.ComplianceToApply.TAG.Replace("_", " ").Trim()) + ") </b>";
                        }
                        else
                        {
                            lblConfirmEndCheck.Text = "Confirmez-vous la non conformité de la souscription ?<br/><br/>";
                            lblConfirmEndCheck.Text += "<span style=\"font-style:italic; font-weight:bold\">AUCUNE COMPLIANCE A APPLIQUER</span>";
                            //throw new Exception("NULL or EMPTY Compliance");
                        }
                    }
                    upConfirmEndCheck.Update();
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "ShowConfirmEndCheck();", true);
                }
                else if (bShowNextStep)
                {
                    panelCheckID.Visible = false;
                    panelCheckIDNotAvailable.Visible = true;
                    panelCheckIDParent.Visible = true;
                    panelCheckIDParentNotAvailable.Visible = false;

                    if (iStep != 27)
                    {
                        panelCurrentStep.Visible = false;
                        panelNextStep.Visible = true;
                        iStep++;
                        registr.iCurrentStep = iStep;
                        checkParentNextStep(registr);
                    }
                    else
                    {

                    }
                }

                lblCheckParentIDCurrentStep.Text = registr.iCurrentStep.ToString();
            }
            /*else if (iStep == 24)
            {
                panelCheckIDDocumentbuttons.Visible = true;
                if (registration.CheckRegistrationStep(registr, iStep, ddlIDDocumentSignature.SelectedValue, out bEndCheck) || bEndCheck)
                {
                    Response.Redirect("RegistrationSearchV2.aspx");
                }
            }*/
            else
            {
                registration.Clear(lblRegistrationSelected.Text);
                Response.Redirect("RegistrationSearchV2.aspx");
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Une erreur est survenue', '" + HttpUtility.JavaScriptStringEncode(ex.Message) + "', 'RegistrationSearchV2.aspx');", true);
        }
    }
    protected void checkParentNextStep(registration registr)
    {
        try
        {
            int iStep = registr.iCurrentStep;
            Panel panelNextStep = (Panel)upCheckParentIDDocument.FindControl("panelParentStep" + iStep.ToString());
            bool bCheckAgain = false;

            switch (iStep)
            {
                case 4: //check recto verso
                    if (ddlParentIDDocumentType.SelectedValue == "P")
                    {
                        iStep++;
                        panelNextStep.Visible = false;
                        panelNextStep = (Panel)upCheckParentIDDocument.FindControl("panelParentStep" + (iStep).ToString());
                        panelNextStep.Visible = true;
                        bCheckAgain = true;
                    }
                    break;
                case 6: //check titre de séjour étudiant algérien
                    if (ddlParentIDDocumentType.SelectedValue != "T")
                    {
                        iStep++;
                        panelNextStep.Visible = false;
                        panelNextStep = (Panel)upCheckParentIDDocument.FindControl("panelParentStep" + (iStep).ToString());
                        panelNextStep.Visible = true;
                        bCheckAgain = true;
                    }
                    break;
                case 9: //check RF document français
                    if (ddlParentIDDocumentCountry.SelectedValue != "FR")
                    {
                        iStep++;
                        panelNextStep.Visible = false;
                        panelNextStep = (Panel)upCheckParentIDDocument.FindControl("panelParentStep" + (iStep).ToString());
                        panelNextStep.Visible = true;
                        bCheckAgain = true;
                    }
                    break;
                case 10: //check puce titre de séjour
                    if (ddlParentIDDocumentType.SelectedValue != "T")
                    {
                        iStep++;
                        panelNextStep.Visible = false;
                        panelNextStep = (Panel)upCheckParentIDDocument.FindControl("panelParentStep" + (iStep).ToString());
                        panelNextStep.Visible = true;
                        bCheckAgain = true;
                    }
                    break;
                case 20: //check birth country
                    iStep++;
                    panelNextStep.Visible = false;
                    panelNextStep = (Panel)upCheckParentIDDocument.FindControl("panelParentStep" + (iStep).ToString());
                    panelNextStep.Visible = true;
                    bCheckAgain = true;
                    break;
                case 23: //face capture non applicable pour le moment
                    iStep = 24;
                    panelNextStep.Visible = false;
                    panelNextStep = (Panel)upCheckParentIDDocument.FindControl("panelParentStep" + (iStep).ToString());
                    panelNextStep.Visible = true;
                    bCheckAgain = true;
                    break;
                case 25: //
                    break;
                case 26: //check copy only for family book
                    if(ddlFamilyBookType.SelectedValue != "FB")
                    {
                        iStep++;
                        panelNextStep.Visible = false;
                        panelNextStep = (Panel)upCheckParentIDDocument.FindControl("panelParentStep" + (iStep).ToString());
                        panelNextStep.Visible = true;
                        bCheckAgain = true;
                    }
                    break;
            }

            registr.iCurrentStep = iStep;

            if (bCheckAgain) { checkParentNextStep(registr); }

        }
        catch (Exception ex)
        {

        }
    }


    protected void btnCheckIDPreviousStep_Click(object sender, EventArgs e)
    {
        try
        {

            panelCheckIDDocumentbuttons.Visible = false;
            registration registr = registration.GetCurrent(false, lblRegistrationSelected.Text);
            int iStep = registr.iCurrentStep;

            Panel panelCurrentStep = null;
            Panel panelPreviousStep = null;

            if (iStep > 1)
            {
                registr.iCurrentStep = iStep-1;
                panelCurrentStep = (Panel)upCheckIDDocument.FindControl("panelStep" + iStep.ToString());
                panelPreviousStep = (Panel)upCheckIDDocument.FindControl("panelStep" + (iStep - 1).ToString());
                panelCheckIDDocumentbuttons.Visible = true;
                panelCurrentStep.Visible = false;
                panelPreviousStep.Visible = true;

                checkPreviousStep(registr);

                lblCheckIDCurrentStep.Text = registr.iCurrentStep.ToString();
            }
        }
        catch (Exception ex)
        {

        }
    }
    protected void checkPreviousStep(registration registr)
    {
        try
        {
            int iStep = registr.iCurrentStep--;
            Panel panelPreviousStep = (Panel)upCheckIDDocument.FindControl("panelStep" + iStep.ToString());
            bool bCheckAgain = false;

            switch (iStep)
            {
                case 4: //check recto verso
                    if (ddlIDDocumentType.SelectedValue == "P")
                    {
                        iStep--;
                        panelPreviousStep.Visible = false;
                        panelPreviousStep = (Panel)upCheckIDDocument.FindControl("panelStep" + (iStep).ToString());
                        panelPreviousStep.Visible = true;
                        bCheckAgain = true;
                    }
                    break;
                case 6: //check titre de séjour étudiant algérien
                    if (ddlIDDocumentType.SelectedValue != "T")
                    {
                        iStep--;
                        panelPreviousStep.Visible = false;
                        panelPreviousStep = (Panel)upCheckIDDocument.FindControl("panelStep" + (iStep).ToString());
                        panelPreviousStep.Visible = true;
                        bCheckAgain = true;
                    }
                    break;
                case 9: //check RF document français
                    if (ddlIDDocumentCountry.SelectedValue != "FR")
                    {
                        iStep--;
                        panelPreviousStep.Visible = false;
                        panelPreviousStep = (Panel)upCheckIDDocument.FindControl("panelStep" + (iStep).ToString());
                        panelPreviousStep.Visible = true;
                        bCheckAgain = true;
                    }
                    break;
                case 10: //check puce titre de séjour
                    if (ddlIDDocumentType.SelectedValue != "T")
                    {
                        iStep--;
                        panelPreviousStep.Visible = false;
                        panelPreviousStep = (Panel)upCheckIDDocument.FindControl("panelStep" + (iStep).ToString());
                        panelPreviousStep.Visible = true;
                        bCheckAgain = true;
                    }
                    break;
                case 20: //check birth country
                    iStep--;
                    panelPreviousStep.Visible = false;
                    panelPreviousStep = (Panel)upCheckParentIDDocument.FindControl("panelStep" + (iStep).ToString());
                    panelPreviousStep.Visible = true;
                    bCheckAgain = true;
                    break;
                case 23: //face capture non applicable pour le moment
                    iStep = 22;
                    panelPreviousStep.Visible = false;
                    panelPreviousStep = (Panel)upCheckIDDocument.FindControl("panelStep" + (iStep).ToString());
                    panelPreviousStep.Visible = true;
                    bCheckAgain = true;
                    break;
            }

            registr.iCurrentStep = iStep;
            lblCheckIDCurrentStep.Text = iStep.ToString();

            if (iStep > 1) { btnCheckIDPreviousStep.Visible = true; }
            else
            {
                btnCheckIDPreviousStep.Visible = false;
            }

            if (bCheckAgain) { checkPreviousStep(registr); }

        }
        catch (Exception ex)
        {

        }
    }

    protected void btnCheckParentIDPreviousStep_Click(object sender, EventArgs e)
    {
        try
        {
            panelCheckParentIDDocumentbuttons.Visible = false;
            registration registr = registration.GetCurrent(false, lblRegistrationSelected.Text);
            int iStep = registr.iCurrentStep;

            Panel panelCurrentStep = null;
            Panel panelPreviousStep = null;
            registr.iCurrentStep = iStep - 1;

            if (iStep == 1)
            {
                checkParentPreviousStep(registr);
                lblCheckParentIDCurrentStep.Text = registr.iCurrentStep.ToString();
            }
            else if (iStep > 1)
            {
                panelCurrentStep = (Panel)upCheckParentIDDocument.FindControl("panelParentStep" + iStep.ToString());
                panelPreviousStep = (Panel)upCheckParentIDDocument.FindControl("panelParentStep" + (iStep - 1).ToString());
                panelCheckParentIDDocumentbuttons.Visible = true;
                panelCurrentStep.Visible = false;
                panelPreviousStep.Visible = true;

                checkParentPreviousStep(registr);
                lblCheckParentIDCurrentStep.Text = registr.iCurrentStep.ToString();
            }
        }
        catch (Exception ex)
        {

        }
    }
    protected void checkParentPreviousStep(registration registr)
    {
        try
        {
            int iStep = registr.iCurrentStep--;
            Panel panelPreviousStep = (Panel)upCheckParentIDDocument.FindControl("panelParentStep" + iStep.ToString());
            bool bCheckAgain = false;

            switch (iStep)
            {
                case 4: //check recto verso
                    if (ddlParentIDDocumentType.SelectedValue == "P")
                    {
                        iStep--;
                        panelPreviousStep.Visible = false;
                        panelPreviousStep = (Panel)upCheckParentIDDocument.FindControl("panelParentStep" + (iStep).ToString());
                        
                        bCheckAgain = true;
                    }
                    break;
                case 6: //check titre de séjour étudiant algérien
                    if (ddlParentIDDocumentType.SelectedValue != "T")
                    {
                        iStep--;
                        panelPreviousStep.Visible = false;
                        panelPreviousStep = (Panel)upCheckParentIDDocument.FindControl("panelParentStep" + (iStep).ToString());
                        bCheckAgain = true;
                    }
                    break;
                case 9: //check RF document français
                    if (ddlParentIDDocumentCountry.SelectedValue != "FR")
                    {
                        iStep--;
                        panelPreviousStep.Visible = false;
                        panelPreviousStep = (Panel)upCheckParentIDDocument.FindControl("panelParentStep" + (iStep).ToString());
                        bCheckAgain = true;
                    }
                    break;
                case 10: //check puce titre de séjour
                    if (ddlParentIDDocumentType.SelectedValue != "T")
                    {
                        iStep--;
                        panelPreviousStep.Visible = false;
                        panelPreviousStep = (Panel)upCheckParentIDDocument.FindControl("panelParentStep" + (iStep).ToString());
                        bCheckAgain = true;
                    }
                    break;
                case 20://check birth country
                    iStep--;
                    panelPreviousStep.Visible = false;
                    panelPreviousStep = (Panel)upCheckParentIDDocument.FindControl("panelParentStep" + (iStep).ToString());
                    bCheckAgain = true;
                    break;
                case 23: //face capture non applicable pour le moment
                    iStep = 22;
                    panelPreviousStep.Visible = false;
                    panelPreviousStep = (Panel)upCheckParentIDDocument.FindControl("panelParentStep" + (iStep).ToString());
                    bCheckAgain = true;
                    break;
                case 24:
                    if (registr.ClientSheet.isParentHasAccount) {
                        panelPreviousStep.Visible = false;
                        iStep = 0;
                    }
                    break;
                case 25: //
                    break;
                case 26: //check copy only for family book
                    if (ddlFamilyBookType.SelectedValue != "FB")
                    {
                        iStep--;
                        panelPreviousStep = (Panel)upCheckParentIDDocument.FindControl("panelParentStep" + (iStep).ToString());
                        bCheckAgain = true;
                    }
                    break;
            }

            registr.iCurrentStep = iStep;
            lblCheckParentIDCurrentStep.Text = iStep.ToString();

            if(iStep > 0)
            {
                panelPreviousStep.Visible = true;
                btnCheckParentIDPreviousStep.Visible = true;
            }
            else
            {
                btnCheckParentIDPreviousStep.Visible = false;

                registr.bCurrentStepIsAdult = false;
                registr.bCheckParentId = true;
                registr.bMinorAccount = true;
                registr.iCurrentStep = 24;
                lblCheckIDCurrentStep.Text = "24";
                panelCheckIDParent.Visible = false;
                panelCheckIDParentNotAvailable.Visible = true;
                panelCheckIDNotAvailable.Visible = false;
                panelCheckID.Visible = true;

                panelPreviousStep = (Panel)upCheckParentIDDocument.FindControl("panelStep24");
                panelPreviousStep.Visible = true;
                //upCheckAllIDDocument.Update();
                upCheckIDDocument.Update();
                upCheckParentIDDocument.Update();
            }

            if (bCheckAgain) { checkParentPreviousStep(registr); }

        }
        catch (Exception ex)
        {

        }
    }

    protected void btnConfirmEndCheck_Click(object sender, EventArgs e)
    {
        registration registr = registration.GetCurrent(true, lblRegistrationSelected.Text);
        string sXmlOUT = "";

        try
        {
            if (!string.IsNullOrEmpty(registr.ComplianceToApply.sXML))
            {
                if (registration.ValidateEndCheckStep(registr, out sXmlOUT))
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Contrôle souscription', 'Souscription traitée avec succès', 'RegistrationSearchV2.aspx');", true);
                }
                else { throw new Exception("End Check Impossible : "+ HttpUtility.JavaScriptStringEncode(sXmlOUT)); }
            }
            else { throw new Exception("End Check Impossible : NULL XML"); }
        }
        catch(Exception ex)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Une erreur est survenue', '" + HttpUtility.JavaScriptStringEncode(ex.Message) + "', 'RegistrationSearchV2.aspx');", true);
        }
        
    }
}