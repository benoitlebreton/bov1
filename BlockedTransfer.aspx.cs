﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using XMLMethodLibrary;

public partial class BlockedTransfer : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SetBlockedTransfer();
        }
    }

    protected void SetBlockedTransfer()
    {
        

        authentication auth = authentication.GetCurrent();
        BlockedTransfer1.isFilterVisible = true;
        BlockedTransfer1.RefCustomer = -1;
        BlockedTransfer1.iPage = 1;
        BlockedTransfer1.iPageSize = 10;
        BlockedTransfer1.isClientDetailsLink = true;
        if (Request.Params["Reserved"] != null && Request.Params["Reserved"].ToString() == "Y")
            BlockedTransfer1.sStatus = "RESERVED";
        BlockedTransfer1.Init();
    }
}