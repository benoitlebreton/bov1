﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Text;
using System.Xml.Linq;
using XMLMethodLibrary;
using Newtonsoft.Json.Linq;

public partial class CreOrderNew : System.Web.UI.Page
{
    private List<string> _sListAuthorizedFeature
    {
        get { return (ViewState["ListAuthorizedFeature"] != null) ? ViewState["ListAuthorizedFeature"] as List<string> : null; }
        set { ViewState["ListAuthorizedFeature"] = value; }
    }

    private List<string> _sListAuthorizedCre
    {
        get { return (ViewState["ListAuthorizedCre"] != null) ? ViewState["ListAuthorizedCre"] as List<string> : null; }
        set { ViewState["ListAuthorizedCre"] = value; }
    }

    private string _sCurrentTagFeature
    {
        get { return (ViewState["CurrentTagFeature"] != null) ? ViewState["CurrentTagFeature"].ToString() : ""; }
        set { ViewState["CurrentTagFeature"] = value; }
    }

    private string _sRedirectUrl
    {
        get { return (ViewState["RedirectUrl"] != null) ? ViewState["RedirectUrl"].ToString() : null; }
        set { ViewState["RedirectUrl"] = value; }
    }

    private string _sJsonVal4Cre
    {
        get { return (ViewState["JsonVal4Cre"] != null) ? ViewState["JsonVal4Cre"].ToString() : null; }
        set { ViewState["JsonVal4Cre"] = value; }
    }

    private CRETools.CREValues _CreValues
    {
        get { return (ViewState["CREValues"] != null) ? ViewState["CREValues"] as CRETools.CREValues : null; }
        set { ViewState["CREValues"] = value; }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        clientSearch1.ClientFound += new EventHandler(clientFound);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!CRETools.CheckRights(authentication.GetCurrent().sToken))
            {
                Response.Redirect("Default.aspx");
            }
            else
            {
                txtDate1.Attributes.Add("readonly", "true");
                txtDate2.Attributes.Add("readonly", "true");
                //initTypeOperations();
                initFeatureDdl();
                clientFileUpload1.ClientFileUploaded += new EventHandler(fileUploaded);

                if (Request.Params["cretreatment"] != null)
                    InitPageFromParams(Request.Params["cretreatment"]);
                //debut test
                /*if (Request.Params["data"] != null)
                {
                    CRETools.CREValues creValuesTest = new CRETools.CREValues();
                    creValuesTest.sOperationTAG = "_REMB_CARTE_CHROME_";
                    creValuesTest.mMontant1 = "30,00";
                    creValuesTest.sCompte1 = "00001380001";
                    creValuesTest.sRedirect = "BoCobalt_OpDiverses.aspx";
                    InitPageFromParams(CRETools.GetJWTFromCREValues(creValuesTest));
                }*/
                // fin test
            }
        }
    }

    protected bool CheckRight()
    {
        bool bResult = false;
        string sXmlOut = tools.GetRights(authentication.GetCurrent().sToken, "SC_Menu");

        List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Page", "RC");

        if (listRC.Count > 0 && listRC[0] == "0")
        {
            List<string> listAction = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/Page/Action");

            for (int i = 0; i < listAction.Count; i++)
            {
                List<string> listTagAction = CommonMethod.GetAttributeValues(listAction[i], "Action", "TagAction");
                List<string> listViewAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ViewAllowed");
                List<string> listActionAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ActionAllowed");

                if (listTagAction.Count > 0 && listViewAllowed.Count > 0 && listViewAllowed[0] == "1")
                {
                    switch (listTagAction[0])
                    {
                        case "BoCobalt":
                            bResult = true;
                            break;
                    }
                }
            }
        }
        return bResult;
    }

    protected bool bGetFeatureList()
    {
        bool bResultat = false;
        /*_sListAuthorizedCre = "<ALL_XML_OUT>"
            + "<UserCREFeaturesOrder RC=\"0\" />"
            + "<CREFeaturesOrder>"
            + "<CREFeature>"
            + "<Feature iRefFeature=\"1\" sTAGFeature=\"_EXPLOIT_CHARENTON_\" sLabelFeature=\"Groupe Exploitation Charenton\" sDescriptionFeature=\"Opérations réalisées par l'ensemble de l'exploitation à Charenton\" />"
            + "<CRE sOperationTAG=\"_FRAIS_REJET_PRELEVEMENTT_ABUSIF_\" sCodeOperation=\"+FD\" sOperationDescription=\"Prise de Frais sur Rejet de Prelevement Abusif\" bCREValidationRequested=\"1\" sValidationGroup=\"Finance\" iValidationRefUser=\"0\" sValidationUserName=\"\" sValidationUserFirstName=\"\" sValidationUserMail=\"\" />"
            + "<CRE sOperationTAG=\"_FRAIS_ATD_\" sCodeOperation=\"+AT\" sOperationDescription=\"Prise de Frais sur Avis à Tiers Détenteur (ATD)\" bCREValidationRequested=\"1\" sValidationGroup=\"Finance\" iValidationRefUser=\"0\" sValidationUserName=\"\" sValidationUserFirstName=\"\" sValidationUserMail=\"\" />"
            + "<CRE sOperationTAG=\"_COTISATION_CARTE_CHROME_\" sCodeOperation=\"+CP\" sOperationDescription=\"Cotisation Carte Nickel Chrome\" bCREValidationRequested=\"1\" sValidationGroup=\"Finance\" iValidationRefUser=\"0\" sValidationUserName=\"\" sValidationUserFirstName=\"\" sValidationUserMail=\"\" />"
            + "<CRE sOperationTAG=\"_FRAIS_RECHERCHE_\" sCodeOperation=\"+FR\" sOperationDescription=\"Prise de Frais de Recherche\" bCREValidationRequested=\"1\" sValidationGroup=\"Finance\" iValidationRefUser=\"0\" sValidationUserName=\"\" sValidationUserFirstName=\"\" sValidationUserMail=\"\" />"
            + "<CRE sOperationTAG=\"_REMB_FRAIS_CLIENT_CARTE_PERSO_NON_RECUE_\" sCodeOperation=\"+RB\" sOperationDescription=\"Remboursement Frais Client - Carte Personnalisée - Carte Non Recue\" bCREValidationRequested=\"1\" sValidationGroup=\"Finance\" iValidationRefUser=\"0\" sValidationUserName=\"\" sValidationUserFirstName=\"\" sValidationUserMail=\"\" />"
            + "<CRE sOperationTAG=\"_REMB_FRAIS_CLIENT_CARTE_PERSO_GEST_CO_\" sCodeOperation=\"+RB\" sOperationDescription=\"Remboursement Frais Client - Carte Personnalisée - Geste Commercial\" bCREValidationRequested=\"1\" sValidationGroup=\"Finance\" iValidationRefUser=\"0\" sValidationUserName=\"\" sValidationUserFirstName=\"\" sValidationUserMail=\"\" />"
            + "<CRE sOperationTAG=\"_SAISIE_ATTRIBUTION_\" sCodeOperation=\"+RT\" sOperationDescription=\"Saisie Attribution\" bCREValidationRequested=\"1\" sValidationGroup=\"Finance\" iValidationRefUser=\"0\" sValidationUserName=\"\" sValidationUserFirstName=\"\" sValidationUserMail=\"\" />"
            + "<CRE sOperationTAG=\"_COTISATION_ANNUELLE_\" sCodeOperation=\"+CA\" sOperationDescription=\"Cotisation Annuelle\" bCREValidationRequested=\"1\" sValidationGroup=\"Finance\" iValidationRefUser=\"0\" sValidationUserName=\"\" sValidationUserFirstName=\"\" sValidationUserMail=\"\" />"
            + "<CRE sOperationTAG=\"_COTISATION_ANNUELLE_PARTIELLE_1_\" sCodeOperation=\"+CA\" sOperationDescription=\"Première prise de Cotisation Annuelle Partielle\" bCREValidationRequested=\"1\" sValidationGroup=\"Finance\" iValidationRefUser=\"0\" sValidationUserName=\"\" sValidationUserFirstName=\"\" sValidationUserMail=\"\" />"
            + "<CRE sOperationTAG=\"_COTISATION_ANNUELLE_PARTIELLE_2_\" sCodeOperation=\"+CA\" sOperationDescription=\"Suite de prise de Cotisation Annuelle Partielle\" bCREValidationRequested=\"1\" sValidationGroup=\"Finance\" iValidationRefUser=\"0\" sValidationUserName=\"\" sValidationUserFirstName=\"\" sValidationUserMail=\"\" />"
            + "<CRE sOperationTAG=\"_FRAIS_COMPTE_A_CLOTURER_\" sCodeOperation=\"+CL\" sOperationDescription=\"Frais Compte à Cloturer (CACF)\" bCREValidationRequested=\"1\" sValidationGroup=\"Finance\" iValidationRefUser=\"0\" sValidationUserName=\"\" sValidationUserFirstName=\"\" sValidationUserMail=\"\" />"
            + "<CRE sOperationTAG=\"_COM_REMPLACEMENT_CARTE_\" sCodeOperation=\"+RZ\" sOperationDescription=\"Commission Buraliste sur Remplacement Carte\" bCREValidationRequested=\"1\" sValidationGroup=\"Finance\" iValidationRefUser=\"0\" sValidationUserName=\"\" sValidationUserFirstName=\"\" sValidationUserMail=\"\" />"
            + "<CRE sOperationTAG=\"_COM_OUVERTURE_COMPTE_\" sCodeOperation=\"+DZ\" sOperationDescription=\"Commission Buraliste sur Ouverture de Compte\" bCREValidationRequested=\"1\" sValidationGroup=\"Finance\" iValidationRefUser=\"0\" sValidationUserName=\"\" sValidationUserFirstName=\"\" sValidationUserMail=\"\" />"
            + "</CREFeature>"
            + "<CREFeature>"
            + "<Feature iRefFeature=\"2\" sTAGFeature=\"_EXPLOIT_CHARENTON_JAN_\" sLabelFeature=\"Exploitation Charenton - Jan\" sDescriptionFeature=\"Opérations réalisées par Jan de l'exploitation à Charenton\" />"
            + "<CRE sOperationTAG=\"_FRAIS_REJET_PRELEVEMENTT_ABUSIF_\" sCodeOperation=\"+FD\" sOperationDescription=\"Prise de Frais sur Rejet de Prelevement Abusif\" bCREValidationRequested=\"1\" sValidationGroup=\"\" iValidationRefUser=\"102\" sValidationUserName=\"WALLEZ\" sValidationUserFirstName=\"YANN PHILIPPE\" sValidationUserMail=\"yann.wallez@compte-nickel.fr\" />"
            + "<CRE sOperationTAG=\"_FRAIS_ATD_\" sCodeOperation=\"+AT\" sOperationDescription=\"Prise de Frais sur Avis à Tiers Détenteur (ATD)\" bCREValidationRequested=\"1\" sValidationGroup=\"\" iValidationRefUser=\"102\" sValidationUserName=\"WALLEZ\" sValidationUserFirstName=\"YANN PHILIPPE\" sValidationUserMail=\"yann.wallez@compte-nickel.fr\" />"
            + "<CRE sOperationTAG=\"_COTISATION_CARTE_CHROME_\" sCodeOperation=\"+CP\" sOperationDescription=\"Cotisation Carte Nickel Chrome\" bCREValidationRequested=\"1\" sValidationGroup=\"\" iValidationRefUser=\"102\" sValidationUserName=\"WALLEZ\" sValidationUserFirstName=\"YANN PHILIPPE\" sValidationUserMail=\"yann.wallez@compte-nickel.fr\" />"
            + "<CRE sOperationTAG=\"_FRAIS_RECHERCHE_\" sCodeOperation=\"+FR\" sOperationDescription=\"Prise de Frais de Recherche\" bCREValidationRequested=\"1\" sValidationGroup=\"\" iValidationRefUser=\"102\" sValidationUserName=\"WALLEZ\" sValidationUserFirstName=\"YANN PHILIPPE\" sValidationUserMail=\"yann.wallez@compte-nickel.fr\" />"
            + "<CRE sOperationTAG=\"_REMB_FRAIS_CLIENT_CARTE_PERSO_NON_RECUE_\" sCodeOperation=\"+RB\" sOperationDescription=\"Remboursement Frais Client - Carte Personnalisée - Carte Non Recue\" bCREValidationRequested=\"1\" sValidationGroup=\"\" iValidationRefUser=\"102\" sValidationUserName=\"WALLEZ\" sValidationUserFirstName=\"YANN PHILIPPE\" sValidationUserMail=\"yann.wallez@compte-nickel.fr\" />"
            + "<CRE sOperationTAG=\"_REMB_FRAIS_CLIENT_CARTE_PERSO_GEST_CO_\" sCodeOperation=\"+RB\" sOperationDescription=\"Remboursement Frais Client - Carte Personnalisée - Geste Commercial\" bCREValidationRequested=\"1\" sValidationGroup=\"\" iValidationRefUser=\"102\" sValidationUserName=\"WALLEZ\" sValidationUserFirstName=\"YANN PHILIPPE\" sValidationUserMail=\"yann.wallez@compte-nickel.fr\" />"
            + "<CRE sOperationTAG=\"_SAISIE_ATTRIBUTION_\" sCodeOperation=\"+RT\" sOperationDescription=\"Saisie Attribution\" bCREValidationRequested=\"1\" sValidationGroup=\"\" iValidationRefUser=\"102\" sValidationUserName=\"WALLEZ\" sValidationUserFirstName=\"YANN PHILIPPE\" sValidationUserMail=\"yann.wallez@compte-nickel.fr\" />"
            + "<CRE sOperationTAG=\"_COTISATION_ANNUELLE_\" sCodeOperation=\"+CA\" sOperationDescription=\"Cotisation Annuelle\" bCREValidationRequested=\"1\" sValidationGroup=\"\" iValidationRefUser=\"102\" sValidationUserName=\"WALLEZ\" sValidationUserFirstName=\"YANN PHILIPPE\" sValidationUserMail=\"yann.wallez@compte-nickel.fr\" />"
            + "<CRE sOperationTAG=\"_COTISATION_ANNUELLE_PARTIELLE_1_\" sCodeOperation=\"+CA\" sOperationDescription=\"Première prise de Cotisation Annuelle Partielle\" bCREValidationRequested=\"1\" sValidationGroup=\"\" iValidationRefUser=\"102\" sValidationUserName=\"WALLEZ\" sValidationUserFirstName=\"YANN PHILIPPE\" sValidationUserMail=\"yann.wallez@compte-nickel.fr\" />"
            + "<CRE sOperationTAG=\"_COTISATION_ANNUELLE_PARTIELLE_2_\" sCodeOperation=\"+CA\" sOperationDescription=\"Suite de prise de Cotisation Annuelle Partielle\" bCREValidationRequested=\"1\" sValidationGroup=\"\" iValidationRefUser=\"102\" sValidationUserName=\"WALLEZ\" sValidationUserFirstName=\"YANN PHILIPPE\" sValidationUserMail=\"yann.wallez@compte-nickel.fr\" />"
            + "<CRE sOperationTAG=\"_FRAIS_COMPTE_A_CLOTURER_\" sCodeOperation=\"+CL\" sOperationDescription=\"Frais Compte à Cloturer (CACF)\" bCREValidationRequested=\"1\" sValidationGroup=\"\" iValidationRefUser=\"102\" sValidationUserName=\"WALLEZ\" sValidationUserFirstName=\"YANN PHILIPPE\" sValidationUserMail=\"yann.wallez@compte-nickel.fr\" />"
            + "<CRE sOperationTAG=\"_COM_REMPLACEMENT_CARTE_\" sCodeOperation=\"+RZ\" sOperationDescription=\"Commission Buraliste sur Remplacement Carte\" bCREValidationRequested=\"1\" sValidationGroup=\"\" iValidationRefUser=\"102\" sValidationUserName=\"WALLEZ\" sValidationUserFirstName=\"YANN PHILIPPE\" sValidationUserMail=\"yann.wallez@compte-nickel.fr\" />"
            + "<CRE sOperationTAG=\"_COM_OUVERTURE_COMPTE_\" sCodeOperation=\"+DZ\" sOperationDescription=\"Commission Buraliste sur Ouverture de Compte\" bCREValidationRequested=\"1\" sValidationGroup=\"\" iValidationRefUser=\"102\" sValidationUserName=\"WALLEZ\" sValidationUserFirstName=\"YANN PHILIPPE\" sValidationUserMail=\"yann.wallez@compte-nickel.fr\" />"
            + "</CREFeature>"
            + "</CREFeaturesOrder>"
            + "</ALL_XML_OUT>";*/
        SqlConnection conn = null;
        string sErrorMessage = "";
        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRE"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("[CRE].[P_GetUserCREFeaturesAutorizationsV3]", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@sXmlIn", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@sXmlOut", SqlDbType.VarChar, -1);

            cmd.Parameters["@sXmlIn"].Value = new XElement("ALL_XML_IN",
                new XElement("UserCREFeaturesAutorizations"
                    , new XAttribute("iRefUser", authentication.GetCurrent().sRef)
                    , new XAttribute("TOKEN", authentication.GetCurrent().sToken))).ToString();
            cmd.Parameters["@sXmlOut"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@sXmlOut"].Value.ToString();

            if (sXmlOut.Length > 0)
            {
                //_sListAuthorizedCre = "<ALL_XML_OUT><UserCREFeaturesAutorizations RC=\"0\"/><CREFeaturesOrder><CREFeature><Feature iRefFeature=\"6\" sTAGFeature=\"_TESTS_USR_RVN_USR_YJU_\" sLabelFeature=\"TESTS USR RVN USR YJU\" sDescriptionFeature=\"\"/><CRE sOperationTAG=\"_FRAIS_ATD_\" sCode...

                List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/UserCREFeaturesAutorizations", "RC");
                if (listRC.Count > 0)
                {
                    if (listRC[0] != "0")
                    {
                        bResultat = false;
                        List<string> listMessage = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/UserCREFeaturesAutorizations", "ERROR_MSG");
                        if (listMessage.Count > 0)
                        {
                            sErrorMessage = listMessage[0];
                        }
                        else
                        {
                            sErrorMessage = "erreur interne : " + listRC[0];
                        }
                    }
                    else
                    {
                        _sListAuthorizedFeature = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/CREFeaturesOrder/CREFeature");
                        bResultat = true;
                    }
                }
                else
                {
                    bResultat = false;
                }
            }
        }
        catch (Exception ex)
        {
            bResultat = false;
            sErrorMessage = "exception " + ex.Message;
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }
        return bResultat;
    }

    protected void initFeatureDdl()
    {
        pOperation.Visible = false;
        pNoOperation.Visible = false;
        
        if (bGetFeatureList() == true)
        {
            PopulateDdlFeature();
        }
        else
        {
            ddlFeature.Visible = false;
            AlertMessage("Problème", "Vous n'avez pas de profil de défini");
        }
    }

    protected void PopulateDdlFeature(string sOperationTag = "")
    {
        ddlFeature.Items.Clear();

        ddlFeature.Items.Add(new ListItem("Sélectionnez un profil ...", "-1"));
        
        for (int idxCreFeature = 0; idxCreFeature < _sListAuthorizedFeature.Count; idxCreFeature++)
        {
            List<string> listFeature = CommonMethod.GetTags(_sListAuthorizedFeature[idxCreFeature], "CREFeature/Feature");
            for (int i = 0; i < listFeature.Count; i++)
            {
                List<string> listRefFeature = CommonMethod.GetAttributeValues(listFeature[i], "Feature", "iRefFeature");
                List<string> listLabelFeature = CommonMethod.GetAttributeValues(listFeature[i], "Feature", "sLabelFeature");
                List<string> listTagFeature = CommonMethod.GetAttributeValues(listFeature[i], "Feature", "sTagFeature");
                ListItem item = new ListItem(listLabelFeature[0], listRefFeature[0]);

                if (sOperationTag != "")
                {
                    if (_sListAuthorizedFeature[idxCreFeature].IndexOf(sOperationTag) != -1)
                        ddlFeature.Items.Add(item);
                }
                else
                    ddlFeature.Items.Add(item);
            }
        }

        if (ddlFeature.Items.Count == 2)
        {
            ddlFeature.SelectedIndex = 1;
            ddlFeature.Enabled = false;
            ddlFeature_OnChange(this, null);
        }
        else if (ddlFeature.Items.Count > 2)
        {
            ddlFeature.Enabled = true;
        }
        else
        {
            ddlFeature.Enabled = false;
            pOperation.Visible = false;
            pNoOperation.Visible = true;
        }
    }

    protected void ddlFeature_OnChange(object sender, EventArgs e)
    {
        int iSelectedIndex = ddlFeature.SelectedIndex;
        
        if (_sListAuthorizedFeature != null && iSelectedIndex > 0)
        {
            ddlTypeOperation.Items.Clear();
            ddlTypeOperation.Items.Add(new ListItem("Sélectionnez l'opération ...", "-1"));
            
            // réinitilisation des champs
            cleanFields();
            HideField();
            btnSoummission.Enabled = false;
            pUserComments.Visible = false;
            lblParams.Visible = false;

            DataTable dt = new DataTable();
            dt.Columns.Add("libelle");
            dt.Columns.Add("tag");

            if (_sListAuthorizedCre != null)
                _sListAuthorizedCre.Clear();
            else
                _sListAuthorizedCre = new List<string>();
            List<string> sListCreBrut = CommonMethod.GetTags(_sListAuthorizedFeature[iSelectedIndex - 1], "CREFeature/CRE");

            List<string> listTagFeature = CommonMethod.GetAttributeValues(_sListAuthorizedFeature[iSelectedIndex - 1], "CREFeature/Feature", "sTAGFeature");
            if (listTagFeature.Count > 0)
                _sCurrentTagFeature = listTagFeature[0];
            else
                _sCurrentTagFeature = "";

            for (int idxCre = 0; idxCre < sListCreBrut.Count; idxCre++)
            {
                string sOperationTAG = "";
                string sCodeOperation = "";
                string sOperationDescription = "";
                CRETools.GetCREInfos(sListCreBrut[idxCre], out sOperationTAG, out sCodeOperation, out sOperationDescription);
                if (sOperationDescription.Length > 0 && sOperationTAG.Length > 0)
                {
                    dt.Rows.Add(new object[] {
                        sCodeOperation + " - " + sOperationDescription
                        , sOperationTAG
                    });
                }
            }
 
            if (dt.Rows.Count > 0)
            {
                DataView dv = new DataView(dt);
                dv.Sort = "libelle ASC";
                DataTable dtSorted = dv.ToTable();
                
                for (int idx = 0; idx < dt.Rows.Count; idx++)
                {
                    ListItem item = new ListItem(dtSorted.Rows[idx]["libelle"].ToString(), dtSorted.Rows[idx]["tag"].ToString());
                    ddlTypeOperation.Items.Add(item);
                    for(int idxCre = 0; idxCre < sListCreBrut.Count; idxCre++)
                    {
                        string sOperationTAG = "";
                        string sCodeOperation = "";
                        string sOperationDescription = "";
                        CRETools.GetCREInfos(sListCreBrut[idxCre], out sOperationTAG, out sCodeOperation, out sOperationDescription);
                        if (dtSorted.Rows[idx]["libelle"].ToString().CompareTo(sCodeOperation + " - " + sOperationDescription) == 0)
                        {
                            _sListAuthorizedCre.Add(sListCreBrut[idxCre]);
                            break;
                        }
                    }
                }
            }
        }

        if (ddlTypeOperation.Items.Count > 1)
        {
            pOperation.Visible = true;
            pNoOperation.Visible = false;

            if (_CreValues != null)
                InitPreData();
        }
        else
        {
            pOperation.Visible = false;
            pNoOperation.Visible = true;
        }
    }

    protected void ddlTypeOperation_OnChange(object sender, EventArgs e)
    {
        try
        {
            ListItem selectedItem = ddlTypeOperation.SelectedItem;
            int iSelectedIndex = ddlTypeOperation.SelectedIndex;
            string sMontantMin;
            string sMontantMax;
            btnSoummission.Enabled = true;

            // réinitilisation des champs
            cleanFields();
            HideField();

            ltlValideur.Visible = false;
            if (iSelectedIndex > 0)
            {
                bool bIsValidationRequested = true;
                string sValidationText = "";

                CRETools.GetCREValidationTextInfos(_sListAuthorizedCre[iSelectedIndex - 1], out bIsValidationRequested, out sValidationText);
                
                if (sValidationText.Length > 0)
                    ltlValideur.Text = sValidationText;
                else
                    ltlValideur.Text = "Pas de validation requise";
                ltlValideur.Visible = true;
            }


            switch (selectedItem.Value)
            {
                case "_FRAIS_REJET_PRELEVEMENTT_ABUSIF_":
                    pCompte1.Visible = true;
                    lblCompte1.Text = "Compte client";
                    pLibelle1.Visible = true;
                    lblLibelle1.Text = "Libellé";
                    pMontant1.Visible = true;
                    lblMontant1.Text = "Montant TTC frais à prendre";
                    break;
                case "_FRAIS_ATD_":
                case "_COTISATION_CARTE_CHROME_":
                case "_FRAIS_REMPLACEMENT_CARTE_CHROME_":
                case "_REMB_CARTE_CHROME_":
                    pCompte1.Visible = true;
                    lblCompte1.Text = "Compte client";
                    pMontant1.Visible = true;
                    lblMontant1.Text = "Montant";
                    break;
                case "_FRAIS_RECHERCHE_":
                    pCompte1.Visible = true;
                    lblCompte1.Text = "Compte client";
                    pLibelle1.Visible = true;
                    lblLibelle1.Text = "Libellé 1";
                    //pLibelle2.Visible = true;
                    lblLibelle2.Text = "Libellé 2";
                    pMontant1.Visible = true;
                    lblMontant1.Text = "Montant frais de recherche";
                    break;
                case "_FRAIS_UTILISATION_IRREGULIERE_COMPTE_":
                    pCompte1.Visible = true;
                    lblCompte1.Text = "Compte client";
                    pLibelle1.Visible = true;
                    lblLibelle1.Text = "Libellé 1";
                    //pLibelle2.Visible = true;
                    lblLibelle2.Text = "Libellé 2";
                    pMontant1.Visible = true;
                    lblMontant1.Text = "Montant commission utilisation irrégulière";
                    break;
                case "_SAISIE_ATTRIBUTION_": //+RT
                    pCompte1.Visible = true;
                    lblCompte1.Text = "Compte client";
                    pDI3001.Visible = true;
                    lblDI3001.Text = "Type d'opération";
                    ddlDI3001.Items.Clear();
                    ddlDI3001.Items.Add(new ListItem("Débit client", "_DEBIT_"));
                    ddlDI3001.Items.Add(new ListItem("Crédit client", "_CREDIT_"));
                    pMontant1.Visible = true;
                    lblMontant1.Text = "Montant";
                    break;
                /*case "_REMB_FRAIS_CLIENT_CARTE_PERSO_NON_RECUE_":
                case "_REMB_FRAIS_CLIENT_CARTE_PERSO_GEST_CO_":*/
                case "_REMB_FRAIS_CLIENT_":
                    pCompte1.Visible = true;
                    lblCompte1.Text = "Compte client";
                    pDate1.Visible = true;
                    pDI3001.Visible = true;
                    lblDI3001.Text = "Nature de l'opération";
                    ddlDI3001.Items.Clear();
                    ddlDI3001.Items.Add(new ListItem("Remboursement coffret (C)", "_REMB_COFFRET_"));
                    ddlDI3001.Items.Add(new ListItem("Remboursement commission de dépôt (D)", "_REMB_COM_DEPOT_"));
                    ddlDI3001.Items.Add(new ListItem("Geste commercial (G)", "_GEST_CO_"));
                    ddlDI3001.Items.Add(new ListItem("Remboursement commission de retrait (R)", "_REMB_COM_RETRAIT_"));
                    ddlDI3001.Items.Add(new ListItem("Remboursement carte personalisée (P)", "_REMB_CARTE_A_LA_CARTE_"));
                    pLibelle1.Visible = true;
                    lblLibelle1.Text = "Libellé";
                    pMontant1.Visible = true;
                    lblMontant1.Text = "Montant du remboursement";
                    break;
                case "_COTISATION_ANNUELLE_":
                    pCompte1.Visible = true;
                    lblCompte1.Text = "Compte client";
                    pCompte2.Visible = true;
                    lblCompte2.Text = "Compte buraliste";
                    pLibelle1.Visible = true;
                    lblLibelle1.Text = "Libellé";
                    pMontant1.Visible = true;
                    lblMontant1.Text = "Montant de la cotisation";
                    pMontant2.Visible = true;
                    lblMontant2.Text = "Montant commission PDV";
                    break;
                case "_COTISATION_ANNUELLE_PARTIELLE_1_":
                    pCompte1.Visible = true;
                    lblCompte1.Text = "Compte client";
                    pCompte2.Visible = true;
                    lblCompte2.Text = "Compte buraliste";
                    pLibelle1.Visible = true;
                    lblLibelle1.Text = "Libellé";
                    pMontant1.Visible = true;
                    lblMontant1.Text = "Montant total de la cotisation";
                    pMontant2.Visible = true;
                    lblMontant2.Text = "Montant partiel de la cotisation";
                    break;
                case "_COTISATION_ANNUELLE_PARTIELLE_2_":
                    pCompte1.Visible = true;
                    lblCompte1.Text = "Compte client";
                    pCompte2.Visible = true;
                    lblCompte2.Text = "Compte buraliste";
                    pLibelle1.Visible = true;
                    lblLibelle1.Text = "Libellé";
                    pMontant1.Visible = true;
                    lblMontant1.Text = "Montant partiel de la cotisation";
                    pMontant2.Visible = true;
                    lblMontant2.Text = "Montant commission PDV (si dernier prélèvement)";
                    break;
                case "_COM_REMPLACEMENT_CARTE_":
                    pCompte1.Visible = true;
                    lblCompte1.Text = "Compte client";
                    pCompte2.Visible = true;
                    lblCompte2.Text = "Compte PDV";
                    pDate1.Visible = true;
                    pMontant1.Visible = true;
                    lblMontant1.Text = "Montant";
                    pMontant2.Visible = true;
                    lblMontant2.Text = "Montant commission PDV";
                    break;
                case "_COM_OUVERTURE_COMPTE_":
                    pCompte1.Visible = true;
                    lblCompte1.Text = "Compte client";
                    pCompte2.Visible = true;
                    lblCompte2.Text = "Compte PDV";
                    pMontant1.Visible = true;
                    lblMontant1.Text = "Montant commission PDV";
                    break;
                case "_FRAIS_COMPTE_A_CLOTURER_":
                    pCompte1.Visible = true;
                    lblCompte1.Text = "Compte client";
                    pMontant1.Visible = true;
                    lblMontant1.Text = "Montant TTC frais à prendre";
                    break;
                case "_REGUL_OPE_CARTE_REJETEE_IMPAYE_DEBIT_":// +RE
                case "_REGUL_OPE_CARTE_REJETEE_IMPAYE_CREDIT_":// +RE
                    if (    _sCurrentTagFeature == "_F_REMB_FRAIS_CLIENT_FRAUDE_PASS_PERTE_"
                        ||  _sCurrentTagFeature == "_F_REMB_FRAIS_CLIENT_FRAUDE_AVANCE_")
                    {
                        pCompte1.Visible = true;
                        lblCompte1.Text = "Compte client";
                        pDI3001.Visible = true;
                        lblDI3001.Text = "Compte destinataire";
                        ddlDI3001.Items.Clear();
                        ddlDI3001.Items.Add(new ListItem("Compte client(CLT)", "_COMPTE_CLIENT_"));
                        pDI3002.Visible = true;
                        lblDI3002.Text = "Code rejet d'origine";
                        ddlDI3002.Items.Clear();
                        ddlDI3002.Items.Add(new ListItem("Reservé impayé client (000)", "_RESERVE_IMPAYE_CLIENT_"));
                        pLibelle1.Visible = true;
                        lblLibelle1.Text = "Libellé 1";
                        pMontant1.Visible = true;
                        lblMontant1.Text = "Montant opération";
                        pMotif.Visible = true;
                        lblTypeRemb.Text = "Type de remboursement";
                        if (_sCurrentTagFeature == "_F_REMB_FRAIS_CLIENT_FRAUDE_PASS_PERTE_")
                        {
                            ddlTypeRemb.Items.Add(new ListItem("Passage à perte", "_TYPE_REMB_PASSAGE_A_PERTE_"));
                            ddlMotif.Items.Add(new ListItem("Phishing carte", "_MOTIF_PHISHING_CARTE_"));
                            ddlMotif.Items.Add(new ListItem("Phishing Virement", "_MOTIF_PHISHING_VIREMENT_"));
                        }
                        else //_F_REMB_FRAIS_CLIENT_FRAUDE_AVANCE_
                        {
                            ddlTypeRemb.Items.Add(new ListItem("Avance", "_TYPE_REMB_AVANCE_"));
                        }
                        lblMotif.Text = "Motif";
                        ddlMotif.Items.Add(new ListItem("Fraude", "_MOTIF_FRAUDE_"));
                        ddlMotif.Items.Add(new ListItem("Contestation d'opération retrait", "_MOTIF_CONTEST_RETRAIT_"));
                    }
                    else
                    {
                        pCompte1.Visible = true;
                        lblCompte1.Text = "Compte client";
                        pDI3001.Visible = true;
                        lblDI3001.Text = "Compte destinataire";
                        ddlDI3001.Items.Clear();
                        ddlDI3001.Items.Add(new ListItem("Compte client(CLT)", "_COMPTE_CLIENT_"));
                        ddlDI3001.Items.Add(new ListItem("Compte d'impayé (IMP)", "_COMPTE_IMPAYE_"));
                        pDI3002.Visible = true;
                        lblDI3002.Text = "Code rejet d'origine";
                        ddlDI3002.Items.Clear();
                        ddlDI3002.Items.Add(new ListItem("Reservé impayé client (000)", "_RESERVE_IMPAYE_CLIENT_"));
                        ddlDI3002.Items.Add(new ListItem("Compte bloqué ou clos (003)", "_COMPTE_BLOQUE_CLOS_"));
                        ddlDI3002.Items.Add(new ListItem("Carte opposée (012)", "_CARTE_OPPOSEE_"));
                        ddlDI3002.Items.Add(new ListItem("Carte clôturée (014)", "_CARTE_CLOTUREE_"));
                        ddlDI3002.Items.Add(new ListItem("Réservé rejet (999)", "_RESERVE_REJET_999_"));
                        pLibelle1.Visible = true;
                        lblLibelle1.Text = "Libellé 1";
                        //pLibelle2.Visible = true;
                        lblLibelle2.Text = "Libellé 2";
                        pMontant1.Visible = true;
                        lblMontant1.Text = "Montant opération";
                        pMontant2.Visible = true;
                        lblMontant2.Text = "Montant commission";
                        hdnMontant1OrMontant2.Value = "true";
                    }
                    break;
                case "+RP":
                    pCompte1.Visible = true;
                    lblCompte1.Text = "Compte client";
                    pDI3001.Visible = true;
                    lblDI3001.Text = "Type d'opération";
                    ddlDI3001.Items.Clear();
                    ddlDI3001.Items.Add(new ListItem("Alimentation PDV par chèque", "C"));
                    ddlDI3001.Items.Add(new ListItem("Extourne alimentation PDV par chèque", "D"));
                    pMontant1.Visible = true;
                    lblMontant1.Text = "Montant du chèque";
                    break;
                case "+ZZ":
                    pDate1.Visible = true;
                    pCompte1.Visible = true;
                    lblCompte1.Text = "Compte 1";
                    pCompte2.Visible = true;
                    lblCompte2.Text = "Compte 2";
                    pDI3001.Visible = true;
                    lblDI3001.Text = "Type d'opération";
                    ddlDI3001.Items.Clear();
                    ddlDI3001.Items.Add(new ListItem("Débit compte 1", "D"));
                    ddlDI3001.Items.Add(new ListItem("Crédit compte 1", "C"));
                    pDI3002.Visible = true;
                    lblDI3002.Text = "Type d'opération";
                    ddlDI3002.Items.Clear();
                    ddlDI3002.Items.Add(new ListItem("Débit compte 2", "D"));
                    ddlDI3002.Items.Add(new ListItem("Crédit compte 2", "C"));
                    pLibelle1.Visible = true;
                    lblLibelle1.Text = "Libellé compte 1";
                    pLibelle2.Visible = true;
                    lblLibelle2.Text = "Libellé compte 2";
                    pMontant1.Visible = true;
                    lblMontant1.Text = "Montant régul";
                    break;
                case "+CC":
                    pCompte1.Visible = true;
                    lblCompte1.Text = "Compte client";
                    pDate1.Visible = true;
                    pDI3001.Visible = true;
                    lblDI3001.Text = "Sur provision (DTX+CLT)";
                    ddlDI3001.Items.Clear();
                    ddlDI3001.Items.Add(new ListItem("OUI", "O"));
                    ddlDI3001.Items.Add(new ListItem("NON", "N"));
                    pDI3002.Visible = true;
                    lblDI3002.Text = "Sens solde compte client";
                    ddlDI3001.Items.Clear();
                    ddlDI3001.Items.Add(new ListItem("Créditeur", "C"));
                    ddlDI3001.Items.Add(new ListItem("Débiteur", "D"));
                    pMontant1.Visible = true;
                    lblMontant1.Text = "Solde client";
                    pMontant2.Visible = true;
                    lblMontant2.Text = "Créance impayée";
                    break;
                case "+DC":
                    pCompte1.Visible = true;
                    lblCompte1.Text = "Compte PDV";
                    pDate1.Visible = true;
                    pMontant1.Visible = true;
                    lblMontant1.Text = "Montant décommission";
                    pLibelle1.Visible = true;
                    lblLibelle1.Text = "Libellé client";
                    break;
                case "MULTI":
                    pMulti.Visible = true;
                    break;
                default:
                    btnSoummission.Enabled = false;
                    break;
            }

            pUserComments.Visible = btnSoummission.Enabled;
            lblParams.Visible = btnSoummission.Enabled;
            pCreNonImplementer.Visible = !btnSoummission.Enabled;
        }
        catch (Exception ex)
        {
            AlertMessage("exception", ex.Message);
        }
    }

    protected void cleanFields()
    {
        tbCompte1.Text = "";
        tbCompte2.Text = "";
        txtDate1.Text = "";
        txtDate2.Text = "";
        hdnLibelle1.Value = "0";
        tbLibelle1.Text = "";
        hdnLibelle2.Value = "0";
        tbLibelle2.Text = "";
        hdnMontant1OrMontant2.Value = "false";
        tbMontant1.Text = "";
        tbMontant1Cts.Text = "00";
        tbMontant2.Text = "";
        tbMontant2Cts.Text = "00";
        tbUserComments.Text = "";

        ddlDI3001.Items.Clear();
        ddlDI3002.Items.Clear();
        ddlMotif.Items.Clear();
        ddlTypeRemb.Items.Clear();

        lblBirthDate.Text = "";
        lblName.Text = "";
    }

    protected void HideField()
    {
        pEve.Visible = false;
        pDate1.Visible = false;
        pDate2.Visible = false;
        pCompte1.Visible = false;
        pCompte2.Visible = false;
        pLibelle1.Visible = false;
        pLibelle2.Visible = false;
        pMontant1.Visible = false;
        pMontant2.Visible = false;
        pMulti.Visible = false;
        pDI3001.Visible = false;
        pDI3002.Visible = false;
        pMotif.Visible = false;
        pTypeRemb.Visible = false;
        ltlValideur.Visible = false;
        //panelClientInfos.Visible = false;
    }

    protected void AlertMessage(string sTitle, string sMessage, bool bIsOk = false)
    {
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessageOD(\"" + sTitle + "\", \"" + sMessage + "\", '" + bIsOk.ToString() + "');", true);
    }

    protected void btnRedirectPage_Click(object sender, EventArgs e)
    {
        bool bCREOk = false;
        try
        {
            if (hdnResultCre.Value == "True")
                bCREOk = true;
        }
        catch (Exception ex)
        {
            bCREOk = false;
        }
        if (_CreValues != null && bCREOk)
        {
            _CreValues.sTreatmentStatus = CRETools.TreatmentStatus.Successful;
            CRETools.Redirect(_CreValues);
        }
    }

    protected bool bCheckAmount(decimal dMontant, int iTypeMontant, out string sMontantMin, out string sMontantMax)
    {
        bool bResult = true;
        
        decimal dMontantMin;
        decimal dMontantMax;
        int iSelectedIndex = ddlTypeOperation.SelectedIndex;
        ListItem currentItem = ddlMotif.SelectedItem;

        CRETools.GetCREOrderMontant(_sListAuthorizedCre[iSelectedIndex - 1], iTypeMontant, out sMontantMin, out sMontantMax);

        if (Decimal.TryParse(sMontantMax.Replace('.', ','), NumberStyles.Currency, new CultureInfo("fr-FR"), out dMontantMax))
            dMontantMax = decimal.Round(dMontantMax, 2, MidpointRounding.AwayFromZero);
        else
            dMontantMax = -1;

        if (Decimal.TryParse(sMontantMin.ToString().Replace('.', ','), NumberStyles.Currency, new CultureInfo("fr-FR"), out dMontantMin))
            dMontantMin = decimal.Round(dMontantMin, 2, MidpointRounding.AwayFromZero);
        else
            dMontantMin = -1;

/*        if (_sCurrentTagFeature == "_F_REMB_FRAIS_CLIENT_FRAUDE_PASS_PERTE_")
        {
            if (currentItem.Value == "_MOTIF_FRAUDE_")
            {
                if (dMontantMax == -1 || (dMontantMax != -1 && dMontantMax > 120))
                {
                    Decimal.TryParse("120,00", NumberStyles.Currency, new CultureInfo("fr-FR"), out dMontantMax);
                }
            }
            else if (currentItem.Value == "_MOTIF_CONTEST_RETRAIT_")
            {
                if (dMontantMax == -1 || (dMontantMax != -1 && dMontantMax > 30))
                {
                    Decimal.TryParse("30,00", NumberStyles.Currency, new CultureInfo("fr-FR"), out dMontantMax);
                }
            }
        }
        else */
        if (_sCurrentTagFeature == "_F_REMB_FRAIS_CLIENT_FRAUDE_AVANCE_")
        {
            if (currentItem.Value == "_MOTIF_FRAUDE_")
            {
                if (dMontantMin == -1 || (dMontantMin != -1 && dMontantMin < 120))
                {
                    Decimal.TryParse("120,01", NumberStyles.Currency, new CultureInfo("fr-FR"), out dMontantMin);
                }
            }
            else if (currentItem.Value == "_MOTIF_CONTEST_RETRAIT_")
            {
                if (dMontantMin == -1 || (dMontantMin != -1 && dMontantMin < 30))
                {
                    Decimal.TryParse("30,01", NumberStyles.Currency, new CultureInfo("fr-FR"), out dMontantMin);
                }
            }
        }

        if (dMontantMin != -1 && dMontant <= dMontantMin)
        {
            bResult = false;
        }

        if (dMontantMax != -1 && dMontant >= dMontantMax)
        {
            bResult = false;
        }

        sMontantMin = dMontantMin.ToString("0.00");
        sMontantMax = dMontantMax.ToString("0.00");

        return bResult;
    }

    protected bool bCheckForm(out string sMessage)
    {
        bool bResult = true;
        int iSelectedIndex = ddlTypeOperation.SelectedIndex;
        string sMontantMin;
        string sMontantMax;
        decimal dMontant1 = 0;
        decimal dMontant2 = 0;
        sMessage = "";

        if (pMontant1.Visible == true)
        {
            if (Decimal.TryParse(tbMontant1.Text.Trim() + "," + tbMontant1Cts.Text.Trim(), NumberStyles.Currency, new CultureInfo("fr-FR"), out dMontant1))
                dMontant1 = decimal.Round(dMontant1, 2, MidpointRounding.AwayFromZero);
            else
                dMontant1 = 0;
        }
        if (pMontant2.Visible == true)
        {
            if (Decimal.TryParse(tbMontant1.Text.Trim() + "," + tbMontant1Cts.Text.Trim(), NumberStyles.Currency, new CultureInfo("fr-FR"), out dMontant2))
                dMontant2 = decimal.Round(dMontant2, 2, MidpointRounding.AwayFromZero);
            else
                dMontant2 = 0;
        }

        if (hdnMontant1OrMontant2.Value == "true")
        {
            if (dMontant1 == 0 && dMontant2 == 0)
            {
                bResult = false;
                sMessage += "Montants : veuiller remplir au moins un des montants.<br/>";
            }
            else
            {
                if (bCheckAmount(dMontant1, 1, out sMontantMin, out sMontantMax) == false)
                {
                    bResult = false;
                    sMessage += "Montant1 invalide : il doit être compris entre ";
                    sMessage += ((sMontantMin != "-1,00") ? sMontantMin : "0") + "€ et ";
                    sMessage += ((sMontantMax != "-1,00") ? sMontantMax : "999999999") + "€.<br/>";
                }

                if (bCheckAmount(dMontant2, 2, out sMontantMin, out sMontantMax) == false)
                {
                    bResult = false;
                    sMessage += "Montant2 invalide : il doit être compris entre ";
                    sMessage += ((sMontantMin != "-1,00") ? sMontantMin : "0") + "€ et ";
                    sMessage += ((sMontantMax != "-1,00") ? sMontantMax : "999999999") + "€.<br/>";
                }
            }
        }
        else
        {
            if (pMontant1.Visible == true)
            {
                if (bCheckAmount(dMontant1, 1, out sMontantMin, out sMontantMax) == false)
                {
                    bResult = false;
                    sMessage += "Montant1 invalide : il doit être compris entre ";
                    sMessage += ((sMontantMin != "-1,00") ? sMontantMin : "0") + "€ et ";
                    sMessage += ((sMontantMax != "-1,00") ? sMontantMax : "999999999") + "€.<br/>";
                }
            }

            if (pMontant2.Visible == true)
            {
                if (bCheckAmount(dMontant2, 2, out sMontantMin, out sMontantMax) == false)
                {
                    bResult = false;
                    sMessage += "Montant2 invalide : il doit être compris entre ";
                    sMessage += ((sMontantMin != "-1,00") ? sMontantMin : "0") + "€ et ";
                    sMessage += ((sMontantMax != "-1,00") ? sMontantMax : "999999999") + "€.<br/>";
                }
            }
        }

        return bResult;
    }

    protected void btnSoummission_Click(object sender, EventArgs e)
    {
        string sMessage = "";
        if (bCheckForm(out sMessage))
        {
            vSoumission();
        }
        else
        {
            AlertMessage("saisie incorrecte", sMessage);
        }
    }

    protected void vSoumission()
    { 
        bool bSoummissionOK = true;
        bool bResultOK = false;
        string sErrorMessage = "Opération non implémentée";
        string sMontant1 = (pMontant1.Visible) ? tbMontant1.Text.Trim() + "." + tbMontant1Cts.Text.Trim() : "";
        string sMontant2 = (pMontant2.Visible) ? tbMontant2.Text.Trim() + "." + tbMontant2Cts.Text.Trim() : "";
        ListItem selectedItem = ddlTypeOperation.SelectedItem;
        int iSelectedIndex = ddlTypeOperation.SelectedIndex;
        bool bIsValidationAsked = true;
        XElement xAdditionalInfos = null;
        
        XElement sXmlIn4Cre = new XElement("CreOrder"
            , new XAttribute("TOKEN", authentication.GetCurrent().sToken)
            , new XAttribute("sOperationTAG", selectedItem.Value)
            , new XAttribute("iRefFeature", ddlFeature.SelectedItem.Value)
            , new XAttribute("sTAGFeature", _sCurrentTagFeature)
            );

        if (iSelectedIndex > 0)
        {
            bIsValidationAsked = CRETools.IsValidationRequested(_sListAuthorizedCre[iSelectedIndex - 1], sMontant1, sMontant2);
        }

        sXmlIn4Cre.SetAttributeValue("bCREValidationRequested", bIsValidationAsked ? "1" : "0");

        DateTime date;

        if (tbUserComments.Text.Length > 0)
            sXmlIn4Cre.SetAttributeValue("sUserComment", tbUserComments.Text.Replace("\n", "").Replace("\r", ""));

        switch (selectedItem.Value)
        {
            case "_FRAIS_REJET_PRELEVEMENTT_ABUSIF_":
                sXmlIn4Cre.SetAttributeValue("mMontant1", tbMontant1.Text.Trim() + "." + tbMontant1Cts.Text.Trim());
                sXmlIn4Cre.SetAttributeValue("sCompte1", tbCompte1.Text);
                if (tbLibelle1.Text.Length > 0)
                    sXmlIn4Cre.SetAttributeValue("sLibelle1", tbLibelle1.Text);
                break;
            case "_FRAIS_ATD_":
                sXmlIn4Cre.SetAttributeValue("mMontant1", tbMontant1.Text.Trim() + "." + tbMontant1Cts.Text.Trim());
                sXmlIn4Cre.SetAttributeValue("sCompte1", tbCompte1.Text);
                break;
            case "_COTISATION_CARTE_CHROME_":
            case "_FRAIS_REMPLACEMENT_CARTE_CHROME_":
            case "_REMB_CARTE_CHROME_":
                sXmlIn4Cre.SetAttributeValue("mMontant1", tbMontant1.Text.Trim() + "." + tbMontant1Cts.Text.Trim());
                sXmlIn4Cre.SetAttributeValue("sCompte1", tbCompte1.Text);
                break;
            case "_FRAIS_RECHERCHE_":
            case "_FRAIS_UTILISATION_IRREGULIERE_COMPTE_":
                sXmlIn4Cre.SetAttributeValue("mMontant1", tbMontant1.Text.Trim() + "." + tbMontant1Cts.Text.Trim());
                sXmlIn4Cre.SetAttributeValue("sCompte1", tbCompte1.Text);
                if (tbLibelle1.Text.Length > 0)
                    sXmlIn4Cre.SetAttributeValue("sLibelle1", tbLibelle1.Text);
                if (tbLibelle2.Text.Length > 0)
                    sXmlIn4Cre.SetAttributeValue("sLibelle2", tbLibelle2.Text);
                break;
            case "_SAISIE_ATTRIBUTION_":
                sXmlIn4Cre.SetAttributeValue("sCompte1", tbCompte1.Text);
                sXmlIn4Cre.SetAttributeValue("sDivers", ddlDI3001.SelectedItem.Value);
                sXmlIn4Cre.SetAttributeValue("mMontant1", tbMontant1.Text.Trim() + "." + tbMontant1Cts.Text.Trim());
                break;
            case "_REMB_FRAIS_CLIENT_CARTE_PERSO_NON_RECUE_":
            case "_REMB_FRAIS_CLIENT_CARTE_PERSO_GEST_CO_":
            case "_REMB_FRAIS_CLIENT_":
                sXmlIn4Cre.SetAttributeValue("sCompte1", tbCompte1.Text);
                sXmlIn4Cre.SetAttributeValue("sCompte2", tbCompte2.Text);
                if (DateTime.TryParseExact(txtDate1.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    //sXmlIn4Cre.SetAttributeValue("sDate2_AAAAMMJJ", date.ToString("yyyyMMdd"));
                    sXmlIn4Cre.SetAttributeValue("sDate1_AAAAMMJJ", date.ToString("yyyyMMdd"));
                }
                sXmlIn4Cre.SetAttributeValue("sLibelle1", tbLibelle1.Text);
                sXmlIn4Cre.SetAttributeValue("mMontant1", tbMontant1.Text.Trim() + "." + tbMontant1Cts.Text.Trim());
                sXmlIn4Cre.SetAttributeValue("sDivers", ddlDI3001.SelectedItem.Value);
                break;
            case "_COTISATION_ANNUELLE_":
            case "_COTISATION_ANNUELLE_PARTIELLE_1_":
            case "_COTISATION_ANNUELLE_PARTIELLE_2_":
                sXmlIn4Cre.SetAttributeValue("sCompte1", tbCompte1.Text);
                sXmlIn4Cre.SetAttributeValue("sCompte2", tbCompte2.Text);
                sXmlIn4Cre.SetAttributeValue("mMontant1", tbMontant1.Text.Trim() + "." + tbMontant1Cts.Text.Trim());
                sXmlIn4Cre.SetAttributeValue("mMontant2", tbMontant2.Text.Trim() + "." + tbMontant2Cts.Text.Trim());
                break;
            case "_COM_REMPLACEMENT_CARTE_":
                sXmlIn4Cre.SetAttributeValue("sCompte1", tbCompte1.Text);
                sXmlIn4Cre.SetAttributeValue("sCompte2", tbCompte2.Text);
                sXmlIn4Cre.SetAttributeValue("mMontant1", tbMontant1.Text.Trim() + "." + tbMontant1Cts.Text.Trim());
                sXmlIn4Cre.SetAttributeValue("mMontant2", tbMontant2.Text.Trim() + "." + tbMontant2Cts.Text.Trim());
                if (DateTime.TryParseExact(txtDate1.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    sXmlIn4Cre.SetAttributeValue("sDate1_AAAAMMJJ", date.ToString("yyyyMMdd"));
                }
                break;
            case "_COM_OUVERTURE_COMPTE_":
                sXmlIn4Cre.SetAttributeValue("sCompte1", tbCompte1.Text);
                sXmlIn4Cre.SetAttributeValue("sCompte2", tbCompte2.Text);
                sXmlIn4Cre.SetAttributeValue("mMontant1", tbMontant1.Text.Trim() + "." + tbMontant1Cts.Text.Trim());
                break;
            case "_FRAIS_COMPTE_A_CLOTURER_":
                sXmlIn4Cre.SetAttributeValue("sCompte1", tbCompte1.Text);
                sXmlIn4Cre.SetAttributeValue("mMontant1", tbMontant1.Text.Trim() + "." + tbMontant1Cts.Text.Trim());
                break;
            case "_REGUL_OPE_CARTE_REJETEE_IMPAYE_DEBIT_":// +RE
            case "_REGUL_OPE_CARTE_REJETEE_IMPAYE_CREDIT_":// +RE":
                sXmlIn4Cre.SetAttributeValue("sCompte1", tbCompte1.Text);
                sXmlIn4Cre.SetAttributeValue("sDivers", ddlDI3001.SelectedItem.Value);
                sXmlIn4Cre.SetAttributeValue("sDivers2", ddlDI3002.SelectedItem.Value);
                if (tbLibelle1.Text.Length > 0)
                    sXmlIn4Cre.SetAttributeValue("sLibelle1", tbLibelle1.Text);
                sXmlIn4Cre.SetAttributeValue("mMontant1", tbMontant1.Text.Trim() + "." + tbMontant1Cts.Text.Trim());
                if (    _sCurrentTagFeature == "_F_REMB_FRAIS_CLIENT_FRAUDE_PASS_PERTE_"
                    ||  _sCurrentTagFeature == "_F_REMB_FRAIS_CLIENT_FRAUDE_AVANCE_")
                {
                    xAdditionalInfos = new XElement("AdditionalInfos"
                        , new XAttribute("sMotif", ddlMotif.SelectedItem.Value)
                        , new XAttribute("sTypeRemb", ddlTypeRemb.SelectedItem.Value)
                        );
                }
                else
                {
                    if (tbLibelle2.Text.Length > 0)
                        sXmlIn4Cre.SetAttributeValue("sLibelle2", tbLibelle2.Text);
                    sXmlIn4Cre.SetAttributeValue("mMontant2", tbMontant2.Text.Trim() + "." + tbMontant2Cts.Text.Trim());
                }
                break;
            case "+RP":
                sXmlIn4Cre.SetAttributeValue("sCompte1", tbCompte1.Text);
                sXmlIn4Cre.SetAttributeValue("DI1001", ddlDI3001.SelectedItem.Value);
                sXmlIn4Cre.SetAttributeValue("mMontant1", tbMontant1.Text.Trim() + "." + tbMontant1Cts.Text.Trim());
                break;
            case "+ZZ":
                sXmlIn4Cre.SetAttributeValue("sCompte1", tbCompte1.Text);
                sXmlIn4Cre.SetAttributeValue("sCompte2", tbCompte2.Text);
                if (DateTime.TryParseExact(txtDate1.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    sXmlIn4Cre.SetAttributeValue("sDate1_AAAAMMJJ", date.ToString("yyyyMMdd"));
                }
                sXmlIn4Cre.SetAttributeValue("DI1001", ddlDI3001.SelectedItem.Value);
                sXmlIn4Cre.SetAttributeValue("DI1002", ddlDI3002.SelectedItem.Value);
                sXmlIn4Cre.SetAttributeValue("sLibelle1", tbLibelle1.Text);
                sXmlIn4Cre.SetAttributeValue("sLibelle2", tbLibelle2.Text);
                sXmlIn4Cre.SetAttributeValue("mMontant1", tbMontant1.Text.Trim() + "." + tbMontant1Cts.Text.Trim());
                break;
            case "+CC":
                sXmlIn4Cre.SetAttributeValue("sCompte1", tbCompte1.Text);
                if (DateTime.TryParseExact(txtDate1.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    sXmlIn4Cre.SetAttributeValue("sDate1_AAAAMMJJ", date.ToString("yyyyMMdd"));
                }
                sXmlIn4Cre.SetAttributeValue("DI1001", ddlDI3001.SelectedItem.Value);
                sXmlIn4Cre.SetAttributeValue("DI1002", ddlDI3002.SelectedItem.Value);
                sXmlIn4Cre.SetAttributeValue("DI1003", "2");
                sXmlIn4Cre.SetAttributeValue("mMontant1", tbMontant1.Text.Trim() + "." + tbMontant1Cts.Text.Trim());
                break;
            case "+DC":
                sXmlIn4Cre.SetAttributeValue("sCompte1", tbCompte1.Text);
                if (DateTime.TryParseExact(txtDate1.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    sXmlIn4Cre.SetAttributeValue("sDate1_AAAAMMJJ", date.ToString("yyyyMMdd"));
                }
                sXmlIn4Cre.SetAttributeValue("mMontant1", tbMontant1.Text.Trim() + "." + tbMontant1Cts.Text.Trim());
                sXmlIn4Cre.SetAttributeValue("sLibelle2", tbLibelle1.Text);
                break;

            case "MULTI":

                break;
            default:
                bSoummissionOK = false;
                break;
        }

        if (bSoummissionOK)
        {
            SqlConnection conn = null;

            try
            {
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRE"].ConnectionString);
                conn.Open();

                SqlCommand cmd = new SqlCommand("CRE.P_AddCREOrderV3", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@sXmlIn", SqlDbType.VarChar, 8000);
                cmd.Parameters.Add("@sXmlOut", SqlDbType.VarChar, -1);

                if (xAdditionalInfos != null)
                    sXmlIn4Cre.Add(xAdditionalInfos);

                cmd.Parameters["@sXmlIn"].Value = new XElement("ALL_XML_IN", sXmlIn4Cre).ToString();
                cmd.Parameters["@sXmlOut"].Direction = ParameterDirection.Output;

                cmd.ExecuteNonQuery();

                string sXmlOut = cmd.Parameters["@sXmlOut"].Value.ToString();

                if (sXmlOut.Length > 0)
                {
                    List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Result", "RC");

                    if (listRC.Count > 0 && listRC[0] != "0")
                    {
                        List<string> listMessage = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Result", "ERROR_MSG");
                        List<string> listLog = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Result", "ERROR_LOG");
                        if (bIsValidationAsked)
                            sErrorMessage = "Echec enregistrement : ";
                        else
                            sErrorMessage = "Echec opération : ";

                        if (listMessage.Count > 0)
                        {
                            sErrorMessage += listMessage[0];
                        }
                        else
                        {
                            sErrorMessage += "erreur interne : " + listRC[0];
                        }

                        if (listLog.Count > 0)
                            sErrorMessage += "<br/>" + listLog[0];

                        bResultOK = false;
                    }
                    else
                    {
                        List<string> listRefCre = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Result", "iRefCreOrder");
                        if (listRefCre.Count > 0 && _CreValues != null)
                        {
                            _CreValues.iRefCREOrder = int.Parse(listRefCre[0]);
                        }

                        if (bIsValidationAsked)
                            sErrorMessage = "Demande enregistrée";
                        else
                            sErrorMessage = "Opération OK";

                        cleanFields();
                        HideField();
                        ddlTypeOperation.SelectedIndex = 0;
                        ddlTypeOperation.SelectedValue = "-1";
                        btnSoummission.Enabled = false;
                        pUserComments.Visible = btnSoummission.Enabled;
                        lblParams.Visible = btnSoummission.Enabled;

                        bResultOK = true;
                    }
                }
            }
            catch (Exception ex)
            {
                sErrorMessage = ex.Message;
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        AlertMessage(selectedItem.Text, sErrorMessage, bResultOK);
    }

    protected void fileUploaded(object sender, EventArgs e)
    {
        //BindFileControls();
    }

    protected void InitPageFromParams(string sDatas)
    {
        //_sJsonVal4Cre = JWTDecode(sDatas);
        CRETools.CREValues CreValue;

        if (CRETools.GetCREValuesFromJWT(sDatas, out CreValue))
        {
            _CreValues = CreValue;

            if (_CreValues.sRedirect.Length > 0)
                btnPrevious.PostBackUrl = _CreValues.sRedirect;

            // on se positionne par défaut sur le premier profil
            PopulateDdlFeature(_CreValues.sOperationTAG);
            ddlFeature.SelectedIndex = 1;
            ddlFeature_OnChange(this, null);
        }

    }

    protected void InitPreData()
    {
        string[] montantTab;

        if (string.IsNullOrWhiteSpace(_CreValues.sOperationTAG))
            return;
        ddlTypeOperation.SelectedValue = _CreValues.sOperationTAG;
        ddlTypeOperation_OnChange(this, null);
        ddlTypeOperation.Enabled = false;

        if (!string.IsNullOrWhiteSpace(_CreValues.sCompte1))
            tbCompte1.Text = _CreValues.sCompte1;
        tbCompte1.Enabled = _CreValues.bCompte1Editable;

        if (!string.IsNullOrWhiteSpace(_CreValues.sCompte2))
            tbCompte2.Text = _CreValues.sCompte2;
        tbCompte2.Enabled = _CreValues.bCompte1Editable;

        if (!string.IsNullOrWhiteSpace(_CreValues.mMontant1))
        {
            montantTab = (_CreValues.mMontant1).Replace(",", ".").Split('.');
            tbMontant1.Text = montantTab[0];
            if (montantTab.Count() > 1)
                tbMontant1Cts.Text = montantTab[1];
        }
        tbMontant1.Enabled = _CreValues.bMontant1Editable;
        tbMontant1Cts.Enabled = _CreValues.bMontant1Editable;

        if (!string.IsNullOrWhiteSpace(_CreValues.mMontant2))
        {
            montantTab = (_CreValues.mMontant2).Replace(",", ".").Split('.');
            tbMontant2.Text = montantTab[0];
            if (montantTab.Count() > 1)
                tbMontant2Cts.Text = montantTab[1];
        }
        tbMontant2.Enabled = _CreValues.bMontant2Editable;
        tbMontant2Cts.Enabled = _CreValues.bMontant2Editable;

        if (!string.IsNullOrWhiteSpace(_CreValues.sLibelle1))
            tbLibelle1.Text = _CreValues.sLibelle1;
        tbLibelle1.Enabled = _CreValues.bLibelle1Editable;

        if (!string.IsNullOrWhiteSpace(_CreValues.sLibelle2))
            tbLibelle1.Text = _CreValues.sLibelle2;
        tbLibelle1.Enabled = _CreValues.bLibelle2Editable;

        if (!string.IsNullOrWhiteSpace(_CreValues.sDate1_AAAAMMJJ))
        {
            txtDate1.Text = _CreValues.sDate1_AAAAMMJJ.Substring(6, 2) + " / " + _CreValues.sDate1_AAAAMMJJ.Substring(4, 2) + "/" + _CreValues.sDate1_AAAAMMJJ.Substring(0, 4);
        }
        txtDate1.Enabled = _CreValues.bDate1Editable;

        if (!string.IsNullOrWhiteSpace(_CreValues.sDivers))
            ddlDI3001.SelectedValue = _CreValues.sDivers;
        ddlDI3001.Enabled = _CreValues.bDiversEditable;

        if (!string.IsNullOrWhiteSpace(_CreValues.sDivers2))
            ddlDI3002.SelectedValue = _CreValues.sDivers2;
        ddlDI3002.Enabled = _CreValues.bDivers2Editable;

        /*
        if (!string.IsNullOrWhiteSpace(_CreValues.sUserComment))
            tbUserComments.Text = _CreValues.sUserComment;
        tbUserComments.Enabled = _CreValues.bUserCommentEditable;
        */
    }

    protected void UpdateClientPanel(string sRefCustomer, string sLastName, string sFirstName, string sBirthDate)
    {
        lblName.Text = sLastName + " " + sFirstName;
        lblBirthDate.Text = sBirthDate;
        panelClientInfos.Visible = true;
    }

    protected bool SearchCustomer(string sAccountNumber, string sToken, out string sRefCustomer, out string sLastName, out string sFirstName, out string sBirthDate)
    {
        bool bOk = false;
        sFirstName = "";
        sLastName = "";
        sRefCustomer = "";
        sBirthDate = "";

        DataTable dt = Client.GetClientList(sAccountNumber, sToken);

        if (dt.Rows.Count > 0)
        {
            bOk = true;
            sLastName = dt.Rows[0]["LastName"].ToString();
            sFirstName = dt.Rows[0]["FirstName"].ToString();
            sRefCustomer = dt.Rows[0]["RefCustomer"].ToString();
            sBirthDate = dt.Rows[0]["BirthDate"].ToString();
        }

        return bOk;
    }

    protected void btnSearchClient_Click(object sender, EventArgs e)
    {
        panelClientInfos.Visible = false;
        clientSearch1.btnClientSearch_Click(sender, e);
        clientSearch1.RepeatDirection = API_ClientSearch.ObjRepeatDirection.Vertical;
        clientSearch1.FindControl("txtClientSearch").Focus();
    }

    protected void clientFound(object sender, EventArgs e)
    {
        int iRefCustomerFound = clientSearch1.RefCustomer;
        string sXml = Client.GetCustomerInformations(iRefCustomerFound);

        if (!String.IsNullOrWhiteSpace(sXml))
        {
            List<string> listLastName = CommonMethod.GetAttributeValues(sXml, "ALL_XML_OUT/Customer", "LastName");
            List<string> listFirstName = CommonMethod.GetAttributeValues(sXml, "ALL_XML_OUT/Customer", "FirstName");
            List<string> listAccountNumber = CommonMethod.GetAttributeValues(sXml, "ALL_XML_OUT/Customer", "AccountNumber");
            List<string> listBirthDate = CommonMethod.GetAttributeValues(sXml, "ALL_XML_OUT/Customer", "BirthDate");

            tbCompte1.Text = listAccountNumber[0];
            UpdateClientPanel(iRefCustomerFound.ToString(), listLastName[0], listFirstName[0], listBirthDate[0]);
        }
        
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), 
            "CloseSearchClient('"+ tbCompte1.Text + "','" + lblName.Text
            + "','" + lblBirthDate.Text + "','" + "');", true);
    }
    
}
