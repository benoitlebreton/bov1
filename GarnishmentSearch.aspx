﻿<%@ Page Title="Recherche Avis" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="GarnishmentSearch.aspx.cs" Inherits="GarnishmentSearch" %>
<%@ Register Src="~/API/GarnishmentList.ascx" TagName="GarnishmentList" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

    <h2 style="color: #344b56;margin-bottom:10px;text-transform:uppercase">Recherche Avis</h2>
    <asp:GarnishmentList ID="GarnishmentList1" runat="server" />

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" Runat="Server">
</asp:Content>

