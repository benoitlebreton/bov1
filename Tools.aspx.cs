﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Hosting;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using XMLMethodLibrary;

public partial class Tools : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            CheckRights();
            InitTimeDdl();
            BindServicesStatus();
        }
    }

    protected void CheckRights()
    {
        panelServiceLock.Visible = false;
        panelLimits.Visible = false;
        panelGateway.Visible = false;
        panelCobalt.Visible = false;

        string sXmlOut = tools.GetRights(authentication.GetCurrent().sToken, "SC_Tools");
        DataTable dtTabs = new DataTable();
        dtTabs.Columns.Add("tab");

        List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Page", "RC");

        if (listRC.Count > 0 && listRC[0] == "0")
        {
            List<string> listAction = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/Page/Action");
            bool bMessagesMenu = false;

            for (int i = 0; i < listAction.Count; i++)
            {
                List<string> listTag = CommonMethod.GetAttributeValues(listAction[i], "Action", "TagAction");
                List<string> listActionAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ActionAllowed");
                List<string> listViewAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ViewAllowed");

                if (listTag.Count > 0)
                {
                    switch (listTag[0])
                    {
                        case "ServiceLock":
                            if (listViewAllowed.Count > 0 && listViewAllowed[0] == "1")
                            {
                                panelServiceLock.Visible = true;
                                dtTabs.Rows.Add(new object[] { "<li><a href=\"#" + panelServiceLock.ClientID + "\">Blocage service</a></li>" });
                            }
                            else panelServiceLock.Visible = false;
                            break;
                        case "Limits":
                            if (listViewAllowed.Count > 0 && listViewAllowed[0] == "1")
                            {
                                panelLimits.Visible = true;
                                dtTabs.Rows.Add(new object[] { "<li><a href=\"#" + panelLimits.ClientID + "\">Plafonds</a></li>" });
                            }
                            break;
                        case "GatewayMon":
                            if (listViewAllowed.Count > 0 && listViewAllowed[0] == "1")
                            {
                                panelGateway.Visible = true;
                                dtTabs.Rows.Add(new object[] { "<li><a href=\"#" + panelGateway.ClientID + "\" onclick=\"$('#" + btnGatewayRefreshMonitoring.ClientID + "').click();\">Gateway</a></li>" });
                            }
                            if (listViewAllowed.Count > 0 && listViewAllowed[0] == "1")
                            {
                                panelOnUs.Visible = true;
                                dtTabs.Rows.Add(new object[] { "<li><a href=\"#" + panelOnUs.ClientID + "\" onclick=\"$('#" + btnOnUsRefreshMonitoring.ClientID + "').click();\">ON US</a></li>" });
                            }
                            if (listViewAllowed.Count > 0 && listViewAllowed[0] == "1")
                            {
                                panelCobalt.Visible = true;
                                dtTabs.Rows.Add(new object[] { "<li><a href=\"#" + panelCobalt.ClientID + "\">COBALT</a></li>" });
                            }
                            break;
                        case "3DSMon":
                            if (listViewAllowed.Count > 0 && listViewAllowed[0] == "1")
                            {
                                panel3DS.Visible = true;
                                dtTabs.Rows.Add(new object[] { "<li><a href=\"#" + panel3DS.ClientID + "\" onclick=\"$('#" + btn3DSRefreshMonitoring.ClientID + "').click();\">3DS</a></li>" });
                            }
                            break;
                        case "SMSMon":
                            if (listViewAllowed.Count > 0 && listViewAllowed[0] == "1")
                            {
                                panelSMS.Visible = true;
                                dtTabs.Rows.Add(new object[] { "<li><a href=\"#" + panelSMS.ClientID + "\" onclick=\"$('#" + btnSMSRefreshMonitoring.ClientID + "').click();\">SMS</a></li>" });
                            }
                            break;
                        case "OnUsMon":
                            if (listViewAllowed.Count > 0 && listViewAllowed[0] == "1")
                            {
                                panelSMS.Visible = true;
                                dtTabs.Rows.Add(new object[] { "<li><a href=\"#" + panelOnUs.ClientID + "\" onclick=\"$('#" + btnOnUsRefreshMonitoring.ClientID + "').click();\">ON US</a></li>" });
                            }
                            break;
                    }
                }
            }

            rptTabs.DataSource = dtTabs;
            rptTabs.DataBind();

        }
        else Response.Redirect("Default.aspx", true);
    }

    protected void InitTimeDdl()
    {
        DataTable dtTime = new DataTable();
        dtTime.Columns.Add("time");
        for (int i = 0; i < 24; i++)
        {
            string sTime = i.ToString();
            if (sTime.Length < 2)
                sTime = "0" + sTime;
            dtTime.Rows.Add(new object[] { sTime });
        }

        ddlHourFromProd.DataSource = dtTime;
        ddlHourToProd.DataSource = dtTime;
        ddlHourFromTest.DataSource = dtTime;
        ddlHourToTest.DataSource = dtTime;
        ddlHourFromProd.DataBind();
        ddlHourToProd.DataBind();
        ddlHourFromTest.DataBind();
        ddlHourToTest.DataBind();

        dtTime = new DataTable();
        dtTime.Columns.Add("time");
        for (int i = 0; i < 60; i++)
        {
            string sTime = i.ToString();
            if (sTime.Length < 2)
                sTime = "0" + sTime;
            dtTime.Rows.Add(new object[] { sTime });
        }

        ddlMinFromProd.DataSource = dtTime;
        ddlMinToProd.DataSource = dtTime;
        ddlMinFromTest.DataSource = dtTime;
        ddlMinToTest.DataSource = dtTime;
        ddlMinFromProd.DataBind();
        ddlMinToProd.DataBind();
        ddlMinFromTest.DataBind();
        ddlMinToTest.DataBind();
    }

    protected void BindServicesStatus()
    {
        string sLabel;
        string sType;
        int iAvailableTest;
        int iAvailableProd;
        bool bTestOffline = false;
        bool bProdOffline = false;
        string sTitle;
        string sMessage;

        DataTable dt = new DataTable();
        dt.Columns.Add("Label");
        dt.Columns.Add("Type");
        dt.Columns.Add("AvailableTest");
        dt.Columns.Add("AvailableProd");
        dt.Columns.Add("Title");
        dt.Columns.Add("Message");

        sLabel = "Mon Compte-Nickel";
        sType = "HMB";

        GetServiceStatus(sType, out iAvailableTest, out iAvailableProd, out sTitle, out sMessage);
        DataRow row = dt.NewRow();
        row["Label"] = sLabel;
        row["Type"] = sType;
        row["AvailableTest"] = iAvailableTest;
        row["AvailableProd"] = iAvailableProd;
        row["Title"] = sTitle;
        row["Message"] = sMessage;
        GetServiceStatusOffline(out bTestOffline, out bProdOffline, out sTitle, out sMessage);
        if (bTestOffline || bProdOffline)
        {
            if (bTestOffline)
                row["AvailableTest"] = -1;
            if (bProdOffline)
                row["AvailableProd"] = -1;

            row["Title"] = sTitle;
            row["Message"] = sMessage;
        }

        dt.Rows.Add(row);

        sLabel = "Inscription borne";
        sType = "MPAD";
        GetServiceStatus(sType, out iAvailableTest, out iAvailableProd, out sTitle, out sMessage);
        row = dt.NewRow();
        row["Label"] = sLabel;
        row["Type"] = sType;
        row["AvailableTest"] = iAvailableTest;
        row["AvailableProd"] = iAvailableProd;
        row["Title"] = sTitle;
        row["Message"] = sMessage;
        dt.Rows.Add(row);

        rptServices.DataSource = dt;
        rptServices.DataBind();

        upServiceLock.Update();
    }

    protected void GetServiceStatus(string sType, out int iAvailableTest, out int iAvailableProd, out string sTitle, out string sMessage)
    {
        iAvailableTest = 0;
        iAvailableProd = 0;
        sTitle = "";
        sMessage = "";

        SqlConnection conn = null;
        string sXmlOut = "";

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("WEB.P_GetWebSiteAccess", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN", new XElement("SERVICE", new XAttribute("Type", sType))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if (sXmlOut.Length > 0)
            {
                List<string> listAvailableProd = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/SERVICE", "AvailableProd");
                List<string> listAvailableTest = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/SERVICE", "AvailableTest");

                if (listAvailableTest.Count == 0 || !int.TryParse(listAvailableTest[0].ToString(), out iAvailableTest))
                    iAvailableTest = 0;

                if (listAvailableProd.Count == 0 || !int.TryParse(listAvailableProd[0].ToString(), out iAvailableProd))
                    iAvailableProd = 0;

                List<string> listTitle = CommonMethod.GetTagValues(sXmlOut, "ALL_XML_OUT/SERVICE/TITLE");
                if (listTitle.Count > 0 && listTitle[0].Length > 0)
                    sTitle = listTitle[0];

                List<string> listMessage = CommonMethod.GetTagValues(sXmlOut, "ALL_XML_OUT/SERVICE/MESSAGE");
                if (listMessage.Count > 0 && listMessage[0].Length > 0)
                    sMessage = listMessage[0];
            }
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }
    }
    protected void GetServiceStatusOffline(out bool bTestOffline, out bool bProdOffline, out string sTitle, out string sMessage)
    {
        bTestOffline = false; bProdOffline = false; sMessage = ""; sTitle = "";
        string sMessageProd = "", sMessageTest = "";
        string sTitleProd = "", sTitleTest = "";
        //string sHmbProdPath = System.Web.HttpContext.Current.Request.MapPath(".") + ConfigurationManager.AppSettings["ServiceStatusOffline"].ToString() + "PROD/HMB/";
        //string sHmbTestPath = System.Web.HttpContext.Current.Request.MapPath(".") + ConfigurationManager.AppSettings["ServiceStatusOffline"].ToString() + "TEST/HMB/";
        string sHmbProdPath = Server.MapPath(ConfigurationManager.AppSettings["ServiceStatusOffline"].ToString() + "PROD/HMB/");
        string sHmbTestPath = Server.MapPath(ConfigurationManager.AppSettings["ServiceStatusOffline"].ToString() + "TEST/HMB/");
        bTestOffline = GetOfflineStatus(sHmbTestPath, out sMessageTest, out sTitleTest);
        bProdOffline = GetOfflineStatus(sHmbProdPath, out sMessageProd, out sTitleProd);

        if (bTestOffline)
        {
            sMessage = sMessageTest;
            sTitle = sTitleTest;
        }

        if (bProdOffline)
        {
            sMessage = sMessageProd;
            sTitle = sTitleProd;
        }
    }
    protected bool GetOfflineStatus(string sPath, out string sMessage, out string sTitle)
    {
        bool isOffline = false;
        sMessage = "";
        sTitle = "";

        try
        {
            string[] filePaths = Directory.GetFiles(sPath);

            List<string> lsFileList = new List<string>();
            string sLastFile = "";

            for (int i = 0; i < filePaths.Length; i++)
            {
                lsFileList.Add(filePaths[i]);
            }
            lsFileList.Sort();
            lsFileList.Reverse();
            string sReadText = "";
            sLastFile = (lsFileList.Count > 0) ? lsFileList[0] : "";
            if (sLastFile.Trim().Length > 0)
            {
                sReadText = File.ReadAllText(sLastFile, Encoding.UTF8);
                List<string> lsServiceStatus = CommonMethod.GetAttributeValues(sReadText, "Service", "Status");
                List<string> lsTitleMessage = CommonMethod.GetAttributeValues(sReadText, "Service/Message", "Title");
                List<string> lsMessage = CommonMethod.GetTagValues(sReadText, "Service/Message");

                if (lsServiceStatus.Count > 0 && lsServiceStatus[0].Trim().ToUpper() == "OFF")
                {
                    isOffline = true;
                    sTitle = (lsTitleMessage.Count > 0 && lsTitleMessage[0].Trim().Length > 0) ? lsTitleMessage[0] : "";
                    sMessage = (lsMessage.Count > 0 && lsMessage[0].Trim().Length > 0) ? lsMessage[0] : "";
                }
            }
        }
        catch (Exception ex)
        {
            tools.LogToFile(ex.ToString(), "GetOfflineStatus");
        }

        return isOffline;
    }

    protected string GetStatusLabel(int iAvailable)
    {
        string sStatusLabel = "";

        switch (iAvailable)
        {
            case 1:
                sStatusLabel = "<span class='service-active'>Actif</span>";
                break;
            case 2:
                sStatusLabel = "<span class='service-active-without-operation'>Actif (sans opération)</span>";
                break;
            case 3:
                sStatusLabel = "<span class='service-active-without-SAB'>Actif (SAB down)</span>";
                break;
            case -1:
                sStatusLabel = "<span class='service-inactive-without-BDD'>Inactif (sans BDD)</span>";
                break;
            default:
            case 0:
                sStatusLabel = "<span class='service-inactive'>Inactif</span>";
                break;
        }

        return sStatusLabel;
    }
    protected string GetMessageLink(string sRef)
    {
        return (sRef.Length > 0) ? "<span onclick=\"MessageToHTML($('#title-" + sRef + "').val(), $('#message-" + sRef + "').val());\">Voir le message</span>" : "";
    }

    protected void btnSaveChanges_Click(object sender, EventArgs e)
    {
        SetStatus(hdnType.Value, rblAvailableProd.SelectedValue, rblAvailableTest.SelectedValue, txtTitle.Text, txtMessage.Text);
        if (hdnType.Value == "HMB") // && (rblAvailableProd.SelectedValue.Trim() == "-1" || rblAvailableTest.SelectedValue.Trim() == "-1"))
            SetOfflineStatus(rblAvailableProd.SelectedValue, rblAvailableTest.SelectedValue, txtTitle.Text, txtMessage.Text);
        BindServicesStatus();
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "HideEditPopupKey", "HideEditPopup();", true);
    }
    protected bool SetOfflineStatus(string sAvailableProd, string sAvailableTest, string sTitle, string sMessage)
    {
        bool isOK = false;
        //string sHmbProdPath = System.Web.HttpContext.Current.Request.MapPath(".") + ConfigurationManager.AppSettings["ServiceStatusOffline"].ToString()+"PROD/HMB/";
        //string sHmbDevPath = System.Web.HttpContext.Current.Request.MapPath(".") + ConfigurationManager.AppSettings["ServiceStatusOffline"].ToString()+ "TEST/HMB/";
        string sHmbProdPath = Server.MapPath(ConfigurationManager.AppSettings["ServiceStatusOffline"].ToString() + "TEST/HMB/");
        string sHmbDevPath = Server.MapPath(ConfigurationManager.AppSettings["ServiceStatusOffline"].ToString() + "TEST/HMB/");

        string sFileName = DateTime.Now.ToString("yyyyMMddHHmmss");

        if (sAvailableProd.Trim() == "-1") isOK = WriteOfflineFile(sHmbProdPath, sFileName, sTitle, sMessage);
        else ArchiveOfflineFiles(sHmbProdPath);

        if (sAvailableTest.Trim() == "-1") isOK = WriteOfflineFile(sHmbDevPath, sFileName, sTitle, sMessage);
        else ArchiveOfflineFiles(sHmbDevPath);

        return isOK;
    }
    protected bool WriteOfflineFile(string sPath, string sFileName, string sTitle, string sMessage)
    {
        bool isOK = true;
        string sXml = new XElement("Service", new XAttribute("Status", "OFF"),
                            new XElement("Message", new XAttribute("Title", sTitle), sMessage)).ToString();

        try
        {
            //Sauvegarde des fichiers existant
            ArchiveOfflineFiles(sPath);

            //Création Fichier
            using (StreamWriter sw = File.CreateText(sPath + sFileName + ".txt"))
            {
                sw.WriteLine(sXml);
                sw.Dispose();
                sw.Close();
            }
        }
        catch (Exception ex)
        {
            isOK = false;
            tools.LogToFile(ex.ToString(), "WriteOfflineFile");
        }

        return isOK;
    }
    protected void ArchiveOfflineFiles(string sPath)
    {
        try
        {
            //if (File.Exists(sPath))
            //File.Move(sPath + sFileName, sPath + "History/" + sFileName);

            //Sauvegarde des fichiers existants
            string[] filePaths = Directory.GetFiles(sPath);
            for (int i = 0; i < filePaths.Length; i++)
            {
                string sHistoryFilename = Path.GetFileName(filePaths[i]);
                string sHistoryFilePath = sPath + "History/" + sHistoryFilename;
                File.Move(filePaths[i], sHistoryFilePath);
            }
        }
        catch (Exception ex)
        {
            tools.LogToFile(ex.ToString(), "ArchiveOfflineFiles");
        }
    }

    protected bool SetStatus(string sType, string sAvailableProd, string sAvailableTest, string sTitle, string sMessage) //, string sProdNotAvailableFrom, string sProdNotAvailableTo, string sTestNotAvailableFrom, string sTestNotAvailableTo
    {
        bool bSet = true;
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("WEB.P_SetWebSiteAccess", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);
            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                new XElement("SERVICE",
                    new XAttribute("Type", sType),
                    new XAttribute("AvailableProd", sAvailableProd),
                    new XAttribute("AvailableTest", sAvailableTest),
                    new XElement("MESSAGE",
                        new XAttribute("Title", sTitle),
                        sMessage))).ToString();

            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].ToString();


        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return bSet;
    }
    protected void btnApproveChangeLimits_Click(object sender, EventArgs e)
    {
        if (ApproveChangeLimits())
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "hideBigLoading", "hideBigLoading();AlertMessage('Changements de plafond','<span style=\"color:#000;\">Les changements de plafond ont été effectués.</span>');", true);
        else
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "hideBigLoading", "hideBigLoading();AlertMessage('Changements de plafond','Une erreur est survenue pendant le traitement.<br/> Veuillez réessayer ultérieurement.');", true);
    }
    protected bool ApproveChangeLimits()
    {
        bool bSet = true;
        SqlConnection conn = null;
        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("BankSI.P_ApproveChangeLimit", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = 300;
            cmd.ExecuteNonQuery();
        }
        catch (Exception e)
        {
            bSet = false;
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }
        return bSet;
    }

    protected void upEdit_Update(object sender, EventArgs e)
    {
        string sRef = hfEditRef.Value;
        string sType = hfEditType.Value;

        DataTable dt = new DataTable();

        rblAvailableProd.Items.Clear();
        rblAvailableTest.Items.Clear();
        if (sType == "HMB")
        {
            rblAvailableProd.Items.Add(new ListItem("Actif", "1"));
            rblAvailableProd.Items.Add(new ListItem("Actif (sans opération)", "2"));
            rblAvailableProd.Items.Add(new ListItem("Actif (SAB down)", "3"));
            rblAvailableProd.Items.Add(new ListItem("Inactif", "0"));
            rblAvailableProd.Items.Add(new ListItem("Inactif (sans BDD)", "-1"));

            rblAvailableTest.Items.Add(new ListItem("Actif", "1"));
            rblAvailableTest.Items.Add(new ListItem("Actif (sans opération)", "2"));
            rblAvailableTest.Items.Add(new ListItem("Actif (SAB down)", "3"));
            rblAvailableTest.Items.Add(new ListItem("Inactif", "0"));
            rblAvailableTest.Items.Add(new ListItem("Inactif (sans BDD)", "-1"));
        }
        else
        {
            rblAvailableProd.Items.Add(new ListItem("Actif", "1"));
            rblAvailableProd.Items.Add(new ListItem("Inactif", "0"));

            rblAvailableTest.Items.Add(new ListItem("Actif", "1"));
            rblAvailableTest.Items.Add(new ListItem("Inactif", "0"));
        }
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "HideEditPopupKey", "ShowEditPopup('" + sRef + "');", true);
        upEdit.Update();
    }

    protected void btnGatewayRefreshMonitoring_Click(object sender, EventArgs e)
    {
        GatewayRefresh();
    }

    protected void btnSaveGatewayModeChange_Click(object sender, EventArgs e)
    {
        lblMessage.Text = "";

        if (Gateway.setGatewayStatus(rblGatewayMode.SelectedValue))
            lblMessage.Text = "Changement de mode de la Gateway enregistré.";
        else
            lblMessage.Text = "Une erreur est survenue.<br/>Le changement de mode de la Gateway n'a pas pu être effectué.";

        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "ToolsAlertMessage", "ToolsAlertMessage();", true);
        GatewayRefresh();
    }

    protected void GatewayRefresh()
    {
        panelGatewayError.Visible = false;
        panelGatewayTransactions.Visible = false;
        //panelGatewayStatus.Visible = false;

        //panelGatewayChangeMode.Visible = false;
        //panelGatewayNbTrxToInjectInSab.Visible = false;
        //panelGatewayNbAccounts.Visible = false;
        //PanelGatewayNbPositiveAccounts.Visible = false;
        //panelGatewayLastBalanceRefreshDate.Visible = false;

        Gateway.GatewayStatus _gatewayStatus = new Gateway.GatewayStatus();
        DataTable dtGatewayStatusDetails = new DataTable();

        panelGatewayDetails.CssClass = "";
        bool isGatewayStatusOk = Gateway.getGatewayStatus(out _gatewayStatus, out dtGatewayStatusDetails);
        panelGatewayStatus.Visible = true;

        rptGatewayStatusDetails.DataSource = dtGatewayStatusDetails;
        rptGatewayStatusDetails.DataBind();

        lblGatewayStatus.Text = Gateway.getStatusLabel(_gatewayStatus.Status, _gatewayStatus.StatusUntil);
        lblGatewayCurrentMode.Text = Gateway.getModeLabel(_gatewayStatus.CurrentMode);
        hfGatewayCurrentMode.Value = _gatewayStatus.CurrentMode;
        hfGatewayRequestedMode.Value = _gatewayStatus.RequestedMode;
        lblGatewayCurrentModeConfirmation.Text = Gateway.getModeLabel(_gatewayStatus.CurrentMode);
        panelGatewayDelegationChangement.Visible = false;
        if (_gatewayStatus.CurrentMode.Trim().ToUpper() == "D")
            panelGatewayDelegationChangement.Visible = true;
        upGatewatModeChange.Update();

        ListItem lGatewayStatus = rblGatewayMode.Items.FindByValue(_gatewayStatus.RequestedMode);
        rblGatewayMode.ClearSelection();
        if (lGatewayStatus != null)
        {
            lGatewayStatus.Selected = true;
            panelGatewayChangeMode.Visible = true;
        }

        lblNbTrxToInjectInSab.Text = _gatewayStatus.NbTrxToInjectInSab.ToString();
        //if (_gatewayStatus.NbTrxToInjectInSab > 0)
        //panelGatewayNbTrxToInjectInSab.Visible = true;

        lblGatewayNbAccounts.Text = _gatewayStatus.NbAccounts.ToString();
        //if (_gatewayStatus.NbAccounts > 0)
        //panelGatewayNbAccounts.Visible = true;

        lblGatewayNbPositiveAccounts.Text = _gatewayStatus.NbPositiveAccounts.ToString();
        //if (_gatewayStatus.NbPositiveAccounts > 0)
        //PanelGatewayNbPositiveAccounts.Visible = true;

        if (_gatewayStatus.LastBalanceRefreshDate != null && _gatewayStatus.LastBalanceRefreshDate.ToString().Trim().Length > 0)
        {
            lblGatewayLastBalanceRefreshDate.Text = _gatewayStatus.LastBalanceRefreshDate;
            //panelGatewayLastBalanceRefreshDate.Visible = true;
        }

        if (isGatewayStatusOk)
            panelGatewayDetails.CssClass = "ErrorValues";


        int iTotalTrx; int iLast10Trx;
        DataTable dtGatewayLastTransactions = new DataTable();
        if (Gateway.getGatewayLastTransactions(out iTotalTrx, out iLast10Trx, out dtGatewayLastTransactions))
        {
            if (dtGatewayLastTransactions.Rows.Count > 0)
            {
                panelGatewayTransactions.Visible = true;
                lblNbTrxTotal.Text = iTotalTrx.ToString();
                lblTrxLast10Minutes.Text = iLast10Trx.ToString();
                rptGatewayMonitoring.DataSource = dtGatewayLastTransactions;
                rptGatewayMonitoring.DataBind();
            }
        }
        else { panelGatewayError.Visible = true; }
    }
    protected string getGatewayStatusDetailsVisibility(string sRow)
    {
        int iRow;
        string sClass = "";
        if (int.TryParse(sRow, out iRow) && iRow > 3)
        {
            sClass = "notVisible";
        }

        return sClass;
    }

    protected void btnRefreshLastImportedSabFiles_Click(object sender, EventArgs e)
    {
        RefreshLastImportedSabFiles();
    }

    protected void RefreshLastImportedSabFiles()
    {
        rptLastImportedSabFiles.DataSource = Gateway.getLastImportedSabFiles();
        rptLastImportedSabFiles.DataBind();
        upLastImportedSabFiles.Update();
    }

    protected void btnForceSabFileImport_Click(object sender, EventArgs e)
    {
        bool isOK = Gateway.importSabFiles();
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "ImportSabFilesKey", "hideLoading();refreshSabImportedFiles();", true);
    }

    protected void btn3DSRefreshMonitoring_Click(object sender, EventArgs e)
    {
        Refresh3DS();
    }
    protected void Refresh3DS()
    {
        // web.P_3DSecureLogs
        DataTable dt = new DataTable();
        getLast3DSExchanges(30, out dt);
        rpt3DSLastExchanges.DataSource = dt;
        rpt3DSLastExchanges.DataBind();
    }
    protected bool getLast3DSExchanges(int nbLogs, out DataTable dt3DSLastExchanges)
    {
        bool isOK = false;
        dt3DSLastExchanges = new DataTable();
        SqlConnection conn = null;
        authentication auth = authentication.GetCurrent();

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("WEB.P_3DSecureLogs", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@NbLogs", SqlDbType.Int);
            cmd.Parameters["@NbLogs"].Value = nbLogs;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt3DSLastExchanges);
        }
        catch (Exception e)
        {
            isOK = false;
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return isOK;
    }
    protected string get3DSCssStatus(string sRC)
    {
        string sStyle = "";
        if (sRC.Trim() != "0")
            sStyle = "color:orange; font-weight:bold;";

        return sStyle;
    }

    protected bool getSmsWay(out string sCurrentWay, out string sAllowedWay)
    {
        bool isOK = false;
        sCurrentWay = "";
        sAllowedWay = "";
        SqlConnection conn = null;
        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_GetSMSWay", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@CurrentWay", SqlDbType.VarChar, 100);
            cmd.Parameters.Add("@AllowedWay", SqlDbType.VarChar, -1);
            cmd.Parameters["@CurrentWay"].Direction = ParameterDirection.Output;
            cmd.Parameters["@AllowedWay"].Direction = ParameterDirection.Output;
            cmd.ExecuteNonQuery();
            sCurrentWay = cmd.Parameters["@CurrentWay"].Value.ToString();
            sAllowedWay = cmd.Parameters["@AllowedWay"].Value.ToString();

            isOK = true;
        }
        catch (Exception e)
        {
            isOK = false;
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return isOK;
    }

    protected bool setSmsWay(string sSmsWay)
    {
        bool isOK = false;
        SqlConnection conn = null;
        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_SetSMSWay", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@NewWay", SqlDbType.VarChar, 100);
            cmd.Parameters["@NewWay"].Value = sSmsWay;
            cmd.ExecuteNonQuery();

            isOK = true;
        }
        catch (Exception e)
        {
            isOK = false;
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return isOK;
    }

    protected void btnSMSRefreshMonitoring_Click(object sender, EventArgs e)
    {
        manageSmsWay();
    }

    protected void manageSmsWay()
    {
        string sAllowedWay = "", sCurrentWay = "";
        getSmsWay(out sCurrentWay, out sAllowedWay);
        lblSmsCurrentProvider.Text = sCurrentWay;
        rblSmsProvider.Visible = false;
        rblSmsProvider.Items.Clear();
        List<string> listWAYS = CommonMethod.GetTags(sAllowedWay, "WAYS/WAY");
        for (int i = 0; i < listWAYS.Count; i++)
        {
            List<string> listWay = CommonMethod.GetTagValues(listWAYS[i], "WAY");
            if (listWay.Count > 0 && listWay[0].Trim().Length > 0)
                rblSmsProvider.Items.Add(new ListItem(listWay[0], listWay[0]));
        }

        if (rblSmsProvider.Items.Count > 1)
        {
            rblSmsProvider.Visible = true;
            rblSmsProvider.SelectedIndexChanged += rblSmsProvider_SelectedIndexChanged;
            for (int i = 0; i < rblSmsProvider.Items.Count; i++)
            {
                if (rblSmsProvider.Items[i].Value == sCurrentWay)
                    rblSmsProvider.Items[i].Selected = true;
            }
        }
    }

    protected void rblSmsProvider_SelectedIndexChanged(object sender, EventArgs e)
    {
        setSmsWay(rblSmsProvider.SelectedValue);
        manageSmsWay();
    }

    protected void btnSaveSmsWayChange_Click(object sender, EventArgs e)
    {
        setSmsWay(rblSmsProvider.SelectedValue);
        manageSmsWay();
    }

    protected void OnUsRefresh()
    {
        rblOnUsStatus.Visible = false;
        panelOnUsStatus.Visible = false;

        authentication auth = authentication.GetCurrent();

        OnUs _OnUs = null;
        OnUs.GetGlobalStatus(auth.sToken, out _OnUs);

        if (_OnUs != null)
        {
            if (_OnUs.status != null)
            {
                rblOnUsStatus.Visible = true;
                rblOnUsStatus.SelectedValue = (_OnUs.status == true) ? "START" : "STOP";
                lblOnUsCurrentStatusConfirmation.Text = rblOnUsStatus.SelectedItem.Text;
                hfOnUsCurrentStatus.Value = rblOnUsStatus.SelectedValue;
                upOnUsStatusChange.Update();
            }

            if (_OnUs.lastTransactions != null && _OnUs.lastTransactions.Rows.Count > 0)
            {
                rptOnUsMonitoring.DataSource = _OnUs.lastTransactions;
                rptOnUsMonitoring.DataBind();
            }

            panelOnUsStatus.Visible = true;
            
        }

        DataTable dt = new DataTable("dtCommunicationModelList");
        if (OnUs.GetOnUsModelCommunicationList(out dt))
        {
            ddlCommunicationModelList.DataTextField = "ModelDescription";
            ddlCommunicationModelList.DataValueField = "Model";
            ddlCommunicationModelList.DataSource = OnUs.GetModelCommunicationListByKind(dt, "SMS");
            ddlCommunicationModelList.DataBind();
            upOnUsSms.Update();
        }

        mwccOnUsSms.BindSplittedMessageLengthCheck();

        upOnUs.Update();
    }
    protected void btnOnUsRefreshMonitoring_Click(object sender, EventArgs e)
    {
        OnUsRefresh();
    }

    protected void btnSaveOnUsStatusChange_Click(object sender, EventArgs e)
    {
        /*'<ALL_XML_IN><ONUS CashierToken = "DBTREATMENT" Action = "START" /></ALL_XML_IN>*/
        string sXmlIn = new XElement("ALL_XML_IN", new XElement("ONUS", new XAttribute("CashierToken", authentication.GetCurrent().sToken), new XAttribute("Action", rblOnUsStatus.SelectedValue))).ToString();
        string sXmlOut = "";
        if(!OnUs.setGatewayOnUsStatus(sXmlIn, out sXmlOut))
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "hideBigLoading", "hideBigLoading();AlertMessage('Changement statut On Us','<span style=\"color:#000;\">Une erreur est survenue.<br/>Détails : " + HttpUtility.JavaScriptStringEncode(sXmlIn) + "</span>');", true);
        }
        //else
        //{
        //    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "hideBigLoading", "hideBigLoading();AlertMessage('Changement statut On Us','<span style=\"color:#000;\">Les changements de plafond ont été effectués.</span>');", true);
        //}

        OnUsRefresh();
    }

    protected void btnOnUsSmsApplyCommunicationModel_Click(object sender, EventArgs e)
    {
        mwccOnUsSms.Message = ddlCommunicationModelList.SelectedValue;
        //mwccOnUsSms.BindMessageLengthCheck(ddlCommunicationModelList.SelectedValue);
        mwccOnUsSms.BindSplittedMessageLengthCheck();
    }

    protected void btnOnUsSendSMS_Click(object sender, EventArgs e)
    {
        string sMessage = "";

        if (!string.IsNullOrWhiteSpace(mwccOnUsSms.Message)) {
            if(tools.SendSMSToShop(mwccOnUsSms.Message, rblOnUsSmsRecipientGroup.SelectedValue))
            {
                sMessage = "<span style=\"color:green;\">Le message a été envoyé.</span>";
                mwccOnUsSms.Message = "";
            }
            else { sMessage = "<span style=\"color:red;\">Une erreur est survenue.<br/>Le message n&apos;a pas pu être envoyé</span>"; }
        }
        else { sMessage = "<span style=\"color:red;\">Aucun message à envoyer.</span>"; }

        OnUsRefresh();
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "hideBigLoading", "hideBigLoading();AlertMessage('Changement statut On Us','"+sMessage+"');", true);
    }

    protected void btnAddCobaltPointOfSale_Click(object sender, EventArgs e)
    {
        int iIdPro;
        string sIdPro = txtIdPRO.Text.Replace("-","");
        string sFullIdPRO = "PRO-" + txtIdPRO.Text;
        if (sIdPro.Length == 7 && int.TryParse(sIdPro, out iIdPro))
        {
            if(Agency.AddCobaltPointOfSale("PRO" + sIdPro))
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "hideBigLoading", "hideBigLoading();AlertMessage('Ajout Point de Vente COBALT','<span style=\"color:green\">Point de vente COBALT ("+ sFullIdPRO + ") créé avec succès</span>');", true);
            else
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "hideBigLoading", "hideBigLoading();AlertMessage('Ajout Point de Vente COBALT','<span style=\"color:red\">Une erreur est survenue.</span>');", true);
        }
        else
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "hideBigLoading", "hideBigLoading();AlertMessage('Ajout Point de Vente COBALT','<span style=\"color:red\">Identifiant PRO non valide.</span>');", true);
    }
}