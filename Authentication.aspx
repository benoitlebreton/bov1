﻿<%@ Page Title="Authentification - BO Compte-Nickel" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Authentication.aspx.cs" Inherits="Authentication" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <script type="text/javascript">
        $(function () {
            setFocus();
        });

        function setFocus() {
            if ($('#<%=panelLoginNotSaved.ClientID%>') != null && $('#<%=panelLoginNotSaved.ClientID%>').is(':visible')) {
                $('#<%=LoginTxt.ClientID%>').focus();
            }
            else {
                $('#<%=PassTxt.ClientID%>').focus();
            }
        }

        function changeLogin() {
            __doPostBack("deleteLoginCookie", "");
        }

        function forgotPassword() {
            showForgotPassword();
        }

        function showForgotPassword() {
            $('#emailError_pwdForgotten').html('');
            $('#div_pwdForgotten').show();
            $('#dialog-message').dialog({
                resizable: false,
                draggable: false,
                modal: true,
                minWidth: 500,
                title: "Mot de passe oublié",
                buttons: [
                    { text: 'Annuler', click: function () { $(this).dialog('close'); } },
                    {
                        text: 'Valider', click: function () {
                            $('#emailError_pwdForgotten').html('');
                            if (checkEmail_pwdForgotten()) {
                                $(this).dialog('close');
                                __doPostBack("forgotPassword", $('#<%= txtEmail_pwdForgotten.ClientID %>').val());
                            }
                            else
                                $('#emailError_pwdForgotten').html('e-mail non valide');
                        }
                    }
                ]
            });
        }

        function showMessage(title, message, redirect) {
            var sTitle = "Message";
            if (message != null && message.trim().length > 0)
                $('#<%=lblInfoMessage.ClientID%>').html(message.trim());
                if (title != null && title.trim().length > 0)
                    sTitle = title;

                $('#dialog-message').dialog({
                    resizable: false,
                    draggable: false,
                    modal: true,
                    minWidth: 500,
                    title: sTitle,
                    buttons: [{ text: 'Fermer', click: function () { $(this).dialog('close'); if (redirect == "true") { document.location.href = "Default.aspx"; } else { setFocus(); } } }]
                });

        }

        function checkEmail_pwdForgotten() {
            var isOK = false;
            var email = $('#<%= txtEmail_pwdForgotten.ClientID %>').val().trim();

            if (email.length > 0) {
                var email_regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
                isOK = email_regex.test(email);
            }
            
            return isOK;
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

    <asp:UpdatePanel ID="UpdatePanelSend" runat="server" width="100%">
        <ContentTemplate>
            <asp:Panel ID="panelAuthentication" runat="server" DefaultButton="btnConnection" style="position:relative; width:100%">
                <div style="margin:auto; width:90%">
                    <asp:UpdateProgress ID="UpdateProgressSend" runat="server" AssociatedUpdatePanelID="UpdatePanelSend">
                        <ProgressTemplate>
                            <div style="width: 100%; height: 100%; text-align: right;position:absolute; top:-5px; right:50px; ">
                                <img src="./Styles/Img/loading.gif" alt="loading" width="30px" height="30px" />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
            
                    <div style="padding-bottom:5px; margin:auto; width:100%">
                        <h2 style="color:#344b56; margin-bottom:10px; text-transform:uppercase">
                            Authentification
                        </h2>
                        <div style="border: 2px solid #f57527;border-radius:8px; padding: 0;">
                            <div style="margin:auto; padding:10px">
                                <div style="display:table; margin:auto; width:60%">
                                    <div style="display:table-row">
                                        <div style="display:table-cell; width: 30%; text-align:left; padding:10px; font-size: 16px;">
                                            <asp:Label ID="LoginLbl" AssociatedControlID="LoginTxt" runat="server"><%=Resources.res.LoginID %></asp:Label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="Validation" 
                                                ControlToValidate="LoginTxt" ForeColor="Red" CssClass="reqStarStyle">*</asp:RequiredFieldValidator>    
                                        </div>
                                        <div style="display:table-cell; width: 70%; text-align:left; padding:10px; font-size: 16px;">
                                            <asp:Panel ID="panelLoginNotSaved" runat="server" style="margin-top:5px">
                                                <asp:TextBox ID="LoginTxt" runat="server" autocomplete="off" Width="300px"></asp:TextBox>
                                                <div style="margin-top:5px">
                                                    <asp:CheckBox ID="cbSaveClientID" runat="server" /><label for="<%=cbSaveClientID.ClientID%>" style="font-size:0.9em"> Enregistrer mon identifiant</label>
                                                </div>
                                            </asp:Panel>
                                            <asp:Panel ID="panelLoginSaved" runat="server" Visible="false">
                                                <asp:Label ID="LoginTxtLbl" runat="server" style="font-family:Arial; color:#f57527"></asp:Label>
                                                <div style="margin-top:2px">
                                                    <a class="linkNoStyle" style="font-size:14px; " onclick="changeLogin();">se connecter avec un autre identifiant ?</a>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                    <div style="display:table-row">
                                        <div style="display:table-cell; width: 30%; text-align:left; padding:10px; font-size: 16px;">
                                            <asp:Label ID="PassLbl" AssociatedControlID="PassTxt" runat="server"><%=Resources.res.Password %></asp:Label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ValidationGroup="Validation" 
                                                ControlToValidate="PassTxt" ForeColor="Red" CssClass="reqStarStyle">*</asp:RequiredFieldValidator>
                                        </div>
                                        <div style="display:table-cell; width: 70%; text-align:left; padding:10px; font-size: 16px;">
                                            <asp:TextBox ID="PassTxt" runat="server" TextMode="Password" autocomplete="off" ></asp:TextBox>
                                            <div style="margin-top:2px">
                                                <a class="linkNoStyle" style="font-size:14px; " onclick="forgotPassword();">mot de passe oublié ?</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="width:100%; text-align:right;padding:0; margin:0">
                            <div style="margin-top:10px">
                                <asp:Button CssClass="button" ID="btnConnection" runat="server" ValidationGroup="Validation" OnClick="btnConnection_Click" Text="Connexion" />
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <asp:Label ID="lblAuthenticationError" runat="server" ForeColor="Red"></asp:Label>

            <div id="dialog-message" style="display:none">
                <asp:Label ID="lblInfoMessage" runat="server" style="font-weight:bold"></asp:Label>
                <div id="div_pwdForgotten" style="display:none">
                    <div>
                        Veuillez saisir votre e-mail d'identification
                    </div>
                    <asp:TextBox ID="txtEmail_pwdForgotten" runat="server" MaxLength="500" style="width:400px"></asp:TextBox>
                    <div style="padding:10px 0; height:20px">
                        <span id="emailError_pwdForgotten" style="color:red; font-weight:bold"></span>
                    </div>
                </div>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>