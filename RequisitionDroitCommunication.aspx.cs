﻿using Ghostscript.NET.Rasterizer;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Hosting;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using XMLMethodLibrary;

public partial class RequisitionDroitCommunication : System.Web.UI.Page
{
    protected int _iRefRJDC { get { return (ViewState["RefRJDC"] != null) ? int.Parse(ViewState["RefRJDC"].ToString()) : 0; } set { ViewState["RefRJDC"] = value; } }
    protected int _iRefCustomer { get { return (ViewState["RefCustomer"] != null) ? int.Parse(ViewState["RefCustomer"].ToString()) : 0; } set { ViewState["RefCustomer"] = value; } }
    protected string _ServerFilePath = ConfigurationManager.AppSettings["RJ_DC_FilePath"].ToString();
    private string _sUploadedFileName { get { return (ViewState["UploadedFileName"] != null) ? ViewState["UploadedFileName"].ToString() : ""; } set { ViewState["UploadedFileName"] = value; } }
    public bool FormEditable { get { return (ViewState["FormEditable"] != null) ? bool.Parse(ViewState["FormEditable"].ToString()) : false; } set { ViewState["FormEditable"] = value; } }
    protected bool bFileUploaded { get { return (ViewState["FileUploaded"] != null) ? bool.Parse(ViewState["FileUploaded"].ToString()) : false; } set { ViewState["FileUploaded"] = value; } }
    protected bool bFileSigned { get { return (ViewState["FileSigned"] != null) ? bool.Parse(ViewState["FileSigned"].ToString()) : false; } set { ViewState["FileSigned"] = value; } }
    protected bool bSignatureRequired { get { return (ViewState["NoSignatureReq"] != null) ? bool.Parse(ViewState["NoSignatureReq"].ToString()) : false; } set { ViewState["NoSignatureReq"] = value; } }
    private bool _EditablePostTreatment { get { return (Session["RDCEditablePostTreatment"] != null) ? bool.Parse(Session["RDCEditablePostTreatment"].ToString()) : false; } set { Session["RDCEditablePostTreatment"] = value; } }

    protected void Page_Init(object sender, EventArgs e)
    {
        ClientSearch1.ClientFound += new EventHandler(ClientFound);
        clientFileUpload1.ClientFileUploaded += new EventHandler(FileUploaded);
        clientFileUpload1.ClientFileUploadError += new EventHandler(FileUploadError);
        clientFileUpload1.DocumentTypeChanged += new EventHandler(DocumentTypeChanged);
        clientFileUpload1.ServerFileSelected += new EventHandler(ServerFileSelected);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "Init();", true);

        if (!IsPostBack)
        {
            if(Request.Params["ref"] != null)
            {
                int iTmp;
                if (int.TryParse(Request.Params["ref"].ToString(), out iTmp))
                {
                    _iRefRJDC = iTmp;
                }
            }

            CheckRights();

            if (_iRefRJDC != 0)
            {
                FormEditable = false;
                BindRJDCValues(_iRefRJDC);
            }
            else
            {
                FormEditable = true;

                clientFileUpload1.ServerFilePath = _ServerFilePath;

                ddlMultiSelect1.ListChoices = Requisition.GetMotifRequisitionList();
            }
        }
    }

    protected void CheckRights()
    {
        string sXmlOut = tools.GetRights(authentication.GetCurrent().sToken, "SC_RequisitionDCom");

        List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Page", "RC");

        if (listRC.Count == 0 || listRC[0] != "0")
        {
            Response.Redirect("Default.aspx", true);
        }
        else
        {
            List<string> listAction = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/Page/Action");

            for (int i = 0; i < listAction.Count; i++)
            {
                List<string> listTag = CommonMethod.GetAttributeValues(listAction[i], "Action", "TagAction");
                List<string> listActionAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ActionAllowed");
                List<string> listViewAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ViewAllowed");

                if (listTag.Count > 0)
                {
                    switch(listTag[0])
                    {
                        case "RequisitionDroitCommunication":
                            if(listViewAllowed[0] != "1" || (listActionAllowed[0] == "0" && _iRefRJDC == 0))
                                Response.Redirect("Default.aspx", true);
                            break;
                        case "RDCModifyPostTreatment":
                            if (listActionAllowed.Count > 0 && listActionAllowed[0] == "1")
                                _EditablePostTreatment = true;
                            break;
                    }
                }
            }
        }
    }

    protected void FileUploaded(object sender, EventArgs e)
    {
        bFileUploaded = true;
        _sUploadedFileName = sender.ToString();

        if(clientFileUpload1.DocumentType == "REQ" && !bFileSigned && bSignatureRequired)
        {
            InitFileEdit(_sUploadedFileName);
        }
        else SaveFinal();

        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "HideRequisitionsDroitsCommunicationLoading();", true);
    }
    protected void FileUploadError(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "HideRequisitionsDroitsCommunicationLoading();", true);
    }

    protected void InitFileEdit(string sFileName)
    {
        string sCustomerDirPath = ConfigurationManager.AppSettings["NickelDocPath"].ToString();
        string sCustomerWebDirName = tools.GetCustomerWebDirectoryName(_iRefCustomer);
        string sFilePath = HostingEnvironment.MapPath("~" + sCustomerDirPath + "/" + sCustomerWebDirName + "/" + sFileName);
        string sFileOutputPath = HostingEnvironment.MapPath("~" + sCustomerDirPath + "/" + sCustomerWebDirName + "/");

        if (File.Exists(sFilePath))
        {
            try
            {
                ClientFile.RemoveUntreatedDocs(HttpContext.Current.Server.MapPath("~/UploadTmp"), 15);

                string sFileNameNoExt = sFileName.Substring(0, sFileName.LastIndexOf("."));
                string sPath = ConfigurationManager.AppSettings["NickelDocPath"].ToString() + tools.GetCustomerWebDirectoryName(_iRefCustomer) + "/";

                PdfToJpg(sFilePath, sFileOutputPath, sFileNameNoExt);

                if (File.Exists(sFileOutputPath + sFileNameNoExt + ".jpg"))
                {
                    tools.AssetInfos assetInfos = new tools.AssetInfos();
                    assetInfos.template = sPath + sFileNameNoExt + ".jpg";

                    authentication auth = authentication.GetCurrent();
                    List<tools.Asset> listAsset = new List<tools.Asset>();
                    tools.Asset assetCross = new tools.Asset("text", "X", 5, 0, 0, "");
                    tools.Asset assetAgent = new tools.Asset("text", auth.sLastName + " " + auth.sFirstName, 0, 0, 0, "");

                    DateTime utc = DateTime.UtcNow;
                    TimeZoneInfo zone = TimeZoneInfo.FindSystemTimeZoneById("W. Europe Standard Time");
                    DateTime localDateTime = TimeZoneInfo.ConvertTimeFromUtc(utc, zone);
                    tools.Asset assetDate = new tools.Asset("text", localDateTime.ToString("dd/MM/yyyy HH'h'mm"), 10, 0, 0, "");

                    //tools.Asset assetStamp = new tools.Asset("img", "data:image/gif;base64," + tools.ImageFileToBase64(System.Web.HttpContext.Current.Server.MapPath("~/Styles/Img/FPE_tampon.png")), 15, 0, 0, "");
                    tools.Asset assetSign = new tools.Asset("img", "data:image/gif;base64," + tools.ImageFileToBase64(System.Web.HttpContext.Current.Server.MapPath("~/Styles/Img/signature.png")),20, 0, 0, "");

                    listAsset.Add(assetCross);
                    listAsset.Add(assetAgent);
                    listAsset.Add(assetDate);
                   // listAsset.Add(assetStamp);
                    listAsset.Add(assetSign);

                    assetInfos.assets = listAsset;

                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "InitAssets(" + JsonConvert.SerializeObject(assetInfos) + ");ShowPDFEditor();", true);
                }
            }
            catch (Exception e)
            {

            }
        }
    }

    private static void PdfToJpg(string sPdfFilePath, string sOutputPath, string sFileName)
    {
        var xDpi = 200; //set the x DPI
        var yDpi = 200; //set the y DPI
        var pageNumber = 1; // the pages in a PDF document

        using (var rasterizer = new GhostscriptRasterizer()) //create an instance for GhostscriptRasterizer
        {
            rasterizer.Open(sPdfFilePath); //opens the PDF file for rasterizing

            //set the output image(png's) complete path
            var outputPNGPath = Path.Combine(sOutputPath, string.Format("{0}.jpg", sFileName));

            //converts the PDF pages to png's 
            var pdf2JPG = rasterizer.GetPage(xDpi, yDpi, pageNumber);

            //save the jpg's
            pdf2JPG.Save(outputPNGPath, ImageFormat.Jpeg);
        }
    }

    protected void DocumentTypeChanged(object sender, EventArgs e)
    {
        panelEditFile.CssClass = "hidden";
        panelReqInfos.CssClass = "hidden";
        panelDComInfos.CssClass = "hidden";
        string sTagMacro = "";

        switch(clientFileUpload1.DocumentType)
        {
            case "REQ":
                panelReqInfos.CssClass = "";
                panelEditFile.CssClass = "";
                sTagMacro = "RREQ";
                clientFileUpload1.AllowedDocType = ".pdf";
                break;
            case "DCOM":
                panelDComInfos.CssClass = "";
                sTagMacro = "RDCOM";
                clientFileUpload1.AllowedDocType = "";
                break;
        }
        clientFileUpload1.RefreshAllowedDocType();

        if (!String.IsNullOrWhiteSpace(sTagMacro))
            txtResponseContent.Text = AML.GetMacroAnswer(0, sTagMacro);
        else txtResponseContent.Text = "";

        upAdditionalInfos.Update();
        upResponse.Update();
        upEditFile.Update();
    }

    protected void ClientFound(object sender, EventArgs e)
    {
        _iRefCustomer = ClientSearch1.RefCustomer;
        clientFileUpload1.RefCustomer = _iRefCustomer;
        clientFileUpload1.InitInputFiles();

        BindClientGroupList();

        BindClientReleveList(null);
    }

    protected void BindClientGroupList()
    {
        AccountGroupManager1.RefCustomer = _iRefCustomer;
        AccountGroupManager1.Update();
        panelAccountGroupManager.CssClass = "";
        upAdditionalInfos.Update();
    }

    protected void BindClientReleveList(List<string> listFilenameToSelect)
    {
        DataTable dt = GetReleveList();
        if (dt.Rows.Count > 0)
        {
            DataColumn col = new DataColumn("Check", typeof(bool));
            col.DefaultValue = false;
            dt.Columns.Add(col);

            DataTable dtSource = null;
            if (listFilenameToSelect != null)
            {
                dtSource = dt.Clone();

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    bool bCheck = false;
                    if (listFilenameToSelect.Contains(dt.Rows[i]["File"]))
                        bCheck = true;
                    dtSource.Rows.Add(new object[] { dt.Rows[i][0], dt.Rows[i][1], bCheck });
                }
            }
            else dtSource = dt;

            rptReleve.DataSource = dtSource;
            rptReleve.DataBind();
            panelReleve.Visible = true;
        }
        else panelReleve.Visible = false;
        upReleve.Update();
    }

    protected DataTable GetReleveList()
    {
        DataTable dt = Client.FormatDateBankStatementList(Client.GetBankStatementList(_iRefCustomer, _iRefCustomer));

        DataTable dtTmp = dt.Clone();

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (!dt.Rows[i]["Date"].ToString().ToLower().Contains("frais"))
            {
                string sFile = dt.Rows[i]["File"].ToString();

                dtTmp.Rows.Add(new object[] { dt.Rows[i]["Date"], sFile.Substring(sFile.LastIndexOf('/') + 1) });
            }
        }

        return dtTmp;
    }

    protected void rblResponseMethod_SelectedIndexChanged(object sender, EventArgs e)
    {
        panelEmailDetail.Visible = false;
        panelCourrierDetail.Visible = false;

        if (rblResponseMethod.SelectedValue == "email")
        {
            panelEmailDetail.Visible = true;

            UpdateMailSubject();
        }
        else panelCourrierDetail.Visible = true;
    }

    protected void UpdateMailSubject()
    {

    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string sErrorMessage = "";

        // Check if session still valid
        authentication.GetCurrent(true);

        if (CheckForm(out sErrorMessage))
        {
            if (!bFileUploaded)
            {
                bSignatureRequired = false;
                hdnBtnSaveClicked.Value = "1";
                clientFileUpload1.ManualUploadScript();
            }
            else SaveFinal();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage(\"Erreur\", \"" + sErrorMessage + "\");", true);
        }
    }

    protected bool SaveDB(string sXmlIn)
    {
        if(sXmlIn == null)
            sXmlIn = GetFormToXML();
        bool bSaved = false;
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_AddDelCustomerRequisitions", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@XMLIn", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@XMLOut", SqlDbType.VarChar, 8000);

            cmd.Parameters["@XMLIn"].Value = sXmlIn;
            cmd.Parameters["@XMLOut"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@XMLOut"].Value.ToString();

            if(sXmlOut.Length > 0)
            {
                List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "RJ_DC/Doc", "RC");

                if(listRC.Count > 0 && listRC[0] == "0")
                    bSaved = true;
            }
        }
        catch(Exception e)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return bSaved;
    }
    protected string GetFormToXML()
    {
        XElement xml = new XElement("RJ_DC",
                                new XAttribute("Token", authentication.GetCurrent().sToken)
                                , new XAttribute("RefCustomer", _iRefCustomer)
                                , new XElement("DOC",
                                    new XAttribute("Filename", _sUploadedFileName)
                                    , new XAttribute("DocType", clientFileUpload1.DocumentType))
                                );

        // INFOS REQ
        if (clientFileUpload1.DocumentType == "REQ")
        {
            string sReasons = "";
            DataTable dtReasons = ddlMultiSelect1.GetSelectedChoices();
            for (int i = 0; i < dtReasons.Rows.Count; i++)
            {
                sReasons += dtReasons.Rows[i]["Value"] + ";";
            }
            sReasons = sReasons.Substring(0, sReasons.Length - 1);

            xml.Add(new XElement("INFOS",
                        new XAttribute("GardeAVue", (ckbGardeAVue.Checked) ? "1" : "0")
                        , new XAttribute("EmissionDate", txtEmissionDate.Text)
                        , new XAttribute("ReceptionDate", txtReceptionDate.Text)
                        , new XAttribute("NumeroPV", txtPVNumber.Text)
                        , new XAttribute("Reasons", sReasons)
                        , new XAttribute("CloseAccount", (ckbNoClose.Checked) ? "0" : "1")));
        }

        // REPONSE
        XElement xmlDocs = new XElement("DOCS",
                                    new XAttribute("FicheSynthese", (ckbFicheSynthese.Checked) ? "1" : "0"));
        List<string> listReleve = GetSelectedReleveList();
        for (int i = 0; i < listReleve.Count; i++)
        {
            xmlDocs.Add(new XElement("DOC", new XAttribute("Filename", listReleve[i])));
        }

        XElement xmlResp = new XElement("RESPONSE",
                                new XAttribute("ResponseType", rblResponseMethod.SelectedValue)
                                , new XElement("MACRO", txtResponseContent.Text)
                                , xmlDocs);
        switch (rblResponseMethod.SelectedValue)
        {
            case "email":
                xmlResp.Add(new XAttribute("EmailAddress", txtEmailAddress.Text)
                            , new XAttribute("EmailSubject", txtEmailSubject.Text));
                break;
            case "courrier":
                xmlResp.Add(new XAttribute("CourrierTo", txtDest.Text)
                            , new XAttribute("CourrierAddress1", txtAddress1.Text)
                            , new XAttribute("CourrierAddress2", txtAddress2.Text)
                            , new XAttribute("CourrierZipcode", txtZipcode.Text)
                            , new XAttribute("CourrierCity", txtCity.Text));
                break;
        }
        xml.Add(xmlResp);

        xml = new XElement("ALL_XML_IN", xml);

        return xml.ToString();
    }

    protected bool CheckForm(out string sErrorMessage)
    {
        bool bOk = true;
        sErrorMessage = "";
        StringBuilder sbMessage = new StringBuilder();
        int iTmp = 0;

        if(String.IsNullOrWhiteSpace(clientFileUpload1.DocumentType))
            sbMessage.AppendLine("Type de document : champ requis");

        if (_iRefCustomer == 0)
            sbMessage.AppendLine("Client : champ requis");

        // SPECIFIC DATA (DOC TYPE DEPENDANT)
        if(clientFileUpload1.DocumentType == "REQ")
        {
            if(String.IsNullOrWhiteSpace(txtEmissionDate.Text))
                sbMessage.AppendLine("Date émission : champ requis");
            else if(!tools.IsDateValid(txtEmissionDate.Text))
                sbMessage.AppendLine("Date émission : format non valide");
            if (String.IsNullOrWhiteSpace(txtReceptionDate.Text))
                sbMessage.AppendLine("Date réception : champ requis");
            else if (!tools.IsDateValid(txtReceptionDate.Text))
                sbMessage.AppendLine("Date réception : format non valide");
            if (String.IsNullOrWhiteSpace(txtPVNumber.Text))
                sbMessage.AppendLine("Numéro PV : champ requis");
            DataTable dt = ddlMultiSelect1.GetSelectedChoices();
            if(dt.Rows.Count == 0)
                sbMessage.AppendLine("Motif : champ requis");

            //if(!bFileSigned)
                //sbMessage.AppendLine("Réquisition : Veuillez signer le document");
        }
        else if (clientFileUpload1.DocumentType == "DCOM")
        {
            if (String.IsNullOrWhiteSpace(txtDComOrigin.Text))
                sbMessage.AppendLine("Demandeur : champ requis");
        }

        // RESPONSE
        //if (!ckbFicheSynthese.Checked && GetSelectedReleveList().Count == 0)
            //sbMessage.AppendLine("Document attaché : veuillez en sélectionner au moins un");

        if (String.IsNullOrWhiteSpace(rblResponseMethod.SelectedValue))
            sbMessage.AppendLine("Méthode d'envoi : champ requis");
        else
        {
            switch (rblResponseMethod.SelectedValue)
            {
                case "email":
                    if (String.IsNullOrWhiteSpace(txtEmailAddress.Text))
                        sbMessage.AppendLine("Adresse : champ requis");
                    else
                    {
                        string[] arEmail = txtEmailAddress.Text.Split(';');
                        for (int i = 0; i < arEmail.Length; i++)
                        {
                            if (!tools.IsEmailValid(arEmail[i]))
                            {
                                sbMessage.AppendLine("Adresse : format non valide");
                                break;
                            }
                        }
                    }
                    if (String.IsNullOrWhiteSpace(txtEmailSubject.Text))
                        sbMessage.AppendLine("Objet : champ requis");
                    break;
                case "courrier":
                    if (String.IsNullOrWhiteSpace(txtDest.Text))
                        sbMessage.AppendLine("Destinataire : champ requis");
                    if (String.IsNullOrWhiteSpace(txtAddress1.Text))
                        sbMessage.AppendLine("Adresse : champ requis");
                    if (String.IsNullOrWhiteSpace(txtZipcode.Text))
                        sbMessage.AppendLine("Code postal : champ requis");
                    else if (!int.TryParse(txtZipcode.Text, out iTmp))
                        sbMessage.AppendLine("Code postal : format non valide");
                    if (String.IsNullOrWhiteSpace(txtCity.Text))
                        sbMessage.AppendLine("Ville : champ requis");
                    break;
            }
        }

        if(string.IsNullOrWhiteSpace(txtResponseContent.Text))
            sbMessage.AppendLine("Contenu : champ requis");

        if (!String.IsNullOrWhiteSpace(sbMessage.ToString()))
        {
            bOk = false;
            sErrorMessage = sbMessage.ToString().Replace(Environment.NewLine, "<br />");
        }

        return bOk;
    }

    protected List<string> GetSelectedReleveList()
    {
        List<string> listReleve = new List<string>();

        foreach (RepeaterItem item in rptReleve.Items)
        {
            if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
            {
                CheckBox ckb = item.FindControl("ckbSelectReleve") as CheckBox;
                HiddenField hdn = item.FindControl("hdnFilename") as HiddenField;

                if(ckb != null && hdn != null && !String.IsNullOrWhiteSpace(hdn.Value))
                {
                    if (ckb.Checked)
                        listReleve.Add(hdn.Value);
                }
            }
        }

        return listReleve;
    }

    protected void BindRJDCValues(int iRefRJDC)
    {
        string sXmlOut = GetRJDCValues(iRefRJDC);

        if(sXmlOut.Length > 0)
        {
            List<string> listRefCustomer = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/RJ_DC", "RefCustomer");

            int iTmp;
            if (listRefCustomer.Count > 0 && int.TryParse(listRefCustomer[0], out iTmp))
            {
                _iRefCustomer = iTmp;
                btnSave.Visible = false;

                List<string> listFilename = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/RJ_DC/DOC", "FileName");
                List<string> listDocType = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/RJ_DC/DOC", "DocType");
                List<string> listTreatmentDate = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/RJ_DC/INFOS", "TreatmentDate");
                txtTreatmentDate.Text = listTreatmentDate[0];
                panelTreatmentDate.Visible = true;

                if (listDocType.Count > 0)
                {
                    switch (listDocType[0])
                    {
                        case "REQ":
                            lblFileUploadedType.Text = "Réquisition judiciaire";
                            break;
                        case "DCOM":
                            lblFileUploadedType.Text = "Droit de communication";
                            break;
                    }
                }

                if (listFilename.Count > 0)
                {
                    clientFileUpload1.Visible = false;
                    panelClientFileUploaded.Visible = true;

                    string sCustomerDirPath = ConfigurationManager.AppSettings["NickelDocPath"].ToString();
                    string sCustomerWebDirName = tools.GetCustomerWebDirectoryName(_iRefCustomer);
                    hlFileUploaded.Text = listFilename[0];
                    hlFileUploaded.NavigateUrl = "~" + sCustomerDirPath + "/" + sCustomerWebDirName + "/" + listFilename[0];
                }

                ClientSearch1.Visible = false;
                panelClient.Visible = true;
                string sXmlClient = Client.GetCustomerInformations(_iRefCustomer);
                //List<string> listPolit = CommonMethod.GetAttributeValues(sXmlClient, "ALL_XML_OUT/Customer", "Politeness");
                List<string> listFirstName = CommonMethod.GetAttributeValues(sXmlClient, "ALL_XML_OUT/Customer", "FirstName");
                List<string> listLastName = CommonMethod.GetAttributeValues(sXmlClient, "ALL_XML_OUT/Customer", "LastName");
                List<string> listBirthDate = CommonMethod.GetAttributeValues(sXmlClient, "ALL_XML_OUT/Customer", "BirthDate");
                lblClient.Text = string.Format("{0} {1} {2}", listFirstName[0], listLastName[0], listBirthDate[0]);

                if (listDocType.Count > 0 && listDocType[0] == "REQ")
                {
                    List<string> listGardeAVue = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/RJ_DC/INFOS", "GardeAVue");
                    List<string> listEmissionDate = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/RJ_DC/INFOS", "EmissionDate");
                    List<string> listReceptionDate = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/RJ_DC/INFOS", "ReceptionDate");
                    List<string> listNumeroPV = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/RJ_DC/INFOS", "NumeroPV");
                    List<string> listReasons = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/RJ_DC/INFOS", "Reasons");
                    List<string> listCloseAccount = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/RJ_DC/INFOS", "CloseAccount");

                    panelReqInfos.CssClass = "";
                    ckbGardeAVue.Checked = (listGardeAVue[0] == "1") ? true : false;
                    ckbGardeAVue.Attributes["disabled"] = "disabled";
                    txtEmissionDate.Text = listEmissionDate[0];
                    txtEmissionDate.ReadOnly = true;
                    txtReceptionDate.Text = listReceptionDate[0];
                    txtReceptionDate.ReadOnly = true;
                    txtPVNumber.Text = listNumeroPV[0];
                    txtPVNumber.ReadOnly = true;
                    ddlMultiSelect1.ListChoices = Requisition.GetMotifRequisitionList();
                    ddlMultiSelect1.BindListChoiceSelected(listReasons[0]);
                    ddlMultiSelect1.HideEdit = true;
                    ckbNoClose.Checked = (listCloseAccount[0] == "1") ? false : true;
                    ckbNoClose.Attributes["disabled"] = "disabled";

                    BindClientGroupList();
                    AccountGroupManager1.HideEdit = true;
                }   

                List<string> listResponseType = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/RJ_DC/RESPONSE", "ResponseType");
                if (listResponseType.Count > 0)
                {
                    rblResponseMethod.SelectedValue = listResponseType[0];
                    rblResponseMethod.Enabled = false;
                    switch (listResponseType[0])
                    {
                        case "email":
                            List<string> listEmailAddress = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/RJ_DC/RESPONSE", "EmailAddress");
                            List<string> listEmailSubject = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/RJ_DC/RESPONSE", "EmailSubject");

                            panelEmailDetail.Visible = true;
                            txtEmailAddress.Text = listEmailAddress[0];
                            txtEmailAddress.ReadOnly = true;
                            txtEmailSubject.Text = listEmailSubject[0];
                            txtEmailSubject.ReadOnly = true;
                            break;
                        case "courrier":
                            List<string> listCourrierTo = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/RJ_DC/RESPONSE", "CourrierTo");
                            List<string> listCourrierAddress1 = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/RJ_DC/RESPONSE", "CourrierAddress1");
                            List<string> listCourrierAddress2 = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/RJ_DC/RESPONSE", "CourrierAddress2");
                            List<string> listCourrierZipcode = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/RJ_DC/RESPONSE", "CourrierZipcode");
                            List<string> listCourrierCity = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/RJ_DC/RESPONSE", "CourrierCity");

                            panelCourrierDetail.Visible = true;
                            txtDest.Text = listCourrierTo[0];
                            txtDest.ReadOnly = true;
                            txtAddress1.Text = listCourrierAddress1[0];
                            txtAddress1.ReadOnly = true;
                            txtAddress2.Text = listCourrierAddress2[0];
                            txtAddress2.ReadOnly = true;
                            txtZipcode.Text = listCourrierZipcode[0];
                            txtZipcode.ReadOnly = true;
                            txtCity.Text = listCourrierCity[0];
                            txtCity.ReadOnly = true;
                            break;
                    }
                }

                List<string> listMacro = CommonMethod.GetTagValues(sXmlOut, "ALL_XML_OUT/RJ_DC/RESPONSE/MACRO");
                if (listMacro.Count > 0)
                {
                    Regex rgx = new Regex("\r\n?|\n");
                    txtResponseContent.Text = rgx.Replace(listMacro[0], Environment.NewLine);
                }
                txtResponseContent.ReadOnly = true;
                List<string> listFS = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/RJ_DC/RESPONSE/DOCS", "FicheSynthese");
                ckbFicheSynthese.Checked = (listFS[0] == "1") ? true : false;
                ckbFicheSynthese.Attributes["disabled"] = "disabled";
                List<string> listDocs = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/RJ_DC/RESPONSE/DOCS/DOC", "Filename");
                BindClientReleveList(listDocs);

                if(_EditablePostTreatment)
                {
                    ddlMultiSelect1.HideEdit = false;
                    ckbNoClose.Attributes.Remove("disabled");
                    panelSavePostTreatment.Visible = true;
                }
            }
        }
    }

    protected string GetRJDCValues(int iRefRJDC)
    {
        return Requisition.GetRJDCInfos(iRefRJDC, 0);
        
        //return "<ALL_XML_OUT>  <RJ_DC Token=\"0X56F43AE1E9595A9980A220B6533AE5936F7B4711\" RefCustomer=\"25\">    <DOC Filename=\"Test_David_OA2.pdf\" DocType=\"REQ\" />    <INFOS TreatmentDate=\"27/01/2017\" GardeAVue=\"1\" EmissionDate=\"09/01/2017\" ReceptionDate=\"25/01/2017\" NumeroPV=\"00000\" Reasons=\"ddp;aes\" CloseAccount=\"1\" />    <RESPONSE ResponseType=\"email\" EmailAddress=\"david.bouly@compte-nickel.fr\" EmailSubject=\"Test\">      <MACRO>Bonjour,En réponse à votre réquisition judiciaire, je vous prie de bien vouloir trouver ci-après les éléments dont nous disposons sur l'individu que vous référencez à savoir, le dossier d'ouverture de compte avec notamment sa pièce d'identité et tous les flux relatifs à son Compte-Nickel.SI BESOIN, MERCI DE REVENIR VERS NOUS UNE FOIS VOS INVESTIGATIONS TERMINÉES POUR QUE NOUS PUISSIONS CLÔTURER LE COMPTE.Je me tiens à votre disposition pour tout complément d'information.IMPORTANT : Afin de pouvoir ouvrir la pièce jointe vous devez disposer d'un accès à internet afin d'ouvrir le lien externe et télécharger le document PDF.Nous joindre :@ : requisition@compte-nickel.frTél : 01.80.91.60.19Bien à vous,Compte-Nickel</MACRO>      <DOCS FicheSynthese=\"0\">        <DOC Filename=\"170109RM00001410001.pdf\" />        <DOC Filename=\"161112RM00001410001.pdf\" />        <DOC Filename=\"161001RM00001410001.pdf\" />      </DOCS>    </RESPONSE>  </RJ_DC></ALL_XML_OUT>";
        //return "<ALL_XML_OUT>  <RJ_DC Token=\"0X56F43AE1E9595A9980A220B6533AE5936F7B4711\" RefCustomer=\"25\">    <DOC Filename=\"Test_David_AO.pdf\" DocType=\"DCOM\" />  <INFOS TreatmentDate=\"27/01/2017\" />  <RESPONSE ResponseType=\"courrier\" CourrierTo=\"M. David BOULY\" CourrierAddress1=\"15 AVENUE MAURICE CHEVALIER\" CourrierAddress2=\"\" CourrierZipcode=\"77330\" CourrierCity=\"OZOIR LA FERRIERE\">      <MACRO>Bonjour,En réponse à votre droit de communication, je vous prie de bien vouloir trouver ci-après les éléments dont nous disposons sur le titulaire du compte que vous référencez à savoir, le dossier d'ouverture de compte avec notamment sa pièce d'identité et tous les flux relatifs à son Compte-Nickel.Je me tiens à votre disposition pour tout complément d'information.IMPORTANT : Afin de pouvoir ouvrir la pièce jointe vous devez disposer d'un accès à internet afin d'ouvrir le lien externe et télécharger le document PDF.Nous joindre :@ : requisition@compte-nickel.frTél : 01.80.91.60.19Bien à vous,Compte-Nickel</MACRO>      <DOCS FicheSynthese=\"1\">        <DOC Filename=\"170109RM00001410001.pdf\" />      </DOCS>    </RESPONSE>  </RJ_DC></ALL_XML_OUT>";
    }

    protected void ServerFileSelected(object sender, EventArgs e)
    {
        string[] arData = sender.ToString().Split(';');

        if(arData.Length > 1)
            txtReceptionDate.Text = arData[1];
    }

    protected void btnEditFile_Click(object sender, EventArgs e)
    {
        Button btn = (Button)sender;
        if(btn != null && btn.ID == "btnNewEdit")
        {
            btnEditFile.Visible = true;
            btnShowFileEdited.Visible = false;
            bFileSigned = false;
            upEditFile.Update();
        }

        // Check if session still valid
        authentication.GetCurrent(true);

        StringBuilder sbMessage = new StringBuilder();

        if (String.IsNullOrWhiteSpace(clientFileUpload1.DocumentType))
            sbMessage.AppendLine("Type de document : champ requis");

        if (_iRefCustomer == 0)
            sbMessage.AppendLine("Client : champ requis");

        if (String.IsNullOrWhiteSpace(sbMessage.ToString()))
        {
            bSignatureRequired = true;
            clientFileUpload1.ManualUploadScript();
        }
        else
        {
            string sErrorMessage = sbMessage.ToString().Replace(Environment.NewLine, "<br />");
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage(\"Erreur\", \"" + sErrorMessage + "\");", true);
        }
    }

    protected void SaveFinal()
    {
        if (SaveDB(null))
        {
            // ADD AML NOTE
            string sNote = "";
            switch (clientFileUpload1.DocumentType)
            {
                case "REQ":
                    DataTable dt = ddlMultiSelect1.GetSelectedChoices();
                    string sMotif = "";
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (i != 0)
                            sMotif += ", ";
                        sMotif += dt.Rows[i]["Text"];
                    }

                    sNote = "REQUISITION POUR " + sMotif + " PVN° " + txtPVNumber.Text;
                    break;
                case "DCOM":
                    sNote = "DROIT DE COMMUNICATION POUR " + txtDComOrigin.Text;
                    break;
            }
            if(sNote.Length > 0)
                AML.addClientNote(authentication.GetCurrent().sToken, _iRefCustomer, sNote);

            // DELETE 
            File.Delete(HostingEnvironment.MapPath("~" + _ServerFilePath + _sUploadedFileName));

            panelForm.Visible = false;
            panelSaved.Visible = true;
            upForm.Update();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage(\"Erreur\", \"Une erreur est survenue lors de l'enregistrement des données.\");", true);
        }
    }

    protected void btnGeneratePDF_Click(object sender, EventArgs e)
    {
        string sOuputFilePath = "";

        if(GeneratePDF(_iRefCustomer, out sOuputFilePath))
        {
            if (File.Exists(System.Web.HttpContext.Current.Server.MapPath("~" + sOuputFilePath)))
            {
                bFileSigned = true;

                btnEditFile.Visible = false;
                btnShowFileEdited.Visible = true;
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "HideRequisitionsDroitsCommunicationLoading(); ShowDialogCheckResponse('" + sOuputFilePath + "');", true);
            }
            else ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Erreur', \"Document introuvable.\"); HideRequisitionsDroitsCommunicationLoading();", true);
        }
        else ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage(\"Erreur\", \"Une erreur est survenue lors de la création du fichier.\"); HideRequisitionsDroitsCommunicationLoading();", true);
    }
    protected bool GeneratePDF(int iRefCustomer, out string sOuputFilePath)
    {
        sOuputFilePath = "";
        bool bOk = false;
        string sJSONData = hfAssets.Value;

        tools.AssetInfos infos = JsonConvert.DeserializeObject<tools.AssetInfos>(sJSONData);
        XElement xeAssets = new XElement("ASSETS");

        for (int i = 0; i < infos.assets.Count; i++)
        {
            xeAssets.Add(new XElement("ASSET",
                            new XAttribute("type", infos.assets[i].type),
                            new XAttribute("content", infos.assets[i].content),
                            new XAttribute("top", infos.assets[i].top),
                            new XAttribute("left", infos.assets[i].left),
                            new XAttribute("deg", infos.assets[i].deg)
                            ));
        }

        string sPath = "/" + tools.GetCustomerWebDirectoryName(iRefCustomer) + "/";

        string sXmlIn = new XElement("ALL_XML_IN",
                            new XElement("FILE",
                                new XElement("PATH", sPath),
                                new XElement("TEMPLATE", sPath + _sUploadedFileName.Substring(0, _sUploadedFileName.LastIndexOf('.')) + ".jpg"),
                                new XElement("FILENAME", _sUploadedFileName.Substring(0, _sUploadedFileName.LastIndexOf('.'))),
                                xeAssets)).ToString();

        WS_PDFCreator.PDFCreatorClient ws = new WS_PDFCreator.PDFCreatorClient();
        string sXmlOut = ws.CreateAssetFile(sXmlIn);
        //string sXmlOut = "<ALL_XML_OUT><FILE RC=\"0\" /></ALL_XML_OUT>"; // TEST

        List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/FILE", "RC");
        if (listRC.Count > 0 && listRC[0] == "0")
        {
            bOk = true;

            sOuputFilePath = ConfigurationManager.AppSettings["NickelDocPath"].ToString() + sPath + _sUploadedFileName;

            File.Delete(System.Web.HttpContext.Current.Server.MapPath("~" + sOuputFilePath.Replace(".pdf", ".jpg")));
        }

        return bOk;
    }

    protected void btnShowFileEdited_Click(object sender, EventArgs e)
    {
        string sPath = "/" + tools.GetCustomerWebDirectoryName(_iRefCustomer) + "/";
        string sOuputFilePath = ConfigurationManager.AppSettings["NickelDocPath"].ToString() + sPath + _sUploadedFileName;
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "ShowDialogCheckResponse('" + sOuputFilePath + "');", true);
    }

    protected void btnSavePostTreatment_Click(object sender, EventArgs e)
    {
        string sErrorMessage;

        if (clientFileUpload1.DocumentType == "REQ")
        {
            if (CheckForm(out sErrorMessage))
            {
                XElement xml = new XElement("RJ_DC",
                                    new XAttribute("Token", authentication.GetCurrent().sToken)
                                    , new XAttribute("RefCustomer", _iRefCustomer)
                                    , new XElement("DOC",
                                        new XAttribute("Filename", _sUploadedFileName)
                                        , new XAttribute("DocType", clientFileUpload1.DocumentType))
                                    );

                string sReasons = "";
                DataTable dtReasons = ddlMultiSelect1.GetSelectedChoices();
                for (int i = 0; i < dtReasons.Rows.Count; i++)
                {
                    sReasons += dtReasons.Rows[i]["Value"] + ";";
                }
                sReasons = sReasons.Substring(0, sReasons.Length - 1);

                xml.Add(new XElement("INFOS",
                            new XAttribute("Reasons", sReasons)
                            , new XAttribute("CloseAccount", (ckbNoClose.Checked) ? "0" : "1")));

                xml = new XElement("ALL_XML_IN", xml);
            }
            else ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage(\"Erreur\", \"" + sErrorMessage + "\");", true);
        }
    }
}