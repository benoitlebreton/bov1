﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using XMLMethodLibrary;

public partial class GarnishmentPendingTransfer : System.Web.UI.Page
{
    protected bool bHasActionRights {
        get { return (ViewState["bHasActionRights"] != null) ? bool.Parse(ViewState["bHasActionRights"].ToString()) : false; }
        set { ViewState["bHasActionRights"] = value; }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        checkRights();

        if (!IsPostBack)
            SetGarnishmentPendingTransfer();
    }

    protected void checkRights()
    {
        panelGarnishmentPending.Visible = false;
        panelGarnishmentPendingUnauthorized.Visible = true;

        bool bView, bAction;
        authentication auth = authentication.GetCurrent();
        AML.HasPendingTransferRights(auth.sToken, out bView, out bAction);
        bHasActionRights = bAction;
        if (bView)
        {
            panelGarnishmentPending.Visible = true;
            panelGarnishmentPendingUnauthorized.Visible = false;
        }
    }

    protected void SetGarnishmentPendingTransfer()
    {
        gvGarnishmentPendingTransfer.DataSource = GetGarnishmentPendingTransfer(ddlNbResult.SelectedValue, "1");
        gvGarnishmentPendingTransfer.DataBind();

        upGarnishmentPending.Update();
    }

    protected DataTable GetGarnishmentPendingTransfer(string sPageSize, string sPageIndex)
    {
        DataTable dt = new DataTable();
        //dt.Columns.Add("RefTransferPending");
        //dt.Columns.Add("LinkedRef");
        //dt.Columns.Add("RefCustomer");
        //dt.Columns.Add("AccountNumber");
        //dt.Columns.Add("IBAN");
        //dt.Columns.Add("Amount");
        //dt.Columns.Add("isRefused");
        //dt.Columns.Add("isTerminated");
        //dt.Columns.Add("isCancelled");

        //dt.Rows.Add(new object[] { "99999", "1", "25", "00001410001", "FR913000100123801H000000083", "150 €", false, false, false });

        XElement xPendingTransfer = new XElement("PendingTransfer",
            new XAttribute("CashierToken", authentication.GetCurrent().sToken),
            new XAttribute("PageSize", sPageSize),
            new XAttribute("PageIndex", sPageIndex),
            new XAttribute("OrderBy", "RefTransferPending"),
            new XAttribute("OrderASC", "DESC"),
            new XAttribute("ValidationTeam", "SC"),
            new XAttribute("Culture", ((Session["CultureID"] != null) ? Session["CultureID"].ToString() : "fr-FR")));

        string sXmlIn = new XElement("ALL_XML_IN", xPendingTransfer).ToString();
        string sXmlOut = "";
        dt = AML.getPendingTransferList(sXmlIn, out sXmlOut);

        if(sXmlOut.Length > 0)
        {
            List<string> listNbResult = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/PendingTransfer", "NbResult");
            if (listNbResult.Count > 0)
                lblNbOfResults.Text = listNbResult[0];
        }

        return dt;
    }

    protected void btnPendingTransferAction_Click(object sender, EventArgs e)
    {
        if (hdnRefPendingTransferSelected.Value.Length > 0 && hdnPendingTransferActionSelected.Value.Length > 0 && hdnRefCustomerSelected.Value.Trim().Length > 0)
        {
            string sErrorMessage = "";
            //if(true)
            if (AML.SetPendingTransferStatus(hdnRefPendingTransferSelected.Value, hdnRefCustomerSelected.Value, authentication.GetCurrent().sToken, hdnPendingTransferActionSelected.Value, out sErrorMessage))
            {
                string sActionDone = "";
                switch (hdnPendingTransferActionSelected.Value)
                {
                    case "TERMINATE":
                        sActionDone = "approuvé";
                        break;
                    case "REFUSE":
                        sActionDone = "refusé";
                        break;
                    case "CANCEL":
                        sActionDone = "annulé";
                        break;
                }
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Action réussie', \"<span style=\"color:green;\">Le virement a été " + sActionDone + ".</span>\");", true);
            }
            else ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Erreur', \"" + sErrorMessage + "\");", true);
        }
        else ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Erreur', \"Une erreur est survenue. Veuillez réessayer.\");", true);

        SetGarnishmentPendingTransfer();

        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "hideLoading();", true);
    }

    protected string getAmountCleaned(string sAmount)
    {
        string sCleanValue = "";
        sCleanValue = sAmount.Replace("+", "").Replace(" &amp;euro;", "").Replace(" &euro;", "");
        return sCleanValue;
    }

    protected void RefreshPendingList(object sender, EventArgs e)
    {
        SetGarnishmentPendingTransfer();
    }
}