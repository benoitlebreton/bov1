﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="UnauthorizedAccess.aspx.cs" Inherits="UnauthorizedAccess" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <h2>Accès refusé</h2>

    <div style="margin-top:20px;">
        Vous ne disposez pas des droits nécessaires pour accéder à ce contenu.

    </div>
    <div>
        <asp:Label ID="lblMessageDetails" runat="server"></asp:Label>
    </div>

    <div style="margin-top:20px;text-align:right">
        <asp:Button ID="btnBack" runat="server" CssClass="buttonHigher" Text="Revenir à la page précédente" OnClick="btnBack_Click" />
    </div>

</asp:Content>

