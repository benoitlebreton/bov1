﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using XMLMethodLibrary;

public partial class CreOrder : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindMenu();
        }
    }

    protected void BindMenu()
    {
        
        //Droits spécifiques CRE
        if (CRETools.CheckRights(authentication.GetCurrent().sToken))
        {
            DataTable dtMenu = new DataTable();
            dtMenu.Columns.Add("Ref");
            dtMenu.Columns.Add("Label");
            dtMenu.Columns.Add("Value");
            dtMenu.Rows.Add(new object[] { "01", "Nouvelle opération", "CreOrderNew.aspx" });
            dtMenu.Rows.Add(new object[] { "02", "Rechercher/Valider une opération", "CreOrderValidation.aspx" });
            DataView dv = dtMenu.DefaultView;
            dv.Sort = "Ref asc";
            rptMenu.DataSource = dv.ToTable();
            rptMenu.DataBind();
        }
        else 
        {
            Response.Redirect("Default.aspx");
        }
    }
}