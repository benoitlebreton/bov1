﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using XMLMethodLibrary;
using System.Data;
using System.Xml.Linq;
using System.Text;
using System.Globalization;

public partial class AccountClosing : System.Web.UI.Page
{
    private decimal _dAccountBalance { get { return (ViewState["CustomerAccountBalance"] != null) ? decimal.Parse(ViewState["CustomerAccountBalance"].ToString()) : 0; } set { ViewState["CustomerAccountBalance"] = value; } }

    protected int getCurrentRefCustomer()
    {
        int iRefCustomer = 0;

        if (!(Request.Params["ref"] != null && int.TryParse(Request.Params["ref"], out iRefCustomer)))
            if (!(ViewState["ref"] != null && int.TryParse(ViewState["ref"].ToString(), out iRefCustomer)))
                Response.Redirect("Default.aspx", true);

        return iRefCustomer;
    }
    protected int getCurrentRefAlert()
    {
        int iRefAlert = 0;

        if (!(Request.Params["alert"] != null && int.TryParse(Request.Params["alert"], out iRefAlert)))
            if (ViewState["alert"] != null)
                int.TryParse(ViewState["alert"].ToString(), out iRefAlert);

        return iRefAlert;
    }
    protected int getCurrentRefClose()
    {
        int iRefClose = 0;

        if (!(Request.Params["edit"] != null && int.TryParse(Request.Params["edit"], out iRefClose)))
            if (ViewState["edit"] != null)
                int.TryParse(ViewState["edit"].ToString(), out iRefClose);

        return iRefClose;
    }
    protected int getCurrentCloseType()
    {
        int iRefCloseAction = 0;

        if (!(Request.Params["type"] != null && int.TryParse(Request.Params["type"], out iRefCloseAction)))
            if (ViewState["type"] != null)
                int.TryParse(ViewState["type"].ToString(), out iRefCloseAction);

        return iRefCloseAction;
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        AccountClosingFee.FeeAddedRemoved += new EventHandler(UpdateCalculation);
        ReturnFunds.ReturnFundAddedRemoved += new EventHandler(UpdateCalculation);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            CheckRights();

            int iRefCustomer = getCurrentRefCustomer();
            ViewState["ref"] = iRefCustomer;

            txtAccountClosingLaterDate.Text = DateTime.Now.AddDays(60).ToString("dd/MM/yyyy");

            BindClientInfos();

            amlSurveillance1.BindAmlSurveillance(iRefCustomer, false, false, false, "");

            int iRefClose = getCurrentRefClose();
            if (iRefClose != 0)
                BindAccountCloseInformation(GetAccountCloseInformation(authentication.GetCurrent().sToken, iRefClose.ToString()));

            int iCloseType = getCurrentCloseType();
            if (iCloseType != 0)
            {
                switch(iCloseType)
                {
                    case 1:
                        rblAccountClosingTime.SelectedIndex = 1;
                        break;
                    case 2:
                        rblAccountClosingTime.SelectedIndex = 0;
                        panelClosingTimeLater.CssClass = "";
                        break;
                }
            }
        }

        if (panelSurveillance.Visible)
            amlSurveillance1.InitScripts();

        if(panelCommentAccountClosing.Visible)
            amlCommentAccountClosing.BindCommentLengthCheck();
    }

    protected void CheckRights()
    {
        bool bAction, bView;
        tools.GetTagActionRight(tools.GetRights(authentication.GetCurrent().sToken, "SC_AccountClosing"), "RequestAccountClose", out bAction, out bView);
        if (!bAction)
            Response.Redirect("Default.aspx", true);

        string sXmlOut = tools.GetRights(authentication.GetCurrent().sToken, "SC_Customer");
        List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Page", "RC");

        if (listRC.Count > 0 && listRC[0] == "0")
        {
            List<string> listAction = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/Page/Action");

            for (int i = 0; i < listAction.Count; i++)
            {
                List<string> listTag = CommonMethod.GetAttributeValues(listAction[i], "Action", "TagAction");
                List<string> listActionAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ActionAllowed");
                List<string> listViewAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ViewAllowed");

                if (listTag.Count > 0)
                {
                    switch (listTag[0])
                    {
                        case "Surveillance":
                            if (listViewAllowed.Count > 0 && listViewAllowed[0] == "1" && listActionAllowed.Count > 0 && listActionAllowed[0] == "1")
                                panelSurveillance.Visible = true;
                            else panelSurveillance.Visible = false;

                            //panelSurveillance.Visible = false; // TEST
                            break;
                    }
                }
            }
        }
    }

    protected void BindClientInfos()
    {
        int iRefCustomer = getCurrentRefCustomer();
        string sXMLClientInfos = Client.GetCustomerInformations(iRefCustomer);
        if (sXMLClientInfos.Length > 0)
        {
            List<string> listCivility = CommonMethod.GetAttributeValues(sXMLClientInfos, "ALL_XML_OUT/Customer", "Politeness");
            List<string> listLastName = CommonMethod.GetAttributeValues(sXMLClientInfos, "ALL_XML_OUT/Customer", "LastName");
            List<string> listFirstName = CommonMethod.GetAttributeValues(sXMLClientInfos, "ALL_XML_OUT/Customer", "FirstName");
            List<string> listAccountNumber = CommonMethod.GetAttributeValues(sXMLClientInfos, "ALL_XML_OUT/Customer", "AccountNumber");
            List<string> listWebIdentifier = CommonMethod.GetAttributeValues(sXMLClientInfos, "ALL_XML_OUT/Customer", "WebIdentifier");
            List<string> listIsCobalt = CommonMethod.GetAttributeValues(sXMLClientInfos, "ALL_XML_OUT/Customer", "IsCobalt");
            List<string> listIBAN = CommonMethod.GetAttributeValues(sXMLClientInfos, "ALL_XML_OUT/Customer", "IBAN");

            lblClientCardNumber.Text = (listWebIdentifier.Count > 0) ? listWebIdentifier[0] : "";
            lblClientAccountNumber.Text = listAccountNumber[0];
            lblClientName.Text = listCivility[0] + " " + listFirstName[0] + " " + listLastName[0];

            btnClientDetails.Attributes.Add("onclick", "document.location.href=\"ClientDetails.aspx?ref=" + iRefCustomer + "\";");
            bool bIsCobalt = (listIsCobalt.Count > 0 && listIsCobalt[0] == "1") ? true : false;

            decimal dBalance = 0;
            if (tools.GetAccountBalance(listAccountNumber[0], listIBAN[0], bIsCobalt, out dBalance))
            {
                if (dBalance < 0)
                {
                    panelRfTypeChoice.Visible = false;
                    panelWithRF.Visible = false;

                    panelFees.Visible = false;
                }

                _dAccountBalance = dBalance;
                UpdateCalculation(null, null);
            }
        }
    }

    protected void rblAccountClosingRfType_SelectedIndexChanged(object sender, EventArgs e)
    {
        panelWithRF.Visible = false;

        switch (rblAccountClosingRfType.SelectedValue)
        {
            case "WithRF":
                panelWithRF.Visible = true;
                ReturnFunds.InitMask();
                break;
        }

        //btnValidate.Visible = true;

        upAccountClosing.Update();
    }

    protected void rblAccountClosingTime_SelectedIndexChanged(object sender, EventArgs e)
    {
        panelClosingTimeLater.CssClass = "hidden";

        switch (rblAccountClosingTime.SelectedValue)
        {
            case "later":
                panelClosingTimeLater.CssClass = "";
                break;
        }

        //panelRfTypeChoice.Visible = true;
        upAccountClosing.Update();
    }

    protected bool CloseAccount(string sXmlIn)
    {
        bool bClosed = false;
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_CloseCustomerAccount", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);

            cmd.Parameters["@IN"].Value = sXmlIn;
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if (sXmlOut.Length > 0)
            {
                List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Result", "RC");

                if (listRC.Count > 0 && listRC[0] == "0")
                    bClosed = true;
            }
        }
        catch (Exception e)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return bClosed;
    }

    protected bool CheckForm(out string sErrorMessage)
    {
        bool bOk = true;
        sErrorMessage = "";
        StringBuilder sbErrorMessage = new StringBuilder();

        if (!AccountClosingReason.CheckForm())
            sbErrorMessage.AppendLine("Raison : champ requis");

        if (String.IsNullOrWhiteSpace(rblAccountClosingTime.SelectedValue))
            sbErrorMessage.AppendLine("Préavis : champ requis");
        if (panelRfTypeChoice.Visible)
        {
            if (String.IsNullOrWhiteSpace(rblAccountClosingRfType.SelectedValue))
                sbErrorMessage.AppendLine("Retour de fonds : champ requis");
            else
            {
                if (rblAccountClosingRfType.SelectedValue == "WithRF" && ReturnFunds.GetDataToXML() == null)
                    sbErrorMessage.AppendLine("Retour de fonds : champ requis");
            }
        }

        if (rblAccountClosingTime.SelectedValue == "later")
                if (String.IsNullOrWhiteSpace(txtAccountClosingLaterDate.Text))
                    sbErrorMessage.AppendLine("Date fin préavis : champ requis");

        if (panelSumNeg.Visible == true)
            sbErrorMessage.AppendLine("Attention : le solde du compte est insuffisant");

        if (panelCommentAccountClosing.Visible && String.IsNullOrWhiteSpace(amlCommentAccountClosing.Commentary))
            sbErrorMessage.AppendLine("Commentaire : champ requis");

        if(panelClosingTimeLater.Visible && panelSurveillance.Visible)
            if(!amlSurveillance1.IsFormValid())
                sbErrorMessage.AppendLine("Surveillance : paramétrage incorrect");

        sErrorMessage = sbErrorMessage.Replace(Environment.NewLine, "<br />").ToString();
        
        if (sErrorMessage.Length > 0)
            bOk = false;

        return bOk;
    }

    protected void btnValidate_Click(object sender, EventArgs e)
    {
        SaveCloseAccount("Create");
    }

    protected void SaveCloseAccount(string sAction)
    {
        string sErrorMessage = "";
        if (CheckForm(out sErrorMessage))
        {
            bool bNotify = (rblAccountClosingTime.SelectedValue == "now") ? false : true;
            bool bReturnFunds = (rblAccountClosingRfType.SelectedValue == "WithRF") ? true : false;
            int iRefCustomer = getCurrentRefCustomer();

            XElement xml = new XElement("ACCOUNT_CLOSURE",
                                new XAttribute("CashierToken", authentication.GetCurrent().sToken),
                                new XAttribute("RefCustomer", iRefCustomer.ToString()),
                                new XAttribute("CustomerAccountBalance", _dAccountBalance.ToString()),
                                new XAttribute("NotifyBefore", (bNotify) ? "1" : "0"),
                                new XAttribute("ReturnFunds", (bReturnFunds) ? "1" : "0"),
                                new XAttribute("AskDate", DateTime.UtcNow.ToString("dd/MM/yyyy HH:mm")),
                                new XAttribute("Commentary", amlCommentAccountClosing.Commentary));

            if (sAction == "Update")
                xml.Add(new XAttribute("RefClose", getCurrentRefClose().ToString()));

            if (getCurrentRefAlert() != 0)
                xml.Add(new XAttribute("RefAlert", getCurrentRefAlert()));

            if (bNotify)
            {
                xml.Add(new XAttribute("NoticeEndDate", txtAccountClosingLaterDate.Text));
                xml.Add(new XAttribute("IsCACF", (ckbCACF.Checked) ? "1" : "0"));
            }

            xml.Add(AccountClosingReason.GetDataToXML());
            xml.Add(AccountClosingFee.GetDataToXML());

            if (bReturnFunds)
            {
                xml.Add(ReturnFunds.GetDataToXML());
            }

            bool bContinue = true;
            string sSurveillanceErrorMessage = "";
            if (panelClosingTimeLater.CssClass != "hidden" && panelSurveillance.Visible)
            {
                if ((!amlSurveillance1.SaveAMLAlerts(ref sSurveillanceErrorMessage) || !amlSurveillance1.SaveTransferOutSituation(ref sSurveillanceErrorMessage)))
                {
                    bContinue = false;
                    AlertMessage("Erreur", sSurveillanceErrorMessage);
                }
                else
                {
                    // ajouter specificité DRSS
                    if (amlSurveillance1.IsSurveillanceActive)
                        AML.addClientSpecificity(authentication.GetCurrent().sToken, iRefCustomer, "44");
                }
            }

            if (bContinue)
            {
                string sXmlIn = new XElement("ALL_XML_IN", xml).ToString();
                if (sAction == "Create" && CloseAccount(sXmlIn))
                {
                    panelAccountClosing.Visible = false;
                    panelAccountClosed.Visible = true;

                    if (getCurrentRefAlert() != 0)
                        SetAlertTreated();
                }
                else if(sAction == "Update" && SetCloseCustomerAccount(sXmlIn))
                {
                    Response.Redirect("SearchAccountClosing.aspx");
                }
                else AlertMessage("Erreur", "Une erreur est survenue lors de l'enregistrement.");
            }
        }
        else AlertMessage("Erreur", sErrorMessage);
    }

    protected void SetAlertTreated()
    {
        XElement xml = new XElement("ALERT_TREATMENT",
                            new XAttribute("RefAlert", getCurrentRefAlert()),
                            new XAttribute("CashierToken", authentication.GetCurrent().sToken),
                            new XAttribute("Treated", "1"));
        AML.SetAlertTreatment(new XElement("ALL_XML_IN", xml).ToString());
    }

    protected void AlertMessage(string sTitle, string sMessage)
    {
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('" + sTitle + "', \"" + sMessage + "\");", true);
    }

    protected void UpdateCalculation(object sender, EventArgs e)
    {
        decimal dTotal = 0;

        litAccountBalance.Text = ((_dAccountBalance == 0) ? "0" : _dAccountBalance.ToString("0.00").Replace(".", ",")) + " €";
        dTotal += _dAccountBalance;

        decimal dTotalRF = ReturnFunds.GetTotalReturnFunds();
        litReturnFundsSum.Text = ((dTotalRF == 0) ? "0" : dTotalRF.ToString("0.00").Replace(".", ",")) + " €";
        dTotal -= dTotalRF;

        decimal dTotalFee = AccountClosingFee.GetTotalFees();
        litFeesSum.Text = ((dTotalFee == 0) ? "0" : dTotalFee.ToString("0.00").Replace(".", ",")) + " €";
        dTotal -= dTotalFee;

        litSumLeft.Text = ((dTotal == 0) ? "0" : dTotal.ToString("0.00").Replace(".", ",")) + " €";

        if(dTotal < 0 && _dAccountBalance >= 0)
        {
            panelSum.Visible = false;
            panelSumNeg.Visible = true;
        }
        else
        {
            panelSum.Visible = true;
            panelSumNeg.Visible = false;
        }

        upCalculation.Update();
    }

    // UPDATE
    protected void BindAccountCloseInformation(string sXmlOut)
    {
        if (sXmlOut.Length > 0)
        {
            List<string> listClose = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/Close");

            for (int i = 0; i < listClose.Count; i++)
            {
                #region // XML OUT SAMPLE
                /*
                 *
                  <Close RefClose="8" LastName="BATERGIL" FirstName="PATRICK" BankAccountNumber="03712610001" BOUser="Angelita MARIDUENA" CloseStatus="A CLOTURER" CustomerBalance="1044.61">
                    <Fees>
                      <Fee FeeAmount="165.00" FeeDescription="Utilisation irrégulière Compte nickel" />
                      <Fee FeeAmount="150.00" FeeDescription="Frais de recherche" />
                    </Fees>
                    <ReturnFunds>
                      <ReturnFund TransferAmount="885" BeneficiaryName="ing" IBAN="FR7630438001004000064602274" BIC="INGBFR21XXX" Libelle="FRAUDE – 40003588353 Anne MORVAN – ASG " />
                    </ReturnFunds>
                    <REASONS>
                      <REASON TAGReason="FFCS" />
                    </REASONS>
                  </Close>
                 * 
                */
                #endregion

                //List<string> listRefClose = CommonMethod.GetAttributeValues(listClose[i], "Close", "RefClose");
                //List<string> listRefCustomer = CommonMethod.GetAttributeValues(listClose[i], "Close", "RefCustomer");
                //List<string> listLastName = CommonMethod.GetAttributeValues(listClose[i], "Close", "LastName");
                //List<string> listFirstName = CommonMethod.GetAttributeValues(listClose[i], "Close", "FirstName");
                //List<string> listBankAccountNumber = CommonMethod.GetAttributeValues(listClose[i], "Close", "BankAccountNumber");
                //List<string> listBOUser = CommonMethod.GetAttributeValues(listClose[i], "Close", "BOUser");
                //List<string> listCloseStatus = CommonMethod.GetAttributeValues(listClose[i], "Close", "CloseStatus");
                //List<string> listCustomerBalance = CommonMethod.GetAttributeValues(listClose[i], "Close", "CustomerBalance");
                List<string> listNotifyBefore = CommonMethod.GetAttributeValues(listClose[i], "Close", "NotifyBefore");
                List<string> listNoticeEndDate = CommonMethod.GetAttributeValues(listClose[i], "Close", "NoticeEndDate");

                if (listNotifyBefore.Count > 0 && listNotifyBefore[0] == "1")
                {
                    rblAccountClosingTime.SelectedIndex = 0;
                    txtAccountClosingLaterDate.Text = listNoticeEndDate[0];
                }
                else rblAccountClosingTime.SelectedIndex = 1;
                rblAccountClosingTime_SelectedIndexChanged(new object(), new EventArgs());

                List<string> listFees = CommonMethod.GetTags(listClose[i], "Close/Fees/Fee");
                DataTable dtFees = new DataTable();
                dtFees.Columns.Add("Tag");
                dtFees.Columns.Add("Label");
                dtFees.Columns.Add("Amount");
                for (int j = 0; j < listFees.Count; j++)
                {
                    List<string> listTag = CommonMethod.GetAttributeValues(listFees[j], "Fee", "FeeTag");
                    List<string> listAmount = CommonMethod.GetAttributeValues(listFees[j], "Fee", "FeeAmount");
                    List<string> listDesc = CommonMethod.GetAttributeValues(listFees[j], "Fee", "FeeDescription");

                    dtFees.Rows.Add(new object[] { listTag[0], listDesc[0], listAmount[0].Replace('.', ',') });
                }
                AccountClosingFee.SetFees(dtFees);

                List<string> listReturnFunds = CommonMethod.GetTags(listClose[i], "Close/ReturnFunds/ReturnFund");
                if (listReturnFunds.Count > 0)
                {
                    rblAccountClosingRfType.SelectedIndex = 0;

                    DataTable dtReturnFunds = new DataTable();
                    dtReturnFunds.Columns.Add("Amount");
                    dtReturnFunds.Columns.Add("Beneficiary");
                    dtReturnFunds.Columns.Add("IBAN");
                    dtReturnFunds.Columns.Add("Label");
                    for (int j = 0; j < listReturnFunds.Count; j++)
                    {
                        List<string> listAmount = CommonMethod.GetAttributeValues(listReturnFunds[j], "ReturnFund", "TransferAmount");
                        List<string> listBenef = CommonMethod.GetAttributeValues(listReturnFunds[j], "ReturnFund", "BeneficiaryName");
                        List<string> listIBAN = CommonMethod.GetAttributeValues(listReturnFunds[j], "ReturnFund", "IBAN");
                        List<string> listLabel = CommonMethod.GetAttributeValues(listReturnFunds[j], "ReturnFund", "Libelle");

                        dtReturnFunds.Rows.Add(new object[] { listAmount[0].Replace('.', ','), listBenef[0], listIBAN[0], listLabel[0] });
                    }

                    ReturnFunds.SetReturnFunds(dtReturnFunds);
                }
                else rblAccountClosingRfType.SelectedIndex = 1;
                rblAccountClosingRfType_SelectedIndexChanged(new object(), new EventArgs());

                List<string> listTagReason = CommonMethod.GetAttributeValues(listClose[i], "Close/REASONS/REASON", "TAGReason");
                DataTable dtReasons = new DataTable();
                dtReasons.Columns.Add("Tag");
                dtReasons.Columns.Add("Label");
                for (int j = 0; j < listTagReason.Count; j++)
                {
                    string sReasonDesc = AccountClosingReason.GetTagDescription(listTagReason[j]);

                    if (sReasonDesc != null)
                        dtReasons.Rows.Add(new object[] { listTagReason[j], sReasonDesc });
                }
                AccountClosingReason.SetAccountClosingReasons(dtReasons);
            }

            UpdateCalculation(new object(), new EventArgs());

            btnValidate.Visible = false;
            btnUpdate.Visible = true;
            panelCommentAccountClosing.Visible = false;
        }
    }
    protected string GetAccountCloseInformation(string sToken, string sRefClose)
    {
        string sXmlOut = "";
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("BankSI.P_GetCustomerCloseInformation", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN", 
                                            new XElement("Close", 
                                                new XAttribute("CashierToken", sToken), 
                                                //new XAttribute("ShowNotifyBefore", "1"), 
                                                new XAttribute("RefClose", sRefClose))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            sXmlOut = cmd.Parameters["@OUT"].Value.ToString();
        }
        catch (Exception e)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return sXmlOut;
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        SaveCloseAccount("Update");
    }

    protected bool SetCloseCustomerAccount(string sXmlIn)
    {
        bool bUpdate = false;
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_SetCloseCustomerAccount", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);

            cmd.Parameters["@IN"].Value = sXmlIn;
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if(sXmlOut.Length > 0)
            {
                List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Result", "RC");

                if (listRC.Count > 0 && listRC[0] == "0")
                    bUpdate = true;
            }
        }
        catch(Exception e)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return bUpdate;
    }
}