﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using XMLMethodLibrary;
using System.Xml.Linq;

public partial class Transfer : System.Web.UI.Page
{
    protected string RefCustomer
    {
        get { return ViewState["RefCustomer"] != null ? ViewState["RefCustomer"].ToString() : null; }
        set { ViewState["RefCustomer"] = value; }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        int iRefCustomer = 0;

        if (Request.Params["fromClient"] != null && int.TryParse(Request.Params["fromClient"].ToString(), out iRefCustomer))
        {
            RefCustomer = iRefCustomer.ToString();
        }
        else Response.Redirect("Default.aspx", true);
    }

    protected void btnExecuteReturnFunds_Click(object sender, EventArgs e)
    {
        string sErrorMessage = null;
        bool bEmpty;

        if (ReturnFunds1.CheckForm(out sErrorMessage, out bEmpty))
        {
            XElement xml = ReturnFunds1.GetDataToXML();

            if (xml != null && !string.IsNullOrWhiteSpace(xml.ToString()))
            {
                xml.Add(new XAttribute("CashierToken", authentication.GetCurrent().sToken));
                foreach (XElement elem in xml.Elements())
                {
                    if (elem.Name == "RETURN_FUND")
                        elem.Add(new XAttribute("RefCustomer", RefCustomer));
                }

                if (AddReturnFunds(new XElement("ALL_XML_IN", xml).ToString()))
                {
                    Response.Redirect(String.Format("ClientDetails.aspx?ref={0}&view={1}", RefCustomer, "operations"));
                }
                else
                {
                    AlertMessage("Erreur", "Une erreur s'est produite lors de l'enregistrement.");
                }
            }
            else AlertMessage("Erreur", "Une erreur inattendue s'est produite.");
        }
        else AlertMessage("Erreur", sErrorMessage);
    }

    protected bool AddReturnFunds(string sXmlIn)
    {
        bool bAdd = false;
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("Web.P_AddReturnFunds", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);

            cmd.Parameters["@IN"].Value = sXmlIn;
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if(!string.IsNullOrWhiteSpace(sXmlOut))
            {
                List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Result", "RC");
                if (listRC.Count > 0 && listRC[0] == "0")
                    bAdd = true;
            }
        }
        catch(Exception e)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }


        return bAdd;
    }

    protected void AlertMessage(string sTitle, string sMessage)
    {
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('" + sTitle + "', \"" + sMessage + "\");", true);
    }
}