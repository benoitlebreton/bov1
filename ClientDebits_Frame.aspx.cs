﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ClientDebits_Frame : System.Web.UI.Page
{
    private string _sClientBankAccount
    {
        get { return ViewState["ClientBankAccount"].ToString(); }
        set { ViewState["ClientBankAccount"] = value; }
    }

    private string _sRefCustomer
    {
        get { return ViewState["RefCustomer"].ToString(); }
        set { ViewState["RefCustomer"] = value; }
    }
    private string _sIBAN
    {
        get { return (ViewState["IBAN"] != null) ? ViewState["IBAN"].ToString() : ""; }
        set { ViewState["IBAN"] = value; }
    }
    private bool _bIsCobalt
    {
        get { return (ViewState["IsCobalt"] != null) ? bool.Parse(ViewState["IsCobalt"].ToString()) : false; }
        set { ViewState["IsCobalt"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("fr-FR");
        Thread.CurrentThread.CurrentUICulture = new CultureInfo("fr-FR");
        ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
        scriptManager.RegisterPostBackControl(this.btnExcelExport);

        panelTransferListTable.Visible = true;
        lblDateNow.Text = DateTime.Now.ToString("dd MMMM yyyy");

        if (!IsPostBack)
        {
            if (Request.Params["ref"] != null && Request.Params["ref"].Length > 0)
            {
                _sRefCustomer = Request.Params["ref"].ToString();
            }

            if (Request.Params["account"] != null && Request.Params["account"].Length > 0)
            {
                _sClientBankAccount = Request.Params["account"].ToString();
                //rptAuthorizationList.DataSource = operation.getUserAuthorizationList(_sClientBankAccount.Trim(), 1, 100, "", "", "ALL");
                //rptAuthorizationList.DataBind();
                if (Request.Params["isCobalt"] != null && Request.Params["isCobalt"].ToString() == "1")
                    _bIsCobalt = true;
                
                if (Request.Params["iban"] != null && Request.Params["iban"].Length > 0)
                    _sIBAN = Request.Params["iban"].ToString();

                operation ope = new operation();
                int iSize = 15;
                int iStep = 15;
                operation.SearchCriteria searchCriteria = new operation.SearchCriteria(iSize, iStep, "", "", _sClientBankAccount, "2", "ALL", "ALL", true);
                searchCriteria.bIsCobalt = _bIsCobalt;
                searchCriteria.sIBAN = _sIBAN;
                
                UpdateOperationList(searchCriteria);
                searchCriteria.bForceRefresh = false;
                ViewState["SearchCriteria"] = searchCriteria;

                if (rptTransferList.Items.Count > 0)
                {
                    panelNoSearchResult.Visible = false;
                    rptTransferList.Visible = true;
                }
                else
                {
                    panelNoSearchResult.Visible = true;
                    rptTransferList.Visible = false;
                }
            }
            else
            {
                panelTransferListTable.Visible = false;
            }

        }
    }

    protected void rptTransferList_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
    }

    protected void btnShowMore_Click(object sender, EventArgs e)
    {
        int iSize = 15;
        int iStep = 15;
        operation ope = new operation();
        int iRowCount;

        if (ViewState["SearchCriteria"] != null)
        {
            operation.SearchCriteria searchCriteria = (operation.SearchCriteria)ViewState["SearchCriteria"];
            iSize = searchCriteria.iPageSize;
            iStep = searchCriteria.iPageStep;
            iSize += iStep;
            searchCriteria.iPageSize = iSize;
            searchCriteria.bIsCobalt = _bIsCobalt;
            searchCriteria.sIBAN = _sIBAN;

            ViewState["SearchCriteria"] = searchCriteria;
            UpdateOperationList(searchCriteria);
        }
        else
        {
            operation.SearchCriteria searchCriteria = new operation.SearchCriteria(iSize, iStep, "", "", _sClientBankAccount, "2", "ALL", "ALL", true);
            searchCriteria.bIsCobalt = _bIsCobalt;
            searchCriteria.sIBAN = _sIBAN;
            UpdateOperationList(searchCriteria);
            UpdateSearchCriteria(searchCriteria);

            ViewState["SearchCriteria"] = searchCriteria;
        }
    }

    protected void UpdateSearchCriteria(operation.SearchCriteria searchCriteria)
    {
        DateTime date;
        string sDateFrom = "";
        string sDateTo = "";
        panelNoFilters.Visible = false;
        panelFilters.Visible = true;
        string sFiltersLabel = "Virements/Prélèvements ";

        if (DateTime.TryParse(searchCriteria.sDateFrom, out date))
            sDateFrom = date.ToString("dd MMMM yyyy");
        if (DateTime.TryParse(searchCriteria.sDateTo, out date))
            sDateTo = date.ToString("dd MMMM yyyy");

        string sTransferType = ddlTransferStatus.Items.FindByValue(searchCriteria.sOperationType).Text;

        if (sTransferType.Length > 0 && sTransferType != "TOUS")
            sFiltersLabel += ": <span class=\"font-bold uppercase\">" + sTransferType + "</span> ";

        if (sDateFrom.Length > 0 && sDateTo.Length > 0)
        {
            sFiltersLabel += "du <span class=\"font-bold\"  style=\"text-transform:uppercase\">" + sDateFrom + "</span> au <span class=\"font-bold\"  style=\"text-transform:uppercase\">" + sDateTo + "</span>";
        }
        else
        {
            if (sDateFrom.Length > 0)
            {
                sFiltersLabel += "depuis le <span class=\"font-bold uppercase\">" + sDateFrom + "</span>";
            }
            else if (sDateTo.Length > 0)
            {
                sFiltersLabel += "au <span class=\"font-bold uppercase\">" + sDateTo + "</span>";
            }
            else
            {
                sFiltersLabel += "au <span class=\"font-bold uppercase\">" + DateTime.Now.ToString("dd MMMM yyyy") + "</span>, à <span class=\"lblClientTime font-bold\"></span>";
            }
        }

        ltlFilters.Text = sFiltersLabel;
    }
    protected void UpdateNbPages(int iPageSize, int iNbTotal)
    {
        ltlNbResults.Text = "<span class=\"font-bold\">" + iPageSize.ToString() + "</span>/<span class=\"font-bold\">" + iNbTotal.ToString() + "</span>";
    }

    protected void UpdateOperationList(operation.SearchCriteria searchCriteria)
    {
        operation ope = new operation();
        int iRowCount;

        searchCriteria.sOperationType = "2";

        DataTable dt = operation.getUserTransferListTreated(operation.getUserTransferList(searchCriteria, out iRowCount), ref iRowCount);

        if (iRowCount == dt.Rows.Count)
        {
            btnShowMore.Visible = false;
            UpdateNbPages(iRowCount, iRowCount);
        }
        else
        {
            btnShowMore.Visible = true;
            UpdateNbPages(dt.Rows.Count, iRowCount);
        }

        if (dt.Rows.Count > 0)
        {
            rptTransferList.Visible = true;
            panelNoSearchResult.Visible = false;
            rptTransferList.DataSource = dt;
            rptTransferList.DataBind();
        }
        else
        {
            rptTransferList.Visible = false;
            panelNoSearchResult.Visible = true;
            rptTransferList.DataSource = null;
            rptTransferList.DataBind();
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        if (ViewState["SearchCriteria"] != null)
        {
            operation.SearchCriteria searchCriteria = (operation.SearchCriteria)ViewState["SearchCriteria"];
            DateTime date;
            if (txtDateFrom.Text.Length > 0 && DateTime.TryParse(txtDateFrom.Text, out date))
                searchCriteria.sDateFrom = txtDateFrom.Text;
            else searchCriteria.sDateFrom = "";
            if (txtDateTo.Text.Length > 0 && DateTime.TryParse(txtDateTo.Text, out date))
                searchCriteria.sDateTo = txtDateTo.Text;
            else searchCriteria.sDateTo = "";
            searchCriteria.iPageSize = 50;
            searchCriteria.iPageStep = 50;
            searchCriteria.sOperationType = "2";
            searchCriteria.sDirection = "ALL";
            searchCriteria.sStatus = ddlTransferStatus.SelectedValue;

            UpdateOperationList(searchCriteria);

            ViewState["SearchCriteria"] = searchCriteria;

            UpdateSearchCriteria(searchCriteria);
        }
    }

    protected void btnExportExcel_Click(object sender, EventArgs e)
    {
        if (ViewState["SearchCriteria"] != null)
        {
            int iRowCount;
            operation ope = new operation();
            operation.SearchCriteria searchCriteria = (operation.SearchCriteria)ViewState["SearchCriteria"];
            DateTime date;
            if (txtDateFrom.Text.Length > 0 && DateTime.TryParse(txtDateFrom.Text, out date))
                searchCriteria.sDateFrom = txtDateFrom.Text;
            else searchCriteria.sDateFrom = "";
            if (txtDateTo.Text.Length > 0 && DateTime.TryParse(txtDateTo.Text, out date))
                searchCriteria.sDateTo = txtDateTo.Text;
            else searchCriteria.sDateTo = "";
            searchCriteria.iPageSize = -1;
            searchCriteria.iPageStep = -1;
            searchCriteria.sOperationType = "2";
            searchCriteria.sDirection = "ALL";
            searchCriteria.sOperationType = ddlTransferStatus.SelectedValue;

            ViewState["SearchCriteria"] = searchCriteria;

            //ExportDataSetToExcel(dgTransactionExcelExport(operation.getUserTransferList(searchCriteria, out iRowCount)), "Virements_Prelevements_" + _sClientBankAccount + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xls");
        }
    }
    protected DataGrid dgTransactionExcelExport(DataTable dt)
    {
        DataGrid dg = new DataGrid();
        DataTable dtCustom = new DataTable();

        dg.AutoGenerateColumns = false;
        dg.HeaderStyle.BackColor = System.Drawing.Color.FromArgb(245, 117, 39);
        dg.HeaderStyle.ForeColor = System.Drawing.Color.White;
        dg.HeaderStyle.Font.Bold = true;

        BoundColumn bcAuthorizationDate = new BoundColumn();
        bcAuthorizationDate.HeaderText = "Date";
        bcAuthorizationDate.DataField = "Date";
        bcAuthorizationDate.ItemStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Left;

        BoundColumn bcAuthorizationStatus = new BoundColumn();
        bcAuthorizationStatus.HeaderText = "Statut";
        bcAuthorizationStatus.DataField = "Statut";
        bcAuthorizationStatus.ItemStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Left;

        BoundColumn bcAuthorizationResponseCode = new BoundColumn();
        bcAuthorizationResponseCode.HeaderText = "Code Reponse";
        bcAuthorizationResponseCode.DataField = "CodeReponse";
        bcAuthorizationResponseCode.ItemStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Left;

        BoundColumn bcAuthorizationNum = new BoundColumn();
        bcAuthorizationNum.HeaderText = "N° Autorisation";
        bcAuthorizationNum.DataField = "NumAutorisation";
        bcAuthorizationNum.ItemStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Left;

        BoundColumn bcAuthorizationPlace = new BoundColumn();
        bcAuthorizationPlace.HeaderText = "Commerçant";
        bcAuthorizationPlace.DataField = "Lieu";
        bcAuthorizationPlace.ItemStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Left;

        BoundColumn bcAuthorizationAmount = new BoundColumn();
        bcAuthorizationAmount.HeaderText = "Montant";
        bcAuthorizationAmount.DataField = "Montant";
        bcAuthorizationAmount.ItemStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Right;

        dg.Columns.Add(bcAuthorizationDate);
        dg.Columns.Add(bcAuthorizationStatus);
        dg.Columns.Add(bcAuthorizationResponseCode);
        dg.Columns.Add(bcAuthorizationNum);
        dg.Columns.Add(bcAuthorizationPlace);
        dg.Columns.Add(bcAuthorizationAmount);

        dtCustom.Columns.Add("Date");
        dtCustom.Columns.Add("Statut");
        dtCustom.Columns.Add("CodeReponse");
        dtCustom.Columns.Add("NumAutorisation");
        dtCustom.Columns.Add("Lieu");
        dtCustom.Columns.Add("Montant");

        string sAuthorizationDate = "";
        string sAuthorizationStatus = "";
        string sAuthorizationResponseCode = "";
        string sAuthorizationNum = "";
        string sAuthorizationPlace = "";
        string sAuthorizationAmount = "";

        foreach (DataRow row in dt.Rows)
        {
            sAuthorizationDate = "";
            sAuthorizationStatus = "";
            sAuthorizationResponseCode = "";
            sAuthorizationNum = "";
            sAuthorizationPlace = "";
            sAuthorizationAmount = "";

            if (row.Field<DateTime>("DateLocale") != null)
            {
                try
                {
                    if (row.Field<DateTime>("DateLocale") != null)
                        sAuthorizationDate = row.Field<DateTime>("DateLocale").ToString("dd/MM/yyyy HH:mm:ss");
                }
                catch (Exception ex)
                {
                    sAuthorizationDate = row.Field<DateTime>("DateLocale").ToString("dd/MM/yyyy");
                }
            }

            if (row.Field<string>("ReponseAutorisation") != null)
                sAuthorizationStatus = row.Field<string>("ReponseAutorisation").ToString();
            if (row.Field<string>("CodeReponseAutorisation") != null)
                sAuthorizationResponseCode = row.Field<string>("CodeReponseAutorisation").ToString();
            if (row.Field<string>("NumAutorisation") != null)
                sAuthorizationNum = row.Field<string>("NumAutorisation").ToString();

            if (row.Field<string>("LocalisationAccepteur") != null)
                sAuthorizationPlace = row.Field<string>("LocalisationAccepteur").ToString();

            if (row.Field<Decimal>("MontantTransaction") != null)
                sAuthorizationAmount = tools.getFormattedAmount(row.Field<Decimal>("MontantTransaction").ToString().Replace('.', ','));

            dtCustom.Rows.Add(new object[] { sAuthorizationDate, sAuthorizationStatus, sAuthorizationResponseCode, sAuthorizationNum, sAuthorizationPlace, sAuthorizationAmount });
        }

        dg.DataSource = dtCustom;
        dg.DataBind();

        return dg;
    }
    protected void ExportDataSetToExcel(DataGrid dg, string filename)
    {
        try
        {
            string attachment = "attachment; filename=" + filename;
            Response.ClearContent();
            Response.Charset = "";
            Response.ContentEncoding = System.Text.Encoding.Default;
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            dg.RenderControl(htw);
            Response.Write(sw.ToString());
            Response.Flush();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.Close();
        }
        catch (Exception ex)
        {
        }
    }

    // traitement du rejet de prélèvement
    protected void btnCancelPrelevement_Click(object sender, EventArgs e)
    {
        Button btn = (Button)sender;
        operation.SearchCriteria searchCriteria = (operation.SearchCriteria)ViewState["SearchCriteria"];
        int iRowCount;

        searchCriteria.sOperationType = "2";
        searchCriteria.bIsCobalt = _bIsCobalt;
        searchCriteria.sIBAN = _sIBAN;

        DataTable dt = operation.getUserTransferListTreated(operation.getUserTransferList(searchCriteria, out iRowCount), ref iRowCount);
        if (hdnRefPrelevement.Value.Length > 0)
        {
            int currentRow = 0;
            string sChampNumOPE = "";
            string sChampCodeOPE = "";

            for (currentRow = 0; currentRow < dt.Rows.Count; currentRow++)
            {
                sChampNumOPE = dt.Rows[currentRow].Field<string>("NumOPE");
                sChampCodeOPE = dt.Rows[currentRow].Field<string>("CodeOPE");
                if (sChampNumOPE.Equals(hdnRefPrelevement.Value.ToString()) == true && sChampCodeOPE.Equals("R0P") == true)
                {
                    string sError = "", sRC = "";
                    string sCodeRejet = "PR25"; // PR25 est un refund pour SAB car Le client n'a pas la possibilité de rejeter pour n'importe quel motif

                    //DateTime dtChampDateCreation = dt.Rows[currentRow].Field<DateTime>("DateCreation");
                    //decimal dChampMontantEchange = dt.Rows[currentRow].Field<decimal>("MontantEchange");
                    //string sChampDeviseEchange = dt.Rows[currentRow].Field<string>("DeviseEchange");
                    DateTime dtChampDateReglement = dt.Rows[currentRow].Field<DateTime>("DateReglement");
                    //string sNomDonneurOrdre = dt.Rows[currentRow].Field<string>("NomDO");

                    DateTime dtBlocageRejet = dtChampDateReglement.AddDays(5);


                    if (dtChampDateReglement.CompareTo(DateTime.Now) >= 0)
                    {
                        sCodeRejet = "RV02";
                    }

                    //string sRomain = "NumOpe = " + sChampNumOPE + " créée le " + dtChampDateCreation.ToString();
                    if (dtBlocageRejet.CompareTo(DateTime.Now) > 0)
                    {
                        // on empeche le rejet du prélèvement pendant 5 jours après la date de réglement PRCH-784
                        ShowAlertMessage("Vous pourrez rejeter ce prélèvement à partir du " + dtBlocageRejet.ToString("dd/MM/yyyy") + ".");
                        break;
                    }

                    // rejeter tous les prélèvement de plus de 13 mois
                    dtBlocageRejet = DateTime.Now.AddMonths(-13);
                    if (dtBlocageRejet.CompareTo(dtChampDateReglement) > 0)
                    {
                        // on empeche le rejet du prélèvement pendant 5 jours après la date de réglement PRCH-784
                        ShowAlertMessage("Vous ne pouvez plus rejeter ce prélèvement car il est trop ancien.");
                        break;
                    }

                    bool bResult = false;
                    if (_bIsCobalt)
                    {
                        sCodeRejet = dt.Rows[currentRow].Field<string>("CodeRejet");
                        if (sCodeRejet == "MigratedDemandStatus")
                        {
                            ShowAlertMessage("Contacter le service exploitation");
                            break;
                        }
                        bResult = operation.doCancelPrelevementCobalt(_sRefCustomer, _sIBAN, sChampNumOPE, out sError, out sRC);
                    }
                    else
                    {
                        bResult = operation.doCancelPrelevement(_sRefCustomer, sChampNumOPE, sChampCodeOPE, sCodeRejet, out sError, out sRC);
                    }
                    if (bResult == true)
                    {
                        UpdateOperationList(searchCriteria);
                        ShowAlertMessage("Rejet de prélèvement", "<span class='ui-helper-hidden-accessible'><input type='text'/></span>Votre prélèvement a été rejeté.");
                    }
                    else
                    {
                        ShowAlertMessage("Rejet prélèvement échoué", sError);
                    }
                    break;
                }
            }
        }
    }

    protected void ShowAlertMessage(string sTitle, string message)
    {
        ClientScriptManager cs = Page.ClientScript;
        cs.RegisterStartupScript(this.Page.GetType(), "alertMessageVirementKey", "alertMessageVirement(\"" + sTitle + "\",\"" + message + "\");", true);
    }
    protected void ShowAlertMessage(string message)
    {
        ClientScriptManager cs = Page.ClientScript;
        cs.RegisterStartupScript(this.Page.GetType(), "alertMessageVirementKey", "alertMessageVirement('Erreur',\"" + message + "\");", true);
    }
}
