﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="RequisitionDroitCommunication.aspx.cs" Inherits="RequisitionDroitCommunication" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">

    <%@ Register Src="~/API/AccountGroupManager.ascx" TagPrefix="asp" TagName="AccountGroupManager" %>
    <%@ Register Src="~/API/ClientFileUpload.ascx" TagPrefix="asp" TagName="ClientFileUpload" %>
    <%@ Register Src="~/API/ClientSearch.ascx" TagName="ClientSearch" TagPrefix="asp" %>
    <%@ Register Src="~/API/DropdownListMultiSelect.ascx" TagName="DropdownListMultiSelect" TagPrefix="asp" %>

    <link rel="Stylesheet" href="Styles/ClientFileUpload.css" />
    <link rel="Stylesheet" href="Styles/ClientSearch.css" />
    <link rel="Stylesheet" href="Styles/AccountGroupManager.css" />
    <link rel="Stylesheet" href="Styles/pdf-editor.css" />
    <link rel="Stylesheet" href="Styles/DropdownListMultiSelect.css" />

    <style type="text/css">
        input[type=checkbox].csscheckbox + label.csscheckbox-label {
            font-size:16px;
            font-weight:bold;
            color:#f57527;
        }

        input[type=text], input[type=text][readonly] {
            width: 100%;
            padding: 0 5px;
            box-sizing: border-box;
            height: 26px;
            font-size:1em;
            background-color: white;
            border: 1px solid #a9a9a9;
        }
        select {
            height: 26px !important;
            width: 100%;
            font-size: 1em;
        }
        textarea, textarea[readonly] {
            width:100%;
            min-height:300px;
            resize:vertical;
            box-sizing:border-box;
            font-family:inherit;
            font-size:1em;
            background-color: white;
            border: 1px solid #a9a9a9;
            white-space: pre-wrap;
            overflow: auto;
        }

        .multi-row-footer .multi-row-btn input {
            margin-top:2px;
        }

        .default-form-spacing {
            margin-top:10px;
        }

        .main {
            min-height:unset;
        }

        .r-dcom-panel-loading, .clientsearch-panel-loading
        {
            background-size:20px !important;
        }

        a.ext-link {
            color:#344b56 !important;
            text-decoration:none !important;
            position:relative;
            top:-3px;
        }
        a.ext-link::after {
            content:'';
            display:inline-block;
            background:url('Styles/Img/open_new_white.png') center no-repeat #f57527;
            background-size:14px;
            width:16px;
            height:16px;
            position:relative;
            top:2px;
            left:5px;
        }
        a.ext-link:hover {
            text-decoration:underline !important;
        }

        legend {
            padding: 2px 4px 0px 4px !important;
        }

        .ui-state-disabled {
            opacity:1;
        }
    </style>

    <script type="text/javascript" src="Scripts/pdf-editor-fix.min.js"></script>
    <script type="text/javascript" src="Scripts/jquery.ui.rotatable.min.js"></script>
    <script type="text/javascript">
        function Init() {
            $('.RadioButtonList').buttonset();

            InitDatePicker();
        }

        function InitDatePicker() {
            $('#<%=txtReceptionDate.ClientID %>').mask("99/99/9999");
            $('#<%=txtEmissionDate.ClientID %>').mask("99/99/9999");

            $('#<%=txtReceptionDate.ClientID%>:not([readonly])').datepicker({
                defaultDate: "0",
                dateFormat: "dd/mm/yy",
                dayNamesMin: ["Di", "Lu", "Ma", "Me", "Je", "Ve", "Sa"],
                monthNames: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"],
                firstDay: 1,
                maxDate: "0",
                numberOfMonths: 1,
                onClose: function (selectedDate) {
                    $("#<%=txtEmissionDate.ClientID %>").datepicker("option", "maxDate", selectedDate);
                }
            });
            $('#<%=txtEmissionDate.ClientID%>:not([readonly])').datepicker(
                {
                    defaultDate: "0",
                    dateFormat: "dd/mm/yy",
                    dayNamesMin: ["Di", "Lu", "Ma", "Me", "Je", "Ve", "Sa"],
                    monthNames: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"],
                    firstDay: 1,
                    maxDate: "0",
                    numberOfMonths: 1,
                    onClose: function (selectedDate) {
                        $("#<%=txtReceptionDate.ClientID %>").datepicker("option", "minDate", selectedDate);
                    }
                }
            );
        }

        // PDF EDITOR
        function InitAssets(infos) {
            $('.template').attr('src', infos.template);
            $(infos.assets).each(function () {
                var asset = $(this)[0];

                var color = asset.color;
                var assetDrag = $('<div/>');
                assetDrag.addClass('asset draggable');
                assetDrag.css('top', asset.top + '%');
                assetDrag.css('left', asset.left + '%');
                var assetRotate = $('<div/>');
                assetRotate.addClass('rotatable');

                assetRotate.css('transform', 'rotate(' + asset.deg + 'deg)');
                if (asset.color.trim().length > 0) {
                    assetRotate.css('color', asset.color);
                }
                if (asset.type == 'text')
                    assetRotate.append(asset.content);
                else if (asset.type == 'img') {
                    var img = $('<img/>');
                    img.attr('src', asset.content);
                    assetRotate.append(img);
                }

                assetDrag.append(assetRotate);
                $('.doc-container').append(assetDrag);
            });

            $('.doc-container .draggable').draggable({
                containment: 'parent'
            });
            $('.doc-container .rotatable').rotatable({
                wheelRotate: false,
                snap: true,
                step: 2
            });
        }

        function SaveAssets() {
            var assets = new Array();
            $('.asset').each(function () {
                var asset = new Object();
                asset.top = Math.round(($(this).position().top * 100 / $('.doc-container').innerHeight()) * 100) / 100;
                asset.left = Math.round(($(this).position().left * 100 / $('.doc-container').innerWidth()) * 100) / 100;
                asset.deg = GetRotationDegrees($(this).find('.rotatable'));
                if ($(this).find('img').length > 0) {
                    asset.content = $(this).find('img').attr('src');
                    asset.type = 'img';
                }
                else {
                    asset.content = $(this).text();
                    asset.type = 'text';
                }
                assets.push(asset);
            });

            //var infos = new Object();
            //infos.template = $('.template').attr('src');
            //infos.assets = assets;
            //console.log(infos);

            //console.log(JSON.stringify(assets));

            var JSONdata = new Object();
            JSONdata.assets = assets;

            $('#<%=hfAssets.ClientID%>').val(JSON.stringify(JSONdata));
        }

        function GetRotationDegrees(obj) {
            var matrix = obj.css("-webkit-transform") ||
            obj.css("-moz-transform") ||
            obj.css("-ms-transform") ||
            obj.css("-o-transform") ||
            obj.css("transform");
            if (matrix !== 'none') {
                var values = matrix.split('(')[1].split(')')[0].split(',');
                var a = values[0];
                var b = values[1];
                var angle = Math.round(Math.atan2(b, a) * (180 / Math.PI));
            } else { var angle = 0; }
            return (angle < 0) ? angle + 360 : angle;
        }

        function ShowPDFEditor() {
            $('#dialog-pdfeditor').dialog({
                draggable: false,
                resizable: false,
                width: 950,
                //dialogClass: "no-close",
                modal: true,
                title: 'Signer la réquisition judiciaire',
            });
            $('#dialog-pdfeditor').parent().appendTo(jQuery("form:first"));
        }
        function HidePDFEditor() {
            $('#dialog-pdfeditor').dialog('close');
        }
        function GeneratePDF() {
            SaveAssets();
            HidePDFEditor();
            $('#<%=btnGeneratePDF.ClientID%>').click();
        }

        function ShowDialogCheckResponse(file) {
            if(file != null)
                $('#frame-response').attr('src', file + '#zoom=150');

            $('#dialog-check-response').dialog({
                draggable: false,
                resizable: false,
                width: 940,
                //dialogClass: "no-close",
                modal: true,
                title: 'Vérification de la réquisition signée'
            });
            $('#dialog-check-response').parent().appendTo(jQuery("form:first"));

        }
        function HideDialogCheckResponse() {
            $('#dialog-check-response').dialog('close');
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

    <div class="r-dcom-panel-loading panel-loading"></div>

    <h2 style="color:#344b56;text-transform:uppercase;">Réquisition judiciaire & Droit de communication</h2>

    <div id="r-dcom-form" style="margin-top:10px">
        
        <asp:UpdatePanel ID="upForm" runat="server" UpdateMode="Conditional">
            <ContentTemplate>

                <asp:Panel ID="panelForm" runat="server">

                    <asp:Panel ID="panelTreatmentDate" runat="server" Visible="false" style="margin:10px 0 5px 0">
                        <div class="label">
                            Date traitement
                        </div>
                        <div>
                            <asp:TextBox ID="txtTreatmentDate" runat="server" ReadOnly="true"></asp:TextBox>
                        </div>
                    </asp:Panel>

                    <fieldset style="padding:5px 10px 10px 10px;border:1px solid #bbb;background-color:#fafafa;margin:10px 0 0 0">
                        <legend class="label" style="color:#344b56">Document</legend>
                        <asp:ClientFileUpload ID="clientFileUpload1" runat="server" HideSaveButton="true" HideDesc="true" TypeFilters="REQ;DCOM" AllowedDocType=".pdf" />

                        <asp:Panel ID="panelClientFileUploaded" runat="server" Visible="false">
                            <div class="label">Type</div>
                            <div>
                                <asp:Label ID="lblFileUploadedType" runat="server"></asp:Label>
                            </div>
                            <div class="label">Fichier</div>
                            <div>
                                <asp:HyperLink ID="hlFileUploaded" runat="server" CssClass="ext-link" Target="_blank"></asp:HyperLink>
                            </div>
                        </asp:Panel>
                    </fieldset>

                    <div class="default-form-spacing">
                        <div class="label">Client</div>
                        <asp:ClientSearch ID="ClientSearch1" runat="server" />
                        <asp:Panel ID="panelClient" runat="server" Visible="false">
                            <asp:Label ID="lblClient" runat="server"></asp:Label>
                        </asp:Panel>
                    </div>

                    <asp:UpdatePanel ID="upAdditionalInfos" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>

                            <asp:Panel ID="panelReqInfos" runat="server" CssClass="hidden">

                                <div style="margin-top:12px">
                                    <input type="checkbox" id="ckbGardeAVue" runat="server" class="csscheckbox" />
                                    <label for="<%= ckbGardeAVue.ClientID %>" class="csscheckbox-label">Garde à vue</label>
                                </div>

                                <div class="default-form-spacing">
                                    <div class="label">
                                        Date émission
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtEmissionDate" runat="server" MaxLength="10"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="default-form-spacing">
                                    <div class="label">
                                        Date réception
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtReceptionDate" runat="server" MaxLength="10"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="default-form-spacing">
                                    <div class="label">
                                        Numéro PV
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtPVNumber" runat="server" MaxLength="50"></asp:TextBox>
                                    </div>
                                </div>

                                <div style="margin-top:5px">
                                    <div class="label" style="position:relative;top:5px;">
                                        Motif
                                    </div>
                                    <div style="margin-top:5px">
                                        <asp:DropdownListMultiSelect ID="ddlMultiSelect1" runat="server" AlphaOrder="false" />
                                    </div>
                                </div>

                                <asp:Panel ID="panelAccountGroupManager" runat="server" CssClass="hidden">
                                    <div class="default-form-spacing">
                                        <span class="label">En lien avec</span>
                                        <asp:AccountGroupManager ID="AccountGroupManager1" runat="server" />
                                    </div>
                                </asp:Panel>

                                <div class="default-form-spacing">
                                    <input type="checkbox" id="ckbNoClose" runat="server" class="csscheckbox" />
                                    <label for="<%= ckbNoClose.ClientID %>" class="csscheckbox-label">Ne pas clôturer</label>
                                </div>
                            </asp:Panel>

                            <asp:Panel ID="panelDComInfos" runat="server" CssClass="hidden">
                                <div class="default-form-spacing">
                                    <div class="label">
                                        Demandeur
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtDComOrigin" runat="server" MaxLength="200" placeholder="CAF de Paris, DGFIP de l'Oise, etc."></asp:TextBox>
                                    </div>
                                </div>
                            </asp:Panel>

                        </ContentTemplate>
                    </asp:UpdatePanel>

                    <div id="form-response">
                        <fieldset style="padding:5px 10px 10px 10px;border:1px solid #bbb;background-color:#fafafa;margin:10px 0 0 0">
                            <legend class="label" style="color:#344b56">Réponse</legend>

                            <div id="dialog-pdfeditor" style="display:none">
                                <div class="doc-container">
                                    <img class="template" src="" />
                                </div>
                                <div style="text-align:center">
                                    <input type="button" value="Annuler" class="button" onclick="HidePDFEditor();" style="margin-right:50px" />
                                    <input type="button" value="Terminer" class="button" onclick="GeneratePDF();" />
                                </div>
                            </div>
                            <asp:UpdatePanel ID="upEditFile" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:Panel ID="panelEditFile" runat="server" CssClass="hidden" style="padding-top:5px">
                                        <asp:Button ID="btnEditFile" runat="server" Text="Signer la réquisition judiciaire" OnClick="btnEditFile_Click" CssClass="button" />
                                        <asp:Button ID="btnShowFileEdited" runat="server" Text="Voir la réquisition signée" Visible="false" CssClass="button" OnClick="btnShowFileEdited_Click" />
                                        <asp:HiddenField ID="hfAssets" runat="server" />
                                        <asp:Button ID="btnGeneratePDF" runat="server" OnClick="btnGeneratePDF_Click" style="display:none" />
                                    </asp:Panel>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="btnGeneratePDF" EventName="click" />
                                </Triggers>
                            </asp:UpdatePanel>

                            <div style="margin-top:5px;">
                                <input type="checkbox" id="ckbFicheSynthese" runat="server" class="csscheckbox" />
                                <label for="<%= ckbFicheSynthese.ClientID %>" class="csscheckbox-label">Fiche de synthèse</label>
                            </div>

                            <asp:UpdatePanel ID="upReleve" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:Panel ID="panelReleve" runat="server" Visible="false">
                                        <div class="label default-form-spacing">
                                            Relevés
                                        </div>
                                        <asp:Repeater ID="rptReleve" runat="server">
                                            <HeaderTemplate>
                                                <table class="defaultTable2">
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr class='<%#Container.ItemIndex % 2 == 0 ? "row-even" : "row-odd" %>'>
                                                    <td style="width:10px">
                                                        <asp:CheckBox ID="ckbSelectReleve" runat="server" Enabled='<%#FormEditable %>' Checked='<%#Eval("Check") %>' />
                                                    </td>
                                                    <td style="text-transform:capitalize">
                                                        <asp:Label ID="lblDate" runat="server" Text='<%#Eval("Date") %>' AssociatedControlID="ckbSelectReleve"></asp:Label>
                                                        <asp:HiddenField ID="hdnFilename" runat="server" Value='<%#Eval("File") %>' />
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </asp:Panel>
                                </ContentTemplate>
                            </asp:UpdatePanel>

                            <asp:UpdatePanel ID="upResponse" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>

                                    <div class="default-form-spacing">
                                        <div class="label">
                                            Envoi par
                                        </div>
                                        <asp:RadioButtonList ID="rblResponseMethod" runat="server" RepeatDirection="Horizontal" CssClass="RadioButtonList" OnSelectedIndexChanged="rblResponseMethod_SelectedIndexChanged" AutoPostBack="true">
                                            <asp:ListItem Value="email" Text="E-mail"></asp:ListItem>
                                            <asp:ListItem Value="courrier" Text="Courrier"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>

                                    <asp:Panel ID="panelEmailDetail" runat="server" Visible="false">
                                        <div class="label default-form-spacing">
                                            Adresse
                                        </div>
                                        <div>
                                            <asp:TextBox ID="txtEmailAddress" runat="server" placeholder="veuillez séparer les adresses par un point-virgule (;)"></asp:TextBox>
                                        </div>
                                        <div class="label default-form-spacing">
                                            Objet
                                        </div>
                                        <div>
                                            <asp:TextBox ID="txtEmailSubject" runat="server"></asp:TextBox>
                                        </div>
                                    </asp:Panel>

                                    <asp:Panel ID="panelCourrierDetail" runat="server" Visible="false">
                                        <div class="label default-form-spacing">
                                            Destinataire
                                        </div>
                                        <div>
                                            <asp:TextBox ID="txtDest" runat="server" MaxLength="100"></asp:TextBox>
                                        </div>
                                        <div class="label default-form-spacing">
                                            Adresse
                                        </div>
                                        <div>
                                            <asp:TextBox ID="txtAddress1" runat="server" MaxLength="100"></asp:TextBox>
                                        </div>
                                        <div class="label default-form-spacing">
                                            Adresse 2
                                        </div>
                                        <div>
                                            <asp:TextBox ID="txtAddress2" runat="server" MaxLength="100"></asp:TextBox>
                                        </div>
                                        <div class="default-form-spacing">
                                            <div class="label">
                                                Code postal
                                            </div>
                                            <div>
                                                <asp:TextBox ID="txtZipcode" runat="server" MaxLength="5"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="default-form-spacing">
                                            <div class="label">
                                                Ville
                                            </div>
                                            <div>
                                                <asp:TextBox ID="txtCity" runat="server"></asp:TextBox>
                                            </div>
                                        </div>
                                    </asp:Panel>

                                    <div class="default-form-spacing">
                                        <div class="label">
                                            Contenu
                                        </div>
                                        <asp:TextBox ID="txtResponseContent" runat="server" TextMode="MultiLine"></asp:TextBox>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </fieldset>
                    </div>

                    <asp:UpdatePanel ID="upSave" runat="server">
                        <ContentTemplate>
                            <div style="margin-top:20px;text-align:center">
                                <asp:HiddenField ID="hdnBtnSaveClicked" runat="server" />
                                <asp:Button ID="btnSave" runat="server" Text="Enregistrer" OnClick="btnSave_Click" CssClass="button" style="width:100%" />
                            </div>
                            <asp:Panel ID="panelSavePostTreatment" runat="server" Visible="false" style="margin-top:20px;text-align:center">
                                <asp:Button ID="btnPostTreatmentSave" runat="server" Text="Enregistrer les modifications" CssClass="button" OnClick="btnSavePostTreatment_Click" />
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </asp:Panel>

                <asp:Panel ID="panelSaved" runat="server" Visible="false" HorizontalAlign="Center">
                    <div style="font-size:1.2em;font-weight:bold;color:green;text-transform:uppercase;padding:25px 0 40px 0;">
                        Enregistrement effectué
                    </div>
                    <div>
                        <asp:Button ID="btnNew" runat="server" Text="Traiter une nouvelle demande" OnClientClick="return ReloadForm();" CssClass="button"></asp:Button>
                        <asp:Button ID="btnMenu" runat="server" PostBackUrl="Default.aspx" Text="Retour à l'accueil" CssClass="button"></asp:Button>
                    </div>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <div id="dialog-check-response" style="display: none">
        <div id="ResponsePDFPanel" style="margin-bottom: 10px">
            <asp:UpdatePanel ID="upResponseMethod" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div style="width: 900px; text-align: center; margin: auto">
                        <iframe id="frame-response" width="899" height="600"></iframe>
                    </div>
                    <div style="width: 55%; margin: 20px auto 0 auto">
                        <b style="position: relative; top: 5px">Le document de réponse est-il correct ?</b>
                        <div style="float: right">
                            <%--<input type="button" class="button" value="Oui" onclick="ShowDialogResponseMethod();" style="font-size:14px;" />--%>
                            <input type="button" class="button" value="Oui" onclick="HideDialogCheckResponse();" style="font-size: 14px;" />
                            <asp:Button ID="btnNewEdit" runat="server" Text="Non, modifier" CssClass="button" Style="font-size: 14px;" OnClientClick="HideDialogCheckResponse();" OnClick="btnEditFile_Click" />
                        </div>
                        <div style="clear: both"></div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" Runat="Server">

    <script type="text/javascript">

        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(RequisitionsDroitsCommunicationBeginRequestHandler);
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(RequisitionsDroitsCommunicationEndRequestHandler);
        function RequisitionsDroitsCommunicationBeginRequestHandler(sender, args) {
            try {
                pbControl = args.get_postBackElement();

                if (pbControl != null) {
                    console.log(pbControl.id);
                    if (pbControl.id.indexOf('btnSave') != -1
                        || pbControl.id.indexOf('btnUploadServerFile') != -1
                         || pbControl.id.indexOf('btnUploadZendeskFile') != -1
                         || pbControl.id.indexOf('btnFileUploaded') != -1) {
                        if ($('#<%=hdnBtnSaveClicked.ClientID%>').val() == '1')
                            ShowRequisitionsDroitsCommunicationLoading('#<%=btnSave.ClientID%>', '1000');
                        else {
                            if ($('#<%=btnEditFile.ClientID%>').length > 0)
                                ShowRequisitionsDroitsCommunicationLoading('#<%=btnEditFile.ClientID%>', '1000');
                            else ShowRequisitionsDroitsCommunicationLoading('#<%=btnShowFileEdited.ClientID%>', '1000');
                        }
                    }
                    else if (pbControl.id.indexOf('btnShowFileEdited') != -1)
                        ShowRequisitionsDroitsCommunicationLoading('#<%=btnShowFileEdited.ClientID%>', '1000');
                    else if (pbControl.id.indexOf('btnEditFile') != -1)
                        ShowRequisitionsDroitsCommunicationLoading('#<%=btnEditFile.ClientID%>');
                    else if (pbControl.id.indexOf('ddlDocumentType') != -1
                        || pbControl.id.indexOf('btnGeneratePDF') != -1)
                        ShowRequisitionsDroitsCommunicationLoading('#r-dcom-form');
                    else if (pbControl.id.indexOf('rblResponseMethod') != -1)
                        ShowRequisitionsDroitsCommunicationLoading('#form-response');
                    else if (pbControl.id.indexOf('btnPostTreatmentSave' != -1))
                        ShowRequisitionsDroitsCommunicationLoading('#<%=btnPostTreatmentSave.ClientID%>', '1000');
                }
            }
            catch (e) {

            }
        }
        function RequisitionsDroitsCommunicationEndRequestHandler(sender, args) {
            var pbControl = sender._postBackSettings.sourceElement;

            if (pbControl != null) {
                //console.log(pbControl.id);
                if (pbControl.id.indexOf('btnSave') != -1
                    || pbControl.id.indexOf('ddlDocumentType') != -1
                    || pbControl.id.indexOf('rblResponseMethod') != -1
                    || pbControl.id.indexOf('btnGeneratePDF') != -1
                    || pbControl.id.indexOf('btnShowFileEdited') != -1
                    || pbControl.id.indexOf('btnPostTreatmentSave') != -1
                    || pbControl.id.indexOf('btnSearchTicketID') != -1
                    || pbControl.id.indexOf('ddlMultiSelect1_btnAdd') != -1
                    || pbControl.id.indexOf('btnClientSearch') != -1)
                    HideRequisitionsDroitsCommunicationLoading();
            }
        }
        function ShowRequisitionsDroitsCommunicationLoading(controlID, zIndex) {
            if (controlID == null)
                controlID = '#r-dcom-form';
            var panel = $(controlID);

            $('.r-dcom-panel-loading').show();
            $('.r-dcom-panel-loading').width(panel.outerWidth());
            $('.r-dcom-panel-loading').height(panel.outerHeight());

            if (zIndex != null)
                $('.r-dcom-panel-loading').css('z-index', zIndex);

            $('.r-dcom-panel-loading').position({
                my: 'center',
                at: 'center',
                of: controlID
            });

            //console.log('show');
        }
        function HideRequisitionsDroitsCommunicationLoading() {
            $('.r-dcom-panel-loading').hide();

            //console.log('hide');
        }

        function ReloadForm()
        {
            ShowRequisitionsDroitsCommunicationLoading('#<%=btnNew.ClientID%>');
            $('.r-dcom-panel-loading.panel-loading').css('left', '+=9'); // weird bug fix
            window.location.reload();

            return false;
        }

        function ShowFileUploadLoading(controlID) {
            //console.log(controlID);
            if ($('#<%=hdnBtnSaveClicked.ClientID%>').val() == '1')
                ShowRequisitionsDroitsCommunicationLoading('#<%=btnSave.ClientID%>', '1000');
            else {
                if ($('#<%=btnEditFile.ClientID%>').length > 0 || $('#<%=btnShowFileEdited.ClientID%>').length > 0) {
                    if ($('#<%=btnEditFile.ClientID%>').length > 0)
                        ShowRequisitionsDroitsCommunicationLoading('#<%=btnEditFile.ClientID%>', '1000');
                    else ShowRequisitionsDroitsCommunicationLoading('#<%=btnShowFileEdited.ClientID%>', '1000');
                }
                else ShowRequisitionsDroitsCommunicationLoading('#<%=btnSave.ClientID%>', '1000');
            }
        }
    </script>

</asp:Content>

