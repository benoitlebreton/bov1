﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Transfer.aspx.cs" Inherits="Transfer" %>
<%@ Register Src="~/API/ReturnFunds.ascx" TagName="ReturnFunds" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <link href="Styles/ReturnFunds.css" type="text/css" rel="Stylesheet" />
    <link href="Styles/DropdownListMultiSelect.css" type="text/css" rel="Stylesheet" />

    <script type="text/javascript">
        function ShowPanelLoading(panelID) {
            console.log(panelID);
            var panel = $('#'+panelID);
            if(panelID.trim().length > 0 && panel != null){

                $('.panel-loading').show();
                $('.panel-loading').width(panel.outerWidth());
                $('.panel-loading').height(panel.outerHeight());

                $('.panel-loading').position({
                    my: 'center',
                    at: 'center',
                    of: panel
                });
            }
        }
        function HidePanelLoading(){
            $('.panel-loading').hide();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

    <h2 style="color: #344b56;text-transform:uppercase;">
        Virement externe
    </h2>

    <asp:UpdatePanel ID="upReturnFunds" runat="server">
        <ContentTemplate>
            <div id="div-return-funds" style="margin-top:20px">
                <div>
                    <asp:ReturnFunds ID="ReturnFunds1" runat="server" />
                </div>
                <div style="text-align:center">
                    <asp:Button ID="btnExecuteReturnFunds" runat="server" Text="Enregistrer" OnClick="btnExecuteReturnFunds_Click" CssClass="button" />
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" Runat="Server">
    
    <div class="panel-loading"></div>

    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginClientDetailsRequestHandler);
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndClientDetailsRequestHandler);
        function BeginClientDetailsRequestHandler(sender, args) {
            try{
                pbControl = args.get_postBackElement();
                console.log(pbControl.id);
                var containerID = '';
                if (pbControl != null) {
                    if (pbControl.id.indexOf('btnExecuteReturnFunds') != -1)
                        containerID = "div-return-funds";
                }

                if(containerID.length > 0)
                    ShowPanelLoading(containerID);
            }
            catch(e)
            {

            }
        }
        function EndClientDetailsRequestHandler(sender, args) {
            HidePanelLoading();
        }
    </script>

</asp:Content>

