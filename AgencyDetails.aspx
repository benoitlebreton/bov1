﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="AgencyDetails.aspx.cs" Inherits="AgencyDetails" %>

<%@ Register Src="~/API/StatsPRO.ascx" TagName="StatsPRO" TagPrefix="asp" %>
<%@ Register Src="~/API/AgencyAlert.ascx" TagName="KYCAlert" TagPrefix="asp" %>
<%@ Register Src="~/API/StaffManagement.ascx" TagName="StaffManagement" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">

    <link href="Styles/StatsPRO.css" rel="Stylesheet" type="text/css" />
    <link href="Styles/Scoring.css" rel="Stylesheet" type="text/css" />
    <link href="Styles/StaffManagement.css" rel="Stylesheet" type="text/css" />
    <style type="text/css">
        .page {
            width:1030px;
        }
        li {
            list-style-type:disc;
        }

        .alertPastille {
            z-index: 100;
            position: absolute;
        }

        .semiTransparent {
            filter: alpha(opacity=60);
            opacity: 0.6;
        }

        .scoring-details {
            display:block;
            position:static;
            width: auto;
        }
    </style>

    <script type="text/javascript" src="Scripts/jquery.marquee.min.js"></script>
    <script type="text/javascript" src="Scripts/ui.tabs.paging.js"></script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC_bY7BDy0wM76i-UEV-MSdjMrKDKxELJc" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            InitControls();
            if($('#<%=hdnNbAlert.ClientID%>').val().trim().length > 0)
                showNbAlert($('#<%=hdnNbAlert.ClientID%>').val().trim(), "lblNbAlert", "divNbAlert", "<%=panelAgencyAlerts.ClientID%>");
        });

        function InitControls() {

            $(".AgencyDetails").tabs({
                activate: function (event, ui) {
                    $('#ar-loading').hide();
                    if ($("#<%=panelAgencyAddress.ClientID %>").is(':visible')) {
                        InitGMap();
                    }
                    else if ($("#<%=panelAgencyActivity.ClientID %>").is(':visible')) {
                        //console.log($('#<%=rblPeriod.ClientID%> input:checked').length);
                        if ($('#<%=rblPeriod.ClientID%> input:checked').length == 0) {
                            $('#<%=rblPeriod.ClientID%> input[type=radio]').each(function () {
                                if ($(this).val() == "5")
                                    $(this).attr('checked', true);
                                else $(this).attr('checked', false);

                                RefreshPeriodControl();
                            });
                            $('#<%=btnLoadFirstActivity.ClientID%>').click();
                        }
                    }
                    else if ($('#<%=panelAgencyDashboard.ClientID %>').is(':visible')) {
                        $('#<%=btnLoadDashboard.ClientID%>').click();
                    }
                    else if ($("#<%=panelAgencySettings.ClientID %>").is(':visible')) {
                        $('#<%=btnLoadAgencySettings.ClientID%>').click();
                    }

                    if ($('#<%=panelAgencyAlerts.ClientID%>').is(':visible'))
                    {
                        $("#divNbAlert").removeClass("semiTransparent");
                    }
                    else $("#divNbAlert").addClass("semiTransparent");

                    $('#<%=hdnActiveTab.ClientID%>').val($(".ClientDetails").tabs("option", "active"));
                }
            });
            $(".AgencyDetails").tabs('paging', { cycle: true, follow: true, followOnActive: true, activeOnAdd: true });

            $('.RadioButtonList').buttonset();

            $('#mpad-details-popup').dialog({
                autoOpen: false,
                resizable: false,
                draggable: false,
                modal: true,
                title: "Détails borne"
            });
            $('#mpad-details-popup').parent().appendTo(jQuery("form:first"));

            $('#pos-details-popup').dialog({
                autoOpen: false,
                resizable: false,
                draggable: false,
                modal: true,
                title: "Détails TPE"
            });
            $('#pos-details-popup').parent().appendTo(jQuery("form:first"));

            $('#mpad-maintenance-popup-confirm').dialog({
                autoOpen: false,
                resizable: false,
                draggable: false,
                modal: true,
                title: 'Alerte',
                width: '350px',
                dialogClass: "no-close",
                buttons: [{ text: "Oui", click: function () { $('#<%=btnForceCloseMaintenance.ClientID%>').click(); } }, { text: "Non", click: function () { $(this).dialog("close"); } }]
            });
            $('#mpad-maintenance-popup-confirm').parent().appendTo(jQuery("form:first"));
            $('#mpad-maintenance-popup').dialog({
                autoOpen: false,
                resizable: false,
                draggable: false,
                modal: true,
                title: "Actions borne",
                width: '450px',
                beforeClose: function (event, ui) {
                    //console.log($('#<%=rblRemoteAccess.ClientID%> input:checked').val());
                    if ($('#<%=rblRemoteAccess.ClientID%> input:checked').val() == '1') {
                        $('#mpad-maintenance-popup-confirm').dialog('open');
                        return false;
                    }
                }
            });
            $('#mpad-maintenance-popup').parent().appendTo(jQuery("form:first"));
        }

        function RefreshPeriodControl() {
            $('.RadioButtonList').buttonset('refresh');
        }

        function InitPostBack() {

        }

        function InitGMap() {
            var lat = $('#<%=hdnLatitude.ClientID%>').val();
            var long = $('#<%=hdnLongitude.ClientID%>').val();
            //console.log(lat +'-'+long);

            var LatLng = new google.maps.LatLng(lat, long);

            var mapOptions = {
                zoom: 15,
                center: LatLng
            };

            var map = new google.maps.Map($('#map-canvas')[0],
                mapOptions);

            var imageUrl = 'Styles/img/gmap-store-marker.png';
            var markerImage = new google.maps.MarkerImage(imageUrl, new google.maps.Size(27, 43));
            var marker = new google.maps.Marker({
                position: LatLng,
                icon: markerImage,
                map: map
            });
        }

        function showMpadDetail(sSerial, isLocked, sSoftRelease, sSoftReleaseABM, sInitStatusText, sCode, sCodeCreationDate, sLastCnxDate, sRemoteAccessReq) {
            var sStatus = sInitStatusText;

            $('#sMpadSerial').html(sSerial);
            $('#sMpadStatus').html(sStatus);
            $('#sMpadRelease').html('v' + sSoftRelease.trim());
            $('#sMpadReleaseABM').html('v' + sSoftReleaseABM.trim());
            if (sCode != null && sCode.trim().length > 0) {
                $('#divMpadCode').show();
                $('#sMpadCode').html(sCode.trim());
                if (sCodeCreationDate.trim().length > 0)
                    $('#sMpadCodeDate').html("généré le <span class=\"font-orange\" style=\"font-weight:bold\">" + sCodeCreationDate.trim().substr(0, 10) + "</span>");
            }
            else
                $('#divMpadCode').hide();

            if (sLastCnxDate != null)
                $('#sMpadLastCnxDate').html(sLastCnxDate.trim());

            if (sRemoteAccessReq != null)
                $('#sMpadRemoteAccessRequested').html(sRemoteAccessReq.trim());

            $('#divMpadDetails').show();
            $('#mpad-details-popup').dialog('open');
        }
        function showPosDetail(sSerial, isLocked, sSoftRelease, sLastCnxDate, sInitStatusText, sPosType) {
            var sStatus = sInitStatusText;

            $('#sPosType').html(sPosType);
            $('#sPosSerial').html(sSerial);
            $('#sPosStatus').html(sStatus);
            $('#sPosRelease').html(sSoftRelease.trim());
            if (sLastCnxDate != null)
                $('#sPosLastCnxDate').html(sLastCnxDate.trim());

            if (sPosType != null && sPosType.trim().length > 0)
                $('#imgPosDetails').attr("src", "mobile/Styles/Img/" + sPosType + ".png");

            $('#pos-details-popup').dialog('open');
        }

        function ShowMaintenancePopup() {
            $('.RadioButtonList').buttonset();
            $('.RadioButtonList .ui-button').css('height', '24px');
            $('.RadioButtonList .ui-button .ui-button-text').css('padding-top', '0').css('padding-bottom', '0');

            $(".action-mpad-help").tooltip({
                position: {
                    my: "center bottom-10",
                    at: "center top",
                    using: function (position, feedback) {
                        $(this).css(position);
                        $("<div>")
                            .addClass("arrow")
                            .addClass(feedback.vertical)
                            .addClass(feedback.horizontal)
                            .appendTo(this);
                    }
                }
            });

            $('#mpad-maintenance-popup').dialog('open');
        }

        function CloseAllDialogs() {
            $('#mpad-maintenance-popup').dialog('close');
            $('#mpad-maintenance-popup-confirm').dialog('close');
        }

        function GoToTab(tab) {
            //console.log(tab);
            var index = 0;
            for (i = 0; i < $('.ui-tabs-nav li').length; i++) {
                if ($('.ui-tabs-nav li:eq(' + i + ')').attr('aria-controls') == tab) {
                    index = i;
                    break;
                }
            }
            $(".AgencyDetails").tabs("option", "active", index);
            window.scrollTo(0, document.body.scrollHeight);
        }

        function sendContract() {
            //page
            $(this).removeAttr('disabled').attr('disabled', 'disabled');
        }

        function SaveAgencyInformation() {
            if (checkAgencyInformation())
                $('#<%=btnSaveAgencyInformation.ClientID%>').click();
        }

        function SaveAgencyAddress() {
            if (checkAgencyAddress())
                $('#<%=btnSaveAgencyAddress.ClientID%>').click();
        }
        function checkAgencyAddress() {
            var isOK = true;

            return isOK;
        }

        function checkAgencyInformation() {
            var isOK = true;
            var sMessage = "Veuillez corriger les champs suivants : <ul>";

            if (!isSiretValid($('#<%=txtSiret.ClientID%>').val())) {
                isOK = false;
                sMessage += "<li>SIRET non valide</li>";
            }

            sMessage += "</ul>";

            if (!isOK)
                ShowMessage("ERREUR", sMessage);

            return isOK;
        }

        function isSiretValid(siret) {
            var isValid;
            if ((siret.length != 14) || (isNaN(siret)))
                isValid = false;
            else {
                // Donc le SIRET est un numérique à 14 chiffres
                // Les 9 premiers chiffres sont ceux du SIREN (ou RCS), les 4 suivants
                // correspondent au numéro d'établissement
                // et enfin le dernier chiffre est une clef de LUHN.
                var somme = 0;
                var tmp;
                for (var cpt = 0; cpt < siret.length; cpt++) {
                    if ((cpt % 2) == 0) { // Les positions impaires : 1er, 3è, 5è, etc...
                        tmp = siret.charAt(cpt) * 2; // On le multiplie par 2
                        if (tmp > 9)
                            tmp -= 9;// Si le résultat est supérieur à 9, on lui soustrait 9
                    }
                    else
                        tmp = siret.charAt(cpt);
                    somme += parseInt(tmp);
                }
                if ((somme % 10) == 0)
                    isValid = true; // Si la somme est un multiple de 10 alors le SIRET est valide
                else
                    isValid = false;
            }
            return isValid;
        }

        function ShowMessage(title, message) {
            var sTitle = "Message";
            if (message != null && message.trim().length > 0)
                $('#<%=lblInfoMessage.ClientID%>').html(message.trim());
                if (title != null && title.trim().length > 0)
                    sTitle = title;

                $('#dialog-message').dialog({
                    resizable: false,
                    draggable: false,
                    modal: true,
                    minWidth: 500,
                    title: sTitle,
                    buttons: [{ text: 'Fermer', click: function () { $(this).dialog('close'); } }]
                });

        }

        var iCpt = 4;
        var interval = null;
        function OpenZendeskTab(id) {
            OpenZendeskInterval(id);
            interval = setInterval(function(){OpenZendeskInterval(id)}, 1000);
        }
        function OpenZendeskInterval(id) {
            iCpt--;
            $('.zendesk-interval').html(iCpt);
            if (iCpt == 0) {
                $('#zendesk-countdown').hide();
                clearInterval(interval);
                var newwin = window.open('https://comptenickel.zendesk.com/agent/tickets/new/1?requester_id=' + id);
                if (!newwin)
                    AlertMessage("Erreur", "Veuillez autoriser l'ouverture de popup puis recharger la page.", null, null);
            }
        }
    </script>

    <style type="text/css">
        #ar-loading {
            background:url('Styles/img/loading2.gif') no-repeat center;
            background-size: 35px 27px;
            display:none;
            position:absolute;
            background-color:rgba(255, 255, 255, 0.5);
            z-index:10000;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

    <div id="ar-loading"></div>

    <h2 style="color: #344b56;margin-bottom:10px;text-transform:uppercase">Informations agence</h2>
    <div class="table" style="margin-bottom:10px">
        <div class="row">
            <div class="cell" style="vertical-align:top;">
                <div style="font-weight:bold; color:#f57527">ID</div>
                <div><asp:Label ID="lblID" runat="server"></asp:Label></div>
            </div>
            <div class="cell" style="vertical-align:top;padding-left:20px;">
                <div style="font-weight:bold; color:#f57527">ID BiMedia</div>
                <div><asp:Label ID="lblIDBimedia" runat="server"></asp:Label></div>
            </div>
            <div class="cell" style="padding-left:20px;vertical-align:top;">
                <div style="font-weight:bold; color:#f57527">ID SAB</div>
                <div><asp:Label ID="lblIDPDVSAB" runat="server"></asp:Label></div>
            </div>
            <div class="cell" style="padding-left:20px;vertical-align:top;">
                <div style="font-weight:bold; color:#f57527">1ère activation</div>
                <div><asp:Label ID="lblFirstActivation" runat="server"></asp:Label></div>
            </div>
            <div class="cell" style="padding-left:20px;vertical-align:top;">
                <div style="font-weight:bold; color:#f57527">Région commerciale</div>
                <div><asp:Label ID="lblRegionCommerciale" runat="server" style="width:100%"></asp:Label></div>
            </div>
        </div>
    </div>

    <div>
        <asp:Button ID="btnPrev" runat="server" Text="Recherche agence" PostBackUrl="~/AgencySearch.aspx" CssClass="button" />
    </div>

    <asp:HiddenField ID="hdnNbAlert" runat="server" />
    <div id="divNbAlert" class="alertPastille semiTransparent" style="display:none;position:absolute;top:-15px;right:-15px">
        <div style="width:30px;height:30px;text-align:center;line-height:30px;background:url(Styles/Img/red_circle.png) no-repeat center">
            <%--<img id="imgNbAlert" alt="" src="Styles/Img/red_circle.png" style="position:absolute; top:0; left:0; z-index:1;width:30px; height:30px;" />--%>
            <%--<div style="position:absolute; top:0; left:0; z-index:2; display:inline; width:30px; height:30px; vertical-align:middle; text-align:center; padding-top:6px">--%>
                <label id="lblNbAlert" style="font-size:12px; font-weight:bold; color:#fff;"></label>
            <%--</div>--%>
        </div>
    </div>

    <asp:HiddenField ID="hdnActiveTab" runat="server" />
    <div style="border: 3px solid #344b56; border-radius: 8px; width: 100%; margin: auto;margin-bottom:20px; text-transform:uppercase" class="AgencyDetails">
        <asp:Repeater ID="rptTabs" runat="server">
            <HeaderTemplate>
                <ul>
            </HeaderTemplate>
            <ItemTemplate>
                <%# Eval("tab") %>
            </ItemTemplate>
            <FooterTemplate>
                </ul>
            </FooterTemplate>
        </asp:Repeater>

        <asp:Panel ID="panelAgencyGeneralInfo" runat="server">
            <div id="divAgencyGeneralInfo" style="padding: 10px; margin-bottom: 10px;">
                <asp:UpdatePanel ID="upAgencyGeneralInfo" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <%--<div class="table" style="width:100%;border-collapse:collapse">
                            <div class="row">
                                <div class="cell" style="width:33%">
                                    <div style="display:inline-block;text-align:left">
                                        <div class="label">
                                            ID Interne
                                        </div>
                                        <div>
                                            <asp:Label ID="lblID" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                                <div class="cell" style="width:33%;text-align:center">
                                    <div style="display:inline-block;text-align:left">
                                        <div class="label">
                                            ID BiMedia
                                        </div>
                                        <div>
                                            <asp:Label ID="lblIDBimedia" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                                <div class="cell" style="text-align:right">
                                    <div style="display:inline-block;text-align:left">
                                        <div class="label">
                                            ID SAB
                                        </div>
                                        <div>
                                            <asp:Label ID="lblIDPDVSAB" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>--%>

                        <div style="margin-bottom:10px;">
                            <asp:Button ID="btnSendContractToTobacco" runat="server" CssClass="button" Text="Envoyer le contrat au buraliste" OnClick="btnSendContractToTobacco_Click" OnClientClick="sendContract();" />
                            <asp:Button ID="btnSendContractToMe" runat="server" CssClass="button" Text="M'envoyer le contrat" OnClick="btnSendContractToMe_Click" OnClientClick="sendContract();" />
                        </div>

                        <div style="float:left;width:49%">
                            <div class="label">
                                <asp:Label ID="lblRaisonSociale" runat="server" AssociatedControlID="txtRaisonSociale">Raison sociale</asp:Label>
                            </div>
                            <div>
                                <asp:TextBox ID="txtRaisonSociale" runat="server" style="width:100%"></asp:TextBox>
                            </div>
                        </div>
                        <div style="float:right;width:49%">
                            <div class="label">
                                <asp:Label ID="lblNomCommercial" runat="server" AssociatedControlID="txtNomCommercial">Nom commercial</asp:Label>
                            </div>
                            <div>
                                <asp:TextBox ID="txtNomCommercial" runat="server" style="width:100%"></asp:TextBox>
                            </div>
                        </div>
                        <div style="clear:both"></div>

                        <div class="label">
                            <asp:Label ID="lblSiret" runat="server" AssociatedControlID="txtSiret">SIRET</asp:Label>
                        </div>
                        <div>
                            <asp:TextBox ID="txtSiret" runat="server" style="width:49%"></asp:TextBox>
                        </div>

                        <div style="color:#344b56;font-weight:bold;text-transform:uppercase;border-bottom:1px solid #344b56;margin-top:30px">
                            Gérant(e)
                        </div>
                        <div style="float:left;width:49%">
                            <div class="label">
                                <asp:Label ID="lblManagerFirstName" runat="server" AssociatedControlID="txtManagerFirstName">Prénom</asp:Label>
                            </div>
                            <div>
                                <asp:TextBox ID="txtManagerFirstName" runat="server" style="width:100%"></asp:TextBox>
                            </div>
                        </div>
                        <div style="float:right;width:49%">
                            <div class="label">
                                <asp:Label ID="lblManagerLastName" runat="server" AssociatedControlID="txtManagerLastName">Nom</asp:Label>
                            </div>
                            <div>
                                <asp:TextBox ID="txtManagerLastName" runat="server" style="width:100%"></asp:TextBox>
                            </div>
                        </div>
                        <div style="clear:both"></div>

                        <div style="color:#344b56;font-weight:bold;text-transform:uppercase;border-bottom:1px solid #344b56;margin-top:30px">
                            Contact
                        </div>
                        <div style="float:left;width:49%">
                            <div class="label">
                                <asp:Label ID="lblMobileNumber" runat="server" AssociatedControlID="txtMobilePhone">N° de mobile</asp:Label>
                            </div>
                            <div>
                                <asp:TextBox ID="txtMobilePhone" runat="server" style="width:100%"></asp:TextBox>
                            </div>
                        </div>
                        <div style="float:right;width:49%">
                            <div class="label">
                                <asp:Label ID="lblEmail" runat="server" AssociatedControlID="txtEmail">Email</asp:Label>
                            </div>
                            <div>
                                <asp:TextBox ID="txtEmail" runat="server" style="width:100%"></asp:TextBox>
                            </div>
                        </div>
                        <div style="clear:both"></div>
                        <div style="float:left;width:49%">
                            <div class="label">
                                <asp:Label ID="lblPhoneNumber" runat="server" AssociatedControlID="txtPhoneNumber">N° fixe</asp:Label>
                            </div>
                            <div>
                                <asp:TextBox ID="txtPhoneNumber" runat="server" style="width:100%"></asp:TextBox>
                            </div>
                        </div>
                        <div style="clear:both"></div>
                        <div style="margin-top:20px; text-align:center">
                            <input type="button" value="Enregistrer" class="buttonHigher" onclick="SaveAgencyInformation();" />
                            <asp:Button ID="btnSaveAgencyInformation" runat="server" style="display:none" OnClick="btnSaveAgencyInformation_Click" />
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSendContractToTobacco" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="btnSendContractToMe" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="btnSendContractToMe" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="btnSaveAgencyInformation" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </asp:Panel>

        <asp:Panel ID="panelAgencyAddress" runat="server">
            <div id="divAgencyAddress" style="padding:10px;margin-bottom:10px">
                <asp:UpdatePanel ID="upAgencyAddress" runat="server">
                    <ContentTemplate>

                        <div>
                            <div class="label">
                                <asp:Label ID="lblAddress" runat="server" AssociatedControlID="txtAddress">Adresse</asp:Label>
                            </div>
                            <div>
                                <asp:TextBox ID="txtAddress" runat="server" style="width:100%"></asp:TextBox>
                            </div>
                        </div>
                        <div style="float:left;width:49%">
                            <div class="label">
                                <asp:Label ID="lblZip" runat="server" AssociatedControlID="txtZip">Code postal</asp:Label>
                            </div>
                            <div>
                                <asp:TextBox ID="txtZip" runat="server" style="width:100%"></asp:TextBox>
                            </div>
                        </div>
                        <div style="float:right;width:49%">
                            <div class="label">
                                <asp:Label ID="lblCity" runat="server" AssociatedControlID="txtCity">Ville</asp:Label>
                            </div>
                            <div>
                                <asp:TextBox ID="txtCity" runat="server" style="width:100%"></asp:TextBox>
                            </div>
                        </div>
                        <div style="clear:both"></div>
                        <div style="margin-top:10px; text-align:center">
                            <input type="button" value="Enregistrer" class="buttonHigher" onclick="SaveAgencyAddress();" />
                            <asp:Button ID="btnSaveAgencyAddress" runat="server" style="display:none" OnClick="btnSaveAgencyAddress_Click" />
                        </div>

                        <asp:HiddenField ID="hdnLatitude" runat="server" />
                        <asp:HiddenField ID="hdnLongitude" runat="server" />
                        <div id="map-canvas" style="height:350px;margin-top:20px;">
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </asp:Panel>

        <asp:Panel ID="panelAgencyEquipment" runat="server">
            <div id="divAgencyEquipment" style="padding:10px;margin-bottom:10px">

                <div id="mpad-details-popup" style="display:none">
                    <div id="divMpadDetails">
                        <div class="table" style="width:100%;border-collapse:collapse">
                            <div class="row">
                                <div class="cell" style="vertical-align: middle;">
                                    <img src="mobile/Styles/Img/Mpad.png" style="max-height: 70px; max-width: 70px;" />
                                </div>
                                <div class="cell" style="padding-left: 10px; vertical-align: middle">
                                    <div id="divMpadSerial">
                                        S/N :
                                        <span id="sMpadSerial" class="font-orange" style="font-weight: bold"></span>
                                    </div>
                                    <div id="divMpadStatus">
                                        Statut :
                                        <span id="sMpadStatus" style="font-weight: bold"></span>
                                    </div>
                                    <div id="divMpadInitStatus" style="display: none"></div>
                                </div>
                            </div>
                        </div>
                        <div id="divMpadRelease" style="margin-top: 5px;">
                            Logiciel principal :
                            <span id="sMpadRelease" class="font-orange" style="font-weight: bold"></span>
                        </div>
                        <div id="divMpadReleaseABM">
                            Logiciel associé :
                            <span id="sMpadReleaseABM" class="font-orange" style="font-weight: bold"></span>
                        </div>
                        <div id="divMpadCode" style="margin-top: 10px;">
                            <div>
                                Code initialisation :
                                <span id="sMpadCode" class="font-orange" style="font-weight: bold"></span>
                            </div>
                            <span id="sMpadCodeDate"></span>
                        </div>
                        <div id="divMpadLastCnxDate" style="margin-top: 5px;">
                            <div>Dernière connexion :</div>
                            <span id="sMpadLastCnxDate" class="font-orange" style="font-weight: bold"></span>
                        </div>
                        <div id="divMpadRemoteAccessRequested" style="margin-top: 5px; display: none">
                            <div>Remote access demandé :</div>
                            <span id="sMpadRemoteAccessRequested" class="font-orange" style="font-weight: bold"></span>
                        </div>
                    </div>
                </div>
                <div id="pos-details-popup" style="display:none">
                    <div id="divPosDetails">
                        <div class="table">
                            <div class="row">
                                <div class="cell" style="vertical-align:middle;">
                                    <img id="imgPosDetails" src="mobile/Styles/Img/Ingenico.png" alt="" style="max-height:70px; max-width:70px;" />
                                </div>
                                <div class="cell" style="padding-left:10px; vertical-align: top">
                                    <div id="divPosType">
                                        Marque :
                                        <span id="sPosType" class="font-orange" style="font-weight:bold; text-transform:uppercase"></span>
                                    </div>
                                    <div id="divPosSerial">
                                        S/N :
                                        <span id="sPosSerial" class="font-orange" style="font-weight:bold"></span>
                                    </div>
                                    <div id="divPosRelease">
                                        Version logiciel :
                                        <span id="sPosRelease" class="font-orange" style="font-weight:bold"></span>
                                    </div>
                                    <!--<div id="divPosStatus">
                                        Statut :
                                        <span id="sPosStatus" class="font-orange" style="font-weight:bold"></span>
                                    </div>-->
                                </div>
                            </div>
                        </div>
                        <div id="divPostLastCnxDate" style="margin-top:5px;">
                            <div>Dernière connexion :</div>
                            <span id="sPosLastCnxDate" class="font-orange" style="font-weight:bold"></span>
                        </div>
                    </div>
                </div>

                <div id="mpad-maintenance-popup" style="display:none">
                    <div id="mpad-maintenance-popup-content">
                        <asp:UpdatePanel ID="upMaintenance" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div style="color:#344b56;font-weight:bold;text-transform:uppercase;border-bottom:1px solid #344b56;margin-bottom:10px">
                                    Prise de contrôle à distance
                                </div>

                                <asp:HiddenField ID="hdnRefMPAD" runat="server" />

                                <asp:Panel ID="panelLocalAccess" runat="server">
                                    <div class="label">
                                        Accès via réseau local
                                        <img class="action-mpad-help" title="Vous êtes actuellement connecté au même réseau local que la borne."
                                            src="Styles/Img/help.png" alt="?" style="width:20px;position:relative;top:2px" />
                                    </div>
                                    <div>
                                        <asp:HyperLink ID="lnkLocalEth" runat="server" Target="_blank" style="color:#000;white-space:nowrap"></asp:HyperLink>
                                        <asp:HyperLink ID="lnkLocalWifi" runat="server" Target="_blank" style="color:#000;white-space:nowrap"></asp:HyperLink>
                                    </div>
                                </asp:Panel>

                                <div style="margin-top:20px">
                                    <div class="label">
                                        Accès via internet
                                        <img class="action-mpad-help" title="Vous n'êtes actuellement pas connecté au même réseau local que la borne."
                                            src="Styles/Img/help.png" alt="?" style="width:20px;position:relative;top:2px" />
                                    </div>
                                    <div>
                                        <div style="float:left">
                                            Demander une connexion sécurisée
                                        </div>
                                        <div style="float:right;position:relative;top:-5px">
                                            <asp:RadioButtonList ID="rblRemoteAccess" runat="server" CssClass="RadioButtonList" AutoPostBack="true" OnSelectedIndexChanged="rblRemoteAccess_SelectedIndexChanged" RepeatDirection="Horizontal" style="float:right"><asp:ListItem Value="1" Text="Oui"></asp:ListItem><asp:ListItem Value="0" Text="Non"></asp:ListItem></asp:RadioButtonList>
                                        </div>
                                        <div style="clear:both"></div>
                                    </div>
                                    <div>
                                        <asp:Panel ID="panelAccessRequestSent" runat="server" Visible="false">
                                            <asp:ImageButton ID="btnRefresh" runat="server" ImageUrl="~/Styles/Img/refresh.png" OnClick="btnMaintenance_Click" CssClass="action-mpad-help" title="Rafraîchir" Height="20px" style="position:relative;top:2px" />
                                            &nbsp;
                                            Attente de connexion sécurisée...
                                        </asp:Panel>

                                        <asp:HyperLink ID="lnkAccessVPN" runat="server" Target="_blank" style="color:#000"></asp:HyperLink>
                                    </div>
                                </div>

                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <div id="mpad-maintenance-popup-confirm" style="display:none">
                    <div id="mpad-maintenance-popup-confirm-content">
                        <asp:UpdatePanel ID="upMaintenanceConfirm" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <span class="ui-helper-hidden-accessible"><input type="text"/></span>
                                <span class="font-orange">Attention :</span> Toute connexion active via Internet risque d'être coupée.<br />
                                Voulez-vous vraiment fermer ?
                                <asp:Button ID="btnForceCloseMaintenance" runat="server" OnClick="btnForceCloseMaintenance_Click" style="display:none" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <asp:UpdatePanel ID="upAgencyEquipment" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div style="width:49%;float:left">
                            <div class="label">
                                Borne(s)
                            </div>
                            <asp:Repeater ID="rptMPAD" runat="server">
                                <HeaderTemplate>
                                    <table id="table-mpad" class="defaultTable" style="width:100%;border-collapse:collapse">
                                        <tr>
                                            <th></th>
                                            <th>N&deg; de série</th>
                                            <th>Statut</th>
                                            <th style="width:1px"></th>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                        <tr style="cursor:pointer">
                                            <td style="vertical-align:middle;width:1px" onclick="showMpadDetail('<%# Eval("MP_Serial") %>','<%# Eval("A0_LCK") %>', '<%# Eval("MP_MainSoftRelease") %>', '<%# Eval("MP_ABMSoftRelease") %>', '<%# Agency.getMpadStatus(Eval("MP_sInitStatus").ToString(),Eval("MP_sInitStatusColor").ToString()).Replace("\"", "&quot;") %>','<%# Eval("MP_Token") %>', '<%#Eval("MP_TokenCreationDate")%>', '<%# Eval("MP_LastCnxDate") %>', '<%# Agency.getRemoteAccessStatus(Eval("A2_RA").ToString()) %>');">
                                                <img src="mobile/Styles/Img/Mpad.png" style="height:30px" />
                                            </td>
                                            <td style="vertical-align:middle; padding:5px;text-align:center;" onclick="showMpadDetail('<%# Eval("MP_Serial") %>','<%# Eval("A0_LCK") %>', '<%# Eval("MP_MainSoftRelease") %>', '<%# Eval("MP_ABMSoftRelease") %>','<%# Agency.getMpadStatus(Eval("MP_sInitStatus").ToString(),Eval("MP_sInitStatusColor").ToString()).Replace("\"", "&quot;") %>','<%# Eval("MP_Token") %>', '<%#Eval("MP_TokenCreationDate")%>', '<%# Eval("MP_LastCnxDate") %>', '<%# Agency.getRemoteAccessStatus(Eval("A2_RA").ToString()) %>');">
                                                <%# Eval("MP_Serial") %>
                                            </td>
                                            <td style="vertical-align:middle; padding:5px;text-align:center;width:100px" onclick="showMpadDetail('<%# Eval("MP_Serial") %>','<%# Eval("A0_LCK") %>', '<%# Eval("MP_MainSoftRelease") %>', '<%# Eval("MP_ABMSoftRelease") %>','<%# Agency.getMpadStatus(Eval("MP_sInitStatus").ToString(),Eval("MP_sInitStatusColor").ToString()).Replace("\"", "&quot;") %>','<%# Eval("MP_Token") %>', '<%#Eval("MP_TokenCreationDate")%>', '<%# Eval("MP_LastCnxDate") %>', '<%# Agency.getRemoteAccessStatus(Eval("A2_RA").ToString()) %>');">
                                                <%# Agency.getMpadStatus(Eval("MP_sInitStatus").ToString(),Eval("MP_sInitStatusColor").ToString()) %>
                                            </td>
                                            <td onclick="" style="width:1px;text-align:center;vertical-align:middle">
                                                <asp:ImageButton ID="btnMaintenance" runat="server" Visible="<%# bInfosActionAllowed %>" ImageUrl="~/Styles/Img/maintenance.png" CommandArgument='<%#Eval("MP_Ref").ToString()%>' OnClick="btnMaintenance_Click" Height="25px" />
                                            </td>
                                        </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                            <asp:Panel ID="panelMPADListEmpty" runat="server" Visible ="false">
                                Aucune borne affectée.
                            </asp:Panel>
                        </div>
                        <div style="width:49%;float:right">
                            <div class="label">
                                TPE
                            </div>
                            <asp:Repeater ID="rptPOS" runat="server">
                                <HeaderTemplate>
                                    <table class="defaultTable" style="width:100%;border-collapse:collapse">
                                        <tr>
                                            <th></th>
                                            <th>N&deg; de série</th>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                        <tr style="cursor:pointer" onclick="showPosDetail('<%# Eval("PS_Serial") %>','<%# Eval("PS_IsLocked") %>','<%# Eval("PS_SoftRelease") %>','<%# Eval("PS_LastCnxDate") %>','<%# Agency.getPosStatus(Eval("PS_IsLocked").ToString()).Replace("\"", "&quot;") %>','<%# Agency.getPosImageName(Eval("PS_Serial").ToString())%>');">
                                            <td style="vertical-align:middle;width:1px">
                                                <img src="mobile/Styles/Img/Ingenico.png" style="height:30px" />
                                            </td>
                                            <td style="vertical-align:middle; padding:5px;text-align:center;">
                                                <%# Eval("PS_Serial") %>
                                            </td>
                                        </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                            <asp:Panel ID="panelPosListEmpty" runat="server" Visible ="false">
                                Aucun TPE affecté.
                            </asp:Panel>
                        </div>
                        <div style="clear:both"></div>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </asp:Panel>

        <asp:Panel ID="panelAgencyActivity" runat="server">
            <div id="divAgencyActivity" style="padding:10px;margin-bottom:10px">
                <div class="label">
                    Détail par période
                </div>
                <asp:RadioButtonList ID="rblPeriod" runat="server" OnSelectedIndexChanged="rblPeriod_SelectedIndexChanged" AutoPostBack="true" RepeatDirection="Horizontal" CssClass="RadioButtonList" style="font-size:0.8em;text-transform:none">
                    <asp:ListItem Text="Aujourd'hui" Value="5"></asp:ListItem>
                    <asp:ListItem Text="Hier" Value="1"></asp:ListItem>
                    <asp:ListItem Text="7 derniers jours" Value="6"></asp:ListItem>
                    <asp:ListItem Text="30 derniers jours" Value="7"></asp:ListItem>
                    <asp:ListItem Text="Semaine dernière" Value="2"></asp:ListItem>
                    <asp:ListItem Text="Mois dernier" Value="3"></asp:ListItem>
                </asp:RadioButtonList>

                <asp:UpdatePanel ID="upAgencyActivity" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>

                        <asp:Panel ID="panelActivityDetails" runat="server" Visible="false">
                            <table id="activity-table" class="defaultTable" style="width:100%;border-collapse:collapse;margin-top:20px">
                                <tr>
                                    <th></th>
                                    <th>Quantité</th>
                                    <th>Montant cumulé</th>
                                </tr>
                                <tr>
                                    <td>
                                        Ventes coffrets
                                    </td>
                                    <td>
                                        <asp:Label ID="lblPackQty" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblPackTotalAmnt" runat="server"></asp:Label> &euro;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Activations cartes nickel
                                    </td>
                                    <td>
                                        <asp:Label ID="lblActivationQty" runat="server"></asp:Label>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>
                                        Dépôts cash à l'ouverture
                                    </td>
                                    <td>
                                        <asp:Label ID="lblCashDepositOpenQty" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblCashDepositOpenTotalAmnt" runat="server"></asp:Label> &euro;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Dépôts cash
                                    </td>
                                    <td>
                                        <asp:Label ID="lblCashDepositQty" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblCashDepositTotalAmnt" runat="server"></asp:Label> &euro;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Retraits cash
                                    </td>
                                    <td>
                                        <asp:Label ID="lblCashWithdrawQty" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblCashWithdrawTotalAmnt" runat="server"></asp:Label> &euro;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Paiements cartes nickel
                                    </td>
                                    <td>
                                        <asp:Label ID="lblPaymentOnUsQty" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblPaymentOnUsTotalAmnt" runat="server"></asp:Label> &euro;
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>

                        <asp:Button ID="btnLoadFirstActivity" runat="server" OnClick="btnLoadFirstActivity_Click" style="display:none" />

                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="rblPeriod" EventName="SelectedIndexChanged" />
                        <asp:AsyncPostBackTrigger ControlID="btnLoadFirstActivity" EventName="click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </asp:Panel>

        <asp:Panel ID="panelAgencyDashboard" runat="server">
            <div id="divAgencyDashboard" style="padding:10px;margin-bottom:10px;font-size:0.8em">
                <asp:UpdatePanel ID="upAgencyDashboard" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Panel ID="panelStatsPRO" runat="server" style="display:none">
                            <asp:StatsPRO ID="StatsPRO1" runat="server" />
                        </asp:Panel>
                        <asp:Button ID="btnLoadDashboard" runat="server" OnClick="btnLoadDashboard_Click" style="display:none" />
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnLoadDashboard" EventName="click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </asp:Panel>

        <asp:Panel ID="panelAgencyAlerts" runat="server">
            <div style="font-family:Arial;font-size:0.85em;text-transform:none;margin-top:20px">
                <asp:KYCAlert ID="kycAlert" runat="server" />
            </div>
        </asp:Panel>

        <asp:Panel ID="panelAgencySettings" runat="server">
            <div style="font-family:Arial;font-size:0.85em;text-transform:none;margin-top:20px">
                <asp:UpdatePanel ID="upAgencySettings" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Panel ID="panelReadSettings" runat="server">
                            <div class="label">
                                <label for="<%= lblSmsAlertAmount.ClientID %>">Montant seuil alerte SMS</label>
                            </div>
                            <div>
                                <asp:Label ID="lblSmsAlertAmount" runat="server"></asp:Label>
                            </div>
                            <div style="margin-top:20px;width:100%; text-align:center">
                                <asp:Button ID="btnEditAgencySettings" runat="server" OnClick="btnEditAgencySettings_Click" Text="Modifier" CssClass="button" />
                            </div>
                        </asp:Panel>
                        <asp:Panel ID="panelEditSettings" runat="server" Visible="false">
                            <div class="label">
                                <label for="<%= txtSmsAlertAmount.ClientID %>">Montant seuil alerte SMS</label>
                            </div>
                            <div>
                                <asp:TextBox ID="txtSmsAlertAmount" runat="server"></asp:TextBox> &euro;
                            </div>
                            <div style="margin-top:5px;">
                                <asp:Label ID="lblSmsAlertAmountError" runat="server" ForeColor="Red"></asp:Label>
                            </div>
                            <div style="margin-top:20px;width:100%; text-align:center">
                                <asp:Button ID="btnCancelEditAgencySettings" runat="server" OnClick="btnCancelEditAgencySettings_Click" Text="Annuler" CssClass="button" />
                                <asp:Button  ID="btnSaveAgencySettings" runat="server" OnClick="btnSaveAgencySettings_Click" Text="Enregistrer" CssClass="button"/>
                            </div>
                        </asp:Panel>
                        <asp:Button ID="btnLoadAgencySettings" runat="server" OnClick="btnLoadAgencySettings_Click" style="display:none" />
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnLoadAgencySettings" EventName="click" />
                        <asp:AsyncPostBackTrigger ControlID="btnEditAgencySettings" EventName="click" />
                        <asp:AsyncPostBackTrigger ControlID="btnCancelEditAgencySettings" EventName="click" />
                        <asp:AsyncPostBackTrigger ControlID="btnSaveAgencySettings" EventName="click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </asp:Panel>

        <asp:Panel ID="panelFormation" runat="server">
            <div style="font-size:0.8em">
                <asp:StaffManagement ID="staffManagement" runat="server" />
            </div>
        </asp:Panel>

        <asp:Panel ID="panelRisks" runat="server">
            <div>
                <asp:Label ID="lblScoreLastChangeDate" runat="server" CssClass="label">
                    Date des calculs
                </asp:Label>
            </div>
            <asp:Literal ID="litScoreLastChangeDate" runat="server"></asp:Literal>
            <asp:Repeater ID="rptAgentScore" runat="server">
                <ItemTemplate>
                    <div style="margin-top:15px">
                        <div>
                            <asp:Label ID="lblScoreType" runat="server" CssClass="label" style="margin-right:5px">
                                <%#Eval("ScoreType") %>
                            </asp:Label>
                            <b><%#Eval("ScoreValue") %> / 5</b>
                        </div>
                    </div>
                    <div class="scoring-details">
                        <div class="scoring-details-header">
                            <div>
                                Critère
                            </div>
                            <div>
                                Valeur
                            </div>
                        </div>
                        <div class="scoring-details-inner">
                            <asp:Repeater ID="rptScoreDetails" runat="server" DataSource='<%#Eval("ScoreDetails") %>'>
                                <ItemTemplate>
                                    <div class="scoring-details-item">
                                        <div>
                                            <%#Eval("Label") %>
                                        </div>
                                        <div>
                                            <%#Eval("Value") %>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </asp:Panel>

    </div>

    <div id="dialog-message" style="display:none">
        <asp:Label ID="lblInfoMessage" runat="server"></asp:Label>
    </div>

    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            pbControl = args.get_postBackElement();
            //console.log(pbControl.id);

            try {
                if (pbControl != null) {
                    var panel = '';
                    if (pbControl.id == '<%=btnLoadFirstActivity.ClientID%>' || pbControl.id.indexOf('<%=rblPeriod.ClientID %>') != -1)
                        panel = '#<%=panelAgencyActivity.ClientID %>';
                    else if (pbControl.id == '<%=btnLoadDashboard.ClientID%>')
                        panel = '#<%=panelAgencyDashboard.ClientID %>';
                    else if (pbControl.id.indexOf('btnMaintenance') != -1)
                        panel = '#table-mpad';
                    else if (pbControl.id.indexOf('rblRemoteAccess') != -1 || pbControl.id.indexOf('btnRefresh') != -1)
                        panel = '#mpad-maintenance-popup-content';
                    else if (pbControl.id == '<%=btnForceCloseMaintenance.ClientID%>')
                        panel = '#mpad-maintenance-popup-confirm-content';
                    else if (pbControl.id == '<%=btnLoadAgencySettings.ClientID%>')
                        panel = '#<%=panelAgencySettings.ClientID%>';
                    else if (pbControl.id == '<%=btnSendContractToMe.ClientID%>' || pbControl.id == '<%=btnSendContractToTobacco.ClientID%>')
                        panel = '#<%=panelAgencyGeneralInfo.ClientID%>';
                }
                //console.log(panel);
                if (panel.length > 0) {
                    var width = $(panel).outerWidth();
                    var height = $(panel).outerHeight();
                    $('#ar-loading').css('width', width);
                    $('#ar-loading').css('height', height);

                    $('#ar-loading').show();

                    $('#ar-loading').position({ my: "left top", at: "left top", of: panel });
                }
            }
            catch(ex){
                console.log(ex.toString());
            }

        }
        function EndRequestHandler(sender, args) {
            $('#ar-loading').hide();
        }
    </script>


</asp:Content>

