﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using XMLMethodLibrary;

public partial class SearchAlert : System.Web.UI.Page
{
    private DataTable _dtGroupDefinition
    {
        get { return (Session["dtGroupDefinition"] != null) ? (DataTable)Session["dtGroupDefinition"] : null; }
        set
        {
            DataTable dt = new DataTable("dtGroupDefinition");
            dt = value;
            Session["dtGroupDefinition"] = dt;
        }
    }
    private string _sSortColumn { get { return (ViewState["AlertSortColumn"] != null ? ViewState["AlertSortColumn"].ToString() : null); } set { ViewState["AlertSortColumn"] = value; } }
    private string _sSortDirection { get { return (ViewState["AlertSortDirection"] != null ? ViewState["AlertSortDirection"].ToString() : "DESC"); } set { ViewState["AlertSortDirection"] = value; } }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            CheckRights();

            getParam();
            if (rblGroupType.SelectedValue == "0")
                bindAlertGrid();

            authentication auth = authentication.GetCurrent();
            hdnCurrentRefAgent.Value = auth.sRef;
            amlComment.BindCommentLengthCheck();
        }

        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "init();", true);
    }

    protected void CheckRights()
    {
        bool bRedirect = true;
        string sXmlOut = tools.GetRights(authentication.GetCurrent().sToken, "SC_AlertSearch");

        List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Page", "RC");

        if (listRC.Count > 0 && listRC[0] == "0")
        {
            List<string> listAction = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/Page/Action");

            for (int i = 0; i < listAction.Count; i++)
            {
                List<string> listTag = CommonMethod.GetAttributeValues(listAction[i], "Action", "TagAction");
                List<string> listActionAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ActionAllowed");
                List<string> listViewAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ViewAllowed");

                if (listTag.Count > 0)
                {
                    switch (listTag[0])
                    {
                        case "SearchAlert":
                            if (listViewAllowed.Count > 0 && listViewAllowed[0] == "1")
                            {
                                bRedirect = false;
                            }
                            break;

                        case "ShowAlertWithoutTakingCharge":
                            if (listViewAllowed.Count > 0 && listViewAllowed[0] == "1" && listActionAllowed.Count > 0 && listActionAllowed[0] == "1")
                                btnShowAlertWithoutCharge.Visible = true;
                            break;
                    }
                }
            }
        }

        if (bRedirect)
            Response.Redirect("Default.aspx");
    }

    protected void saveParam()
    {
        DataTable dtParam = new DataTable();
        dtParam.TableName = "dtParam";
        dtParam.Columns.Add("Name");
        dtParam.Columns.Add("Value");
        dtParam.Rows.Add(new object[] { "groupType", rblGroupType.SelectedValue });
        dtParam.Rows.Add(new object[] { "groupTagAlert", ddlGroupTagAlert.SelectedValue });
        dtParam.Rows.Add(new object[] { "tagAlert", ddlTagAlert.SelectedValue });
        dtParam.Rows.Add(new object[] { "alertType", ddlAlertType.SelectedValue });
        dtParam.Rows.Add(new object[] { "nbResult", ddlNbResult.SelectedValue });
        dtParam.Rows.Add(new object[] { "nbPages", (lblTotalPage.Text.Trim().Length > 0) ? lblTotalPage.Text.Replace("/","").Trim() : "1" });
        dtParam.Rows.Add(new object[] { "pageIndex", ddlPage.SelectedValue });
        dtParam.Rows.Add(new object[] { "profile", ddlProfile.SelectedValue });

        Session["SearchAlertParam"] = dtParam;
    }
    protected void getParam()
    {
        ddlProfile.DataSource = AML.getProfiles();
        ddlProfile.DataBind();

        ddlAlertType.Items.Add(new ListItem("TOUS", ""));
        ddlAlertType.DataSource = AML.getAlertTypeList();
        ddlAlertType.DataBind();
        ddlAlertType.Items[0].Selected = true;

        ddlTagAlert.Items.Add(new ListItem("TOUS", ""));
        ddlTagAlert.DataSource = AML.getAlertTagList();
        ddlTagAlert.DataBind();
        ddlTagAlert.Items[0].Selected = true;

        //ddlGroupTagAlert.Items.Add(new ListItem("TOUS", ""));
        _dtGroupDefinition = AML.getGroupAlertTypeList();
        ddlGroupTagAlert.DataSource = _dtGroupDefinition;
        ddlGroupTagAlert.DataBind();
        ddlGroupTagAlert.Items[0].Selected = true;

        if (Session["SearchAlertParam"] != null)
        {
            try
            {
                DataTable dtParam = new DataTable();
                dtParam.TableName = "dtParam";
                dtParam = (DataTable)Session["SearchAlertParam"];
                string sParamName = "", sParamValue = "";

                for (int i = 0; i < dtParam.Rows.Count; i++)
                {

                    sParamValue = dtParam.Rows[i]["Value"].ToString().Trim();
                    sParamName = dtParam.Rows[i]["Name"].ToString().Trim();

                    if (sParamName.Length > 0 && sParamValue.Length > 0)
                    {
                        switch (sParamName)
                        {
                            case "groupType":
                                if (rblGroupType.Items.FindByValue(sParamValue) != null)
                                    rblGroupType.SelectedValue = sParamValue;
                                break;
                            case "groupTagAlert":
                                if (ddlGroupTagAlert.Items.FindByValue(sParamValue) != null)
                                    ddlGroupTagAlert.SelectedValue = sParamValue;
                                break;
                            case "tagAlert":
                                if (ddlTagAlert.Items.FindByValue(sParamValue) != null)
                                    ddlTagAlert.SelectedValue = sParamValue;
                                break;
                            case "alertType":
                                if (ddlAlertType.Items.FindByValue(sParamValue) != null)
                                    ddlAlertType.SelectedValue = sParamValue;
                                break;
                            case "nbResult":
                                if (ddlNbResult.Items.FindByValue(sParamValue) != null)
                                    ddlNbResult.SelectedValue = sParamValue;
                                break;
                            case "nbPages":
                                int iNbPages = 1;
                                int.TryParse(sParamValue, out iNbPages);
                                ddlPage.DataSource = getPages(iNbPages);
                                ddlPage.DataBind();
                                break;
                            case "pageIndex":
                                if (ddlPage.Items.FindByValue(sParamValue) != null)
                                    ddlPage.SelectedValue = sParamValue;
                                break;
                            case "profile":
                                if (ddlProfile.Items.FindByValue(sParamValue) != null)
                                    ddlProfile.SelectedValue = sParamValue;
                                break;
                        }
                    }
                }

                rblGroupType_SelectedIndexChanged(new object(), new EventArgs());
            }
            catch (Exception ex)
            {

            }
        }
        else
        {
            ddlPage.DataSource = getPages(1);
            ddlPage.DataBind();
            if (ddlAlertType.Items.Count > 0)
                ddlAlertType.Items[0].Selected = true;
            if (ddlTagAlert.Items.Count > 0)
                ddlTagAlert.Items[0].Selected = true;
        }
    }

    protected void bindAlertGrid()
    {
        btnBackToGroup.Visible = false;

        authentication auth = authentication.GetCurrent();
        string sXmlOut = "";
        XElement xml = new XElement("Alert",
                            new XAttribute("RefAlertStatus", "1"),
                            new XAttribute("CashierToken", auth.sToken),
                            new XAttribute("Version", "2"),
                            new XAttribute("PageSize", ddlNbResult.SelectedValue),
                            new XAttribute("PageIndex", ddlPage.SelectedValue),
                            new XAttribute("TAGAlert", ddlTagAlert.SelectedValue),
                            new XAttribute("AlertCategory", ddlAlertType.SelectedValue),
                            new XAttribute("Culture", ((Session["CultureID"] != null) ? Session["CultureID"].ToString() : "fr-FR")));

        if (!String.IsNullOrWhiteSpace(_sSortColumn))
        {
            xml.Add(new XAttribute("OrderBy", _sSortColumn),
                    new XAttribute("OrderAsc", _sSortDirection));
        }
        else
        {
            if (ddlTagAlert.SelectedValue.ToLower() != "requisition")
            {
                xml.Add(new XAttribute("OrderBy", "refAlertStatus ASC,RefAlert"));
            }
        }

        if (rblGroupType.SelectedValue == "1" && !String.IsNullOrEmpty(hdnRefAlertGroupSelected.Value))
        {
            xml.Add(new XAttribute("refAlertGroup", hdnRefAlertGroupSelected.Value));
            btnBackToGroup.Visible = true;
        }

        string sXmlIn = new XElement("ALL_XML_IN", xml).ToString();
        gvClientAlertList.DataSource = AML.getClientAlertList(sXmlIn, out sXmlOut);
        List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/RESULT", "RC");
        List<string> listErrorLabel = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/RESULT", "ErrorLabel");
        List<string> listRowCount = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Alert", "NbResult");
        if (listRC.Count == 0 && listRowCount != null && listRowCount.Count > 0)
        {
            panelGrid.Visible = true;
            panelError.Visible = false;
            lblNbOfResults.Text = listRowCount[0].ToString();
            PaginationManagement(lblNbOfResults.Text, sXmlOut);
            //getDynamicTitleColumns(sXmlOut);
            getAmlDynamicColumns(sXmlOut);
        }
        else
        {
            if (listErrorLabel != null && listErrorLabel.Count > 0)
            {
                panelGrid.Visible = false;
                panelError.Visible = true;
                lblError.Text = listErrorLabel[0].ToString().ToUpper();
            }
        }
        try
        {
            gvClientAlertList.DataBind();
            if (gvClientAlertList.Rows.Count > 0)
            {
                panelGrid.Visible = true;
                divGridSettings.Visible = true;

            }
        }
        catch (Exception ex)
        {
        }

        upSearch.Update();

        saveParam();
    }

    protected void gvClientAlertList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HiddenField hdnRefCustomer = (HiddenField)e.Row.FindControl("hdnRefCustomer");
            HiddenField hdnRefAlert = (HiddenField)e.Row.FindControl("hdnRefAlert");
            if (hdnRefCustomer != null)
            {
                string sRowStyle = "background-color:#fff";
                HiddenField hdnRowStyle = (HiddenField)e.Row.FindControl("hdnRowStyle");
                if (hdnRowStyle != null && !String.IsNullOrWhiteSpace(hdnRowStyle.Value))
                {
                    sRowStyle = hdnRowStyle.Value;
                    e.Row.Attributes["style"] = sRowStyle;
                }

                e.Row.Attributes.Add("onclick", "AlertFastAnalysis('" + hdnRefAlert.Value + "', '"+ hdnRefCustomer.Value + "','en attente', true)");
                e.Row.Attributes.Add("onmouseover", "this.style.cursor = 'pointer';this.style.backgroundColor = '#ff9966'");
                e.Row.Attributes.Add("onmouseout", "this.style.cursor = 'auto';this.style = '" + sRowStyle + "'");

                HiddenField hdnRowCssClass = (HiddenField)e.Row.FindControl("hdnRowCssClass");
                if (hdnRowCssClass != null && !String.IsNullOrWhiteSpace(hdnRowCssClass.Value))
                {
                    e.Row.CssClass += hdnRowCssClass.Value;
                }
            }
        }
    }

    protected void getAmlDynamicColumns(string sXML)
    {
        ViewState["AmlDynamicColumns"] = null;

        DataTable dt = new DataTable();
        dt.TableName = "dtAmlDynamicColumns";
        dt.Columns.Add("TAG");
        dt.Columns.Add("INT1");
        dt.Columns.Add("INT2");
        dt.Columns.Add("VARCHAR1");
        dt.Columns.Add("MONEY1");

        string sINT1="", sINT2="", sMONEY1 ="", sVARCHAR1 ="", sTAG =  "";
        List<string> listAlertCriteriaDescription = XMLMethodLibrary.CommonMethod.GetTags(sXML, "ALL_XML_OUT/Alert/CriteriaDescription");
        for (int i = 0; i < listAlertCriteriaDescription.Count; i++)
        {
            sTAG = tools.GetValueFromXml(listAlertCriteriaDescription[i], "CriteriaDescription", "CategoryTAG");
            sINT1 = tools.GetValueFromXml(listAlertCriteriaDescription[i], "CriteriaDescription", "Critere_INT1");
            sINT2 = tools.GetValueFromXml(listAlertCriteriaDescription[i], "CriteriaDescription", "Critere_INT2");
            sVARCHAR1 = tools.GetValueFromXml(listAlertCriteriaDescription[i], "CriteriaDescription", "Critere_VARCHAR1");
            sMONEY1 = tools.GetValueFromXml(listAlertCriteriaDescription[i], "CriteriaDescription", "Critere_MONEY1");

            dt.Rows.Add(new object[] { sTAG, sINT1, sINT2, sVARCHAR1, sMONEY1 });
        }

        ViewState["AmlDynamicColumns"] = dt;
    }

    protected DataTable getPages(int nbPages)
    {
        DataTable dt = new DataTable();

        dt.Columns.Add("L");
        dt.Columns.Add("V");

        for (int i = 1; i <= nbPages; i++)
        {
            dt.Rows.Add(i.ToString(), i.ToString());
        }

        return dt;
    }

    protected int CalculateTotalPages(double totalRows, int pageSize)
    {
        int totalPages = (int)Math.Ceiling(totalRows / pageSize);

        return totalPages;
    }
    protected void PaginationManagement(string sRowCount, string sXml)
    {
        if (int.Parse(sRowCount) > int.Parse(ddlNbResult.SelectedValue.ToString()))
        {

            int selectedPage = ddlPage.SelectedIndex;

            DataTable dtPages = new DataTable();
            dtPages.TableName = "dtPages";
            dtPages = getPages(CalculateTotalPages(int.Parse(sRowCount), int.Parse(ddlNbResult.SelectedValue)));
            ddlPage.DataSource = dtPages;
            ddlPage.DataBind();

            string sPageIndex = ""; int iPageIndex = 0;
            sPageIndex = tools.GetValueFromXml(sXml, "ALL_XML_IN/Registration", "PageIndex");
            if (sPageIndex.Trim().Length > 0 && int.TryParse(sPageIndex, out iPageIndex))
            {
                if (ddlPage.Items.Count > 0 && ddlPage.Items.FindByValue(iPageIndex.ToString()) != null)
                    ddlPage.Items.FindByValue(iPageIndex.ToString()).Selected = true;
            }
            else if (Session["RegistrationPageIndex"] != null)
            {
                if (ddlPage.Items.Count > 0)
                    ddlPage.Items.FindByValue(Session["RegistrationPageIndex"].ToString()).Selected = true;
            }
            else if (ddlPage.Items.Count > selectedPage)
                ddlPage.SelectedIndex = selectedPage;

            if (int.Parse(ddlPage.SelectedValue) == ddlPage.Items.Count)
                btnNextPage.Enabled = false;
            else
                btnNextPage.Enabled = true;

            if (int.Parse(ddlPage.SelectedValue) == 1)
                btnPreviousPage.Enabled = false;
            else
                btnPreviousPage.Enabled = true;

            if (ddlPage.Items.Count > 1)
                divPagination.Visible = true;
            else
                divPagination.Visible = false;

            lblTotalPage.Text = " / " + ddlPage.Items.Count.ToString();


        }
        else
        {
            divPagination.Visible = false;
        }
    }

    protected void nextPage(object sender, EventArgs e)
    {
        int iNbPage;

        int.TryParse(ddlPage.SelectedValue, out iNbPage);
        iNbPage++;
        if (ddlPage.Items.FindByValue(iNbPage.ToString()) != null)
            ddlPage.SelectedValue = iNbPage.ToString();

        bindAlertGrid();
    }
    protected void previousPage(object sender, EventArgs e)
    {
        int iNbPage;

        int.TryParse(ddlPage.SelectedValue, out iNbPage);
        iNbPage--;
        if (ddlPage.Items.FindByValue(iNbPage.ToString()) != null)
            ddlPage.SelectedValue = iNbPage.ToString();

        bindAlertGrid();
    }
    protected void onChangePageNumber(object sender, EventArgs e)
    {
        bindAlertGrid();
    }
    protected void onChangeNbResult(object sender, EventArgs e)
    {
        bindAlertGrid();
    }
    protected void onChangeAlertType(object sender, EventArgs e)
    {
        _sSortColumn = null;
        _sSortDirection = null;
        bindAlertGrid();
    }


    protected string getAmlDetails(string sTagAlert, string sCritere_INT1, string sCritere_INT2, string sCritere_VARCHAR1, string sCritere_MONEY1, string sTransferXML)
    {
        string sAmlDetails = "";

        if (ViewState["AmlDynamicColumns"] != null)
        {
            try
            {
                DataTable dt = new DataTable();
                dt.TableName = "dtAmlDynamicColumns";
                dt = (DataTable)ViewState["AmlDynamicColumns"];
                sAmlDetails = "<div class=\"tableAmlSearch\"><div>";

                string hTransferXML = "";
                if (!string.IsNullOrWhiteSpace(sTransferXML)) {
                    hTransferXML = HttpUtility.HtmlDecode(sTransferXML);
                    sAmlDetails = "<div class=\"tableAmlSearch tooltip\" title=\""+hTransferXML+"\"><div>";
                }

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if(dt.Rows[i]["TAG"].ToString().Trim() == sTagAlert.Trim())
                    {
                        if(sTagAlert != "Requisition")
                            if (!String.IsNullOrWhiteSpace(dt.Rows[i]["INT1"].ToString()) && !String.IsNullOrWhiteSpace(sCritere_INT1))
                            {
                                sAmlDetails += "<div style=\"padding: 0 5px;\"><b>" + dt.Rows[i]["INT1"].ToString().Trim() + "</b>";
                                sAmlDetails += " <span>" + sCritere_INT1 + "</span></div>";

                            }
                        if (!String.IsNullOrWhiteSpace(dt.Rows[i]["INT2"].ToString()) && !String.IsNullOrWhiteSpace(sCritere_INT2))
                        {
                            sAmlDetails += "<div style=\"padding: 0 5px;\"><b>" + dt.Rows[i]["INT2"].ToString().Trim() + "</b>";
                            sAmlDetails += " <span>" + sCritere_INT2 + "</span></div>";

                        }
                        if (!String.IsNullOrWhiteSpace(dt.Rows[i]["VARCHAR1"].ToString()) && !String.IsNullOrWhiteSpace(sCritere_VARCHAR1))
                        {
                            if (sTagAlert != "Requisition")
                            {
                                sAmlDetails += "<div style=\"padding: 0 5px;\"><b>" + dt.Rows[i]["VARCHAR1"].ToString().Trim() + "</b>";
                                sAmlDetails += " <span>" + sCritere_VARCHAR1 + "</span></div>";
                            }
                            else
                            {
                                DataTable dtReq = new DataTable("dtReq");
                                dtReq = Requisition.GetMotifRequisitionList();
                                String[] arMotif = sCritere_VARCHAR1.Split(';');
                                if(dtReq != null && dtReq.Rows.Count > 0 && arMotif.Length > 0)
                                {
                                    StringBuilder sb = new StringBuilder();

                                    for (int j = 0; j < arMotif.Length; j++)
                                    {
                                        for (int k = 0; k < dtReq.Rows.Count; k++)
                                        {
                                            if(arMotif[j] == dtReq.Rows[k]["Value"].ToString())
                                            {
                                                sb.AppendLine(dtReq.Rows[k]["Text"].ToString());
                                                break;
                                            }
                                        }
                                    }

                                    sAmlDetails += "<div style=\"padding: 0 5px;\"><b>" + dt.Rows[i]["VARCHAR1"].ToString().Trim() + "</b></div>";
                                    sAmlDetails += " <div style=\"padding: 0 5px;\">" + sb.ToString().Replace(Environment.NewLine, "<br/>") + "</div>";
                                }
                            }

                        }
                        if (!String.IsNullOrWhiteSpace(dt.Rows[i]["MONEY1"].ToString()) && !String.IsNullOrWhiteSpace(sCritere_MONEY1))
                        {
                            if (sTagAlert != "Requisition")
                            {
                                sAmlDetails += "<div style=\"padding: 0 5px;\"><b>" + dt.Rows[i]["MONEY1"].ToString().Trim() + "</b>";
                                sAmlDetails += " <span style=\"white-space:nowrap\">" + tools.getFormattedMoney(sCritere_MONEY1) + "</span></div>";
                            }
                            else
                            {
                                int iCloseAccount;
                                int.TryParse(sCritere_MONEY1.Split(',')[0], out iCloseAccount);

                                sAmlDetails += "<div style=\"padding: 0 5px;\"><b>" + dt.Rows[i]["MONEY1"].ToString().Trim() + "</b>";
                                sAmlDetails += " <span>" + ((iCloseAccount == 0) ? "Non" : "Oui") + "</span></div>";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        return sAmlDetails;
    }

    protected string getRowStyle(string sTagAlert, string sCritere_INT1, string sCritere_INT2, string sCritere_VARCHAR1, string sCritere_MONEY1)
    {
        string sRowStyle = "";
        string hTransferXML = "";

        if (sTagAlert == "Requisition")
        {
            if (sTagAlert == "Requisition")
            {
                string[] arReasons = sCritere_VARCHAR1.Split(';');
                for (int i = 0; i < arReasons.Length; i++)
                {
                    if (arReasons[i] == "64") // Remontée de soupçon client sensible
                    {
                        sRowStyle = "background: repeating-linear-gradient(-45deg,rgba(255, 0, 0, 0.3),rgba(255, 0, 0, 0.3) 40px,rgba(255, 255, 0, 0.2) 40px,rgba(255, 255, 0, 0.2) 80px)";
                        break;
                    }
                }
            }

            if (String.IsNullOrWhiteSpace(sRowStyle))
            {
                int iNbReq, iCloseAccount;

                if (int.TryParse(sCritere_INT2, out iNbReq) && int.TryParse(sCritere_MONEY1.Split(',')[0], out iCloseAccount))
                {
                    if (iNbReq >= 3)
                    {
                        if (iCloseAccount == 0)
                            sRowStyle = "background-color:orange";
                        else sRowStyle = "background-color:rgba(255, 0, 0, 0.85)";
                    }
                }
            }
        }

        return sRowStyle;
    }

    protected void UpdateMessage(string sMessage)
    {
        hdnMessage.Value = sMessage;
        //upMessage.Update();
        ScriptManager.RegisterStartupScript(upSearch, upSearch.GetType(), "blockedTransfer_UpdateMessageKEY", "BlockedTransfer_CheckMessages();", true);
        upSearch.Update();
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "blockedTransfer_UpdateMessageKEY", "CheckMessages();", true);
    }

    protected void btnAlertTreatmentUnreservation_Click(object sender, EventArgs e)
    {
        if (hdnRefCustomerSelected.Value.Trim().Length > 0 && hdnRefAlertSelected.Value.Length > 0)
        {
            string sErrorMessage = "";
            if (AML.SetAlertTreatmentStatus(hdnRefAlertSelected.Value.ToString(), hdnRefCustomerSelected.Value.ToString(), authentication.GetCurrent().sToken, "UNRESERVED", out sErrorMessage))
                UpdateMessage("Cette alerte ne vous est plus affectée.");
            else
                UpdateMessage("Une erreur est survenue." + "<br/>Raison : " + sErrorMessage);
        }
        else
            UpdateMessage("Une erreur est survenue.");

        bindAlertGrid();
        //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "getNbPendingTranferReservedCount();getNbPendingTransferCount();", true);
    }

    protected void btnAlertTreatmentReservation_Click(object sender, EventArgs e)
    {
        string sErrorLabel = "";
        authentication auth = authentication.GetCurrent();
        if (hdnRefCustomerSelected.Value.Trim().Length > 0 && hdnRefAlertSelected.Value.Length > 0)
        {
            XElement xml = new XElement("ALL_XML_IN",
                                new XElement("ALERT_TREATMENT",
                                    new XAttribute("CashierToken", auth.sToken),
                                    new XAttribute("RefAlert", hdnRefAlertSelected.Value),
                                    new XAttribute("ReserveAlert", "1")
                                    //,new XAttribute("Profil", "2C")
                                    ));
            //if (AML.SetClientAlertManagement(auth.sToken, hdnRefAlertSelected.Value, out sErrorLabel))
            if (AML.SetAlertTreatment(xml.ToString()))
                Response.Redirect("FastAnalysisSheet.aspx?ref=" + hdnRefAlertSelected.Value + "&client=" + hdnRefCustomerSelected.Value, true);
            else
                UpdateMessage(sErrorLabel);
        }
        else
            UpdateMessage("Une erreur est survenue.");

        bindAlertGrid();
    }

    protected void btnChangeStatusDREOK_Click(object sender, EventArgs e)
    {
        string sRefAlert = hdnRefAlertSelected.Value;

        XElement xml = new XElement("ALERT_TREATMENT",
            new XAttribute("CashierToken", authentication.GetCurrent().sToken),
            new XAttribute("RefAlert", hdnRefAlertSelected.Value),
            new XAttribute("Status", "DREOK"),
            new XAttribute("Treated", "1")
            );

        if (!String.IsNullOrWhiteSpace(amlComment.Commentary))
            xml.Add(new XElement("NOTES",
                new XElement("NOTE",
                    new XAttribute("Note", amlComment.Commentary),
                    new XAttribute("Type", "DRE"))));

        if (AML.SetAlertTreatment(new XElement("ALL_XML_IN", xml).ToString()))
            UpdateMessage("Changement de statut effectué.");
        else
            UpdateMessage("Une erreur est survenue lors de l'enregistrement.");

        bindAlertGrid();
    }

    protected void btnShowAlertWithoutCharge_Click(object sender, EventArgs e)
    {
        Response.Redirect("FastAnalysisSheet.aspx?ref=" + hdnRefAlertSelected.Value + "&client=" + hdnRefCustomerSelected.Value, true);
    }

    protected void ddlGroupTagAlert_SelectedIndexChanged(object sender, EventArgs e)
    {
        SearchGroupAlert();
    }

    protected DataTable GetGroupAlert(string sCashierToken, string sGroupTag)
    {
        DataTable dt = new DataTable();
        dt.TableName = "dtGetGroupAlert";
        //dt.Columns.Add("Header", typeof(DataTable));
        //dt.Columns.Add("Body", typeof(DataTable));
        SqlConnection conn = null;

        if (_dtGroupDefinition.Rows.Count > 0)
        {
            DataTable dtHeader = new DataTable("dtHeader");
            for (int i = 0; i < _dtGroupDefinition.Rows.Count; i++)
            {
                if (_dtGroupDefinition.Rows[i]["GroupTAG"].ToString() == sGroupTag)
                {
                    dtHeader = (DataTable)_dtGroupDefinition.Rows[i]["GroupParamsNames"];
                    break;
                }
            }

            try
            {
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
                conn.Open();

                SqlCommand cmd = new SqlCommand("AML.P_SearchAlertGroup", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
                cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);
                cmd.Parameters.Add("@ReturnSelect", SqlDbType.Bit);

                cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                            new XElement("AlertGroup",
                                                new XAttribute("CashierToken", sCashierToken),
                                                new XAttribute("GroupTAG", sGroupTag))).ToString();
                cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;
                cmd.Parameters["@ReturnSelect"].Value = 0;

                cmd.ExecuteNonQuery();

                string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

                if (sXmlOut.Length > 0)
                {
                    List<string> listAlertGroup = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/AlertGroup");

                    if (listAlertGroup.Count > 0)
                    {
                        DataTable dtBody = new DataTable();
                        dtBody.TableName = "dtBody";
                        dtBody.Columns.Add("RefAlertGroup");
                        for (int i = 0; i < dtHeader.Rows.Count; i++)
                            dtBody.Columns.Add(dtHeader.Rows[i]["N"].ToString());
                        DataRow row = dtBody.NewRow();
                        for (int i = 0; i < dtHeader.Rows.Count; i++)
                            row[dtHeader.Rows[i]["N"].ToString()] = dtHeader.Rows[i]["V"].ToString();
                        dtBody.Rows.Add(row);

                        for (int i = 0; i < listAlertGroup.Count; i++)
                        {
                            List<string> listRefAlertGroup = CommonMethod.GetAttributeValues(listAlertGroup[i], "AlertGroup", "refAlertGroup");
                            List<string> listParams = CommonMethod.GetTags(listAlertGroup[i], "AlertGroup/GroupParamsValues/PARAMS/P");
                            row = dtBody.NewRow();

                            row["RefAlertGroup"] = listRefAlertGroup[0];

                            for (int j = 0; j < listParams.Count; j++)
                            {
                                List<string> listN = CommonMethod.GetAttributeValues(listParams[j], "P", "N");
                                List<string> listV = CommonMethod.GetAttributeValues(listParams[j], "P", "V");

                                if (dtBody.Columns[listN[0].ToString()] != null)
                                {
                                    row[listN[0].ToString()] = listV[0];
                                }
                            }

                            dtBody.Rows.Add(row);
                        }

                        //dt.Rows.Add(new object[] { dtHeader, dtBody });
                        dt = dtBody;
                    }
                }
            }
            catch (Exception e)
            {

            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        return dt;
    }

    protected void rblGroupType_SelectedIndexChanged(object sender, EventArgs e)
    {
        panelFiltersMultiple.Visible = false;
        panelFiltersSingle.Visible = false;
        panelGrid.Visible = false;
        panelGroupResult.Visible = false;
        btnBackToGroup.Visible = false;

        if (rblGroupType.SelectedValue == "1")
        {
            panelFiltersMultiple.Visible = true;
            panelGroupResult.Visible = true;
            SearchGroupAlert();
        }
        else
        {
            panelFiltersSingle.Visible = true;
            panelGrid.Visible = true;
            bindAlertGrid();
        }
    }

    protected void SearchGroupAlert()
    {
        DataTable dt = new DataTable();
        dt.TableName = "dtSearchGroupAlert";
        dt = GetGroupAlert(authentication.GetCurrent().sToken, ddlGroupTagAlert.SelectedValue);

        if(dt.Rows.Count > 0)
        {
            gvGroupResult.Visible = true;
            panelGroupNoResult.Visible = false;
            gvGroupResult.DataSource = dt;
            gvGroupResult.DataBind();
        }
        else
        {
            gvGroupResult.Visible = false;
            panelGroupNoResult.Visible = true;
        }

        upSearch.Update();

        saveParam();
    }

    protected void gvGroupResult_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HiddenField hdnRefAlertGroup = (HiddenField)e.Row.FindControl("hdnRefAlertGroup");
            if (hdnRefAlertGroup != null && !String.IsNullOrEmpty(hdnRefAlertGroup.Value))
            {
                e.Row.Attributes.Add("onclick", "SelectGroupAlert(" + hdnRefAlertGroup.Value + ");");
                e.Row.Attributes.Add("onmouseover", "this.style.cursor = 'pointer';this.style.backgroundColor = '#ff9966'");
                e.Row.Attributes.Add("onmouseout", "this.style.cursor = 'auto';this.style.backgroundColor = '#fff'");
            }
        }
    }

    protected void btnSelectRefAlertGroup_Click(object sender, EventArgs e)
    {
        if(!String.IsNullOrEmpty(hdnRefAlertGroupSelected.Value))
        {
            panelGroupResult.Visible = false;
            panelGrid.Visible = true;
            bindAlertGrid();
            btnBackToGroup.Visible = true;
        }
    }

    protected void btnBackToGroup_Click(object sender, EventArgs e)
    {
        rblGroupType_SelectedIndexChanged(sender, e);
    }

    protected void btnSort_Click(object sender, EventArgs e)
    {
        string sOrderBy = null;

        if (!String.IsNullOrWhiteSpace(hdnSort.Value))
        {
            sOrderBy = hdnSort.Value;
        }

        if (_sSortColumn != null && _sSortColumn == sOrderBy)
            _sSortDirection = (_sSortDirection == "ASC") ? "DESC" : "ASC";
        else _sSortDirection = "DESC";

        _sSortColumn = sOrderBy;

        bindAlertGrid();
    }
    protected string GetSortDirection(string sSortExpression)
    {
        string sSortDirectionClass = "";

        if(_sSortColumn == sSortExpression)
        {
            if (_sSortDirection == "ASC")
                sSortDirectionClass = "arrow-up";
            else sSortDirectionClass = "arrow-down";
        }

        return sSortDirectionClass;
    }

    protected string GetCustomerXMLInfos(string sCustomerXML)
    {
        string hCustomerInfos = "";
        if (!string.IsNullOrWhiteSpace(sCustomerXML))
            hCustomerInfos = HttpUtility.HtmlDecode(sCustomerXML);
        return hCustomerInfos;
    }
}