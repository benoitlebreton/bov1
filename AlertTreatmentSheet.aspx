﻿<%@ Page Title="Fiche de traitement alerte" Language="C#" AutoEventWireup="true" CodeFile="AlertTreatmentSheet.aspx.cs" Inherits="AlertTreatmentSheet" MasterPageFile="~/Site.master" EnableEventValidation="false" %>

<%@ Register Src="~/API/AmlProfileChange.ascx" TagName="AmlProfileChange" TagPrefix="asp" %>
<%@ Register Src="~/API/AmlSurveillance.ascx" TagName="AmlSurveillance" TagPrefix="asp" %>
<%@ Register Src="~/API/AmlComment.ascx" TagName="AmlComment" TagPrefix="asp" %>
<%@ Register Src="~/API/ReturnFunds.ascx" TagName="ReturnFunds" TagPrefix="asp" %>
<%@ Register Src="~/API/LightLock.ascx" TagName="LightLock" TagPrefix="asp" %>
<%@ Register Src="~/API/ProNotificationCounter.ascx" TagName="ProNotificationCounter" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script type="text/javascript" src="Scripts/AccountClosing.js"></script>
    <script type="text/javascript" src="Scripts/beneficiary.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            init();
        });

        function init() {
            $('.radioButtonList').buttonset();

            initMailAutocomplete();
            InitCheckboxes();
            InitAmlMultiComment();
        }

        function MaxLength(text, idTextNbChar, maxlength) {
            maxlength_val = 100;
            if (maxlength != null) {
                if(isNaN(maxlength)) {
                    if($('#'+maxlength) != null && !isNaN($('#'+maxlength).html())) { 
                        maxlength_val = $('#'+maxlength).html(); 
                        //console.log('maxlength from lbl : ' + $('#'+maxlength).html()); 
                    }
                }
                else { 
                    maxlength_val = maxlength; 
                    //console.log('maxlength from number'); 
                }
            }

            var object = document.getElementById(text.id)  //get your object
            if (object.value.length > maxlength_val) {
                object.focus(); //set focus to prevent jumping
                object.value = text.value.substring(0, maxlength_val); //truncate the value
                object.scrollTop = object.scrollHeight; //scroll to the end to prevent jumping
                $('#'+idTextNbChar).css('color', '#FF0000');
                return false;
            }
            else if (object.value.length == maxlength_val) {
                $('#'+idTextNbChar).css('color', '#FF0000');
            }
            else {
                $('#'+idTextNbChar).css('color', '#344B56');
            }

            $('#'+idTextNbChar).html(object.value.length);
            return true;
        }
        function clearComment(idCommentTxt, idCommentNbChar) {
            if ($('#' + idCommentTxt) != null) {
                $('#' + idCommentTxt).val("");
                $('#' + idCommentNbChar).html("0");
            }
        }

        function InitCheckboxes(){
            $('#<%=cbDriAskConsideration.ClientID%>').unbind('click').bind('click', function(){
                onCheckAskConsideration();
            });

            $('#<%=cbDriAskClient.ClientID%>').unbind('click').bind('click', function(){ 
                onCheckAskClient();
            });

            $('#<%=cbDriNoContact.ClientID%>').unbind('click').bind('click', function(){ 
                onCheckNoContact();
            });

            $('#<%=ckbCounterpartNoAnswer.ClientID%>').unbind('click').bind('click', function(){ 
                onCheckCounterpartNoAnswer();
            });

            $('#<%=ckbCallCounterpart.ClientID%>').unbind('click').bind('click', function(){ 
                onCheckCounterpartNoAnswerCall();
            });

            $('#<%=ckbNotificationPro.ClientID%>').unbind('click').bind('click', function () {
                onCheckNotificationPro();
            });
            $('#<%=ckbNotificationProMail.ClientID%>').unbind('click').bind('click', function () {
                onCheckNotificationProMail();
            });
            $('#<%=ckbNotificationProDRI.ClientID%>').unbind('click').bind('click', function () {
                onCheckNotificationPro('dri');
            });
            $('#<%=ckbNotificationProMailDRI.ClientID%>').unbind('click').bind('click', function () {
                onCheckNotificationProMail('dri');
            });

            $('#<%=panelDriAskClient.ClientID%> input[type=checkbox]:not(#<%=panelClientCall.ClientID%> input[type=checkbox])').unbind('click').on('click', function () {
                var that = $(this);
                $('#<%=panelDriAskClient.ClientID%> input[type=checkbox]:not(#<%=panelClientCall.ClientID%> input[type=checkbox])').each(function(){
                    if($(this).attr('id') != $(that).attr('id'))
                        $(this).attr('checked', false);
                });

                onCheckAutoEntrepreneur();
                $('#<%=btnRefreshMacro.ClientID%>').click();
                $('#<%=panelMacro.ClientID%>').removeClass('hidden');
                <%--$('#<%=panelClientManualResponse.ClientID%>').removeClass('hidden');--%>
            });

            $('#<%=ckbClientCall.ClientID%>').unbind('click').bind('click', function(){ 
                onCheckClientCall();
            });

            $('#<%=ckbClientNoAnswer.ClientID%>').unbind('click').bind('click', function () {
                onCheckClientNoAnswer();
            });

            $('#<%=panelCallCounterpart.ClientID%> input[type=checkbox]').unbind('click').on('click', function () {
                var that = $(this);
                $('#<%=panelCallCounterpart.ClientID%> input[type=checkbox]').each(function(){
                    if($(this).attr('id') != $(that).attr('id'))
                        $(this).attr('checked', false);
                });
            });
        }
        function onCheckNotificationPro(kind)
        {
            if (kind == null) {
                if ($('#<%=ckbNotificationPro.ClientID%>').is(':checked'))
                    $('#<%=panelNotificationpro.ClientID%>').removeClass('hidden');
                else {
                    $('#<%=panelNotificationpro.ClientID%>').addClass('hidden');
                    onCheckNotificationProMail(kind);
                }
            }
            else {
                if ($('#<%=ckbNotificationProDRI.ClientID%>').is(':checked'))
                    $('#divNotificationProDRI').removeClass('hidden');
                else {
                    $('#divNotificationProDRI').addClass('hidden');
                    onCheckNotificationProMail(kind);
                }
            }
        }
        function onCheckNotificationProMail(kind) {
            if (kind == null) {
                if ($('#<%=ckbNotificationProMail.ClientID%>').is(':checked'))
                    $('#<%=panelNotificationproemail.ClientID%>').removeClass('hidden');
                else $('#<%=panelNotificationproemail.ClientID%>').addClass('hidden');
            }
            else {
                if ($('#<%=ckbNotificationProMailDRI.ClientID%>').is(':checked'))
                    $('#divNotificationProEmailDRI').removeClass('hidden');
                else $('#divNotificationProEmailDRI').addClass('hidden');
            }
        }
        function onCheckAskClient() {
            var callback = null;
            if ($('#<%=cbDriAskClient.ClientID%>').is(':checked')) {
                callback = function () { $('#<%=panelDriAskClient.ClientID%>').slideDown(300, function () { $(this).removeClass('hidden'); }); };
                onCheckNotNoContact(callback);
            }
            else $('#<%=panelDriAskClient.ClientID%>').slideUp(300, function () { $(this).addClass('hidden'); onCheckNotNoContact(); });
        }

        function onCheckAskConsideration() {
            var callback = null;
            if ($('#<%=cbDriAskConsideration.ClientID%>').is(':checked')) {
                callback = function () { $('#<%=panelDriAskConsideration.ClientID%>').slideDown(300, function () { $(this).removeClass('hidden'); }); };
                onCheckNotNoContact(callback);
            }
            else $('#<%=panelDriAskConsideration.ClientID%>').slideUp(300, function () { $(this).addClass('hidden'); onCheckNotNoContact(); });
        }

        function onCheckNotNoContact(callback) {
            if($('#<%=cbDriAskClient.ClientID%>').is(':checked') || $('#<%=cbDriAskConsideration.ClientID%>').is(':checked'))
                $('#<%=panelNoContact.ClientID%>').animate({ 'width': '0' }, 300, function () { $(this).addClass('hidden'); if (typeof callback === 'function') callback(); });
            else $('#<%=panelNoContact.ClientID%>').removeClass('hidden').animate({ 'width': '220' }, 300, callback);
        }

        function onCheckNoContact() {
            if ($('#<%=cbDriNoContact.ClientID%>').is(':checked')) {
                $('#<%=panelClient.ClientID%>').addClass('hidden');
                $('#<%=panelCounterpart.ClientID%>').addClass('hidden');
                $('#<%=panelNoContact.ClientID%>').css({ 'border-left': '0', 'width': '100%' });

                <%--$('#<%=panelDriNoContact.ClientID%>').slideDown(300, function () { $(this).removeClass('hidden'); });--%>
            }
            else {
                <%--$('#<%=panelDriNoContact.ClientID%>').slideUp(300, function () {--%>
                    $(this).addClass('hidden');
                    $('#<%=panelClient.ClientID%>').removeClass('hidden');
                    $('#<%=panelCounterpart.ClientID%>').removeClass('hidden');
                    $('#<%=panelNoContact.ClientID%>').css({ 'border-left': '1px solid #999', 'width': '220px' });
                <%--});--%>
            }
        }

        function onCheckCounterpartNoAnswer() {
            if($('#<%=ckbCounterpartNoAnswer.ClientID%>').is(':checked'))
                $('#<%=panelCounterpartNoAnswer.ClientID%>').removeClass('hidden');
            else $('#<%=panelCounterpartNoAnswer.ClientID%>').addClass('hidden');
        }

        function onCheckCounterpartNoAnswerCall() {
            if($('#<%=ckbCallCounterpart.ClientID%>').is(':checked'))
                $('#<%=panelCallCounterpart.ClientID%>').removeClass('hidden');
            else $('#<%=panelCallCounterpart.ClientID%>').addClass('hidden');
        }

        function onCheckReturnFunds() {
            if($('#<%=ckbReturnFunds.ClientID%>').is(':checked'))
                $('#<%=panelReturnFunds.ClientID%>').removeClass('hidden');
            else $('#<%=panelReturnFunds.ClientID%>').addClass('hidden');
        }

        function onCheckAutoEntrepreneur(){
            if($('#<%=ckbAutoEntrepreneur.ClientID%>').is(':checked'))
            {
                $('#<%=panelDriAskConsideration.ClientID%>').addClass('hidden');
                $('#<%=cbDriAskConsideration.ClientID%>').attr('checked', false).attr('disabled', true);
            }
            else $('#<%=cbDriAskConsideration.ClientID%>').removeAttr('disabled');
        }

        function onCheckClientCall()
        {
            if($('#<%=ckbClientCall.ClientID%>').is(':checked'))
            {
                $('#<%=panelClientCallDetails.ClientID%>').removeClass('hidden');
                $('#<%=panelClientManualResponse.ClientID%>').addClass('hidden');
            }
            else {
                $('#<%=panelClientCallDetails.ClientID%>').addClass('hidden');
                $('#<%=panelClientManualResponse.ClientID%>').removeClass('hidden');
            }
        }
        function onCheckClientNoAnswer()
        {
            if($('#<%=ckbClientNoAnswer.ClientID%>').is(':checked'))
                $('#<%=panelClientManualResponse.ClientID%>').addClass('hidden');
            else $('#<%=panelClientManualResponse.ClientID%>').removeClass('hidden');
        }

        function ShowDofResponse(title) {
            $('#dialog-DOFResponse').dialog({
                autoOpen: true,
                resizable: false,
                draggable: false,
                modal: true,
                minWidth: 800,
                height: 'auto',
                minHeight: 'auto',
                zIndex: 99997,
                title: title
                //,buttons: [{ text: 'Annuler', click: function () { $(this).dialog('close'); } }]
            });
            $('#dialog-DOFResponse').parent().appendTo(jQuery("form:first"));

            $("#<%=panelDOFResponseOK.ClientID%> input[type=hidden]").bind('change', function(){
                if($("#<%=panelDOFResponseOK.ClientID%> input[type=hidden]").val() == "2BC")
                {
                    InitAMLToggles();
                    InitAMLSurveillance();
                    $('#divAmlSurveillanceResponse').removeClass('hidden');
                    $('#dialog-DOFResponse').dialog({ position: 'center' });
                }
                else $('#divAmlSurveillanceResponse').addClass('hidden');
            });

            $('#<%=ckbReturnFunds.ClientID%>').unbind('click').bind('click', function(){
                onCheckReturnFunds();
                $('#dialog-DOFResponse').dialog({ position: 'center' });
            });

            date_picker();
            //InitIBANinputs();
            //beneficiaryInit();
        }

        function InitProfileToSurveillance() {
            $("#<%=panelDREOK.ClientID%> input[type=hidden]").bind('change', function(){
                if($("#<%=panelDREOK.ClientID%> input[type=hidden]").val() == "2BC")
                {
                    $('input[id$=ckbMiseSousSurveillance]').attr('checked', true);
                    InitAMLToggles();
                    InitAMLSurveillance();
                }
            });
        }

        function changeDOFResponse() {
            $('#<%=panelDOFResponseOK.ClientID%>').addClass('hidden');
            $('#<%=panelDOFResponseNotSatisfactory.ClientID%>').addClass('hidden');
            $('#<%=panelDOFResponseKO.ClientID%>').addClass('hidden');

            //console.log($('input[name="ctl00$MainContent$rblDOFResponse"]:checked').val());

            switch($('input[name="ctl00$MainContent$rblDOFResponse"]:checked').val())
            {
                case "OK":
                    $('#<%=panelDOFResponseOK.ClientID%>').removeClass('hidden');
                    break;
                case "KO":
                    $('#<%=panelDOFResponseKO.ClientID%>').removeClass('hidden');
                    break;
                case "NotSatisfactory":
                    $('#<%=panelDOFResponseNotSatisfactory.ClientID%>').removeClass('hidden');
                    break;
            }
            $('#<%=panelDOFResponseValidation.ClientID%>').removeClass('hidden');
            $('#dialog-DOFResponse').dialog({ position: 'center' });
            //panelToLoad = "divDOFResponse";
            //__doPostBack("<=btnDOFResponseChange.UniqueID%>","");
        }

        function changeDeclarable() {
            $('#<%=panelDeclarable3.ClientID%>').addClass('hidden');

            //console.log($('input[name="ctl00$MainContent$rblDOFResponse"]:checked').val());

            switch($('input[name="ctl00$MainContent$rblDeclarable"]:checked').val())
            {
                case "3":
                    $('#<%=panelDeclarable3.ClientID%>').removeClass('hidden');
                    break;
            }
            $('#dialog-DOFResponse').dialog({ position: 'center' });
        }

        function split(val) {
            return val.split(/;\s*/);
        }
        function extractLast(term) {
            return split(term).pop();
        }
        function initMailAutocomplete() {
            $("#<%=txtEmailContactCounterpart.ClientID%>")
              // don't navigate away from the field on tab when selecting an item
              .on("keydown", function (event) {
                  if (event.keyCode === $.ui.keyCode.TAB &&
                      $(this).autocomplete("instance").menu.active) {
                      event.preventDefault();
                  }
              })
              .autocomplete({
                  source: function (request, response) {
                      //console.log(request.term);
                    $.ajax({
                        type: "POST",
                        url: 'ws_tools.asmx/GetCounterpartEmailList',
                        data: '{ "prefix": "' + extractLast(request.term) + '" }',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (msg) {
                            //console.log(msg.d);
                            response(msg.d);
                        },
                        error: function (e) {
                        
                        }
                    });
                  },
                  search: function () {
                      // custom minLength
                      var term = extractLast(this.value);
                      if (term.length < 2) {
                          return false;
                      }
                  },
                  focus: function () {
                      // prevent value inserted on focus
                      return false;
                  },
                  select: function (event, ui) {
                      var terms = split(this.value);
                      // remove the current input
                      terms.pop();
                      // add the selected item
                      terms.push(ui.item.value);
                      // add placeholder to get the comma-and-space at the end
                      terms.push("");
                      this.value = terms.join("; ");
                      return false;
                  }
              });
        }

        function CheckDREState() {
            if ($('#<%=rblDREOKKO.ClientID%> input[type=radio]:checked').prop('value') == 'DREKO') {
                $('#dialog-DREKO-declarable').dialog({
                    autoOpen: true,
                    resizable: false,
                    draggable: false,
                    modal: true,
                    minWidth: 440,
                    height: 'auto',
                    minHeight: 'auto',
                    zIndex: 99997,
                    title: "Voulez-vous déclarer ?"
                    //, buttons: [{ text: 'Valider', click: function () { $('#<%=btnSaveDREOK.ClientID%>').click(); } }]
                });
                $('#dialog-DREKO-declarable').parent().appendTo(jQuery("form:first"));
            }
            else $('#<%=btnSaveDREOK.ClientID%>').click();
        }
    </script>

    <link type="text/css" rel="Stylesheet" href="Styles/AccountClosingControls.css" />
    <link type="text/css" rel="Stylesheet" href="Styles/ReturnFunds.css" />
    <link type="text/css" rel="Stylesheet" href="Styles/ProNotificationCounter.css" />
    <style type="text/css">
        input.multi-row-data, select.multi-row-data, .multi-row-data input, .multi-row-data select {
            height:auto;
        }

        h3 {
            margin:0 0 5px 0;
        }

        h4 {
            font-size:1em;
            margin:0;
        }

        #ar-loading {
            background:url('Styles/img/loading2.gif') no-repeat center;
            background-size: 35px 27px;
            display:none;
            position:absolute;
            background-color:rgba(255, 255, 255, 0.5);
            z-index:100000;
        }

        .cadreOrange {
             margin:auto;
             border:1px solid #ff6a00; 
             padding:5px; 
             margin:10px 0;
        }

        .hidden {
            display:none;
        }
        .visible {
            display:auto;
        }

        /*#divAlertTreatment*/ .ui-button-text-only .ui-button-text {
            padding:.1em 1em;
        }

        .div-form-dri, .div-counterpart-answer, .flex-container {
            display:flex;
            flex-direction:row;
        }
        .flex-item {
            flex-grow:1;
            flex:1;
        }
        .div-form-dri .div-client, .div-form-dri .div-counterpart,
        .div-counterpart-answer .div-counterpart-noanswer, .div-counterpart-answer .div-counterpart-answered {
            flex-grow:1;
            flex:6;
            padding:0 4px;
        }
        .div-counterpart-answer .div-counterpart-noanswer, .div-counterpart-answer .div-counterpart-answered {
            padding:0;
        }
        .div-form-dri .div-counterpart {
            flex:7;
        }
        .div-form-dri .div-client,
        .div-counterpart-answer .div-counterpart-noanswer {
            padding-right:6px;
        }
        .div-form-dri .div-counterpart,
        .div-counterpart-answer .div-counterpart-answered,
        .div-form-dri .div-nocontact {
            padding-left:6px;
            border-left:1px solid #999;
        }
        .div-form-dri .div-nocontact {
            width:220px;
            white-space:nowrap;
        }
        .div-counterpart-answer {
            margin-top:10px;
        }
        .div-counterpart-answer .div-counterpart-noanswer {
            flex:3;
        }
        .div-counterpart-answer .div-counterpart-answered {
            flex:2;
        }

        select {
            height: 30px;
            font-size: 17px;
        }
        .normal-select {
            height:auto;
            font-size:1em;
            width:100%;
        }

        .ui-widget .button {
            font-size: 14px;
            font-weight: bold;
            font-family: Arial, Verdana;
        }

        .ui-dialog .ui-dialog-title {
            text-transform: uppercase;
        }

        .main {
            min-height:240px;
        }

        .ui-widget-overlay {
            position:fixed;
        }

        #MainContent_rblAlertStatus .ui-state-disabled {
            opacity:unset;
        }

        input[readonly], textarea[readonly], textarea[disabled], input[readonly] {
            background-color:#fff;
            border:1px solid #999;
            color:#000;
            font-size: 14px !important;
        }

        #dialog-DOFResponse .ui-buttonset {
            width:100%;
            border-spacing:0;
        }
        #dialog-DOFResponse .ui-buttonset td:nth-of-type(2) {
            width:40%;
        }
        /*#dialog-DOFResponse .ui-buttonset td {
            width:50%;
        }*/
        #dialog-DOFResponse .ui-buttonset .ui-button {
            width:100%;
            text-transform:uppercase;
        }

        .date-j {
            font-size:1.2em;
            font-weight:bold;
            position: relative;
            left: 5px;
            top: 3px;
        }

        .new-item {
            /* 0 does not work so we have to use a small number */
            /* Start our small */
            flex: .00001;

            -webkit-animation: flexGrow 500ms ease forwards;
            -o-animation: flexGrow 500ms ease forwards;
            animation: flexGrow 500ms ease forwards;
        }
        .remove-item {
            flex: 1 !important;

            -webkit-animation: flexShrink 500ms ease forwards;
            -o-animation: flexShrink 500ms ease forwards;
            animation: flexShrink 500ms ease forwards;
        }

        @-webkit-keyframes flexGrow {
          to {
            flex: 1;
            width:initial;
          }
        }
        @-o-keyframes flexGrow {
          to {
            flex: 1;
            width:initial;
          }
        }
        @keyframes flexGrow {
          to {
            flex: 1;
            width:initial;
          }
        }

        @-webkit-keyframes flexShrink {
          to {
            flex: .01;
            flex: .00001;
            width:0;
          }
        }
        @-o-keyframes flexShrink {
          to {
            flex: .01;
            flex: .00001;
            width:0;
          }
        }
        @keyframes flexShrink {
          to {
            flex: .01;
            flex: .00001;
            width:0;
            box-sizing:border-box;
          }
        }

        #MainContent_rblDREOKKO {
            width:100%;
            border-collapse:collapse;
        }
        #MainContent_rblDREOKKO label {
            width:100%;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="ar-loading"></div>
    
    <div>
        <asp:Button ID="btnPrev" runat="server" CssClass="button" Text="Fiche d'analyse rapide" />
    </div>
    <h2 style="color: #344b56;text-transform:uppercase;">Fiche de traitement alerte</h2>
    <div>
        <div class="table" style="margin:5px 0 0 0;">
            <div class="row">
                <div class="cell" style="vertical-align:middle;">
                    <div style="font-weight:bold; color:#f57527">Titulaire principal</div>
                    <div><asp:Label ID="lblClientName" runat="server"></asp:Label></div>
                </div>
                <div class="cell" style="vertical-align:middle;padding-left:20px;">
                    <div style="font-weight:bold; color:#f57527">N&deg; Carte</div>
                    <div><asp:Label ID="lblClientCardNumber" runat="server"></asp:Label></div>
                </div>
                <div class="cell" style="padding-left:20px;vertical-align:middle;">
                    <div style="font-weight:bold; color:#f57527">N&deg; Compte</div>
                    <div><asp:Label ID="lblClientAccountNumber" runat="server"></asp:Label></div>
                </div>
                <%--<div class="cell" style="padding-left:20px;vertical-align:top;visibility:hidden">
                    <div style="font-weight:bold; color:#f57527">N&deg; Inscription</div>
                    <div><asp:Label ID="lblClientRegistrationNumber" runat="server"></asp:Label></div>
                </div>--%>
                <div class="cell" style="padding-left:20px;vertical-align:top">
                    <asp:LightLock ID="LightLock1" runat="server" ActionAllowed="true" />
                </div>
                <div class="cell" style="padding-left:20px;vertical-align:middle">
                    <asp:ProNotificationCounter ID="proNotificationCounter1" runat="server" />
                </div>
            </div>
        </div>
    </div>
    <asp:UpdatePanel ID="upAlertTreatment" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="flex-container" style="font-size: 1.2em;font-weight: bold;margin-top:10px">
                <div class="flex-item">
                    <asp:Label ID="lblClientProfile" runat="server"></asp:Label>
                </div>
                <div class="flex-item" style="text-align:center;position:relative;">
                    <asp:Label ID="lblCreationDate" runat="server"></asp:Label>
                    <asp:Label ID="lblNbDaysSinceCreation" runat="server" CssClass="font-orange"></asp:Label> <!--style="font-size:1.5em;position:absolute;top:-3px;padding-left:10px;"-->
                </div>
                <div class="flex-item" style="text-align:right;position:relative;top:-21px">
                    <asp:Label ID="lblBOAgent" runat="server"></asp:Label><br />
                    <asp:Label ID="lblBOAgentAttributionDate" runat="server"></asp:Label>
                    <asp:Label ID="lblNbDaysSinceAttribution" runat="server" CssClass="font-orange"></asp:Label>
                </div>
            </div>
            <div id="divAlertTreatment">
                <div class="label" style="margin-top:-5px">
                    Statut
                </div>
                <asp:RadioButtonList ID="rblAlertStatus" runat="server" CssClass="radioButtonList" AutoPostBack="true" RepeatDirection="Horizontal" OnSelectedIndexChanged="rblAlertStatus_SelectedIndexChanged">
                    <asp:ListItem Text="DRI" Value="DRI"></asp:ListItem>
                    <asp:ListItem Text="DRE" Value="DRE"></asp:ListItem>
                </asp:RadioButtonList>
                <asp:HiddenField ID="hfAlertStatusValue" runat="server" Value="" />

                <asp:Panel ID="panelDRI" runat="server" CssClass="hidden" Style="margin: 10px 0" DefaultButton="btnSaveDri">
                    <div class="div-form-dri">
                        <asp:Panel ID="panelClient" runat="server" CssClass="div-client">
                            <input type="checkbox" id="cbDriAskClient" runat="server" class="csscheckbox" />
                            <label for="<%= cbDriAskClient.ClientID %>" class="csscheckbox-label">Interroger le client</label>
                            <asp:Label ID="lblAskClientDate" runat="server" CssClass="date-j"></asp:Label>
                            <asp:Label ID="lblNbDaysSinceAskClient" runat="server" CssClass="date-j font-orange"></asp:Label>
                            <asp:Panel ID="panelDriAskClient" runat="server" CssClass="hidden" style="margin:10px 0 0 20px;">
                                <div>
                                    <input type="checkbox" id="ckbSourceFunds" runat="server" class="csscheckbox" />
                                    <label for="<%= ckbSourceFunds.ClientID %>" class="csscheckbox-label">Demande origine de fonds</label>
                                </div>
                                <div style="margin:10px 0">
                                    <input type="checkbox" id="ckbAskInformationAccountBlocked" runat="server" class="csscheckbox" />
                                    <label for="<%= ckbAskInformationAccountBlocked.ClientID %>" class="csscheckbox-label">Demande d'information compte bloqué</label>
                                </div>
                                <div>
                                    <input type="checkbox" id="ckbAutoEntrepreneur" runat="server" class="csscheckbox" />
                                    <label for="<%= ckbAutoEntrepreneur.ClientID %>" class="csscheckbox-label">Auto-entrepreneur séparation activité</label>
                                </div>
                                <div style="margin:10px 0 0 0">
                                    <input type="checkbox" id="ckbAutreMacro" runat="server" class="csscheckbox" />
                                    <label for="<%= ckbAutreMacro.ClientID %>" class="csscheckbox-label">Autre</label>
                                </div>
                                <asp:Panel ID="panelMacro" runat="server" CssClass="hidden" style="margin-top:10px">
                                    <asp:UpdatePanel ID="upMacro" runat="server">
                                        <ContentTemplate>
                                            <asp:Panel ID="panelDdlMacroClient" runat="server" Visible="false">
                                                <div class="label">
                                                    Macro
                                                </div>
                                                <div>
                                                    <asp:DropDownList ID="ddlMacroClient" runat="server" DataTextField="Description" DataValueField="Tag" 
                                                        AppendDataBoundItems="true" OnSelectedIndexChanged="btnRefreshMacro_Click" AutoPostBack="true" CssClass="normal-select">
                                                        <asp:ListItem Text="" Value=""></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </asp:Panel>
                                            <asp:AmlComment ID="amlMacroContent" runat="server" MaxLength="-1" Height="300" Label="Contenu e-mail" />
                                            <asp:Button ID="btnRefreshMacro" runat="server" style="display:none" OnClick="btnRefreshMacro_Click" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </asp:Panel>
                                <div>
                                    <asp:Panel ID="panelClientCall" runat="server" CssClass="hidden" style="margin:10px 0 0 20px">
                                        <div style="float:left;width:50%">
                                            <input type="checkbox" id="ckbClientCall" runat="server" class="csscheckbox" />
                                            <label for="<%= ckbClientCall.ClientID %>" class="csscheckbox-label">Appel client</label>
                                            <asp:Panel ID="panelClientCallDetails" runat="server" CssClass="hidden" style="margin:10px 0 0 20px">
                                                <div class="label">
                                                    Durée appel
                                                </div>
                                                <div>
                                                    <asp:DropDownList ID="ddlClientCallDuration" runat="server" DataTextField="duration" DataValueField="duration" AppendDataBoundItems="true">
                                                        <asp:ListItem Text="" Value=""></asp:ListItem>
                                                    </asp:DropDownList>
                                                    min
                                                </div>
                                            </asp:Panel>
                                        </div>
                                        <div style="float:right;width:50%">
                                            <input type="checkbox" id="ckbClientNoAnswer" runat="server" class="csscheckbox" />
                                            <label for="<%= ckbClientNoAnswer.ClientID %>" class="csscheckbox-label">Il ne répond pas</label>
                                        </div>
                                        <div style="clear:both"></div>
                                    </asp:Panel>
                                </div>

                                <asp:Panel ID="panelClientManualResponse" runat="server" CssClass="hidden" style="margin-top:10px">
                                    <asp:Button ID="btnClientAnswered" runat="server" Text="Il a répondu" OnClick="ShowDOFResponse_click" CommandArgument="Le client a répondu" CssClass="button" style="width:100%" />
                                </asp:Panel>
                            </asp:Panel>
                        </asp:Panel>
                        <asp:Panel ID="panelCounterpart" runat="server" CssClass="div-counterpart">
                            <input type="checkbox" id="cbDriAskConsideration" runat="server" class="csscheckbox" />
                            <label for="<%=cbDriAskConsideration.ClientID %>" class="csscheckbox-label">Interroger une contrepartie</label>
                            <asp:Label ID="lblAskConsiderationDate" runat="server" CssClass="date-j"></asp:Label>
                            <asp:Label ID="lblNbDaysSinceAskConsideration" runat="server" CssClass="date-j font-orange"></asp:Label>
                            <asp:Panel ID="panelDriAskConsideration" runat="server" CssClass="hidden" style="margin:10px 0 0 20px">
                                <%--<div>
                                    <asp:Panel ID="panelPrevAmlCommentCounterpart" runat="server" CssClass="hidden">
                                        <div id="previous-aml-comment">
                                            <asp:Repeater ID="rptAmlCommentCounterpart" runat="server">
                                                <ItemTemplate>
                                                    <div class='<%# Container.ItemIndex % 2 == 0 ? "row-even" : "row-odd" %>'>
                                                        <asp:Literal ID="litAmlCommentCounterpart" runat="server" Text='<%#Eval("Note") %>'></asp:Literal>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </div>
                                        <div id="show-previous-aml-comment">
                                            Afficher les commentaires précédents
                                        </div>
                                    </asp:Panel>
                                    <asp:AmlComment ID="amlCommentCounterpart" runat="server" MaxLength="400" />
                                </div>--%>

                                <div style="margin-top:9px">
                                    <div class="label">
                                        Adresse e-mail
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtEmailContactCounterpart" runat="server" style="width:100%;box-sizing:border-box"></asp:TextBox>
                                    </div>
                                    <div style="margin-top:11px">
                                    <asp:UpdatePanel ID="upMacroCounterpart" runat="server">
                                        <ContentTemplate>
                                            <div class="label">
                                                Macro
                                            </div>
                                            <div>
                                                <asp:DropDownList ID="ddlMacroCounterpart" runat="server" DataTextField="Description" DataValueField="Tag" 
                                                    AppendDataBoundItems="true" OnSelectedIndexChanged="ddlMacroCounterpart_SelectedIndexChanged" AutoPostBack="true" CssClass="normal-select">
                                                    <asp:ListItem Text="" Value=""></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <asp:AmlComment ID="amlMacroContentCounterpart" runat="server" MaxLength="-1" Height="266" Label="Contenu" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    </div>
                                </div>
                                <div class="div-counterpart-answer">
                                    <div class="div-counterpart-noanswer">
                                        <input type="checkbox" id="ckbCounterpartNoAnswer" runat="server" class="csscheckbox" />
                                        <label for="<%= ckbCounterpartNoAnswer.ClientID %>" class="csscheckbox-label">Elle ne répond pas</label>
                                        <asp:Panel ID="panelCounterpartNoAnswer" runat="server" CssClass="hidden" style="margin:10px 0 0 20px">
                                            <div>
                                                <input type="checkbox" id="ckbUnfreezeAccount" runat="server" class="csscheckbox" />
                                                <label for="<%= ckbUnfreezeAccount.ClientID %>" class="csscheckbox-label">Débloquer le compte</label>
                                            </div>
                                            <div style="margin:10px 0">
                                                <input type="checkbox" id="ckbCallCounterpart" runat="server" class="csscheckbox" />
                                                <label for="<%= ckbCallCounterpart.ClientID %>" class="csscheckbox-label">Appel</label>
                                                <asp:Panel ID="panelCallCounterpart" runat="server" CssClass="hidden" style="margin:10px 0 0 20px">
                                                    <div>
                                                        <input type="checkbox" id="ckbCallCounterpartOk" runat="server" class="csscheckbox" />
                                                        <label for="<%= ckbCallCounterpartOk.ClientID %>" class="csscheckbox-label">Réponse satisfaisante</label>
                                                    </div>
                                                    <div style="margin-top:10px">
                                                        <input type="checkbox" id="ckbCallCounterpartPending" runat="server" class="csscheckbox" />
                                                        <label for="<%= ckbCallCounterpartPending.ClientID %>" class="csscheckbox-label">Attente</label>
                                                    </div>
                                                </asp:Panel>
                                            </div>
                                            <div>
                                                <input type="checkbox" id="ckbContactCounterpartAgain" runat="server" class="csscheckbox" />
                                                <label for="<%= ckbContactCounterpartAgain.ClientID %>" class="csscheckbox-label">Relance</label>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                    <div class="div-counterpart-answered">
                                        <input type="checkbox" id="ckbCounterpartAnswered" runat="server" class="csscheckbox" />
                                        <label for="<%= ckbCounterpartAnswered.ClientID %>" class="csscheckbox-label">Elle a répondu</label>
                                    </div>
                                </div>
                            </asp:Panel>
                        </asp:Panel>
                        <asp:Panel ID="panelNoContact" runat="server" CssClass="div-nocontact">
                            <input type="checkbox" id="cbDriNoContact" runat="server" class="csscheckbox" />
                            <label for="<%=cbDriNoContact.ClientID %>" class="csscheckbox-label">Etude du dossier</label>
                            <asp:Label ID="lblNoContactDate" runat="server" CssClass="date-j"></asp:Label>
                            <asp:Label ID="lblNbDaysSinceNoContact" runat="server" CssClass="date-j font-orange"></asp:Label>
                            <%--<asp:Panel ID="panelDriNoContact" runat="server" CssClass="hidden" style="margin:10px 0 0 20px">
                                <asp:AmlComment ID="amlCommentNoContact" runat="server" MaxLength="400" />
                            </asp:Panel>--%>
                        </asp:Panel>
                    </div>
                    <div style="margin-top:15px">
                        <asp:Panel ID="panelPrevAmlCommentDRI" runat="server" CssClass="hidden">
                            <div id="previous-aml-comment">
                                <asp:Repeater ID="rptAmlCommentDRI" runat="server">
                                    <ItemTemplate>
                                        <div class='<%# Container.ItemIndex % 2 == 0 ? "row-even" : "row-odd" %>'>
                                            <asp:Literal ID="litAmlCommentDRI" runat="server" Text='<%#Eval("Note") %>'></asp:Literal>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                            <div id="show-previous-aml-comment">
                                Afficher les commentaires précédents
                            </div>
                        </asp:Panel>
                        <asp:AmlComment ID="amlCommentDRI" runat="server" MaxLength="400" />
                    </div>
                    <div style="text-align:right; margin-top:40px">
                        <asp:Button ID="btnSaveDri" runat="server" CssClass="button" Text="Enregistrer" OnClick="btnSaveDri_Click" style="width:100%" />
                    </div>
                </asp:Panel>

                <asp:Panel ID="panelDREOK" runat="server" CssClass="hidden">
                    <div style="margin-top:10px">
                        <asp:AmlComment ID="amlCommentDREOK" runat="server" MaxLength="400" />
                    </div>
                    <div style="margin-top:10px">
                        <input type="checkbox" id="ckbNotificationPro" runat="server" class="csscheckbox" />
                        <label for="<%= ckbNotificationPro.ClientID %>" class="csscheckbox-label">Notification Pro</label>
                        <asp:Panel ID="panelNotificationpro" runat="server" style="margin-top:10px;padding-left:10px;" CssClass="hidden">
                            <div>
                                <div style="display:inline-block">
                                    <input type="checkbox" id="ckbNotificationProMail" runat="server" class="csscheckbox" />
                                    <label for="<%= ckbNotificationProMail.ClientID %>" class="csscheckbox-label">par email</label>
                                </div>
                                <div style="display:inline-block;margin-left:10px">
                                    <input type="checkbox" id="ckbNotificationProPhone" runat="server" class="csscheckbox" />
                                    <label for="<%= ckbNotificationProPhone.ClientID %>" class="csscheckbox-label">par téléphone</label>
                                </div>
                            </div>
                            <asp:Panel ID="panelNotificationproemail" runat="server" CssClass="hidden" style="padding-left:10px">
                                <asp:AmlComment ID="amlMacroPro" runat="server" MaxLength="-1" Height="300" Label="Contenu e-mail" />
                            </asp:Panel>
                        </asp:Panel>
                    </div>
                    <div style="margin-top:10px">
                        <asp:AmlProfileChange ID="amlProfileChangeDREOK" runat="server" />
                    </div>
                    <div style="margin-top:10px">
                        <asp:AmlSurveillance ID="amlSurveillanceDREOK" runat="server" ActionAllowed="true" DisplayMode="AlertTreatment" />
                    </div>
                    <div style="margin-top:20px">
                        <asp:RadioButtonList ID="rblDREOKKO" runat="server" CssClass="radioButtonList" RepeatDirection="Horizontal">
                            <asp:ListItem Text="DREOK" Value="DREOK"></asp:ListItem>
                            <asp:ListItem Text="DREKO" Value="DREKO"></asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                    <div style="margin-top:30px">
                        <input type="button" value="Valider" class="button" style="width:100%" onclick="CheckDREState();" />
                    </div>
                </asp:Panel>

                <asp:Panel ID="panelResponseDetails" runat="server" CssClass="hidden">
                    <div class="label">
                        Réponse <asp:Label ID="lblResponseStatus" runat="server"></asp:Label>
                    </div>
                    <asp:Panel ID="panelResponseNotSatisfactoryCommentary" runat="server" CssClass="hidden">
                        <div class="label">
                            Commentaire
                        </div>
                        <div>
                            <asp:Label ID="lblResponseNotSatisfactoryCommentary" runat="server"></asp:Label>
                        </div>
                    </asp:Panel>
                </asp:Panel>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div id="dialog-DOFResponse" style="display:none">
        <asp:UpdatePanel ID="upDOFResponse" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="panelZendesk" runat="server" Visible="false" style="margin-bottom:10px">
                    <asp:Repeater ID="rptZendeskTicket" runat="server">
                        <ItemTemplate>
                            <div>
                                <asp:HyperLink ID="hlZendeskTicket" runat="server" NavigateUrl='<%#Eval("URL") %>' Target="_blank"><%#Eval("Label") %></asp:HyperLink>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </asp:Panel>
                <div id="divDOFResponse">
                    <asp:RadioButtonList ID="rblDOFResponse" runat="server" CssClass="radioButtonList" RepeatDirection="Horizontal" onchange="changeDOFResponse();" style="margin:auto">
                        <asp:ListItem Text="DOFOK" Value="OK">DRIOK</asp:ListItem>
                        <asp:ListItem Text="DOFNotSatisfactory" Value="NotSatisfactory">Réponse non satisfaisante</asp:ListItem>
                        <asp:ListItem Text="DOFKO" Value="KO">DRIKO</asp:ListItem>
                    </asp:RadioButtonList>
                    <asp:Panel ID="panelDOFResponseOK" runat="server" CssClass="hidden" style="margin-top:10px">
                        <asp:AmlComment ID="AmlCommentDRIOK" runat="server" MaxLength="400" />
                        <asp:AmlProfileChange ID="AmlProfileChangeResponse" runat="server" />
                        <div id="divAmlSurveillanceResponse" class="hidden">
                            <asp:AmlSurveillance ID="AmlSurveillanceReponse" runat="server" ActionAllowed="true" DisplayMode="AlertTreatment" />
                        </div>
                        <div>
                            <input type="checkbox" id="ckbReturnFunds" runat="server" class="csscheckbox" />
                            <label for="<%= ckbReturnFunds.ClientID %>" class="csscheckbox-label">Avec retour de fonds</label>
                        </div>
                        <asp:Panel ID="panelReturnFunds" runat="server" CssClass="hidden" style="padding-top:10px;">
                            <asp:ReturnFunds ID="ReturnFunds1" runat="server" />             
                        </asp:Panel>
                        <div style="margin-top:10px">
                            <input type="checkbox" id="ckbNotificationProDRI" runat="server" class="csscheckbox" />
                            <label for="<%= ckbNotificationProDRI.ClientID %>" class="csscheckbox-label">Notification Pro</label>
                            <div id="divNotificationProDRI" class="hidden" style="margin-top:10px;padding-left:10px;">
                                <div>
                                    <div style="display:inline-block">
                                        <input type="checkbox" id="ckbNotificationProMailDRI" runat="server" class="csscheckbox" />
                                        <label for="<%= ckbNotificationProMailDRI.ClientID %>" class="csscheckbox-label">par email</label>
                                    </div>
                                    <div style="display:inline-block;margin-left:10px">
                                        <input type="checkbox" id="ckbNotificationProPhoneDRI" runat="server" class="csscheckbox" />
                                        <label for="<%= ckbNotificationProPhoneDRI.ClientID %>" class="csscheckbox-label">par téléphone</label>
                                    </div>
                                </div>
                                <div id="divNotificationProEmailDRI" class="hidden" style="padding-left:10px">
                                    <asp:AmlComment ID="amlMacroProDRI" runat="server" MaxLength="-1" Height="300" Label="Contenu e-mail" />
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="panelDOFResponseNotSatisfactory" runat="server" CssClass="hidden" style="margin-top:10px">
                        <asp:AmlComment ID="AmlCommentResponseNotSatisfactory" runat="server" MaxLength="400" />
                    </asp:Panel>
                    <asp:Panel ID="panelDOFResponseKO" runat="server" CssClass="hidden" style="margin-top:15px;">
                        
                        <div style="font-size:1.1em">
                            Voulez-vous déclarer ?
                        </div>
                        <asp:RadioButtonList ID="rblDeclarable" runat="server" CssClass="radioButtonList" RepeatDirection="Horizontal" style="margin:auto">
                            <asp:ListItem Value="1">Oui maintenant</asp:ListItem>
                            <asp:ListItem Value="2">Pas maintenant</asp:ListItem>
                            <asp:ListItem Value="3">Non</asp:ListItem>
                        </asp:RadioButtonList>
                        <asp:Panel ID="panelDeclarable3" runat="server" style="margin-top:20px">
                            <input type="checkbox" id="ckbAskRIB" runat="server" class="csscheckbox" />
                            <label for="<%= ckbAskRIB.ClientID %>" class="csscheckbox-label">Demander un RIB</label>
                            <div style="margin-top:15px">
                                En validant, vous serez redirigé vers l'écran de clôture du compte.
                                <br />
                                Si une demande de RIB est faite, la réponse du client fera automatiquement remonter l'alerte dans la liste.
                            </div>
                        </asp:Panel>
                    </asp:Panel>
                    <asp:Panel ID="panelDOFResponseValidation" runat="server" CssClass="hidden" style="margin-top:20px;width:100%; text-align:right">
                        <asp:UpdatePanel ID="upResponseValidation" runat="server">
                            <ContentTemplate>
                                <asp:Button ID="btnDOFResponseValidation" runat="server" CssClass="button" Text="Valider" OnClick="btnDOFResponseValidation_Click" style="width:100%" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </asp:Panel>
                </div>
                <%--<asp:Button ID="btnDOFResponseChange" runat="server" Text="OK" OnClick="btnDOFResponseChange_Click" Style="display: none" />--%>
            </ContentTemplate>
            <%--<Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnDOFResponseChange" EventName="Click" />
            </Triggers>--%>
        </asp:UpdatePanel>
    </div>
    
    <div id="dialog-DREKO-declarable" style="display:none;text-transform:uppercase;padding-top:20px">
        <asp:RadioButtonList ID="rblDeclarableDREKO" runat="server" CssClass="radioButtonList" RepeatDirection="Horizontal" style="margin:auto">
            <asp:ListItem Value="1">Oui maintenant</asp:ListItem>
            <asp:ListItem Value="2">Pas maintenant</asp:ListItem>
            <asp:ListItem Value="3">Non</asp:ListItem>
        </asp:RadioButtonList>
        <asp:Panel ID="panel1" runat="server" style="margin-top:20px;width:100%; text-align:right">
            <asp:UpdatePanel ID="upDREKODeclarable" runat="server">
                <ContentTemplate>
                    <asp:Button ID="btnSaveDREOK" runat="server" Text="Valider" CssClass="button" OnClick="btnSaveDREOK_Click" style="width:100%" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </div>

    <div id="dialog-alert" style="display: none;">
        <asp:Label ID="lblDialogAlertMessage" runat="server"></asp:Label>
    </div>

    <div id="popup-message">
        <span id="message"></span>
    </div>

    <script type="text/javascript">
        var panelToLoad = "";

        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            var panel = '';
            pbControl = args.get_postBackElement();
            //console.log(sender);
            //console.log(args);
            //console.log(pbControl.id);
            if (pbControl != null && pbControl.id != null) {
                if (pbControl.id.indexOf('<%=rblAlertStatus.ClientID %>') != -1 ||
                    pbControl.id == '<%=btnSaveDri.ClientID%>')
                    panel = '#divAlertTreatment';
                else if (pbControl.id == '<%=btnSaveDREOK.ClientID%>')
                {
                    if($('#dialog-DREKO-declarable').is(':visible'))
                        panel = '#dialog-DREKO-declarable';
                    else panel = '#divAlertTreatment';
                }
                else if (pbControl.id == '<%=btnDOFResponseValidation.ClientID%>')
                    panel = '#dialog-DOFResponse';
                else if (pbControl.id == '<%=btnRefreshMacro.ClientID%>')
                    panel = '#<%=panelDriAskClient.ClientID%>';
                <%--if (pbControl.id == '<=btnDOFResponseChange.ClientID %>')
                    panel = '#divDOFResponse'--%>
                //else if (pbControl.id == '<=btnLoadDashboard.ClientID%>')
                //panel = '#<=panelAgencyDashboard.ClientID %>';
                //console.log(panel);
                
            }
            else if(panelToLoad != null && panelToLoad.trim() != "")
                panel = '#'+panelToLoad;
            else panel = '#divAlertTreatment'

            if (panel.length > 0) {
                var width = $(panel).outerWidth();
                var height = $(panel).outerHeight();
                $('#ar-loading').css('width', width);
                $('#ar-loading').css('height', height);
                $('#ar-loading').show();
                $('#ar-loading').position({ my: "left top", at: "left top", of: panel });
            }
        }
        function EndRequestHandler(sender, args) {
            $('#ar-loading').hide();
            init();
        }
    </script>
</asp:Content>
