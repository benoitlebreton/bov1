﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ErreurCookies : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        bool bCookieSupported = true;

        if (!Request.Browser.Cookies)
            bCookieSupported = false;

        if (Request.Cookies["AspxAutoDetectCookieSupport"] == null)
            bCookieSupported = false;

        if (Request.Headers["Cookie"] == null || Request.Headers["Cookie"].ToString() == "")
            bCookieSupported = false;

        if (bCookieSupported)
            Response.Redirect("Default.aspx");
    }
}