﻿<%@ Page Language="C#" Title="Etat de suivi des alertes" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="StatusTrackingAccount.aspx.cs" Inherits="StatusTrackingAccount" %>
<%@ Register TagName="AmlComment" TagPrefix="asp" Src="~/API/AmlComment.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link href="Styles/jquery.stepProgressBar.css" rel="Stylesheet" type="text/css"/>
    <script type="text/javascript">
        $(document).ready(function () {
            InitControls();
        });

        function InitControls() {
            initProgressBarList();
            $('.alert-tabs').tabs({
                activate: function (event, ui) {
                    var tabIndex = $(".alert-tabs").tabs("option", "active");
                    if (tabIndex == 1 || tabIndex == 2 || tabIndex == 3)
                    {
                        $('#<%=hdnActiveTab.ClientID%>').val(tabIndex);
                        $('#<%=btnRefreshTracfinStatement.ClientID%>').click();
                    }
                    else if (tabIndex == 4)
                    {
                        $('#<%=hdnActiveTab.ClientID%>').val(tabIndex);
                        $('#<%=btnRefreshAccountClose.ClientID%>').click();
                    }
                }
            });

            showTab($('#<%=hdnForceShowTab.ClientID%>').val());
        }

        function showTab(forceTabId) {
            if(forceTabId.trim().length > 0 && $('#'+forceTabId) != null) {
                try {
                    $('.alert-tabs a[href="#'+forceTabId+'"]').click();
                } catch(ex){console.log(ex.toString());}
            }
            else if ($('#<%=hdnActiveTab.ClientID%>').val().trim().length > 0) {
                $(".alert-tabs").tabs("option", "active", $('#<%=hdnActiveTab.ClientID%>').val().trim());
            }
        }

        function initProgressBarList() {
            $('.AlertProgressBar').each(function (index, element) {
                //console.log($(element).attr('id'));
                initProgressBar($(element).attr('id'));
            });

            $('#<%=ddlBOUsers.ClientID%>').combobox();

            $('.image-tooltip').tooltip({
                position: {
                    my: "center bottom-10",
                    at: "center top",
                    using: function (position, feedback) {
                        $(this).css(position);
                        $("<div>")
                            .addClass("arrow")
                            .addClass(feedback.vertical)
                            .addClass(feedback.horizontal)
                            .appendTo(this);
                    }
                }
            });
        }

        function initProgressBar(idProgressBar) {
            /*[{ value: 0, bottomLabel: '' },{ value: 7, bottomLabel: 'Relance' },{ value: 24, bottomLabel: 'Retour client' },{ value: 100 }]*/
            var stepsValues = []; //{ value: 0, topLabel: 'J', bottomLabel: '' }
            var stepValues = $('#' + idProgressBar).attr('stepsValues').split('|');
            var currentStep = $('#' + idProgressBar).attr('current');
            //console.log(stepValues.length);
            for (var i = 0; i < stepValues.length; i++) {
                //console.log(stepValues[i]);
                //console.log(stepValues[i].split(';')[0]);
                //console.log(stepValues[i].split(';')[1]);

                var topLabel = 'J';
                if (stepValues[i].split(';')[0] != 0)
                {
                    topLabel += ((stepValues[i].split(';')[0] > 0) ? "+" : "") + stepValues[i].split(';')[0];
                }
                stepsValues.push(
                    {
                        value: parseInt(stepValues[i].split(';')[0]),
                        topLabel: topLabel,
                        bottomLabel: stepValues[i].split(';')[2]
                    });
            }
            
            var currentStepLabel = currentStep;
            if (currentStep > 0)
                currentStepLabel = '+' + currentStep;
            else currentStepLabel = '';
            stepsValues.push({ value: parseInt(currentStep), topLabel: 'J' + currentStepLabel, bottomLabel: '' });
            stepsValues.push({ value: 30, topLabel: 'J+30', bottomLabel: '' });

            $('#' + idProgressBar).stepProgressBar({
                currentValue: currentStep,
                steps: stepsValues,
                unit: 'j'
            });
        }
        function showAlertDetails(detailsLnk, idAlert) {
            $('.AlertProgressDetails').each(function (index, element) {
                if ($(element).is(':visible')) {
                    $(element).slideUp(function () { $(element).prev().children('a').html('Afficher détails'); });
                }
            });

            if ($('#' + idAlert).is(':visible')) {
                $('#' + idAlert).slideUp(function () { $(detailsLnk).html('Afficher détails'); });
            }
            else {
                $('#' + idAlert).slideDown(function () { $(detailsLnk).html('Masquer détails'); });
            }
        }

        function ShowChangeAttributionDialog(refAlert) {
            //console.log(refAlert);
            $('#<%=hdnRefAlert.ClientID%>').val(refAlert);
            $('#change-alert-attribution-dialog').dialog({
                autoOpen: true,
                modal: true,
                draggable: false,
                resizable: false,
                title: 'Réattribuer à',
                buttons: {
                    'Annuler': function(){$(this).dialog('close').dialog('destroy');},
                    'Ok': function(){$('#<%=btnChangeAlertAttribution.ClientID%>').click();$(this).dialog('close').dialog('destroy');}
                }
            });
            $("#change-alert-attribution-dialog").parent().appendTo(jQuery("form:first"));
        }

        $.widget( "ui.combobox", {
          _create: function() {
            var self = this;
            var select = this.element.hide(),
              selected = select.children( ":selected" ),
              value = selected.val() ? selected.text() : "";
            var input = $( "<input />" )
              .insertAfter(select)
              .val( value )
              .autocomplete({
                delay: 0,
                minLength: 0,
                source: function(request, response) {
                  var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
                  response( select.children("option" ).map(function() {
                    var text = $( this ).text();
                    if ( this.value && ( !request.term || matcher.test(text) ) )
                      return {
                        label: text.replace(
                          new RegExp(
                            "(?![^&;]+;)(?!<[^<>]*)(" +
                            $.ui.autocomplete.escapeRegex(request.term) +
                            ")(?![^<>]*>)(?![^&;]+;)", "gi"),
                          "<strong>$1</strong>"),
                        value: text,
                        option: this
                      };
                  }) );
                },
                select: function( event, ui ) {
                  ui.item.option.selected = true;
                  self._trigger( "selected", event, {
                    item: ui.item.option
                  });
                },
                change: function(event, ui) {
                  if ( !ui.item ) {
                    var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( $(this).val() ) + "$", "i" ),
                    valid = false;
                    select.children( "option" ).each(function() {
                      if ( this.value.match( matcher ) ) {
                        this.selected = valid = true;
                        return false;
                      }
                    });
                    if ( !valid ) {
                      // remove invalid value, as it didn't match anything
                      $( this ).val( "" );
                      select.val( "" );
                      return false;
                    }
                  }
                }
              })
              .addClass("ui-widget ui-widget-content ui-corner-left");
   
            input.data( "autocomplete" )._renderItem = function( ul, item ) {
              return $( "<li></li>" )
                .data( "item.autocomplete", item )
                .append( "<a>" + item.label + "</a>" )
                .appendTo( ul );
            };
   
            $( "<a> </a>" )
            .attr( "tabIndex", -1 )
            .attr( "title", "Show All Items" )
            .insertAfter( input )
            .button({
              icons: {
                primary: "ui-icon-triangle-1-s"
              },
              text: false
            })
            .removeClass( "ui-corner-all" )
            .addClass( "ui-corner-right ui-button-icon" )
            .click(function() {
              // close if already visible
              if (input.autocomplete("widget").is(":visible")) {
                input.autocomplete("close");
                return;
              }
              // pass empty string as value to search for, displaying all results
              input.autocomplete("search", "");
              input.focus();
            });
          }
        });

        function ShowAccountCloseDialog(refCustomer, type, DRICounter)
        {
            //console.log(DRICounter);
            //console.log(DRICounter > 0);
            if (DRICounter > 0)
            {
                $('#<%=rblDRKO.ClientID%> td:eq(0)').hide();
                $('#<%=rblDRKO.ClientID%> td:eq(1)').show();
            }
            else
            {
                $('#<%=rblDRKO.ClientID%> td:eq(0)').show();
                $('#<%=rblDRKO.ClientID%> td:eq(1)').hide();
            }

            $('.radioButtonList').buttonset();
            $('#<%=hdnRefCustomerToClose.ClientID%>').val(refCustomer);
            $('#<%=hdnTypeToClose.ClientID%>').val(type);

            $('#close-account-pre-dialog').dialog({
                autoOpen: true,
                modal: true,
                draggable: false,
                resizable: false,
                title: 'Ajouter une spécificité',
                buttons: {
                    'Annuler': function () {
                        $('.radioButtonList input[type=radio]').attr('checked', false);
                        $(this).dialog('close').dialog('destroy');
                    },
                    'Ok': function(){$('#<%=btnGoToCloseAccount.ClientID%>').click();$(this).dialog('close').dialog('destroy');}
                }
            });
            $("#close-account-pre-dialog").parent().appendTo(jQuery("form:first"));
        }
    </script>

    <style type="text/css">
        .tableHistory {
            width:100%;
            background-color:#fff;
            border-spacing:0;
        }
        .tableHistory tr td {
            position:relative;
            vertical-align:middle;
        }
        .tableHistory tr td i {
            position:absolute;
            top:8px;
        }
        .tableHistory tr td span {
            margin-left:40px;
        }

        .div-notification {
            position:absolute;
            top:-4px;
            right:0;
            text-transform:uppercase;
            color:#f00;
        }
        .div-notification i {
            font-size:1.2em;
            position:relative;
            top:2px;
        }

        .div-blocked {
            color:#f00;
            
        }


        #change-alert-attribution-dialog {
            text-align: center;
        }
        #change-alert-attribution-dialog .ui-button-icon-only .ui-button-text, .ui-button-icons-only .ui-button-text {
            padding:0;
        }

        #ar-loading {
            background: url('Styles/img/loading2.gif') no-repeat center;
            background-size: 35px 27px;
            display: none;
            position: absolute;
            background-color: rgba(255, 255, 255, 0.5);
            z-index: 100000;
        }

        .ui-tabs .ui-tabs-panel {
            padding:unset;
            font-size: 0.8em;
            font-family: arial;
        }

        .defaultTable2 td {
            vertical-align:middle;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <h2 style="color: #344b56;text-transform:uppercase;">Etat de suivi des alertes <asp:Label ID="lblAgent" runat="server"></asp:Label></h2>
    
    <asp:HiddenField ID="hdnActiveTab" runat="server" />
    <asp:HiddenField ID="hdnForceShowTab" runat="server" />
    <div class="alert-tabs" style="margin:10px 0 20px 0;">
        <ul>
            <li><a href="#alert-progress-tab">Investigation</a></li>
            <li><a href="#tracfin-tab">Déclaration</a></li>
            <li><a href="#tracfin-tab">Validation / Approbation</a></li>
            <li><a href="#tracfin-tab">Saisie</a></li>
            <li><a href="#account-close-tab">Clôture</a></li>
        </ul>

        <div id="alert-progress-tab">
            <div id="div-alert-progress">
                <asp:UpdatePanel ID="upAlertProgress" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>

                        <%--<input type="checkbox" id="ckbViewAll" runat="server" class="csscheckbox" />
                        <label for="<%= ckbViewAll.ClientID %>" class="csscheckbox-label">Global</label>--%>
                        <asp:Repeater ID="rptAlertProgressList" runat="server" OnItemDataBound="rptAlertProgressList_ItemDataBound">
                            <HeaderTemplate>
                                <div style="border:2px solid">
                            </HeaderTemplate>
                            <ItemTemplate>
                                <div style="padding:5px;border-bottom:1px solid #808080" class='<%# Container.ItemIndex % 2 == 0 ? "AlertProgress-Row" : "AlertProgress-AlternateRow" %>'>
                                    <div style="margin:5px 0 10px 5px; font-weight:bold;position:relative">
                                        <asp:Label ID="lblClientName" runat="server"><%# Eval("CustomerName") %></asp:Label>
                                        -
                                        <asp:Label ID="lblAlertLabel" runat="server"><%# Eval("AlertLabel") %></asp:Label>

                                        <asp:Panel ID="panelNotification" runat="server" Visible='<%#(Eval("Notification").ToString() == "1") ? true : false %>'
                                            CssClass="div-notification">
                                            <%# Eval("NotificationLabel").ToString() %><%# Eval("NotificationIcon").ToString() %>

                                            <asp:ImageButton ID="btnRemoveNotification" runat="server" ImageUrl="~/Styles/Img/remove_notification.png" Height="20px" ToolTip="Supprimer la notification" CommandArgument='<%#Eval("RefAlert").ToString() %>' OnClick="btnRemoveNotification_Click" CssClass="image-tooltip" style="float:right" />
                                        </asp:Panel>

                                        <asp:Panel ID="panelBlocked" runat="server" Visible='<%#(Eval("BlockedLabel").ToString() != "") ? true : false %>' CssClass="div-blocked" >
                                            <asp:Image ID="imgClientDebitStatus" runat="server" ImageUrl="~/Styles/Img/blocked.png" ToolTip=' <%# string.Format("Bloqué depuis le {0}",Eval("BlockedDate")) %>' style="left:-4px;height:20px;max-width:40px;float:left"  />
                                            <asp:Label ID="lblBlockedLabel" runat="server"  Height="20px" >&nbsp;<%#Eval("BlockedLabel") %></asp:Label>
                                        </asp:Panel>
                                    </div>
                                    <asp:Panel ID="AlertProgressBar" runat="server" current='<%# Eval("CurrentStep") %>' stepsValues='<%# Eval("StepMarkers") %>' class='<%# "AlertProgressBar " + Eval("StatusClass").ToString() %>'></asp:Panel>
                                    <div style="margin:10px 0 5px 5px;text-align:right;position:relative">
                                        <div style="float:right">
                                            <input type="button" value="Réattribuer l'alerte" class="MiniButton" onclick='<%#String.Format("ShowChangeAttributionDialog({0});", Eval("RefAlert")) %>' />
                                            <asp:Button ID="hlGoToFastAnalysisSheet" runat="server" CssClass="MiniButton" Text="Accéder à la fiche d'analyse" OnClick="hlGoToFastAnalysisSheet_Click" CommandArgument='<%# String.Format("FastAnalysisSheet.aspx?ref={0}&client={1}", Eval("RefAlert"), Eval("RefCustomer")) %>' style="margin: 0 10px" />
                                            <asp:Button ID="hlGoToAlert" runat="server" CssClass="MiniButton" Text="Accéder à la fiche de traitement" OnClick="hlGoToAlert_Click" CommandArgument='<%# String.Format("AlertTreatmentSheet.aspx?ref={0}&client={1}", Eval("RefAlert"), Eval("RefCustomer")) %>' style="margin-right:10px" />
                                        </div>
                                        <div style="float:left">
                                            <asp:HyperLink ID="hlShowAlertProgressDetails" runat="server" href="#details">Afficher détails</asp:HyperLink>
                                        </div>
                                        <div style="clear:both"></div>
                                    </div>
                                    <asp:Panel ID="AlertProgressDetails" runat="server" style="display:none" CssClass="AlertProgressDetails">
                                        <asp:Repeater ID="rptAlertProgressDetails" runat="server" DataSource='<%#Eval("Steps") %>'>
                                            <HeaderTemplate>
                                                <table class="tableHistory">
                                                    <tr>
                                                        <th>Date</th>
                                                        <th>Détail</th>
                                                        <th>Utilisateur</th>
                                                    </tr>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                    <tr>
                                                        <td style="width:1px; white-space:nowrap"><%# Eval("Date") %></td>
                                                        <td style="padding-left:20px"><%# Eval("Icon") %>&nbsp;<span><%# Eval("Label") %></span></td>
                                                        <td><%# Eval("BoUser") %></td>
                                                    </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </asp:Panel>
                                </div>
                            </ItemTemplate>
                            <FooterTemplate>
                                </div>
                            </FooterTemplate>
                        </asp:Repeater>

                        <asp:Panel ID="panelEmpty" runat="server" Visible="false" HorizontalAlign="Center" style="padding:10px">
                            Aucune alerte en cours
                        </asp:Panel>

                        <div id="change-alert-attribution-dialog" style="display:none">
                            <asp:UpdatePanel ID="upChangeAlertAttribution" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList ID="ddlBOUsers" runat="server" AppendDataBoundItems="true" DataValueField="Id" DataTextField="CompleteName">
                                        <asp:ListItem Value="" Text=""></asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:HiddenField ID="hdnRefAlert" runat="server" />
                                    <asp:Button ID="btnChangeAlertAttribution" runat="server" OnClick="btnChangeAlertAttribution_Click" style="display:none" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>

                    </ContentTemplate>
                    <%--<Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ckbViewAll" />
                    </Triggers>--%>
                </asp:UpdatePanel>
            </div>
        </div>
        <div id="tracfin-tab">
            <asp:UpdatePanel ID="upTracfinStatement" runat="server">
                <ContentTemplate>
                    <asp:Button ID="btnRefreshTracfinStatement" runat="server" OnClick="btnRefreshTracfinStatement_Click" style="display:none" />
                    <asp:Panel ID="panelNoStatement" runat="server" Visible="false" style="padding:10px;text-align:center">
                        Aucune déclaration
                    </asp:Panel>
                    <asp:Repeater ID="rptTracfinStatement" runat="server">
                        <HeaderTemplate>
                            <table class="defaultTable2">
                                <thead>
                                <tr>
                                    <th style="width:120px">
                                        N° de compte
                                    </th>
                                    <th>
                                        Prénom
                                    </th>
                                    <th>
                                        Nom
                                    </th>
                                    <th>
                                        Etat
                                    </th>
                                    <th style="width:80px">
                                        jours depuis DRI/DRE
                                    </th>
                                    <th>
                                        Porteur
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                                <tr class='row-over-color  <%# Container.ItemIndex % 2 == 0 ? "row-even" : "row-odd" %>' onclick='<%# String.Format("document.location.href=\"TracfinStatement.aspx?ref={0}\"", Eval("RefStatement")) %>' style='<%# GetDSLineColor(Eval("NbDaysSinceDR").ToString(), Eval("AlertTAG").ToString()) %>'>
                                    <td style="text-align:center"><%#Eval("AccountNumber") %></td>
                                    <td><%#Eval("FirstName") %></td>
                                    <td><%#Eval("LastName") %></td>
                                    <td><%#Eval("LastAction") %></td>
                                    <td style="text-align:center"><%#Eval("NbDaysSinceDR") %></td>
                                    <td><%#Eval("Porteur") %></td>
                                </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                                </tbody>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div id="account-close-tab">
            <asp:UpdatePanel ID="upAccountClose" runat="server">
                <ContentTemplate>
                    <asp:Button ID="btnRefreshAccountClose" runat="server" OnClick="btnRefreshAccountClose_Click" style="display:none" />
                    <asp:Panel ID="panelNoAccountClose" runat="server" Visible="false" style="padding:10px;text-align:center">
                        Aucun compte à clôturer
                    </asp:Panel>
                    <asp:Repeater ID="rptAccountClose" runat="server">
                        <HeaderTemplate>
                            <table class="defaultTable2">
                                <thead>
                                <tr>
                                    <th style="width:160px">
                                        Référence
                                    </th>
                                    <th style="width:160px">
                                        N° de compte
                                    </th>
                                    <th>
                                        Prénom
                                    </th>
                                    <th>
                                        Nom
                                    </th>
                                    <th>
                                        Date
                                    </th>
                                    <th>
                                        Type
                                    </th>
                                    <th>
                                        Porteur
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                                <tr class='row-over-color  <%#Container.ItemIndex % 2 == 0 ? "row-even" : "row-odd" %>' onclick='<%# String.Format((int.Parse(Eval("DRIKOCounter").ToString()) > 0) ? "StatusTrackingLoading(\"#account-close-tab\");document.location.href=\"AccountClosing.aspx?ref={0}&type={1}\";" : "ShowAccountCloseDialog({0},{1},{2});", Eval("RefCustomer"), Eval("RefCloseAction"),Eval("DRICounter")) %>'>
                                    <td><%#Eval("RefStatementInt") %></td>
                                    <td style="text-align:center"><%#Eval("BankAccountNumber") %></td>
                                    <td><%#Eval("FirstName") %></td>
                                    <td><%#Eval("LastName") %></td>
                                    <td style="text-align:center"><%#Eval("DateApproval") %></td>
                                    <td><%#GetCloseActionLabel(Eval("RefCloseAction").ToString()) %></td>
                                    <td><%# String.Format("{0} {1}", Eval("BoUserFirstName"), Eval("BoUserLastName")) %></td>
                                </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                                </tbody>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </ContentTemplate>
            </asp:UpdatePanel>

            <div id="close-account-pre-dialog" style="display:none">
                <asp:UpdatePanel ID="upCloseAccount" runat="server">
                    <ContentTemplate>
                        <div class="label">
                            Spécificité
                        </div>
                        <div>
                            <asp:RadioButtonList ID="rblDRKO" runat="server" RepeatDirection="Horizontal" CssClass="radioButtonList">
                                <asp:ListItem Text="DREKO" Value="55"></asp:ListItem>
                                <asp:ListItem Text="DRIKO" Value="37"></asp:ListItem>
                                <asp:ListItem Text="Aucune" Value=""></asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                        <div>
                            <asp:AmlComment ID="amlComment1" runat="server" Label="Commentaire" Height="80" MaxLength="400" />
                        </div>
                        
                        <asp:HiddenField ID="hdnRefCustomerToClose" runat="server" />
                        <asp:HiddenField ID="hdnTypeToClose" runat="server" />
                        <asp:Button ID="btnGoToCloseAccount" runat="server" OnClick="btnGoToCloseAccount_Click" style="display:none" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>

    <div id="ar-loading"></div>

    <script type="text/javascript" src="Scripts/jquery.stepProgressBar.js"></script>

    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            pbControl = args.get_postBackElement();
            //console.log(pbControl.id);
            var containerID = '';
            if (pbControl != null) {
                if (pbControl.id.indexOf('btnChangeAlertAttribution') != -1)
                    containerID = '#div-alert-progress';
                else if (pbControl.id.indexOf('btnRefreshTracfinStatement') != -1)
                    containerID = '#tracfin-tab';
                else if (pbControl.id.indexOf('btnRefreshAccountClose') != -1
                    || pbControl.id.indexOf('btnGoToCloseAccount') != -1)
                    containerID = '#account-close-tab';
            }

            StatusTrackingLoading(containerID);
        }
        function StatusTrackingLoading(containerID)
        {
            if (containerID.length > 0) {
                var width = $(containerID).outerWidth();
                var height = $(containerID).outerHeight();
                $('#ar-loading').css('width', width);
                $('#ar-loading').css('height', height);
                $('#ar-loading').show();
                $('#ar-loading').position({ my: "left top", at: "left top", of: containerID });
            }
        }
        function EndRequestHandler(sender, args) {
            $('#ar-loading').hide();
        }
    </script>
</asp:Content>