﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using XMLMethodLibrary;

public partial class AlertTreatmentSheet : System.Web.UI.Page
{
    private bool _bTreated;
    private int _iNbDaysSinceAskClient;
    private string _sOriginDate;
    private bool _bNewZendeskResponse;
    private List<string> _listNewZendeskResponseFrom;
    private List<string> _listNewZendeskResponseTicketID;
    //private DataTable _dtAmlCommentCounterpart { get { return (Session["dtAmlCommentCounterpart"] != null) ? (DataTable)Session["dtAmlCommentCounterpart"] : new DataTable(); } set { Session["dtAmlCommentCounterpart"] = value; } }
    private DataTable _dtAmlCommentDRI { get { return (Session["dtAmlCommentDRI"] != null) ? (DataTable)Session["dtAmlCommentDRI"] : new DataTable(); } set { Session["dtAmlCommentDRI"] = value; } }

    protected int getCurrentRefCustomer()
    {
        int iRefCustomer = 0;

        if (!(Request.Params["client"] != null && int.TryParse(Request.Params["client"], out iRefCustomer)))
            if (!(ViewState["client"] != null && int.TryParse(ViewState["client"].ToString(), out iRefCustomer)))
                Response.Redirect("SearchAlert.aspx", true);

        return iRefCustomer;
    }
    protected int getCurrentRefAlert()
    {
        int iRefAlert = 0;

        if (!(Request.Params["ref"] != null && int.TryParse(Request.Params["ref"], out iRefAlert)))
            if (!(ViewState["ref"] != null && int.TryParse(ViewState["ref"].ToString(), out iRefAlert)))
                Response.Redirect("SearchAlert.aspx", true);

        return iRefAlert;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            int iRefAlert = getCurrentRefAlert();
            ViewState["ref"] = iRefAlert;
            int iRefCustomer = getCurrentRefCustomer();
            ViewState["client"] = iRefCustomer;

            btnPrev.PostBackUrl = "FastAnalysisSheet.aspx?ref=" + iRefAlert.ToString() + "&client=" + iRefCustomer.ToString();
            ////txtDreOkComment.Attributes.Add("onKeyUp", "javascript:MaxLength(this, 'lblNbCharCheckDreOkComment','" + lblNbMaxCheckDreOkComment.ClientID+"');");
            ////txtDreOkComment.Attributes.Add("onChange", "javascript:MaxLength(this, 'lblNbCharCheckDreOkComment','" + lblNbMaxCheckDreOkComment.ClientID + "');");
            //txtDriAskConsiderationComment.Attributes.Add("onKeyUp", "javascript:MaxLength(this, 'lblNbCharCheckDriAskConsiderationComment','" + lblNbMaxCheckDriAskConsiderationComment.ClientID + "');");
            //txtDriAskConsiderationComment.Attributes.Add("onChange", "javascript:MaxLength(this, 'lblNbCharCheckDriAskConsiderationComment','" + lblNbMaxCheckDriAskConsiderationComment.ClientID + "');");

            BindOtherMacro("client");
            BindOtherMacro("counterpart");

            BindAlertInfos();

            checkPreviousButtonUrl();

            if(!_bTreated)
                CheckDOFResponse();

            ReturnFunds1.InitMask();

            LightLock1.RefCustomer = iRefCustomer;
            proNotificationCounter1.RefCustomer = iRefCustomer;
            SetCustomerInfos(iRefCustomer);
        }
    }

    protected void SetCustomerInfos(int iRefCustomer)
    {
        string sXmlCustomerInfos = Client.GetCustomerInformations(iRefCustomer);

        if (sXmlCustomerInfos.Length > 0)
        {
            LightLock1.RefreshClientDebitStatus(sXmlCustomerInfos);

            List<string> listCivility = CommonMethod.GetAttributeValues(sXmlCustomerInfos, "ALL_XML_OUT/Customer", "Politeness");
            List<string> listLastName = CommonMethod.GetAttributeValues(sXmlCustomerInfos, "ALL_XML_OUT/Customer", "LastName");
            List<string> listFirstName = CommonMethod.GetAttributeValues(sXmlCustomerInfos, "ALL_XML_OUT/Customer", "FirstName");
            List<string> listAccountNumber = CommonMethod.GetAttributeValues(sXmlCustomerInfos, "ALL_XML_OUT/Customer", "AccountNumber");
            List<string> listWebIdentifier = CommonMethod.GetAttributeValues(sXmlCustomerInfos, "ALL_XML_OUT/Customer", "WebIdentifier");

            List<string> listLockCustomerDebit = CommonMethod.GetAttributeValues(sXmlCustomerInfos, "ALL_XML_OUT/Customer", "LockCustomerDebit");
            List<string> listLockCustomerDebitRefReason = CommonMethod.GetAttributeValues(sXmlCustomerInfos, "ALL_XML_OUT/Customer", "LockCustomerDebitRefReason");
            List<string> listLockList = CommonMethod.GetTags(sXmlCustomerInfos, "ALL_XML_OUT/Customer/LOCKLIST/LOCK");

            lblClientCardNumber.Text = (listWebIdentifier.Count > 0) ? listWebIdentifier[0] : "";
            lblClientAccountNumber.Text = listAccountNumber[0];
            lblClientName.Text = listCivility[0] + " " + listFirstName[0] + " " + listLastName[0];
        }
    }

    protected string GetAlertTreatment(string sRefAlert, string sToken)
    {
        SqlConnection conn = null;
        string sXmlOut = "";

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("AML.P_GetAlertTreatment", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                            new XElement("ALERT_TREATMENT",
                                                new XAttribute("RefAlert", sRefAlert),
                                                new XAttribute("CashierToken", sToken))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            sXmlOut = cmd.Parameters["@OUT"].Value.ToString();
        }
        catch (Exception e)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return sXmlOut;
    }

    protected void BindAlertInfos()
    {
        int iRefAlert = getCurrentRefAlert();
        int iRefCustomer = getCurrentRefCustomer();

        //string sXMLClientInfos = Client.GetCustomerInformations(iRefCustomer);
        //if(sXMLClientInfos.Length > 0)
        //{
        //    List<string> listCivility = CommonMethod.GetAttributeValues(sXMLClientInfos, "ALL_XML_OUT/Customer", "Politeness");
        //    List<string> listLastName = CommonMethod.GetAttributeValues(sXMLClientInfos, "ALL_XML_OUT/Customer", "LastName");
        //    List<string> listFirstName = CommonMethod.GetAttributeValues(sXMLClientInfos, "ALL_XML_OUT/Customer", "FirstName");
        //    List<string> listAccountNumber = CommonMethod.GetAttributeValues(sXMLClientInfos, "ALL_XML_OUT/Customer", "AccountNumber");

        //    txtClientNickelName.Text = listCivility[0] + " " + listFirstName[0] + " " + listLastName[0];
        //    txtNickelAccountNumber.Text = listAccountNumber[0];
        //}

        string sXmlOut = GetAlertTreatment(iRefAlert.ToString(), authentication.GetCurrent().sToken);

        if (sXmlOut.Length > 0)
        {
            List<string> listTreated = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/ALERT_TREATMENT", "Treated");
            List<string> listBOUser = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/ALERT_TREATMENT", "BOUser");
            List<string> listBOUserAttributionDate = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/ALERT_TREATMENT", "BOUserAttributionDate");
            List<string> listAlertDate = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/ALERT_TREATMENT", "AlertDate");
            List<string> listAlertTAG = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/ALERT_TREATMENT", "AlertTAG");

            List<string> listProfil = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/ALERT_TREATMENT", "Profil");
            List<string> listStatus = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/ALERT_TREATMENT", "Status");
            List<string> listNotes = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/ALERT_TREATMENT/NOTES/NOTE");

            List<string> listNoContact = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/ALERT_TREATMENT", "NoContact");

            List<string> listContactClient = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/ALERT_TREATMENT", "ContactClient");
            List<string> listDemandeOrigineFonds = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/ALERT_TREATMENT", "DemandeOrigineFonds");
            List<string> listDemandeInfoCptBloque = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/ALERT_TREATMENT", "DemandeInfoCptBloque");
            List<string> listAutoEntrSepActivite = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/ALERT_TREATMENT", "AutoEntrSepActivite");
            List<string> listClientCall = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/ALERT_TREATMENT", "ClientCall");
            List<string> listClientCallDuration = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/ALERT_TREATMENT", "ClientCallDuration");
            List<string> listClientNoAnswer = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/ALERT_TREATMENT", "ClientNoAnswer");

            List<string> listContactCounterpart = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/ALERT_TREATMENT", "ContactCounterpart");
            List<string> listCounterpartNoAnwser = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/ALERT_TREATMENT", "CounterpartNoAnwser");
            List<string> listUnfreezeAccount = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/ALERT_TREATMENT", "UnfreezeAccount");
            List<string> listContactCounterpartAgain = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/ALERT_TREATMENT", "ContactCounterpartAgain");
            List<string> listCallCounterpart = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/ALERT_TREATMENT", "CallCounterpart");
            List<string> listCallCounterpartAnswerOk = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/ALERT_TREATMENT", "CallCounterpartAnswerOk");
            List<string> listCallCounterpartAnswerPending = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/ALERT_TREATMENT", "CallCounterpartAnswerPending");

            List<string> listCounterpartAnswer = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/ALERT_TREATMENT", "CounterpartAnswer");
            List<string> listResponseStatus = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/ALERT_TREATMENT", "ResponseStatus");

            List<string> listNewZendeskResponse = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/ALERT_TREATMENT", "NewZendeskResponseReceived");
            List<string> listNewZendeskResponseFrom = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/ALERT_TREATMENT", "NewZendeskResponseFrom");
            List<string> listNewZendeskResponseTicketID = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/ALERT_TREATMENT", "NewZendeskResponseTicketID");

            List<string> listWatch = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/ALERT_TREATMENT/AML_ALERTS", "Watch");
            List<string> listLockTransfer = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/ALERT_TREATMENT/TRANSFER", "LockTransfer");
            List<string> listPendingTransfer = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/ALERT_TREATMENT/TRANSFER", "PendingTransfer");
            List<string> listPendingTransferAmount = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/ALERT_TREATMENT/TRANSFER", "PendingTransferAmount");

            List<string> listMacro = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/ALERT_TREATMENT/MACROS/MACRO");
            List<string> listEvent = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/ALERT_TREATMENT/Events/Event");

            _bTreated = (listTreated.Count > 0 && listTreated[0] == "1") ? true : false;

            authentication auth = authentication.GetCurrent();

            // Cas DRIKO/DREKO mais clôture non effectuée
            if ((listStatus[0] == "DRIKO" || listStatus[0] == "DREKO") && !_bTreated)
                Response.Redirect("AccountClosing.aspx?ref=" + getCurrentRefCustomer() + "&alert=" + getCurrentRefAlert(), true);

            // Forcer attribution si aucun agent n'est responsable
            if (listBOUser.Count == 0 || String.IsNullOrWhiteSpace(listBOUser[0]))
            {
                XElement xml = new XElement("ALL_XML_IN",
                                new XElement("ALERT_TREATMENT",
                                    new XAttribute("CashierToken", auth.sToken),
                                    new XAttribute("RefAlert", iRefAlert.ToString()),
                                    new XAttribute("ReserveAlert", "1")
                                    ));
                if (!AML.SetAlertTreatment(xml.ToString()))
                    Response.Redirect(btnPrev.PostBackUrl, true);
                else
                {
                    lblBOAgent.Text = auth.sFirstName + " " + auth.sLastName;
                    _sOriginDate = DateTime.Now.ToString("dd/MM/yyyy");
                    lblBOAgentAttributionDate.Text = _sOriginDate;
                }
            }
            else
            {
                lblBOAgent.Text = listBOUser[0];
                _sOriginDate = listBOUserAttributionDate[0].Substring(0, 10);
                lblBOAgentAttributionDate.Text = listBOUserAttributionDate[0].Substring(0, 10);
            }
            lblNbDaysSinceAttribution.Text = "J";

            lblCreationDate.Text = listAlertDate[0].Substring(0, 10);
            string sNbDays = tools.GetNbDaysFromDate(listAlertDate[0].Substring(0, 10), _sOriginDate).ToString();
            int iNbDays = 0;
            if (int.TryParse(sNbDays, out iNbDays))
                if (iNbDays > 0)
                    sNbDays = "+" + sNbDays;
            if(iNbDays == 0)
                lblNbDaysSinceCreation.Text = "J";
            else lblNbDaysSinceCreation.Text = "J" + sNbDays;

            string sProfile = (listProfil.Count > 0 && !String.IsNullOrWhiteSpace(listProfil[0])) ? listProfil[0] : "2C";
            lblClientProfile.Text = "Profil " + sProfile;
            amlProfileChangeDREOK.SelectedProfile = sProfile;

            string sStatus = listStatus[0];
            if (sStatus.Contains("DRI"))
                sStatus = "DRI";
            ListItem item = rblAlertStatus.Items.FindByValue(sStatus);
            if (item != null)
                item.Selected = true;

            if (_bTreated)
            {
                panelResponseDetails.CssClass = "";
                lblResponseStatus.Text = listResponseStatus[0];
            }

            switch (listStatus[0])
            {
                case "DRI":
                case "DRIOK":
                case "DRIKO":
                    cbDriAskClient.Checked = (listContactClient.Count > 0 && listContactClient[0] == "1") ? true : false;
                    ckbSourceFunds.Checked = (listDemandeOrigineFonds.Count > 0 && listDemandeOrigineFonds[0] == "1") ? true : false;
                    ckbAskInformationAccountBlocked.Checked = (listDemandeInfoCptBloque.Count > 0 && listDemandeInfoCptBloque[0] == "1") ? true : false;
                    ckbAutoEntrepreneur.Checked = (listAutoEntrSepActivite.Count > 0 && listAutoEntrSepActivite[0] == "1") ? true : false;
                    if (cbDriAskClient.Checked && !ckbSourceFunds.Checked && !ckbAskInformationAccountBlocked.Checked && !ckbAskInformationAccountBlocked.Checked)
                        ckbAutreMacro.Checked = true;
                    ckbClientCall.Checked = (listClientCall.Count > 0 && listClientCall[0] == "1") ? true : false;
                    ckbClientNoAnswer.Checked = (listClientNoAnswer.Count > 0 && listClientNoAnswer[0] == "1") ? true : false;
                    if (listClientCallDuration.Count > 0)
                    {
                        BindDurationList();
                        ddlClientCallDuration.Items.FindByValue(listClientCallDuration[0]).Selected = true;
                    }

                    cbDriAskConsideration.Checked = (listContactCounterpart.Count > 0 && listContactCounterpart[0] == "1") ? true : false;
                    ckbCounterpartAnswered.Checked = (listCounterpartAnswer.Count > 0 && listCounterpartAnswer[0] == "1") ? true : false;
                    ckbCounterpartNoAnswer.Checked = (listCounterpartNoAnwser.Count > 0 && listCounterpartNoAnwser[0] == "1") ? true : false;
                    ckbUnfreezeAccount.Checked = (listUnfreezeAccount.Count > 0 && listUnfreezeAccount[0] == "1") ? true : false;
                    ckbContactCounterpartAgain.Checked = (listContactCounterpartAgain.Count > 0 && listContactCounterpartAgain[0] == "1") ? true : false;
                    ckbCallCounterpart.Checked = (listCallCounterpart.Count > 0 && listCallCounterpart[0] == "1") ? true : false;
                    ckbCallCounterpartOk.Checked = (listCallCounterpartAnswerOk.Count > 0 && listCallCounterpartAnswerOk[0] == "1") ? true : false;
                    ckbCallCounterpartPending.Checked = (listCallCounterpartAnswerPending.Count > 0 && listCallCounterpartAnswerPending[0] == "1") ? true : false;

                    cbDriNoContact.Checked = (listNoContact.Count > 0 && listNoContact[0] == "1") ? true : false;
                    break;
                case "DRE":
                case "":
                    amlSurveillanceDREOK.BindAmlSurveillance(
                        iRefCustomer,
                        (listWatch.Count > 0 && listWatch[0] == "1") ? true : false,
                        (listLockTransfer.Count > 0 && listLockTransfer[0] == "1") ? true : false,
                        (listPendingTransfer.Count > 0 && listPendingTransfer[0] == "1") ? true : false,
                        (listPendingTransferAmount.Count > 0) ? listPendingTransferAmount[0] : "");
                    break;
            }

            //DataTable dtAmlCommentCounterpart = new DataTable();
            //dtAmlCommentCounterpart.Columns.Add("Note");
            DataTable dtAmlCommentDRI = new DataTable();
            dtAmlCommentDRI.Columns.Add("Note");
            for (int i = 0; i < listNotes.Count; i++)
            {
                List<string> listNote = CommonMethod.GetAttributeValues(listNotes[i], "NOTE", "Note");
                List<string> listType = CommonMethod.GetAttributeValues(listNotes[i], "NOTE", "Type");

                switch(listType[0])
                {
                    case "DRE":
                        amlCommentDREOK.Commentary = listNote[0];
                        break;
                    case "CPC":
                        if (listNote[0].Length > 0)
                        {
                            //dtAmlCommentCounterpart.Rows.Add(new object[] { listNote[0].Replace("\n", "<br/>") });
                            //amlCommentCounterpart.Commentary = listNote[0];
                            //amlCommentCounterpart.Disabled = true;
                        }
                        break;
                    case "CDI":
                        if(listNote[0].Length > 0)
                        {
                            dtAmlCommentDRI.Rows.Add(new object[] { listNote[0].Replace("\n", "<br/>") });
                        }
                        break;
                    case "RNS":
                        if(listNote[0].Length > 0)
                        {
                            panelResponseNotSatisfactoryCommentary.CssClass = "";
                            lblResponseNotSatisfactoryCommentary.Text = listNote[0];
                        }
                        break;
                    //case "NOC":
                    //    amlCommentNoContact.Commentary = listNote[0];
                    //    break;
                }
            }
            //if (dtAmlCommentCounterpart.Rows.Count > 0)
            //{
            //    panelPrevAmlCommentCounterpart.CssClass = "";
            //    rptAmlCommentCounterpart.DataSource = dtAmlCommentCounterpart;
            //    rptAmlCommentCounterpart.DataBind();
            //}
            //_dtAmlCommentCounterpart = dtAmlCommentCounterpart;
            if (dtAmlCommentDRI.Rows.Count > 0)
            {
                panelPrevAmlCommentDRI.CssClass = "";
                rptAmlCommentDRI.DataSource = dtAmlCommentDRI;
                rptAmlCommentDRI.DataBind();
            }
            _dtAmlCommentDRI = dtAmlCommentDRI;

            for (int i = 0; i < listMacro.Count; i++)
            {
                List<string> listTAG = CommonMethod.GetAttributeValues(listMacro[i], "MACRO", "TAG");
                List<string> listContent = CommonMethod.GetAttributeValues(listMacro[i], "MACRO", "Content");

                if (listContent.Count > 0 && !String.IsNullOrWhiteSpace(listContent[0]))
                {
                    Regex rgx = new Regex("\r\n?|\n");

                    if (listTAG[0] == "DOF" || listTAG[0] == "ICB" || listTAG[0] == "ASA")
                    {
                        amlMacroContent.Commentary = rgx.Replace(listContent[0], Environment.NewLine);
                        amlMacroContent.Disabled = true;
                    }
                    else if (listTAG[0] == "APF" || listTAG[0] == "APFR")
                    {
                        amlMacroContentCounterpart.Commentary = rgx.Replace(listContent[0], Environment.NewLine);
                        amlMacroContentCounterpart.Disabled = true;

                        List<string> listEmail = CommonMethod.GetAttributeValues(listMacro[i], "MACRO", "EmailToUse");
                        txtEmailContactCounterpart.Text = listEmail[0];
                        txtEmailContactCounterpart.ReadOnly = true;

                        ddlMacroCounterpart.Items.FindByValue(listTAG[0]).Selected = true;
                    }
                }
            }
            if(amlMacroContentCounterpart.Disabled == false)
            {
                amlMacroContentCounterpart.Commentary = AML.GetMacroAnswer(iRefAlert, "APF");
            }

            for (int i = 0; i < listEvent.Count; i++)
            {
                List<string> listTag = CommonMethod.GetAttributeValues(listEvent[i], "Event", "EventTAG");
                List<string> listDate = CommonMethod.GetAttributeValues(listEvent[i], "Event", "EventDate");

                string sDate = "";
                switch (listTag[0])
                {
                    case "FUNDSOURCE":
                    case "LOCKACCOUNTINFO":
                    case "SELFEMPLOYEDSPLIT":
                    case "MACROCLIENT":
                        sDate = listDate[0].Substring(0, 10);
                        lblAskClientDate.Text = sDate;
                        _iNbDaysSinceAskClient = tools.GetNbDaysFromDate(DateTime.Now.ToString("dd/MM/yyyy"), _sOriginDate);
                        if (_iNbDaysSinceAskClient == 0)
                            lblNbDaysSinceAskClient.Text = "J";
                        else if (_iNbDaysSinceAskClient > 0)
                            lblNbDaysSinceAskClient.Text = "J+" + _iNbDaysSinceAskClient;
                        else lblNbDaysSinceAskClient.Text = "J" + _iNbDaysSinceAskClient;
                        break;
                    case "COUNTERPART":
                        sDate = listDate[0].Substring(0, 10);
                        lblAskConsiderationDate.Text = sDate;
                        int iNbDaysSinceAskConsideration = tools.GetNbDaysFromDate(sDate, _sOriginDate);
                        if (iNbDaysSinceAskConsideration == 0)
                            lblNbDaysSinceAskConsideration.Text = "J";
                        else if (iNbDaysSinceAskConsideration > 0)
                            lblNbDaysSinceAskConsideration.Text = "J+" + iNbDaysSinceAskConsideration;
                        else lblNbDaysSinceAskConsideration.Text = "J" + iNbDaysSinceAskConsideration;
                        break;
                }
            }
            
            if (listNewZendeskResponse.Count > 0 && listNewZendeskResponse[0] == "1")
            {
                _bNewZendeskResponse = true;
                _listNewZendeskResponseFrom = listNewZendeskResponseFrom[0].Split(';').ToList();
                _listNewZendeskResponseTicketID = listNewZendeskResponseTicketID[0].Split(';').ToList();
            }
        }

        BindComponentsState(true);
    }

    protected void BindComponentsState(bool bDisableControls)
    {
        panelDREOK.CssClass = "hidden";
        panelDRI.CssClass = "hidden";

        if (bDisableControls && rblAlertStatus.SelectedValue.Length > 0)
            rblAlertStatus.Enabled = false;

        switch (rblAlertStatus.SelectedValue)
        {
            case "DRE":
                panelDREOK.CssClass = "";
                InitJsDREOK();
                amlCommentDREOK.BindCommentLengthCheck();
                amlMacroPro.BindCommentLengthCheck();
                if(String.IsNullOrWhiteSpace(amlMacroPro.Commentary))
                    amlMacroPro.Commentary = AML.GetMacroAnswer(getCurrentRefAlert(), "NPRO");
                AmlProfileChangeResponse.Visible = false;
                AmlSurveillanceReponse.Visible = false;
                amlProfileChangeDREOK.Visible = true;
                amlSurveillanceDREOK.Visible = true;

                if (ckbNotificationPro.Checked)
                    panelNotificationpro.CssClass = "";
                if (ckbNotificationProMail.Checked)
                    panelNotificationproemail.CssClass = "";
                break;
            case "DRI":
                panelDRI.CssClass = "";
                amlCommentDRI.BindCommentLengthCheck();
                //amlCommentNoContact.BindCommentLengthCheck();
                AmlCommentResponseNotSatisfactory.BindCommentLengthCheck();
                AmlCommentDRIOK.BindCommentLengthCheck();
                amlProfileChangeDREOK.Visible = false;
                amlSurveillanceDREOK.Visible = false;
                AmlProfileChangeResponse.Visible = true;
                AmlSurveillanceReponse.Visible = true;

                if (cbDriAskClient.Checked)
                {
                    panelDriAskClient.CssClass = "";
                }
                if (ckbSourceFunds.Checked || ckbAskInformationAccountBlocked.Checked || ckbAutoEntrepreneur.Checked)
                {
                    panelMacro.CssClass = "";

                    if (ckbAutreMacro.Checked)
                        ddlMacroClient.Visible = true;

                    if (!ckbClientCall.Checked && !ckbClientNoAnswer.Checked)
                    {
                        panelClientManualResponse.CssClass = "";
                    }
                    else panelClientManualResponse.CssClass = "hidden";
                }
                if (cbDriAskConsideration.Checked)
                {
                    panelDriAskConsideration.CssClass = "";

                    if (ckbCounterpartNoAnswer.Checked)
                    {
                        panelCounterpartNoAnswer.CssClass = "";
                        if (ckbCallCounterpart.Checked)
                            panelCallCounterpart.CssClass = "";
                    }
                }
                else panelDriAskConsideration.CssClass = "hidden";
                if (ckbClientCall.Checked)
                    panelClientCallDetails.CssClass = "";

                if(cbDriNoContact.Checked)
                {
                    //panelDriNoContact.CssClass = "";
                    panelNoContact.Attributes["style"] = "border-left:0;width:100%";
                    panelClient.CssClass = "div-client hidden";
                    panelCounterpart.CssClass = "div-counterpart hidden";
                }
                if(cbDriAskClient.Checked || cbDriAskConsideration.Checked)
                {
                    panelNoContact.CssClass = "div-nocontact hidden";
                }

                break;
        }

        // if J+7 après contact client
        if (_iNbDaysSinceAskClient >= 7)
            panelClientCall.CssClass = "";

        //if (!String.IsNullOrWhiteSpace(amlCommentNoContact.Commentary))
        //    amlCommentNoContact.Disabled = true;

        if (_bTreated)
        {
            btnSaveDREOK.Visible = false;
            btnSaveDri.Visible = false;
            panelClientManualResponse.Visible = false;
            amlSurveillanceDREOK.ActionAllowed = false;
            amlCommentDREOK.Disabled = true;
            amlProfileChangeDREOK.ReadOnly = true;
            DisableCheckbox(false);
        }
        else
        {
            if(bDisableControls)
                DisableCheckbox(true);
        }

        upAlertTreatment.Update();
    }

    protected void DisableCheckbox(bool bOnlyChecked)
    {
        if (cbDriAskClient.Checked || !bOnlyChecked)
            cbDriAskClient.Disabled = true;
        if (ckbAskInformationAccountBlocked.Checked || ckbAutoEntrepreneur.Checked || ckbSourceFunds.Checked || !bOnlyChecked)
        {
            ckbAskInformationAccountBlocked.Disabled = true;
            ckbAutoEntrepreneur.Disabled = true;
            ckbSourceFunds.Disabled = true;
        }
        if (ckbClientCall.Checked || !bOnlyChecked)
            ckbClientCall.Disabled = true;
        if (ckbClientNoAnswer.Checked || !bOnlyChecked)
            ckbClientNoAnswer.Disabled = true;
        if (cbDriAskConsideration.Checked || !bOnlyChecked)
            cbDriAskConsideration.Disabled = true;
        if (ckbCounterpartAnswered.Checked || !bOnlyChecked)
            ckbCounterpartAnswered.Disabled = true;
        if (ckbCounterpartNoAnswer.Checked || !bOnlyChecked)
            ckbCounterpartNoAnswer.Disabled = true;
        if (ckbUnfreezeAccount.Checked || !bOnlyChecked)
            ckbUnfreezeAccount.Disabled = true;
        if (ckbCallCounterpart.Checked || !bOnlyChecked)
            ckbCallCounterpart.Disabled = true;
        if (ckbCallCounterpartOk.Checked || ckbCallCounterpartPending.Checked || !bOnlyChecked)
        {
            ckbCallCounterpartOk.Disabled = true;
            ckbCallCounterpartPending.Disabled = true;
        }
        if (ckbContactCounterpartAgain.Checked || !bOnlyChecked)
            ckbContactCounterpartAgain.Disabled = true;
    }

    protected void InitJsDREOK()
    {
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "InitAMLToggles();InitAMLSurveillance();InitProfileToSurveillance();", true);
    }

    protected bool CheckFormValid(string sForm, bool bLightCheck, out string sErrorMessage)
    {
        bool bValid = true;
        sErrorMessage = "";
        StringBuilder sbErrorMessage = new StringBuilder();

        switch (sForm)
        {
            case "DRE":
                if (String.IsNullOrWhiteSpace(amlCommentDREOK.Commentary))
                    sbErrorMessage.AppendLine("Commentaire : champ requis");
                if (!amlSurveillanceDREOK.IsFormValid())
                    sbErrorMessage.AppendLine("Surveillance : paramétrage incorrect");
                if(String.IsNullOrWhiteSpace(rblDREOKKO.SelectedValue))
                    sbErrorMessage.AppendLine("Statut final : champ requis");
                if(ckbNotificationPro.Checked)
                {
                    if(!ckbNotificationProMail.Checked && !ckbNotificationProPhone.Checked)
                        sbErrorMessage.AppendLine("Notification Pro : méthode d'envoi requise");
                    else if(ckbNotificationProMail.Checked && String.IsNullOrWhiteSpace(amlMacroPro.Commentary))
                        sbErrorMessage.AppendLine("Notification Pro : contenu email requis");
                }
                break;
            case "DRI":
                if (cbDriAskConsideration.Checked)
                {
                    if (String.IsNullOrWhiteSpace(txtEmailContactCounterpart.Text))
                        sbErrorMessage.AppendLine("Adresse e-mail : champ requis");
                    else
                    {
                        string[] arEmail = txtEmailContactCounterpart.Text.Replace(" ", "").Split(';');

                        for (int i = 0; i < arEmail.Length; i++)
                        {
                            if (!tools.IsEmailValid(arEmail[i]))
                                sbErrorMessage.AppendLine("Adresse e-mail : valeur \\\"" + arEmail[i] + "\\\" incorrecte");
                        }
                    }

                    if(!bLightCheck && ddlMacroCounterpart.SelectedIndex == 0)
                        sbErrorMessage.AppendLine("Macro contrepartie : champ requis");
                }

                if(!bLightCheck && ckbAutreMacro.Checked && ddlMacroClient.SelectedIndex == 0)
                    sbErrorMessage.AppendLine("Macro client : champ requis");

                if (ckbClientCall.Checked && ddlClientCallDuration.SelectedValue.Length == 0)
                    sbErrorMessage.AppendLine("Durée appel : champ requis");

                //if (cbDriNoContact.Checked)
                //{
                //    if (String.IsNullOrWhiteSpace(amlCommentNoContact.Commentary))
                //        sbErrorMessage.AppendLine("Commentaire : champ requis");
                //}
                //else
                //{
                    if (!bLightCheck && String.IsNullOrWhiteSpace(amlCommentDRI.Commentary) && _dtAmlCommentDRI.Rows.Count == 0)
                        sbErrorMessage.AppendLine("Commentaire : champ requis");
                //}
                break;
            case "DRI_RESPONSE":
                switch (rblDOFResponse.SelectedValue)
                {
                    case "OK":
                        //if (String.IsNullOrWhiteSpace(AmlCommentDRIOK.Commentary))
                        //    sbErrorMessage.AppendLine("Commentaire : champ requis");
                        if (AmlProfileChangeResponse.SelectedProfile == "2BC" && !AmlSurveillanceReponse.IsFormValid())
                            sbErrorMessage.AppendLine("Surveillance : paramétrage incorrect");
                        if (ckbReturnFunds.Checked)
                        {
                            string sErrorMessageTmp;
                            bool bEmpty;

                            if (!ReturnFunds1.CheckForm(out sErrorMessageTmp, out bEmpty))
                                sbErrorMessage.AppendLine(sErrorMessageTmp);
                        }

                        if (ckbNotificationProDRI.Checked)
                        {
                            if (!ckbNotificationProMailDRI.Checked && !ckbNotificationProPhoneDRI.Checked)
                                sbErrorMessage.AppendLine("Notification Pro : méthode d'envoi requise");
                            else if (ckbNotificationProMailDRI.Checked && String.IsNullOrWhiteSpace(amlMacroProDRI.Commentary))
                                sbErrorMessage.AppendLine("Notification Pro : contenu email requis");
                        }
                        break;
                    case "NotSatisfactory":
                        //if (String.IsNullOrWhiteSpace(AmlCommentResponseNotSatisfactory.Commentary))
                        //    sbErrorMessage.AppendLine("Commentaire : champ requis");
                        break;
                    case "KO":
                        if(String.IsNullOrWhiteSpace(rblDeclarable.SelectedValue))
                            sbErrorMessage.AppendLine("Déclaration : veuillez sélectionner une option");
                        break;
                }
                break;
        }

        sErrorMessage = sbErrorMessage.Replace(Environment.NewLine, "<br />").ToString();

        if (sErrorMessage.Length > 0)
            bValid = false;

        return bValid;
    }

    protected void btnSaveDREOK_Click(object sender, EventArgs e)
    {
        string sErrorMessage = "";
        if (CheckFormValid("DRE", false, out sErrorMessage))
        {
            bool bSave = true;
            XElement xmlAlerts, xmlTransfer;
            int iRefAlert = getCurrentRefAlert();

            XElement xml = new XElement("ALERT_TREATMENT",
                                new XAttribute("RefAlert", iRefAlert),
                                new XAttribute("CashierToken", authentication.GetCurrent().sToken),
                                new XAttribute("Status", rblDREOKKO.SelectedValue),
                                new XAttribute("Profil", amlProfileChangeDREOK.SelectedProfile),
                                    new XElement("NOTES",
                                        new XElement("NOTE",
                                            new XAttribute("Note", amlCommentDREOK.Commentary),
                                            new XAttribute("Type", "DRE"))));

            if (rblDREOKKO.SelectedValue == "DREOK")
                xml.Add(new XAttribute("Treated", "1"));

            if (amlSurveillanceDREOK.IsFormValid())
            {
                amlSurveillanceDREOK.GetDataToXml(false, out xmlAlerts, out xmlTransfer);
                xml.Add(xmlAlerts, xmlTransfer);
            }

            if(ckbNotificationPro.Checked)
            {
                if(ckbNotificationProMail.Checked)
                    xml.Add(new XElement("MACROS",
                                    new XElement("MACRO",
                                        new XAttribute("ToSend", "1"),
                                        new XAttribute("Content", amlMacroPro.Commentary),
                                        new XAttribute("Type", "NPRO"),
                                        new XAttribute("ToEntity", "Client"))));

                if (ckbNotificationProPhone.Checked)
                    if (!AddAlertEvent(iRefAlert, 24, int.Parse(authentication.GetCurrent().sRef)))
                        bSave = false;
            }

            if (rblDREOKKO.SelectedValue == "DREKO" && rblDeclarableDREKO.SelectedValue != "3")
                xml.Add(new XAttribute("Declarable", "1"));

            string sRefStatement = "";
            if (bSave && AML.SetAlertTreatment(new XElement("ALL_XML_IN", xml).ToString(), out sRefStatement))
            {
                if (rblDREOKKO.SelectedValue == "DREKO")
                {
                    AddAlertEvent(iRefAlert, 29, int.Parse(authentication.GetCurrent().sRef));

                    switch (rblDeclarableDREKO.SelectedValue)
                    {
                        case "1":
                            Response.Redirect("TracfinStatement.aspx?ref=" + sRefStatement);
                            break;
                        case "2":
                            Response.Redirect("StatusTrackingAccount.aspx?view=2");
                            break;
                        case "3":
                            if (!ckbAskRIB.Checked)
                                Response.Redirect("AccountClosing.aspx?ref=" + getCurrentRefCustomer() + "&alert=" + getCurrentRefAlert(), true);
                            else Response.Redirect("StatusTrackingAccount.aspx");
                            break;
                    }
                }
                else Response.Redirect("ClientDetails.aspx?ref=" + getCurrentRefCustomer() + "&view=aml", true);
            }
            else AlertMessage("Erreur", "Une erreur est survenue lors de l'enregistrement.");
        }
        else AlertMessage("Erreur", sErrorMessage);

        InitJsDREOK();
        BindComponentsState(false);
    }

    protected bool AddAlertEvent(int iRefAlert, int iRefEvent, int iRefCashier)
    {
        bool bAdded = false;
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("AML.P_AddAlertEvent", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@RefAlert", SqlDbType.Int);
            cmd.Parameters.Add("@RefEvent", SqlDbType.Int);
            cmd.Parameters.Add("@RefCashier", SqlDbType.Int);
            cmd.Parameters.Add("@RC", SqlDbType.Int);

            cmd.Parameters["@RefAlert"].Value = iRefAlert;
            cmd.Parameters["@RefEvent"].Value = iRefEvent;
            cmd.Parameters["@RefCashier"].Value = iRefCashier;
            cmd.Parameters["@RC"].Direction = ParameterDirection.ReturnValue;

            cmd.ExecuteNonQuery();

            int iRC = int.Parse(cmd.Parameters["@RC"].Value.ToString());

            if (iRC == 0)
                bAdded = true;
        }
        catch(Exception e)
        {

        }

        return bAdded;
    }

    protected void btnSaveDri_Click(object sender, EventArgs e)
    {
        bool bSaved = SaveDRIInfos(true, false);

        BindComponentsState(bSaved);
    }

    protected bool SaveDRIInfos(bool bShowResponseDialog, bool bLightCheck)
    {
        bool bSaved = false;
        string sErrorMessage = "";
        if (CheckFormValid("DRI", bLightCheck, out sErrorMessage))
        {
            int iRefAlert = getCurrentRefAlert();

            XElement xml = new XElement("ALERT_TREATMENT",
                                new XAttribute("RefAlert", iRefAlert),
                                new XAttribute("CashierToken", authentication.GetCurrent().sToken),
                                new XAttribute("Status", rblAlertStatus.SelectedValue),

                                new XAttribute("NoContact", cbDriNoContact.Checked),

                                new XAttribute("ContactClient", cbDriAskClient.Checked),
                                new XAttribute("DemandeOrigineFonds", ckbSourceFunds.Checked),
                                new XAttribute("DemandeInfoCptBloque", ckbAskInformationAccountBlocked.Checked),
                                new XAttribute("AutoEntrSepActivite", ckbAutoEntrepreneur.Checked),
                                new XAttribute("ClientCall", ckbClientCall.Checked),
                                new XAttribute("ClientCallDuration", ddlClientCallDuration.SelectedValue),
                                new XAttribute("ClientNoAnswer", ckbClientNoAnswer.Checked),

                                new XAttribute("ContactCounterpart", cbDriAskConsideration.Checked),
                                new XAttribute("CounterpartNoAnwser", ckbCounterpartNoAnswer.Checked),
                                new XAttribute("UnfreezeAccount", ckbUnfreezeAccount.Checked),
                                new XAttribute("ContactCounterpartAgain", ckbContactCounterpartAgain.Checked),
                                new XAttribute("CallCounterpart", ckbCallCounterpart.Checked),
                                new XAttribute("CallCounterpartAnswerOk", ckbCallCounterpartOk.Checked),
                                new XAttribute("CallCounterpartAnswerPending", ckbCallCounterpartPending.Checked),

                                new XAttribute("CounterpartAnswer", ckbCounterpartAnswered.Checked)
                                );

            XElement xmlNotes = new XElement("NOTES");
            //if (amlCommentCounterpart.Commentary.Length > 0 && !amlCommentCounterpart.ReadOnly && !amlCommentCounterpart.Disabled)
            //{
            //    xmlNotes.Add(new XElement("NOTE",
            //                    new XAttribute("Note", amlCommentCounterpart.Commentary),
            //                    new XAttribute("Type", "CPC")));

            //    //amlCommentCounterpart.Disabled = true;
            //    _dtAmlCommentCounterpart.Rows.Add(new object[] { amlCommentCounterpart.Commentary.Replace("\n", "<br/>") });
            //    rptAmlCommentCounterpart.DataSource = _dtAmlCommentCounterpart;
            //    rptAmlCommentCounterpart.DataBind();
            //    amlCommentCounterpart.Commentary = "";
            //    panelPrevAmlCommentCounterpart.CssClass = "";
            //}
            if (amlCommentDRI.Commentary.Length > 0 && !amlCommentDRI.ReadOnly && !amlCommentDRI.Disabled)
            {
                xmlNotes.Add(new XElement("NOTE",
                                new XAttribute("Note", amlCommentDRI.Commentary),
                                new XAttribute("Type", "CDI")));

                //amlCommentCounterpart.Disabled = true;
                _dtAmlCommentDRI.Rows.Add(new object[] { amlCommentDRI.Commentary.Replace("\n", "<br/>") });
                rptAmlCommentDRI.DataSource = _dtAmlCommentDRI;
                rptAmlCommentDRI.DataBind();
                amlCommentDRI.Commentary = "";
                panelPrevAmlCommentDRI.CssClass = "";
            }

            //if (amlCommentNoContact.Commentary.Length > 0 && !amlCommentNoContact.ReadOnly && !amlCommentNoContact.Disabled)
            //{
            //    xmlNotes.Add(new XElement("NOTE",
            //                    new XAttribute("Note", amlCommentNoContact.Commentary),
            //                    new XAttribute("Type", "NOC")));
            //    amlCommentNoContact.Disabled = true;
            //}

            xml.Add(xmlNotes);

                XElement xmlMacros = null;
            if (cbDriAskClient.Checked && amlMacroContent.Commentary.Length > 0 && !amlMacroContent.ReadOnly && !amlMacroContent.Disabled)
            {
                string sType = "";
                if (ckbSourceFunds.Checked)
                    sType = "DOF";
                else if (ckbAskInformationAccountBlocked.Checked)
                    sType = "ICB";
                else if (ckbAutoEntrepreneur.Checked)
                    sType = "ASA";
                else if (ckbAutreMacro.Checked)
                    sType = ddlMacroClient.SelectedValue;

                xmlMacros = new XElement("MACROS",
                                    new XElement("MACRO",
                                        new XAttribute("ToSend", "1"),
                                        new XAttribute("Content", amlMacroContent.Commentary),
                                        new XAttribute("Type", sType),
                                        new XAttribute("ToEntity", "Client")));
                amlMacroContent.Disabled = true;
            }
            if (cbDriAskConsideration.Checked && amlMacroContentCounterpart.Commentary.Length > 0 && !amlMacroContentCounterpart.ReadOnly && !amlMacroContentCounterpart.Disabled)
            {
                XElement xmlMacroCounterpart = new XElement("MACRO",
                                                    new XAttribute("ToSend", "1"),
                                                    new XAttribute("Content", amlMacroContentCounterpart.Commentary),
                                                    new XAttribute("Type", "APF"),
                                                    new XAttribute("EmailToUse", txtEmailContactCounterpart.Text.Replace(" ", "")),
                                                    new XAttribute("ToEntity", "Counterpart"));
                if (xmlMacros == null)
                    xmlMacros = new XElement("MACROS",
                                    xmlMacroCounterpart);
                else xmlMacros.Add(xmlMacroCounterpart);

                amlMacroContent.Disabled = true;
            }

            if (xmlMacros != null)
                xml.Add(xmlMacros);

            if (AML.SetAlertTreatment(new XElement("ALL_XML_IN", xml).ToString()))
            {
                bSaved = true;

                if (bShowResponseDialog)
                    if (!CheckDOFResponse())
                        AlertMessage("Enregistrement effectué", "<span style='color:#000'>Les informations ont été enregistrées.</span>");

                // Add other macro event
                if (cbDriAskClient.Checked && ckbAutreMacro.Checked)
                    AddAlertEvent(iRefAlert, 27, int.Parse(authentication.GetCurrent().sRef));
            }
            else AlertMessage("Erreur", "Une erreur est survenue lors de l'enregistrement.");
        }
        else AlertMessage("Erreur", sErrorMessage);

        return bSaved;
    }

    protected bool CheckDOFResponse()
    {
        bool bResponse = false;

        if (_bNewZendeskResponse)
        {
            bResponse = true;
            DataTable dt = new DataTable();
            dt.Columns.Add("URL");
            dt.Columns.Add("Label");

            string sTitle = "";

            for (int i = 0; i < _listNewZendeskResponseFrom.Count; i++)
            {
                if(_listNewZendeskResponseTicketID.Count > i)
                {
                    string sLabel = "";

                    switch (_listNewZendeskResponseFrom[i].ToLower())
                    {
                        case "client":
                            sTitle = "Le client a répondu";
                            sLabel = "Voir la réponse du client";
                            break;
                        case "counterpart":
                            sTitle = "La contrepartie a répondu";
                            sLabel = "Voir la réponse de la contrepartie";
                            break;
                    }

                    dt.Rows.Add(new object[] { String.Format("https://comptenickel.zendesk.com/agent/tickets/{0}", _listNewZendeskResponseTicketID[i]), sLabel });                    
                }
            }

            if(_listNewZendeskResponseFrom.Count > 1)
            {
                sTitle = "Plusieurs réponses ont été reçues";
            }

            rptZendeskTicket.DataSource = dt;
            rptZendeskTicket.DataBind();
            panelZendesk.Visible = true;
            ShowDOFResponse(sTitle);
        }
        else
        {
            if ((cbDriAskConsideration.Checked && ckbCounterpartAnswered.Checked) || (cbDriAskConsideration.Checked && ckbCallCounterpart.Checked && ckbCallCounterpartOk.Checked))
            {
                bResponse = true;
                ShowDOFResponse("La contrepartie a répondu");
            }
            else if (ckbClientCall.Checked)
            {
                bResponse = true;
                ShowDOFResponse("Le client a répondu");
            }
            else if(cbDriNoContact.Checked || ckbClientNoAnswer.Checked)
            {
                bResponse = true;
                ShowDOFResponse("Traiter l'alerte");
            }
        }

        return bResponse;
    }

    protected void ShowDOFResponse_click(object sender, EventArgs e)
    {
        Button btn = (Button)sender;

        if (btn != null)
            ShowDOFResponse(btn.CommandArgument);
    }
    protected void ShowDOFResponse(string sTitle)
    {
        if (String.IsNullOrWhiteSpace(amlMacroProDRI.Commentary))
        {
            amlMacroProDRI.Commentary = AML.GetMacroAnswer(getCurrentRefAlert(), "NPRO");
            upDOFResponse.Update();
        }

        ScriptManager.RegisterStartupScript(this, this.GetType(), "DofResponseKEY", "ShowDofResponse(\"" + sTitle + "\");", true);
    }

    protected void btnDOFResponseValidation_Click(object sender, EventArgs e)
    {
        string sErrorMessage;
        if (CheckFormValid("DRI_RESPONSE", false, out sErrorMessage))
        {
            if (SaveDRIInfos(false, true))
            {
                bool bTreated = false;

                int iRefAlert = getCurrentRefAlert();

                XElement xml = new XElement("ALERT_TREATMENT",
                                    new XAttribute("RefAlert", iRefAlert),
                                    new XAttribute("CashierToken", authentication.GetCurrent().sToken),
                                    new XAttribute("ResponseStatus", rblDOFResponse.SelectedValue));

                if(rblDOFResponse.SelectedValue == "OK" || rblDOFResponse.SelectedValue == "KO")
                    xml.Add(new XAttribute("Status", "DRI"+ rblDOFResponse.SelectedValue));

                switch (rblDOFResponse.SelectedValue)
                {
                    case "OK":
                        if (!String.IsNullOrEmpty(AmlCommentDRIOK.Commentary))
                            xml.Add(new XElement("NOTES",
                                    new XElement("NOTE",
                                        new XAttribute("Type", "DOK"),
                                        new XAttribute("Note", AmlCommentDRIOK.Commentary))));

                        if (!String.IsNullOrWhiteSpace(AmlProfileChangeResponse.SelectedProfile))
                            xml.Add(new XAttribute("Profil", AmlProfileChangeResponse.SelectedProfile));
                        if (AmlProfileChangeResponse.SelectedProfile == "2BC")
                        {
                            XElement xmlAlerts, xmlTransfer;
                            AmlSurveillanceReponse.GetDataToXml(false, out xmlAlerts, out xmlTransfer);
                            xml.Add(xmlAlerts, xmlTransfer);
                        }

                        if (ckbReturnFunds.Checked)
                        {
                            xml.Add(ReturnFunds1.GetDataToXML());
                        }

                        if (ckbNotificationProDRI.Checked)
                        {
                            if (ckbNotificationProMailDRI.Checked)
                                xml.Add(new XElement("MACROS",
                                                new XElement("MACRO",
                                                    new XAttribute("ToSend", "1"),
                                                    new XAttribute("Content", amlMacroProDRI.Commentary),
                                                    new XAttribute("Type", "NPRO"),
                                                    new XAttribute("ToEntity", "Client"))));

                            if (ckbNotificationProPhone.Checked)
                                AddAlertEvent(iRefAlert, 24, int.Parse(authentication.GetCurrent().sRef));
                        }

                        bTreated = true;
                        break;
                    case "NotSatisfactory":
                        if(!String.IsNullOrEmpty(AmlCommentResponseNotSatisfactory.Commentary))
                            xml.Add(new XElement("NOTES",
                                    new XElement("NOTE",
                                        new XAttribute("Type", "RNS"),
                                        new XAttribute("Note", AmlCommentResponseNotSatisfactory.Commentary))));
                        break;
                    case "KO":
                        xml.Add(new XAttribute("AskIBAN", ckbAskRIB.Checked ? "1" : "0"));
                        if (rblDeclarable.SelectedValue != "3")
                        {
                            xml.Add(new XAttribute("Declarable", "1"));

                            bTreated = true;
                        }
                        break;
                }

                xml.Add(new XAttribute("Treated", (bTreated) ? "1" : "0"));

                string sRefStatement = "";
                if (AML.SetAlertTreatment(new XElement("ALL_XML_IN", xml).ToString(), out sRefStatement))
                {
                    if (rblDOFResponse.SelectedValue == "KO")
                    {
                        switch(rblDeclarable.SelectedValue)
                        {
                            case "1":
                                Response.Redirect("TracfinStatement.aspx?ref=" + sRefStatement);
                                break;
                            case "2":
                                Response.Redirect("StatusTrackingAccount.aspx?view=2");
                                break;
                            case "3":
                                if (!ckbAskRIB.Checked)
                                    Response.Redirect("AccountClosing.aspx?ref=" + getCurrentRefCustomer() + "&alert=" + getCurrentRefAlert(), true);
                                else Response.Redirect("StatusTrackingAccount.aspx");
                                break;
                        }
                    }
                    else Response.Redirect("ClientDetails.aspx?ref=" + getCurrentRefCustomer() + "&view=aml", true);
                }
                else AlertMessage("Erreur", "Une erreur est survenue lors de l'enregistrement.");
            }
        }
        else AlertMessage("Erreur", sErrorMessage);
    }

    protected void btnRefreshMacro_Click(object sender, EventArgs e)
    {
        string sType = "";
        panelDdlMacroClient.Visible = false;

        if (ckbSourceFunds.Checked)
            sType = "DOF";
        else if (ckbAskInformationAccountBlocked.Checked)
            sType = "ICB";
        else if (ckbAutoEntrepreneur.Checked)
            sType = "ASA";
        else if (ckbAutreMacro.Checked)
        {
            panelDdlMacroClient.Visible = true;
            sType = ddlMacroClient.SelectedValue;
        }

        amlMacroContent.Commentary = AML.GetMacroAnswer(getCurrentRefAlert(), sType);
    }

    protected void BindOtherMacro(string sFor)
    {
        string sXmlOut = AML.GetMacroAnswer(0, "");
        DataTable dt = new DataTable();
        dt.Columns.Add("Tag");
        dt.Columns.Add("Description");

        List<string> listMacro = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/MACRO");
        List<string> listMacroClient = new List<string> { "DRBE", "NPRO" };
        List<string> listMacroCounterpart = new List<string> { "APF", "APFR" };

        for (int i = 0; i < listMacro.Count; i++)
        {
            List<string> listTag = CommonMethod.GetAttributeValues(listMacro[i], "MACRO", "TAGMacro");
            List<string> listDesc = CommonMethod.GetAttributeValues(listMacro[i], "MACRO", "DescriptionMacro");

            if((sFor == "client" && listMacroClient.Contains(listTag[0])) || (sFor == "counterpart" && listMacroCounterpart.Contains(listTag[0])))
                dt.Rows.Add(new object[] { listTag[0], listDesc[0] });
        }

        DropDownList ddl = null;
        switch (sFor)
        {
            case "client":
                ddl = ddlMacroClient;
                break;
            case "counterpart":
                ddl = ddlMacroCounterpart;
                break;
        }

        ddl.DataSource = dt;
        ddl.DataBind();
    }

    protected void BindDurationList()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("duration");

        for (int i = 5; i <= 60; i += 5)
            dt.Rows.Add(new object[] { i });

        ddlClientCallDuration.DataSource = dt;
        ddlClientCallDuration.DataBind();
    }

    protected void checkPreviousButtonUrl()
    {
        string sUrl = "";
        string sButtonText = "";
        if (tools.GetBackUrl(out sUrl, out sButtonText))
        {
            btnPrev.Text = sButtonText;
            btnPrev.PostBackUrl = sUrl;
        }
    }

    protected void AlertMessage(string sTitle, string sMessage)
    {
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('" + sTitle + "', \"" + sMessage + "\");", true);
    }

    protected void rblAlertStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindComponentsState(false);
    }

    protected void ddlMacroCounterpart_SelectedIndexChanged(object sender, EventArgs e)
    {
        amlMacroContentCounterpart.Commentary = AML.GetMacroAnswer(getCurrentRefAlert(), ddlMacroCounterpart.SelectedValue);
    }
}