﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ClientMandats_Frame.aspx.cs" Inherits="ClientMandats_Frame" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Mandats du client</title>

    <link href="Styles/tipTip.css" rel="Stylesheet" type="text/css" />
    <link href="Styles/UI/jquery-ui-1.9.2.custom.css" rel="Stylesheet" type="text/css" />
    <link href="~/Styles/Site.css" rel="stylesheet" type="text/css" />
    <link href="Styles/Operations.css?v2" type="text/css" rel="Stylesheet" />

    <script src="Scripts/jquery-1.8.3.js" type="text/javascript"></script>
    <script src="Scripts/jquery.tipTip.minified.js" type="text/javascript"></script>
    <script src="Scripts/jquery-ui-1.9.2.custom.min.js" type="text/javascript"></script>
    <script src="Scripts/jQuery_mousewheel_plugin.js" type="text/javascript"></script>
    <script src="Scripts/jquery.jloupe.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="Scripts/mapbox.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery.watermark.min.js" type="text/javascript"></script>
    <%--<script src="Scripts/jquery.pnotify.min.js" type="text/javascript"></script>--%>

    <script type="text/javascript">
        $(document).ready(function () {

            $('.lblClientTime').text(GetClientTime());
        });

        function GetClientTime() {
            var date = new Date();
            var sHour = date.getHours().toString();
            var sMinute = date.getMinutes().toString();
            if (sMinute.length == 1)
                sMinute = '0' + sMinute;
            return sHour + ':' + sMinute;
        }

        function ToggleOperationDetails(row) {
            if ($(row).next('tr').find('.operation-detail, .operation-detail-alternate').is(':hidden')) {
                $(row).next('tr').find('td').css('background-color', $(row).find('td').css('background-color'));
                //$(row).next('tr').find('td').css('padding-bottom', '5px');
                $(row).next('tr').find('.operation-detail, .operation-detail-alternate').slideDown(400);
                //$(row).find('td').css('border-bottom-right-radius', '0').css('border-bottom-left-radius', '0');

                $(row).find('.detail-arrow').attr('src', $(row).find('.detail-arrow').attr('src').replace(/down/g, 'up'));
            }
            else {
                $(row).next('tr').find('.operation-detail, .operation-detail-alternate').slideUp(400, function () {
                    //$(row).find('td:first-of-type').css('border-top-left-radius', '8px').css('border-bottom-left-radius', '8px');
                    //$(row).find('td:last-of-type').css('border-top-right-radius', '8px').css('border-bottom-right-radius', '8px');
                    //$(row).next('tr').find('td').animate({ paddingBottom: 0 }, 0);
                });

                $(row).find('.detail-arrow').attr('src', $(row).find('.detail-arrow').attr('src').replace(/up/g, 'down'));
            }
        }

        function ShowSearchPopup() {
            $("#search-popup").dialog('open');
        }

        function ShowConfirmSuspendMandate(refOperation) {
            $('#<%=hdnSuspendMandate.ClientID%>').val(refOperation);
                $('#dialog-SuspendMandate').dialog({
                    autoOpen: true,
                    resizable: false,
                    width: 400,
                    dialogClass: "no-close",
                    modal: true,
                    title: "Suspension mandat",
                });

                $(".ui-dialog-titlebar").hide();
                $("#dialog-SuspendMandate").parent().appendTo(jQuery("form:first"));
            }

            function closeSuspendMandate() {
                $("#dialog-SuspendMandate").dialog.close();
                $("#dialog-SuspendMandate").dialog.destroy();
            }

            function ShowConfirmActivateMandate(refOperation) {
                $('#<%=hdnActivateMandate.ClientID%>').val(refOperation);
                $('#dialog-ActivateMandate').dialog({
                    autoOpen: true,
                    resizable: false,
                    width: 400,
                    dialogClass: "no-close",
                    modal: true,
                    title: "Activation mandat",
                });

                $(".ui-dialog-titlebar").hide();
                $("#dialog-ActivateMandate").parent().appendTo(jQuery("form:first"));
            }

            function closeActivateMandate() {
                $("#dialog-ActivateMandate").dialog.close();
                $("#dialog-ActivateMandate").dialog.destroy();
            }

            function ShowConfirmBlockMandate(refOperation) {
                $('#<%=hdnBlockMandate.ClientID%>').val(refOperation);
                $('#dialog-BlockMandate').dialog({
                    autoOpen: true,
                    resizable: false,
                    width: 400,
                    dialogClass: "no-close",
                    modal: true,
                    title: "Blocage mandat",
                });

                $(".ui-dialog-titlebar").hide();
                $("#dialog-BlockMandate").parent().appendTo(jQuery("form:first"));
            }

            function closeBlockMandate() {
                $("#dialog-BlockMandate").dialog.close();
                $("#dialog-BlockMandate").dialog.destroy();
            }

            function alertMessageVirement(titleMessage, message, focusID) {
                $('#lblAlertVirement').html(message.toString().trim());
                $('#dialog-alert-virement').dialog({
                    title: "",
                    width: 500,
                    resizable: false,
                    modal: true,
                    closeOnEscape: false,
                    draggable: false,
                    dialogClass: "no-close",
                    title: titleMessage,
                    buttons: {
                        Fermer: function () {
                            $(this).dialog("close");

                            if (focusID != null && focusID.trim().length > 0 && $('#' + focusID) != null)
                                $('#' + focusID).focus();
                        }
                    }
                });
            }
            function ToggleSuspendMenu(item, event) {
                var fn = function () { $('.action-container .action-details').hide() };
                fn();
                $(item).find('.action-details').toggle();
                event.stopPropagation();
                $('body').unbind('click').bind('click', fn);
            }
    </script>
    <style type="text/css">

        .action-container {
            position: relative;
        }

        /*.action-container:hover .action-details {
            display: block;
        }*/

        .action-details {
            display: none;
            position: absolute;
            top: 20px;
            right: 0px;
            background-color: #DDD;
            box-shadow: 1px 1px 5px #FF5F00;
            z-index: 100;
            height: auto !important;
            text-align: left;
        }

        .action-details span {
            display:block;
            margin:4px 0;
        }

        .action-details span:hover {
            cursor: pointer;
            text-decoration: underline;
        }
    </style>
</head>
<body style="background-color: #fff">
    <form id="form1" runat="server">
        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" EnablePartialRendering="true" EnableScriptLocalization="true"
            EnableScriptGlobalization="true" EnablePageMethods="true">
        </asp:ToolkitScriptManager>
        <div style="width: 100%;">
            <div style="margin-top: 0px">
                <asp:Panel ID="panelTransferListTable" runat="server">
                    <asp:UpdatePanel ID="upOpe" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Panel ID="panelNoFilters" runat="server">
                                <div style="margin: 10px 0 5px 0; padding: 0 5px; text-align: left; font-size: 1.2em; text-transform: uppercase">
                                    au
                                    <asp:Label ID="lblDateNow" runat="server" Style="font-weight: bold; text-transform: uppercase"></asp:Label>, à <span class="lblClientTime" style="font-weight: bold"></span>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="panelFilters" runat="server" Visible="false">
                                <div style="margin: 10px 0 5px 0; padding: 0 5px; text-align: left; font-size: 1.2em;" class="font-AracneRegular">
                                    <div style="width: 90%; float: left">
                                        <asp:Literal ID="ltlFilters" runat="server"></asp:Literal>
                                    </div>
                                    <div style="width: 10%; float: right; text-align: right">
                                        <asp:Literal ID="ltlNbResults" runat="server"></asp:Literal>
                                    </div>
                                    <div style="clear: both"></div>
                                </div>
                            </asp:Panel>

                            <div id="ar-loading" style="display: none; position: absolute; background-color: rgba(255, 255, 255, 0.5); z-index: 100"></div>
                            <asp:Repeater ID="rptTransferList" runat="server" OnItemDataBound="rptTransferList_ItemDataBound">
                                <HeaderTemplate>
                                    <table id="tOperationList" style="width: 100%" class="operation-table pending-transfer-table">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <div>
                                                        Date du dernier prélèvement
                                                    </div>
                                                </th>
                                                <th>
                                                    <div>
                                                        Montant du dernier prélèvement
                                                    </div>
                                                </th>
                                                <th style="white-space: nowrap">
                                                    <div>
                                                        Organisme préleveur
                                                    </div>
                                                </th>
                                                <th>
                                                    <div>
                                                        Statut du mandat
                                                    </div>
                                                </th>
                                                <th>
                                                    <div>
                                                        Action possible
                                                    </div>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr class='<%# Container.ItemIndex % 2 == 0 ? "operation-table-row" : "operation-table-alternate-row" %>' style="cursor: auto">
                                        <%--onclick="ToggleOperationDetails(this);"--%>
                                        <td style="width: 100px; white-space: nowrap; text-align: center">
                                            <div>
                                                <%# operation.getTransferFormattedShortDate(Eval("dateDernierPrelevement").ToString(),'/') %>
                                            </div>
                                        </td>
                                        <td style="width: 100px; white-space: nowrap; text-align: center">
                                            <div style="border-right: 0"><%# operation.getFormattedAmount("-" + Eval("MontantEchange").ToString())%></div>
                                        </td>
                                        <td style="width: 300px;">
                                            <div class="ellipsis" title='<%# Eval("creditorName").ToString().Replace("'", "&apos;")%>'><%# Eval("creditorName")%></div>
                                        </td>
                                        <td style="width: 100px">
                                            <div class="ellipsis">
                                                <%# getMandateStatus(Eval("rumStatus").ToString()) %>
                                            </div>
                                        </td>
                                        <td style="width: 80px; white-space: nowrap; text-align: center">
                                            <div class="action-container" onclick="ToggleSuspendMenu(this, event);" style="cursor: pointer">
                                                <span class="tooltip" tooltiptext="Gestion du mandat" style="top: 4px!important">
                                                    <img class="detail-arrow" src="Styles/Img/row-detail-arrow-down.png" style="cursor: pointer" alt="rejeter" />
                                                </span>
                                                <div class="action-details">
                                                    <%# sGetMandateMenu(Eval("rumId").ToString(), Eval("rumStatus").ToString()) %>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </tbody>
                                        </table>
                                </FooterTemplate>
                            </asp:Repeater>
                            <div style="clear: both"></div>
                            <asp:Panel ID="panelNoSearchResult" runat="server" Visible="false" Style="margin: 10px 0; text-align: center">
                                Votre recherche n'a retourné aucun résultat.
                            </asp:Panel>
                            <%--
                            <div style="text-align: center; margin: 3px 0 50px 0">
                                <asp:Button ID="btnShowMore" runat="server" CssClass="button" Text="Afficher plus d'opérations" Style="padding-left: 50px; padding-right: 50px; margin-right: 1px" OnClick="btnShowMore_Click" />
                                <input type="button" value="Rechercher une opération" class="button" onclick="ShowSearchPopup();" style="display: none; padding-left: 50px; padding-right: 50px;" />
                                <asp:Button ID="btnExcelExport" Visible="false" runat="server" CssClass="button excel-button" Text="Export" OnClick="btnExportExcel_Click" />
                            </div>
                            --%>
                        </ContentTemplate>
                        <Triggers>
                            <%-- <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" /> --%>
                        </Triggers>
                    </asp:UpdatePanel>

                    <div id="search-popup" style="display: none">
                        <span class="ui-helper-hidden-accessible">
                            <input type="text" /></span>
                        <div>
                            <div class="font-bold uppercase" style="margin: 5px 0">
                                Période
                            </div>
                            <div style="white-space: nowrap">
                                <asp:Label ID="lblDateFrom" runat="server" AssociatedControlID="txtDateFrom" CssClass="font-AracneRegular font-orange" Font-Bold="true">Du</asp:Label>&nbsp;
                            <asp:TextBox ID="txtDateFrom" runat="server" Style="width: 100px"></asp:TextBox>&nbsp;
                            <asp:Label ID="lblDateTo" runat="server" AssociatedControlID="txtDateTo" CssClass="font-AracneRegular font-orange" Font-Bold="true">Au</asp:Label>&nbsp;
                            <asp:TextBox ID="txtDateTo" runat="server" Style="width: 100px"></asp:TextBox>

                            </div>
                            <div class="font-bold uppercase" style="margin: 10px 0 5px 0">
                                Type de virement
                            </div>
                            <div style="white-space: nowrap">
                                <asp:DropDownList ID="ddlTransferStatus" runat="server" AppendDataBoundItems="true">
                                    <asp:ListItem Value="ALL">TOUS</asp:ListItem>
                                    <asp:ListItem Value="OK">Réussi</asp:ListItem>
                                    <asp:ListItem Value="KO">&Eacute;choué</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div style="text-align: center">
                            <%-- <asp:Button ID="btnSearch" runat="server" Text="Rechercher" OnClick="btnSearch_Click" CssClass="button" Style="display: none" />--%>
                        </div>
                    </div>

                    <div id="dialog-SuspendMandate" style="display: none">
                        Vous êtes sur le point de refuser temporairement les prélèvements ultérieurs de ce mandat.
                    Le créancier peut continuer à prélever sur les autres mandats actifs.
                    <br />
                        <br />
                        Cette action est immédiate. Vous pourrez mettre fin à cette suspension en ligne. 
                    <asp:HiddenField ID="hdnSuspendMandate" runat="server" />
                        <br />
                        <br />
                        <div style="text-align: right">
                            <asp:Button ID="annulSuspendMandate" Text="annuler" runat="server" OnClientClick="closeSuspendMandate(); return false;" CssClass="white-button" />
                            <asp:Button ID="btnSuspendMandate" runat="server" Text="Suspendre le mandat" OnClick="btnSuspendMandate_Click" CssClass="orange-big-button" OnClientClick="closeSuspendMandate()" />
                        </div>
                    </div>

                    <div id="dialog-ActivateMandate" style="display: none">
                        Vous êtes sur le point d'activer les prélèvements de ce mandat.
                    <br />
                        <br />
                        Cette action est immédiate. Vous pourrez suspendre ce mandat en ligne. 
                    <asp:HiddenField ID="hdnActivateMandate" runat="server" />
                        <br />
                        <br />
                        <div style="text-align: right">
                            <asp:Button ID="annulActivateMandate" Text="annuler" runat="server" OnClientClick="closeActivateMandate(); return false;" CssClass="white-button" />
                            <asp:Button ID="btnActivateMandate" runat="server" Text="Activer le mandat" OnClick="btnActivateMandate_Click" CssClass="orange-big-button" OnClientClick="closeActivateMandate()" />
                        </div>
                    </div>

                    <div id="dialog-BlockMandate" style="display: none">
                        Vous êtes sur le point de refuser tous les prélèvements ultérieurs de ce mandat.
                    Vous ne pourrez pas le réactiver en ligne.
                    Le créancier peut continuer à prélever sur les autres mandats actifs.
                    <br />
                        <br />
                        Cette action est immédiate et irrévocable.
                    <asp:HiddenField ID="hdnBlockMandate" runat="server" />
                        <br />
                        <br />
                        <div style="text-align: right">
                            <asp:Button ID="annulBlockMandate" Text="annuler" runat="server" OnClientClick="closeBlockMandate(); return false;" CssClass="white-button" />
                            <asp:Button ID="btnBlockMandate" runat="server" Text="Bloquer le mandat" OnClick="btnBlockMandate_Click" CssClass="orange-big-button" OnClientClick="closeBlockMandate()" />
                        </div>

                    </div>

                </asp:Panel>

            </div>
        </div>

        <div id="dialog-alert-virement" style="display: none">
            <label id="lblAlertVirement"></label>
        </div>
        <script type="text/javascript">
            Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
            var sRequestControlID = "";
            function BeginRequestHandler(sender, args) {
                pbControl = args.get_postBackElement();
                
                {
                    var width = $('#tOperationList').width();
                    var height = $('#tOperationList').height();
                    $('#ar-loading').css('width', width);
                    $('#ar-loading').css('height', height);

                    $('#ar-loading').show();

                    $('#ar-loading').position({ my: "left top", at: "left top", of: "#tOperationList" });
                }
                sRequestControlID = pbControl.id;
            }
            function EndRequestHandler(sender, args) {
                $('#ar-loading').hide();

                $('.lblClientTime').text(GetClientTime());
            }
        </script>
    </form>
</body>
</html>
