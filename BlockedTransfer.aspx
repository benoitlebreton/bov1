﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="BlockedTransfer.aspx.cs" Inherits="BlockedTransfer" %>
<%@ Register Src="~/API/BlockedTransfer.ascx" TagPrefix="asp" TagName="BlockedTransfer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <script type="text/javascript">
        $(document).ready(function(){
            $('.tooltip').tooltip({
                    position: {
                        my: "center bottom-10",
                        at: "center top",
                        using: function (position, feedback) {
                            $(this).css(position);
                            $("<div>")
                                .addClass("arrow")
                                .addClass(feedback.vertical)
                                .addClass(feedback.horizontal)
                                .appendTo(this);
                        }
                    }
                }).css('cursor', 'Help');
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

    <h2 style="color: #344b56;margin-bottom:10px;text-transform:uppercase">Recherche virement</h2>
    <asp:BlockedTransfer ID="BlockedTransfer1" runat="server" />

</asp:Content>

