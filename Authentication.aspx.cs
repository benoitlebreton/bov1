﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using XMLMethodLibrary;

public partial class Authentication : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("fr-FR");
        System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("fr-FR");

        panelLoginSaved.Visible = false;
        panelLoginNotSaved.Visible = true;

        if (IsPostBack)
        {
            string controlName = Request.Params.Get("__EVENTTARGET");
            string argument = Request.Params.Get("__EVENTARGUMENT");
            if (controlName == "deleteLoginCookie")
            {
                deleteLoginCookie(sender, e);
            }
            else if (controlName == "forgotPassword")
            {
                string sMessage = "";
                if(argument.Split(';').Length > 0)
                {
                    string sEmail = argument.Split(';')[0].Trim();

                    if (tools.ForgotPassword(sEmail, out sMessage))
                        lblInfoMessage.ForeColor = Color.Green;
                    else
                        lblInfoMessage.ForeColor = Color.Red;
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "showMessage('Mot de passe oublié','"+sMessage+"','false');", true);
                }
            }
        }

        if (Request.Cookies["LoginCookie"] != null && Request.Cookies["LoginCookie"].Value.Trim().Length > 0)
        {
            panelLoginSaved.Visible = true;
            panelLoginNotSaved.Visible = false;
            LoginTxtLbl.Text = Request.Cookies["LoginCookie"].Value.Trim();
            LoginTxt.Text = Request.Cookies["LoginCookie"].Value.Trim();
        }
    }

    protected void btnConnection_Click(object sender, EventArgs e)
    {
        string sMessage = "";
        string sIPAddress = "0.0.0.0";
        string sServerName = System.Environment.MachineName;
        string sLogin = LoginTxt.Text;
        string sPassword = tools.getHash(PassTxt.Text);

        try { sIPAddress = Request.ServerVariables["REMOTE_ADDR"].ToString(); }
        catch (Exception ex) { }

        /*
        string sXml = "";
        if (hfUseCookieClientID.Value == "true")
        {
            sXml = new XElement("LOGINCOOKIE",
                        new XAttribute("cookieSaved", cbSaveClientID.Checked),
                        new XAttribute("login", Tools.getHash(hfCookieClientID.Value.Trim() + txtClientBirthDate.Text.Trim())),
                        new XAttribute("password", txtPassword.Text),
                        new XAttribute("cookieUsed", "true")).ToString();
        }
        else
        {
            sXml = new XElement("LOGINCOOKIE",
                        new XAttribute("cookieSaved", cbSaveClientID.Checked),
                        new XAttribute("login", txtClientID.Text),
                        new XAttribute("password", txtPassword.Text),
                        new XAttribute("cookieUsed", "false")).ToString();
        }
        */

        if (UserAuthentication(sLogin, sPassword, cbSaveClientID.Checked, sIPAddress, sServerName, out sMessage))
        {
            string sRedirectURL;
            tools.GetRedirectCookieValues(out sRedirectURL);
            if (String.IsNullOrWhiteSpace(sRedirectURL) || sRedirectURL.ToLower().Contains("authentication.aspx"))
                Response.Redirect("Default.aspx");
            else Response.Redirect(sRedirectURL);
        }
        else
        {
            //lblAuthenticationError.Text = sMessage;
            lblInfoMessage.Text = sMessage;
            lblInfoMessage.ForeColor = Color.Red;
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "showMessage('Echec authentification','Une erreur est survenue.<br/> Veuillez réessayer svp. <br/><span style=\"font-size:10px;\">Détails : " + sMessage + "</span>','false');", true);
        }
    }
    protected bool UserAuthentication(string sLogin, string sPassword, bool bSaveCookie, string sIP, string sServer, out string sMessage)
    {
        bool bAuth = false;
        sMessage = "";
        SqlConnection conn = null;

        string sXmlIn = "";

        HttpClientCertificate clientCert = Request.ClientCertificate;
        if (clientCert.IsPresent)
        {
            sXmlIn = new XElement("ALL_XML_IN",
                        new XElement("A",
                            new XAttribute("L", sLogin),
                                new XAttribute("P", sPassword),
                                new XAttribute("I", sIP),
                                new XAttribute("S", sServer),
                                new XAttribute("Cert_ServerSubject", clientCert.ServerSubject.ToString()),
                                new XAttribute("Cert_Subject", clientCert.Subject.ToString()),
                                new XAttribute("Cert_ValidFrom", clientCert.ValidFrom.ToString("dd/MM/yyyy HH:mm:ss")),
                                new XAttribute("Cert_ValidUntil", clientCert.ValidUntil.ToString("dd/MM/yyyy HH:mm:ss")),
                                new XAttribute("Cert_IsValid", (clientCert.IsValid) ? "Y" : "N"))).ToString();
        }
        else
        {
            sXmlIn = new XElement("ALL_XML_IN",
                        new XElement("A",
                            new XAttribute("L", sLogin),
                                new XAttribute("P", sPassword),
                                new XAttribute("I", sIP),
                                new XAttribute("S", sServer),
                                new XAttribute("Certificate", "N"))).ToString();
        }

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("ServiceClient.P_UserAuthentication", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);

            cmd.Parameters["@IN"].Value = sXmlIn;
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if (sXmlOut.Length > 0)
            {
                List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/A", "RC");

                if (listRC.Count > 0 && listRC[0] == "0")
                {
                    List<string> listRef = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/A", "RefUID");
                    List<string> listLastName = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/A", "LastName");
                    List<string> listFirstName = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/A", "FirstName");
                    List<string> listLastConnection = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/A", "LastConnectionDate");
                    List<string> listToken = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/A", "TOKEN");
                    List<string> listProfile = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/A", "TAGPROFILE");
                    List<string> listChangePassword = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/A", "ChangePassword");

                    authentication auth = new authentication();
                    auth.sRef = (listRef.Count > 0) ? listRef[0] : "";
                    auth.sLastName = (listLastName.Count > 0) ? listLastName[0] : "";
                    auth.sFirstName = (listFirstName.Count > 0) ? listFirstName[0] : "";
                    auth.sLastConnectionDate = (listLastConnection.Count > 0) ? listLastConnection[0] : "";
                    auth.sToken = (listToken.Count > 0) ? listToken[0] : "";
                    auth.sTagProfile = (listProfile.Count > 0) ? listProfile[0] : "";
                    auth.bChangePassword = (listChangePassword.Count > 0 && listChangePassword[0] == "1") ? true : false;
                    auth.sEmail = sLogin;
                    auth.sCulture = "fr-FR";

                    Session["Authentication"] = auth;

                    if (bSaveCookie)
                    {
                        bool bCookieSecured = true;
                        string sCookieSecured = (ConfigurationManager.AppSettings["CookieSecured"] != null) ? ConfigurationManager.AppSettings["CookieSecured"].ToString() : "";

                        Response.Cookies["LoginCookie"].Value = sLogin;
                        Response.Cookies["LoginCookie"].Secure = (bool.TryParse(sCookieSecured, out bCookieSecured))?bCookieSecured:true;
                        Response.Cookies["LoginCookie"].HttpOnly = true;
                        Response.Cookies["LoginCookie"].Expires = DateTime.Now.AddDays(365);
                    }

                    bAuth = true;
                }
                else sMessage = "Echec authentification.<br/>Veuillez vérifier votre identifiant et mot de passe.<br/><span style=\"font-size:10px\">" + listRC[0].ToString() + "</span>";
            }
        }
        catch (Exception e)
        {
            sMessage = e.Message.ToString();
            //tools.LogToFile(sMessage, "UserAuthentication");
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return bAuth;
    }

    private void RemoveRedirCookie()
    {
        Response.Cookies.Add(new HttpCookie("__LOGINCOOKIE__", ""));
    }
    private void AddRedirCookie(string sXml)
    {
        FormsAuthenticationTicket ticket =
        new FormsAuthenticationTicket(1, "SessionID", DateTime.Now, DateTime.Now.AddSeconds(10), false, sXml);
        string encryptedText = FormsAuthentication.Encrypt(ticket);

        Session.Abandon();

        Response.Cookies.Add(new HttpCookie("__LOGINCOOKIE__", encryptedText));
    }
    protected void deleteLoginCookie(object sender, EventArgs e)
    {
        if (Request.Cookies["LoginCookie"] != null)
        {
            Response.Cookies.Add(new HttpCookie("LoginCookie", ""));
            Response.Redirect("Authentication.aspx");
        }
    }
}