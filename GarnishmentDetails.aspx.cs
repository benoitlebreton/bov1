﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using XMLMethodLibrary;
using System.Globalization;

public partial class GarnishmentDetails : System.Web.UI.Page
{
    private string _RefGarnishment { get { return (ViewState["RefGarnishment"] != null) ? ViewState["RefGarnishment"].ToString() : ""; } set { ViewState["RefGarnishment"] = value; } }
    private string _DocType { get { return (ViewState["DocType"] != null) ? ViewState["DocType"].ToString() : ""; } set { ViewState["DocType"] = value; } }
    private string _OriginalFile { get { return (ViewState["OriginalFile"] != null) ? ViewState["OriginalFile"].ToString() : ""; } set { ViewState["OriginalFile"] = value; } }
    private string _ResponseFile { get { return (ViewState["ResponseFile"] != null) ? ViewState["ResponseFile"].ToString() : ""; } set { ViewState["ResponseFile"] = value; } }
    private string _Page1 { get { return (ViewState["Page1"] != null) ? ViewState["Page1"].ToString() : ""; } set { ViewState["Page1"] = value; } }
    private string _Page2 { get { return (ViewState["Page2"] != null) ? ViewState["Page2"].ToString() : ""; } set { ViewState["Page2"] = value; } }
    private string _AccountNumber { get { return (ViewState["AccountNumber"] != null) ? ViewState["AccountNumber"].ToString() : ""; } set { ViewState["AccountNumber"] = value; } }
    private string _RefCustomer { get { return (ViewState["RefCustomer"] != null) ? ViewState["RefCustomer"].ToString() : ""; } set { ViewState["RefCustomer"] = value; } }
    private string _BankStatus { get { return (ViewState["BankStatus"] != null) ? ViewState["BankStatus"].ToString() : ""; } set { ViewState["BankStatus"] = value; } }
    private bool _Certified { get { return (ViewState["Treated"] != null) ? bool.Parse(ViewState["Treated"].ToString()) : false; } set { ViewState["Treated"] = value; } }
    private bool _FileRetrieved { get { return (ViewState["FileRetrieved"] != null) ? bool.Parse(ViewState["FileRetrieved"].ToString()) : false; } set { ViewState["FileRetrieved"] = value; } }
    private string _Prev { get { return (ViewState["Prev"] != null) ? ViewState["Prev"].ToString() : ""; } set { ViewState["Prev"] = value; } }
    private bool _Editable { get { return (Session["GarnishmentEditable"] != null) ? bool.Parse(Session["GarnishmentEditable"].ToString()) : false; } set { Session["GarnishmentEditable"] = value; } }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.Params["prev"] != null)
            {
                _Prev = Request.Params["prev"].ToString();
            }

            CheckRights();
            BindGarnishmentDetails();

            if(Session["GarnishmentSaved"] != null && Session["GarnishmentSaved"].ToString() == "1")
            {
                Session.Remove("GarnishmentSaved");
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Avis traité', \"<span style='color:green'>Le traitement de l'avis est terminé.</span>\");", true);
            }
        }
    }

    protected void CheckRights()
    {
        string sXmlOut = tools.GetRights(authentication.GetCurrent().sToken, "SC_GarnishmentDetails");

        List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Page", "RC");

        if (listRC.Count == 0 || listRC[0] != "0")
        {
            Response.Redirect("Default.aspx", true);
        }
        else
        {
            List<string> listAction = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/Page/Action");

            for (int i = 0; i < listAction.Count; i++)
            {
                List<string> listTag = CommonMethod.GetAttributeValues(listAction[i], "Action", "TagAction");
                List<string> listActionAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ActionAllowed");
                List<string> listViewAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ViewAllowed");

                if (listTag.Count > 0)
                {
                    switch (listTag[0])
                    {
                        case "GarnishmentRejected":
                            if (listActionAllowed.Count > 0 && listActionAllowed[0] == "1")
                                panelReject.Visible = true;
                            break;
                        case "GarnishmentRejectedFinal":
                            if (listActionAllowed.Count > 0 && listActionAllowed[0] == "1")
                                panelRejectFinal.Visible = true;
                            break;
                        case "GarnishmentCancelRejection":
                            if (listActionAllowed.Count > 0 && listActionAllowed[0] == "1")
                                panelCancelRejection.Visible = true;
                            break;
                        case "GarnishmentRetreat":
                            panelRetreat.Visible = true;
                            break;
                        case "GarnishmentModifyTreatment":
                            if (listActionAllowed.Count > 0 && listActionAllowed[0] == "1")
                                _Editable = true;
                            break;
                    }
                }
            }
        }
    }

    protected void BindGarnishmentDetails()
    {
        if (Request.Params["ref"] != null && !String.IsNullOrWhiteSpace(Request.Params["ref"].ToString()))
        {
            _RefGarnishment = Request.Params["ref"].ToString();
            string sXmlOut = Garnishment.GetGarnishmentDetails(_RefGarnishment, authentication.GetCurrent().sToken);

            //sXmlOut = "<ALL_XML_OUT><Garnishment BOUserReserved=\"David BOULY\" SBI=\"524.68\" Amount=\"21802.62\" IBAN=\"FR693000100286364C000000015\" AccountNumber=\"00049720001\" AccountFirstName=\"GUILLAUME\" AccountLastName=\"NORMAND\" Date=\"09/06/2016 19:28:08\" ExternalRef=\"1617078020124 075013\" DocType=\"ATD\" Ref=\"7\"><Documents><Document FileIndex=\"0\" Filename=\"Test_David_ATD.pdf\"/><Document FileIndex=\"1\" Filename=\"1_Test_David_ATD.JPG\"/><Document FileIndex=\"2\" Filename=\"2_Test_David_ATD.JPG\"/></Documents></Garnishment></ALL_XML_OUT>"; // TEST
            //sXmlOut = "<ALL_XML_OUT><Garnishment BOUserReserved=\"David BOULY\" SBI=\"524.68\" Amount=\"21802.62\" IBAN=\"FR693000100286364C000000015\" AccountNumber=\"00049720001\" AccountFirstName=\"GUILLAUME\" AccountLastName=\"NORMAND\" Date=\"09/06/2016 19:28:08\" ExternalRef=\"1617078020124 075013\" DocType=\"AO\" Ref=\"7\"><Documents><Document FileIndex=\"0\" Filename=\"Test_David_AO.pdf\"/><Document FileIndex=\"1\" Filename=\"1_Test_David_AO.JPG\"/><Document FileIndex=\"2\" Filename=\"2_Test_David_AO.JPG\"/></Documents></Garnishment></ALL_XML_OUT>"; // TEST
            //sXmlOut = "<ALL_XML_OUT><Garnishment BOUserReserved=\"David BOULY\" SBI=\"524.68\" Amount=\"21802.62\" IBAN=\"FR693000100286364C000000015\" AccountNumber=\"00049720001\" AccountFirstName=\"GUILLAUME\" AccountLastName=\"NORMAND\" Date=\"09/06/2016 19:28:08\" ExternalRef=\"1617078020124 075013\" DocType=\"OA1\" Ref=\"7\"><Documents><Document FileIndex=\"0\" Filename=\"Test_David_OA1.pdf\"/><Document FileIndex=\"1\" Filename=\"1_Test_David_OA1.JPG\"/><Document FileIndex=\"2\" Filename=\"2_Test_David_OA1.JPG\"/></Documents></Garnishment></ALL_XML_OUT>"; // TEST
            //sXmlOut = "<ALL_XML_OUT><Garnishment BOUserReserved=\"David BOULY\" SBI=\"524.68\" Amount=\"21802.62\" IBAN=\"FR693000100286364C000000015\" AccountNumber=\"00049720001\" AccountFirstName=\"GUILLAUME\" AccountLastName=\"NORMAND\" Date=\"09/06/2016 19:28:08\" ExternalRef=\"1617078020124 075013\" DocType=\"OA2\" Ref=\"7\"><Documents><Document FileIndex=\"0\" Filename=\"Test_David_OA2.pdf\"/><Document FileIndex=\"1\" Filename=\"1_Test_David_OA2.JPG\"/><Document FileIndex=\"2\" Filename=\"2_Test_David_OA2.JPG\"/></Documents></Garnishment></ALL_XML_OUT>"; // TEST

            if (sXmlOut.Length > 0)
            {
                List<string> listStatus = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Garnishment", "StatusTAG");
                List<string> listDate = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Garnishment", "Date");
                List<string> listRefCustomer = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Garnishment", "RefCustomer");
                List<string> listAccountNumber = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Garnishment", "AccountNumber");
                List<string> listExternalRef = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Garnishment", "ExternalRef");
                List<string> listDocType = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Garnishment", "DocType");
                List<string> listIBAN = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Garnishment", "IBAN");
                List<string> listAmount = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Garnishment", "Amount");
                List<string> listTransferAmount = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Garnishment", "TransferAmount");
                List<string> listEmail = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Garnishment", "ToEmail");
                List<string> listDocuments = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/Garnishment/Documents/Document");
                List<string> listBOUserCertified = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Garnishment", "BOUserCertified");
                List<string> listRejectedBy = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Garnishment", "RejectedBy");
                List<string> listRejectedFinalBy = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Garnishment", "RejectedFinalBy");
                List<string> listRejectedReason = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Garnishment", "RejectedReason");

                List<string> listSBI = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Garnishment", "SBI");
                ViewState["SBI"] = (listSBI.Count > 0) ? listSBI [0] : "-1";

                List<string> listResponseMethod = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Garnishment", "ResponseMethod");

                List<string> listAddress_Recipient = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Garnishment", "ADD_Recipient");
                List<string> listAddress_Street = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Garnishment", "ADD_Street");
                List<string> listAddress_Zip = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Garnishment", "ADD_Zip");
                List<string> listAddress_City = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Garnishment", "ADD_City");
                txtAddress_Recipient.Text = (listAddress_Recipient.Count > 0) ? listAddress_Recipient[0] : "";
                txtAddress_Street.Text = (listAddress_Street.Count > 0) ? listAddress_Street[0] : "";
                txtAddress_Zipcode.Text = (listAddress_Zip.Count > 0) ? listAddress_Zip[0] : "";
                txtAddress_City.Text = (listAddress_City.Count > 0) ? listAddress_City[0] : "";

                lblDate.Text = listDate[0];
                _AccountNumber = (listAccountNumber.Count > 0) ? listAccountNumber[0] : "";
                txtAccountNumber.Text = _AccountNumber;
                txtExternalRef.Text = (listExternalRef.Count > 0) ? listExternalRef[0] : "";
                txtIBAN.Text = (listIBAN.Count > 0) ? listIBAN[0] : "";
                txtEmail.Text = (listEmail.Count > 0) ? listEmail[0] : "";
                _Certified = (listStatus.Count > 0 && (listStatus[0] == "Certified" || listStatus[0] == "Treated")) ? true : false;
                if(_Certified)
                    hdnStatus.Value = "treated";
                //_Certified = false; // TEST
                //listStatus[0] = ""; // TEST

                if (listStatus[0] != "Treated" && listStatus[0] != "Certified" && listStatus[0] != "TreatedError" && listStatus[0] != "RejectedFinal")
                {
                    panelRetreat.Visible = false;
                }

                Decimal dAmount = -1;
                if (listAmount.Count > 0 && listAmount[0].Length > 0 && Decimal.TryParse(listAmount[0].Replace('.', ','), NumberStyles.Currency, new CultureInfo("fr-FR"), out dAmount))
                    txtAmount.Text = dAmount.ToString();
                Decimal dTransferAmount = -1;
                if (listTransferAmount.Count > 0 && listTransferAmount[0].Length > 0 && Decimal.TryParse(listTransferAmount[0].Replace('.', ','), NumberStyles.Currency, new CultureInfo("fr-FR"), out dTransferAmount))
                    txtTransferAmount.Text = dTransferAmount.ToString();

                if (listRefCustomer.Count > 0 && listRefCustomer[0].Length > 0)
                {
                    List<string> listLastName = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Garnishment", "AccountLastName");
                    List<string> listFirstName = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Garnishment", "AccountFirstName");
                    List<string> listBirthDate = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Garnishment", "AccountBirthDate");

                    UpdateClientPanel(listRefCustomer[0], listLastName[0], listFirstName[0], listBirthDate[0]);

                    if (_Prev == "fc")
                    {
                        btnPrev.Text = "Fiche client";
                        btnPrev.PostBackUrl = "ClientDetails.aspx?ref=" + listRefCustomer[0];
                    }
                }
                //else panelClientDetails.Visible = false;

                // DOCS
                //if (listDocuments.Count == 3)
                {
                    _DocType = (listDocType.Count > 0 && listDocType[0].Length > 0) ? listDocType[0] : "";
                    ListItem item = ddlGarnishmentType.Items.FindByValue(_DocType);
                    if (item != null)
                        item.Selected = true;
                    else ddlGarnishmentType.Items[0].Selected = true;

                    for (int i = 0; i < listDocuments.Count; i++)
                    {
                        List<string> listIndex = CommonMethod.GetAttributeValues(listDocuments[i], "Document", "FileIndex");
                        List<string> listFileName = CommonMethod.GetAttributeValues(listDocuments[i], "Document", "Filename");

                        if (listIndex.Count > 0)
                            switch (listIndex[0])
                            {
                                case "0":
                                    _OriginalFile = listFileName[0];
                                    break;
                                case "1":
                                    _Page1 = listFileName[0];
                                    break;
                                case "2":
                                    _Page2 = listFileName[0];
                                    break;
                            }
                    }
                }
                //else ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Erreur', \"Nombre de document insuffisant.\");", true);

                string sFileName = "";
                if (!String.IsNullOrWhiteSpace(_Page2))
                {
                    sFileName = _Page2.Substring(0, _Page2.LastIndexOf('.')).Trim().Replace(' ', '_');
                    _ResponseFile = sFileName + ".pdf";
                }

                TransferDocFromGoogle();

                if (!listStatus[0].ToUpper().Contains("REJECTED"))
                    UpdateZoomInfos(_Page1, _DocType, 1, true);

                if (_Certified)
                {
                    panelReject.Visible = false;
                    panelClientSearch.Visible = false;
                    panelFormBG.Visible = false;
                    panelFormButton.Visible = false;
                    ddlGarnishmentType.Enabled = false;
                    txtAccountNumber.Enabled = false;
                    txtExternalRef.Enabled = false;
                    txtIBAN.Enabled = false;
                    txtAmount.Enabled = false;
                    txtEmail.Enabled = false;
                    txtAddress_Recipient.Enabled = false;
                    txtAddress_Street.Enabled = false;
                    txtAddress_Zipcode.Enabled = false;
                    txtAddress_City.Enabled = false;
                    panelAllForm.CssClass = "active";
                    if (txtTransferAmount.Text.Length > 0)
                        panelForm6.Visible = true;

                    if (listResponseMethod.Count > 0 && listResponseMethod[0] == "email")
                        rblResponseMethod.SelectedValue = "email";
                    else
                        rblResponseMethod.SelectedValue = "mail";
                    rblResponseMethod.Enabled = false;

                    panelPDFEditor.Visible = false;
                    panelPDFResponse.Visible = true;
                    panelResponse.Visible = true;
                    string sGarnishmentResponsePath = (ConfigurationManager.AppSettings["GarnishmentResponsePath"] != null) ? ConfigurationManager.AppSettings["GarnishmentResponsePath"].ToString() : "";

                    if (File.Exists(System.Web.HttpContext.Current.Server.MapPath("~" + sGarnishmentResponsePath + sFileName + ".pdf")))
                        frameResponse.Attributes["src"] = sGarnishmentResponsePath + sFileName + ".pdf#zoom=150";
                    else ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Erreur', \"Document réponse introuvable.\");", true);

                    panelStatus.Visible = true;
                    lblStatus.Text = "Certifié par " + listBOUserCertified[0];

                    if(_Editable)
                    {
                        ddlGarnishmentType.Enabled = true;
                        txtExternalRef.Enabled = true;
                        panelSaveModificationPostTreatment.Visible = true;
                    }
                }
                else if (listStatus[0].ToUpper().Contains("REJECTED"))
                {
                    panelForm.Visible = false;
                    panelReject.Visible = false;
                    panelRejectDetails.Visible = true;

                    lblRejectedBy.Text = (listRejectedBy.Count > 0) ? listRejectedBy[0] : "";
                    lblRejectedReason.Text = (listRejectedReason.Count > 0) ? listRejectedReason[0] : "";
                    string sGarnishmentOriginalPath = (ConfigurationManager.AppSettings["GarnishmentOriginalPath"] != null) ? ConfigurationManager.AppSettings["GarnishmentOriginalPath"].ToString() : "";
                    if (File.Exists(System.Web.HttpContext.Current.Server.MapPath("~" + sGarnishmentOriginalPath + _OriginalFile)))
                        frameOriginalFile.Attributes["src"] = sGarnishmentOriginalPath + _OriginalFile;
                    else ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Erreur', \"Document original introuvable.\");", true);

                    if (listStatus[0].ToUpper() == "REJECTEDFINAL")
                    {
                        panelCancelRejection.Visible = false;
                        panelRejectFinal.Visible = false;
                        panelRejectFinalDetails.Visible = true;
                        lblRejectedFinalBy.Text = (listRejectedFinalBy.Count > 0) ? listRejectedFinalBy[0] : "";
                    }
                }
            }
        }
        else Response.Redirect("GarnishmentSearch.aspx", true);
    }

    protected void TransferDocFromGoogle()
    {
        string sGarnishmentOriginalPath = (ConfigurationManager.AppSettings["GarnishmentOriginalPath"] != null) ? ConfigurationManager.AppSettings["GarnishmentOriginalPath"].ToString() : "";
        string sGarnishmentResponsePath = (ConfigurationManager.AppSettings["GarnishmentResponsePath"] != null) ? ConfigurationManager.AppSettings["GarnishmentResponsePath"].ToString() : "";
        if ((!File.Exists(System.Web.HttpContext.Current.Server.MapPath("~" + sGarnishmentOriginalPath + _OriginalFile))
            || (!File.Exists(System.Web.HttpContext.Current.Server.MapPath("~" + sGarnishmentResponsePath + _ResponseFile)) && _Certified))
            && !_FileRetrieved)
        {
            Garnishment.GetGarnishmentFileFromGoogle(_RefCustomer);
            _FileRetrieved = true;
            BindGarnishmentDetails();
        }
    }

    protected void UpdateClientPanel(string sRefCustomer, string sLastName, string sFirstName, string sBirthDate)
    {
        lblName.Text = sLastName + " " + sFirstName;
        lblBirthDate.Text = sBirthDate;
        panelClientInfos.Visible = true;
        _RefCustomer = sRefCustomer;
        //btnShowClient.Attributes.Add("onclick", "window.open('ClientDetails.aspx?ref=" + sRefCustomer + "');");
        //panelClientDetails.Visible = true;
    }

    [Serializable]
    protected class ZoomInfos
    {
        public string template { get; set; }
        public List<Zoom> zooms { get; set; }
        public int zoomIndex { get; set; }
    }
    [Serializable]
    protected class Zoom
    {
        public int x { get; set; }
        public int y { get; set; }
        public int level { get; set; }

        public Zoom(int x, int y, int level)
        {
            this.x = x;
            this.y = y;
            this.level = level;
        }
    }
    protected void UpdateZoomInfos(string sFileName, string sDocType, int iStep, bool bAlert)
    {
        //UpdateZoomInfos(_Page1, _DocType, (iStep != 6) ? iStep - 1 : 0, false);
        string sGarnishmentPath = (ConfigurationManager.AppSettings["GarnishmentPath"] != null) ? ConfigurationManager.AppSettings["GarnishmentPath"].ToString() : "";

        if (File.Exists(System.Web.HttpContext.Current.Server.MapPath("~" + sGarnishmentPath + _Page1)))
        {
            ZoomInfos zoomInfos = new ZoomInfos();
            zoomInfos.template = sGarnishmentPath + _Page1;
            List<Zoom> listZoom = new List<Zoom>();

            switch (sDocType)
            {
                case "AO":
                    listZoom.Add(new Zoom(0, 0, 0));
                    listZoom.Add(new Zoom(200, 220, 2));
                    listZoom.Add(new Zoom(200, 220, 2));
                    listZoom.Add(new Zoom(270, 940, 4));
                    listZoom.Add(new Zoom(200, 290, 2));
                    listZoom.Add(new Zoom(200, 350, 3));
                    listZoom.Add(new Zoom(550, 0, 4));
                    break;
                case "ATD":
                    listZoom.Add(new Zoom(0, 0, 0));
                    listZoom.Add(new Zoom(270, 940, 4));
                    listZoom.Add(new Zoom(200, 290, 2));
                    listZoom.Add(new Zoom(700, 940, 4));
                    listZoom.Add(new Zoom(200, 290, 2));
                    listZoom.Add(new Zoom(200, 240, 4));
                    listZoom.Add(new Zoom(550, 0, 4));
                    break;
                case "OA1":
                    listZoom.Add(new Zoom(0, 0, 0));
                    listZoom.Add(new Zoom(200, 270, 2));
                    listZoom.Add(new Zoom(200, 200, 2));
                    listZoom.Add(new Zoom(700, 1050, 4));
                    listZoom.Add(new Zoom(200, 400, 3));
                    listZoom.Add(new Zoom(0, 0, 0)); // not used but needed
                    listZoom.Add(new Zoom(200, 400, 3));
                    break;
                case "OA2":
                    listZoom.Add(new Zoom(0, 0, 0));
                    listZoom.Add(new Zoom(250, 850, 2));
                    listZoom.Add(new Zoom(250, 850, 2));
                    listZoom.Add(new Zoom(700, 850, 4));
                    listZoom.Add(new Zoom(200, 0, 3));
                    listZoom.Add(new Zoom(0, 0, 0)); // not used but needed
                    listZoom.Add(new Zoom(200, 0, 3));
                    break;
            }

            zoomInfos.zooms = listZoom;

            int iZoomIndex = iStep;
            if (iStep == 7)
                iZoomIndex = 6;
            else if (iStep == 6)
                iZoomIndex = 5;
            zoomInfos.zoomIndex = iZoomIndex;

            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "InitDocReader(" + JsonConvert.SerializeObject(zoomInfos) + ");", true);
        }
        else InitOriginalFrame(bAlert);

        if(!_Certified)
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "InitBlur();", true);
    }

    protected void InitOriginalFrame(bool bAlertError)
    {
        panelCroppedImg.Visible = false;
        string sGarnishmentOriginalPath = (ConfigurationManager.AppSettings["GarnishmentOriginalPath"] != null) ? ConfigurationManager.AppSettings["GarnishmentOriginalPath"].ToString() : "";

        if (File.Exists(System.Web.HttpContext.Current.Server.MapPath("~" + sGarnishmentOriginalPath + _OriginalFile)))
        {
            if(bAlertError)
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Erreur', \"Document détouré introuvable.<br/>Le document PDF original sera affiché à la place.\");", true);

            frameOriginalSmall.Attributes["src"] = sGarnishmentOriginalPath + _OriginalFile;
            panelOriginalFile.Visible = true;
            upFile1.Update();
        }
        else ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Erreur', \"Document original introuvable.\");", true);
    }

    protected void btnSaveForm_Click(object sender, EventArgs e)
    {
        Button btn = (Button)sender;

        if (btn != null)
        {
            int iStep = int.Parse(btn.CommandArgument);
            string sErrorMessage = "";
            string sToken = authentication.GetCurrent().sToken;
            decimal dAccountBalance = -1;
            string sFormattedAccountBalance = "";

            if (CheckForm(iStep, out sErrorMessage))
            {
                if (SaveInformation(iStep))
                {
                    switch (iStep)
                    {
                        case 1:
                            _DocType = ddlGarnishmentType.SelectedValue;

                            if (_DocType.Contains("OA"))
                            {
                                rblResponseMethod.Items.Clear();
                                rblResponseMethod.Items.Add(new ListItem("Courrier postal", "mail"));
                                rblResponseMethod.Items.FindByValue("mail").Selected = true;
                                panelEmail.Visible = false;
                                upResponseMethod.Update();
                            }
                            break;
                        case 2:
                            string sRefCustomer = "", sLastName = "", sFirstName = "", sBirthDate = "";
                            if (SearchCustomer(txtAccountNumber.Text, sToken, out sRefCustomer, out sLastName, out sFirstName, out sBirthDate))
                            {
                                _AccountNumber = txtAccountNumber.Text;
                                UpdateClientPanel(sRefCustomer, sLastName, sFirstName, sBirthDate);

                                if (_RefCustomer.Length > 0)
                                {
                                    string sXMLCustomerInfos = tools.GetXMLCustomerInformations(int.Parse(sRefCustomer));
                                    List<string> listBankStatus = CommonMethod.GetAttributeValues(sXMLCustomerInfos, "ALL_XML_OUT/Customer", "BankStatus");

                                    if (listBankStatus.Count > 0)
                                    {
                                        _BankStatus = listBankStatus[0];
                                        //1:Bloque debit
                                        //2:Bloque credit
                                        //3:Bloque debit/credit
                                        //4:Cloture
                                    }

                                    //_BankStatus = "4"; // TEST
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Erreur', \"Client inconnu.\");", true);
                                iStep = iStep - 1;
                            }
                            break;
                        case 4:
                            if (_BankStatus != "4") // Compte clôturé
                            {
                                dAccountBalance = -1;
                                if (tools.GetAccountBalance(int.Parse(_RefCustomer), out dAccountBalance, out sFormattedAccountBalance))
                                {
                                    //dAccountBalance = 600M; // TEST

                                    decimal dSBI = -1;
                                    if (ViewState["SBI"].ToString().Trim().Length > 0 && Decimal.TryParse(ViewState["SBI"].ToString().Replace('.', ','), NumberStyles.Currency, new CultureInfo("fr-FR"), out dSBI))
                                    {
                                        decimal dAmount = -1;
                                        if (Decimal.TryParse(txtAmount.Text.Replace('.', ','), NumberStyles.Currency, new CultureInfo("fr-FR"), out dAmount))
                                        {
                                            decimal dBalanceMoinsSBI = dAccountBalance - dSBI;
                                            decimal dAmountTransferable = (dBalanceMoinsSBI > dAmount) ? dAmount : dBalanceMoinsSBI;

                                            if (dAccountBalance > dSBI && dAmountTransferable > 1)
                                            {
                                                lblSBIstatus.Text = "Solde supérieur au SBI (" + dSBI.ToString() + " €)";
                                                panelForm5.Visible = true;
                                                panelForm6.Visible = true;

                                                txtTransferAmount.Text = dAmountTransferable.ToString().Replace('.', ',');
                                            }
                                            else
                                            {
                                                lblSBIstatus.Text = "Solde inférieur au SBI (" + dSBI.ToString() + " €)";
                                                panelForm5.Visible = false;
                                                panelForm6.Visible = false;

                                                iStep = 6;
                                                //InitResponsePanel(0, dAccountBalance);
                                            }

                                            panelSBIStatus.Visible = true;
                                        }
                                    }
                                    else
                                    {
                                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Erreur', \"Erreur lors de la récupération du SBI.<br/>Veuillez réessayer.\");", true);
                                        iStep = iStep - 1;
                                    }
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Erreur', \"Erreur lors de la récupération du solde client.<br/>Veuillez réessayer.\");", true);
                                    iStep = iStep - 1;
                                }
                            }
                            else
                            {
                                lblSBIstatus.Text = "Compte clôturé";
                                panelForm5.Visible = false;
                                panelForm6.Visible = false;
                                panelSBIStatus.Visible = true;

                                iStep = 6;
                                //InitResponsePanel(0, dAccountBalance);
                            }
                            break;
                        //case 6:
                        case 7:
                            dAccountBalance = -1;
                            tools.GetAccountBalance(int.Parse(_RefCustomer), out dAccountBalance, out sFormattedAccountBalance);
                            decimal dTransferAmount = txtTransferAmount.Text.Length > 0 ? Decimal.Parse(txtTransferAmount.Text.Replace('.', ','), NumberStyles.Currency, new CultureInfo("fr-FR")) : 0;
                            InitResponsePanel(dTransferAmount, dAccountBalance);
                            break;
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Erreur', \"Erreur lors de l'enregistrement.<br/>Veuillez réessayer.\");", true);
                    iStep = iStep - 1;
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Erreur', \"" + sErrorMessage + "\");", true);
                iStep = iStep - 1;
            }

            iStep = iStep + 1;
            btnSaveForm.CommandArgument = iStep.ToString();
            Panel panel = (Panel)panelForm.FindControl("panelForm" + iStep);
            if (panel != null)
                panel.CssClass = "row active";

            UpdateZoomInfos(_Page1, _DocType, iStep, false);
        }
    }

    protected bool SaveInformation(int iStep)
    {
        bool bOk = false;

        XElement xelem = new XElement("Garnishment");
        xelem.Add(new XAttribute("Ref", _RefGarnishment),
        new XAttribute("CashierToken", authentication.GetCurrent().sToken));

        if(iStep == 0 || iStep == 1)
        {
            xelem.Add(
                new XAttribute("DocType", ddlGarnishmentType.SelectedValue));
        }
        if(iStep == 0 || iStep == 2)
        {
            xelem.Add(
                new XAttribute("AccountNumber", txtAccountNumber.Text.Trim()));
        }
        if(iStep == 0 || iStep == 3)
        {
            xelem.Add(
                new XAttribute("ExternalRef", txtExternalRef.Text.Trim()));
        }
        
        if (iStep == 0 || iStep == 4)
        {
            xelem.Add(
                new XAttribute("Amount", txtAmount.Text.Trim()));
        }
        if(iStep == 0 || iStep == 7)
        {
            if (rblResponseMethod.SelectedValue == "email")
            {
                xelem.Add(
                    new XAttribute("MailBDF", txtEmail.Text.Trim()));
            }
            else
            {
                xelem.Add(
                    new XAttribute("ADD_Recipient", txtAddress_Recipient.Text.Trim()));
                xelem.Add(
                    new XAttribute("ADD_Street", txtAddress_Street.Text.Trim()));
                xelem.Add(
                    new XAttribute("ADD_Zip", txtAddress_Zipcode.Text.Trim()));
                xelem.Add(
                    new XAttribute("ADD_City", txtAddress_City.Text.Trim()));
            }
        }

        if (iStep == 0 || iStep == 5)
        {
            xelem.Add(
                new XAttribute("IBAN", txtIBAN.Text.Trim()));
        }
        if (iStep == 0 || iStep == 6)
        {
            xelem.Add(
                new XAttribute("TransferAmount", txtTransferAmount.Text.Trim()));
        }

        return Garnishment.SetGarnishmentDetails(new XElement("ALL_XML_IN", xelem).ToString());
    }

    protected bool CheckForm(int iRefForm, out string sErrorMessage)
    {
        bool bOk = false;
        StringBuilder sb = new StringBuilder();

        switch (iRefForm)
        {
            case 1:
                if (ddlGarnishmentType.SelectedValue.Length > 0)
                    bOk = true;
                else sb.AppendLine("Type d'avis : champ requis");
                break;
            case 2:
                if (!String.IsNullOrWhiteSpace(txtAccountNumber.Text))
                    bOk = true;
                else sb.AppendLine("Numéro compte : champ requis");
                break;
            case 3:
                if (!String.IsNullOrWhiteSpace(txtExternalRef.Text))
                    bOk = true;
                else sb.AppendLine("Référence externe : champ requis");
                break;
            case 4:
                if (!String.IsNullOrWhiteSpace(txtAmount.Text))
                {
                    Decimal dAmount = -1;
                    if (!Decimal.TryParse(txtAmount.Text.Replace('.', ','), NumberStyles.Currency, new CultureInfo("fr-FR"), out dAmount))
                    {
                        sb.AppendLine("Montant : format non valide");
                        break;
                    }

                    bOk = true;
                }
                else sb.AppendLine("Montant : champ requis");
                break;
            case 7:
                if (rblResponseMethod.SelectedValue != null && rblResponseMethod.SelectedValue.Trim() != "")
                {
                    if (rblResponseMethod.SelectedValue == "email")
                    {
                        if (!String.IsNullOrWhiteSpace(txtEmail.Text))
                        {
                            if (tools.IsEmailValid(txtEmail.Text.Trim()))
                                bOk = true;
                            else
                                sb.AppendLine("Courriel : format non valide");
                        }
                        else sb.AppendLine("Courriel : champ requis");
                    }
                    else
                    {
                        bOk = true;
                        if (String.IsNullOrWhiteSpace(txtAddress_Recipient.Text))
                        {
                            bOk = false;
                            sb.AppendLine("Centre des impôts : champs requis");
                        }
                        if (String.IsNullOrWhiteSpace(txtAddress_Street.Text))
                        {
                            bOk = false;
                            sb.AppendLine("Adresse Centre des impôts : champs requis");
                        }
                        int iZipcode;
                        if (String.IsNullOrWhiteSpace(txtAddress_Zipcode.Text))
                        {
                            bOk = false;
                            sb.AppendLine("Code postal Centre des impôts : champs requis");
                        }
                        else if (txtAddress_Zipcode.Text.Trim().Length != 5 || !int.TryParse(txtAddress_Zipcode.Text, out iZipcode))
                        {
                            bOk = false;
                            sb.AppendLine("Code postal Centre des impôts : format non valide");
                        }

                        if (String.IsNullOrWhiteSpace(txtAddress_City.Text))
                        {
                            bOk = false;
                            sb.AppendLine("Ville Centre des impôts : champs requis");
                        }
                    }
                }
                else
                {
                    sb.AppendLine("Type d'envoi : champs requis");
                }
                break;
            case 5:
                if (!String.IsNullOrWhiteSpace(txtIBAN.Text))
                {
                    if(tools.IsIbanChecksumValid(txtIBAN.Text.Trim()))
                        bOk = true;
                    else sb.AppendLine("IBAN trésorerie : non valide");
                }
                else sb.AppendLine("IBAN trésorerie : champ requis");
                break;
            case 6:
                if(!String.IsNullOrWhiteSpace(txtTransferAmount.Text))
                {
                    Decimal dAmount = -1;
                    if (!Decimal.TryParse(txtTransferAmount.Text.Replace('.', ','), NumberStyles.Currency, new CultureInfo("fr-FR"), out dAmount))
                    {
                        sb.AppendLine("Montant à transférer : format non valide");
                        break;
                    }

                    bOk = true;
                }
                else sb.AppendLine("Montant à transférer : champ requis");
                break;
        }

        sErrorMessage = sb.ToString().Replace("\r\n", "<br/>");

        return bOk;
    }

    protected bool SearchCustomer(string sAccountNumber, string sToken, out string sRefCustomer, out string sLastName, out string sFirstName, out string sBirthDate)
    {
        bool bOk = false;
        sFirstName = "";
        sLastName = "";
        sRefCustomer = "";
        sBirthDate = "";

        DataTable dt = Client.GetClientList(sAccountNumber, sToken);

        if(dt.Rows.Count > 0)
        {
            bOk = true;
            sLastName = dt.Rows[0]["LastName"].ToString();
            sFirstName = dt.Rows[0]["FirstName"].ToString();
            sRefCustomer = dt.Rows[0]["RefCustomer"].ToString();
            sBirthDate = dt.Rows[0]["BirthDate"].ToString();
        }

        return bOk;
    }

    protected void InitResponsePanel(decimal dTransferAmount, decimal dAccountBalance)
    {
        string sGarnishmentPath = (ConfigurationManager.AppSettings["GarnishmentPath"] != null) ? ConfigurationManager.AppSettings["GarnishmentPath"].ToString() : "";
        if (File.Exists(System.Web.HttpContext.Current.Server.MapPath("~" + sGarnishmentPath + _Page2)))
        {
            panelFormButton.Visible = false;
            panelResponse.Visible = true;
            upResponse.Update();

            panelReject.Visible = false;
            upRejectBtn.Update();

            tools.AssetInfos assetInfos = new tools.AssetInfos();
            assetInfos.template = sGarnishmentPath + _Page2;

            List<tools.Asset> listAsset = new List<tools.Asset>();
            tools.Asset assetCity = new tools.Asset("text", "Charenton-le-Pont", 0, 0, 0, "");
            tools.Asset assetDate = new tools.Asset("text", DateTime.Now.ToString("dd/MM/yyyy"), 0, 0, 0, "");
            //tools.Asset assetStamp = new tools.Asset("text", "FINANCIERE DES PAIEMENTS ELECTRONIQUES\r\n18 avenue Winston Churchill\r\n94220 CHARENTON LE PONT\r\nTél. 01 75 43 50 00 - Fax : 09 78 16 78 38\r\nSiren 753 886 092 - RCS CRETEIL\r\nEtablissement de Paiement agréé N°16598R", 0, 0, 0, "#AAAAFF");
            tools.Asset assetStamp = new tools.Asset("img", "data:image/gif;base64," + tools.ImageFileToBase64(System.Web.HttpContext.Current.Server.MapPath("~/Styles/Img/FPE_tampon.png")), 0, 0, 0, "");
            tools.Asset assetSign = new tools.Asset("img", "data:image/gif;base64," + tools.ImageFileToBase64(System.Web.HttpContext.Current.Server.MapPath("~/Styles/Img/signature.png")), 0, 0, 0, "");

            if (dTransferAmount == 0)
            {
                tools.Asset assetSolde = _BankStatus != "4"
                    ? new tools.Asset("text", "Solde de "+ dAccountBalance.ToString(new CultureInfo("fr-FR")).Replace('.',',') +" &euro; inférieur au SBI", 0, 0, 0, "")
                    : new tools.Asset("text", "Compte clôturé", 0, 0, 0, "");
                tools.Asset assetX = new tools.Asset("text", "X", 0, 0, 0, "");
                switch (_DocType)
                {
                    case "ATD":
                        assetSolde.top = 65.88M;
                        assetSolde.left = 11.33M;
                        listAsset.Add(assetSolde);
                        assetX.top = 64.62M;
                        assetX.left = 9.55M;
                        listAsset.Add(assetX);

                        assetCity.top = 67.92M;
                        assetCity.left = 56M;
                        listAsset.Add(assetCity);
                        assetDate.top = 67.92M;
                        assetDate.left = 75.33M;
                        listAsset.Add(assetDate);
                        assetStamp.top = 71.7M;
                        assetStamp.left = 47.77M;
                        listAsset.Add(assetStamp);
                        assetSign.top = 69.02M;
                        assetSign.left = 57.34M;
                        assetSign.deg = 12;
                        listAsset.Add(assetSign);
                        break;
                    case "AO":
                        assetSolde.top = 71.07M;
                        assetSolde.left = 4.22M;
                        listAsset.Add(assetSolde);
                        assetX.top = 69.5M;
                        assetX.left = 4.22M;
                        listAsset.Add(assetX);

                        assetCity.top = 72.48M;
                        assetCity.left = 17.33M;
                        listAsset.Add(assetCity);
                        assetDate.top = 72.48M;
                        assetDate.left = 37.11M;
                        listAsset.Add(assetDate);
                        assetStamp.top = 71.07M;
                        assetStamp.left = 56.33M;
                        listAsset.Add(assetStamp);
                        assetSign.top = 70.44M;
                        assetSign.left = 47.56M;
                        assetSign.deg = 12;
                        listAsset.Add(assetSign);
                        break;
                    case "OA1":
                        assetSolde.top = 67.77M;
                        assetSolde.left = 4.44M;
                        listAsset.Add(assetSolde);
                        assetX.top = 64.31M;
                        assetX.left = 4.22M;
                        listAsset.Add(assetX);

                        assetCity.top = 69.81M;
                        assetCity.left = 48.22M;
                        listAsset.Add(assetCity);
                        assetDate.top = 69.81M;
                        assetDate.left = 72.44M;
                        listAsset.Add(assetDate);
                        assetStamp.top = 74.69M;
                        assetStamp.left = 49.33M;
                        listAsset.Add(assetStamp);
                        assetSign.top = 72.01M;
                        assetSign.left = 56.67M;
                        assetSign.deg = 12;
                        listAsset.Add(assetSign);
                        break;
                    case "OA2":
                        assetSolde.top = 61.78M;
                        assetSolde.left = 11.33M;
                        listAsset.Add(assetSolde);
                        assetX.top = 58.81M;
                        assetX.left = 9.33M;
                        listAsset.Add(assetX);

                        assetCity.top = 62.1M;
                        assetCity.left = 64.22M;
                        listAsset.Add(assetCity);
                        assetDate.top = 62.1M;
                        assetDate.left = 81.77M;
                        listAsset.Add(assetDate);
                        assetStamp.top = 67.61M;
                        assetStamp.left = 44.89M;
                        listAsset.Add(assetStamp);
                        assetSign.top = 64.62M;
                        assetSign.left = 58.67M;
                        assetSign.deg = 12;
                        listAsset.Add(assetSign);
                        break;
                }
            }
            else
            {
                switch (_DocType)
                {
                    case "ATD":
                        listAsset.Add(new tools.Asset("text", dTransferAmount.ToString() + " (SBI déduit)", 37.42M, 32.22M, 0, ""));
                        listAsset.Add(new tools.Asset("text", "X", 36.47M, 10M, 0, ""));
                        listAsset.Add(new tools.Asset("text", "&squf; virement", 37.58M, 2.43M, 0, ""));

                        assetCity.top = 68.55M;
                        assetCity.left = 56.89M;
                        listAsset.Add(assetCity);
                        assetDate.top = 68.55M;
                        assetDate.left = 75.55M;
                        listAsset.Add(assetDate);
                        assetStamp.top = 72.32M;
                        assetStamp.left = 56.22M;
                        listAsset.Add(assetStamp);
                        assetSign.top = 70.44M;
                        assetSign.left = 58.67M;
                        assetSign.deg = 12;
                        listAsset.Add(assetSign);
                        break;
                    case "OA1":
                        listAsset.Add(new tools.Asset("text", dTransferAmount.ToString() + " (SBI déduit)", 54.72M, 13.33M, 0, ""));
                        listAsset.Add(new tools.Asset("text", "X", 52.35M, 4M, 0, ""));
                        listAsset.Add(new tools.Asset("text", "————————  virement", 52.36M, 42.66M, 0, ""));

                        assetCity.top = 69.81M;
                        assetCity.left = 48.22M;
                        listAsset.Add(assetCity);
                        assetDate.top = 69.81M;
                        assetDate.left = 72.44M;
                        listAsset.Add(assetDate);
                        assetStamp.top = 74.37M;
                        assetStamp.left = 44.22M;
                        listAsset.Add(assetStamp);
                        assetSign.top = 72.01M;
                        assetSign.left = 56.67M;
                        assetSign.deg = 12;
                        listAsset.Add(assetSign);
                        break;
                    case "AO":
                        listAsset.Add(new tools.Asset("text", dTransferAmount.ToString() + " (SBI déduit)", 31.45M, 61.55M, 0, ""));
                        listAsset.Add(new tools.Asset("text", "X", 29.71M, 4.22M, 0, ""));
                        listAsset.Add(new tools.Asset("text", "&squf; virement", 31.92M, 18.88M, 0, ""));

                        assetCity.top = 71.38M;
                        assetCity.left = 17.33M;
                        listAsset.Add(assetCity);
                        assetDate.top = 71.38M;
                        assetDate.left = 17.33M;
                        listAsset.Add(assetDate);
                        assetStamp.top = 72.95M;
                        assetStamp.left = 54.22M;
                        listAsset.Add(assetStamp);
                        assetSign.top = 71.07M;
                        assetSign.left = 53.56M;
                        assetSign.deg = 12;
                        listAsset.Add(assetSign);
                        break;
                    case "OA2":
                        listAsset.Add(new tools.Asset("text", dTransferAmount.ToString() + " (SBI déduit)", 50.16M, 13.33M, 0, ""));
                        listAsset.Add(new tools.Asset("text", "X", 45.27M, 9.56M, 0, ""));
                        listAsset.Add(new tools.Asset("text", "&squf; virement", 47.96M, 48.44M, 0, ""));

                        assetCity.top = 62.1M;
                        assetCity.left = 64.22M;
                        listAsset.Add(assetCity);
                        assetDate.top = 62.1M;
                        assetDate.left = 81.77M;
                        listAsset.Add(assetDate);
                        assetStamp.top = 67.61M;
                        assetStamp.left = 44.89M;
                        listAsset.Add(assetStamp);
                        assetSign.top = 64.62M;
                        assetSign.left = 58.67M;
                        assetSign.deg = 12;
                        listAsset.Add(assetSign);
                        break;
                }
            }

            assetInfos.assets = listAsset;

            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "InitAssets(" + JsonConvert.SerializeObject(assetInfos) + ");", true);
        }
        else ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Erreur', \"Document détouré introuvable.<br/>Impossible de générer le document de réponse.\");", true);
    }

    protected void btnGenerateResponse_Click(object sender, EventArgs e)
    {
        string sFileName = _Page2.Substring(0, _Page2.LastIndexOf('.')).Trim().Replace(" ", "_");
        //tools.LogToFile(sFileName, "btnGenerateResponse_Click|sFileName");

        if (GenerateResponseFile(sFileName))
        {
            string sGarnishmentResponsePath = (ConfigurationManager.AppSettings["GarnishmentResponsePath"] != null) ? ConfigurationManager.AppSettings["GarnishmentResponsePath"].ToString() : "";
            if (File.Exists(System.Web.HttpContext.Current.Server.MapPath("~" + sGarnishmentResponsePath + sFileName + ".pdf")))
            {
                //tools.LogToFile(sGarnishmentResponsePath + sFileName + ".pdf", "btnGenerateResponse_Click|File exists");
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "ShowDialogCheckResponse('" + sGarnishmentResponsePath + sFileName + ".pdf" + "');", true);
            }
            else
            {
                tools.LogToFile(sGarnishmentResponsePath + sFileName + ".pdf", "btnGenerateResponse_Click|File not exists");
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Erreur', \"Document introuvable.\");", true);
            }
        }
        else
        {
            tools.LogToFile("Generation du fichier KO", "btnGenerateResponse_Click|GenerateResponseFile KO");
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Erreur', \"Une erreur est survenue lors de la génération du fichier.<br/>Veuillez réessayer.\");", true);
        }
    }

    protected void btnValidatePDF_Click(object sender, EventArgs e)
    {
        // Si OK
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "ShowDialogResponseMethod();", true);
    }

    protected bool GenerateResponseFile(string sFileName)
    {
        bool bOk = false;
        string sJSONData = hfAssets.Value;
        string sXmlOut = "";

        tools.AssetInfos infos = JsonConvert.DeserializeObject<tools.AssetInfos>(sJSONData);
        XElement xeAssets = new XElement("ASSETS");

        for (int i = 0; i < infos.assets.Count; i++)
        {
            xeAssets.Add(new XElement("ASSET",
                            new XAttribute("type", infos.assets[i].type),
                            new XAttribute("content", infos.assets[i].content),
                            new XAttribute("top", infos.assets[i].top),
                            new XAttribute("left", infos.assets[i].left),
                            new XAttribute("deg", infos.assets[i].deg)
                            ));
        }

        string sXmlIn = new XElement("ALL_XML_IN",
                            new XElement("GARNISHMENT",
                                new XElement("TEMPLATE", _Page2),
                                new XElement("FILENAME", sFileName),
                                xeAssets)).ToString();

        try
        {
            WS_PDFCreator.PDFCreatorClient ws = new WS_PDFCreator.PDFCreatorClient();
            sXmlOut = ws.CreateGarnishmentResponse(sXmlIn);
            //string sXmlOut = "<ALL_XML_OUT><GARNISHMENT RC=\"0\" /></ALL_XML_OUT>"; // TEST

            List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/GARNISHMENT", "RC");
            if (listRC.Count > 0 && listRC[0] == "0")
                bOk = true;
            else
            {
                tools.LogToFile(sXmlOut, "GenerateResponseFile|" + sFileName);
            }
        }
        catch(Exception ex)
        {
            tools.LogToFile(sXmlOut, "GenerateResponseFile|"+sFileName);
            tools.LogToFile(ex.Message, "GenerateResponseFile|" + sFileName);
            bOk = false;
        }

        return bOk;
    }

    protected void btnTerminate_Click(object sender, EventArgs e)
    {
        XElement xelem = new XElement("Garnishment");
        xelem.Add(new XAttribute("Ref", _RefGarnishment),
                new XAttribute("CashierToken", authentication.GetCurrent().sToken),
                new XAttribute("ResponseMethod", rblResponseMethod.SelectedValue));

        /*if (rblResponseMethod.SelectedValue == "email")
        {
            xelem.Add(new XAttribute("MailBDF", txtEmail.Text));
        }*/

        if (Garnishment.SetGarnishmentDetails(new XElement("ALL_XML_IN", xelem).ToString()))
        {
            if (Garnishment.SetGarnishmentStatus(_RefGarnishment, authentication.GetCurrent().sToken, "CERTIFIED", ""))
            {
                Session["GarnishmentSaved"] = "1";
                Response.Redirect("GarnishmentDetails.aspx?ref=" + _RefGarnishment);
            }
            else ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Erreur', \"Une erreur est survenue lors de l'enregistrement des informations.<br/>Veuillez réessayer.\");", true);
        }
        else ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Erreur', \"Une erreur est survenue lors de l'enregistrement des informations.<br/>Veuillez réessayer.\");", true);
    }

    protected void btnRetreat_Click(object sender, EventArgs e)
    {
        if (Garnishment.SetGarnishmentStatus(_RefGarnishment, authentication.GetCurrent().sToken, "TORETREAT", ""))
        {
            Response.Redirect("GarnishmentSearch.aspx");
        }
        else ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Erreur', \"Une erreur est survenue lors de l'enregistrement.<br/>Veuillez réessayer.\"); InitBlur();", true);
    }

    protected void btnReject_Click(object sender, EventArgs e)
    {
        string sReason = (ddlRejectReason.SelectedValue.Length > 0) ? ddlRejectReason.SelectedValue : txtReason.Text;

        if (Garnishment.SetGarnishmentStatus(_RefGarnishment, authentication.GetCurrent().sToken, "REJECTED", sReason))
        {
            Response.Redirect("GarnishmentSearch.aspx?status=Rejected");
        }
        else ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Erreur', \"Une erreur est survenue lors de l'enregistrement.<br/>Veuillez réessayer.\"); InitBlur();", true);
    }

    protected void btnSearchClient_Click(object sender, EventArgs e)
    {
        if (txtName.Text.Length > 0)
        {
            string sToken = authentication.GetCurrent(false).sToken;
            DataTable dt = Client.GetClientList("", "", "", "", "", txtName.Text, sToken);

            if (dt.Rows.Count > 0)
            {
                if(dt.Rows.Count == 1)
                {
                    txtAccountNumber.Text = dt.Rows[0]["AccountNumber"].ToString();
                    UpdateClientPanel(dt.Rows[0]["RefCustomer"].ToString(), dt.Rows[0]["LastName"].ToString(), dt.Rows[0]["FirstName"].ToString(), dt.Rows[0]["BirthDate"].ToString());
                }
                else
                {
                    rptSearchResult.DataSource = dt;
                    rptSearchResult.DataBind();
                    UpdateClientPanel("", "", "", "");
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "ShowSearchResult();", true);
                }
            }
            else ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Aucun client trouvé', \"Votre recherche ne retourne aucun résultat.<br/>Veuillez vérifier la saisie et réessayer.\", null, null);", true);

            txtName.Text = "";
        }
        else ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Erreur', \"Recherche : champ requis\", null, null);", true);

        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "InitBlur();", true);
    }

    protected void btnCancelRejection_Click(object sender, EventArgs e)
    {
        if (Garnishment.SetGarnishmentStatus(_RefGarnishment, authentication.GetCurrent().sToken, "CANCELLEDREJECTION", ""))
        {
            Response.Redirect("GarnishmentDetails.aspx?ref="+_RefGarnishment);
        }
        else
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Erreur', \"Une erreur est survenue lors de l'enregistrement.<br/>Veuillez réessayer.\"); InitBlur();", true);
    }

    protected void btnRejectFinal_Click(object sender, EventArgs e)
    {
        if (Garnishment.SetGarnishmentStatus(_RefGarnishment, authentication.GetCurrent().sToken, "REJECTEDFINAL", ""))
        {
            Response.Redirect("GarnishmentDetails.aspx?ref=" + _RefGarnishment);
        }
        else
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Erreur', \"Une erreur est survenue lors de l'enregistrement.<br/>Veuillez réessayer.\"); InitBlur();", true);
    }

    protected void btnSaveModificationPostTreatment_Click(object sender, EventArgs e)
    {
        string sErrorMessage;
        if(CheckForm(1, out sErrorMessage))
        {
            if(CheckForm(3, out sErrorMessage))
            {
                XElement xelem = new XElement("Garnishment");
                xelem.Add(new XAttribute("Ref", _RefGarnishment),
                new XAttribute("CashierToken", authentication.GetCurrent().sToken));

                xelem.Add(
                new XAttribute("DocType", ddlGarnishmentType.SelectedValue));

                xelem.Add(
                new XAttribute("ExternalRef", txtExternalRef.Text.Trim()));

                if (Garnishment.SetGarnishmentDetails(new XElement("ALL_XML_IN", xelem).ToString()))
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Succès', \"<span style='color:green'>L'enregistrement a été effectué.</span>\");", true);
                    //panelSaveModificationPostTreatment.Visible = false;
                    //ddlGarnishmentType.Enabled = false;
                    //txtExternalRef.Enabled = false;
                }
                else sErrorMessage = "Erreur lors de l'enregistrement.<br/>Veuillez réessayer.";
            }
        }

        if(!String.IsNullOrWhiteSpace(sErrorMessage))
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Erreur', \"" + sErrorMessage + "\");", true);
    }
}
