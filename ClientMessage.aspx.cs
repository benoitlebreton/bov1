﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using XMLMethodLibrary;

public partial class ClientMessage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            CheckRights();
        }

        CreateMessage2.validated += new EventHandler(UpdateHistory);
        MessageHistory1.unpublished += new EventHandler(UpdateHistory);
    }

    protected void CheckRights()
    {
        bool bDirect = true;
        bool bHome = true;

        string sXmlOut = tools.GetRights(authentication.GetCurrent().sToken, "SC_ClientMessage");

        List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Page", "RC");

        if (listRC.Count == 0 || listRC[0] != "0")
        {
            //Response.Redirect("Default.aspx", true);
            bDirect = false;
        }
        else
        {
            List<string> listAction = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/Page/Action");

            for (int i = 0; i < listAction.Count; i++)
            {
                List<string> listTagAction = CommonMethod.GetAttributeValues(listAction[i], "Action", "TagAction");
                List<string> listActionAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ActionAllowed");

                if (listTagAction.Count > 0)
                {
                    switch (listTagAction[0])
                    {
                        case "ClientMessage":
                            if (listActionAllowed.Count > 0 && listActionAllowed[0] == "1")
                            {
                                CreateMessage1.ActionAllowed = true;
                                CreateMessage2.ActionAllowed = true;
                            }
                            else
                            {
                                CreateMessage1.ActionAllowed = false;
                                CreateMessage2.ActionAllowed = false;
                            }
                            break;
                    }
                }
            }
        }

        sXmlOut = tools.GetRights(authentication.GetCurrent().sToken, "SC_HomeMessage");

        listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Page", "RC");

        if (listRC.Count == 0 || listRC[0] != "0")
        {
            //Response.Redirect("Default.aspx", true);
            bHome = false;
        }
        else
        {
            List<string> listAction = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/Page/Action");

            for (int i = 0; i < listAction.Count; i++)
            {
                List<string> listTagAction = CommonMethod.GetAttributeValues(listAction[i], "Action", "TagAction");
                List<string> listActionAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ActionAllowed");

                if (listTagAction.Count > 0)
                {
                    switch (listTagAction[0])
                    {
                        case "ManageHomeMessage":
                            if (listActionAllowed.Count > 0 && listActionAllowed[0] == "1")
                            {
                                CreateMessage1.ActionAllowed = true;
                                MessageHistory1.ActionAllowed = true;
                            }
                            else
                            {
                                CreateMessage1.ActionAllowed = false;
                                MessageHistory1.ActionAllowed = false;
                            }
                            break;
                    }
                }
            }
        }

        if(!bDirect && !bHome)
            Response.Redirect("Default.aspx", true);
        else if (bDirect && bHome)
        {
            panelHomeMessage.Visible = true;
            rblMessageType.SelectedIndex = 0;
        }
        else
        {
            rblMessageType.Visible = false;
            if (bDirect)
                panelDirectMessage.Visible = true;
            else if (bHome)
                panelHomeMessage.Visible = true;
        }
    }

    protected void UpdateHistory(object sender, EventArgs e)
    {
        MessageHistory1.SetMessages();
        upHistory.Update();
    }
    protected void rblMessageType_SelectedIndexChanged(object sender, EventArgs e)
    {
        panelHomeMessage.Visible = false;
        panelDirectMessage.Visible = false;

        if (rblMessageType.SelectedValue == "home")
            panelHomeMessage.Visible = true;
        else if (rblMessageType.SelectedValue == "dm")
            panelDirectMessage.Visible = true;
    }
}