﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Period2 : System.Web.UI.UserControl
{
    #region parameters
    public String sBeginDate
    {
        get { return txtBeginDate.Text; }
        set { txtBeginDate.Text = value; }
    }

    public String sEndDate
    {
        get { return txtEndDate.Text; }
        set { txtEndDate.Text = value; }
    }

    public string sPeriod
    {
        get { return rblPredifinedPeriod.SelectedValue; }
        set { rblPredifinedPeriod.SelectedValue = value; }
    }

    public RadioButtonList PredifinedPeriodChoice
    {
        get { return rblPredifinedPeriod; }
        set { rblPredifinedPeriod = value; }
    }

    public bool isPeriodBold
    {
        get { return lblPeriod.Font.Bold; }
        set { lblPeriod.Font.Bold = value; }
    }

    public bool isValid
    {
        get { return isPeriodValid(); }
    }

    public event EventHandler PeriodChanged;

    #endregion

    protected bool isPeriodValid()
    {
        bool bIsValid = true;
        return bIsValid;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "date_picker();RadioButtonClassPeriod();", true);

        if (!IsPostBack)
        {

            if (rblPredifinedPeriod.SelectedValue == "")
            {
                rblPredifinedPeriod.Items[0].Selected = true;
            }

            switch (rblPredifinedPeriod.SelectedValue)
            {
                //Dernier jour
                case "0":
                    getLastDay();
                    //MaskedEditValidator5.Enabled = false;
                    //MaskedEditValidator1.Enabled = false;
                    break;

                //Dernière semaine
                case "1":
                    getLastWeek();
                    //MaskedEditValidator5.Enabled = false;
                    //MaskedEditValidator1.Enabled = false;
                    break;

                //Dernier mois 
                case "2":
                    getLastMonth();
                    //MaskedEditValidator5.Enabled = false;
                    //MaskedEditValidator1.Enabled = false;
                    break;

                //Personnalisée
                case "3":
                    getCustomizedDate();
                    //MaskedEditValidator5.Enabled = true;
                    //MaskedEditValidator1.Enabled = true;
                    //txtBeginDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                    //txtEndDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                    break;

                //Aujourd'hui
                case "4":
                    getToday();
                    //MaskedEditValidator5.Enabled = false;
                    //MaskedEditValidator1.Enabled = false;
                    break;
                    //30 derniers jours
                case "5":
                    txtEndDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                    txtBeginDate.Text = DateTime.Now.AddDays(-29).ToString("dd/MM/yyyy");
                    txtBeginDate.Enabled = false;
                    txtEndDate.Enabled = false;
                    break;

            }
        }
        else
        {
            string[] eventArgs = Page.Request["__EVENTARGUMENT"].ToString().Split(';');
            if (eventArgs.Length > 0 && eventArgs[0].ToString().ToLower() == "datechanged")
            {
                PeriodChanged(null, null);
            }
        }
    }

    protected void onChangeRblPredifinedPeriod(object sender, EventArgs e)
    {
        string beginDate = "";
        string endDate = "";
        getDateFromPeriodValue(rblPredifinedPeriod.SelectedValue, out beginDate, out endDate);
        PeriodChanged(null, null);
    }

    public bool getDateFromPeriodValue(string sPeriodValue, out string DateFrom, out string DateTo)
    {
        bool isOK = false;

        switch (sPeriodValue)
        {
            //Dernier jour
            case "0":
                getLastDay();
                isOK = true;
                break;

            //Dernière semaine
            case "1":
                getLastWeek();
                isOK = true;
                break;

            //Dernier mois 
            case "2":
                getLastMonth();
                isOK = true;
                break;

            //Personnalisée
            case "3":
                getCustomizedDate();
                txtBeginDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtEndDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                isOK = true;
                break;

            case "4":
                getToday();
                isOK = true;
                break;

            case "5":
                txtBeginDate.Enabled = false;
                txtEndDate.Enabled = false;
                txtEndDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtBeginDate.Text = DateTime.Now.AddDays(-29).ToString("dd/MM/yyyy");
                isOK = true;
                break;
        }

        DateFrom = txtBeginDate.Text;
        DateTo = txtEndDate.Text;

        return isOK;
    }

    protected string getFirstDayOfLastWeek(string sDay)
    {
        string sFirstDay = "";
        string sJour = Convert.ToDateTime(sDay).ToString("dddd", new System.Globalization.CultureInfo("en-US"));

        switch (sJour.ToLower())
        {
            case "monday":
                sFirstDay = Convert.ToDateTime(sDay).AddDays(-7).ToString("dd/MM/yyyy");
                break;

            case "tuesday":
                sFirstDay = Convert.ToDateTime(sDay).AddDays(-8).ToString("dd/MM/yyyy");
                break;

            case "wednesday":
                sFirstDay = Convert.ToDateTime(sDay).AddDays(-9).ToString("dd/MM/yyyy");
                break;

            case "thursday":
                sFirstDay = Convert.ToDateTime(sDay).AddDays(-10).ToString("dd/MM/yyyy");
                break;

            case "friday":
                sFirstDay = Convert.ToDateTime(sDay).AddDays(-11).ToString("dd/MM/yyyy");
                break;

            case "saturday":
                sFirstDay = Convert.ToDateTime(sDay).AddDays(-12).ToString("dd/MM/yyyy");
                break;

            case "sunday":
                sFirstDay = Convert.ToDateTime(sDay).AddDays(-13).ToString("dd/MM/yyyy");
                break;
        }

        return sFirstDay;
    }

    protected string getLastDayOfLastWeek(string sDay)
    {
        string sLastDay = "";

        string sJour = Convert.ToDateTime(sDay).ToString("dddd", new System.Globalization.CultureInfo("fr-FR"));

        switch (sJour.ToLower())
        {
            case "lundi":
                sLastDay = Convert.ToDateTime(sDay).AddDays(-1).ToString("dd/MM/yyyy");
                break;

            case "mardi":
                sLastDay = Convert.ToDateTime(sDay).AddDays(-2).ToString("dd/MM/yyyy");
                break;

            case "mercredi":
                sLastDay = Convert.ToDateTime(sDay).AddDays(-3).ToString("dd/MM/yyyy");
                break;

            case "jeudi":
                sLastDay = Convert.ToDateTime(sDay).AddDays(-4).ToString("dd/MM/yyyy");
                break;

            case "vendredi":
                sLastDay = Convert.ToDateTime(sDay).AddDays(-5).ToString("dd/MM/yyyy");
                break;

            case "samedi":
                sLastDay = Convert.ToDateTime(sDay).AddDays(-6).ToString("dd/MM/yyyy");
                break;

            case "dimanche":
                sLastDay = Convert.ToDateTime(sDay).AddDays(-7).ToString("dd/MM/yyyy");
                break;
        }

        //sLastDay = Convert.ToDateTime(sLastDay).ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("fr-FR"));

        return sLastDay;
    }

    protected void getToday()
    {
        string sBeginDateTemp = "";
        string sEndDateTemp = "";

        sBeginDateTemp = DateTime.Now.ToString("dd/MM/yyyy");
        sEndDateTemp = sBeginDateTemp;

        txtBeginDate.Text = sBeginDateTemp;
        txtEndDate.Text = sEndDateTemp;

        txtBeginDate.Enabled = false;
        txtEndDate.Enabled = false;
    }

    protected void getLastDay()
    {
        string sToday = DateTime.Now.ToString("dd/MM/yyyy");
        string sBeginDateTemp = "";
        string sEndDateTemp = "";

        sBeginDateTemp = Convert.ToDateTime(sToday).AddDays(-1).ToString("dd/MM/yyyy");
        sEndDateTemp = sBeginDateTemp;

        txtBeginDate.Text = sBeginDateTemp;
        txtEndDate.Text = sEndDateTemp;

        txtBeginDate.Enabled = false;
        txtEndDate.Enabled = false;
    }

    protected void getLastWeek()
    {
        string sToday = DateTime.Now.ToString("dd/MM/yyyy");

        txtBeginDate.Text = getFirstDayOfLastWeek(sToday);
        txtEndDate.Text = getLastDayOfLastWeek(sToday);

        txtBeginDate.Enabled = false;
        txtEndDate.Enabled = false;
    }

    protected void getLastMonth()
    {
        string sToday = DateTime.Now.ToString("dd/MM/yyyy");

        txtBeginDate.Text = getFirstDayOfLastMonth(sToday);
        txtEndDate.Text = getLastDayOfLastMonth(sToday);

        txtEndDate.Enabled = false;
        txtBeginDate.Enabled = false;
    }

    protected void getCustomizedDate()
    {
        txtBeginDate.Enabled = true;
        txtEndDate.Enabled = true;
    }

    protected string getFirstDayOfLastMonth(string sDay)
    {
        string sFirstDay = "";

        sFirstDay = Convert.ToDateTime(sDay).AddMonths(-1).ToString("dd/MM/yyyy");
        sFirstDay = "01" + sFirstDay.Substring(2, 8);

        return sFirstDay;
    }

    protected string getLastDayOfLastMonth(string sDay)
    {
        string sLastDay = "";

        sLastDay = "01" + sDay.Substring(2, 8);
        sLastDay = Convert.ToDateTime(sLastDay).AddDays(-1).ToString("dd/MM/yyyy");
        
        return sLastDay;
    }


    protected void btnCustomPeriod_Click(object sender, EventArgs e)
    {
        PeriodChanged(null, null);
    }
}

