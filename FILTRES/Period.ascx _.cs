﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Period : System.Web.UI.UserControl
{

    public String sBeginDate
    {
        get { return txtBeginDate.Text; }
        set { txtBeginDate.Text = value; }
    }

    public String sEndDate
    {
        get { return txtEndDate.Text; }
        set { txtEndDate.Text = value; }
    }

    public RadioButtonList PredifinedPeriodChoice
    {
        get { return rblPredifinedPeriod; }
        set { rblPredifinedPeriod = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        /*if (rbLastDay.Checked == true)
        {
            getLastDay();
        }
        else if (rbLastWeek.Checked == true)
        {
            getLastWeek();
        }
        else if (rbLastMonth.Checked == true)
        {
            getLastMonth();
        }
        else if (rbCustomized.Checked == true)
        {
            getCustomizedDate();
        }*/
        if (IsPostBack != true)
        {
            if (rblPredifinedPeriod.SelectedValue == "")
            {
                rblPredifinedPeriod.Items[0].Selected = true;
            }
            
            switch (rblPredifinedPeriod.SelectedValue)
            {
                //Dernier jour
                case "0":
                    getLastDay();
                    MaskedEditValidator5.Enabled = false;
                    MaskedEditValidator1.Enabled = false;
                    break;

                //Dernière semaine
                case "1":
                    getLastWeek();
                    MaskedEditValidator5.Enabled = false;
                    MaskedEditValidator1.Enabled = false;
                    break;

                //Dernier mois 
                case "2":
                    getLastMonth();
                    MaskedEditValidator5.Enabled = false;
                    MaskedEditValidator1.Enabled = false;
                    break;

                //Personnalisée
                case "3":
                    getCustomizedDate();
                    MaskedEditValidator5.Enabled = true;
                    MaskedEditValidator1.Enabled = true;
                    txtBeginDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                    txtEndDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                    break;
            }
        }

        if (rblPredifinedPeriod.SelectedValue == "3")
        {
            txtBeginDate.Enabled = true;
            txtEndDate.Enabled = true;
        }
    }

    protected void onChangeRblPredifinedPeriod(object sender, EventArgs e)
    {
        /*if (rbLastDay.Checked == true)
        {
            getLastDay();
        }
        else if (rbLastWeek.Checked == true)
        {
            getLastWeek();
        }
        else if (rbLastMonth.Checked == true)
        {
            getLastMonth();
        }
        else if (rbCustomized.Checked == true)
        {
            getCustomizedDate();
        }*/

        switch (rblPredifinedPeriod.SelectedValue)
        {
            //Dernier jour
            case "0":
                getLastDay();

                MaskedEditValidator5.Enabled = false;
                MaskedEditValidator1.Enabled = false;
                break;

            //Dernière semaine
            case "1":
                getLastWeek();

                MaskedEditValidator5.Enabled = false;
                MaskedEditValidator1.Enabled = false;
                break;

            //Dernier mois 
            case "2":
                getLastMonth();

                MaskedEditValidator5.Enabled = false;
                MaskedEditValidator1.Enabled = false;
                break;
                
            //Personnalisée
            case "3":
                getCustomizedDate();
                MaskedEditValidator5.Enabled = true;
                MaskedEditValidator1.Enabled = true;
                txtBeginDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtEndDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtBeginDate.Enabled = true;
                txtEndDate.Enabled = true;
                break;
        }
    }

    protected string getFirstDayOfLastWeek(string sDay)
    {
        string sFirstDay = "";
        string sJour = Convert.ToDateTime(sDay).ToString("dddd", new System.Globalization.CultureInfo("en-US"));

        switch (sJour.ToLower())
        {
            case "monday":
                sFirstDay = Convert.ToDateTime(sDay).AddDays(-7).ToString("dd/MM/yyyy");
                break;

            case "tuesday":
                sFirstDay = Convert.ToDateTime(sDay).AddDays(-8).ToString("dd/MM/yyyy");
                break;

            case "wednesday":
                sFirstDay = Convert.ToDateTime(sDay).AddDays(-9).ToString("dd/MM/yyyy");
                break;

            case "thursday":
                sFirstDay = Convert.ToDateTime(sDay).AddDays(-10).ToString("dd/MM/yyyy");
                break;

            case "friday":
                sFirstDay = Convert.ToDateTime(sDay).AddDays(-11).ToString("dd/MM/yyyy");
                break;

            case "saturday":
                sFirstDay = Convert.ToDateTime(sDay).AddDays(-12).ToString("dd/MM/yyyy");
                break;

            case "sunday":
                sFirstDay = Convert.ToDateTime(sDay).AddDays(-13).ToString("dd/MM/yyyy");
                break;
        }

        return sFirstDay;
    }

    protected string getLastDayOfLastWeek(string sDay)
    {
        string sLastDay = "";

        string sJour = Convert.ToDateTime(sDay).ToString("dddd", new System.Globalization.CultureInfo("fr-FR"));

        switch (sJour.ToLower())
        {
            case "lundi":
                sLastDay = Convert.ToDateTime(sDay).AddDays(-1).ToString("dd/MM/yyyy");
                break;

            case "mardi":
                sLastDay = Convert.ToDateTime(sDay).AddDays(-2).ToString("dd/MM/yyyy");
                break;

            case "mercredi":
                sLastDay = Convert.ToDateTime(sDay).AddDays(-3).ToString("dd/MM/yyyy");
                break;

            case "jeudi":
                sLastDay = Convert.ToDateTime(sDay).AddDays(-4).ToString("dd/MM/yyyy");
                break;

            case "vendredi":
                sLastDay = Convert.ToDateTime(sDay).AddDays(-5).ToString("dd/MM/yyyy");
                break;

            case "samedi":
                sLastDay = Convert.ToDateTime(sDay).AddDays(-6).ToString("dd/MM/yyyy");
                break;

            case "dimanche":
                sLastDay = Convert.ToDateTime(sDay).AddDays(-7).ToString("dd/MM/yyyy");
                break;
        }

        //sLastDay = Convert.ToDateTime(sLastDay).ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("fr-FR"));

        return sLastDay;
    }

    protected void getLastDay()
    {
        string sToday = DateTime.Now.ToString("dd/MM/yyyy");
        string sBeginDateTemp = "";
        string sEndDateTemp = "";

        sBeginDateTemp = Convert.ToString(Convert.ToDateTime(sToday).AddDays(-1));
        sEndDateTemp = sBeginDateTemp;

        txtBeginDate.Text = sBeginDateTemp;
        txtEndDate.Text = sEndDateTemp;

        txtBeginDate.Enabled = false;
        txtEndDate.Enabled = false;
        CalendarExtender1.Enabled = false;
        CalendarExtender2.Enabled = false;
        ImgBntCalc1.Visible = false;
        ImgBntCalc2.Visible = false;
    }

    protected void getLastWeek()
    {
        string sToday = DateTime.Now.ToString("dd/MM/yyyy");

        txtBeginDate.Text = getFirstDayOfLastWeek(sToday);
        txtEndDate.Text = getLastDayOfLastWeek(sToday);

        txtBeginDate.Enabled = false;
        txtEndDate.Enabled = false;
        CalendarExtender1.Enabled = false;
        CalendarExtender2.Enabled = false;
        ImgBntCalc1.Visible = false;
        ImgBntCalc2.Visible = false;
    }

    protected void getLastMonth()
    {
        string sToday = DateTime.Now.ToString("dd/MM/yyyy");

        txtBeginDate.Text = getFirstDayOfLastMonth(sToday);
        txtEndDate.Text = getLastDayOfLastMonth(sToday);

        txtEndDate.Enabled = false;
        txtBeginDate.Enabled = false;
        CalendarExtender1.Enabled = false;
        CalendarExtender2.Enabled = false;
        ImgBntCalc1.Visible = false;
        ImgBntCalc2.Visible = false;
    }

    protected void getCustomizedDate()
    {
        txtBeginDate.Text = "";
        txtEndDate.Text = "";

        txtBeginDate.Enabled = true;
        txtEndDate.Enabled = true;
        CalendarExtender1.Enabled = true;
        CalendarExtender2.Enabled = true;
        ImgBntCalc1.Visible = true;
        ImgBntCalc2.Visible = true;
    }

    protected string getFirstDayOfLastMonth(string sDay)
    {
        string sFirstDay = "";

        sFirstDay = Convert.ToDateTime(sDay).AddMonths(-1).ToString("dd/MM/yyyy");
        sFirstDay = "01" + sFirstDay.Substring(2, 8);

        return sFirstDay;
    }

    protected string getLastDayOfLastMonth(string sDay)
    {
        string sLastDay = "";

        sLastDay = "01" + sDay.Substring(2, 8);
        sLastDay = Convert.ToDateTime(sLastDay).AddDays(-1).ToString("dd/MM/yyyy");
        
        return sLastDay;
    }

}

