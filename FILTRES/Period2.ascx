﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Period2.ascx.cs" Inherits="Period2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:UpdatePanel ID="updatePanel_Filter" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
    <ContentTemplate>
        <script language="javascript" type="text/javascript">

            function RadioButtonClassPeriod() {
                $("#<%=rblPredifinedPeriod.ClientID %>").buttonset();
            };

            function date_picker() {
                $('#<%=txtEndDate.ClientID %>').mask("99/99/9999");
                $('#<%=txtBeginDate.ClientID %>').mask("99/99/9999");

                $.datepicker.setDefaults($.datepicker.regional["fr"]);

                $('#<%=txtBeginDate.ClientID %>').datepicker($.datepicker.regional['fr'] = {
                    closeText: 'Fermer',
                    prevText: '&#x3c;Préc',
                    nextText: 'Suiv&#x3e;',
                    currentText: 'Courant',
                    monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
                    monthNamesShort: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
                    dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
                    dayNamesShort: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],
                    dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
                    weekHeader: 'Sm',
                    dateFormat: 'dd/mm/yy',
                    firstDay: 1,
                    isRTL: false,
                    showMonthAfterYear: false,
                    yearSuffix: '',
                    changeMonth: true,
                    changeYear: true,
                    defaultDate: 0,
                    minDate: new Date(2013, 1 - 1, 1)
                });


                $('#<%=txtEndDate.ClientID %>').datepicker($.datepicker.regional['fr'] = {
                    closeText: 'Fermer',
                    prevText: '&#x3c;Préc',
                    nextText: 'Suiv&#x3e;',
                    currentText: 'Courant',
                    monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
                    monthNamesShort: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
                    //monthNamesShort: ['Jan', 'Fév', 'Mar', 'Avr', 'Mai', 'Jun', 'Jul', 'Aoû', 'Sep', 'Oct', 'Nov', 'Déc'],
                    dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
                    dayNamesShort: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],
                    dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
                    weekHeader: 'Sm',
                    dateFormat: 'dd/mm/yy',
                    firstDay: 1,
                    isRTL: false,
                    showMonthAfterYear: false,
                    yearSuffix: '',
                    changeMonth: true,
                    changeYear: true,
                    defaultDate: 0,
                    minDate: new Date(2013, 1 - 1, 1)
                });

                $('#<%=txtEndDate.ClientID %>').change(function () {
                    try {
                        showLoading();
                        $.datepicker.parseDate('dd/mm/yy', $('#<%=txtEndDate.ClientID %>').val());
                        //__doPostBack("MainContent_searchFilter_RegistrationSearchByAgencyPeriod_updatePanel_Filter", "dateChanged");
                        $('#<%=btnCustomPeriod.ClientID%>').click();
                    } catch (e) {
                        $('#EndDateValidation').css('visibility', 'visible');
                    }
                });

                $('#<%=txtBeginDate.ClientID %>').change(function () {
                    try {
                        showLoading();
                        $.datepicker.parseDate('dd/mm/yy', $('#<%=txtBeginDate.ClientID %>').val());
                        //__doPostBack("MainContent_searchFilter_RegistrationSearchByAgencyPeriod_updatePanel_Filter", "dateChanged");
                        $('#<%=btnCustomPeriod.ClientID%>').click();
                    } catch (e) {
                        $('#BeginDateValidation').css('visibility', 'visible');
                    }
                });

            }
                
        </script>
        <div id="divPeriod" runat="server" style="width: 100%; margin: auto; padding: 0px 0 20px 0;
            z-index: 99">
            <div style="display: table; padding: 0; width: 100%;">
                <div style="display: table-row; padding: 0;">
                    <div style="width: 15%; display: table-cell; padding: 0; height: 14px; margin: 0;
                        vertical-align: middle">
                        <asp:Label runat="server" ID="lblPeriod" Font-Bold="False">Période</asp:Label>
                    </div>
                </div>
                <div style="display: table-row; padding: 0; margin-left: -20px;">
                    <div style="width: 100%; display: table-cell; padding: 0; margin: 0; height: 14px;
                        vertical-align: middle">
                        <asp:RadioButtonList runat="server" ID="rblPredifinedPeriod" onchange="showLoading();" OnSelectedIndexChanged="onChangeRblPredifinedPeriod"
                            RepeatDirection="Horizontal" AutoPostBack="true" CssClass="RadioButtonList" Height="14px"
                            Style="padding: 0;">
                            <asp:ListItem Value="4">Aujourd'hui</asp:ListItem>
                            <asp:ListItem Value="0">Hier</asp:ListItem>
                            <asp:ListItem Value="1">Semaine dernière</asp:ListItem>
                            <asp:ListItem Value="5">30 derniers jours</asp:ListItem>
                            <asp:ListItem Value="2">Mois dernier</asp:ListItem>
                            <asp:ListItem Value="3">Personnalisée</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                </div>
            </div>
            <div style="float: right;display: table;width: 35%;text-align: right;padding: 0;margin: 0;margin-right: 50px;">
                <div style="margin: 0; padding: 0; display: table-row; height: 14px;">
                    <div style="width: 50%; display: table-cell; margin: 0; padding: 0; height: 14px;">
                        Du<span id="BeginDateValidation" style="font-weight: bold; color: red; font-size: 24px;
                            visibility: hidden">*</span><span style="font-style: italic"> (jj/mm/aaaa)</span>
                    </div>
                    <div style="width: 50%; display: table-cell; margin: 0; padding: 0; height: 14px;">
                        Au<span id="EndDateValidation" style="font-weight: bold; color: red; font-size: 24px;
                            visibility: hidden">*</span><span style="font-style: italic"> (jj/mm/aaaa)</span>
                    </div>
                </div>
                <div style="margin: 0; padding: 0; display: table-row">
                    <div style="width: 50%; display: table-cell; vertical-align: bottom">
                        <asp:TextBox runat="server" ID="txtBeginDate" MaxLength="1" ValidationGroup="MKE"
                            Width="110px" Style="text-align: center"></asp:TextBox>
                    </div>
                    <div style="width: 50%; display: table-cell; vertical-align: bottom">
                        <asp:TextBox runat="server" ID="txtEndDate" MaxLength="1" ValidationGroup="MKE" Width="110px"
                            Style="text-align: center"></asp:TextBox>
                    </div>
                </div>
                <asp:Button ID="btnCustomPeriod" runat="server" OnClick="btnCustomPeriod_Click" style="display:none" />
            </div>
        </div>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="rblPredifinedPeriod" EventName="SelectedIndexChanged" />
        <asp:AsyncPostBackTrigger ControlID="btnCustomPeriod" EventName="Click" />
    </Triggers>
</asp:UpdatePanel>
