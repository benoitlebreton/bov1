﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Period.ascx.cs" Inherits="Period" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<div id="divPeriod" runat="server" style="width: 100%; margin-top: 10px; padding-bottom: 20px;">
    <div style="display: table">
        <div style="display: table-row">
            <div style="width: 15%; display: table-cell; padding: 0; height: 20px; margin: 0;
                vertical-align: middle">
                Période :</div>
            <div style="width: 80%; display: table-cell; padding: 0; margin: 0; height: 20px">
                <%--<input runat="server" id="rbLastDay" checked="True" type="radio" name="rblPredifinedPeriod" value="0" ="onChangeRblPredifinedPeriod" /><label id="lblLastDay" for="<%=rbLastDay.ClientID %>">Hier</label>
                <input runat="server" id="rbLastWeek" type="radio" name="rblPredifinedPeriod" value="1" onselect="onChangeRblPredifinedPeriod"/><label id="lblLastWeek" for="<%=rbLastWeek.ClientID %>">Semaine dernière</label>
                <input runat="server" id="rbLastMonth" type="radio" name="rblPredifinedPeriod" value="2" onselect="onChangeRblPredifinedPeriod"/><label id="lblLastMonth" for="<%=rbLastMonth.ClientID %>">Mois dernier</label>
                <input runat="server" id="rbCustomized" type="radio" name="rblPredifinedPeriod" value="3" onselect="onChangeRblPredifinedPeriod"/><label id="lblCustomized" for="<%=rbCustomized.ClientID %>">Personnalisée</label>--%>
                <asp:RadioButtonList runat="server" ID="rblPredifinedPeriod" OnSelectedIndexChanged="onChangeRblPredifinedPeriod"
                    RepeatDirection="Horizontal" AutoPostBack="true" CssClass="RadioButtonList" Height="20px"
                    CellSpacing="10">
                    <asp:ListItem Value="0">Hier</asp:ListItem>
                    <asp:ListItem Value="1">Semaine dernière</asp:ListItem>
                    <asp:ListItem Value="2">Mois dernier</asp:ListItem>
                    <asp:ListItem Value="3">Personnalisée</asp:ListItem>
                </asp:RadioButtonList>
            </div>
        </div>
    </div>
    <div style="display: table">
        <div style="margin: 0; padding: 0; display: table-row">
            <div style="width: 15%; display: table-cell;">
                Du <span style="font-style: italic">(jj/mm/aaaa)</span>
            </div>
            <div style="width: 25%; display: table-cell; ">
                <asp:TextBox runat="server" ID="txtBeginDate" MaxLength="1" ValidationGroup="MKE"
                    Width="80px"></asp:TextBox>
                <asp:ImageButton ID="ImgBntCalc1" runat="server" ImageUrl="~/Img/calendar.png" CausesValidation="False"
                    AlternateText="Cal" ToolTip="<%=Resources.res.OpenCalendar %>" Width="22px" Style="vertical-align: middle" />
                <asp:RequiredFieldValidator runat="server" ID="reqBeginDate" ControlToValidate="txtBeginDate"
                    CssClass="reqStarStyle" ValidationGroup="MKE">*</asp:RequiredFieldValidator>
                <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender1" runat="server" TargetControlID="txtBeginDate"
                    Mask="99/99/9999" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                    OnInvalidCssClass="MaskedEditError" MaskType="Date" DisplayMoney="Left" AcceptNegative="Left"
                    ErrorTooltipEnabled="True" />
                <ajaxToolkit:MaskedEditValidator ID="MaskedEditValidator1" runat="server" ControlExtender="MaskedEditExtender1"
                    ControlToValidate="txtBeginDate" InvalidValueMessage="Date invalide" Display="Dynamic"  ForeColor="Red"
                    EmptyValueBlurredText="*" InvalidValueBlurredMessage="Date invalide" ValidationGroup="MKE" />
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtBeginDate"
                    PopupButtonID="ImgBntCalc1" Format="dd/MM/yyyy" />
            </div>
            <div style="width: 15%; display: table-cell;">
                Au <span style="font-style: italic">(jj/mm/aaaa)</span>
            </div>
            <div style="width: 25%; display: table-cell;">
                <asp:TextBox runat="server" ID="txtEndDate" MaxLength="1" ValidationGroup="MKE" Width="80px"></asp:TextBox>
                <asp:ImageButton ID="ImgBntCalc2" runat="server" ImageUrl="~/Img/calendar.png" CausesValidation="False"
                    AlternateText="Cal" ToolTip="<%=Resources.res.OpenCalendar %>" Width="22px" Style="vertical-align: middle" />
                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtEndDate"
                    CssClass="reqStarStyle" ValidationGroup="MKE">*</asp:RequiredFieldValidator>
                <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender5" runat="server" TargetControlID="txtEndDate"
                    Mask="99/99/9999" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                    OnInvalidCssClass="MaskedEditError" MaskType="Date" DisplayMoney="Left" AcceptNegative="Left"
                    ErrorTooltipEnabled="True" />
                <ajaxToolkit:MaskedEditValidator ID="MaskedEditValidator5" runat="server" ControlExtender="MaskedEditExtender5"
                    ControlToValidate="txtEndDate" InvalidValueMessage="Date invalide" Display="Dynamic" ForeColor="Red"
                    InvalidValueBlurredMessage="Date invalide" ValidationGroup="MKE" />
                <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtEndDate"
                    PopupButtonID="ImgBntCalc2" Format="dd/MM/yyyy" />
            </div>
        </div>
    </div>
</div>
