﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ClientRejectsSAB_Frame : System.Web.UI.Page
{
    private string _sClientBankAccount
    {
        get { return ViewState["ClientBankAccount"].ToString(); }
        set { ViewState["ClientBankAccount"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            lblDateNow.Text = DateTime.Now.ToString("dd MMMM yyyy");
            if (Request.Params["ref"] != null && Request.Params["ref"].Length > 0)
            {
                _sClientBankAccount = Request.Params["ref"].ToString();
                InitOpeRejectList();
            }
        }
    }

    protected void InitOpeRejectList()
    {
        int iSize = 15;
        int iStep = 15;
        operation.SearchCriteria searchCriteria = new operation.SearchCriteria(iSize, iStep, "", "", _sClientBankAccount, "2", "ALL", "ALL", true);
        UpdateOpeRejectList(searchCriteria);
        searchCriteria.bForceRefresh = false;
        ViewState["SearchCriteria"] = searchCriteria;

        if (rptOpeRejectList.Items.Count > 0)
        {
            panelNoSearchResult.Visible = false;
            rptOpeRejectList.Visible = true;
        }
        else
        {
            panelNoSearchResult.Visible = true;
            rptOpeRejectList.Visible = false;
        }

        searchCriteria.sOperationType = "";
        searchCriteria.sCategoryTag = "";
    }
    protected void UpdateOpeRejectList(operation.SearchCriteria searchCriteria)
    {
        operation ope = new operation();
        int iRowCount = 0;

        DataTable dt = operation.getUserOpeRejectListTreated(operation.getUserOpeRejectList(searchCriteria, out iRowCount), ref iRowCount);

        if (iRowCount == dt.Rows.Count)
        {
            btnShowMore.Visible = false;
            UpdateNbPages(iRowCount, iRowCount);
        }
        else
        {
            btnShowMore.Visible = true;
            UpdateNbPages(searchCriteria.iPageSize, iRowCount);
        }

        if (dt.Rows.Count > 0)
        {
            rptOpeRejectList.Visible = true;
            panelNoSearchResult.Visible = false;
            rptOpeRejectList.DataSource = dt;
            rptOpeRejectList.DataBind();
        }
        else
        {
            rptOpeRejectList.Visible = false;
            panelNoSearchResult.Visible = true;
            rptOpeRejectList.DataSource = null;
            rptOpeRejectList.DataBind();
        }
    }

    protected void UpdateNbPages(int iPageSize, int iNbTotal)
    {
        ltlNbResults.Text = "<span class=\"font-bold\">" + iPageSize.ToString() + "</span>/<span class=\"font-bold\">" + iNbTotal.ToString() + "</span>";
    }

    protected void btnShowMore_Click(object sender, EventArgs e)
    {
        int iSize = 15;
        int iStep = 15;
        operation ope = new operation();
        int iRowCount;

        if (ViewState["SearchCriteria"] != null)
        {
            operation.SearchCriteria searchCriteria = (operation.SearchCriteria)ViewState["SearchCriteria"];
            iSize = searchCriteria.iPageSize;
            iStep = searchCriteria.iPageStep;
            iSize += iStep;
            searchCriteria.iPageSize = iSize;

            UpdateOpeRejectList(searchCriteria);

            ViewState["SearchCriteria"] = searchCriteria;
        }
    }

    protected void rptOperationList_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hdnOpeCode = (HiddenField)e.Item.FindControl("hdnOpeCode");
            if (hdnOpeCode != null && (hdnOpeCode.Value == "1"))
            {
                Panel panel = (Panel)e.Item.FindControl("panelPurchaseDetails");
                if (panel != null)
                    panel.Visible = true;
            }

            HiddenField hdnOpeStatus = (HiddenField)e.Item.FindControl("hdnOpeStatus");
            if (hdnOpeStatus != null && (hdnOpeStatus.Value == "2"))
            {
                // OPE UNTREATED
                /*HtmlTableCell td = (HtmlTableCell)e.Item.FindControl("tdDetails");
                if(td != null)
                    td.Visible = false;*/
            }
        }
    }
}