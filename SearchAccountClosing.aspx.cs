﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using XMLMethodLibrary;

public partial class SearchAccountClosing : System.Web.UI.Page
{
    private DataTable _dtAccountClose { get { return (Session["dtAccountClose"] != null) ? (DataTable)Session["dtAccountClose"] : null; } set { Session["dtAccountClose"] = value; } }
    protected bool bViewRights;
    protected bool bActionRights
    {
        get { return (ViewState["ActionRights"] != null) ? bool.Parse(ViewState["ActionRights"].ToString()) : false; }
        set { ViewState["ActionRights"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            CheckRights();

            if (bViewRights)
            {
                BindAccountClosing(true);
            }
            else Response.Redirect("Default.aspx", true);
        }
    }

    protected void CheckRights()
    {
        bool bAction = false, bView;
        AML.HasAccountClosingRights(authentication.GetCurrent().sToken, out bView, out bAction);

        bViewRights = bView;
        bActionRights = bAction;
    }

    protected void BindAccountClosing(bool bForceRefresh)
    {
        if(_dtAccountClose == null || bForceRefresh)
            _dtAccountClose = GetAccountCloseInformation(authentication.GetCurrent().sToken);

        if (_dtAccountClose.Rows.Count > 0)
        {
            panelNoResult.Visible = false;
            panelList.Visible = true;

            rptClose.DataSource = _dtAccountClose;
            rptClose.DataBind();
        }
        else
        {
            panelNoResult.Visible = true;
            panelList.Visible = false;
        }

        //DataTable dtTmp = _dtAccountClose.Clone();
        //for (int i = 0; i < _dtAccountClose.Rows.Count; i++)
        //{
        //    bool bMatch = true;

        //    if (ddlNotifyBefore.SelectedValue != "")
        //    {
        //        if (bool.Parse(_dtAccountClose.Rows[i]["NotifyBefore"].ToString()) != (ddlNotifyBefore.SelectedValue == "1") ? true : false)
        //            bMatch = false;
        //    }
        //    if(!String.IsNullOrWhiteSpace(txtFilter.Text))
        //    {
        //        if (!_dtAccountClose.Rows[i]["LastName"].ToString().ToLower().Contains(txtFilter.Text.ToLower())
        //            && !_dtAccountClose.Rows[i]["FirstName"].ToString().ToLower().Contains(txtFilter.Text.ToLower())
        //            && !_dtAccountClose.Rows[i]["AccountNumber"].ToString().ToLower().Contains(txtFilter.Text.ToLower()))
        //            bMatch = false;
        //    }

        //    if (bMatch)
        //        dtTmp.Rows.Add(_dtAccountClose.Rows[i].ItemArray);
        //}

        //if (dtTmp.Rows.Count > 0)
        //{
        //    int iPageCount = int.Parse(ddlPageCount.SelectedValue);
        //    int iPageIndex = int.Parse(ddlPageIndex.SelectedValue);

        //    int iNbPage = dtTmp.Rows.Count / iPageCount;
        //    if (dtTmp.Rows.Count % iPageCount > 0)
        //        iNbPage++;

        //    if (iPageIndex > iNbPage)
        //        iPageIndex = 0;

        //    int iNbToSkip = iPageCount * (iPageIndex - 1);
        //    int iNbToTake = (iNbToSkip + iPageCount <= dtTmp.Rows.Count) ? iPageCount : dtTmp.Rows.Count - iNbToSkip;
        //    DataTable dtFinal = (from r in dtTmp.AsEnumerable()
        //                         select r).Skip(iNbToSkip).Take(iNbToTake).CopyToDataTable();

        //    if (iNbPage != GetLastPageIndex())
        //    {
        //        ddlPageIndex.Items.Clear();
        //        for (int i = 0; i < iNbPage; i++)
        //            ddlPageIndex.Items.Add(new ListItem((i + 1).ToString(), (i + 1).ToString()));
        //        if (ddlPageIndex.SelectedItem == null)
        //            ddlPageIndex.SelectedIndex = 0;
        //    }

        //    panelNoResult.Visible = false;
        //    panelList.Visible = true;

        //    rptClose.DataSource = dtFinal;
        //    rptClose.DataBind();
        //}
        //else
        //{
        //    panelNoResult.Visible = true;
        //    panelList.Visible = false;
        //}

        upClose.Update();

        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "InitDetailsSlider();InitTooltip();", true);
    }

    protected DataTable GetAccountCloseInformation(string sToken)
    {
        DataTable dt = new DataTable();
        dt.TableName = "dtAccountCloseInformation";
        dt.Columns.Add("RefClose");
        dt.Columns.Add("RefCustomer");
        dt.Columns.Add("LastName");
        dt.Columns.Add("FirstName");
        dt.Columns.Add("AccountNumber");
        dt.Columns.Add("BOUser");
        dt.Columns.Add("CloseStatus");
        dt.Columns.Add("CustomerBalance");
        dt.Columns.Add("Fees", typeof(DataTable));
        dt.Columns.Add("NbFees");
        dt.Columns.Add("ReturnFunds", typeof(DataTable));
        dt.Columns.Add("NbReturnFunds");
        dt.Columns.Add("Reasons", typeof(DataTable));
        dt.Columns.Add("NotifyBefore", typeof(Boolean));
        dt.Columns.Add("NoticeEndDate");
        dt.Columns.Add("Sum", typeof(Decimal));
        dt.Columns.Add("Logs", typeof(DataTable));
        dt.Columns.Add("NbLogs", typeof(int));

        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("BankSI.P_GetCustomerCloseInformation", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, -1);

            XElement xml = new XElement("Close",
                                new XAttribute("CashierToken", sToken));
            if (!String.IsNullOrWhiteSpace(ddlNotifyBefore.SelectedValue))
                xml.Add(new XAttribute("ShowNotifyBefore", ddlNotifyBefore.SelectedValue));
            if (!String.IsNullOrWhiteSpace(txtFilter.Text))
                xml.Add(new XAttribute("Search", txtFilter.Text.Trim()));
            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN", xml).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if (sXmlOut.Length > 0)
            {
                List<string> listClose = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/Close");

                for (int i = 0; i < listClose.Count && i < 30; i++)
                {
                    #region // XML OUT SAMPLE
                    /*
                     *
                      <Close RefClose="8" LastName="BATERGIL" FirstName="PATRICK" BankAccountNumber="03712610001" BOUser="Angelita MARIDUENA" CloseStatus="A CLOTURER" CustomerBalance="1044.61">
                        <Fees>
                          <Fee FeeAmount="165.00" FeeDescription="Utilisation irrégulière Compte nickel" />
                          <Fee FeeAmount="150.00" FeeDescription="Frais de recherche" />
                        </Fees>
                        <ReturnFunds>
                          <ReturnFund TransferAmount="885" BeneficiaryName="ing" IBAN="FR7630438001004000064602274" BIC="INGBFR21XXX" Libelle="FRAUDE – 40003588353 Anne MORVAN – ASG " />
                        </ReturnFunds>
                        <REASONS>
                          <REASON TAGReason="FFCS" />
                        </REASONS>
                      </Close>
                     * 
                    */
                    #endregion

                    Decimal dSum = 0;

                    List<string> listRefClose = CommonMethod.GetAttributeValues(listClose[i], "Close", "RefClose");
                    List<string> listRefCustomer = CommonMethod.GetAttributeValues(listClose[i], "Close", "RefCustomer");
                    List<string> listLastName = CommonMethod.GetAttributeValues(listClose[i], "Close", "LastName");
                    List<string> listFirstName = CommonMethod.GetAttributeValues(listClose[i], "Close", "FirstName");
                    List<string> listBankAccountNumber = CommonMethod.GetAttributeValues(listClose[i], "Close", "BankAccountNumber");
                    List<string> listBOUser = CommonMethod.GetAttributeValues(listClose[i], "Close", "BOUser");
                    List<string> listCloseStatus = CommonMethod.GetAttributeValues(listClose[i], "Close", "CloseStatus");
                    List<string> listCustomerBalance = CommonMethod.GetAttributeValues(listClose[i], "Close", "CustomerBalance");
                    List<string> listNotifyBefore = CommonMethod.GetAttributeValues(listClose[i], "Close", "NotifyBefore");
                    List<string> listNoticeEndDate = CommonMethod.GetAttributeValues(listClose[i], "Close", "NoticeEndDate");

                    List<string> listFees = CommonMethod.GetTags(listClose[i], "Close/Fees/Fee");
                    DataTable dtFees = new DataTable();
                    dtFees.TableName = "dtFees";
                    dtFees.Columns.Add("Amount");
                    dtFees.Columns.Add("Description");
                    for (int j = 0; j < listFees.Count; j++)
                    {
                        List<string> listAmount = CommonMethod.GetAttributeValues(listFees[j], "Fee", "FeeAmount");
                        List<string> listDesc = CommonMethod.GetAttributeValues(listFees[j], "Fee", "FeeDescription");

                        dtFees.Rows.Add(new object[] { listAmount[0].Replace('.', ',') + " €", listDesc[0] });

                        Decimal dTmp = Decimal.Parse(listAmount[0].Replace('.', ','), new CultureInfo("fr-FR"));
                        dSum += dTmp;
                    }

                    List<string> listReturnFunds = CommonMethod.GetTags(listClose[i], "Close/ReturnFunds/ReturnFund");
                    DataTable dtReturnFunds = new DataTable();
                    dtReturnFunds.TableName = "dtReturnFunds";
                    dtReturnFunds.Columns.Add("Amount");
                    dtReturnFunds.Columns.Add("BeneficiaryName");
                    dtReturnFunds.Columns.Add("IBAN");
                    dtReturnFunds.Columns.Add("BIC");
                    dtReturnFunds.Columns.Add("Label");
                    dtReturnFunds.Columns.Add("SabErrorCode");
                    dtReturnFunds.Columns.Add("SabErrorLabel");
                    for (int j = 0; j < listReturnFunds.Count; j++)
                    {
                        List<string> listAmount = CommonMethod.GetAttributeValues(listReturnFunds[j], "ReturnFund", "TransferAmount");
                        List<string> listBenef = CommonMethod.GetAttributeValues(listReturnFunds[j], "ReturnFund", "BeneficiaryName");
                        List<string> listIBAN = CommonMethod.GetAttributeValues(listReturnFunds[j], "ReturnFund", "IBAN");
                        List<string> listBIC = CommonMethod.GetAttributeValues(listReturnFunds[j], "ReturnFund", "BIC");
                        List<string> listLabel = CommonMethod.GetAttributeValues(listReturnFunds[j], "ReturnFund", "Libelle");
                        List<string> listSabErrorCode = CommonMethod.GetAttributeValues(listReturnFunds[j], "ReturnFund", "SabErrorCode");
                        List<string> listSabErrorLabel = CommonMethod.GetAttributeValues(listReturnFunds[j], "ReturnFund", "SabErrorLabel");

                        dtReturnFunds.Rows.Add(new object[] { listAmount[0].Replace('.', ',') + " €", listBenef[0], listIBAN[0], listBIC[0], listLabel[0], listSabErrorCode[0], listSabErrorLabel[0] });

                        Decimal dTmp = Decimal.Parse(listAmount[0].Replace('.', ','), new CultureInfo("fr-FR"));
                        dSum += dTmp;
                    }

                    List<string> listTagReason = CommonMethod.GetAttributeValues(listClose[i], "Close/REASONS/REASON", "TAGReason");
                    DataTable dtReasons = new DataTable();
                    dtReasons.TableName = "dtReasons";
                    dtReasons.Columns.Add("Tag");
                    dtReasons.Columns.Add("Desc");
                    for (int j = 0; j < listTagReason.Count; j++)
                    {
                        string sReasonDesc = ClosingReason1.GetTagDescription(listTagReason[j]);

                        if (sReasonDesc != null)
                            dtReasons.Rows.Add(new object[] { listTagReason[j], sReasonDesc });
                    }

                    List<string> listLogs = CommonMethod.GetTags(listClose[i], "Close/CloseLogs/CloseLog");
                    DataTable dtLogs = new DataTable();
                    dtLogs.TableName = "dtLogs";
                    dtLogs.Columns.Add("LogDate");
                    dtLogs.Columns.Add("Log");
                    for (int j = 0; j < listLogs.Count; j++)
                    {
                        List<string> listDate = CommonMethod.GetAttributeValues(listLogs[j], "CloseLog", "LogDate");
                        List<string> listLog = CommonMethod.GetAttributeValues(listLogs[j], "CloseLog", "Log");

                        dtLogs.Rows.Add(new object[] { listDate[0], listLog[0] });
                    }

                    dt.Rows.Add(new object[] { listRefClose[0], listRefCustomer[0], listLastName[0], listFirstName[0], listBankAccountNumber[0], listBOUser[0], listCloseStatus[0], listCustomerBalance[0].Replace('.', ',') + " €", dtFees, dtFees.Rows.Count, dtReturnFunds, dtReturnFunds.Rows.Count, dtReasons, listNotifyBefore[0] == "1" ? true : false, listNoticeEndDate[0], dSum, dtLogs, dtLogs.Rows.Count });
                }
            }
        }
        catch(Exception e)
        {
            string stmp = e.Message;
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }

    protected bool ApproveCustomerClose(string sRefClose, string sToken)
    {
        bool bClose = false;
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("Web.P_ApproveCustomerClose", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                                new XElement("Close",
                                                    new XAttribute("CashierToken", sToken),
                                                    new XAttribute("RefClose", sRefClose))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if(sXmlOut.Length > 0)
            {
                List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Close", "RC");

                if (listRC.Count > 0 && listRC[0] == "0")
                    bClose = true;
            }
        }
        catch(Exception e)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return bClose;
    }

    protected void btnApprove_Click(object sender, EventArgs e)
    {
        string sRefClose = hdnRefClose.Value;

        if (ApproveCustomerClose(sRefClose, authentication.GetCurrent().sToken))
            BindAccountClosing(true);
        else ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Erreur', \"Une erreur est survenue, nous n'avons pas pu finaliser la clôture.\");", true);
    }

    protected void ddlNotifyBefore_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindAccountClosing(true);
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        string sRefClose = hdnRefClose.Value;

        if (CancelCustomerClose(sRefClose, authentication.GetCurrent().sToken))
            BindAccountClosing(true);
        else ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Erreur', \"Une erreur est survenue, nous n'avons pas pu annuler la clôture.\");", true);
    }

    protected bool CancelCustomerClose(string sRefClose, string sToken)
    {
        bool bCanceled = false;
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("Web.P_CancelCustomerClose", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                                new XElement("Close",
                                                    new XAttribute("CashierToken", sToken),
                                                    new XAttribute("RefClose", sRefClose))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@OUT"].Value.ToString();

            if(sXmlOut.Length > 0)
            {
                List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Close", "RC");

                if (listRC.Count > 0 && listRC[0] == "0")
                    bCanceled = true;
            }
        }
        catch(Exception e)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return bCanceled;
    }

    protected string DecimalToString(Decimal d)
    {
        return d.ToString().Replace(".", ",");
    }

    //protected void btnPrevNext_Click(object sender, EventArgs e)
    //{
    //    Button btn = (Button)sender;
    //    if (btn != null)
    //    {
    //        int iPageIndex = int.Parse(ddlPageIndex.SelectedValue);

    //        switch (btn.CommandArgument)
    //        {
    //            case "prev":
    //                if (iPageIndex > 1)
    //                    iPageIndex--;
    //                break;
    //            case "next":
    //                int iLastPage = GetLastPageIndex();
    //                if (iPageIndex < iLastPage)
    //                    iPageIndex++;
    //                break;
    //        }

    //        if (iPageIndex.ToString() != ddlPageIndex.SelectedValue)
    //        {
    //            ddlPageIndex.SelectedValue = iPageIndex.ToString();
    //            BindAccountClosing(false);
    //        }
    //    }
    //}

    //private int GetLastPageIndex()
    //{
    //    return int.Parse(ddlPageIndex.Items[ddlPageIndex.Items.Count - 1].Value);
    //}
}