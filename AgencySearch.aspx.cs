﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using XMLMethodLibrary;

public partial class AgencySearch : System.Web.UI.Page
{
    int iMaxResult = 20;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            CheckRights();
            GetAgencySearchFilters();
        }

        System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("fr-FR");
        System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("fr-FR");

        panelSearchResultNb.Visible = false;
    }

    protected void CheckRights()
    {
        string sXmlOut = tools.GetRights(authentication.GetCurrent().sToken, "SC_AgencySearch");

        List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Page", "RC");
        List<string> listView = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Page/Action", "ViewAllowed");

        if (listRC.Count == 0 || listRC[0] != "0" || listView.Count == 0 || listView[0] != "1")
        {
            Response.Redirect("Default.aspx", true);
        }
    }

    protected void GetAgencySearchFilters()
    {
        if (Session["AgencySearchFilters"] != null)
        {
            txtSearch.Text = Session["AgencySearchFilters"].ToString();
            if(txtSearch.Text.Length > 0)
                btnSearch_Click(null, null);
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        string sXmlOut = "";
        authentication auth = authentication.GetCurrent();
        string sSearchValue = txtSearch.Text;
        DataTable dtSearchResult = Agency.getAgencySearchResult(sSearchValue, iMaxResult, auth.sToken, out sXmlOut);
        rptSearchResult.DataSource = dtSearchResult;
        rptSearchResult.DataBind();

        int iSearchResultNb = dtSearchResult.Rows.Count;
        if (iSearchResultNb > 0)
        {
            if (iSearchResultNb > 1)
                lblSearchResultNb.Text = rptSearchResult.Items.Count.ToString() + " agences trouvées.";
            else
                lblSearchResultNb.Text = rptSearchResult.Items.Count.ToString() + " agence trouvée.";
        }
        else
        {
            lblSearchResultNb.Text = "Aucune agence trouvée.";
        }
        panelSearchResultNb.Visible = true;

        Session["AgencySearchFilters"] = sSearchValue;
    }
}