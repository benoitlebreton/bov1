﻿<%@ Page Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="BoCobalt_ApiAccount.aspx.cs" Inherits="BoCobalt_ApiAccount" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <div style="position: relative;">
        <asp:Button ID="btnPrevious" runat="server" Text="Menu précédent" PostBackUrl="~/BoCobalt.aspx" CssClass="button"/>
    </div>
    <div style="padding: 20px 0;">
        <div style="margin: auto; border: 4px solid #f57527; border-radius: 8px; display: table; width: 80%">
            <%--<asp:TreeView runat="server" ID="tvOperationType" ImageSet="Custom" NodeStyle-CssClass="classMenu"></asp:TreeView>--%>

            <div id="iFrameDiv" style="width: 900px; height: 400px; margin: auto; background: url(Styles/Img/loading.gif) 50% 50% no-repeat; border: 0; z-index: 100">
                <iframe id="iFrameMonitoring" width="898px" height="394px" style="z-index: 100; border: 0" src="">Votre navigateur ne supporte pas les iFrames.</iframe>
            </div>

        </div>
    </div>
    
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">
</asp:Content>

