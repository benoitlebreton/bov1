﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Timers;
using System.Web;
using System.Web.Hosting;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using XMLMethodLibrary;

public partial class mes_releves : System.Web.UI.Page
{
    private DataTable _dtDoc { get { return (ViewState["dtDoc"] != null) ? (DataTable)ViewState["dtDoc"] : null; } set { ViewState["dtDoc"] = value; } }
    private int _iRefCustomer { get { return (ViewState["RefCustomer"] != null) ? int.Parse(ViewState["RefCustomer"].ToString()) : 0; } set { ViewState["RefCustomer"] = value; } }
    private string _sCardNumber { get { return (ViewState["CardNumber"] != null) ? ViewState["CardNumber"].ToString() : ""; } set { ViewState["CardNumber"] = value; } }
    protected void Page_Load(object sender, EventArgs e)
    {
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("fr-FR");
        Thread.CurrentThread.CurrentUICulture = new CultureInfo("fr-FR");
        //auth.RefCustomer = 23;
        try
        {
            _iRefCustomer = int.Parse(Request.Params["ref"].ToString());
            _sCardNumber = Request.Params["card"].ToString();
        }
        catch(Exception ex){}


        if (!IsPostBack)
        {
            hfRefCustomer.Value = _iRefCustomer.ToString();

            DataTable dt;

            if (!tools.isTobaccoShopAccount(_sCardNumber))
            {
                dt = Client.GetBankStatementList(_iRefCustomer, _iRefCustomer);
                BindBankStatementList(dt);
            }
            else
            {
                // TEST
                //auth.RefCustomer = 339;

                dt = GetBankStatementListPRO(_iRefCustomer, _iRefCustomer);
                BindBankStatementListPRO(dt);
            }
            
            if (dt.Rows.Count > 0)
                BindSearchInputs(dt);
        }

        if (rptStatement.Items.Count == 0 && tools.isTobaccoShopAccount(_sCardNumber) && rptStatementPRO.Items.Count == 0)
        {
            panelStatementList.Visible = false;
            panelStatementListEmpty.Visible = true;
        }

        if (tools.isTobaccoShopAccount(_sCardNumber))
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "InitDropDocListKEY", "InitDropDocList();", true);
    }

    protected void BindBankStatementList(DataTable dt)
    {
        rptStatement.DataSource = Client.FormatDateBankStatementList(dt);
        rptStatement.DataBind();
    }
    protected void BindBankStatementListPRO(DataTable dt)
    {
        DataTable dt2 = dt.Clone();

        string sLastMonth = "";
        int x = 0;
        int y = 0;
        // Limit list to 12 months
        while (x < 12)
        {
            if (dt.Rows.Count > y)
            {
                string sDate = dt.Rows[y]["Date"].ToString();
                if(sDate.Length > 0 && sDate.Contains('/'))
                {
                    string sCurMonth = sDate.Split('/')[1];
                    if (sCurMonth != sLastMonth)
                    {
                        sLastMonth = sCurMonth;
                        x++;
                    }

                    dt2.ImportRow(dt.Rows[y]);
                }
            }
            else break;

            y++;
        }

        rptStatementPRO.DataSource = dt2;
        rptStatementPRO.DataBind();
    }
    
    protected DataTable GetBankStatementListPRO(int iRefCustomer, int iInitialRefCustomer)
    {
        SqlConnection conn = null;
        DataTable dt = new DataTable();
        dt.Columns.Add("Title");
        dt.Columns.Add("Date");

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK2"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_GetCustomerDocuments", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@sXmlIn", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@sXmlOut", SqlDbType.VarChar, -1);

            cmd.Parameters["@sXmlIn"].Value = new XElement("ALL_XML_IN", new XElement("Releve", new XAttribute("RefCustomer", iRefCustomer), new XAttribute("InitialRefCustomer", iInitialRefCustomer))).ToString();
            cmd.Parameters["@sXmlOut"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@sXmlOut"].Value.ToString();

            if (sXmlOut.Length > 0)
            {
                List<string> listDoc = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/Doc");

                if (listDoc.Count > 0)
                {
                    DataTable dtDoc = new DataTable();
                    List<string> listTitles = new List<string>();
                    dtDoc.Columns.Add("Date");
                    dtDoc.Columns.Add("Title");
                    dtDoc.Columns.Add("FileName");
                    dtDoc.Columns.Add("File");

                    for (int i = 0; i < listDoc.Count; i++)
                    {
                        List<string> listDate = CommonMethod.GetAttributeValues(listDoc[i], "Doc", "DATESTART");
                        List<string> listTitle = CommonMethod.GetAttributeValues(listDoc[i], "Doc", "TITLE");
                        List<string> listLabel = CommonMethod.GetAttributeValues(listDoc[i], "Doc", "DOC_TYPE_LABEL");
                        List<string> listFileName = CommonMethod.GetAttributeValues(listDoc[i], "Doc", "FILENAME");
                        List<string> listFileDir = CommonMethod.GetAttributeValues(listDoc[i], "Doc", "CUST_FILES_DIR");

                        if (!listTitles.Contains(listTitle[0] + "|" + listDate[0]))
                            listTitles.Add(listTitle[0] + "|" + listDate[0]);

                        string sPath = ConfigurationManager.AppSettings["NickelDocPath"].ToString();
                        string sFilePath = listFileDir[0] + "/" + listFileName[0];
                        string sTmp = HostingEnvironment.MapPath("~" + sPath + sFilePath.Replace("\\", "/"));
                        //Uri uriCurrentUrl = new Uri(Request.Url.AbsoluteUri);
                        string sFile = "";
                        //if (File.Exists(sTmp))
                        {
                            sFile = "." + sPath + "/" + sFilePath.Replace("\\", "/");
                            //sFile = uriCurrentUrl.Scheme + Uri.SchemeDelimiter + uriCurrentUrl.Host + ":" + uriCurrentUrl.Port + "/" + sFilePath.Replace("\\", "/"); 
                            string sLabel = listLabel[0].Replace("'", "&#39;");

                            dtDoc.Rows.Add(new object[] { listDate[0], listTitle[0], sLabel, sFile });
                        }
                    }
                    _dtDoc = dtDoc;

                    for (int i = 0; i < listTitles.Count; i++)
                    {
                        dt.Rows.Add(new object[] { listTitles[i].Split('|')[0], listTitles[i].Split('|')[1] });
                    }
                }
            }
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }

    protected string GetAnnualFeesStyle(string sLabel)
    {
        if (sLabel.Contains("Relevé des frais"))
        {
            return "annual-fees-row";
        }
        else return "";
    }

    protected void BindSearchInputs(DataTable dt)
    {
        string sOldestDate = dt.Rows[dt.Rows.Count-1][0].ToString();
        string sOldestYear = sOldestDate.Substring(sOldestDate.Length - 4);

        for (int i = int.Parse(sOldestYear); i <= DateTime.Now.Year; i++)
        {
            ddlYears.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }

        for (int i = 0; i < 12; i++)
        {
            ddlMonths.Items.Add(new ListItem(new DateTime(2013, i+1, 1).ToString("MMMM"), (i + 1).ToString()));
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        string sMonth = (ddlMonths.SelectedValue.Length == 1) ? "0" + ddlMonths.SelectedValue : ddlMonths.SelectedValue;
        string sYear = ddlYears.SelectedValue;
        string sMonthText = ddlMonths.SelectedItem.Text;
        string sYearText = ddlYears.SelectedItem.Text;

        //if (sMonth.Length > 0 || sYear.Length > 0)
        //{
            DataTable dt;
            if (!tools.isTobaccoShopAccount(_sCardNumber))
                dt = Client.GetBankStatementList(_iRefCustomer, _iRefCustomer);
            else dt = GetBankStatementListPRO(_iRefCustomer, _iRefCustomer);
            DataTable dt2 = dt.Clone();

            if (sMonth.Length > 0 || sYear.Length > 0)
            {

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (!tools.isTobaccoShopAccount(_sCardNumber))
                    {
                        if (sMonth.Length > 0 && sYear.Length > 0)
                        {
                            if (dt.Rows[i][0].ToString() == sYear + sMonth)
                                dt2.ImportRow(dt.Rows[i]);
                        }
                        else if (sYear.Length > 0)
                        {
                            if (dt.Rows[i][0].ToString().StartsWith(sYear))
                                dt2.ImportRow(dt.Rows[i]);
                        }
                        else if (sMonth.Length > 0)
                        {
                            if (dt.Rows[i][0].ToString().EndsWith(sMonth))
                                dt2.ImportRow(dt.Rows[i]);
                        }
                    }
                    else
                    {
                        string sDate = dt.Rows[i][0].ToString();

                        if (sMonthText.Length > 0 && sYearText.Length > 0)
                        {
                            if (sDate.ToLower().StartsWith(sMonthText.ToLower()) && sDate.ToLower().EndsWith(sYearText.ToLower()))
                                dt2.ImportRow(dt.Rows[i]);
                        }
                        else if (sYear.Length > 0)
                        {
                            if (sDate.ToLower().EndsWith(sYearText.ToLower()))
                                dt2.ImportRow(dt.Rows[i]);
                        }
                        else if (sMonthText.Length > 0)
                        {
                            if (sDate.ToLower().StartsWith(sMonthText.ToLower()))
                                dt2.ImportRow(dt.Rows[i]);
                        }
                    }
                }

                if (dt2.Rows.Count > 0)
                {
                    if (!tools.isTobaccoShopAccount(_sCardNumber))
                        BindBankStatementList(dt2);
                    else BindBankStatementListPRO(dt2);
                    panelResult.Visible = false;
                }
                else
                {
                    lblResult.Text = "Votre recherche n'a retourné aucun résultat.";
                    panelResult.Visible = true;
                }
            }
            else
            {
                if (!tools.isTobaccoShopAccount(_sCardNumber))
                    BindBankStatementList(dt);
                else BindBankStatementListPRO(dt);
                panelResult.Visible = false;
            }
        //}
    }

    private static int iNbTryFindFile;
    protected void btnDownload_Click(object sender, ImageClickEventArgs e)
    {
        panelResult.Visible = false;
        hfFileToDownload.Value = "";
        try
        {
            ImageButton btn = (ImageButton)sender;
            if (btn != null)
            {
                HiddenField hdn = (HiddenField)btn.Parent.FindControl("hdnFilePath");
                if (hdn != null)
                {
                    string sFile = hdn.Value;
                    if (sFile.Length > 0)
                    {
                        if (File.Exists(HostingEnvironment.MapPath("~" + sFile.Substring(1))) && sFile.EndsWith(".pdf"))
                        {
                            //HttpContext.Current.Response.SetCookie(new HttpCookie("fileDownload", "false") { Path = "/" });
                            FileInfo info = new FileInfo(HostingEnvironment.MapPath("~" + sFile.Substring(1)));
                            string sUrlPath = HttpContext.Current.Request.Url.AbsoluteUri.Substring(0,HttpContext.Current.Request.Url.AbsoluteUri.LastIndexOf('/')) + sFile.Substring(1);
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "execDownloadFile_key", "execDownloadFile('" + sFile + "','" + info.Length.ToString() + "');", true);
                            //hlReportToDw.NavigateUrl = sUrlPath;

                            hfFileToDownload.Value = sFile;
                        }
                        else
                        {
                            if(tools.GetClientDocForDownload(_iRefCustomer.ToString(), _iRefCustomer.ToString(), sFile))
                            {
                                iNbTryFindFile = 6;
                                GetClientFile(sFile);
                            }
                            else throw new Exception();
                        }
                    }
                    else throw new Exception();
                }
                else throw new Exception();
            }
            else throw new Exception();
        }
        catch (Exception ex)
        {
            lblResult.Text = "Momentanément indisponible.<br />Veuillez réessayer ulterieurement.";
            panelResult.Visible = true;
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "execDownloadFile_key", "$('html, body').animate({scrollTop: $('#" + panelResult.ClientID + "').offset().top}, 2000);", true);
        }

        
    }
    protected void btnFileToDownload_Click(object sender, EventArgs e) 
    {
        Button btn = (Button)sender;
        if (btn != null && btn.ID == "btnFileToDownload" && hfFileToDownload.Value.Trim().Length > 0)
        {
            string sFile = hfFileToDownload.Value;
            if (File.Exists(HostingEnvironment.MapPath("~" + sFile.Substring(1))) && sFile.EndsWith(".pdf"))
            {
                //Tools.LogToFile(sFile, "btnFileToDownload_Click");

                FileInfo info = new FileInfo(HostingEnvironment.MapPath("~" + sFile.Substring(1)));
                Response.Clear();
                Response.ContentType = "application/pdf";
                Response.AppendHeader("Content-Disposition", "attachment; filename=" + info.Name);
                Response.TransmitFile(sFile);
                Response.End();
            }
            else
            {
                lblResult.Text = "Momentanément indisponible.<br />Veuillez réessayer ulterieurement.";
                panelResult.Visible = true;
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "scrollDownloadError_key", "$('html, body').animate({scrollTop: $('#" + panelResult.ClientID + "').offset().top}, 2000);", true);
            }
        }
        else
        {
            lblResult.Text = "Momentanément indisponible.<br />Veuillez réessayer ulterieurement.";
            panelResult.Visible = true;
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "scrollDownloadError_key", "$('html, body').animate({scrollTop: $('#" + panelResult.ClientID + "').offset().top}, 2000);", true);
        }
    }
    protected void GetClientFile(string sFile)
    {
        //bool isOK = false;
        iNbTryFindFile--;
        //while(iNbTryFindFile > 0)
        if (iNbTryFindFile > 0)
        {
            if (File.Exists(HostingEnvironment.MapPath("~" + sFile.Substring(1))) && sFile.EndsWith(".pdf"))
            {
                //isOK = true;
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "execDownloadFile_key", "execDownloadFile('" + sFile + "');", true);
                /*
                Response.Clear();
                Response.ContentType = "application/pdf";
                Response.AppendHeader("Content-Disposition", "attachment; filename=" + info.Name);
                Response.TransmitFile(sFile);
                Response.End();
                */
            }
            else
            {
                //iNbTryFindFile--;
                System.Threading.Thread.Sleep(2000);
                GetClientFile(sFile);
            }
        }
        else
        {
            lblResult.Text = "Momentanément indisponible.<br />Veuillez réessayer ulterieurement.";
            panelResult.Visible = true;
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "scrollDownloadError_key", "$('html, body').animate({scrollTop: $('#" + panelResult.ClientID + "').offset().top}, 2000);", true);
        }
        //if(iNbTryFindFile == 0 && !isOK)
        //{
        //    lblResult.Text = "Momentanément indisponible.<br />Veuillez réessayer ulterieurement.";
        //    panelResult.Visible = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "scrollDownloadError_key", "$('html, body').animate({scrollTop: $('#" + panelResult.ClientID + "').offset().top}, 2000);", true);
        //}
    }

    protected void rptStatementPRO_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        RepeaterItem item = e.Item;
        if ((item.ItemType == ListItemType.Item) ||
            (item.ItemType == ListItemType.AlternatingItem))
        {
            Label lblTitle = (Label)item.FindControl("lblTitle");
            if (lblTitle != null)
            {
                if (_dtDoc != null)
                {
                    Repeater rptStatementPROFileList = (Repeater)item.FindControl("rptStatementPROFileList");

                    if (rptStatementPROFileList != null)
                    {
                        DataTable dt = _dtDoc.Clone();

                        for (int i = 0; i < _dtDoc.Rows.Count; i++)
                        {
                            if (_dtDoc.Rows[i]["Title"].ToString() == lblTitle.Text)
                                dt.Rows.Add(new object[] { _dtDoc.Rows[i]["Date"], _dtDoc.Rows[i]["Title"], _dtDoc.Rows[i]["FileName"], _dtDoc.Rows[i]["File"] });
                        }

                        rptStatementPROFileList.DataSource = dt;
                        rptStatementPROFileList.DataBind();
                    }
                }
            }
        }
    }
}