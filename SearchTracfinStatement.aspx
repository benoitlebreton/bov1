﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="SearchTracfinStatement.aspx.cs" Inherits="SearchTracfinStatement" %>
<%@ Register Src="~/API/SearchTracfinStatement.ascx" TagName="SearchTracfinStatement" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">

    <link rel="Stylesheet" href="Styles/ClientSearch.css" />
    
    <style type="text/css">
        input[type=text]{
            height:26px;
            padding:0 5px;
            box-sizing:border-box;
        }
    </style>

    <script type="text/javascript">
        function ShowPanelLoading(panelID) {
            var panel = $('#' + panelID);
            if (panelID.trim().length > 0 && panel != null) {

                $('.panel-loading').show();
                $('.panel-loading').width(panel.outerWidth());
                $('.panel-loading').height(panel.outerHeight());

                $('.panel-loading').position({
                    my: 'center',
                    at: 'center',
                    of: panel
                });
            }
        }
        function HidePanelLoading() {
            $('.panel-loading').hide();
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

    <h2 style="color: #344b56;text-transform:uppercase">Recherche déclaration Tracfin</h2>

    <asp:SearchTracfinStatement ID="Search1" runat="server" />

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" Runat="Server">
</asp:Content>

