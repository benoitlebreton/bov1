﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using XMLMethodLibrary;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;

public partial class ClientBalanceOperations_Frame : System.Web.UI.Page
{
    private string _sClientBankAccount
    {
        get { return ViewState["ClientBankAccount"].ToString(); }
        set { ViewState["ClientBankAccount"] = value; }
    }
    private string _sRefClient
    {
        get { return ViewState["RefClient"].ToString(); }
        set { ViewState["RefClient"] = value; }
    }
    private string _sIBAN
    {
        get { return (ViewState["IBAN"] != null) ? ViewState["IBAN"].ToString() : ""; }
        set { ViewState["IBAN"] = value; }
    }
    private bool _bIsCobalt
    {
        get { return (ViewState["IsCobalt"] != null) ? bool.Parse(ViewState["IsCobalt"].ToString()) : false; }
        set { ViewState["IsCobalt"] = value; }
    }
    private bool _bERP
    {
        get { return (ViewState["ShowERP"] != null) ? bool.Parse(ViewState["ShowERP"].ToString()) : false; }
        set { ViewState["ShowERP"] = value; }
    }
    private bool _bCanCancelOpeMinute
    {
        get { return (ViewState["CanCancelOpeMinute"] != null) ? bool.Parse(ViewState["CanCancelOpeMinute"].ToString()) : false; }
        set { ViewState["CanCancelOpeMinute"] = value; }
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        panelOperationError.Visible = false;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("fr-FR");
        Thread.CurrentThread.CurrentUICulture = new CultureInfo("fr-FR");

        ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
        scriptManager.RegisterPostBackControl(this.btnExcelExport);

        lblDateNow.Text = DateTime.Now.ToString("dd MMMM yyyy");
        //lblTimeNow.Text = DateTime.Now.ToString("HH:mm");

        if (!IsPostBack)
        {
            if (Request.Params["ref"] != null && Request.Params["ref"].Length > 0 && Request.Params["accountno"] != null && Request.Params["accountno"].Length > 0)
            {
                _sRefClient = Request.Params["ref"].ToString();
                _sClientBankAccount = Request.Params["accountno"].ToString();

                if (Request.Params["isCobalt"] != null && Request.Params["isCobalt"].ToString() == "1")
                    _bIsCobalt = true;
                if (_bIsCobalt)
                {
                    //btnSearchTmp.Visible = false;
                    panelCategoryChoice.Visible = false;
                }

                if (Request.Params["iban"] != null && Request.Params["iban"].Length > 0)
                    _sIBAN = Request.Params["iban"].ToString();

                if (Request.Params["erp"] != null && Request.Params["erp"].ToString() == "1")
                    _bERP = true;

                CheckRights();
                BindClientBalance();
                BindOperationTypes();
                BindCategories();

                operation ope = new operation();
                int iSize = 15;
                int iStep = 15;
                operation.SearchCriteria searchCriteria = new operation.SearchCriteria(iSize, iStep, "", "", _sClientBankAccount, true);
                searchCriteria.bIsCobalt = _bIsCobalt;
                searchCriteria.sIBAN = _sIBAN;

                string sOpeType = "";
                if (Request.Params["type"] != null)
                {
                    switch (Request.Params["type"])
                    {
                        case "vir":
                            string sValue = "5";
                            searchCriteria.sOperationType = sValue;
                            cbPaymentMethod.SelectedIndex = cbPaymentMethod.Items.IndexOf(cbPaymentMethod.Items.FindByValue(sValue));
                            //ddlPaymentMethod.SelectedIndex = ddlPaymentMethod.Items.IndexOf(ddlPaymentMethod.Items.FindByValue(sValue));
                            break;
                    }

                    UpdateSearchCriteria(searchCriteria);
                }

                UpdateOperationList(searchCriteria);
                searchCriteria.bForceRefresh = false;

                if (_bIsCobalt)
                {
                    UpdateSearchCriteria(searchCriteria);
                    panelOperationListTable.Visible = true;
                    searchCriteria.sDateFrom = null;
                }

                ViewState["SearchCriteria"] = searchCriteria;

                if (rptOperationList.Items.Count > 0)
                    panelOperationListTable.Visible = true;
            }
            else
            {
                panelOperationListTable.Visible = false;
                panelOperationListEmpty.Visible = false;
                panelOperationError.Visible = true;
            }
        }
    }

    protected void CheckRights()
    {
        authentication auth = authentication.GetCurrent();
        string sXmlOut = tools.GetRights(auth.sToken, "SC_BalanceOperations");

        List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Page", "RC");

        if (listRC.Count == 0 || listRC[0] != "0")
        {
            Response.Redirect("Default.aspx", true);
        }

        switch (auth.sTagProfile.ToUpper())
        {
            case "ADMIN":
            case "AGENTN2SENIOR":
            case "AGENTN3":
                _bCanCancelOpeMinute = true;
                break;
            default:
                _bCanCancelOpeMinute = false;
                break;
        }
    }

    protected void BindClientBalance()
    {
        decimal dAccountBalance = -1;
        string sFormattedAccountBalance = "";
        tools.GetAccountBalance(_sClientBankAccount, _sIBAN, _bIsCobalt, _bERP, out dAccountBalance);
        //tools.GetAccountBalance(int.Parse(_sRefClient), out dAccountBalance, out sFormattedAccountBalance);
        sFormattedAccountBalance = String.Format("{0:0.00}", dAccountBalance).Replace('.', ',') + " &euro;";
        lblClientBalance.Text = sFormattedAccountBalance;
    }
    protected void BindOperationTypes()
    {
        DataTable dt = GetOperationsTypes();

        if (dt.Rows.Count > 0)
        {
            cbPaymentMethod.DataSource = dt;
            cbPaymentMethod.DataBind();
            //ddlPaymentMethod.DataSource = dt;
            //ddlPaymentMethod.DataBind();
        }
    }
    protected void BindCategories()
    {
        List<operation.Category> list = operation.Category.GetCategories();

        DataTable dt1 = new DataTable();
        dt1.Columns.Add("TAG");
        dt1.Columns.Add("IMG");
        dt1.Columns.Add("LBL");
        DataTable dt2 = dt1.Clone();

        for (int i = 0; i < list.Count; i++)
        {
            if (list[i].bUpdate == true)
            {
                DataRow row = dt1.NewRow();
                row["TAG"] = list[i].sTag;
                row["IMG"] = list[i].sImg;
                row["LBL"] = list[i].sLabel;
                dt1.Rows.Add(row);
            }
            if (list[i].bSearch == true)
            {
                DataRow row = dt2.NewRow();
                row["TAG"] = list[i].sTag;
                row["IMG"] = list[i].sImg;
                row["LBL"] = list[i].sLabel;
                dt2.Rows.Add(row);
            }
        }

        rptCategory1.DataSource = dt1;
        rptCategory1.DataBind();
        rptCategory2.DataSource = dt2;
        rptCategory2.DataBind();
    }
    protected DataTable GetOperationsTypes()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("Text");
        dt.Columns.Add("Value");

        if (!_bIsCobalt)
        {
            SqlConnection conn = null;

            try
            {
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Nickel"].ConnectionString);
                conn.Open();

                SqlCommand cmd = new SqlCommand("Nickel.P_GetOperationsTypes", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@sXmlIn", SqlDbType.VarChar, 8000);
                cmd.Parameters.Add("@sXmlOut", SqlDbType.VarChar, 8000);

                cmd.Parameters["@sXmlIn"].Value = new XElement("ALL_XML_IN", new XElement("OperationsTypes", new XAttribute("Culture", "fr-FR"))).ToString();
                cmd.Parameters["@sXmlOut"].Direction = ParameterDirection.Output;

                cmd.ExecuteNonQuery();

                string sXmlOut = cmd.Parameters["@sXmlOut"].Value.ToString();

                List<string> listOpeTypes = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/OperationsTypes");

                for (int i = 0; i < listOpeTypes.Count; i++)
                {
                    List<string> listText = CommonMethod.GetAttributeValues(listOpeTypes[i], "OperationsTypes", "Label");
                    List<string> listValue = CommonMethod.GetAttributeValues(listOpeTypes[i], "OperationsTypes", "Id");

                    if (listText.Count > 0 && listValue.Count > 0)
                    {
                        DataRow row = dt.NewRow();
                        row["Text"] = listText[0];
                        row["Value"] = listValue[0];
                        dt.Rows.Add(row);
                    }
                }
            }
            catch (Exception e)
            {
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }
        else
        {
            foreach (KeyValuePair<int, string> entry in OperationType.CobaltOperationTypes)
            {
                dt.Rows.Add(new object[] { entry.Value, entry.Key });
            }
        }

        return dt;
    }
    protected string getFormattedDate(string sDate)
    {
        string sFormattedDate = "";
        DateTime dt;
        DateTime.TryParse(sDate, out dt);
        //sFormattedDate = dt.ToString("dd MMMM yyyy");
        sFormattedDate = dt.ToString("dd.MM.yyyy");
        return sFormattedDate;
    }
    protected string getFormattedAmount(string sAmount)
    {
        string sAmountSign = (sAmount.StartsWith("-")) ? "-" : "+";
        string sLblAmount = "";

        sAmount = getFormattedAmountNoStyle(sAmount);

        // Add Euro symbol
        sAmount += " &euro;";

        // Add sign and corresponding colors
        //sAmount = sAmountSign + sAmount;
        if (sAmountSign == "+")
        {
            sLblAmount = "<span class='font-green'>" + sAmount + "</span>";
        }
        else
        {
            sLblAmount = "<span class='font-red'>" + sAmount + "</span>";
        }

        return sLblAmount;
    }
    protected string getFormattedAmountNoStyle(string sAmount)
    {
        // Remove last two zeros
        if(sAmount != "0")
            sAmount = sAmount.Substring(0, sAmount.Length - 2);

        // Add space every 3 numbers
        try
        {
            //sAmount = sAmount.Replace("-", "");
            string[] arAmount = sAmount.Split(',');
            int iTmp = int.Parse(arAmount[0]);
            arAmount[0] = String.Format("{0:### ### ### ###}", iTmp).Trim();
            if (arAmount[0].Length == 0)
                arAmount[0] = "0";
            sAmount = (sAmount.StartsWith("-0,") ? "-" : "") + ((arAmount.Length > 1) ? arAmount[0] + "," + arAmount[1].PadRight(2, '0') : arAmount[0]);
        }
        catch (Exception e)
        {
        }

        return sAmount;
    }
    protected string getOpeTypeClass(string sPrefix, string sOpeCode)
    {
        string sOpeClass = sPrefix;

        switch (sOpeCode)
        {
            case "6":
                sOpeClass += "font-orange";
                break;
        }

        return sOpeClass;
    }
    protected string getStatusClass(string sStatus)
    {
        string sClass = "";
        if (sStatus == "2")
            sClass = "operation-not-achieved";

        return sClass;
    }
    protected string getStatusImage(string sStatus, string row)
    {
        string sImg = "";

        if (sStatus == "2")
        {
            sImg = "<span class=\"tooltip\" tooltiptext=\"En traitement\" style=\"position:relative;top:-5px;left:-2px;\"";

            if (_bCanCancelOpeMinute == true)
            {
                if (_bIsCobalt)
                {
                    sImg += " onclick=\"ShowConfirmCancelMinuteOperation(" + row + ")\"";
                }
            }
            sImg += "><img src=\"Styles/Img/clock.png\" style=\"width:20px\" alt=\"En traitement\" /></span>";
        }

        return sImg;
    }
    protected string getCategoryImage(string sTag, string sMerchantNumber, string sLabel, string sUpdatable)
    {
        return operation.getCategoryImage(sTag, sMerchantNumber, sLabel, false, sUpdatable);
    }
    protected string getOperationLabel(string sOpeStoreName, string sOpeLabelDetail)
    {
        string sOperationLabel = "";

        if (sOpeStoreName.Length > 0)
            sOperationLabel = sOpeStoreName;
        else sOperationLabel = sOpeLabelDetail;

        return sOperationLabel;
    }
    protected string ShowOpeDetail(string sDetail, string sOpeCode)
    {
        if (sOpeCode == "1" || sOpeCode == "8")
            return "";
        else return sDetail;
    }
    protected void rptOperationList_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hdnOpeCode = (HiddenField)e.Item.FindControl("hdnOpeCode");
            if (hdnOpeCode != null && (hdnOpeCode.Value == "1"))
            {
                Panel panel = (Panel)e.Item.FindControl("panelPurchaseDetails");
                if (panel != null)
                    panel.Visible = true;
            }

            HiddenField hdnOpeStatus = (HiddenField)e.Item.FindControl("hdnOpeStatus");
            /*if (hdnOpeStatus != null && (hdnOpeStatus.Value == "2"))
            {
                HtmlTableCell td = (HtmlTableCell)e.Item.FindControl("tdDetails");
                if (td != null)
                    td.Visible = false;
            }*/
        }
    }
    protected DataTable getPages(int nbPages)
    {
        DataTable dt = new DataTable();

        dt.Columns.Add("L");
        dt.Columns.Add("V");

        for (int i = 1; i <= nbPages; i++)
        {
            dt.Rows.Add(i.ToString(), i.ToString());
        }

        return dt;
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        if (ViewState["SearchCriteria"] != null)
        {
            operation.SearchCriteria searchCriteria = (operation.SearchCriteria)ViewState["SearchCriteria"];
            DateTime date;
            if (txtDateFrom.Text.Length > 0 && DateTime.TryParse(txtDateFrom.Text, out date))
                searchCriteria.sDateFrom = txtDateFrom.Text;
            else searchCriteria.sDateFrom = "";
            if (txtDateTo.Text.Length > 0 && DateTime.TryParse(txtDateTo.Text, out date))
                searchCriteria.sDateTo = txtDateTo.Text;
            else searchCriteria.sDateTo = "";
            searchCriteria.iPageSize = 50;
            searchCriteria.iPageStep = 50;
            searchCriteria.sOperationType = getOperationType(cbPaymentMethod);//ddlPaymentMethod.SelectedValue;
            searchCriteria.sCategoryTag = (hdnSearchCategory.Value.Trim().Length > 0 && hdnSearchCategory.Value.LastIndexOf(',') == hdnSearchCategory.Value.Length - 1) ? hdnSearchCategory.Value.Substring(0, hdnSearchCategory.Value.Length - 1) : hdnSearchCategory.Value;

            searchCriteria.bIsCobalt = _bIsCobalt;
            searchCriteria.sIBAN = _sIBAN;

            UpdateOperationList(searchCriteria);

            ViewState["SearchCriteria"] = searchCriteria;

            UpdateSearchCriteria(searchCriteria);
        }
    }
    protected string getOperationType(CheckBoxList cbl)
    {
        string sOperationType = "";
        foreach(ListItem cb in cbl.Items)
        {
            if (cb.Selected)
                sOperationType += cb.Value + ",";
        }
        sOperationType = (sOperationType.Trim().Length > 0) ? sOperationType.Substring(0, sOperationType.Length - 1) : "";
        return sOperationType;
    }
    protected void UpdateOperationList(operation.SearchCriteria searchCriteria)
    {
        operation ope = new operation();
        int iRowCount;
        lblTotal.Text = "0 &euro;";

        DataTable dt = ope.getOperations(searchCriteria, out iRowCount);
        if (ope.bCheckOpeResult(dt))
        {
            if (iRowCount == dt.Rows.Count && !_bIsCobalt)
            {
                btnShowMore.Visible = false;
                UpdateNbPages(iRowCount, iRowCount);
            }
            else
            {
                btnShowMore.Visible = true;
                UpdateNbPages(searchCriteria.iPageSize, iRowCount);
            }

            if (dt.Rows.Count > 0)
            {
                // Calcul total montant
                Decimal dTotal = 0;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Decimal dTmp = 0;
                    Decimal.TryParse(dt.Rows[i]["AMOUNT"].ToString(), out dTmp);

                    dTotal += dTmp;
                }
                string sTotalAmount = getFormattedAmountNoStyle(dTotal.ToString());
                lblTotal.Text = sTotalAmount + " &euro;";

                rptOperationList.Visible = true;
                panelNoSearchResult.Visible = false;
                rptOperationList.DataSource = dt;
                rptOperationList.DataBind();
            }
            else
            {
                rptOperationList.Visible = false;
                panelNoSearchResult.Visible = true;
                rptOperationList.DataSource = null;
                rptOperationList.DataBind();
            }
        }
    }
    protected void UpdateSearchCriteria(operation.SearchCriteria searchCriteria)
    {
        DateTime date;
        string sDateFrom = "";
        string sDateTo = "";
        panelNoFilters.Visible = false;
        panelFilters.Visible = true;
        string sFiltersLabel = "Vos opérations ";

        if (DateTime.TryParse(searchCriteria.sDateFrom, out date))
            sDateFrom = date.ToString("dd MMMM yyyy");
        if (DateTime.TryParse(searchCriteria.sDateTo, out date))
            sDateTo = date.ToString("dd MMMM yyyy");

        string sPaymentMethod = searchCriteria.sOperationType;//ddlPaymentMethod.Items.FindByValue(searchCriteria.sOperationType).Text;
        string sCategoryTag = searchCriteria.sCategoryTag;

        if (!string.IsNullOrWhiteSpace(sPaymentMethod) || !string.IsNullOrWhiteSpace(sCategoryTag))
            sFiltersLabel += ": ";
        if (!string.IsNullOrWhiteSpace(sPaymentMethod))
        {
            sFiltersLabel += "<span class=\"font-bold uppercase\">";
            DataTable dtOperationType = GetOperationsTypes();
            string[] tPaymentMethod = sPaymentMethod.Split(',');
            for (int i = 0; i < tPaymentMethod.Length; i++)
            {
                foreach (DataRow dr in dtOperationType.Rows)
                {
                    if (tPaymentMethod[i] == dr["Value"].ToString())
                         sFiltersLabel += dr["Text"].ToString() + ", ";
                }
            }

            sFiltersLabel = sFiltersLabel.Substring(0, sFiltersLabel.Length - 2);
            sFiltersLabel += "</span> ";
        }

        if (!string.IsNullOrWhiteSpace(sCategoryTag))
        {
            string sCategoryLbl = "";
            List<operation.Category> list = operation.Category.GetCategories();
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].sTag == sCategoryTag)
                {
                    sCategoryLbl = list[i].sLabel;
                    break;
                }
            }

            if (sCategoryLbl.Length > 0)
                sFiltersLabel += "catégorie <span class=\"font-bold uppercase\">" + sCategoryLbl + "</span> ";
        }

        if (sDateFrom.Length > 0 && sDateTo.Length > 0)
        {
            sFiltersLabel += "du <span class=\"font-bold\"  style=\"text-transform:uppercase\">" + sDateFrom + "</span> au <span class=\"font-bold\"  style=\"text-transform:uppercase\">" + sDateTo + "</span>";
        }
        else
        {
            if (sDateFrom.Length > 0)
            {
                sFiltersLabel += "depuis le <span class=\"font-bold uppercase\">" + sDateFrom + "</span>";
            }
            else if (sDateTo.Length > 0)
            {
                sFiltersLabel += "au <span class=\"font-bold uppercase\">" + sDateTo + "</span>";
            }
            else
            {
                sFiltersLabel += "au <span class=\"font-bold uppercase\">" + DateTime.Now.ToString("dd MMMM yyyy") + "</span>, à <span class=\"lblClientTime font-bold\"></span>";
            }
        }

        ltlFilters.Text = sFiltersLabel;

        /*if (sDateFrom.Length == 0 && sDateTo.Length == 0 && sPaymentMethod.Length == 0)
        {
            panelFilters.Visible = false;
            panelNoFilters.Visible = true;
        }*/
    }
    protected void btnClearSearchCriteria_Click(object sender, EventArgs e)
    {
    }
    protected void UpdateNbPages(int iPageSize, int iNbTotal)
    {
        ltlNbResults.Text = "<span class=\"font-bold\">" + iPageSize.ToString() + "</span>/<span class=\"font-bold\">" + iNbTotal.ToString() + "</span>";
    }
    protected void btnShowMore_Click(object sender, EventArgs e)
    {
        int iSize = 15;
        int iStep = 15;
        operation ope = new operation();
        int iRowCount;

        if (ViewState["SearchCriteria"] != null)
        {
            operation.SearchCriteria searchCriteria = (operation.SearchCriteria)ViewState["SearchCriteria"];
            iSize = searchCriteria.iPageSize;
            iStep = searchCriteria.iPageStep;
            iSize += iStep;
            searchCriteria.iPageSize = iSize;

            searchCriteria.bIsCobalt = _bIsCobalt;
            searchCriteria.sIBAN = _sIBAN;

            UpdateOperationList(searchCriteria);

            ViewState["SearchCriteria"] = searchCriteria;
        }
        else
        {
            operation.SearchCriteria searchCriteria = new operation.SearchCriteria(iSize, iStep, "", "", _sClientBankAccount);

            UpdateOperationList(searchCriteria);
            panelFilters.Visible = false;
            panelNoFilters.Visible = true;

            ViewState["SearchCriteria"] = searchCriteria;
        }
    }
    protected void btnChangeCategory_Click(object sender, EventArgs e)
    {
        string sCategoryLabel = txtCategoryLabel.Text;
        string sMerchantNumber = hdnMerchantNumber.Value;
        string sCategoryTag = hdnCategoryTag.Value;

        ChangeMerchantCategory(0, sMerchantNumber, sCategoryTag, sCategoryLabel);

        operation.SearchCriteria searchCriteria = (operation.SearchCriteria)ViewState["SearchCriteria"];
        UpdateOperationList(searchCriteria);
    }
    protected void ChangeMerchantCategory(int sRefCustomer, string sMerchantNumber, string sCategoryTag, string sCategoryLabel)
    {
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_ChangeCustomerCategory", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);

            cmd.Parameters["@IN"].Value = new XElement("ALL_XML_IN",
                                            new XElement("Category",
                                                new XAttribute("RefCustomer", sRefCustomer),
                                                new XAttribute("MerchantNumber", sMerchantNumber),
                                                new XAttribute("TAGCategory", sCategoryTag),
                                                new XAttribute("Category", sCategoryLabel))).ToString();
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }
    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        if (_sRefClient != null && _sRefClient.Length > 0)
        {
            Response.Redirect("ClientDetails.aspx?ref=" + _sRefClient, true);
        }
    }

    protected void btnExportExcel_Click(object sender, EventArgs e)
    {
        if (ViewState["SearchCriteria"] != null)
        {
            int iRowCount;
            operation ope = new operation();
            operation.SearchCriteria searchCriteria = (operation.SearchCriteria)ViewState["SearchCriteria"];
            DateTime date;
            if (txtDateFrom.Text.Length > 0 && DateTime.TryParse(txtDateFrom.Text, out date))
                searchCriteria.sDateFrom = txtDateFrom.Text;
            else searchCriteria.sDateFrom = "";
            if (txtDateTo.Text.Length > 0 && DateTime.TryParse(txtDateTo.Text, out date))
                searchCriteria.sDateTo = txtDateTo.Text;
            else searchCriteria.sDateTo = "";
            searchCriteria.iPageSize = -1;
            searchCriteria.iPageStep = -1;
            searchCriteria.sOperationType = getOperationType(cbPaymentMethod);//ddlPaymentMethod.SelectedValue;
            searchCriteria.sCategoryTag = hdnSearchCategory.Value;
            searchCriteria.sOutputFormat = "EXCEL";
            if (_bIsCobalt)
            {
                searchCriteria.bIsCobalt = _bIsCobalt;
                searchCriteria.sIBAN = _sIBAN;
            }
            ViewState["SearchCriteria"] = searchCriteria;
            //UpdateSearchCriteria(searchCriteria);

            DataTable dtExcelExport = new DataTable();
            dtExcelExport.Columns.Add("Date");
            dtExcelExport.Columns.Add("Type");
            dtExcelExport.Columns.Add("Lieu");
            dtExcelExport.Columns.Add("Objet");
            dtExcelExport.Columns.Add("Categorie");
            dtExcelExport.Columns.Add("Details");
            dtExcelExport.Columns.Add("Montant");
            dtExcelExport.Columns["Montant"].DataType = System.Type.GetType("System.Decimal");
            dtExcelExport.Columns["Date"].DataType = System.Type.GetType("System.DateTime");
            DataTable dtOperationList = ope.getOperations(searchCriteria, out iRowCount);
            string sOperationDate = "";
            string sOperationType = "";
            string sOperationPlace = "";
            string sOperationObject = "";
            string sOperationAmount = "";
            string sOperationCategory = "";
            string sOperationDetails = "";
            decimal dOperationAmount = 0;
            DateTime dtOperationDate = DateTime.MinValue;
            foreach (DataRow row in dtOperationList.Rows)
            {
                sOperationDate = "";
                sOperationType = "";
                sOperationPlace = "";
                sOperationObject = "";
                sOperationAmount = "";
                sOperationCategory = "";
                sOperationDetails = "";
                dOperationAmount = 0;
                dtOperationDate = DateTime.MinValue;

                if (row.Field<DateTime>("OPE_DATE") != null)
                {
                    if (!_bIsCobalt)
                    {
                        try
                        {
                            if (row.Field<TimeSpan>("OPE_TIME") != null)
                                sOperationDate = row.Field<DateTime>("OPE_DATE").ToString("dd/MM/yyyy") + " " + row.Field<TimeSpan>("OPE_TIME").ToString();
                            else
                                sOperationDate = row.Field<DateTime>("OPE_DATE").ToString("dd/MM/yyyy");
                        }
                        catch (Exception ex)
                        {
                            sOperationDate = row.Field<DateTime>("OPE_DATE").ToString("dd/MM/yyyy");
                        }
                        finally
                        {
                            try { dtOperationDate = DateTime.Parse(sOperationDate, new CultureInfo("fr-FR", true)); }
                            catch (Exception ex) { }
                        }
                    }
                    else
                    {
                        try { dtOperationDate = row.Field<DateTime>("OPE_DATE"); }
                        catch (Exception ex) { }
                    }
                }

                if (row.Field<string>("OPE_CODE_LABEL") != null)
                    sOperationType = row.Field<string>("OPE_CODE_LABEL").ToString();

                if (row.Field<string>("MERC_ADD") != null)
                    sOperationPlace += row.Field<string>("MERC_ADD").ToString() + " ";
                if (row.Field<string>("MERC_ZIP") != null)
                    sOperationPlace += row.Field<string>("MERC_ZIP").ToString() + " ";
                if (row.Field<string>("MERC_CITY") != null)
                    sOperationPlace += row.Field<string>("MERC_CITY").ToString() + " ";
                if (row.Field<string>("COUNTRY") != null)
                    sOperationPlace += row.Field<string>("COUNTRY").ToString();

                if (row.Field<string>("OPE_LABEL") != null)
                    sOperationObject = row.Field<string>("OPE_LABEL").ToString();

                if (row.Field<Decimal>("AMOUNT") != null)
                {
                    sOperationAmount = row.Field<Decimal>("AMOUNT").ToString().Replace('.', ',');
                    try { dOperationAmount = decimal.Parse(sOperationAmount, new CultureInfo("fr-FR", true)); } catch(Exception ex) { }
                    //sOperationAmount = getFormattedAmount(row.Field<Decimal>("AMOUNT").ToString().Replace('.', ','));
                }

                if (row.Field<string>("CATEGORY") != null)
                    sOperationCategory = row.Field<string>("CATEGORY").ToString();//.Replace("","").Replace("","");

                if (row.Field<string>("OPE_LABEL_DET1") != null)
                    sOperationDetails += row.Field<string>("OPE_LABEL_DET1").ToString() + " ";
                if (row.Field<string>("OPE_LABEL_DET2") != null)
                    sOperationDetails += row.Field<string>("OPE_LABEL_DET2").ToString();

                dtExcelExport.Rows.Add(new object[] { dtOperationDate, sOperationType, sOperationPlace, sOperationObject, sOperationCategory, sOperationDetails, dOperationAmount });
            }

            // sorting
            DataView dv = dtExcelExport.DefaultView;
            dv.Sort = "Date asc";
            DataTable sortedDT = dv.ToTable();

            DumpExcel(sortedDT, "Operations_" + _sClientBankAccount + "_" + DateTime.Now.ToString("yyyyMMddHHmmss"));
            //ExportDataSetToExcel(dgTransactionExcelExport(ope.getOperations(searchCriteria, out iRowCount)), "Operations_" + _sClientBankAccount + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xls");
        }
    }
    protected DataGrid dgTransactionExcelExport(DataTable dt)
    {
        DataGrid dg = new DataGrid();
        DataTable dtCustom = new DataTable();

        dg.AutoGenerateColumns = false;
        dg.HeaderStyle.BackColor = System.Drawing.Color.FromArgb(245, 117, 39);
        dg.HeaderStyle.ForeColor = System.Drawing.Color.White;
        dg.HeaderStyle.Font.Bold = true;

        BoundColumn bcOperationDate = new BoundColumn();
        bcOperationDate.HeaderText = "Date";
        bcOperationDate.DataField = "Date";
        bcOperationDate.ItemStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Left;

        BoundColumn bcOperationPlace = new BoundColumn();
        bcOperationPlace.HeaderText = "Lieu";
        bcOperationPlace.DataField = "Lieu";
        bcOperationPlace.ItemStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Left;

        BoundColumn bcOperationType = new BoundColumn();
        bcOperationType.HeaderText = "Type";
        bcOperationType.DataField = "Type";
        bcOperationType.ItemStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Left;

        BoundColumn bcOperationObject = new BoundColumn();
        bcOperationObject.HeaderText = "Objet";
        bcOperationObject.DataField = "Objet";
        bcOperationObject.ItemStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Left;

        BoundColumn bcOperationAmount = new BoundColumn();
        bcOperationAmount.HeaderText = "Montant";
        bcOperationAmount.DataField = "Montant";
        bcOperationAmount.ItemStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Right;

        BoundColumn bcOperationCategory = new BoundColumn();
        bcOperationCategory.HeaderText = "Cat&eacute;gorie";
        bcOperationCategory.DataField = "Categorie";
        bcOperationCategory.ItemStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Left;

        BoundColumn bcOperationDetails = new BoundColumn();
        bcOperationDetails.HeaderText = "D&eacute;tails";
        bcOperationDetails.DataField = "Details";
        bcOperationDetails.ItemStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Left;

        dg.Columns.Add(bcOperationDate);
        dg.Columns.Add(bcOperationType);
        dg.Columns.Add(bcOperationPlace);
        dg.Columns.Add(bcOperationObject);
        dg.Columns.Add(bcOperationCategory);
        dg.Columns.Add(bcOperationDetails);
        dg.Columns.Add(bcOperationAmount);

        dtCustom.Columns.Add("Date");
        dtCustom.Columns.Add("Type");
        dtCustom.Columns.Add("Lieu");
        dtCustom.Columns.Add("Objet");
        dtCustom.Columns.Add("Categorie");
        dtCustom.Columns.Add("Details");
        dtCustom.Columns.Add("Montant");


        string sOperationDate = "";
        string sOperationType = "";
        string sOperationPlace = "";
        string sOperationObject = "";
        string sOperationAmount = "";
        string sOperationCategory = "";
        string sOperationDetails = "";

        char[] MyChar = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };

        foreach (DataRow row in dt.Rows)
        {
            sOperationDate = "";
            sOperationType = "";
            sOperationPlace = "";
            sOperationObject = "";
            sOperationAmount = "";
            sOperationCategory = "";
            sOperationDetails = "";

            if (row.Field<DateTime>("OPE_DATE") != null)
            {
                try
                {
                    if (row.Field<TimeSpan>("OPE_TIME") != null)
                        sOperationDate = row.Field<DateTime>("OPE_DATE").ToString("dd/MM/yyyy") + " " + row.Field<TimeSpan>("OPE_TIME").ToString();
                    else
                        sOperationDate = row.Field<DateTime>("OPE_DATE").ToString("dd/MM/yyyy");
                }
                catch (Exception ex)
                {
                    sOperationDate = row.Field<DateTime>("OPE_DATE").ToString("dd/MM/yyyy");
                }
            }

            if (row.Field<string>("OPE_CODE_LABEL") != null)
                sOperationType = row.Field<string>("OPE_CODE_LABEL").ToString();

            if (row.Field<string>("MERC_ADD") != null)
                sOperationPlace += row.Field<string>("MERC_ADD").ToString() + " ";
            if (row.Field<string>("MERC_ZIP") != null)
                sOperationPlace += row.Field<string>("MERC_ZIP").ToString() + " ";
            if (row.Field<string>("MERC_CITY") != null)
                sOperationPlace += row.Field<string>("MERC_CITY").ToString() + " ";
            if (row.Field<string>("COUNTRY") != null)
                sOperationPlace += row.Field<string>("COUNTRY").ToString();

            if (row.Field<string>("OPE_LABEL") != null)
                sOperationObject = row.Field<string>("OPE_LABEL").ToString();

            if (row.Field<Decimal>("AMOUNT") != null)
                sOperationAmount = getFormattedAmount(row.Field<Decimal>("AMOUNT").ToString().Replace('.', ','));

            if (row.Field<string>("CATEGORY") != null)
                sOperationCategory = row.Field<string>("CATEGORY").ToString();

            if (row.Field<string>("OPE_LABEL_DET1") != null)
                sOperationDetails += row.Field<string>("OPE_LABEL_DET1").ToString() + " ";
            if (row.Field<string>("OPE_LABEL_DET2") != null)
                sOperationDetails += row.Field<string>("OPE_LABEL_DET2").ToString();

            dtCustom.Rows.Add(new object[] { sOperationDate, sOperationType, sOperationPlace, sOperationObject, sOperationCategory, sOperationDetails, sOperationAmount });
        }

        dg.DataSource = dtCustom;
        dg.DataBind();

        return dg;
    }
    protected void ExportDataSetToExcel(DataGrid dg, string filename)
    {
        try
        {
            string attachment = "attachment; filename=" + filename;
            Response.ClearContent();
            Response.Charset = "";
            Response.ContentEncoding = System.Text.Encoding.Default;
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            dg.RenderControl(htw);
            Response.Write(sw.ToString());
            Response.Flush();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.Close();
        }
        catch (Exception ex)
        {
        }
    }
    private void DumpExcel(DataTable tbl, string filename)
    {
        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.ClearContent();
        HttpContext.Current.Response.ClearHeaders();
        HttpContext.Current.Response.Buffer = true;
        HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.UTF8;
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + filename + ".xlsx");

        using (ExcelPackage pack = new ExcelPackage())
        {
            ExcelWorksheet ws = pack.Workbook.Worksheets.Add(filename);
            ws.Cells["A1"].LoadFromDataTable(tbl, true);
            //Set columns width
            ws.Column(1).Width = 20;
            ws.Column(2).Width = 20;
            ws.Column(3).Width = 20;
            ws.Column(4).Width = 20;
            ws.Column(5).Width = 20;
            ws.Column(6).Width = 20;
            ws.Column(7).Width = 15;
            //Set header style
            using (ExcelRange rng = ws.Cells["A1:G1"])
            {
                rng.Style.Font.Bold = true;
                rng.Style.Fill.PatternType = ExcelFillStyle.Solid;
                rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(245, 117, 39));
                rng.Style.Font.Color.SetColor(Color.White);
            }

            //Apply amount format
            using (ExcelRange rng = ws.Cells[2, 7, tbl.Rows.Count + 2, 7])
            {
                rng.Style.Numberformat.Format = "0.00 €";
            }

            //Apply date format
            using (ExcelRange rng = ws.Cells[2, 1, tbl.Rows.Count + 2, 1])
            {
                rng.Style.Numberformat.Format = "dd/MM/yyyy HH:mm:ss";
            }

            //Set RED Color when amount < 0
            for (int i = 0; i < tbl.Rows.Count; i++)
            {
                string sMontant = tbl.Rows[i]["Montant"].ToString().Trim();
                if (sMontant.Length > 0 && sMontant.Substring(0, 1) == "-")
                {
                    ws.Cells[i + 2, 7].Style.Font.Color.SetColor(Color.Red);
                    //ws.Cells[i + 2, 1, i + 2, 7].Style.Font.Color.SetColor(Color.Red);
                }

            }

            var ms = new System.IO.MemoryStream();
            pack.SaveAs(ms);
            ms.WriteTo(HttpContext.Current.Response.OutputStream);
        }

        HttpContext.Current.Response.Flush();
        HttpContext.Current.Response.End();

    }

    protected void btnCancelMinuteOpe_Click(object sender, EventArgs e)
    {
        string sIdRowSelected = hdnRowNumber.Value.ToString();
        string sNumOpe = "";
        int iRowCount = 0;
        string sError = "";

        operation ope = new operation();
        operation.SearchCriteria searchCriteria = (operation.SearchCriteria)ViewState["SearchCriteria"];
        if (_bIsCobalt)
        {
            searchCriteria.bIsCobalt = _bIsCobalt;
            searchCriteria.sIBAN = _sIBAN;
        }
        DataTable dtOperationList = ope.getOperations(searchCriteria, out iRowCount);
        string sOperationDate = "";
        DateTime dtTmp;

        try
        {
            foreach (DataRow row in dtOperationList.Rows)
            {
                if (row.Field<string>("row") != null)
                {
                    if (sIdRowSelected.CompareTo(row.Field<string>("row")) == 0)
                    {
                        sOperationDate = row.Field<DateTime>("OPE_DATE").ToString("yyyy-MM-ddThh:mm:ss");
                        sNumOpe = row.Field<string>("OPE_ID");
                        break;
                    }
                }
            }
        }
        catch(Exception ex)
        {
            sNumOpe = "";
        }

        if (sNumOpe.Length > 0)
        {
            // appel à Cobalt
            if (ope.bCancelMinuteOpe(_sIBAN, sNumOpe, sOperationDate, sError) == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Annulation échouée', \"" + sError + "\", null, null);", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Annulation réussie', '', null, null);", true);
            }
        }
        UpdateOperationList(searchCriteria);        
    }
}
