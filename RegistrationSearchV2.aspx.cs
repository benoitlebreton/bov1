﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Xml.Linq;
using XMLMethodLibrary;
using OfficeOpenXml;
using System.IO;
using OfficeOpenXml.Style;
using System.Drawing;

public partial class RegistrationSearchV2 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack)
        {
            string[] eventArgs = Page.Request["__EVENTARGUMENT"].ToString().Split(';');
            if (eventArgs[0].ToString().ToLower() == "showregistration" && eventArgs.Length > 1)
            {
                //registrationDetails.RegistrationNumber = eventArgs[1];
                //divRegistrationDetails.Visible = true;
                //divRegistrationSearch.Visible = false;

                //registrationDetails.Visible = true;
                //panelRegistrationSearch.Visible = false;

                //btnPrevious.Visible = true;
                //btnPrevious_2.Visible = false;
                //btnValidate.Visible = true;

                //registration.CheckUser _checkUser = registration.GetCurrent(true, eventArgs[1]).checkUser;
                authentication auth = authentication.GetCurrent();
                registration registr = registration.GetRegistrationCheckDetails(eventArgs[1], eventArgs[2]);
                registration.Save(registr, eventArgs[1]);


                string sErrorMessage = "";
                if((registr.iRefUserChecker.ToString() == auth.sRef || registr.iRefUserChecker == 0) && registration.CheckRegistrationAllocate(registr, out sErrorMessage))
                { 
                    Response.Redirect("RegistrationDetailsV2.aspx?No=" + eventArgs[1]);
                }
                //else if(registr.checkUser.iLevel == 2 && registr.sCheckGeneralStatus == "_L1_CHECKS_KO_L2_CHECKS_TO_START_" && registration.CheckRegistrationAllocate(registr))
                else if ((auth.cuCheckUser.iLevel == 2 || (auth.cuCheckUser.iLevel == 1 && auth.cuCheckUser.bIsManager)) && registration.CheckRegistrationAllocate(registr, out sErrorMessage))
                {
                    Response.Redirect("RegistrationDetailsV2.aspx?No=" + eventArgs[1]);
                }
                else
                {
                    if (string.IsNullOrWhiteSpace(sErrorMessage)) { sErrorMessage = "Une erreur est survenue"; }
                    //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Contôle souscription', 'Souscription déjà réservée');initTooltip();FilterStatusDisplayManagement();", true);
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Contôle souscription', '"+HttpUtility.JavaScriptStringEncode(sErrorMessage)+"');", true);
                    btnRegistrationSearch_Click(btnRegistrationSearch, EventArgs.Empty);
                }
                //else Response.Redirect("RegistrationSearchV2.aspx");

                //Response.Redirect("RegistrationDetailsV2.aspx?No=" + eventArgs[1]);
            }
            //else if (eventArgs[0].ToString().ToLower() == "getregistrationsearchfilter" && eventArgs.Length > 1)
            //{
            //    Session["RegistrationSearchFilter"] = eventArgs[1];
            //    divGridView.Visible = true;
            //    divGridViewInfo.Visible = true;
            //    gvRegistrationSearchResult.DataSource = GetRegistrationSearch(CreateXmlStringFromFilter(eventArgs[1]));
            //    gvRegistrationSearchResult.DataBind();

            //    //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "InitAccordion();initTooltip();FilterStatusDisplayManagement();", true);
            //    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "initTooltip();FilterStatusDisplayManagement();", true);
            //}
        }
        else
        {
            CheckRights();
            
            divRegistrationSearch.Visible = true;
            //divRegistrationDetails.Visible = false;

            //registrationDetails.Visible = false;
            panelRegistrationSearch.Visible = true;
            btnPrevious.Visible = false;
            btnPrevious_2.Visible = false;
            btnValidate.Visible = false;

            //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "InitAccordion();initTooltip();FilterStatusDisplayManagement();", true);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "initTooltip();FilterStatusDisplayManagement();", true);
            //hfBeginDate.Value = DateTime.Now.ToString("dd/MM/yyyy");
            //hfEndDate.Value = DateTime.Now.ToString("dd/MM/yyyy");
            //hfPeriod.Value = "";

            if (Request.Params.Count > 0)
            {
                if (Request.Params["NotChecked"] != null && Request.Params["NotChecked"].ToString().Length > 0
                    && Request.Params["NotChecked"].ToString().Trim().ToUpper() == "Y")
                {
                    string sParams = "RefStatus!15,StatusCheck!W,Period!3";
                    Session["RegistrationSearchFilter"] = sParams;
                    divGridView.Visible = true;
                    divGridViewInfo.Visible = true;
                    gvRegistrationSearchResult.DataSource = GetRegistrationSearch(CreateXmlStringFromFilter(sParams));
                    gvRegistrationSearchResult.DataBind();

                    //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "InitAccordion();initTooltip();FilterStatusDisplayManagement();", true);
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "initTooltip();FilterStatusDisplayManagement();", true);
                }
                else
                    getRegistrationSearchFilter();
            }
            else
                getRegistrationSearchFilter();
        }

        ReportingPeriod.PeriodChanged += new EventHandler(ReportingPeriodChanged);
        csClientSearchFilter.ClientFound += new EventHandler(ClientFound);

        //btnRegistrationSearch_Click(btnRegistrationSearch, EventArgs.Empty);
    }

    protected void ClientFound(object sender, EventArgs e)
    {
        int _iRefCustomer = csClientSearchFilter.RefCustomer;
        if (_iRefCustomer > 0)
        {
            int iNbResult;
            DataTable dt = GetRegistrationSearch(CreateXmlStringFromFilter("RefStatus!15,StatusCheck!W,Period!3,RefCustomer!" + _iRefCustomer.ToString()));
            if (dt.Rows.Count > 0)
            {
                gvRegistrationSearchResult.DataSource = dt;
                gvRegistrationSearchResult.DataBind();
                upGridView.Update();
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "initTooltip();FilterStatusDisplayManagement();", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Recherche par client','Aucune alerte pour ce client');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Recherche alert par client','Une erreur est survenue.');", true);
        }
    }

    protected void ReportingPeriodChanged(object sender, EventArgs e)
    {

    }

    protected void CheckRights()
    {
        authentication auth = authentication.GetCurrent();
        string sXmlOut = tools.GetRights(authentication.GetCurrent().sToken, "SC_RegistrationControlSearch");

        List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Page", "RC");

        if (listRC.Count == 0 || listRC[0] != "0")
        {
            Response.Redirect("Default.aspx", true);
        }
        else
        {
            DataTable dtTabs = new DataTable();
            dtTabs.Columns.Add("tab");
            panelReporting.Visible = false;

            panelRegistrationSearch.Visible = true;
            dtTabs.Rows.Add(new object[] { "<li><a href=\"#" + panelRegistrationSearch.ClientID + "\">Recherche souscription</a></li>" });

            auth.cuCheckUser = authentication.GetCheckUserInfos(auth.sToken);
            if (auth.cuCheckUser != null)
            {
                if (auth.cuCheckUser.bIsManager)
                {

                    panelReporting.Visible = true;
                    dtTabs.Rows.Add(new object[] { "<li><a href=\"#" + panelReporting.ClientID + "\">Reporting</a></li>" });
                }

                InitFilterList(auth.cuCheckUser.iLevel, auth.cuCheckUser.bIsManager);
            }

            rptTabs.DataSource = dtTabs;
            rptTabs.DataBind();
        }
    }

    private void InitFilterList(int iLevel, bool bIsManager)
    {
        panelFilterControlStatus.Visible = false;
        panelFilterAccountType.Visible = false;

        authentication auth = authentication.GetCurrent();
        if (auth.cuCheckUser != null && auth.cuCheckUser.iLevel == 2)
        {
            panelFilterControlStatus.Visible = true;
            panelFilterAccountType.Visible = true;
            panelFilterByClient.Visible = true;

            ddlControlStatus.Items.Clear();
            ddlControlStatus.DataTextField = "Name";
            ddlControlStatus.DataValueField = "Value";
            ddlControlStatus.DataSource = GetControlStatusList(iLevel, bIsManager);
            ddlControlStatus.DataBind();
        }
        else if (auth.cuCheckUser.iLevel == 1 && auth.cuCheckUser.bIsManager)
        {
            panelFilterControlStatus.Visible = true;
            ddlControlStatus.Items.Clear();
            ddlControlStatus.DataTextField = "Name";
            ddlControlStatus.DataValueField = "Value";
            ddlControlStatus.DataSource = GetControlStatusList(iLevel, bIsManager);
            ddlControlStatus.DataBind();
            try { ddlControlStatus.Items.FindByValue("_CHECK_NOT_STARTED_").Selected = true; } catch(Exception ex) { }
            try { ddlControlStatus.Items.Remove(ddlControlStatus.Items.FindByValue("_L1_CHECKS_KO_L2_CHECKS_TO_START_")); } catch (Exception ex) { }
        }

        ddlByUserGroup.Items.Clear();
        //ddlByUserGroup.SelectedIndexChanged += ddlByUserGroup_SelectedIndexChanged;
        //ddlByUserGroup.AutoPostBack = true;
        ddlByUserGroup.DataTextField = "Name";
        ddlByUserGroup.DataValueField = "Value";
        ddlByUserGroup.DataSource = GetUserGroup(iLevel, bIsManager);
        ddlByUserGroup.DataBind();

        ddlByUser.Visible = false;
    }
    private DataTable GetControlStatusList(int iLevel, bool bIsManager)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("Name");
        dt.Columns.Add("Value");

        dt.Rows.Add(new object[] { "Tous", "" });
        dt.Rows.Add(new object[] { "Non traités", "_CHECK_NOT_STARTED_" });
        dt.Rows.Add(new object[] { "En attente N2 (KO N1)", "_L1_CHECKS_KO_L2_CHECKS_TO_START_" });

        return dt;
    }
    
    protected DataTable GetUserGroup(int iLevel, bool bIsManager)
    {
        DataTable dt = new DataTable();

        dt.Columns.Add("Name");
        dt.Columns.Add("Value");

        dt.Rows.Add(new object[] { "Tous", "" });
        dt.Rows.Add(new object[] { "Niveau 1", "1" });
        if (iLevel == 2) { dt.Rows.Add(new object[] { "Niveau 2", "2" }); }

        return dt;
    }

    protected DataTable GetUserList(string GroupUserValue)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("Name");
        dt.Columns.Add("Value");

        authentication auth = authentication.GetCurrent();

        DataTable dtUserList = new DataTable();
        dtUserList = authentication.GetUserCheckListByLevel(auth.sToken, ddlByUserGroup.SelectedValue);

        if(dtUserList.Rows.Count > 1)
        {
            dt.Rows.Add(new object[] { "Tous", "" });
        }

        foreach (DataRow dr in dtUserList.Rows)
        {
            dt.Rows.Add(new object[] { dr["FirstName"] + " " + dr["LastName"], dr["Ref"] });
        }

        return dt;
    }

    protected void ddlByUserGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(ddlByUserGroup.SelectedValue))
        {
            ddlByUser.Visible = true;
            ddlByUser.Items.Clear();
            ddlByUser.DataTextField = "Name";
            ddlByUser.DataValueField = "Value";
            
            DataTable dt = GetUserList(ddlByUserGroup.SelectedValue);

            if (dt.Rows.Count > 0)
            {
                ddlByUser.DataSource = dt;
                ddlByUser.DataBind();
            }
            else ddlByUser.Visible = false;
        }
        else ddlByUser.Visible = false;
    }

    protected void onClickPreviousButton(object sender, EventArgs e)
    {
        //if (registrationDetails.Visible && !searchFilter.Visible)
        //{
        //    btnPrevious.Visible = false;
        //    btnPrevious_2.Visible = false;
        //    btnValidate.Visible = false;
        //    registrationDetails.Visible = false;
        //    divRegistrationDetails.Visible = false;
        //    divRegistrationSearch.Visible = true;
        //    searchFilter.Visible = true;
        //    registrationDetails.RegistrationNumber = "";

        //    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "InitAccordion();onChangePageNumber();", true);
        //}

        //Page_Load(null, null);
    }

    protected void onClickValidate(object sender, EventArgs e)
    {

    }

    protected void getRegistrationSearchFilter()
    {
        try
        {
            if (Session["RegistrationSearchFilter"] != null && Session["RegistrationSearchFilter"].ToString().Length > 0)
            {
                divGridView.Visible = true;
                divGridViewInfo.Visible = true;
                gvRegistrationSearchResult.DataSource = GetRegistrationSearch(CreateXmlStringFromFilter(Session["RegistrationSearchFilter"].ToString()));
                gvRegistrationSearchResult.DataBind();

                if (gvRegistrationSearchResult.Rows.Count > 0) { divPagination.Visible = false; }
                else { divPagination.Visible = true; }

                //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "InitAccordion();initTooltip();FilterStatusDisplayManagement();", true);
            }
        }
        catch (Exception ex)
        {

        }
    }

    protected string CreateXmlStringFromFilter(string sParameters)
    {
        string sXML = "";
        authentication auth = authentication.GetCurrent();

        try
        {

            int iPageSize = int.Parse(ddlNbResult.SelectedValue);
            string PackNumber = GetParam(sParameters, ',', '!', "PackNumber");
            string RegistrationCode = GetParam(sParameters, ',', '!', "RegistrationNumber");
            string ClientLastName = GetParam(sParameters, ',', '!', "ClientLastName");
            string ClientFirstName = GetParam(sParameters, ',', '!', "ClientFirstName");
            string ClientBirthDate = GetParam(sParameters, ',', '!', "ClientBirthDate");
            string PhoneNumber = GetParam(sParameters, ',', '!', "PhoneNumber");
            string BeginDate = GetParam(sParameters, ',', '!', "BeginDate");
            string EndDate = GetParam(sParameters, ',', '!', "EndDate");
            string AgencyID = GetParam(sParameters, ',', '!', "AgencyID");
            string CashierCheck = GetParam(sParameters, ',', '!', "CashierCheck");
            string StatusCheck = GetParam(sParameters, ',', '!', "StatusCheck");
            string RefStatus = GetParam(sParameters, ',', '!', "RefStatus");
            string Period = GetParam(sParameters, ',', '!', "Period");
            string ServiceTAG = GetParam(sParameters, ',', '!', "ServiceTAG");
            string RefCustomer = GetParam(sParameters, ',', '!', "RefCustomer");
            int iRefCustomer;

            int iPageIndex;
            if (!int.TryParse(GetParam(sParameters, ',', '!', "PageIndex"), out iPageIndex))
                iPageIndex = 1;

            XElement xRegistr = new XElement("Registration", new XAttribute(new XAttribute("CashierToken", auth.sToken)));

            if (ServiceTAG.Trim().Length > 0 ) { xRegistr.Add(new XAttribute("sRegistrationServiceTAGFilter", ServiceTAG)); }
            if (!string.IsNullOrWhiteSpace(ddlByUserGroup.SelectedValue)) { xRegistr.Add(new XAttribute("iCheckUserGroupLevelFilter", ddlByUserGroup.SelectedValue)); }
            if (!string.IsNullOrWhiteSpace(ddlByUserGroup.SelectedValue) && !string.IsNullOrWhiteSpace(ddlByUser.SelectedValue))
            { xRegistr.Add(new XAttribute("iCheckUserFilter", ddlByUser.SelectedValue)); }
            if (!string.IsNullOrWhiteSpace(ddlControlStatus.SelectedValue)) { xRegistr.Add(new XAttribute("sCheckGeneralStatusFilter", ddlControlStatus.SelectedValue)); }
            if (!string.IsNullOrWhiteSpace(RefCustomer) && int.TryParse(RefCustomer, out iRefCustomer)) {
                string sRegistrationCode = registration.GetRegistrationCodeFromRefCustomer(iRefCustomer);
                if (!string.IsNullOrWhiteSpace(sRegistrationCode)) { xRegistr.Add(new XAttribute("sRegistrationCodeFilter", sRegistrationCode)); }
            }

            xRegistr.Add(
                    new XAttribute("PageSize", iPageSize.ToString()),
                    new XAttribute("PageIndex", iPageIndex.ToString()),

                    new XAttribute("dtRegistrationDateStartFilter", BeginDate),
                    new XAttribute("dtRegistrationDateEndFilter", EndDate),
                    new XAttribute("NbMinutesLastChange", ""),
                    new XAttribute("PartnerID", ""),
                    new XAttribute("PartnerAgencyID", ""),
                    new XAttribute("StatusCheck", StatusCheck),
                    new XAttribute("AgencyID", AgencyID),
                    //iDebug="1"
                    //iCheckUserGroupLevelFilter = "1"
                    new XAttribute("Culture", "fr-FR"));

            /*sXML = new XElement("ALL_XML_IN",
                        new XElement("Registration",
                            
                        )
                    ).ToString();*/

            sXML = new XElement("ALL_XML_IN", xRegistr).ToString();

            initFieldsFromXML(sXML);
        }
        catch (Exception ex)
        {
        }

        return sXML;
    }

    protected void initFieldsFromXML(string sXml)
    {
        if (tools.GetValueFromXml(sXml, "ALL_XML_IN/Registration", "PageSize").Length > 0)
            if (ddlNbResult.Items.FindByValue(tools.GetValueFromXml(sXml, "ALL_XML_IN/Registration", "PageSize")) != null)
                ddlNbResult.SelectedValue = tools.GetValueFromXml(sXml, "ALL_XML_IN/Registration", "PageSize");

        if (tools.GetValueFromXml(sXml, "ALL_XML_IN/Registration", "PageNumber").Length > 0)
            if (ddlPage.Items.FindByValue(tools.GetValueFromXml(sXml, "ALL_XML_IN/Registration", "PageNumber")) != null)
                ddlPage.SelectedValue = tools.GetValueFromXml(sXml, "ALL_XML_IN/Registration", "PageNumber");
    }

    protected DataTable GetRegistrationSearch(string sXML)
    {
        SqlConnection conn = null;
        DataTable dt = new DataTable();

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Partner"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("Registration.P_SearchRegistrationV6", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@sXmlIn", SqlDbType.VarChar, -1);
            cmd.Parameters.Add("@sXmlOut", SqlDbType.VarChar, -1);

            cmd.Parameters["@sXmlIn"].Value = sXML;

            cmd.Parameters["@sXmlOut"].Direction = ParameterDirection.Output;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;
            da.Fill(dt);

            string sXmlOut = cmd.Parameters["@sXmlOut"].Value.ToString();

            /*<ALL_XML_OUT>
				<RESULT RC="71008" ErrorLabel="Session Terminee" />
			</ALL_XML_OUT>*/

            string rc = "";
            GetValueFromXml(sXmlOut, "ALL_XML_OUT/RESULT", "RC", out rc);

            if (rc != null && rc != "" && rc != "0")
            {
                lblNbOfResults.Text = "0";
                ddlPage.Items.Clear();
                btnNextPage.Enabled = false;
                btnPreviousPage.Enabled = false;
                divPagination.Visible = false;

                if (rc == "71008")
                {
                    Session.Remove("Authentication");
                    Response.Redirect("RegistrationSearchV2.aspx");
                }

            }
            else
            {
                rc = "";
                GetValueFromXml(sXmlOut, "ALL_XML_OUT/Registration", "RC", out rc);
                if (!string.IsNullOrWhiteSpace(rc) && rc != "0")
                    tools.LogToFile("XML IN : " + sXML + "|XML OUT :" + sXmlOut, "GetRegistrationSearch");

                string sRowCount = "0";
                GetValueFromXml(sXmlOut, "ALL_XML_OUT/Registration", "NbResult", out sRowCount);
                lblNbOfResults.Text = sRowCount;
                divPagination.Visible = false;

                if (int.Parse(sRowCount) > int.Parse(ddlNbResult.SelectedValue.ToString()))
                {

                    int selectedPage = ddlPage.SelectedIndex;

                    DataTable dtPages = getPages(CalculateTotalPages(int.Parse(sRowCount), int.Parse(ddlNbResult.SelectedValue)));
                    ddlPage.DataSource = dtPages;
                    ddlPage.DataBind();

                    string sPageIndex = ""; int iPageIndex = 0;
                    GetValueFromXml(sXML, "ALL_XML_IN/Registration", "PageIndex", out sPageIndex);
                    if (sPageIndex.Trim().Length > 0 && int.TryParse(sPageIndex, out iPageIndex))
                    {
                        if (ddlPage.Items.Count > 0 && ddlPage.Items.FindByValue(iPageIndex.ToString()) != null)
                            ddlPage.Items.FindByValue(iPageIndex.ToString()).Selected = true;
                    }
                    else if (Session["RegistrationPageIndex"] != null)
                    {
                        if (ddlPage.Items.Count > 0)
                            ddlPage.Items.FindByValue(Session["RegistrationPageIndex"].ToString()).Selected = true;
                    }
                    else if (ddlPage.Items.Count > selectedPage)
                        ddlPage.SelectedIndex = selectedPage;

                    if (int.Parse(ddlPage.SelectedValue) == ddlPage.Items.Count)
                        btnNextPage.Enabled = false;
                    else
                        btnNextPage.Enabled = true;

                    if (int.Parse(ddlPage.SelectedValue) == 1)
                        btnPreviousPage.Enabled = false;
                    else
                        btnPreviousPage.Enabled = true;

                    if (ddlPage.Items.Count > 1)
                        divPagination.Visible = true;
                    else
                        divPagination.Visible = false;

                    lblTotalPage.Text = " / " + ddlPage.Items.Count.ToString();
                }
                else
                {
                    divPagination.Visible = false;
                }
            }
        }
        catch (Exception ex)
        {
            tools.LogToFile("XML IN : "+ sXML +"|DETAILS :" + ex.Message, "GetRegistrationSearch");
        }

        return dt;
    }

    protected void GetValueFromXml(string sXml, string sNode, string sAttribute, out string sValue)
    {
        sValue = "";

        List<string> tagList = CommonMethod.GetAttributeValues(sXml.ToString(), sNode, sAttribute);

        if (tagList.Count > 0)
            sValue = tagList[0];
    }

    protected string GetParam(string ListOfParam, char ParamSeparator, char ValueSeparator, string ParamName)
    {
        string Value = "";

        string[] paramList = ListOfParam.Split(ParamSeparator);

        for (int i = 0; i < paramList.Length; i++)
        {
            if (paramList[i].Split(ValueSeparator)[0] == ParamName && paramList[i].Split(ValueSeparator).Length > 1)
                Value = paramList[i].Split(ValueSeparator)[1];
        }

        return Value;
    }

    protected DataTable getPages(int nbPages)
    {
        DataTable dt = new DataTable();

        dt.Columns.Add("L");
        dt.Columns.Add("V");

        for (int i = 1; i <= nbPages; i++)
        {
            dt.Rows.Add(i.ToString(), i.ToString());
        }

        return dt;
    }

    protected int CalculateTotalPages(double totalRows, int pageSize)
    {
        int totalPages = (int)Math.Ceiling(totalRows / pageSize);

        return totalPages;
    }

    bool isFirstTime = true;
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (this.isFirstTime)
            {
                System.Data.DataView dv = (e.Row.DataItem as
                System.Data.DataRowView).DataView;
                //NbOfResults.Text = dv.Count.ToString();
                this.isFirstTime = false;
            }

            Label lblRegistrationNumber = (Label)e.Row.FindControl("lblRegistrationNumber");
            Label lblRefTransaction = (Label)e.Row.FindControl("lblRefTransaction");
            //Label bDeleteField = (Label)e.Row.FindControl("bDelete");

            if (lblRegistrationNumber != null && lblRefTransaction != null)
            {
                e.Row.Cells[0].Attributes.Add("onclick", "showRegistration( '" + lblRegistrationNumber.Text + "', '" + lblRefTransaction.Text  + "')");
                e.Row.Cells[1].Attributes.Add("onclick", "showRegistration( '" + lblRegistrationNumber.Text + "', '" + lblRefTransaction.Text + "')");
                e.Row.Cells[2].Attributes.Add("onclick", "showRegistration( '" + lblRegistrationNumber.Text + "', '" + lblRefTransaction.Text + "')");
                e.Row.Cells[3].Attributes.Add("onclick", "showRegistration( '" + lblRegistrationNumber.Text + "', '" + lblRefTransaction.Text + "')");
                e.Row.Cells[4].Attributes.Add("onclick", "showRegistration( '" + lblRegistrationNumber.Text + "', '" + lblRefTransaction.Text + "')");
                //e.Row.Cells[5].Attributes.Add("onclick", "showRegistration( '" + lblRegistrationNumber.Text + "', '" + lblRefTransaction.Text + "')");
                //e.Row.Cells[6].Attributes.Add("onclick", "showRegistration( '" + lblRegistrationNumber.Text + "', '" + lblRefTransaction.Text + "')");
                //e.Row.Cells[7].Attributes.Add("onclick", "showRegistration( '" + lblRegistrationNumber.Text + "', '" + lblRefTransaction.Text + "')");
            }

            HiddenField hdn = (HiddenField)e.Row.FindControl("hdnServiceTag");
            if(hdn != null && hdn.Value == "CNJ")
                e.Row.Attributes.Add("onmouseover", "this.style.cursor = 'pointer';this.style.backgroundColor = '#1dc39c'");
            else e.Row.Attributes.Add("onmouseover", "this.style.cursor = 'pointer';this.style.backgroundColor = '#ff9966'");
            e.Row.Attributes.Add("onmouseout", "this.style.cursor = 'auto';this.style.backgroundColor = '#fff'");
        }
    }

    protected string GetCheckUser(string RefUser, string FirstName, string LastName)
    {
        return (!string.IsNullOrWhiteSpace(RefUser))? FirstName + " " + LastName : "<span style=\"font-style:italic; text-transform:capitalize; color:#ccc\">non réservé</span>";
    }

    protected string ConvertToLocalDateTime(string UTCDate)
    {
        DateTime convertedDate = DateTime.SpecifyKind(
                    DateTime.Parse(UTCDate),
                    DateTimeKind.Utc);

        DateTime dt = convertedDate.ToLocalTime();

        return dt.ToString().Substring(0, 16);
    }

    protected System.Drawing.Color ColorStatusLabel(string Color)
    {
        System.Drawing.Color color;

        color = System.Drawing.Color.Black;

        try
        {
            color = System.Drawing.Color.FromName(Color);
        }
        catch (Exception ex)
        {
        }

        return color;
    }
    protected void btnRegistrationSearch_Click(object sender, EventArgs e)
    {
        string sValues = "";
        /*switch (hfPanelActive.Value)
        {
            case "0":
                sValues = "PackNumber!" + txtPackNumber.Text.Trim();
                sValues += ",BeginDate!";
                sValues += ",EndDate!";
                sValues += ",Period!3";
                break;
            case "1":
                sValues = "RegistrationNumber!" + txtRegistrationNumber.Text.Trim();
                sValues += ",BeginDate!";
                sValues += ",EndDate!";
                sValues += ",Period!3";
                break;
            case "2":
                sValues = "PhoneNumber!" + txtPhoneNumber.Text.Trim();
                sValues += ",BeginDate!";
                sValues += ",EndDate!";
                sValues += ",Period!3";
                break;
            case "3":
                sValues = "ClientLastName!" + txtClientLastName.Text.Trim();
                sValues += ",ClientFirstName!" + txtClientFirstName.Text.Trim();
                sValues += ",ClientBirthDate!" + hfClientBirthDate.Value.Trim();
                sValues += ",BeginDate!";
                sValues += ",EndDate!";
                sValues += ",Period!3";
                break;
            default:
            case "4":
                sValues = "AgencyID!" + ddlAgencyID.SelectedValue;
                sValues += ",BeginDate!" + RegistrationSearchByAgencyPeriod.sBeginDate;
                sValues += ",EndDate!" + RegistrationSearchByAgencyPeriod.sEndDate;
                sValues += ",Period!" + RegistrationSearchByAgencyPeriod.sPeriod;

                sValues += ",RefStatus!" + rblStatus.SelectedValue;
                sValues += ",StatusCheck!" + rblStatusCheck.SelectedValue;
                break;
        }*/

        sValues = "AgencyID!";
        sValues += ",BeginDate!";
        sValues += ",EndDate!";
        sValues += ",Period!3";
        sValues += ",RefStatus!15";
        sValues += ",StatusCheck!W";

        sValues += ",ServiceTAG!" + ddlAccountType.SelectedValue;

        int iPageSelected;
        if (divPagination.Visible && ddlPage.SelectedValue.Trim().Length != 0 && int.TryParse(ddlPage.SelectedValue.Trim(), out iPageSelected))
             sValues += ",PageIndex!" + iPageSelected.ToString();
        else sValues += ",PageIndex!1";

        Session["RegistrationSearchFilter"] = sValues;
        divGridView.Visible = true;
        divGridViewInfo.Visible = true;
        gvRegistrationSearchResult.DataSource = GetRegistrationSearch(CreateXmlStringFromFilter(sValues));
        gvRegistrationSearchResult.DataBind();

        //if (gvRegistrationSearchResult.Rows.Count > 0) { divPagination.Visible = true; }
        //else { divPagination.Visible = false; }

        //upFilter.Update();
        upFilterStatus.Update();

        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "initTooltip();FilterStatusDisplayManagement();hideLoading();", true);
    }
    protected void btnDeleteFilter_Click(object sender, EventArgs e)
    {
        string sValues = "";
        sValues += "AgencyID!,RefStatus!15,StatusCheck!W,Period!3";

        upFilterStatus.Update();

        Session["RegistrationSearchFilter"] = sValues;
        divGridView.Visible = true;
        divGridViewInfo.Visible = true;
        gvRegistrationSearchResult.DataSource = GetRegistrationSearch(CreateXmlStringFromFilter(sValues));
        gvRegistrationSearchResult.DataBind();

        //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "InitAccordion(4);initTooltip();FilterStatusDisplayManagement();hideLoading();", true);
    }

    protected bool CreateExportExcelFile(DataTable dt, string sFilename, string sWorkbookLabel, out string sFilePath)
    {
        bool bGenerated = false;
        sFilePath = "";
        try
        {
            if (dt.Rows.Count > 0)
            {
                string sExportsExcelFilePath = ConfigurationManager.AppSettings["ExportsExcelFilePath"].ToString();
                sFilePath = "~/" + sExportsExcelFilePath + sFilename + ".xlsx";

                // Create the file using the FileInfo object
                var file = new FileInfo(Server.MapPath("~/" + sExportsExcelFilePath + sFilename + ".xlsx"));

                // Delete file if exists
                if (file.Exists)
                    file.Delete();

                // Create the package and make sure you wrap it in a using statement
                using (var package = new ExcelPackage(file))
                {
                    // add a new worksheet to the empty workbook
                    ExcelWorksheet worksheet = package.Workbook.Worksheets.Add(sWorkbookLabel);

                    // Add some formatting to the worksheet
                    worksheet.TabColor = Color.Blue;
                    worksheet.DefaultRowHeight = 20;
                    worksheet.HeaderFooter.FirstFooter.LeftAlignedText = string.Format("Generated: {0}", DateTime.Now.ToShortDateString());

                    worksheet.Row(1).Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Row(1).Style.Fill.BackgroundColor.SetColor(Color.FromArgb(245, 117, 39));
                    worksheet.Row(1).Style.Font.Color.SetColor(Color.White);
                    worksheet.Row(1).Style.Font.Bold = true;
                    worksheet.Row(1).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Row(1).Height = 30;

                    worksheet.Cells[1, 1].Value = "Nom Utilisateur";
                    worksheet.Cells[1, 2].Value = "Prénom Utilisateur";
                    worksheet.Cells[1, 2].Value = "Temps de prise en charge (H)";
                    worksheet.Cells[1, 3].Value = "Nb en cours N1";
                    worksheet.Cells[1, 4].Value = "Nb conforme N1 sans changement";
                    worksheet.Cells[1, 5].Value = "Nb conforme N1 avec changement";
                    worksheet.Cells[1, 6].Value = "Nb non conforme N1";
                    worksheet.Cells[1, 7].Value = "Nb non conforme N1 en attente N2";
                    worksheet.Cells[1, 8].Value = "Nb non conforme N1 en cours N2";
                    worksheet.Cells[1, 9].Value = "Nb non conforme N1 conforme N2";
                    worksheet.Cells[1, 10].Value = "Nb non conforme N1 non conforme N2";
                    worksheet.Cells[1, 11].Value = "Nb en cours N2";
                    worksheet.Cells[1, 12].Value = "Nb conforme N2";
                    worksheet.Cells[1, 13].Value = "Nb non conforme N2";
                    worksheet.Cells[1, 14].Value = "Niveau";

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        worksheet.Cells[i + 2, 1].Value = dt.Rows[i]["LastName"];
                        worksheet.Cells[i + 2, 2].Value = dt.Rows[i]["FirstName"];
                        worksheet.Cells[i + 2, 2].Value = dt.Rows[i]["TreatmentHourAverage"];
                        worksheet.Cells[i + 2, 3].Value = int.Parse(dt.Rows[i]["_L1_CHECKS_IN_PROGRESS_"].ToString());
                        worksheet.Cells[i + 2, 4].Value = int.Parse(dt.Rows[i]["_L1_CHECKS_OK_NO_CHANGES_"].ToString());
                        worksheet.Cells[i + 2, 5].Value = int.Parse(dt.Rows[i]["_L1_CHECKS_OK_WITH_CHANGES_"].ToString());
                        worksheet.Cells[i + 2, 6].Value = int.Parse(dt.Rows[i]["_L1_CHECKS_KO_NO_L2_"].ToString());
                        worksheet.Cells[i + 2, 7].Value = int.Parse(dt.Rows[i]["_L1_CHECKS_KO_L2_CHECKS_TO_START_"].ToString());
                        worksheet.Cells[i + 2, 8].Value = int.Parse(dt.Rows[i]["_L1_CHECKS_KO_L2_CHECKS_IN_PROGRESS_"].ToString());
                        worksheet.Cells[i + 2, 9].Value = int.Parse(dt.Rows[i]["_L1_CHECKS_KO_L2_CHECKS_OK_"].ToString());
                        worksheet.Cells[i + 2, 10].Value = int.Parse(dt.Rows[i]["_L1_CHECKS_KO_L2_CHECKS_KO_"].ToString());
                        worksheet.Cells[i + 2, 11].Value = int.Parse(dt.Rows[i]["_L1_NO_CHECK_L2_CHECKS_IN_PROGRESS_"].ToString());
                        worksheet.Cells[i + 2, 12].Value = int.Parse(dt.Rows[i]["_L1_NO_CHECK_L2_CHECKS_OK_"].ToString());
                        worksheet.Cells[i + 2, 13].Value = int.Parse(dt.Rows[i]["_L1_NO_CHECK_L2_CHECKS_KO_"].ToString());
                        worksheet.Cells[i + 2, 14].Value = dt.Rows[i]["Level"];
                    }

                    worksheet.Cells[worksheet.Dimension.Address].AutoFitColumns();

                    package.Save();

                    bGenerated = true;
                }
            }
        }
        catch (Exception ex)
        {

        }

        return bGenerated;
    }

    protected void btnExportExcel_Click(object sender, EventArgs e)
    {
        string sFilePath = "";

        registration.ReportingFilter _reportingFilter = new registration.ReportingFilter();
        try
        {
            _reportingFilter.beginDate = DateTime.Parse(ReportingPeriod.sBeginDate);
            _reportingFilter.endDate = DateTime.Parse(ReportingPeriod.sEndDate);
        }
        catch (Exception ex) { }

        if (CreateExportExcelFile(registration.GetReportingCheckRegistration(authentication.GetCurrent().sToken, _reportingFilter), "controle_souscription", "Controle souscription " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss"), out sFilePath))
            Response.Redirect(sFilePath);
    }

    
}