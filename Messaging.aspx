﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Messaging.aspx.cs" Inherits="Messaging" %>
<%@ Register Src="~/API/CreateMessage.ascx" TagPrefix="asp" TagName="CreateMessage" %>
<%@ Register Src="~/API/MessageHistory.ascx" TagPrefix="asp" TagName="MessageHistory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">

    <style type="text/css">
        #ar-loading {
            background:url('Styles/img/loading2.gif') no-repeat center;
            background-size: 35px 27px;
            display:none;
            position:absolute;
            background-color:rgba(255, 255, 255, 0.5);
            z-index:10000;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

    <div id="ar-loading"></div>

    <h2 style="color: #344b56;margin-bottom:10px;text-transform:uppercase">Compte-Nickel Info</h2>

    <div style="border: 3px solid #344b56; border-radius: 8px; width: 100%; margin: auto;margin-bottom:20px;">
        <div style="margin:20px">
            <div id="panelCreate">
                <asp:UpdatePanel ID="upCreate" runat="server">
                    <ContentTemplate>
                        <asp:CreateMessage ID="CreateMessage1" runat="server" IsPro="true" IsHome="true" ActionAllowed="true" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>

            <div id="panelHistory" style="margin-top:10px;">
                <asp:UpdatePanel ID="upHistory" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:MessageHistory ID="MessageHistory1" runat="server" IsPro="true" IsHome="true" ActionAllowed="true" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        var sRequestControlID = "";
        function BeginRequestHandler(sender, args) {
            pbControl = args.get_postBackElement();
            //console.log(pbControl.id);
            var panel = '';
            
            if (pbControl.id.indexOf('_CreateMessage1_') != -1)
                panel = '#panelCreate';
            else if (pbControl.id.indexOf('_MessageHistory1_') != -1)
                panel = '#panelHistory';

            if (panel.length > 0) {
                var width = $(panel).width();
                var height = $(panel).height();
                $('#ar-loading').css('width', width);
                $('#ar-loading').css('height', height);

                $('#ar-loading').show();

                $('#ar-loading').position({ my: "left top", at: "left top", of: panel });
            }

            sRequestControlID = pbControl.id;
        }
        function EndRequestHandler(sender, args) {
            $('#ar-loading').hide();
        }
    </script>

</asp:Content>

