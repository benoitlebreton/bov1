﻿<%@ Application Language="C#" %>

<script runat="server">

    void Application_Start(object sender, EventArgs e) 
    {
        // Code qui s'exécute au démarrage de l'application

    }
    
    void Application_End(object sender, EventArgs e) 
    {
        //  Code qui s'exécute à l'arrêt de l'application

    }
        
    void Application_Error(object sender, EventArgs e) 
    {
        //  Code qui s’exécute lors d'une erreur dans l’application
        try
        {
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("fr-FR");
            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("fr-FR");
            
            Exception ex = Server.GetLastError();
            
            if (ex != null)
            {
                if (ex.GetBaseException() != null)
                    ex = ex.GetBaseException();

                string sServerName = ConfigurationManager.AppSettings["ServerName"].ToString();
                string sMessage = ex.GetType().ToString();
                
                sMessage += (ex.Message != null && ex.Message.Length > 0) ? " - " + ex.Message : "";
                if(ex.Message.Trim() == "Le fichier n'existe pas.")
                    sMessage += (Request.CurrentExecutionFilePath != null && Request.CurrentExecutionFilePath.Length > 0) ? " - " + Request.CurrentExecutionFilePath : "";
                sMessage += (ex.InnerException != null) ? " - " + ex.InnerException.ToString() : "";
                
                int iRefCustomer = 0;
                try { iRefCustomer = int.Parse(authentication.GetCurrent().sRef); }
                catch (Exception ex2) { iRefCustomer = 0; }

                tools.LogErrorDB("BONickel", sServerName, iRefCustomer, sMessage, ex.StackTrace);
            }
        }
        catch (Exception ex)
        {
        }
    }

    void Session_Start(object sender, EventArgs e) 
    {
        // Code qui s'exécute lorsqu'une nouvelle session démarre

    }

    void Session_End(object sender, EventArgs e) 
    {
        // Code qui s'exécute lorsqu'une session se termine. 
        // Remarque : l'événement Session_End est déclenché uniquement lorsque le mode sessionstate
        // a la valeur InProc dans le fichier Web.config. Si le mode de session a la valeur StateServer 
        // ou SQLServer, l'événement n'est pas déclenché.

    }
       
</script>
