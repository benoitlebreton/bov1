﻿<%@ Page Title="Recherche incident KYC - BO Compte-Nickel" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="AgencyAlertSearch.aspx.cs" Inherits="AgencyAlertSearch" %>
<%@ Register Src="~/API/AgencyAlert.ascx" TagName="KYCAlert" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

    <h2 style="color: #344b56;margin-bottom:10px;text-transform:uppercase">Recherche incident KYC</h2>
    <div style="margin-top:30px">
        <asp:KYCAlert ID="kycAlert" runat="server" />
    </div>
</asp:Content>