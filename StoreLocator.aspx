﻿<%@ Page Language="C#" MasterPageFile="~/Site2.master" AutoEventWireup="true"
    CodeFile="StoreLocator.aspx.cs" Inherits="_StoreLocator" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">

    <script type="text/javascript">
        function CheckFieldValues() {
            var bOk = true;
            $('textarea, input[type="text"]').each(function () {
                var value = $(this).val();
                //console.log(value);
                if (value.indexOf('<') != -1 || value.indexOf('>') != -1) {
                    bOk = false;
                }
            });

            if (!bOk)
                alert('Pour inclure du code HTML, veuillez remplacer les "< >" par des "[ ]".');

            return bOk;
        }

    </script>

    <style type="text/css">
        #ar-loading {
            background:url('Styles/img/loading2.gif') no-repeat center;
            background-size: 35px 27px;
            display:none;
            position:fixed;
            background-color:rgba(255, 255, 255, 0.5);
            z-index:10000;
        }
    </style>

</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    
    <div id="ar-loading"></div>

    <div id="store-locator">
        <asp:UpdatePanel ID="upStores" runat="server">
            <ContentTemplate>
                
                <asp:Panel ID="panelFilter" runat="server" DefaultButton="btnRefresh" style="margin-top:5px">
                    <div class="table" style="width:100%;border-collapse:collapse">
                        <div class="row">
                            <div class="cell" style="width:100%;padding:0 10px">
                                <asp:TextBox ID="txtFilter" runat="server" placeholder="ID SAB, nom, code postal, ville" style="width:100%"></asp:TextBox>
                            </div>
                            <div class="cell" style="white-space:nowrap;padding-right:10px">
                                <asp:CheckBox ID="ckbFilterVisible" runat="server" Text="Visible" Checked="false" />
                            </div>
                            <div class="cell">
                                <asp:Button ID="btnRefresh" runat="server" Text="Filtrer" CssClass="button" OnClick="btnRefresh_Click" />
                            </div>
                            <div class="cell">
                                <asp:Button ID="btnExport" runat="server" Text="Tout exporter" CssClass="button" OnClick="btnExport_Click" />
                            </div>
                        </div>
                    </div>
                </asp:Panel>

                <asp:GridView ID="gvStores" runat="server" CssClass="grid" AllowSorting="True" AutoGenerateColumns="False" ShowFooter="True" AlternatingRowStyle-BackColor="#dddddd"
                onrowediting="EditStore" onrowupdating="UpdateStore"  onrowcancelingedit="CancelEdit" OnSorting="gvStores_Sorting">
                    <AlternatingRowStyle BackColor="#DDDDDD" />
                    <Columns>
                        <asp:TemplateField HeaderText="N°" SortExpression="refAgency" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblRef" runat="server" Text='<%#Eval("refAgency")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ID SAB" SortExpression="IdePDVSAB">
                            <ItemTemplate>
                                <asp:Label ID="lblIdePDVSAB" runat="server"><%#Eval("IdePDVSAB")%></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtIdePDVSAB" runat="server" Text='<%#Eval("IdePDVSAB")%>' style="width:100%" MaxLength="11"></asp:TextBox>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtIdePDVSAB" runat="server" Visible="<%# bActionAllowed %>" placeHolder="ID SAB" style="width:100%" MaxLength="11"></asp:TextBox>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Nom / Nom 2" SortExpression="AgencyName">
                            <ItemTemplate>
                                <asp:Label ID="lblName" runat="server"><%#Eval("AgencyName")%></asp:Label>
                                <br />
                                <asp:Label ID="lblName2" runat="server"><%#Eval("Label1")%></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtName" runat="server" Text='<%#Eval("AgencyName")%>' style="width:100%"></asp:TextBox>
                                <br />
                                <asp:TextBox ID="txtName2" runat="server" Text='<%#Eval("Label1")%>' style="width:100%"></asp:TextBox>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtName" runat="server" Visible="<%# bActionAllowed %>" placeHolder="Nom" style="width:100%"></asp:TextBox>
                                <br />
                                <asp:TextBox ID="txtName2" runat="server" Visible="<%# bActionAllowed %>" placeHolder="Nom 2" style="width:100%"></asp:TextBox>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Adresse" SortExpression="Address">
                            <ItemTemplate>
                                <asp:Label ID="lblAddress" runat="server"><%#Eval("Address")%></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtAddress" runat="server" Text='<%#Eval("Address")%>' style="width:100%"></asp:TextBox>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtAddress" runat="server" Visible="<%# bActionAllowed %>" placeHolder="Adresse" style="width:100%"></asp:TextBox>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Code postal" SortExpression="Zip">
                            <ItemTemplate>
                                <asp:Label ID="lblZip" runat="server"><%#Eval("Zip")%></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtZip" runat="server" Text='<%#Eval("Zip")%>' style="width:100%"></asp:TextBox>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtZip" runat="server" Visible="<%# bActionAllowed %>" placeHolder="Code postal" style="width:100%"></asp:TextBox>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Ville" SortExpression="City">
                            <ItemTemplate>
                                <asp:Label ID="lblCity" runat="server"><%#Eval("City")%></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtCity" runat="server" Text='<%#Eval("City")%>' style="width:100%"></asp:TextBox>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtCity" runat="server" Visible="<%# bActionAllowed %>" placeHolder="Ville" style="width:100%"></asp:TextBox>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Habitants" SortExpression="NbCitizens">
                            <ItemTemplate>
                                <asp:Label ID="lblNbCitizens" runat="server"><%#Eval("NbCitizens")%></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtNbCitizens" runat="server" Text='<%#Eval("NbCitizens")%>' style="width:100%"></asp:TextBox>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtNbCitizens" Visible="<%# bActionAllowed %>" runat="server" placeHolder="Nbre d'habitants" style="width:100%"></asp:TextBox>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Latitude / Longitude" SortExpression="Latitude">
                            <ItemTemplate>
                                <asp:Label ID="lblLatitude" runat="server"><%#Eval("Latitude")%></asp:Label>
                                <br />
                                <asp:Label ID="lblLongitude" runat="server"><%#Eval("Longitude")%></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtLatitude" runat="server" Text='<%#Eval("Latitude")%>' style="width:100%"></asp:TextBox>
                                <br />
                                <asp:TextBox ID="txtLongitude" runat="server" Text='<%#Eval("Longitude")%>' style="width:100%"></asp:TextBox>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtLatitude" Visible="<%# bActionAllowed %>" runat="server" placeHolder="Latitude" style="width:100%"></asp:TextBox>
                                <br />
                                <asp:TextBox ID="txtLongitude" Visible="<%# bActionAllowed %>" runat="server" placeHolder="Longitude" style="width:100%"></asp:TextBox>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Infos complémentaires">
                            <ItemTemplate>
                                <asp:Label ID="lblFreeLabel" runat="server" Visible="false"><%#Eval("FreeLabel1").ToString()%></asp:Label>
                                <b>Horaires d'ouverture :</b><br />
                                <asp:Label ID="lblOpeningHours" runat="server"><%#Eval("OpeningHours").ToString()%></asp:Label><br />
                                <b>Téléphone :</b><br />
                                <asp:Label ID="lblPhoneNumber" runat="server"><%#Eval("PhoneNumber").ToString()%></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtFreeLabel" TextMode="MultiLine" Rows="5" runat="server" Visible="false" Text='<%#HTMLToText(Eval("FreeLabel1").ToString())%>' style="width:100%"></asp:TextBox>
                                <asp:TextBox ID="txtOpeningHours" runat="server" Text='<%#Eval("OpeningHours")%>' placeHolder="Horaires d'ouverture" style="width:100%"></asp:TextBox><br />
                                <asp:TextBox ID="txtPhoneNumber" runat="server" Text='<%#Eval("PhoneNumber")%>' placeHolder="N° de téléphone" style="width:100%"></asp:TextBox>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtFreeLabel" Visible="false" TextMode="MultiLine" Rows="5" runat="server" placeHolder="Infos complémentaires" style="width:100%"></asp:TextBox>
                                <asp:TextBox ID="txtOpeningHours" runat="server" placeHolder="Horaires d'ouverture" style="width:100%"></asp:TextBox><br />
                                <asp:TextBox ID="txtPhoneNumber" runat="server" placeHolder="N° de téléphone" style="width:100%"></asp:TextBox>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Borne" SortExpression="IsMPAD">
                            <ItemTemplate>
                                <asp:Label ID="lblBorne" runat="server"><%#CheckboxToLabel(Eval("IsMPAD").ToString())%></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:CheckBox ID="ckMPAD" runat="server" Checked='<%#Eval("IsMPAD")%>' />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:CheckBox ID="ckMPAD" Visible="<%# bActionAllowed %>" runat="server" Checked="false" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Certifié" SortExpression="Certified">
                            <ItemTemplate>
                                <asp:Label ID="lblCertified" runat="server"><%#CheckboxToLabel(Eval("Certified").ToString())%></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:CheckBox ID="ckCertified" runat="server" Checked='<%#Eval("Certified")%>' />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:CheckBox ID="ckCertified" Visible="<%# bActionAllowed %>" runat="server" Checked="false" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Visible" SortExpression="Visible">
                            <ItemTemplate>
                                <asp:Label ID="lblVisible" runat="server"><%#CheckboxToLabel(Eval("Visible").ToString())%></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:CheckBox ID="ckVisible" runat="server" Checked='<%#ConvertBitToBool(Eval("Visible").ToString())%>' />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:CheckBox ID="ckVisible" Visible="<%# bActionAllowed %>" runat="server" Checked="false" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ShowHeader="False">
                            <EditItemTemplate>
                                <asp:Button ID="Button1" runat="server" Visible="<%# bActionAllowed %>" CausesValidation="True" CommandName="Update" Text="Mettre à jour" OnClientClick="return CheckFieldValues();" />
                                <asp:Button ID="Button2" runat="server" Visible="<%# bActionAllowed %>" CausesValidation="False" CommandName="Cancel" Text="Annuler" />
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Button ID="Button1" runat="server" Visible="<%# bActionAllowed %>" CausesValidation="False" CommandName="Edit" Text="Modifier" />
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Button ID="btnRemove" runat="server"  Visible="<%# bActionAllowed %>" CommandArgument='<%#Eval("refAgency")%>' OnClientClick="return confirm('Voulez-vous vraiment supprimer cette agence ?')" Text="Supprimer" OnClick="DeleteStore"></asp:Button>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Button ID="btnAdd" runat="server" Visible="<%# bActionAllowed %>" Text="Ajouter" OnClick = "AddNewStore" />
                            </FooterTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        <asp:Panel ID="panelAdd" runat="server" Visible="<%# bActionAllowed %>" style="position:relative;left:-10px;top:-5px">
                        <table style="width:100%;border-collapse:collapse">
                            <tr>
                                <th>ID SAB</th>
                                <th>Nom / Nom 2</th>
                                <th>Adresse</th>
                                <th>Code postal</th>
                                <th>Ville</th>
                                <th>Nbre d'habitants</th>
                                <th>Latitude / Longitude</th>
                                <th>Infos complémentaires</th>
                                <th>Borne</th>
                                <th>Certifié</th>
                                <th>Visible</th>
                                <th></th>
                            </tr>
                            <tr>
                                <td><asp:TextBox ID="txtIdePDVSAB" runat="server" placeHolder="ID SAB" style="width:100%" MaxLength="11"></asp:TextBox></td>
                                <td><asp:TextBox ID="txtName" runat="server" placeHolder="Nom" style="width:100%"></asp:TextBox><br /><asp:TextBox ID="txtName2" runat="server" placeHolder="Nom 2" style="width:100%"></asp:TextBox></td>
                                <td><asp:TextBox ID="txtAddress" runat="server" placeHolder="Adresse" style="width:100%"></asp:TextBox></td>
                                <td><asp:TextBox ID="txtZip" runat="server" placeHolder="Code postal" style="width:100%"></asp:TextBox></td>
                                <td><asp:TextBox ID="txtCity" runat="server" placeHolder="Ville" style="width:100%"></asp:TextBox></td>
                                <td><asp:TextBox ID="txtNbCitizens" runat="server" placeHolder="Nbre d'habitants" style="width:100%"></asp:TextBox></td>
                                <td><asp:TextBox ID="txtLatitude" runat="server" placeHolder="Latitude" style="width:100%"></asp:TextBox><br /><asp:TextBox ID="txtLongitude" runat="server" placeHolder="Longitude" style="width:100%"></asp:TextBox></td>
                                <td><asp:TextBox ID="txtFreeLabel" TextMode="MultiLine" Rows="5" runat="server" placeHolder="Infos complémentaires" style="width:100%"></asp:TextBox></td>
                                <td><asp:CheckBox ID="ckMPAD" runat="server" Checked="false" /></td>
                                <td><asp:CheckBox ID="ckCertified" runat="server" Checked="false" /></td>
                                <td><asp:CheckBox ID="ckVisible" runat="server" Checked="false" /></td>
                                <td><asp:Button ID="btnAdd" runat="server" Text="Ajouter" OnClick = "AddNewStore" /></td>
                            </tr>
                        </table>
                        </asp:Panel>
                    </EmptyDataTemplate>
                </asp:GridView>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="gvStores" />
            </Triggers>
        </asp:UpdatePanel>
    </div>

    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        var sRequestControlID = "";
        function BeginRequestHandler(sender, args) {
            pbControl = args.get_postBackElement();

            if (pbControl != null && pbControl.id != 'MainContent_btnExport') {

                var width = $(window).outerWidth();
                var height = $(window).outerHeight();
                $('#ar-loading').css('width', width);
                $('#ar-loading').css('height', height);

                $('#ar-loading').show();

                $('#ar-loading').position({ my: "left top", at: "left top", of: $(window) });

                sRequestControlID = pbControl.id;
            }
        }
        function EndRequestHandler(sender, args) {
            $('#ar-loading').hide();
        }
    </script>

</asp:Content>
