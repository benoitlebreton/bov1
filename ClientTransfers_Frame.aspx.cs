﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ClientTransfers_Frame : System.Web.UI.Page
{
    private string _sClientBankAccount
    {
        get { return ViewState["ClientBankAccount"].ToString(); }
        set { ViewState["ClientBankAccount"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("fr-FR");
        Thread.CurrentThread.CurrentUICulture = new CultureInfo("fr-FR");
        ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
        scriptManager.RegisterPostBackControl(this.btnExcelExport);

        panelTransferListTable.Visible = true;
        lblDateNow.Text = DateTime.Now.ToString("dd MMMM yyyy");

        if (!IsPostBack)
        {
            if (Request.Params["ref"] != null && Request.Params["ref"].Length > 0)
            {
                _sClientBankAccount = Request.Params["ref"].ToString();
                //rptAuthorizationList.DataSource = operation.getUserAuthorizationList(_sClientBankAccount.Trim(), 1, 100, "", "", "ALL");
                //rptAuthorizationList.DataBind();

                operation ope = new operation();
                int iSize = 15;
                int iStep = 1;
                operation.SearchCriteria searchCriteria = new operation.SearchCriteria(iSize, iStep, "", "", _sClientBankAccount, "1", "2", "ALL", true);
                UpdateOperationList(searchCriteria);
                searchCriteria.bForceRefresh = false;
                ViewState["SearchCriteria"] = searchCriteria;

                if (rptTransferList.Items.Count > 0)
                {
                    panelNoSearchResult.Visible = false;
                    rptTransferList.Visible = true;
                }
                else
                {
                    panelNoSearchResult.Visible = true;
                    rptTransferList.Visible = false;
                }
            }
            else
            {
                panelTransferListTable.Visible = false;
            }
            
        }
    }

    protected void rptTransferList_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
    }

    protected void btnShowMore_Click(object sender, EventArgs e)
    {
        int iSize = 15;
        int iStep = 1;
        operation ope = new operation();
        int iRowCount;

        if (ViewState["SearchCriteria"] != null)
        {
            operation.SearchCriteria searchCriteria = (operation.SearchCriteria)ViewState["SearchCriteria"];
            iSize = searchCriteria.iPageSize;
            iStep = searchCriteria.iPageStep;
            iSize += iStep;
            searchCriteria.iPageSize = iSize;

            ViewState["SearchCriteria"] = searchCriteria;
            UpdateOperationList(searchCriteria);
        }
        else
        {
            operation.SearchCriteria searchCriteria = new operation.SearchCriteria(iSize, iStep, "", "", _sClientBankAccount);

            UpdateOperationList(searchCriteria);
            UpdateSearchCriteria(searchCriteria);

            ViewState["SearchCriteria"] = searchCriteria;
        }
    }

    protected void UpdateSearchCriteria(operation.SearchCriteria searchCriteria)
    {
        DateTime date;
        string sDateFrom = "";
        string sDateTo = "";
        panelNoFilters.Visible = false;
        panelFilters.Visible = true;
        string sFiltersLabel = "Virements/Prélèvements ";

        if (DateTime.TryParse(searchCriteria.sDateFrom, out date))
            sDateFrom = date.ToString("dd MMMM yyyy");
        if (DateTime.TryParse(searchCriteria.sDateTo, out date))
            sDateTo = date.ToString("dd MMMM yyyy");

        string sTransferType = ddlTransferStatus.Items.FindByValue(searchCriteria.sOperationType).Text;

        if (sTransferType.Length > 0 && sTransferType != "TOUS")
            sFiltersLabel += ": <span class=\"font-bold uppercase\">" + sTransferType + "</span> ";

        if (sDateFrom.Length > 0 && sDateTo.Length > 0)
        {
            sFiltersLabel += "du <span class=\"font-bold\"  style=\"text-transform:uppercase\">" + sDateFrom + "</span> au <span class=\"font-bold\"  style=\"text-transform:uppercase\">" + sDateTo + "</span>";
        }
        else
        {
            if (sDateFrom.Length > 0)
            {
                sFiltersLabel += "depuis le <span class=\"font-bold uppercase\">" + sDateFrom + "</span>";
            }
            else if (sDateTo.Length > 0)
            {
                sFiltersLabel += "au <span class=\"font-bold uppercase\">" + sDateTo + "</span>";
            }
            else
            {
                sFiltersLabel += "au <span class=\"font-bold uppercase\">" + DateTime.Now.ToString("dd MMMM yyyy") + "</span>, à <span class=\"lblClientTime font-bold\"></span>";
            }
        }

        ltlFilters.Text = sFiltersLabel;
    }
    protected void UpdateNbPages(int iPageSize, int iNbTotal)
    {
        ltlNbResults.Text = "<span class=\"font-bold\">" + iPageSize.ToString() + "</span>/<span class=\"font-bold\">" + iNbTotal.ToString() + "</span>";
    }

    protected void UpdateOperationList(operation.SearchCriteria searchCriteria)
    {
        operation ope = new operation();
        int iRowCount;

        DataTable dt = operation.getUserTransferList(searchCriteria, out iRowCount);
        
        if (iRowCount == dt.Rows.Count)
        {
            btnShowMore.Visible = false;
            UpdateNbPages(iRowCount, iRowCount);
        }
        else
        {
            btnShowMore.Visible = true;
            UpdateNbPages(searchCriteria.iPageSize, iRowCount);
        }

        if (dt.Rows.Count > 0)
        {
            rptTransferList.Visible = true;
            panelNoSearchResult.Visible = false;
            rptTransferList.DataSource = dt;
            rptTransferList.DataBind();
        }
        else
        {
            rptTransferList.Visible = false;
            panelNoSearchResult.Visible = true;
            rptTransferList.DataSource = null;
            rptTransferList.DataBind();
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        if (ViewState["SearchCriteria"] != null)
        {
            operation.SearchCriteria searchCriteria = (operation.SearchCriteria)ViewState["SearchCriteria"];
            DateTime date;
            if (txtDateFrom.Text.Length > 0 && DateTime.TryParse(txtDateFrom.Text, out date))
                searchCriteria.sDateFrom = txtDateFrom.Text;
            else searchCriteria.sDateFrom = "";
            if (txtDateTo.Text.Length > 0 && DateTime.TryParse(txtDateTo.Text, out date))
                searchCriteria.sDateTo = txtDateTo.Text;
            else searchCriteria.sDateTo = "";
            searchCriteria.iPageSize = 50;
            searchCriteria.iPageStep = 50;
            searchCriteria.sOperationType= "1";
            searchCriteria.sDirection = "2";
            searchCriteria.sStatus = ddlTransferStatus.SelectedValue;

            UpdateOperationList(searchCriteria);

            ViewState["SearchCriteria"] = searchCriteria;

            UpdateSearchCriteria(searchCriteria);
        }
    }

    protected void btnExportExcel_Click(object sender, EventArgs e)
    {
        if (ViewState["SearchCriteria"] != null)
        {
            int iRowCount;
            operation ope = new operation();
            operation.SearchCriteria searchCriteria = (operation.SearchCriteria)ViewState["SearchCriteria"];
            DateTime date;
            if (txtDateFrom.Text.Length > 0 && DateTime.TryParse(txtDateFrom.Text, out date))
                searchCriteria.sDateFrom = txtDateFrom.Text;
            else searchCriteria.sDateFrom = "";
            if (txtDateTo.Text.Length > 0 && DateTime.TryParse(txtDateTo.Text, out date))
                searchCriteria.sDateTo = txtDateTo.Text;
            else searchCriteria.sDateTo = "";
            searchCriteria.iPageSize = -1;
            searchCriteria.iPageStep = -1;
            searchCriteria.sOperationType = "1";
            searchCriteria.sDirection = "2";
            searchCriteria.sOperationType = ddlTransferStatus.SelectedValue;

            ViewState["SearchCriteria"] = searchCriteria;

            //ExportDataSetToExcel(dgTransactionExcelExport(operation.getUserTransferList(searchCriteria, out iRowCount)), "Virements_Prelevements_" + _sClientBankAccount + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xls");
        }
    }
    protected DataGrid dgTransactionExcelExport(DataTable dt)
    {
        DataGrid dg = new DataGrid();
        DataTable dtCustom = new DataTable();

        dg.AutoGenerateColumns = false;
        dg.HeaderStyle.BackColor = System.Drawing.Color.FromArgb(245, 117, 39);
        dg.HeaderStyle.ForeColor = System.Drawing.Color.White;
        dg.HeaderStyle.Font.Bold = true;

        BoundColumn bcAuthorizationDate = new BoundColumn();
        bcAuthorizationDate.HeaderText = "Date";
        bcAuthorizationDate.DataField = "Date";
        bcAuthorizationDate.ItemStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Left;

        BoundColumn bcAuthorizationStatus = new BoundColumn();
        bcAuthorizationStatus.HeaderText = "Statut";
        bcAuthorizationStatus.DataField = "Statut";
        bcAuthorizationStatus.ItemStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Left;

        BoundColumn bcAuthorizationResponseCode = new BoundColumn();
        bcAuthorizationResponseCode.HeaderText = "Code Reponse";
        bcAuthorizationResponseCode.DataField = "CodeReponse";
        bcAuthorizationResponseCode.ItemStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Left;

        BoundColumn bcAuthorizationNum = new BoundColumn();
        bcAuthorizationNum.HeaderText = "N° Autorisation";
        bcAuthorizationNum.DataField = "NumAutorisation";
        bcAuthorizationNum.ItemStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Left;

        BoundColumn bcAuthorizationPlace = new BoundColumn();
        bcAuthorizationPlace.HeaderText = "Commerçant";
        bcAuthorizationPlace.DataField = "Lieu";
        bcAuthorizationPlace.ItemStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Left;

        BoundColumn bcAuthorizationAmount = new BoundColumn();
        bcAuthorizationAmount.HeaderText = "Montant";
        bcAuthorizationAmount.DataField = "Montant";
        bcAuthorizationAmount.ItemStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Right;

        dg.Columns.Add(bcAuthorizationDate);
        dg.Columns.Add(bcAuthorizationStatus);
        dg.Columns.Add(bcAuthorizationResponseCode);
        dg.Columns.Add(bcAuthorizationNum);
        dg.Columns.Add(bcAuthorizationPlace);
        dg.Columns.Add(bcAuthorizationAmount);

        dtCustom.Columns.Add("Date");
        dtCustom.Columns.Add("Statut");
        dtCustom.Columns.Add("CodeReponse");
        dtCustom.Columns.Add("NumAutorisation");
        dtCustom.Columns.Add("Lieu");
        dtCustom.Columns.Add("Montant");

        string sAuthorizationDate = "";
        string sAuthorizationStatus = "";
        string sAuthorizationResponseCode = "";
        string sAuthorizationNum = "";
        string sAuthorizationPlace = "";
        string sAuthorizationAmount = "";

        foreach (DataRow row in dt.Rows)
        {
            sAuthorizationDate = "";
            sAuthorizationStatus = "";
            sAuthorizationResponseCode = "";
            sAuthorizationNum = "";
            sAuthorizationPlace = "";
            sAuthorizationAmount = "";

            if (row.Field<DateTime>("DateLocale") != null)
            {
                try
                {
                    if (row.Field<DateTime>("DateLocale") != null)
                        sAuthorizationDate = row.Field<DateTime>("DateLocale").ToString("dd/MM/yyyy HH:mm:ss");
                }
                catch (Exception ex)
                {
                    sAuthorizationDate = row.Field<DateTime>("DateLocale").ToString("dd/MM/yyyy");
                }
            }

            if (row.Field<string>("ReponseAutorisation") != null)
                sAuthorizationStatus = row.Field<string>("ReponseAutorisation").ToString();
            if (row.Field<string>("CodeReponseAutorisation") != null)
                sAuthorizationResponseCode = row.Field<string>("CodeReponseAutorisation").ToString();
            if (row.Field<string>("NumAutorisation") != null)
                sAuthorizationNum = row.Field<string>("NumAutorisation").ToString();

            if (row.Field<string>("LocalisationAccepteur") != null)
                sAuthorizationPlace = row.Field<string>("LocalisationAccepteur").ToString();

            if (row.Field<Decimal>("MontantTransaction") != null)
                sAuthorizationAmount = tools.getFormattedAmount(row.Field<Decimal>("MontantTransaction").ToString().Replace('.', ','));

            dtCustom.Rows.Add(new object[] { sAuthorizationDate, sAuthorizationStatus, sAuthorizationResponseCode, sAuthorizationNum, sAuthorizationPlace, sAuthorizationAmount });
        }

        dg.DataSource = dtCustom;
        dg.DataBind();

        return dg;
    }
    protected void ExportDataSetToExcel(DataGrid dg, string filename)
    {
        try
        {
            string attachment = "attachment; filename=" + filename;
            Response.ClearContent();
            Response.Charset = "";
            Response.ContentEncoding = System.Text.Encoding.Default;
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            dg.RenderControl(htw);
            Response.Write(sw.ToString());
            Response.Flush();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.Close();
        }
        catch (Exception ex)
        {
        }
    }
}