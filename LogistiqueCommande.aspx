﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="LogistiqueCommande.aspx.cs" Inherits="Logistique" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {

            if ($('#CommandSearchByNumber').is(':visible')) {
                $('#<%=txtCommandNumber.ClientID%>').focus();
            }

            $("#divFilters").accordion({
                heightStyle: "content",
                beforeActivate: function (event, ui) {
                    var active = $("#divFilters").accordion("option", "active");
                    if (active == 0) {
                        $('#<%=txtCommandNumber.ClientID%>').val('');
                    }
                    else if (active == 1) {
                        $('#<%=txtCommandQuantity.ClientID%>').val('');
                    }
                    else if (active == 2) {
                        $('#<%=txtCommandDate.ClientID%>').val('');
                    }
                }
            });
            $("#divFilters").accordion("option", "animate", true);

            if ($('#<%=txtCommandQuantity.ClientID%>').val().length > 0) {
                $("#divFilters").accordion({ active: 0 });
            }
            else if ($('#<%=txtCommandDate.ClientID%>').val().length > 0) {
                $("#divFilters").accordion({ active: 1 });
            }

            $('#divFiltersErrorPopup').dialog({
                autoOpen: false,
                resizable: false,
                draggable: false,
                modal: true,
                minWidth: 400,
                title: "Erreur",
                buttons: [{ text: "OK", click: function () { $(this).dialog('close'); } }]
            });
            /*
            $("#divOperation").accordion({
                heightStyle: "content",
                beforeActivate: function (event, ui) {
                    var active = $("#divOperation").accordion("option", "active");
                    if (active == 0) {
                        cleanFilters();
                    }
                    else if (active == 1) {
                        //cleanCommandData();
                    }
                }
            });
            $("#divOperation").accordion("option", "animate", true);*/

            $('#dialog-Recap-Command').dialog({
                autoOpen: false,
                resizable: false,
                width: 400,
                dialogClass: "no-close",
                modal: true,
                title: "Recapitulatif de la création de commande",
                buttons: [
                    { text: "confirmer", click: function () { $(this).dialog('close'); $('#<%=btnConfirmCommand.ClientID%>').click(); } },
                    { text: "annuler", click: function () { $(this).dialog('close'); } }
                ]
            });
        });

        function CheckFilters() {
            var minGlobalSearchAlpha = 3;
            var minGlobalSearchNum = 6;


            if ($('#<%=txtCommandNumber.ClientID%>').val().trim() == ''
                && $('#<%=txtCommandQuantity.ClientID%>').val().trim() == ''
                && $('#<%=txtCommandDate.ClientID%>').val().trim() == '') {
                $('#sFiltersError').html("Veuillez remplir l'un des champs de recherche");
                $('#divFiltersErrorPopup').dialog('open');
                return false;
            }
            else {
                var isOK = true;
                if (isOK)
                    showLoading();

                return isOK;
            }
        }

        function cleanFilters() {
            $('#<%=txtCommandNumber.ClientID%>').val('');
            $('#<%=txtCommandQuantity.ClientID%>').val('');
            $('#<%=txtCommandDate.ClientID%>').val('');
            $('#<%=panelGrid.ClientID%>').visible = false;
            $('#<%=lblRowCount.ClientID%>').val('');
        }

        function CheckCommandFilters() {
            if ($('#<%=txtNombreCarteCommand.ClientID%>').val().trim() == ''
                && $('#<%=txtPrixUnitaire.ClientID%>').val().trim() == ''
                && $('#<%=txtCommandNumberCreation.ClientID%>').val().trim() == '') {
                $('#sFiltersError').html("Veuillez remplir tous les champs du formulaire");
                $('#divFiltersErrorPopup').dialog('open');
                return false;
            }
            else {
                var isOK = true;
                var sErreur = "";
                var reg = /^[0-9]*$/g;
                var value2Test = $('#<%=txtNombreCarteCommand.ClientID%>').val();
                if (reg.test(value2Test) == false || value2Test.length == 0 || parseInt(value2Test) == 0) {
                    isOK = false;
                    sErreur += "Le nombre de cartes commandées est erronée<br/>";
                }

                reg = /^\d*([\,\.]\d{1,2}$)?$/g; // test un montant
                value2Test = $('#<%=txtPrixUnitaire.ClientID%>').val();
                console.log(value2Test);
                if (reg.test(value2Test) == false || value2Test.length == 0 || parseFloat(value2Test) == 0) {
                    isOK = false;
                    sErreur += "Le prix unitaire est erroné<br/>";
                }

                reg = /^[0-9a-zA-Z]*$/g;
                value2Test = $('#<%=txtCommandNumberCreation.ClientID%>').val();
                if (reg.test(value2Test) == false || value2Test.length == 0) {
                    isOK = false;
                    sErreur += "Le numéro de commande est erroné (il ne peut contenir que des lettres et des chiffres)<br/>";
                }

                if (isOK) {
                    $('#sCommandNumber').html($('#<%=txtCommandNumberCreation.ClientID%>').val());
                    $('#sCommandDate').text(GetClientDate());
                    $('#sCommandQuantity').html($('#<%=txtNombreCarteCommand.ClientID%>').val());
                    var currentType = $('#<%=ddlVisuel.ClientID%>').text();
                    $('#sCommandVisualType').html(currentType);
                    
                    $('#dialog-Recap-Command').dialog('open');
                }
                else {
                    $('#sFiltersError').html(sErreur);
                    $('#divFiltersErrorPopup').dialog('open');
                }

                return isOK;
            }
        }

        function GetClientDate() {
            var date = new Date();
            var sDay = date.getDate().toString();
            if (sDay.length == 1)
                sDay = "0" + sDay;
            var sMonth = (date.getMonth() + 1).toString();
            if (sMonth.length == 1)
                sMonth = "0" + sMonth;
            var sYear = date.getFullYear().toString();
            
            return sDay + "/" + sMonth + "/" + sYear;
        }

        function ShowCommandDetails(sRef) {
            document.location.href = "LogistiqueCommande_Details.aspx?RefCommand=" + sRef;
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="dLogistiqueCommande">
        <h2 style="color: #344b56">COMMANDE FOURNISSEUR
        </h2>
        <div id="divOperation">
            <asp:Panel runat="server" ID="pRechercheCommande" DefaultButton="btnSearch">
                <h3 style="color: #344b56">RECHERCHE COMMANDE
                </h3>
                <div style="margin: 20px auto; width: 90%">
                    <div id="divFilters">
                        <h4 style="text-transform: uppercase">Par numéro de commande
                        </h4>
                        <div id="CommandSearchByNumber" style="margin: auto">
                            <div class="table" style="width: 100%">
                                <div class="row">
                                    <div class="cell" style="width: 200px;">
                                        Numéro de commande
                                    </div>
                                    <div class="cell" style="width: 600px;">
                                        <asp:TextBox ID="txtCommandNumber" runat="server" autocomplete="off" Style="width: 600px;" MaxLength="200"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h4 style="text-transform: uppercase">Par nombre de cartes commandées
                        </h4>
                        <div id="CommandSearchByQuantity" style="margin: auto">
                            <div class="table" style="width: 100%">
                                <div class="row">
                                    <div class="cell" style="width: 200px;">
                                        Nombre de cartes
                                    </div>
                                    <div class="cell" style="width: 600px;">
                                        <asp:TextBox ID="txtCommandQuantity" runat="server" autocomplete="off" Style="width: 600px;" MaxLength="200"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h4 style="text-transform: uppercase">par date de commande
                        </h4>
                        <div id="CommandSearchByDate" style="margin: auto">
                            <div class="table" style="width: 100%">
                                <div class="row">
                                    <div class="cell" style="width: 200px;">
                                        Date de commande
                                    </div>
                                    <div class="cell" style="width: 200px">
                                        <asp:TextBox ID="txtCommandDate" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="text-align: center; margin-top: 20px">
                        <input type="button" value="SUPPRIMER LES FILTRES" class="button" onclick="cleanFilters();" style="padding-left: 50px; padding-right: 50px;" />
                        <asp:Button ID="btnSearch" runat="server" Text="Rechercher" OnClick="btnSearch_Click" OnClientClick="return CheckFilters();" Style="width: 200px" CssClass="button" />
                    </div>

                    <asp:UpdatePanel ID="upSearch" runat="server" UpdateMode="Conditional">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
                        </Triggers>
                        <ContentTemplate>
                            <asp:Panel ID="panelGrid" runat="server" Visible="false">
                                <div style="text-align: right; margin-top: 10px">
                                    Nombre de résultat(s) :
                            <asp:Label ID="lblRowCount" runat="server"></asp:Label>
                                </div>
                                <div style="border: 3px solid #344b56; border-top-left-radius: 8px; border-top-right-radius: 8px; margin-top: 5px">
                                    <asp:GridView ID="gvCommandSearchResult" runat="server" AllowSorting="false"
                                        AllowPaging="false" AutoGenerateColumns="False" GridLines="None" CellPadding="10"
                                        DataKeyNames="RefCommand" ShowHeader="true" Width="100%" SortedDescendingCellStyle-Font-Overline="true"
                                        ViewStateMode="Disabled" BorderWidth="0" BackColor="Transparent" OnRowDataBound="GridView1_RowDataBound">
                                        <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="#344b56" Font-Bold="true" />
                                        <EmptyDataTemplate>
                                            Aucune donnée ne correspond à la recherche
                                        </EmptyDataTemplate>
                                        <Columns>
                                            <asp:TemplateField Visible="false" InsertVisible="false">
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="hdnRefCommand" runat="server" Value='<%# Eval("RefCommand")%>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField SortExpression="CommandNumber" InsertVisible="false" ItemStyle-CssClass="buraliste-icon"
                                                ItemStyle-HorizontalAlign="Center" HeaderText="N° Commande">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCommandNumber" Text='<%# Eval("CommandNumber")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Date de commande" SortExpression="CommandDate" InsertVisible="false"
                                                ControlStyle-CssClass="trimmer" ControlStyle-Width="100px" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCommandDate" Text='<%# Eval("CommandDate")%>' CssClass="tooltip"
                                                        ToolTip='<%# Eval("CommandDate")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Nombre de cartes commandées" SortExpression="CommandQuantity" InsertVisible="false"
                                                ControlStyle-CssClass="trimmer" ControlStyle-Width="100px" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCommandQuantity" Text='<%# Eval("CommandQuantity") %>'
                                                        CssClass="tooltip" ToolTip='<%# Eval("CommandQuantity")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField SortExpression="VisualType" HeaderText="Type de visuel" InsertVisible="false" ControlStyle-CssClass=""
                                                ControlStyle-Width="70px" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblVisualType" Text='<%# Eval("VisualType")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField SortExpression="ValidationDate" HeaderText="Validation" InsertVisible="false" ControlStyle-CssClass=""
                                                ControlStyle-Width="70px" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblValidationDate" Text='<%# Eval("ValidationDate")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <FooterStyle BackColor="#344b56" Font-Bold="True" ForeColor="White" />
                                        <HeaderStyle BackColor="#344b56" Font-Bold="True" ForeColor="White" Height="25px" />
                                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                        <RowStyle CssClass="GridViewRowStyle" />
                                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                        <%--<PagerStyle CssClass="GridViewPager" />--%>
                                    </asp:GridView>
                                </div>
                                <div style="background-color: #344b56; color: White; border: 3px solid #344b56; border-bottom-left-radius: 8px; border-bottom-right-radius: 8px; height: 25px">
                                </div>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </asp:Panel>
            <asp:Panel runat="server" ID="pCreateCommand">
                <h3 style="color: #344b56">CREATION COMMANDE
                </h3>
                <div style="margin: 20px auto; width: 90%">
                    <div id="CommandCreation" style="margin: auto">
                        <div class="table" style="width: 100%">
                            <div class="row">
                                <div class="cell" style="width: 35%">
                                    <div class="table">
                                        <div class="row">
                                            <div class="cell">
                                                Nombre de cartes
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="cell" style="width: 100px">
                                                <asp:TextBox ID="txtNombreCarteCommand" runat="server" autocomplete="off"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="cell">
                                                Prix unitaire (€ TTC)
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="cell" style="width: 100px">
                                                <asp:TextBox ID="txtPrixUnitaire" runat="server" autocomplete="off"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="cell" style="width: 35%">
                                    <div class="table">
                                        <div class="row">
                                            <div class="cell" style="width: 100px;">
                                                Visuel
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="cell" style="width: 100px">
                                                <asp:DropDownList ID="ddlVisuel" runat="server">
                                                    <asp:ListItem value="VISTAND" Text="Standard"></asp:ListItem>
                                      <%--              <asp:ListItem Text="Gold"></asp:ListItem>
                                                    <asp:ListItem Text="Chrome"></asp:ListItem>
                                                    <asp:ListItem Text="Black"></asp:ListItem> --%>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="cell">
                                                Numéro de commande
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="cell" style="width: 100px">
                                                <asp:TextBox ID="txtCommandNumberCreation" runat="server" autocomplete="off"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="cell" style="width: 30%; vertical-align: bottom">
                                    <input type="button" value="COMmander" class="button" onclick="CheckCommandFilters();" style="padding-left: 50px; padding-right: 50px;" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <asp:Button ID="btnConfirmCommand" runat="server" Text="commander" OnClick="btnConfirmCommand_Click" Style="visibility:hidden" CssClass="button" />
            </asp:Panel>
            <div id="divFiltersErrorPopup" style="display: none">
                <span id="sFiltersError">Veuillez remplir l'un des champs de recherche</span>
            </div>
        </div>
    </div>
    <div id="dialog-Recap-Command" style="display: none">
        <table>
            <tr>
                <td><span><b>Création commande numéro : </b><span id="sCommandNumber"></span></span></td>
            </tr>
            <tr>
                <td><span><b>Date de création : </b><span id="sCommandDate"></span></span></td>
            </tr>
            <tr>
                <td><span><b>Quantité totale : </b><span id="sCommandQuantity"></span></span></td>
            </tr>
            <tr>
                <td><span><b>Type de visuel : </b><span id="sCommandVisualType"></span></span></td>
            </tr>
        </table>
    </div>
    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        var sRequestControlID = "";
        function BeginRequestHandler(sender, args) {
        }
        function EndRequestHandler(sender, args) {
            hideLoading();
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">
</asp:Content>
