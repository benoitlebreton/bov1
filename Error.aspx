﻿<%@ Page Language="C#" Title="Erreur - BO Compte-Nickel" AutoEventWireup="true" CodeFile="Error.aspx.cs" Inherits="erreur" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>

    <link href="Styles/tipTip.css" rel="Stylesheet" type="text/css" />
    <link href="Styles/UI/jquery-ui-1.9.2.custom.css" rel="Stylesheet" type="text/css" />
    <link href="~/Styles/Site.css" rel="stylesheet" type="text/css" />

    <script src="Scripts/jquery-1.8.3.js" type="text/javascript"></script>
    <script src="Scripts/jquery-ui-1.9.2.custom.min.js" type="text/javascript"></script>
    
</head>
<body>
    <form id="form1" runat="server">
        <div class="page">
            <div class="header">
                <div class="title">
                    <div style="display:table; width:100%">
                        <div style="display:table-row">
                            <div style="display:table-cell; width:70%">
                                <div style="margin:5px; display:table;">
                                    <div style="display:table-row">
                                        <div style="display:table-cell; vertical-align:middle"><img src="Styles/Img/LogoNickel.png" alt="NICKEL Backoffice" height="110px" /></div>
                                        <div style="display:table-cell;padding-left:20px; padding-top:5px ; vertical-align:middle; font-size:42px; color:#1A171B; font-weight:bold; font-style:normal; text-transform:uppercase; font-family:dinBold, Verdana">Backoffice</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clear">
            </div>
            <div class="content">
                <div style="width: 100%" class="table">
                    <div class="row">
                        <div style="display: table-cell; vertical-align: top">
                            <div class="main" style="min-height:100px">
                            
                                <div style="width:80%;margin:100px auto">
                                    <div class="font-orange" style="font-size:3em">
                                        <asp:Label ID="lblErrorTitle" runat="server"></asp:Label>
                                    </div>
                                    <div style="font-size:1em;">
                                        <asp:Literal ID="lblErrorMessage" runat="server"></asp:Literal>
                                    </div>

                                    <div style="text-align:right; margin-top:50px">
                                        <asp:Button ID="btnHome" runat="server" CssClass="buttonHigher" Text="RETOURNER &Agrave; LA PAGE D'ACCUEIL" PostBackUrl="~/Default.aspx" />
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
