﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using XMLMethodLibrary;

public partial class Messaging : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            CheckRights();
        }

        CreateMessage1.validated += new EventHandler(UpdateHistory);
        MessageHistory1.unpublished += new EventHandler(UpdateHistory);
    }

    protected void CheckRights()
    {
        string sXmlOut = tools.GetRights(authentication.GetCurrent().sToken, "SC_ProMessage");

        List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Page", "RC");

        if (listRC.Count == 0 || listRC[0] != "0")
        {
            Response.Redirect("Default.aspx", true);
        }
        else
        {
            List<string> listAction = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/Page/Action");

            for (int i = 0; i < listAction.Count; i++)
            {
                List<string> listTagAction = CommonMethod.GetAttributeValues(listAction[i], "Action", "TagAction");
                List<string> listActionAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ActionAllowed");

                if (listTagAction.Count > 0)
                {
                    switch (listTagAction[0])
                    {
                        case "ManageProMessage":
                            if (listActionAllowed.Count > 0 && listActionAllowed[0] == "1")
                            {
                                CreateMessage1.ActionAllowed = true;
                                MessageHistory1.ActionAllowed = true;
                            }
                            else
                            {
                                CreateMessage1.ActionAllowed = false;
                                MessageHistory1.ActionAllowed = false;
                            }
                            break;
                    }
                }
            }
        }
    }

    protected void UpdateHistory(object sender, EventArgs e)
    {
        MessageHistory1.SetMessages();
        upHistory.Update();
    }
}