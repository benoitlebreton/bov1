﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using XMLMethodLibrary;
using System.Runtime.Serialization;
using System.Globalization;

public partial class CreOrderValidation : System.Web.UI.Page
{
    private int iPageSize = 6;
    [Serializable()]
    public class SearchCriteria : ISerializable
    {
        public int iPageSize { get; set; }
        public int iPageIndex { get; set; }
        public string dtCREOrderUTCDateTimeFrom { get; set; }
        public string dtCREOrderUTCDateTimeTo { get; set; }
        public int iRefCREOrder { get; set; }
        public int iCREOrderRefUser { get; set; }
        public string sTAGFeature { get; set; }
        public string sOperationTAG { get; set; }
        public int iCREOrderStatus { get; set; }
        public string dtCREValidationUTCDateTimeFrom { get; set; }
        public string dtCREValidationUTCDateTimeTo { get; set; }
        public int iCREValidationRefUser { get; set; }
        public string sUserComment { get; set; }
        public string sOrderBy { get; set; }
        public string sOrderAsc { get; set; }

        public SearchCriteria(int iPageSize, int iPageIndex, string dtCREOrderUTCDateTimeFrom, string dtCREOrderUTCDateTimeTo)
        {
            this.iPageSize = iPageSize;
            this.iPageIndex = iPageIndex;
            this.dtCREOrderUTCDateTimeFrom = dtCREOrderUTCDateTimeFrom;
            this.dtCREOrderUTCDateTimeTo = dtCREOrderUTCDateTimeTo;
            this.iCREOrderStatus = 2; // par défaut on affiche que celles à valider
            this.sOrderAsc = "DESC";
            this.sTAGFeature = "";
            this.sOperationTAG = "";
            this.sOrderBy = "iRefCREOrder";
            this.dtCREValidationUTCDateTimeFrom = "";
            this.dtCREValidationUTCDateTimeTo = "";
            this.sUserComment = "";
            this.iCREValidationRefUser = 0;
        }

        public SearchCriteria(int iPageSize, int iPageStep, string dtCREOrderUTCDateTimeFrom, string dtCREOrderUTCDateTimeTo
            , int iRefCREOrder, int iCREOrderRefUser, string sTAGFeature, string sOperationTAG, int iCREOrderStatus
            , string dtCREValidationUTCDateTimeFrom, string dtCREValidationUTCDateTimeTo, int iCREValidationRefUser
            , string sUserComment, string sOrderBy, string sOrderAsc)
        {
            this.iPageSize = iPageSize;
            this.iPageIndex = iPageIndex;
            this.dtCREOrderUTCDateTimeFrom = dtCREOrderUTCDateTimeFrom;
            this.dtCREOrderUTCDateTimeTo = dtCREOrderUTCDateTimeTo;
            this.iRefCREOrder = iRefCREOrder;
            this.iCREOrderRefUser = iCREOrderRefUser;
            this.sTAGFeature = sTAGFeature;
            this.sOperationTAG = sOperationTAG;
            this.iCREOrderStatus = iCREOrderStatus;
            this.dtCREValidationUTCDateTimeFrom = dtCREValidationUTCDateTimeFrom;
            this.dtCREValidationUTCDateTimeTo = dtCREValidationUTCDateTimeTo;
            this.iCREValidationRefUser = iCREValidationRefUser;
            this.sUserComment = sUserComment;
            this.sOrderBy = sOrderBy;
            this.sOrderAsc = sOrderAsc;
        }

        //Deserialization constructor.
        public SearchCriteria(SerializationInfo info, StreamingContext ctxt)
        {
            //Get the values from info and assign them to the appropriate properties
            iPageSize = (int)info.GetValue("iPageSize", typeof(int));
            iPageIndex = (int)info.GetValue("iPageIndex", typeof(int));
            dtCREOrderUTCDateTimeFrom = (string)info.GetValue("dtCREOrderUTCDateTimeFrom", typeof(string));
            dtCREOrderUTCDateTimeTo = (string)info.GetValue("dtCREOrderUTCDateTimeTo", typeof(string));
            iRefCREOrder = (int)info.GetValue("iRefCREOrder", typeof(int));
            iCREOrderRefUser = (int)info.GetValue("iCREOrderRefUser", typeof(int));
            sTAGFeature = (string)info.GetValue("sTAGFeature", typeof(string));
            sOperationTAG = (string)info.GetValue("sOperationTAG", typeof(string));
            iCREOrderStatus = (int)info.GetValue("iCREOrderStatus", typeof(int));
            dtCREValidationUTCDateTimeFrom = (string)info.GetValue("dtCREValidationUTCDateTimeFrom", typeof(string));
            dtCREValidationUTCDateTimeTo = (string)info.GetValue("dtCREValidationUTCDateTimeTo", typeof(string));
            iCREValidationRefUser = (int)info.GetValue("iCREValidationRefUser", typeof(int));
            sUserComment = (string)info.GetValue("sUserComment", typeof(string));
            sOrderBy = (string)info.GetValue("sOrderBy", typeof(string));
            sOrderAsc = (string)info.GetValue("sOrderAsc", typeof(string));
        }

        //Serialization function.
        public void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            //You can use any custom name for your name-value pair. But make sure you
            // read the values with the same name. For ex:- If you write EmpId as "EmployeeId"
            // then you should read the same with "EmployeeId"
            info.AddValue("iPageSize", iPageSize);
            info.AddValue("iPageIndex", iPageIndex);
            info.AddValue("dtCREOrderUTCDateTimeFrom", dtCREOrderUTCDateTimeFrom);
            info.AddValue("dtCREOrderUTCDateTimeTo", dtCREOrderUTCDateTimeTo);
            info.AddValue("iRefCREOrder", iRefCREOrder);
            info.AddValue("iCREOrderRefUser", iCREOrderRefUser);
            info.AddValue("sTAGFeature", sTAGFeature);
            info.AddValue("sOperationTAG", sOperationTAG);
            info.AddValue("iCREOrderStatus", iCREOrderStatus);
            info.AddValue("dtCREValidationUTCDateTimeFrom", dtCREValidationUTCDateTimeFrom);
            info.AddValue("dtCREValidationUTCDateTimeTo", dtCREValidationUTCDateTimeTo);
            info.AddValue("iCREValidationRefUser", iCREValidationRefUser);
            info.AddValue("sUserComment", sUserComment);
            info.AddValue("sOrderBy", sOrderBy);
            info.AddValue("sOrderAsc", sOrderAsc);
        }
    }

    private string _sActionAuthorized
    {
        get { if (Session["BoCobaltActionAutho"] != null) return Session["BoCobaltActionAutho"].ToString(); else return "0"; }
    }

    private SearchCriteria _SearchCriteria
    {
        get { return ViewState["creSearchCriteria"] as SearchCriteria; }
        set { ViewState["creSearchCriteria"] = value; }
    }

    private string _sListAuthorizedCre
    {
        get { return (ViewState["ListAuthorizedCre"] != null) ? ViewState["ListAuthorizedCre"].ToString() : null; }
        set { ViewState["ListAuthorizedCre"] = value; }
    }

    private string _sCurrentFeature
    {
        get { return (ViewState["CurrentFeature"] != null) ? ViewState["CurrentFeature"].ToString() : null; }
        set { ViewState["CurrentFeature"] = value; }
    }

    private Dictionary<string, string> _dCodeOperation
    {
        get { return (ViewState["dCodeOperation"] != null) ? ViewState["dCodeOperation"] as Dictionary<string, string> : null; }
        set { ViewState["dCodeOperation"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!CRETools.CheckRights(authentication.GetCurrent().sToken))
            {
                Response.Redirect("Default.aspx");
            }
            else
            {
                _dCodeOperation = new Dictionary<string, string>();
	            //iRefCREOrder
	            if (Request.Params["iRefCREOrder"] != null && Request.Params["iRefCREOrder"].Length > 0)
	            {
	                int iTmp = 0;
	                if (int.TryParse(Request.Params["iRefCREOrder"].ToString(), out iTmp))
	                {
	                    Session["iRefCREOrder"] = iTmp;
	                    Response.Redirect("CreOrderValidation.aspx");
	                }
	            }
	            InitSearchCriteria();
	            GetOperationDiverseListe();
            }
        }
    }

    protected bool CheckRight()
    {
        bool bResult = false;
        string sXmlOut = tools.GetRights(authentication.GetCurrent().sToken, "SC_Menu");

        List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Page", "RC");

        if (listRC.Count > 0 && listRC[0] == "0")
        {
            List<string> listAction = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/Page/Action");

            for (int i = 0; i < listAction.Count; i++)
            {
                List<string> listTagAction = CommonMethod.GetAttributeValues(listAction[i], "Action", "TagAction");
                List<string> listViewAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ViewAllowed");
                List<string> listActionAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ActionAllowed");

                if (listTagAction.Count > 0 && listViewAllowed.Count > 0 && listViewAllowed[0] == "1")
                {
                    switch (listTagAction[0])
                    {
                        case "BoCobalt":
                            bResult = true;
                            break;
                    }
                }
            }
        }
        return bResult;
    }

    protected void InitSearchCriteria()
    {
        txtDateFrom.Text = "01/01/2000";
        txtDateTo.Text = DateTime.Now.ToString("dd/MM/yyyy");
        //_SearchCriteria = new SearchCriteria(15, 1, "01/01/2000 00:00:00", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss"));
        _SearchCriteria = new SearchCriteria(iPageSize, 1, "01/01/2000 00:00:00", DateTime.Now.ToString("dd/MM/yyyy 23:59:59"));
        if (Session["iRefCREOrder"] != null)
        {
            int iTmp = 0;
            if (int.TryParse(Session["iRefCREOrder"].ToString(), out iTmp))
            {
                _SearchCriteria.iRefCREOrder = iTmp;
                _SearchCriteria.iCREOrderStatus = 0;
            }
            Session["iRefCREOrder"] = null;
        }
        initFeatureDdl();
    }

    protected void UpdateSearchCriteria()
    {
        _SearchCriteria.dtCREOrderUTCDateTimeFrom = txtDateFrom.Text + " 00:00:00";
        _SearchCriteria.dtCREOrderUTCDateTimeTo = txtDateTo.Text + " 23:59:59";
        _SearchCriteria.iCREOrderStatus = int.Parse(rblStatus.SelectedValue.ToString());
        _SearchCriteria.sTAGFeature = ddlFeature.SelectedValue;
        _SearchCriteria.sOperationTAG = ddlTypeCre.SelectedValue;
        _SearchCriteria.sOrderAsc = ddlOrderAsc.SelectedValue;
        _SearchCriteria.sOrderBy = ddlOrderBy.SelectedValue;
        _SearchCriteria.iPageSize = iPageSize;
    }

    protected void btnShowMore_Click(object sender, EventArgs e)
    {
        _SearchCriteria.iPageSize += iPageSize;
        GetOperationDiverseListe();
    }

    protected void GetOperationDiverseListe()
    {
        SqlConnection conn = null;

        try
        {
            DataTable dt = new DataTable();
            XElement xXmlIn = new XElement("CreOrder"
                                    , new XAttribute("iRefUser", authentication.GetCurrent().sRef)
                                    , new XAttribute("TOKEN", authentication.GetCurrent().sToken)
                                    , new XAttribute("iPageSize", _SearchCriteria.iPageSize)
                                    , new XAttribute("iPageIndex", _SearchCriteria.iPageIndex)
                                    , new XAttribute("dtCREOrderUTCDateTimeFrom", _SearchCriteria.dtCREOrderUTCDateTimeFrom)
                                    , new XAttribute("dtCREOrderUTCDateTimeTo", _SearchCriteria.dtCREOrderUTCDateTimeTo)
                                    , new XAttribute("sOrderAsc", _SearchCriteria.sOrderAsc)
                                    , new XAttribute("iRefCREOrder", _SearchCriteria.iRefCREOrder)
                                    , new XAttribute("iCREOrderRefUser", _SearchCriteria.iCREOrderRefUser)
                                    //, new XAttribute("iCREValidationRefUser", _SearchCriteria.iCREValidationRefUser)
                                    );

            switch (_SearchCriteria.iCREOrderStatus)
            {
                case 5:
                    xXmlIn.Add(new XAttribute("iCREOrderStatus", _SearchCriteria.iCREOrderStatus));
                    xXmlIn.Add(new XAttribute("iCRECr", 0));
                    break;
                case 6:
                    xXmlIn.Add(new XAttribute("iCREOrderStatus", 5));
                    xXmlIn.Add(new XAttribute("iCRECr", -1));
                    break;
                case 0:
                case 2:
                case 3:
                case 4:
                default:
                    xXmlIn.Add(new XAttribute("iCREOrderStatus", _SearchCriteria.iCREOrderStatus));
                    break;
            }


            if (_SearchCriteria.sOrderBy != null && _SearchCriteria.sOrderBy.Length > 0)
                xXmlIn.Add(new XAttribute("sOrderBy", _SearchCriteria.sOrderBy));
            if (_SearchCriteria.sTAGFeature != null && _SearchCriteria.sTAGFeature.Length > 0)
                xXmlIn.Add(new XAttribute("sTAGFeature", _SearchCriteria.sTAGFeature));
            if (_SearchCriteria.sOperationTAG != null && _SearchCriteria.sOperationTAG.Length > 0)
                xXmlIn.Add(new XAttribute("sOperationTAG", _SearchCriteria.sOperationTAG));
            if (_SearchCriteria.dtCREValidationUTCDateTimeFrom != null && _SearchCriteria.dtCREValidationUTCDateTimeFrom.Length > 0)
                xXmlIn.Add(new XAttribute("dtCREValidationUTCDateTimeFrom", _SearchCriteria.dtCREValidationUTCDateTimeFrom));
            if (_SearchCriteria.dtCREValidationUTCDateTimeTo != null && _SearchCriteria.dtCREValidationUTCDateTimeTo.Length > 0)
                xXmlIn.Add(new XAttribute("dtCREValidationUTCDateTimeTo", _SearchCriteria.dtCREValidationUTCDateTimeTo));
            if (_SearchCriteria.sUserComment != null && _SearchCriteria.sUserComment.Length > 0)
                xXmlIn.Add(new XAttribute("sUserComment", _SearchCriteria.sUserComment));

            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRE"].ConnectionString);
            //conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("[CRE].[P_GetCREOrdersV2]", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@sXmlIn", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@sXmlOut", SqlDbType.VarChar, -1);
            cmd.Parameters["@sXmlOut"].Direction = ParameterDirection.Output;

            cmd.Parameters["@sXmlIn"].Value = new XElement("ALL_XML_IN", xXmlIn).ToString();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);

            string sXmlOut = cmd.Parameters["@sXmlOut"].Value.ToString();
            string sRC = tools.GetValueFromXml(sXmlOut, "ALL_XML_OUT/CreOrder", "RC");
            string sRowCount = tools.GetValueFromXml(sXmlOut, "ALL_XML_OUT/CreOrder", "NB_CRE");
            if (sRC == "0" && dt.Rows.Count > 0)
            {
                rptOperationList.DataSource = dt;
                rptOperationList.DataBind();
                panelNoSearchResult.Visible = false;
                pOperation.Visible = true;
                int iRowCount = int.Parse(sRowCount);
                if (iRowCount == dt.Rows.Count)
                {
                    btnShowMore.Visible = false;
                    UpdateNbPages(iRowCount, iRowCount);
                }
                else
                {
                    btnShowMore.Visible = true;
                    UpdateNbPages(_SearchCriteria.iPageSize, iRowCount);
                }
                /*if (listOpe.Count > 5)
                    pOperation.ScrollBars = new ScrollBars(); */
            }
            else
            {
                string sERROR_MSG = tools.GetValueFromXml(sXmlOut, "ALL_XML_OUT/CreOrder", "ERROR_MSG");
                if (sERROR_MSG != null && sERROR_MSG.Length > 0)
                {
                    //AlertMessage("Erreur à la récupération", sERROR_MSG);
                }
                panelNoSearchResult.Visible = true;
                pOperation.Visible = false;
                UpdateNbPages(0, 0);
            }
        }
        catch (Exception e)
        {
            AlertMessage("Exception à la récupération", e.Message);
            panelNoSearchResult.Visible = true;
            pOperation.Visible = false;
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }
    }

    protected void UpdateNbPages(int iPageSize, int iNbTotal)
    {
        ltlNbResults.Text = "<span class=\"font-bold\">" + iPageSize.ToString() + "</span>/<span class=\"font-bold\">" + iNbTotal.ToString() + "</span>";
    }

    protected string GetCodeOperation(string sTypeOpe)
    {
        string sOperationType = "non défini";

        try
        {
            sOperationType = _dCodeOperation[sTypeOpe];
        }
        catch (Exception ex)
        {
            sOperationType = "non défini";
        }

        return sOperationType;
    }

    protected string getActionAuthorized(string sOpeStatus, string sIdOperation, string sCanBeRejected, string sCanBeDeleted, string sCanBeValidated,
        string sMontant1, string sMontant2, string sOperationTAG, string sTAGFeature, string sCRECr)
    {
        string sRetour = "";

        switch (sOpeStatus)
        {
            case "2":
                if (sCanBeValidated == "True")
                {
                    sRetour += "<td align=\"center\">";
                    sRetour += "<span class=\"tooltip\" tooltiptext=\"Valider\" style=\"position: relative; top: -4px\">";
                    sRetour += "<img src=\"Styles/Img/check_2.png\" style=\"height: 15px; width:15px; cursor: pointer\" alt=\"Valider\" onclick=\"ShowConfirmValidOpe(";
                    sRetour += sIdOperation;
                    sRetour += ",'" + IsValidationPossible(sTAGFeature, sOperationTAG, sMontant1, sMontant2);
                    sRetour += "');\" /></span></td>";
                }

                if (sCanBeRejected == "True" || sCanBeDeleted == "True")
                {
                    string sToolTip = "Rejeter";

                    if (sCanBeRejected == "True")
                        sToolTip = "Rejeter";
                    else
                        sToolTip = "Supprimer";

                    sRetour += "<td align=\"center\">";
                    sRetour += "<span class=\"tooltip\" tooltiptext=\"";
                    sRetour += sToolTip;
                    sRetour += "\" style=\"position: relative; top: -4px\">";
                    sRetour += "<img src=\"Styles/Img/delete.png\" style=\"height: 15px; cursor: pointer;width:15px\" alt=\"";
                    sRetour += sToolTip;
                    sRetour += "\" onclick =\"ShowConfirmCancelOpe(";
                    sRetour += sIdOperation;
                    sRetour += ");\" /></span></td>";
                }

                if (sRetour.Length == 0)
                    sRetour = "en attente";
                break;
            case "3":
                sRetour = "annulé";
                break;
            case "4":
                sRetour = "rejeté";
                break;
            case "5":
                if (sCanBeValidated == "True")
                {
                    sRetour += "<td align=\"center\">";
                    sRetour += "<span class=\"tooltip\" tooltiptext=\"Valider\" style=\"position: relative; top: -4px\">";
                    sRetour += "<img src=\"Styles/Img/check_2.png\" style=\"height: 15px; width:15px; cursor: pointer\" alt=\"Valider\" onclick=\"ShowConfirmValidOpe(";
                    sRetour += sIdOperation;
                    sRetour += ",'" + IsValidationPossible(sTAGFeature, sOperationTAG, sMontant1, sMontant2);
                    sRetour += "');\" /></span></td>";
                }

                if (sCanBeRejected == "True" || sCanBeDeleted == "True")
                {
                    string sToolTip = "Rejeter";

                    if (sCanBeRejected == "True")
                        sToolTip = "Rejeter";
                    else
                        sToolTip = "Supprimer";

                    sRetour += "<td align=\"center\">";
                    sRetour += "<span class=\"tooltip\" tooltiptext=\"";
                    sRetour += sToolTip;
                    sRetour += "\" style=\"position: relative; top: -4px\">";
                    sRetour += "<img src=\"Styles/Img/delete.png\" style=\"height: 15px; cursor: pointer;width:15px\" alt=\"";
                    sRetour += sToolTip;
                    sRetour += "\" onclick =\"ShowConfirmCancelOpe(";
                    sRetour += sIdOperation;
                    sRetour += ");\" /></span></td>";
                }

                if (sRetour.Length == 0)
                {
                    if (sCRECr == "0")
                        sRetour = "envoyé";
                    else
                        sRetour = "échoué";
                }
                break;
        }

        return sRetour;
    }

    protected string IsValidationPossible(string sTAGFeature, string sOperationTAG, string sMontant1, string sMontant2)
    {
        decimal dMontantMin, dMontantMax, dMontantTmp;
        string sLibelleError = "";
        //sTAGFeature = "_TESTS_USR_YJU_GRP_RVN_";

        List<string> listCREFeature = CommonMethod.GetTags(_sListAuthorizedCre, "ALL_XML_OUT/CREFeaturesValidation/CREFeature");
        for (int idxCreFeature = 0; idxCreFeature < listCREFeature.Count; idxCreFeature++)
        {
            // on cherche la feature du CREOrder
            List<string> listTagFeature = CommonMethod.GetAttributeValues(listCREFeature[idxCreFeature], "CREFeature/Feature", "sTAGFeature");
            if (listTagFeature.Count > 0 && listTagFeature[0] == sTAGFeature)
            {
                // on cherche la ligne correspondante au CREOrder
                List<string> listCre = CommonMethod.GetTags(listCREFeature[idxCreFeature], "CREFeature/CRE");
                for(int idxCre = 0; idxCre < listCre.Count; idxCre++)
                {
                    List<string> listOperationTAG = CommonMethod.GetAttributeValues(listCre[idxCre], "CRE/Infos", "sOperationTAG");
                    if (listOperationTAG.Count > 0 && listOperationTAG[0] == sOperationTAG)
                    {
                        string sMontantMin;
                        string sMontantMax;
                        if (sMontant1.Length > 0 && Decimal.TryParse(sMontant1.Replace('.', ','), NumberStyles.Currency, new CultureInfo("fr-FR"), out dMontantTmp))
                        {
                            dMontantTmp = decimal.Round(dMontantTmp, 2, MidpointRounding.AwayFromZero);
                            CRETools.GetCREValidationMontant(listCre[idxCre], 1, out sMontantMin, out sMontantMax);
                            if (Decimal.TryParse(sMontantMin.Replace('.', ','), NumberStyles.Currency, new CultureInfo("fr-FR"), out dMontantMin)
                                && Decimal.TryParse(sMontantMax.Replace('.', ','), NumberStyles.Currency, new CultureInfo("fr-FR"), out dMontantMax))
                            {
                                dMontantMin = decimal.Round(dMontantMin, 2, MidpointRounding.AwayFromZero);
                                dMontantMax = decimal.Round(dMontantMax, 2, MidpointRounding.AwayFromZero);

                                if ((dMontantMin != -1 && dMontantTmp < dMontantMin)
                                    || (dMontantMax != -1 && dMontantTmp > dMontantMax))
                                {
                                    if (sLibelleError.Length == 0)
                                        sLibelleError += "Vous n&#146;avez pas le droit de valider cette opération<br/>";
                                    sLibelleError += "Vous n&#146;êtes autorisé(e) à valider ce type de CRE que pour les montants 1 ";
                                    if (dMontantMin != -1 && dMontantMax != -1)
                                    {
                                        sLibelleError += "compris entre ";
                                        sLibelleError += sMontantMin + "€ et " + sMontantMax + "€.<br/>";
                                    }
                                    else if (dMontantMin != -1)
                                    {
                                        sLibelleError += "supérieur à ";
                                        sLibelleError += sMontantMin + "€.<br/>";
                                    }
                                    else //dMontantMax != -1
                                    {
                                        sLibelleError += "inférieur à ";
                                        sLibelleError += sMontantMax + "€.<br/>";
                                    }
                                }
                            }
                        }

                        if (sMontant2.Length > 0 && Decimal.TryParse(sMontant2.Replace('.', ','), NumberStyles.Currency, new CultureInfo("fr-FR"), out dMontantTmp))
                        {
                            dMontantTmp = decimal.Round(dMontantTmp, 2, MidpointRounding.AwayFromZero);
                            CRETools.GetCREValidationMontant(listCre[idxCre], 2, out sMontantMin, out sMontantMax);
                            if (Decimal.TryParse(sMontantMin.Replace('.', ','), NumberStyles.Currency, new CultureInfo("fr-FR"), out dMontantMin)
                                && Decimal.TryParse(sMontantMax.Replace('.', ','), NumberStyles.Currency, new CultureInfo("fr-FR"), out dMontantMax))
                            {
                                dMontantMin = decimal.Round(dMontantMin, 2, MidpointRounding.AwayFromZero);
                                dMontantMax = decimal.Round(dMontantMax, 2, MidpointRounding.AwayFromZero);

                                if ((dMontantMin != -1 && dMontantTmp < dMontantMin)
                                    || (dMontantMax != -1 && dMontantTmp > dMontantMax))
                                {
                                    if (sLibelleError.Length == 0)
                                        sLibelleError += "Vous n&#146;avez pas le droit de valider cette opération<br/>";
                                    sLibelleError += "Vous n&#146;êtes autorisé(e) à valider ce type de CRE que pour les montants 2 ";
                                    if (dMontantMin != -1 && dMontantMax != -1)
                                    {
                                        sLibelleError += "compris entre ";
                                        sLibelleError += sMontantMin + "€ et " + sMontantMax + "€.<br/>";
                                    }
                                    else if (dMontantMin != -1)
                                    {
                                        sLibelleError += "supérieur à ";
                                        sLibelleError += sMontantMin + "€.<br/>";
                                    }
                                    else //dMontantMax != -1
                                    {
                                        sLibelleError += "inférieur à ";
                                        sLibelleError += sMontantMax + "€.<br/>";
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return sLibelleError;
    }

    protected string getOperationStatus(string sOpeStatus)
    {
        string sRetour = "";
        switch (sOpeStatus)
        {
            case "1":
                sRetour = "cre demandé sans validation";
                break;
            case "2":
                sRetour = "cre demandé en attente de validation";
                break;
            case "3":
                sRetour = "cre annulé";
                break;
            case "4":
                sRetour = "cre rejeté";
                break;
            case "5":
                sRetour = "cre envoyé";
                break;
        }
        return sRetour;
    }

    protected string GetOpeationDetailsToShow(string sDivers
                                            , string sLibelle1
                                            , string sCREOrderUserName
                                            , string sLabelFeature
                                            , string sDescriptionFeature
                                            , string sUserComment
                                            , string sCREToValidateRefUserLabelGroup
                                            , string sCREToValidateUserName
                                            , string sCREValidateUserName
                                            , string dtCREValidationUTCDateTime
                                            , string sStatusOperation
                                            , string sCRECr
                                            , string sRefCRE
                                            , string sCREErrorMessage
                                            )
    {
        string sRetour = "";


        sRetour = "<div class=\"cobalt-ope-details\">";
        if (!string.IsNullOrWhiteSpace(sDivers))
            sRetour += "<div><div>Divers</div><div>" + sDivers + "</div></div>";

        if (!string.IsNullOrWhiteSpace(sLibelle1))
            sRetour += "<div><div>Libelle</div><div>" + sLibelle1 + "</div></div>";

        if (!string.IsNullOrWhiteSpace(sCREOrderUserName))
            sRetour += "<div><div>Demandé par</div><div>" + sCREOrderUserName + "</div></div>";

        if (!string.IsNullOrWhiteSpace(sLabelFeature) || !string.IsNullOrWhiteSpace(sDescriptionFeature))
        {
            sRetour += "<div><div>Profile</div>";
            if (!string.IsNullOrWhiteSpace(sLabelFeature))
                sRetour += "<div>" + sLabelFeature + "</div>";
            if (!string.IsNullOrWhiteSpace(sDescriptionFeature))
                sRetour += "<div>" + sDescriptionFeature + "</div>";
            sRetour += "</div>";
        }

        if (!string.IsNullOrWhiteSpace(sUserComment))
            sRetour += "<div><div>Commentaire Utilisateur</div><div>" + sUserComment + "</div></div>";

        if (!string.IsNullOrWhiteSpace(sCREToValidateRefUserLabelGroup))
        {
            int iTailleChaine = sCREToValidateRefUserLabelGroup.Trim().Length;
            int iTailleALire = (iTailleChaine > 50) ? 50 : iTailleChaine;
            int iTailleDejaLu = 0;
            sRetour += "<div><div>A faire valider par</div><div>" + sCREToValidateRefUserLabelGroup.Trim().Substring(iTailleDejaLu, iTailleALire);
            iTailleDejaLu = iTailleALire;
            while (iTailleDejaLu < iTailleChaine)
            {
                iTailleALire = ((iTailleChaine - iTailleDejaLu) > 50) ? 50 : (iTailleChaine - iTailleDejaLu);
                sRetour += "<div>" + sCREToValidateRefUserLabelGroup.Trim().Substring(iTailleDejaLu, iTailleALire) + "</div>";
                iTailleDejaLu += iTailleALire;
            }
            sRetour += "</div>";
        }
        else if (!string.IsNullOrWhiteSpace(sCREToValidateUserName))
            sRetour += "<div><div>A faire valider par</div><div>" + sCREToValidateUserName + "</div></div>";

        if (!string.IsNullOrWhiteSpace(sCREValidateUserName))
        {
            sRetour += "<div><div>";
            switch (sStatusOperation)
            {
                case "3":
                    sRetour += "Annulé";
                    break;
                case "4":
                    sRetour += "Rejeté";
                    break;
                default:
                    sRetour += "Validé";
                    break;
            }

            sRetour += " par</div><div>" + sCREValidateUserName + "</div></div>";
        }

        if (!string.IsNullOrWhiteSpace(dtCREValidationUTCDateTime))
        {
            sRetour += "<div><div>Date ";
            switch (sStatusOperation)
            {
                case "3":
                    sRetour += "annulation";
                    break;
                case "4":
                    sRetour += "rejet";
                    break;
                default:
                    sRetour += "validation";
                    break;
            }
            sRetour += "</div><div>" + dtCREValidationUTCDateTime + "</div></div>";
        }

        if (!string.IsNullOrWhiteSpace(sRefCRE))
        {
            sRetour += "<div width=\"100%\"><div>Données CRE</div>";
            sRetour += "<div>Code exécution : " + sCRECr + "</div>";
            sRetour += "<div>Référence : " + sRefCRE + "</div>";
            if (!string.IsNullOrWhiteSpace(sCREErrorMessage))
            {
                int iTailleChaine = sCREErrorMessage.Trim().Length;
                int iTailleALire = (iTailleChaine > 50) ? 50 : iTailleChaine;
                int iTailleDejaLu = 0;
                sRetour += "<div >Message : " + sCREErrorMessage.Trim().Substring(iTailleDejaLu, iTailleALire);
                iTailleDejaLu = iTailleALire;
                while (iTailleDejaLu < iTailleChaine)
                {
                    iTailleALire = ((iTailleChaine - iTailleDejaLu) > 50) ? 50 : (iTailleChaine - iTailleDejaLu);
                    sRetour += "<div>" + sCREErrorMessage.Trim().Substring(iTailleDejaLu, iTailleALire) + "</div>";
                    iTailleDejaLu += iTailleALire;
                }

                sRetour += "</div>";
            }
            sRetour += "</div>";
        }

        sRetour += "</div>";
        return sRetour;
    }

    protected string getFormattedAmount(string sAmount)
    {
        sAmount = getFormattedAmountNoStyle(sAmount);

        // Add Euro symbol
        if (sAmount.Length > 0)
            sAmount += " &euro;";

        // Add sign and corresponding colors


        return sAmount;
    }
    protected string getFormattedAmountNoStyle(string sAmount)
    {
        // Add space every 3 numbers
        try
        {
            //sAmount = sAmount.Replace("-", "");
            string[] arAmount = sAmount.Split(',');
            int iTmp = int.Parse(arAmount[0]);
            arAmount[0] = String.Format("{0:### ### ### ###}", iTmp).Trim();
            if (arAmount[0].Length == 0)
                arAmount[0] = "0";

            sAmount = (arAmount.Length > 1) ? arAmount[0] + "," + arAmount[1].Substring(0, 2).PadRight(2, '0') : arAmount[0] + ",00";
        }
        catch (Exception e)
        {
        }

        return sAmount;
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        UpdateSearchCriteria();
        GetOperationDiverseListe();
    }

    // traitement du rejet de l'opération
    protected void btnCancelOpe_Click(object sender, EventArgs e)
    {
        SendAction2BDD("Suppression");
    }

    // traitement de la validation de l'opération
    protected void btnValidOpe_Click(object sender, EventArgs e)
    {
        SendAction2BDD("Validation");
    }

    protected void SendAction2BDD(string sAction)
    {
        string sErrorMessage = "";
        bool bIssucceeded = false;
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRE"].ConnectionString);

            conn.Open();

            SqlCommand cmd = new SqlCommand("[CRE].[P_SetCREOrderV3]", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@sXmlIn", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@sXmlOut", SqlDbType.VarChar, -1);

            cmd.Parameters["@sXmlIn"].Value = new XElement("ALL_XML_IN", 
                new XElement("CreOrder"
                    , new XAttribute("TOKEN", authentication.GetCurrent().sToken)
                    , new XAttribute("TypeAction", sAction)
                    , new XAttribute("iRefCreOrder", hdnRefOperation.Value.ToString()))).ToString();
            cmd.Parameters["@sXmlOut"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@sXmlOut"].Value.ToString();
            if (sXmlOut.Length > 0)
            {
                List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Result", "RC");

                if (listRC.Count > 0 && listRC[0] != "0")
                {
                    List<string> listMessage = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Result", "ERROR_MSG");
                    List<string> listLog = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Result", "ERROR_LOG");
                    if (listMessage.Count > 0)
                    {
                        sErrorMessage = listMessage[0];
                    }
                    else
                    {
                        sErrorMessage = "erreur interne";
                    }

                    if (listLog.Count > 0)
                        sErrorMessage += "<br/>" + listLog[0];
                }
                else
                {
                    _SearchCriteria.iRefCREOrder = 0;
                    GetOperationDiverseListe();
                    sErrorMessage = sAction + " réussie";
                    bIssucceeded = true;
                }
            }
        }

        catch (Exception ex)
        {
            sErrorMessage = ex.Message;
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        AlertMessage(sAction + " de l'opération", sErrorMessage, bIssucceeded);
    }

    protected void AlertMessage(string sTitle, string sMessage, bool bIsOk = false)
    {
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessageOD(\"" + sTitle + "\", \"" + sMessage + "\", '" + bIsOk.ToString() + "');", true);
    }

    protected bool bGetFeatureList()
    {
        bool bResultat = false;
        SqlConnection conn = null;
        string sErrorMessage = "";
        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CRE"].ConnectionString);
            //conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("[CRE].[P_GetUserCREFeaturesAutorizationsV3]", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@sXmlIn", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@sXmlOut", SqlDbType.VarChar, -1);

            cmd.Parameters["@sXmlIn"].Value = new XElement("ALL_XML_IN",
                new XElement("UserCREFeaturesAutorizations"
                    , new XAttribute("iRefUser", authentication.GetCurrent().sRef)
                    , new XAttribute("TOKEN", authentication.GetCurrent().sToken))).ToString();
            cmd.Parameters["@sXmlOut"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            _sListAuthorizedCre = cmd.Parameters["@sXmlOut"].Value.ToString();

            if (_sListAuthorizedCre.Length > 0)
            {
                //_sListAuthorizedCre = "<ALL_XML_OUT><UserCREFeaturesAutorizations RC=\"0\"/><CREFeaturesOrder><CREFeature><Feature iRefFeature=\"6\" sTAGFeature=\"_TESTS_USR_RVN_USR_YJU_\" sLabelFeature=\"TESTS USR RVN USR YJU\" sDescriptionFeature=\"\"/><CRE sOperationTAG=\"_FRAIS_ATD_\" sCode...

                List<string> listRC = CommonMethod.GetAttributeValues(_sListAuthorizedCre, "ALL_XML_OUT/UserCREFeaturesAutorizations", "RC");
                if (listRC.Count > 0)
                {
                    if (listRC[0] != "0")
                    {
                        bResultat = false;
                        List<string> listMessage = CommonMethod.GetAttributeValues(_sListAuthorizedCre, "ALL_XML_OUT/UserCREFeaturesAutorizations", "ERROR_MSG");
                        if (listMessage.Count > 0)
                        {
                            sErrorMessage = listMessage[0];
                        }
                        else
                        {
                            sErrorMessage = "erreur interne : " + listRC[0];
                        }
                    }
                    else
                    {
                        bResultat = true;
                    }
                }
                else
                {
                    bResultat = false;
                }
            }
        }
        catch (Exception ex)
        {
            bResultat = false;
            sErrorMessage = "exception " + ex.Message;
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }
        return bResultat;
    }

    protected void initFeatureDdl()
    {
        ddlFeature.Items.Clear();
        _sCurrentFeature = "";

        if (bGetFeatureList() == true)
        {
            ddlFeature.Items.Add(new ListItem("Tous", ""));
            List<string> listCREFeature = CommonMethod.GetTags(_sListAuthorizedCre, "ALL_XML_OUT/CREFeaturesValidation/CREFeature");
            for (int idxCreFeature = 0; idxCreFeature < listCREFeature.Count; idxCreFeature++)
            {
                List<string> listFeature = CommonMethod.GetTags(listCREFeature[idxCreFeature], "CREFeature/Feature");
                for (int i = 0; i < listFeature.Count; i++)
                {
                    List<string> listRefFeature = CommonMethod.GetAttributeValues(listFeature[i], "Feature", "iRefFeature");
                    List<string> listLabelFeature = CommonMethod.GetAttributeValues(listFeature[i], "Feature", "sLabelFeature");
                    List<string> listTagFeature = CommonMethod.GetAttributeValues(listFeature[i], "Feature", "sTagFeature");
                    ListItem item = new ListItem(listLabelFeature[0], listRefFeature[0]);

                    ddlFeature.Items.Add(item);
                }
            }
            InitddlTypeCre();
        }
    }

    //protected void ddlFeature_OnChange(object sender, EventArgs e)
    protected void InitddlTypeCre()
    {
        //ListItem selectedItem = ddlFeature.SelectedItem;
        if (_sListAuthorizedCre != null)
        {
            ddlTypeCre.Items.Clear();
            ddlTypeCre.Items.Add(new ListItem("Tous", ""));
            DataTable dt = new DataTable();
            dt.Columns.Add("CodeOpe");
            dt.Columns.Add("OpeDescrition");
            dt.Columns.Add("TagOpe");

            List<string> listCREFeature = CommonMethod.GetTags(_sListAuthorizedCre, "ALL_XML_OUT/CREFeaturesValidation/CREFeature");
            for (int idxCreFeature = 0; idxCreFeature < listCREFeature.Count; idxCreFeature++)
            {
                List<string> listFeature = CommonMethod.GetTags(listCREFeature[idxCreFeature], "CREFeature/Feature");
                for (int idxFeature = 0; idxFeature < listFeature.Count; idxFeature++)
                {
                    List<string> listRefFeature = CommonMethod.GetAttributeValues(listFeature[idxFeature], "Feature", "iRefFeature");
                    List<string> listTagFeature = CommonMethod.GetAttributeValues(listFeature[idxFeature], "Feature", "sTAGFeature");

                    if (listRefFeature.Count > 0)
                    {
                        List<string> listCre = CommonMethod.GetTags(listCREFeature[idxCreFeature], "CREFeature/CRE");
                        for (int idxCre = 0; idxCre < listCre.Count; idxCre++)
                        {
                            List<string> sOperationTAG = CommonMethod.GetAttributeValues(listCre[idxCre], "CRE/Infos", "sOperationTAG");
                            List<string> sCodeOperation = CommonMethod.GetAttributeValues(listCre[idxCre], "CRE/Infos", "sCodeOperation");
                            List<string> sOperationDescription = CommonMethod.GetAttributeValues(listCre[idxCre], "CRE/Infos", "sOperationDescription");
                            List<string> bCREValidationRequested = CommonMethod.GetAttributeValues(listCre[idxCre], "CRE/Infos", "bCREValidationRequested");
                            
                            if (sOperationDescription.Count > 0 && sOperationTAG.Count > 0)
                            {
                                bool bTypeCrePresent = false;
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    if (dt.Rows[i]["TagOpe"].ToString() == sOperationTAG[0])
                                    {
                                        bTypeCrePresent = true;
                                        break;
                                    }
                                }
                                if (bTypeCrePresent == false)
                                {
                                    dt.Rows.Add(new Object[]
                                    {
                                        sCodeOperation[0]
                                        , sOperationDescription[0]
                                        , sOperationTAG[0]
                                });
                                }
                            }
                        }
                    }
                }
            }

            if (dt.Rows.Count > 0)
            {
                DataView dv = new DataView(dt);
                dv.Sort = "CodeOpe ASC";
                DataTable dtSorted = dv.ToTable();

                for (int idx = 0; idx < dt.Rows.Count; idx++)
                {
                    ListItem item = new ListItem(dtSorted.Rows[idx]["CodeOpe"].ToString() + " - " + dtSorted.Rows[idx]["OpeDescrition"].ToString(), dtSorted.Rows[idx]["TagOpe"].ToString());
                    ddlTypeCre.Items.Add(item);
                    _dCodeOperation.Add(dtSorted.Rows[idx]["TagOpe"].ToString(), dtSorted.Rows[idx]["CodeOpe"].ToString());
                }
            }
        }
    }

    protected string GetImageStatus(string sCREOrderStatus, string sCreCr)
    {
        string sImageURL = "";

        switch (sCREOrderStatus)
        {
            case "2":
                sImageURL = "yellow_status.png";
                break;
            case "5":
                if (sCreCr == "0")
                    sImageURL = "green_status.png";
                else
                    sImageURL = "red_status.png";
                break;
            case "3":
                sImageURL = "gray_status.png";
                break;
            case "4":
                sImageURL = "orange_status.png";
                break;
        }
        return String.Format("~/Styles/Img/{0}", sImageURL);
    }
}

