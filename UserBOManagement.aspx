﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="UserBOManagement.aspx.cs" Inherits="UserBOManagement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">

    <script src="Scripts/jquery.stickytableheaders.min.js" type="text/javascript"></script>

    <script type="text/javascript">

        $(document).ready(function () {
            $('#popup-user-info').dialog({
                autoOpen: false,
                resizable: false,
                draggable: false,
                modal: true,
                minWidth: 550,
                buttons:
                    [{
                        text: 'OK',
                        click: function () {
                            $("#<%=btnAddSetUser.ClientID %>").click();
                            //$(this).dialog('close');
                        }
                    },
                {
                    text: 'Annuler',
                    click: function () { $(this).dialog('close'); }
                }]
            });
            $('#popup-user-info').parent().appendTo(jQuery("form:first"));

            $('#popup-user-delete').dialog({
                autoOpen: false,
                resizable: false,
                draggable: false,
                modal: true,
                title: 'Supprimer l\'utilisateur...',
                minWidth: 550,
                buttons:
                    [{
                        text: 'OK',
                        click: function () {
                            $("#<%=btnDeleteUser.ClientID %>").click();
                            $(this).dialog('close');
                        }
                    },
                {
                    text: 'Annuler',
                    click: function () { $(this).dialog('close'); }
                }]
            });
            $('#popup-user-delete').parent().appendTo(jQuery("form:first"));

            $('#popup-user-ip').dialog({
                autoOpen: false,
                resizable: false,
                draggable: false,
                modal: true,
                title: 'Autorisation IP',
                minWidth: 300
            });
            $('#popup-user-ip').parent().appendTo(jQuery("form:first"));
        });

        function InitPage() {
            //var arColW = new Array();

            ////console.log($('.defaultTable:eq(1)'));
            //$('.defaultTable:eq(1) tr:eq(0) td').each(function (index) {
            //    //console.log(index + '-' + $(this).text());
            //    arColW[index] = $(this).width();
            //});

            ////console.log(arColW);
            //$('.defaultTable:eq(0) tr:eq(0) th').each(function (index) {
            //    //console.log($(this).text());
            //    if (index < 5)
            //        $(this).width(arColW[index]);
            //});
            //$('.defaultTable:eq(0)').outerWidth($('.defaultTable:eq(1)').outerWidth());

            $("#user-list").stickyTableHeaders({ scrollableArea: $(".scrollable-area")[0] });

            $('.tooltip').tooltip({
                position: {
                    my: "center bottom-10",
                    at: "center top",
                    using: function (position, feedback) {
                        $(this).css(position);
                        $("<div>")
                            .addClass("arrow")
                            .addClass(feedback.vertical)
                            .addClass(feedback.horizontal)
                            .appendTo(this);
                    }
                }
            });
        }

        function ShowPopup(sTitle) {
            $('#popup-user-info').dialog('option', 'title', sTitle);
            $('.RadioButtonList').buttonset();
            $('.RadioButtonList .ui-button').css('height', '24px');
            $('.RadioButtonList .ui-button .ui-button-text').css('padding-top', '0').css('padding-bottom', '0');
            $('#popup-user-info').dialog('open');
        }
        function HidePopup() {
            $('#popup-user-info').dialog('close');
        }
        function ShowDeletePopup() {
            $('#popup-user-delete').dialog('open');
        }
        function ShowIPPopup() {
            $('#popup-user-ip').dialog('open');
        }

        function IsAddSetFormValid() {
            var bOk = true;
            var sMessage = '';

            if ($('#<%=txtLastName.ClientID%>').val().trim().length == 0) {
                bOk = false;
                sMessage += 'Nom : champ requis<br/>';
            }
            if ($('#<%=txtFirstName.ClientID%>').val().trim().length == 0) {
                bOk = false;
                sMessage += 'Prénom : champ requis<br/>';
            }
            if ($('#<%=txtBirthdate.ClientID%>').length > 0 && $('#<%=txtBirthdate.ClientID%>').val().trim().length == 0) {
                bOk = false;
                sMessage += 'Date de naissance : champ requis<br/>';
            }
            if ($('#<%=txtEmail.ClientID%>').val().trim().length == 0) {
                bOk = false;
                sMessage += 'Email : champ requis<br/>';
            }
            else if (!IsEmailValid($('#<%=txtEmail.ClientID%>').val())) {
                bOk = false;
                sMessage += 'Email : format non valide<br/>';
            }
            if ($('#<%=ddlProfile.ClientID%>').val().trim().length == 0) {
                bOk = false;
                sMessage += 'Profil : champ requis<br/>';
            }

            if (!bOk)
                AlertMessage('Erreur', sMessage, null, null);

            return bOk;
        }

        function IsEmailValid(email) {
            var email_regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return email_regex.test(email);
        }

        function CheckNewIP() {
            var val = $('#<%=txtNewIP.ClientID%>').val();
            if (val.length > 0 && IsIPValid(val))
                return true;
            else {
                var message = '';
                if (val.length == 0)
                    message = 'champ requis';
                else message = 'format non valide';
                AlertMessage('Erreur', 'Adresse IP : ' + message, null, null);
            }
        }
        function IsIPValid(ip) {
            var ip_regex = /^(([1-9]?[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([1-9]?[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/;
            return ip_regex.test(ip);
        }

    </script>

    <style type="text/css">
        #ar-loading {
            background:url('Styles/img/loading2.gif') no-repeat center;
            background-size: 35px 27px;
            display:none;
            position:absolute;
            background-color:rgba(255, 255, 255, 0.5);
            z-index:10000;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    
    <div id="ar-loading"></div>

    <h2 style="color: #344b56;margin-bottom:10px;text-transform:uppercase">Gestion des utilisateurs</h2>

    <div style="border: 3px solid #344b56; border-radius: 8px; width: 100%; margin: auto;margin-bottom:20px;">
        <div style="margin:20px">

            <div id="popup-user-delete" style="display:none">
                <asp:UpdatePanel ID="upPopupDelete" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:HiddenField ID="hdnRefUIDToDelete" runat="server" />
                        Voulez-vous vraiment supprimer l'utilisateur backoffice <asp:Label ID="lblLastNameToDelete" runat="server"></asp:Label> <asp:Label ID="lblFirstNameToDelete" runat="server"></asp:Label> ?
                        <div style="display:none">
                            <asp:Button ID="btnDeleteUser" runat="server" OnClick="btnDeleteUser_Click" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>

            <div id="popup-user-info" style="display:none">
                <asp:UpdatePanel ID="upPopupAddSet" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Panel ID="panelAddSet" runat="server" DefaultButton="btnAddSetUser">
                            <asp:HiddenField ID="hdnRefUID" runat="server" />
                            <div style="float:left;width:49%">
                                <div class="label">
                                    <asp:Label ID="lblLastName" runat="server" AssociatedControlID="txtLastName">Nom</asp:Label>
                                </div>
                                <div>
                                    <asp:TextBox ID="txtLastName" runat="server" MaxLength="100" style="width:100%"></asp:TextBox>
                                </div>
                            </div>
                            <div style="float:right;width:49%">
                                <div class="label">
                                    <asp:Label ID="lblFirstName" runat="server" AssociatedControlID="txtFirstName">Prénom</asp:Label>
                                </div>
                                <div>
                                    <asp:TextBox ID="txtFirstName" runat="server" MaxLength="100" style="width:100%"></asp:TextBox>
                                </div>
                            </div>
                            <div style="clear:both"></div>
                            <asp:Panel ID="panelBirthdate" runat="server" style="width:49%">
                                <div class="label">
                                    <asp:Label ID="lblBirthdate" runat="server" AssociatedControlID="txtBirthdate">Date de naissance</asp:Label>
                                </div>
                                <div>
                                    <asp:TextBox ID="txtBirthdate" runat="server" MaxLength="10" style="width:100%"></asp:TextBox>
                                </div>
                            </asp:Panel>
                            <div>
                                <div class="label">
                                    <asp:Label ID="lblEmail" runat="server" AssociatedControlID="txtEmail">Email</asp:Label>
                                </div>
                                <div>
                                    <asp:TextBox ID="txtEmail" runat="server" MaxLength="100" style="width:100%"></asp:TextBox>
                                </div>
                            </div>
                            <div style="float:left;width:49%">
                                <div class="label">
                                    <asp:Label ID="lblProfile" runat="server" AssociatedControlID="ddlProfile">Profil</asp:Label>
                                </div>
                                <div>
                                    <asp:DropDownList ID="ddlProfile" runat="server" DataTextField="Description" DataValueField="TAGProfile" AppendDataBoundItems="true" style="width:100%">
                                        <asp:ListItem Text="" Value=""></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <asp:Panel ID="panelDisabled" runat="server" style="float:right;width:49%">
                                <div class="label">
                                    <asp:Label ID="lblStatus" runat="server" AssociatedControlID="rblStatus">Statut</asp:Label>
                                </div>
                                <div style="position:relative;top:-2px">
                                    <asp:RadioButtonList ID="rblStatus" runat="server" AutoPostBack="false" RepeatDirection="Horizontal" CssClass="RadioButtonList" style="position: relative; left: 0px; text-transform: uppercase">
                                        <asp:ListItem Value="0"><span style="color:green;text-shadow: -1px -1px 0px #FFF, 1px 1px 0px #FFF, -1px 1px 0px #FFF, 1px -1px 0px #FFF;">Actif</span></asp:ListItem>
                                        <asp:ListItem Value="1"><span style="color:red;text-shadow: -1px -1px 0px #FFF, 1px 1px 0px #FFF, -1px 1px 0px #FFF, 1px -1px 0px #FFF;">Bloqué</span></asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </asp:Panel>
                            <div style="clear:both"></div>
                            <div style="display:none">
                                <asp:Button ID="btnAddSetUser" runat="server" OnClientClick="return IsAddSetFormValid();" OnClick="btnAddSetUser_Click" />
                            </div>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>

            <div id="popup-user-ip" style="display:none">
                <asp:UpdatePanel ID="upIP" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>    
                        <asp:HiddenField ID="hdnRefUIDIP" runat="server" />
                        <div>
                            <asp:Panel ID="panelIPList" runat="server" DefaultButton="btnAddNewIP">
                                <table class="defaultTable" style="width:100%;border-collapse:collapse">
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtNewIP" runat="server" style="width:100%" MaxLength="15"></asp:TextBox>
                                        </td>
                                        <td style="width:30px;text-align:center">
                                            <asp:ImageButton ID="btnAddNewIP" runat="server" ImageUrl="~/Styles/Img/plus.png" Width="18px" OnClientClick="return CheckNewIP()" OnClick="btnAddNewIP_Click" CssClass="tooltip" ToolTip="Ajouter" style="position:relative;top:5px" />
                                            <%--<asp:Button ID="btnAddNewIP2" runat="server" Text="Ajouter" CssClass="button" />--%>
                                        </td>
                                    </tr>
                                    <asp:Repeater ID="rptIP" runat="server" OnItemDataBound="rptIP_ItemDataBound">
                                        <ItemTemplate>
                                                <tr>
                                                    <td><asp:Label ID="lblIPAddress" runat="server" Text='<%#Eval("IPAddress") %>'></asp:Label></td>
                                                        <td style="width:20px;text-align:center;border-left:1px dashed #344b56">
                                                        <asp:ImageButton ID="btnRemoveIP" runat="server" CommandArgument='<%#Eval("IPAddress") %>' OnClick="btnRemoveIP_Click" ImageUrl="~/Styles/Img/delete.png" Height="15px" CssClass="tooltip" ToolTip="Supprimer" />
                                                    </td>
                                                </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </table>
                            </asp:Panel>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnAddNewIP" EventName="click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>

            <asp:Panel ID="panelList" runat="server">
                <asp:UpdatePanel ID="upFilter" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>

                        <asp:Panel ID="panelFilter" runat="server" CssClass="table" DefaultButton="btnFilter" style="width:100%;border-collapse:collapse">
                            <div class="row">
                                <div class="cell">
                                    <asp:TextBox ID="txtFilter" runat="server" Width="98%" placeholder="Nom, Prénom, Email, Profil, Statut"></asp:TextBox>
                                </div>
                                <div class="cell" style="width:1px">
                                    <asp:Button ID="btnFilter" runat="server" Text="Filtrer" CssClass="button" OnClick="btnFilter_Click" />
                                </div>
                            </div>
                        </asp:Panel>

                        <div style="background-color:#344b56">
                            <table class="defaultTable" style="width:100%;border-collapse:collapse">
                                <tr>
                                    
                                </tr>
                            </table>
                        </div>
                        <div class="scrollable-area" style="max-height:293px;overflow:hidden;overflow-y:auto">
                            <asp:Repeater ID="rptUser" runat="server" OnItemDataBound="rptUser_ItemDataBound">
                                <HeaderTemplate>
                                    <table id="user-list" class="defaultTable" style="width:100%;border-collapse:collapse">
                                        <thead>
                                            <th>Nom</th>
                                            <th>Prénom</th>
                                            <th>Email</th>
                                            <th>Profil</th>
                                            <th>Statut</th>
                                            <th>&nbsp;</th>
                                            <th>&nbsp;</th>
                                            <th>&nbsp;</th>
                                        </thead>
                                        <tbody>
                                </HeaderTemplate>
                                <ItemTemplate>
                                            <tr>
                                                <td><asp:Label ID="lblLastName" runat="server" Text='<%#Eval("LastName") %>'></asp:Label></td>
                                                <td><asp:Label ID="lblFirstName" runat="server" Text='<%#Eval("FirstName") %>'></asp:Label></td>
                                                <td><a href='mailto:<%#Eval("Email") %>' target="_bank"><asp:Label ID="lblEmail" runat="server" Text='<%#Eval("Email") %>'></asp:Label></a></td>
                                                <td>
                                                    <%#Eval("Profile") %>
                                                    <asp:HiddenField ID="hdnTAGProfile" runat="server" Value='<%#Eval("TAGProfile") %>' />
                                                </td>
                                                <td style="text-align:center">
                                                    <asp:HiddenField ID="hdnDisabled" runat="server" Value='<%#Eval("Disabled") %>' />
                                                    <asp:Label ID="lblDisabled" runat="server" Text='<%#Eval("LabelDisabled") %>'></asp:Label>
                                                </td>
                                                <td style="width:20px;text-align:center;border-left:1px dashed #344b56">
                                                    <asp:HiddenField ID="hdnIsUpdateAllowed" runat="server" Value='<%#Eval("IsUpdateAllowed") %>' />
                                                    <asp:ImageButton ID="btnEdit" runat="server" CommandArgument='<%#Eval("RefUID") %>' OnClick="btnEdit_Click" ImageUrl="~/Styles/Img/modify.png" Height="15px" CssClass="tooltip" ToolTip="Modifier" />
                                                </td>
                                                <td style="width:20px;text-align:center">
                                                    <asp:HiddenField ID="hdnIP" runat="server" Value='<%#Eval("IPAddress") %>' />
                                                    <asp:ImageButton ID="btnIP" runat="server" CommandArgument='<%#Eval("RefUID") %>' OnClick="btnIP_Click" ImageUrl="~/Styles/Img/IP.png" Height="15px" CssClass="tooltip" ToolTip="Autorisation IP" />
                                                </td>
                                                <td style="width:20px;text-align:center">
                                                    <asp:HiddenField ID="hdnIsDelAllowed" runat="server" Value='<%#Eval("IsDelAllowed") %>' />
                                                    <asp:ImageButton ID="btnRemove" runat="server" CommandArgument='<%#Eval("RefUID") %>' OnClick="btnRemove_Click" ImageUrl="~/Styles/Img/delete.png" Height="15px" CssClass="tooltip" ToolTip="Supprimer" />
                                                </td>
                                            </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                        </tbody>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>

                        <asp:Panel ID="panelAddUser" runat="server" style="text-align:center;margin:30px 0 15px 0">
                            <asp:Button ID="btnAddUser" runat="server" CssClass="button" Text="Créer un utilisateur" OnClick="btnAddUser_Click" />
                        </asp:Panel>

                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnFilter" EventName="click" />
                        <asp:AsyncPostBackTrigger ControlID="btnAddSetUser" EventName="click" />
                    </Triggers>
                </asp:UpdatePanel>
            </asp:Panel>

        </div>
    </div>

    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        var sRequestControlID = "";
        function BeginRequestHandler(sender, args) {
            pbControl = args.get_postBackElement();
            //console.log(pbControl.id);
            var panel = '';
            if (pbControl.id == '<%=btnAddUser.ClientID %>' || pbControl.id == '<%=btnFilter.ClientID %>' || pbControl.id.indexOf('rptUser_btnRemove') != -1 || pbControl.id.indexOf('rptUser_btnEdit') != -1 || pbControl.id.indexOf('rptUser_btnIP') != -1)
                panel = '#<%=panelList.ClientID %>';
            else if (pbControl.id == '<%=btnAddSetUser.ClientID %>' || pbControl.id == '<%=btnDeleteUser.ClientID %>' || pbControl.id == '<%=btnAddNewIP.ClientID %>' || pbControl.id.indexOf('rptIP_btnRemoveIP') != -1)
                panel = $(window);

            if (panel.length > 0) {
                var width = $(panel).width();
                var height = $(panel).height();
                $('#ar-loading').css('width', width);
                $('#ar-loading').css('height', height);

                $('#ar-loading').show();

                $('#ar-loading').position({ my: "left top", at: "left top", of: panel });
            }

            sRequestControlID = pbControl.id;
        }
        function EndRequestHandler(sender, args) {
            $('#ar-loading').hide();
        }
    </script>

</asp:Content>