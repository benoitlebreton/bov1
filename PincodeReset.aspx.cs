﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using XMLMethodLibrary;

public partial class PincodeReset : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            CheckRights();
            ddlAgencyID.DataSource = tools.GetAgencyList();
            ddlAgencyID.DataBind();
        }
    }

    protected void CheckRights()
    {
        string sXmlOut = tools.GetRights(authentication.GetCurrent().sToken, "SC_SellerPincodeReset");

        List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Page", "RC");

        if (listRC.Count == 0 || listRC[0] != "0")
        {
            Response.Redirect("Default.aspx", true);
        }
    }

    protected void clickResetPincode(object sender, EventArgs e)
    {
        if (ViewState["RefUser"] != null && ViewState["RefUser"].ToString().Length > 0)
        {
            int refUser;
            if (int.TryParse(ViewState["RefUser"].ToString(), out refUser))
            {
                ResetPincode(refUser, txtCustomPincode.Text);
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "PincodeChanged();", true);
            }
        }
    }

    protected void btnResetSellerPinCodeClick(object sender, EventArgs e)
    {
        ViewState["RefUser"] = ddlSeller.SelectedValue;
        lblAlertMessage.Text = "Attention, la réinitialisation du code PIN est irréversible.<br />Etes-vous sûr de vouloir effectuer cette action ?";
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "ConfirmResetPincode();", true);
    }

    protected void onAgencyIdChanged(object sender, EventArgs e)
    {
        hfAgencyIdSelected.Value = ddlAgencyID.SelectedValue;

        ddlSeller.Items.Clear();
        ddlSeller.Items.Add("");
        ddlSeller.DataSource = tools.GetSellerList(ddlAgencyID.SelectedValue,"","","");
        ddlSeller.DataBind();
        ddlSeller.Items[0].Selected = true;

        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "init(); setTimeout(function () { $('#" + ddlSeller.ClientID + "').next().find('input').focus(); }, 500);", true);
    }

    protected void ResetPincode(int refUser, string sPincode)
    {
        if (PincodeSecured(sPincode) || sPincode.Length == 0)
        {

            SqlConnection conn = null;
            authentication auth = authentication.GetCurrent();

            try
            {
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Partner"].ConnectionString);
                conn.Open();

                SqlCommand cmd = new SqlCommand("BO.P_RESET_USER_PINCODE", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@ALL_XML_IN", SqlDbType.VarChar, -1);
                cmd.Parameters.Add("@ALL_XML_OUT", SqlDbType.VarChar, -1);

                cmd.Parameters["@ALL_XML_IN"].Value = new XElement("ALL_XML_IN",
                                                new XElement("USER",
                                                    new XAttribute("REFUSER", refUser.ToString()),
                                                    new XAttribute("NEWPINCODE", sPincode),
                                                    new XAttribute("TOKEN", auth.sToken))).ToString();

                cmd.Parameters["@ALL_XML_OUT"].Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();

                if (cmd.Parameters["@ALL_XML_OUT"].Value.ToString().Length > 0)
                {
                    List<string> listXmlOut = XMLMethodLibrary.CommonMethod.GetAttributeValues(cmd.Parameters["@ALL_XML_OUT"].Value.ToString(), "ALL_XML_OUT/USER", "NEWPINCODE");

                    if (listXmlOut.Count > 0 && listXmlOut[0].Length == 4) // Succes
                    {
                        lblResetResult.Text = "Réinitialisation réussie<br />Nouveau code PIN :<br /><span style='font-weight:bold;font-size:1.4em'>" + listXmlOut[0] + "</span>";
                        lblResetResult.ForeColor = System.Drawing.Color.Green;
                    }
                    else
                    {
                        lblResetResult.Text = "Echec réinitialisation";
                        lblResetResult.ForeColor = System.Drawing.Color.Red;
                    }
                }
                else
                {
                    lblResetResult.Text = "Echec réinitialisation";
                    lblResetResult.ForeColor = System.Drawing.Color.Red;
                }
            }
            catch (Exception ex)
            {
                lblResetResult.Text = ex.Message;
                lblResetResult.ForeColor = System.Drawing.Color.Red;
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }
        else
        {
            lblResetResult.Text = "Echec réinitialisation<br /><span style='font-size:14px'>Le code PIN doit contenir 4 chiffres et ne doit pas être 0000 ou 1234.</span>";
            lblResetResult.ForeColor = System.Drawing.Color.Red;
        }
    }

    protected bool PincodeSecured(string pincode)
    {
        bool isSecured = false;
        int iPincode;

        if (pincode.Length == 4 && int.TryParse(pincode, out iPincode) && pincode.Trim() != "0000" && pincode.Trim() != "1234")
            isSecured = true;

        return isSecured;
    }
}