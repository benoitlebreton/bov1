﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="BoCobalt_Monitoring.aspx.cs" Inherits="BoCobalt_Monitoring" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <div style="position: relative;">
        <asp:Button ID="btnPrevious" runat="server" Text="Menu précédent" PostBackUrl="~/BoCobalt.aspx" CssClass="button"/>
    </div>
    <h2 style="color:#344b56; margin-bottom:10px; text-transform:uppercase">
        Monitoring
    </h2>
    <div style="padding:20px 0;">
        <div style="margin:auto;border:4px solid #f57527; border-radius:8px; display:table; width:80%">
            <%--<asp:TreeView runat="server" ID="tvOperationType" ImageSet="Custom" NodeStyle-CssClass="classMenu"></asp:TreeView>--%>

            <div id="iFrameDiv" style="width: 900px; height: 400px; margin: auto; background: url(Styles/Img/loading.gif) 50% 50% no-repeat; border: 0; z-index: 100">
                <iframe id="iFrameMonitoring" width="898px" height="394px" style="z-index: 100; border: 0" src="http://172.16.60.4:8080/dashboard/db/metier-on-us?refresh=1m&orgId=1">Votre navigateur ne supporte pas les iFrames.</iframe>
            </div>

        </div>
    </div>
    
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" Runat="Server">
</asp:Content>

