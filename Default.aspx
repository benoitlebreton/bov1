﻿<%@ Page Title="Page d'accueil - BO Compte-Nickel" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2 style="color:#344b56; margin-bottom:10px; text-transform:uppercase">
        Accueil
    </h2>
    <div style="padding:20px 0;">
        <div style="margin:auto;border:4px solid #f57527; border-radius:8px; display:table; width:80%">
            <%--<asp:TreeView runat="server" ID="tvOperationType" ImageSet="Custom" NodeStyle-CssClass="classMenu"></asp:TreeView>--%>

            <asp:Repeater ID="rptMenu" runat="server">
                <ItemTemplate>
                    <div style="display:table-row">
                        <div style="display:table-cell;height:40px; vertical-align:middle; text-align:center;" class="classMenu" onclick='document.location.href="<%#Eval("Value") %>"'>
                            <%#Eval("Label") %>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>

        </div>
    </div>
</asp:Content>
