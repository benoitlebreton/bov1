﻿<%@ Page Language="C#" Title="Etat de suivi des alertes" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="TrackingAlerts.aspx.cs" Inherits="TrackingAlerts" %>
<%@ Register TagName="AmlComment" TagPrefix="asp" Src="~/API/AmlComment.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link href="~/Styles/Site.css?v=20181306" rel="stylesheet" type="text/css" />
    <link href="Styles/jquery.stepProgressBarLight.css" rel="Stylesheet" type="text/css"/>
    <script type="text/javascript">
        $(document).ready(function () {
            

        });
        
        function ShowChangeAttributionDialog(refAlert, title) {

            //console.log(refAlert);
            //console.log(customerName);
            $('#<%=hdnRefAlert.ClientID%>').val(refAlert);
            $('#change-alert-attribution-dialog').dialog({
                autoOpen: true,
                modal: true,
                draggable: false,
                resizable: false,
                title: title,
                width: 'auto' ,
                buttons: {
                    'Annuler': function(){$(this).dialog('close').dialog('destroy');},
                    'Ok': function(){$('#<%=btnChangeAlertAttribution.ClientID%>').click();$(this).dialog('close').dialog('destroy');}
                }
            });
            $("#change-alert-attribution-dialog").parent().appendTo(jQuery("form:first"));
        }

        function initProgressBarList()
        {
             $('.AlertProgressBar').each(function (index, element) {
                initProgressBar($(element).attr('id'));
            });
        }

        
        function initProgressBar(idProgressBar) {
            var stepsValues = []; //{ value: 0, topLabel: 'J', bottomLabel: '' }
            var stepValues = $('#' + idProgressBar).attr('stepsValues').split('|');
            var currentStep = $('#' + idProgressBar).attr('current');
            for (var i = 0; i < stepValues.length; i++) {
                var topLabel = 'J';
                if (stepValues[i].split(';')[0] != 0)
                {
                    topLabel += ((stepValues[i].split(';')[0] > 0) ? "+" : "") + stepValues[i].split(';')[0];
                }
                stepsValues.push(
                    {
                        value: parseInt(stepValues[i].split(';')[0]),
                        topLabel: topLabel,
                        bottomLabel: stepValues[i].split(';')[2]
                    });
            }
            
            var currentStepLabel = currentStep;
            if (currentStep > 0)
                currentStepLabel = '+' + currentStep;
            else currentStepLabel = '';
            stepsValues.push({ value: parseInt(currentStep), topLabel: 'J' + currentStepLabel, bottomLabel: '' });
            stepsValues.push({ value: 30, topLabel: 'J+30', bottomLabel: '' });

            $('#' + idProgressBar).stepProgressBar({
                currentValue: currentStep,
                steps: stepsValues,
                unit: 'j'
            });
        }

          
    </script>

    <style type="text/css">
       
        .div-nbre-Alert {
            display: inline-block;
            border: 2px solid #fff;
            font-size: 10px;
            background-color: red;
            color: #fff;
            border-radius: 20px;
            width: 15px;
            height: 15px;
            line-height: 15px;
            text-align: center;
         
        }


        #change-alert-attribution-dialog {
            text-align: center;
        }
        #change-alert-attribution-dialog .ui-button-icon-only .ui-button-text, .ui-button-icons-only .ui-button-text {
            padding:0;
        }

        #ar-loading {
            background: url('Styles/img/loading2.gif') no-repeat center;
            background-size: 35px 27px;
            display: none;
            position: absolute;
            background-color: rgba(255, 255, 255, 0.5);
            z-index: 100000;
        }

        .template {
           height: 120px;
           overflow-y: scroll;
        }

       .button{
           background: url(/Styles/Img/affect.png) no-repeat;
                cursor:pointer;
                border: none;
                width:32px;
                height:24px;
            }
        .buttonReaffect{
           background: url(/Styles/Img/reaffect.png) no-repeat;
                cursor:pointer;
                border: none;
                width:32px;
                height:24px;
            }
    </style>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <h2 style="color: #344b56;text-transform:uppercase;">Suivi de toutes les alertes <asp:Label ID="lblAgent" runat="server"></asp:Label></h2>
    
    <div>
        <div id="alert-progress-tab">
            <div id="div-alert-progress">
                <asp:UpdatePanel ID="upAlertProgress" runat="server" UpdateMode="Conditional">
                   <ContentTemplate>
                        <asp:DataList ID="rptAlertProgressList" runat="server"  RepeatColumns="3"   
                                CellSpacing="3" RepeatLayout="Table" Width="100%">  
                                <ItemTemplate>
                                    <div id="divAgent">
                                    <asp:Panel runat="server" BorderStyle="Solid" BorderWidth="1px" BorderColor="#344b56">
                                    <table class="defaultTable"  width="100%">  
                                        <tr >  
                                            <td>  
                                                <b class="dd-alert-user"><%# Eval("BOUserName")%></b>  
                                            </td>  
                                            <td style="text-align:end">  
                                                <label style="font-size:10px">J+<%# daysCriticalAlert%></label> <b class="div-nbre-Alert"><%# Eval("NumberCriticalAlert") %> </b>
                                                <label style="font-size:10px">Total</label> <b class="div-nbre-Alert"><%# Eval("NumberAlert") %> </b>
                                            </td>  
                                        </tr>  
                                        <tr style="height:120px">  
                                            <td colspan="2" >
                                                <asp:Repeater ID="rptAlertProgressDetails" runat="server" DataSource='<%#Eval("CriticalAlertList") %>' OnItemDataBound="RepeaterAlertList_ItemDataBound" >
                                                    <HeaderTemplate>
                                                        
                                                      <div class="template">
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <div id="divParent"" style="text-align:center;" >
                                                            <div id="divAlertDrag" style="display:inline-block;width:75%;height:20px;" >
                                                                <asp:Panel ID="AlertProgressBar"  runat="server" ToolTip='<%# Eval("Title") %>' current='<%# Eval("CurrentStep") %>' stepsValues='<%# Eval("StepMarkers") %>' class='<%# ((int)Eval("CurrentStep") < 15) ? "AlertProgressBar" : "AlertProgressBar step-progressbar-bar-red" %>'></asp:Panel>
                                                            </div>
                                                            <div id="divAlertDrag" style="display:inline-block;height:20px;width:20px;vertical-align:central" >
                                                                <input type="button" title='<%# ((bool)Eval("IsReassigned")) ? "Alerte réattribuée le " + Eval("ReassignedDate","{0:d}") : "Réattribuer l&apos;alerte" %>'  class='<%# ((bool)Eval("IsReassigned")) ? "buttonReaffect" : "button" %>' value="" onclick='<%#String.Format("ShowChangeAttributionDialog({0}, &apos;{1}&apos;);", Eval("RefAlert"), Eval("Title")) %>'>                                                                                                                        
                                                            </div>
                                                        </div>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:Panel ID="panelAlertEmpty" runat="server" Visible="false" HorizontalAlign="Center" style="padding:10px">
                                                            Aucune alerte à J+<%# daysCriticalAlert.ToString()%> assignée
                                                        </asp:Panel>
                                                      </div>
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                            </td>  
                                        </tr>  
                                       
                                    </table>  
                                    </asp:Panel>
                                    </div>
                                </ItemTemplate>  
                            </asp:DataList>  
                       
                        <asp:Panel ID="panelEmpty" runat="server" Visible="false" HorizontalAlign="Center" style="padding:10px">
                            Aucune alerte en cours
                        </asp:Panel>
                    
                       <div id="change-alert-attribution-dialog" style="display:none;">
                            <asp:UpdatePanel ID="upChangeAlertAttribution" runat="server">
                                <ContentTemplate>
                                    <b>Réattribuer à : </b>
                                    <asp:DropDownList ID="ddlBOUsers" runat="server"  AppendDataBoundItems="true" DataValueField="Id" DataTextField="CompleteName">
                                        <asp:ListItem Value="" Text=""></asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:HiddenField ID="hdnRefAlert" runat="server" />
                                    <asp:Button ID="btnChangeAlertAttribution" runat="server" OnClick="btnChangeAlertAttribution_Click" style="display:none" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>

                    </ContentTemplate>
                    
                </asp:UpdatePanel>
            </div>
        </div>
        
        
    </div>

    <div id="ar-loading"></div>

    <script type="text/javascript" src="Scripts/jquery.stepProgressBar.js"></script>

    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            pbControl = args.get_postBackElement();
            //console.log(pbControl.id);
            var containerID = '';
            if (pbControl != null) {
                if (pbControl.id.indexOf('btnChangeAlertAttribution') != -1)
                    containerID = '#div-alert-progress';
                else if (pbControl.id.indexOf('btnRefreshTracfinStatement') != -1)
                    containerID = '#tracfin-tab';
                else if (pbControl.id.indexOf('btnRefreshAccountClose') != -1
                    || pbControl.id.indexOf('btnGoToCloseAccount') != -1)
                    containerID = '#account-close-tab';
            }

            StatusTrackingLoading(containerID);
        }
        function StatusTrackingLoading(containerID)
        {
            if (containerID.length > 0) {
                var width = $(containerID).outerWidth();
                var height = $(containerID).outerHeight();
                $('#ar-loading').css('width', width);
                $('#ar-loading').css('height', height);
                $('#ar-loading').show();
                $('#ar-loading').position({ my: "left top", at: "left top", of: containerID });
            }
        }
        function EndRequestHandler(sender, args) {
            $('#ar-loading').hide();
        }
    </script>
</asp:Content>