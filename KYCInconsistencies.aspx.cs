﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using XMLMethodLibrary;

public partial class KYCInconsistencies : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            if (CheckRights()) { BindKYCInconsistenciesList(); }
            else { Response.Redirect("Default.aspx"); }
        }

        csClientSearchFilter.ClientFound += new EventHandler(ClientFound);
    }

    private bool CheckRights()
    {
        string sXmlOut = tools.GetRights(authentication.GetCurrent().sToken, "SC_KYCInconsistenciesSearch");

        List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Page", "RC");

        if (listRC.Count > 0 && listRC[0].Trim() == "0")
        {
            List<string> listAction = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/Page/Action");

            for (int i = 0; i < listAction.Count; i++)
            {
                List<string> listTag = CommonMethod.GetAttributeValues(listAction[i], "Action", "TagAction");
                List<string> listActionAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ActionAllowed");
                List<string> listViewAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ViewAllowed");

                if (listTag.Count > 0 && listTag[0] == "KYCInconsistencies")
                {
                    if (listActionAllowed[0] == "1")
                    {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    private void BindKYCInconsistenciesList()
    {
        int iNbResult;
        DataTable dt = GetKYCInconsistenciesList(GetFilterXmlIn(), out iNbResult);

        //foreach(DataRow dr in dt.Rows)
        //{
        //    if(dr["bClosed"].ToString().Trim().ToLower() == "true")
        //    {
        //        dr.Delete();
        //    }
        //}

        //dt.AcceptChanges();

        rptKycInconsistencies.DataSource = dt;
        rptKycInconsistencies.DataBind();
        litNbResults.Text = iNbResult.ToString();

        BindPages(iNbResult);
    }

    protected void ClientFound(object sender, EventArgs e)
    {
        int _iRefCustomer = csClientSearchFilter.RefCustomer;
        if(_iRefCustomer > 0)
        {
            int iNbResult;
            DataTable dt = GetKYCInconsistenciesList(GetFilterXmlIn(_iRefCustomer), out iNbResult);
            if (dt.Rows.Count > 0)
            {
                rptKycInconsistencies.DataSource = dt;
                rptKycInconsistencies.DataBind();
                litNbResults.Text = iNbResult.ToString();
                BindPages(iNbResult);
                upKycInconsistencies.Update();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Recherche par client','Aucune alerte pour ce client');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Recherche alert par client','Une erreur est survenue.');", true);
        }
    }

    private string GetFilterXmlIn()
    {
        XElement xml = new XElement("Alert",
                    new XAttribute("TOKEN", authentication.GetCurrent().sToken),
                    new XAttribute("iPageSize", ddlPageSize.SelectedValue),
                    new XAttribute("iPageIndex", ddlPageIndex.SelectedValue),
                    new XAttribute("sAlertTypeTAG", "_INCONSISTENCY_"),
                    new XAttribute("bClosed", rblTreated.SelectedValue));
        if (ckbShowOnlyMine.Checked)
            xml.Add(new XAttribute("iRefUserWatchedBy", authentication.GetCurrent().sRef));

        return new XElement("ALL_XML_IN", xml).ToString();
    }
    private string GetFilterXmlIn(int iRefCustomer)
    {
        XElement xml = new XElement("Alert",
                    new XAttribute("TOKEN", authentication.GetCurrent().sToken),
                    new XAttribute("iPageSize", ddlPageSize.SelectedValue),
                    new XAttribute("iPageIndex", ddlPageIndex.SelectedValue),
                    new XAttribute("iRefCustomer", iRefCustomer.ToString()),
                    new XAttribute("sAlertTypeTAG", "_INCONSISTENCY_"),
                    new XAttribute("bClosed", rblTreated.SelectedValue));
        if (ckbShowOnlyMine.Checked)
            xml.Add(new XAttribute("iRefUserWatchedBy", authentication.GetCurrent().sRef));

        return new XElement("ALL_XML_IN", xml).ToString();
    }


    protected DataTable GetKYCInconsistenciesList(string sXmlIn, out int iNbResult)
    {
        SqlConnection conn = null;
        DataTable dt = new DataTable();
        iNbResult = 0;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("web.P_GetAlerts", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@sXmlIn", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@sXmlOut", SqlDbType.VarChar, -1);

            cmd.Parameters["@sXmlIn"].Value = sXmlIn;
            cmd.Parameters["@sXmlOut"].Direction = ParameterDirection.Output;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);

            string sXmlOut = cmd.Parameters["@sXmlOut"].Value.ToString();

            if(sXmlOut.Length > 0)
            {
                List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Alert", "RC");
                List<string> listNbResult = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Alert", "NB_ALERTS");

                if (listRC.Count == 0 || listRC[0] != "0")
                    dt.Clear();
                else
                {
                    int.TryParse(listNbResult[0].ToString(), out iNbResult);
                }
            }
        }
        catch(Exception e)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return dt;
    }

    private void BindPages(int iNbResult)
    {
        string sSelectedPageIndex = (ddlPageIndex != null && ddlPageIndex.Items.Count > 0) ? ddlPageIndex.SelectedValue : "1";

        int iTotalPage = CalculateTotalPages(iNbResult, int.Parse(ddlPageSize.SelectedValue));

        if (iTotalPage <= 1)
            panelPagination.Visible = false;
        else
        {
            panelPagination.Visible = true;
            litTotalPages.Text = iTotalPage.ToString();

            ddlPageIndex.DataSource = GetPageList(iTotalPage);
            ddlPageIndex.DataBind();

            ddlPageIndex.SelectedValue = sSelectedPageIndex;
        }
    }
    protected DataTable GetPageList(int iNbPages)
    {
        DataTable dt = new DataTable();

        dt.Columns.Add("T");
        dt.Columns.Add("V");

        for (int i = 1; i <= iNbPages; i++)
        {
            dt.Rows.Add(i.ToString(), i.ToString());
        }

        return dt;
    }
    protected int CalculateTotalPages(double dTotalRows, int iPageSize)
    {
        return (int)Math.Ceiling(dTotalRows / iPageSize);
    }

    protected void RefreshList_event(object sender, EventArgs e)
    {
        BindKYCInconsistenciesList();
    }

    protected void btnChangePage_Click(object sender, EventArgs e)
    {
        Button btn = (Button)sender;

        if(btn != null)
        {
            int iPageIndex = 0;
            if(int.TryParse(ddlPageIndex.SelectedValue, out iPageIndex))
            switch (btn.CommandArgument)
            {
                case "prev":
                        if (iPageIndex != 0)
                            iPageIndex--;
                    break;
                case "next":
                        if (iPageIndex < ddlPageIndex.Items.Count)
                            iPageIndex++;
                    break;
            }

            ListItem item = ddlPageIndex.Items.FindByValue(iPageIndex.ToString());
            if (item != null)
            {
                ddlPageIndex.SelectedValue = item.Value;
                BindKYCInconsistenciesList();
            }
        }
    }

    protected void btnShowAlertDetails_Click(object sender, EventArgs e)
    {
        XElement xml = new XElement("ALL_XML_IN", 
                            new XElement("Alert",
                                new XAttribute("TOKEN", authentication.GetCurrent().sToken),
                                new XAttribute("sAlertTypeTAG", "_INCONSISTENCY_"),
                                new XAttribute("iRefAlert", hdnRefAlertDetails.Value)));

        int iNbResult;
        DataTable dt = GetKYCInconsistenciesList(xml.ToString(), out iNbResult);

        if (dt.Rows.Count > 0)
        {
            string sRefUserWatchedBy = dt.Rows[0]["iRefUserWatchedBy"].ToString();
            //string sRefUserUnwatchedBy = dt.Rows[0]["iRefUserUnwatchedBy"].ToString();
            //DateTime dtWatchDate = DateTime.MinValue;
            //DateTime dtUnwatchedDate = DateTime.MinValue;

            //if(!string.IsNullOrWhiteSpace(dt.Rows[0]["dtWatchedDate"].ToString()) && DateTime.TryParse(dt.Rows[0]["dtWatchedDate"].ToString(), out dtWatchDate) &&
            //    !string.IsNullOrWhiteSpace(dt.Rows[0]["dtUnwatchedDate"].ToString()) && DateTime.TryParse(dt.Rows[0]["dtUnwatchedDate"].ToString(), out dtUnwatchedDate))
            //{
            //    btnReserveUnreserve.Visible = false;
            //    //UpdateReserveButtonState(sRefUserWatchedBy, dtWatchDate, sRefUserUnwatchedBy, dtUnwatchedDate, authentication.GetCurrent().sRef);
            //}

            UpdateReserveButtonState(sRefUserWatchedBy, authentication.GetCurrent().sRef);

            hdnRefAlert.Value = dt.Rows[0]["iRefAlert"].ToString();
            litDate.Text = dt.Rows[0]["dtAlertDate"].ToString();
            litAlertType.Text = GetAlertTypeLabel(dt.Rows[0]["sAlertIdTAG"].ToString());
            litCustomer.Text = ParseCustomerListToHTML(dt.Rows[0]["sCustomerList"].ToString(), dt.Rows[0]["sAlertIdTAG"].ToString());
            txtComment.Text = dt.Rows[0]["sUserComment"].ToString();
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "ShowAlertDetailsPopup();", true);
        }
    }

    private void UpdateReserveButtonState(string sReservedBy, string sRef)
    {
        btnReserveUnreserve.Visible = true;
        if (!String.IsNullOrWhiteSpace(sReservedBy) && sReservedBy.Trim() != "0")
        {
            if (sReservedBy == sRef)
            {
                btnReserveUnreserve.Text = "Déréserver l'alerte";
                btnReserveUnreserve.CommandArgument = "unreserve";
            }
            else btnReserveUnreserve.Visible = false;
        }
        else
        {
            btnReserveUnreserve.Text = "Réserver l'alerte";
            btnReserveUnreserve.CommandArgument = "reserve";
        }
    }

    private void UpdateReserveButtonState(string sReservedBy, DateTime dtReservationDate, string sUnreservedBy, DateTime dtUnreservationDate, string sRef)
    {
        btnReserveUnreserve.Visible = true;
        if (!String.IsNullOrWhiteSpace(sReservedBy))
        {
            if (sReservedBy == sRef)
            {
                btnReserveUnreserve.Text = "Déréserver l'alerte";
                btnReserveUnreserve.CommandArgument = "unreserve";
            }
            else btnReserveUnreserve.Visible = false;
        }
        else
        {
            btnReserveUnreserve.Text = "Réserver l'alerte";
            btnReserveUnreserve.CommandArgument = "reserve";
        }
    }

    protected void btnReserveUnreserve_Click(object sender, EventArgs e)
    {
        Button btn = (Button)sender;

        if (btn != null)
        {
            string sRef = authentication.GetCurrent().sRef;
            bool bReserve = true;

            XElement xml = new XElement("Alert",
                                new XAttribute("TOKEN", authentication.GetCurrent().sToken),
                                new XAttribute("iRefAlert", hdnRefAlert.Value));
            //xml.Add(new XAttribute("iRefUserWatchedBy", (btn.CommandArgument == "reserve") ? sRef : ""));
            if(btn.CommandArgument == "unreserve")
            {
                bReserve = false;
                //xml.Add(new XAttribute("iRefUserUnwatchedBy", sRef));
            }
                

            if (!ReserveUnreserveAlert(new XElement("ALL_XML_IN", xml).ToString(), bReserve))
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Erreur', \"Une erreur est survenue lors de l'enregistrement.\");", true);
            else
            {
                UpdateReserveButtonState((btn.CommandArgument == "reserve") ? sRef : "", sRef);
            }
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (!SetKYCInconsistencyAlertInfos(GetKYCInconsistencyTreatmentXML()))
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "AlertMessage('Erreur', \"Une erreur est survenue lors de l'enregistrement.\");", true);
        else
        {
            BindKYCInconsistenciesList();
            upKycInconsistencies.Update();
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "CloseAlertDetailsPopup();", true);
        }
    }

    protected string GetKYCInconsistencyTreatmentXML()
    {
        //XElement xml = new XElement("Alert",
        //                        new XAttribute("TOKEN", authentication.GetCurrent().sToken),
        //                        new XAttribute("iRefAlert", hdnRefAlert.Value),
        //                        new XAttribute("iAlertStatus", ddlStatus.SelectedValue),
        //                        new XAttribute("sUserComment", txtComment.Text)
        //                        );

        XElement xml = new XElement("Alert",
                                new XAttribute("TOKEN", authentication.GetCurrent().sToken),
                                new XAttribute("iRefAlert", hdnRefAlert.Value),
                                new XAttribute("sAlertStatusTAG", ddlStatus.SelectedValue),
                                new XAttribute("sUserComment", txtComment.Text)
                                );

        return new XElement("ALL_XML_IN", xml).ToString();
    }
    protected bool SetKYCInconsistencyAlertInfos(string sXmlIn)
    {
        bool bSaved = false;
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            //SqlCommand cmd = new SqlCommand("web.P_SetInconsistencyAlertInfos", conn);
            SqlCommand cmd = new SqlCommand("[dbo].[P_ChangeAlertStatus]", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@sXmlIn", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@sXmlOut", SqlDbType.VarChar, 8000);

            cmd.Parameters["@sXmlIn"].Value = sXmlIn;
            cmd.Parameters["@sXmlOut"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@sXmlOut"].Value.ToString();

            if(!String.IsNullOrWhiteSpace(sXmlOut))
            {
                List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Alert", "RC");

                if (listRC.Count > 0 && listRC[0] == "0")
                    bSaved = true;
            }
        }
        catch(Exception e)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return bSaved;
    }

    protected bool ReserveUnreserveAlert(string sXmlIn, bool Reserve)
    {
        bool bSaved = false;
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            string sProcedureName = "[dbo].[P_WatchAlert]";

            if (!Reserve) { sProcedureName = "[dbo].[P_UnwatchAlert]"; }

            SqlCommand cmd = new SqlCommand(sProcedureName, conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@sXmlIn", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@sXmlOut", SqlDbType.VarChar, 8000);

            cmd.Parameters["@sXmlIn"].Value = sXmlIn;
            cmd.Parameters["@sXmlOut"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOut = cmd.Parameters["@sXmlOut"].Value.ToString();

            if (!String.IsNullOrWhiteSpace(sXmlOut))
            {
                List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Alert", "RC");

                if (listRC.Count > 0 && listRC[0] == "0")
                    bSaved = true;
            }
        }
        catch (Exception e)
        {

        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return bSaved;
    }

    protected string ParseCustomerListToHTML(string sCustomerList)
    {
        StringBuilder sb = new StringBuilder();

        if (!String.IsNullOrWhiteSpace(sCustomerList))
        {
            string[] arCustomers = sCustomerList.Split(';');

            sb.AppendLine("<ul class=\"customer-list\">");
            for (int i = 0; i < arCustomers.Length; i++)
            {
                string[] arCustomerInfos = arCustomers[i].Split(',');

                try
                {
                    sb.AppendLine(string.Format("<li><a href=\"ClientDetails.aspx?ref={0}\" target=\"_blank\" onclick=\"event.stopPropagation();\">{1} {2}</a></li>", arCustomerInfos[0], arCustomerInfos[1], arCustomerInfos[2]));
                }
                catch(Exception ex)
                {
                    sb.AppendLine(string.Format("<li>??? ???</li>"));
                }
            }
            sb.AppendLine("</ul>");
        }

        return sb.ToString();
    }
    protected string ParseCustomerListToHTML(string sCustomerList, string sAlertTAG)
    {
        StringBuilder sb = new StringBuilder();

        if (!String.IsNullOrWhiteSpace(sCustomerList))
        {
            string[] arCustomers = sCustomerList.Split(';');

            sb.AppendLine("<ul class=\"customer-list\">");
            for (int i = 0; i < arCustomers.Length; i++)
            {
                string[] arCustomerInfos = arCustomers[i].Split(',');

                try
                {
                    sb.AppendLine(string.Format("<li><a href=\"ClientDetails.aspx?ref={0}\" target=\"_blank\" onclick=\"event.stopPropagation();\">{1} {2}</a>", arCustomerInfos[0], arCustomerInfos[1], arCustomerInfos[2]));
                }
                catch(Exception ex)
                {
                    sb.AppendLine(string.Format("<li>??? ???</li>"));
                }

                try
                {
                    string sImageUrl = "";
                    string sImageAlt = "";
                    string sRefSpecificity = "";
                    switch (sAlertTAG)
                    {
                        case "_INC_DOUBLE_PHONE_":
                            sImageUrl = "./Styles/Img/phone.png";
                            sImageAlt = "phone";
                            sRefSpecificity = "87";
                            break;
                        case "_INC_DOUBLE_EMAIL_":
                            sImageUrl = "./Styles/Img/email.png";
                            sImageAlt = "email";
                            sRefSpecificity = "90";
                            break;
                    }
                    if (sImageUrl.Trim().Length > 0 && !AML.isClientHasSpecificity(int.Parse(arCustomerInfos[0]), sRefSpecificity))
                    {
                        sb.AppendLine(string.Format("<img src=\"" + sImageUrl + "\" alt=\"" + sImageAlt + "\" height=\"20\" onclick=\"ApplySpecification('" + sAlertTAG + "'," + arCustomerInfos[0] + ")\" />"));
                    }
                }
                catch(Exception ex) { }

                sb.AppendLine(string.Format("</li>"));
            }
            sb.AppendLine("</ul>");
        }

        return sb.ToString();
    }

    protected string GetAlertTypeLabel(string sType)
    {
        switch(sType)
        {
            case "_INC_OLD_STUDENT_":
                return "Etudiants > 32 ans";
            case "_INC_YOUNG_RETIRED_":
                return "Retraité < 45 ans";
            case "_INC_OLD_WORKER_":
                return "Clients > 80 ans avec activité pro";
            case "_INC_DOUBLE_NAME_":
                return "Nom + prénom identiques";
            case "_INC_DOUBLE_PHONE_":
                return "Tel identique";
            case "_INC_DOUBLE_EMAIL_":
                return "Mail identique";
            case "_INC_DOUBLE_ID_":
                return "Numero ID identique";
            default:
                return sType;
        } 
    }

    protected void lbDeleteFilters_Click(object sender, EventArgs e)
    {
        rblTreated.Items[0].Selected = true;
        ckbShowOnlyMine.Checked = false;
        BindKYCInconsistenciesList();
    }

    protected void btnApplySpecification_Click(object sender, EventArgs e)
    {
        try
        {
            string sRefCustomer = hfRefCustomer.Value;
            string AlertTAG = hfAlertTAG.Value;
            int iRefCustomer = int.Parse(sRefCustomer);

            if (iRefCustomer > 0 && AlertTAG.Trim().Length > 0)
            {
                authentication auth = authentication.GetCurrent();
                //<option value="90">KYC - Appel doublon email</option>
                //<option value="87">KYC - Appel doublon téléphone</option>
                //Appliquer spécification
                switch (AlertTAG)
                {
                    case "_INC_DOUBLE_PHONE_":
                        if(AML.addClientSpecificity(auth.sToken, iRefCustomer, "87"))
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "RefreshAlertDetailsPopup(" + hdnRefAlert.Value + ");", true);
                        }
                        break;
                    case "_INC_DOUBLE_EMAIL_":
                        if(AML.addClientSpecificity(auth.sToken, iRefCustomer, "90"))
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "RefreshAlertDetailsPopup(" + hdnRefAlert.Value + ");", true);
                        }
                        break;
                }
            }
        }
        catch(Exception ex)
        {

        }
    }
}