﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Xml.Linq;
using XMLMethodLibrary;
using System.Drawing;

public partial class UserBOManagement : System.Web.UI.Page
{
    private DataTable _dtUsers { get { return (DataTable)ViewState["dtUsers"]; } set { ViewState["dtUsers"] = value; } }
    private DataTable _dtProfiles { get { return (DataTable)ViewState["dtProfiles"]; } set { ViewState["dtProfiles"] = value; } }
    private bool _bIsAddAllowed { get { return (ViewState["bIsAddAllowed"] != null) ? bool.Parse(ViewState["bIsAddAllowed"].ToString()) : false; } set { ViewState["bIsAddAllowed"] = value; } }
    private bool _bIsUpdateAllowed { get { return (ViewState["bIsUpdateAllowed"] != null) ? bool.Parse(ViewState["bIsUpdateAllowed"].ToString()) : false; } set { ViewState["bIsUpdateAllowed"] = value; } }
    private bool _bIsDelAllowed { get { return (ViewState["bIsDelAllowed"] != null) ? bool.Parse(ViewState["bIsDelAllowed"].ToString()) : false; } set { ViewState["bIsDelAllowed"] = value; } }
    private DataTable _dtUserIP { get { return (DataTable)ViewState["dtUserIP"]; } set { ViewState["dtUserIP"] = value; } }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            CheckRights();
            SetProfileList();
            SetUserList(tools.GetUserList(authentication.GetCurrent(false).sToken));
        }

        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "InitPageKey", "InitPage();", true);
    }

    protected void CheckRights()
    {
        string sXmlOut = tools.GetRights(authentication.GetCurrent().sToken, "SC_UserManagement");

        List<string> listRC = CommonMethod.GetAttributeValues(sXmlOut, "ALL_XML_OUT/Page", "RC");

        if (listRC.Count == 0 || listRC[0] != "0")
        {
            Response.Redirect("Default.aspx", true);
        }
        else
        {
            List<string> listAction = CommonMethod.GetTags(sXmlOut, "ALL_XML_OUT/Page/Action");

            for (int i = 0; i < listAction.Count; i++)
            {
                List<string> listTagAction = CommonMethod.GetAttributeValues(listAction[i], "Action", "TagAction");
                List<string> listActionAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ActionAllowed");
                List<string> listViewAllowed = CommonMethod.GetAttributeValues(listAction[i], "Action", "ViewAllowed");

                if (listTagAction.Count > 0)
                {
                    switch (listTagAction[0])
                    {
                        case "UserList":
                            if (listViewAllowed.Count > 0 && listViewAllowed[0] == "1")
                            {
                                panelList.Visible = true;
                            }
                            else
                            {
                                panelList.Visible = false;
                            }
                            break;
                        case "AddUser":
                            if (listActionAllowed.Count > 0 && listActionAllowed[0] == "1")
                            {
                                _bIsAddAllowed = true;
                                panelAddUser.Visible = true;
                            }
                            else
                            {
                                _bIsAddAllowed = false;
                                panelAddUser.Visible = false;
                            }
                            break;
                        case "UpdateUser":
                            if (listActionAllowed.Count > 0 && listActionAllowed[0] == "1")
                            {
                                _bIsUpdateAllowed = true;
                                txtNewIP.Visible = true;
                                btnAddNewIP.Visible = true;
                            }
                            else
                            {
                                _bIsUpdateAllowed = false;
                                txtNewIP.Visible = false;
                                btnAddNewIP.Visible = false;
                            }
                            break;
                        case "DelUser":
                            if (listActionAllowed.Count > 0 && listActionAllowed[0] == "1")
                                _bIsDelAllowed = true;
                            else _bIsDelAllowed = false;
                            break;
                    }
                }
            }
        }
    }

    #region // User list

    protected void SetProfileList()
    {
        SqlConnection conn = null;
        DataTable dt = new DataTable();

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("ServiceClient.P_GetTAGProfileList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);

            _dtProfiles = dt;

            ddlProfile.DataSource = dt;
            ddlProfile.DataBind();
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }
    }
    
    protected void SetUserList(string sXml)
    {
        if (sXml.Length > 0)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("RefUID");
            dt.Columns.Add("FirstName");
            dt.Columns.Add("LastName");
            dt.Columns.Add("Email");
            dt.Columns.Add("TAGProfile");
            dt.Columns.Add("Profile");
            dt.Columns.Add("Disabled");
            dt.Columns.Add("LabelDisabled");
            dt.Columns.Add("IsUpdateAllowed");
            dt.Columns.Add("IsDelAllowed");
            dt.Columns.Add("IPAddress");

            List<string> listUser = CommonMethod.GetTags(sXml, "ALL_XML_OUT/BOUser");

            for (int i = 0; i < listUser.Count; i++)
            {
                List<string> listRefUID = CommonMethod.GetAttributeValues(listUser[i], "BOUser", "RefUID");
                List<string> listLastName = CommonMethod.GetAttributeValues(listUser[i], "BOUser", "LastName");
                List<string> listFirstName = CommonMethod.GetAttributeValues(listUser[i], "BOUser", "FirstName");
                List<string> listEmail = CommonMethod.GetAttributeValues(listUser[i], "BOUser", "Email");
                List<string> listProfile = CommonMethod.GetAttributeValues(listUser[i], "BOUser", "TAGProfile");
                List<string> listDisabled = CommonMethod.GetAttributeValues(listUser[i], "BOUser", "Disabled");
                List<string> listIPAddress = CommonMethod.GetAttributeValues(listUser[i], "BOUser/IP", "IPAddress");

                DataRow row = dt.NewRow();
                row["RefUID"] = (listRefUID.Count > 0) ? listRefUID[0] : "";
                row["FirstName"] = (listFirstName.Count > 0) ? listFirstName[0] : "";
                row["LastName"] = (listLastName.Count > 0) ? listLastName[0] : "";
                row["Email"] = (listEmail.Count > 0) ? listEmail[0] : "";
                row["TAGProfile"] = (listProfile.Count > 0) ? listProfile[0] : "";
                if (listProfile.Count > 0)
                {
                    ListItem item = ddlProfile.Items.FindByValue(listProfile[0].ToString());
                    row["Profile"] = (item != null) ? item.Text : listProfile[0].ToString();
                }
                row["Disabled"] = (listDisabled.Count > 0) ? listDisabled[0] : "0";
                row["LabelDisabled"] = (listDisabled.Count > 0 && listDisabled[0] == "0") ? "Actif" : "Bloqué";

                row["IsUpdateAllowed"] = (_bIsUpdateAllowed) ? "1" : "0";
                row["IsDelAllowed"] = (_bIsDelAllowed) ? "1" : "0";

                row["IPAddress"] = String.Join(";", listIPAddress);

                dt.Rows.Add(row);
            }

            _dtUsers = dt;

            rptUser.DataSource = OrderDataTable(dt);
            rptUser.DataBind();
        }
    }

    protected DataTable FilterDataTable(DataTable dt, string sFilter)
    {
        DataTable dtTmp = dt.Clone();

        for (int x = 0; x < dt.Rows.Count; x++)
        {
            bool bFound = false;

            for (int y = 0; y < dt.Columns.Count; y++)
            {
                if (dt.Rows[x][y].ToString().ToLower().Contains(sFilter.ToLower()))
                {
                    bFound = true;
                    break;
                }
            }

            if (bFound)
                dtTmp.Rows.Add(dt.Rows[x].ItemArray);
        }

        return dtTmp;
    }
    protected void btnFilter_Click(object sender, EventArgs e)
    {
        txtFilter.Focus();

        rptUser.DataSource = OrderDataTable(FilterDataTable(_dtUsers, txtFilter.Text));
        rptUser.DataBind();
    }
    protected DataTable OrderDataTable(DataTable dt)
    {
        DataView dv = dt.DefaultView;
        dv.Sort = "LastName asc";
        return dv.ToTable();
    }
    protected void rptUser_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            if (((HiddenField)e.Item.FindControl("hdnDisabled")).Value == "1")
                ((Label)e.Item.FindControl("lblDisabled")).ForeColor = Color.Red;
            else ((Label)e.Item.FindControl("lblDisabled")).ForeColor = Color.Green;

            if (((HiddenField)e.Item.FindControl("hdnIsUpdateAllowed")).Value == "0")
            {
                ((ImageButton)e.Item.FindControl("btnEdit")).Visible = false;
                //((ImageButton)e.Item.FindControl("btnIP")).Visible = false;
            }
            if (((HiddenField)e.Item.FindControl("hdnIsDelAllowed")).Value == "0")
                ((ImageButton)e.Item.FindControl("btnRemove")).Visible = false;
        }
    }

    protected void UpdateUserList()
    {
        SetUserList(tools.GetUserList(authentication.GetCurrent().sToken));
        upFilter.Update();
    }

    #endregion

    #region // Add / Del / Set Users

    protected void btnAddSetUser_Click(object sender, EventArgs e)
    {
        string sXmlIN = "";
        string sToken = authentication.GetCurrent(false).sToken;

        bool bAddSet = true;

        if (hdnRefUID.Value.Length == 0)
        {
            for (int i = 0; i < _dtUsers.Rows.Count; i++)
            {
                if (_dtUsers.Rows[i]["Email"].ToString().ToLower().Trim() == txtEmail.Text.ToLower().Trim())
                {
                    bAddSet = false;
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "AlertMessageKey", "HidePopup();AlertMessage('Erreur', 'Adresse email \""+txtEmail.Text+"\" déjà associée à un utilisateur.', null, null);", true);
                    break;
                }
            }

            sXmlIN = new XElement("ALL_XML_IN", new XElement("BOUser",
                                                        new XAttribute("BOUserToken", sToken),
                                                        new XAttribute("Action", "A"),
                                                        new XAttribute("LastName", txtLastName.Text),
                                                        new XAttribute("FirstName", txtFirstName.Text),
                                                        new XAttribute("BirthDate", txtBirthdate.Text),
                                                        new XAttribute("Email", txtEmail.Text),
                                                        new XAttribute("TAGProfile", ddlProfile.SelectedValue))).ToString();
        }
        else
        {
            sXmlIN = new XElement("ALL_XML_IN", new XElement("BOUser",
                                                        new XAttribute("BOUserToken", sToken),
                                                        new XAttribute("Action", "U"),
                                                        new XAttribute("RefUID", hdnRefUID.Value),
                                                        new XAttribute("LastName", txtLastName.Text),
                                                        new XAttribute("FirstName", txtFirstName.Text),
                                                        new XAttribute("Email", txtEmail.Text),
                                                        new XAttribute("Disabled", rblStatus.SelectedValue),
                                                        new XAttribute("TAGProfile", ddlProfile.SelectedValue))).ToString();
        }

        if (bAddSet)
        {
            if (AddDelSetUser(sXmlIN))
            {
                UpdateUserList();

                string sMessage = "";
                if (hdnRefUID.Value.Length == 0)
                    sMessage = "<span style=\"color:green\">Utilisateur ajouté</span>";
                else
                {
                    sMessage = "<span style=\"color:green\">Utilisateur modifié</span>";
                    btnFilter_Click(sender, e);
                }

                hdnRefUID.Value = "";
                txtLastName.Text = "";
                txtFirstName.Text = "";
                txtBirthdate.Text = "";
                txtEmail.Text = "";
                rblStatus.SelectedValue = "0";
                ddlProfile.SelectedIndex = 0;

                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "AlertMessageKey", "HidePopup();AlertMessage('Succès', '" + sMessage + "', null, null);", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "AlertMessageKey", "HidePopup();AlertMessage('Erreur', 'Une erreur est survenue', null, null);", true);
            }
        }
    }
    protected void btnEdit_Click(object sender, ImageClickEventArgs e)
    {
        if (_bIsUpdateAllowed)
        {
            ImageButton btn = (ImageButton)sender;

            if (btn != null && btn.CommandArgument.Length > 0)
            {
                hdnRefUID.Value = btn.CommandArgument;

                Label lbl = (Label)btn.Parent.FindControl("lblLastName");
                if (lbl != null)
                    txtLastName.Text = lbl.Text;
                lbl = (Label)btn.Parent.FindControl("lblFirstName");
                if (lbl != null)
                    txtFirstName.Text = lbl.Text;
                lbl = (Label)btn.Parent.FindControl("lblEmail");
                if (lbl != null)
                    txtEmail.Text = lbl.Text;

                panelBirthdate.Visible = false;
                panelDisabled.Visible = true;

                HiddenField hdn = (HiddenField)btn.Parent.FindControl("hdnTAGProfile");
                if (hdn != null)
                {
                    ListItem item = ddlProfile.Items.FindByValue(hdn.Value);
                    ddlProfile.SelectedIndex = (item != null) ? ddlProfile.Items.IndexOf(item) : 0;
                }
                hdn = (HiddenField)btn.Parent.FindControl("hdnDisabled");
                if (hdn != null)
                {
                    rblStatus.SelectedValue = hdn.Value;
                }

                upPopupAddSet.Update();

                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "ShowPopupKey", "ShowPopup('Modifier');", true);
            }
        }
    }

    protected bool AddDelSetUser(string sXmlIN)
    {
        SqlConnection conn = null;
        bool bOk = false;

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NOBANK"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("ServiceClient.P_AddDelSetBOUser", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@IN", SqlDbType.VarChar, 8000);
            cmd.Parameters.Add("@OUT", SqlDbType.VarChar, 8000);

            cmd.Parameters["@IN"].Value = sXmlIN;
            cmd.Parameters["@OUT"].Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string sXmlOUT = cmd.Parameters["@OUT"].Value.ToString();

            List<string> listUpdated = CommonMethod.GetAttributeValues(sXmlOUT, "ALL_XML_OUT/Result", "Updated");
            List<string> listInserted = CommonMethod.GetAttributeValues(sXmlOUT, "ALL_XML_OUT/Result", "Inserted");
            List<string> listDeleted = CommonMethod.GetAttributeValues(sXmlOUT, "ALL_XML_OUT/Result", "Deleted");

            if ((listUpdated.Count > 0 && listUpdated[0] == "1") || (listInserted.Count > 0 && listInserted[0] == "1") || (listDeleted.Count > 0 && listDeleted[0] == "1"))
            {
                bOk = true;
            }
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (conn != null)
                conn.Close();
        }

        return bOk;
    }
    protected void btnAddUser_Click(object sender, EventArgs e)
    {
        hdnRefUID.Value = "";
        txtLastName.Text = "";
        txtFirstName.Text = "";
        txtEmail.Text = "";
        txtBirthdate.Text = "";
        ddlProfile.SelectedIndex = 0;

        panelBirthdate.Visible = true;
        panelDisabled.Visible = false;

        upPopupAddSet.Update();

        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "ShowPopupKey", "ShowPopup('Ajouter un utilisateur');", true);
    }

    protected void btnRemove_Click(object sender, ImageClickEventArgs e)
    {
        if (_bIsDelAllowed)
        {
            ImageButton btn = (ImageButton)sender;

            if (btn != null && btn.CommandArgument.Length > 0)
            {
                hdnRefUIDToDelete.Value = btn.CommandArgument;

                Label lbl = (Label)btn.Parent.FindControl("lblLastName");
                if (lbl != null)
                    lblLastNameToDelete.Text = lbl.Text;
                lbl = (Label)btn.Parent.FindControl("lblFirstName");
                if (lbl != null)
                    lblFirstNameToDelete.Text = lbl.Text;

                upPopupDelete.Update();

                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "ShowDeletePopupKey", "ShowDeletePopup();", true);
            }
        }
    }
    protected void btnDeleteUser_Click(object sender, EventArgs e)
    {
        if (_bIsDelAllowed)
        {
            string sToken = authentication.GetCurrent(false).sToken;

            string sXmlIN = new XElement("ALL_XML_IN", new XElement("BOUser",
                                                            new XAttribute("BOUserToken", sToken),
                                                            new XAttribute("Action", "D"),
                                                            new XAttribute("RefUID", hdnRefUIDToDelete.Value))).ToString();

            if (AddDelSetUser(sXmlIN))
            {
                string sMessage = "";
                if (hdnRefUID.Value.Length == 0)
                    sMessage = "<span style=\"color:green\">Utilisateur supprimé</span>";

                hdnRefUIDToDelete.Value = "";
                UpdateUserList();

                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "AlertMessageKey", "AlertMessage('Succès', '" + sMessage + "', null, null);", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "AlertMessageKey", "AlertMessage('Erreur', 'Une erreur est survenue', null, null);", true);
            }
        }
    }

    #endregion

    #region // IP

    protected void btnIP_Click(object sender, EventArgs e)
    {
        ImageButton btn = (ImageButton)sender;

        if (btn != null && btn.CommandArgument.Length > 0)
        {
            hdnRefUIDIP.Value = btn.CommandArgument;

            HiddenField hdn = (HiddenField)btn.Parent.FindControl("hdnIP");
            if (hdn != null)
            {
                string[] arIP = hdn.Value.Split(';');

                if (arIP.Length > 0)
                {
                    DataTable dt = new DataTable();
                    dt.Columns.Add("IPAddress");

                    for (int i = 0; i < arIP.Length; i++)
                    {
                        if (arIP[i].Length > 0)
                        {
                            DataRow row = dt.NewRow();
                            row["IPAddress"] = arIP[i];
                            dt.Rows.Add(row);
                        }
                    }

                    _dtUserIP = dt;
                    rptIP.DataSource = dt;
                    rptIP.DataBind();
                    upIP.Update();
                }
            }

            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "ShowIPPopupKey", "ShowIPPopup();", true);
        }
    }
    protected void btnAddNewIP_Click(object sender, ImageClickEventArgs e)
    {
        if (_bIsUpdateAllowed)
        {
            string sToken = authentication.GetCurrent(false).sToken;

            string sXmlIN = new XElement("ALL_XML_IN", new XElement("BOUser",
                                                        new XAttribute("BOUserToken", sToken),
                                                        new XElement("IP",
                                                            new XAttribute("RefUID", hdnRefUIDIP.Value),
                                                            new XAttribute("Action", "A"),
                                                            new XAttribute("IPAddress", txtNewIP.Text)))).ToString();

            if (AddDelSetUser(sXmlIN))
            {
                DataTable dt = _dtUserIP.Copy();
                dt.Rows.Add(new object[] { txtNewIP.Text });
                _dtUserIP = dt;
                rptIP.DataSource = dt;
                rptIP.DataBind();
                upIP.Update();

                txtNewIP.Text = "";

                UpdateUserList();
                btnFilter_Click(sender, e);

                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "AlertMessageKey", "AlertMessage('Succès', '<span style=\"color:green\">Adresse IP autorisée</span>', null, null);", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "AlertMessageKey", "AlertMessage('Erreur', 'Une erreur est survenue', null, null);", true);
            }
        }
    }
    protected void btnRemoveIP_Click(object sender, EventArgs e)
    {
        if (_bIsUpdateAllowed)
        {
            ImageButton btn = (ImageButton)sender;

            if (btn != null && btn.CommandArgument.Length > 0)
            {
                string sToken = authentication.GetCurrent(false).sToken;

                string sXmlIN = new XElement("ALL_XML_IN", new XElement("BOUser",
                                                            new XAttribute("BOUserToken", sToken),
                                                            new XElement("IP",
                                                                new XAttribute("RefUID", hdnRefUIDIP.Value),
                                                                new XAttribute("Action", "D"),
                                                                new XAttribute("IPAddress", btn.CommandArgument)))).ToString();

                if (AddDelSetUser(sXmlIN))
                {
                    DataTable dt = _dtUserIP.Clone();

                    for (int i = 0; i < _dtUserIP.Rows.Count; i++)
                    {
                        if (_dtUserIP.Rows[i][0].ToString() != btn.CommandArgument)
                            dt.Rows.Add(_dtUserIP.Rows[i].ItemArray);
                    }
                    _dtUserIP = dt;
                    rptIP.DataSource = dt;
                    rptIP.DataBind();
                    upIP.Update();

                    UpdateUserList();
                    btnFilter_Click(sender, e);

                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "AlertMessageKey", "AlertMessage('Succès', '<span style=\"color:green\">Adresse IP révoquée</span>', null, null);", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "AlertMessageKey", "AlertMessage('Erreur', 'Une erreur est survenue', null, null);", true);
                }
            }
        }
    }

    protected void rptIP_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            if (!_bIsUpdateAllowed)
            {
                ((ImageButton)e.Item.FindControl("btnRemoveIP")).Visible = false;
            }
        }
    }

    #endregion
}